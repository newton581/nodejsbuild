	.file	"startup-deserializer.cc"
	.text
	.section	.rodata._ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"StartupDeserializer"
	.section	.text._ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE
	.type	_ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE, @function
_ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE:
.LFB21372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE@PLT
	leaq	328(%r12), %rdi
	call	_ZN2v88internal21DeserializerAllocator12ReserveSpaceEv@PLT
	testb	%al, %al
	je	.L19
	leaq	37592(%r13), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap15IterateSmiRootsEPNS0_11RootVisitorE@PLT
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap18IterateStrongRootsEPNS0_11RootVisitorENS0_9VisitModeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal22SerializerDeserializer7IterateEPNS0_7IsolateEPNS0_11RootVisitorE@PLT
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap16IterateWeakRootsEPNS0_11RootVisitorENS0_9VisitModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal12Deserializer26DeserializeDeferredObjectsEv@PLT
	leaq	208(%r12), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_12AccessorInfoESaIS3_EE@PLT
	leaq	232(%r12), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_15CallHandlerInfoESaIS3_EE@PLT
	movq	80(%r12), %rax
	movq	37856(%rax), %rax
	movq	32(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	movq	40(%rbx), %rdi
	movq	48(%rbx), %rsi
	subq	%rdi, %rsi
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L4
.L3:
	movq	88(%r13), %rax
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rcx
	movq	%rax, 39120(%r13)
	cmpq	%rcx, 39128(%r13)
	jne	.L5
	movq	%rax, 39128(%r13)
.L5:
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	movb	$1, 41192(%r13)
	jne	.L20
.L7:
	cmpb	$0, _ZN2v88internal20FLAG_rehash_snapshotE(%rip)
	je	.L1
	cmpb	$0, 593(%r12)
	jne	.L21
.L1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	80(%r12), %rax
	movq	41016(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L7
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10LogAllMapsEv@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L21:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12Deserializer6RehashEv@PLT
.L19:
	.cfi_restore_state
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.cfi_endproc
.LFE21372:
	.size	_ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE, .-_ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE
	.section	.text._ZN2v88internal19StartupDeserializer15LogNewMapEventsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19StartupDeserializer15LogNewMapEventsEv
	.type	_ZN2v88internal19StartupDeserializer15LogNewMapEventsEv, @function
_ZN2v88internal19StartupDeserializer15LogNewMapEventsEv:
.LFB21373:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	jne	.L33
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	80(%rdi), %rax
	movq	41016(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L34
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Logger10LogAllMapsEv@PLT
	.cfi_endproc
.LFE21373:
	.size	_ZN2v88internal19StartupDeserializer15LogNewMapEventsEv, .-_ZN2v88internal19StartupDeserializer15LogNewMapEventsEv
	.section	.text._ZN2v88internal19StartupDeserializer11FlushICacheEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19StartupDeserializer11FlushICacheEv
	.type	_ZN2v88internal19StartupDeserializer11FlushICacheEv, @function
_ZN2v88internal19StartupDeserializer11FlushICacheEv:
.LFB21374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	80(%rdi), %rax
	movq	37856(%rax), %rax
	movq	32(%rax), %rbx
	testq	%rbx, %rbx
	je	.L35
	.p2align 4,,10
	.p2align 3
.L37:
	movq	40(%rbx), %rdi
	movq	48(%rbx), %rsi
	subq	%rdi, %rsi
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L37
.L35:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21374:
	.size	_ZN2v88internal19StartupDeserializer11FlushICacheEv, .-_ZN2v88internal19StartupDeserializer11FlushICacheEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE:
.LFB27321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27321:
	.size	_GLOBAL__sub_I__ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
