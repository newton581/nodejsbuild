	.file	"startup-serializer.cc"
	.text
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB6715:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE6715:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,"axG",@progbits,_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.type	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, @function
_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE:
.LFB6716:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6716:
	.size	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, .-_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.rodata._ZN2v88internal17StartupSerializerD2Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"StartupSerializer"
	.section	.text._ZN2v88internal17StartupSerializerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17StartupSerializerD2Ev
	.type	_ZN2v88internal17StartupSerializerD2Ev, @function
_ZN2v88internal17StartupSerializerD2Ev:
.LFB20420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal17StartupSerializerE(%rip), %rax
	leaq	504(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_12AccessorInfoESaIS3_EE@PLT
	leaq	528(%r12), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_15CallHandlerInfoESaIS3_EE@PLT
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal10Serializer16OutputStatisticsEPKc@PLT
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movq	504(%r12), %rdi
	testq	%rdi, %rdi
	je	.L6
	call	_ZdlPv@PLT
.L6:
	leaq	16+_ZTVN2v88internal15RootsSerializerE(%rip), %rax
	movq	456(%r12), %rdi
	movq	%rax, (%r12)
	call	free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10SerializerD2Ev@PLT
	.cfi_endproc
.LFE20420:
	.size	_ZN2v88internal17StartupSerializerD2Ev, .-_ZN2v88internal17StartupSerializerD2Ev
	.globl	_ZN2v88internal17StartupSerializerD1Ev
	.set	_ZN2v88internal17StartupSerializerD1Ev,_ZN2v88internal17StartupSerializerD2Ev
	.section	.text._ZN2v88internal23SerializedHandleCheckerD0Ev,"axG",@progbits,_ZN2v88internal23SerializedHandleCheckerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23SerializedHandleCheckerD0Ev
	.type	_ZN2v88internal23SerializedHandleCheckerD0Ev, @function
_ZN2v88internal23SerializedHandleCheckerD0Ev:
.LFB26217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23SerializedHandleCheckerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L15
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L16
.L15:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r12), %rdi
	leaq	64(%r12), %rax
	movq	$0, 40(%r12)
	movq	$0, 32(%r12)
	cmpq	%rax, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	popq	%rbx
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26217:
	.size	_ZN2v88internal23SerializedHandleCheckerD0Ev, .-_ZN2v88internal23SerializedHandleCheckerD0Ev
	.section	.rodata._ZN2v88internal23SerializedHandleChecker17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"global"
.LC2:
	.string	"eternal"
.LC3:
	.string	"%s handle not serialized: "
	.section	.text._ZN2v88internal23SerializedHandleChecker17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23SerializedHandleChecker17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.type	_ZN2v88internal23SerializedHandleChecker17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, @function
_ZN2v88internal23SerializedHandleChecker17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_:
.LFB20453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r8, %rcx
	jnb	.L23
	cmpl	$13, %esi
	leaq	.LC1(%rip), %rbx
	movq	%rdi, %r14
	movq	%r8, %r15
	leaq	.LC2(%rip), %rax
	movq	%rcx, %r13
	leaq	-64(%rbp), %r12
	cmovne	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L29:
	movq	0(%r13), %r9
	movq	24(%r14), %r10
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%r10
	movq	16(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rcx
	testq	%rax, %rax
	je	.L25
	movq	(%rax), %rsi
	movq	16(%rsi), %rdi
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L25
	movq	16(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r10
	cmpq	%rdx, %rcx
	jne	.L25
.L28:
	cmpq	%rdi, %r9
	jne	.L26
	cmpq	8(%rsi), %r9
	jne	.L26
	addq	$8, %r13
	cmpq	%r13, %r15
	ja	.L29
.L23:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	%rbx, %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	addq	$8, %r13
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-8(%r13), %rax
	movq	stdout(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movb	$0, 72(%r14)
	cmpq	%r13, %r15
	ja	.L29
	jmp	.L23
.L42:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20453:
	.size	_ZN2v88internal23SerializedHandleChecker17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, .-_ZN2v88internal23SerializedHandleChecker17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.section	.text._ZN2v88internal23SerializedHandleCheckerD2Ev,"axG",@progbits,_ZN2v88internal23SerializedHandleCheckerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23SerializedHandleCheckerD2Ev
	.type	_ZN2v88internal23SerializedHandleCheckerD2Ev, @function
_ZN2v88internal23SerializedHandleCheckerD2Ev:
.LFB26215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23SerializedHandleCheckerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L44
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L45
.L44:
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	addq	$64, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L43
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26215:
	.size	_ZN2v88internal23SerializedHandleCheckerD2Ev, .-_ZN2v88internal23SerializedHandleCheckerD2Ev
	.weak	_ZN2v88internal23SerializedHandleCheckerD1Ev
	.set	_ZN2v88internal23SerializedHandleCheckerD1Ev,_ZN2v88internal23SerializedHandleCheckerD2Ev
	.section	.rodata._ZN2v88internal17StartupSerializer15SerializeObjectENS0_10HeapObjectE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"bitset::test"
	.section	.rodata._ZN2v88internal17StartupSerializer15SerializeObjectENS0_10HeapObjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"%s: __position (which is %zu) >= _Nb (which is %zu)"
	.section	.text._ZN2v88internal17StartupSerializer15SerializeObjectENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17StartupSerializer15SerializeObjectENS0_10HeapObjectE
	.type	_ZN2v88internal17StartupSerializer15SerializeObjectENS0_10HeapObjectE, @function
_ZN2v88internal17StartupSerializer15SerializeObjectENS0_10HeapObjectE:
.LFB20423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal10Serializer18SerializeHotObjectENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L105
.L52:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movq	144(%r12), %rax
	movl	8(%rax), %ecx
	movq	(%rax), %rsi
	subl	$1, %ecx
	movl	%ecx, %eax
	andl	%ebx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	jne	.L56
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L107:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L58
.L56:
	cmpq	(%rdx), %rbx
	jne	.L107
	movl	8(%rdx), %ecx
	movzwl	%cx, %edx
	cmpw	$599, %cx
	ja	.L108
	movl	$1, %eax
	shrq	$6, %rdx
	salq	%cl, %rax
	testq	%rax, 368(%r12,%rdx,8)
	je	.L58
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer13SerializeRootENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L52
.L58:
	leaq	80(%r12), %r13
	movq	496(%r12), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal18ReadOnlySerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L52
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer22SerializeBackReferenceENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L52
	movq	-1(%rbx), %rax
	cmpw	$96, 11(%rax)
	je	.L59
	leaq	-96(%rbp), %r14
.L62:
	movq	-1(%rbx), %rax
	cmpw	$160, 11(%rax)
	je	.L109
.L61:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal15RootsSerializer18CheckRehashabilityENS0_10HeapObjectE@PLT
	leaq	16+_ZTVN2v88internal10Serializer16ObjectSerializerE(%rip), %rax
	movq	%r14, %rdi
	movq	%r12, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%r13, -72(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal10Serializer16ObjectSerializer9SerializeEv@PLT
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	-96(%rbp), %r14
	movq	%rbx, -96(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L62
	movq	104(%r12), %rax
	leaq	39(%rbx), %rsi
	movq	3840(%rax), %r15
	movq	%r15, 39(%rbx)
	testb	$1, %r15b
	je	.L61
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	testl	$262144, %eax
	je	.L64
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	8(%rcx), %rax
.L64:
	testb	$24, %al
	je	.L61
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L61
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L109:
	movq	31(%rbx), %rax
	testb	$1, %al
	jne	.L110
.L66:
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L111
.L68:
	movq	7(%rbx), %rax
	testb	$1, %al
	je	.L61
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L77
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L61
.L77:
	movq	7(%rbx), %r15
	leaq	7(%r15), %rsi
	movq	104(%r12), %rax
	movq	128(%rax), %rdx
	movq	%rdx, 7(%r15)
	testb	$1, %dl
	je	.L61
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -120(%rbp)
	testl	$262144, %eax
	je	.L78
	movq	%r15, %rdi
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	8(%rcx), %rax
.L78:
	testb	$24, %al
	je	.L61
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L61
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L111:
	movq	7(%rbx), %rax
	testb	$1, %al
	je	.L61
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L61
	jmp	.L68
.L110:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L112
.L67:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	je	.L68
	jmp	.L66
.L112:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L67
	jmp	.L66
.L106:
	call	__stack_chk_fail@PLT
.L108:
	movl	$600, %ecx
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE20423:
	.size	_ZN2v88internal17StartupSerializer15SerializeObjectENS0_10HeapObjectE, .-_ZN2v88internal17StartupSerializer15SerializeObjectENS0_10HeapObjectE
	.section	.text._ZN2v88internal17StartupSerializerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17StartupSerializerD0Ev
	.type	_ZN2v88internal17StartupSerializerD0Ev, @function
_ZN2v88internal17StartupSerializerD0Ev:
.LFB20422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal17StartupSerializerE(%rip), %rax
	leaq	504(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_12AccessorInfoESaIS3_EE@PLT
	leaq	528(%r12), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_15CallHandlerInfoESaIS3_EE@PLT
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal10Serializer16OutputStatisticsEPKc@PLT
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L114
	call	_ZdlPv@PLT
.L114:
	movq	504(%r12), %rdi
	testq	%rdi, %rdi
	je	.L115
	call	_ZdlPv@PLT
.L115:
	leaq	16+_ZTVN2v88internal15RootsSerializerE(%rip), %rax
	movq	456(%r12), %rdi
	movq	%rax, (%r12)
	call	free@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10SerializerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$552, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20422:
	.size	_ZN2v88internal17StartupSerializerD0Ev, .-_ZN2v88internal17StartupSerializerD0Ev
	.section	.text._ZN2v88internal17StartupSerializerC2EPNS0_7IsolateEPNS0_18ReadOnlySerializerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17StartupSerializerC2EPNS0_7IsolateEPNS0_18ReadOnlySerializerE
	.type	_ZN2v88internal17StartupSerializerC2EPNS0_7IsolateEPNS0_18ReadOnlySerializerE, @function
_ZN2v88internal17StartupSerializerC2EPNS0_7IsolateEPNS0_18ReadOnlySerializerE:
.LFB20417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	movl	$539, %edx
	call	_ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE@PLT
	leaq	16+_ZTVN2v88internal17StartupSerializerE(%rip), %rax
	movq	%rbx, 496(%r12)
	movq	%r12, %rdi
	movq	%rax, (%r12)
	popq	%rbx
	movq	$0, 504(%r12)
	movq	$0, 512(%r12)
	movq	$0, 520(%r12)
	movq	$0, 528(%r12)
	movq	$0, 536(%r12)
	movq	$0, 544(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10Serializer24InitializeCodeAddressMapEv@PLT
	.cfi_endproc
.LFE20417:
	.size	_ZN2v88internal17StartupSerializerC2EPNS0_7IsolateEPNS0_18ReadOnlySerializerE, .-_ZN2v88internal17StartupSerializerC2EPNS0_7IsolateEPNS0_18ReadOnlySerializerE
	.globl	_ZN2v88internal17StartupSerializerC1EPNS0_7IsolateEPNS0_18ReadOnlySerializerE
	.set	_ZN2v88internal17StartupSerializerC1EPNS0_7IsolateEPNS0_18ReadOnlySerializerE,_ZN2v88internal17StartupSerializerC2EPNS0_7IsolateEPNS0_18ReadOnlySerializerE
	.section	.text._ZN2v88internal17StartupSerializer34SerializeWeakReferencesAndDeferredEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17StartupSerializer34SerializeWeakReferencesAndDeferredEv
	.type	_ZN2v88internal17StartupSerializer34SerializeWeakReferencesAndDeferredEv, @function
_ZN2v88internal17StartupSerializer34SerializeWeakReferencesAndDeferredEv:
.LFB20424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rcx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	104(%rdi), %rax
	movq	88(%rax), %rax
	movq	%rax, -32(%rbp)
	movq	(%rdi), %rax
	movq	24(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L126
	leaq	-24(%rbp), %r8
	xorl	%edx, %edx
	movl	$19, %esi
	call	*16(%rax)
.L127:
	movq	104(%r12), %rax
	movl	$6, %edx
	movq	%r12, %rsi
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap16IterateWeakRootsEPNS0_11RootVisitorENS0_9VisitModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer24SerializeDeferredObjectsEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer3PadEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$19, %esi
	call	*%r8
	jmp	.L127
.L130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20424:
	.size	_ZN2v88internal17StartupSerializer34SerializeWeakReferencesAndDeferredEv, .-_ZN2v88internal17StartupSerializer34SerializeWeakReferencesAndDeferredEv
	.section	.rodata._ZN2v88internal17StartupSerializer25SerializeStrongReferencesEv.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"(isolate->thread_manager()->FirstThreadStateInUse()) == nullptr"
	.section	.rodata._ZN2v88internal17StartupSerializer25SerializeStrongReferencesEv.str1.1,"aMS",@progbits,1
.LC7:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal17StartupSerializer25SerializeStrongReferencesEv.str1.8
	.align 8
.LC8:
	.string	"isolate->handle_scope_implementer()->blocks()->empty()"
	.section	.text._ZN2v88internal17StartupSerializer25SerializeStrongReferencesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17StartupSerializer25SerializeStrongReferencesEv
	.type	_ZN2v88internal17StartupSerializer25SerializeStrongReferencesEv, @function
_ZN2v88internal17StartupSerializer25SerializeStrongReferencesEv:
.LFB20425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	104(%rdi), %r12
	movq	41168(%r12), %rdi
	call	_ZN2v88internal13ThreadManager21FirstThreadStateInUseEv@PLT
	testq	%rax, %rax
	jne	.L135
	movq	41120(%r12), %rax
	cmpq	$0, 24(%rax)
	jne	.L136
	addq	$37592, %r12
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap15IterateSmiRootsEPNS0_11RootVisitorE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$6, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4Heap18IterateStrongRootsEPNS0_11RootVisitorENS0_9VisitModeE@PLT
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L136:
	leaq	.LC8(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20425:
	.size	_ZN2v88internal17StartupSerializer25SerializeStrongReferencesEv, .-_ZN2v88internal17StartupSerializer25SerializeStrongReferencesEv
	.section	.text._ZN2v88internal17StartupSerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17StartupSerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE
	.type	_ZN2v88internal17StartupSerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE, @function
_ZN2v88internal17StartupSerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE:
.LFB20450:
	.cfi_startproc
	endbr64
	movq	496(%rdi), %rdi
	jmp	_ZN2v88internal18ReadOnlySerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE@PLT
	.cfi_endproc
.LFE20450:
	.size	_ZN2v88internal17StartupSerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE, .-_ZN2v88internal17StartupSerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE
	.section	.text._ZN2v88internal23SerializedHandleChecker8AddToSetENS0_10FixedArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23SerializedHandleChecker8AddToSetENS0_10FixedArrayE
	.type	_ZN2v88internal23SerializedHandleChecker8AddToSetENS0_10FixedArrayE, @function
_ZN2v88internal23SerializedHandleChecker8AddToSetENS0_10FixedArrayE:
.LFB20452:
	.cfi_startproc
	endbr64
	movslq	11(%rsi), %rax
	testq	%rax, %rax
	jle	.L180
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$1, %eax
	leaq	23(%rsi,%rax,8), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	15(%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r15, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rax, -56(%rbp)
	leaq	48(%rdi), %rax
	movq	%rax, -72(%rbp)
	leaq	32(%rdi), %rax
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L158:
	movq	(%r14), %r12
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r12, %rax
	divq	%rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r13
	testq	%rax, %rax
	je	.L140
	movq	(%rax), %rcx
	movq	16(%rcx), %rsi
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L141:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L140
	movq	16(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L140
.L143:
	cmpq	%rsi, %r12
	jne	.L141
	cmpq	8(%rcx), %r12
	jne	.L141
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	jne	.L158
.L183:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	40(%rbx), %rdx
	movq	24(%rbx), %rsi
	movl	$1, %ecx
	movq	$0, (%rax)
	movq	-72(%rbp), %rdi
	movq	%r12, 8(%rax)
	movq	%rax, -64(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	movq	%rdx, %r15
	jne	.L144
	movq	16(%rbx), %r10
	movq	%r12, 16(%r9)
	leaq	(%r10,%r13), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L154
.L186:
	movq	(%rdx), %rdx
	movq	%rdx, (%r9)
	movq	(%rax), %rax
	movq	%r9, (%rax)
.L155:
	addq	$1, 40(%rbx)
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	jne	.L158
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L144:
	cmpq	$1, %rdx
	je	.L184
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L185
	leaq	0(,%rdx,8), %r13
	movq	%r9, -64(%rbp)
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-64(%rbp), %r9
	leaq	64(%rbx), %r8
	movq	%rax, %r10
.L147:
	movq	32(%rbx), %rsi
	movq	$0, 32(%rbx)
	testq	%rsi, %rsi
	je	.L149
	movq	-80(%rbp), %rdi
	xorl	%r11d, %r11d
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L151:
	movq	0(%r13), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L152:
	testq	%rsi, %rsi
	je	.L149
.L150:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	16(%rcx), %rax
	divq	%r15
	leaq	(%r10,%rdx,8), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	jne	.L151
	movq	32(%rbx), %r13
	movq	%r13, (%rcx)
	movq	%rcx, 32(%rbx)
	movq	%rdi, (%rax)
	cmpq	$0, (%rcx)
	je	.L159
	movq	%rcx, (%r10,%r11,8)
	movq	%rdx, %r11
	testq	%rsi, %rsi
	jne	.L150
	.p2align 4,,10
	.p2align 3
.L149:
	movq	16(%rbx), %rdi
	cmpq	%r8, %rdi
	je	.L153
	movq	%r10, -88(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r10
	movq	-64(%rbp), %r9
.L153:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r15, 24(%rbx)
	divq	%r15
	movq	%r10, 16(%rbx)
	movq	%r12, 16(%r9)
	leaq	0(,%rdx,8), %r13
	leaq	(%r10,%r13), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L186
.L154:
	movq	32(%rbx), %rdx
	movq	%r9, 32(%rbx)
	movq	%rdx, (%r9)
	testq	%rdx, %rdx
	je	.L156
	movq	16(%rdx), %rax
	xorl	%edx, %edx
	divq	24(%rbx)
	movq	%r9, (%r10,%rdx,8)
	movq	16(%rbx), %rax
	addq	%r13, %rax
.L156:
	movq	-80(%rbp), %rdi
	movq	%rdi, (%rax)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%rdx, %r11
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L184:
	leaq	64(%rbx), %r10
	movq	$0, 64(%rbx)
	movq	%r10, %r8
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L185:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE20452:
	.size	_ZN2v88internal23SerializedHandleChecker8AddToSetENS0_10FixedArrayE, .-_ZN2v88internal23SerializedHandleChecker8AddToSetENS0_10FixedArrayE
	.section	.text._ZN2v88internal23SerializedHandleCheckerC2EPNS0_7IsolateEPSt6vectorINS0_7ContextESaIS5_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23SerializedHandleCheckerC2EPNS0_7IsolateEPSt6vectorINS0_7ContextESaIS5_EE
	.type	_ZN2v88internal23SerializedHandleCheckerC2EPNS0_7IsolateEPSt6vectorINS0_7ContextESaIS5_EE, @function
_ZN2v88internal23SerializedHandleCheckerC2EPNS0_7IsolateEPSt6vectorINS0_7ContextESaIS5_EE:
.LFB20448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23SerializedHandleCheckerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	leaq	64(%rdi), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, 16(%rdi)
	movq	$1, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movl	$0x3f800000, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	4728(%rsi), %rsi
	movb	$1, 72(%rdi)
	call	_ZN2v88internal23SerializedHandleChecker8AddToSetENS0_10FixedArrayE
	movq	0(%r13), %rbx
	movq	8(%r13), %r13
	cmpq	%r13, %rbx
	je	.L187
	.p2align 4,,10
	.p2align 3
.L189:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	addq	$8, %rbx
	movq	1159(%rax), %rsi
	call	_ZN2v88internal23SerializedHandleChecker8AddToSetENS0_10FixedArrayE
	cmpq	%rbx, %r13
	jne	.L189
.L187:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20448:
	.size	_ZN2v88internal23SerializedHandleCheckerC2EPNS0_7IsolateEPSt6vectorINS0_7ContextESaIS5_EE, .-_ZN2v88internal23SerializedHandleCheckerC2EPNS0_7IsolateEPSt6vectorINS0_7ContextESaIS5_EE
	.globl	_ZN2v88internal23SerializedHandleCheckerC1EPNS0_7IsolateEPSt6vectorINS0_7ContextESaIS5_EE
	.set	_ZN2v88internal23SerializedHandleCheckerC1EPNS0_7IsolateEPSt6vectorINS0_7ContextESaIS5_EE,_ZN2v88internal23SerializedHandleCheckerC2EPNS0_7IsolateEPSt6vectorINS0_7ContextESaIS5_EE
	.section	.text._ZN2v88internal23SerializedHandleChecker28CheckGlobalAndEternalHandlesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23SerializedHandleChecker28CheckGlobalAndEternalHandlesEv
	.type	_ZN2v88internal23SerializedHandleChecker28CheckGlobalAndEternalHandlesEv, @function
_ZN2v88internal23SerializedHandleChecker28CheckGlobalAndEternalHandlesEv:
.LFB20454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rbx, %rsi
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles15IterateAllRootsEPNS0_11RootVisitorE@PLT
	movq	8(%rbx), %rax
	movq	%rbx, %rsi
	movq	41160(%rax), %rdi
	call	_ZN2v88internal14EternalHandles15IterateAllRootsEPNS0_11RootVisitorE@PLT
	movzbl	72(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20454:
	.size	_ZN2v88internal23SerializedHandleChecker28CheckGlobalAndEternalHandlesEv, .-_ZN2v88internal23SerializedHandleChecker28CheckGlobalAndEternalHandlesEv
	.section	.rodata._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC10:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.type	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, @function
_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_:
.LFB23059:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L208
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L203
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L209
.L205:
	movq	%rcx, %r14
.L196:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L202:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L210
	testq	%rsi, %rsi
	jg	.L198
	testq	%r15, %r15
	jne	.L201
.L199:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L198
.L201:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L209:
	testq	%r14, %r14
	js	.L205
	jne	.L196
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L199
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L203:
	movl	$1, %r14d
	jmp	.L196
.L208:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23059:
	.size	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, .-_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.section	.rodata._ZN2v88internal17StartupSerializer34SerializeUsingPartialSnapshotCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"partial_snapshot_cache_index"
	.section	.text._ZN2v88internal17StartupSerializer34SerializeUsingPartialSnapshotCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17StartupSerializer34SerializeUsingPartialSnapshotCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE
	.type	_ZN2v88internal17StartupSerializer34SerializeUsingPartialSnapshotCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE, @function
_ZN2v88internal17StartupSerializer34SerializeUsingPartialSnapshotCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE:
.LFB20451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal15RootsSerializer22SerializeInObjectCacheENS0_10HeapObjectE@PLT
	movb	$16, -25(%rbp)
	movq	8(%r12), %rsi
	movl	%eax, %ebx
	cmpq	16(%r12), %rsi
	je	.L212
	movb	$16, (%rsi)
	addq	$1, 8(%r12)
.L213:
	movslq	%ebx, %rsi
	leaq	.LC11(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	leaq	-25(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L213
.L216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20451:
	.size	_ZN2v88internal17StartupSerializer34SerializeUsingPartialSnapshotCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE, .-_ZN2v88internal17StartupSerializer34SerializeUsingPartialSnapshotCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal17StartupSerializerC2EPNS0_7IsolateEPNS0_18ReadOnlySerializerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal17StartupSerializerC2EPNS0_7IsolateEPNS0_18ReadOnlySerializerE, @function
_GLOBAL__sub_I__ZN2v88internal17StartupSerializerC2EPNS0_7IsolateEPNS0_18ReadOnlySerializerE:
.LFB26267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26267:
	.size	_GLOBAL__sub_I__ZN2v88internal17StartupSerializerC2EPNS0_7IsolateEPNS0_18ReadOnlySerializerE, .-_GLOBAL__sub_I__ZN2v88internal17StartupSerializerC2EPNS0_7IsolateEPNS0_18ReadOnlySerializerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal17StartupSerializerC2EPNS0_7IsolateEPNS0_18ReadOnlySerializerE
	.weak	_ZTVN2v88internal17StartupSerializerE
	.section	.data.rel.ro._ZTVN2v88internal17StartupSerializerE,"awG",@progbits,_ZTVN2v88internal17StartupSerializerE,comdat
	.align 8
	.type	_ZTVN2v88internal17StartupSerializerE, @object
	.size	_ZTVN2v88internal17StartupSerializerE, 72
_ZTVN2v88internal17StartupSerializerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal17StartupSerializerD1Ev
	.quad	_ZN2v88internal17StartupSerializerD0Ev
	.quad	_ZN2v88internal15RootsSerializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal15RootsSerializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.quad	_ZN2v88internal17StartupSerializer15SerializeObjectENS0_10HeapObjectE
	.quad	_ZN2v88internal10Serializer14MustBeDeferredENS0_10HeapObjectE
	.weak	_ZTVN2v88internal23SerializedHandleCheckerE
	.section	.data.rel.ro.local._ZTVN2v88internal23SerializedHandleCheckerE,"awG",@progbits,_ZTVN2v88internal23SerializedHandleCheckerE,comdat
	.align 8
	.type	_ZTVN2v88internal23SerializedHandleCheckerE, @object
	.size	_ZTVN2v88internal23SerializedHandleCheckerE, 56
_ZTVN2v88internal23SerializedHandleCheckerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23SerializedHandleCheckerD1Ev
	.quad	_ZN2v88internal23SerializedHandleCheckerD0Ev
	.quad	_ZN2v88internal23SerializedHandleChecker17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
