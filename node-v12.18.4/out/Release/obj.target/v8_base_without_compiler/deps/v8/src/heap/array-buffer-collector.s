	.file	"array-buffer-collector.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4551:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4551:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4552:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4552:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4554:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4554:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal20ArrayBufferCollector15FreeAllocationsEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal20ArrayBufferCollector15FreeAllocationsEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal20ArrayBufferCollector15FreeAllocationsEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation:
.LFB23489:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L6
	cmpl	$3, %edx
	je	.L7
	cmpl	$1, %edx
	je	.L11
.L7:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23489:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal20ArrayBufferCollector15FreeAllocationsEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal20ArrayBufferCollector15FreeAllocationsEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8841:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L18
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L21
.L12:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L12
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8841:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal20ArrayBufferCollector22PerformFreeAllocationsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ArrayBufferCollector22PerformFreeAllocationsEv
	.type	_ZN2v88internal20ArrayBufferCollector22PerformFreeAllocationsEv, @function
_ZN2v88internal20ArrayBufferCollector22PerformFreeAllocationsEv:
.LFB20093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rax, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rax, -96(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	56(%r13), %rax
	movq	48(%r13), %r12
	movq	%rax, -88(%rbp)
	cmpq	%rax, %r12
	je	.L23
	.p2align 4,,10
	.p2align 3
.L25:
	movq	0(%r13), %rax
	movq	8(%r12), %r15
	movq	(%r12), %rbx
	leaq	-37592(%rax), %r14
	cmpq	%r15, %rbx
	je	.L28
	.p2align 4,,10
	.p2align 3
.L27:
	movq	24(%rbx), %rdi
	movq	(%rbx), %rax
	addq	$32, %rbx
	movq	-24(%rbx), %rdx
	movq	-16(%rbx), %rsi
	pushq	%rdi
	pushq	%rsi
	pushq	%rdx
	pushq	%rax
	movq	%rdi, -56(%rbp)
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal13JSArrayBuffer16FreeBackingStoreEPNS0_7IsolateENS1_10AllocationE@PLT
	addq	$32, %rsp
	cmpq	%rbx, %r15
	jne	.L27
.L28:
	addq	$24, %r12
	cmpq	%r12, -88(%rbp)
	jne	.L25
	movq	48(%r13), %r12
	movq	56(%r13), %r14
	cmpq	%r14, %r12
	je	.L23
	movq	%r12, %rbx
	.p2align 4,,10
	.p2align 3
.L32:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L29
	call	_ZdlPv@PLT
	addq	$24, %rbx
	cmpq	%r14, %rbx
	jne	.L32
.L30:
	movq	%r12, 56(%r13)
.L23:
	movq	-96(%rbp), %rdi
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	addq	$24, %rbx
	cmpq	%r14, %rbx
	jne	.L32
	jmp	.L30
	.cfi_endproc
.LFE20093:
	.size	_ZN2v88internal20ArrayBufferCollector22PerformFreeAllocationsEv, .-_ZN2v88internal20ArrayBufferCollector22PerformFreeAllocationsEv
	.section	.rodata._ZNSt17_Function_handlerIFvvEZN2v88internal20ArrayBufferCollector15FreeAllocationsEvEUlvE_E9_M_invokeERKSt9_Any_data.str1.1,"aMS",@progbits,1
.LC0:
	.string	"disabled-by-default-v8.gc"
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v88internal20ArrayBufferCollector15FreeAllocationsEvEUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvvEZN2v88internal20ArrayBufferCollector15FreeAllocationsEvEUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v88internal20ArrayBufferCollector15FreeAllocationsEvEUlvE_E9_M_invokeERKSt9_Any_data:
.LFB23488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-184(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	2008(%rax), %rdi
	call	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	(%rbx), %rax
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	-184(%rbp), %rcx
	movq	(%rax), %rax
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer15BackgroundScopeC1EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE@PLT
	movq	_ZZZN2v88internal20ArrayBufferCollector15FreeAllocationsEvENKUlvE_clEvE27trace_event_unique_atomic54(%rip), %rdx
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L59
.L40:
	movq	$0, -176(%rbp)
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L60
.L42:
	movq	(%rbx), %rdi
	call	_ZN2v88internal20ArrayBufferCollector22PerformFreeAllocationsEv
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8GCTracer15BackgroundScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L62
.L41:
	movq	%r14, _ZZZN2v88internal20ArrayBufferCollector15FreeAllocationsEvENKUlvE_clEvE27trace_event_unique_atomic54(%rip)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L60:
	xorl	%edi, %edi
	xorl	%r15d, %r15d
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -200(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L63
.L43:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L44
	movq	(%rdi), %rax
	call	*8(%rax)
.L44:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L45
	movq	(%rdi), %rax
	call	*8(%rax)
.L45:
	xorl	%edi, %edi
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	movq	%r14, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%r15, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L63:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r14, %rdx
	movl	$88, %esi
	pushq	%rcx
	movq	-200(%rbp), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L43
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23488:
	.size	_ZNSt17_Function_handlerIFvvEZN2v88internal20ArrayBufferCollector15FreeAllocationsEvEUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v88internal20ArrayBufferCollector15FreeAllocationsEvEUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZN2v88internal20ArrayBufferCollector15FreeAllocationsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ArrayBufferCollector15FreeAllocationsEv
	.type	_ZN2v88internal20ArrayBufferCollector15FreeAllocationsEv, @function
_ZN2v88internal20ArrayBufferCollector15FreeAllocationsEv:
.LFB20094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	56(%rax), %rdx
	subq	%rdx, -37560(%rax)
	movq	$0, 56(%rax)
	mfence
	movq	(%rdi), %rax
	cmpl	$4, 392(%rax)
	je	.L65
	testb	$1, 2756(%rax)
	je	.L84
.L65:
	call	_ZN2v88internal20ArrayBufferCollector22PerformFreeAllocationsEv
.L64:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	cmpb	$0, _ZN2v88internal36FLAG_concurrent_array_buffer_freeingE(%rip)
	je	.L65
	movq	%rdi, -104(%rbp)
	leaq	-80(%rbp), %r13
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	-104(%rbp), %rdi
	leaq	-96(%rbp), %r8
	movq	%r13, %rdx
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal20ArrayBufferCollector15FreeAllocationsEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation(%rip), %rcx
	movq	(%rdi), %rsi
	movq	%rcx, %xmm0
	movq	%rdi, -80(%rbp)
	movq	%r8, %rdi
	movq	56(%rax), %rbx
	leaq	_ZNSt17_Function_handlerIFvvEZN2v88internal20ArrayBufferCollector15FreeAllocationsEvEUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rax, %xmm1
	subq	$37592, %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal18MakeCancelableTaskEPNS0_7IsolateESt8functionIFvvEE@PLT
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-88(%rbp), %rsi
	movq	$0, -96(%rbp)
	leaq	32(%rdx), %rax
	testq	%rdx, %rdx
	cmovne	%rax, %rdx
	movq	%rdx, -88(%rbp)
	call	*%rbx
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	movq	(%rdi), %rax
	call	*8(%rax)
.L69:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	movq	(%rdi), %rax
	call	*8(%rax)
.L70:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L64
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L64
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20094:
	.size	_ZN2v88internal20ArrayBufferCollector15FreeAllocationsEv, .-_ZN2v88internal20ArrayBufferCollector15FreeAllocationsEv
	.section	.rodata._ZNSt6vectorIS_IN2v88internal13JSArrayBuffer10AllocationESaIS3_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIS_IN2v88internal13JSArrayBuffer10AllocationESaIS3_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIS_IN2v88internal13JSArrayBuffer10AllocationESaIS3_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIS_IN2v88internal13JSArrayBuffer10AllocationESaIS3_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorIS_IN2v88internal13JSArrayBuffer10AllocationESaIS3_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorIS_IN2v88internal13JSArrayBuffer10AllocationESaIS3_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB24309:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movabsq	$384307168202282325, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%r8, %rax
	je	.L118
	movq	%r12, %rcx
	movq	%rdi, %r14
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L101
	movabsq	$9223372036854775800, %rdi
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L119
.L88:
	movq	%rdx, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	leaq	(%rax,%rdi), %rax
	leaq	24(%r13), %r8
.L100:
	movdqu	(%rdx), %xmm7
	movq	16(%rdx), %rdi
	addq	%r13, %rcx
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rdx)
	movq	%rdi, 16(%rcx)
	movups	%xmm7, (%rcx)
	movups	%xmm0, (%rdx)
	cmpq	%r15, %r12
	je	.L90
	leaq	-24(%r12), %rdi
	movq	%r15, %rdx
	movabsq	$768614336404564651, %rcx
	subq	%r15, %rdi
	shrq	$3, %rdi
	imulq	%rcx, %rdi
	leaq	47(%r13), %rcx
	subq	%r15, %rcx
	cmpq	$94, %rcx
	jbe	.L103
	movabsq	$2305843009213693950, %rcx
	testq	%rcx, %rdi
	je	.L103
	movabsq	$2305843009213693951, %r8
	movq	%r13, %rcx
	andq	%rdi, %r8
	addq	$1, %r8
	movq	%r8, %r9
	shrq	%r9
	leaq	(%r9,%r9,2), %r9
	salq	$4, %r9
	addq	%r15, %r9
	.p2align 4,,10
	.p2align 3
.L92:
	movdqu	16(%rdx), %xmm1
	movdqu	(%rdx), %xmm7
	addq	$48, %rdx
	addq	$48, %rcx
	movdqu	-16(%rdx), %xmm0
	movups	%xmm7, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	%xmm0, -16(%rcx)
	cmpq	%r9, %rdx
	jne	.L92
	movq	%r8, %r9
	andq	$-2, %r9
	leaq	(%r9,%r9,2), %rdx
	salq	$3, %rdx
	leaq	(%r15,%rdx), %rcx
	addq	%r13, %rdx
	cmpq	%r9, %r8
	je	.L94
	movdqu	(%rcx), %xmm6
	movq	16(%rcx), %rcx
	movq	%rcx, 16(%rdx)
	movups	%xmm6, (%rdx)
.L94:
	leaq	(%rdi,%rdi,2), %rdx
	leaq	48(%r13,%rdx,8), %r8
.L90:
	cmpq	%rsi, %r12
	je	.L95
	subq	%r12, %rsi
	movq	%r12, %rdx
	leaq	-24(%rsi), %rcx
	movabsq	$768614336404564651, %rsi
	shrq	$3, %rcx
	imulq	%rsi, %rcx
	movabsq	$2305843009213693951, %rsi
	andq	%rsi, %rcx
	leaq	1(%rcx), %rdi
	je	.L104
	movq	%rdi, %rsi
	movq	%r8, %rcx
	shrq	%rsi
	leaq	(%rsi,%rsi,2), %rsi
	salq	$4, %rsi
	addq	%r12, %rsi
	.p2align 4,,10
	.p2align 3
.L97:
	movdqu	(%rdx), %xmm3
	movdqu	16(%rdx), %xmm4
	addq	$48, %rdx
	addq	$48, %rcx
	movdqu	-16(%rdx), %xmm5
	movups	%xmm3, -48(%rcx)
	movups	%xmm4, -32(%rcx)
	movups	%xmm5, -16(%rcx)
	cmpq	%rsi, %rdx
	jne	.L97
	movq	%rdi, %rcx
	andq	$-2, %rcx
	leaq	(%rcx,%rcx,2), %rbx
	salq	$3, %rbx
	leaq	(%r8,%rbx), %rdx
	addq	%r12, %rbx
	cmpq	%rcx, %rdi
	je	.L98
.L96:
	movq	16(%rbx), %rcx
	movdqu	(%rbx), %xmm2
	movq	%rcx, 16(%rdx)
	movups	%xmm2, (%rdx)
.L98:
	leaq	(%rdi,%rdi,2), %rdx
	leaq	(%r8,%rdx,8), %r8
.L95:
	testq	%r15, %r15
	je	.L99
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L99:
	movq	%r13, %xmm0
	movq	%r8, %xmm6
	movq	%rax, 16(%r14)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, (%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L89
	movl	$24, %r8d
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$24, %edi
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r13, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L91:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %r8
	movq	%r8, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L91
	jmp	.L94
.L104:
	movq	%r8, %rdx
	jmp	.L96
.L89:
	cmpq	%r8, %r9
	movq	%r8, %rax
	cmovbe	%r9, %rax
	imulq	$24, %rax, %rdi
	jmp	.L88
.L118:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24309:
	.size	_ZNSt6vectorIS_IN2v88internal13JSArrayBuffer10AllocationESaIS3_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorIS_IN2v88internal13JSArrayBuffer10AllocationESaIS3_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE
	.type	_ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE, @function
_ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE:
.LFB20091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	testb	$1, 2756(%rax)
	je	.L121
	movq	8(%rsi), %r14
	movq	(%rsi), %rbx
	leaq	-37592(%rax), %r13
	cmpq	%r14, %rbx
	je	.L120
	.p2align 4,,10
	.p2align 3
.L124:
	movq	24(%rbx), %rdi
	movq	(%rbx), %rax
	addq	$32, %rbx
	movq	-24(%rbx), %rdx
	movq	-16(%rbx), %rsi
	pushq	%rdi
	pushq	%rsi
	pushq	%rdx
	pushq	%rax
	movq	%rdi, -40(%rbp)
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal13JSArrayBuffer16FreeBackingStoreEPNS0_7IsolateENS1_10AllocationE@PLT
	addq	$32, %rsp
	cmpq	%rbx, %r14
	jne	.L124
.L120:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	leaq	8(%rdi), %r13
	movq	%rdi, %rbx
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	56(%rbx), %rsi
	cmpq	64(%rbx), %rsi
	je	.L126
	movdqu	(%r12), %xmm1
	movq	16(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r12)
	movups	%xmm0, (%r12)
	movq	%rax, 16(%rsi)
	movups	%xmm1, (%rsi)
	addq	$24, 56(%rbx)
.L127:
	leaq	-32(%rbp), %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	leaq	48(%rbx), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIS_IN2v88internal13JSArrayBuffer10AllocationESaIS3_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L127
	.cfi_endproc
.LFE20091:
	.size	_ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE, .-_ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE, @function
_GLOBAL__sub_I__ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE:
.LFB25303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25303:
	.size	_GLOBAL__sub_I__ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE, .-_GLOBAL__sub_I__ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE
	.section	.bss._ZZZN2v88internal20ArrayBufferCollector15FreeAllocationsEvENKUlvE_clEvE27trace_event_unique_atomic54,"aw",@nobits
	.align 8
	.type	_ZZZN2v88internal20ArrayBufferCollector15FreeAllocationsEvENKUlvE_clEvE27trace_event_unique_atomic54, @object
	.size	_ZZZN2v88internal20ArrayBufferCollector15FreeAllocationsEvENKUlvE_clEvE27trace_event_unique_atomic54, 8
_ZZZN2v88internal20ArrayBufferCollector15FreeAllocationsEvENKUlvE_clEvE27trace_event_unique_atomic54:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
