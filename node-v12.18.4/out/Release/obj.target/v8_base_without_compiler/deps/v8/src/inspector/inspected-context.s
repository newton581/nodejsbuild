	.file	"inspected-context.cc"
	.text
	.section	.text._ZN12v8_inspector16InspectedContext16WeakCallbackData20callContextCollectedERKN2v816WeakCallbackInfoIS1_EE,"axG",@progbits,_ZN12v8_inspector16InspectedContext16WeakCallbackData20callContextCollectedERKN2v816WeakCallbackInfoIS1_EE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector16InspectedContext16WeakCallbackData20callContextCollectedERKN2v816WeakCallbackInfoIS1_EE
	.type	_ZN12v8_inspector16InspectedContext16WeakCallbackData20callContextCollectedERKN2v816WeakCallbackInfoIS1_EE, @function
_ZN12v8_inspector16InspectedContext16WeakCallbackData20callContextCollectedERKN2v816WeakCallbackInfoIS1_EE:
.LFB7237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movl	16(%r12), %esi
	movq	8(%r12), %rdi
	movl	20(%r12), %edx
	call	_ZN12v8_inspector15V8InspectorImpl16contextCollectedEii@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7237:
	.size	_ZN12v8_inspector16InspectedContext16WeakCallbackData20callContextCollectedERKN2v816WeakCallbackInfoIS1_EE, .-_ZN12v8_inspector16InspectedContext16WeakCallbackData20callContextCollectedERKN2v816WeakCallbackInfoIS1_EE
	.section	.text._ZN12v8_inspector16InspectedContext16WeakCallbackData12resetContextERKN2v816WeakCallbackInfoIS1_EE,"axG",@progbits,_ZN12v8_inspector16InspectedContext16WeakCallbackData12resetContextERKN2v816WeakCallbackInfoIS1_EE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector16InspectedContext16WeakCallbackData12resetContextERKN2v816WeakCallbackInfoIS1_EE
	.type	_ZN12v8_inspector16InspectedContext16WeakCallbackData12resetContextERKN2v816WeakCallbackInfoIS1_EE, @function
_ZN12v8_inspector16InspectedContext16WeakCallbackData12resetContextERKN2v816WeakCallbackInfoIS1_EE:
.LFB7236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rax
	movq	%rdi, %rbx
	movq	(%rax), %rax
	movq	$0, 256(%rax)
	movq	8(%rdi), %rax
	movq	(%rax), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L5:
	movq	16(%rbx), %rax
	leaq	_ZN12v8_inspector16InspectedContext16WeakCallbackData20callContextCollectedERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	movq	%rdx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7236:
	.size	_ZN12v8_inspector16InspectedContext16WeakCallbackData12resetContextERKN2v816WeakCallbackInfoIS1_EE, .-_ZN12v8_inspector16InspectedContext16WeakCallbackData12resetContextERKN2v816WeakCallbackInfoIS1_EE
	.section	.rodata._ZN12v8_inspector16InspectedContextC2EPNS_15V8InspectorImplERKNS_13V8ContextInfoEi.str1.1,"aMS",@progbits,1
.LC1:
	.string	"console"
	.section	.text._ZN12v8_inspector16InspectedContextC2EPNS_15V8InspectorImplERKNS_13V8ContextInfoEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16InspectedContextC2EPNS_15V8InspectorImplERKNS_13V8ContextInfoEi
	.type	_ZN12v8_inspector16InspectedContextC2EPNS_15V8InspectorImplERKNS_13V8ContextInfoEi, @function
_ZN12v8_inspector16InspectedContextC2EPNS_15V8InspectorImplERKNS_13V8ContextInfoEi:
.LFB7281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, (%rdi)
	movq	(%rdx), %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L11
	movq	%rax, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, %rsi
.L11:
	movq	%rsi, 8(%rbx)
	movl	8(%r12), %eax
	leaq	24(%rbx), %rdi
	leaq	40(%r12), %rsi
	movl	%r13d, 16(%rbx)
	movl	%eax, 20(%rbx)
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	leaq	64(%rbx), %rdi
	leaq	16(%r12), %rsi
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	leaq	104(%rbx), %rdi
	leaq	64(%r12), %rsi
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	leaq	192(%rbx), %rax
	movss	.LC0(%rip), %xmm0
	movl	%r13d, %esi
	movq	%rax, 144(%rbx)
	leaq	248(%rbx), %rax
	movq	%rax, 200(%rbx)
	movq	$1, 152(%rbx)
	movq	$0, 160(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 184(%rbx)
	movq	$0, 192(%rbx)
	movq	$1, 208(%rbx)
	movq	$0, 216(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 240(%rbx)
	movq	$0, 248(%rbx)
	movq	$0, 264(%rbx)
	movss	%xmm0, 176(%rbx)
	movss	%xmm0, 232(%rbx)
	movq	(%r12), %rdi
	call	_ZN2v85debug12SetContextIdENS_5LocalINS_7ContextEEEi@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	movl	20(%rbx), %edx
	movq	(%rbx), %rcx
	movq	%rax, %rsi
	movl	16(%rbx), %eax
	movq	8(%rbx), %rdi
	movq	%rcx, 8(%rsi)
	xorl	%ecx, %ecx
	movl	%edx, 16(%rsi)
	leaq	_ZN12v8_inspector16InspectedContext16WeakCallbackData12resetContextERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	movq	%rbx, (%rsi)
	movl	%eax, 20(%rsi)
	movq	%rsi, 256(%rbx)
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	cmpb	$0, 88(%r12)
	jne	.L32
.L10:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	(%r12), %r14
	leaq	-128(%rbp), %r15
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	(%r12), %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	(%r12), %rdi
	call	_ZN2v87Context6GlobalEv@PLT
	leaq	-96(%rbp), %r8
	leaq	.LC1(%rip), %rsi
	movq	%r8, %rdi
	movq	%rax, %r13
	movq	%r8, -136(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	(%rbx), %rax
	movq	-136(%rbp), %r8
	movq	8(%rax), %rdi
	movq	%r8, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L14
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L14
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	(%rbx), %rdi
	call	_ZN12v8_inspector15V8InspectorImpl7consoleEv@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	_ZN12v8_inspector9V8Console19installMemoryGetterEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE@PLT
.L16:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L14:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L16
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	jmp	.L10
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7281:
	.size	_ZN12v8_inspector16InspectedContextC2EPNS_15V8InspectorImplERKNS_13V8ContextInfoEi, .-_ZN12v8_inspector16InspectedContextC2EPNS_15V8InspectorImplERKNS_13V8ContextInfoEi
	.globl	_ZN12v8_inspector16InspectedContextC1EPNS_15V8InspectorImplERKNS_13V8ContextInfoEi
	.set	_ZN12v8_inspector16InspectedContextC1EPNS_15V8InspectorImplERKNS_13V8ContextInfoEi,_ZN12v8_inspector16InspectedContextC2EPNS_15V8InspectorImplERKNS_13V8ContextInfoEi
	.section	.text._ZN12v8_inspector16InspectedContextD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16InspectedContextD2Ev
	.type	_ZN12v8_inspector16InspectedContextD2Ev, @function
_ZN12v8_inspector16InspectedContextD2Ev:
.LFB7284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$0, 8(%rdi)
	movq	%rdi, %rbx
	je	.L35
	movq	256(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L35
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L35:
	movq	264(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L36:
	movq	216(%rbx), %r12
	testq	%r12, %r12
	jne	.L40
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%r14, %rdi
	call	_ZN12v8_inspector14InjectedScriptD1Ev@PLT
	movq	%r14, %rdi
	movl	$272, %esi
	call	_ZdlPvm@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L37
.L39:
	movq	%r13, %r12
.L40:
	movq	16(%r12), %r14
	movq	(%r12), %r13
	testq	%r14, %r14
	jne	.L69
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L39
.L37:
	movq	208(%rbx), %rax
	movq	200(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	200(%rbx), %rdi
	leaq	248(%rbx), %rax
	movq	$0, 224(%rbx)
	movq	$0, 216(%rbx)
	cmpq	%rax, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	160(%rbx), %r12
	testq	%r12, %r12
	je	.L42
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L43
.L42:
	movq	152(%rbx), %rax
	movq	144(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	144(%rbx), %rdi
	leaq	192(%rbx), %rax
	movq	$0, 168(%rbx)
	movq	$0, 160(%rbx)
	cmpq	%rax, %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movq	24(%rbx), %rdi
	leaq	40(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L47
	call	_ZdlPv@PLT
.L47:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L34
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V813DisposeGlobalEPm@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7284:
	.size	_ZN12v8_inspector16InspectedContextD2Ev, .-_ZN12v8_inspector16InspectedContextD2Ev
	.globl	_ZN12v8_inspector16InspectedContextD1Ev
	.set	_ZN12v8_inspector16InspectedContextD1Ev,_ZN12v8_inspector16InspectedContextD2Ev
	.section	.text._ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE
	.type	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE, @function
_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE:
.LFB7286:
	.cfi_startproc
	endbr64
	jmp	_ZN2v85debug12GetContextIdENS_5LocalINS_7ContextEEE@PLT
	.cfi_endproc
.LFE7286:
	.size	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE, .-_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE
	.section	.text._ZNK12v8_inspector16InspectedContext7contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16InspectedContext7contextEv
	.type	_ZNK12v8_inspector16InspectedContext7contextEv, @function
_ZNK12v8_inspector16InspectedContext7contextEv:
.LFB7287:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L77
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdx
	movq	(%rax), %rsi
	movq	8(%rdx), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE7287:
	.size	_ZNK12v8_inspector16InspectedContext7contextEv, .-_ZNK12v8_inspector16InspectedContext7contextEv
	.section	.text._ZNK12v8_inspector16InspectedContext7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16InspectedContext7isolateEv
	.type	_ZNK12v8_inspector16InspectedContext7isolateEv, @function
_ZNK12v8_inspector16InspectedContext7isolateEv:
.LFB7288:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE7288:
	.size	_ZNK12v8_inspector16InspectedContext7isolateEv, .-_ZNK12v8_inspector16InspectedContext7isolateEv
	.section	.text._ZNK12v8_inspector16InspectedContext10isReportedEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16InspectedContext10isReportedEi
	.type	_ZNK12v8_inspector16InspectedContext10isReportedEi, @function
_ZNK12v8_inspector16InspectedContext10isReportedEi:
.LFB7289:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %r8
	movslq	%esi, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	144(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L86
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L88:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L86
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L86
.L83:
	cmpl	%edi, %esi
	jne	.L88
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7289:
	.size	_ZNK12v8_inspector16InspectedContext10isReportedEi, .-_ZNK12v8_inspector16InspectedContext10isReportedEi
	.section	.text._ZN12v8_inspector16InspectedContext17getInjectedScriptEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16InspectedContext17getInjectedScriptEi
	.type	_ZN12v8_inspector16InspectedContext17getInjectedScriptEi, @function
_ZN12v8_inspector16InspectedContext17getInjectedScriptEi:
.LFB7291:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rcx
	movslq	%esi, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	200(%rdi), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	testq	%r8, %r8
	je	.L89
	movq	(%r8), %r8
	movl	8(%r8), %edi
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L101:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L89
	movslq	8(%r8), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L100
.L92:
	cmpl	%esi, %edi
	jne	.L101
	movq	16(%r8), %r8
.L89:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE7291:
	.size	_ZN12v8_inspector16InspectedContext17getInjectedScriptEi, .-_ZN12v8_inspector16InspectedContext17getInjectedScriptEi
	.section	.text._ZN12v8_inspector16InspectedContext21discardInjectedScriptEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16InspectedContext21discardInjectedScriptEi
	.type	_ZN12v8_inspector16InspectedContext21discardInjectedScriptEi, @function
_ZN12v8_inspector16InspectedContext21discardInjectedScriptEi:
.LFB7294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	208(%rdi), %rcx
	movq	200(%rdi), %r13
	divq	%rcx
	leaq	0(,%rdx,8), %r15
	leaq	0(%r13,%r15), %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L102
	movq	(%rdi), %r12
	movq	%rdx, %r11
	movq	%rdi, %r10
	movl	8(%r12), %r9d
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L127:
	movq	(%r12), %r8
	testq	%r8, %r8
	je	.L102
	movslq	8(%r8), %rax
	xorl	%edx, %edx
	movq	%r12, %r10
	movq	%rax, %r9
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L102
	movq	%r8, %r12
.L105:
	cmpl	%r9d, %esi
	jne	.L127
	movq	(%r12), %rsi
	cmpq	%r10, %rdi
	je	.L128
	testq	%rsi, %rsi
	je	.L107
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L107
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r12), %rsi
.L107:
	movq	%rsi, (%r10)
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L109
	movq	%r13, %rdi
	call	_ZN12v8_inspector14InjectedScriptD1Ev@PLT
	movl	$272, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L109:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 224(%rbx)
.L102:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L112
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L107
	movq	%r10, 0(%r13,%rdx,8)
	addq	200(%rbx), %r15
	movq	(%r15), %rax
	movq	%r15, %r14
.L106:
	leaq	216(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L129
.L108:
	movq	$0, (%r14)
	movq	(%r12), %rsi
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L112:
	movq	%r10, %rax
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%rsi, 216(%rbx)
	jmp	.L108
	.cfi_endproc
.LFE7294:
	.size	_ZN12v8_inspector16InspectedContext21discardInjectedScriptEi, .-_ZN12v8_inspector16InspectedContext21discardInjectedScriptEi
	.section	.text._ZN12v8_inspector16InspectedContext17addInternalObjectEN2v85LocalINS1_6ObjectEEENS_19V8InternalValueTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16InspectedContext17addInternalObjectEN2v85LocalINS1_6ObjectEEENS_19V8InternalValueTypeE
	.type	_ZN12v8_inspector16InspectedContext17addInternalObjectEN2v85LocalINS1_6ObjectEEENS_19V8InternalValueTypeE, @function
_ZN12v8_inspector16InspectedContext17addInternalObjectEN2v85LocalINS1_6ObjectEEENS_19V8InternalValueTypeE:
.LFB7295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	264(%rdi), %r12
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	testq	%r12, %r12
	je	.L146
.L131:
	movq	(%r12), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
.L145:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
.L134:
	movl	%r14d, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r14
	testq	%rsi, %rsi
	je	.L136
	movq	(%rbx), %rax
	movq	(%rsi), %rsi
	movq	8(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
.L136:
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v85debug7WeakMap3SetENS_5LocalINS_7ContextEEENS2_INS_5ValueEEES6_@PLT
	testq	%rax, %rax
	setne	%al
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	call	_ZN2v85debug7WeakMap3NewEPNS_7IsolateE@PLT
	movq	264(%rbx), %rdi
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	8(%rax), %r8
	testq	%rdi, %rdi
	je	.L132
	movq	%r8, -56(%rbp)
	movq	%r15, %r12
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	testq	%r15, %r15
	movq	-56(%rbp), %r8
	movq	$0, 264(%rbx)
	je	.L145
.L133:
	movq	%r8, %rdi
	movq	%r15, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 264(%rbx)
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	testq	%r12, %r12
	je	.L134
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%r15, %r12
	movq	%r8, %rdi
	testq	%r15, %r15
	jne	.L133
	jmp	.L134
	.cfi_endproc
.LFE7295:
	.size	_ZN12v8_inspector16InspectedContext17addInternalObjectEN2v85LocalINS1_6ObjectEEENS_19V8InternalValueTypeE, .-_ZN12v8_inspector16InspectedContext17addInternalObjectEN2v85LocalINS1_6ObjectEEENS_19V8InternalValueTypeE
	.section	.text._ZN12v8_inspector16InspectedContext15getInternalTypeEN2v85LocalINS1_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16InspectedContext15getInternalTypeEN2v85LocalINS1_6ObjectEEE
	.type	_ZN12v8_inspector16InspectedContext15getInternalTypeEN2v85LocalINS1_6ObjectEEE, @function
_ZN12v8_inspector16InspectedContext15getInternalTypeEN2v85LocalINS1_6ObjectEEE:
.LFB7296:
	.cfi_startproc
	endbr64
	movq	264(%rdi), %rax
	testq	%rax, %rax
	je	.L159
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdx
	movq	(%rax), %rsi
	movq	8(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r12
	testq	%rsi, %rsi
	je	.L149
	movq	(%rbx), %rax
	movq	(%rsi), %rsi
	movq	8(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
.L149:
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN2v85debug7WeakMap3GetENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L147
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L147
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v85Int325ValueEv@PLT
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7296:
	.size	_ZN12v8_inspector16InspectedContext15getInternalTypeEN2v85LocalINS1_6ObjectEEE, .-_ZN12v8_inspector16InspectedContext15getInternalTypeEN2v85LocalINS1_6ObjectEEE
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector14InjectedScriptESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector14InjectedScriptESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector14InjectedScriptESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector14InjectedScriptESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector14InjectedScriptESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm:
.LFB11179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L163
	movq	(%rbx), %r8
.L164:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L173
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L174:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L187
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L188
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L166:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L168
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L170:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L171:
	testq	%rsi, %rsi
	je	.L168
.L169:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L170
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L176
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L169
	.p2align 4,,10
	.p2align 3
.L168:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L172
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L172:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L173:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L175
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L175:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%rdx, %rdi
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L166
.L188:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE11179:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector14InjectedScriptESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector14InjectedScriptESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.section	.rodata._ZN12v8_inspector16InspectedContext20createInjectedScriptEi.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"m_injectedScripts.find(sessionId) == m_injectedScripts.end()"
	.section	.rodata._ZN12v8_inspector16InspectedContext20createInjectedScriptEi.str1.1,"aMS",@progbits,1
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN12v8_inspector16InspectedContext20createInjectedScriptEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16InspectedContext20createInjectedScriptEi
	.type	_ZN12v8_inspector16InspectedContext20createInjectedScriptEi, @function
_ZN12v8_inspector16InspectedContext20createInjectedScriptEi:
.LFB7292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movslq	%esi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$272, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r13, %rbx
	subq	$8, %rsp
	call	_Znwm@PLT
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN12v8_inspector14InjectedScriptC1EPNS_16InspectedContextEi@PLT
	movq	208(%r12), %rsi
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	200(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L190
	movq	(%rax), %rcx
	movl	8(%rcx), %r8d
	movq	%rcx, %rdi
	movl	%r8d, %r9d
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L225:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L195
	movslq	8(%rdi), %rax
	xorl	%edx, %edx
	movq	%rax, %r9
	divq	%rsi
	cmpq	%rdx, %r15
	jne	.L195
.L193:
	cmpl	%ebx, %r9d
	jne	.L225
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L226:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L190
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rsi
	cmpq	%rdx, %r15
	jne	.L190
.L195:
	cmpl	%ebx, %r8d
	jne	.L226
	addq	$16, %rcx
.L201:
	movq	(%rcx), %r15
	movq	%r14, (%rcx)
	testq	%r15, %r15
	je	.L196
	movq	%r15, %rdi
	call	_ZN12v8_inspector14InjectedScriptD1Ev@PLT
	movl	$272, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L196:
	movq	208(%r12), %rcx
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	200(%r12), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r13
	testq	%r8, %r8
	je	.L189
	movq	(%r8), %r8
	movl	8(%r8), %esi
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L228:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L189
	movslq	8(%r8), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rcx
	cmpq	%r13, %rdx
	jne	.L227
.L199:
	cmpl	%ebx, %esi
	jne	.L228
	movq	16(%r8), %r8
.L189:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L190:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	200(%r12), %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movl	%ebx, 8(%rax)
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector14InjectedScriptESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L201
	.cfi_endproc
.LFE7292:
	.size	_ZN12v8_inspector16InspectedContext20createInjectedScriptEi, .-_ZN12v8_inspector16InspectedContext20createInjectedScriptEi
	.section	.text._ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm
	.type	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm, @function
_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm:
.LFB11957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L230
	movq	(%rbx), %r8
.L231:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L240
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L241:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L254
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L255
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L233:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L235
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L237:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L238:
	testq	%rsi, %rsi
	je	.L235
.L236:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L237
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L243
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L236
	.p2align 4,,10
	.p2align 3
.L235:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L239
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L239:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L240:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L242
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L242:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%rdx, %rdi
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L233
.L255:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE11957:
	.size	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm, .-_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm
	.section	.text._ZN12v8_inspector16InspectedContext11setReportedEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16InspectedContext11setReportedEib
	.type	_ZN12v8_inspector16InspectedContext11setReportedEib, @function
_ZN12v8_inspector16InspectedContext11setReportedEib:
.LFB7290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%esi, %r14
	pushq	%r13
	movq	%r14, %rax
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r14, %rbx
	subq	$8, %rsp
	movq	152(%rdi), %rcx
	movq	144(%rdi), %r10
	divq	%rcx
	leaq	0(,%rdx,8), %r15
	movq	%rdx, %r13
	leaq	(%r10,%r15), %r11
	movq	(%r11), %r8
	testb	%r9b, %r9b
	je	.L257
	testq	%r8, %r8
	je	.L258
	movq	(%r8), %rsi
	movl	8(%rsi), %edi
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L291:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L258
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rcx
	cmpq	%r13, %rdx
	jne	.L258
.L260:
	cmpl	%ebx, %edi
	jne	.L291
.L256:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	testq	%r8, %r8
	je	.L256
	movq	(%r8), %rdi
	movq	%r8, %r14
	movl	8(%rdi), %r9d
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L292:
	testq	%rsi, %rsi
	je	.L256
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	movq	%rdi, %r14
	movq	%rax, %r9
	divq	%rcx
	cmpq	%r13, %rdx
	jne	.L256
	movq	%rsi, %rdi
.L264:
	movq	(%rdi), %rsi
	cmpl	%ebx, %r9d
	jne	.L292
	cmpq	%r8, %r14
	je	.L293
	testq	%rsi, %rsi
	je	.L266
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r13
	je	.L266
	movq	%r14, (%r10,%rdx,8)
	movq	(%rdi), %rsi
.L266:
	movq	%rsi, (%r14)
	call	_ZdlPv@PLT
	subq	$1, 168(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	144(%r12), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movl	%ebx, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L265
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r13
	je	.L266
	movq	%r14, (%r10,%rdx,8)
	addq	144(%r12), %r15
	movq	(%r15), %r8
	movq	%r15, %r11
.L265:
	leaq	160(%r12), %rax
	cmpq	%rax, %r8
	je	.L294
.L267:
	movq	$0, (%r11)
	movq	(%rdi), %rsi
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%rsi, 160(%r12)
	jmp	.L267
	.cfi_endproc
.LFE7290:
	.size	_ZN12v8_inspector16InspectedContext11setReportedEib, .-_ZN12v8_inspector16InspectedContext11setReportedEib
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
