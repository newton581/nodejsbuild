	.file	"code-stats.cc"
	.text
	.section	.text._ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE
	.type	_ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE, @function
_ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE:
.LFB19520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdi), %rax
	cmpw	$96, 11(%rax)
	jne	.L2
	movq	7(%rdi), %rax
	testb	$1, %al
	jne	.L32
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	-1(%rdi), %rax
	cmpw	$72, 11(%rax)
	je	.L9
	movq	-1(%rdi), %rax
	cmpw	$69, 11(%rax)
	jne	.L1
.L9:
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	je	.L34
	movq	15(%rbx), %rax
	movslq	11(%rbx), %r13
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	-1(%rax), %rsi
	addl	$61, %r13d
	andl	$-8, %r13d
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	31(%rbx), %rdx
	addl	%eax, %r13d
	movq	23(%rbx), %rax
	movslq	11(%rax), %rax
	addl	$23, %eax
	andl	$-8, %eax
	addl	%r13d, %eax
	testb	$1, %dl
	jne	.L12
.L18:
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	leaq	-37592(%rcx), %rsi
	cmpq	-37280(%rcx), %rdx
	je	.L35
	movq	7(%rdx), %rdx
.L17:
	movslq	11(%rdx), %rdx
	addl	$23, %edx
	andl	$-8, %edx
	addl	%edx, %eax
.L11:
	movq	-1(%rbx), %rdx
	cmpw	$69, 11(%rdx)
	je	.L36
.L19:
	addl	%eax, 41804(%r12)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L32:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$2, %dx
	jne	.L1
	movl	41808(%rsi), %ebx
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal14ExternalString19ExternalPayloadSizeEv@PLT
	addl	%ebx, %eax
	movl	%eax, 41808(%r12)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	leaq	-37592(%rcx), %rsi
	cmpq	-37504(%rcx), %rdx
	je	.L11
	cmpq	312(%rsi), %rdx
	je	.L11
	movq	-1(%rdx), %rcx
	cmpw	$70, 11(%rcx)
	jne	.L18
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L34:
	movl	39(%rbx), %edx
	testb	$1, 43(%rbx)
	je	.L10
	leal	71(%rdx), %eax
	andl	$-8, %eax
	cltq
	addq	%rbx, %rax
	movslq	-1(%rax), %rdx
	leaq	7(%rax,%rdx), %rdx
	leaq	63(%rbx), %rax
	subl	%eax, %edx
.L10:
	movq	7(%rbx), %rax
	addl	$7, %edx
	leaq	-48(%rbp), %rdi
	andl	$-8, %edx
	movslq	11(%rax), %rax
	addl	$95, %edx
	andl	$-32, %edx
	addl	$23, %eax
	andl	$-8, %eax
	leal	(%rax,%rdx), %r13d
	movq	15(%rbx), %rax
	movq	%rax, -48(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-1(%rbx), %rdx
	addl	%r13d, %eax
	cmpw	$69, 11(%rdx)
	jne	.L19
.L36:
	addl	%eax, 41800(%r12)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L35:
	movq	976(%rsi), %rdx
	jmp	.L17
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19520:
	.size	_ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE, .-_ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE
	.section	.text._ZN2v88internal14CodeStatistics30ResetCodeAndMetadataStatisticsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CodeStatistics30ResetCodeAndMetadataStatisticsEPNS0_7IsolateE
	.type	_ZN2v88internal14CodeStatistics30ResetCodeAndMetadataStatisticsEPNS0_7IsolateE, @function
_ZN2v88internal14CodeStatistics30ResetCodeAndMetadataStatisticsEPNS0_7IsolateE:
.LFB19521:
	.cfi_startproc
	endbr64
	movq	$0, 41800(%rdi)
	movl	$0, 41808(%rdi)
	ret
	.cfi_endproc
.LFE19521:
	.size	_ZN2v88internal14CodeStatistics30ResetCodeAndMetadataStatisticsEPNS0_7IsolateE, .-_ZN2v88internal14CodeStatistics30ResetCodeAndMetadataStatisticsEPNS0_7IsolateE
	.section	.text._ZN2v88internal14CodeStatistics21CollectCodeStatisticsEPNS0_10PagedSpaceEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CodeStatistics21CollectCodeStatisticsEPNS0_10PagedSpaceEPNS0_7IsolateE
	.type	_ZN2v88internal14CodeStatistics21CollectCodeStatisticsEPNS0_10PagedSpaceEPNS0_7IsolateE, @function
_ZN2v88internal14CodeStatistics21CollectCodeStatisticsEPNS0_10PagedSpaceEPNS0_7IsolateE:
.LFB19522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-104(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-96(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal24PagedSpaceObjectIteratorC1EPNS0_10PagedSpaceE@PLT
	.p2align 4,,10
	.p2align 3
.L46:
	movq	-88(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L40
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L62:
	movq	112(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L41
	movq	%rdx, -88(%rbp)
	movq	%rdx, %rax
	cmpq	%rax, -80(%rbp)
	je	.L45
.L40:
	movq	-72(%rbp), %rdx
	cmpq	%rax, 104(%rdx)
	je	.L62
.L41:
	leaq	1(%rax), %rdx
	movq	%r12, %rdi
	movq	%rdx, -104(%rbp)
	movq	(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, -88(%rbp)
	movq	-104(%rbp), %rax
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	cmpw	$73, %dx
	je	.L43
	cmpw	$76, %dx
	jne	.L63
.L43:
	movq	-88(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L40
.L45:
	movq	%rbx, %rdi
	call	_ZN2v88internal24PagedSpaceObjectIterator17AdvanceToNextPageEv@PLT
	testb	%al, %al
	jne	.L46
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L45
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r13, %rsi
	call	_ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L53:
	movq	-88(%rbp), %rax
	cmpq	-80(%rbp), %rax
	jne	.L48
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L65:
	movq	112(%rdx), %rdx
	cmpq	%rdx, %rax
	je	.L49
	movq	%rdx, -88(%rbp)
	movq	%rdx, %rax
	cmpq	%rax, -80(%rbp)
	je	.L52
.L48:
	movq	-72(%rbp), %rdx
	cmpq	104(%rdx), %rax
	je	.L65
.L49:
	leaq	1(%rax), %rdx
	movq	%r12, %rdi
	movq	%rdx, -104(%rbp)
	movq	(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, -88(%rbp)
	movq	-104(%rbp), %rax
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	cmpw	$73, %dx
	je	.L51
	cmpw	$76, %dx
	jne	.L66
.L51:
	movq	-88(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L48
.L52:
	movq	%rbx, %rdi
	call	_ZN2v88internal24PagedSpaceObjectIterator17AdvanceToNextPageEv@PLT
	testb	%al, %al
	jne	.L53
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L66:
	movq	-1(%rax), %rdx
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L44
	movq	%rbx, %rdi
	call	_ZN2v88internal24PagedSpaceObjectIterator17AdvanceToNextPageEv@PLT
	testb	%al, %al
	jne	.L53
	jmp	.L38
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19522:
	.size	_ZN2v88internal14CodeStatistics21CollectCodeStatisticsEPNS0_10PagedSpaceEPNS0_7IsolateE, .-_ZN2v88internal14CodeStatistics21CollectCodeStatisticsEPNS0_10PagedSpaceEPNS0_7IsolateE
	.section	.text._ZN2v88internal14CodeStatistics21CollectCodeStatisticsEPNS0_16LargeObjectSpaceEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CodeStatistics21CollectCodeStatisticsEPNS0_16LargeObjectSpaceEPNS0_7IsolateE
	.type	_ZN2v88internal14CodeStatistics21CollectCodeStatisticsEPNS0_16LargeObjectSpaceEPNS0_7IsolateE, @function
_ZN2v88internal14CodeStatistics21CollectCodeStatisticsEPNS0_16LargeObjectSpaceEPNS0_7IsolateE:
.LFB19523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	%rdi, %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	-48(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal30LargeObjectSpaceObjectIteratorC1EPNS0_16LargeObjectSpaceE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal30LargeObjectSpaceObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L67
	movq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%r12, %rsi
	call	_ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE
	movq	%rbx, %rdi
	call	_ZN2v88internal30LargeObjectSpaceObjectIterator4NextEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L69
.L67:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19523:
	.size	_ZN2v88internal14CodeStatistics21CollectCodeStatisticsEPNS0_16LargeObjectSpaceEPNS0_7IsolateE, .-_ZN2v88internal14CodeStatistics21CollectCodeStatisticsEPNS0_16LargeObjectSpaceEPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE:
.LFB24398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24398:
	.size	_GLOBAL__sub_I__ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal14CodeStatistics31RecordCodeAndMetadataStatisticsENS0_10HeapObjectEPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
