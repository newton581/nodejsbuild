	.file	"builtins-console.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4860:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4860:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4861:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4861:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4863:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4863:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"(location_) != nullptr"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE:
.LFB18763:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movl	%ecx, -136(%rbp)
	movl	%r8d, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r13, -128(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal4Name14ToFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movl	-136(%rbp), %edx
	testq	%rax, %rax
	je	.L15
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal15NewFunctionArgs26ForBuiltinWithoutPrototypeENS0_6HandleINS0_6StringEEEiNS0_12LanguageModeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	movl	$1, %ecx
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %eax
	orl	$32, %eax
	movl	%eax, 47(%rdx)
	movl	$-1, %edx
	movq	(%r14), %rax
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	(%r14), %rax
	movq	23(%rax), %rax
	movq	-144(%rbp), %rsi
	movw	%cx, 39(%rax)
	movq	41112(%r12), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L7
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L8:
	leaq	3632(%r12), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L16
.L11:
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L18
.L9:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L16:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L11
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	3640(%r12), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L9
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18763:
	.size	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE, .-_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_113LogTimerEventEPNS0_7IsolateENS0_16BuiltinArgumentsENS0_6Logger8StartEndE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"default"
	.section	.text._ZN2v88internal12_GLOBAL__N_113LogTimerEventEPNS0_7IsolateENS0_16BuiltinArgumentsENS0_6Logger8StartEndE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_113LogTimerEventEPNS0_7IsolateENS0_16BuiltinArgumentsENS0_6Logger8StartEndE, @function
_ZN2v88internal12_GLOBAL__N_113LogTimerEventEPNS0_7IsolateENS0_16BuiltinArgumentsENS0_6Logger8StartEndE:
.LFB18671:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	41016(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L19
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	41096(%r15), %r14
	movq	%rax, -88(%rbp)
	cmpl	$5, %ebx
	jle	.L21
	movq	-8(%r12), %rax
	testb	$1, %al
	jne	.L38
.L21:
	movq	41016(%r15), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L39
.L24:
	movq	-88(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r14
	je	.L19
	movq	%r14, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	leaq	.LC2(%rip), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L21
	leaq	-64(%rbp), %rdi
	xorl	%r8d, %r8d
	leaq	-72(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	41016(%r15), %rbx
	movq	-64(%rbp), %r12
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L41
.L22:
	testq	%r12, %r12
	je	.L24
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	jmp	.L24
.L40:
	call	__stack_chk_fail@PLT
.L41:
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L22
	.cfi_endproc
.LFE18671:
	.size	_ZN2v88internal12_GLOBAL__N_113LogTimerEventEPNS0_7IsolateENS0_16BuiltinArgumentsENS0_6Logger8StartEndE, .-_ZN2v88internal12_GLOBAL__N_113LogTimerEventEPNS0_7IsolateENS0_16BuiltinArgumentsENS0_6Logger8StartEndE
	.section	.rodata._ZN2v88internalL27Builtin_Impl_ConsoleContextENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"Context"
.LC4:
	.string	"debug"
.LC5:
	.string	"error"
.LC6:
	.string	"info"
.LC7:
	.string	"log"
.LC8:
	.string	"warn"
.LC9:
	.string	"dir"
.LC10:
	.string	"dirXml"
.LC11:
	.string	"table"
.LC12:
	.string	"trace"
.LC13:
	.string	"group"
.LC14:
	.string	"groupCollapsed"
.LC15:
	.string	"groupEnd"
.LC16:
	.string	"clear"
.LC17:
	.string	"count"
.LC18:
	.string	"countReset"
.LC19:
	.string	"assert"
.LC20:
	.string	"profile"
.LC21:
	.string	"profileEnd"
.LC22:
	.string	"timeLog"
.LC23:
	.string	"time"
.LC24:
	.string	"timeEnd"
.LC25:
	.string	"timeStamp"
	.section	.text._ZN2v88internalL27Builtin_Impl_ConsoleContextENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Builtin_Impl_ConsoleContextENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL27Builtin_Impl_ConsoleContextENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18766:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%r13, %rsi
	subq	$104, %rsp
	movq	41096(%rdx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rax
	movq	$7, -120(%rbp)
	movq	%rax, -136(%rbp)
	leaq	.LC3(%rip), %rax
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%rax, %r15
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1247(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L44:
	movq	%r15, %rsi
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	call	_ZN2v88internal15NewFunctionArgs22ForFunctionWithoutCodeENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEENS0_12LanguageModeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	movq	%rax, %r13
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L46
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L47:
	xorl	%edx, %edx
	movq	%r12, %rdi
	subq	$8, %rbx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%rbx, %r9
	movl	$261, %ecx
	movq	%r12, %rdi
	movq	%rax, %r13
	movl	41844(%r12), %eax
	leaq	.LC4(%rip), %rdx
	movq	%r13, %rsi
	leal	1(%rax), %r15d
	movl	%r15d, 41844(%r12)
	movl	%r15d, %r8d
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$262, %ecx
	leaq	.LC5(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$263, %ecx
	leaq	.LC6(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$264, %ecx
	leaq	.LC7(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$265, %ecx
	leaq	.LC8(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$266, %ecx
	leaq	.LC9(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$267, %ecx
	leaq	.LC10(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$268, %ecx
	leaq	.LC11(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$269, %ecx
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$270, %ecx
	leaq	.LC13(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$271, %ecx
	leaq	.LC14(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$272, %ecx
	leaq	.LC15(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$273, %ecx
	leaq	.LC16(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$274, %ecx
	leaq	.LC17(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$275, %ecx
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$276, %ecx
	leaq	.LC19(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$278, %ecx
	leaq	.LC20(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$279, %ecx
	leaq	.LC21(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$281, %ecx
	leaq	.LC22(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$280, %ecx
	leaq	.LC23(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$282, %ecx
	leaq	.LC24(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	$283, %ecx
	leaq	.LC25(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122InstallContextFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEiNS4_INS0_6ObjectEEE
	movq	-136(%rbp), %rax
	movq	0(%r13), %r13
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L49
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L49:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L53
.L45:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L46:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L54
.L48:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%r12, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L48
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18766:
	.size	_ZN2v88internalL27Builtin_Impl_ConsoleContextENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL27Builtin_Impl_ConsoleContextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"!isolate->has_pending_exception()"
	.align 8
.LC27:
	.string	"!isolate->has_scheduled_exception()"
	.section	.text._ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0:
.LFB23041:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	96(%rdi), %rax
	cmpq	12480(%rdi), %rax
	jne	.L106
	movq	%rdi, %r15
	cmpq	12552(%rdi), %rax
	jne	.L107
	cmpq	$0, 45648(%rdi)
	je	.L55
	movq	41088(%rdi), %rax
	movq	%rsi, %r14
	movq	%rdx, %rbx
	addl	$1, 41104(%rdi)
	movq	12464(%rdi), %r12
	movq	%rdx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	41096(%rdi), %rax
	movq	41112(%rdi), %rdi
	movq	%rax, -184(%rbp)
	testq	%rdi, %rdi
	je	.L59
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L60:
	movq	(%r14), %rdi
	cmpl	$4, %edi
	jle	.L62
	leaq	-144(%rbp), %rax
	xorl	%r12d, %r12d
	movq	%rax, -208(%rbp)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$1, %r12
	leal	-4(%rdi), %eax
	cmpl	%r12d, %eax
	jle	.L62
.L73:
	movq	8(%r14), %r13
	leaq	0(,%r12,8), %rax
	subq	%rax, %r13
	movq	0(%r13), %rdx
	testb	$1, %dl
	je	.L64
	movq	-1(%rdx), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L64
	movq	-1(%rdx), %rax
	cmpw	$1026, 11(%rax)
	je	.L108
	movq	-1(%rdx), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L70:
	testb	%al, %al
	jne	.L102
	movq	(%r14), %rdi
	addq	$1, %r12
	leal	-4(%rdi), %eax
	cmpl	%r12d, %eax
	jg	.L73
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	-176(%rbp), %r12
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERNS_8internal16BuiltinArgumentsE@PLT
	movq	(%r14), %rax
	movq	8(%r14), %r13
	leaq	3632(%r15), %rsi
	leal	-16(,%rax,8), %edx
	movl	$2, %eax
	movslq	%edx, %rdx
	subq	%rdx, %r13
	movq	0(%r13), %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rdi
	movq	3632(%r15), %rcx
	movq	-1(%rcx), %r8
	subq	$37592, %rdi
	cmpw	$64, 11(%r8)
	jne	.L74
	xorl	%eax, %eax
	testb	$1, 11(%rcx)
	sete	%al
	addl	%eax, %eax
.L74:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	3632(%r15), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %ecx
	andl	$-32, %ecx
	cmpl	$32, %ecx
	je	.L109
.L75:
	movq	%r13, -96(%rbp)
	movq	%r13, -80(%rbp)
	leaq	-144(%rbp), %r13
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L76
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L77:
	movq	(%rax), %rax
	movq	%rax, %rdx
	sarq	$32, %rdx
	testb	$1, %al
	movl	$0, %eax
	movl	%edx, %esi
	movq	8(%r14), %rdx
	cmovne	%eax, %esi
	movq	(%r14), %rax
	leal	-16(,%rax,8), %eax
	movl	%esi, -208(%rbp)
	leaq	3640(%r15), %rsi
	cltq
	subq	%rax, %rdx
	movl	$2, %eax
	movq	(%rdx), %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rdi
	movq	3640(%r15), %rcx
	movq	-1(%rcx), %r10
	subq	$37592, %rdi
	cmpw	$64, 11(%r10)
	jne	.L79
	xorl	%eax, %eax
	testb	$1, 11(%rcx)
	sete	%al
	addl	%eax, %eax
.L79:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	3640(%r15), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %ecx
	andl	$-32, %ecx
	cmpl	$32, %ecx
	je	.L110
.L80:
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L81
	movq	-120(%rbp), %rax
	movq	88(%rax), %rdx
	addq	$88, %rax
	testb	$1, %dl
	jne	.L83
.L85:
	leaq	2016(%r15), %rax
.L84:
	movq	45648(%r15), %rdi
	testb	$1, %bl
	je	.L86
	movq	(%rdi), %rdx
	movq	-1(%rdx,%rbx), %rbx
	movq	%rbx, -192(%rbp)
.L86:
	movl	-208(%rbp), %ebx
	movq	%rax, -136(%rbp)
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	-192(%rbp), %rax
	movl	%ebx, -144(%rbp)
	call	*%rax
.L105:
	movq	-200(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-184(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L55
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L55:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	%rax, %rcx
	movq	-200(%rbp), %rax
	movq	%rax, %rsi
	cmpq	%rcx, %rax
	je	.L112
.L61:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r12, (%rsi)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	(%rax), %rdx
	testb	$1, %dl
	je	.L85
.L83:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L85
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	je	.L103
	movq	(%r14), %rdi
	movq	-216(%rbp), %rsi
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%rdx, %r11
	movq	-208(%rbp), %rdi
	movq	%rsi, -232(%rbp)
	andq	$-262144, %r11
	movq	%rdx, -224(%rbp)
	movq	24(%r11), %rax
	movq	%r11, -216(%rbp)
	movq	-25128(%rax), %rax
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-216(%rbp), %r11
	movq	-224(%rbp), %rdx
	movq	24(%r11), %r11
	movq	-1(%rdx), %rdi
	movq	-232(%rbp), %rsi
	cmpw	$1024, 11(%rdi)
	je	.L113
	movq	-1(%rdx), %rdx
	movq	23(%rdx), %rdx
.L69:
	cmpq	%rax, %rdx
	setne	%al
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-216(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L109:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L75
.L106:
	leaq	.LC26(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L107:
	leaq	.LC27(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L112:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L61
.L113:
	movq	-37488(%r11), %rdx
	jmp	.L69
.L103:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	jmp	.L105
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23041:
	.size	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0, .-_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB18192:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L120
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L123
.L114:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L114
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE18192:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC28:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC29:
	.string	"V8.Builtin_ConsoleDebug"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateE:
.LFB18697:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L155
.L125:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L156
.L127:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L157
.L129:
	leaq	-176(%rbp), %rsi
	movl	$1, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L158
	movq	88(%r12), %r12
.L134:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L159
.L124:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L160
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L157:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L161
.L130:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L131
	movq	(%rdi), %rax
	call	*8(%rax)
.L131:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L132
	movq	(%rdi), %rax
	call	*8(%rax)
.L132:
	leaq	.LC29(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L156:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L162
.L128:
	movq	%rbx, _ZZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L155:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$712, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L159:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L162:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L161:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC29(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L130
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18697:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_ConsoleErrorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"V8.Builtin_ConsoleError"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_ConsoleErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_ConsoleErrorEiPmPNS0_7IsolateE:
.LFB18700:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L194
.L164:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L195
.L166:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L196
.L168:
	leaq	-176(%rbp), %rsi
	movl	$9, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L197
	movq	88(%r12), %r12
.L173:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L198
.L163:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L196:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L200
.L169:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L170
	movq	(%rdi), %rax
	call	*8(%rax)
.L170:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L171
	movq	(%rdi), %rax
	call	*8(%rax)
.L171:
	leaq	.LC30(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L195:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L201
.L167:
	movq	%rbx, _ZZN2v88internalL31Builtin_Impl_Stats_ConsoleErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L194:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$713, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L198:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L201:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L200:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC30(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L169
.L199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18700:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleErrorEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_ConsoleErrorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL30Builtin_Impl_Stats_ConsoleInfoEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC31:
	.string	"V8.Builtin_ConsoleInfo"
	.section	.text._ZN2v88internalL30Builtin_Impl_Stats_ConsoleInfoEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Builtin_Impl_Stats_ConsoleInfoEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Builtin_Impl_Stats_ConsoleInfoEiPmPNS0_7IsolateE:
.LFB18703:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L233
.L203:
	movq	_ZZN2v88internalL30Builtin_Impl_Stats_ConsoleInfoEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L234
.L205:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L235
.L207:
	leaq	-176(%rbp), %rsi
	movl	$17, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L236
	movq	88(%r12), %r12
.L212:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L237
.L202:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L238
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L235:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L239
.L208:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L209
	movq	(%rdi), %rax
	call	*8(%rax)
.L209:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L210
	movq	(%rdi), %rax
	call	*8(%rax)
.L210:
	leaq	.LC31(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L234:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L240
.L206:
	movq	%rbx, _ZZN2v88internalL30Builtin_Impl_Stats_ConsoleInfoEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L233:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$714, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L240:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L239:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC31(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L208
.L238:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18703:
	.size	_ZN2v88internalL30Builtin_Impl_Stats_ConsoleInfoEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Builtin_Impl_Stats_ConsoleInfoEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Builtin_Impl_Stats_ConsoleLogEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"V8.Builtin_ConsoleLog"
	.section	.text._ZN2v88internalL29Builtin_Impl_Stats_ConsoleLogEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Builtin_Impl_Stats_ConsoleLogEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Builtin_Impl_Stats_ConsoleLogEiPmPNS0_7IsolateE:
.LFB18706:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L272
.L242:
	movq	_ZZN2v88internalL29Builtin_Impl_Stats_ConsoleLogEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L273
.L244:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L274
.L246:
	leaq	-176(%rbp), %rsi
	movl	$25, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L275
	movq	88(%r12), %r12
.L251:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L276
.L241:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L277
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L274:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L278
.L247:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L248
	movq	(%rdi), %rax
	call	*8(%rax)
.L248:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L249
	movq	(%rdi), %rax
	call	*8(%rax)
.L249:
	leaq	.LC32(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L273:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L279
.L245:
	movq	%rbx, _ZZN2v88internalL29Builtin_Impl_Stats_ConsoleLogEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L272:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$715, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L276:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L278:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC32(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L247
.L277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18706:
	.size	_ZN2v88internalL29Builtin_Impl_Stats_ConsoleLogEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Builtin_Impl_Stats_ConsoleLogEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL30Builtin_Impl_Stats_ConsoleWarnEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC33:
	.string	"V8.Builtin_ConsoleWarn"
	.section	.text._ZN2v88internalL30Builtin_Impl_Stats_ConsoleWarnEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Builtin_Impl_Stats_ConsoleWarnEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Builtin_Impl_Stats_ConsoleWarnEiPmPNS0_7IsolateE:
.LFB18709:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L311
.L281:
	movq	_ZZN2v88internalL30Builtin_Impl_Stats_ConsoleWarnEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L312
.L283:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L313
.L285:
	leaq	-176(%rbp), %rsi
	movl	$33, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L314
	movq	88(%r12), %r12
.L290:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L315
.L280:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L316
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L313:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L317
.L286:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L287
	movq	(%rdi), %rax
	call	*8(%rax)
.L287:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L288
	movq	(%rdi), %rax
	call	*8(%rax)
.L288:
	leaq	.LC33(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L312:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L318
.L284:
	movq	%rbx, _ZZN2v88internalL30Builtin_Impl_Stats_ConsoleWarnEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L311:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$716, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L315:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L318:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L317:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC33(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L286
.L316:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18709:
	.size	_ZN2v88internalL30Builtin_Impl_Stats_ConsoleWarnEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Builtin_Impl_Stats_ConsoleWarnEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Builtin_Impl_Stats_ConsoleDirEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC34:
	.string	"V8.Builtin_ConsoleDir"
	.section	.text._ZN2v88internalL29Builtin_Impl_Stats_ConsoleDirEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Builtin_Impl_Stats_ConsoleDirEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Builtin_Impl_Stats_ConsoleDirEiPmPNS0_7IsolateE:
.LFB18712:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L350
.L320:
	movq	_ZZN2v88internalL29Builtin_Impl_Stats_ConsoleDirEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L351
.L322:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L352
.L324:
	leaq	-176(%rbp), %rsi
	movl	$41, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L353
	movq	88(%r12), %r12
.L329:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L354
.L319:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L355
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L352:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L356
.L325:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L326
	movq	(%rdi), %rax
	call	*8(%rax)
.L326:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L327
	movq	(%rdi), %rax
	call	*8(%rax)
.L327:
	leaq	.LC34(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L351:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L357
.L323:
	movq	%rbx, _ZZN2v88internalL29Builtin_Impl_Stats_ConsoleDirEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L350:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$717, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L354:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L357:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L356:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC34(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L325
.L355:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18712:
	.size	_ZN2v88internalL29Builtin_Impl_Stats_ConsoleDirEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Builtin_Impl_Stats_ConsoleDirEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Builtin_Impl_Stats_ConsoleDirXmlEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC35:
	.string	"V8.Builtin_ConsoleDirXml"
	.section	.text._ZN2v88internalL32Builtin_Impl_Stats_ConsoleDirXmlEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Builtin_Impl_Stats_ConsoleDirXmlEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Builtin_Impl_Stats_ConsoleDirXmlEiPmPNS0_7IsolateE:
.LFB18715:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L389
.L359:
	movq	_ZZN2v88internalL32Builtin_Impl_Stats_ConsoleDirXmlEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L390
.L361:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L391
.L363:
	leaq	-176(%rbp), %rsi
	movl	$49, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L392
	movq	88(%r12), %r12
.L368:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L393
.L358:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L394
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L391:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L395
.L364:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L365
	movq	(%rdi), %rax
	call	*8(%rax)
.L365:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L366
	movq	(%rdi), %rax
	call	*8(%rax)
.L366:
	leaq	.LC35(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L390:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L396
.L362:
	movq	%rbx, _ZZN2v88internalL32Builtin_Impl_Stats_ConsoleDirXmlEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L389:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$718, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L393:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L396:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L395:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC35(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L364
.L394:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18715:
	.size	_ZN2v88internalL32Builtin_Impl_Stats_ConsoleDirXmlEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Builtin_Impl_Stats_ConsoleDirXmlEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_ConsoleTableEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC36:
	.string	"V8.Builtin_ConsoleTable"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_ConsoleTableEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleTableEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_ConsoleTableEiPmPNS0_7IsolateE:
.LFB18718:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L428
.L398:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleTableEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L429
.L400:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L430
.L402:
	leaq	-176(%rbp), %rsi
	movl	$57, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L431
	movq	88(%r12), %r12
.L407:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L432
.L397:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L433
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L430:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L434
.L403:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L404
	movq	(%rdi), %rax
	call	*8(%rax)
.L404:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L405
	movq	(%rdi), %rax
	call	*8(%rax)
.L405:
	leaq	.LC36(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L429:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L435
.L401:
	movq	%rbx, _ZZN2v88internalL31Builtin_Impl_Stats_ConsoleTableEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L428:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$719, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L432:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L435:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L434:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC36(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L403
.L433:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18718:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleTableEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_ConsoleTableEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_ConsoleTraceEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC37:
	.string	"V8.Builtin_ConsoleTrace"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_ConsoleTraceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleTraceEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_ConsoleTraceEiPmPNS0_7IsolateE:
.LFB18721:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L467
.L437:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleTraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L468
.L439:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L469
.L441:
	leaq	-176(%rbp), %rsi
	movl	$65, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L470
	movq	88(%r12), %r12
.L446:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L471
.L436:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L472
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L469:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L473
.L442:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L443
	movq	(%rdi), %rax
	call	*8(%rax)
.L443:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L444
	movq	(%rdi), %rax
	call	*8(%rax)
.L444:
	leaq	.LC37(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L468:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L474
.L440:
	movq	%rbx, _ZZN2v88internalL31Builtin_Impl_Stats_ConsoleTraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L467:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$720, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L471:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L474:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L473:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC37(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L442
.L472:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18721:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleTraceEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_ConsoleTraceEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_ConsoleGroupEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC38:
	.string	"V8.Builtin_ConsoleGroup"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_ConsoleGroupEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleGroupEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_ConsoleGroupEiPmPNS0_7IsolateE:
.LFB18724:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L506
.L476:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleGroupEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L507
.L478:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L508
.L480:
	leaq	-176(%rbp), %rsi
	movl	$73, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L509
	movq	88(%r12), %r12
.L485:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L510
.L475:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L511
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L508:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L512
.L481:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L482
	movq	(%rdi), %rax
	call	*8(%rax)
.L482:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L483
	movq	(%rdi), %rax
	call	*8(%rax)
.L483:
	leaq	.LC38(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L507:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L513
.L479:
	movq	%rbx, _ZZN2v88internalL31Builtin_Impl_Stats_ConsoleGroupEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L506:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$721, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L510:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L513:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L512:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC38(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L481
.L511:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18724:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleGroupEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_ConsoleGroupEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_Stats_ConsoleGroupCollapsedEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"V8.Builtin_ConsoleGroupCollapsed"
	.section	.text._ZN2v88internalL40Builtin_Impl_Stats_ConsoleGroupCollapsedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_Stats_ConsoleGroupCollapsedEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_Stats_ConsoleGroupCollapsedEiPmPNS0_7IsolateE:
.LFB18727:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L545
.L515:
	movq	_ZZN2v88internalL40Builtin_Impl_Stats_ConsoleGroupCollapsedEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L546
.L517:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L547
.L519:
	leaq	-176(%rbp), %rsi
	movl	$81, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L548
	movq	88(%r12), %r12
.L524:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L549
.L514:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L550
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L547:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L551
.L520:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L521
	movq	(%rdi), %rax
	call	*8(%rax)
.L521:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L522
	movq	(%rdi), %rax
	call	*8(%rax)
.L522:
	leaq	.LC39(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L546:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L552
.L518:
	movq	%rbx, _ZZN2v88internalL40Builtin_Impl_Stats_ConsoleGroupCollapsedEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L545:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$722, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L549:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L552:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L551:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC39(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L520
.L550:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18727:
	.size	_ZN2v88internalL40Builtin_Impl_Stats_ConsoleGroupCollapsedEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_Stats_ConsoleGroupCollapsedEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Builtin_Impl_Stats_ConsoleGroupEndEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC40:
	.string	"V8.Builtin_ConsoleGroupEnd"
	.section	.text._ZN2v88internalL34Builtin_Impl_Stats_ConsoleGroupEndEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Builtin_Impl_Stats_ConsoleGroupEndEiPmPNS0_7IsolateE, @function
_ZN2v88internalL34Builtin_Impl_Stats_ConsoleGroupEndEiPmPNS0_7IsolateE:
.LFB18730:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L584
.L554:
	movq	_ZZN2v88internalL34Builtin_Impl_Stats_ConsoleGroupEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L585
.L556:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L586
.L558:
	leaq	-176(%rbp), %rsi
	movl	$89, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L587
	movq	88(%r12), %r12
.L563:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L588
.L553:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L589
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L586:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L590
.L559:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L560
	movq	(%rdi), %rax
	call	*8(%rax)
.L560:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L561
	movq	(%rdi), %rax
	call	*8(%rax)
.L561:
	leaq	.LC40(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L585:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L591
.L557:
	movq	%rbx, _ZZN2v88internalL34Builtin_Impl_Stats_ConsoleGroupEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L584:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$723, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L588:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L591:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L590:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC40(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L559
.L589:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18730:
	.size	_ZN2v88internalL34Builtin_Impl_Stats_ConsoleGroupEndEiPmPNS0_7IsolateE, .-_ZN2v88internalL34Builtin_Impl_Stats_ConsoleGroupEndEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_ConsoleClearEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC41:
	.string	"V8.Builtin_ConsoleClear"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_ConsoleClearEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleClearEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_ConsoleClearEiPmPNS0_7IsolateE:
.LFB18733:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L623
.L593:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleClearEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L624
.L595:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L625
.L597:
	leaq	-176(%rbp), %rsi
	movl	$97, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L626
	movq	88(%r12), %r12
.L602:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L627
.L592:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L628
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L625:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L629
.L598:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L599
	movq	(%rdi), %rax
	call	*8(%rax)
.L599:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L600
	movq	(%rdi), %rax
	call	*8(%rax)
.L600:
	leaq	.LC41(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L624:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L630
.L596:
	movq	%rbx, _ZZN2v88internalL31Builtin_Impl_Stats_ConsoleClearEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L623:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$724, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L627:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L630:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L629:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC41(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L598
.L628:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18733:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleClearEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_ConsoleClearEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_ConsoleCountEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC42:
	.string	"V8.Builtin_ConsoleCount"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_ConsoleCountEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleCountEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_ConsoleCountEiPmPNS0_7IsolateE:
.LFB18736:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L662
.L632:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleCountEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L663
.L634:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L664
.L636:
	leaq	-176(%rbp), %rsi
	movl	$105, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L665
	movq	88(%r12), %r12
.L641:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L666
.L631:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L667
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L665:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L664:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L668
.L637:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L638
	movq	(%rdi), %rax
	call	*8(%rax)
.L638:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L639
	movq	(%rdi), %rax
	call	*8(%rax)
.L639:
	leaq	.LC42(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L663:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L669
.L635:
	movq	%rbx, _ZZN2v88internalL31Builtin_Impl_Stats_ConsoleCountEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L662:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$725, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L666:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L669:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L668:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC42(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L637
.L667:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18736:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleCountEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_ConsoleCountEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_Stats_ConsoleCountResetEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC43:
	.string	"V8.Builtin_ConsoleCountReset"
	.section	.text._ZN2v88internalL36Builtin_Impl_Stats_ConsoleCountResetEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_Stats_ConsoleCountResetEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_Stats_ConsoleCountResetEiPmPNS0_7IsolateE:
.LFB18739:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L701
.L671:
	movq	_ZZN2v88internalL36Builtin_Impl_Stats_ConsoleCountResetEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L702
.L673:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L703
.L675:
	leaq	-176(%rbp), %rsi
	movl	$113, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L704
	movq	88(%r12), %r12
.L680:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L705
.L670:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L706
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L704:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L703:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L707
.L676:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L677
	movq	(%rdi), %rax
	call	*8(%rax)
.L677:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L678
	movq	(%rdi), %rax
	call	*8(%rax)
.L678:
	leaq	.LC43(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L702:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L708
.L674:
	movq	%rbx, _ZZN2v88internalL36Builtin_Impl_Stats_ConsoleCountResetEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L701:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$726, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L705:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L708:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L707:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC43(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L676
.L706:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18739:
	.size	_ZN2v88internalL36Builtin_Impl_Stats_ConsoleCountResetEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_Stats_ConsoleCountResetEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Builtin_Impl_Stats_ConsoleAssertEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC44:
	.string	"V8.Builtin_ConsoleAssert"
	.section	.text._ZN2v88internalL32Builtin_Impl_Stats_ConsoleAssertEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Builtin_Impl_Stats_ConsoleAssertEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Builtin_Impl_Stats_ConsoleAssertEiPmPNS0_7IsolateE:
.LFB18742:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L740
.L710:
	movq	_ZZN2v88internalL32Builtin_Impl_Stats_ConsoleAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L741
.L712:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L742
.L714:
	leaq	-176(%rbp), %rsi
	movl	$121, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L743
	movq	88(%r12), %r12
.L719:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L744
.L709:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L745
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L743:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L742:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L746
.L715:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L716
	movq	(%rdi), %rax
	call	*8(%rax)
.L716:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L717
	movq	(%rdi), %rax
	call	*8(%rax)
.L717:
	leaq	.LC44(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L741:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L747
.L713:
	movq	%rbx, _ZZN2v88internalL32Builtin_Impl_Stats_ConsoleAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L740:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$727, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L744:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L747:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L746:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC44(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L715
.L745:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18742:
	.size	_ZN2v88internalL32Builtin_Impl_Stats_ConsoleAssertEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Builtin_Impl_Stats_ConsoleAssertEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Builtin_Impl_Stats_ConsoleProfileEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC45:
	.string	"V8.Builtin_ConsoleProfile"
	.section	.text._ZN2v88internalL33Builtin_Impl_Stats_ConsoleProfileEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Builtin_Impl_Stats_ConsoleProfileEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Builtin_Impl_Stats_ConsoleProfileEiPmPNS0_7IsolateE:
.LFB18745:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L779
.L749:
	movq	_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleProfileEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L780
.L751:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L781
.L753:
	leaq	-176(%rbp), %rsi
	movl	$129, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L782
	movq	88(%r12), %r12
.L758:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L783
.L748:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L784
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L781:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L785
.L754:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L755
	movq	(%rdi), %rax
	call	*8(%rax)
.L755:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L756
	movq	(%rdi), %rax
	call	*8(%rax)
.L756:
	leaq	.LC45(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L780:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L786
.L752:
	movq	%rbx, _ZZN2v88internalL33Builtin_Impl_Stats_ConsoleProfileEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L779:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$728, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L783:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L786:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L785:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC45(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L754
.L784:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18745:
	.size	_ZN2v88internalL33Builtin_Impl_Stats_ConsoleProfileEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Builtin_Impl_Stats_ConsoleProfileEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_Stats_ConsoleProfileEndEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC46:
	.string	"V8.Builtin_ConsoleProfileEnd"
	.section	.text._ZN2v88internalL36Builtin_Impl_Stats_ConsoleProfileEndEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_Stats_ConsoleProfileEndEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_Stats_ConsoleProfileEndEiPmPNS0_7IsolateE:
.LFB18748:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L818
.L788:
	movq	_ZZN2v88internalL36Builtin_Impl_Stats_ConsoleProfileEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L819
.L790:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L820
.L792:
	leaq	-176(%rbp), %rsi
	movl	$137, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L821
	movq	88(%r12), %r12
.L797:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L822
.L787:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L823
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L821:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L820:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L824
.L793:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L794
	movq	(%rdi), %rax
	call	*8(%rax)
.L794:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L795
	movq	(%rdi), %rax
	call	*8(%rax)
.L795:
	leaq	.LC46(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L819:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L825
.L791:
	movq	%rbx, _ZZN2v88internalL36Builtin_Impl_Stats_ConsoleProfileEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L818:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$729, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L822:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L825:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L824:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC46(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L793
.L823:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18748:
	.size	_ZN2v88internalL36Builtin_Impl_Stats_ConsoleProfileEndEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_Stats_ConsoleProfileEndEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeLogEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC47:
	.string	"V8.Builtin_ConsoleTimeLog"
	.section	.text._ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeLogEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeLogEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeLogEiPmPNS0_7IsolateE:
.LFB18751:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L857
.L827:
	movq	_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeLogEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L858
.L829:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L859
.L831:
	leaq	-176(%rbp), %rsi
	movl	$153, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L860
	movq	88(%r12), %r12
.L836:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L861
.L826:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L862
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L860:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L859:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L863
.L832:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L833
	movq	(%rdi), %rax
	call	*8(%rax)
.L833:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L834
	movq	(%rdi), %rax
	call	*8(%rax)
.L834:
	leaq	.LC47(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L858:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L864
.L830:
	movq	%rbx, _ZZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeLogEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L857:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$731, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L861:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L864:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L863:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC47(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L832
.L862:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18751:
	.size	_ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeLogEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeLogEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Builtin_Impl_Stats_ConsoleContextEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC48:
	.string	"V8.Builtin_ConsoleContext"
	.section	.text._ZN2v88internalL33Builtin_Impl_Stats_ConsoleContextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Builtin_Impl_Stats_ConsoleContextEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Builtin_Impl_Stats_ConsoleContextEiPmPNS0_7IsolateE:
.LFB18764:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L894
.L866:
	movq	_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic153(%rip), %rbx
	testq	%rbx, %rbx
	je	.L895
.L868:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L896
.L870:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL27Builtin_Impl_ConsoleContextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L897
.L874:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L898
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L895:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L899
.L869:
	movq	%rbx, _ZZN2v88internalL33Builtin_Impl_Stats_ConsoleContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic153(%rip)
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L896:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L900
.L871:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L872
	movq	(%rdi), %rax
	call	*8(%rax)
.L872:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L873
	movq	(%rdi), %rax
	call	*8(%rax)
.L873:
	leaq	.LC48(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L897:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L894:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$734, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L900:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC48(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L899:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L869
.L898:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18764:
	.size	_ZN2v88internalL33Builtin_Impl_Stats_ConsoleContextEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Builtin_Impl_Stats_ConsoleContextEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL30Builtin_Impl_Stats_ConsoleTimeEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC49:
	.string	"V8.Builtin_ConsoleTime"
	.section	.text._ZN2v88internalL30Builtin_Impl_Stats_ConsoleTimeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Builtin_Impl_Stats_ConsoleTimeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Builtin_Impl_Stats_ConsoleTimeEiPmPNS0_7IsolateE:
.LFB18754:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L932
.L902:
	movq	_ZZN2v88internalL30Builtin_Impl_Stats_ConsoleTimeEiPmPNS0_7IsolateEE28trace_event_unique_atomic105(%rip), %rbx
	testq	%rbx, %rbx
	je	.L933
.L904:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L934
.L906:
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_113LogTimerEventEPNS0_7IsolateENS0_16BuiltinArgumentsENS0_6Logger8StartEndE
	movl	$145, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	-176(%rbp), %rsi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L935
	movq	88(%r12), %r12
.L911:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L936
.L901:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L937
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L935:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L934:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L938
.L907:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L908
	movq	(%rdi), %rax
	call	*8(%rax)
.L908:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L909
	movq	(%rdi), %rax
	call	*8(%rax)
.L909:
	leaq	.LC49(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L933:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L939
.L905:
	movq	%rbx, _ZZN2v88internalL30Builtin_Impl_Stats_ConsoleTimeEiPmPNS0_7IsolateEE28trace_event_unique_atomic105(%rip)
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L932:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$730, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L936:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L939:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L938:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC49(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L907
.L937:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18754:
	.size	_ZN2v88internalL30Builtin_Impl_Stats_ConsoleTimeEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Builtin_Impl_Stats_ConsoleTimeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeEndEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC50:
	.string	"V8.Builtin_ConsoleTimeEnd"
	.section	.text._ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeEndEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeEndEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeEndEiPmPNS0_7IsolateE:
.LFB18757:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L971
.L941:
	movq	_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic112(%rip), %rbx
	testq	%rbx, %rbx
	je	.L972
.L943:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L973
.L945:
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_113LogTimerEventEPNS0_7IsolateENS0_16BuiltinArgumentsENS0_6Logger8StartEndE
	movl	$161, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	-176(%rbp), %rsi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L974
	movq	88(%r12), %r12
.L950:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L975
.L940:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L976
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L974:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L973:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L977
.L946:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L947
	movq	(%rdi), %rax
	call	*8(%rax)
.L947:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L948
	movq	(%rdi), %rax
	call	*8(%rax)
.L948:
	leaq	.LC50(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L972:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L978
.L944:
	movq	%rbx, _ZZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic112(%rip)
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L971:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$732, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L975:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L978:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L977:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC50(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L946
.L976:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18757:
	.size	_ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeEndEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeEndEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Builtin_Impl_Stats_ConsoleTimeStampEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC51:
	.string	"V8.Builtin_ConsoleTimeStamp"
	.section	.text._ZN2v88internalL35Builtin_Impl_Stats_ConsoleTimeStampEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Builtin_Impl_Stats_ConsoleTimeStampEiPmPNS0_7IsolateE, @function
_ZN2v88internalL35Builtin_Impl_Stats_ConsoleTimeStampEiPmPNS0_7IsolateE:
.LFB18760:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1010
.L980:
	movq	_ZZN2v88internalL35Builtin_Impl_Stats_ConsoleTimeStampEiPmPNS0_7IsolateEE28trace_event_unique_atomic119(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1011
.L982:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1012
.L984:
	movl	$2, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r14, -176(%rbp)
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_113LogTimerEventEPNS0_7IsolateENS0_16BuiltinArgumentsENS0_6Logger8StartEndE
	movl	$169, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	-176(%rbp), %rsi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1013
	movq	88(%r12), %r12
.L989:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1014
.L979:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1015
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1013:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r12
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L1012:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1016
.L985:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L986
	movq	(%rdi), %rax
	call	*8(%rax)
.L986:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L987
	movq	(%rdi), %rax
	call	*8(%rax)
.L987:
	leaq	.LC51(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1011:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1017
.L983:
	movq	%rbx, _ZZN2v88internalL35Builtin_Impl_Stats_ConsoleTimeStampEiPmPNS0_7IsolateEE28trace_event_unique_atomic119(%rip)
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$733, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L1014:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1017:
	leaq	.LC28(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1016:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC51(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L985
.L1015:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18760:
	.size	_ZN2v88internalL35Builtin_Impl_Stats_ConsoleTimeStampEiPmPNS0_7IsolateE, .-_ZN2v88internalL35Builtin_Impl_Stats_ConsoleTimeStampEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_ConsoleDebugEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_ConsoleDebugEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_ConsoleDebugEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_ConsoleDebugEiPmPNS0_7IsolateE:
.LFB18698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1025
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$1, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1026
	movq	88(%r12), %rax
.L1018:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1027
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1026:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1025:
	call	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateE
	jmp	.L1018
.L1027:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18698:
	.size	_ZN2v88internal20Builtin_ConsoleDebugEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_ConsoleDebugEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_ConsoleErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_ConsoleErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_ConsoleErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_ConsoleErrorEiPmPNS0_7IsolateE:
.LFB18701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1035
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$9, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1036
	movq	88(%r12), %rax
.L1028:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1037
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1036:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1035:
	call	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleErrorEiPmPNS0_7IsolateE
	jmp	.L1028
.L1037:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18701:
	.size	_ZN2v88internal20Builtin_ConsoleErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_ConsoleErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Builtin_ConsoleInfoEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Builtin_ConsoleInfoEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Builtin_ConsoleInfoEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Builtin_ConsoleInfoEiPmPNS0_7IsolateE:
.LFB18704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1045
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$17, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1046
	movq	88(%r12), %rax
.L1038:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1047
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1046:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1045:
	call	_ZN2v88internalL30Builtin_Impl_Stats_ConsoleInfoEiPmPNS0_7IsolateE
	jmp	.L1038
.L1047:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18704:
	.size	_ZN2v88internal19Builtin_ConsoleInfoEiPmPNS0_7IsolateE, .-_ZN2v88internal19Builtin_ConsoleInfoEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Builtin_ConsoleLogEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Builtin_ConsoleLogEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Builtin_ConsoleLogEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Builtin_ConsoleLogEiPmPNS0_7IsolateE:
.LFB18707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1055
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$25, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1056
	movq	88(%r12), %rax
.L1048:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1057
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1056:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1055:
	call	_ZN2v88internalL29Builtin_Impl_Stats_ConsoleLogEiPmPNS0_7IsolateE
	jmp	.L1048
.L1057:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18707:
	.size	_ZN2v88internal18Builtin_ConsoleLogEiPmPNS0_7IsolateE, .-_ZN2v88internal18Builtin_ConsoleLogEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Builtin_ConsoleWarnEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Builtin_ConsoleWarnEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Builtin_ConsoleWarnEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Builtin_ConsoleWarnEiPmPNS0_7IsolateE:
.LFB18710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1065
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$33, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1066
	movq	88(%r12), %rax
.L1058:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1067
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1066:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1065:
	call	_ZN2v88internalL30Builtin_Impl_Stats_ConsoleWarnEiPmPNS0_7IsolateE
	jmp	.L1058
.L1067:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18710:
	.size	_ZN2v88internal19Builtin_ConsoleWarnEiPmPNS0_7IsolateE, .-_ZN2v88internal19Builtin_ConsoleWarnEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Builtin_ConsoleDirEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Builtin_ConsoleDirEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Builtin_ConsoleDirEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Builtin_ConsoleDirEiPmPNS0_7IsolateE:
.LFB18713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1075
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$41, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1076
	movq	88(%r12), %rax
.L1068:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1077
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1076:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1068
	.p2align 4,,10
	.p2align 3
.L1075:
	call	_ZN2v88internalL29Builtin_Impl_Stats_ConsoleDirEiPmPNS0_7IsolateE
	jmp	.L1068
.L1077:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18713:
	.size	_ZN2v88internal18Builtin_ConsoleDirEiPmPNS0_7IsolateE, .-_ZN2v88internal18Builtin_ConsoleDirEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Builtin_ConsoleDirXmlEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Builtin_ConsoleDirXmlEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Builtin_ConsoleDirXmlEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Builtin_ConsoleDirXmlEiPmPNS0_7IsolateE:
.LFB18716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1085
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$49, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1086
	movq	88(%r12), %rax
.L1078:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1087
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1086:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1085:
	call	_ZN2v88internalL32Builtin_Impl_Stats_ConsoleDirXmlEiPmPNS0_7IsolateE
	jmp	.L1078
.L1087:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18716:
	.size	_ZN2v88internal21Builtin_ConsoleDirXmlEiPmPNS0_7IsolateE, .-_ZN2v88internal21Builtin_ConsoleDirXmlEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_ConsoleTableEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_ConsoleTableEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_ConsoleTableEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_ConsoleTableEiPmPNS0_7IsolateE:
.LFB18719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1095
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$57, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1096
	movq	88(%r12), %rax
.L1088:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1097
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1096:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1095:
	call	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleTableEiPmPNS0_7IsolateE
	jmp	.L1088
.L1097:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18719:
	.size	_ZN2v88internal20Builtin_ConsoleTableEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_ConsoleTableEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_ConsoleTraceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_ConsoleTraceEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_ConsoleTraceEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_ConsoleTraceEiPmPNS0_7IsolateE:
.LFB18722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1105
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$65, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1106
	movq	88(%r12), %rax
.L1098:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1107
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1106:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1105:
	call	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleTraceEiPmPNS0_7IsolateE
	jmp	.L1098
.L1107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18722:
	.size	_ZN2v88internal20Builtin_ConsoleTraceEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_ConsoleTraceEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_ConsoleGroupEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_ConsoleGroupEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_ConsoleGroupEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_ConsoleGroupEiPmPNS0_7IsolateE:
.LFB18725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1115
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$73, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1116
	movq	88(%r12), %rax
.L1108:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1117
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1116:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1115:
	call	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleGroupEiPmPNS0_7IsolateE
	jmp	.L1108
.L1117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18725:
	.size	_ZN2v88internal20Builtin_ConsoleGroupEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_ConsoleGroupEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Builtin_ConsoleGroupCollapsedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Builtin_ConsoleGroupCollapsedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Builtin_ConsoleGroupCollapsedEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Builtin_ConsoleGroupCollapsedEiPmPNS0_7IsolateE:
.LFB18728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1125
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$81, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1126
	movq	88(%r12), %rax
.L1118:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1127
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1126:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1125:
	call	_ZN2v88internalL40Builtin_Impl_Stats_ConsoleGroupCollapsedEiPmPNS0_7IsolateE
	jmp	.L1118
.L1127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18728:
	.size	_ZN2v88internal29Builtin_ConsoleGroupCollapsedEiPmPNS0_7IsolateE, .-_ZN2v88internal29Builtin_ConsoleGroupCollapsedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Builtin_ConsoleGroupEndEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Builtin_ConsoleGroupEndEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Builtin_ConsoleGroupEndEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Builtin_ConsoleGroupEndEiPmPNS0_7IsolateE:
.LFB18731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1135
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$89, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1136
	movq	88(%r12), %rax
.L1128:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1137
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1136:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1135:
	call	_ZN2v88internalL34Builtin_Impl_Stats_ConsoleGroupEndEiPmPNS0_7IsolateE
	jmp	.L1128
.L1137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18731:
	.size	_ZN2v88internal23Builtin_ConsoleGroupEndEiPmPNS0_7IsolateE, .-_ZN2v88internal23Builtin_ConsoleGroupEndEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_ConsoleClearEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_ConsoleClearEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_ConsoleClearEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_ConsoleClearEiPmPNS0_7IsolateE:
.LFB18734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1145
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$97, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1146
	movq	88(%r12), %rax
.L1138:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1147
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1146:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1145:
	call	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleClearEiPmPNS0_7IsolateE
	jmp	.L1138
.L1147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18734:
	.size	_ZN2v88internal20Builtin_ConsoleClearEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_ConsoleClearEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_ConsoleCountEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_ConsoleCountEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_ConsoleCountEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_ConsoleCountEiPmPNS0_7IsolateE:
.LFB18737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1155
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$105, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1156
	movq	88(%r12), %rax
.L1148:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1157
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1156:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1155:
	call	_ZN2v88internalL31Builtin_Impl_Stats_ConsoleCountEiPmPNS0_7IsolateE
	jmp	.L1148
.L1157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18737:
	.size	_ZN2v88internal20Builtin_ConsoleCountEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_ConsoleCountEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Builtin_ConsoleCountResetEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Builtin_ConsoleCountResetEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Builtin_ConsoleCountResetEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Builtin_ConsoleCountResetEiPmPNS0_7IsolateE:
.LFB18740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1165
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$113, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1166
	movq	88(%r12), %rax
.L1158:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1167
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1166:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1165:
	call	_ZN2v88internalL36Builtin_Impl_Stats_ConsoleCountResetEiPmPNS0_7IsolateE
	jmp	.L1158
.L1167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18740:
	.size	_ZN2v88internal25Builtin_ConsoleCountResetEiPmPNS0_7IsolateE, .-_ZN2v88internal25Builtin_ConsoleCountResetEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Builtin_ConsoleAssertEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Builtin_ConsoleAssertEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Builtin_ConsoleAssertEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Builtin_ConsoleAssertEiPmPNS0_7IsolateE:
.LFB18743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1175
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$121, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1176
	movq	88(%r12), %rax
.L1168:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1177
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1176:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1168
	.p2align 4,,10
	.p2align 3
.L1175:
	call	_ZN2v88internalL32Builtin_Impl_Stats_ConsoleAssertEiPmPNS0_7IsolateE
	jmp	.L1168
.L1177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18743:
	.size	_ZN2v88internal21Builtin_ConsoleAssertEiPmPNS0_7IsolateE, .-_ZN2v88internal21Builtin_ConsoleAssertEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Builtin_ConsoleProfileEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Builtin_ConsoleProfileEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Builtin_ConsoleProfileEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Builtin_ConsoleProfileEiPmPNS0_7IsolateE:
.LFB18746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1185
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$129, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1186
	movq	88(%r12), %rax
.L1178:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1187
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1186:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1185:
	call	_ZN2v88internalL33Builtin_Impl_Stats_ConsoleProfileEiPmPNS0_7IsolateE
	jmp	.L1178
.L1187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18746:
	.size	_ZN2v88internal22Builtin_ConsoleProfileEiPmPNS0_7IsolateE, .-_ZN2v88internal22Builtin_ConsoleProfileEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Builtin_ConsoleProfileEndEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Builtin_ConsoleProfileEndEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Builtin_ConsoleProfileEndEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Builtin_ConsoleProfileEndEiPmPNS0_7IsolateE:
.LFB18749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1195
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$137, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1196
	movq	88(%r12), %rax
.L1188:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1197
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1196:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1195:
	call	_ZN2v88internalL36Builtin_Impl_Stats_ConsoleProfileEndEiPmPNS0_7IsolateE
	jmp	.L1188
.L1197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18749:
	.size	_ZN2v88internal25Builtin_ConsoleProfileEndEiPmPNS0_7IsolateE, .-_ZN2v88internal25Builtin_ConsoleProfileEndEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Builtin_ConsoleTimeLogEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Builtin_ConsoleTimeLogEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Builtin_ConsoleTimeLogEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Builtin_ConsoleTimeLogEiPmPNS0_7IsolateE:
.LFB18752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1205
	movslq	%edi, %rdi
	movq	%rsi, -40(%rbp)
	movl	$153, %edx
	leaq	-48(%rbp), %rsi
	movq	%rdi, -48(%rbp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1206
	movq	88(%r12), %rax
.L1198:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1207
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1206:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1205:
	call	_ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeLogEiPmPNS0_7IsolateE
	jmp	.L1198
.L1207:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18752:
	.size	_ZN2v88internal22Builtin_ConsoleTimeLogEiPmPNS0_7IsolateE, .-_ZN2v88internal22Builtin_ConsoleTimeLogEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Builtin_ConsoleTimeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Builtin_ConsoleTimeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Builtin_ConsoleTimeEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Builtin_ConsoleTimeEiPmPNS0_7IsolateE:
.LFB18755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1233
	movslq	%edi, %rax
	movq	41016(%rdx), %rdi
	movq	%rsi, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L1218
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	cmpl	$5, %r13d
	jle	.L1214
	movq	-8(%r14), %rax
	testb	$1, %al
	jne	.L1234
.L1214:
	movq	41016(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L1235
.L1217:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1218
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1218:
	leaq	-80(%rbp), %rsi
	movl	$145, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1236
	movq	88(%r12), %rax
.L1208:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1237
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1236:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1235:
	leaq	.LC2(%rip), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1234:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1214
	leaq	-96(%rbp), %rdi
	xorl	%r8d, %r8d
	leaq	-88(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	41016(%r12), %r14
	movq	-96(%rbp), %r13
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L1238
.L1215:
	testq	%r13, %r13
	je	.L1217
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1233:
	call	_ZN2v88internalL30Builtin_Impl_Stats_ConsoleTimeEiPmPNS0_7IsolateE
	jmp	.L1208
.L1237:
	call	__stack_chk_fail@PLT
.L1238:
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L1215
	.cfi_endproc
.LFE18755:
	.size	_ZN2v88internal19Builtin_ConsoleTimeEiPmPNS0_7IsolateE, .-_ZN2v88internal19Builtin_ConsoleTimeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Builtin_ConsoleTimeEndEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Builtin_ConsoleTimeEndEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Builtin_ConsoleTimeEndEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Builtin_ConsoleTimeEndEiPmPNS0_7IsolateE:
.LFB18758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1264
	movslq	%edi, %rax
	movq	41016(%rdx), %rdi
	movq	%rsi, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L1249
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	cmpl	$5, %r13d
	jle	.L1245
	movq	-8(%r14), %rax
	testb	$1, %al
	jne	.L1265
.L1245:
	movq	41016(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L1266
.L1248:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1249
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1249:
	leaq	-80(%rbp), %rsi
	movl	$161, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1267
	movq	88(%r12), %rax
.L1239:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1268
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1267:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1266:
	leaq	.LC2(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1245
	leaq	-96(%rbp), %rdi
	xorl	%r8d, %r8d
	leaq	-88(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	41016(%r12), %r14
	movq	-96(%rbp), %r13
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L1269
.L1246:
	testq	%r13, %r13
	je	.L1248
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1264:
	call	_ZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeEndEiPmPNS0_7IsolateE
	jmp	.L1239
.L1268:
	call	__stack_chk_fail@PLT
.L1269:
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L1246
	.cfi_endproc
.LFE18758:
	.size	_ZN2v88internal22Builtin_ConsoleTimeEndEiPmPNS0_7IsolateE, .-_ZN2v88internal22Builtin_ConsoleTimeEndEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Builtin_ConsoleTimeStampEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Builtin_ConsoleTimeStampEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Builtin_ConsoleTimeStampEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Builtin_ConsoleTimeStampEiPmPNS0_7IsolateE:
.LFB18761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1295
	movslq	%edi, %rax
	movq	41016(%rdx), %rdi
	movq	%rsi, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L1280
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	cmpl	$5, %r13d
	jle	.L1276
	movq	-8(%r14), %rax
	testb	$1, %al
	jne	.L1296
.L1276:
	movq	41016(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L1297
.L1279:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1280
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1280:
	leaq	-80(%rbp), %rsi
	movl	$169, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ConsoleCallEPNS0_7IsolateERNS0_16BuiltinArgumentsEMNS_5debug15ConsoleDelegateEFvRKNS6_20ConsoleCallArgumentsERKNS6_14ConsoleContextEE.constprop.0
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1298
	movq	88(%r12), %rax
.L1270:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1299
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1298:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1297:
	leaq	.LC2(%rip), %rdx
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1276
	leaq	-96(%rbp), %rdi
	xorl	%r8d, %r8d
	leaq	-88(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	41016(%r12), %r14
	movq	-96(%rbp), %r13
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L1300
.L1277:
	testq	%r13, %r13
	je	.L1279
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1295:
	call	_ZN2v88internalL35Builtin_Impl_Stats_ConsoleTimeStampEiPmPNS0_7IsolateE
	jmp	.L1270
.L1299:
	call	__stack_chk_fail@PLT
.L1300:
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L1277
	.cfi_endproc
.LFE18761:
	.size	_ZN2v88internal24Builtin_ConsoleTimeStampEiPmPNS0_7IsolateE, .-_ZN2v88internal24Builtin_ConsoleTimeStampEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Builtin_ConsoleContextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Builtin_ConsoleContextEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Builtin_ConsoleContextEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Builtin_ConsoleContextEiPmPNS0_7IsolateE:
.LFB18765:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1305
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL27Builtin_Impl_ConsoleContextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1305:
	.cfi_restore 6
	jmp	_ZN2v88internalL33Builtin_Impl_Stats_ConsoleContextEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18765:
	.size	_ZN2v88internal22Builtin_ConsoleContextEiPmPNS0_7IsolateE, .-_ZN2v88internal22Builtin_ConsoleContextEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal20Builtin_ConsoleDebugEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal20Builtin_ConsoleDebugEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal20Builtin_ConsoleDebugEiPmPNS0_7IsolateE:
.LFB23003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23003:
	.size	_GLOBAL__sub_I__ZN2v88internal20Builtin_ConsoleDebugEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal20Builtin_ConsoleDebugEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal20Builtin_ConsoleDebugEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL33Builtin_Impl_Stats_ConsoleContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic153,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic153, @object
	.size	_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic153, 8
_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic153:
	.zero	8
	.section	.bss._ZZN2v88internalL35Builtin_Impl_Stats_ConsoleTimeStampEiPmPNS0_7IsolateEE28trace_event_unique_atomic119,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Builtin_Impl_Stats_ConsoleTimeStampEiPmPNS0_7IsolateEE28trace_event_unique_atomic119, @object
	.size	_ZZN2v88internalL35Builtin_Impl_Stats_ConsoleTimeStampEiPmPNS0_7IsolateEE28trace_event_unique_atomic119, 8
_ZZN2v88internalL35Builtin_Impl_Stats_ConsoleTimeStampEiPmPNS0_7IsolateEE28trace_event_unique_atomic119:
	.zero	8
	.section	.bss._ZZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic112,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic112, @object
	.size	_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic112, 8
_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic112:
	.zero	8
	.section	.bss._ZZN2v88internalL30Builtin_Impl_Stats_ConsoleTimeEiPmPNS0_7IsolateEE28trace_event_unique_atomic105,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Builtin_Impl_Stats_ConsoleTimeEiPmPNS0_7IsolateEE28trace_event_unique_atomic105, @object
	.size	_ZZN2v88internalL30Builtin_Impl_Stats_ConsoleTimeEiPmPNS0_7IsolateEE28trace_event_unique_atomic105, 8
_ZZN2v88internalL30Builtin_Impl_Stats_ConsoleTimeEiPmPNS0_7IsolateEE28trace_event_unique_atomic105:
	.zero	8
	.section	.bss._ZZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeLogEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeLogEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeLogEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleTimeLogEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL36Builtin_Impl_Stats_ConsoleProfileEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Builtin_Impl_Stats_ConsoleProfileEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL36Builtin_Impl_Stats_ConsoleProfileEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL36Builtin_Impl_Stats_ConsoleProfileEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL33Builtin_Impl_Stats_ConsoleProfileEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleProfileEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleProfileEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL33Builtin_Impl_Stats_ConsoleProfileEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL32Builtin_Impl_Stats_ConsoleAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Builtin_Impl_Stats_ConsoleAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL32Builtin_Impl_Stats_ConsoleAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL32Builtin_Impl_Stats_ConsoleAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL36Builtin_Impl_Stats_ConsoleCountResetEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Builtin_Impl_Stats_ConsoleCountResetEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL36Builtin_Impl_Stats_ConsoleCountResetEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL36Builtin_Impl_Stats_ConsoleCountResetEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_ConsoleCountEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleCountEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleCountEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleCountEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_ConsoleClearEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleClearEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleClearEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleClearEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL34Builtin_Impl_Stats_ConsoleGroupEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Builtin_Impl_Stats_ConsoleGroupEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL34Builtin_Impl_Stats_ConsoleGroupEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL34Builtin_Impl_Stats_ConsoleGroupEndEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL40Builtin_Impl_Stats_ConsoleGroupCollapsedEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Builtin_Impl_Stats_ConsoleGroupCollapsedEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL40Builtin_Impl_Stats_ConsoleGroupCollapsedEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL40Builtin_Impl_Stats_ConsoleGroupCollapsedEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_ConsoleGroupEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleGroupEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleGroupEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleGroupEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_ConsoleTraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleTraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleTraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleTraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_ConsoleTableEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleTableEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleTableEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleTableEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL32Builtin_Impl_Stats_ConsoleDirXmlEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Builtin_Impl_Stats_ConsoleDirXmlEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL32Builtin_Impl_Stats_ConsoleDirXmlEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL32Builtin_Impl_Stats_ConsoleDirXmlEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL29Builtin_Impl_Stats_ConsoleDirEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Builtin_Impl_Stats_ConsoleDirEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL29Builtin_Impl_Stats_ConsoleDirEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL29Builtin_Impl_Stats_ConsoleDirEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL30Builtin_Impl_Stats_ConsoleWarnEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Builtin_Impl_Stats_ConsoleWarnEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL30Builtin_Impl_Stats_ConsoleWarnEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL30Builtin_Impl_Stats_ConsoleWarnEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL29Builtin_Impl_Stats_ConsoleLogEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Builtin_Impl_Stats_ConsoleLogEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL29Builtin_Impl_Stats_ConsoleLogEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL29Builtin_Impl_Stats_ConsoleLogEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL30Builtin_Impl_Stats_ConsoleInfoEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Builtin_Impl_Stats_ConsoleInfoEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL30Builtin_Impl_Stats_ConsoleInfoEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL30Builtin_Impl_Stats_ConsoleInfoEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_ConsoleErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL31Builtin_Impl_Stats_ConsoleDebugEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
