	.file	"constants-table-builder.cc"
	.text
	.section	.text._ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv,"axG",@progbits,_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv
	.type	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv, @function
_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv:
.LFB24404:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE24404:
	.size	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv, .-_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv
	.section	.text._ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm,"axG",@progbits,_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm
	.type	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm, @function
_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm:
.LFB24403:
	.cfi_startproc
	endbr64
	leaq	0(,%rsi,8), %rdi
	jmp	_ZN2v88internal8MallocednwEm@PLT
	.cfi_endproc
.LFE24403:
	.size	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm, .-_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm
	.section	.text._ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev,"axG",@progbits,_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev
	.type	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev, @function
_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev:
.LFB21591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	.cfi_endproc
.LFE21591:
	.size	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev, .-_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev
	.weak	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED1Ev
	.set	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED1Ev,_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev
	.section	.text._ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev,"axG",@progbits,_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev
	.type	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev, @function
_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev:
.LFB21593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21593:
	.size	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev, .-_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev
	.section	.text._ZN2v88internal29BuiltinsConstantsTableBuilderC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29BuiltinsConstantsTableBuilderC2EPNS0_7IsolateE
	.type	_ZN2v88internal29BuiltinsConstantsTableBuilderC2EPNS0_7IsolateE, @function
_ZN2v88internal29BuiltinsConstantsTableBuilderC2EPNS0_7IsolateE:
.LFB19504:
	.cfi_startproc
	endbr64
	movdqa	.LC0(%rip), %xmm0
	movq	%rsi, (%rdi)
	leaq	16+_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE(%rip), %rax
	addq	$37592, %rsi
	movq	%rsi, 24(%rdi)
	movups	%xmm0, 32(%rdi)
	pxor	%xmm0, %xmm0
	movb	$0, 64(%rdi)
	movq	%rax, 8(%rdi)
	movups	%xmm0, 48(%rdi)
	ret
	.cfi_endproc
.LFE19504:
	.size	_ZN2v88internal29BuiltinsConstantsTableBuilderC2EPNS0_7IsolateE, .-_ZN2v88internal29BuiltinsConstantsTableBuilderC2EPNS0_7IsolateE
	.globl	_ZN2v88internal29BuiltinsConstantsTableBuilderC1EPNS0_7IsolateE
	.set	_ZN2v88internal29BuiltinsConstantsTableBuilderC1EPNS0_7IsolateE,_ZN2v88internal29BuiltinsConstantsTableBuilderC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal29BuiltinsConstantsTableBuilder9AddObjectENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29BuiltinsConstantsTableBuilder9AddObjectENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal29BuiltinsConstantsTableBuilder9AddObjectENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal29BuiltinsConstantsTableBuilder9AddObjectENS0_6HandleINS0_6ObjectEEE:
.LFB19506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rsi
	call	_ZNK2v88internal15IdentityMapBase9FindEntryEm@PLT
	testq	%rax, %rax
	je	.L13
	movl	(%rax), %r12d
	addq	$8, %rsp
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movl	36(%r12), %r12d
	movq	%r13, %rdi
	call	_ZN2v88internal15IdentityMapBase8GetEntryEm@PLT
	movl	%r12d, (%rax)
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19506:
	.size	_ZN2v88internal29BuiltinsConstantsTableBuilder9AddObjectENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal29BuiltinsConstantsTableBuilder9AddObjectENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal29BuiltinsConstantsTableBuilder18PatchSelfReferenceENS0_6HandleINS0_6ObjectEEENS2_INS0_4CodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29BuiltinsConstantsTableBuilder18PatchSelfReferenceENS0_6HandleINS0_6ObjectEEENS2_INS0_4CodeEEE
	.type	_ZN2v88internal29BuiltinsConstantsTableBuilder18PatchSelfReferenceENS0_6HandleINS0_6ObjectEEENS2_INS0_4CodeEEE, @function
_ZN2v88internal29BuiltinsConstantsTableBuilder18PatchSelfReferenceENS0_6HandleINS0_6ObjectEEENS2_INS0_4CodeEEE:
.LFB19507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	8(%rdi), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	leaq	-48(%rbp), %rdx
	subq	$24, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	_ZN2v88internal15IdentityMapBase11DeleteEntryEmPPv@PLT
	testb	%al, %al
	je	.L14
	movq	(%rbx), %rsi
	movl	-48(%rbp), %r13d
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBase8GetEntryEm@PLT
	movl	%r13d, (%rax)
.L14:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L22
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19507:
	.size	_ZN2v88internal29BuiltinsConstantsTableBuilder18PatchSelfReferenceENS0_6HandleINS0_6ObjectEEENS2_INS0_4CodeEEE, .-_ZN2v88internal29BuiltinsConstantsTableBuilder18PatchSelfReferenceENS0_6HandleINS0_6ObjectEEENS2_INS0_4CodeEEE
	.section	.rodata._ZN2v88internal29BuiltinsConstantsTableBuilder8FinalizeEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"!map_->is_iterable()"
.LC2:
	.string	"Check failed: %s."
.LC3:
	.string	"map_->is_iterable()"
	.section	.text._ZN2v88internal29BuiltinsConstantsTableBuilder8FinalizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29BuiltinsConstantsTableBuilder8FinalizeEv
	.type	_ZN2v88internal29BuiltinsConstantsTableBuilder8FinalizeEv, @function
_ZN2v88internal29BuiltinsConstantsTableBuilder8FinalizeEv:
.LFB19508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	41088(%r13), %rax
	movq	%rax, -96(%rbp)
	movq	41096(%r13), %rax
	movq	%rax, -104(%rbp)
	movl	41104(%r13), %eax
	leal	1(%rax), %edx
	movl	%edx, 41104(%r13)
	movl	36(%rdi), %esi
	testl	%esi, %esi
	je	.L50
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movl	$1, %edx
	leaq	8(%rbx), %r12
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	addq	$41184, %rax
	cmpb	$0, 64(%rbx)
	movq	%rax, -80(%rbp)
	jne	.L51
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBase15EnableIterationEv@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal15IdentityMapBase9NextIndexEi@PLT
	cmpl	40(%rbx), %eax
	je	.L35
	movq	%r13, -112(%rbp)
	movq	-88(%rbp), %r14
	movl	%eax, %r13d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L30:
	movq	(%r14), %rax
	movq	%rdx, -1(%r15,%rax)
.L37:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal15IdentityMapBase9NextIndexEi@PLT
	movl	%eax, %r13d
	cmpl	%eax, 40(%rbx)
	je	.L52
.L27:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal15IdentityMapBase12EntryAtIndexEi@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	(%rax), %r15d
	call	_ZNK2v88internal15IdentityMapBase10KeyAtIndexEi@PLT
	movq	%rax, %rdx
	leal	16(,%r15,8), %eax
	movslq	%eax, %r15
	testb	$1, %dl
	je	.L30
	movq	-1(%rdx), %rax
	cmpw	$69, 11(%rax)
	je	.L53
.L31:
	movq	(%r14), %rdi
	leaq	-1(%r15,%rdi), %r15
	movq	%rdx, (%r15)
.L38:
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L33
	movq	%r15, %rsi
	movq	%rdx, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
.L33:
	testb	$24, %al
	je	.L37
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L37
	movq	%r15, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L52:
	movq	-112(%rbp), %r13
.L35:
	movq	(%rbx), %rax
	leaq	37592(%rax), %rdi
	movq	-88(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal4Heap25SetBuiltinsConstantsTableENS0_10FixedArrayE@PLT
	cmpb	$0, 64(%rbx)
	je	.L54
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBase16DisableIterationEv@PLT
	movq	-96(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-104(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L23
	movq	%rax, 41096(%r13)
	addq	$72, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movl	%eax, 41104(%r13)
.L23:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movl	43(%rdx), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$3, %eax
	jne	.L31
	movl	59(%rdx), %esi
	movq	-80(%rbp), %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	(%r14), %rdi
	movq	%rax, %rdx
	leaq	-1(%r15,%rdi), %r15
	movq	%rax, (%r15)
	testb	$1, %al
	jne	.L38
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L54:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19508:
	.size	_ZN2v88internal29BuiltinsConstantsTableBuilder8FinalizeEv, .-_ZN2v88internal29BuiltinsConstantsTableBuilder8FinalizeEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal29BuiltinsConstantsTableBuilderC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal29BuiltinsConstantsTableBuilderC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal29BuiltinsConstantsTableBuilderC2EPNS0_7IsolateE:
.LFB24405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24405:
	.size	_GLOBAL__sub_I__ZN2v88internal29BuiltinsConstantsTableBuilderC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal29BuiltinsConstantsTableBuilderC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal29BuiltinsConstantsTableBuilderC2EPNS0_7IsolateE
	.weak	_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE
	.section	.data.rel.ro.local._ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE,"awG",@progbits,_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE,comdat
	.align 8
	.type	_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE, @object
	.size	_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE, 48
_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED1Ev
	.quad	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev
	.quad	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm
	.quad	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	-1
	.long	0
	.long	0
	.long	0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
