	.file	"field-type.cc"
	.text
	.section	.text._ZN2v88internal9FieldType4NoneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9FieldType4NoneEv
	.type	_ZN2v88internal9FieldType4NoneEv, @function
_ZN2v88internal9FieldType4NoneEv:
.LFB17811:
	.cfi_startproc
	endbr64
	movabsq	$8589934592, %rax
	ret
	.cfi_endproc
.LFE17811:
	.size	_ZN2v88internal9FieldType4NoneEv, .-_ZN2v88internal9FieldType4NoneEv
	.section	.text._ZN2v88internal9FieldType3AnyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9FieldType3AnyEv
	.type	_ZN2v88internal9FieldType3AnyEv, @function
_ZN2v88internal9FieldType3AnyEv:
.LFB17812:
	.cfi_startproc
	endbr64
	movabsq	$4294967296, %rax
	ret
	.cfi_endproc
.LFE17812:
	.size	_ZN2v88internal9FieldType3AnyEv, .-_ZN2v88internal9FieldType3AnyEv
	.section	.text._ZN2v88internal9FieldType4NoneEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9FieldType4NoneEPNS0_7IsolateE
	.type	_ZN2v88internal9FieldType4NoneEPNS0_7IsolateE, @function
_ZN2v88internal9FieldType4NoneEPNS0_7IsolateE:
.LFB17813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	movabsq	$8589934592, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L9
.L7:
	movabsq	$8589934592, %rcx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rcx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L7
	.cfi_endproc
.LFE17813:
	.size	_ZN2v88internal9FieldType4NoneEPNS0_7IsolateE, .-_ZN2v88internal9FieldType4NoneEPNS0_7IsolateE
	.section	.text._ZN2v88internal9FieldType3AnyEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE
	.type	_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE, @function
_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE:
.LFB17814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L11
	movabsq	$4294967296, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L15
.L13:
	movabsq	$4294967296, %rcx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rcx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L13
	.cfi_endproc
.LFE17814:
	.size	_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE, .-_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE
	.section	.text._ZN2v88internal9FieldType5ClassENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9FieldType5ClassENS0_3MapE
	.type	_ZN2v88internal9FieldType5ClassENS0_3MapE, @function
_ZN2v88internal9FieldType5ClassENS0_3MapE:
.LFB17815:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE17815:
	.size	_ZN2v88internal9FieldType5ClassENS0_3MapE, .-_ZN2v88internal9FieldType5ClassENS0_3MapE
	.section	.text._ZN2v88internal9FieldType5ClassENS0_6HandleINS0_3MapEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9FieldType5ClassENS0_6HandleINS0_3MapEEEPNS0_7IsolateE
	.type	_ZN2v88internal9FieldType5ClassENS0_6HandleINS0_3MapEEEPNS0_7IsolateE, @function
_ZN2v88internal9FieldType5ClassENS0_6HandleINS0_3MapEEEPNS0_7IsolateE:
.LFB17816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L22
.L20:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L20
	.cfi_endproc
.LFE17816:
	.size	_ZN2v88internal9FieldType5ClassENS0_6HandleINS0_3MapEEEPNS0_7IsolateE, .-_ZN2v88internal9FieldType5ClassENS0_6HandleINS0_3MapEEEPNS0_7IsolateE
	.section	.text._ZN2v88internal9FieldType4castENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9FieldType4castENS0_6ObjectE
	.type	_ZN2v88internal9FieldType4castENS0_6ObjectE, @function
_ZN2v88internal9FieldType4castENS0_6ObjectE:
.LFB17817:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE17817:
	.size	_ZN2v88internal9FieldType4castENS0_6ObjectE, .-_ZN2v88internal9FieldType4castENS0_6ObjectE
	.section	.text._ZNK2v88internal9FieldType7IsClassEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9FieldType7IsClassEv
	.type	_ZNK2v88internal9FieldType7IsClassEv, @function
_ZNK2v88internal9FieldType7IsClassEv:
.LFB17818:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	testb	$1, %dl
	jne	.L27
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movq	-1(%rdx), %rax
	cmpw	$68, 11(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE17818:
	.size	_ZNK2v88internal9FieldType7IsClassEv, .-_ZNK2v88internal9FieldType7IsClassEv
	.section	.text._ZNK2v88internal9FieldType7AsClassEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9FieldType7AsClassEv
	.type	_ZNK2v88internal9FieldType7AsClassEv, @function
_ZNK2v88internal9FieldType7AsClassEv:
.LFB17819:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE17819:
	.size	_ZNK2v88internal9FieldType7AsClassEv, .-_ZNK2v88internal9FieldType7AsClassEv
	.section	.text._ZNK2v88internal9FieldType9NowStableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9FieldType9NowStableEv
	.type	_ZNK2v88internal9FieldType9NowStableEv, @function
_ZNK2v88internal9FieldType9NowStableEv:
.LFB17820:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testb	$1, %al
	jne	.L30
.L32:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L32
	movl	15(%rax), %eax
	shrl	$25, %eax
	xorl	$1, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE17820:
	.size	_ZNK2v88internal9FieldType9NowStableEv, .-_ZNK2v88internal9FieldType9NowStableEv
	.section	.text._ZNK2v88internal9FieldType5NowIsES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9FieldType5NowIsES1_
	.type	_ZNK2v88internal9FieldType5NowIsES1_, @function
_ZNK2v88internal9FieldType5NowIsES1_:
.LFB17821:
	.cfi_startproc
	endbr64
	movabsq	$4294967296, %rdx
	movl	$1, %eax
	cmpq	%rdx, %rsi
	je	.L33
	movq	(%rdi), %rcx
	movabsq	$8589934592, %rdi
	cmpq	%rdi, %rcx
	je	.L33
	cmpq	%rdi, %rsi
	je	.L37
	cmpq	%rdx, %rcx
	je	.L37
	cmpq	%rcx, %rsi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%eax, %eax
.L33:
	ret
	.cfi_endproc
.LFE17821:
	.size	_ZNK2v88internal9FieldType5NowIsES1_, .-_ZNK2v88internal9FieldType5NowIsES1_
	.section	.text._ZNK2v88internal9FieldType5NowIsENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9FieldType5NowIsENS0_6HandleIS1_EE
	.type	_ZNK2v88internal9FieldType5NowIsENS0_6HandleIS1_EE, @function
_ZNK2v88internal9FieldType5NowIsENS0_6HandleIS1_EE:
.LFB17822:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	$1, %r8d
	movabsq	$4294967296, %rdx
	cmpq	%rdx, %rax
	je	.L38
	movabsq	$8589934592, %rsi
	movq	(%rdi), %rcx
	cmpq	%rsi, %rcx
	je	.L38
	cmpq	%rsi, %rax
	je	.L42
	cmpq	%rdx, %rcx
	je	.L42
	cmpq	%rcx, %rax
	sete	%r8b
.L38:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE17822:
	.size	_ZNK2v88internal9FieldType5NowIsENS0_6HandleIS1_EE, .-_ZNK2v88internal9FieldType5NowIsENS0_6HandleIS1_EE
	.section	.rodata._ZNK2v88internal9FieldType7PrintToERSo.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Any"
.LC1:
	.string	"None"
.LC2:
	.string	"Class("
.LC3:
	.string	")"
	.section	.text._ZNK2v88internal9FieldType7PrintToERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9FieldType7PrintToERSo
	.type	_ZNK2v88internal9FieldType7PrintToERSo, @function
_ZNK2v88internal9FieldType7PrintToERSo:
.LFB17823:
	.cfi_startproc
	endbr64
	movabsq	$4294967296, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	cmpq	%rdx, %rax
	je	.L48
	movabsq	$8589934592, %rdx
	cmpq	%rdx, %rax
	je	.L49
	movq	%rdi, %rbx
	movl	$6, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	popq	%rbx
	popq	%r12
	movl	$1, %edx
	movq	%rax, %rdi
	leaq	.LC3(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	$4, %edx
	popq	%r12
	leaq	.LC1(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	$3, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.cfi_endproc
.LFE17823:
	.size	_ZNK2v88internal9FieldType7PrintToERSo, .-_ZNK2v88internal9FieldType7PrintToERSo
	.section	.text._ZNK2v88internal9FieldType11NowContainsENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9FieldType11NowContainsENS0_6ObjectE
	.type	_ZNK2v88internal9FieldType11NowContainsENS0_6ObjectE, @function
_ZNK2v88internal9FieldType11NowContainsENS0_6ObjectE:
.LFB17824:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	$1, %eax
	movabsq	$4294967296, %rcx
	cmpq	%rcx, %rdx
	je	.L50
	movabsq	$8589934592, %rcx
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	je	.L50
	testb	$1, %sil
	jne	.L55
.L50:
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movq	-1(%rsi), %rax
	cmpq	%rax, %rdx
	sete	%al
	ret
	.cfi_endproc
.LFE17824:
	.size	_ZNK2v88internal9FieldType11NowContainsENS0_6ObjectE, .-_ZNK2v88internal9FieldType11NowContainsENS0_6ObjectE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal9FieldType4NoneEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal9FieldType4NoneEv, @function
_GLOBAL__sub_I__ZN2v88internal9FieldType4NoneEv:
.LFB21509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21509:
	.size	_GLOBAL__sub_I__ZN2v88internal9FieldType4NoneEv, .-_GLOBAL__sub_I__ZN2v88internal9FieldType4NoneEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal9FieldType4NoneEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
