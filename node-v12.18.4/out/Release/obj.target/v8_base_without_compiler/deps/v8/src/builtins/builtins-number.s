	.file	"builtins-number.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internalL42Builtin_Impl_NumberPrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Number.prototype.toLocaleString"
	.section	.rodata._ZN2v88internalL42Builtin_Impl_NumberPrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"(location_) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL42Builtin_Impl_NumberPrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_NumberPrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_NumberPrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21152:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$65, %esi
	subq	$56, %rsp
	movq	41096(%rdx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	(%rbx), %rsi
	testb	$1, %sil
	jne	.L25
.L7:
	cmpl	$6, %r13d
	jle	.L26
	leaq	-16(%rbx), %rcx
.L17:
	leaq	-8(%rbx), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl20NumberToLocaleStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_@PLT
	testq	%rax, %rax
	je	.L27
.L18:
	movq	(%rax), %r13
.L14:
	subl	$1, 41104(%r12)
	movq	-88(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L20
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L20:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	leaq	88(%r12), %rdx
	movq	%rdx, %rcx
	je	.L17
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl20NumberToLocaleStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_@PLT
	testq	%rax, %rax
	jne	.L18
.L27:
	movq	312(%r12), %r13
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L25:
	movq	-1(%rsi), %rax
	cmpw	$1041, 11(%rax)
	je	.L29
.L9:
	testb	$1, %sil
	je	.L7
	movq	-1(%rsi), %rax
	cmpw	$65, 11(%rax)
	je	.L7
	leaq	.LC0(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	2976(%r12), %r13
	movq	$31, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L30
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$92, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L29:
	movq	41112(%r12), %rdi
	movq	23(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L10
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L31
.L12:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L9
.L31:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21152:
	.size	_ZN2v88internalL42Builtin_Impl_NumberPrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_NumberPrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_NumberPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"Number.prototype.toString"
	.section	.text._ZN2v88internalL36Builtin_Impl_NumberPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_NumberPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_NumberPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21158:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	-8(%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rsi
	leaq	88(%rdx), %rax
	cmpl	$5, %edi
	cmovle	%rax, %r15
	testb	$1, %sil
	jne	.L103
.L81:
	sarq	$32, %rsi
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
.L46:
	movq	(%r15), %rax
	cmpq	%rax, 88(%r12)
	je	.L101
	testb	$1, %al
	jne	.L104
.L49:
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L52:
	ucomisd	.LC5(%rip), %xmm1
	jnp	.L105
.L53:
	movsd	.LC6(%rip), %xmm2
	comisd	%xmm1, %xmm2
	ja	.L55
	comisd	.LC7(%rip), %xmm1
	jbe	.L94
.L55:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$218, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L102:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L44:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L78
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L78:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	jne	.L53
.L101:
	movq	%r14, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%rax), %r14
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L103:
	movq	-1(%rsi), %rdx
	movq	%rsi, %rax
	notq	%rax
	cmpw	$1041, 11(%rdx)
	je	.L107
.L38:
	testb	$1, %al
	jne	.L81
	movq	-1(%rsi), %rax
	cmpw	$65, 11(%rax)
	jne	.L108
	movq	(%r14), %rsi
	testb	$1, %sil
	je	.L81
	movsd	7(%rsi), %xmm0
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movsd	%xmm0, -104(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movsd	-104(%rbp), %xmm0
	testq	%rax, %rax
	jne	.L50
	movq	312(%r12), %r14
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L94:
	movabsq	$-9223372036854775808, %rax
	movq	%xmm0, %rcx
	cmpq	%rax, %rcx
	je	.L58
	pxor	%xmm2, %xmm2
	comisd	%xmm2, %xmm0
	jb	.L59
	movsd	.LC9(%rip), %xmm3
	comisd	%xmm0, %xmm3
	jb	.L59
	comisd	%xmm0, %xmm2
	ja	.L109
	movsd	.LC11(%rip), %xmm3
	comisd	%xmm0, %xmm3
	jbe	.L96
	addsd	%xmm0, %xmm3
	movd	%xmm3, %eax
	pxor	%xmm3, %xmm3
	cvtsi2sdq	%rax, %xmm3
.L65:
	ucomisd	%xmm3, %xmm0
	jp	.L59
	jne	.L59
	comisd	%xmm0, %xmm1
	jbe	.L59
	.p2align 4,,10
	.p2align 3
.L58:
	cvttsd2siq	%xmm0, %rax
	leaq	_ZZN2v88internalL36Builtin_Impl_NumberPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateEE10kCharTable(%rip), %rdx
	movq	%r12, %rdi
	movl	%eax, %eax
	movsbw	(%rdx,%rax), %si
	movzwl	%si, %esi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	movq	(%rax), %r14
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L59:
	ucomisd	%xmm0, %xmm0
	jp	.L110
	movapd	%xmm0, %xmm3
	andpd	.LC12(%rip), %xmm3
	ucomisd	.LC13(%rip), %xmm3
	jbe	.L72
	comisd	%xmm0, %xmm2
	ja	.L111
	movq	2696(%r12), %r14
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L50:
	movq	(%rax), %rax
	testb	$1, %al
	je	.L49
	movsd	7(%rax), %xmm1
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	.LC4(%rip), %rax
	xorl	%edx, %edx
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	leaq	2976(%r12), %r14
	movq	$25, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L76
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$92, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L107:
	movq	41112(%r12), %rdi
	movq	23(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L39
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
	movq	%rsi, %rax
	notq	%rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r13, %r14
	cmpq	%r13, 41096(%r12)
	je	.L112
.L41:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, %rax
	movq	%rsi, (%r14)
	notq	%rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L72:
	cvttsd2sil	%xmm1, %edi
	call	_ZN2v88internal20DoubleToRadixCStringEdi@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r14, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L76
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movq	(%r15), %r14
	jmp	.L44
.L112:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	movq	2848(%r12), %r14
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L96:
	movsd	.LC3(%rip), %xmm3
	jmp	.L65
.L109:
	comisd	.LC10(%rip), %xmm0
	jbe	.L96
	movsd	.LC11(%rip), %xmm3
	subsd	%xmm0, %xmm3
	movq	%xmm3, %rax
	pxor	%xmm3, %xmm3
	negl	%eax
	movl	%eax, %eax
	cvtsi2sdq	%rax, %xmm3
	jmp	.L65
.L110:
	movq	2880(%r12), %r14
	jmp	.L44
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21158:
	.size	_ZN2v88internalL36Builtin_Impl_NumberPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_NumberPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_NumberPrototypeToExponentialENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"Number.prototype.toExponential"
	.section	.rodata._ZN2v88internalL41Builtin_Impl_NumberPrototypeToExponentialENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"toExponential()"
	.section	.text._ZN2v88internalL41Builtin_Impl_NumberPrototypeToExponentialENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_NumberPrototypeToExponentialENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_NumberPrototypeToExponentialENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21146:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	-8(%rsi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %r13
	leaq	88(%rdx), %rax
	cmpl	$5, %edi
	cmovle	%rax, %r15
	testb	$1, %r13b
	jne	.L160
.L145:
	movq	(%r15), %rax
	sarq	$32, %r13
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	testb	$1, %al
	jne	.L161
.L129:
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L132:
	ucomisd	%xmm0, %xmm0
	jp	.L162
	movapd	%xmm0, %xmm2
	andpd	.LC12(%rip), %xmm2
	ucomisd	.LC13(%rip), %xmm2
	ja	.L163
	pxor	%xmm2, %xmm2
	comisd	%xmm1, %xmm2
	ja	.L138
	comisd	.LC15(%rip), %xmm1
	ja	.L138
	movq	(%r15), %rax
	movl	$-1, %edi
	cmpq	%rax, 88(%r12)
	je	.L142
	cvttsd2sil	%xmm1, %edi
.L142:
	call	_ZN2v88internal26DoubleToExponentialCStringEdi@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L141
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	movq	(%r15), %r13
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L163:
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L164
	movq	2696(%r12), %r13
	.p2align 4,,10
	.p2align 3
.L125:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L144
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L144:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movq	-1(%r13), %rdx
	movq	%r13, %rax
	notq	%rax
	cmpw	$1041, 11(%rdx)
	je	.L166
.L119:
	testb	$1, %al
	jne	.L145
	movq	-1(%r13), %rax
	cmpw	$65, 11(%rax)
	jne	.L167
	movq	(%rsi), %r13
	testb	$1, %r13b
	je	.L145
	movq	(%r15), %rax
	movsd	7(%r13), %xmm0
	testb	$1, %al
	je	.L129
.L161:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movsd	%xmm0, -120(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movsd	-120(%rbp), %xmm0
	testq	%rax, %rax
	jne	.L130
	movq	312(%r12), %r13
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L130:
	movq	(%rax), %rax
	testb	$1, %al
	je	.L129
	movsd	7(%rax), %xmm1
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L164:
	movq	2848(%r12), %r13
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L138:
	leaq	.LC16(%rip), %rax
	xorl	%edx, %edx
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	$15, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L141
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$212, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L159:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L167:
	leaq	.LC14(%rip), %rax
	xorl	%edx, %edx
	leaq	-112(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	leaq	2976(%r12), %r13
	movq	$30, -104(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L141
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$92, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L166:
	movq	41112(%r12), %rdi
	movq	23(%r13), %r13
	testq	%rdi, %rdi
	je	.L120
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %rsi
	movq	%r13, %rax
	notq	%rax
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L162:
	movq	2880(%r12), %r13
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L120:
	movq	%r14, %rsi
	cmpq	%r14, 41096(%r12)
	je	.L168
.L122:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, %rax
	movq	%r13, (%rsi)
	notq	%rax
	jmp	.L119
.L168:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L141:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21146:
	.size	_ZN2v88internalL41Builtin_Impl_NumberPrototypeToExponentialENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_NumberPrototypeToExponentialENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Builtin_Impl_NumberPrototypeToFixedENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"Number.prototype.toFixed"
.LC18:
	.string	"toFixed() digits"
	.section	.text._ZN2v88internalL35Builtin_Impl_NumberPrototypeToFixedENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Builtin_Impl_NumberPrototypeToFixedENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL35Builtin_Impl_NumberPrototypeToFixedENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21149:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	-8(%rsi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %r13
	leaq	88(%rdx), %rax
	cmpl	$5, %edi
	cmovle	%rax, %r15
	testb	$1, %r13b
	jne	.L214
.L200:
	movq	(%r15), %rax
	sarq	$32, %r13
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	testb	$1, %al
	jne	.L215
.L185:
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L188:
	pxor	%xmm2, %xmm2
	comisd	%xmm1, %xmm2
	ja	.L189
	comisd	.LC15(%rip), %xmm1
	jbe	.L209
.L189:
	leaq	.LC18(%rip), %rax
	xorl	%edx, %edx
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	$16, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L192
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$212, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L213:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L181:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L199
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L199:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	ucomisd	%xmm0, %xmm0
	jp	.L217
	movapd	%xmm0, %xmm3
	andpd	.LC12(%rip), %xmm3
	ucomisd	.LC13(%rip), %xmm3
	jbe	.L194
	comisd	%xmm0, %xmm2
	ja	.L218
	movq	2696(%r12), %r13
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L214:
	movq	-1(%r13), %rdx
	movq	%r13, %rax
	notq	%rax
	cmpw	$1041, 11(%rdx)
	je	.L219
.L175:
	testb	$1, %al
	jne	.L200
	movq	-1(%r13), %rax
	cmpw	$65, 11(%rax)
	jne	.L220
	movq	(%rsi), %r13
	testb	$1, %r13b
	je	.L200
	movq	(%r15), %rax
	movsd	7(%r13), %xmm0
	testb	$1, %al
	je	.L185
.L215:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movsd	%xmm0, -120(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movsd	-120(%rbp), %xmm0
	testq	%rax, %rax
	jne	.L186
	movq	312(%r12), %r13
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L186:
	movq	(%rax), %rax
	testb	$1, %al
	je	.L185
	movsd	7(%rax), %xmm1
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L194:
	cvttsd2sil	%xmm1, %edi
	call	_ZN2v88internal20DoubleToFixedCStringEdi@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L192
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	movq	(%r15), %r13
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L220:
	leaq	.LC17(%rip), %rax
	xorl	%edx, %edx
	leaq	-112(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	leaq	2976(%r12), %r13
	movq	$24, -104(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L192
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$92, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L219:
	movq	41112(%r12), %rdi
	movq	23(%r13), %r13
	testq	%rdi, %rdi
	je	.L176
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %rsi
	movq	%r13, %rax
	notq	%rax
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L192:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L218:
	movq	2848(%r12), %r13
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%r14, %rsi
	cmpq	41096(%r12), %r14
	je	.L221
.L178:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, %rax
	movq	%r13, (%rsi)
	notq	%rax
	jmp	.L175
.L221:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L217:
	movq	2880(%r12), %r13
	jmp	.L181
.L216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21149:
	.size	_ZN2v88internalL35Builtin_Impl_NumberPrototypeToFixedENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL35Builtin_Impl_NumberPrototypeToFixedENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_NumberPrototypeToPrecisionENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"Number.prototype.toPrecision"
	.section	.text._ZN2v88internalL39Builtin_Impl_NumberPrototypeToPrecisionENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_NumberPrototypeToPrecisionENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_NumberPrototypeToPrecisionENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21155:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	-8(%rsi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %r13
	leaq	88(%rdx), %rax
	cmpl	$5, %edi
	cmovle	%rax, %r15
	testb	$1, %r13b
	jne	.L265
.L254:
	sarq	$32, %r13
	pxor	%xmm0, %xmm0
	movq	(%r15), %rax
	cvtsi2sdl	%r13d, %xmm0
	cmpq	%rax, 88(%r12)
	je	.L266
.L237:
	testb	$1, %al
	jne	.L267
.L239:
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L242:
	ucomisd	%xmm0, %xmm0
	jp	.L268
	movapd	%xmm0, %xmm2
	andpd	.LC12(%rip), %xmm2
	ucomisd	.LC13(%rip), %xmm2
	jbe	.L244
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L269
	movq	2696(%r12), %r13
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L265:
	movq	-1(%r13), %rdx
	movq	%r13, %rax
	notq	%rax
	cmpw	$1041, 11(%rdx)
	je	.L270
.L228:
	testb	$1, %al
	jne	.L254
	movq	-1(%r13), %rax
	cmpw	$65, 11(%rax)
	jne	.L271
	movq	(%rsi), %r13
	testb	$1, %r13b
	je	.L254
	movsd	7(%r13), %xmm0
	movq	(%r15), %rax
	cmpq	%rax, 88(%r12)
	jne	.L237
.L266:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%rax), %r13
.L234:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L253
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L253:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L272
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movsd	.LC20(%rip), %xmm2
	comisd	%xmm1, %xmm2
	ja	.L248
	comisd	.LC15(%rip), %xmm1
	jbe	.L261
.L248:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$217, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L264:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movsd	%xmm0, -104(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movsd	-104(%rbp), %xmm0
	testq	%rax, %rax
	jne	.L240
	movq	312(%r12), %r13
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L269:
	movq	2848(%r12), %r13
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L240:
	movq	(%rax), %rax
	testb	$1, %al
	je	.L239
	movsd	7(%rax), %xmm1
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L271:
	leaq	.LC19(%rip), %rax
	xorl	%edx, %edx
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	leaq	2976(%r12), %r13
	movq	$28, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L251
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$92, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L270:
	movq	41112(%r12), %rdi
	movq	23(%r13), %r13
	testq	%rdi, %rdi
	je	.L229
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %rsi
	movq	%r13, %rax
	notq	%rax
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%rbx, %rsi
	cmpq	%rbx, 41096(%r12)
	je	.L273
.L231:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, %rax
	movq	%r13, (%rsi)
	notq	%rax
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L261:
	cvttsd2sil	%xmm1, %edi
	call	_ZN2v88internal24DoubleToPrecisionCStringEdi@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L251
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	movq	(%r15), %r13
	jmp	.L234
.L273:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L251:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L268:
	movq	2880(%r12), %r13
	jmp	.L234
.L272:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21155:
	.size	_ZN2v88internalL39Builtin_Impl_NumberPrototypeToPrecisionENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_NumberPrototypeToPrecisionENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L280
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L283
.L274:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L274
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_NumberPrototypeToExponentialEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC22:
	.string	"V8.Builtin_NumberPrototypeToExponential"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_NumberPrototypeToExponentialEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_NumberPrototypeToExponentialEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_NumberPrototypeToExponentialEiPmPNS0_7IsolateE:
.LFB21144:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L313
.L285:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_NumberPrototypeToExponentialEiPmPNS0_7IsolateEE27trace_event_unique_atomic22(%rip), %rbx
	testq	%rbx, %rbx
	je	.L314
.L287:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L315
.L289:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL41Builtin_Impl_NumberPrototypeToExponentialENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L316
.L293:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L317
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L318
.L288:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_NumberPrototypeToExponentialEiPmPNS0_7IsolateEE27trace_event_unique_atomic22(%rip)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L315:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L319
.L290:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L291
	movq	(%rdi), %rax
	call	*8(%rax)
.L291:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L292
	movq	(%rdi), %rax
	call	*8(%rax)
.L292:
	leaq	.LC22(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L316:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L313:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$788, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L319:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC22(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L318:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L288
.L317:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21144:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_NumberPrototypeToExponentialEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_NumberPrototypeToExponentialEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_Stats_NumberPrototypeToFixedEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"V8.Builtin_NumberPrototypeToFixed"
	.section	.text._ZN2v88internalL41Builtin_Impl_Stats_NumberPrototypeToFixedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_Stats_NumberPrototypeToFixedEiPmPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_Stats_NumberPrototypeToFixedEiPmPNS0_7IsolateE:
.LFB21147:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L349
.L321:
	movq	_ZZN2v88internalL41Builtin_Impl_Stats_NumberPrototypeToFixedEiPmPNS0_7IsolateEE27trace_event_unique_atomic67(%rip), %rbx
	testq	%rbx, %rbx
	je	.L350
.L323:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L351
.L325:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL35Builtin_Impl_NumberPrototypeToFixedENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L352
.L329:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L353
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L354
.L324:
	movq	%rbx, _ZZN2v88internalL41Builtin_Impl_Stats_NumberPrototypeToFixedEiPmPNS0_7IsolateEE27trace_event_unique_atomic67(%rip)
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L351:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L355
.L326:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L327
	movq	(%rdi), %rax
	call	*8(%rax)
.L327:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L328
	movq	(%rdi), %rax
	call	*8(%rax)
.L328:
	leaq	.LC23(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L352:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L349:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$789, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L355:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC23(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L354:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L324
.L353:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21147:
	.size	_ZN2v88internalL41Builtin_Impl_Stats_NumberPrototypeToFixedEiPmPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_Stats_NumberPrototypeToFixedEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL48Builtin_Impl_Stats_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"V8.Builtin_NumberPrototypeToLocaleString"
	.section	.text._ZN2v88internalL48Builtin_Impl_Stats_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Builtin_Impl_Stats_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL48Builtin_Impl_Stats_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE:
.LFB21150:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L385
.L357:
	movq	_ZZN2v88internalL48Builtin_Impl_Stats_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic112(%rip), %rbx
	testq	%rbx, %rbx
	je	.L386
.L359:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L387
.L361:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL42Builtin_Impl_NumberPrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L388
.L365:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L389
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L390
.L360:
	movq	%rbx, _ZZN2v88internalL48Builtin_Impl_Stats_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic112(%rip)
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L387:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L391
.L362:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L363
	movq	(%rdi), %rax
	call	*8(%rax)
.L363:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L364
	movq	(%rdi), %rax
	call	*8(%rax)
.L364:
	leaq	.LC24(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L388:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L385:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$790, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L391:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC24(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L360
.L389:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21150:
	.size	_ZN2v88internalL48Builtin_Impl_Stats_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL48Builtin_Impl_Stats_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_Stats_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"V8.Builtin_NumberPrototypeToPrecision"
	.section	.text._ZN2v88internalL45Builtin_Impl_Stats_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_Stats_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_Stats_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE:
.LFB21153:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L421
.L393:
	movq	_ZZN2v88internalL45Builtin_Impl_Stats_NumberPrototypeToPrecisionEiPmPNS0_7IsolateEE28trace_event_unique_atomic144(%rip), %rbx
	testq	%rbx, %rbx
	je	.L422
.L395:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L423
.L397:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL39Builtin_Impl_NumberPrototypeToPrecisionENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L424
.L401:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L425
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L426
.L396:
	movq	%rbx, _ZZN2v88internalL45Builtin_Impl_Stats_NumberPrototypeToPrecisionEiPmPNS0_7IsolateEE28trace_event_unique_atomic144(%rip)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L423:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L427
.L398:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L399
	movq	(%rdi), %rax
	call	*8(%rax)
.L399:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L400
	movq	(%rdi), %rax
	call	*8(%rax)
.L400:
	leaq	.LC25(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L424:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L421:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$791, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L427:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC25(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L426:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L396
.L425:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21153:
	.size	_ZN2v88internalL45Builtin_Impl_Stats_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_Stats_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_NumberPrototypeToStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"V8.Builtin_NumberPrototypeToString"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_NumberPrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_NumberPrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_NumberPrototypeToStringEiPmPNS0_7IsolateE:
.LFB21156:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L457
.L429:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_NumberPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic189(%rip), %rbx
	testq	%rbx, %rbx
	je	.L458
.L431:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L459
.L433:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_NumberPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L460
.L437:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L461
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L462
.L432:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_NumberPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic189(%rip)
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L459:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L463
.L434:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L435
	movq	(%rdi), %rax
	call	*8(%rax)
.L435:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L436
	movq	(%rdi), %rax
	call	*8(%rax)
.L436:
	leaq	.LC26(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L460:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L457:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$792, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L463:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC26(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L462:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L432
.L461:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21156:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_NumberPrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_NumberPrototypeToStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_NumberPrototypeToExponentialEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_NumberPrototypeToExponentialEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_NumberPrototypeToExponentialEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_NumberPrototypeToExponentialEiPmPNS0_7IsolateE:
.LFB21145:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L468
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL41Builtin_Impl_NumberPrototypeToExponentialENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore 6
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_NumberPrototypeToExponentialEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21145:
	.size	_ZN2v88internal36Builtin_NumberPrototypeToExponentialEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_NumberPrototypeToExponentialEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Builtin_NumberPrototypeToFixedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Builtin_NumberPrototypeToFixedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Builtin_NumberPrototypeToFixedEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Builtin_NumberPrototypeToFixedEiPmPNS0_7IsolateE:
.LFB21148:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L473
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL35Builtin_Impl_NumberPrototypeToFixedENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore 6
	jmp	_ZN2v88internalL41Builtin_Impl_Stats_NumberPrototypeToFixedEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21148:
	.size	_ZN2v88internal30Builtin_NumberPrototypeToFixedEiPmPNS0_7IsolateE, .-_ZN2v88internal30Builtin_NumberPrototypeToFixedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Builtin_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Builtin_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Builtin_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Builtin_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE:
.LFB21151:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L478
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL42Builtin_Impl_NumberPrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore 6
	jmp	_ZN2v88internalL48Builtin_Impl_Stats_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21151:
	.size	_ZN2v88internal37Builtin_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE, .-_ZN2v88internal37Builtin_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Builtin_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Builtin_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Builtin_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Builtin_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE:
.LFB21154:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L483
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL39Builtin_Impl_NumberPrototypeToPrecisionENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore 6
	jmp	_ZN2v88internalL45Builtin_Impl_Stats_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21154:
	.size	_ZN2v88internal34Builtin_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE, .-_ZN2v88internal34Builtin_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_NumberPrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_NumberPrototypeToStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_NumberPrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_NumberPrototypeToStringEiPmPNS0_7IsolateE:
.LFB21157:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L488
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_NumberPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_NumberPrototypeToStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21157:
	.size	_ZN2v88internal31Builtin_NumberPrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_NumberPrototypeToStringEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal36Builtin_NumberPrototypeToExponentialEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal36Builtin_NumberPrototypeToExponentialEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal36Builtin_NumberPrototypeToExponentialEiPmPNS0_7IsolateE:
.LFB25750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25750:
	.size	_GLOBAL__sub_I__ZN2v88internal36Builtin_NumberPrototypeToExponentialEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal36Builtin_NumberPrototypeToExponentialEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal36Builtin_NumberPrototypeToExponentialEiPmPNS0_7IsolateE
	.section	.rodata._ZZN2v88internalL36Builtin_Impl_NumberPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateEE10kCharTable,"a"
	.align 32
	.type	_ZZN2v88internalL36Builtin_Impl_NumberPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateEE10kCharTable, @object
	.size	_ZZN2v88internalL36Builtin_Impl_NumberPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateEE10kCharTable, 37
_ZZN2v88internalL36Builtin_Impl_NumberPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateEE10kCharTable:
	.string	"0123456789abcdefghijklmnopqrstuvwxyz"
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_NumberPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic189,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_NumberPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic189, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_NumberPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic189, 8
_ZZN2v88internalL42Builtin_Impl_Stats_NumberPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic189:
	.zero	8
	.section	.bss._ZZN2v88internalL45Builtin_Impl_Stats_NumberPrototypeToPrecisionEiPmPNS0_7IsolateEE28trace_event_unique_atomic144,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Builtin_Impl_Stats_NumberPrototypeToPrecisionEiPmPNS0_7IsolateEE28trace_event_unique_atomic144, @object
	.size	_ZZN2v88internalL45Builtin_Impl_Stats_NumberPrototypeToPrecisionEiPmPNS0_7IsolateEE28trace_event_unique_atomic144, 8
_ZZN2v88internalL45Builtin_Impl_Stats_NumberPrototypeToPrecisionEiPmPNS0_7IsolateEE28trace_event_unique_atomic144:
	.zero	8
	.section	.bss._ZZN2v88internalL48Builtin_Impl_Stats_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic112,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL48Builtin_Impl_Stats_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic112, @object
	.size	_ZZN2v88internalL48Builtin_Impl_Stats_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic112, 8
_ZZN2v88internalL48Builtin_Impl_Stats_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic112:
	.zero	8
	.section	.bss._ZZN2v88internalL41Builtin_Impl_Stats_NumberPrototypeToFixedEiPmPNS0_7IsolateEE27trace_event_unique_atomic67,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Builtin_Impl_Stats_NumberPrototypeToFixedEiPmPNS0_7IsolateEE27trace_event_unique_atomic67, @object
	.size	_ZZN2v88internalL41Builtin_Impl_Stats_NumberPrototypeToFixedEiPmPNS0_7IsolateEE27trace_event_unique_atomic67, 8
_ZZN2v88internalL41Builtin_Impl_Stats_NumberPrototypeToFixedEiPmPNS0_7IsolateEE27trace_event_unique_atomic67:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_NumberPrototypeToExponentialEiPmPNS0_7IsolateEE27trace_event_unique_atomic22,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_NumberPrototypeToExponentialEiPmPNS0_7IsolateEE27trace_event_unique_atomic22, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_NumberPrototypeToExponentialEiPmPNS0_7IsolateEE27trace_event_unique_atomic22, 8
_ZZN2v88internalL47Builtin_Impl_Stats_NumberPrototypeToExponentialEiPmPNS0_7IsolateEE27trace_event_unique_atomic22:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	0
	.long	1105199104
	.align 8
.LC5:
	.long	0
	.long	1076101120
	.align 8
.LC6:
	.long	0
	.long	1073741824
	.align 8
.LC7:
	.long	0
	.long	1078067200
	.align 8
.LC9:
	.long	4292870144
	.long	1106247679
	.align 8
.LC10:
	.long	0
	.long	-1020264448
	.align 8
.LC11:
	.long	0
	.long	1127219200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC12:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC13:
	.long	4294967295
	.long	2146435071
	.align 8
.LC15:
	.long	0
	.long	1079574528
	.align 8
.LC20:
	.long	0
	.long	1072693248
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
