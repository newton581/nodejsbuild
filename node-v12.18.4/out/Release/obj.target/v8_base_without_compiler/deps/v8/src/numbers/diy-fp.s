	.file	"diy-fp.cc"
	.text
	.section	.text._ZN2v88internal5DiyFp8MultiplyERKS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5DiyFp8MultiplyERKS1_
	.type	_ZN2v88internal5DiyFp8MultiplyERKS1_, @function
_ZN2v88internal5DiyFp8MultiplyERKS1_:
.LFB2773:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rdx
	movl	8(%rsi), %esi
	movq	%rax, %r8
	movq	%rdx, %r9
	movl	%eax, %eax
	movl	%edx, %edx
	shrq	$32, %r8
	movq	%rax, %r10
	shrq	$32, %r9
	addl	$64, %esi
	movq	%r8, %rcx
	imulq	%rdx, %rax
	addl	%esi, 8(%rdi)
	imulq	%rdx, %rcx
	imulq	%r9, %r10
	shrq	$32, %rax
	imulq	%r9, %r8
	movl	%ecx, %edx
	shrq	$32, %rcx
	addq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rax, %rdx
	movl	%r10d, %eax
	shrq	$32, %r10
	addq	%rdx, %rax
	addq	%r10, %rcx
	shrq	$32, %rax
	movq	%rax, %rdx
	leaq	(%rcx,%r8), %rax
	addq	%rdx, %rax
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE2773:
	.size	_ZN2v88internal5DiyFp8MultiplyERKS1_, .-_ZN2v88internal5DiyFp8MultiplyERKS1_
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
