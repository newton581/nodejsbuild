	.file	"cancelable-task.cc"
	.text
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CancelableTask3RunEv
	.type	_ZN2v88internal14CancelableTask3RunEv, @function
_ZN2v88internal14CancelableTask3RunEv:
.LFB3729:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L1
	movq	(%rdi), %rax
	jmp	*24(%rax)
.L1:
	ret
	.cfi_endproc
.LFE3729:
	.size	_ZN2v88internal14CancelableTask3RunEv, .-_ZN2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal18CancelableIdleTask3RunEd,"axG",@progbits,_ZN2v88internal18CancelableIdleTask3RunEd,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18CancelableIdleTask3RunEd
	.type	_ZN2v88internal18CancelableIdleTask3RunEd, @function
_ZN2v88internal18CancelableIdleTask3RunEd:
.LFB3730:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L4
	movq	(%rdi), %rax
	jmp	*24(%rax)
.L4:
	ret
	.cfi_endproc
.LFE3730:
	.size	_ZN2v88internal18CancelableIdleTask3RunEd, .-_ZN2v88internal18CancelableIdleTask3RunEd
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14CancelableTask3RunEv
	.type	_ZThn32_N2v88internal14CancelableTask3RunEv, @function
_ZThn32_N2v88internal14CancelableTask3RunEv:
.LFB10180:
	.cfi_startproc
	endbr64
	leaq	-32(%rdi), %r8
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L6
	movq	-32(%rdi), %rax
	movq	%r8, %rdi
	movq	24(%rax), %rax
	jmp	*%rax
.L6:
	ret
	.cfi_endproc
.LFE10180:
	.size	_ZThn32_N2v88internal14CancelableTask3RunEv, .-_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal18CancelableIdleTask3RunEd,"axG",@progbits,_ZN2v88internal18CancelableIdleTask3RunEd,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal18CancelableIdleTask3RunEd
	.type	_ZThn32_N2v88internal18CancelableIdleTask3RunEd, @function
_ZThn32_N2v88internal18CancelableIdleTask3RunEd:
.LFB10181:
	.cfi_startproc
	endbr64
	leaq	-32(%rdi), %r8
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L8
	movq	-32(%rdi), %rax
	movq	%r8, %rdi
	movq	24(%rax), %rax
	jmp	*%rax
.L8:
	ret
	.cfi_endproc
.LFE10181:
	.size	_ZThn32_N2v88internal18CancelableIdleTask3RunEd, .-_ZThn32_N2v88internal18CancelableIdleTask3RunEd
	.section	.text._ZN2v88internal21CancelableTaskManagerC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21CancelableTaskManagerC2Ev
	.type	_ZN2v88internal21CancelableTaskManagerC2Ev, @function
_ZN2v88internal21CancelableTaskManagerC2Ev:
.LFB8642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	56(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -56(%rdi)
	movq	$0, -64(%rdi)
	movq	$1, -48(%rdi)
	movq	$0, -40(%rdi)
	movq	$0, -32(%rdi)
	movl	$0x3f800000, -24(%rdi)
	movq	$0, -16(%rdi)
	movq	$0, -8(%rdi)
	call	_ZN2v84base17ConditionVariableC1Ev@PLT
	leaq	112(%rbx), %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	movb	$0, 152(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8642:
	.size	_ZN2v88internal21CancelableTaskManagerC2Ev, .-_ZN2v88internal21CancelableTaskManagerC2Ev
	.globl	_ZN2v88internal21CancelableTaskManagerC1Ev
	.set	_ZN2v88internal21CancelableTaskManagerC1Ev,_ZN2v88internal21CancelableTaskManagerC2Ev
	.section	.rodata._ZN2v88internal21CancelableTaskManagerD2Ev.str1.1,"aMS",@progbits,1
.LC1:
	.string	"canceled_"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal21CancelableTaskManagerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21CancelableTaskManagerD2Ev
	.type	_ZN2v88internal21CancelableTaskManagerD2Ev, @function
_ZN2v88internal21CancelableTaskManagerD2Ev:
.LFB8645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpb	$0, 152(%rdi)
	je	.L22
	movq	%rdi, %rbx
	leaq	112(%rdi), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	leaq	64(%rbx), %rdi
	call	_ZN2v84base17ConditionVariableD1Ev@PLT
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L15
.L14:
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	addq	$56, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L12
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8645:
	.size	_ZN2v88internal21CancelableTaskManagerD2Ev, .-_ZN2v88internal21CancelableTaskManagerD2Ev
	.globl	_ZN2v88internal21CancelableTaskManagerD1Ev
	.set	_ZN2v88internal21CancelableTaskManagerD1Ev,_ZN2v88internal21CancelableTaskManagerD2Ev
	.section	.text._ZN2v88internal21CancelableTaskManager13CancelAndWaitEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21CancelableTaskManager13CancelAndWaitEv
	.type	_ZN2v88internal21CancelableTaskManager13CancelAndWaitEv, @function
_ZN2v88internal21CancelableTaskManager13CancelAndWaitEv:
.LFB8650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	112(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movb	$1, 152(%rbx)
.L26:
	cmpq	$0, 32(%rbx)
	je	.L24
	movq	24(%rbx), %r14
	testq	%r14, %r14
	je	.L34
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r14, %rdi
	movq	(%r14), %r14
	movl	$1, %ecx
	movl	%r12d, %eax
	movq	16(%rdi), %rdx
	lock cmpxchgl	%ecx, 16(%rdx)
	jne	.L28
	movq	16(%rbx), %rcx
	movq	8(%rdi), %rax
	xorl	%edx, %edx
	movq	8(%rbx), %r10
	divq	%rcx
	leaq	0(,%rdx,8), %r11
	movq	%rdx, %r9
	leaq	(%r10,%r11), %r15
	movq	(%r15), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rdx, %r8
	movq	(%rdx), %rdx
	cmpq	%rdx, %rdi
	jne	.L29
	movq	(%rdi), %rsi
	cmpq	%r8, %rax
	je	.L45
	testq	%rsi, %rsi
	je	.L32
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r9
	je	.L32
	movq	%r8, (%r10,%rdx,8)
	movq	(%rdi), %rsi
.L32:
	movq	%rsi, (%r8)
	call	_ZdlPv@PLT
	subq	$1, 32(%rbx)
.L28:
	testq	%r14, %r14
	jne	.L25
	cmpq	$0, 32(%rbx)
	jne	.L34
.L24:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L35
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r9
	je	.L32
	movq	%r8, (%r10,%rdx,8)
	addq	8(%rbx), %r11
	leaq	24(%rbx), %rdx
	movq	(%r11), %rax
	movq	%r11, %r15
	cmpq	%rdx, %rax
	je	.L46
.L33:
	movq	$0, (%r15)
	movq	(%rdi), %rsi
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%r8, %rax
	leaq	24(%rbx), %rdx
	cmpq	%rdx, %rax
	jne	.L33
.L46:
	movq	%rsi, 24(%rbx)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	64(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE@PLT
	jmp	.L26
	.cfi_endproc
.LFE8650:
	.size	_ZN2v88internal21CancelableTaskManager13CancelAndWaitEv, .-_ZN2v88internal21CancelableTaskManager13CancelAndWaitEv
	.section	.text._ZN2v88internal21CancelableTaskManager11TryAbortAllEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21CancelableTaskManager11TryAbortAllEv
	.type	_ZN2v88internal21CancelableTaskManager11TryAbortAllEv, @function
_ZN2v88internal21CancelableTaskManager11TryAbortAllEv:
.LFB8651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	112(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpq	$0, 32(%rbx)
	je	.L48
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L59
	.p2align 4,,10
	.p2align 3
.L49:
	movq	16(%rdi), %rdx
	movl	$1, %ecx
	movl	%r12d, %eax
	lock cmpxchgl	%ecx, 16(%rdx)
	jne	.L51
	movq	16(%rbx), %rcx
	movq	8(%rdi), %rax
	xorl	%edx, %edx
	movq	8(%rbx), %r9
	divq	%rcx
	leaq	0(,%rdx,8), %r10
	movq	%rdx, %r8
	leaq	(%r9,%r10), %r11
	movq	(%r11), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%rdx, %rsi
	movq	(%rdx), %rdx
	cmpq	%rdx, %rdi
	jne	.L52
	movq	(%rdi), %r14
	cmpq	%rsi, %rax
	je	.L68
	testq	%r14, %r14
	je	.L69
	movq	8(%r14), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r8
	je	.L55
	movq	%rsi, (%r9,%rdx,8)
	movq	(%rdi), %r14
.L55:
	movq	%r14, (%rsi)
	call	_ZdlPv@PLT
	subq	$1, 32(%rbx)
	movq	%r14, %rdi
.L58:
	testq	%rdi, %rdi
	jne	.L49
	cmpq	$0, 32(%rbx)
	je	.L70
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$1, %r12d
.L48:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L61
	movq	8(%r14), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r8
	je	.L55
	movq	%rsi, (%r9,%rdx,8)
	addq	8(%rbx), %r10
	leaq	24(%rbx), %rdx
	movq	(%r10), %rax
	movq	%r10, %r11
	cmpq	%rdx, %rax
	je	.L71
.L56:
	movq	$0, (%r11)
	movq	(%rdi), %r14
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L69:
	movq	$0, (%rsi)
	call	_ZdlPv@PLT
	subq	$1, 32(%rbx)
	cmpq	$0, 32(%rbx)
	jne	.L59
.L70:
	movl	$2, %r12d
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%rsi, %rax
	leaq	24(%rbx), %rdx
	cmpq	%rdx, %rax
	jne	.L56
.L71:
	movq	%r14, 24(%rbx)
	jmp	.L56
.L51:
	movq	(%rdi), %rdi
	jmp	.L58
	.cfi_endproc
.LFE8651:
	.size	_ZN2v88internal21CancelableTaskManager11TryAbortAllEv, .-_ZN2v88internal21CancelableTaskManager11TryAbortAllEv
	.section	.text._ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.type	_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, @function
_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm:
.LFB9838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L73
	movq	(%rbx), %r8
.L74:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L83
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L84:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L97
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L98
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L76:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L78
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L80:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L81:
	testq	%rsi, %rsi
	je	.L78
.L79:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L80
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L86
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L79
	.p2align 4,,10
	.p2align 3
.L78:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r10
	je	.L82
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L82:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L83:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L85
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L85:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%rdx, %rdi
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L76
.L98:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9838:
	.size	_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, .-_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.section	.rodata._ZN2v88internal21CancelableTaskManager8RegisterEPNS0_10CancelableE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"kInvalidTaskId != id"
	.section	.text._ZN2v88internal21CancelableTaskManager8RegisterEPNS0_10CancelableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21CancelableTaskManager8RegisterEPNS0_10CancelableE
	.type	_ZN2v88internal21CancelableTaskManager8RegisterEPNS0_10CancelableE, @function
_ZN2v88internal21CancelableTaskManager8RegisterEPNS0_10CancelableE:
.LFB8647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	112(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpb	$0, 152(%rbx)
	jne	.L116
	movq	(%rbx), %rax
	leaq	1(%rax), %r13
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L117
	movq	16(%rbx), %rdi
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	8(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L103
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L118:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L103
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r15
	jne	.L103
.L105:
	cmpq	%rsi, %r13
	jne	.L118
	addq	$16, %rcx
.L106:
	movq	%r12, (%rcx)
.L101:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 16(%r12)
	xorl	%r13d, %r13d
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	8(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r13, 8(%rax)
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L117:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8647:
	.size	_ZN2v88internal21CancelableTaskManager8RegisterEPNS0_10CancelableE, .-_ZN2v88internal21CancelableTaskManager8RegisterEPNS0_10CancelableE
	.section	.text._ZN2v88internal14CancelableTaskC2EPNS0_21CancelableTaskManagerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CancelableTaskC2EPNS0_21CancelableTaskManagerE
	.type	_ZN2v88internal14CancelableTaskC2EPNS0_21CancelableTaskManagerE, @function
_ZN2v88internal14CancelableTaskC2EPNS0_21CancelableTaskManagerE:
.LFB8663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10CancelableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	%rsi, 8(%rbx)
	movq	%rbx, %rsi
	movq	%rax, (%rbx)
	movl	$0, 16(%rbx)
	call	_ZN2v88internal21CancelableTaskManager8RegisterEPNS0_10CancelableE
	movq	%rax, 24(%rbx)
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$48, %rax
	movq	%rax, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8663:
	.size	_ZN2v88internal14CancelableTaskC2EPNS0_21CancelableTaskManagerE, .-_ZN2v88internal14CancelableTaskC2EPNS0_21CancelableTaskManagerE
	.globl	_ZN2v88internal14CancelableTaskC1EPNS0_21CancelableTaskManagerE
	.set	_ZN2v88internal14CancelableTaskC1EPNS0_21CancelableTaskManagerE,_ZN2v88internal14CancelableTaskC2EPNS0_21CancelableTaskManagerE
	.section	.text._ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE
	.type	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE, @function
_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE:
.LFB8653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10CancelableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	45640(%rsi), %rdi
	movq	%rax, (%rbx)
	movq	%rbx, %rsi
	movl	$0, 16(%rbx)
	movq	%rdi, 8(%rbx)
	call	_ZN2v88internal21CancelableTaskManager8RegisterEPNS0_10CancelableE
	movq	%rax, 24(%rbx)
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$48, %rax
	movq	%rax, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8653:
	.size	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE, .-_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE
	.globl	_ZN2v88internal14CancelableTaskC1EPNS0_7IsolateE
	.set	_ZN2v88internal14CancelableTaskC1EPNS0_7IsolateE,_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal18CancelableIdleTaskC2EPNS0_21CancelableTaskManagerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CancelableIdleTaskC2EPNS0_21CancelableTaskManagerE
	.type	_ZN2v88internal18CancelableIdleTaskC2EPNS0_21CancelableTaskManagerE, @function
_ZN2v88internal18CancelableIdleTaskC2EPNS0_21CancelableTaskManagerE:
.LFB8676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10CancelableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	%rsi, 8(%rbx)
	movq	%rbx, %rsi
	movq	%rax, (%rbx)
	movl	$0, 16(%rbx)
	call	_ZN2v88internal21CancelableTaskManager8RegisterEPNS0_10CancelableE
	movq	%rax, 24(%rbx)
	leaq	16+_ZTVN2v88internal18CancelableIdleTaskE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$48, %rax
	movq	%rax, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8676:
	.size	_ZN2v88internal18CancelableIdleTaskC2EPNS0_21CancelableTaskManagerE, .-_ZN2v88internal18CancelableIdleTaskC2EPNS0_21CancelableTaskManagerE
	.globl	_ZN2v88internal18CancelableIdleTaskC1EPNS0_21CancelableTaskManagerE
	.set	_ZN2v88internal18CancelableIdleTaskC1EPNS0_21CancelableTaskManagerE,_ZN2v88internal18CancelableIdleTaskC2EPNS0_21CancelableTaskManagerE
	.section	.text._ZN2v88internal18CancelableIdleTaskC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CancelableIdleTaskC2EPNS0_7IsolateE
	.type	_ZN2v88internal18CancelableIdleTaskC2EPNS0_7IsolateE, @function
_ZN2v88internal18CancelableIdleTaskC2EPNS0_7IsolateE:
.LFB8666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10CancelableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	45640(%rsi), %rdi
	movq	%rax, (%rbx)
	movq	%rbx, %rsi
	movl	$0, 16(%rbx)
	movq	%rdi, 8(%rbx)
	call	_ZN2v88internal21CancelableTaskManager8RegisterEPNS0_10CancelableE
	movq	%rax, 24(%rbx)
	leaq	16+_ZTVN2v88internal18CancelableIdleTaskE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$48, %rax
	movq	%rax, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8666:
	.size	_ZN2v88internal18CancelableIdleTaskC2EPNS0_7IsolateE, .-_ZN2v88internal18CancelableIdleTaskC2EPNS0_7IsolateE
	.globl	_ZN2v88internal18CancelableIdleTaskC1EPNS0_7IsolateE
	.set	_ZN2v88internal18CancelableIdleTaskC1EPNS0_7IsolateE,_ZN2v88internal18CancelableIdleTaskC2EPNS0_7IsolateE
	.section	.text._ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS8_15_Hash_node_baseEPNS8_10_Hash_nodeIS6_Lb0EEE,"axG",@progbits,_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS8_15_Hash_node_baseEPNS8_10_Hash_nodeIS6_Lb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS8_15_Hash_node_baseEPNS8_10_Hash_nodeIS6_Lb0EEE
	.type	_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS8_15_Hash_node_baseEPNS8_10_Hash_nodeIS6_Lb0EEE, @function
_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS8_15_Hash_node_baseEPNS8_10_Hash_nodeIS6_Lb0EEE:
.LFB9935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	0(,%rsi,8), %r9
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	movq	(%rbx), %rcx
	movq	(%rdi), %r12
	leaq	(%rcx,%r9), %rax
	cmpq	%rdx, (%rax)
	je	.L137
	testq	%r12, %r12
	je	.L130
	movq	8(%r12), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	cmpq	%rdx, %rsi
	je	.L130
	movq	%r8, (%rcx,%rdx,8)
	movq	(%rdi), %r12
.L130:
	movq	%r12, (%r8)
	call	_ZdlPv@PLT
	movq	%r12, %rax
	subq	$1, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L129
	movq	8(%r12), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	cmpq	%rdx, %rsi
	je	.L130
	movq	%r8, (%rcx,%rdx,8)
	movq	(%rbx), %rax
	addq	%r9, %rax
	movq	(%rax), %rdx
.L129:
	leaq	16(%rbx), %rcx
	cmpq	%rcx, %rdx
	je	.L138
.L131:
	movq	$0, (%rax)
	movq	(%rdi), %r12
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r12, 16(%rbx)
	jmp	.L131
	.cfi_endproc
.LFE9935:
	.size	_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS8_15_Hash_node_baseEPNS8_10_Hash_nodeIS6_Lb0EEE, .-_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS8_15_Hash_node_baseEPNS8_10_Hash_nodeIS6_Lb0EEE
	.section	.text._ZN2v88internal21CancelableTaskManager18RemoveFinishedTaskEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21CancelableTaskManager18RemoveFinishedTaskEm
	.type	_ZN2v88internal21CancelableTaskManager18RemoveFinishedTaskEm, @function
_ZN2v88internal21CancelableTaskManager18RemoveFinishedTaskEm:
.LFB8648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L152
	leaq	112(%rdi), %r13
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	8(%rbx), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r10
	testq	%r8, %r8
	je	.L141
	movq	(%r8), %rcx
	movq	8(%rcx), %rsi
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L153:
	movq	(%rcx), %r9
	testq	%r9, %r9
	je	.L141
	movq	8(%r9), %rsi
	xorl	%edx, %edx
	movq	%rcx, %r8
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L141
	movq	%r9, %rcx
.L143:
	cmpq	%r12, %rsi
	jne	.L153
	leaq	8(%rbx), %rdi
	movq	%r8, %rdx
	movq	%r10, %rsi
	call	_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS8_15_Hash_node_baseEPNS8_10_Hash_nodeIS6_Lb0EEE
.L141:
	leaq	64(%rbx), %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8648:
	.size	_ZN2v88internal21CancelableTaskManager18RemoveFinishedTaskEm, .-_ZN2v88internal21CancelableTaskManager18RemoveFinishedTaskEm
	.section	.text._ZN2v88internal10CancelableD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10CancelableD2Ev
	.type	_ZN2v88internal10CancelableD2Ev, @function
_ZN2v88internal10CancelableD2Ev:
.LFB8617:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal10CancelableE(%rip), %rax
	movl	$2, %edx
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	lock cmpxchgl	%edx, 16(%rdi)
	sete	%dl
	cmpl	$2, %eax
	je	.L157
	testb	%dl, %dl
	je	.L154
.L157:
	movq	24(%rdi), %rsi
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal21CancelableTaskManager18RemoveFinishedTaskEm
.L154:
	ret
	.cfi_endproc
.LFE8617:
	.size	_ZN2v88internal10CancelableD2Ev, .-_ZN2v88internal10CancelableD2Ev
	.globl	_ZN2v88internal10CancelableD1Ev
	.set	_ZN2v88internal10CancelableD1Ev,_ZN2v88internal10CancelableD2Ev
	.section	.text._ZN2v88internal10CancelableD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10CancelableD0Ev
	.type	_ZN2v88internal10CancelableD0Ev, @function
_ZN2v88internal10CancelableD0Ev:
.LFB8619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10CancelableE(%rip), %rax
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	lock cmpxchgl	%edx, 16(%rdi)
	sete	%dl
	cmpl	$2, %eax
	je	.L161
	testb	%dl, %dl
	je	.L159
.L161:
	movq	24(%r12), %rsi
	movq	8(%r12), %rdi
	call	_ZN2v88internal21CancelableTaskManager18RemoveFinishedTaskEm
.L159:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8619:
	.size	_ZN2v88internal10CancelableD0Ev, .-_ZN2v88internal10CancelableD0Ev
	.section	.text._ZN2v88internal21CancelableTaskManager8TryAbortEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21CancelableTaskManager8TryAbortEm
	.type	_ZN2v88internal21CancelableTaskManager8TryAbortEm, @function
_ZN2v88internal21CancelableTaskManager8TryAbortEm:
.LFB8649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L179
	leaq	112(%rdi), %r13
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	8(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L175
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L180:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L175
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L175
.L170:
	cmpq	%r12, %rsi
	jne	.L180
	movq	16(%rcx), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	lock cmpxchgl	%esi, 16(%rdx)
	jne	.L181
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	16(%rbx)
	movq	8(%rbx), %rax
	movq	%rdx, %rsi
	movq	(%rax,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%rdx, %r8
	movq	(%rdx), %rdx
	cmpq	%rdx, %rcx
	jne	.L171
	leaq	8(%rbx), %rdi
	movq	%r8, %rdx
	movl	$2, %r12d
	call	_ZNSt10_HashtableImSt4pairIKmPN2v88internal10CancelableEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS8_15_Hash_node_baseEPNS8_10_Hash_nodeIS6_Lb0EEE
	leaq	64(%rbx), %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
.L168:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L179:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L181:
	movl	$1, %r12d
	jmp	.L168
	.cfi_endproc
.LFE8649:
	.size	_ZN2v88internal21CancelableTaskManager8TryAbortEm, .-_ZN2v88internal21CancelableTaskManager8TryAbortEm
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10CancelableD2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10CancelableD2Ev, @function
_GLOBAL__sub_I__ZN2v88internal10CancelableD2Ev:
.LFB10125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10125:
	.size	_GLOBAL__sub_I__ZN2v88internal10CancelableD2Ev, .-_GLOBAL__sub_I__ZN2v88internal10CancelableD2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10CancelableD2Ev
	.weak	_ZTVN2v88internal14CancelableTaskE
	.section	.data.rel.ro._ZTVN2v88internal14CancelableTaskE,"awG",@progbits,_ZTVN2v88internal14CancelableTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal14CancelableTaskE, @object
	.size	_ZTVN2v88internal14CancelableTaskE, 88
_ZTVN2v88internal14CancelableTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal18CancelableIdleTaskE
	.section	.data.rel.ro._ZTVN2v88internal18CancelableIdleTaskE,"awG",@progbits,_ZTVN2v88internal18CancelableIdleTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal18CancelableIdleTaskE, @object
	.size	_ZTVN2v88internal18CancelableIdleTaskE, 88
_ZTVN2v88internal18CancelableIdleTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal18CancelableIdleTask3RunEd
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal18CancelableIdleTask3RunEd
	.weak	_ZTVN2v88internal10CancelableE
	.section	.data.rel.ro.local._ZTVN2v88internal10CancelableE,"awG",@progbits,_ZTVN2v88internal10CancelableE,comdat
	.align 8
	.type	_ZTVN2v88internal10CancelableE, @object
	.size	_ZTVN2v88internal10CancelableE, 32
_ZTVN2v88internal10CancelableE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal10CancelableD1Ev
	.quad	_ZN2v88internal10CancelableD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
