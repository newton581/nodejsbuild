	.file	"eh-frame-x64.cc"
	.text
	.section	.text._ZN2v88internal13EhFrameWriter30WriteReturnAddressRegisterCodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter30WriteReturnAddressRegisterCodeEv
	.type	_ZN2v88internal13EhFrameWriter30WriteReturnAddressRegisterCodeEv, @function
_ZN2v88internal13EhFrameWriter30WriteReturnAddressRegisterCodeEv:
.LFB5653:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZN2v88internal13EhFrameWriter12WriteULeb128Ej@PLT
	.cfi_endproc
.LFE5653:
	.size	_ZN2v88internal13EhFrameWriter30WriteReturnAddressRegisterCodeEv, .-_ZN2v88internal13EhFrameWriter30WriteReturnAddressRegisterCodeEv
	.section	.text._ZN2v88internal13EhFrameWriter22WriteInitialStateInCieEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter22WriteInitialStateInCieEv
	.type	_ZN2v88internal13EhFrameWriter22WriteInitialStateInCieEv, @function
_ZN2v88internal13EhFrameWriter22WriteInitialStateInCieEv:
.LFB5654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	_ZN2v88internalL3rspE(%rip), %esi
	call	_ZN2v88internal13EhFrameWriter31SetBaseAddressRegisterAndOffsetENS0_8RegisterEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$-8, %edx
	popq	%r12
	movl	$16, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13EhFrameWriter26RecordRegisterSavedToStackEii@PLT
	.cfi_endproc
.LFE5654:
	.size	_ZN2v88internal13EhFrameWriter22WriteInitialStateInCieEv, .-_ZN2v88internal13EhFrameWriter22WriteInitialStateInCieEv
	.section	.rodata._ZN2v88internal13EhFrameWriter19RegisterToDwarfCodeENS0_8RegisterE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unimplemented code"
	.section	.text._ZN2v88internal13EhFrameWriter19RegisterToDwarfCodeENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter19RegisterToDwarfCodeENS0_8RegisterE
	.type	_ZN2v88internal13EhFrameWriter19RegisterToDwarfCodeENS0_8RegisterE, @function
_ZN2v88internal13EhFrameWriter19RegisterToDwarfCodeENS0_8RegisterE:
.LFB5655:
	.cfi_startproc
	endbr64
	cmpl	$4, %edi
	je	.L7
	movl	%edi, %eax
	cmpl	$5, %edi
	je	.L8
	testl	%edi, %edi
	je	.L5
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$6, %eax
.L5:
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$7, %eax
	ret
	.cfi_endproc
.LFE5655:
	.size	_ZN2v88internal13EhFrameWriter19RegisterToDwarfCodeENS0_8RegisterE, .-_ZN2v88internal13EhFrameWriter19RegisterToDwarfCodeENS0_8RegisterE
	.section	.rodata._ZN2v88internal19EhFrameDisassembler25DwarfRegisterCodeToStringEi.str1.1,"aMS",@progbits,1
.LC1:
	.string	"rsp"
.LC2:
	.string	"rip"
.LC3:
	.string	"rbp"
	.section	.text._ZN2v88internal19EhFrameDisassembler25DwarfRegisterCodeToStringEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19EhFrameDisassembler25DwarfRegisterCodeToStringEi
	.type	_ZN2v88internal19EhFrameDisassembler25DwarfRegisterCodeToStringEi, @function
_ZN2v88internal19EhFrameDisassembler25DwarfRegisterCodeToStringEi:
.LFB5656:
	.cfi_startproc
	endbr64
	cmpl	$7, %edi
	je	.L17
	cmpl	$16, %edi
	je	.L18
	cmpl	$6, %edi
	je	.L19
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE5656:
	.size	_ZN2v88internal19EhFrameDisassembler25DwarfRegisterCodeToStringEi, .-_ZN2v88internal19EhFrameDisassembler25DwarfRegisterCodeToStringEi
	.globl	_ZN2v88internal16EhFrameConstants20kDataAlignmentFactorE
	.section	.rodata._ZN2v88internal16EhFrameConstants20kDataAlignmentFactorE,"a"
	.align 4
	.type	_ZN2v88internal16EhFrameConstants20kDataAlignmentFactorE, @object
	.size	_ZN2v88internal16EhFrameConstants20kDataAlignmentFactorE, 4
_ZN2v88internal16EhFrameConstants20kDataAlignmentFactorE:
	.long	-8
	.globl	_ZN2v88internal16EhFrameConstants20kCodeAlignmentFactorE
	.section	.rodata._ZN2v88internal16EhFrameConstants20kCodeAlignmentFactorE,"a"
	.align 4
	.type	_ZN2v88internal16EhFrameConstants20kCodeAlignmentFactorE, @object
	.size	_ZN2v88internal16EhFrameConstants20kCodeAlignmentFactorE, 4
_ZN2v88internal16EhFrameConstants20kCodeAlignmentFactorE:
	.long	1
	.section	.rodata._ZN2v88internalL3rspE,"a"
	.align 4
	.type	_ZN2v88internalL3rspE, @object
	.size	_ZN2v88internalL3rspE, 4
_ZN2v88internalL3rspE:
	.long	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
