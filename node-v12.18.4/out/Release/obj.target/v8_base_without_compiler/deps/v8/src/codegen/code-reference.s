	.file	"code-reference.cc"
	.text
	.section	.rodata._ZNK2v88internal13CodeReference13constant_poolEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal13CodeReference13constant_poolEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13CodeReference13constant_poolEv
	.type	_ZNK2v88internal13CodeReference13constant_poolEv, @function
_ZNK2v88internal13CodeReference13constant_poolEv:
.LFB17830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	cmpl	$2, %eax
	je	.L2
	cmpl	$3, %eax
	je	.L3
	cmpl	$1, %eax
	je	.L14
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	movq	8(%rdi), %rdi
	call	_ZNK2v88internal4wasm8WasmCode13constant_poolEv@PLT
.L1:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L15
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	8(%rdi), %rax
	leaq	-32(%rbp), %r12
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal4Code17has_constant_poolEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L1
	movq	-32(%rbp), %rdx
	movl	43(%rdx), %eax
	leaq	63(%rdx), %rcx
	testl	%eax, %eax
	js	.L16
.L7:
	movslq	55(%rdx), %rax
	addq	%rcx, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	movq	8(%rdi), %rdx
	movslq	32(%rdx), %rax
	addq	(%rdx), %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r12, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-32(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L7
.L15:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17830:
	.size	_ZNK2v88internal13CodeReference13constant_poolEv, .-_ZNK2v88internal13CodeReference13constant_poolEv
	.section	.text._ZNK2v88internal13CodeReference17instruction_startEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13CodeReference17instruction_startEv
	.type	_ZNK2v88internal13CodeReference17instruction_startEv, @function
_ZNK2v88internal13CodeReference17instruction_startEv:
.LFB17831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	cmpl	$2, %eax
	je	.L19
	cmpl	$3, %eax
	je	.L19
	cmpl	$1, %eax
	je	.L26
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	movq	8(%rdi), %rax
	movq	(%rax), %rax
.L17:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L27
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	movq	%rdx, -16(%rbp)
	leaq	63(%rdx), %rax
	movl	43(%rdx), %edx
	testl	%edx, %edx
	jns	.L17
	leaq	-16(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L17
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17831:
	.size	_ZNK2v88internal13CodeReference17instruction_startEv, .-_ZNK2v88internal13CodeReference17instruction_startEv
	.section	.text._ZNK2v88internal13CodeReference15instruction_endEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13CodeReference15instruction_endEv
	.type	_ZNK2v88internal13CodeReference15instruction_endEv, @function
_ZNK2v88internal13CodeReference15instruction_endEv:
.LFB17832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	cmpl	$2, %eax
	je	.L29
	cmpl	$3, %eax
	je	.L30
	cmpl	$1, %eax
	je	.L37
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	movq	8(%rdi), %rdx
	movq	8(%rdx), %rax
	addq	(%rdx), %rax
.L28:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L38
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	movl	43(%rax), %edx
	testl	%edx, %edx
	js	.L39
	movslq	39(%rax), %rdx
	leaq	63(%rax,%rdx), %rax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L30:
	movq	8(%rdi), %rdx
	movslq	12(%rdx), %rax
	addq	(%rdx), %rax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	-16(%rbp), %rdi
	call	_ZNK2v88internal4Code21OffHeapInstructionEndEv@PLT
	jmp	.L28
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17832:
	.size	_ZNK2v88internal13CodeReference15instruction_endEv, .-_ZNK2v88internal13CodeReference15instruction_endEv
	.section	.text._ZNK2v88internal13CodeReference16instruction_sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13CodeReference16instruction_sizeEv
	.type	_ZNK2v88internal13CodeReference16instruction_sizeEv, @function
_ZNK2v88internal13CodeReference16instruction_sizeEv:
.LFB17833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	cmpl	$2, %eax
	je	.L41
	cmpl	$3, %eax
	je	.L42
	cmpl	$1, %eax
	je	.L49
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	movq	8(%rdi), %rax
	movl	8(%rax), %eax
.L40:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L50
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	movl	43(%rax), %edx
	testl	%edx, %edx
	js	.L51
	movl	39(%rax), %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L42:
	movq	8(%rdi), %rax
	movl	12(%rax), %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	-16(%rbp), %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	jmp	.L40
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17833:
	.size	_ZNK2v88internal13CodeReference16instruction_sizeEv, .-_ZNK2v88internal13CodeReference16instruction_sizeEv
	.section	.text._ZNK2v88internal13CodeReference16relocation_startEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13CodeReference16relocation_startEv
	.type	_ZNK2v88internal13CodeReference16relocation_startEv, @function
_ZNK2v88internal13CodeReference16relocation_startEv:
.LFB17834:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$2, %eax
	je	.L53
	cmpl	$3, %eax
	je	.L54
	cmpl	$1, %eax
	je	.L59
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	8(%rdi), %rax
	movq	16(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	7(%rax), %rax
	addq	$15, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movq	8(%rdi), %rdx
	movslq	48(%rdx), %rax
	addq	(%rdx), %rax
	ret
	.cfi_endproc
.LFE17834:
	.size	_ZNK2v88internal13CodeReference16relocation_startEv, .-_ZNK2v88internal13CodeReference16relocation_startEv
	.section	.text._ZNK2v88internal13CodeReference14relocation_endEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13CodeReference14relocation_endEv
	.type	_ZNK2v88internal13CodeReference14relocation_endEv, @function
_ZNK2v88internal13CodeReference14relocation_endEv:
.LFB17835:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$2, %eax
	je	.L61
	cmpl	$3, %eax
	je	.L62
	cmpl	$1, %eax
	je	.L67
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	8(%rdi), %rdx
	movslq	24(%rdx), %rax
	addq	16(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	7(%rax), %rax
	movslq	11(%rax), %rdx
	leaq	15(%rax,%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	movq	8(%rdi), %rdx
	movslq	8(%rdx), %rax
	addq	(%rdx), %rax
	ret
	.cfi_endproc
.LFE17835:
	.size	_ZNK2v88internal13CodeReference14relocation_endEv, .-_ZNK2v88internal13CodeReference14relocation_endEv
	.section	.text._ZNK2v88internal13CodeReference15relocation_sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13CodeReference15relocation_sizeEv
	.type	_ZNK2v88internal13CodeReference15relocation_sizeEv, @function
_ZNK2v88internal13CodeReference15relocation_sizeEv:
.LFB17836:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$2, %eax
	je	.L69
	cmpl	$3, %eax
	je	.L70
	cmpl	$1, %eax
	je	.L75
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	8(%rdi), %rax
	movl	24(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	7(%rax), %rax
	movl	11(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	movq	8(%rdi), %rax
	movl	52(%rax), %eax
	ret
	.cfi_endproc
.LFE17836:
	.size	_ZNK2v88internal13CodeReference15relocation_sizeEv, .-_ZNK2v88internal13CodeReference15relocation_sizeEv
	.section	.text._ZNK2v88internal13CodeReference13code_commentsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13CodeReference13code_commentsEv
	.type	_ZNK2v88internal13CodeReference13code_commentsEv, @function
_ZNK2v88internal13CodeReference13code_commentsEv:
.LFB17837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	cmpl	$2, %eax
	je	.L77
	cmpl	$3, %eax
	je	.L78
	cmpl	$1, %eax
	je	.L85
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	movq	8(%rdi), %rdi
	call	_ZNK2v88internal4wasm8WasmCode13code_commentsEv@PLT
.L76:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L86
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	movq	%rdx, -16(%rbp)
	movl	43(%rdx), %eax
	leaq	63(%rdx), %rcx
	testl	%eax, %eax
	js	.L87
.L81:
	movslq	55(%rdx), %rax
	addq	%rcx, %rax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L78:
	movq	8(%rdi), %rdx
	movslq	40(%rdx), %rax
	addq	(%rdx), %rax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	-16(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-16(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L81
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17837:
	.size	_ZNK2v88internal13CodeReference13code_commentsEv, .-_ZNK2v88internal13CodeReference13code_commentsEv
	.section	.text._ZNK2v88internal13CodeReference18code_comments_sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13CodeReference18code_comments_sizeEv
	.type	_ZNK2v88internal13CodeReference18code_comments_sizeEv, @function
_ZNK2v88internal13CodeReference18code_comments_sizeEv:
.LFB17838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	cmpl	$2, %eax
	je	.L89
	cmpl	$3, %eax
	je	.L90
	cmpl	$1, %eax
	je	.L95
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	movq	8(%rdi), %rdi
	call	_ZNK2v88internal4wasm8WasmCode18code_comments_sizeEv@PLT
.L88:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L96
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	8(%rdi), %rax
	leaq	-16(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	call	_ZNK2v88internal4Code18code_comments_sizeEv@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L90:
	movq	8(%rdi), %rax
	movl	44(%rax), %eax
	jmp	.L88
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17838:
	.size	_ZNK2v88internal13CodeReference18code_comments_sizeEv, .-_ZNK2v88internal13CodeReference18code_comments_sizeEv
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal13CodeReference13constant_poolEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal13CodeReference13constant_poolEv, @function
_GLOBAL__sub_I__ZNK2v88internal13CodeReference13constant_poolEv:
.LFB21503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21503:
	.size	_GLOBAL__sub_I__ZNK2v88internal13CodeReference13constant_poolEv, .-_GLOBAL__sub_I__ZNK2v88internal13CodeReference13constant_poolEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal13CodeReference13constant_poolEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
