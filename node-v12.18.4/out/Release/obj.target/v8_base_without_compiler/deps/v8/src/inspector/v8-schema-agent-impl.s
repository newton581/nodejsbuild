	.file	"v8-schema-agent-impl.cc"
	.text
	.section	.text._ZN12v8_inspector17V8SchemaAgentImplD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector17V8SchemaAgentImplD2Ev
	.type	_ZN12v8_inspector17V8SchemaAgentImplD2Ev, @function
_ZN12v8_inspector17V8SchemaAgentImplD2Ev:
.LFB5587:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5587:
	.size	_ZN12v8_inspector17V8SchemaAgentImplD2Ev, .-_ZN12v8_inspector17V8SchemaAgentImplD2Ev
	.globl	_ZN12v8_inspector17V8SchemaAgentImplD1Ev
	.set	_ZN12v8_inspector17V8SchemaAgentImplD1Ev,_ZN12v8_inspector17V8SchemaAgentImplD2Ev
	.section	.text._ZN12v8_inspector8protocol6Schema7Backend7disableEv,"axG",@progbits,_ZN12v8_inspector8protocol6Schema7Backend7disableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol6Schema7Backend7disableEv
	.type	_ZN12v8_inspector8protocol6Schema7Backend7disableEv, @function
_ZN12v8_inspector8protocol6Schema7Backend7disableEv:
.LFB4477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4477:
	.size	_ZN12v8_inspector8protocol6Schema7Backend7disableEv, .-_ZN12v8_inspector8protocol6Schema7Backend7disableEv
	.section	.text._ZN12v8_inspector17V8SchemaAgentImplD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector17V8SchemaAgentImplD0Ev
	.type	_ZN12v8_inspector17V8SchemaAgentImplD0Ev, @function
_ZN12v8_inspector17V8SchemaAgentImplD0Ev:
.LFB5589:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5589:
	.size	_ZN12v8_inspector17V8SchemaAgentImplD0Ev, .-_ZN12v8_inspector17V8SchemaAgentImplD0Ev
	.section	.text._ZN12v8_inspector8protocol6Schema6DomainD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol6Schema6DomainD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol6Schema6DomainD0Ev
	.type	_ZN12v8_inspector8protocol6Schema6DomainD0Ev, @function
_ZN12v8_inspector8protocol6Schema6DomainD0Ev:
.LFB4443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	72(%r12), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	56(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4443:
	.size	_ZN12v8_inspector8protocol6Schema6DomainD0Ev, .-_ZN12v8_inspector8protocol6Schema6DomainD0Ev
	.section	.text._ZN12v8_inspector17V8SchemaAgentImpl10getDomainsEPSt10unique_ptrISt6vectorIS1_INS_8protocol6Schema6DomainESt14default_deleteIS5_EESaIS8_EES6_ISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector17V8SchemaAgentImpl10getDomainsEPSt10unique_ptrISt6vectorIS1_INS_8protocol6Schema6DomainESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.type	_ZN12v8_inspector17V8SchemaAgentImpl10getDomainsEPSt10unique_ptrISt6vectorIS1_INS_8protocol6Schema6DomainESt14default_deleteIS5_EESaIS8_EES6_ISA_EE, @function
_ZN12v8_inspector17V8SchemaAgentImpl10getDomainsEPSt10unique_ptrISt6vectorIS1_INS_8protocol6Schema6DomainESt14default_deleteIS5_EESaIS8_EES6_ISA_EE:
.LFB5590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-80(%rbp), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector22V8InspectorSessionImpl20supportedDomainsImplEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-64(%rbp), %rdx
	movq	(%rbx), %r14
	pxor	%xmm0, %xmm0
	movdqa	-80(%rbp), %xmm3
	movq	$0, -64(%rbp)
	movq	%rdx, 16(%rax)
	movq	%rax, (%rbx)
	movups	%xmm3, (%rax)
	movaps	%xmm0, -80(%rbp)
	testq	%r14, %r14
	je	.L13
	movq	8(%r14), %rbx
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L14
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm7
	movq	%rdx, %xmm2
	punpcklqdq	%xmm7, %xmm2
	movaps	%xmm2, -96(%rbp)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L47:
	movdqa	-96(%rbp), %xmm4
	movq	56(%r15), %rdi
	leaq	72(%r15), %rax
	movups	%xmm4, (%r15)
	cmpq	%rax, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	16(%r15), %rdi
	leaq	32(%r15), %rax
	cmpq	%rax, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movl	$96, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L15:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L46
.L19:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L15
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol6Schema6DomainD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L47
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %rbx
	jne	.L19
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%r14), %r12
.L14:
	testq	%r12, %r12
	je	.L20
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L20:
	movq	%r14, %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-72(%rbp), %rbx
	movq	-80(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L21
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	leaq	_ZN12v8_inspector8protocol6Schema6DomainD0Ev(%rip), %r15
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm6
	movq	%rdx, %xmm1
	punpcklqdq	%xmm6, %xmm1
	movaps	%xmm1, -96(%rbp)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L49:
	movdqa	-96(%rbp), %xmm5
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	movups	%xmm5, (%r12)
	cmpq	%rax, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L22:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L48
.L26:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L22
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	je	.L49
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L26
	.p2align 4,,10
	.p2align 3
.L48:
	movq	-80(%rbp), %r14
.L21:
	testq	%r14, %r14
	je	.L13
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L13:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L50:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5590:
	.size	_ZN12v8_inspector17V8SchemaAgentImpl10getDomainsEPSt10unique_ptrISt6vectorIS1_INS_8protocol6Schema6DomainESt14default_deleteIS5_EESaIS8_EES6_ISA_EE, .-_ZN12v8_inspector17V8SchemaAgentImpl10getDomainsEPSt10unique_ptrISt6vectorIS1_INS_8protocol6Schema6DomainESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.section	.text._ZN12v8_inspector8protocol6Schema6DomainD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol6Schema6DomainD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol6Schema6DomainD2Ev
	.type	_ZN12v8_inspector8protocol6Schema6DomainD2Ev, @function
_ZN12v8_inspector8protocol6Schema6DomainD2Ev:
.LFB4441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	72(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	56(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L51
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4441:
	.size	_ZN12v8_inspector8protocol6Schema6DomainD2Ev, .-_ZN12v8_inspector8protocol6Schema6DomainD2Ev
	.weak	_ZN12v8_inspector8protocol6Schema6DomainD1Ev
	.set	_ZN12v8_inspector8protocol6Schema6DomainD1Ev,_ZN12v8_inspector8protocol6Schema6DomainD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev
	.type	_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev, @function
_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev:
.LFB9416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	movq	48(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L55
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9416:
	.size	_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev, .-_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev
	.section	.text._ZN12v8_inspector8protocol6Schema6DomainD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol6Schema6DomainD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev
	.type	_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev, @function
_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev:
.LFB9415:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movups	%xmm0, -8(%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	popq	%rbx
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9415:
	.size	_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev, .-_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev
	.section	.text._ZN12v8_inspector17V8SchemaAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector17V8SchemaAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.type	_ZN12v8_inspector17V8SchemaAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE, @function
_ZN12v8_inspector17V8SchemaAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE:
.LFB5584:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector17V8SchemaAgentImplE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	ret
	.cfi_endproc
.LFE5584:
	.size	_ZN12v8_inspector17V8SchemaAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE, .-_ZN12v8_inspector17V8SchemaAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.globl	_ZN12v8_inspector17V8SchemaAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.set	_ZN12v8_inspector17V8SchemaAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE,_ZN12v8_inspector17V8SchemaAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.weak	_ZTVN12v8_inspector17V8SchemaAgentImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector17V8SchemaAgentImplE,"awG",@progbits,_ZTVN12v8_inspector17V8SchemaAgentImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector17V8SchemaAgentImplE, @object
	.size	_ZTVN12v8_inspector17V8SchemaAgentImplE, 48
_ZTVN12v8_inspector17V8SchemaAgentImplE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector17V8SchemaAgentImplD1Ev
	.quad	_ZN12v8_inspector17V8SchemaAgentImplD0Ev
	.quad	_ZN12v8_inspector17V8SchemaAgentImpl10getDomainsEPSt10unique_ptrISt6vectorIS1_INS_8protocol6Schema6DomainESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.quad	_ZN12v8_inspector8protocol6Schema7Backend7disableEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
