	.file	"fixed-dtoa.cc"
	.text
	.section	.text._ZN2v88internalL15FillFractionalsEmiiNS0_6VectorIcEEPiS3_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL15FillFractionalsEmiiNS0_6VectorIcEEPiS3_.isra.0, @function
_ZN2v88internalL15FillFractionalsEmiiNS0_6VectorIcEEPiS3_.isra.0:
.LFB5839:
	.cfi_startproc
	movq	%rcx, %r10
	cmpl	$-64, %esi
	jl	.L2
	negl	%esi
	movl	%esi, %ecx
	testl	%edx, %edx
	jle	.L33
	testq	%rdi, %rdi
	je	.L46
	movl	%esi, %r11d
	movl	(%r8), %eax
	subl	%edx, %r11d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L8:
	testq	%rdi, %rdi
	je	.L46
.L6:
	leaq	(%rdi,%rdi,4), %rdi
	subl	$1, %ecx
	cltq
	movq	%rdi, %rdx
	shrq	%cl, %rdx
	leal	48(%rdx), %esi
	movslq	%edx, %rdx
	movb	%sil, (%r10,%rax)
	movl	(%r8), %eax
	salq	%cl, %rdx
	subq	%rdx, %rdi
	addl	$1, %eax
	movl	%eax, (%r8)
	cmpl	%r11d, %ecx
	jne	.L8
.L3:
	testl	%r11d, %r11d
	jle	.L46
	subl	$1, %r11d
	btq	%r11, %rdi
	jnc	.L46
	movl	(%r8), %eax
	testl	%eax, %eax
	je	.L49
	subl	$1, %eax
	cltq
	addb	$1, (%r10,%rax)
	movl	(%r8), %ecx
	leal	-1(%rcx), %eax
	testl	%eax, %eax
	jle	.L12
	movslq	%ecx, %rdx
	cltq
	subl	$2, %ecx
	leaq	-2(%r10,%rdx), %rdx
	addq	%r10, %rax
	subq	%rcx, %rdx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L50:
	addb	$1, -1(%rax)
	subq	$1, %rax
	movb	$48, 1(%rax)
	cmpq	%rax, %rdx
	je	.L12
.L13:
	cmpb	$58, (%rax)
	je	.L50
.L46:
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-64, %ecx
	subl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpl	$64, %ecx
	je	.L51
	movq	%rdi, %rax
	subl	$-128, %esi
	shrq	%cl, %rax
	movl	%esi, %ecx
	salq	%cl, %rdi
.L17:
	testl	%edx, %edx
	jle	.L52
	movl	$128, %r11d
	movl	$128, %ecx
	movl	$64, %ebx
	subl	%edx, %r11d
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%rax, %rdx
	leal	-1(%rcx), %esi
	orq	%rdi, %rdx
	jne	.L53
	xorl	%edi, %edi
.L18:
	subl	$65, %ecx
	cmpl	$63, %esi
	jg	.L15
	movl	%esi, %ecx
	shrq	%cl, %rdi
	andl	$1, %edi
.L24:
	cmpl	$1, %edi
	je	.L54
.L1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movl	%edi, %edx
	shrq	$32, %rdi
	leaq	(%rdx,%rdx,4), %r12
	leaq	(%rdi,%rdi,4), %rdx
	movq	%r12, %r13
	movl	%r12d, %edi
	movl	%eax, %r12d
	shrq	$32, %rax
	shrq	$32, %r13
	leaq	(%r12,%r12,4), %r12
	leaq	(%rax,%rax,4), %rax
	addq	%r13, %rdx
	movq	%rdx, %r13
	shrq	$32, %rdx
	addq	%r12, %rdx
	salq	$32, %r13
	movq	%rdx, %r12
	movl	%edx, %edx
	addq	%r13, %rdi
	shrq	$32, %r12
	addq	%r12, %rax
	salq	$32, %rax
	addq	%rdx, %rax
	cmpl	$63, %esi
	jle	.L19
	subl	$65, %ecx
	movq	%rax, %rdx
	shrq	%cl, %rdx
	movslq	%edx, %r12
	addl	$48, %edx
	salq	%cl, %r12
	movslq	(%r8), %rcx
	subq	%r12, %rax
	movb	%dl, (%r10,%rcx)
	addl	$1, (%r8)
	cmpl	%esi, %r11d
	je	.L55
	movl	%esi, %ecx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L19:
	movl	%esi, %ecx
	movq	%rdi, %rdx
	movslq	(%r8), %r12
	shrq	%cl, %rdx
	movq	%rdx, %r13
	salq	%cl, %r13
	movl	%ebx, %ecx
	subl	%esi, %ecx
	subq	%r13, %rdi
	salq	%cl, %rax
	leal	48(%rax,%rdx), %eax
	movb	%al, (%r10,%r12)
	addl	$1, (%r8)
	cmpl	%esi, %r11d
	je	.L37
	xorl	%eax, %eax
	movl	%esi, %ecx
	jmp	.L22
.L52:
	movq	%rax, %rdx
	movl	$63, %ecx
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rdx, %rdi
	shrq	%cl, %rdi
	andl	$1, %edi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L51:
	xorl	%eax, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L54:
	movl	(%r8), %eax
	testl	%eax, %eax
	je	.L56
	subl	$1, %eax
	cltq
	addb	$1, (%r10,%rax)
	movl	(%r8), %ecx
	leal	-1(%rcx), %eax
	testl	%eax, %eax
	jle	.L31
	movslq	%ecx, %rdx
	cltq
	subl	$2, %ecx
	leaq	-2(%r10,%rdx), %rdx
	addq	%r10, %rax
	subq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L32:
	cmpb	$58, (%rax)
	jne	.L1
	addb	$1, -1(%rax)
	subq	$1, %rax
	movb	$48, 1(%rax)
	cmpq	%rax, %rdx
	jne	.L32
.L31:
	cmpb	$58, (%r10)
	jne	.L1
	movb	$49, (%r10)
	addl	$1, (%r9)
	jmp	.L1
.L56:
	movb	$49, (%r10)
	movl	$1, (%r9)
	movl	$1, (%r8)
	jmp	.L1
.L55:
	movq	%rax, %rdx
.L20:
	leal	-1(%r11), %esi
	movl	%r11d, %ecx
	jmp	.L18
.L12:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	cmpb	$58, (%r10)
	jne	.L46
	movb	$49, (%r10)
	addl	$1, (%r9)
	ret
.L49:
	movb	$49, (%r10)
	movl	$1, (%r9)
	movl	$1, (%r8)
	ret
.L33:
	movl	%esi, %r11d
	jmp	.L3
.L37:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	xorl	%edx, %edx
	jmp	.L20
	.cfi_endproc
.LFE5839:
	.size	_ZN2v88internalL15FillFractionalsEmiiNS0_6VectorIcEEPiS3_.isra.0, .-_ZN2v88internalL15FillFractionalsEmiiNS0_6VectorIcEEPiS3_.isra.0
	.section	.text._ZN2v88internalL12FillDigits64EmNS0_6VectorIcEEPi.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL12FillDigits64EmNS0_6VectorIcEEPi.isra.0, @function
_ZN2v88internalL12FillDigits64EmNS0_6VectorIcEEPi.isra.0:
.LFB5837:
	.cfi_startproc
	movabsq	$-2972493582642298179, %r11
	movq	%rdi, %rax
	movq	%rdx, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	mulq	%r11
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %r10
	shrq	$23, %rdx
	shrq	$23, %r10
	movq	%rdx, %r9
	imulq	$10000000, %r10, %r10
	subq	%r10, %rax
	movq	%rax, %r10
	movl	%eax, %r8d
	movq	%rdx, %rax
	mulq	%r11
	movabsq	$99999999999999, %r11
	movq	%rdx, %rax
	shrq	$23, %rax
	imulq	$10000000, %rax, %rax
	subq	%rax, %r9
	cmpq	%r11, %rdi
	ja	.L79
	xorl	%edi, %edi
	testq	%r9, %r9
	jne	.L80
	testq	%r10, %r10
	je	.L67
	movl	$3435973837, %r9d
	.p2align 4,,10
	.p2align 3
.L68:
	movl	%r8d, %eax
	movl	%r8d, %ebx
	movl	%r8d, %r10d
	imulq	%r9, %rax
	shrq	$35, %rax
	movl	%eax, %edx
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	movl	%edx, %r8d
	movl	(%rcx), %edx
	subl	%eax, %ebx
	addl	%edi, %edx
	movl	%ebx, %eax
	addl	$1, %edi
	movslq	%edx, %rdx
	addl	$48, %eax
	movb	%al, (%rsi,%rdx)
	cmpl	$9, %r10d
	ja	.L68
.L67:
	movslq	(%rcx), %rax
	leal	(%rdi,%rax), %r8d
	leal	-1(%r8), %edx
	cmpl	%edx, %eax
	jge	.L69
	movslq	%edx, %rdx
	.p2align 4,,10
	.p2align 3
.L70:
	movzbl	(%rsi,%rax), %r8d
	movzbl	(%rsi,%rdx), %r9d
	movb	%r9b, (%rsi,%rax)
	addq	$1, %rax
	movb	%r8b, (%rsi,%rdx)
	subq	$1, %rdx
	cmpl	%eax, %edx
	jg	.L70
	addl	(%rcx), %edi
	movl	%edi, %r8d
.L69:
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movl	$3435973837, %r10d
	.p2align 4,,10
	.p2align 3
.L64:
	movl	%r9d, %eax
	movl	%r9d, %ebx
	movl	%r9d, %r11d
	imulq	%r10, %rax
	shrq	$35, %rax
	movl	%eax, %edx
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	movl	%edx, %r9d
	movl	(%rcx), %edx
	subl	%eax, %ebx
	addl	%edi, %edx
	movl	%ebx, %eax
	addl	$1, %edi
	movslq	%edx, %rdx
	addl	$48, %eax
	movb	%al, (%rsi,%rdx)
	cmpl	$9, %r11d
	ja	.L64
	movslq	(%rcx), %rax
	leal	(%rdi,%rax), %r9d
	leal	-1(%r9), %edx
	cmpl	%edx, %eax
	jge	.L65
	movslq	%edx, %rdx
	.p2align 4,,10
	.p2align 3
.L66:
	movzbl	(%rsi,%rax), %r9d
	movzbl	(%rsi,%rdx), %r10d
	movb	%r10b, (%rsi,%rax)
	addq	$1, %rax
	movb	%r9b, (%rsi,%rdx)
	subq	$1, %rdx
	cmpl	%eax, %edx
	jg	.L66
	addl	(%rcx), %edi
	movl	%edi, %r9d
.L65:
	movl	%r9d, (%rcx)
	addl	$6, %r9d
	movl	%r8d, %eax
	movslq	%r9d, %rdx
	movl	$3435973837, %r9d
	imulq	%r9, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %edi
	addl	%edi, %edi
	subl	%edi, %r8d
	addl	$48, %r8d
	movb	%r8b, (%rsi,%rdx)
	movl	%eax, %edx
	movl	(%rcx), %ebx
	imulq	%r9, %rdx
	leal	5(%rbx), %edi
	movslq	%edi, %rdi
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %r8d
	addl	%r8d, %r8d
	subl	%r8d, %eax
	addl	$48, %eax
	movb	%al, (%rsi,%rdi)
	movl	(%rcx), %eax
	leal	4(%rax), %edi
	movl	%edx, %eax
	imulq	%r9, %rax
	movslq	%edi, %rdi
	shrq	$35, %rax
	leal	(%rax,%rax,4), %r8d
	addl	%r8d, %r8d
	subl	%r8d, %edx
	addl	$48, %edx
	movb	%dl, (%rsi,%rdi)
	movl	%eax, %edx
	movl	(%rcx), %ebx
	imulq	%r9, %rdx
	leal	3(%rbx), %edi
	movslq	%edi, %rdi
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %r8d
	addl	%r8d, %r8d
	subl	%r8d, %eax
	addl	$48, %eax
	movb	%al, (%rsi,%rdi)
	movl	(%rcx), %eax
	leal	2(%rax), %r8d
	movl	%edx, %eax
	imulq	%r9, %rax
	movslq	%r8d, %r8
	shrq	$35, %rax
	leal	(%rax,%rax,4), %edi
	addl	%edi, %edi
	subl	%edi, %edx
	leal	48(%rdx), %edi
	movl	%eax, %edx
	imulq	%r9, %rdx
	movb	%dil, (%rsi,%r8)
	movl	(%rcx), %ebx
	leal	1(%rbx), %edi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrq	$35, %rdx
	movslq	%edi, %rdi
	leal	(%rdx,%rdx,4), %r8d
	addl	$48, %edx
	addl	%r8d, %r8d
	subl	%r8d, %eax
	addl	$48, %eax
	movb	%al, (%rsi,%rdi)
	movslq	(%rcx), %rax
	movb	%dl, (%rsi,%rax)
	addl	$7, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	shrq	$23, %rdx
	xorl	%edi, %edi
	movl	$3435973837, %r11d
	.p2align 4,,10
	.p2align 3
.L59:
	movl	%edx, %eax
	movl	%edx, %ebx
	imulq	%r11, %rax
	shrq	$35, %rax
	movl	%eax, %r10d
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	subl	%eax, %ebx
	movl	%ebx, %eax
	movl	%edx, %ebx
	movl	%r10d, %edx
	movl	(%rcx), %r10d
	addl	$48, %eax
	addl	%edi, %r10d
	addl	$1, %edi
	movslq	%r10d, %r10
	movb	%al, (%rsi,%r10)
	cmpl	$9, %ebx
	ja	.L59
	movslq	(%rcx), %rax
	leal	(%rdi,%rax), %r10d
	leal	-1(%r10), %edx
	cmpl	%edx, %eax
	jge	.L60
	movslq	%edx, %rdx
	.p2align 4,,10
	.p2align 3
.L61:
	movzbl	(%rsi,%rax), %r10d
	movzbl	(%rsi,%rdx), %r11d
	movb	%r11b, (%rsi,%rax)
	addq	$1, %rax
	movb	%r10b, (%rsi,%rdx)
	subq	$1, %rdx
	cmpl	%eax, %edx
	jg	.L61
	addl	(%rcx), %edi
	movl	%edi, %r10d
.L60:
	movl	$3435973837, %edx
	movl	%r9d, %eax
	movl	%r10d, (%rcx)
	addl	$6, %r10d
	imulq	%rdx, %rax
	movslq	%r10d, %r10
	shrq	$35, %rax
	leal	(%rax,%rax,4), %edi
	addl	%edi, %edi
	subl	%edi, %r9d
	movl	%eax, %edi
	imulq	%rdx, %rdi
	addl	$48, %r9d
	movb	%r9b, (%rsi,%r10)
	movl	(%rcx), %ebx
	shrq	$35, %rdi
	leal	5(%rbx), %r9d
	leal	(%rdi,%rdi,4), %r10d
	movslq	%r9d, %r9
	addl	%r10d, %r10d
	subl	%r10d, %eax
	addl	$48, %eax
	movb	%al, (%rsi,%r9)
	movl	(%rcx), %eax
	leal	4(%rax), %r9d
	movl	%edi, %eax
	imulq	%rdx, %rax
	movslq	%r9d, %r9
	shrq	$35, %rax
	leal	(%rax,%rax,4), %r10d
	addl	%r10d, %r10d
	subl	%r10d, %edi
	addl	$48, %edi
	movb	%dil, (%rsi,%r9)
	movl	%eax, %edi
	movl	(%rcx), %ebx
	imulq	%rdx, %rdi
	leal	3(%rbx), %r9d
	movslq	%r9d, %r9
	shrq	$35, %rdi
	leal	(%rdi,%rdi,4), %r10d
	addl	%r10d, %r10d
	subl	%r10d, %eax
	addl	$48, %eax
	movb	%al, (%rsi,%r9)
	movl	(%rcx), %eax
	leal	2(%rax), %r9d
	movl	%edi, %eax
	imulq	%rdx, %rax
	movslq	%r9d, %r9
	shrq	$35, %rax
	leal	(%rax,%rax,4), %r10d
	addl	%r10d, %r10d
	subl	%r10d, %edi
	addl	$48, %edi
	movb	%dil, (%rsi,%r9)
	movl	%eax, %edi
	movl	(%rcx), %ebx
	imulq	%rdx, %rdi
	leal	1(%rbx), %r9d
	movslq	%r9d, %r9
	shrq	$35, %rdi
	leal	(%rdi,%rdi,4), %r10d
	addl	$48, %edi
	addl	%r10d, %r10d
	subl	%r10d, %eax
	addl	$48, %eax
	movb	%al, (%rsi,%r9)
	movslq	(%rcx), %rax
	movb	%dil, (%rsi,%rax)
	movl	(%rcx), %eax
	leal	7(%rax), %edi
	addl	$13, %eax
	movl	%edi, (%rcx)
	movslq	%eax, %rdi
	movl	%r8d, %eax
	imulq	%rdx, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %r9d
	addl	%r9d, %r9d
	subl	%r9d, %r8d
	addl	$48, %r8d
	movb	%r8b, (%rsi,%rdi)
	movl	%eax, %edi
	movl	(%rcx), %ebx
	imulq	%rdx, %rdi
	leal	5(%rbx), %r8d
	movslq	%r8d, %r8
	shrq	$35, %rdi
	leal	(%rdi,%rdi,4), %r9d
	addl	%r9d, %r9d
	subl	%r9d, %eax
	addl	$48, %eax
	movb	%al, (%rsi,%r8)
	movl	(%rcx), %eax
	leal	4(%rax), %r8d
	movl	%edi, %eax
	imulq	%rdx, %rax
	movslq	%r8d, %r8
	shrq	$35, %rax
	leal	(%rax,%rax,4), %r9d
	addl	%r9d, %r9d
	subl	%r9d, %edi
	addl	$48, %edi
	movb	%dil, (%rsi,%r8)
	movl	%eax, %edi
	movl	(%rcx), %ebx
	imulq	%rdx, %rdi
	leal	3(%rbx), %r8d
	movslq	%r8d, %r8
	shrq	$35, %rdi
	leal	(%rdi,%rdi,4), %r9d
	addl	%r9d, %r9d
	subl	%r9d, %eax
	addl	$48, %eax
	movb	%al, (%rsi,%r8)
	movl	(%rcx), %eax
	leal	2(%rax), %r8d
	movl	%edi, %eax
	imulq	%rdx, %rax
	movslq	%r8d, %r8
	shrq	$35, %rax
	leal	(%rax,%rax,4), %r9d
	addl	%r9d, %r9d
	subl	%r9d, %edi
	addl	$48, %edi
	movb	%dil, (%rsi,%r8)
	movl	%eax, %edi
	movl	(%rcx), %ebx
	imulq	%rdi, %rdx
	leal	1(%rbx), %r8d
	popq	%rbx
	movslq	%r8d, %r8
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %edi
	addl	$48, %edx
	addl	%edi, %edi
	subl	%edi, %eax
	addl	$48, %eax
	movb	%al, (%rsi,%r8)
	movslq	(%rcx), %rax
	movb	%dl, (%rsi,%rax)
	addl	$7, (%rcx)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5837:
	.size	_ZN2v88internalL12FillDigits64EmNS0_6VectorIcEEPi.isra.0, .-_ZN2v88internalL12FillDigits64EmNS0_6VectorIcEEPi.isra.0
	.section	.text._ZN2v88internal13FastFixedDtoaEdiNS0_6VectorIcEEPiS3_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal13FastFixedDtoaEdiNS0_6VectorIcEEPiS3_
	.type	_ZN2v88internal13FastFixedDtoaEdiNS0_6VectorIcEEPiS3_, @function
_ZN2v88internal13FastFixedDtoaEdiNS0_6VectorIcEEPiS3_:
.LFB5058:
	.cfi_startproc
	endbr64
	movabsq	$9218868437227405312, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%xmm0, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	testq	%rdx, %rax
	jne	.L125
	xorl	%eax, %eax
	cmpl	$20, %edi
	jg	.L81
	.p2align 4,,10
	.p2align 3
.L109:
	movl	%r13d, %eax
	movb	$0, (%rbx)
	negl	%eax
	movl	$0, (%r12)
	movl	%eax, (%r14)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%xmm0, %rsi
	shrq	$52, %rsi
	andl	$2047, %esi
	leal	-1075(%rsi), %r15d
	cmpl	$20, %r15d
	jg	.L110
	cmpl	$20, %edi
	jg	.L110
	movabsq	$4503599627370495, %rdx
	movl	$0, (%rcx)
	andq	%rdx, %rax
	addq	$1, %rdx
	addq	%rdx, %rax
	cmpl	$11, %r15d
	jle	.L84
	cmpl	$17, %r15d
	jg	.L126
	movabsq	$762939453125, %rdx
	movl	$17, %ecx
	subl	%r15d, %ecx
	salq	%cl, %rdx
	movq	%rdx, %rcx
	xorl	%edx, %edx
	divq	%rcx
	movl	%r15d, %ecx
	salq	%cl, %rdx
	movq	%rdx, %rcx
.L86:
	movl	%eax, %r8d
	movl	$3435973837, %edi
	movl	$1, %esi
	imulq	%rdi, %r8
	shrq	$35, %r8
	leal	(%r8,%r8,4), %edx
	addl	%edx, %edx
	subl	%edx, %eax
	addl	$48, %eax
	movb	%al, (%rbx)
	.p2align 4,,10
	.p2align 3
.L87:
	movl	%r8d, %eax
	movl	%r8d, %r10d
	movl	%r8d, %r9d
	imulq	%rdi, %rax
	shrq	$35, %rax
	movl	%eax, %edx
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	movl	%edx, %r8d
	movl	(%r12), %edx
	subl	%eax, %r10d
	addl	%esi, %edx
	movl	%r10d, %eax
	addl	$1, %esi
	movslq	%edx, %rdx
	addl	$48, %eax
	movb	%al, (%rbx,%rdx)
	cmpl	$9, %r9d
	ja	.L87
	movslq	(%r12), %rax
	leal	(%rsi,%rax), %edi
	leal	-1(%rdi), %edx
	cmpl	%edx, %eax
	jge	.L88
	movslq	%edx, %rdx
	.p2align 4,,10
	.p2align 3
.L89:
	movzbl	(%rbx,%rax), %edi
	movzbl	(%rbx,%rdx), %r8d
	movb	%r8b, (%rbx,%rax)
	addq	$1, %rax
	movb	%dil, (%rbx,%rdx)
	subq	$1, %rdx
	cmpl	%eax, %edx
	jg	.L89
	addl	(%r12), %esi
	movl	%esi, %edi
.L88:
	movabsq	$-2972493582642298179, %r8
	movq	%rcx, %rax
	movl	%edi, (%r12)
	addl	$2, %edi
	mulq	%r8
	movslq	%edi, %rdi
	movq	%rdx, %rsi
	shrq	$23, %rsi
	imulq	$10000000, %rsi, %rax
	subq	%rax, %rcx
	movq	%rsi, %rax
	mulq	%r8
	shrq	$23, %rdx
	imulq	$10000000, %rdx, %rax
	movl	%edx, %r8d
	subq	%rax, %rsi
	movq	%rsi, %rax
	movl	$3435973837, %esi
	imulq	%rsi, %r8
	shrq	$35, %r8
	leal	(%r8,%r8,4), %r9d
	addl	%r9d, %r9d
	subl	%r9d, %edx
	addl	$48, %edx
	movb	%dl, (%rbx,%rdi)
	movl	%r8d, %edx
	movl	(%r12), %edi
	imulq	%rsi, %rdx
	addl	$1, %edi
	movslq	%edi, %rdi
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %r9d
	addl	%r9d, %r9d
	subl	%r9d, %r8d
	addl	$48, %r8d
	movb	%r8b, (%rbx,%rdi)
	movl	%edx, %edi
	movslq	(%r12), %r8
	imulq	%rsi, %rdi
	shrq	$35, %rdi
	leal	(%rdi,%rdi,4), %edi
	addl	%edi, %edi
	subl	%edi, %edx
	addl	$48, %edx
	movb	%dl, (%rbx,%r8)
	movl	%eax, %r8d
	movl	(%r12), %edx
	imulq	%rsi, %r8
	leal	3(%rdx), %edi
	addl	$9, %edx
	movl	%edi, (%r12)
	movslq	%edx, %rdx
	shrq	$35, %r8
	leal	(%r8,%r8,4), %edi
	addl	%edi, %edi
	subl	%edi, %eax
	addl	$48, %eax
	movb	%al, (%rbx,%rdx)
	movl	(%r12), %eax
	movl	%r8d, %edx
	addl	$5, %eax
	imulq	%rsi, %rdx
	cltq
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %edi
	addl	%edi, %edi
	subl	%edi, %r8d
	addl	$48, %r8d
	movb	%r8b, (%rbx,%rax)
	movl	%edx, %r8d
	movl	(%r12), %eax
	imulq	%rsi, %r8
	addl	$4, %eax
	cltq
	shrq	$35, %r8
	leal	(%r8,%r8,4), %edi
	addl	%edi, %edi
	subl	%edi, %edx
	addl	$48, %edx
	movb	%dl, (%rbx,%rax)
	movl	%r8d, %edx
	movl	(%r12), %eax
	imulq	%rsi, %rdx
	addl	$3, %eax
	cltq
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %edi
	addl	%edi, %edi
	subl	%edi, %r8d
	movl	%edx, %edi
	imulq	%rsi, %rdi
	addl	$48, %r8d
	movb	%r8b, (%rbx,%rax)
	movl	(%r12), %eax
	shrq	$35, %rdi
	addl	$2, %eax
	leal	(%rdi,%rdi,4), %r8d
	cltq
	addl	%r8d, %r8d
	subl	%r8d, %edx
	addl	$48, %edx
	movb	%dl, (%rbx,%rax)
	movl	(%r12), %eax
	leal	1(%rax), %edx
	movl	%edi, %eax
	imulq	%rsi, %rax
	movslq	%edx, %rdx
	shrq	$35, %rax
	leal	(%rax,%rax,4), %r8d
	addl	$48, %eax
	addl	%r8d, %r8d
	subl	%r8d, %edi
	movl	%ecx, %r8d
	addl	$48, %edi
	imulq	%rsi, %r8
	movb	%dil, (%rbx,%rdx)
	movslq	(%r12), %rdx
	movb	%al, (%rbx,%rdx)
	movl	(%r12), %eax
	shrq	$35, %r8
	leal	7(%rax), %edx
	addl	$13, %eax
	movl	%edx, (%r12)
	movslq	%eax, %r9
	movl	%r8d, %edx
	leal	(%r8,%r8,4), %eax
	addl	%eax, %eax
	imulq	%rsi, %rdx
	subl	%eax, %ecx
	addl	$48, %ecx
	shrq	$35, %rdx
	movb	%cl, (%rbx,%r9)
	movl	(%r12), %eax
	leal	(%rdx,%rdx,4), %ecx
	addl	%ecx, %ecx
	addl	$5, %eax
	subl	%ecx, %r8d
	cltq
	addl	$48, %r8d
	movb	%r8b, (%rbx,%rax)
	movl	(%r12), %eax
	leal	4(%rax), %ecx
	movl	%edx, %eax
	imulq	%rsi, %rax
	movslq	%ecx, %rcx
	shrq	$35, %rax
	leal	(%rax,%rax,4), %edi
	addl	%edi, %edi
	subl	%edi, %edx
	addl	$48, %edx
	movb	%dl, (%rbx,%rcx)
	movl	%eax, %edx
	movl	(%r12), %ecx
	imulq	%rsi, %rdx
	addl	$3, %ecx
	movslq	%ecx, %rcx
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %edi
	addl	%edi, %edi
	subl	%edi, %eax
	addl	$48, %eax
	movb	%al, (%rbx,%rcx)
	movl	(%r12), %eax
	leal	2(%rax), %ecx
	movl	%edx, %eax
	imulq	%rsi, %rax
	movslq	%ecx, %rcx
	shrq	$35, %rax
	leal	(%rax,%rax,4), %edi
	addl	%edi, %edi
	subl	%edi, %edx
	addl	$48, %edx
	movb	%dl, (%rbx,%rcx)
	movl	%eax, %edx
	movl	(%r12), %ecx
	imulq	%rdx, %rsi
	addl	$1, %ecx
	movslq	%ecx, %rcx
	shrq	$35, %rsi
	leal	(%rsi,%rsi,4), %edx
	addl	$48, %esi
	addl	%edx, %edx
	subl	%edx, %eax
	addl	$48, %eax
	movb	%al, (%rbx,%rcx)
	movslq	(%r12), %rax
	movb	%sil, (%rbx,%rax)
	movl	(%r12), %eax
	addl	$7, %eax
	movl	%eax, (%r12)
	movl	%eax, (%r14)
.L93:
	movslq	(%r12), %rdx
	movq	%rdx, %rax
	leaq	-1(%rbx,%rdx), %rcx
	testl	%edx, %edx
	jg	.L103
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L127:
	movl	%eax, (%r12)
	subq	$1, %rcx
	testl	%eax, %eax
	je	.L91
.L103:
	movslq	%eax, %rdx
	subl	$1, %eax
	cmpb	$48, (%rcx)
	movq	%rcx, %rsi
	je	.L127
	movq	%rbx, %rax
	xorl	%ecx, %ecx
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L104:
	addl	$1, %ecx
	addq	$1, %rax
	cmpl	%edx, %ecx
	je	.L105
.L106:
	cmpb	$48, (%rax)
	je	.L104
	testl	%ecx, %ecx
	je	.L124
.L105:
	cmpl	%edx, %ecx
	jge	.L107
	movslq	%ecx, %rax
	movq	%rbx, %rsi
	subq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L108:
	movzbl	(%rbx,%rax), %edx
	movb	%dl, (%rsi,%rax)
	movl	(%r12), %edx
	addq	$1, %rax
	cmpl	%eax, %edx
	jg	.L108
.L107:
	subl	%ecx, %edx
	movl	%edx, (%r12)
	subl	%ecx, (%r14)
	movslq	(%r12), %rdx
.L124:
	leaq	(%rbx,%rdx), %rsi
.L91:
	movb	$0, (%rsi)
	movl	(%r12), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L81
	negl	%r13d
	movl	%r13d, (%r14)
.L81:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	testl	%r15d, %r15d
	jns	.L128
	cmpl	$-52, %r15d
	jl	.L94
	movl	$1075, %ecx
	movq	%rax, %rdi
	subl	%esi, %ecx
	shrq	%cl, %rdi
	movq	%rdi, %rdx
	salq	%cl, %rdx
	subq	%rdx, %rax
	movl	$4294967295, %edx
	cmpq	%rdx, %rdi
	jbe	.L95
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internalL12FillDigits64EmNS0_6VectorIcEEPi.isra.0
	movl	(%r12), %edi
	movq	-56(%rbp), %rax
.L96:
	movl	%edi, (%r14)
.L123:
	movq	%r14, %r9
	movq	%r12, %r8
	movq	%rbx, %rcx
	movl	%r13d, %edx
	movl	%r15d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internalL15FillFractionalsEmiiNS0_6VectorIcEEPiS3_.isra.0
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L126:
	movabsq	$6646139978924579365, %rdx
	leal	-1092(%rsi), %ecx
	movabsq	$762939453125, %rsi
	salq	%cl, %rax
	movq	%rax, %rcx
	imulq	%rdx
	movq	%rcx, %rax
	sarq	$63, %rax
	sarq	$38, %rdx
	subq	%rax, %rdx
	movl	%edx, %eax
	imulq	%rsi, %rdx
	subq	%rdx, %rcx
	salq	$17, %rcx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L128:
	movl	%r15d, %ecx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	salq	%cl, %rax
	movq	%rax, %rdi
	call	_ZN2v88internalL12FillDigits64EmNS0_6VectorIcEEPi.isra.0
	movl	(%r12), %eax
	movl	%eax, (%r14)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L94:
	cmpl	$-128, %r15d
	jl	.L109
	movl	$0, (%r8)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L95:
	movl	%edi, %ecx
	movl	$3435973837, %r8d
	movl	%edi, %esi
	imulq	%r8, %rcx
	shrq	$35, %rcx
	leal	(%rcx,%rcx,4), %edx
	addl	%edx, %edx
	subl	%edx, %esi
	movl	%esi, %edx
	movl	$1, %esi
	addl	$48, %edx
	movb	%dl, (%rbx)
	cmpq	$9, %rdi
	jbe	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	movl	%ecx, %edx
	movl	%ecx, %r11d
	movl	%ecx, %r9d
	imulq	%r8, %rdx
	shrq	$35, %rdx
	movl	%edx, %edi
	leal	(%rdx,%rdx,4), %edx
	addl	%edx, %edx
	movl	%edi, %ecx
	movl	(%r12), %edi
	subl	%edx, %r11d
	addl	%esi, %edi
	movl	%r11d, %edx
	addl	$1, %esi
	movslq	%edi, %rdi
	addl	$48, %edx
	movb	%dl, (%rbx,%rdi)
	cmpl	$9, %r9d
	ja	.L98
.L97:
	movslq	(%r12), %rdx
	leal	(%rdx,%rsi), %edi
	leal	-1(%rdi), %ecx
	cmpl	%ecx, %edx
	jge	.L99
	movslq	%ecx, %rcx
	.p2align 4,,10
	.p2align 3
.L100:
	movzbl	(%rbx,%rdx), %edi
	movzbl	(%rbx,%rcx), %r8d
	movb	%r8b, (%rbx,%rdx)
	addq	$1, %rdx
	movb	%dil, (%rbx,%rcx)
	subq	$1, %rcx
	cmpl	%edx, %ecx
	jg	.L100
	addl	(%r12), %esi
	movl	%esi, %edi
.L99:
	movl	%edi, (%r12)
	jmp	.L96
	.cfi_endproc
.LFE5058:
	.size	_ZN2v88internal13FastFixedDtoaEdiNS0_6VectorIcEEPiS3_, .-_ZN2v88internal13FastFixedDtoaEdiNS0_6VectorIcEEPiS3_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal13FastFixedDtoaEdiNS0_6VectorIcEEPiS3_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal13FastFixedDtoaEdiNS0_6VectorIcEEPiS3_, @function
_GLOBAL__sub_I__ZN2v88internal13FastFixedDtoaEdiNS0_6VectorIcEEPiS3_:
.LFB5826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5826:
	.size	_GLOBAL__sub_I__ZN2v88internal13FastFixedDtoaEdiNS0_6VectorIcEEPiS3_, .-_GLOBAL__sub_I__ZN2v88internal13FastFixedDtoaEdiNS0_6VectorIcEEPiS3_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal13FastFixedDtoaEdiNS0_6VectorIcEEPiS3_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
