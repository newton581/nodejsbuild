	.file	"incremental-marking.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5049:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5049:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5050:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5050:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5052:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5052:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,"axG",@progbits,_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.type	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, @function
_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE:
.LFB7097:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7097:
	.size	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, .-_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB7101:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rdx), %rcx
	movq	32(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7101:
	.size	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.type	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, @function
_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_:
.LFB7102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r12), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	40(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE7102:
	.size	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, .-_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE:
.LFB7103:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7103:
	.size	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB7104:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7104:
	.size	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.type	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm, @function
_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm:
.LFB7105:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7105:
	.size	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm, .-_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.section	.text._ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB7106:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7106:
	.size	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB7107:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7107:
	.size	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal18AllocationObserver15GetNextStepSizeEv,"axG",@progbits,_ZN2v88internal18AllocationObserver15GetNextStepSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18AllocationObserver15GetNextStepSizeEv
	.type	_ZN2v88internal18AllocationObserver15GetNextStepSizeEv, @function
_ZN2v88internal18AllocationObserver15GetNextStepSizeEv:
.LFB7286:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE7286:
	.size	_ZN2v88internal18AllocationObserver15GetNextStepSizeEv, .-_ZN2v88internal18AllocationObserver15GetNextStepSizeEv
	.section	.text._ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_,"axG",@progbits,_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_
	.type	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_, @function
_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_:
.LFB26507:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	jnb	.L46
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L49
.L28:
	movq	(%rbx), %r12
	testb	$1, %r12b
	je	.L18
	movq	%r12, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L50
.L20:
	movl	%r12d, %eax
	movl	%r8d, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	leaq	(%rdx,%rax,4), %rdx
	sall	%cl, %esi
	.p2align 4,,10
	.p2align 3
.L24:
	movl	(%rdx), %ecx
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%esi, %eax
	je	.L18
	movl	%ecx, %edi
	movl	%ecx, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %ecx
	jne	.L24
	movq	16(%r14), %rdx
	movq	104(%rdx), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L51
	leaq	1(%rcx), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%rax)
	movq	%r12, 16(%rax,%rcx,8)
	cmpq	%rbx, %r13
	ja	.L28
.L49:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	%r15, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L21
	testb	$-128, %ah
	je	.L20
.L21:
	movq	%rbx, %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-56(%rbp), %rdx
	movl	$1, %r8d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	744(%rdx), %rdi
	movq	%rcx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	784(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 784(%rdx)
	movq	%rdx, -56(%rbp)
	movq	-64(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 104(%rdx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L18
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%r12, 16(%rsi,%rax,8)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE26507:
	.size	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_, .-_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_
	.section	.text._ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES7_,"axG",@progbits,_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES7_
	.type	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES7_, @function
_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES7_:
.LFB26511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	cmpq	%rcx, %rdx
	jnb	.L52
	movq	%rdi, %r14
	movq	%rcx, %r15
	movq	%rdx, %r12
	movl	$1, %r13d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L56:
	addq	$8, %r12
	cmpq	%r12, %r15
	jbe	.L52
.L53:
	movq	(%r12), %rbx
	movq	%rbx, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L54
	cmpq	$3, %rax
	jne	.L56
	cmpl	$3, %ebx
	je	.L56
	movq	%rbx, %rdx
	andq	$-3, %rbx
	andq	$-262144, %rdx
	movq	%rbx, %rcx
	movl	%r13d, %ebx
	subl	%edx, %ecx
	movq	16(%rdx), %rsi
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	sall	%cl, %ebx
	movl	(%rsi,%rax,4), %eax
	testl	%eax, %ebx
	je	.L66
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L56
	movq	-56(%rbp), %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L69
	testb	$-128, %ah
	je	.L56
.L69:
	movq	%r12, %rsi
	addq	$8, %r12
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	cmpq	%r12, %r15
	ja	.L53
.L52:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	%rbx, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L106
.L58:
	movl	%ebx, %eax
	movl	%r13d, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	leaq	(%rdx,%rax,4), %rdx
	sall	%cl, %esi
	.p2align 4,,10
	.p2align 3
.L62:
	movl	(%rdx), %ecx
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%esi, %eax
	je	.L56
	movl	%ecx, %edi
	movl	%ecx, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %ecx
	jne	.L62
	movq	16(%r14), %rdx
	movq	104(%rdx), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L107
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%rbx, 16(%rax,%rcx,8)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L106:
	movq	-56(%rbp), %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L59
	testb	$-128, %ah
	je	.L58
.L59:
	movq	%r12, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-64(%rbp), %rdx
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L66:
	movq	16(%r14), %rsi
	movq	5672(%rsi), %rbx
	movq	8(%rbx), %rax
	cmpq	$64, %rax
	je	.L108
	addq	$1, %rax
	movq	%rax, 8(%rbx)
	salq	$4, %rax
	addq	%rbx, %rax
	movq	-56(%rbp), %rbx
	movq	%r12, 8(%rax)
	movq	%rbx, (%rax)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L107:
	leaq	744(%rdx), %rdi
	movq	%rcx, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-64(%rbp), %rdx
	movq	-80(%rbp), %rax
	movq	784(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 784(%rdx)
	movq	%rdx, -64(%rbp)
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-88(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 104(%rdx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L56
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%rbx, 16(%rsi,%rax,8)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	6312(%rsi), %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-64(%rbp), %rsi
	movq	6352(%rsi), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 6352(%rsi)
	movq	%rsi, -64(%rbp)
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, 8(%rax)
	leaq	16(%rax), %rdx
	leaq	1040(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L72:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L72
	movq	%rax, 5672(%rsi)
	movq	8(%rax), %rdx
	cmpq	$64, %rdx
	je	.L56
	addq	$1, %rdx
	movq	-56(%rbp), %rbx
	movq	%rdx, 8(%rax)
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rbx, (%rax)
	movq	%r12, 8(%rax)
	jmp	.L56
	.cfi_endproc
.LFE26511:
	.size	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES7_, .-_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES7_
	.section	.text._ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB27462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdx), %r12
	testb	$1, %r12b
	jne	.L125
.L109:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	%r12, %r13
	movq	%rdi, %rbx
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testb	$64, %al
	jne	.L126
.L112:
	movl	%r12d, %edx
	movl	$1, %eax
	subl	%r13d, %edx
	movl	%edx, %ecx
	shrl	$8, %edx
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movq	16(%r13), %rax
	leaq	(%rax,%rdx,4), %rsi
	.p2align 4,,10
	.p2align 3
.L115:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L109
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L115
	movq	16(%rbx), %rdi
	addq	$8, %rsp
	movq	%r12, %rdx
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	addq	$104, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	andq	$-262144, %rsi
	movq	8(%rsi), %rax
	movq	%rsi, %rdi
	testb	$88, %al
	je	.L113
	testb	$-128, %ah
	je	.L112
.L113:
	movq	%rdx, %rsi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L112
	.cfi_endproc
.LFE27462:
	.size	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,"axG",@progbits,_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.type	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, @function
_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE:
.LFB27958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	subq	$24, %rsp
	movq	(%rdx), %r12
	movq	%r12, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L128
	cmpq	$3, %rax
	je	.L158
.L127:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	%r12, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testb	$64, %al
	jne	.L159
.L132:
	movl	%r12d, %eax
	movl	$1, %edx
	subl	%r13d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r13), %rcx
	leaq	(%rcx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L136:
	movl	(%rsi), %ecx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%edx, %eax
	je	.L127
	movl	%ecx, %edi
	movl	%ecx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L136
	movq	16(%rbx), %rdi
	addq	$24, %rsp
	movq	%r12, %rdx
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	addq	$104, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L133
	testb	$-128, %ah
	je	.L132
.L133:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L158:
	cmpl	$3, %r12d
	je	.L127
	movq	%r12, %rdx
	andq	$-3, %r12
	andq	$-262144, %rdx
	subl	%edx, %r12d
	movq	16(%rdx), %rcx
	movl	%r12d, %eax
	shrl	$3, %r12d
	shrl	$8, %eax
	movl	(%rcx,%rax,4), %eax
	btl	%r12d, %eax
	jnc	.L138
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L127
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L141
	testb	$-128, %ah
	je	.L127
.L141:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movq	16(%rbx), %r8
	movq	%rdi, %xmm0
	movq	%rsi, %xmm1
	xorl	%esi, %esi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	addq	$5672, %r8
	movq	-48(%rbp), %rdx
	movq	-40(%rbp), %rcx
	addq	$24, %rsp
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_
	.cfi_endproc
.LFE27958:
	.size	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, .-_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.section	.text._ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_,"axG",@progbits,_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_
	.type	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_, @function
_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_:
.LFB28234:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28234:
	.size	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_, .-_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_
	.section	.text._ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED2Ev,"axG",@progbits,_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED2Ev
	.type	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED2Ev, @function
_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED2Ev:
.LFB28621:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28621:
	.size	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED2Ev, .-_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED2Ev
	.weak	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED1Ev
	.set	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED1Ev,_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED2Ev
	.section	.text._ZN2v88internal36IncrementalMarkingRootMarkingVisitorD2Ev,"axG",@progbits,_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD2Ev
	.type	_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD2Ev, @function
_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD2Ev:
.LFB28625:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28625:
	.size	_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD2Ev, .-_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD2Ev
	.weak	_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD1Ev
	.set	_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD1Ev,_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD2Ev
	.section	.text._ZN2v88internal18IncrementalMarking8ObserverD2Ev,"axG",@progbits,_ZN2v88internal18IncrementalMarking8ObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18IncrementalMarking8ObserverD2Ev
	.type	_ZN2v88internal18IncrementalMarking8ObserverD2Ev, @function
_ZN2v88internal18IncrementalMarking8ObserverD2Ev:
.LFB28629:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28629:
	.size	_ZN2v88internal18IncrementalMarking8ObserverD2Ev, .-_ZN2v88internal18IncrementalMarking8ObserverD2Ev
	.weak	_ZN2v88internal18IncrementalMarking8ObserverD1Ev
	.set	_ZN2v88internal18IncrementalMarking8ObserverD1Ev,_ZN2v88internal18IncrementalMarking8ObserverD2Ev
	.section	.rodata._ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"address < start || address >= end"
	.section	.rodata._ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB28681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdx), %rax
	movq	%rdi, -56(%rbp)
	movslq	(%rax), %rbx
	addq	%rax, %rbx
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	leaq	4(%rbx), %r12
	movq	%rax, %r14
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %eax
	addq	%r14, %rax
	cmpq	%r12, %rax
	jbe	.L165
	cmpq	%r12, %r14
	jbe	.L187
.L165:
	subq	$59, %rbx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal20MarkCompactCollector15RecordRelocSlotENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE@PLT
	movl	%ebx, %eax
	movq	%rbx, %rsi
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%rsi), %rcx
	leaq	(%rcx,%rax,4), %rsi
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L189:
	movl	%ecx, %edi
	movl	%ecx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L188
.L167:
	movl	(%rsi), %ecx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%edx, %eax
	jne	.L189
.L164:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	movq	16(%rax), %r14
	movq	104(%r14), %r13
	movq	8(%r13), %r12
	cmpq	$64, %r12
	je	.L190
	leaq	1(%r12), %rax
	movq	%rax, 8(%r13)
	movq	%rbx, 16(%r13,%r12,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L190:
	leaq	744(%r14), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	784(%r14), %rax
	movq	%r15, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 784(%r14)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r12, %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 104(%r14)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L164
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%rbx, 16(%rdx,%rax,8)
	jmp	.L164
	.cfi_endproc
.LFE28681:
	.size	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE:
.LFB28682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	movq	(%rdx), %rax
	movq	(%rax), %r13
	movq	%r13, %rdx
	call	_ZN2v88internal20MarkCompactCollector15RecordRelocSlotENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE@PLT
	movl	%r13d, %eax
	movq	%r13, %rdi
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %rdi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	leaq	0(,%rax,4), %rsi
	sall	%cl, %edx
	movq	16(%rdi), %rcx
	movl	(%rcx,%rax,4), %eax
	testl	%edx, %eax
	je	.L214
.L191:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	testb	$62, 43(%r12)
	je	.L215
.L193:
	addq	16(%rdi), %rsi
	.p2align 4,,10
	.p2align 3
.L200:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L191
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L200
	movq	16(%rbx), %rdi
	addq	$8, %rsp
	movq	%r13, %rdx
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	addq	$104, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	31(%r12), %rax
	movl	15(%rax), %eax
	testb	$8, %al
	je	.L193
	movq	-1(%r13), %rax
	movzwl	11(%rax), %eax
	cmpw	$68, %ax
	je	.L216
	cmpw	$159, %ax
	je	.L195
	cmpw	$1023, %ax
	ja	.L195
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L193
	.p2align 4,,10
	.p2align 3
.L195:
	movq	16(%rbx), %r14
	movq	6368(%r14), %rbx
	movq	8(%rbx), %rax
	cmpq	$64, %rax
	je	.L217
	addq	$1, %rax
	movq	%rax, 8(%rbx)
	salq	$4, %rax
	addq	%rbx, %rax
	movq	%r13, (%rax)
	movq	%r12, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	cmpw	$1024, 11(%r13)
	jbe	.L193
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L217:
	leaq	7008(%r14), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	7048(%r14), %rax
	movq	%r15, %rdi
	movq	%rax, (%rbx)
	movq	%rbx, 7048(%r14)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 6368(%r14)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L191
	addq	$1, %rax
	movq	%rax, 8(%rdx)
	salq	$4, %rax
	addq	%rax, %rdx
	movq	%r13, (%rdx)
	movq	%r12, 8(%rdx)
	jmp	.L191
	.cfi_endproc
.LFE28682:
	.size	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal36IncrementalMarkingRootMarkingVisitorD0Ev,"axG",@progbits,_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD0Ev
	.type	_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD0Ev, @function
_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD0Ev:
.LFB28627:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28627:
	.size	_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD0Ev, .-_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD0Ev
	.section	.text._ZN2v88internal18IncrementalMarking8ObserverD0Ev,"axG",@progbits,_ZN2v88internal18IncrementalMarking8ObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18IncrementalMarking8ObserverD0Ev
	.type	_ZN2v88internal18IncrementalMarking8ObserverD0Ev, @function
_ZN2v88internal18IncrementalMarking8ObserverD0Ev:
.LFB28631:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28631:
	.size	_ZN2v88internal18IncrementalMarking8ObserverD0Ev, .-_ZN2v88internal18IncrementalMarking8ObserverD0Ev
	.section	.text._ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED0Ev,"axG",@progbits,_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED0Ev
	.type	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED0Ev, @function
_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED0Ev:
.LFB28623:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28623:
	.size	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED0Ev, .-_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED0Ev
	.section	.text._ZN2v88internal36IncrementalMarkingRootMarkingVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_,"axG",@progbits,_ZN2v88internal36IncrementalMarkingRootMarkingVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal36IncrementalMarkingRootMarkingVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.type	_ZN2v88internal36IncrementalMarkingRootMarkingVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, @function
_ZN2v88internal36IncrementalMarkingRootMarkingVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_:
.LFB22362:
	.cfi_startproc
	endbr64
	cmpq	%r8, %rcx
	jnb	.L242
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$1, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	.p2align 4,,10
	.p2align 3
.L230:
	movq	(%rbx), %r12
	testb	$1, %r12b
	je	.L224
	movq	8(%r14), %rax
	movq	%r12, %rdx
	movl	%r15d, %esi
	andq	$-262144, %rdx
	movq	2064(%rax), %rdi
	movl	%r12d, %eax
	movq	16(%rdx), %rdx
	andl	$262143, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L246:
	movl	%esi, %r8d
	movl	%edx, %eax
	orl	%edx, %r8d
	lock cmpxchgl	%r8d, (%rcx)
	cmpl	%eax, %edx
	je	.L245
.L226:
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	jne	.L246
.L224:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	ja	.L230
.L248:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movq	8(%rdi), %rdx
	movq	(%rdx), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L247
	leaq	1(%rcx), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%rax)
	movq	%r12, 16(%rax,%rcx,8)
	cmpq	%rbx, %r13
	ja	.L230
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L247:
	leaq	640(%rdx), %rdi
	movq	%rcx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	680(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 680(%rdx)
	movq	%rdx, -56(%rbp)
	movq	-64(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rdx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L224
	leaq	1(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%rsi)
	movq	%r12, 16(%rsi,%rax,8)
	cmpq	%rbx, %r13
	ja	.L230
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE22362:
	.size	_ZN2v88internal36IncrementalMarkingRootMarkingVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, .-_ZN2v88internal36IncrementalMarkingRootMarkingVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.section	.text._ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm.part.0, @function
_ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm.part.0:
.LFB28825:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	1(%rdi), %r14
	pushq	%r13
	movq	%r14, %r15
	pushq	%r12
	andq	$-262144, %r15
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$1, %ebx
	subq	$40, %rsp
	movq	16(%r15), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r14d, %eax
	andl	$262143, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	leaq	0(,%rax,4), %r13
	sall	%cl, %ebx
	movl	(%rdx,%rax,4), %eax
	testl	%eax, %ebx
	jne	.L249
	testb	$24, 8(%r15)
	je	.L267
.L249:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L268
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal4Heap13IsLargeObjectENS0_10HeapObjectE@PLT
	movq	-72(%rbp), %rsi
	testb	%al, %al
	je	.L253
	movq	16(%r15), %rcx
	addq	%r13, %rcx
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L270:
	movl	%ebx, %esi
	movl	%edx, %eax
	orl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L269
.L254:
	movl	(%rcx), %edx
	movl	%ebx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ebx
	jne	.L270
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L253:
	movq	%r12, %rdi
	leaq	(%r12,%rsi), %rdx
	movq	%r12, %rsi
	andq	$-262144, %rdi
	call	_ZN2v88internal4Page15CreateBlackAreaEmm@PLT
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L269:
	subl	%r15d, %r12d
	movl	$1, %eax
	movq	%r14, -64(%rbp)
	movl	%r12d, %ecx
	shrl	$8, %r12d
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movq	16(%r15), %rax
	leaq	(%rax,%r12,4), %rsi
	movl	(%rsi), %eax
	testl	%eax, %ecx
	je	.L249
	addl	%ecx, %ecx
	jne	.L258
	addq	$4, %rsi
	movl	$1, %ecx
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L272:
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L271
.L258:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %ecx
	jne	.L272
	jmp	.L249
.L271:
	movq	-64(%rbp), %rax
	leaq	-64(%rbp), %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, 96(%r15)
	jmp	.L249
.L268:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28825:
	.size	_ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm.part.0, .-_ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm.part.0
	.section	.text._ZN2v88internal36IncrementalMarkingRootMarkingVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal36IncrementalMarkingRootMarkingVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal36IncrementalMarkingRootMarkingVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal36IncrementalMarkingRootMarkingVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal36IncrementalMarkingRootMarkingVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB22361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rcx), %rbx
	testb	$1, %bl
	je	.L273
	movl	%ebx, %edx
	movq	8(%rdi), %rax
	movq	%rbx, %rsi
	andl	$262143, %edx
	andq	$-262144, %rsi
	movl	%edx, %ecx
	movq	2064(%rax), %r8
	movl	$1, %eax
	shrl	$8, %edx
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movq	16(%rsi), %rax
	leaq	(%rax,%rdx,4), %rsi
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L292:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L291
.L277:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L292
.L273:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movq	8(%r8), %r14
	movq	(%r14), %r13
	movq	8(%r13), %r12
	cmpq	$64, %r12
	je	.L293
	leaq	1(%r12), %rax
	movq	%rax, 8(%r13)
	movq	%rbx, 16(%r13,%r12,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	leaq	640(%r14), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r14), %rax
	movq	%r15, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 680(%r14)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r12, %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%r14)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L273
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%rbx, 16(%rdx,%rax,8)
	jmp	.L273
	.cfi_endproc
.LFE22361:
	.size	_ZN2v88internal36IncrementalMarkingRootMarkingVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal36IncrementalMarkingRootMarkingVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB9935:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L300
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L303
.L294:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L294
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE9935:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internal18IncrementalMarking12EmbedderStepEd.part.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"disabled-by-default-v8.gc"
	.section	.text._ZN2v88internal18IncrementalMarking12EmbedderStepEd.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal18IncrementalMarking12EmbedderStepEd.part.0, @function
_ZN2v88internal18IncrementalMarking12EmbedderStepEd.part.0:
.LFB28909:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movsd	%xmm0, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	2008(%rax), %rsi
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18IncrementalMarking12EmbedderStepEdE28trace_event_unique_atomic768(%rip), %rbx
	testq	%rbx, %rbx
	je	.L351
.L306:
	movq	$0, -208(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L352
.L308:
	movq	(%r12), %rdi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	addsd	-216(%rbp), %xmm0
	movsd	%xmm0, -224(%rbp)
	.p2align 4,,10
	.p2align 3
.L323:
	movq	(%r12), %rax
	movq	%r13, %rdi
	movl	$500, %ebx
	xorl	%r14d, %r14d
	movq	2128(%rax), %rsi
	call	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeC1EPS1_@PLT
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L312:
	leaq	-1(%rdx), %rcx
	movq	%rcx, 8(%rax)
	movq	8(%rax,%rdx,8), %r14
.L315:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope20TracePossibleWrapperENS0_8JSObjectE@PLT
	subq	$1, %rbx
	je	.L353
.L321:
	movq	8(%r12), %r15
	movq	1400(%r15), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L312
	movq	1392(%r15), %rdx
	cmpq	$0, 8(%rdx)
	je	.L354
	movq	%rax, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 1392(%r15)
.L316:
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L315
	leaq	-1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	8(%rdx,%rax,8), %r14
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L354:
	movq	2072(%r15), %rax
	leaq	2032(%r15), %rdi
	testq	%rax, %rax
	je	.L317
	movq	%rdi, -216(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	2072(%r15), %rdx
	movq	-216(%rbp), %rdi
	testq	%rdx, %rdx
	jne	.L318
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L317:
	movq	%r13, %rdi
	call	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeD1Ev@PLT
	movq	(%r12), %rax
	movsd	-224(%rbp), %xmm0
	movq	2128(%rax), %rdi
	call	_ZN2v88internal23LocalEmbedderHeapTracer5TraceEd@PLT
	movq	(%r12), %rax
	xorl	%r12d, %r12d
	movq	2128(%rax), %rax
	movb	$1, 28(%rax)
.L322:
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-232(%rbp), %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L355
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L353:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeD1Ev@PLT
	movq	(%r12), %rax
	movsd	-224(%rbp), %xmm0
	movq	2128(%rax), %rdi
	call	_ZN2v88internal23LocalEmbedderHeapTracer5TraceEd@PLT
	movq	(%r12), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movsd	-224(%rbp), %xmm2
	comisd	%xmm0, %xmm2
	ja	.L323
	movq	(%r12), %rax
	movl	$1, %r12d
	movq	2128(%rax), %rax
	movb	$0, 28(%rax)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L318:
	movq	(%rdx), %rax
	movq	%rdx, -216(%rbp)
	movq	%rax, 2072(%r15)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	1400(%r15), %rdi
	movq	-216(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L319
	movl	$144, %esi
	call	_ZdlPvm@PLT
	movq	-216(%rbp), %rdx
.L319:
	movq	%rdx, 1400(%r15)
	jmp	.L316
.L352:
	movl	$2, %edi
	xorl	%r13d, %r13d
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r15
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L356
.L309:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L310
	movq	(%rdi), %rax
	call	*8(%rax)
.L310:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L311
	movq	(%rdi), %rax
	call	*8(%rax)
.L311:
	movl	$2, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%rbx, -200(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-200(%rbp), %rax
	movq	%r13, -184(%rbp)
	movq	%rax, -208(%rbp)
	jmp	.L308
.L351:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L357
.L307:
	movq	%rbx, _ZZN2v88internal18IncrementalMarking12EmbedderStepEdE28trace_event_unique_atomic768(%rip)
	jmp	.L306
.L356:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r15, %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L309
.L357:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L307
.L355:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28909:
	.size	_ZN2v88internal18IncrementalMarking12EmbedderStepEd.part.0, .-_ZN2v88internal18IncrementalMarking12EmbedderStepEd.part.0
	.section	.text._ZNK2v88internal10HeapObject4SizeEv,"axG",@progbits,_ZNK2v88internal10HeapObject4SizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10HeapObject4SizeEv
	.type	_ZNK2v88internal10HeapObject4SizeEv, @function
_ZNK2v88internal10HeapObject4SizeEv:
.LFB15064:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-1(%rax), %rsi
	jmp	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	.cfi_endproc
.LFE15064:
	.size	_ZNK2v88internal10HeapObject4SizeEv, .-_ZNK2v88internal10HeapObject4SizeEv
	.section	.text._ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv,"axG",@progbits,_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv
	.type	_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv, @function
_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv:
.LFB15507:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	23(%rax), %rax
	movq	(%rdi), %rdx
	movq	47(%rdx), %rdx
	testb	$1, %al
	jne	.L360
.L362:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	movq	-1(%rax), %rcx
	cmpw	$160, 11(%rcx)
	jne	.L362
	testb	$1, %dl
	je	.L362
	movq	-1(%rdx), %rcx
	cmpw	$69, 11(%rcx)
	jne	.L362
	movabsq	$287762808832, %rcx
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L364
	testb	$1, %al
	je	.L362
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L364
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L362
.L364:
	cmpl	$67, 59(%rdx)
	setne	%al
	ret
	.cfi_endproc
.LFE15507:
	.size	_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv, .-_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv
	.section	.text._ZN2v88internal18SharedFunctionInfo19ShouldFlushBytecodeENS0_17BytecodeFlushModeE,"axG",@progbits,_ZN2v88internal18SharedFunctionInfo19ShouldFlushBytecodeENS0_17BytecodeFlushModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18SharedFunctionInfo19ShouldFlushBytecodeENS0_17BytecodeFlushModeE
	.type	_ZN2v88internal18SharedFunctionInfo19ShouldFlushBytecodeENS0_17BytecodeFlushModeE, @function
_ZN2v88internal18SharedFunctionInfo19ShouldFlushBytecodeENS0_17BytecodeFlushModeE:
.LFB18101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.L366
.L368:
	xorl	%eax, %eax
.L365:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L379
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	movq	(%rdi), %rcx
	movl	47(%rcx), %edx
	andl	$31, %edx
	leal	-9(%rdx), %eax
	cmpb	$6, %al
	setbe	%al
	cmpb	$1, %dl
	sete	%dl
	orb	%dl, %al
	jne	.L368
	movl	47(%rcx), %edx
	andb	$16, %dh
	je	.L368
	movq	7(%rcx), %rdx
	testb	$1, %dl
	je	.L365
	movq	-1(%rdx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L365
	movl	$1, %eax
	cmpl	$2, %esi
	je	.L365
	leaq	-16(%rbp), %rdi
	movq	%rdx, -16(%rbp)
	call	_ZNK2v88internal13BytecodeArray5IsOldEv@PLT
	jmp	.L365
.L379:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18101:
	.size	_ZN2v88internal18SharedFunctionInfo19ShouldFlushBytecodeENS0_17BytecodeFlushModeE, .-_ZN2v88internal18SharedFunctionInfo19ShouldFlushBytecodeENS0_17BytecodeFlushModeE
	.section	.text._ZN2v88internal18IncrementalMarkingC2EPNS0_4HeapEPNS0_20MarkCompactCollector15MarkingWorklistEPNS0_11WeakObjectsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarkingC2EPNS0_4HeapEPNS0_20MarkCompactCollector15MarkingWorklistEPNS0_11WeakObjectsE
	.type	_ZN2v88internal18IncrementalMarkingC2EPNS0_4HeapEPNS0_20MarkCompactCollector15MarkingWorklistEPNS0_11WeakObjectsE, @function
_ZN2v88internal18IncrementalMarkingC2EPNS0_4HeapEPNS0_20MarkCompactCollector15MarkingWorklistEPNS0_11WeakObjectsE:
.LFB22351:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	movq	%rdi, 120(%rdi)
	leaq	16+_ZTVN2v88internal18IncrementalMarking8ObserverE(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 96(%rdi)
	movups	%xmm0, (%rdi)
	movdqa	.LC3(%rip), %xmm0
	movq	%rax, 128(%rdi)
	xorl	%eax, %eax
	movups	%xmm0, 104(%rdi)
	movdqa	.LC4(%rip), %xmm0
	movq	%rdi, 152(%rdi)
	movups	%xmm0, 136(%rdi)
	pxor	%xmm0, %xmm0
	movw	%ax, 88(%rdi)
	movb	$0, 90(%rdi)
	movq	%rcx, 16(%rdi)
	movq	$0, 32(%rdi)
	movl	$0, 92(%rdi)
	movq	$0x000000000, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movb	$0, 2736(%rsi)
	movups	%xmm0, 48(%rdi)
	ret
	.cfi_endproc
.LFE22351:
	.size	_ZN2v88internal18IncrementalMarkingC2EPNS0_4HeapEPNS0_20MarkCompactCollector15MarkingWorklistEPNS0_11WeakObjectsE, .-_ZN2v88internal18IncrementalMarkingC2EPNS0_4HeapEPNS0_20MarkCompactCollector15MarkingWorklistEPNS0_11WeakObjectsE
	.globl	_ZN2v88internal18IncrementalMarkingC1EPNS0_4HeapEPNS0_20MarkCompactCollector15MarkingWorklistEPNS0_11WeakObjectsE
	.set	_ZN2v88internal18IncrementalMarkingC1EPNS0_4HeapEPNS0_20MarkCompactCollector15MarkingWorklistEPNS0_11WeakObjectsE,_ZN2v88internal18IncrementalMarkingC2EPNS0_4HeapEPNS0_20MarkCompactCollector15MarkingWorklistEPNS0_11WeakObjectsE
	.section	.rodata._ZN2v88internal18IncrementalMarking19RecordWriteIntoCodeENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"[IncrementalMarking] Restarting (new grey objects)\n"
	.section	.text._ZN2v88internal18IncrementalMarking19RecordWriteIntoCodeENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking19RecordWriteIntoCodeENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE
	.type	_ZN2v88internal18IncrementalMarking19RecordWriteIntoCodeENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE, @function
_ZN2v88internal18IncrementalMarking19RecordWriteIntoCodeENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE:
.LFB22355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	andl	$262143, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movq	%rcx, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$1, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	movl	%eax, %ecx
	shrl	$3, %ecx
	andq	$-262144, %rdi
	shrl	$8, %eax
	sall	%cl, %esi
	subq	$40, %rsp
	movq	16(%rdi), %rcx
	leaq	(%rcx,%rax,4), %rdi
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L401:
	movl	%ecx, %r8d
	movl	%ecx, %eax
	orl	%esi, %r8d
	lock cmpxchgl	%r8d, (%rdi)
	cmpl	%eax, %ecx
	je	.L400
.L384:
	movl	(%rdi), %ecx
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%esi, %eax
	jne	.L401
.L383:
	cmpb	$0, 84(%rbx)
	jne	.L402
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	addq	$40, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal20MarkCompactCollector15RecordRelocSlotENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE@PLT
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	movq	(%rsi), %r15
	movq	8(%r15), %r14
	cmpq	$64, %r14
	je	.L403
	leaq	1(%r14), %rax
	movq	%rax, 8(%r15)
	movq	%rdx, 16(%r15,%r14,8)
.L386:
	cmpl	$3, 80(%rbx)
	jne	.L383
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movl	$2, 80(%rbx)
	je	.L383
	movq	(%rbx), %rax
	leaq	.LC6(%rip), %rsi
	movq	%rdx, -56(%rbp)
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movq	-56(%rbp), %rdx
	jmp	.L383
.L403:
	leaq	640(%rsi), %rdi
	movq	%rdx, -72(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-56(%rbp), %rsi
	movq	680(%rsi), %rax
	movq	%rax, (%r15)
	movq	%r15, 680(%rsi)
	movq	%rsi, -56(%rbp)
	movq	-64(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r14, %rcx
	movq	-56(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %r8
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%r8, (%rsi)
	movq	8(%r8), %rax
	cmpq	$64, %rax
	je	.L386
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r8)
	movq	%rdx, 16(%r8,%rax,8)
	jmp	.L386
	.cfi_endproc
.LFE22355:
	.size	_ZN2v88internal18IncrementalMarking19RecordWriteIntoCodeENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE, .-_ZN2v88internal18IncrementalMarking19RecordWriteIntoCodeENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE
	.section	.text._ZN2v88internal18IncrementalMarking41DeactivateIncrementalWriteBarrierForSpaceEPNS0_10PagedSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking41DeactivateIncrementalWriteBarrierForSpaceEPNS0_10PagedSpaceE
	.type	_ZN2v88internal18IncrementalMarking41DeactivateIncrementalWriteBarrierForSpaceEPNS0_10PagedSpaceE, @function
_ZN2v88internal18IncrementalMarking41DeactivateIncrementalWriteBarrierForSpaceEPNS0_10PagedSpaceE:
.LFB22364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	32(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L404
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L406
.L404:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22364:
	.size	_ZN2v88internal18IncrementalMarking41DeactivateIncrementalWriteBarrierForSpaceEPNS0_10PagedSpaceE, .-_ZN2v88internal18IncrementalMarking41DeactivateIncrementalWriteBarrierForSpaceEPNS0_10PagedSpaceE
	.section	.text._ZN2v88internal18IncrementalMarking41DeactivateIncrementalWriteBarrierForSpaceEPNS0_8NewSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking41DeactivateIncrementalWriteBarrierForSpaceEPNS0_8NewSpaceE
	.type	_ZN2v88internal18IncrementalMarking41DeactivateIncrementalWriteBarrierForSpaceEPNS0_8NewSpaceE, @function
_ZN2v88internal18IncrementalMarking41DeactivateIncrementalWriteBarrierForSpaceEPNS0_8NewSpaceE:
.LFB22365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	240(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L412
	.p2align 4,,10
	.p2align 3
.L414:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11MemoryChunk27SetYoungGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L414
.L412:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22365:
	.size	_ZN2v88internal18IncrementalMarking41DeactivateIncrementalWriteBarrierForSpaceEPNS0_8NewSpaceE, .-_ZN2v88internal18IncrementalMarking41DeactivateIncrementalWriteBarrierForSpaceEPNS0_8NewSpaceE
	.section	.text._ZN2v88internal18IncrementalMarking33DeactivateIncrementalWriteBarrierEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking33DeactivateIncrementalWriteBarrierEv
	.type	_ZN2v88internal18IncrementalMarking33DeactivateIncrementalWriteBarrierEv, @function
_ZN2v88internal18IncrementalMarking33DeactivateIncrementalWriteBarrierEv:
.LFB22366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	256(%rax), %rdx
	movq	32(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L421
	.p2align 4,,10
	.p2align 3
.L422:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L422
	movq	(%r12), %rax
.L421:
	movq	272(%rax), %rdx
	movq	32(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L423
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L424
	movq	(%r12), %rax
.L423:
	movq	264(%rax), %rdx
	movq	32(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L425
	.p2align 4,,10
	.p2align 3
.L426:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L426
	movq	(%r12), %rax
.L425:
	movq	248(%rax), %rdx
	movq	240(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L427
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11MemoryChunk27SetYoungGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L428
	movq	(%r12), %rax
.L427:
	movq	296(%rax), %rdx
	movq	32(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L429
	.p2align 4,,10
	.p2align 3
.L430:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11MemoryChunk27SetYoungGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L430
	movq	(%r12), %rax
.L429:
	movq	280(%rax), %rdx
	movq	32(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L431
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L432
	movq	(%r12), %rax
.L431:
	movq	288(%rax), %rax
	movq	32(%rax), %rbx
	testq	%rbx, %rbx
	je	.L420
	.p2align 4,,10
	.p2align 3
.L433:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L433
.L420:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22366:
	.size	_ZN2v88internal18IncrementalMarking33DeactivateIncrementalWriteBarrierEv, .-_ZN2v88internal18IncrementalMarking33DeactivateIncrementalWriteBarrierEv
	.section	.text._ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEPNS0_10PagedSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEPNS0_10PagedSpaceE
	.type	_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEPNS0_10PagedSpaceE, @function
_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEPNS0_10PagedSpaceE:
.LFB22367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	32(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L462
	.p2align 4,,10
	.p2align 3
.L464:
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L464
.L462:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22367:
	.size	_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEPNS0_10PagedSpaceE, .-_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEPNS0_10PagedSpaceE
	.section	.text._ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEPNS0_8NewSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEPNS0_8NewSpaceE
	.type	_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEPNS0_8NewSpaceE, @function
_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEPNS0_8NewSpaceE:
.LFB22368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	240(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L470
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_ZN2v88internal11MemoryChunk27SetYoungGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L472
.L470:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22368:
	.size	_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEPNS0_8NewSpaceE, .-_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEPNS0_8NewSpaceE
	.section	.text._ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEv
	.type	_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEv, @function
_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEv:
.LFB22369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	256(%rax), %rdx
	movq	32(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L479
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L480
	movq	(%r12), %rax
.L479:
	movq	272(%rax), %rdx
	movq	32(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L481
	.p2align 4,,10
	.p2align 3
.L482:
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L482
	movq	(%r12), %rax
.L481:
	movq	264(%rax), %rdx
	movq	32(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L483
	.p2align 4,,10
	.p2align 3
.L484:
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L484
	movq	(%r12), %rax
.L483:
	movq	248(%rax), %rdx
	movq	240(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L485
	.p2align 4,,10
	.p2align 3
.L486:
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_ZN2v88internal11MemoryChunk27SetYoungGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L486
	movq	(%r12), %rax
.L485:
	movq	296(%rax), %rdx
	movq	32(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L487
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_ZN2v88internal11MemoryChunk27SetYoungGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L488
	movq	(%r12), %rax
.L487:
	movq	280(%rax), %rdx
	movq	32(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L489
	.p2align 4,,10
	.p2align 3
.L490:
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L490
	movq	(%r12), %rax
.L489:
	movq	288(%rax), %rax
	movq	32(%rax), %rbx
	testq	%rbx, %rbx
	je	.L478
	.p2align 4,,10
	.p2align 3
.L491:
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb@PLT
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L491
.L478:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22369:
	.size	_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEv, .-_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEv
	.section	.text._ZN2v88internal18IncrementalMarking12WasActivatedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking12WasActivatedEv
	.type	_ZN2v88internal18IncrementalMarking12WasActivatedEv, @function
_ZN2v88internal18IncrementalMarking12WasActivatedEv:
.LFB22370:
	.cfi_startproc
	endbr64
	movzbl	86(%rdi), %eax
	ret
	.cfi_endproc
.LFE22370:
	.size	_ZN2v88internal18IncrementalMarking12WasActivatedEv, .-_ZN2v88internal18IncrementalMarking12WasActivatedEv
	.section	.text._ZN2v88internal18IncrementalMarking14CanBeActivatedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking14CanBeActivatedEv
	.type	_ZN2v88internal18IncrementalMarking14CanBeActivatedEv, @function
_ZN2v88internal18IncrementalMarking14CanBeActivatedEv:
.LFB22371:
	.cfi_startproc
	endbr64
	movzbl	_ZN2v88internal24FLAG_incremental_markingE(%rip), %eax
	testb	%al, %al
	je	.L521
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movl	392(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L521
	movzbl	2868(%rdx), %eax
	testb	%al, %al
	je	.L521
	movzbl	3864(%rdx), %eax
	xorl	$1, %eax
.L521:
	ret
	.cfi_endproc
.LFE22371:
	.size	_ZN2v88internal18IncrementalMarking14CanBeActivatedEv, .-_ZN2v88internal18IncrementalMarking14CanBeActivatedEv
	.section	.text._ZNK2v88internal18IncrementalMarking27IsBelowActivationThresholdsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18IncrementalMarking27IsBelowActivationThresholdsEv
	.type	_ZNK2v88internal18IncrementalMarking27IsBelowActivationThresholdsEv, @function
_ZNK2v88internal18IncrementalMarking27IsBelowActivationThresholdsEv:
.LFB22372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	_ZN2v88internal4Heap26OldGenerationSizeOfObjectsEv@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	cmpq	$8388608, %r8
	jbe	.L535
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	_ZN2v88internal4Heap19GlobalSizeOfObjectsEv@PLT
	cmpq	$16777216, %rax
	setbe	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22372:
	.size	_ZNK2v88internal18IncrementalMarking27IsBelowActivationThresholdsEv, .-_ZNK2v88internal18IncrementalMarking27IsBelowActivationThresholdsEv
	.section	.text._ZN2v88internal18IncrementalMarking10DeactivateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking10DeactivateEv
	.type	_ZN2v88internal18IncrementalMarking10DeactivateEv, @function
_ZN2v88internal18IncrementalMarking10DeactivateEv:
.LFB22373:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal18IncrementalMarking33DeactivateIncrementalWriteBarrierEv
	.cfi_endproc
.LFE22373:
	.size	_ZN2v88internal18IncrementalMarking10DeactivateEv, .-_ZN2v88internal18IncrementalMarking10DeactivateEv
	.section	.rodata._ZN2v88internal18IncrementalMarking12StartMarkingEv.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"[IncrementalMarking] Start delayed - serializer\n"
	.align 8
.LC8:
	.string	"[IncrementalMarking] Start marking\n"
	.align 8
.LC9:
	.string	"[IncrementalMarking] Black allocation started\n"
	.section	.rodata._ZN2v88internal18IncrementalMarking12StartMarkingEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	"[IncrementalMarking] Running\n"
	.section	.text._ZN2v88internal18IncrementalMarking12StartMarkingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking12StartMarkingEv
	.type	_ZN2v88internal18IncrementalMarking12StartMarkingEv, @function
_ZN2v88internal18IncrementalMarking12StartMarkingEv:
.LFB22375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movzbl	_ZN2v88internal30FLAG_trace_incremental_markingE(%rip), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movzbl	3864(%rax), %r12d
	leaq	-37592(%rax), %rdi
	testb	%r12b, %r12b
	jne	.L573
	testb	%dl, %dl
	jne	.L574
	cmpb	$0, _ZN2v88internal18FLAG_never_compactE(%rip)
	je	.L575
.L542:
	movb	%r12b, 84(%rbx)
	movq	%rbx, %rdi
	movl	$2, 80(%rbx)
	movb	$1, 2736(%rax)
	call	_ZN2v88internal18IncrementalMarking31ActivateIncrementalWriteBarrierEv
	movq	(%rbx), %rax
	movq	3360(%rax), %rdi
	call	_ZN2v88internal16CompilationCache19MarkCompactPrologueEv@PLT
	movq	(%rbx), %rax
	movb	$1, 87(%rbx)
	movq	256(%rax), %rdi
	call	_ZN2v88internal10PagedSpace29MarkLinearAllocationAreaBlackEv@PLT
	movq	(%rbx), %rax
	movq	272(%rax), %rdi
	call	_ZN2v88internal10PagedSpace29MarkLinearAllocationAreaBlackEv@PLT
	movq	(%rbx), %rax
	movq	264(%rax), %rdi
	call	_ZN2v88internal10PagedSpace29MarkLinearAllocationAreaBlackEv@PLT
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	jne	.L576
.L543:
	movq	(%rbx), %rdi
	leaq	16+_ZTVN2v88internal36IncrementalMarkingRootMarkingVisitorE(%rip), %rax
	movl	$5, %edx
	leaq	-192(%rbp), %rsi
	movq	%rax, -192(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN2v88internal4Heap18IterateStrongRootsEPNS0_11RootVisitorENS0_9VisitModeE@PLT
	cmpb	$0, _ZN2v88internal23FLAG_concurrent_markingE(%rip)
	movq	(%rbx), %rdi
	je	.L545
	cmpl	$4, 392(%rdi)
	jne	.L577
.L545:
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	jne	.L578
.L546:
	movq	2008(%rdi), %rsi
	leaq	-144(%rbp), %r13
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18IncrementalMarking12StartMarkingEvE28trace_event_unique_atomic369(%rip), %r12
	testq	%r12, %r12
	je	.L579
.L548:
	movq	$0, -176(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L580
.L550:
	movq	(%rbx), %rdi
	movq	2128(%rdi), %r12
	call	_ZNK2v88internal4Heap25flags_for_embedder_tracerEv@PLT
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23LocalEmbedderHeapTracer13TracePrologueENS_18EmbedderHeapTracer10TraceFlagsE@PLT
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
.L537:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L581
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L576:
	.cfi_restore_state
	movq	(%rbx), %rax
	leaq	.LC9(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L575:
	movq	2016(%rax), %rdi
	call	_ZN2v88internal20MarkCompactCollector15StartCompactionEv@PLT
	movl	%eax, %r12d
	movq	(%rbx), %rax
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L574:
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	cmpb	$0, _ZN2v88internal18FLAG_never_compactE(%rip)
	movq	(%rbx), %rax
	jne	.L542
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L580:
	movl	$1, %edi
	xorl	%r15d, %r15d
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r14
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L582
.L551:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L552
	movq	(%rdi), %rax
	call	*8(%rax)
.L552:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L553
	movq	(%rdi), %rax
	call	*8(%rax)
.L553:
	movl	$1, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%r12, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%r15, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L579:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L583
.L549:
	movq	%r12, _ZZN2v88internal18IncrementalMarking12StartMarkingEvE28trace_event_unique_atomic369(%rip)
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L578:
	subq	$37592, %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movq	(%rbx), %rdi
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L573:
	testb	%dl, %dl
	je	.L537
	leaq	.LC7(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L577:
	movq	2072(%rdi), %rdi
	call	_ZN2v88internal17ConcurrentMarking13ScheduleTasksEv@PLT
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movq	(%rbx), %rdi
	je	.L546
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L583:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L582:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r14, %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L551
.L581:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22375:
	.size	_ZN2v88internal18IncrementalMarking12StartMarkingEv, .-_ZN2v88internal18IncrementalMarking12StartMarkingEv
	.section	.rodata._ZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"[IncrementalMarking] Start (%s): (size/limit/slack) v8: %zuMB / %zuMB / %zuMB global: %zuMB / %zuMB / %zuMB\n"
	.section	.rodata._ZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"v8"
.LC13:
	.string	"V8.GCIncrementalMarkingStart"
	.section	.rodata._ZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonE.str1.8
	.align 8
.LC14:
	.string	"[IncrementalMarking] Start sweeping.\n"
	.section	.text._ZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonE
	.type	_ZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonE, @function
_ZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonE:
.LFB22374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	jne	.L631
.L585:
	movq	3368(%rdi), %rbx
	movl	%r12d, %esi
	leaq	288(%rbx), %rdi
	leaq	2376(%rbx), %r12
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	2408(%rbx), %rax
	leaq	2424(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rsi, -216(%rbp)
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	_ZZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonEE28trace_event_unique_atomic289(%rip), %r15
	testq	%r15, %r15
	je	.L632
.L589:
	movq	$0, -208(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L633
.L591:
	movq	0(%r13), %rax
	leaq	-144(%rbp), %r14
	movl	$8, %edx
	movq	%r14, %rdi
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonEE28trace_event_unique_atomic290(%rip), %rdx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L634
.L596:
	movq	$0, -176(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L635
.L598:
	movq	0(%r13), %rax
	movq	2008(%rax), %rdi
	call	_ZN2v88internal8GCTracer29NotifyIncrementalMarkingStartEv@PLT
	movq	0(%r13), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	0(%r13), %rdi
	movsd	%xmm0, 24(%r13)
	call	_ZN2v88internal4Heap26OldGenerationSizeOfObjectsEv@PLT
	movq	0(%r13), %rdx
	movq	%rax, 32(%r13)
	movq	%rdx, %rdi
	movq	2152(%rdx), %r15
	movq	%rdx, -224(%rbp)
	call	_ZN2v88internal4Heap26OldGenerationSizeOfObjectsEv@PLT
	movq	-224(%rbp), %rdx
	movsd	24(%r13), %xmm0
	movq	0(%r13), %rdi
	subq	2160(%rdx), %r15
	movq	$0, 48(%r13)
	addq	%rax, %r15
	movl	$256, %eax
	movq	$0, 56(%r13)
	movq	%r15, 40(%r13)
	movq	$0, 72(%r13)
	movw	%ax, 85(%r13)
	movsd	%xmm0, 64(%r13)
	movq	2016(%rdi), %rax
	movq	9984(%rax), %rax
	cmpb	$0, 265(%rax)
	je	.L636
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	jne	.L637
.L604:
	movl	$1, 80(%r13)
	movb	$0, 2736(%rdi)
.L603:
	movq	0(%r13), %rdi
	leaq	96(%r13), %rdx
	leaq	128(%r13), %rsi
	call	_ZN2v88internal4Heap33AddAllocationObserversToAllSpacesEPNS0_18AllocationObserverES3_@PLT
	movq	0(%r13), %rsi
	leaq	89(%r13), %rdi
	call	_ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE@PLT
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	2408(%rbx), %rax
	movq	-216(%rbp), %rsi
	movq	%r12, %rdi
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L638
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	.cfi_restore_state
	subq	$37592, %rdi
	leaq	.LC14(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movq	0(%r13), %rdi
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L636:
	movq	%r13, %rdi
	call	_ZN2v88internal18IncrementalMarking12StartMarkingEv
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L635:
	movl	$8, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -232(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	$0, -224(%rbp)
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L639
.L599:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L600
	movq	(%rdi), %rax
	call	*8(%rax)
.L600:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L601
	movq	(%rdi), %rax
	call	*8(%rax)
.L601:
	movl	$8, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%r15, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	-224(%rbp), %rax
	movq	%rax, -152(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rax, -176(%rbp)
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L634:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L640
.L597:
	movq	%r15, _ZZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonEE28trace_event_unique_atomic290(%rip)
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L633:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L641
.L592:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L593
	movq	(%rdi), %rax
	call	*8(%rax)
.L593:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L594
	movq	(%rdi), %rax
	call	*8(%rax)
.L594:
	leaq	.LC13(%rip), %rax
	movq	%r15, -200(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-200(%rbp), %rax
	movq	%r14, -184(%rbp)
	movq	%rax, -208(%rbp)
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L632:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L642
.L590:
	movq	%r15, _ZZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonEE28trace_event_unique_atomic289(%rip)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L631:
	call	_ZN2v88internal4Heap26OldGenerationSizeOfObjectsEv@PLT
	movq	0(%r13), %rdi
	shrq	$20, %rax
	movq	%rax, %r14
	movq	1504(%rdi), %rbx
	call	_ZN2v88internal4Heap19GlobalSizeOfObjectsEv@PLT
	movq	0(%r13), %r11
	movl	%r12d, %edi
	shrq	$20, %rax
	shrq	$20, %rbx
	movq	1512(%r11), %r15
	movq	%rax, %rsi
	subq	$37592, %r11
	xorl	%eax, %eax
	movq	%rbx, %r9
	movq	%r11, -240(%rbp)
	shrq	$20, %r15
	movq	%rsi, -232(%rbp)
	movq	%r15, %rcx
	subq	%rsi, %rcx
	cmpq	%r15, %rsi
	cmova	%rax, %rcx
	subq	%r14, %r9
	cmpq	%rbx, %r14
	cmova	%rax, %r9
	movq	%rcx, -224(%rbp)
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal4Heap31GarbageCollectionReasonToStringENS0_23GarbageCollectionReasonE@PLT
	movq	-224(%rbp), %rcx
	subq	$8, %rsp
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %r11
	movq	-216(%rbp), %r9
	movq	%rax, %rdx
	movq	%rbx, %r8
	pushq	%rcx
	xorl	%eax, %eax
	movq	%r14, %rcx
	pushq	%r15
	movq	%r11, %rdi
	pushq	%rsi
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movq	0(%r13), %rdi
	addq	$32, %rsp
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L639:
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	-232(%rbp), %rcx
	movq	%r15, %rdx
	movl	$88, %esi
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	movq	%rax, -224(%rbp)
	addq	$64, %rsp
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L642:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L641:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r15, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L640:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L597
.L638:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22374:
	.size	_ZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonE, .-_ZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonE
	.section	.text._ZN2v88internal18IncrementalMarking20StartBlackAllocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking20StartBlackAllocationEv
	.type	_ZN2v88internal18IncrementalMarking20StartBlackAllocationEv, @function
_ZN2v88internal18IncrementalMarking20StartBlackAllocationEv:
.LFB22376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movb	$1, 87(%rdi)
	movq	256(%rax), %rdi
	call	_ZN2v88internal10PagedSpace29MarkLinearAllocationAreaBlackEv@PLT
	movq	(%rbx), %rax
	movq	272(%rax), %rdi
	call	_ZN2v88internal10PagedSpace29MarkLinearAllocationAreaBlackEv@PLT
	movq	(%rbx), %rax
	movq	264(%rax), %rdi
	call	_ZN2v88internal10PagedSpace29MarkLinearAllocationAreaBlackEv@PLT
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	jne	.L646
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L646:
	.cfi_restore_state
	movq	(%rbx), %rdi
	addq	$8, %rsp
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	subq	$37592, %rdi
	jmp	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	.cfi_endproc
.LFE22376:
	.size	_ZN2v88internal18IncrementalMarking20StartBlackAllocationEv, .-_ZN2v88internal18IncrementalMarking20StartBlackAllocationEv
	.section	.rodata._ZN2v88internal18IncrementalMarking20PauseBlackAllocationEv.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"[IncrementalMarking] Black allocation paused\n"
	.section	.text._ZN2v88internal18IncrementalMarking20PauseBlackAllocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking20PauseBlackAllocationEv
	.type	_ZN2v88internal18IncrementalMarking20PauseBlackAllocationEv, @function
_ZN2v88internal18IncrementalMarking20PauseBlackAllocationEv:
.LFB22377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	256(%rax), %rdi
	call	_ZN2v88internal10PagedSpace26UnmarkLinearAllocationAreaEv@PLT
	movq	(%rbx), %rax
	movq	272(%rax), %rdi
	call	_ZN2v88internal10PagedSpace26UnmarkLinearAllocationAreaEv@PLT
	movq	(%rbx), %rax
	movq	264(%rax), %rdi
	call	_ZN2v88internal10PagedSpace26UnmarkLinearAllocationAreaEv@PLT
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	jne	.L650
	movb	$0, 87(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L650:
	.cfi_restore_state
	movq	(%rbx), %rax
	leaq	.LC15(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movb	$0, 87(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22377:
	.size	_ZN2v88internal18IncrementalMarking20PauseBlackAllocationEv, .-_ZN2v88internal18IncrementalMarking20PauseBlackAllocationEv
	.section	.rodata._ZN2v88internal18IncrementalMarking21FinishBlackAllocationEv.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"[IncrementalMarking] Black allocation finished\n"
	.section	.text._ZN2v88internal18IncrementalMarking21FinishBlackAllocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking21FinishBlackAllocationEv
	.type	_ZN2v88internal18IncrementalMarking21FinishBlackAllocationEv, @function
_ZN2v88internal18IncrementalMarking21FinishBlackAllocationEv:
.LFB22378:
	.cfi_startproc
	endbr64
	cmpb	$0, 87(%rdi)
	je	.L651
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movb	$0, 87(%rdi)
	jne	.L655
.L651:
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	movq	(%rdi), %rdi
	leaq	.LC16(%rip), %rsi
	xorl	%eax, %eax
	subq	$37592, %rdi
	jmp	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	.cfi_endproc
.LFE22378:
	.size	_ZN2v88internal18IncrementalMarking21FinishBlackAllocationEv, .-_ZN2v88internal18IncrementalMarking21FinishBlackAllocationEv
	.section	.text._ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm
	.type	_ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm, @function
_ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm:
.LFB22379:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	cmpb	$0, 87(%r8)
	je	.L656
	testq	%rsi, %rsi
	jne	.L661
.L656:
	ret
	.p2align 4,,10
	.p2align 3
.L661:
	movq	%rdx, %rsi
	jmp	_ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm.part.0
	.cfi_endproc
.LFE22379:
	.size	_ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm, .-_ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm
	.section	.text._ZN2v88internal18IncrementalMarking9MarkRootsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking9MarkRootsEv
	.type	_ZN2v88internal18IncrementalMarking9MarkRootsEv, @function
_ZN2v88internal18IncrementalMarking9MarkRootsEv:
.LFB22380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rsi
	leaq	16+_ZTVN2v88internal36IncrementalMarkingRootMarkingVisitorE(%rip), %rax
	movq	%rax, -32(%rbp)
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal4Heap18IterateStrongRootsEPNS0_11RootVisitorENS0_9VisitModeE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L665
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L665:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22380:
	.size	_ZN2v88internal18IncrementalMarking9MarkRootsEv, .-_ZN2v88internal18IncrementalMarking9MarkRootsEv
	.section	.text._ZN2v88internal18IncrementalMarking15ShouldRetainMapENS0_3MapEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking15ShouldRetainMapENS0_3MapEi
	.type	_ZN2v88internal18IncrementalMarking15ShouldRetainMapENS0_3MapEi, @function
_ZN2v88internal18IncrementalMarking15ShouldRetainMapENS0_3MapEi:
.LFB22381:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jne	.L676
.L667:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L676:
	movq	31(%rsi), %rcx
	testb	$1, %cl
	je	.L667
.L669:
	movq	-1(%rcx), %rax
	cmpw	$68, 11(%rax)
	je	.L677
	movq	%rcx, %rdx
	andl	$262143, %ecx
	andq	$-262144, %rdx
	movl	%ecx, %eax
	shrl	$3, %ecx
	movq	16(%rdx), %rdx
	shrl	$8, %eax
	movl	(%rdx,%rax,4), %edx
	movl	$1, %eax
	sall	%cl, %eax
	testl	%edx, %eax
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L677:
	movq	31(%rcx), %rcx
	testb	$1, %cl
	je	.L667
	jmp	.L669
	.cfi_endproc
.LFE22381:
	.size	_ZN2v88internal18IncrementalMarking15ShouldRetainMapENS0_3MapEi, .-_ZN2v88internal18IncrementalMarking15ShouldRetainMapENS0_3MapEi
	.section	.text._ZN2v88internal18IncrementalMarking33UpdateWeakReferencesAfterScavengeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking33UpdateWeakReferencesAfterScavengeEv
	.type	_ZN2v88internal18IncrementalMarking33UpdateWeakReferencesAfterScavengeEv, @function
_ZN2v88internal18IncrementalMarking33UpdateWeakReferencesAfterScavengeEv:
.LFB22386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r12
	movl	4168(%r12), %r10d
	leaq	3480(%r12), %rdi
	testl	%r10d, %r10d
	jle	.L692
	.p2align 4,,10
	.p2align 3
.L693:
	movq	8(%rdi), %rcx
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L682
	leaq	16(%rcx), %r9
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L1110:
	addq	$1, %rax
.L684:
	testq	%rax, %rax
	je	.L685
	addq	$1, %rdx
	subq	%r13, %rsi
	movq	%rdx, %r10
	salq	$4, %r10
	addq	%rcx, %r10
	movq	%rax, (%r10)
	addq	%rsi, %rax
	movq	%rax, 8(%r10)
.L685:
	addq	$16, %r9
	cmpq	%r11, 8(%rcx)
	jbe	.L682
.L686:
	movq	(%r9), %r13
	movq	8(%r9), %rsi
	addq	$1, %r11
	movq	-1(%r13), %rax
	testb	$1, %al
	je	.L1110
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$8, 8(%rax)
	jne	.L685
	movq	%r13, %rax
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L682:
	movq	%rdx, 8(%rcx)
	movq	(%rdi), %rcx
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L687
	leaq	16(%rcx), %r9
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L1111:
	addq	$1, %rax
.L689:
	testq	%rax, %rax
	je	.L690
	addq	$1, %rdx
	subq	%r13, %rsi
	movq	%rdx, %r10
	salq	$4, %r10
	addq	%rcx, %r10
	movq	%rax, (%r10)
	addq	%rsi, %rax
	movq	%rax, 8(%r10)
.L690:
	addq	$16, %r9
	cmpq	%r11, 8(%rcx)
	jbe	.L687
.L691:
	movq	(%r9), %r13
	movq	8(%r9), %rsi
	addq	$1, %r11
	movq	-1(%r13), %rax
	testb	$1, %al
	je	.L1111
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$8, 8(%rax)
	jne	.L690
	movq	%r13, %rax
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L687:
	movq	%rdx, 8(%rcx)
	addl	$1, %r8d
	addq	$80, %rdi
	cmpl	%r8d, 4168(%r12)
	jg	.L693
.L692:
	leaq	4120(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	4160(%r12), %rdi
	testq	%rdi, %rdi
	je	.L680
	xorl	%r15d, %r15d
	xorl	%edx, %edx
	leaq	16(%rdi), %rsi
	xorl	%r9d, %r9d
	cmpq	$0, 8(%rdi)
	jne	.L699
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1114:
	addq	$1, %rax
.L697:
	testq	%rax, %rax
	je	.L698
	addq	$1, %rdx
	subq	%r10, %rcx
	movq	%rdx, %r8
	salq	$4, %r8
	addq	%rdi, %r8
	movq	%rax, (%r8)
	addq	%rcx, %rax
	movq	%rax, 8(%r8)
.L698:
	addq	$16, %rsi
	cmpq	%r9, 8(%rdi)
	jbe	.L1113
.L699:
	movq	(%rsi), %r10
	movq	8(%rsi), %rcx
	addq	$1, %r9
	movq	-1(%r10), %rax
	testb	$1, %al
	je	.L1114
	movq	%r10, %rax
	andq	$-262144, %rax
	testb	$8, 8(%rax)
	jne	.L698
	movq	%r10, %rax
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	(%rdi), %r14
	movq	%rdx, 8(%rdi)
	movq	%r14, %rax
	testq	%rdx, %rdx
	je	.L695
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.L680
.L1116:
	movq	%r14, %rdi
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	cmpq	$0, 8(%rdi)
	leaq	16(%rdi), %rsi
	jne	.L699
.L1112:
	movq	(%rdi), %rax
.L695:
	testq	%r15, %r15
	je	.L1115
	movq	%rax, (%r15)
.L702:
	movq	(%rdi), %r14
	movl	$1040, %esi
	call	_ZdlPvm@PLT
	testq	%r14, %r14
	jne	.L1116
.L680:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	16(%rbx), %r12
	xorl	%r8d, %r8d
	movl	4864(%r12), %r9d
	leaq	4176(%r12), %rsi
	testl	%r9d, %r9d
	jle	.L716
	.p2align 4,,10
	.p2align 3
.L717:
	movq	8(%rsi), %rcx
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L706
	leaq	16(%rcx), %rdi
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L1117:
	leaq	1(%r10), %rax
.L708:
	testq	%rax, %rax
	je	.L709
	addq	$1, %rdx
	movq	%rax, %xmm0
	movq	%r11, %xmm1
	movq	%rdx, %rax
	punpcklqdq	%xmm1, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rcx,%rax)
.L709:
	addq	$16, %rdi
	cmpq	%r9, 8(%rcx)
	jbe	.L706
.L710:
	movq	(%rdi), %rax
	movq	8(%rdi), %r11
	addq	$1, %r9
	movq	-1(%rax), %r10
	testb	$1, %r10b
	je	.L1117
	movq	%rax, %r10
	andq	$-262144, %r10
	testb	$8, 8(%r10)
	je	.L708
	addq	$16, %rdi
	cmpq	%r9, 8(%rcx)
	ja	.L710
.L706:
	movq	%rdx, 8(%rcx)
	movq	(%rsi), %rcx
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L711
	leaq	16(%rcx), %rdi
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L1118:
	leaq	1(%r10), %rax
.L713:
	testq	%rax, %rax
	je	.L714
	addq	$1, %rdx
	movq	%rax, %xmm0
	movq	%r11, %xmm2
	movq	%rdx, %rax
	punpcklqdq	%xmm2, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rcx,%rax)
.L714:
	addq	$16, %rdi
	cmpq	%r9, 8(%rcx)
	jbe	.L711
.L715:
	movq	(%rdi), %rax
	movq	8(%rdi), %r11
	addq	$1, %r9
	movq	-1(%rax), %r10
	testb	$1, %r10b
	je	.L1118
	movq	%rax, %r10
	andq	$-262144, %r10
	testb	$8, 8(%r10)
	je	.L713
	addq	$16, %rdi
	cmpq	%r9, 8(%rcx)
	ja	.L715
.L711:
	movq	%rdx, 8(%rcx)
	addl	$1, %r8d
	addq	$80, %rsi
	cmpl	%r8d, 4864(%r12)
	jg	.L717
.L716:
	leaq	4816(%r12), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	4856(%r12), %rdi
	testq	%rdi, %rdi
	je	.L704
	xorl	%r15d, %r15d
	xorl	%edx, %edx
	leaq	16(%rdi), %rcx
	xorl	%esi, %esi
	cmpq	$0, 8(%rdi)
	jne	.L723
	jmp	.L1119
	.p2align 4,,10
	.p2align 3
.L1121:
	leaq	1(%r8), %rax
.L721:
	testq	%rax, %rax
	je	.L722
	addq	$1, %rdx
	movq	%rax, %xmm0
	movq	%r9, %xmm3
	movq	%rdx, %rax
	punpcklqdq	%xmm3, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rdi,%rax)
.L722:
	addq	$16, %rcx
	cmpq	%rsi, 8(%rdi)
	jbe	.L1120
.L723:
	movq	(%rcx), %rax
	movq	8(%rcx), %r9
	addq	$1, %rsi
	movq	-1(%rax), %r8
	testb	$1, %r8b
	je	.L1121
	movq	%rax, %r8
	andq	$-262144, %r8
	testb	$8, 8(%r8)
	je	.L721
	addq	$16, %rcx
	cmpq	%rsi, 8(%rdi)
	ja	.L723
.L1120:
	movq	(%rdi), %r13
	movq	%rdx, 8(%rdi)
	movq	%r13, %rax
	testq	%rdx, %rdx
	je	.L719
	movq	%rdi, %r15
	testq	%r13, %r13
	je	.L704
.L1123:
	movq	%r13, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	cmpq	$0, 8(%rdi)
	leaq	16(%rdi), %rcx
	jne	.L723
.L1119:
	movq	(%rdi), %rax
.L719:
	testq	%r15, %r15
	je	.L1122
	movq	%rax, (%r15)
.L726:
	movq	(%rdi), %r13
	movl	$1040, %esi
	call	_ZdlPvm@PLT
	testq	%r13, %r13
	jne	.L1123
.L704:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	16(%rbx), %r12
	xorl	%r8d, %r8d
	movl	1384(%r12), %edi
	leaq	696(%r12), %rsi
	testl	%edi, %edi
	jle	.L740
	.p2align 4,,10
	.p2align 3
.L741:
	movq	8(%rsi), %rdx
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L730
	xorl	%eax, %eax
	xorl	%edi, %edi
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L1124:
	leaq	1(%r9), %rcx
.L732:
	testq	%rcx, %rcx
	je	.L733
	movq	%rcx, 16(%rdx,%rax,8)
	addq	$1, %rax
.L733:
	addq	$1, %rdi
	cmpq	%rdi, 8(%rdx)
	jbe	.L730
.L734:
	movq	16(%rdx,%rdi,8), %rcx
	movq	-1(%rcx), %r9
	testb	$1, %r9b
	je	.L1124
	movq	%rcx, %r9
	andq	$-262144, %r9
	testb	$8, 8(%r9)
	je	.L732
	addq	$1, %rdi
	cmpq	%rdi, 8(%rdx)
	ja	.L734
.L730:
	movq	%rax, 8(%rdx)
	movq	(%rsi), %rdx
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L735
	xorl	%eax, %eax
	xorl	%edi, %edi
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L1125:
	leaq	1(%r9), %rcx
.L737:
	testq	%rcx, %rcx
	je	.L738
	movq	%rcx, 16(%rdx,%rax,8)
	addq	$1, %rax
.L738:
	addq	$1, %rdi
	cmpq	%rdi, 8(%rdx)
	jbe	.L735
.L739:
	movq	16(%rdx,%rdi,8), %rcx
	movq	-1(%rcx), %r9
	testb	$1, %r9b
	je	.L1125
	movq	%rcx, %r9
	andq	$-262144, %r9
	testb	$8, 8(%r9)
	je	.L737
	addq	$1, %rdi
	cmpq	%rdi, 8(%rdx)
	ja	.L739
.L735:
	movq	%rax, 8(%rdx)
	addl	$1, %r8d
	addq	$80, %rsi
	cmpl	%r8d, 1384(%r12)
	jg	.L741
.L740:
	leaq	1336(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	1376(%r12), %rdi
	testq	%rdi, %rdi
	je	.L728
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	cmpq	$0, 8(%rdi)
	jne	.L742
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1128:
	leaq	1(%rsi), %rdx
.L745:
	testq	%rdx, %rdx
	je	.L746
	movq	%rdx, 16(%rdi,%rax,8)
	addq	$1, %rax
.L746:
	addq	$1, %rcx
	cmpq	%rcx, 8(%rdi)
	jbe	.L1127
.L742:
	movq	16(%rdi,%rcx,8), %rdx
	movq	-1(%rdx), %rsi
	testb	$1, %sil
	je	.L1128
	movq	%rdx, %rsi
	andq	$-262144, %rsi
	testb	$8, 8(%rsi)
	je	.L745
	addq	$1, %rcx
	cmpq	%rcx, 8(%rdi)
	ja	.L742
.L1127:
	movq	(%rdi), %r13
	movq	%rax, 8(%rdi)
	movq	%r13, %rdx
	testq	%rax, %rax
	je	.L743
	movq	%rdi, %r14
	testq	%r13, %r13
	je	.L728
.L1130:
	movq	%r13, %rdi
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	cmpq	$0, 8(%rdi)
	jne	.L742
.L1126:
	movq	(%rdi), %rdx
.L743:
	testq	%r14, %r14
	je	.L1129
	movq	%rdx, (%r14)
.L749:
	movq	(%rdi), %r13
	movl	$528, %esi
	call	_ZdlPvm@PLT
	testq	%r13, %r13
	jne	.L1130
.L728:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	16(%rbx), %r12
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	movl	2080(%r12), %esi
	leaq	1392(%r12), %rdi
	testl	%esi, %esi
	jle	.L767
	.p2align 4,,10
	.p2align 3
.L768:
	movq	8(%rdi), %rcx
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L753
	leaq	16(%rcx), %r8
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L1131:
	leaq	1(%r13), %rsi
.L757:
	testq	%rax, %rax
	je	.L758
	testq	%rsi, %rsi
	je	.L758
	addq	$1, %rdx
	movq	%rax, %xmm0
	movq	%rsi, %xmm4
	movq	%rdx, %rax
	punpcklqdq	%xmm4, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rcx,%rax)
.L758:
	addq	$16, %r8
	cmpq	8(%rcx), %r11
	jnb	.L753
.L759:
	movq	(%r8), %rax
	movq	8(%r8), %rsi
	addq	$1, %r11
	movq	-1(%rax), %r13
	testb	$1, %r13b
	jne	.L754
	leaq	1(%r13), %rax
.L755:
	movq	-1(%rsi), %r13
	testb	$1, %r13b
	je	.L1131
	movq	%rsi, %r13
	andq	$-262144, %r13
	testb	$8, 8(%r13)
	je	.L757
	addq	$16, %r8
	cmpq	8(%rcx), %r11
	jb	.L759
.L753:
	movq	%rdx, 8(%rcx)
	movq	(%rdi), %rcx
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L760
	leaq	16(%rcx), %r8
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L1132:
	leaq	1(%r13), %rax
	movq	-1(%rsi), %r13
	testb	$1, %r13b
	jne	.L763
.L1133:
	leaq	1(%r13), %rsi
.L764:
	testq	%rax, %rax
	je	.L765
	testq	%rsi, %rsi
	je	.L765
	addq	$1, %rdx
	movq	%rax, %xmm0
	movq	%rsi, %xmm5
	movq	%rdx, %rax
	punpcklqdq	%xmm5, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rcx,%rax)
.L765:
	addq	$16, %r8
	cmpq	%r11, 8(%rcx)
	jbe	.L760
.L766:
	movq	(%r8), %rax
	movq	8(%r8), %rsi
	addq	$1, %r11
	movq	-1(%rax), %r13
	testb	$1, %r13b
	je	.L1132
	movq	%rax, %r13
	andq	$-262144, %r13
	testb	$8, 8(%r13)
	movq	-1(%rsi), %r13
	cmovne	%r10, %rax
	testb	$1, %r13b
	je	.L1133
.L763:
	movq	%rsi, %r13
	andq	$-262144, %r13
	testb	$8, 8(%r13)
	je	.L764
	addq	$16, %r8
	cmpq	%r11, 8(%rcx)
	ja	.L766
.L760:
	movq	%rdx, 8(%rcx)
	addl	$1, %r9d
	addq	$80, %rdi
	cmpl	%r9d, 2080(%r12)
	jg	.L768
.L767:
	leaq	2032(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	2072(%r12), %rdi
	testq	%rdi, %rdi
	je	.L751
	xorl	%r13d, %r13d
	xorl	%edx, %edx
	leaq	16(%rdi), %rsi
	xorl	%r8d, %r8d
	cmpq	$0, 8(%rdi)
	jne	.L776
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1136:
	leaq	1(%r10), %rcx
.L774:
	testq	%rax, %rax
	je	.L775
	testq	%rcx, %rcx
	je	.L775
	addq	$1, %rdx
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%rdx, %rax
	punpcklqdq	%xmm6, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rdi,%rax)
.L775:
	addq	$16, %rsi
	cmpq	8(%rdi), %r8
	jnb	.L1135
.L776:
	movq	(%rsi), %rax
	movq	8(%rsi), %rcx
	addq	$1, %r8
	movq	-1(%rax), %r10
	testb	$1, %r10b
	jne	.L771
	leaq	1(%r10), %rax
.L772:
	movq	-1(%rcx), %r10
	testb	$1, %r10b
	je	.L1136
	movq	%rcx, %r10
	andq	$-262144, %r10
	testb	$8, 8(%r10)
	je	.L774
	addq	$16, %rsi
	cmpq	8(%rdi), %r8
	jb	.L776
.L1135:
	movq	(%rdi), %r14
	movq	%rdx, 8(%rdi)
	movq	%r14, %rax
	testq	%rdx, %rdx
	je	.L770
	movq	%rdi, %r13
	testq	%r14, %r14
	je	.L751
.L1138:
	movq	%r14, %rdi
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	cmpq	$0, 8(%rdi)
	leaq	16(%rdi), %rsi
	jne	.L776
.L1134:
	movq	(%rdi), %rax
.L770:
	testq	%r13, %r13
	je	.L1137
	movq	%rax, 0(%r13)
.L779:
	movq	(%rdi), %r14
	movl	$1040, %esi
	call	_ZdlPvm@PLT
	testq	%r14, %r14
	jne	.L1138
.L751:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	16(%rbx), %r12
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	movl	2776(%r12), %ecx
	leaq	2088(%r12), %rdi
	testl	%ecx, %ecx
	jle	.L797
	.p2align 4,,10
	.p2align 3
.L798:
	movq	8(%rdi), %rcx
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L783
	leaq	16(%rcx), %r8
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L1139:
	leaq	1(%r13), %rsi
.L787:
	testq	%rax, %rax
	je	.L788
	testq	%rsi, %rsi
	je	.L788
	addq	$1, %rdx
	movq	%rax, %xmm0
	movq	%rsi, %xmm7
	movq	%rdx, %rax
	punpcklqdq	%xmm7, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rcx,%rax)
.L788:
	addq	$16, %r8
	cmpq	%r11, 8(%rcx)
	jbe	.L783
.L789:
	movq	(%r8), %rax
	movq	8(%r8), %rsi
	addq	$1, %r11
	movq	-1(%rax), %r13
	testb	$1, %r13b
	jne	.L784
	leaq	1(%r13), %rax
.L785:
	movq	-1(%rsi), %r13
	testb	$1, %r13b
	je	.L1139
	movq	%rsi, %r13
	andq	$-262144, %r13
	testb	$8, 8(%r13)
	je	.L787
	addq	$16, %r8
	cmpq	%r11, 8(%rcx)
	ja	.L789
.L783:
	movq	%rdx, 8(%rcx)
	movq	(%rdi), %rcx
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L790
	leaq	16(%rcx), %r8
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L1140:
	leaq	1(%r13), %rax
	movq	-1(%rsi), %r13
	testb	$1, %r13b
	jne	.L793
.L1141:
	leaq	1(%r13), %rsi
.L794:
	testq	%rax, %rax
	je	.L795
	testq	%rsi, %rsi
	je	.L795
	addq	$1, %rdx
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	%rdx, %rax
	punpcklqdq	%xmm1, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rcx,%rax)
.L795:
	addq	$16, %r8
	cmpq	8(%rcx), %r11
	jnb	.L790
.L796:
	movq	(%r8), %rax
	movq	8(%r8), %rsi
	addq	$1, %r11
	movq	-1(%rax), %r13
	testb	$1, %r13b
	je	.L1140
	movq	%rax, %r13
	andq	$-262144, %r13
	testb	$8, 8(%r13)
	movq	-1(%rsi), %r13
	cmovne	%r10, %rax
	testb	$1, %r13b
	je	.L1141
.L793:
	movq	%rsi, %r13
	andq	$-262144, %r13
	testb	$8, 8(%r13)
	je	.L794
	addq	$16, %r8
	cmpq	8(%rcx), %r11
	jb	.L796
.L790:
	movq	%rdx, 8(%rcx)
	addl	$1, %r9d
	addq	$80, %rdi
	cmpl	%r9d, 2776(%r12)
	jg	.L798
.L797:
	leaq	2728(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	2768(%r12), %rdi
	testq	%rdi, %rdi
	je	.L781
	xorl	%r13d, %r13d
	xorl	%edx, %edx
	leaq	16(%rdi), %rsi
	xorl	%r8d, %r8d
	cmpq	$0, 8(%rdi)
	jne	.L806
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1144:
	leaq	1(%r10), %rax
	movq	-1(%rcx), %r10
	testb	$1, %r10b
	jne	.L803
.L1145:
	leaq	1(%r10), %rcx
.L804:
	testq	%rax, %rax
	je	.L805
	testq	%rcx, %rcx
	je	.L805
	addq	$1, %rdx
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, %rax
	punpcklqdq	%xmm2, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rdi,%rax)
.L805:
	addq	$16, %rsi
	cmpq	8(%rdi), %r8
	jnb	.L1143
.L806:
	movq	(%rsi), %rax
	movq	8(%rsi), %rcx
	addq	$1, %r8
	movq	-1(%rax), %r10
	testb	$1, %r10b
	je	.L1144
	movq	%rax, %r10
	movl	$0, %r14d
	andq	$-262144, %r10
	testb	$8, 8(%r10)
	movq	-1(%rcx), %r10
	cmovne	%r14, %rax
	testb	$1, %r10b
	je	.L1145
.L803:
	movq	%rcx, %r10
	andq	$-262144, %r10
	testb	$8, 8(%r10)
	je	.L804
	addq	$16, %rsi
	cmpq	8(%rdi), %r8
	jb	.L806
.L1143:
	movq	(%rdi), %r14
	movq	%rdx, 8(%rdi)
	movq	%r14, %rax
	testq	%rdx, %rdx
	je	.L800
	movq	%rdi, %r13
	testq	%r14, %r14
	je	.L781
.L1147:
	movq	%r14, %rdi
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	cmpq	$0, 8(%rdi)
	leaq	16(%rdi), %rsi
	jne	.L806
.L1142:
	movq	(%rdi), %rax
.L800:
	testq	%r13, %r13
	je	.L1146
	movq	%rax, 0(%r13)
.L809:
	movq	(%rdi), %r14
	movl	$1040, %esi
	call	_ZdlPvm@PLT
	testq	%r14, %r14
	jne	.L1147
.L781:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	16(%rbx), %r12
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	movl	3472(%r12), %edx
	leaq	2784(%r12), %rdi
	testl	%edx, %edx
	jle	.L827
	.p2align 4,,10
	.p2align 3
.L828:
	movq	8(%rdi), %rcx
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L813
	leaq	16(%rcx), %r8
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L1148:
	leaq	1(%r13), %rax
	movq	-1(%rsi), %r13
	testb	$1, %r13b
	jne	.L816
.L1149:
	leaq	1(%r13), %rsi
.L817:
	testq	%rax, %rax
	je	.L818
	testq	%rsi, %rsi
	je	.L818
	addq	$1, %rdx
	movq	%rax, %xmm0
	movq	%rsi, %xmm3
	movq	%rdx, %rax
	punpcklqdq	%xmm3, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rcx,%rax)
.L818:
	addq	$16, %r8
	cmpq	8(%rcx), %r11
	jnb	.L813
.L819:
	movq	(%r8), %rax
	movq	8(%r8), %rsi
	addq	$1, %r11
	movq	-1(%rax), %r13
	testb	$1, %r13b
	je	.L1148
	movq	%rax, %r13
	andq	$-262144, %r13
	testb	$8, 8(%r13)
	movq	-1(%rsi), %r13
	cmovne	%r10, %rax
	testb	$1, %r13b
	je	.L1149
.L816:
	movq	%rsi, %r13
	andq	$-262144, %r13
	testb	$8, 8(%r13)
	je	.L817
	addq	$16, %r8
	cmpq	8(%rcx), %r11
	jb	.L819
.L813:
	movq	%rdx, 8(%rcx)
	movq	(%rdi), %rcx
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L820
	leaq	16(%rcx), %r8
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L1150:
	leaq	1(%r13), %rax
	movq	-1(%rsi), %r13
	testb	$1, %r13b
	jne	.L823
.L1151:
	leaq	1(%r13), %rsi
.L824:
	testq	%rax, %rax
	je	.L825
	testq	%rsi, %rsi
	je	.L825
	addq	$1, %rdx
	movq	%rax, %xmm0
	movq	%rsi, %xmm4
	movq	%rdx, %rax
	punpcklqdq	%xmm4, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rcx,%rax)
.L825:
	addq	$16, %r8
	cmpq	%r11, 8(%rcx)
	jbe	.L820
.L826:
	movq	(%r8), %rax
	movq	8(%r8), %rsi
	addq	$1, %r11
	movq	-1(%rax), %r13
	testb	$1, %r13b
	je	.L1150
	movq	%rax, %r13
	andq	$-262144, %r13
	testb	$8, 8(%r13)
	movq	-1(%rsi), %r13
	cmovne	%r10, %rax
	testb	$1, %r13b
	je	.L1151
.L823:
	movq	%rsi, %r13
	andq	$-262144, %r13
	testb	$8, 8(%r13)
	je	.L824
	addq	$16, %r8
	cmpq	%r11, 8(%rcx)
	ja	.L826
.L820:
	movq	%rdx, 8(%rcx)
	addl	$1, %r9d
	addq	$80, %rdi
	cmpl	%r9d, 3472(%r12)
	jg	.L828
.L827:
	leaq	3424(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	3464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L811
	xorl	%r13d, %r13d
	xorl	%edx, %edx
	leaq	16(%rdi), %rsi
	xorl	%r8d, %r8d
	cmpq	$0, 8(%rdi)
	jne	.L836
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1154:
	leaq	1(%r10), %rcx
.L834:
	testq	%rax, %rax
	je	.L835
	testq	%rcx, %rcx
	je	.L835
	addq	$1, %rdx
	movq	%rax, %xmm0
	movq	%rcx, %xmm5
	movq	%rdx, %rax
	punpcklqdq	%xmm5, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rdi,%rax)
.L835:
	addq	$16, %rsi
	cmpq	8(%rdi), %r8
	jnb	.L1153
.L836:
	movq	(%rsi), %rax
	movq	8(%rsi), %rcx
	addq	$1, %r8
	movq	-1(%rax), %r10
	testb	$1, %r10b
	jne	.L831
	leaq	1(%r10), %rax
.L832:
	movq	-1(%rcx), %r10
	testb	$1, %r10b
	je	.L1154
	movq	%rcx, %r10
	andq	$-262144, %r10
	testb	$8, 8(%r10)
	je	.L834
	addq	$16, %rsi
	cmpq	8(%rdi), %r8
	jb	.L836
.L1153:
	movq	(%rdi), %r14
	movq	%rdx, 8(%rdi)
	movq	%r14, %rax
	testq	%rdx, %rdx
	je	.L830
	movq	%rdi, %r13
	testq	%r14, %r14
	je	.L811
.L1156:
	movq	%r14, %rdi
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	cmpq	$0, 8(%rdi)
	leaq	16(%rdi), %rsi
	jne	.L836
.L1152:
	movq	(%rdi), %rax
.L830:
	testq	%r13, %r13
	je	.L1155
	movq	%rax, 0(%r13)
.L839:
	movq	(%rdi), %r14
	movl	$1040, %esi
	call	_ZdlPvm@PLT
	testq	%r14, %r14
	jne	.L1156
.L811:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	16(%rbx), %rbx
	xorl	%r8d, %r8d
	movl	7648(%rbx), %eax
	leaq	6960(%rbx), %rsi
	testl	%eax, %eax
	jle	.L853
	.p2align 4,,10
	.p2align 3
.L854:
	movq	8(%rsi), %rdx
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L843
	xorl	%eax, %eax
	xorl	%edi, %edi
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L1157:
	leaq	1(%r9), %rcx
.L845:
	testq	%rcx, %rcx
	je	.L846
	movq	%rcx, 16(%rdx,%rax,8)
	addq	$1, %rax
.L846:
	addq	$1, %rdi
	cmpq	%rdi, 8(%rdx)
	jbe	.L843
.L847:
	movq	16(%rdx,%rdi,8), %rcx
	movq	-1(%rcx), %r9
	testb	$1, %r9b
	je	.L1157
	movq	%rcx, %r9
	andq	$-262144, %r9
	testb	$8, 8(%r9)
	je	.L845
	addq	$1, %rdi
	cmpq	%rdi, 8(%rdx)
	ja	.L847
.L843:
	movq	%rax, 8(%rdx)
	movq	(%rsi), %rdx
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L848
	xorl	%eax, %eax
	xorl	%edi, %edi
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L1158:
	leaq	1(%r9), %rcx
.L850:
	testq	%rcx, %rcx
	je	.L851
	movq	%rcx, 16(%rdx,%rax,8)
	addq	$1, %rax
.L851:
	addq	$1, %rdi
	cmpq	%rdi, 8(%rdx)
	jbe	.L848
.L852:
	movq	16(%rdx,%rdi,8), %rcx
	movq	-1(%rcx), %r9
	testb	$1, %r9b
	je	.L1158
	movq	%rcx, %r9
	andq	$-262144, %r9
	testb	$8, 8(%r9)
	je	.L850
	addq	$1, %rdi
	cmpq	%rdi, 8(%rdx)
	ja	.L852
.L848:
	movq	%rax, 8(%rdx)
	addl	$1, %r8d
	addq	$80, %rsi
	cmpl	%r8d, 7648(%rbx)
	jg	.L854
.L853:
	leaq	7600(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	7640(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L841
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	cmpq	$0, 8(%rdi)
	jne	.L855
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1161:
	leaq	1(%rsi), %rdx
.L858:
	testq	%rdx, %rdx
	je	.L859
	movq	%rdx, 16(%rdi,%rax,8)
	addq	$1, %rax
.L859:
	addq	$1, %rcx
	cmpq	%rcx, 8(%rdi)
	jbe	.L1160
.L855:
	movq	16(%rdi,%rcx,8), %rdx
	movq	-1(%rdx), %rsi
	testb	$1, %sil
	je	.L1161
	movq	%rdx, %rsi
	andq	$-262144, %rsi
	testb	$8, 8(%rsi)
	je	.L858
	addq	$1, %rcx
	cmpq	%rcx, 8(%rdi)
	ja	.L855
.L1160:
	movq	(%rdi), %r12
	movq	%rax, 8(%rdi)
	movq	%r12, %rdx
	testq	%rax, %rax
	je	.L856
	movq	%rdi, %r13
	testq	%r12, %r12
	je	.L841
.L1163:
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	cmpq	$0, 8(%rdi)
	jne	.L855
.L1159:
	movq	(%rdi), %rdx
.L856:
	testq	%r13, %r13
	je	.L1162
	movq	%rdx, 0(%r13)
.L862:
	movq	(%rdi), %r12
	movl	$528, %esi
	call	_ZdlPvm@PLT
	testq	%r12, %r12
	jne	.L1163
.L841:
	addq	$8, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L754:
	.cfi_restore_state
	movq	%rax, %r13
	andq	$-262144, %r13
	testb	$8, 8(%r13)
	cmovne	%r10, %rax
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L771:
	movq	%rax, %r10
	movl	$0, %r11d
	andq	$-262144, %r10
	testb	$8, 8(%r10)
	cmovne	%r11, %rax
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L784:
	movq	%rax, %r13
	andq	$-262144, %r13
	testb	$8, 8(%r13)
	cmovne	%r10, %rax
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L831:
	movq	%rax, %r10
	movl	$0, %r11d
	andq	$-262144, %r10
	testb	$8, 8(%r10)
	cmovne	%r11, %rax
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	%rax, 2768(%r12)
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	%rdx, 7640(%rbx)
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	%rax, 3464(%r12)
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	%rax, 4160(%r12)
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	%rax, 4856(%r12)
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L1129:
	movq	%rdx, 1376(%r12)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	%rax, 2072(%r12)
	jmp	.L779
	.cfi_endproc
.LFE22386:
	.size	_ZN2v88internal18IncrementalMarking33UpdateWeakReferencesAfterScavengeEv, .-_ZN2v88internal18IncrementalMarking33UpdateWeakReferencesAfterScavengeEv
	.section	.text._ZN2v88internal18IncrementalMarking34UpdateMarkingWorklistAfterScavengeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking34UpdateMarkingWorklistAfterScavengeEv
	.type	_ZN2v88internal18IncrementalMarking34UpdateMarkingWorklistAfterScavengeEv, @function
_ZN2v88internal18IncrementalMarking34UpdateMarkingWorklistAfterScavengeEv:
.LFB22384:
	.cfi_startproc
	endbr64
	cmpl	$1, 80(%rdi)
	jle	.L1164
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r10d, %r10d
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %rax
	movl	688(%rbx), %ecx
	movq	-37528(%rax), %r12
	movq	%rbx, %r9
	testl	%ecx, %ecx
	jle	.L1183
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	8(%r9), %rsi
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L1169
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1170:
	testb	$16, %cl
	jne	.L1392
	testl	$131072, %ecx
	jne	.L1392
	movq	-1(%rax), %rcx
	cmpq	%rcx, %r12
	je	.L1171
.L1391:
	movq	%rax, (%r14)
.L1172:
	addq	$1, %rdx
.L1171:
	addq	$1, %r11
	cmpq	%r11, 8(%rsi)
	jbe	.L1169
.L1175:
	movq	16(%rsi,%r11,8), %rax
	leaq	16(%rsi,%rdx,8), %r14
	movq	%rax, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rcx
	testb	$8, %cl
	je	.L1170
	movq	-1(%rax), %rax
	testb	$1, %al
	jne	.L1171
	addq	$1, %rax
	movq	%rax, (%r14)
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1392:
	movl	%eax, %ecx
	subl	%edi, %ecx
	movq	264(%rdi), %rdi
	movl	%ecx, %r15d
	shrl	$3, %ecx
	shrl	$8, %r15d
	movl	(%rdi,%r15,4), %edi
	movl	%r8d, %r15d
	sall	%cl, %r15d
	testl	%edi, %r15d
	jne	.L1391
	addq	$1, %r11
	cmpq	%r11, 8(%rsi)
	ja	.L1175
	.p2align 4,,10
	.p2align 3
.L1169:
	movq	%rdx, 8(%rsi)
	movq	(%r9), %rsi
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L1176
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L1182
	.p2align 4,,10
	.p2align 3
.L1177:
	testb	$16, %cl
	jne	.L1394
	testl	$131072, %ecx
	jne	.L1394
	movq	-1(%rax), %rcx
	cmpq	%rcx, %r12
	je	.L1178
.L1393:
	movq	%rax, (%r14)
.L1179:
	addq	$1, %rdx
.L1178:
	addq	$1, %r11
	cmpq	%r11, 8(%rsi)
	jbe	.L1176
.L1182:
	movq	16(%rsi,%r11,8), %rax
	leaq	16(%rsi,%rdx,8), %r14
	movq	%rax, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rcx
	testb	$8, %cl
	je	.L1177
	movq	-1(%rax), %rax
	testb	$1, %al
	jne	.L1178
	addq	$1, %rax
	movq	%rax, (%r14)
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1394:
	movl	%eax, %ecx
	subl	%edi, %ecx
	movq	264(%rdi), %rdi
	movl	%ecx, %r15d
	shrl	$3, %ecx
	shrl	$8, %r15d
	movl	(%rdi,%r15,4), %edi
	movl	%r8d, %r15d
	sall	%cl, %r15d
	testl	%edi, %r15d
	jne	.L1393
	addq	$1, %r11
	cmpq	%r11, 8(%rsi)
	ja	.L1182
	.p2align 4,,10
	.p2align 3
.L1176:
	movq	%rdx, 8(%rsi)
	addl	$1, %r10d
	addq	$80, %r9
	cmpl	%r10d, 688(%rbx)
	jg	.L1184
.L1183:
	leaq	640(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1167
	xorl	%r8d, %r8d
	cmpq	$0, 8(%rdi)
	movl	$1, %r9d
	je	.L1185
	.p2align 4,,10
	.p2align 3
.L1417:
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1186:
	testb	$16, %cl
	jne	.L1396
	testl	$131072, %ecx
	jne	.L1396
	movq	-1(%rax), %rcx
	cmpq	%rcx, %r12
	je	.L1187
.L1395:
	movq	%rax, (%r11)
.L1188:
	addq	$1, %rdx
.L1187:
	addq	$1, %r10
	cmpq	%r10, 8(%rdi)
	jbe	.L1416
.L1192:
	movq	16(%rdi,%r10,8), %rax
	leaq	16(%rdi,%rdx,8), %r11
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	8(%rsi), %rcx
	testb	$8, %cl
	je	.L1186
	movq	-1(%rax), %rax
	testb	$1, %al
	jne	.L1187
	addq	$1, %rax
	movq	%rax, (%r11)
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1416:
	movq	(%rdi), %r15
	movq	%rdx, 8(%rdi)
	movq	%r15, %rax
	testq	%rdx, %rdx
	je	.L1193
	movq	%rdi, %r8
	testq	%r15, %r15
	je	.L1167
.L1419:
	movq	%r15, %rdi
	cmpq	$0, 8(%rdi)
	jne	.L1417
.L1185:
	movq	(%rdi), %rax
.L1193:
	testq	%r8, %r8
	je	.L1418
	movq	%rax, (%r8)
.L1196:
	movq	(%rdi), %r15
	movl	$528, %esi
	movq	%r8, -56(%rbp)
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %r8
	movl	$1, %r9d
	testq	%r15, %r15
	jne	.L1419
.L1167:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	1384(%rbx), %edx
	xorl	%r10d, %r10d
	leaq	696(%rbx), %r9
	movl	$1, %r8d
	testl	%edx, %edx
	jle	.L1214
	.p2align 4,,10
	.p2align 3
.L1215:
	movq	8(%r9), %rsi
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L1200
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L1206
	.p2align 4,,10
	.p2align 3
.L1201:
	testb	$16, %cl
	jne	.L1398
	testl	$131072, %ecx
	jne	.L1398
	movq	-1(%rax), %rcx
	cmpq	%rcx, %r12
	je	.L1202
.L1397:
	movq	%rax, (%r14)
.L1203:
	addq	$1, %rdx
.L1202:
	addq	$1, %r11
	cmpq	%r11, 8(%rsi)
	jbe	.L1200
.L1206:
	movq	16(%rsi,%r11,8), %rax
	leaq	16(%rsi,%rdx,8), %r14
	movq	%rax, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rcx
	testb	$8, %cl
	je	.L1201
	movq	-1(%rax), %rax
	testb	$1, %al
	jne	.L1202
	addq	$1, %rax
	movq	%rax, (%r14)
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1396:
	movl	%eax, %ecx
	subl	%esi, %ecx
	movq	264(%rsi), %rsi
	movl	%ecx, %r15d
	shrl	$3, %ecx
	shrl	$8, %r15d
	movl	(%rsi,%r15,4), %esi
	movl	%r9d, %r15d
	sall	%cl, %r15d
	testl	%esi, %r15d
	je	.L1187
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1418:
	movq	%rax, 680(%rbx)
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1398:
	movl	%eax, %ecx
	subl	%edi, %ecx
	movq	264(%rdi), %rdi
	movl	%ecx, %r15d
	shrl	$3, %ecx
	shrl	$8, %r15d
	movl	(%rdi,%r15,4), %edi
	movl	%r8d, %r15d
	sall	%cl, %r15d
	testl	%edi, %r15d
	jne	.L1397
	addq	$1, %r11
	cmpq	%r11, 8(%rsi)
	ja	.L1206
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	%rdx, 8(%rsi)
	movq	(%r9), %rsi
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L1207
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1208:
	testb	$16, %cl
	jne	.L1400
	testl	$131072, %ecx
	jne	.L1400
	movq	-1(%rax), %rcx
	cmpq	%rcx, %r12
	je	.L1209
.L1399:
	movq	%rax, (%r14)
.L1210:
	addq	$1, %rdx
.L1209:
	addq	$1, %r11
	cmpq	%r11, 8(%rsi)
	jbe	.L1207
.L1213:
	movq	16(%rsi,%r11,8), %rax
	leaq	16(%rsi,%rdx,8), %r14
	movq	%rax, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rcx
	testb	$8, %cl
	je	.L1208
	movq	-1(%rax), %rax
	testb	$1, %al
	jne	.L1209
	addq	$1, %rax
	movq	%rax, (%r14)
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1400:
	movl	%eax, %ecx
	subl	%edi, %ecx
	movq	264(%rdi), %rdi
	movl	%ecx, %r15d
	shrl	$3, %ecx
	shrl	$8, %r15d
	movl	(%rdi,%r15,4), %edi
	movl	%r8d, %r15d
	sall	%cl, %r15d
	testl	%edi, %r15d
	jne	.L1399
	addq	$1, %r11
	cmpq	%r11, 8(%rsi)
	ja	.L1213
	.p2align 4,,10
	.p2align 3
.L1207:
	movq	%rdx, 8(%rsi)
	addl	$1, %r10d
	addq	$80, %r9
	cmpl	%r10d, 1384(%rbx)
	jg	.L1215
.L1214:
	leaq	1336(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	1376(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1198
	xorl	%r8d, %r8d
	cmpq	$0, 8(%rdi)
	movl	$1, %r9d
	je	.L1216
	.p2align 4,,10
	.p2align 3
.L1421:
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1217:
	testb	$16, %cl
	jne	.L1402
	testl	$131072, %ecx
	jne	.L1402
	movq	-1(%rax), %rcx
	cmpq	%rcx, %r12
	je	.L1218
.L1401:
	movq	%rax, (%r11)
.L1219:
	addq	$1, %rdx
.L1218:
	addq	$1, %r10
	cmpq	%r10, 8(%rdi)
	jbe	.L1420
.L1223:
	movq	16(%rdi,%r10,8), %rax
	leaq	16(%rdi,%rdx,8), %r11
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	8(%rsi), %rcx
	testb	$8, %cl
	je	.L1217
	movq	-1(%rax), %rax
	testb	$1, %al
	jne	.L1218
	addq	$1, %rax
	movq	%rax, (%r11)
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1402:
	movl	%eax, %ecx
	subl	%esi, %ecx
	movq	264(%rsi), %rsi
	movl	%ecx, %r15d
	shrl	$3, %ecx
	shrl	$8, %r15d
	movl	(%rsi,%r15,4), %esi
	movl	%r9d, %r15d
	sall	%cl, %r15d
	testl	%esi, %r15d
	jne	.L1401
	addq	$1, %r10
	cmpq	%r10, 8(%rdi)
	ja	.L1223
.L1420:
	movq	(%rdi), %r15
	movq	%rdx, 8(%rdi)
	movq	%r15, %rax
	testq	%rdx, %rdx
	je	.L1224
	movq	%rdi, %r8
	testq	%r15, %r15
	je	.L1198
.L1423:
	movq	%r15, %rdi
	cmpq	$0, 8(%rdi)
	jne	.L1421
.L1216:
	movq	(%rdi), %rax
.L1224:
	testq	%r8, %r8
	je	.L1422
	movq	%rax, (%r8)
.L1227:
	movq	(%rdi), %r15
	movl	$528, %esi
	movq	%r8, -56(%rbp)
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %r8
	movl	$1, %r9d
	testq	%r15, %r15
	jne	.L1423
.L1198:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	2080(%rbx), %eax
	xorl	%r10d, %r10d
	leaq	1392(%rbx), %r9
	movl	$1, %r8d
	testl	%eax, %eax
	jle	.L1245
	.p2align 4,,10
	.p2align 3
.L1246:
	movq	8(%r9), %rsi
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L1231
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1232:
	testb	$16, %cl
	jne	.L1404
	testl	$131072, %ecx
	jne	.L1404
	movq	-1(%rax), %rcx
	cmpq	%rcx, %r12
	je	.L1233
.L1403:
	movq	%rax, (%r14)
.L1234:
	addq	$1, %rdx
.L1233:
	addq	$1, %r11
	cmpq	%r11, 8(%rsi)
	jbe	.L1231
.L1237:
	movq	16(%rsi,%r11,8), %rax
	leaq	16(%rsi,%rdx,8), %r14
	movq	%rax, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rcx
	testb	$8, %cl
	je	.L1232
	movq	-1(%rax), %rax
	testb	$1, %al
	jne	.L1233
	addq	$1, %rax
	movq	%rax, (%r14)
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1422:
	movq	%rax, 1376(%rbx)
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1404:
	movl	%eax, %ecx
	subl	%edi, %ecx
	movq	264(%rdi), %rdi
	movl	%ecx, %r15d
	shrl	$3, %ecx
	shrl	$8, %r15d
	movl	(%rdi,%r15,4), %edi
	movl	%r8d, %r15d
	sall	%cl, %r15d
	testl	%edi, %r15d
	jne	.L1403
	addq	$1, %r11
	cmpq	%r11, 8(%rsi)
	ja	.L1237
	.p2align 4,,10
	.p2align 3
.L1231:
	movq	%rdx, 8(%rsi)
	movq	(%r9), %rsi
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L1238
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1239:
	testb	$16, %cl
	jne	.L1406
	testl	$131072, %ecx
	jne	.L1406
	movq	-1(%rax), %rcx
	cmpq	%rcx, %r12
	je	.L1240
.L1405:
	movq	%rax, (%r14)
.L1241:
	addq	$1, %rdx
.L1240:
	addq	$1, %r11
	cmpq	%r11, 8(%rsi)
	jbe	.L1238
.L1244:
	movq	16(%rsi,%r11,8), %rax
	leaq	16(%rsi,%rdx,8), %r14
	movq	%rax, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rcx
	testb	$8, %cl
	je	.L1239
	movq	-1(%rax), %rax
	testb	$1, %al
	jne	.L1240
	addq	$1, %rax
	movq	%rax, (%r14)
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1406:
	movl	%eax, %ecx
	subl	%edi, %ecx
	movq	264(%rdi), %rdi
	movl	%ecx, %r15d
	shrl	$3, %ecx
	shrl	$8, %r15d
	movl	(%rdi,%r15,4), %edi
	movl	%r8d, %r15d
	sall	%cl, %r15d
	testl	%edi, %r15d
	jne	.L1405
	addq	$1, %r11
	cmpq	%r11, 8(%rsi)
	ja	.L1244
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	%rdx, 8(%rsi)
	addl	$1, %r10d
	addq	$80, %r9
	cmpl	%r10d, 2080(%rbx)
	jg	.L1246
.L1245:
	leaq	2032(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	2072(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1229
	xorl	%r8d, %r8d
	cmpq	$0, 8(%rdi)
	movl	$1, %r9d
	je	.L1247
	.p2align 4,,10
	.p2align 3
.L1425:
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1248:
	testb	$16, %cl
	jne	.L1408
	testl	$131072, %ecx
	jne	.L1408
	movq	-1(%rax), %rcx
	cmpq	%rcx, %r12
	je	.L1249
.L1407:
	movq	%rax, (%r11)
.L1250:
	addq	$1, %rdx
.L1249:
	addq	$1, %r10
	cmpq	%r10, 8(%rdi)
	jbe	.L1424
.L1254:
	movq	16(%rdi,%r10,8), %rax
	leaq	16(%rdi,%rdx,8), %r11
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	8(%rsi), %rcx
	testb	$8, %cl
	je	.L1248
	movq	-1(%rax), %rax
	testb	$1, %al
	jne	.L1249
	addq	$1, %rax
	movq	%rax, (%r11)
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1424:
	movq	(%rdi), %r15
	movq	%rdx, 8(%rdi)
	movq	%r15, %rax
	testq	%rdx, %rdx
	je	.L1255
	movq	%rdi, %r8
	testq	%r15, %r15
	je	.L1229
.L1427:
	movq	%r15, %rdi
	cmpq	$0, 8(%rdi)
	jne	.L1425
.L1247:
	movq	(%rdi), %rax
.L1255:
	testq	%r8, %r8
	je	.L1426
	movq	%rax, (%r8)
.L1258:
	movq	(%rdi), %r15
	movl	$144, %esi
	movq	%r8, -56(%rbp)
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %r8
	movl	$1, %r9d
	testq	%r15, %r15
	jne	.L1427
.L1229:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal18IncrementalMarking33UpdateWeakReferencesAfterScavengeEv
	.p2align 4,,10
	.p2align 3
.L1408:
	.cfi_restore_state
	movl	%eax, %ecx
	subl	%esi, %ecx
	movq	264(%rsi), %rsi
	movl	%ecx, %r15d
	shrl	$3, %ecx
	shrl	$8, %r15d
	movl	(%rsi,%r15,4), %esi
	movl	%r9d, %r15d
	sall	%cl, %r15d
	testl	%esi, %r15d
	je	.L1249
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	%rax, 2072(%rbx)
	jmp	.L1258
	.p2align 4,,10
	.p2align 3
.L1164:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE22384:
	.size	_ZN2v88internal18IncrementalMarking34UpdateMarkingWorklistAfterScavengeEv, .-_ZN2v88internal18IncrementalMarking34UpdateMarkingWorklistAfterScavengeEv
	.section	.text._ZN2v88internal18IncrementalMarking30UpdateMarkedBytesAfterScavengeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking30UpdateMarkedBytesAfterScavengeEm
	.type	_ZN2v88internal18IncrementalMarking30UpdateMarkedBytesAfterScavengeEm, @function
_ZN2v88internal18IncrementalMarking30UpdateMarkedBytesAfterScavengeEm:
.LFB22402:
	.cfi_startproc
	endbr64
	cmpl	$1, 80(%rdi)
	jle	.L1428
	movq	48(%rdi), %rax
	cmpq	%rsi, %rax
	cmovbe	%rax, %rsi
	subq	%rsi, %rax
	movq	%rax, 48(%rdi)
.L1428:
	ret
	.cfi_endproc
.LFE22402:
	.size	_ZN2v88internal18IncrementalMarking30UpdateMarkedBytesAfterScavengeEm, .-_ZN2v88internal18IncrementalMarking30UpdateMarkedBytesAfterScavengeEm
	.section	.text._ZN2v88internal18IncrementalMarking12EmbedderStepEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking12EmbedderStepEd
	.type	_ZN2v88internal18IncrementalMarking12EmbedderStepEd, @function
_ZN2v88internal18IncrementalMarking12EmbedderStepEd:
.LFB22408:
	.cfi_startproc
	endbr64
	cmpl	$2, 80(%rdi)
	je	.L1435
.L1430:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1435:
	cmpb	$0, _ZN2v88internal33FLAG_incremental_marking_wrappersE(%rip)
	je	.L1430
	movq	(%rdi), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	je	.L1430
	jmp	_ZN2v88internal18IncrementalMarking12EmbedderStepEd.part.0
	.cfi_endproc
.LFE22408:
	.size	_ZN2v88internal18IncrementalMarking12EmbedderStepEd, .-_ZN2v88internal18IncrementalMarking12EmbedderStepEd
	.section	.rodata._ZN2v88internal18IncrementalMarking15FinalizeMarkingENS1_16CompletionActionE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"[IncrementalMarking] requesting finalization of incremental marking.\n"
	.section	.text._ZN2v88internal18IncrementalMarking15FinalizeMarkingENS1_16CompletionActionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking15FinalizeMarkingENS1_16CompletionActionE
	.type	_ZN2v88internal18IncrementalMarking15FinalizeMarkingENS1_16CompletionActionE, @function
_ZN2v88internal18IncrementalMarking15FinalizeMarkingENS1_16CompletionActionE:
.LFB22412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	jne	.L1440
	movl	$2, 92(%rbx)
	testl	%r12d, %r12d
	je	.L1441
.L1436:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1440:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	.LC17(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movl	$2, 92(%rbx)
	testl	%r12d, %r12d
	jne	.L1436
.L1441:
	movq	(%rbx), %rdi
	movl	$2, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	subq	$80, %rdi
	jmp	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE@PLT
	.cfi_endproc
.LFE22412:
	.size	_ZN2v88internal18IncrementalMarking15FinalizeMarkingENS1_16CompletionActionE, .-_ZN2v88internal18IncrementalMarking15FinalizeMarkingENS1_16CompletionActionE
	.section	.rodata._ZN2v88internal18IncrementalMarking15MarkingCompleteENS1_16CompletionActionE.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"[IncrementalMarking] Complete (normal).\n"
	.section	.text._ZN2v88internal18IncrementalMarking15MarkingCompleteENS1_16CompletionActionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking15MarkingCompleteENS1_16CompletionActionE
	.type	_ZN2v88internal18IncrementalMarking15MarkingCompleteENS1_16CompletionActionE, @function
_ZN2v88internal18IncrementalMarking15MarkingCompleteENS1_16CompletionActionE:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movl	$3, 80(%rdi)
	movb	$1, 2736(%rax)
	movb	$1, 85(%rdi)
	jne	.L1446
	movl	$1, 92(%rbx)
	testl	%r12d, %r12d
	je	.L1447
.L1442:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1446:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	.LC18(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movl	$1, 92(%rbx)
	testl	%r12d, %r12d
	jne	.L1442
.L1447:
	movq	(%rbx), %rdi
	movl	$2, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	subq	$80, %rdi
	jmp	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE@PLT
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal18IncrementalMarking15MarkingCompleteENS1_16CompletionActionE, .-_ZN2v88internal18IncrementalMarking15MarkingCompleteENS1_16CompletionActionE
	.section	.text._ZN2v88internal18IncrementalMarking8EpilogueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking8EpilogueEv
	.type	_ZN2v88internal18IncrementalMarking8EpilogueEv, @function
_ZN2v88internal18IncrementalMarking8EpilogueEv:
.LFB22414:
	.cfi_startproc
	endbr64
	movb	$0, 86(%rdi)
	movb	$0, 88(%rdi)
	ret
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal18IncrementalMarking8EpilogueEv, .-_ZN2v88internal18IncrementalMarking8EpilogueEv
	.section	.text._ZN2v88internal18IncrementalMarking20ShouldDoEmbedderStepEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking20ShouldDoEmbedderStepEv
	.type	_ZN2v88internal18IncrementalMarking20ShouldDoEmbedderStepEv, @function
_ZN2v88internal18IncrementalMarking20ShouldDoEmbedderStepEv:
.LFB22415:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$2, 80(%rdi)
	je	.L1455
.L1449:
	ret
	.p2align 4,,10
	.p2align 3
.L1455:
	movzbl	_ZN2v88internal33FLAG_incremental_marking_wrappersE(%rip), %eax
	testb	%al, %al
	je	.L1449
	movq	(%rdi), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	setne	%al
	ret
	.cfi_endproc
.LFE22415:
	.size	_ZN2v88internal18IncrementalMarking20ShouldDoEmbedderStepEv, .-_ZN2v88internal18IncrementalMarking20ShouldDoEmbedderStepEv
	.section	.rodata._ZN2v88internal18IncrementalMarking19FastForwardScheduleEv.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"[IncrementalMarking] Fast-forwarded schedule\n"
	.section	.text._ZN2v88internal18IncrementalMarking19FastForwardScheduleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking19FastForwardScheduleEv
	.type	_ZN2v88internal18IncrementalMarking19FastForwardScheduleEv, @function
_ZN2v88internal18IncrementalMarking19FastForwardScheduleEv:
.LFB22416:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	cmpq	%rax, 56(%rdi)
	jnb	.L1456
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movq	%rax, 56(%rdi)
	jne	.L1460
.L1456:
	ret
	.p2align 4,,10
	.p2align 3
.L1460:
	movq	(%rdi), %rdi
	leaq	.LC19(%rip), %rsi
	xorl	%eax, %eax
	subq	$37592, %rdi
	jmp	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	.cfi_endproc
.LFE22416:
	.size	_ZN2v88internal18IncrementalMarking19FastForwardScheduleEv, .-_ZN2v88internal18IncrementalMarking19FastForwardScheduleEv
	.section	.text._ZN2v88internal18IncrementalMarking40FastForwardScheduleIfCloseToFinalizationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking40FastForwardScheduleIfCloseToFinalizationEv
	.type	_ZN2v88internal18IncrementalMarking40FastForwardScheduleIfCloseToFinalizationEv, @function
_ZN2v88internal18IncrementalMarking40FastForwardScheduleIfCloseToFinalizationEv:
.LFB22417:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	48(%rdi), %rdx
	shrq	$2, %rax
	leaq	(%rax,%rax,2), %rax
	cmpq	%rax, %rdx
	jbe	.L1461
	cmpq	56(%rdi), %rdx
	jbe	.L1461
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movq	%rdx, 56(%rdi)
	jne	.L1465
.L1461:
	ret
	.p2align 4,,10
	.p2align 3
.L1465:
	movq	(%rdi), %rdi
	leaq	.LC19(%rip), %rsi
	xorl	%eax, %eax
	subq	$37592, %rdi
	jmp	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	.cfi_endproc
.LFE22417:
	.size	_ZN2v88internal18IncrementalMarking40FastForwardScheduleIfCloseToFinalizationEv, .-_ZN2v88internal18IncrementalMarking40FastForwardScheduleIfCloseToFinalizationEv
	.section	.rodata._ZN2v88internal18IncrementalMarking30ScheduleBytesToMarkBasedOnTimeEd.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"[IncrementalMarking] Scheduled %zuKB to mark based on time delta %.1fms\n"
	.section	.text._ZN2v88internal18IncrementalMarking30ScheduleBytesToMarkBasedOnTimeEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking30ScheduleBytesToMarkBasedOnTimeEd
	.type	_ZN2v88internal18IncrementalMarking30ScheduleBytesToMarkBasedOnTimeEd, @function
_ZN2v88internal18IncrementalMarking30ScheduleBytesToMarkBasedOnTimeEd:
.LFB22418:
	.cfi_startproc
	endbr64
	movsd	64(%rdi), %xmm1
	movsd	.LC22(%rip), %xmm2
	addsd	%xmm1, %xmm2
	comisd	%xmm0, %xmm2
	ja	.L1466
	movapd	%xmm0, %xmm4
	movsd	.LC21(%rip), %xmm2
	subsd	%xmm1, %xmm4
	comisd	%xmm4, %xmm2
	movapd	%xmm4, %xmm1
	ja	.L1485
	movq	32(%rdi), %rax
	movapd	%xmm2, %xmm1
	movsd	%xmm0, 64(%rdi)
	movsd	.LC20(%rip), %xmm3
	testq	%rax, %rax
	js	.L1472
.L1486:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L1473:
	mulsd	%xmm3, %xmm0
	movsd	.LC23(%rip), %xmm2
	comisd	%xmm2, %xmm0
	jnb	.L1474
	cvttsd2siq	%xmm0, %rdx
.L1475:
	movq	%rdx, %rax
	movq	$-1, %rcx
	addq	56(%rdi), %rax
	cmovc	%rcx, %rax
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movq	%rax, 56(%rdi)
	je	.L1466
	movq	(%rdi), %rdi
	shrq	$10, %rdx
	movapd	%xmm1, %xmm0
	movl	$1, %eax
	leaq	.LC24(%rip), %rsi
	subq	$37592, %rdi
	jmp	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1466:
	ret
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	32(%rdi), %rax
	movapd	%xmm4, %xmm3
	movsd	%xmm0, 64(%rdi)
	divsd	%xmm2, %xmm3
	testq	%rax, %rax
	jns	.L1486
.L1472:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L1473
	.p2align 4,,10
	.p2align 3
.L1474:
	subsd	%xmm2, %xmm0
	cvttsd2siq	%xmm0, %rdx
	btcq	$63, %rdx
	jmp	.L1475
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal18IncrementalMarking30ScheduleBytesToMarkBasedOnTimeEd, .-_ZN2v88internal18IncrementalMarking30ScheduleBytesToMarkBasedOnTimeEd
	.section	.text._ZN2v88internal18IncrementalMarking16FinalizeSweepingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking16FinalizeSweepingEv
	.type	_ZN2v88internal18IncrementalMarking16FinalizeSweepingEv, @function
_ZN2v88internal18IncrementalMarking16FinalizeSweepingEv:
.LFB22421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	2016(%rax), %rdi
	movq	9984(%rdi), %r8
	cmpb	$0, 265(%r8)
	jne	.L1488
.L1491:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal18IncrementalMarking12StartMarkingEv
	.p2align 4,,10
	.p2align 3
.L1488:
	.cfi_restore_state
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_sweepingE(%rip)
	jne	.L1493
.L1489:
	call	_ZN2v88internal20MarkCompactCollector23EnsureSweepingCompletedEv@PLT
.L1490:
	movq	(%r12), %rax
	movq	2016(%rax), %rax
	movq	9984(%rax), %rax
	cmpb	$0, 265(%rax)
	je	.L1491
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1493:
	.cfi_restore_state
	movq	%r8, %rdi
	call	_ZN2v88internal7Sweeper22AreSweeperTasksRunningEv@PLT
	testb	%al, %al
	jne	.L1490
	movq	(%r12), %rax
	movq	2016(%rax), %rdi
	jmp	.L1489
	.cfi_endproc
.LFE22421:
	.size	_ZN2v88internal18IncrementalMarking16FinalizeSweepingEv, .-_ZN2v88internal18IncrementalMarking16FinalizeSweepingEv
	.section	.text._ZN2v88internal18IncrementalMarking31StepSizeToKeepUpWithAllocationsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking31StepSizeToKeepUpWithAllocationsEv
	.type	_ZN2v88internal18IncrementalMarking31StepSizeToKeepUpWithAllocationsEv, @function
_ZN2v88internal18IncrementalMarking31StepSizeToKeepUpWithAllocationsEv:
.LFB22422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %r13
	movq	%r13, %rdi
	movq	2152(%r13), %rbx
	call	_ZN2v88internal4Heap26OldGenerationSizeOfObjectsEv@PLT
	subq	2160(%r13), %rbx
	addq	%rbx, %rax
	movq	%rax, %r8
	subq	40(%r12), %r8
	movq	%rax, 40(%r12)
	addq	$8, %rsp
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22422:
	.size	_ZN2v88internal18IncrementalMarking31StepSizeToKeepUpWithAllocationsEv, .-_ZN2v88internal18IncrementalMarking31StepSizeToKeepUpWithAllocationsEv
	.section	.text._ZN2v88internal18IncrementalMarking22StepSizeToMakeProgressEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking22StepSizeToMakeProgressEv
	.type	_ZN2v88internal18IncrementalMarking22StepSizeToMakeProgressEv, @function
_ZN2v88internal18IncrementalMarking22StepSizeToMakeProgressEv:
.LFB22423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	248(%rax), %rax
	movq	312(%rax), %rax
	shrq	$18, %rax
	movq	%rax, %r12
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	(%rbx), %rdi
	imulq	%r12, %rax
	leaq	67108864(%rax), %rsi
	call	_ZN2v88internal4Heap22CanExpandOldGenerationEm@PLT
	testb	%al, %al
	je	.L1500
	movq	32(%rbx), %rdx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rdx, %rax
	shrq	$8, %rax
	cmpq	$16777215, %rdx
	movl	$65536, %edx
	cmovbe	%rdx, %rax
	movl	$262144, %edx
	cmpq	$262144, %rax
	cmova	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1500:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	_ZN2v88internal4Heap26OldGenerationSizeOfObjectsEv@PLT
	popq	%rbx
	popq	%r12
	shrq	$5, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22423:
	.size	_ZN2v88internal18IncrementalMarking22StepSizeToMakeProgressEv, .-_ZN2v88internal18IncrementalMarking22StepSizeToMakeProgressEv
	.section	.text._ZN2v88internal18IncrementalMarking23AddScheduledBytesToMarkEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking23AddScheduledBytesToMarkEm
	.type	_ZN2v88internal18IncrementalMarking23AddScheduledBytesToMarkEm, @function
_ZN2v88internal18IncrementalMarking23AddScheduledBytesToMarkEm:
.LFB22424:
	.cfi_startproc
	endbr64
	addq	56(%rdi), %rsi
	movq	$-1, %rax
	cmovc	%rax, %rsi
	movq	%rsi, 56(%rdi)
	ret
	.cfi_endproc
.LFE22424:
	.size	_ZN2v88internal18IncrementalMarking23AddScheduledBytesToMarkEm, .-_ZN2v88internal18IncrementalMarking23AddScheduledBytesToMarkEm
	.section	.rodata._ZN2v88internal18IncrementalMarking36ScheduleBytesToMarkBasedOnAllocationEv.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"[IncrementalMarking] Scheduled %zuKB to mark based on allocation (progress=%zuKB, allocation=%zuKB)\n"
	.section	.text._ZN2v88internal18IncrementalMarking36ScheduleBytesToMarkBasedOnAllocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking36ScheduleBytesToMarkBasedOnAllocationEv
	.type	_ZN2v88internal18IncrementalMarking36ScheduleBytesToMarkBasedOnAllocationEv, @function
_ZN2v88internal18IncrementalMarking36ScheduleBytesToMarkBasedOnAllocationEv:
.LFB22425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	248(%rax), %rax
	movq	312(%rax), %r12
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	(%rbx), %rdi
	shrq	$18, %r12
	imulq	%rax, %r12
	leaq	67108864(%r12), %rsi
	call	_ZN2v88internal4Heap22CanExpandOldGenerationEm@PLT
	testb	%al, %al
	je	.L1517
	movq	32(%rbx), %rax
	movq	%rax, %r12
	shrq	$8, %r12
	cmpq	$16777215, %rax
	movl	$65536, %eax
	cmovbe	%rax, %r12
	movl	$262144, %eax
	cmpq	$262144, %r12
	cmova	%rax, %r12
.L1509:
	movq	(%rbx), %r14
	movq	%r14, %rdi
	movq	2152(%r14), %r13
	call	_ZN2v88internal4Heap26OldGenerationSizeOfObjectsEv@PLT
	subq	2160(%r14), %r13
	movq	$-1, %rcx
	addq	%r13, %rax
	movq	%rax, %r8
	subq	40(%rbx), %r8
	movq	%rax, 40(%rbx)
	leaq	(%r8,%r12), %rdx
	movq	%rdx, %rax
	addq	56(%rbx), %rax
	cmovc	%rcx, %rax
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movq	%rax, 56(%rbx)
	jne	.L1518
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1517:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	_ZN2v88internal4Heap26OldGenerationSizeOfObjectsEv@PLT
	shrq	$5, %rax
	movq	%rax, %r12
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	(%rbx), %rdi
	movq	%r12, %rcx
	popq	%rbx
	shrq	$10, %rdx
	popq	%r12
	shrq	$10, %rcx
	popq	%r13
	leaq	.LC25(%rip), %rsi
	popq	%r14
	subq	$37592, %rdi
	shrq	$10, %r8
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	.cfi_endproc
.LFE22425:
	.size	_ZN2v88internal18IncrementalMarking36ScheduleBytesToMarkBasedOnAllocationEv, .-_ZN2v88internal18IncrementalMarking36ScheduleBytesToMarkBasedOnAllocationEv
	.section	.rodata._ZN2v88internal18IncrementalMarking28FetchBytesMarkedConcurrentlyEv.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"[IncrementalMarking] Marked %zuKB on background threads\n"
	.section	.text._ZN2v88internal18IncrementalMarking28FetchBytesMarkedConcurrentlyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking28FetchBytesMarkedConcurrentlyEv
	.type	_ZN2v88internal18IncrementalMarking28FetchBytesMarkedConcurrentlyEv, @function
_ZN2v88internal18IncrementalMarking28FetchBytesMarkedConcurrentlyEv:
.LFB22426:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal23FLAG_concurrent_markingE(%rip)
	jne	.L1528
	ret
	.p2align 4,,10
	.p2align 3
.L1528:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	2072(%rax), %rdi
	call	_ZN2v88internal17ConcurrentMarking16TotalMarkedBytesEv@PLT
	movq	72(%rbx), %rdx
	cmpq	%rax, %rdx
	jb	.L1529
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	jne	.L1530
.L1519:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1529:
	.cfi_restore_state
	movq	48(%rbx), %rcx
	movq	%rax, 72(%rbx)
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	addq	%rax, %rdx
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movq	%rdx, 48(%rbx)
	je	.L1519
.L1530:
	movq	(%rbx), %rbx
	movq	2072(%rbx), %rdi
	call	_ZN2v88internal17ConcurrentMarking16TotalMarkedBytesEv@PLT
	addq	$8, %rsp
	leaq	-37592(%rbx), %rdi
	leaq	.LC26(%rip), %rsi
	shrq	$10, %rax
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	%rax, %rdx
	xorl	%eax, %eax
	jmp	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	.cfi_endproc
.LFE22426:
	.size	_ZN2v88internal18IncrementalMarking28FetchBytesMarkedConcurrentlyEv, .-_ZN2v88internal18IncrementalMarking28FetchBytesMarkedConcurrentlyEv
	.section	.rodata._ZN2v88internal18IncrementalMarking22ComputeStepSizeInBytesENS0_10StepOriginE.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"[IncrementalMarking] Marker is %zuKB behind schedule\n"
	.align 8
.LC28:
	.string	"[IncrementalMarking] Marker is %zuKB ahead of schedule\n"
	.section	.text._ZN2v88internal18IncrementalMarking22ComputeStepSizeInBytesENS0_10StepOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking22ComputeStepSizeInBytesENS0_10StepOriginE
	.type	_ZN2v88internal18IncrementalMarking22ComputeStepSizeInBytesENS0_10StepOriginE, @function
_ZN2v88internal18IncrementalMarking22ComputeStepSizeInBytesENS0_10StepOriginE:
.LFB22427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, _ZN2v88internal23FLAG_concurrent_markingE(%rip)
	jne	.L1543
.L1532:
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movq	56(%rbx), %rcx
	movq	48(%rbx), %rdx
	je	.L1535
	movq	(%rbx), %rax
	leaq	-37592(%rax), %rdi
	cmpq	%rdx, %rcx
	ja	.L1544
	subq	%rcx, %rdx
	leaq	.LC28(%rip), %rsi
	xorl	%eax, %eax
	shrq	$10, %rdx
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
.L1542:
	movq	48(%rbx), %rdx
	movq	56(%rbx), %rcx
.L1535:
	testl	%r12d, %r12d
	jne	.L1539
	leaq	1048576(%rdx), %rsi
	movl	$1048576, %edi
.L1537:
	movq	%rcx, %rax
	subq	%rdx, %rax
	movl	$0, %edx
	subq	%rdi, %rax
	cmpq	%rsi, %rcx
	cmovb	%rdx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1539:
	.cfi_restore_state
	movq	%rdx, %rsi
	xorl	%edi, %edi
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1544:
	subq	%rdx, %rcx
	leaq	.LC27(%rip), %rsi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	shrq	$10, %rdx
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movq	48(%rbx), %rdx
	movq	56(%rbx), %rcx
	jmp	.L1535
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	(%rdi), %rax
	movq	2072(%rax), %rdi
	call	_ZN2v88internal17ConcurrentMarking16TotalMarkedBytesEv@PLT
	movq	72(%rbx), %rdx
	cmpq	%rdx, %rax
	jbe	.L1533
	movq	48(%rbx), %rcx
	movq	%rax, 72(%rbx)
	addq	%rax, %rcx
	subq	%rdx, %rcx
	movq	%rcx, 48(%rbx)
.L1533:
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	je	.L1542
	movq	(%rbx), %r13
	movq	2072(%r13), %rdi
	call	_ZN2v88internal17ConcurrentMarking16TotalMarkedBytesEv@PLT
	leaq	-37592(%r13), %rdi
	leaq	.LC26(%rip), %rsi
	shrq	$10, %rax
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L1532
	.cfi_endproc
.LFE22427:
	.size	_ZN2v88internal18IncrementalMarking22ComputeStepSizeInBytesENS0_10StepOriginE, .-_ZN2v88internal18IncrementalMarking22ComputeStepSizeInBytesENS0_10StepOriginE
	.section	.text._ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_,"axG",@progbits,_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	.type	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_, @function
_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_:
.LFB23141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rbx), %r14
	movq	8(%r14), %r12
	cmpq	$64, %r12
	je	.L1549
	leaq	1(%r12), %rax
	movq	%rax, 8(%r14)
	movq	%rdx, 16(%r14,%r12,8)
.L1547:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1549:
	.cfi_restore_state
	leaq	640(%rdi), %r15
	movq	%rdi, %r13
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r14)
	movq	%r14, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	%r12, %rcx
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rbx)
	movq	$1, 8(%rsi)
	movq	%rdx, 16(%rsi)
	jmp	.L1547
	.cfi_endproc
.LFE23141:
	.size	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_, .-_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	.section	.text._ZN2v88internal18IncrementalMarking10RetainMapsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking10RetainMapsEv
	.type	_ZN2v88internal18IncrementalMarking10RetainMapsEv, @function
_ZN2v88internal18IncrementalMarking10RetainMapsEv:
.LFB22382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	%rdi, -64(%rbp)
	movzbl	2756(%rdx), %eax
	andl	$1, %eax
	jne	.L1551
	movl	_ZN2v88internal25FLAG_retain_maps_for_n_gcE(%rip), %eax
	testl	%eax, %eax
	sete	%al
.L1551:
	movq	-32880(%rdx), %rbx
	xorl	$1, %eax
	movl	244(%rdx), %r10d
	xorl	%r12d, %r12d
	movb	%al, -49(%rbp)
	movslq	19(%rbx), %rcx
	leaq	-1(%rbx), %r11
	addq	$23, %rbx
	movl	%ecx, %r14d
	testq	%rcx, %rcx
	jg	.L1564
	jmp	.L1550
	.p2align 4,,10
	.p2align 3
.L1554:
	addl	$2, %r12d
	addq	$16, %rbx
	cmpl	%r14d, %r12d
	jge	.L1550
.L1564:
	movq	(%rbx), %r8
	movq	%r8, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	jne	.L1554
	cmpl	$3, %r8d
	je	.L1554
	leal	32(,%r12,8), %r13d
	movslq	%r13d, %r13
	addq	%r11, %r13
	movq	0(%r13), %r15
	sarq	$32, %r15
	cmpl	%r12d, %r10d
	jg	.L1559
	cmpb	$0, -49(%rbp)
	je	.L1559
	movq	%r8, %r9
	andq	$-262144, %r8
	movl	$1, %edx
	andq	$-3, %r9
	movl	%r9d, %eax
	subl	%r8d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	leaq	0(,%rax,4), %rsi
	sall	%cl, %edx
	movq	%rsi, -72(%rbp)
	movl	%edx, %ecx
	movq	16(%r8), %rdx
	movl	(%rdx,%rax,4), %eax
	testl	%ecx, %eax
	je	.L1577
.L1559:
	movl	_ZN2v88internal25FLAG_retain_maps_for_n_gcE(%rip), %eax
.L1558:
	cmpl	%r15d, %eax
	je	.L1554
	salq	$32, %rax
	addl	$2, %r12d
	addq	$16, %rbx
	movq	%rax, 0(%r13)
	cmpl	%r14d, %r12d
	jl	.L1564
.L1550:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1577:
	.cfi_restore_state
	movq	-64(%rbp), %rdi
	movq	%r9, %rsi
	movl	%r15d, %edx
	movq	%r11, -96(%rbp)
	movl	%r10d, -56(%rbp)
	movl	%ecx, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal18IncrementalMarking15ShouldRetainMapENS0_3MapEi
	movq	-80(%rbp), %r9
	movl	-88(%rbp), %ecx
	testb	%al, %al
	movl	-56(%rbp), %r10d
	movq	-96(%rbp), %r11
	jne	.L1578
.L1565:
	testq	%r15, %r15
	jle	.L1554
	movq	23(%r9), %rax
	testb	$1, %al
	je	.L1554
	movq	%rax, %rcx
	andl	$262143, %eax
	andq	$-262144, %rcx
	movl	%eax, %edx
	shrl	$3, %eax
	movq	16(%rcx), %rcx
	shrl	$8, %edx
	movl	(%rcx,%rdx,4), %edx
	btl	%eax, %edx
	jc	.L1554
	leal	-1(%r15), %eax
	jmp	.L1558
.L1578:
	movq	-72(%rbp), %rsi
	addq	16(%r8), %rsi
	.p2align 4,,10
	.p2align 3
.L1561:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L1565
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L1561
	movq	-64(%rbp), %rax
	movq	%r9, %rdx
	xorl	%esi, %esi
	movq	%r11, -88(%rbp)
	movl	%r10d, -80(%rbp)
	movq	8(%rax), %rdi
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	movq	-72(%rbp), %r9
	movl	-80(%rbp), %r10d
	movq	-88(%rbp), %r11
	jmp	.L1565
	.cfi_endproc
.LFE22382:
	.size	_ZN2v88internal18IncrementalMarking10RetainMapsEv, .-_ZN2v88internal18IncrementalMarking10RetainMapsEv
	.section	.rodata._ZN2v88internal18IncrementalMarking21FinalizeIncrementallyEv.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"[IncrementalMarking] Finalize incrementally spent %.1f ms.\n"
	.section	.text._ZN2v88internal18IncrementalMarking21FinalizeIncrementallyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking21FinalizeIncrementallyEv
	.type	_ZN2v88internal18IncrementalMarking21FinalizeIncrementallyEv, @function
_ZN2v88internal18IncrementalMarking21FinalizeIncrementallyEv:
.LFB22383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18IncrementalMarking21FinalizeIncrementallyEvE28trace_event_unique_atomic496(%rip), %r12
	testq	%r12, %r12
	je	.L1601
.L1581:
	movq	$0, -176(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L1602
.L1583:
	movq	(%rbx), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	(%rbx), %rdi
	movl	$5, %edx
	leaq	16+_ZTVN2v88internal36IncrementalMarkingRootMarkingVisitorE(%rip), %rax
	leaq	-192(%rbp), %rsi
	movq	%rax, -192(%rbp)
	movq	%rdi, -184(%rbp)
	movsd	%xmm0, -200(%rbp)
	call	_ZN2v88internal4Heap18IterateStrongRootsEPNS0_11RootVisitorENS0_9VisitModeE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal18IncrementalMarking10RetainMapsEv
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movb	$1, 88(%rbx)
	jne	.L1603
.L1587:
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1604
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1601:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1605
.L1582:
	movq	%r12, _ZZN2v88internal18IncrementalMarking21FinalizeIncrementallyEvE28trace_event_unique_atomic496(%rip)
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L1603:
	movq	(%rbx), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	(%rbx), %rdi
	movl	$1, %eax
	subsd	-200(%rbp), %xmm0
	leaq	.LC29(%rip), %rsi
	subq	$37592, %rdi
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1602:
	movl	$6, %edi
	xorl	%r15d, %r15d
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r14
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1606
.L1584:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1585
	movq	(%rdi), %rax
	call	*8(%rax)
.L1585:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1586
	movq	(%rdi), %rax
	call	*8(%rax)
.L1586:
	movl	$6, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%r12, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%r15, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1605:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1606:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r14, %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1584
.L1604:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22383:
	.size	_ZN2v88internal18IncrementalMarking21FinalizeIncrementallyEv, .-_ZN2v88internal18IncrementalMarking21FinalizeIncrementallyEv
	.section	.text._ZN2v88internal8WorklistINS0_18EphemeronHashTableELi64EE4PushEiS2_,"axG",@progbits,_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi64EE4PushEiS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi64EE4PushEiS2_
	.type	_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi64EE4PushEiS2_, @function
_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi64EE4PushEiS2_:
.LFB23151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rbx), %r14
	movq	8(%r14), %r12
	cmpq	$64, %r12
	je	.L1611
	leaq	1(%r12), %rax
	movq	%rax, 8(%r14)
	movq	%rdx, 16(%r14,%r12,8)
.L1609:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1611:
	.cfi_restore_state
	leaq	640(%rdi), %r15
	movq	%rdi, %r13
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r14)
	movq	%r14, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	%r12, %rcx
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rbx)
	movq	$1, 8(%rsi)
	movq	%rdx, 16(%rsi)
	jmp	.L1609
	.cfi_endproc
.LFE23151:
	.size	_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi64EE4PushEiS2_, .-_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi64EE4PushEiS2_
	.section	.text._ZN2v88internal8WorklistINS0_9EphemeronELi64EE4PushEiS2_,"axG",@progbits,_ZN2v88internal8WorklistINS0_9EphemeronELi64EE4PushEiS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE4PushEiS2_
	.type	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE4PushEiS2_, @function
_ZN2v88internal8WorklistINS0_9EphemeronELi64EE4PushEiS2_:
.LFB23152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rbx), %r13
	movq	%rdx, -40(%rbp)
	movq	%rcx, -48(%rbp)
	movq	8(%r13), %rax
	cmpq	$64, %rax
	je	.L1616
	addq	$1, %rax
	movq	%rdx, %xmm0
	movq	%rax, 8(%r13)
	movhps	-48(%rbp), %xmm0
	salq	$4, %rax
	movups	%xmm0, 0(%r13,%rax)
.L1614:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1616:
	.cfi_restore_state
	leaq	640(%rdi), %r14
	movq	%rdi, %r12
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r12), %rax
	movq	%r14, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 680(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	-40(%rbp), %rax
	movq	%rdx, (%rbx)
	movq	$1, 8(%rdx)
	movq	%rax, 16(%rdx)
	movq	-48(%rbp), %rax
	movq	%rax, 24(%rdx)
	jmp	.L1614
	.cfi_endproc
.LFE23152:
	.size	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE4PushEiS2_, .-_ZN2v88internal8WorklistINS0_9EphemeronELi64EE4PushEiS2_
	.section	.text._ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_,"axG",@progbits,_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_
	.type	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_, @function
_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_:
.LFB23153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	(%rsi,%rsi,4), %r12
	pushq	%rbx
	salq	$4, %r12
	addq	%rdi, %r12
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%r12), %rbx
	movq	8(%rbx), %rax
	cmpq	$64, %rax
	je	.L1629
	addq	$1, %rax
	movq	%rax, 8(%rbx)
	salq	$4, %rax
	addq	%rbx, %rax
	movq	%rdx, (%rax)
	movq	%rcx, 8(%rax)
.L1622:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1629:
	.cfi_restore_state
	movq	%rdi, %r15
	leaq	640(%rdi), %rdi
	movq	%rdi, -56(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r15), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 680(%r15)
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 8(%rax)
	leaq	16(%rax), %rdx
	leaq	1040(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L1619:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1619
	movq	8(%rax), %rdx
	movq	%rax, (%r12)
	cmpq	$64, %rdx
	je	.L1622
	addq	$1, %rdx
	movq	%rdx, 8(%rax)
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%r13, (%rax)
	movq	%r14, 8(%rax)
	jmp	.L1622
	.cfi_endproc
.LFE23153:
	.size	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_, .-_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_
	.section	.text._ZN2v88internal8WorklistINS0_9JSWeakRefELi64EE4PushEiS2_,"axG",@progbits,_ZN2v88internal8WorklistINS0_9JSWeakRefELi64EE4PushEiS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_9JSWeakRefELi64EE4PushEiS2_
	.type	_ZN2v88internal8WorklistINS0_9JSWeakRefELi64EE4PushEiS2_, @function
_ZN2v88internal8WorklistINS0_9JSWeakRefELi64EE4PushEiS2_:
.LFB23158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rbx), %r14
	movq	8(%r14), %r12
	cmpq	$64, %r12
	je	.L1634
	leaq	1(%r12), %rax
	movq	%rax, 8(%r14)
	movq	%rdx, 16(%r14,%r12,8)
.L1632:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1634:
	.cfi_restore_state
	leaq	640(%rdi), %r15
	movq	%rdi, %r13
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r14)
	movq	%r14, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	%r12, %rcx
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rbx)
	movq	$1, 8(%rsi)
	movq	%rdx, 16(%rsi)
	jmp	.L1632
	.cfi_endproc
.LFE23158:
	.size	_ZN2v88internal8WorklistINS0_9JSWeakRefELi64EE4PushEiS2_, .-_ZN2v88internal8WorklistINS0_9JSWeakRefELi64EE4PushEiS2_
	.section	.text._ZN2v88internal8WorklistINS0_8WeakCellELi64EE4PushEiS2_,"axG",@progbits,_ZN2v88internal8WorklistINS0_8WeakCellELi64EE4PushEiS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_8WeakCellELi64EE4PushEiS2_
	.type	_ZN2v88internal8WorklistINS0_8WeakCellELi64EE4PushEiS2_, @function
_ZN2v88internal8WorklistINS0_8WeakCellELi64EE4PushEiS2_:
.LFB23159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rbx), %r14
	movq	8(%r14), %r12
	cmpq	$64, %r12
	je	.L1639
	leaq	1(%r12), %rax
	movq	%rax, 8(%r14)
	movq	%rdx, 16(%r14,%r12,8)
.L1637:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1639:
	.cfi_restore_state
	leaq	640(%rdi), %r15
	movq	%rdi, %r13
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r14)
	movq	%r14, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	%r12, %rcx
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rbx)
	movq	$1, 8(%rsi)
	movq	%rdx, 16(%rsi)
	jmp	.L1637
	.cfi_endproc
.LFE23159:
	.size	_ZN2v88internal8WorklistINS0_8WeakCellELi64EE4PushEiS2_, .-_ZN2v88internal8WorklistINS0_8WeakCellELi64EE4PushEiS2_
	.section	.rodata._ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm.str1.1,"aMS",@progbits,1
.LC30:
	.string	"NewArray"
	.section	.text._ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm,"axG",@progbits,_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm,comdat
	.p2align 4
	.weak	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	.type	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm, @function
_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm:
.LFB24892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	112(%rdi), %r12
	testq	%r12, %r12
	je	.L1654
.L1642:
	subq	%rbx, %rsi
	movl	%esi, %edx
	shrq	$18, %rsi
	andl	$262143, %edx
	leaq	(%rsi,%rsi,2), %rcx
	movl	%edx, %ebx
	movl	%edx, %r13d
	sarl	$13, %edx
	salq	$7, %rcx
	movslq	%edx, %rdx
	sarl	$8, %ebx
	leaq	(%rcx,%rdx,8), %rax
	sarl	$3, %r13d
	andl	$31, %ebx
	addq	%rax, %r12
	andl	$31, %r13d
	movq	(%r12), %r8
	testq	%r8, %r8
	je	.L1655
.L1644:
	movl	%r13d, %ecx
	movl	$1, %edx
	movslq	%ebx, %rbx
	sall	%cl, %edx
	leaq	(%r8,%rbx,4), %rcx
	movl	(%rcx), %eax
	testl	%eax, %edx
	je	.L1647
.L1640:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1656:
	.cfi_restore_state
	movl	%edx, %edi
	movl	%esi, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %esi
	je	.L1640
.L1647:
	movl	(%rcx), %esi
	movl	%edx, %eax
	andl	%esi, %eax
	cmpl	%eax, %edx
	jne	.L1656
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1654:
	.cfi_restore_state
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1655:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1657
.L1645:
	leaq	8(%r8), %rdi
	movq	%r8, %rcx
	xorl	%eax, %eax
	movq	$0, (%r8)
	andq	$-8, %rdi
	movq	$0, 120(%r8)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r8, (%r12)
	testq	%rax, %rax
	je	.L1644
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	(%r12), %r8
	jmp	.L1644
.L1657:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L1645
	leaq	.LC30(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE24892:
	.size	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm, .-_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	.section	.text._ZN2v88internal18IncrementalMarking15RecordWriteSlowENS0_10HeapObjectENS0_18FullHeapObjectSlotES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking15RecordWriteSlowENS0_10HeapObjectENS0_18FullHeapObjectSlotES2_
	.type	_ZN2v88internal18IncrementalMarking15RecordWriteSlowENS0_10HeapObjectENS0_18FullHeapObjectSlotES2_, @function
_ZN2v88internal18IncrementalMarking15RecordWriteSlowENS0_10HeapObjectENS0_18FullHeapObjectSlotES2_:
.LFB22353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	andl	$262143, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	movl	%eax, %ecx
	shrl	$8, %eax
	pushq	%r13
	andq	$-262144, %r14
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrl	$3, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$1, %esi
	pushq	%rbx
	sall	%cl, %esi
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%r14), %rdx
	leaq	(%rdx,%rax,4), %rdi
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1693:
	movl	%ecx, %r8d
	movl	%ecx, %eax
	orl	%esi, %r8d
	lock cmpxchgl	%r8d, (%rdi)
	cmpl	%eax, %ecx
	je	.L1692
.L1661:
	movl	(%rdi), %ecx
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%esi, %eax
	jne	.L1693
.L1660:
	cmpb	$0, 84(%rbx)
	je	.L1658
	testq	%r12, %r12
	jne	.L1694
.L1658:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1692:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	movq	(%rdx), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L1695
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%r15, 16(%rax,%rcx,8)
.L1663:
	cmpl	$3, 80(%rbx)
	jne	.L1660
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movl	$2, 80(%rbx)
	je	.L1660
	movq	(%rbx), %rax
	leaq	.LC6(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	8(%r14), %rax
	testb	$64, %al
	je	.L1658
	movq	%r13, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L1669
	testb	$-128, %ah
	je	.L1658
.L1669:
	addq	$40, %rsp
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
.L1695:
	.cfi_restore_state
	leaq	640(%rdx), %rdi
	movq	%rcx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	680(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 680(%rdx)
	movq	%rdx, -56(%rbp)
	movq	-64(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rdx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L1663
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%r15, 16(%rsi,%rax,8)
	jmp	.L1663
	.cfi_endproc
.LFE22353:
	.size	_ZN2v88internal18IncrementalMarking15RecordWriteSlowENS0_10HeapObjectENS0_18FullHeapObjectSlotES2_, .-_ZN2v88internal18IncrementalMarking15RecordWriteSlowENS0_10HeapObjectENS0_18FullHeapObjectSlotES2_
	.section	.text._ZN2v88internal18IncrementalMarking19RecordWriteFromCodeEmmPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking19RecordWriteFromCodeEmmPNS0_7IsolateE
	.type	_ZN2v88internal18IncrementalMarking19RecordWriteFromCodeEmmPNS0_7IsolateE, @function
_ZN2v88internal18IncrementalMarking19RecordWriteFromCodeEmmPNS0_7IsolateE:
.LFB22354:
	.cfi_startproc
	endbr64
	movq	39656(%rdx), %r8
	movq	(%rsi), %rcx
	cmpl	$1, 80(%r8)
	jg	.L1702
.L1699:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1702:
	testb	$1, %cl
	je	.L1699
	cmpl	$3, %ecx
	je	.L1699
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	andq	$-3, %rcx
	movq	%rdi, %rsi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal18IncrementalMarking15RecordWriteSlowENS0_10HeapObjectENS0_18FullHeapObjectSlotES2_
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22354:
	.size	_ZN2v88internal18IncrementalMarking19RecordWriteFromCodeEmmPNS0_7IsolateE, .-_ZN2v88internal18IncrementalMarking19RecordWriteFromCodeEmmPNS0_7IsolateE
	.section	.text._ZN2v88internal18IncrementalMarking16VisitDescriptorsENS0_10HeapObjectENS0_15DescriptorArrayEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking16VisitDescriptorsENS0_10HeapObjectENS0_15DescriptorArrayEi
	.type	_ZN2v88internal18IncrementalMarking16VisitDescriptorsENS0_10HeapObjectENS0_15DescriptorArrayEi, @function
_ZN2v88internal18IncrementalMarking16VisitDescriptorsENS0_10HeapObjectENS0_15DescriptorArrayEi:
.LFB22406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	andq	$-262144, %r15
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	2016(%rax), %rbx
	movl	%edx, %eax
	movl	$1, %edx
	andl	$262143, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	movl	9996(%rbx), %r14d
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r15), %rdx
	leaq	(%rdx,%rax,4), %rsi
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1704:
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1708
.L1707:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	jne	.L1704
.L1708:
	leaq	-1(%r12), %rdx
	movl	$1, %eax
	movq	%r12, -64(%rbp)
	subl	%r15d, %edx
	movl	%edx, %ecx
	shrl	$8, %edx
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movq	16(%r15), %rax
	leaq	(%rax,%rdx,4), %rsi
	movl	(%rsi), %eax
	testl	%ecx, %eax
	jne	.L1705
.L1768:
	leaq	-64(%rbp), %r8
.L1713:
	movq	%r12, -64(%rbp)
	movswl	%r13w, %r12d
	movl	%r14d, %esi
	movq	%r8, %rdi
	movl	%r12d, %edx
	call	_ZN2v88internal15DescriptorArray31UpdateNumberOfMarkedDescriptorsEjs@PLT
	cmpw	%ax, %r13w
	jg	.L1769
.L1703:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1770
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1705:
	.cfi_restore_state
	addl	%ecx, %ecx
	je	.L1771
	.p2align 4,,10
	.p2align 3
.L1710:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L1768
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L1710
	movq	-64(%rbp), %rax
	leaq	-64(%rbp), %r8
	movq	%r8, %rdi
	movq	-1(%rax), %rsi
	movq	%r8, -72(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	15(%r12), %rsi
	movq	-72(%rbp), %r8
	cltq
	addq	%rax, 96(%r15)
	leaq	23(%r12), %rax
	cmpq	%rax, %rsi
	jnb	.L1713
	movq	15(%r12), %r9
	testb	$1, %r9b
	je	.L1713
	movq	%r9, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L1714
.L1719:
	movl	%r9d, %eax
	movl	$1, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L1716:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L1713
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L1716
	leaq	104(%rbx), %rdi
	movq	%r9, %rdx
	xorl	%esi, %esi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	movq	-72(%rbp), %r8
	jmp	.L1713
	.p2align 4,,10
	.p2align 3
.L1769:
	cwtl
	movq	-64(%rbp), %rsi
	leal	3(%r12,%r12,2), %r8d
	leal	3(%rax,%rax,2), %r14d
	sall	$3, %r8d
	sall	$3, %r14d
	leaq	-1(%rsi), %rdx
	movslq	%r8d, %r8
	movq	%rsi, -72(%rbp)
	movslq	%r14d, %r14
	leaq	(%r8,%rdx), %r13
	addq	%rdx, %r14
	cmpq	%r13, %r14
	jnb	.L1703
	movl	$1, %r12d
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1728:
	addq	$8, %r14
	cmpq	%r14, %r13
	jbe	.L1703
.L1736:
	movq	(%r14), %r15
	movq	%r15, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L1772
	cmpq	$3, %rax
	jne	.L1728
	cmpl	$3, %r15d
	je	.L1728
	movq	%r15, %rdx
	movq	%r15, %rcx
	andq	$-262144, %rdx
	andq	$-3, %rcx
	subl	%edx, %ecx
	movq	16(%rdx), %rsi
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	movl	(%rsi,%rax,4), %eax
	movl	%r12d, %esi
	sall	%cl, %esi
	testl	%eax, %esi
	je	.L1734
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L1728
	movq	-72(%rbp), %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L1735
	testb	$-128, %ah
	je	.L1728
.L1735:
	movq	%r14, %rsi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L1728
	.p2align 4,,10
	.p2align 3
.L1772:
	movq	%r15, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L1773
.L1726:
	movl	%r15d, %eax
	movl	%r12d, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L1732:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L1728
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L1732
	leaq	104(%rbx), %rdi
	movq	%r15, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L1728
	.p2align 4,,10
	.p2align 3
.L1734:
	movq	-72(%rbp), %xmm0
	movq	%r14, %xmm1
	leaq	5672(%rbx), %rdi
	xorl	%esi, %esi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rcx
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_
	jmp	.L1728
	.p2align 4,,10
	.p2align 3
.L1771:
	addq	$4, %rsi
	movl	$1, %ecx
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1773:
	movq	-72(%rbp), %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L1729
	testb	$-128, %ah
	je	.L1726
.L1729:
	movq	%r14, %rsi
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-96(%rbp), %rdx
	jmp	.L1726
.L1714:
	movq	8(%r15), %rax
	testb	$88, %al
	je	.L1721
	testb	$-128, %ah
	je	.L1719
.L1721:
	movq	%r15, %rdi
	movq	%r8, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-72(%rbp), %r9
	movq	-96(%rbp), %rdx
	movq	-80(%rbp), %r8
	jmp	.L1719
.L1770:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22406:
	.size	_ZN2v88internal18IncrementalMarking16VisitDescriptorsENS0_10HeapObjectENS0_15DescriptorArrayEi, .-_ZN2v88internal18IncrementalMarking16VisitDescriptorsENS0_10HeapObjectENS0_15DescriptorArrayEi
	.section	.text._ZN2v88internal8WorklistINS0_15TransitionArrayELi64EE4PushEiS2_,"axG",@progbits,_ZN2v88internal8WorklistINS0_15TransitionArrayELi64EE4PushEiS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_15TransitionArrayELi64EE4PushEiS2_
	.type	_ZN2v88internal8WorklistINS0_15TransitionArrayELi64EE4PushEiS2_, @function
_ZN2v88internal8WorklistINS0_15TransitionArrayELi64EE4PushEiS2_:
.LFB24893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rbx), %r14
	movq	8(%r14), %r12
	cmpq	$64, %r12
	je	.L1778
	leaq	1(%r12), %rax
	movq	%rax, 8(%r14)
	movq	%rdx, 16(%r14,%r12,8)
.L1776:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1778:
	.cfi_restore_state
	leaq	640(%rdi), %r15
	movq	%rdi, %r13
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r14)
	movq	%r14, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	%r12, %rcx
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rbx)
	movq	$1, 8(%rsi)
	movq	%rdx, 16(%rsi)
	jmp	.L1776
	.cfi_endproc
.LFE24893:
	.size	_ZN2v88internal8WorklistINS0_15TransitionArrayELi64EE4PushEiS2_, .-_ZN2v88internal8WorklistINS0_15TransitionArrayELi64EE4PushEiS2_
	.section	.text._ZN2v88internal8WorklistINS0_18SharedFunctionInfoELi64EE4PushEiS2_,"axG",@progbits,_ZN2v88internal8WorklistINS0_18SharedFunctionInfoELi64EE4PushEiS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_18SharedFunctionInfoELi64EE4PushEiS2_
	.type	_ZN2v88internal8WorklistINS0_18SharedFunctionInfoELi64EE4PushEiS2_, @function
_ZN2v88internal8WorklistINS0_18SharedFunctionInfoELi64EE4PushEiS2_:
.LFB24894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rbx), %r14
	movq	8(%r14), %r12
	cmpq	$64, %r12
	je	.L1783
	leaq	1(%r12), %rax
	movq	%rax, 8(%r14)
	movq	%rdx, 16(%r14,%r12,8)
.L1781:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1783:
	.cfi_restore_state
	leaq	640(%rdi), %r15
	movq	%rdi, %r13
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r14)
	movq	%r14, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	%r12, %rcx
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rbx)
	movq	$1, 8(%rsi)
	movq	%rdx, 16(%rsi)
	jmp	.L1781
	.cfi_endproc
.LFE24894:
	.size	_ZN2v88internal8WorklistINS0_18SharedFunctionInfoELi64EE4PushEiS2_, .-_ZN2v88internal8WorklistINS0_18SharedFunctionInfoELi64EE4PushEiS2_
	.section	.text._ZN2v88internal8WorklistINS0_10JSFunctionELi64EE4PushEiS2_,"axG",@progbits,_ZN2v88internal8WorklistINS0_10JSFunctionELi64EE4PushEiS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_10JSFunctionELi64EE4PushEiS2_
	.type	_ZN2v88internal8WorklistINS0_10JSFunctionELi64EE4PushEiS2_, @function
_ZN2v88internal8WorklistINS0_10JSFunctionELi64EE4PushEiS2_:
.LFB24895:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rbx), %r14
	movq	8(%r14), %r12
	cmpq	$64, %r12
	je	.L1788
	leaq	1(%r12), %rax
	movq	%rax, 8(%r14)
	movq	%rdx, 16(%r14,%r12,8)
.L1786:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1788:
	.cfi_restore_state
	leaq	640(%rdi), %r15
	movq	%rdi, %r13
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r14)
	movq	%r14, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	%r12, %rcx
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rbx)
	movq	$1, 8(%rsi)
	movq	%rdx, 16(%rsi)
	jmp	.L1786
	.cfi_endproc
.LFE24895:
	.size	_ZN2v88internal8WorklistINS0_10JSFunctionELi64EE4PushEiS2_, .-_ZN2v88internal8WorklistINS0_10JSFunctionELi64EE4PushEiS2_
	.section	.text._ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_,"axG",@progbits,_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	.type	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_, @function
_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_:
.LFB27915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdi), %r8
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	(%rdx,%r8), %r12
	addq	%rsi, %r8
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	cmpq	%r12, %r8
	jnb	.L1789
	movq	%rdi, %r13
	movq	%rcx, %r14
	movl	$1, %r15d
	jmp	.L1790
	.p2align 4,,10
	.p2align 3
.L1792:
	addq	$8, %r8
	cmpq	%r8, %r12
	jbe	.L1789
.L1790:
	movq	(%r8), %rbx
	testb	$1, %bl
	je	.L1792
	movq	%rbx, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L1821
.L1794:
	movl	%ebx, %eax
	movl	%r15d, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L1798:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L1792
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L1798
	movq	16(%r14), %rdx
	movq	104(%rdx), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L1822
	leaq	1(%rcx), %rdx
	addq	$8, %r8
	movq	%rdx, 8(%rax)
	movq	%rbx, 16(%rax,%rcx,8)
	cmpq	%r8, %r12
	ja	.L1790
.L1789:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1821:
	.cfi_restore_state
	movq	%r13, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L1795
	testb	$-128, %ah
	je	.L1794
.L1795:
	movq	%r8, %rsi
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %r8
	jmp	.L1794
.L1822:
	leaq	744(%rdx), %rdi
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	784(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 784(%rdx)
	movq	%rdx, -56(%rbp)
	movq	-64(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 104(%rdx)
	movq	-88(%rbp), %r8
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L1792
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%rbx, 16(%rsi,%rax,8)
	jmp	.L1792
	.cfi_endproc
.LFE27915:
	.size	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_, .-_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	.section	.text._ZN2v88internal18WasmInstanceObject14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_,"axG",@progbits,_ZN2v88internal18WasmInstanceObject14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_,comdat
	.p2align 4
	.weak	_ZN2v88internal18WasmInstanceObject14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_
	.type	_ZN2v88internal18WasmInstanceObject14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_, @function
_ZN2v88internal18WasmInstanceObject14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_:
.LFB27523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	7(%rsi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -112(%rbp)
	movl	%edx, -120(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	cmpq	%r14, %rax
	jbe	.L1838
	leaq	15(%rsi), %rdx
	movl	$1, %r13d
	movq	%rsi, %r15
	cmpq	%rdx, %rax
	sbbq	%r12, %r12
	notq	%r12
	andl	$8, %r12d
	addq	%rdx, %r12
	jmp	.L1837
	.p2align 4,,10
	.p2align 3
.L1827:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1953
.L1837:
	movq	(%r14), %rbx
	testb	$1, %bl
	je	.L1827
	movq	%rbx, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L1954
.L1829:
	movl	%ebx, %eax
	movl	%r13d, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
	.p2align 4,,10
	.p2align 3
.L1833:
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	je	.L1827
	movl	%esi, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	jne	.L1833
	movq	-104(%rbp), %rax
	movq	16(%rax), %rdx
	movq	104(%rdx), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L1955
	leaq	1(%rcx), %rdx
	addq	$8, %r14
	movq	%rdx, 8(%rax)
	movq	%rbx, 16(%rax,%rcx,8)
	cmpq	%r14, %r12
	jne	.L1837
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	%r15, %r8
.L1838:
	leaq	-1(%r8), %r13
	leaq	_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE(%rip), %rbx
	movq	%r8, %r11
	movq	%r8, -128(%rbp)
	leaq	30(%rbx), %r15
	andq	$-262144, %r11
	movq	%r13, %r14
	jmp	.L1825
	.p2align 4,,10
	.p2align 3
.L1840:
	addq	$2, %rbx
	cmpq	%r15, %rbx
	je	.L1956
.L1825:
	movzwl	(%rbx), %r12d
	addq	%r14, %r12
	movq	(%r12), %r13
	testb	$1, %r13b
	je	.L1840
	movq	%r13, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L1841
.L1844:
	movl	%r13d, %eax
	movl	$1, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L1842:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L1840
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L1842
	movq	-104(%rbp), %rax
	movq	16(%rax), %rdx
	movq	104(%rdx), %rax
	movq	8(%rax), %r12
	cmpq	$64, %r12
	je	.L1957
	leaq	1(%r12), %rdx
	addq	$2, %rbx
	movq	%rdx, 8(%rax)
	movq	%r13, 16(%rax,%r12,8)
	cmpq	%r15, %rbx
	jne	.L1825
	.p2align 4,,10
	.p2align 3
.L1956:
	movq	-112(%rbp), %rax
	movq	-128(%rbp), %r8
	movq	%r14, %r13
	movq	47(%rax), %rax
	testq	%rax, %rax
	je	.L1958
	movq	-112(%rbp), %rbx
	movq	$0, -72(%rbp)
	movb	$1, -80(%rbp)
	movl	$0, -76(%rbp)
	movq	47(%rbx), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L1857
	movzbl	8(%rbx), %eax
	movb	$0, -80(%rbp)
	sall	$3, %eax
	movl	%eax, -76(%rbp)
.L1857:
	movl	-120(%rbp), %eax
	movl	$280, %ebx
	leaq	-84(%rbp), %r15
	leaq	-80(%rbp), %r14
	andq	$-262144, %r8
	movq	%r8, -112(%rbp)
	cmpl	$280, %eax
	jle	.L1823
	movq	%r13, -120(%rbp)
	movl	%eax, %r13d
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L1952:
	movslq	-84(%rbp), %rbx
.L1860:
	cmpl	%ebx, %r13d
	jle	.L1823
.L1858:
	movq	%r15, %rcx
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L1952
	movslq	-84(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	%rdx, %rax
	addq	%rsi, %rbx
	addq	%rsi, %rdx
	cmpq	%rbx, %rdx
	jbe	.L1889
	movq	%r14, %r11
	movl	%r13d, %r9d
	movq	%r15, %r10
	movq	%rdx, %r14
	jmp	.L1879
	.p2align 4,,10
	.p2align 3
.L1863:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L1959
.L1879:
	movq	(%rbx), %r12
	testb	$1, %r12b
	je	.L1863
	movq	%r12, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testb	$64, %al
	jne	.L1864
.L1867:
	movl	%r12d, %eax
	movl	$1, %edx
	subl	%r13d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r13), %rdx
	leaq	(%rdx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L1865:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L1863
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L1865
	movq	-104(%rbp), %rax
	movq	16(%rax), %rdx
	movq	104(%rdx), %r15
	movq	8(%r15), %r13
	cmpq	$64, %r13
	je	.L1960
	leaq	1(%r13), %rax
	addq	$8, %rbx
	movq	%rax, 8(%r15)
	movq	%r12, 16(%r15,%r13,8)
	cmpq	%rbx, %r14
	ja	.L1879
	.p2align 4,,10
	.p2align 3
.L1959:
	movl	%r9d, %r13d
	movq	%r10, %r15
	movq	%r11, %r14
	jmp	.L1952
	.p2align 4,,10
	.p2align 3
.L1841:
	movq	8(%r11), %rax
	testb	$88, %al
	je	.L1843
	testb	$-128, %ah
	je	.L1844
.L1843:
	movq	112(%r11), %rax
	testq	%rax, %rax
	je	.L1961
.L1846:
	movq	%r12, %rcx
	andl	$262143, %r12d
	subq	%r11, %rcx
	movl	%r12d, %r9d
	movl	%r12d, %r10d
	sarl	$13, %r12d
	shrq	$18, %rcx
	movslq	%r12d, %r12
	sarl	$8, %r9d
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$3, %r10d
	andl	$31, %r9d
	salq	$7, %rcx
	andl	$31, %r10d
	leaq	(%rcx,%r12,8), %r12
	addq	%rax, %r12
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1962
.L1848:
	movslq	%r9d, %r9
	movl	$1, %edi
	movl	%r10d, %ecx
	leaq	(%rsi,%r9,4), %rsi
	sall	%cl, %edi
	movl	(%rsi), %eax
	testl	%eax, %edi
	jne	.L1844
	movl	(%rsi), %ecx
	movl	%edi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edi
	je	.L1844
	.p2align 4,,10
	.p2align 3
.L1963:
	movl	%edi, %r8d
	movl	%ecx, %eax
	orl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%rsi)
	cmpl	%eax, %ecx
	je	.L1844
	movl	(%rsi), %ecx
	movl	%edi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edi
	jne	.L1963
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1954:
	movq	%r15, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L1830
	testb	$-128, %ah
	je	.L1829
.L1830:
	movq	%r14, %rsi
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-128(%rbp), %rdx
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L1958:
	movq	-104(%rbp), %rcx
	movl	-120(%rbp), %edx
	movl	$280, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
.L1823:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1964
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1864:
	.cfi_restore_state
	movq	-112(%rbp), %rax
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L1866
	testb	$-128, %ah
	je	.L1867
.L1866:
	movq	-112(%rbp), %rax
	movq	112(%rax), %r15
	testq	%r15, %r15
	je	.L1965
.L1869:
	movq	%rbx, %rax
	movl	%ebx, %ecx
	subq	-112(%rbp), %rax
	andl	$262143, %ecx
	shrq	$18, %rax
	movl	%ecx, %edx
	movl	%ecx, %r8d
	leaq	(%rax,%rax,2), %rax
	sarl	$13, %ecx
	salq	$7, %rax
	movslq	%ecx, %rcx
	sarl	$8, %edx
	leaq	(%rax,%rcx,8), %rax
	sarl	$3, %r8d
	andl	$31, %edx
	addq	%rax, %r15
	andl	$31, %r8d
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	je	.L1966
.L1871:
	movl	%r8d, %ecx
	movl	$1, %eax
	movslq	%edx, %rdx
	sall	%cl, %eax
	leaq	(%rsi,%rdx,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L1867
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1967:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1867
.L1874:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L1967
	jmp	.L1867
	.p2align 4,,10
	.p2align 3
.L1961:
	movq	%r11, %rdi
	movq	%rdx, -144(%rbp)
	movq	%r11, -136(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %r11
	jmp	.L1846
	.p2align 4,,10
	.p2align 3
.L1962:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%r11, -160(%rbp)
	movl	%r10d, -152(%rbp)
	movl	%r9d, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-136(%rbp), %rdx
	movl	-144(%rbp), %r9d
	testq	%rax, %rax
	movl	-152(%rbp), %r10d
	movq	-160(%rbp), %r11
	movq	%rax, %rsi
	je	.L1968
.L1849:
	leaq	8(%rsi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	$0, (%rsi)
	andq	$-8, %rdi
	movq	$0, 120(%rsi)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%rsi, (%r12)
	testq	%rax, %rax
	je	.L1848
	movq	%rsi, %rdi
	movq	%r11, -160(%rbp)
	movl	%r10d, -152(%rbp)
	movl	%r9d, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZdaPv@PLT
	movq	(%r12), %rsi
	movq	-160(%rbp), %r11
	movl	-152(%rbp), %r10d
	movl	-144(%rbp), %r9d
	movq	-136(%rbp), %rdx
	jmp	.L1848
	.p2align 4,,10
	.p2align 3
.L1957:
	leaq	744(%rdx), %rdi
	movq	%r11, -160(%rbp)
	movq	%rax, -152(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-136(%rbp), %rdx
	movq	-152(%rbp), %rax
	movq	784(%rdx), %rcx
	movq	%rcx, (%rax)
	movq	%rax, 784(%rdx)
	movq	%rdx, -136(%rbp)
	movq	-144(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r12, %rcx
	movq	-136(%rbp), %rdx
	movq	-160(%rbp), %r11
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 104(%rdx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L1840
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%r13, 16(%rsi,%rax,8)
	jmp	.L1840
	.p2align 4,,10
	.p2align 3
.L1966:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%r11, -160(%rbp)
	movq	%r10, -152(%rbp)
	movl	%r9d, -144(%rbp)
	movl	%r8d, -136(%rbp)
	movl	%edx, -128(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-128(%rbp), %edx
	movl	-136(%rbp), %r8d
	testq	%rax, %rax
	movl	-144(%rbp), %r9d
	movq	%rax, %rsi
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %r11
	je	.L1969
.L1872:
	leaq	8(%rsi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	$0, (%rsi)
	andq	$-8, %rdi
	movq	$0, 120(%rsi)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%rsi, (%r15)
	testq	%rax, %rax
	je	.L1871
	movq	%rsi, %rdi
	movq	%r11, -160(%rbp)
	movq	%r10, -152(%rbp)
	movl	%r9d, -144(%rbp)
	movl	%r8d, -136(%rbp)
	movl	%edx, -128(%rbp)
	call	_ZdaPv@PLT
	movq	(%r15), %rsi
	movq	-160(%rbp), %r11
	movq	-152(%rbp), %r10
	movl	-144(%rbp), %r9d
	movl	-136(%rbp), %r8d
	movl	-128(%rbp), %edx
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L1965:
	movq	-112(%rbp), %rdi
	movq	%r11, -144(%rbp)
	movq	%r10, -136(%rbp)
	movl	%r9d, -128(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-144(%rbp), %r11
	movq	-136(%rbp), %r10
	movl	-128(%rbp), %r9d
	movq	%rax, %r15
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1960:
	leaq	744(%rdx), %rdi
	movq	%r11, -160(%rbp)
	movq	%r10, -152(%rbp)
	movl	%r9d, -144(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rdi, -136(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-128(%rbp), %rdx
	movq	784(%rdx), %rax
	movq	%rax, (%r15)
	movq	%r15, 784(%rdx)
	movq	%rdx, -128(%rbp)
	movq	-136(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r13, %rcx
	movq	-128(%rbp), %rdx
	movl	-144(%rbp), %r9d
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 104(%rdx)
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %r11
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L1863
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%r12, 16(%rsi,%rax,8)
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L1955:
	leaq	744(%rdx), %rdi
	movq	%rcx, -152(%rbp)
	movq	%rax, -144(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rdi, -136(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-128(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	784(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 784(%rdx)
	movq	%rdx, -128(%rbp)
	movq	-136(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-152(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 104(%rdx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L1827
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%rbx, 16(%rsi,%rax,8)
	jmp	.L1827
.L1889:
	movslq	%eax, %rbx
	jmp	.L1860
.L1969:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-128(%rbp), %edx
	movl	-136(%rbp), %r8d
	testq	%rax, %rax
	movl	-144(%rbp), %r9d
	movq	%rax, %rsi
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %r11
	jne	.L1872
.L1873:
	leaq	.LC30(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L1964:
	call	__stack_chk_fail@PLT
.L1968:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-136(%rbp), %rdx
	movl	-144(%rbp), %r9d
	testq	%rax, %rax
	movl	-152(%rbp), %r10d
	movq	-160(%rbp), %r11
	movq	%rax, %rsi
	jne	.L1849
	jmp	.L1873
	.cfi_endproc
.LFE27523:
	.size	_ZN2v88internal18WasmInstanceObject14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_, .-_ZN2v88internal18WasmInstanceObject14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_
	.section	.text._ZN2v88internal9JSWeakRef14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal9JSWeakRef14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_.constprop.0, @function
_ZN2v88internal9JSWeakRef14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_.constprop.0:
.LFB28986:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	leaq	7(%rsi), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	cmpq	%rdx, %rax
	jbe	.L1986
	leaq	15(%rsi), %rcx
	movl	%r14d, -112(%rbp)
	movq	%rsi, %r13
	movq	%rdi, %r14
	cmpq	%rcx, %rax
	movq	%rdx, %r12
	sbbq	%rbx, %rbx
	notq	%rbx
	andl	$8, %ebx
	leaq	(%rbx,%rcx), %rax
	movq	%rax, -104(%rbp)
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L1975:
	addq	$8, %r12
	cmpq	%r12, -104(%rbp)
	je	.L2059
.L1985:
	movq	(%r12), %rbx
	testb	$1, %bl
	je	.L1975
	movq	%rbx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	testb	$64, %al
	jne	.L2060
.L1977:
	movl	%ebx, %eax
	movl	$1, %edi
	subl	%r8d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edi
	movq	16(%r8), %rcx
	leaq	(%rcx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L1981:
	movl	(%rsi), %ecx
	movl	%edi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edi
	je	.L1975
	movl	%edi, %r8d
	movl	%ecx, %eax
	orl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%rsi)
	cmpl	%eax, %ecx
	jne	.L1981
	movq	16(%r15), %rsi
	movq	104(%rsi), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L2061
	leaq	1(%rcx), %rsi
	addq	$8, %r12
	movq	%rsi, 8(%rax)
	movq	%rbx, 16(%rax,%rcx,8)
	cmpq	%r12, -104(%rbp)
	jne	.L1985
	.p2align 4,,10
	.p2align 3
.L2059:
	movq	%r13, %r12
	movq	%r14, %r13
	movl	-112(%rbp), %r14d
.L1986:
	movq	47(%r13), %rax
	testq	%rax, %rax
	jne	.L2062
	movq	%r15, %rcx
	movl	%r14d, %edx
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
.L1970:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2063
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2060:
	.cfi_restore_state
	movq	%r13, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L1978
	testb	$-128, %ah
	je	.L1977
.L1978:
	movq	%r12, %rsi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-120(%rbp), %r8
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L2062:
	movq	$0, -72(%rbp)
	movb	$1, -80(%rbp)
	movl	$0, -76(%rbp)
	movq	47(%r13), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L2064
.L1988:
	leaq	-84(%rbp), %rax
	movl	$32, %ebx
	movq	%rax, -112(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	leaq	-1(%r12), %rax
	andq	$-262144, %r12
	movq	%rax, -128(%rbp)
	cmpl	$32, %r14d
	jle	.L1970
	movq	%r15, -136(%rbp)
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L2058:
	movslq	-84(%rbp), %rbx
.L1991:
	cmpl	%ebx, %r14d
	jle	.L1970
.L1989:
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %rdi
	movl	%r14d, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L2058
	movq	-128(%rbp), %rdi
	movslq	-84(%rbp), %r9
	addq	%rdi, %rbx
	leaq	(%r9,%rdi), %r15
	cmpq	%rbx, %r15
	jbe	.L2017
	movq	%r15, -104(%rbp)
	movq	%r12, %r15
	jmp	.L2009
	.p2align 4,,10
	.p2align 3
.L1994:
	addq	$8, %rbx
	cmpq	%rbx, -104(%rbp)
	jbe	.L2065
.L2009:
	movq	(%rbx), %r12
	testb	$1, %r12b
	je	.L1994
	movq	%r12, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	testb	$64, %al
	jne	.L1995
.L1998:
	movl	%r12d, %eax
	movq	16(%r8), %rdx
	movl	$1, %esi
	subl	%r8d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
	.p2align 4,,10
	.p2align 3
.L1996:
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	je	.L1994
	movl	%esi, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	jne	.L1996
	movq	-136(%rbp), %rax
	movq	16(%rax), %rdx
	movq	104(%rdx), %rax
	movq	8(%rax), %r13
	cmpq	$64, %r13
	je	.L2066
	leaq	1(%r13), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%rax)
	movq	%r12, 16(%rax,%r13,8)
	cmpq	%rbx, -104(%rbp)
	ja	.L2009
	.p2align 4,,10
	.p2align 3
.L2065:
	movq	%r15, %r12
	jmp	.L2058
	.p2align 4,,10
	.p2align 3
.L2064:
	movzbl	8(%r13), %eax
	movb	$0, -80(%rbp)
	sall	$3, %eax
	movl	%eax, -76(%rbp)
	jmp	.L1988
	.p2align 4,,10
	.p2align 3
.L1995:
	movq	8(%r15), %rax
	testb	$88, %al
	je	.L1997
	testb	$-128, %ah
	je	.L1998
.L1997:
	movq	112(%r15), %rax
	testq	%rax, %rax
	je	.L2067
.L2000:
	movq	%rbx, %rcx
	movl	%ebx, %edx
	subq	%r15, %rcx
	andl	$262143, %edx
	shrq	$18, %rcx
	movl	%edx, %r13d
	movl	%edx, %r9d
	sarl	$13, %edx
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$8, %r13d
	movslq	%edx, %rdx
	sarl	$3, %r9d
	salq	$7, %rcx
	andl	$31, %r13d
	andl	$31, %r9d
	leaq	(%rcx,%rdx,8), %rdx
	addq	%rax, %rdx
	movq	(%rdx), %r11
	testq	%r11, %r11
	je	.L2068
.L2002:
	movl	%r9d, %ecx
	movl	$1, %eax
	movslq	%r13d, %r13
	sall	%cl, %eax
	leaq	(%r11,%r13,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L1998
	jmp	.L2004
	.p2align 4,,10
	.p2align 3
.L2069:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1998
.L2004:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L2069
	jmp	.L1998
	.p2align 4,,10
	.p2align 3
.L2068:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%rdx, -160(%rbp)
	movl	%r9d, -152(%rbp)
	movq	%r8, -144(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-144(%rbp), %r8
	movl	-152(%rbp), %r9d
	testq	%rax, %rax
	movq	-160(%rbp), %rdx
	movq	%rax, %r11
	je	.L2070
.L2003:
	leaq	8(%r11), %rdi
	movq	%r11, %rcx
	xorl	%eax, %eax
	movq	$0, (%r11)
	andq	$-8, %rdi
	movq	$0, 120(%r11)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r11, (%rdx)
	movq	%rdx, -144(%rbp)
	testq	%rax, %rax
	je	.L2002
	movq	%r11, %rdi
	movl	%r9d, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZdaPv@PLT
	movq	-144(%rbp), %rdx
	movq	(%rdx), %r11
	movl	-160(%rbp), %r9d
	movq	-152(%rbp), %r8
	jmp	.L2002
	.p2align 4,,10
	.p2align 3
.L2067:
	movq	%r15, %rdi
	movq	%r8, -144(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-144(%rbp), %r8
	jmp	.L2000
	.p2align 4,,10
	.p2align 3
.L2066:
	leaq	744(%rdx), %rdi
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rdi, -152(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-144(%rbp), %rdx
	movq	-160(%rbp), %rax
	movq	784(%rdx), %rcx
	movq	%rcx, (%rax)
	movq	%rax, 784(%rdx)
	movq	%rdx, -144(%rbp)
	movq	-152(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r13, %rcx
	movq	-144(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 104(%rdx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L1994
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%r12, 16(%rsi,%rax,8)
	jmp	.L1994
	.p2align 4,,10
	.p2align 3
.L2061:
	leaq	744(%rsi), %rdi
	movq	%rcx, -144(%rbp)
	movq	%rax, -136(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rdi, -128(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-120(%rbp), %rsi
	movq	-136(%rbp), %rax
	movq	784(%rsi), %r8
	movq	%r8, (%rax)
	movq	%rax, 784(%rsi)
	movq	%rsi, -120(%rbp)
	movq	-128(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	-120(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	%rax, %r8
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%r8, 104(%rsi)
	movq	8(%r8), %rax
	cmpq	$64, %rax
	je	.L1975
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r8)
	movq	%rbx, 16(%r8,%rax,8)
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L2017:
	movslq	%r9d, %rbx
	jmp	.L1991
.L2063:
	call	__stack_chk_fail@PLT
.L2070:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-144(%rbp), %r8
	movl	-152(%rbp), %r9d
	testq	%rax, %rax
	movq	-160(%rbp), %rdx
	movq	%rax, %r11
	jne	.L2003
	leaq	.LC30(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE28986:
	.size	_ZN2v88internal9JSWeakRef14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_.constprop.0, .-_ZN2v88internal9JSWeakRef14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_.constprop.0
	.section	.text._ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_,"axG",@progbits,_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	.type	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_, @function
_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_:
.LFB27917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	leaq	-1(%rdi,%rsi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rsi), %r12
	testb	$1, %r12b
	jne	.L2087
.L2071:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2087:
	.cfi_restore_state
	movq	%r12, %r13
	movq	%rdx, %rbx
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testb	$64, %al
	jne	.L2088
.L2074:
	movl	%r12d, %eax
	movq	16(%r13), %rdx
	movl	$1, %esi
	subl	%r13d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	leaq	(%rdx,%rax,4), %rdi
	sall	%cl, %esi
	.p2align 4,,10
	.p2align 3
.L2077:
	movl	(%rdi), %ecx
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%esi, %eax
	je	.L2071
	movl	%ecx, %r8d
	movl	%ecx, %eax
	orl	%esi, %r8d
	lock cmpxchgl	%r8d, (%rdi)
	cmpl	%eax, %ecx
	jne	.L2077
	movq	16(%rbx), %rdi
	addq	$8, %rsp
	movq	%r12, %rdx
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	addq	$104, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	.p2align 4,,10
	.p2align 3
.L2088:
	.cfi_restore_state
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2075
	testb	$-128, %ah
	je	.L2074
.L2075:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2074
	.cfi_endproc
.LFE27917:
	.size	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_, .-_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	.section	.text._ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_,"axG",@progbits,_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	.type	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_, @function
_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_:
.LFB27921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-1(%rdi), %rbx
	leaq	(%rdx,%rbx), %r13
	addq	%rsi, %rbx
	subq	$40, %rsp
	cmpq	%r13, %rbx
	jnb	.L2089
	movq	%rdi, %r14
	movq	%rcx, %r15
	movl	$1, %r8d
	jmp	.L2090
	.p2align 4,,10
	.p2align 3
.L2102:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2089
.L2090:
	movq	(%rbx), %r12
	movq	%r12, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L2091
	cmpq	$3, %rax
	jne	.L2102
	cmpl	$3, %r12d
	je	.L2102
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-262144, %rdx
	andq	$-3, %rcx
	subl	%edx, %ecx
	movq	16(%rdx), %rsi
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	movl	(%rsi,%rax,4), %eax
	movl	%r8d, %esi
	sall	%cl, %esi
	testl	%eax, %esi
	je	.L2103
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L2102
	movq	%r14, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2104
	testb	$-128, %ah
	je	.L2102
.L2104:
	movq	%rbx, %rsi
	addq	$8, %rbx
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movl	$1, %r8d
	cmpq	%rbx, %r13
	ja	.L2090
.L2089:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2091:
	.cfi_restore_state
	movq	%r12, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L2142
.L2095:
	movl	%r12d, %eax
	movl	%r8d, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L2098:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L2102
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L2098
	movq	16(%r15), %rdx
	movq	104(%rdx), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L2143
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%r12, 16(%rax,%rcx,8)
	jmp	.L2102
	.p2align 4,,10
	.p2align 3
.L2103:
	movq	16(%r15), %rsi
	movq	5672(%rsi), %r12
	movq	8(%r12), %rax
	cmpq	$64, %rax
	je	.L2144
	addq	$1, %rax
	movq	%rax, 8(%r12)
	salq	$4, %rax
	addq	%r12, %rax
	movq	%r14, (%rax)
	movq	%rbx, 8(%rax)
	jmp	.L2102
	.p2align 4,,10
	.p2align 3
.L2142:
	movq	%r14, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2096
	testb	$-128, %ah
	je	.L2095
.L2096:
	movq	%rbx, %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-56(%rbp), %rdx
	movl	$1, %r8d
	jmp	.L2095
.L2143:
	leaq	744(%rdx), %rdi
	movq	%rcx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	784(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 784(%rdx)
	movq	%rdx, -56(%rbp)
	movq	-64(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 104(%rdx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L2102
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%r12, 16(%rsi,%rax,8)
	jmp	.L2102
.L2144:
	leaq	6312(%rsi), %rdi
	movq	%rsi, -56(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-56(%rbp), %rsi
	movq	6352(%rsi), %rax
	movq	%rax, (%r12)
	movq	%r12, 6352(%rsi)
	movq	%rsi, -56(%rbp)
	movq	-64(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movl	$1, %r8d
	movq	$0, 8(%rax)
	leaq	16(%rax), %rdx
	leaq	1040(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L2106:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L2106
	movq	%rax, 5672(%rsi)
	movq	8(%rax), %rdx
	cmpq	$64, %rdx
	je	.L2102
	addq	$1, %rdx
	movq	%rdx, 8(%rax)
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%r14, (%rax)
	movq	%rbx, 8(%rax)
	jmp	.L2102
	.cfi_endproc
.LFE27921:
	.size	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_, .-_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	.section	.text._ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_,"axG",@progbits,_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	.type	_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_, @function
_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_:
.LFB27926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	leaq	-1(%rdi,%rsi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	(%rsi), %r12
	movq	%r12, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L2146
	cmpq	$3, %rax
	je	.L2176
.L2145:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2176:
	.cfi_restore_state
	cmpl	$3, %r12d
	je	.L2145
	movq	%r12, %rdx
	andq	$-3, %r12
	andq	$-262144, %rdx
	subl	%edx, %r12d
	movq	16(%rdx), %rcx
	movl	%r12d, %eax
	shrl	$3, %r12d
	shrl	$8, %eax
	movl	(%rcx,%rax,4), %eax
	btl	%r12d, %eax
	jnc	.L2156
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L2145
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2157
	testb	$-128, %ah
	je	.L2145
.L2157:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	.p2align 4,,10
	.p2align 3
.L2146:
	.cfi_restore_state
	movq	%r12, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testb	$64, %al
	jne	.L2177
.L2150:
	movl	%r12d, %eax
	movq	16(%r13), %rdx
	movl	$1, %esi
	subl	%r13d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	leaq	(%rdx,%rax,4), %rdi
	sall	%cl, %esi
	.p2align 4,,10
	.p2align 3
.L2154:
	movl	(%rdi), %ecx
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%esi, %eax
	je	.L2145
	movl	%ecx, %edx
	movl	%ecx, %eax
	orl	%esi, %edx
	lock cmpxchgl	%edx, (%rdi)
	cmpl	%eax, %ecx
	jne	.L2154
	movq	16(%rbx), %rdi
	addq	$24, %rsp
	movq	%r12, %rdx
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	addq	$104, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	.p2align 4,,10
	.p2align 3
.L2177:
	.cfi_restore_state
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2151
	testb	$-128, %ah
	je	.L2150
.L2151:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2156:
	movq	16(%rbx), %r8
	movq	%rdi, %xmm0
	movq	%rsi, %xmm1
	xorl	%esi, %esi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	addq	$5672, %r8
	movq	-48(%rbp), %rdx
	movq	-40(%rbp), %rcx
	addq	$24, %rsp
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_
	.cfi_endproc
.LFE27926:
	.size	_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_, .-_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	.section	.text._ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_,"axG",@progbits,_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	.type	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_, @function
_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_:
.LFB27928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rbx), %r14
	movq	8(%r14), %r12
	cmpq	$16, %r12
	je	.L2182
	leaq	1(%r12), %rax
	movq	%rax, 8(%r14)
	movq	%rdx, 16(%r14,%r12,8)
.L2180:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2182:
	.cfi_restore_state
	leaq	640(%rdi), %r15
	movq	%rdi, %r13
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r14)
	movq	%r14, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$144, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	%r12, %rcx
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rbx)
	movq	$1, 8(%rsi)
	movq	%rdx, 16(%rsi)
	jmp	.L2180
	.cfi_endproc
.LFE27928:
	.size	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_, .-_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	.section	.text._ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_,"axG",@progbits,_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	.type	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_, @function
_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_:
.LFB27931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%r8, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	47(%rdi), %rax
	testq	%rax, %rax
	jne	.L2184
	movq	%r8, %rcx
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
.L2183:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2240
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2184:
	.cfi_restore_state
	movq	$0, -72(%rbp)
	movb	$1, -80(%rbp)
	movl	$0, -76(%rbp)
	movq	47(%rdi), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L2241
.L2186:
	leaq	-80(%rbp), %rax
	leaq	-84(%rbp), %r15
	movq	%rax, -104(%rbp)
	leaq	-1(%r9), %rax
	andq	$-262144, %r9
	movq	%rax, -128(%rbp)
	movq	%r9, -112(%rbp)
	cmpl	%r14d, %r12d
	jge	.L2183
	movslq	%r12d, %rbx
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2239:
	movslq	-84(%rbp), %rbx
.L2189:
	cmpl	%ebx, %r14d
	jle	.L2183
.L2187:
	movq	-104(%rbp), %rdi
	movq	%r15, %rcx
	movl	%r14d, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L2239
	movslq	-84(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%rdx, %rax
	addq	%rsi, %rbx
	addq	%rsi, %rdx
	cmpq	%rbx, %rdx
	jbe	.L2212
	movq	%r15, %r12
	movl	%r14d, %r15d
	movq	%rdx, %r14
	jmp	.L2207
	.p2align 4,,10
	.p2align 3
.L2192:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L2242
.L2207:
	movq	(%rbx), %r13
	testb	$1, %r13b
	je	.L2192
	movq	%r13, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	testb	$64, %al
	jne	.L2193
.L2196:
	movl	%r13d, %eax
	movl	$1, %edi
	subl	%r8d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edi
	movq	16(%r8), %rcx
	leaq	(%rcx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L2194:
	movl	(%rsi), %edx
	movl	%edi, %eax
	andl	%edx, %eax
	cmpl	%eax, %edi
	je	.L2192
	movl	%edi, %ecx
	movl	%edx, %eax
	orl	%edx, %ecx
	lock cmpxchgl	%ecx, (%rsi)
	cmpl	%eax, %edx
	jne	.L2194
	movq	-120(%rbp), %rax
	movq	16(%rax), %rsi
	movq	104(%rsi), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L2243
	leaq	1(%rcx), %rsi
	addq	$8, %rbx
	movq	%rsi, 8(%rax)
	movq	%r13, 16(%rax,%rcx,8)
	cmpq	%rbx, %r14
	ja	.L2207
	.p2align 4,,10
	.p2align 3
.L2242:
	movl	%r15d, %r14d
	movq	%r12, %r15
	jmp	.L2239
	.p2align 4,,10
	.p2align 3
.L2241:
	movzbl	8(%rdi), %eax
	movb	$0, -80(%rbp)
	sall	$3, %eax
	movl	%eax, -76(%rbp)
	jmp	.L2186
	.p2align 4,,10
	.p2align 3
.L2193:
	movq	-112(%rbp), %rax
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L2195
	testb	$-128, %ah
	je	.L2196
.L2195:
	movq	-112(%rbp), %rax
	movq	112(%rax), %r9
	testq	%r9, %r9
	je	.L2244
.L2198:
	movq	%rbx, %rax
	movl	%ebx, %ecx
	subq	-112(%rbp), %rax
	andl	$262143, %ecx
	shrq	$18, %rax
	movl	%ecx, %r10d
	movl	%ecx, %r11d
	leaq	(%rax,%rax,2), %rax
	sarl	$13, %ecx
	salq	$7, %rax
	movslq	%ecx, %rcx
	sarl	$8, %r10d
	leaq	(%rax,%rcx,8), %rax
	sarl	$3, %r11d
	andl	$31, %r10d
	addq	%rax, %r9
	andl	$31, %r11d
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	je	.L2245
.L2200:
	movslq	%r10d, %r10
	movl	$1, %edi
	movl	%r11d, %ecx
	leaq	(%rsi,%r10,4), %rsi
	sall	%cl, %edi
	movl	(%rsi), %eax
	testl	%eax, %edi
	jne	.L2196
	jmp	.L2202
	.p2align 4,,10
	.p2align 3
.L2246:
	movl	%edi, %ecx
	movl	%edx, %eax
	orl	%edx, %ecx
	lock cmpxchgl	%ecx, (%rsi)
	cmpl	%eax, %edx
	je	.L2196
.L2202:
	movl	(%rsi), %edx
	movl	%edi, %eax
	andl	%edx, %eax
	cmpl	%eax, %edi
	jne	.L2246
	jmp	.L2196
	.p2align 4,,10
	.p2align 3
.L2245:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%r9, -160(%rbp)
	movl	%r11d, -152(%rbp)
	movl	%r10d, -144(%rbp)
	movq	%r8, -136(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-136(%rbp), %r8
	movl	-144(%rbp), %r10d
	testq	%rax, %rax
	movl	-152(%rbp), %r11d
	movq	-160(%rbp), %r9
	movq	%rax, %rsi
	je	.L2247
.L2201:
	leaq	8(%rsi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	$0, (%rsi)
	andq	$-8, %rdi
	movq	$0, 120(%rsi)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%rsi, (%r9)
	movq	%r9, -136(%rbp)
	testq	%rax, %rax
	je	.L2200
	movq	%rsi, %rdi
	movl	%r11d, -160(%rbp)
	movl	%r10d, -152(%rbp)
	movq	%r8, -144(%rbp)
	call	_ZdaPv@PLT
	movq	-136(%rbp), %r9
	movq	(%r9), %rsi
	movl	-160(%rbp), %r11d
	movl	-152(%rbp), %r10d
	movq	-144(%rbp), %r8
	jmp	.L2200
	.p2align 4,,10
	.p2align 3
.L2244:
	movq	-112(%rbp), %rdi
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-136(%rbp), %r8
	movq	%rax, %r9
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2243:
	leaq	744(%rsi), %rdi
	movq	%rcx, -160(%rbp)
	movq	%rax, -152(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-136(%rbp), %rsi
	movq	-152(%rbp), %rax
	movq	784(%rsi), %r8
	movq	%r8, (%rax)
	movq	%rax, 784(%rsi)
	movq	%rsi, -136(%rbp)
	movq	-144(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-160(%rbp), %rcx
	movq	-136(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	%rax, %r8
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%r8, 104(%rsi)
	movq	8(%r8), %rax
	cmpq	$64, %rax
	je	.L2192
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r8)
	movq	%r13, 16(%r8,%rax,8)
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2212:
	movslq	%eax, %rbx
	jmp	.L2189
.L2240:
	call	__stack_chk_fail@PLT
.L2247:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-136(%rbp), %r8
	movl	-144(%rbp), %r10d
	testq	%rax, %rax
	movl	-152(%rbp), %r11d
	movq	-160(%rbp), %r9
	movq	%rax, %rsi
	jne	.L2201
	leaq	.LC30(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE27931:
	.size	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_, .-_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	.section	.rodata._ZN2v88internal18IncrementalMarking5HurryEv.part.0.str1.1,"aMS",@progbits,1
.LC31:
	.string	"[IncrementalMarking] Hurry\n"
.LC32:
	.string	"success"
.LC33:
	.string	"unreachable code"
	.section	.rodata._ZN2v88internal18IncrementalMarking5HurryEv.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"[IncrementalMarking] Complete (hurry), spent %d ms.\n"
	.section	.text._ZN2v88internal18IncrementalMarking5HurryEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal18IncrementalMarking5HurryEv.part.0, @function
_ZN2v88internal18IncrementalMarking5HurryEv.part.0:
.LFB28966:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm3, %xmm3
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movsd	%xmm3, -240(%rbp)
	jne	.L2716
	.p2align 4,,10
	.p2align 3
.L2625:
	movq	8(%r15), %rbx
	movq	8(%rbx), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2717
.L2260:
	leaq	-1(%rdx), %rcx
	movq	%rcx, 8(%rax)
	movq	8(%rax,%rdx,8), %r12
.L2259:
	testq	%r12, %r12
	je	.L2258
	movq	-1(%r12), %rax
	leaq	-1(%r12), %rbx
	movzwl	11(%rax), %eax
	cmpw	$73, %ax
	je	.L2625
	cmpw	$76, %ax
	je	.L2625
	movq	%r12, %r14
	movl	%ebx, %eax
	movq	(%rbx), %r13
	movl	$1, %esi
	andq	$-262144, %r14
	movq	%r12, -128(%rbp)
	subl	%r14d, %eax
	movq	16(%r14), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L2718
.L2502:
	movl	%r13d, %eax
	movq	%r13, %rdx
	movl	$1, %esi
	andl	$262143, %eax
	andq	$-262144, %rdx
	movl	%eax, %ecx
	movq	16(%rdx), %rdx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2720:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L2719
.L2276:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L2720
.L2275:
	movq	(%r15), %rax
	leaq	16+_ZTVN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEE(%rip), %rsi
	movq	%rsi, -176(%rbp)
	movq	2016(%rax), %rax
	movq	8(%rax), %rdx
	movq	%rax, -160(%rbp)
	movl	9996(%rax), %eax
	movq	%rdx, -168(%rbp)
	leaq	160(%r15), %rdx
	movq	%rdx, -152(%rbp)
	movl	%eax, -144(%rbp)
	movzbl	10(%r13), %eax
	cmpb	$56, %al
	ja	.L2280
	leaq	.L2282(%rip), %rsi
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal18IncrementalMarking5HurryEv.part.0,"a",@progbits
	.align 4
	.align 4
.L2282:
	.long	.L2337-.L2282
	.long	.L2334-.L2282
	.long	.L2335-.L2282
	.long	.L2334-.L2282
	.long	.L2332-.L2282
	.long	.L2332-.L2282
	.long	.L2280-.L2282
	.long	.L2331-.L2282
	.long	.L2330-.L2282
	.long	.L2288-.L2282
	.long	.L2328-.L2282
	.long	.L2327-.L2282
	.long	.L2299-.L2282
	.long	.L2286-.L2282
	.long	.L2324-.L2282
	.long	.L2323-.L2282
	.long	.L2286-.L2282
	.long	.L2321-.L2282
	.long	.L2288-.L2282
	.long	.L2319-.L2282
	.long	.L2318-.L2282
	.long	.L2625-.L2282
	.long	.L2316-.L2282
	.long	.L2315-.L2282
	.long	.L2314-.L2282
	.long	.L2313-.L2282
	.long	.L2492-.L2282
	.long	.L2293-.L2282
	.long	.L2310-.L2282
	.long	.L2309-.L2282
	.long	.L2492-.L2282
	.long	.L2307-.L2282
	.long	.L2306-.L2282
	.long	.L2305-.L2282
	.long	.L2304-.L2282
	.long	.L2286-.L2282
	.long	.L2302-.L2282
	.long	.L2301-.L2282
	.long	.L2300-.L2282
	.long	.L2299-.L2282
	.long	.L2299-.L2282
	.long	.L2297-.L2282
	.long	.L2296-.L2282
	.long	.L2295-.L2282
	.long	.L2294-.L2282
	.long	.L2293-.L2282
	.long	.L2290-.L2282
	.long	.L2291-.L2282
	.long	.L2290-.L2282
	.long	.L2289-.L2282
	.long	.L2288-.L2282
	.long	.L2287-.L2282
	.long	.L2286-.L2282
	.long	.L2285-.L2282
	.long	.L2284-.L2282
	.long	.L2283-.L2282
	.long	.L2281-.L2282
	.section	.text._ZN2v88internal18IncrementalMarking5HurryEv.part.0
.L2286:
	leaq	-128(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	movq	%r8, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	-176(%rbp), %rcx
	movl	$16, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2299:
	leaq	-176(%rbp), %rcx
	movl	$32, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2288:
	leaq	-176(%rbp), %rcx
	movl	$16, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2316:
	movq	-168(%rbp), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	je	.L2492
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	1496(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
.L2492:
	movzbl	7(%r13), %ecx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-176(%rbp), %r8
	sall	$3, %ecx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2334:
	movq	7(%r12), %rax
	jmp	.L2625
.L2293:
	movzbl	7(%r13), %edx
	leaq	-176(%rbp), %rcx
	movl	$8, %esi
	movq	%r12, %rdi
	sall	$3, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2332:
	movl	11(%r12), %eax
	jmp	.L2625
.L2290:
	leaq	-176(%rbp), %rcx
	movl	$24, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
	.p2align 4,,10
	.p2align 3
.L2717:
	movq	(%rbx), %r12
	cmpq	$0, 8(%r12)
	je	.L2721
	movq	%rax, %xmm0
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
.L2264:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L2258
	leaq	-1(%rax), %rdx
	movq	%rdx, 8(%r12)
	movq	8(%r12,%rax,8), %r12
	jmp	.L2259
.L2725:
	movq	1376(%rbx), %rax
	leaq	1336(%rbx), %r13
	testq	%rax, %rax
	je	.L2258
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	1376(%rbx), %r12
	testq	%r12, %r12
	jne	.L2722
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L2258:
	movq	(%r15), %rax
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movl	$3, 80(%r15)
	movb	$1, 2736(%rax)
	jne	.L2723
.L2248:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2724
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2718:
	.cfi_restore_state
	addl	%ecx, %ecx
	jne	.L2273
	addq	$4, %rsi
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L2273:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %ecx
	je	.L2502
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L2273
	movq	-128(%rbp), %rax
	leaq	-128(%rbp), %r8
	movq	%r8, %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, 96(%r14)
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2721:
	movq	680(%rbx), %rax
	leaq	640(%rbx), %r13
	testq	%rax, %rax
	je	.L2255
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%rbx), %r12
	testq	%r12, %r12
	jne	.L2256
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L2255:
	movq	704(%rbx), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L2260
	movq	696(%rbx), %r12
	cmpq	$0, 8(%r12)
	je	.L2725
	movq	%rax, %xmm0
	movq	%r12, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 696(%rbx)
	jmp	.L2264
	.p2align 4,,10
	.p2align 3
.L2719:
	movq	8(%r15), %rdx
	movq	(%rdx), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L2726
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%r13, 16(%rax,%rcx,8)
	jmp	.L2275
.L2256:
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	%rax, 680(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2257
	movl	$528, %esi
	call	_ZdlPvm@PLT
.L2257:
	movq	%r12, 8(%rbx)
	jmp	.L2264
.L2726:
	leaq	640(%rdx), %rdi
	movq	%rcx, -232(%rbp)
	movq	%rax, -224(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-208(%rbp), %rdx
	movq	-224(%rbp), %rax
	movq	680(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 680(%rdx)
	movq	%rdx, -208(%rbp)
	movq	-216(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-232(%rbp), %rcx
	movq	-208(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rdx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L2275
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%r13, 16(%rsi,%rax,8)
	jmp	.L2275
.L2294:
	leaq	-176(%rbp), %r13
	movq	%r12, %rdi
	movl	$48, %edx
	movl	$8, %esi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rcx
	movl	$112, %edx
	movq	%r12, %rdi
	movl	$48, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2295:
	movzbl	17(%r12), %eax
	leaq	-176(%rbp), %rcx
	movl	$24, %esi
	movq	%r12, %rdi
	leal	(%rax,%rax,2), %edx
	sall	$4, %edx
	addl	$24, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2296:
	movzbl	9(%r12), %edx
	leaq	-176(%rbp), %rcx
	movl	$16, %esi
	movq	%r12, %rdi
	addl	$1, %edx
	sall	$4, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2297:
	movzbl	9(%r12), %edx
	leaq	-176(%rbp), %rcx
	movl	$16, %esi
	movq	%r12, %rdi
	sall	$5, %edx
	addl	$16, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2300:
	leaq	-176(%rbp), %rcx
	movl	$40, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	movzbl	7(%r13), %eax
	leaq	7(%r12), %r13
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	cmpb	$0, _ZN2v88internal26FLAG_stress_flush_bytecodeE(%rip)
	jne	.L2507
	cmpb	$0, _ZN2v88internal19FLAG_flush_bytecodeE(%rip)
	movl	$1, %edx
	jne	.L2454
.L2457:
	movq	0(%r13), %r12
	testb	$1, %r12b
	je	.L2625
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testb	$64, %al
	jne	.L2727
.L2465:
	movl	%r12d, %eax
	movq	16(%rbx), %rdx
	movl	$1, %esi
	subl	%ebx, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
.L2468:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L2625
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L2468
.L2690:
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L2625
.L2301:
	leaq	-128(%rbp), %r8
	leaq	-176(%rbp), %r14
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	movq	%r8, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$40, %edx
	movl	$8, %esi
	movl	%eax, %r13d
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$40, %esi
	call	_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2302:
	leaq	-176(%rbp), %rcx
	movl	$40, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2291:
	leaq	-176(%rbp), %r13
	movq	%r12, %rdi
	movl	$48, %edx
	movl	$8, %esi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rcx
	movl	$72, %edx
	movq	%r12, %rdi
	movl	$56, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2304:
	movl	7(%r12), %eax
	leaq	-176(%rbp), %rcx
	movq	%r12, %rdi
	leal	23(%rax), %esi
	movl	11(%r12), %eax
	andl	$-8, %esi
	leal	(%rsi,%rax,8), %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2305:
	leaq	-176(%rbp), %rcx
	movl	$48, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2306:
	leaq	-176(%rbp), %rcx
	movl	$1944, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2307:
	cmpw	$1024, 11(%r12)
	ja	.L2728
.L2419:
	leaq	-176(%rbp), %r13
	movq	%r12, %rdi
	movl	$72, %edx
	movl	$24, %esi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rdx
	movl	$72, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	jmp	.L2625
.L2309:
	movq	23(%r12), %rax
	leaq	23(%r12), %r8
	testb	$1, %al
	jne	.L2729
.L2414:
	movzbl	7(%r13), %ebx
	leaq	-176(%rbp), %r14
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%r14, %rcx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movzbl	%bl, %ecx
	movq	%r14, %r8
	movl	$32, %edx
	sall	$3, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2310:
	movq	-168(%rbp), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	jne	.L2730
.L2407:
	leaq	-176(%rbp), %r14
	movzbl	7(%r13), %ebx
	movq	%r12, %rdi
	movl	$32, %edx
	movq	%r14, %rcx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$64, %esi
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movzbl	%bl, %ecx
	movq	%r14, %r8
	movl	$72, %edx
	sall	$3, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2289:
	leaq	-128(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	movq	%r8, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	-176(%rbp), %rcx
	movl	$8, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	-160(%rbp), %r14
	movq	2192(%r14), %r13
	movq	8(%r13), %rbx
	cmpq	$64, %rbx
	je	.L2731
.L2470:
	leaq	1(%rbx), %rax
	movq	%rax, 8(%r13)
	movq	%r12, 16(%r13,%rbx,8)
	jmp	.L2625
.L2313:
	movq	%r12, -128(%rbp)
	movzbl	7(%r13), %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-176(%rbp), %r8
	movl	$8, %edx
	sall	$3, %ecx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	cmpb	$0, _ZN2v88internal19FLAG_flush_bytecodeE(%rip)
	je	.L2625
	leaq	-128(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv
	testb	%al, %al
	je	.L2625
	movq	-160(%rbp), %rax
	movq	-128(%rbp), %rdx
	xorl	%esi, %esi
	leaq	9152(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10JSFunctionELi64EE4PushEiS2_
	jmp	.L2625
.L2314:
	movq	-168(%rbp), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	jne	.L2732
.L2403:
	movzbl	7(%r13), %ebx
	leaq	-176(%rbp), %r14
	movq	%r12, %rdi
	movl	$32, %edx
	movq	%r14, %rcx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movzbl	%bl, %ecx
	movq	%r14, %r8
	movl	$56, %edx
	sall	$3, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2315:
	movq	-168(%rbp), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	jne	.L2733
.L2402:
	movzbl	7(%r13), %ebx
	leaq	-176(%rbp), %r14
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%r14, %rcx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movzbl	%bl, %ecx
	movq	%r14, %r8
	movl	$48, %edx
	sall	$3, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2318:
	leaq	-128(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	movq	%r8, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movl	%eax, %r8d
	testb	$1, 9(%r14)
	je	.L2388
	leaq	88(%r14), %rax
	movq	%rax, -208(%rbp)
	movq	88(%r14), %r9
	movslq	%r9d, %rax
	leal	32768(%r9), %r13d
	testl	%r9d, %r9d
	jne	.L2389
	movl	$32784, %r13d
	movl	$16, %eax
.L2389:
	cmpl	%r13d, %r8d
	cmovle	%r8d, %r13d
	movl	%r13d, -216(%rbp)
	cmpl	%r13d, %eax
	jge	.L2625
	movslq	%r13d, %rsi
	leaq	(%rbx,%rsi), %r13
	addq	%rax, %rbx
	movq	%rsi, -224(%rbp)
	cmpq	%rbx, %r13
	jbe	.L2401
	movq	%r12, %rsi
	movq	%rbx, %rax
	movq	%r13, %r12
	movq	%r14, %rbx
	movl	%r8d, -232(%rbp)
	movq	%rax, %r14
	movq	%rsi, %r13
	movq	%r9, -248(%rbp)
	jmp	.L2391
.L2496:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L2734
.L2391:
	movq	(%r14), %r10
	testb	$1, %r10b
	je	.L2496
	movq	%r10, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L2735
.L2396:
	movl	%r10d, %eax
	movl	$1, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L2399:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L2496
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L2399
	movq	-160(%rbp), %rax
	movq	%r10, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L2496
.L2319:
	movl	31(%r12), %eax
	leaq	-176(%rbp), %r13
	movq	%r12, %rdi
	movl	$8, %esi
	movq	%r13, %rdx
	leal	48(,%rax,8), %r14d
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$16, %esi
	call	_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$24, %esi
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %rcx
	movl	%r14d, %edx
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2321:
	movq	-160(%rbp), %rsi
	movq	%r12, -128(%rbp)
	movq	2888(%rsi), %r14
	movq	8(%r14), %rbx
	cmpq	$64, %rbx
	je	.L2736
	leaq	1(%rbx), %rax
	movq	%rax, 8(%r14)
	movq	%r12, 16(%r14,%rbx,8)
.L2363:
	movq	-128(%rbp), %rax
	movl	$40, %ebx
	xorl	%r12d, %r12d
	movl	35(%rax), %edx
	testl	%edx, %edx
	jle	.L2387
	movq	%r13, -208(%rbp)
	movq	%r15, -216(%rbp)
	jmp	.L2365
.L2740:
	movq	(%rsi), %r14
	movq	-128(%rbp), %rax
	testb	$1, %r14b
	jne	.L2737
.L2371:
	addl	$1, %r12d
	addq	$16, %rbx
	cmpl	%r12d, 35(%rax)
	jle	.L2738
.L2365:
	leaq	-1(%rax,%rbx), %rsi
	movq	(%rsi), %r13
	movq	-128(%rbp), %rdi
	movq	%r13, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testb	$64, %al
	jne	.L2739
.L2367:
	movq	-128(%rbp), %rax
	movl	%r13d, %ecx
	movq	16(%r14), %rdi
	subl	%r14d, %ecx
	leaq	7(%rbx,%rax), %rsi
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	movl	(%rdi,%rax,4), %eax
	movl	$1, %edi
	sall	%cl, %edi
	testl	%eax, %edi
	jne	.L2740
	movq	(%rsi), %r14
	movq	-128(%rbp), %rax
	testb	$1, %r14b
	je	.L2371
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rcx
	andl	$64, %ecx
	jne	.L2741
.L2380:
	movl	%r14d, %ecx
	movq	16(%r15), %rsi
	subl	%r15d, %ecx
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	movl	(%rsi,%rax,4), %eax
	movl	$1, %esi
	sall	%cl, %esi
	testl	%eax, %esi
	je	.L2383
.L2385:
	movq	-128(%rbp), %rax
	addl	$1, %r12d
	addq	$16, %rbx
	cmpl	%r12d, 35(%rax)
	jg	.L2365
.L2738:
	movq	-208(%rbp), %r13
	movq	-216(%rbp), %r15
.L2387:
	leaq	-128(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	jmp	.L2625
.L2287:
	leaq	-176(%rbp), %r13
	movq	%r12, %rdi
	movl	$16, %edx
	movl	$8, %esi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rcx
	movl	$32, %edx
	movq	%r12, %rdi
	movl	$24, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2323:
	leaq	-128(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	movq	%r8, %rdi
	movq	%r8, -208(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	15(%r12), %rsi
	leaq	23(%r12), %rax
	movq	-208(%rbp), %r8
	cmpq	%rsi, %rax
	jbe	.L2495
	movq	15(%r12), %r13
	testb	$1, %r13b
	jne	.L2742
.L2495:
	movswl	9(%r12), %ebx
	movl	-144(%rbp), %esi
	movq	%r8, %rdi
	movq	%r12, -128(%rbp)
	movl	%ebx, %edx
	call	_ZN2v88internal15DescriptorArray31UpdateNumberOfMarkedDescriptorsEjs@PLT
	cmpw	%ax, %bx
	jle	.L2625
	cwtl
	leal	3(%rbx,%rbx,2), %r12d
	movq	-128(%rbp), %r13
	leal	3(%rax,%rax,2), %ebx
	sall	$3, %r12d
	sall	$3, %ebx
	leaq	-1(%r13), %rdx
	movslq	%r12d, %r12
	movslq	%ebx, %rbx
	addq	%rdx, %r12
	addq	%rdx, %rbx
	cmpq	%rbx, %r12
	ja	.L2361
	jmp	.L2625
	.p2align 4,,10
	.p2align 3
.L2353:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jbe	.L2625
.L2361:
	movq	(%rbx), %r14
	movq	%r14, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L2743
	cmpq	$3, %rax
	jne	.L2353
	cmpl	$3, %r14d
	je	.L2353
	movq	%r14, %rdx
	movq	%r14, %rax
	movl	$1, %edi
	andq	$-262144, %rdx
	andq	$-3, %rax
	subl	%edx, %eax
	movq	16(%rdx), %rsi
	movl	%eax, %ecx
	shrl	$3, %eax
	shrl	$8, %ecx
	movl	(%rsi,%rcx,4), %esi
	movl	%eax, %ecx
	sall	%cl, %edi
	testl	%esi, %edi
	je	.L2359
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L2353
	movq	%r13, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2360
	testb	$-128, %ah
	je	.L2353
.L2360:
	movq	%rbx, %rsi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2324:
	leaq	-128(%rbp), %r8
	movq	%r13, %rsi
	leaq	-176(%rbp), %r14
	movq	%r12, -128(%rbp)
	movq	%r8, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$24, %edx
	movl	%eax, %r13d
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2285:
	leaq	-128(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	movq	%r8, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	-176(%rbp), %rcx
	movl	$32, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2327:
	leaq	-176(%rbp), %rcx
	movl	$8, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2328:
	leaq	-176(%rbp), %r13
	movq	%r12, %rdi
	movl	$40, %edx
	movl	$8, %esi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	leaq	-128(%rbp), %r8
	movq	%r12, %rsi
	movl	$1999, %edx
	movq	%r8, %rdi
	movq	%r8, -208(%rbp)
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	movq	-208(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE@PLT
	jmp	.L2625
.L2284:
	movzbl	7(%r13), %eax
	movl	$24, %edx
	movq	%r12, %rdi
	movl	$8, %esi
	sall	$3, %eax
	movl	%eax, -216(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, %rcx
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	leaq	_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE(%rip), %rdx
	movq	%r13, -224(%rbp)
	movq	%r14, %r13
	movq	%r12, -232(%rbp)
	leaq	30(%rdx), %r9
	movq	%rbx, %r12
	movq	%rdx, %rbx
	movq	%r15, -248(%rbp)
	jmp	.L2484
	.p2align 4,,10
	.p2align 3
.L2474:
	addq	$2, %rbx
	cmpq	%r9, %rbx
	je	.L2744
.L2484:
	movzwl	(%rbx), %esi
	addq	%r12, %rsi
	movq	(%rsi), %r14
	testb	$1, %r14b
	je	.L2474
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rcx
	andl	$64, %ecx
	jne	.L2745
.L2476:
	movl	%r14d, %edi
	movq	16(%r15), %rax
	movl	$1, %esi
	subl	%r15d, %edi
	movl	%edi, %ecx
	shrl	$8, %edi
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%edi, %ecx
	leaq	(%rax,%rcx,4), %rdi
	.p2align 4,,10
	.p2align 3
.L2480:
	movl	(%rdi), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	je	.L2474
	movl	%esi, %ecx
	movl	%edx, %eax
	orl	%edx, %ecx
	lock cmpxchgl	%ecx, (%rdi)
	cmpl	%eax, %edx
	jne	.L2480
	movq	-160(%rbp), %r15
	movq	104(%r15), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L2746
	leaq	1(%rcx), %rsi
	addq	$2, %rbx
	movq	%rsi, 8(%rax)
	movq	%r14, 16(%rax,%rcx,8)
	cmpq	%r9, %rbx
	jne	.L2484
.L2744:
	movq	-224(%rbp), %r13
	movq	-232(%rbp), %r12
	movq	-248(%rbp), %r15
	movq	47(%r13), %rax
	testq	%rax, %rax
	je	.L2747
	movq	$0, -120(%rbp)
	movb	$1, -128(%rbp)
	movl	$0, -124(%rbp)
	movq	47(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2487
	movzbl	8(%r13), %eax
	movb	$0, -128(%rbp)
	sall	$3, %eax
	movl	%eax, -124(%rbp)
.L2487:
	movl	-216(%rbp), %r14d
	movl	$280, %r13d
	leaq	-128(%rbp), %r8
	leaq	-180(%rbp), %rbx
	cmpl	$280, %r14d
	jle	.L2625
	movq	%r15, -216(%rbp)
	movq	%r8, %r15
.L2488:
	movq	%rbx, %rcx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	jne	.L2748
	movl	-180(%rbp), %r13d
	cmpl	%r13d, %r14d
	jg	.L2488
.L2713:
	movq	-216(%rbp), %r15
	jmp	.L2625
.L2335:
	movzbl	7(%r13), %eax
	jmp	.L2625
.L2281:
	movq	15(%r12), %rax
	leaq	15(%r12), %r8
	testb	$1, %al
	jne	.L2749
.L2409:
	movzbl	7(%r13), %ebx
	leaq	-176(%rbp), %r13
	movq	%r12, %rdi
	movl	$16, %edx
	movq	%r13, %rcx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movzbl	%bl, %edx
	movq	%r13, %rcx
	movl	$24, %esi
	sall	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2337:
	movl	7(%r12), %eax
	jmp	.L2625
.L2331:
	leaq	-176(%rbp), %rcx
	movl	$32, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	movzbl	7(%r13), %eax
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2283:
	leaq	-128(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	movq	%r8, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	-176(%rbp), %rcx
	movl	$8, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2330:
	movq	%r12, -128(%rbp)
	movq	7(%r12), %rax
	leaq	-176(%rbp), %r13
	movl	$16, %esi
	movq	%r13, %rdx
	movq	-128(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %rdx
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %rdx
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	-168(%rbp), %rax
	cmpb	$0, 2764(%rax)
	jne	.L2625
	leaq	-128(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal13BytecodeArray9MakeOlderEv@PLT
	jmp	.L2625
.L2739:
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2368
	testb	$-128, %ah
	je	.L2367
.L2368:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2367
.L2745:
	movq	8(%r13), %rcx
	testb	$88, %cl
	je	.L2477
	andb	$-128, %ch
	je	.L2476
.L2477:
	movq	%r13, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	leaq	30+_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE(%rip), %r9
	jmp	.L2476
.L2737:
	movq	%r14, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rcx
	andl	$64, %ecx
	jne	.L2750
.L2373:
	movl	%r14d, %eax
	movq	16(%r13), %rdx
	movl	$1, %esi
	subl	%r13d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L2377:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L2385
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L2377
	movq	-160(%rbp), %rax
	movq	%r14, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	movq	-128(%rbp), %rax
	jmp	.L2371
.L2722:
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	%rax, 1376(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	704(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2267
	movl	$528, %esi
	call	_ZdlPvm@PLT
.L2267:
	movq	%r12, 704(%rbx)
	jmp	.L2264
.L2716:
	movq	(%rdi), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movsd	%xmm0, -240(%rbp)
	je	.L2625
	movq	(%r15), %rax
	leaq	.LC31(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L2625
	.p2align 4,,10
	.p2align 3
.L2723:
	movq	(%r15), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	je	.L2248
	subsd	-240(%rbp), %xmm0
	movq	(%r15), %rdi
	leaq	.LC34(%rip), %rsi
	xorl	%eax, %eax
	subq	$37592, %rdi
	cvttsd2sil	%xmm0, %edx
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L2248
.L2388:
	leaq	-176(%rbp), %rcx
	movl	%eax, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2747:
	movq	-208(%rbp), %rcx
	movl	-216(%rbp), %edx
	movl	$280, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2625
.L2507:
	movl	$2, %edx
.L2454:
	movl	47(%r12), %eax
	andl	$31, %eax
	leal	-9(%rax), %esi
	cmpb	$6, %sil
	jbe	.L2457
	cmpb	$1, %al
	je	.L2457
	movl	47(%r12), %eax
	testb	$16, %ah
	je	.L2457
	movq	0(%r13), %rax
	testb	$1, %al
	je	.L2457
	movq	-1(%rax), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L2457
	cmpl	$2, %edx
	je	.L2460
	leaq	-128(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%r8, %rdi
	call	_ZNK2v88internal13BytecodeArray5IsOldEv@PLT
	testb	%al, %al
	je	.L2457
.L2460:
	movq	-160(%rbp), %r14
	movq	8456(%r14), %r13
	movq	8(%r13), %rbx
	cmpq	$64, %rbx
	jne	.L2470
	leaq	9096(%r14), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	9136(%r14), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 9136(%r14)
	movq	-208(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%rbx, %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 8456(%r14)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	jne	.L2691
	jmp	.L2625
	.p2align 4,,10
	.p2align 3
.L2748:
	movq	-208(%rbp), %rcx
	movl	-180(%rbp), %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movl	-180(%rbp), %r13d
	cmpl	%r14d, %r13d
	jl	.L2488
	jmp	.L2713
.L2743:
	movq	%r14, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L2751
.L2351:
	movl	%r14d, %eax
	movl	$1, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
.L2357:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L2353
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L2357
	movq	-160(%rbp), %rax
	movq	%r14, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L2353
.L2750:
	andq	$-262144, %rax
	movq	%rax, %rdi
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L2374
	testb	$-128, %ah
	je	.L2373
.L2374:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2373
.L2383:
	movq	-160(%rbp), %r15
	movq	4976(%r15), %rcx
	movq	8(%rcx), %rax
	cmpq	$64, %rax
	je	.L2752
	addq	$1, %rax
	movq	%r13, %xmm0
	movq	%r14, %xmm7
	movq	%rax, 8(%rcx)
	punpcklqdq	%xmm7, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rcx,%rax)
	jmp	.L2385
.L2741:
	andq	$-262144, %rax
	movq	%rax, %rdi
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L2381
	testb	$-128, %ah
	je	.L2380
.L2381:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2380
.L2746:
	leaq	744(%r15), %rdi
	movq	%rcx, -272(%rbp)
	movq	%rax, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	784(%r15), %r10
	movq	-264(%rbp), %rax
	movq	%r10, (%rax)
	movq	%rax, 784(%r15)
	movq	-256(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-272(%rbp), %rcx
	leaq	30+_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE(%rip), %r9
	movq	$0, 8(%rax)
	movq	%rax, %r10
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%r10, 104(%r15)
	movq	8(%r10), %rax
	cmpq	$64, %rax
	je	.L2474
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r10)
	movq	%r14, 16(%r10,%rax,8)
	jmp	.L2474
.L2749:
	movq	%rax, %rsi
	andl	$262143, %eax
	movl	$1, %ebx
	andq	$-262144, %rsi
	movl	%eax, %edx
	shrl	$3, %eax
	movq	16(%rsi), %rcx
	shrl	$8, %edx
	movl	(%rcx,%rdx,4), %edx
	movl	%eax, %ecx
	sall	%cl, %ebx
	testl	%edx, %ebx
	je	.L2410
	movq	8(%rsi), %rax
	testb	$64, %al
	je	.L2409
	movq	8(%r14), %rax
	testb	$88, %al
	je	.L2412
	testb	$-128, %ah
	je	.L2409
.L2412:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2409
.L2742:
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testb	$64, %al
	jne	.L2340
.L2344:
	movl	%r13d, %eax
	movq	16(%rbx), %rdx
	movl	$1, %esi
	subl	%ebx, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
.L2341:
	movl	(%rcx), %edx
	movl	%edx, %eax
	andl	%esi, %eax
	cmpl	%eax, %esi
	je	.L2495
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	jne	.L2341
	movq	-160(%rbp), %rax
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r8, -208(%rbp)
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	movq	-208(%rbp), %r8
	jmp	.L2495
.L2733:
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	1496(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	jmp	.L2402
.L2730:
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	1496(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	jmp	.L2407
.L2729:
	movq	%rax, %rsi
	andl	$262143, %eax
	movl	$1, %ebx
	andq	$-262144, %rsi
	movl	%eax, %edx
	shrl	$3, %eax
	movq	16(%rsi), %rcx
	shrl	$8, %edx
	movl	(%rcx,%rdx,4), %edx
	movl	%eax, %ecx
	sall	%cl, %ebx
	testl	%edx, %ebx
	je	.L2415
	movq	8(%rsi), %rax
	testb	$64, %al
	je	.L2414
	movq	8(%r14), %rax
	testb	$88, %al
	je	.L2417
	testb	$-128, %ah
	je	.L2414
.L2417:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2414
.L2728:
	movq	39(%r12), %r13
	movl	$1, %esi
	movq	%r13, %r14
	movl	%r13d, %eax
	andl	$262143, %eax
	andq	$-262144, %r14
	movl	%eax, %ecx
	movq	16(%r14), %rdx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
	jmp	.L2423
.L2420:
	movl	%esi, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L2424
.L2423:
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	jne	.L2420
.L2424:
	leaq	-1(%r13), %rax
	movl	$1, %edx
	movq	%r13, -128(%rbp)
	subl	%r14d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r14), %rcx
	leaq	(%rcx,%rax,4), %rcx
	movl	(%rcx), %eax
	testl	%eax, %edx
	jne	.L2753
.L2429:
	movl	15(%r12), %ebx
	shrl	$10, %ebx
	andl	$1023, %ebx
	je	.L2419
	leaq	-128(%rbp), %r8
	movl	-144(%rbp), %esi
	movl	%ebx, %edx
	movq	%r13, -128(%rbp)
	movq	%r8, %rdi
	call	_ZN2v88internal15DescriptorArray31UpdateNumberOfMarkedDescriptorsEjs@PLT
	cmpw	%bx, %ax
	jge	.L2419
	cwtl
	leal	3(%rbx,%rbx,2), %r13d
	movq	-128(%rbp), %r14
	leal	3(%rax,%rax,2), %ebx
	sall	$3, %r13d
	sall	$3, %ebx
	leaq	-1(%r14), %rdx
	movslq	%r13d, %r13
	movslq	%ebx, %rbx
	addq	%rdx, %r13
	addq	%rdx, %rbx
	cmpq	%rbx, %r13
	jbe	.L2419
	movq	%r12, -216(%rbp)
	jmp	.L2453
.L2445:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2754
.L2453:
	movq	(%rbx), %r12
	movq	%r12, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L2755
	cmpq	$3, %rax
	jne	.L2445
	cmpl	$3, %r12d
	je	.L2445
	movq	%r12, %rdx
	movq	%r12, %rax
	movl	$1, %edi
	andq	$-262144, %rdx
	andq	$-3, %rax
	subl	%edx, %eax
	movq	16(%rdx), %rsi
	movl	%eax, %ecx
	shrl	$3, %eax
	shrl	$8, %ecx
	movl	(%rsi,%rcx,4), %esi
	movl	%eax, %ecx
	sall	%cl, %edi
	testl	%esi, %edi
	je	.L2451
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L2445
	movq	%r14, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2452
	testb	$-128, %ah
	je	.L2445
.L2452:
	movq	%rbx, %rsi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2445
	.p2align 4,,10
	.p2align 3
.L2732:
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	1496(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	jmp	.L2403
.L2734:
	movl	-232(%rbp), %r8d
	movq	-248(%rbp), %r9
	movq	%r13, %r12
.L2401:
	movq	-208(%rbp), %rbx
	movq	-224(%rbp), %rsi
	movq	%r9, %rax
	lock cmpxchgq	%rsi, (%rbx)
	jne	.L2756
	cmpl	-216(%rbp), %r8d
	jle	.L2625
	jmp	.L2690
.L2359:
	movq	%r13, %xmm4
	movq	%rbx, %xmm6
	movq	-160(%rbp), %rax
	xorl	%esi, %esi
	punpcklqdq	%xmm6, %xmm4
	movaps	%xmm4, -208(%rbp)
	movq	-208(%rbp), %rdx
	movq	-200(%rbp), %rcx
	leaq	5672(%rax), %rdi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_
	jmp	.L2353
.L2731:
	leaq	2832(%r14), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	2872(%r14), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 2872(%r14)
	movq	-208(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%rbx, %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 2192(%r14)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L2625
.L2691:
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%r12, 16(%rdx,%rax,8)
	jmp	.L2625
.L2735:
	movq	8(%rbx), %rax
	testb	$88, %al
	je	.L2397
	testb	$-128, %ah
	je	.L2396
.L2397:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -264(%rbp)
	movq	%r10, -256(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-264(%rbp), %rdx
	movq	-256(%rbp), %r10
	jmp	.L2396
.L2736:
	leaq	3528(%rsi), %rdi
	movq	%rsi, -208(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-208(%rbp), %rsi
	movq	3568(%rsi), %rax
	movq	%rax, (%r14)
	movq	%r14, 3568(%rsi)
	movq	%rsi, -208(%rbp)
	movq	-216(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%rbx, %rcx
	movq	-208(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 2888(%rsi)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L2363
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%r12, 16(%rdx,%rax,8)
	jmp	.L2363
.L2751:
	movq	%r13, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2354
	testb	$-128, %ah
	je	.L2351
.L2354:
	movq	%rbx, %rsi
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-208(%rbp), %rdx
	jmp	.L2351
.L2415:
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	7064(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_9JSWeakRefELi64EE4PushEiS2_
	jmp	.L2414
.L2410:
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	7760(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_8WeakCellELi64EE4PushEiS2_
	jmp	.L2409
.L2280:
	leaq	.LC33(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2727:
	movq	8(%r14), %rax
	testb	$88, %al
	je	.L2466
	testb	$-128, %ah
	je	.L2465
.L2466:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2465
.L2752:
	leaq	5616(%r15), %rdi
	movq	%rcx, -232(%rbp)
	movq	%rdi, -224(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-232(%rbp), %rcx
	movq	5656(%r15), %rax
	movq	%rax, (%rcx)
	movq	%rcx, 5656(%r15)
	movq	-224(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 4976(%r15)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L2385
	addq	$1, %rax
	movq	%r13, %xmm0
	movq	%r14, %xmm6
	movq	%rax, 8(%rsi)
	punpcklqdq	%xmm6, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rsi,%rax)
	jmp	.L2385
.L2340:
	movq	8(%r14), %rax
	testb	$88, %al
	je	.L2346
	testb	$-128, %ah
	je	.L2344
.L2346:
	movq	%r14, %rdi
	movq	%r8, -208(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-208(%rbp), %r8
	jmp	.L2344
.L2753:
	addl	%edx, %edx
	jne	.L2426
	addq	$4, %rcx
	movl	$1, %edx
.L2426:
	movl	(%rcx), %esi
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %edx
	je	.L2429
	movl	%esi, %edi
	movl	%esi, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %esi
	jne	.L2426
	leaq	-128(%rbp), %r8
	movq	%r8, %rdi
	call	_ZNK2v88internal10HeapObject4SizeEv
	leaq	15(%r13), %rsi
	cltq
	addq	%rax, 96(%r14)
	leaq	23(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L2429
	movq	15(%r13), %rbx
	testb	$1, %bl
	je	.L2429
	movq	%rbx, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L2430
.L2435:
	movl	%ebx, %eax
	movl	$1, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
.L2432:
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	je	.L2429
	movl	%esi, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	jne	.L2432
	movq	-160(%rbp), %rax
	movq	%rbx, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L2429
	.p2align 4,,10
	.p2align 3
.L2755:
	movq	%r12, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L2757
.L2443:
	movl	%r12d, %eax
	movl	$1, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
.L2449:
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	je	.L2445
	movl	%esi, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	jne	.L2449
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L2445
.L2754:
	movq	-216(%rbp), %r12
	jmp	.L2419
.L2757:
	movq	%r14, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2446
	testb	$-128, %ah
	je	.L2443
.L2446:
	movq	%rbx, %rsi
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-208(%rbp), %rdx
	jmp	.L2443
.L2756:
	leaq	.LC32(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2451:
	movq	%r14, %xmm5
	movq	%rbx, %xmm7
	movq	-160(%rbp), %rax
	xorl	%esi, %esi
	punpcklqdq	%xmm7, %xmm5
	movaps	%xmm5, -208(%rbp)
	movq	-208(%rbp), %rdx
	movq	-200(%rbp), %rcx
	leaq	5672(%rax), %rdi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_
	jmp	.L2445
.L2724:
	call	__stack_chk_fail@PLT
.L2430:
	movq	8(%r14), %rax
	testb	$88, %al
	je	.L2437
	testb	$-128, %ah
	je	.L2435
.L2437:
	movq	%r14, %rdi
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-208(%rbp), %rdx
	jmp	.L2435
	.cfi_endproc
.LFE28966:
	.size	_ZN2v88internal18IncrementalMarking5HurryEv.part.0, .-_ZN2v88internal18IncrementalMarking5HurryEv.part.0
	.section	.text._ZN2v88internal18IncrementalMarking5HurryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking5HurryEv
	.type	_ZN2v88internal18IncrementalMarking5HurryEv, @function
_ZN2v88internal18IncrementalMarking5HurryEv:
.LFB22409:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	8(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L2760
	movq	(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L2760
	movq	704(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L2760
	movq	696(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L2760
	movq	680(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2761
.L2760:
	jmp	_ZN2v88internal18IncrementalMarking5HurryEv.part.0
.L2761:
	movq	1376(%rax), %rax
	testq	%rax, %rax
	jne	.L2760
	ret
	.cfi_endproc
.LFE22409:
	.size	_ZN2v88internal18IncrementalMarking5HurryEv, .-_ZN2v88internal18IncrementalMarking5HurryEv
	.section	.rodata._ZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginE.str1.1,"aMS",@progbits,1
.LC35:
	.string	"in v8"
.LC36:
	.string	"in task"
	.section	.rodata._ZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginE.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"[IncrementalMarking] Marking speed %.fKB/ms\n"
	.align 8
.LC38:
	.string	"[IncrementalMarking] Step %s %zuKB (%zuKB) in %.1f\n"
	.section	.text._ZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginE
	.type	_ZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginE, @function
_ZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginE:
.LFB22429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -244(%rbp)
	movq	(%rdi), %rdi
	movl	%edx, -208(%rbp)
	movsd	%xmm0, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movl	80(%r15), %eax
	movsd	%xmm0, -216(%rbp)
	cmpl	$1, %eax
	je	.L3213
.L2763:
	movzbl	_ZN2v88internal23FLAG_concurrent_markingE(%rip), %edx
	cmpl	$2, %eax
	je	.L3214
	movq	$0, -224(%rbp)
	xorl	%ebx, %ebx
	movl	$1, -204(%rbp)
.L2771:
	testb	%dl, %dl
	jne	.L3215
.L3004:
	movq	(%r15), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	(%r15), %rax
	subsd	-216(%rbp), %xmm0
	movq	%rbx, %rsi
	movq	2008(%rax), %rdi
	movsd	%xmm0, -200(%rbp)
	call	_ZN2v88internal8GCTracer25AddIncrementalMarkingStepEdm@PLT
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	jne	.L3216
.L2762:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3217
	movl	-204(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3216:
	.cfi_restore_state
	movl	-208(%rbp), %eax
	movq	(%r15), %rdi
	shrq	$10, %rbx
	leaq	.LC35(%rip), %rdx
	movq	-224(%rbp), %r8
	movsd	-200(%rbp), %xmm0
	movq	%rbx, %rcx
	leaq	.LC38(%rip), %rsi
	subq	$37592, %rdi
	testl	%eax, %eax
	leaq	.LC36(%rip), %rax
	cmovne	%rax, %rdx
	shrq	$10, %r8
	movl	$1, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L2762
.L3215:
	movq	8(%r15), %r12
	movq	8(%r12), %rax
	cmpq	$0, 8(%rax)
	jne	.L3005
	movq	(%r12), %rax
	cmpq	$0, 8(%rax)
	je	.L3006
.L3005:
	movq	680(%r12), %r13
	leaq	640(%r12), %r14
	leaq	680(%r12), %rsi
	testq	%r13, %r13
	je	.L3218
.L3006:
	movq	(%r15), %rax
	movq	2072(%rax), %rdi
	call	_ZN2v88internal17ConcurrentMarking23RescheduleTasksIfNeededEv@PLT
	jmp	.L3004
	.p2align 4,,10
	.p2align 3
.L3214:
	movq	(%r15), %rbx
	testb	%dl, %dl
	jne	.L3219
.L2772:
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movq	2008(%rbx), %rdi
	jne	.L3220
.L2776:
	call	_ZNK2v88internal8GCTracer44IncrementalMarkingSpeedInBytesPerMillisecondEv@PLT
	movapd	%xmm0, %xmm1
	movsd	-200(%rbp), %xmm0
	call	_ZN2v88internal17GCIdleTimeHandler23EstimateMarkingStepSizeEdd@PLT
	movl	-208(%rbp), %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal18IncrementalMarking22ComputeStepSizeInBytesENS0_10StepOriginE
	cmpq	%rbx, %rax
	cmovbe	%rax, %rbx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	movq	%rbx, -224(%rbp)
	setne	%al
	cmpq	$65536, %rbx
	movl	%eax, -204(%rbp)
	movl	$65536, %eax
	cmovnb	%rbx, %rax
	xorl	%r10d, %r10d
	movq	%r10, %r14
	movq	%rax, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L3128:
	movq	8(%r15), %r12
	cmpq	%r14, -200(%rbp)
	jle	.L3186
.L3225:
	movq	8(%r12), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L3221
.L2788:
	leaq	-1(%rdx), %rcx
	movq	%rcx, 8(%rax)
	movq	8(%rax,%rdx,8), %r12
.L2787:
	testq	%r12, %r12
	je	.L3187
	movq	-1(%r12), %rax
	leaq	-1(%r12), %rbx
	movzwl	11(%rax), %eax
	cmpw	$76, %ax
	je	.L3128
	cmpw	$73, %ax
	je	.L3128
	movq	%r12, %r13
	movl	%ebx, %eax
	movq	(%rbx), %r10
	movl	$1, %esi
	andq	$-262144, %r13
	movq	%r12, -144(%rbp)
	subl	%r13d, %eax
	movq	16(%r13), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	movl	(%rsi), %eax
	testl	%ecx, %eax
	jne	.L3222
.L3013:
	movl	%r10d, %eax
	movq	%r10, %rdx
	movl	$1, %esi
	andl	$262143, %eax
	andq	$-262144, %rdx
	movl	%eax, %ecx
	movq	16(%rdx), %rdx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	jmp	.L2804
	.p2align 4,,10
	.p2align 3
.L3224:
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L3223
.L2804:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	jne	.L3224
.L2803:
	movq	(%r15), %rax
	leaq	16+_ZTVN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEE(%rip), %rsi
	movq	2016(%rax), %rax
	movq	%rsi, -192(%rbp)
	movq	8(%rax), %rdx
	movq	%rax, -176(%rbp)
	movq	%rdx, -184(%rbp)
	leaq	160(%r15), %rdx
	movq	%rdx, -168(%rbp)
	movl	9996(%rax), %eax
	movl	%eax, -160(%rbp)
	movzbl	10(%r10), %eax
	cmpb	$56, %al
	ja	.L2806
	leaq	.L2808(%rip), %rsi
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginE,"a",@progbits
	.align 4
	.align 4
.L2808:
	.long	.L2863-.L2808
	.long	.L2862-.L2808
	.long	.L2861-.L2808
	.long	.L2860-.L2808
	.long	.L2859-.L2808
	.long	.L2858-.L2808
	.long	.L2806-.L2808
	.long	.L2857-.L2808
	.long	.L2856-.L2808
	.long	.L2855-.L2808
	.long	.L2854-.L2808
	.long	.L2853-.L2808
	.long	.L2825-.L2808
	.long	.L2812-.L2808
	.long	.L2850-.L2808
	.long	.L2849-.L2808
	.long	.L2812-.L2808
	.long	.L2847-.L2808
	.long	.L2814-.L2808
	.long	.L2845-.L2808
	.long	.L2844-.L2808
	.long	.L2843-.L2808
	.long	.L2842-.L2808
	.long	.L2841-.L2808
	.long	.L2840-.L2808
	.long	.L2839-.L2808
	.long	.L2988-.L2808
	.long	.L2819-.L2808
	.long	.L2836-.L2808
	.long	.L2835-.L2808
	.long	.L2988-.L2808
	.long	.L2833-.L2808
	.long	.L2832-.L2808
	.long	.L2831-.L2808
	.long	.L2830-.L2808
	.long	.L2812-.L2808
	.long	.L2828-.L2808
	.long	.L2827-.L2808
	.long	.L2826-.L2808
	.long	.L2825-.L2808
	.long	.L2825-.L2808
	.long	.L2823-.L2808
	.long	.L2822-.L2808
	.long	.L2821-.L2808
	.long	.L2820-.L2808
	.long	.L2819-.L2808
	.long	.L2816-.L2808
	.long	.L2817-.L2808
	.long	.L2816-.L2808
	.long	.L2815-.L2808
	.long	.L2814-.L2808
	.long	.L2813-.L2808
	.long	.L2812-.L2808
	.long	.L2811-.L2808
	.long	.L2810-.L2808
	.long	.L2809-.L2808
	.long	.L2807-.L2808
	.section	.text._ZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginE
.L2812:
	movq	%r10, %rsi
	leaq	-144(%rbp), %rdi
	movq	%r12, -144(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	-192(%rbp), %rcx
	movl	$16, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	movl	%eax, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movslq	%ebx, %r8
	.p2align 4,,10
	.p2align 3
.L2864:
	movq	8(%r15), %r12
	addq	%r8, %r14
	cmpq	%r14, -200(%rbp)
	jg	.L3225
.L3186:
	movq	%r14, %r10
	jmp	.L2777
	.p2align 4,,10
	.p2align 3
.L3221:
	movq	(%r12), %rbx
	cmpq	$0, 8(%rbx)
	je	.L3226
	movq	%rax, %xmm0
	movq	%rbx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
.L2792:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L3187
.L3228:
	leaq	-1(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movq	8(%rbx,%rax,8), %r12
	jmp	.L2787
	.p2align 4,,10
	.p2align 3
.L3222:
	addl	%ecx, %ecx
	jne	.L2801
	addq	$4, %rsi
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L2801:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L3013
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L2801
	movq	-144(%rbp), %rax
	movq	%r10, -240(%rbp)
	leaq	-144(%rbp), %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-240(%rbp), %r10
	cltq
	addq	%rax, 96(%r13)
	jmp	.L3013
	.p2align 4,,10
	.p2align 3
.L3226:
	movq	680(%r12), %rax
	leaq	640(%r12), %rdi
	leaq	680(%r12), %r13
	testq	%rax, %rax
	je	.L2783
	movq	%rdi, -240(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r12), %rbx
	movq	-240(%rbp), %rdi
	testq	%rbx, %rbx
	jne	.L2784
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L2783:
	movq	704(%r12), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L2788
	movq	696(%r12), %rbx
	cmpq	$0, 8(%rbx)
	je	.L3227
	movq	%rax, %xmm0
	movq	%rbx, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 696(%r12)
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jne	.L3228
.L3187:
	movq	%r14, %r10
	jmp	.L2793
	.p2align 4,,10
	.p2align 3
.L3223:
	movq	8(%r15), %rdi
	movq	%r10, %rdx
	xorl	%esi, %esi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	movq	-240(%rbp), %r10
	jmp	.L2803
.L3227:
	leaq	1376(%r12), %rdx
	leaq	1336(%r12), %r13
	movq	%rdx, -240(%rbp)
	movq	1376(%r12), %rax
	testq	%rax, %rax
	je	.L3187
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	1376(%r12), %rbx
	movq	-240(%rbp), %rdx
	testq	%rbx, %rbx
	jne	.L3229
	movq	%r13, %rdi
	movq	%r14, -200(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-200(%rbp), %r10
.L2793:
	movq	8(%r15), %r12
.L2777:
	addq	%r10, 48(%r15)
	movq	8(%r12), %rax
	movq	%r10, %rbx
	cmpq	$0, 8(%rax)
	jne	.L2991
	movq	(%r12), %rax
	cmpq	$0, 8(%rax)
	jne	.L2991
	movq	704(%r12), %rax
	cmpq	$0, 8(%rax)
	jne	.L2992
	movq	696(%r12), %rax
	cmpq	$0, 8(%rax)
	jne	.L2992
	movq	680(%r12), %rax
	testq	%rax, %rax
	je	.L2993
.L3211:
	movzbl	_ZN2v88internal23FLAG_concurrent_markingE(%rip), %edx
	jmp	.L2771
	.p2align 4,,10
	.p2align 3
.L2784:
	movq	(%rbx), %rax
	movq	%rax, 0(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2785
	movl	$528, %esi
	call	_ZdlPvm@PLT
.L2785:
	movq	%rbx, 8(%r12)
	jmp	.L2792
.L3213:
	movq	(%r15), %rax
	leaq	-144(%rbp), %r12
	movl	$9, %edx
	movq	%r12, %rdi
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginEE29trace_event_unique_atomic1110(%rip), %rax
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3230
.L2765:
	movq	$0, -192(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L3231
.L2767:
	movq	%r15, %rdi
	call	_ZN2v88internal18IncrementalMarking16FinalizeSweepingEv
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	movl	80(%r15), %eax
	jmp	.L2763
.L2825:
	leaq	-192(%rbp), %rcx
	movl	$32, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movl	$32, %r8d
	jmp	.L2864
.L2842:
	movq	-184(%rbp), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	je	.L2988
	movq	-176(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r10, -240(%rbp)
	leaq	1496(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	movq	-240(%rbp), %r10
.L2988:
	movzbl	7(%r10), %r8d
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	leal	0(,%r8,8), %ebx
	leaq	-192(%rbp), %r8
	movl	%ebx, %ecx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	movslq	%ebx, %r8
	jmp	.L2864
.L2814:
	leaq	-192(%rbp), %rcx
	movl	$16, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movl	$24, %r8d
	jmp	.L2864
.L2816:
	leaq	-192(%rbp), %rcx
	movl	$24, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movl	$24, %r8d
	jmp	.L2864
.L2819:
	movzbl	7(%r10), %r8d
	leaq	-192(%rbp), %rcx
	movl	$8, %esi
	movq	%r12, %rdi
	leal	0(,%r8,8), %ebx
	movl	%ebx, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movslq	%ebx, %r8
	jmp	.L2864
.L3231:
	movl	$9, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r14
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3232
.L2768:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2769
	movq	(%rdi), %rax
	call	*8(%rax)
.L2769:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2770
	movq	(%rdi), %rax
	call	*8(%rax)
.L2770:
	movl	$9, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%r13, -184(%rbp)
	movq	%rax, -176(%rbp)
	leaq	-184(%rbp), %rax
	movq	%rbx, -168(%rbp)
	movq	%rax, -192(%rbp)
	jmp	.L2767
.L3230:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3233
.L2766:
	movq	%r13, _ZZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginEE29trace_event_unique_atomic1110(%rip)
	jmp	.L2765
.L2826:
	movq	%r12, -144(%rbp)
	movzbl	7(%r10), %r8d
	movl	$16, %esi
	movq	%r12, %rdi
	leaq	-192(%rbp), %rcx
	movl	$40, %edx
	leal	0(,%r8,8), %ebx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	cmpb	$0, _ZN2v88internal26FLAG_stress_flush_bytecodeE(%rip)
	movl	$2, %esi
	jne	.L2978
	movzbl	_ZN2v88internal19FLAG_flush_bytecodeE(%rip), %esi
.L2978:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal18SharedFunctionInfo19ShouldFlushBytecodeENS0_17BytecodeFlushModeE
	testb	%al, %al
	jne	.L3234
	movq	-144(%rbp), %rax
	movq	7(%rax), %r12
	leaq	7(%rax), %rsi
	testb	$1, %r12b
	je	.L3209
	movq	%r12, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rdx
	andl	$64, %edx
	jne	.L3235
.L2983:
	movl	%r12d, %eax
	movq	16(%r13), %rdx
	movl	$1, %esi
	subl	%r13d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
.L2986:
	movl	(%rcx), %edx
	movl	%edx, %eax
	andl	%esi, %eax
	cmpl	%esi, %eax
	je	.L3209
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	jne	.L2986
	movq	-176(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L3209
	.p2align 4,,10
	.p2align 3
.L2827:
	movq	%r10, %rsi
	leaq	-192(%rbp), %r13
	leaq	-144(%rbp), %rdi
	movq	%r12, -144(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	%r13, %rcx
	movl	$40, %edx
	movq	%r12, %rdi
	movl	$8, %esi
	movl	%eax, %ebx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rdx
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %rcx
	movl	%ebx, %edx
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movslq	%ebx, %r8
	jmp	.L2864
.L2845:
	movl	31(%r12), %eax
	leaq	-192(%rbp), %r13
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%r13, %rdx
	leal	48(,%rax,8), %ebx
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %rdx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %rdx
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %rcx
	movl	%ebx, %edx
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	.p2align 4,,10
	.p2align 3
.L3209:
	movslq	%ebx, %r8
	jmp	.L2864
.L2811:
	leaq	-144(%rbp), %rdi
	movq	%r10, %rsi
	movq	%r12, -144(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	-192(%rbp), %rcx
	movl	$32, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	movl	%eax, %ebx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3209
.L2847:
	movq	-176(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%r10, -240(%rbp)
	movq	%r12, -144(%rbp)
	xorl	%r12d, %r12d
	movl	$40, %ebx
	leaq	2888(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi64EE4PushEiS2_
	movq	-144(%rbp), %rax
	movq	-240(%rbp), %r10
	movl	35(%rax), %esi
	testl	%esi, %esi
	jle	.L2909
	movq	%r14, -272(%rbp)
	movq	%r10, -256(%rbp)
	jmp	.L2890
.L3239:
	movq	(%rsi), %r8
	movq	-144(%rbp), %rax
	testb	$1, %r8b
	jne	.L3236
.L2896:
	addl	$1, %r12d
	addq	$16, %rbx
	cmpl	%r12d, 35(%rax)
	jle	.L3237
.L2890:
	leaq	-1(%rbx,%rax), %rsi
	movq	(%rsi), %r13
	movq	-144(%rbp), %rdi
	movq	%r13, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testb	$64, %al
	jne	.L3238
.L2892:
	movq	-144(%rbp), %rax
	movl	%r13d, %ecx
	movq	16(%r14), %rdx
	subl	%r14d, %ecx
	leaq	7(%rbx,%rax), %rsi
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	movl	(%rdx,%rax,4), %eax
	movl	$1, %edx
	sall	%cl, %edx
	testl	%eax, %edx
	jne	.L3239
	movq	(%rsi), %rdx
	movq	-144(%rbp), %rax
	testb	$1, %dl
	je	.L2896
	movq	%rdx, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rcx
	andl	$64, %ecx
	jne	.L3240
.L2905:
	movl	%edx, %ecx
	movq	16(%r9), %rsi
	subl	%r9d, %ecx
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	movl	(%rsi,%rax,4), %eax
	movl	$1, %esi
	sall	%cl, %esi
	testl	%eax, %esi
	je	.L2908
.L3204:
	movq	-144(%rbp), %rax
	jmp	.L2896
.L2841:
	movq	-184(%rbp), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	jne	.L3241
.L2926:
	movzbl	7(%r10), %r8d
	leaq	-192(%rbp), %r13
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%r13, %rcx
	movl	$8, %esi
	movq	%r10, -240(%rbp)
	leal	0(,%r8,8), %ebx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %r8
	movl	%ebx, %ecx
	movl	$48, %edx
	jmp	.L3207
.L2843:
	movslq	11(%r12), %r8
	jmp	.L2864
.L2844:
	leaq	-144(%rbp), %rdi
	movq	%r10, %rsi
	movq	%r12, -144(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movl	%eax, %r8d
	testb	$1, 9(%r13)
	je	.L2910
	leaq	88(%r13), %rax
	movq	%rax, -256(%rbp)
	movq	88(%r13), %r10
	leal	32768(%r10), %eax
	movl	%r10d, -240(%rbp)
	testl	%r10d, %r10d
	jne	.L2911
	movl	$16, -240(%rbp)
	movl	$32784, %eax
.L2911:
	cmpl	%eax, %r8d
	movl	-240(%rbp), %esi
	cmovle	%r8d, %eax
	movl	%eax, -272(%rbp)
	cmpl	%eax, %esi
	jge	.L2913
	cltq
	leaq	(%rbx,%rax), %r9
	movq	%rax, -280(%rbp)
	movslq	%esi, %rax
	addq	%rax, %rbx
	cmpq	%r9, %rbx
	jnb	.L2924
	movq	%rbx, %rax
	movq	%r12, -296(%rbp)
	movq	%r13, %rbx
	movq	%r9, %r12
	movl	%r8d, -248(%rbp)
	movq	%rax, %r13
	movq	%r10, -288(%rbp)
	jmp	.L2914
.L3011:
	addq	$8, %r13
	cmpq	%r13, %r12
	jbe	.L3242
.L2914:
	movq	0(%r13), %r11
	testb	$1, %r11b
	je	.L3011
	movq	%r11, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	andl	$64, %edx
	jne	.L3243
.L2919:
	movl	%r11d, %edx
	movl	$1, %esi
	subl	%eax, %edx
	movq	16(%rax), %rax
	movl	%edx, %ecx
	shrl	$8, %edx
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rax,%rdx,4), %rsi
	.p2align 4,,10
	.p2align 3
.L2922:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L3011
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L2922
	movq	-176(%rbp), %rax
	movq	%r11, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L3011
.L2836:
	movq	-184(%rbp), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	jne	.L3244
.L2931:
	movzbl	7(%r10), %r8d
	leaq	-192(%rbp), %r13
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%r13, %rcx
	movl	$8, %esi
	movq	%r10, -240(%rbp)
	leal	0(,%r8,8), %ebx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rdx
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %r8
	movl	%ebx, %ecx
	movl	$72, %edx
	movq	-240(%rbp), %r10
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	movslq	%ebx, %r8
	jmp	.L2864
.L2849:
	leaq	-144(%rbp), %rbx
	movq	%r10, %rsi
	movq	%r12, -144(%rbp)
	movq	%rbx, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	15(%r12), %rsi
	movl	%eax, -240(%rbp)
	leaq	23(%r12), %rax
	cmpq	%rax, %rsi
	jnb	.L3010
	movq	15(%r12), %r8
	testb	$1, %r8b
	jne	.L3245
.L3010:
	movswl	9(%r12), %ecx
	movl	-160(%rbp), %esi
	movq	%rbx, %rdi
	movq	%r12, -144(%rbp)
	movl	%ecx, %edx
	movl	%ecx, %r13d
	movl	%ecx, -272(%rbp)
	call	_ZN2v88internal15DescriptorArray31UpdateNumberOfMarkedDescriptorsEjs@PLT
	movl	-272(%rbp), %ecx
	cmpw	%r13w, %ax
	jl	.L2875
.L3206:
	movslq	-240(%rbp), %r8
	jmp	.L2864
.L2810:
	movzbl	7(%r10), %r8d
	leaq	-192(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r10, %rdi
	leal	0(,%r8,8), %ebx
	movl	%ebx, %edx
	call	_ZN2v88internal18WasmInstanceObject14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_
	movslq	%ebx, %r8
	jmp	.L2864
.L2809:
	movq	%r10, %rsi
	leaq	-144(%rbp), %rdi
	movq	%r12, -144(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	-192(%rbp), %rcx
	movl	$8, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	movl	%eax, %edx
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movslq	%ebx, %r8
	jmp	.L2864
.L2807:
	movq	15(%r12), %rax
	leaq	15(%r12), %r8
	testb	$1, %al
	jne	.L3246
.L2933:
	movzbl	7(%r10), %r8d
	leaq	-192(%rbp), %r13
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%r13, %rcx
	movl	$8, %esi
	leal	0(,%r8,8), %ebx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rcx
	movl	%ebx, %edx
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movslq	%ebx, %r8
	jmp	.L2864
.L2850:
	movq	%r10, %rsi
	leaq	-144(%rbp), %rdi
	leaq	-192(%rbp), %r13
	movq	%r12, -144(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	%r13, %rcx
	movl	$24, %edx
	movq	%r12, %rdi
	movl	%eax, %ebx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rcx
	movl	%ebx, %edx
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movslq	%ebx, %r8
	jmp	.L2864
.L2828:
	leaq	-192(%rbp), %rcx
	movl	$40, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movl	$40, %r8d
	jmp	.L2864
.L2830:
	movl	7(%r12), %eax
	leaq	-192(%rbp), %rcx
	movq	%r12, %rdi
	leal	23(%rax), %esi
	movl	11(%r12), %eax
	andl	$-8, %esi
	leal	(%rsi,%rax,8), %ebx
	movl	%ebx, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movslq	%ebx, %r8
	jmp	.L2864
.L2831:
	leaq	-192(%rbp), %rcx
	movl	$48, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movl	$48, %r8d
	jmp	.L2864
.L2832:
	leaq	-192(%rbp), %rcx
	movl	$1944, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movl	$1976, %r8d
	jmp	.L2864
.L2833:
	cmpw	$1024, 11(%r12)
	ja	.L3247
.L2943:
	leaq	-192(%rbp), %r13
	movl	$72, %edx
	movl	$24, %esi
	movq	%r12, %rdi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rdx
	movl	$72, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movl	$80, %r8d
	jmp	.L2864
.L2813:
	leaq	-192(%rbp), %r13
	movl	$16, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rcx
	movl	$32, %edx
	movq	%r12, %rdi
	movl	$24, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movl	$32, %r8d
	jmp	.L2864
.L2835:
	movq	23(%r12), %rax
	leaq	23(%r12), %r8
	testb	$1, %al
	jne	.L3248
.L2938:
	movzbl	7(%r10), %r8d
	leaq	-192(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r10, %rdi
	leal	0(,%r8,8), %ebx
	movl	%ebx, %edx
	call	_ZN2v88internal9JSWeakRef14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_.constprop.0
	movslq	%ebx, %r8
	jmp	.L2864
.L2817:
	leaq	-192(%rbp), %r13
	movl	$48, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rcx
	movl	$72, %edx
	movq	%r12, %rdi
	movl	$56, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movl	$72, %r8d
	jmp	.L2864
.L2853:
	leaq	-192(%rbp), %rcx
	movl	$8, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movl	$24, %r8d
	jmp	.L2864
.L2854:
	movl	39(%r12), %ebx
	testb	$1, 43(%r12)
	je	.L2866
	leal	71(%rbx), %eax
	andl	$-8, %eax
	cltq
	addq	%r12, %rax
	movslq	-1(%rax), %rdx
	leaq	7(%rax,%rdx), %rbx
	leaq	63(%r12), %rax
	subl	%eax, %ebx
.L2866:
	leaq	-192(%rbp), %r13
	movl	$40, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	leaq	-144(%rbp), %r8
	movl	$1999, %edx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -240(%rbp)
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	movq	-240(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE@PLT
	leal	7(%rbx), %r8d
	andl	$-8, %r8d
	addl	$95, %r8d
	andl	$-32, %r8d
	movslq	%r8d, %r8
	jmp	.L2864
.L2855:
	leaq	-192(%rbp), %rcx
	movl	$16, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movl	$16, %r8d
	jmp	.L2864
.L2821:
	movzbl	17(%r12), %r13d
	leaq	-192(%rbp), %rcx
	movl	$24, %esi
	movq	%r12, %rdi
	leal	(%r13,%r13), %ebx
	leal	(%rbx,%r13), %edx
	sall	$4, %edx
	addl	$24, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	leal	3(%rbx,%rbx,2), %eax
	leal	0(%r13,%rax,8), %eax
	leal	7(%rbx,%rax), %r8d
	andl	$262136, %r8d
	jmp	.L2864
.L2822:
	movzbl	9(%r12), %r13d
	leaq	-192(%rbp), %rcx
	movl	$16, %esi
	movq	%r12, %rdi
	leal	1(%r13), %ebx
	sall	$4, %ebx
	movl	%ebx, %edx
	addl	%r13d, %ebx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	leal	7(%rbx,%r13,2), %r8d
	andl	$65528, %r8d
	jmp	.L2864
.L2823:
	movzbl	9(%r12), %ebx
	leaq	-192(%rbp), %rcx
	movl	$16, %esi
	movq	%r12, %rdi
	movl	%ebx, %edx
	leal	(%rbx,%rbx), %r13d
	sall	$5, %edx
	addl	$16, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	leal	1(%r13), %eax
	sall	$4, %eax
	addl	%ebx, %eax
	leal	7(%r13,%rax), %r8d
	andl	$131064, %r8d
	jmp	.L2864
.L2815:
	movq	%r10, %rsi
	leaq	-144(%rbp), %rdi
	movq	%r12, -144(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	-192(%rbp), %rcx
	movl	$8, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	movl	%eax, %ebx
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	-176(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	2192(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_15TransitionArrayELi64EE4PushEiS2_
	movslq	%ebx, %r8
	jmp	.L2864
.L2839:
	movq	%r12, -144(%rbp)
	movzbl	7(%r10), %r8d
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	leal	0(,%r8,8), %ebx
	leaq	-192(%rbp), %r8
	movl	%ebx, %ecx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	cmpb	$0, _ZN2v88internal19FLAG_flush_bytecodeE(%rip)
	je	.L3209
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv
	testb	%al, %al
	je	.L3209
	movq	-176(%rbp), %rax
	movq	-144(%rbp), %rdx
	xorl	%esi, %esi
	leaq	9152(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10JSFunctionELi64EE4PushEiS2_
	jmp	.L3209
.L2840:
	movq	-184(%rbp), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	jne	.L3249
.L2927:
	movzbl	7(%r10), %r8d
	leaq	-192(%rbp), %r13
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%r13, %rcx
	movl	$8, %esi
	movq	%r10, -240(%rbp)
	leal	0(,%r8,8), %ebx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %r8
	movl	%ebx, %ecx
	movl	$56, %edx
.L3207:
	movq	-240(%rbp), %r10
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L3209
.L2820:
	leaq	-192(%rbp), %r13
	movl	$48, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rcx
	movl	$112, %edx
	movq	%r12, %rdi
	movl	$48, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movl	$112, %r8d
	jmp	.L2864
.L2861:
	movzbl	7(%r10), %r8d
	salq	$3, %r8
	andl	$2040, %r8d
	jmp	.L2864
.L2862:
	movq	7(%r12), %r8
	sarq	$32, %r8
	addl	$23, %r8d
	andl	$-8, %r8d
	movslq	%r8d, %r8
	jmp	.L2864
.L2863:
	movl	7(%r12), %eax
	shrl	%eax
	leal	16(,%rax,8), %r8d
	movslq	%r8d, %r8
	jmp	.L2864
.L2857:
	movzbl	7(%r10), %ebx
	leaq	-192(%rbp), %rcx
	movl	$32, %edx
	movq	%r12, %rdi
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	leaq	0(,%rbx,8), %r8
	andl	$2040, %r8d
	jmp	.L2864
.L2858:
	movl	11(%r12), %eax
	leal	23(%rax,%rax), %r8d
	andl	$-8, %r8d
	movslq	%r8d, %r8
	jmp	.L2864
.L2859:
	movl	11(%r12), %r8d
	addl	$23, %r8d
	andl	$-8, %r8d
	movslq	%r8d, %r8
	jmp	.L2864
.L2860:
	movq	7(%r12), %rax
	sarq	$32, %rax
	leal	16(,%rax,8), %r8d
	movslq	%r8d, %r8
	jmp	.L2864
.L2856:
	movq	%r12, -144(%rbp)
	movq	7(%r12), %r8
	movl	$16, %esi
	leaq	-192(%rbp), %r13
	movq	%r13, %rdx
	sarq	$32, %r8
	addl	$61, %r8d
	movl	%r8d, %ebx
	movq	-144(%rbp), %r12
	andl	$-8, %ebx
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %rdx
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %rdx
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	-184(%rbp), %rax
	cmpb	$0, 2764(%rax)
	jne	.L3209
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal13BytecodeArray9MakeOlderEv@PLT
	jmp	.L3209
.L2992:
	cmpb	$0, _ZN2v88internal23FLAG_concurrent_markingE(%rip)
	je	.L3004
	jmp	.L3006
	.p2align 4,,10
	.p2align 3
.L2991:
	cmpb	$0, _ZN2v88internal23FLAG_concurrent_markingE(%rip)
	je	.L3004
	jmp	.L3005
.L3238:
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2893
	testb	$-128, %ah
	je	.L2892
.L2893:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2892
.L3220:
	call	_ZNK2v88internal8GCTracer44IncrementalMarkingSpeedInBytesPerMillisecondEv@PLT
	leaq	-37592(%rbx), %rdi
	movl	$1, %eax
	leaq	.LC37(%rip), %rsi
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movq	(%r15), %rax
	movq	2008(%rax), %rdi
	jmp	.L2776
.L3219:
	movq	248(%rbx), %rax
	movq	104(%rax), %rdx
	movq	%rdx, 192(%rax)
	movq	(%r15), %rax
	movq	296(%rax), %rax
	movq	$0, 128(%rax)
	mfence
	movq	8(%r15), %r12
	leaq	1336(%r12), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	1376(%r12), %r13
	testq	%r13, %r13
	je	.L3202
	movq	$0, 1376(%r12)
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L2775:
	movq	%rax, %rbx
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L2775
	leaq	640(%r12), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r12), %rax
	movq	%rax, (%rbx)
	movq	%r13, 680(%r12)
.L3202:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%r15), %rbx
	jmp	.L2772
.L3236:
	movq	%r8, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rcx
	andl	$64, %ecx
	jne	.L3250
.L2898:
	movl	%r8d, %eax
	movq	16(%r13), %rdx
	movl	$1, %esi
	subl	%r13d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L2902:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L3204
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L2902
	movq	-176(%rbp), %rax
	movq	%r8, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	movq	-144(%rbp), %rax
	jmp	.L2896
.L3229:
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	%rax, (%rdx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	704(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2796
	movl	$528, %esi
	call	_ZdlPvm@PLT
.L2796:
	movq	%rbx, 704(%r12)
	jmp	.L2792
.L3237:
	movq	-272(%rbp), %r14
	movq	-256(%rbp), %r10
.L2909:
	leaq	-144(%rbp), %rdi
	movq	%r10, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movslq	%eax, %r8
	jmp	.L2864
.L2910:
	movl	%eax, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	movl	%eax, -240(%rbp)
	leaq	-192(%rbp), %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3206
.L3234:
	movq	-176(%rbp), %rax
	movq	-144(%rbp), %rdx
	xorl	%esi, %esi
	leaq	8456(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_18SharedFunctionInfoELi64EE4PushEiS2_
	jmp	.L3209
.L2806:
	leaq	.LC33(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3246:
	movq	%rax, %rsi
	andl	$262143, %eax
	movl	$1, %ebx
	andq	$-262144, %rsi
	movl	%eax, %edx
	shrl	$3, %eax
	movq	16(%rsi), %rcx
	shrl	$8, %edx
	movl	(%rcx,%rdx,4), %edx
	movl	%eax, %ecx
	sall	%cl, %ebx
	testl	%edx, %ebx
	je	.L2934
	movq	8(%rsi), %rax
	testb	$64, %al
	je	.L2933
	movq	8(%r13), %rax
	testb	$88, %al
	je	.L2936
	testb	$-128, %ah
	je	.L2933
.L2936:
	movq	%r8, %rsi
	movq	%r13, %rdi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-240(%rbp), %r10
	jmp	.L2933
.L3245:
	movq	%r8, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L2868
.L2872:
	movl	%r8d, %eax
	movl	$1, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
	jmp	.L2869
.L3252:
	movl	%esi, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L3251
.L2869:
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%esi, %eax
	jne	.L3252
	jmp	.L3010
.L3248:
	movq	%rax, %rsi
	andl	$262143, %eax
	movl	$1, %ebx
	andq	$-262144, %rsi
	movl	%eax, %edx
	shrl	$3, %eax
	movq	16(%rsi), %rcx
	shrl	$8, %edx
	movl	(%rcx,%rdx,4), %edx
	movl	%eax, %ecx
	sall	%cl, %ebx
	testl	%edx, %ebx
	je	.L2939
	movq	8(%rsi), %rax
	testb	$64, %al
	je	.L2938
	movq	8(%r13), %rax
	testb	$88, %al
	je	.L2941
	testb	$-128, %ah
	je	.L2938
.L2941:
	movq	%r8, %rsi
	movq	%r13, %rdi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-240(%rbp), %r10
	jmp	.L2938
.L3247:
	movq	39(%r12), %r13
	movl	$1, %esi
	movq	%r13, %rbx
	movl	%r13d, %eax
	andl	$262143, %eax
	andq	$-262144, %rbx
	movl	%eax, %ecx
	movq	16(%rbx), %rdx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
	jmp	.L2947
.L2944:
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L2948
.L2947:
	movl	(%rcx), %edx
	movl	%edx, %eax
	andl	%esi, %eax
	cmpl	%esi, %eax
	jne	.L2944
.L2948:
	leaq	-1(%r13), %rax
	movl	$1, %edx
	movq	%r13, -144(%rbp)
	subl	%ebx, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%rbx), %rcx
	leaq	(%rcx,%rax,4), %rcx
	movl	(%rcx), %eax
	testl	%edx, %eax
	jne	.L3253
.L2953:
	movl	15(%r12), %ebx
	shrl	$10, %ebx
	andl	$1023, %ebx
	je	.L2943
	movl	-160(%rbp), %esi
	leaq	-144(%rbp), %rdi
	movl	%ebx, %edx
	movq	%r13, -144(%rbp)
	call	_ZN2v88internal15DescriptorArray31UpdateNumberOfMarkedDescriptorsEjs@PLT
	cmpw	%bx, %ax
	jge	.L2943
	cwtl
	leal	3(%rbx,%rbx,2), %edx
	movq	-144(%rbp), %r13
	leal	3(%rax,%rax,2), %ebx
	sall	$3, %edx
	sall	$3, %ebx
	leaq	-1(%r13), %rcx
	movslq	%edx, %rdx
	movslq	%ebx, %rbx
	leaq	(%rdx,%rcx), %rsi
	addq	%rcx, %rbx
	movq	%rsi, -240(%rbp)
	cmpq	%rsi, %rbx
	jnb	.L2943
	movq	%r12, -256(%rbp)
	jmp	.L2977
.L2969:
	addq	$8, %rbx
	cmpq	%rbx, -240(%rbp)
	jbe	.L3254
.L2977:
	movq	(%rbx), %r12
	movq	%r12, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L3255
	cmpq	$3, %rax
	jne	.L2969
	cmpl	$3, %r12d
	je	.L2969
	movq	%r12, %rdx
	movq	%r12, %rax
	movl	$1, %edi
	andq	$-262144, %rdx
	andq	$-3, %rax
	subl	%edx, %eax
	movq	16(%rdx), %rsi
	movl	%eax, %ecx
	shrl	$3, %eax
	shrl	$8, %ecx
	movl	(%rsi,%rcx,4), %esi
	movl	%eax, %ecx
	sall	%cl, %edi
	testl	%esi, %edi
	je	.L2975
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L2969
	movq	%r13, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2976
	testb	$-128, %ah
	je	.L2969
.L2976:
	movq	%rbx, %rsi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2969
	.p2align 4,,10
	.p2align 3
.L2875:
	cwtl
	movq	-144(%rbp), %r13
	leal	3(%rcx,%rcx,2), %r12d
	leal	3(%rax,%rax,2), %r8d
	sall	$3, %r12d
	sall	$3, %r8d
	leaq	-1(%r13), %rdx
	movslq	%r12d, %r12
	movslq	%r8d, %r8
	addq	%rdx, %r12
	leaq	(%r8,%rdx), %rbx
	cmpq	%r12, %rbx
	jb	.L2889
	jmp	.L3206
	.p2align 4,,10
	.p2align 3
.L2881:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jbe	.L3206
.L2889:
	movq	(%rbx), %r8
	movq	%r8, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L3256
	cmpq	$3, %rax
	jne	.L2881
	cmpl	$3, %r8d
	je	.L2881
	movq	%r8, %rdx
	movq	%r8, %rax
	movl	$1, %edi
	andq	$-262144, %rdx
	andq	$-3, %rax
	subl	%edx, %eax
	movq	16(%rdx), %rsi
	movl	%eax, %ecx
	shrl	$3, %eax
	shrl	$8, %ecx
	movl	(%rsi,%rcx,4), %esi
	movl	%eax, %ecx
	sall	%cl, %edi
	testl	%esi, %edi
	je	.L2887
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L2881
	movq	%r13, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2888
	testb	$-128, %ah
	je	.L2881
.L2888:
	movq	%rbx, %rsi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2881
.L3244:
	movq	-176(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r10, -240(%rbp)
	leaq	1496(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	movq	-240(%rbp), %r10
	jmp	.L2931
.L3241:
	movq	-176(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r10, -240(%rbp)
	leaq	1496(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	movq	-240(%rbp), %r10
	jmp	.L2926
.L3249:
	movq	-176(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r10, -240(%rbp)
	leaq	1496(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	movq	-240(%rbp), %r10
	jmp	.L2927
.L3218:
	movq	(%r12), %rax
	cmpq	$0, 8(%rax)
	jne	.L3257
.L3007:
	movq	8(%r12), %r13
	cmpq	$0, 8(%r13)
	je	.L3006
	movq	%r14, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r12), %rax
	movq	-200(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, (%rsi)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 8(%r12)
	jmp	.L3006
.L3256:
	movq	%r8, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L3258
.L2879:
	movl	%r8d, %eax
	movl	$1, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
.L2885:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L2881
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L2885
	movq	-176(%rbp), %rax
	movq	%r8, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L2881
.L3250:
	andq	$-262144, %rax
	movq	%rax, %rdi
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L2899
	testb	$-128, %ah
	je	.L2898
.L2899:
	movq	%r8, -240(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-240(%rbp), %r8
	jmp	.L2898
.L3242:
	movl	-248(%rbp), %r8d
	movq	-288(%rbp), %r10
	movq	-296(%rbp), %r12
.L2924:
	movq	-256(%rbp), %rbx
	movq	-280(%rbp), %rsi
	movq	%r10, %rax
	lock cmpxchgq	%rsi, (%rbx)
	jne	.L3259
	cmpl	-272(%rbp), %r8d
	jle	.L2913
	movq	-176(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
.L2913:
	movl	-272(%rbp), %r8d
	subl	-240(%rbp), %r8d
	movslq	%r8d, %r8
	jmp	.L2864
.L3240:
	andq	$-262144, %rax
	movq	%rax, %rdi
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L2906
	testb	$-128, %ah
	je	.L2905
.L2906:
	movq	%r9, -280(%rbp)
	movq	%rdx, -240(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-280(%rbp), %r9
	movq	-240(%rbp), %rdx
	jmp	.L2905
.L2908:
	movq	%rdx, %xmm5
	movq	%r13, %xmm6
	movq	-176(%rbp), %rax
	xorl	%esi, %esi
	punpcklqdq	%xmm5, %xmm6
	movaps	%xmm6, -240(%rbp)
	movq	-240(%rbp), %rdx
	movq	-232(%rbp), %rcx
	leaq	4976(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE4PushEiS2_
	jmp	.L3204
.L2887:
	movq	%r13, %xmm5
	movq	%rbx, %xmm6
	movq	-176(%rbp), %rax
	xorl	%esi, %esi
	punpcklqdq	%xmm6, %xmm5
	movaps	%xmm5, -272(%rbp)
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rcx
	leaq	5672(%rax), %rdi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_
	jmp	.L2881
.L3257:
	movq	%r14, %rdi
	movq	%rax, -240(%rbp)
	movq	%rsi, -200(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r12), %rdx
	movq	-200(%rbp), %rsi
	movq	%r14, %rdi
	movq	-240(%rbp), %rax
	movq	%rdx, (%rax)
	movq	%rax, (%rsi)
	movq	%rsi, -200(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	-200(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	movq	%r13, %rax
	rep stosq
	movq	%rdx, (%r12)
	jmp	.L3007
.L3243:
	movq	8(%rbx), %rdx
	testb	$88, %dl
	je	.L2920
	andb	$-128, %dh
	je	.L2919
.L2920:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, -312(%rbp)
	movq	%r11, -304(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-312(%rbp), %rax
	movq	-304(%rbp), %r11
	jmp	.L2919
.L3233:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L2766
.L3258:
	movq	%r13, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2882
	testb	$-128, %ah
	je	.L2879
.L2882:
	movq	%rbx, %rsi
	movq	%rdx, -256(%rbp)
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-256(%rbp), %rdx
	movq	-272(%rbp), %r8
	jmp	.L2879
.L3232:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r14, %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L2768
.L2934:
	movq	-176(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r10, -240(%rbp)
	leaq	7760(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_8WeakCellELi64EE4PushEiS2_
	movq	-240(%rbp), %r10
	jmp	.L2933
.L2939:
	movq	-176(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r10, -240(%rbp)
	leaq	7064(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_9JSWeakRefELi64EE4PushEiS2_
	movq	-240(%rbp), %r10
	jmp	.L2938
.L3251:
	movq	-176(%rbp), %rax
	movq	%r8, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L3010
.L2868:
	movq	8(%r13), %rax
	testb	$88, %al
	je	.L2874
	testb	$-128, %ah
	je	.L2872
.L2874:
	movq	%r13, %rdi
	movq	%rdx, -256(%rbp)
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-272(%rbp), %r8
	movq	-256(%rbp), %rdx
	jmp	.L2872
.L3253:
	addl	%edx, %edx
	jne	.L2950
	addq	$4, %rcx
	movl	$1, %edx
.L2950:
	movl	(%rcx), %esi
	movl	%edx, %eax
	andl	%esi, %eax
	cmpl	%eax, %edx
	je	.L2953
	movl	%edx, %edi
	movl	%esi, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %esi
	jne	.L2950
	leaq	-144(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject4SizeEv
	leaq	15(%r13), %rsi
	cltq
	addq	%rax, 96(%rbx)
	leaq	23(%r13), %rax
	cmpq	%rax, %rsi
	jnb	.L2953
	movq	15(%r13), %rdx
	testb	$1, %dl
	je	.L2953
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	testb	$64, %al
	jne	.L2954
.L2959:
	movl	%edx, %eax
	movl	$1, %edi
	subl	%r8d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edi
	movq	16(%r8), %rcx
	leaq	(%rcx,%rax,4), %rsi
.L2956:
	movl	(%rsi), %ecx
	movl	%ecx, %eax
	andl	%edi, %eax
	cmpl	%edi, %eax
	je	.L2953
	movl	%ecx, %r8d
	movl	%ecx, %eax
	orl	%edi, %r8d
	lock cmpxchgl	%r8d, (%rsi)
	cmpl	%eax, %ecx
	jne	.L2956
	movq	-176(%rbp), %rax
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L2953
	.p2align 4,,10
	.p2align 3
.L3255:
	movq	%r12, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L3260
.L2967:
	movl	%r12d, %eax
	movl	$1, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
.L2973:
	movl	(%rcx), %edx
	movl	%edx, %eax
	andl	%esi, %eax
	cmpl	%esi, %eax
	je	.L2969
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	jne	.L2973
	movq	-176(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L2969
.L3235:
	andq	$-262144, %rax
	movq	%rax, %rdi
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L2984
	testb	$-128, %ah
	je	.L2983
.L2984:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2983
.L3254:
	movq	-256(%rbp), %r12
	jmp	.L2943
.L2993:
	movq	1376(%r12), %rax
	testq	%rax, %rax
	jne	.L3211
	cmpb	$0, _ZN2v88internal33FLAG_incremental_marking_wrappersE(%rip)
	movq	(%r15), %rsi
	je	.L2995
	movq	2128(%rsi), %r12
	cmpq	$0, 8(%r12)
	je	.L2995
	movq	%r12, %rdi
	call	_ZN2v88internal23LocalEmbedderHeapTracer19IsRemoteTracingDoneEv@PLT
	movq	(%r15), %rsi
	testb	%al, %al
	je	.L2997
	cmpb	$0, 28(%r12)
	jne	.L2995
.L2997:
	cmpq	$3, 16(%r12)
	ja	.L2995
	movq	2128(%rsi), %rax
	movzbl	_ZN2v88internal23FLAG_concurrent_markingE(%rip), %edx
	movl	$0, -204(%rbp)
	addq	$1, 16(%rax)
	jmp	.L2771
.L2995:
	cmpb	$0, 88(%r15)
	movzbl	_ZN2v88internal30FLAG_trace_incremental_markingE(%rip), %eax
	jne	.L2998
	testb	%al, %al
	jne	.L3261
.L2999:
	movl	-244(%rbp), %ecx
	movl	$2, 92(%r15)
	testl	%ecx, %ecx
	je	.L3262
.L3000:
	movq	48(%r15), %rax
	cmpq	%rax, 56(%r15)
	jnb	.L3001
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movq	%rax, 56(%r15)
	jne	.L3263
.L3001:
	leaq	89(%r15), %rdi
	call	_ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE@PLT
	movzbl	_ZN2v88internal23FLAG_concurrent_markingE(%rip), %edx
	movl	$2, -204(%rbp)
	jmp	.L2771
.L2998:
	movl	$3, 80(%r15)
	movb	$1, 2736(%rsi)
	movb	$1, 85(%r15)
	testb	%al, %al
	jne	.L3264
.L3002:
	movl	-244(%rbp), %edx
	movl	$1, 92(%r15)
	testl	%edx, %edx
	je	.L3003
.L3210:
	movl	$2, -204(%rbp)
	movzbl	_ZN2v88internal23FLAG_concurrent_markingE(%rip), %edx
	jmp	.L2771
.L3262:
	leaq	-80(%rsi), %rdi
	movl	$2, %esi
	call	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE@PLT
	movq	(%r15), %rsi
	jmp	.L3000
.L3261:
	leaq	-37592(%rsi), %rdi
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movq	(%r15), %rsi
	jmp	.L2999
.L3003:
	movq	(%r15), %rax
	movl	$2, %esi
	leaq	-80(%rax), %rdi
	call	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE@PLT
	jmp	.L3210
.L3264:
	movq	(%r15), %rax
	leaq	.LC18(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L3002
.L3263:
	leaq	-37592(%rsi), %rdi
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movq	(%r15), %rsi
	jmp	.L3001
.L2954:
	movq	8(%rbx), %rax
	testb	$88, %al
	je	.L2961
	testb	$-128, %ah
	je	.L2959
.L2961:
	movq	%rbx, %rdi
	movq	%rdx, -272(%rbp)
	movq	%r8, -240(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-240(%rbp), %r8
	movq	-272(%rbp), %rdx
	jmp	.L2959
.L3260:
	movq	%r13, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L2970
	testb	$-128, %ah
	je	.L2967
.L2970:
	movq	%rbx, %rsi
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-272(%rbp), %rdx
	jmp	.L2967
.L2975:
	movq	%r13, %xmm7
	movq	%rbx, %xmm5
	movq	-176(%rbp), %rax
	xorl	%esi, %esi
	punpcklqdq	%xmm5, %xmm7
	movaps	%xmm7, -272(%rbp)
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rcx
	leaq	5672(%rax), %rdi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_
	jmp	.L2969
.L3217:
	call	__stack_chk_fail@PLT
.L3259:
	leaq	.LC32(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22429:
	.size	_ZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginE, .-_ZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginE
	.section	.rodata._ZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginE.str1.1,"aMS",@progbits,1
.LC39:
	.string	"V8.GCIncrementalMarking"
	.section	.text._ZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginE
	.type	_ZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginE, @function
_ZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginE:
.LFB22420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$216, %rsp
	movsd	%xmm0, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	3368(%rax), %r13
	movq	2352(%r13), %rax
	leaq	2320(%r13), %rdi
	leaq	2368(%r13), %rsi
	movq	%rdi, -232(%rbp)
	movq	16(%rax), %rdx
	movq	%rsi, -240(%rbp)
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	_ZZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginEE28trace_event_unique_atomic963(%rip), %rdx
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L3339
.L3267:
	movq	$0, -208(%rbp)
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L3340
.L3269:
	movq	(%r15), %rax
	xorl	%edx, %edx
	movq	2008(%rax), %rsi
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginEE28trace_event_unique_atomic964(%rip), %rdx
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L3341
.L3274:
	movq	$0, -176(%rbp)
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L3342
.L3276:
	movq	(%r15), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movsd	64(%r15), %xmm1
	movsd	.LC22(%rip), %xmm2
	addsd	%xmm1, %xmm2
	comisd	%xmm0, %xmm2
	ja	.L3343
	movapd	%xmm0, %xmm3
	movsd	.LC21(%rip), %xmm2
	subsd	%xmm1, %xmm3
	comisd	%xmm3, %xmm2
	movapd	%xmm3, %xmm1
	ja	.L3344
	movsd	.LC20(%rip), %xmm3
	movq	32(%r15), %rcx
	movapd	%xmm2, %xmm1
	movsd	%xmm0, 64(%r15)
	movsd	%xmm3, -224(%rbp)
	testq	%rcx, %rcx
	js	.L3285
.L3350:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
.L3286:
	mulsd	%xmm3, %xmm0
	movsd	.LC23(%rip), %xmm2
	comisd	%xmm2, %xmm0
	jnb	.L3287
	cvttsd2siq	%xmm0, %rdx
.L3288:
	movq	%rdx, %rax
	movq	$-1, %rsi
	addq	56(%r15), %rax
	cmovc	%rsi, %rax
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movq	%rax, 56(%r15)
	jne	.L3345
.L3282:
	shrq	$2, %rcx
	movq	48(%r15), %rax
	leaq	(%rcx,%rcx,2), %rdx
	cmpq	%rdx, %rax
	jbe	.L3300
	cmpq	56(%r15), %rax
	jbe	.L3300
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movq	%rax, 56(%r15)
	jne	.L3346
	.p2align 4,,10
	.p2align 3
.L3300:
	movq	.LC40(%rip), %rax
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	movq	%rax, %xmm0
	call	_ZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginE
	movq	(%r15), %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movsd	-216(%rbp), %xmm4
	xorl	%eax, %eax
	cmpl	$2, 80(%r15)
	movsd	-224(%rbp), %xmm5
	movq	(%r15), %rdi
	subsd	%xmm0, %xmm4
	minsd	%xmm4, %xmm5
	movapd	%xmm5, %xmm0
	je	.L3347
.L3296:
	cmpl	$1, %r14d
	je	.L3297
	testb	%al, %al
	jne	.L3297
	cmpl	$2, %r14d
	je	.L3348
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	xorl	%r14d, %r14d
.L3299:
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-248(%rbp), %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	2352(%r13), %rax
	movq	-240(%rbp), %rsi
	movq	-232(%rbp), %rdi
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3349
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3344:
	.cfi_restore_state
	movsd	.LC20(%rip), %xmm7
	movq	32(%r15), %rcx
	divsd	%xmm2, %xmm3
	movsd	%xmm0, 64(%r15)
	movsd	%xmm7, -224(%rbp)
	testq	%rcx, %rcx
	jns	.L3350
.L3285:
	movq	%rcx, %rax
	movq	%rcx, %rdx
	pxor	%xmm0, %xmm0
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L3286
	.p2align 4,,10
	.p2align 3
.L3297:
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movsd	-216(%rbp), %xmm6
	subsd	%xmm0, %xmm6
	comisd	-224(%rbp), %xmm6
	jnb	.L3300
	movl	$1, %r14d
	jmp	.L3299
	.p2align 4,,10
	.p2align 3
.L3347:
	movzbl	_ZN2v88internal33FLAG_incremental_marking_wrappersE(%rip), %eax
	testb	%al, %al
	je	.L3296
	movq	2128(%rdi), %rdx
	xorl	%eax, %eax
	cmpq	$0, 8(%rdx)
	je	.L3296
	movq	%r15, %rdi
	call	_ZN2v88internal18IncrementalMarking12EmbedderStepEd.part.0
	movq	(%r15), %rdi
	andl	$1, %eax
	jmp	.L3296
	.p2align 4,,10
	.p2align 3
.L3348:
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	jmp	.L3299
	.p2align 4,,10
	.p2align 3
.L3343:
	movsd	.LC20(%rip), %xmm1
	movq	32(%r15), %rcx
	movsd	%xmm1, -224(%rbp)
	jmp	.L3282
	.p2align 4,,10
	.p2align 3
.L3287:
	subsd	%xmm2, %xmm0
	cvttsd2siq	%xmm0, %rdx
	btcq	$63, %rdx
	jmp	.L3288
	.p2align 4,,10
	.p2align 3
.L3342:
	xorl	%edi, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -256(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	$0, -224(%rbp)
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L3351
.L3277:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3278
	movq	(%rdi), %rax
	call	*8(%rax)
.L3278:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3279
	movq	(%rdi), %rax
	call	*8(%rax)
.L3279:
	xorl	%edi, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%r14, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	-224(%rbp), %rax
	movq	%rax, -152(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rax, -176(%rbp)
	jmp	.L3276
	.p2align 4,,10
	.p2align 3
.L3341:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3352
.L3275:
	movq	%r14, _ZZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginEE28trace_event_unique_atomic964(%rip)
	jmp	.L3274
	.p2align 4,,10
	.p2align 3
.L3340:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	$0, -224(%rbp)
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L3353
.L3270:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3271
	movq	(%rdi), %rax
	call	*8(%rax)
.L3271:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3272
	movq	(%rdi), %rax
	call	*8(%rax)
.L3272:
	leaq	.LC39(%rip), %rax
	movq	%r14, -200(%rbp)
	movq	%rax, -192(%rbp)
	movq	-224(%rbp), %rax
	movq	%rax, -184(%rbp)
	leaq	-200(%rbp), %rax
	movq	%rax, -208(%rbp)
	jmp	.L3269
	.p2align 4,,10
	.p2align 3
.L3339:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3354
.L3268:
	movq	%r14, _ZZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginEE28trace_event_unique_atomic963(%rip)
	jmp	.L3267
	.p2align 4,,10
	.p2align 3
.L3345:
	movq	(%r15), %rax
	shrq	$10, %rdx
	movapd	%xmm1, %xmm0
	leaq	.LC24(%rip), %rsi
	leaq	-37592(%rax), %rdi
	movl	$1, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movq	32(%r15), %rcx
	jmp	.L3282
.L3346:
	movq	(%r15), %rax
	leaq	.LC19(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L3300
.L3354:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L3268
.L3353:
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC39(%rip), %rcx
	movq	%r14, %rdx
	movl	$88, %esi
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	movq	%rax, -224(%rbp)
	addq	$64, %rsp
	jmp	.L3270
.L3352:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L3275
.L3351:
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	-256(%rbp), %rcx
	movq	%r14, %rdx
	movl	$88, %esi
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	movq	%rax, -224(%rbp)
	addq	$64, %rsp
	jmp	.L3277
.L3349:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22420:
	.size	_ZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginE, .-_ZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginE
	.section	.text._ZN2v88internal18IncrementalMarking19AdvanceOnAllocationEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal18IncrementalMarking19AdvanceOnAllocationEv.part.0, @function
_ZN2v88internal18IncrementalMarking19AdvanceOnAllocationEv.part.0:
.LFB28967:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	3368(%rax), %rbx
	movq	2352(%rbx), %rax
	leaq	2320(%rbx), %r13
	leaq	2368(%rbx), %r14
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	_ZZN2v88internal18IncrementalMarking19AdvanceOnAllocationEvE29trace_event_unique_atomic1097(%rip), %rdx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L3394
.L3357:
	movq	$0, -208(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L3395
.L3359:
	movq	(%r12), %rax
	leaq	-144(%rbp), %r15
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18IncrementalMarking19AdvanceOnAllocationEvE29trace_event_unique_atomic1098(%rip), %rdx
	testq	%rdx, %rdx
	je	.L3396
.L3364:
	movq	$0, -176(%rbp)
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L3397
.L3366:
	movq	%r12, %rdi
	call	_ZN2v88internal18IncrementalMarking36ScheduleBytesToMarkBasedOnAllocationEv
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movsd	.LC41(%rip), %xmm0
	call	_ZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginE
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	2352(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3398
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3394:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3399
.L3358:
	movq	%r15, _ZZN2v88internal18IncrementalMarking19AdvanceOnAllocationEvE29trace_event_unique_atomic1097(%rip)
	jmp	.L3357
	.p2align 4,,10
	.p2align 3
.L3397:
	xorl	%edi, %edi
	movq	%rdx, -224(%rbp)
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -232(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	$0, -216(%rbp)
	movq	-224(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3400
.L3367:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3368
	movq	(%rdi), %rax
	movq	%rdx, -224(%rbp)
	call	*8(%rax)
	movq	-224(%rbp), %rdx
.L3368:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3369
	movq	(%rdi), %rax
	movq	%rdx, -224(%rbp)
	call	*8(%rax)
	movq	-224(%rbp), %rdx
.L3369:
	xorl	%edi, %edi
	movq	%rdx, -224(%rbp)
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	-224(%rbp), %rdx
	movq	%rax, -160(%rbp)
	movq	-216(%rbp), %rax
	movq	%rdx, -168(%rbp)
	movq	%rax, -152(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rax, -176(%rbp)
	jmp	.L3366
	.p2align 4,,10
	.p2align 3
.L3396:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3401
.L3365:
	movq	%rdx, _ZZN2v88internal18IncrementalMarking19AdvanceOnAllocationEvE29trace_event_unique_atomic1098(%rip)
	jmp	.L3364
	.p2align 4,,10
	.p2align 3
.L3395:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	$0, -216(%rbp)
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3402
.L3360:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3361
	movq	(%rdi), %rax
	call	*8(%rax)
.L3361:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3362
	movq	(%rdi), %rax
	call	*8(%rax)
.L3362:
	leaq	.LC39(%rip), %rax
	movq	%r15, -200(%rbp)
	movq	%rax, -192(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -184(%rbp)
	leaq	-200(%rbp), %rax
	movq	%rax, -208(%rbp)
	jmp	.L3359
	.p2align 4,,10
	.p2align 3
.L3399:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L3358
	.p2align 4,,10
	.p2align 3
.L3402:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r15, %rdx
	movl	$88, %esi
	pushq	%rcx
	leaq	.LC39(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, -216(%rbp)
	addq	$64, %rsp
	jmp	.L3360
	.p2align 4,,10
	.p2align 3
.L3401:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L3365
	.p2align 4,,10
	.p2align 3
.L3400:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movl	$88, %esi
	pushq	%rcx
	movq	-232(%rbp), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	-224(%rbp), %rdx
	movq	%rax, -216(%rbp)
	addq	$64, %rsp
	jmp	.L3367
.L3398:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28967:
	.size	_ZN2v88internal18IncrementalMarking19AdvanceOnAllocationEv.part.0, .-_ZN2v88internal18IncrementalMarking19AdvanceOnAllocationEv.part.0
	.section	.text._ZN2v88internal18IncrementalMarking19AdvanceOnAllocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking19AdvanceOnAllocationEv
	.type	_ZN2v88internal18IncrementalMarking19AdvanceOnAllocationEv, @function
_ZN2v88internal18IncrementalMarking19AdvanceOnAllocationEv:
.LFB22428:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	392(%rax), %edx
	testl	%edx, %edx
	jne	.L3403
	cmpb	$0, _ZN2v88internal24FLAG_incremental_markingE(%rip)
	je	.L3403
	movl	80(%rdi), %ecx
	leal	-1(%rcx), %edx
	cmpl	$1, %edx
	jbe	.L3407
.L3403:
	ret
	.p2align 4,,10
	.p2align 3
.L3407:
	movq	200(%rax), %rax
	testq	%rax, %rax
	jne	.L3403
	jmp	_ZN2v88internal18IncrementalMarking19AdvanceOnAllocationEv.part.0
	.cfi_endproc
.LFE22428:
	.size	_ZN2v88internal18IncrementalMarking19AdvanceOnAllocationEv, .-_ZN2v88internal18IncrementalMarking19AdvanceOnAllocationEv
	.section	.text._ZN2v88internal18IncrementalMarking8Observer4StepEimm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking8Observer4StepEimm
	.type	_ZN2v88internal18IncrementalMarking8Observer4StepEimm, @function
_ZN2v88internal18IncrementalMarking8Observer4StepEimm:
.LFB22346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	(%rax), %rbx
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	-24976(%rbx), %r14d
	movaps	%xmm0, -80(%rbp)
	subq	$37592, %rbx
	movl	$1, 12616(%rbx)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3426
.L3409:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	movl	392(%rax), %edx
	testl	%edx, %edx
	jne	.L3410
	cmpb	$0, _ZN2v88internal24FLAG_incremental_markingE(%rip)
	je	.L3410
	movl	80(%rdi), %esi
	leal	-1(%rsi), %edx
	cmpl	$1, %edx
	jbe	.L3427
.L3410:
	cmpb	$0, 87(%rdi)
	je	.L3412
	testq	%r13, %r13
	jne	.L3428
.L3412:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3429
.L3413:
	movl	%r14d, 12616(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3430
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3427:
	.cfi_restore_state
	movq	200(%rax), %rax
	testq	%rax, %rax
	jne	.L3425
	call	_ZN2v88internal18IncrementalMarking19AdvanceOnAllocationEv.part.0
.L3425:
	movq	24(%r12), %rdi
	jmp	.L3410
	.p2align 4,,10
	.p2align 3
.L3428:
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm.part.0
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3413
	.p2align 4,,10
	.p2align 3
.L3429:
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3413
	.p2align 4,,10
	.p2align 3
.L3426:
	movq	40960(%rbx), %rax
	leaq	-88(%rbp), %rsi
	movl	$148, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3409
.L3430:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22346:
	.size	_ZN2v88internal18IncrementalMarking8Observer4StepEimm, .-_ZN2v88internal18IncrementalMarking8Observer4StepEimm
	.section	.text._ZN2v88internal12JSTypedArray14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_,"axG",@progbits,_ZN2v88internal12JSTypedArray14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_,comdat
	.p2align 4
	.weak	_ZN2v88internal12JSTypedArray14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_
	.type	_ZN2v88internal12JSTypedArray14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_, @function
_ZN2v88internal12JSTypedArray14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_:
.LFB27932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	leaq	31(%rsi), %r10
	andq	$-262144, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	7(%rsi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movl	%edx, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r12, %r10
	jbe	.L3451
	movq	%rdi, %rdx
	movq	%r10, %r15
	movq	%rsi, %r8
	movq	%r9, %rbx
	jmp	.L3432
	.p2align 4,,10
	.p2align 3
.L3436:
	addq	$8, %r12
	cmpq	%r12, %r15
	jbe	.L3592
.L3432:
	movq	(%r12), %r13
	testb	$1, %r13b
	je	.L3436
	movq	%r13, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testb	$64, %al
	jne	.L3437
.L3440:
	movl	%r13d, %eax
	movl	$1, %esi
	subl	%r14d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	movq	16(%r14), %rsi
	leaq	(%rsi,%rax,4), %rdi
	.p2align 4,,10
	.p2align 3
.L3438:
	movl	(%rdi), %esi
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%eax, %ecx
	je	.L3436
	movl	%ecx, %r9d
	movl	%esi, %eax
	orl	%esi, %r9d
	lock cmpxchgl	%r9d, (%rdi)
	cmpl	%eax, %esi
	jne	.L3438
	movq	-104(%rbp), %rax
	movq	16(%rax), %rsi
	movq	104(%rsi), %rax
	movq	8(%rax), %r14
	cmpq	$64, %r14
	je	.L3593
	leaq	1(%r14), %rcx
	addq	$8, %r12
	movq	%rcx, 8(%rax)
	movq	%r13, 16(%rax,%r14,8)
	cmpq	%r12, %r15
	ja	.L3432
	.p2align 4,,10
	.p2align 3
.L3592:
	movq	%rdx, %r15
	movq	%r8, %rbx
.L3451:
	movq	63(%rbx), %r12
	leaq	63(%rbx), %rsi
	testb	$1, %r12b
	jne	.L3594
.L3452:
	movq	47(%r15), %rax
	testq	%rax, %rax
	je	.L3595
	movq	$0, -72(%rbp)
	movb	$1, -80(%rbp)
	movl	$0, -76(%rbp)
	movq	47(%r15), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L3476
	movzbl	8(%r15), %eax
	movb	$0, -80(%rbp)
	sall	$3, %eax
	movl	%eax, -76(%rbp)
.L3476:
	leaq	-84(%rbp), %rax
	movl	-112(%rbp), %r14d
	movl	$72, %r12d
	movq	%rax, -120(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	leaq	-1(%rbx), %rax
	andq	$-262144, %rbx
	movq	%rax, -136(%rbp)
	cmpl	$72, %r14d
	jg	.L3477
	jmp	.L3431
	.p2align 4,,10
	.p2align 3
.L3591:
	movslq	-84(%rbp), %r12
.L3479:
	cmpl	%r12d, %r14d
	jle	.L3431
.L3477:
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdi
	movl	%r14d, %edx
	movl	%r12d, %esi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L3591
	movq	-136(%rbp), %rdi
	movslq	-84(%rbp), %r9
	addq	%rdi, %r12
	leaq	(%r9,%rdi), %r15
	cmpq	%r12, %r15
	jbe	.L3510
	movq	%r15, -112(%rbp)
	movq	%rbx, %r15
	jmp	.L3498
	.p2align 4,,10
	.p2align 3
.L3482:
	addq	$8, %r12
	cmpq	%r12, -112(%rbp)
	jbe	.L3596
.L3498:
	movq	(%r12), %rbx
	testb	$1, %bl
	je	.L3482
	movq	%rbx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	testb	$64, %al
	jne	.L3483
.L3486:
	movl	%ebx, %eax
	movq	16(%r8), %rdx
	movl	$1, %esi
	subl	%r8d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
	.p2align 4,,10
	.p2align 3
.L3484:
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	je	.L3482
	movl	%esi, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	jne	.L3484
	movq	-104(%rbp), %rax
	movq	16(%rax), %rdx
	movq	104(%rdx), %rax
	movq	8(%rax), %r13
	cmpq	$64, %r13
	je	.L3597
	leaq	1(%r13), %rdx
	addq	$8, %r12
	movq	%rdx, 8(%rax)
	movq	%rbx, 16(%rax,%r13,8)
	cmpq	%r12, -112(%rbp)
	ja	.L3498
	.p2align 4,,10
	.p2align 3
.L3596:
	movq	%r15, %rbx
	jmp	.L3591
	.p2align 4,,10
	.p2align 3
.L3437:
	movq	8(%rbx), %rax
	testb	$88, %al
	je	.L3439
	testb	$-128, %ah
	je	.L3440
.L3439:
	movq	112(%rbx), %r9
	testq	%r9, %r9
	je	.L3598
.L3442:
	movq	%r12, %rax
	movl	%r12d, %ecx
	subq	%rbx, %rax
	andl	$262143, %ecx
	shrq	$18, %rax
	movl	%ecx, %r10d
	movl	%ecx, %r11d
	sarl	$13, %ecx
	leaq	(%rax,%rax,2), %rax
	sarl	$8, %r10d
	movslq	%ecx, %rcx
	sarl	$3, %r11d
	salq	$7, %rax
	andl	$31, %r10d
	andl	$31, %r11d
	leaq	(%rax,%rcx,8), %rax
	addq	%rax, %r9
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	je	.L3599
.L3444:
	movl	%r11d, %ecx
	movl	$1, %eax
	movslq	%r10d, %r10
	sall	%cl, %eax
	leaq	(%rsi,%r10,4), %rdi
	movl	%eax, %ecx
	movl	(%rdi), %eax
	testl	%eax, %ecx
	jne	.L3440
	movl	(%rdi), %esi
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%eax, %ecx
	je	.L3440
	.p2align 4,,10
	.p2align 3
.L3600:
	movl	%ecx, %r9d
	movl	%esi, %eax
	orl	%esi, %r9d
	lock cmpxchgl	%r9d, (%rdi)
	cmpl	%eax, %esi
	je	.L3440
	movl	(%rdi), %esi
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%eax, %ecx
	jne	.L3600
	jmp	.L3440
	.p2align 4,,10
	.p2align 3
.L3595:
	movslq	-112(%rbp), %rdx
	leaq	71(%rbx), %r12
	movl	$1, %r15d
	leaq	-1(%rbx,%rdx), %r14
	cmpq	%r12, %r14
	jbe	.L3431
	movq	%r14, %r13
	movq	%r12, %r14
	jmp	.L3462
	.p2align 4,,10
	.p2align 3
.L3465:
	addq	$8, %r14
	cmpq	%r14, %r13
	jbe	.L3431
.L3462:
	movq	(%r14), %r12
	testb	$1, %r12b
	je	.L3465
	movq	%r12, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L3601
.L3467:
	movl	%r12d, %eax
	movl	%r15d, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
	.p2align 4,,10
	.p2align 3
.L3471:
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	je	.L3465
	movl	%esi, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	jne	.L3471
	movq	-104(%rbp), %rax
	movq	16(%rax), %rdx
	movq	104(%rdx), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L3602
	leaq	1(%rcx), %rdx
	addq	$8, %r14
	movq	%rdx, 8(%rax)
	movq	%r12, 16(%rax,%rcx,8)
	cmpq	%r14, %r13
	ja	.L3462
	.p2align 4,,10
	.p2align 3
.L3431:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3603
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3598:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%r8, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rdx
	movq	%rax, %r9
	jmp	.L3442
	.p2align 4,,10
	.p2align 3
.L3599:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%r8, -152(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%r9, -136(%rbp)
	movl	%r11d, -128(%rbp)
	movl	%r10d, -120(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-120(%rbp), %r10d
	movl	-128(%rbp), %r11d
	testq	%rax, %rax
	movq	-136(%rbp), %r9
	movq	%rax, %rsi
	movq	-144(%rbp), %rdx
	movq	-152(%rbp), %r8
	je	.L3604
.L3445:
	leaq	8(%rsi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	$0, (%rsi)
	andq	$-8, %rdi
	movq	$0, 120(%rsi)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%rsi, (%r9)
	movq	%r9, -120(%rbp)
	testq	%rax, %rax
	je	.L3444
	movq	%rsi, %rdi
	movq	%r8, -152(%rbp)
	movq	%rdx, -144(%rbp)
	movl	%r11d, -136(%rbp)
	movl	%r10d, -128(%rbp)
	call	_ZdaPv@PLT
	movq	-120(%rbp), %r9
	movq	(%r9), %rsi
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %rdx
	movl	-136(%rbp), %r11d
	movl	-128(%rbp), %r10d
	jmp	.L3444
	.p2align 4,,10
	.p2align 3
.L3483:
	movq	8(%r15), %rax
	testb	$88, %al
	je	.L3485
	testb	$-128, %ah
	je	.L3486
.L3485:
	movq	112(%r15), %rax
	testq	%rax, %rax
	je	.L3605
.L3488:
	movq	%r12, %rcx
	movl	%r12d, %edx
	subq	%r15, %rcx
	andl	$262143, %edx
	shrq	$18, %rcx
	movl	%edx, %r13d
	movl	%edx, %r9d
	sarl	$13, %edx
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$8, %r13d
	movslq	%edx, %rdx
	sarl	$3, %r9d
	salq	$7, %rcx
	andl	$31, %r13d
	andl	$31, %r9d
	leaq	(%rcx,%rdx,8), %rdx
	addq	%rax, %rdx
	movq	(%rdx), %r11
	testq	%r11, %r11
	je	.L3606
.L3490:
	movl	%r9d, %ecx
	movl	$1, %eax
	movslq	%r13d, %r13
	sall	%cl, %eax
	leaq	(%r11,%r13,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L3486
	jmp	.L3493
	.p2align 4,,10
	.p2align 3
.L3607:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L3486
.L3493:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L3607
	jmp	.L3486
	.p2align 4,,10
	.p2align 3
.L3601:
	movq	%rbx, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3468
	testb	$-128, %ah
	je	.L3467
.L3468:
	movq	%r14, %rsi
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-112(%rbp), %rdx
	jmp	.L3467
	.p2align 4,,10
	.p2align 3
.L3594:
	movq	%r12, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testb	$64, %al
	jne	.L3608
.L3454:
	movl	%r12d, %eax
	movl	$1, %edx
	subl	%r13d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r13), %rdx
	leaq	(%rdx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L3457:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L3452
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L3457
	movq	-104(%rbp), %rax
	movq	16(%rax), %rdx
	movq	104(%rdx), %r14
	movq	8(%r14), %r13
	cmpq	$64, %r13
	je	.L3609
	leaq	1(%r13), %rax
	movq	%rax, 8(%r14)
	movq	%r12, 16(%r14,%r13,8)
	jmp	.L3452
	.p2align 4,,10
	.p2align 3
.L3606:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%rdx, -160(%rbp)
	movl	%r9d, -152(%rbp)
	movq	%r8, -144(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-144(%rbp), %r8
	movl	-152(%rbp), %r9d
	testq	%rax, %rax
	movq	-160(%rbp), %rdx
	movq	%rax, %r11
	je	.L3610
.L3491:
	leaq	8(%r11), %rdi
	movq	%r11, %rcx
	xorl	%eax, %eax
	movq	$0, (%r11)
	andq	$-8, %rdi
	movq	$0, 120(%r11)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r11, (%rdx)
	movq	%rdx, -144(%rbp)
	testq	%rax, %rax
	je	.L3490
	movq	%r11, %rdi
	movl	%r9d, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZdaPv@PLT
	movq	-144(%rbp), %rdx
	movq	(%rdx), %r11
	movl	-160(%rbp), %r9d
	movq	-152(%rbp), %r8
	jmp	.L3490
	.p2align 4,,10
	.p2align 3
.L3605:
	movq	%r15, %rdi
	movq	%r8, -144(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-144(%rbp), %r8
	jmp	.L3488
	.p2align 4,,10
	.p2align 3
.L3597:
	leaq	744(%rdx), %rdi
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rdi, -152(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-144(%rbp), %rdx
	movq	-160(%rbp), %rax
	movq	784(%rdx), %rcx
	movq	%rcx, (%rax)
	movq	%rax, 784(%rdx)
	movq	%rdx, -144(%rbp)
	movq	-152(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r13, %rcx
	movq	-144(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 104(%rdx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L3482
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%rbx, 16(%rsi,%rax,8)
	jmp	.L3482
	.p2align 4,,10
	.p2align 3
.L3593:
	leaq	744(%rsi), %rdi
	movq	%r8, -152(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rax, -136(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rdi, -128(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-120(%rbp), %rsi
	movq	-136(%rbp), %rax
	movq	784(%rsi), %rcx
	movq	%rcx, (%rax)
	movq	%rax, 784(%rsi)
	movq	%rsi, -120(%rbp)
	movq	-128(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r14, %rcx
	movq	-120(%rbp), %rsi
	movq	-144(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %r9
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%r9, 104(%rsi)
	movq	-152(%rbp), %r8
	movq	8(%r9), %rax
	cmpq	$64, %rax
	je	.L3436
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r9)
	movq	%r13, 16(%r9,%rax,8)
	jmp	.L3436
	.p2align 4,,10
	.p2align 3
.L3510:
	movslq	%r9d, %r12
	jmp	.L3479
	.p2align 4,,10
	.p2align 3
.L3608:
	movq	%rbx, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3455
	testb	$-128, %ah
	je	.L3454
.L3455:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3454
	.p2align 4,,10
	.p2align 3
.L3602:
	leaq	744(%rdx), %rdi
	movq	%rcx, -136(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-112(%rbp), %rdx
	movq	-128(%rbp), %rax
	movq	784(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 784(%rdx)
	movq	%rdx, -112(%rbp)
	movq	-120(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-136(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 104(%rdx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L3465
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%r12, 16(%rsi,%rax,8)
	jmp	.L3465
	.p2align 4,,10
	.p2align 3
.L3609:
	leaq	744(%rdx), %rdi
	movq	%rdx, -120(%rbp)
	movq	%rdi, -128(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-120(%rbp), %rdx
	movq	784(%rdx), %rax
	movq	%rax, (%r14)
	movq	%r14, 784(%rdx)
	movq	%rdx, -120(%rbp)
	movq	-128(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r13, %rcx
	movq	-120(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 104(%rdx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L3452
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%r12, 16(%rsi,%rax,8)
	jmp	.L3452
.L3603:
	call	__stack_chk_fail@PLT
.L3610:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-144(%rbp), %r8
	movl	-152(%rbp), %r9d
	testq	%rax, %rax
	movq	-160(%rbp), %rdx
	movq	%rax, %r11
	jne	.L3491
.L3492:
	leaq	.LC30(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L3604:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-120(%rbp), %r10d
	movl	-128(%rbp), %r11d
	testq	%rax, %rax
	movq	-136(%rbp), %r9
	movq	%rax, %rsi
	movq	-144(%rbp), %rdx
	movq	-152(%rbp), %r8
	jne	.L3445
	jmp	.L3492
	.cfi_endproc
.LFE27932:
	.size	_ZN2v88internal12JSTypedArray14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_, .-_ZN2v88internal12JSTypedArray14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_
	.section	.text._ZN2v88internal18IncrementalMarking13RevisitObjectENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking13RevisitObjectENS0_10HeapObjectE
	.type	_ZN2v88internal18IncrementalMarking13RevisitObjectENS0_10HeapObjectE, @function
_ZN2v88internal18IncrementalMarking13RevisitObjectENS0_10HeapObjectE:
.LFB22405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1(%rsi), %rbx
	subq	$200, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rsi), %r13
	movl	$1, %esi
	movl	%r13d, %eax
	movq	%r13, %rdx
	andl	$262143, %eax
	andq	$-262144, %rdx
	movl	%eax, %ecx
	movq	16(%rdx), %rdx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	jmp	.L3613
	.p2align 4,,10
	.p2align 3
.L4065:
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L4064
.L3613:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	jne	.L4065
.L3612:
	movq	(%r14), %rax
	leaq	16+_ZTVN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEE(%rip), %rsi
	addq	$160, %r14
	movq	%rsi, -176(%rbp)
	movq	2016(%rax), %rax
	movq	8(%rax), %rdx
	movq	%rax, -160(%rbp)
	movl	9996(%rax), %eax
	movq	%r14, -152(%rbp)
	movq	%rdx, -168(%rbp)
	movl	%eax, -144(%rbp)
	movzbl	10(%r13), %eax
	cmpb	$56, %al
	ja	.L3615
	leaq	.L3617(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal18IncrementalMarking13RevisitObjectENS0_10HeapObjectE,"a",@progbits
	.align 4
	.align 4
.L3617:
	.long	.L3672-.L3617
	.long	.L3669-.L3617
	.long	.L3670-.L3617
	.long	.L3669-.L3617
	.long	.L3667-.L3617
	.long	.L3667-.L3617
	.long	.L3615-.L3617
	.long	.L3666-.L3617
	.long	.L3665-.L3617
	.long	.L3623-.L3617
	.long	.L3663-.L3617
	.long	.L3662-.L3617
	.long	.L3634-.L3617
	.long	.L3621-.L3617
	.long	.L3659-.L3617
	.long	.L3658-.L3617
	.long	.L3621-.L3617
	.long	.L3656-.L3617
	.long	.L3623-.L3617
	.long	.L3654-.L3617
	.long	.L3653-.L3617
	.long	.L3611-.L3617
	.long	.L3651-.L3617
	.long	.L3650-.L3617
	.long	.L3649-.L3617
	.long	.L3648-.L3617
	.long	.L3851-.L3617
	.long	.L3628-.L3617
	.long	.L3645-.L3617
	.long	.L3644-.L3617
	.long	.L3851-.L3617
	.long	.L3642-.L3617
	.long	.L3641-.L3617
	.long	.L3640-.L3617
	.long	.L3639-.L3617
	.long	.L3621-.L3617
	.long	.L3637-.L3617
	.long	.L3636-.L3617
	.long	.L3635-.L3617
	.long	.L3634-.L3617
	.long	.L3634-.L3617
	.long	.L3632-.L3617
	.long	.L3631-.L3617
	.long	.L3630-.L3617
	.long	.L3629-.L3617
	.long	.L3628-.L3617
	.long	.L3625-.L3617
	.long	.L3626-.L3617
	.long	.L3625-.L3617
	.long	.L3624-.L3617
	.long	.L3623-.L3617
	.long	.L3622-.L3617
	.long	.L3621-.L3617
	.long	.L3620-.L3617
	.long	.L3619-.L3617
	.long	.L3618-.L3617
	.long	.L3616-.L3617
	.section	.text._ZN2v88internal18IncrementalMarking13RevisitObjectENS0_10HeapObjectE
.L3654:
	movl	31(%r12), %eax
	movq	7(%r12), %rbx
	leaq	7(%r12), %rsi
	leal	48(,%rax,8), %eax
	movl	%eax, -192(%rbp)
	testb	$1, %bl
	jne	.L4066
.L3718:
	movq	15(%r12), %r14
	leaq	15(%r12), %rsi
	movq	%r14, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L3728
	cmpq	$3, %rax
	je	.L4067
.L3730:
	movq	23(%r12), %rbx
	leaq	23(%r12), %rsi
	testb	$1, %bl
	jne	.L4068
.L3741:
	movl	-192(%rbp), %edx
	leaq	-176(%rbp), %rcx
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	.p2align 4,,10
	.p2align 3
.L3611:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4069
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4064:
	.cfi_restore_state
	movq	8(%r14), %rdi
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L3612
.L3621:
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	-176(%rbp), %rcx
	movl	$16, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3623:
	leaq	-176(%rbp), %rcx
	movl	$16, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3634:
	leaq	-176(%rbp), %rcx
	movl	$32, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3651:
	movq	-168(%rbp), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	je	.L3851
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	1496(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
.L3851:
	movzbl	7(%r13), %ecx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-176(%rbp), %r8
	sall	$3, %ecx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3667:
	movl	11(%r12), %eax
	jmp	.L3611
.L3669:
	movq	7(%r12), %rax
	jmp	.L3611
.L3625:
	leaq	-176(%rbp), %rcx
	movl	$24, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3628:
	movzbl	7(%r13), %edx
	leaq	-176(%rbp), %rcx
	movl	$8, %esi
	movq	%r12, %rdi
	sall	$3, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3645:
	movq	-168(%rbp), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	jne	.L4070
.L3770:
	movzbl	7(%r13), %edx
	leaq	-176(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	sall	$3, %edx
	call	_ZN2v88internal12JSTypedArray14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_
	jmp	.L3611
.L3656:
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r12, -128(%rbp)
	movl	$40, %ebx
	xorl	%r12d, %r12d
	movl	$1, %r14d
	leaq	2888(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi64EE4PushEiS2_
	movq	-128(%rbp), %rax
	movl	35(%rax), %edx
	testl	%edx, %edx
	jle	.L3716
	movq	%r13, -208(%rbp)
	jmp	.L3697
.L4074:
	movq	(%rsi), %r15
	movq	-128(%rbp), %rax
	testb	$1, %r15b
	jne	.L4071
.L3703:
	addl	$1, %r12d
	addq	$16, %rbx
	cmpl	%r12d, 35(%rax)
	jle	.L4072
.L3697:
	leaq	-1(%rax,%rbx), %rsi
	movq	(%rsi), %r13
	movq	-128(%rbp), %rdi
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testb	$64, %al
	jne	.L4073
.L3699:
	movq	-128(%rbp), %rax
	movl	%r13d, %ecx
	movq	16(%r15), %rdi
	subl	%r15d, %ecx
	leaq	7(%rbx,%rax), %rsi
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	movl	(%rdi,%rax,4), %eax
	movl	%r14d, %edi
	sall	%cl, %edi
	testl	%eax, %edi
	jne	.L4074
	movq	(%rsi), %r15
	movq	-128(%rbp), %rax
	testb	$1, %r15b
	je	.L3703
	movq	%r15, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rcx
	andl	$64, %ecx
	jne	.L4075
.L3712:
	movl	%r15d, %ecx
	movq	16(%r8), %rsi
	subl	%r8d, %ecx
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	movl	(%rsi,%rax,4), %eax
	movl	%r14d, %esi
	sall	%cl, %esi
	testl	%eax, %esi
	je	.L3715
.L4063:
	movq	-128(%rbp), %rax
	jmp	.L3703
.L3658:
	leaq	-128(%rbp), %r14
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	15(%r12), %rsi
	leaq	23(%r12), %rax
	cmpq	%rax, %rsi
	jnb	.L3852
	movq	15(%r12), %r13
	testb	$1, %r13b
	jne	.L4076
.L3852:
	movswl	9(%r12), %ebx
	movl	-144(%rbp), %esi
	movq	%r14, %rdi
	movq	%r12, -128(%rbp)
	movl	%ebx, %edx
	call	_ZN2v88internal15DescriptorArray31UpdateNumberOfMarkedDescriptorsEjs@PLT
	cmpw	%bx, %ax
	jge	.L3611
	cwtl
	leal	3(%rbx,%rbx,2), %r12d
	movq	-128(%rbp), %r13
	leal	3(%rax,%rax,2), %ebx
	sall	$3, %r12d
	sall	$3, %ebx
	leaq	-1(%r13), %rdx
	movslq	%r12d, %r12
	movslq	%ebx, %rbx
	addq	%rdx, %r12
	addq	%rdx, %rbx
	cmpq	%r12, %rbx
	jnb	.L3611
	movl	$1, %r14d
	jmp	.L3696
.L3688:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jbe	.L3611
.L3696:
	movq	(%rbx), %r15
	movq	%r15, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L4077
	cmpq	$3, %rax
	jne	.L3688
	cmpl	$3, %r15d
	je	.L3688
	movq	%r15, %rdx
	movq	%r15, %rax
	movl	%r14d, %edi
	andq	$-262144, %rdx
	andq	$-3, %rax
	subl	%edx, %eax
	movq	16(%rdx), %rsi
	movl	%eax, %ecx
	shrl	$3, %eax
	shrl	$8, %ecx
	movl	(%rsi,%rcx,4), %esi
	movl	%eax, %ecx
	sall	%cl, %edi
	testl	%esi, %edi
	je	.L3694
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L3688
	movq	%r13, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3695
	testb	$-128, %ah
	je	.L3688
.L3695:
	movq	%rbx, %rsi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3688
	.p2align 4,,10
	.p2align 3
.L3659:
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	leaq	-176(%rbp), %r14
	movq	%r12, -128(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$24, %edx
	movl	%eax, %r13d
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3622:
	leaq	-176(%rbp), %r13
	movq	%r12, %rdi
	movl	$16, %edx
	movl	$8, %esi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rcx
	movl	$32, %edx
	movq	%r12, %rdi
	movl	$24, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3632:
	movzbl	9(%r12), %edx
	leaq	-176(%rbp), %rcx
	movl	$16, %esi
	movq	%r12, %rdi
	sall	$5, %edx
	addl	$16, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3629:
	leaq	-176(%rbp), %r13
	movq	%r12, %rdi
	movl	$48, %edx
	movl	$8, %esi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rcx
	movl	$112, %edx
	movq	%r12, %rdi
	movl	$48, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3630:
	movzbl	17(%r12), %eax
	leaq	-176(%rbp), %rcx
	movl	$24, %esi
	movq	%r12, %rdi
	leal	(%rax,%rax,2), %edx
	sall	$4, %edx
	addl	$24, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3635:
	movl	$16, %esi
	movl	$40, %edx
	movq	%r12, %rdi
	movq	%r12, -128(%rbp)
	leaq	-176(%rbp), %rcx
	movzbl	7(%r13), %eax
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	cmpb	$0, _ZN2v88internal26FLAG_stress_flush_bytecodeE(%rip)
	movl	$2, %esi
	jne	.L3841
	movzbl	_ZN2v88internal19FLAG_flush_bytecodeE(%rip), %esi
.L3841:
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal18SharedFunctionInfo19ShouldFlushBytecodeENS0_17BytecodeFlushModeE
	testb	%al, %al
	je	.L3842
	movq	-160(%rbp), %rax
	movq	-128(%rbp), %rdx
	xorl	%esi, %esi
	leaq	8456(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_18SharedFunctionInfoELi64EE4PushEiS2_
	jmp	.L3611
.L3636:
	leaq	-176(%rbp), %r14
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$40, %edx
	movl	$8, %esi
	movl	%eax, %r13d
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$40, %esi
	call	_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3637:
	leaq	-176(%rbp), %rcx
	movl	$40, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3620:
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	-176(%rbp), %rcx
	movl	$32, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3631:
	movzbl	9(%r12), %edx
	leaq	-176(%rbp), %rcx
	movl	$16, %esi
	movq	%r12, %rdi
	addl	$1, %edx
	sall	$4, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3616:
	movq	15(%r12), %rax
	leaq	15(%r12), %r14
	testb	$1, %al
	jne	.L4078
.L3772:
	movzbl	7(%r13), %r13d
	leaq	7(%r12), %rsi
	leal	0(,%r13,8), %eax
	movl	%eax, -192(%rbp)
	cmpq	%rsi, %r14
	jbe	.L3779
	movq	7(%r12), %r14
	testb	$1, %r14b
	jne	.L4079
.L3779:
	movslq	-192(%rbp), %r13
	leaq	23(%r12), %r15
	movl	$1, %r14d
	addq	%rbx, %r13
	cmpq	%r15, %r13
	jbe	.L3611
	movq	%r12, -192(%rbp)
	jmp	.L3787
	.p2align 4,,10
	.p2align 3
.L3789:
	addq	$8, %r15
	cmpq	%r13, %r15
	jnb	.L3611
.L3787:
	movq	(%r15), %rbx
	testb	$1, %bl
	je	.L3789
	movq	%rbx, %r12
	andq	$-262144, %r12
	movq	8(%r12), %rax
	testb	$64, %al
	jne	.L4080
.L3791:
	movl	%ebx, %eax
	movq	16(%r12), %rdx
	movl	%r14d, %esi
	subl	%r12d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L3795:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L3789
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L3795
	movq	-160(%rbp), %r12
	movq	104(%r12), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L4081
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%rbx, 16(%rax,%rcx,8)
	jmp	.L3789
.L3672:
	movl	7(%r12), %eax
	jmp	.L3611
.L3662:
	leaq	-176(%rbp), %rcx
	movl	$8, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3663:
	leaq	-176(%rbp), %r13
	movq	%r12, %rdi
	leaq	-128(%rbp), %r14
	movl	$40, %edx
	movq	%r13, %rcx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$1999, %edx
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE@PLT
	jmp	.L3611
.L3619:
	movzbl	7(%r13), %edx
	leaq	-176(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	sall	$3, %edx
	call	_ZN2v88internal18WasmInstanceObject14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_
	jmp	.L3611
.L3665:
	movq	%r12, -128(%rbp)
	movq	7(%r12), %rax
	leaq	-176(%rbp), %r13
	movl	$16, %esi
	movq	%r13, %rdx
	movq	-128(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %rdx
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	%r13, %rdx
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase14IteratePointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	movq	-168(%rbp), %rax
	cmpb	$0, 2764(%rax)
	jne	.L3611
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal13BytecodeArray9MakeOlderEv@PLT
	jmp	.L3611
.L3626:
	leaq	-176(%rbp), %r13
	movq	%r12, %rdi
	movl	$48, %edx
	movl	$8, %esi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rcx
	movl	$72, %edx
	movq	%r12, %rdi
	movl	$56, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3639:
	movl	7(%r12), %eax
	leaq	-176(%rbp), %rcx
	movq	%r12, %rdi
	leal	23(%rax), %esi
	movl	11(%r12), %eax
	andl	$-8, %esi
	leal	(%rsi,%rax,8), %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3640:
	leaq	-176(%rbp), %rcx
	movl	$48, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3641:
	leaq	-176(%rbp), %rcx
	movl	$1944, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3642:
	cmpw	$1024, 11(%r12)
	ja	.L4082
.L3806:
	leaq	-176(%rbp), %r13
	movq	%r12, %rdi
	movl	$72, %edx
	movl	$24, %esi
	movq	%r13, %rcx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	%r13, %rdx
	movl	$72, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateMaybeWeakPointerINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiPT_
	jmp	.L3611
.L3666:
	leaq	-176(%rbp), %rcx
	movl	$32, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	movzbl	7(%r13), %eax
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3618:
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	-176(%rbp), %rcx
	movl	$8, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3653:
	movq	%r12, %r14
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	andq	$-262144, %r14
	movl	%eax, %r15d
	testb	$1, 9(%r14)
	je	.L3751
	leaq	88(%r14), %rax
	movq	%rax, -192(%rbp)
	movq	88(%r14), %r8
	movslq	%r8d, %rax
	leal	32768(%r8), %r13d
	testl	%r8d, %r8d
	jne	.L3752
	movl	$32784, %r13d
	movl	$16, %eax
.L3752:
	cmpl	%r15d, %r13d
	cmovg	%r15d, %r13d
	movl	%r13d, -208(%rbp)
	cmpl	%r13d, %eax
	jge	.L3611
	movslq	%r13d, %rsi
	leaq	(%rbx,%rsi), %r13
	addq	%rax, %rbx
	movq	%rsi, -216(%rbp)
	cmpq	%r13, %rbx
	jnb	.L3764
	movq	%rbx, %rax
	movq	%r8, -224(%rbp)
	movq	%r14, %rbx
	movq	%r13, %r14
	movq	%rax, %r13
	jmp	.L3754
.L3857:
	addq	$8, %r13
	cmpq	%r13, %r14
	jbe	.L4083
.L3754:
	movq	0(%r13), %r9
	testb	$1, %r9b
	je	.L3857
	movq	%r9, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L4084
.L3759:
	movl	%r9d, %eax
	movl	$1, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L3762:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L3857
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L3762
	movq	-160(%rbp), %rax
	movq	%r9, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L3857
.L3670:
	movzbl	7(%r13), %eax
	jmp	.L3611
.L3624:
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r12, -128(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	%r12, %rdi
	movl	$8, %esi
	leaq	-176(%rbp), %rcx
	movl	%eax, %edx
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	2192(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_15TransitionArrayELi64EE4PushEiS2_
	jmp	.L3611
.L3648:
	movq	%r12, -128(%rbp)
	movzbl	7(%r13), %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-176(%rbp), %r8
	movl	$8, %edx
	sall	$3, %ecx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	cmpb	$0, _ZN2v88internal19FLAG_flush_bytecodeE(%rip)
	je	.L3611
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv
	testb	%al, %al
	je	.L3611
	movq	-160(%rbp), %rax
	movq	-128(%rbp), %rdx
	xorl	%esi, %esi
	leaq	9152(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10JSFunctionELi64EE4PushEiS2_
	jmp	.L3611
.L3649:
	movq	-168(%rbp), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	jne	.L4085
.L3766:
	movzbl	7(%r13), %ebx
	leaq	-176(%rbp), %r14
	movq	%r12, %rdi
	movl	$32, %edx
	movq	%r14, %rcx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movzbl	%bl, %ecx
	movq	%r14, %r8
	movl	$56, %edx
	sall	$3, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3650:
	movq	-168(%rbp), %rax
	movq	2128(%rax), %rax
	cmpq	$0, 8(%rax)
	jne	.L4086
.L3765:
	movzbl	7(%r13), %ebx
	leaq	-176(%rbp), %r14
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%r14, %rcx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	movzbl	%bl, %ecx
	movq	%r14, %r8
	movl	$48, %edx
	sall	$3, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L3611
.L3644:
	movq	23(%r12), %rax
	leaq	23(%r12), %rsi
	testb	$1, %al
	jne	.L4087
.L3801:
	movzbl	7(%r13), %edx
	leaq	-176(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	sall	$3, %edx
	call	_ZN2v88internal9JSWeakRef14BodyDescriptor11IterateBodyINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_3MapENS0_10HeapObjectEiPT_.constprop.0
	jmp	.L3611
.L3615:
	leaq	.LC33(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4073:
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3700
	testb	$-128, %ah
	je	.L3699
.L3700:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3699
.L4071:
	movq	%r15, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rcx
	andl	$64, %ecx
	jne	.L4088
.L3705:
	movl	%r15d, %eax
	movq	16(%r13), %rdx
	movl	%r14d, %esi
	subl	%r13d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L3709:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L4063
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L3709
	movq	-160(%rbp), %rax
	movq	%r15, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	movq	-128(%rbp), %rax
	jmp	.L3703
.L4072:
	movq	-208(%rbp), %r13
.L3716:
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	jmp	.L3611
.L4080:
	movq	-192(%rbp), %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3792
	testb	$-128, %ah
	je	.L3791
.L3792:
	movq	%r15, %rsi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3791
.L4077:
	movq	%r15, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L4089
.L3686:
	movl	%r15d, %eax
	movl	%r14d, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
.L3692:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L3688
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L3692
	movq	-160(%rbp), %rax
	movq	%r15, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L3688
.L3842:
	movq	-128(%rbp), %rax
	movq	7(%rax), %r12
	leaq	7(%rax), %rsi
	testb	$1, %r12b
	je	.L3611
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rdx
	andl	$64, %edx
	jne	.L4090
.L3846:
	movl	%r12d, %edx
	movl	$1, %eax
	subl	%ebx, %edx
	movl	%edx, %ecx
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movl	%edx, %eax
	movq	16(%rbx), %rdx
	shrl	$8, %eax
	leaq	(%rdx,%rax,4), %rsi
	jmp	.L3849
.L4091:
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L4046
.L3849:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	jne	.L4091
	jmp	.L3611
.L3751:
	leaq	-176(%rbp), %rcx
	movl	%eax, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEEEEvNS0_10HeapObjectEiiPT_
	jmp	.L3611
.L4088:
	andq	$-262144, %rax
	movq	%rax, %rdi
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L3706
	testb	$-128, %ah
	je	.L3705
.L3706:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3705
.L3715:
	movq	%r13, %xmm1
	movq	%r15, %xmm5
	movq	-160(%rbp), %rax
	xorl	%esi, %esi
	punpcklqdq	%xmm5, %xmm1
	movaps	%xmm1, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %rcx
	leaq	4976(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE4PushEiS2_
	jmp	.L4063
.L4075:
	andq	$-262144, %rax
	movq	%rax, %rdi
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L3713
	testb	$-128, %ah
	je	.L3712
.L3713:
	movq	%r8, -192(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-192(%rbp), %r8
	jmp	.L3712
.L4082:
	movq	39(%r12), %r13
	movl	$1, %eax
	movl	%r13d, %edx
	movq	%r13, %r14
	andl	$262143, %edx
	andq	$-262144, %r14
	movl	%edx, %ecx
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movl	%edx, %eax
	movq	16(%r14), %rdx
	shrl	$8, %eax
	leaq	(%rdx,%rax,4), %rsi
	jmp	.L3810
.L3807:
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L3811
.L3810:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	jne	.L3807
.L3811:
	leaq	-1(%r13), %rax
	movl	$1, %edx
	movq	%r13, -128(%rbp)
	subl	%r14d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r14), %rcx
	leaq	(%rcx,%rax,4), %rcx
	movl	(%rcx), %eax
	testl	%edx, %eax
	jne	.L4092
.L3816:
	movl	15(%r12), %ebx
	shrl	$10, %ebx
	andl	$1023, %ebx
	je	.L3806
	movl	-144(%rbp), %esi
	leaq	-128(%rbp), %rdi
	movl	%ebx, %edx
	movq	%r13, -128(%rbp)
	call	_ZN2v88internal15DescriptorArray31UpdateNumberOfMarkedDescriptorsEjs@PLT
	cmpw	%bx, %ax
	jge	.L3806
	leal	1(%rbx), %r13d
	movswl	%ax, %ebx
	movq	-128(%rbp), %r14
	addl	$1, %ebx
	imull	$24, %r13d, %r13d
	imull	$24, %ebx, %ebx
	leaq	-1(%r14), %rdx
	movslq	%r13d, %r13
	movslq	%ebx, %rbx
	addq	%rdx, %r13
	addq	%rdx, %rbx
	cmpq	%r13, %rbx
	jnb	.L3806
	movq	%r12, -208(%rbp)
	movl	$1, %r15d
	jmp	.L3840
.L3832:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L4093
.L3840:
	movq	(%rbx), %r12
	movq	%r12, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L4094
	cmpq	$3, %rax
	jne	.L3832
	cmpl	$3, %r12d
	je	.L3832
	movq	%r12, %rdx
	movq	%r12, %rax
	movl	%r15d, %edi
	andq	$-262144, %rdx
	andq	$-3, %rax
	subl	%edx, %eax
	movq	16(%rdx), %rsi
	movl	%eax, %ecx
	shrl	$3, %eax
	shrl	$8, %ecx
	movl	(%rsi,%rcx,4), %esi
	movl	%eax, %ecx
	sall	%cl, %edi
	testl	%esi, %edi
	je	.L3838
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L3832
	movq	%r14, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3839
	testb	$-128, %ah
	je	.L3832
.L3839:
	movq	%rbx, %rsi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3832
	.p2align 4,,10
	.p2align 3
.L4085:
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	1496(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	jmp	.L3766
.L4086:
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	1496(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	jmp	.L3765
.L4076:
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testb	$64, %al
	jne	.L3675
.L3679:
	movl	%r13d, %eax
	movl	$1, %edx
	subl	%ebx, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%rbx), %rdx
	leaq	(%rdx,%rax,4), %rsi
.L3676:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L3852
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L3676
	movq	-160(%rbp), %rax
	movq	%r13, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L3852
.L4070:
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	1496(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	jmp	.L3770
.L4087:
	movq	%rax, %rcx
	andl	$262143, %eax
	andq	$-262144, %rcx
	movl	%eax, %edx
	shrl	$3, %eax
	movq	16(%rcx), %rdi
	shrl	$8, %edx
	movl	(%rdi,%rdx,4), %edx
	btl	%eax, %edx
	jnc	.L3802
	movq	8(%rcx), %rax
	testb	$64, %al
	je	.L3801
	movq	%r12, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3804
	testb	$-128, %ah
	je	.L3801
.L3804:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3801
.L4078:
	movq	%rax, %rcx
	andl	$262143, %eax
	andq	$-262144, %rcx
	movl	%eax, %edx
	shrl	$3, %eax
	movq	16(%rcx), %rsi
	shrl	$8, %edx
	movl	(%rsi,%rdx,4), %edx
	btl	%eax, %edx
	jnc	.L3773
	movq	8(%rcx), %rax
	testb	$64, %al
	je	.L3772
	movq	%r12, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3775
	testb	$-128, %ah
	je	.L3772
.L3775:
	movq	%r14, %rsi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3772
.L4079:
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testb	$64, %al
	jne	.L3777
.L3784:
	movl	%r14d, %eax
	movl	$1, %edx
	subl	%r15d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r15), %rdx
	leaq	(%rdx,%rax,4), %rsi
.L3781:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L3779
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L3781
	movq	-160(%rbp), %r13
	movq	104(%r13), %r15
	movq	8(%r15), %rcx
	cmpq	$64, %rcx
	je	.L4095
	leaq	1(%rcx), %rax
	movq	%rax, 8(%r15)
	movq	%r14, 16(%r15,%rcx,8)
	jmp	.L3779
.L4068:
	movq	%rbx, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testb	$64, %al
	jne	.L4096
.L3743:
	movl	%ebx, %eax
	movl	$1, %edx
	subl	%r14d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r14), %rdx
	leaq	(%rdx,%rax,4), %rsi
.L3747:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L3741
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L3747
	movq	-160(%rbp), %r13
	movq	104(%r13), %r14
	movq	8(%r14), %rcx
	cmpq	$64, %rcx
	je	.L4097
	leaq	1(%rcx), %rax
	movq	%rax, 8(%r14)
	movq	%rbx, 16(%r14,%rcx,8)
	jmp	.L3741
.L4066:
	movq	%rbx, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testb	$64, %al
	jne	.L4098
.L3720:
	movl	%ebx, %eax
	movl	$1, %edx
	subl	%r14d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r14), %rdx
	leaq	(%rdx,%rax,4), %rsi
.L3724:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L3718
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L3724
	movq	-160(%rbp), %r13
	movq	104(%r13), %r14
	movq	8(%r14), %rcx
	cmpq	$64, %rcx
	je	.L4099
	leaq	1(%rcx), %rax
	movq	%rax, 8(%r14)
	movq	%rbx, 16(%r14,%rcx,8)
	jmp	.L3718
.L3694:
	movq	%r13, %xmm0
	movq	%rbx, %xmm4
	movq	-160(%rbp), %rax
	xorl	%esi, %esi
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %rcx
	leaq	5672(%rax), %rdi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_
	jmp	.L3688
.L4067:
	cmpl	$3, %r14d
	je	.L3730
	movq	%r14, %rdx
	movq	%r14, %rax
	andq	$-262144, %rdx
	andq	$-3, %rax
	subl	%edx, %eax
	movq	16(%rdx), %rdi
	movl	%eax, %ecx
	shrl	$3, %eax
	shrl	$8, %ecx
	movl	(%rdi,%rcx,4), %ecx
	btl	%eax, %ecx
	jnc	.L3738
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L3730
	movq	%r12, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3739
	testb	$-128, %ah
	je	.L3730
.L3739:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3730
.L4083:
	movq	-224(%rbp), %r8
.L3764:
	movq	-192(%rbp), %rbx
	movq	-216(%rbp), %rsi
	movq	%r8, %rax
	lock cmpxchgq	%rsi, (%rbx)
	jne	.L4100
	cmpl	-208(%rbp), %r15d
	jle	.L3611
.L4046:
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L3611
.L3728:
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testb	$64, %al
	jne	.L4101
.L3732:
	movl	%r14d, %eax
	movl	$1, %edx
	subl	%ebx, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%rbx), %rdx
	leaq	(%rdx,%rax,4), %rsi
.L3736:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L3730
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L3736
	movq	-160(%rbp), %rax
	movq	%r14, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L3730
.L4081:
	leaq	744(%r12), %rdi
	movq	%rcx, -224(%rbp)
	movq	%rax, -216(%rbp)
	movq	%rdi, -208(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	784(%r12), %rdx
	movq	-216(%rbp), %rax
	movq	%rdx, (%rax)
	movq	%rax, 784(%r12)
	movq	-208(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-224(%rbp), %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 104(%r12)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L3789
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%rbx, 16(%rdx,%rax,8)
	jmp	.L3789
.L4089:
	movq	%r13, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3689
	testb	$-128, %ah
	je	.L3686
.L3689:
	movq	%rbx, %rsi
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-192(%rbp), %rdx
	jmp	.L3686
.L4084:
	movq	8(%rbx), %rax
	testb	$88, %al
	je	.L3760
	testb	$-128, %ah
	je	.L3759
.L3760:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -240(%rbp)
	movq	%r9, -232(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-240(%rbp), %rdx
	movq	-232(%rbp), %r9
	jmp	.L3759
.L3773:
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	7760(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_8WeakCellELi64EE4PushEiS2_
	jmp	.L3772
.L3802:
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	7064(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_9JSWeakRefELi64EE4PushEiS2_
	jmp	.L3801
.L3675:
	movq	%r12, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3681
	testb	$-128, %ah
	je	.L3679
.L3681:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3679
.L3738:
	movq	%rsi, %xmm6
	movq	%r12, %xmm2
	movq	-160(%rbp), %rax
	xorl	%esi, %esi
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm2, -208(%rbp)
	movq	-208(%rbp), %rdx
	movq	-200(%rbp), %rcx
	leaq	5672(%rax), %rdi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_
	jmp	.L3730
.L4098:
	movq	%r12, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3721
	testb	$-128, %ah
	je	.L3720
.L3721:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3720
.L4096:
	movq	%r12, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3744
	testb	$-128, %ah
	je	.L3743
.L3744:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3743
.L3777:
	movq	%r12, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3786
	testb	$-128, %ah
	je	.L3784
.L3786:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3784
.L4092:
	addl	%edx, %edx
	jne	.L3813
	addq	$4, %rcx
	movl	$1, %edx
.L3813:
	movl	(%rcx), %esi
	movl	%edx, %eax
	andl	%esi, %eax
	cmpl	%eax, %edx
	je	.L3816
	movl	%edx, %edi
	movl	%esi, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %esi
	jne	.L3813
	leaq	-128(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject4SizeEv
	leaq	15(%r13), %rsi
	cltq
	addq	%rax, 96(%r14)
	leaq	23(%r13), %rax
	cmpq	%rax, %rsi
	jnb	.L3816
	movq	15(%r13), %r15
	testb	$1, %r15b
	je	.L3816
	movq	%r15, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testb	$64, %al
	jne	.L3817
.L3822:
	movl	%r15d, %eax
	movl	$1, %edx
	subl	%ebx, %eax
	movl	%edx, %esi
	movq	16(%rbx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
.L3819:
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	je	.L3816
	movl	%esi, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	jne	.L3819
	movq	-160(%rbp), %rax
	movq	%r15, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L3816
.L4090:
	andq	$-262144, %rax
	movq	%rax, %rdi
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L3847
	testb	$-128, %ah
	je	.L3846
.L3847:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3846
.L4093:
	movq	-208(%rbp), %r12
	jmp	.L3806
.L4094:
	movq	%r12, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	testb	$64, %al
	jne	.L4102
.L3830:
	movl	%r12d, %eax
	movl	%r15d, %esi
	subl	%edx, %eax
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rcx
.L3836:
	movl	(%rcx), %edx
	movl	%edx, %eax
	andl	%esi, %eax
	cmpl	%esi, %eax
	je	.L3832
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	jne	.L3836
	movq	-160(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	104(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L3832
.L4101:
	movq	%r12, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3733
	testb	$-128, %ah
	je	.L3732
.L3733:
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3732
.L4097:
	leaq	744(%r13), %r15
	movq	%rcx, -208(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	784(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r14)
	movq	%r14, 784(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-208(%rbp), %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 104(%r13)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L3741
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%rbx, 16(%rdx,%rax,8)
	jmp	.L3741
.L4102:
	movq	%r14, %rdi
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L3833
	testb	$-128, %ah
	je	.L3830
.L3833:
	movq	%rbx, %rsi
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	movq	-192(%rbp), %rdx
	jmp	.L3830
.L3838:
	movq	%r14, %xmm3
	movq	%rbx, %xmm7
	movq	-160(%rbp), %rax
	xorl	%esi, %esi
	punpcklqdq	%xmm7, %xmm3
	movaps	%xmm3, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %rcx
	leaq	5672(%rax), %rdi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE4PushEiS5_
	jmp	.L3832
.L4069:
	call	__stack_chk_fail@PLT
.L4099:
	leaq	744(%r13), %r15
	movq	%rcx, -208(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	784(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r14)
	movq	%r14, 784(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-208(%rbp), %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 104(%r13)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L3718
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%rbx, 16(%rdx,%rax,8)
	jmp	.L3718
.L3817:
	movq	8(%r14), %rax
	testb	$88, %al
	je	.L3824
	testb	$-128, %ah
	je	.L3822
.L3824:
	movq	%r14, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L3822
.L4100:
	leaq	.LC32(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4095:
	leaq	744(%r13), %rdi
	movq	%rcx, -216(%rbp)
	movq	%rdi, -208(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	784(%r13), %rax
	movq	%rax, (%r15)
	movq	%r15, 784(%r13)
	movq	-208(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-216(%rbp), %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 104(%r13)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L3779
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%r14, 16(%rdx,%rax,8)
	jmp	.L3779
	.cfi_endproc
.LFE22405:
	.size	_ZN2v88internal18IncrementalMarking13RevisitObjectENS0_10HeapObjectE, .-_ZN2v88internal18IncrementalMarking13RevisitObjectENS0_10HeapObjectE
	.section	.rodata._ZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC42:
	.string	"V8.GCIncrementalMarkingLayoutChange"
	.section	.text._ZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectE
	.type	_ZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectE, @function
_ZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectE:
.LFB22356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4156
.L4105:
	movq	$0, -208(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L4157
.L4107:
	movq	0(%r13), %rax
	leaq	-144(%rbp), %r14
	movl	$7, %edx
	movq	%r14, %rdi
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectEE28trace_event_unique_atomic103(%rip), %rdx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L4158
.L4112:
	movq	$0, -176(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L4159
.L4114:
	movl	%r12d, %eax
	movq	%r12, %rdx
	movl	$1, %esi
	andl	$262143, %eax
	andq	$-262144, %rdx
	movl	%eax, %ecx
	movq	16(%rdx), %rdx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	leaq	(%rdx,%rax,4), %rsi
	jmp	.L4119
	.p2align 4,,10
	.p2align 3
.L4160:
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L4118
.L4119:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	jne	.L4160
.L4118:
	movq	%r12, %rbx
	leaq	-1(%r12), %rdx
	movl	$1, %eax
	movq	%r12, -216(%rbp)
	andq	$-262144, %rbx
	subl	%ebx, %edx
	movl	%edx, %ecx
	shrl	$8, %edx
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movq	16(%rbx), %rax
	leaq	(%rax,%rdx,4), %rsi
	movl	(%rsi), %eax
	testl	%ecx, %eax
	jne	.L4161
.L4124:
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4162
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4156:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4163
.L4106:
	movq	%rbx, _ZZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectEE28trace_event_unique_atomic102(%rip)
	jmp	.L4105
	.p2align 4,,10
	.p2align 3
.L4161:
	addl	%ecx, %ecx
	je	.L4164
	.p2align 4,,10
	.p2align 3
.L4122:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L4124
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L4122
	movq	-216(%rbp), %rax
	leaq	-216(%rbp), %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	cltq
	addq	%rax, 96(%rbx)
	call	_ZN2v88internal18IncrementalMarking13RevisitObjectENS0_10HeapObjectE
	jmp	.L4124
	.p2align 4,,10
	.p2align 3
.L4159:
	movl	$7, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -232(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4165
.L4115:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4116
	movq	(%rdi), %rax
	call	*8(%rax)
.L4116:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4117
	movq	(%rdi), %rax
	call	*8(%rax)
.L4117:
	movl	$7, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%r15, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L4114
	.p2align 4,,10
	.p2align 3
.L4158:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4166
.L4113:
	movq	%r15, _ZZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectEE28trace_event_unique_atomic103(%rip)
	jmp	.L4112
	.p2align 4,,10
	.p2align 3
.L4157:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4167
.L4108:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4109
	movq	(%rdi), %rax
	call	*8(%rax)
.L4109:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4110
	movq	(%rdi), %rax
	call	*8(%rax)
.L4110:
	leaq	.LC42(%rip), %rax
	movq	%rbx, -200(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-200(%rbp), %rax
	movq	%r14, -184(%rbp)
	movq	%rax, -208(%rbp)
	jmp	.L4107
	.p2align 4,,10
	.p2align 3
.L4164:
	addq	$4, %rsi
	movl	$1, %ecx
	jmp	.L4122
.L4165:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r15, %rdx
	movl	$88, %esi
	pushq	%rcx
	movq	-232(%rbp), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L4115
.L4163:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L4106
.L4167:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC42(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L4108
.L4166:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L4113
.L4162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22356:
	.size	_ZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectE, .-_ZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectE
	.section	.text._ZN2v88internal18IncrementalMarking18NotifyLeftTrimmingENS0_10HeapObjectES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking18NotifyLeftTrimmingENS0_10HeapObjectES2_
	.type	_ZN2v88internal18IncrementalMarking18NotifyLeftTrimmingENS0_10HeapObjectES2_, @function
_ZN2v88internal18IncrementalMarking18NotifyLeftTrimmingENS0_10HeapObjectES2_:
.LFB22357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	andq	$-262144, %rdx
	movl	%r13d, %eax
	pushq	%r12
	andl	$262143, %eax
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	$1, %ebx
	movq	16(%rdx), %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	leaq	(%rdx,%rax,4), %r12
	sall	%cl, %ebx
	cmpb	$0, 87(%rdi)
	je	.L4172
	movl	(%r12), %eax
	testl	%eax, %ebx
	jne	.L4192
.L4172:
	movq	%r14, %rsi
	subq	$1, %r13
	addq	$7, %r14
	call	_ZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectE
	cmpq	%r13, %r14
	jne	.L4171
	jmp	.L4193
	.p2align 4,,10
	.p2align 3
.L4195:
	movl	%ebx, %ecx
	movl	%edx, %eax
	orl	%edx, %ecx
	lock cmpxchgl	%ecx, (%r12)
	cmpl	%eax, %edx
	je	.L4194
.L4171:
	movl	(%r12), %edx
	movl	%ebx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ebx
	jne	.L4195
.L4168:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4193:
	.cfi_restore_state
	addl	%ebx, %ebx
	jne	.L4176
	movl	$1, %ebx
	movl	4(%r12), %edx
	addq	$4, %r12
	movl	%ebx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ebx
	je	.L4168
	.p2align 4,,10
	.p2align 3
.L4196:
	movl	%ebx, %ecx
	movl	%edx, %eax
	orl	%edx, %ecx
	lock cmpxchgl	%ecx, (%r12)
	cmpl	%eax, %edx
	je	.L4168
.L4176:
	movl	(%r12), %edx
	movl	%ebx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ebx
	jne	.L4196
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4192:
	.cfi_restore_state
	movl	%ebx, %eax
	movq	%r12, %rdx
	addl	%eax, %eax
	jne	.L4173
	leaq	4(%r12), %rdx
	movl	$1, %eax
.L4173:
	movl	(%rdx), %edx
	testl	%eax, %edx
	jne	.L4168
	jmp	.L4172
	.p2align 4,,10
	.p2align 3
.L4194:
	addl	%ebx, %ebx
	jne	.L4178
	addq	$4, %r12
	movl	$1, %ebx
	jmp	.L4178
	.p2align 4,,10
	.p2align 3
.L4197:
	movl	%edx, %ecx
	movl	%edx, %eax
	orl	%ebx, %ecx
	lock cmpxchgl	%ecx, (%r12)
	cmpl	%eax, %edx
	je	.L4168
.L4178:
	movl	(%r12), %edx
	movl	%edx, %eax
	andl	%ebx, %eax
	cmpl	%ebx, %eax
	jne	.L4197
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22357:
	.size	_ZN2v88internal18IncrementalMarking18NotifyLeftTrimmingENS0_10HeapObjectES2_, .-_ZN2v88internal18IncrementalMarking18NotifyLeftTrimmingENS0_10HeapObjectES2_
	.section	.text._ZN2v88internal18IncrementalMarking27ProcessBlackAllocatedObjectENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking27ProcessBlackAllocatedObjectENS0_10HeapObjectE
	.type	_ZN2v88internal18IncrementalMarking27ProcessBlackAllocatedObjectENS0_10HeapObjectE, @function
_ZN2v88internal18IncrementalMarking27ProcessBlackAllocatedObjectENS0_10HeapObjectE:
.LFB22404:
	.cfi_startproc
	endbr64
	cmpl	$1, 80(%rdi)
	jg	.L4209
.L4198:
	ret
	.p2align 4,,10
	.p2align 3
.L4209:
	movl	%esi, %eax
	movq	%rsi, %r8
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r8
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r8), %rcx
	leaq	(%rcx,%rax,4), %rax
	movl	(%rax), %ecx
	testl	%edx, %ecx
	je	.L4198
	addl	%edx, %edx
	jne	.L4202
	addq	$4, %rax
	movl	$1, %edx
.L4202:
	movl	(%rax), %eax
	testl	%eax, %edx
	je	.L4198
	jmp	_ZN2v88internal18IncrementalMarking13RevisitObjectENS0_10HeapObjectE
	.cfi_endproc
.LFE22404:
	.size	_ZN2v88internal18IncrementalMarking27ProcessBlackAllocatedObjectENS0_10HeapObjectE, .-_ZN2v88internal18IncrementalMarking27ProcessBlackAllocatedObjectENS0_10HeapObjectE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal18IncrementalMarking8Observer4StepEimm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal18IncrementalMarking8Observer4StepEimm, @function
_GLOBAL__sub_I__ZN2v88internal18IncrementalMarking8Observer4StepEimm:
.LFB28683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28683:
	.size	_GLOBAL__sub_I__ZN2v88internal18IncrementalMarking8Observer4StepEimm, .-_GLOBAL__sub_I__ZN2v88internal18IncrementalMarking8Observer4StepEimm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal18IncrementalMarking8Observer4StepEimm
	.section	.rodata._ZN2v88internal18IncrementalMarking4StopEv.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC43:
	.string	"[IncrementalMarking] Stopping: old generation %dMB, limit %dMB, overshoot %dMB\n"
	.section	.text._ZN2v88internal18IncrementalMarking4StopEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal18IncrementalMarking4StopEv.part.0, @function
_ZN2v88internal18IncrementalMarking4StopEv.part.0:
.LFB28978:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	jne	.L4223
.L4213:
	leaq	-64(%rbp), %r12
	leaq	128(%rbx), %r13
	movq	%r12, %rdi
	call	_ZN2v88internal13SpaceIteratorC1EPNS0_4HeapE@PLT
.L4214:
	movq	%r12, %rdi
	call	_ZN2v88internal13SpaceIterator7HasNextEv@PLT
	testb	%al, %al
	je	.L4215
.L4225:
	movq	%r12, %rdi
	call	_ZN2v88internal13SpaceIterator4NextEv@PLT
	movq	%rax, %rdi
	movq	(%rbx), %rax
	cmpq	248(%rax), %rdi
	je	.L4224
	movq	(%rdi), %rax
	movq	%r13, %rsi
	call	*24(%rax)
	movq	%r12, %rdi
	call	_ZN2v88internal13SpaceIterator7HasNextEv@PLT
	testb	%al, %al
	jne	.L4225
.L4215:
	movq	(%rbx), %rax
	movb	$0, 85(%rbx)
	movl	$2, %esi
	leaq	-80(%rax), %rdi
	call	_ZN2v88internal10StackGuard14ClearInterruptENS1_13InterruptFlagE@PLT
	movq	(%rbx), %rax
	movl	$0, 80(%rbx)
	movb	$0, 2736(%rax)
	cmpb	$0, 87(%rbx)
	movb	$0, 84(%rbx)
	je	.L4219
	cmpb	$0, _ZN2v88internal30FLAG_trace_incremental_markingE(%rip)
	movb	$0, 87(%rbx)
	jne	.L4226
.L4219:
	movq	%r12, %rdi
	call	_ZN2v88internal13SpaceIteratorD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4227
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4224:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	96(%rbx), %rsi
	call	*24(%rax)
	jmp	.L4214
	.p2align 4,,10
	.p2align 3
.L4223:
	movq	%rsi, %rdi
	call	_ZN2v88internal4Heap26OldGenerationSizeOfObjectsEv@PLT
	movq	(%rbx), %rdi
	leaq	.LC43(%rip), %rsi
	shrq	$20, %rax
	movq	1504(%rdi), %rcx
	movl	%eax, %r8d
	movq	%rax, %rdx
	movl	$0, %eax
	shrq	$20, %rcx
	subl	%ecx, %r8d
	cmovs	%eax, %r8d
	subq	$37592, %rdi
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movq	(%rbx), %rsi
	jmp	.L4213
	.p2align 4,,10
	.p2align 3
.L4226:
	movq	(%rbx), %rdi
	leaq	.LC16(%rip), %rsi
	xorl	%eax, %eax
	subq	$37592, %rdi
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L4219
.L4227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28978:
	.size	_ZN2v88internal18IncrementalMarking4StopEv.part.0, .-_ZN2v88internal18IncrementalMarking4StopEv.part.0
	.section	.text._ZN2v88internal18IncrementalMarking4StopEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking4StopEv
	.type	_ZN2v88internal18IncrementalMarking4StopEv, @function
_ZN2v88internal18IncrementalMarking4StopEv:
.LFB22410:
	.cfi_startproc
	endbr64
	movl	80(%rdi), %eax
	testl	%eax, %eax
	je	.L4228
	jmp	_ZN2v88internal18IncrementalMarking4StopEv.part.0
	.p2align 4,,10
	.p2align 3
.L4228:
	ret
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal18IncrementalMarking4StopEv, .-_ZN2v88internal18IncrementalMarking4StopEv
	.section	.text._ZN2v88internal18IncrementalMarking8FinalizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18IncrementalMarking8FinalizeEv
	.type	_ZN2v88internal18IncrementalMarking8FinalizeEv, @function
_ZN2v88internal18IncrementalMarking8FinalizeEv:
.LFB22411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	8(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L4233
	movq	(%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L4236
.L4233:
	movq	%r12, %rdi
	call	_ZN2v88internal18IncrementalMarking5HurryEv.part.0
.L4232:
	movl	80(%r12), %eax
	testl	%eax, %eax
	je	.L4230
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal18IncrementalMarking4StopEv.part.0
	.p2align 4,,10
	.p2align 3
.L4236:
	.cfi_restore_state
	movq	704(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L4233
	movq	696(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L4233
	movq	680(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L4233
	movq	1376(%rax), %rax
	testq	%rax, %rax
	jne	.L4233
	jmp	.L4232
	.p2align 4,,10
	.p2align 3
.L4230:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22411:
	.size	_ZN2v88internal18IncrementalMarking8FinalizeEv, .-_ZN2v88internal18IncrementalMarking8FinalizeEv
	.weak	_ZTVN2v88internal18IncrementalMarking8ObserverE
	.section	.data.rel.ro.local._ZTVN2v88internal18IncrementalMarking8ObserverE,"awG",@progbits,_ZTVN2v88internal18IncrementalMarking8ObserverE,comdat
	.align 8
	.type	_ZTVN2v88internal18IncrementalMarking8ObserverE, @object
	.size	_ZTVN2v88internal18IncrementalMarking8ObserverE, 48
_ZTVN2v88internal18IncrementalMarking8ObserverE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal18IncrementalMarking8ObserverD1Ev
	.quad	_ZN2v88internal18IncrementalMarking8ObserverD0Ev
	.quad	_ZN2v88internal18IncrementalMarking8Observer4StepEimm
	.quad	_ZN2v88internal18AllocationObserver15GetNextStepSizeEv
	.weak	_ZTVN2v88internal36IncrementalMarkingRootMarkingVisitorE
	.section	.data.rel.ro.local._ZTVN2v88internal36IncrementalMarkingRootMarkingVisitorE,"awG",@progbits,_ZTVN2v88internal36IncrementalMarkingRootMarkingVisitorE,comdat
	.align 8
	.type	_ZTVN2v88internal36IncrementalMarkingRootMarkingVisitorE, @object
	.size	_ZTVN2v88internal36IncrementalMarkingRootMarkingVisitorE, 56
_ZTVN2v88internal36IncrementalMarkingRootMarkingVisitorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD1Ev
	.quad	_ZN2v88internal36IncrementalMarkingRootMarkingVisitorD0Ev
	.quad	_ZN2v88internal36IncrementalMarkingRootMarkingVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal36IncrementalMarkingRootMarkingVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.weak	_ZTVN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEE
	.section	.data.rel.ro._ZTVN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEE,"awG",@progbits,_ZTVN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEE,comdat
	.align 8
	.type	_ZTVN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEE, @object
	.size	_ZTVN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEE, 152
_ZTVN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED1Ev
	.quad	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEED0Ev
	.quad	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_
	.quad	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES7_
	.quad	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES7_
	.quad	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal14MarkingVisitorILNS0_24FixedArrayVisitationModeE1ELNS0_22TraceRetainingPathModeE1ENS0_23IncrementalMarkingStateEE20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.quad	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE
	.section	.bss._ZZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginEE29trace_event_unique_atomic1110,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginEE29trace_event_unique_atomic1110, @object
	.size	_ZZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginEE29trace_event_unique_atomic1110, 8
_ZZN2v88internal18IncrementalMarking6V8StepEdNS1_16CompletionActionENS0_10StepOriginEE29trace_event_unique_atomic1110:
	.zero	8
	.section	.bss._ZZN2v88internal18IncrementalMarking19AdvanceOnAllocationEvE29trace_event_unique_atomic1098,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18IncrementalMarking19AdvanceOnAllocationEvE29trace_event_unique_atomic1098, @object
	.size	_ZZN2v88internal18IncrementalMarking19AdvanceOnAllocationEvE29trace_event_unique_atomic1098, 8
_ZZN2v88internal18IncrementalMarking19AdvanceOnAllocationEvE29trace_event_unique_atomic1098:
	.zero	8
	.section	.bss._ZZN2v88internal18IncrementalMarking19AdvanceOnAllocationEvE29trace_event_unique_atomic1097,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18IncrementalMarking19AdvanceOnAllocationEvE29trace_event_unique_atomic1097, @object
	.size	_ZZN2v88internal18IncrementalMarking19AdvanceOnAllocationEvE29trace_event_unique_atomic1097, 8
_ZZN2v88internal18IncrementalMarking19AdvanceOnAllocationEvE29trace_event_unique_atomic1097:
	.zero	8
	.section	.bss._ZZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginEE28trace_event_unique_atomic964,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginEE28trace_event_unique_atomic964, @object
	.size	_ZZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginEE28trace_event_unique_atomic964, 8
_ZZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginEE28trace_event_unique_atomic964:
	.zero	8
	.section	.bss._ZZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginEE28trace_event_unique_atomic963,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginEE28trace_event_unique_atomic963, @object
	.size	_ZZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginEE28trace_event_unique_atomic963, 8
_ZZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginEE28trace_event_unique_atomic963:
	.zero	8
	.section	.bss._ZZN2v88internal18IncrementalMarking12EmbedderStepEdE28trace_event_unique_atomic768,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18IncrementalMarking12EmbedderStepEdE28trace_event_unique_atomic768, @object
	.size	_ZZN2v88internal18IncrementalMarking12EmbedderStepEdE28trace_event_unique_atomic768, 8
_ZZN2v88internal18IncrementalMarking12EmbedderStepEdE28trace_event_unique_atomic768:
	.zero	8
	.section	.bss._ZZN2v88internal18IncrementalMarking21FinalizeIncrementallyEvE28trace_event_unique_atomic496,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18IncrementalMarking21FinalizeIncrementallyEvE28trace_event_unique_atomic496, @object
	.size	_ZZN2v88internal18IncrementalMarking21FinalizeIncrementallyEvE28trace_event_unique_atomic496, 8
_ZZN2v88internal18IncrementalMarking21FinalizeIncrementallyEvE28trace_event_unique_atomic496:
	.zero	8
	.section	.bss._ZZN2v88internal18IncrementalMarking12StartMarkingEvE28trace_event_unique_atomic369,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18IncrementalMarking12StartMarkingEvE28trace_event_unique_atomic369, @object
	.size	_ZZN2v88internal18IncrementalMarking12StartMarkingEvE28trace_event_unique_atomic369, 8
_ZZN2v88internal18IncrementalMarking12StartMarkingEvE28trace_event_unique_atomic369:
	.zero	8
	.section	.bss._ZZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonEE28trace_event_unique_atomic290,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonEE28trace_event_unique_atomic290, @object
	.size	_ZZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonEE28trace_event_unique_atomic290, 8
_ZZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonEE28trace_event_unique_atomic290:
	.zero	8
	.section	.bss._ZZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonEE28trace_event_unique_atomic289,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonEE28trace_event_unique_atomic289, @object
	.size	_ZZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonEE28trace_event_unique_atomic289, 8
_ZZN2v88internal18IncrementalMarking5StartENS0_23GarbageCollectionReasonEE28trace_event_unique_atomic289:
	.zero	8
	.section	.bss._ZZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectEE28trace_event_unique_atomic103,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectEE28trace_event_unique_atomic103, @object
	.size	_ZZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectEE28trace_event_unique_atomic103, 8
_ZZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectEE28trace_event_unique_atomic103:
	.zero	8
	.section	.bss._ZZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectEE28trace_event_unique_atomic102, 8
_ZZN2v88internal18IncrementalMarking40MarkBlackAndVisitObjectDueToLayoutChangeENS0_10HeapObjectEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.quad	65536
	.quad	65536
	.align 16
.LC4:
	.quad	262144
	.quad	262144
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC20:
	.long	0
	.long	1072693248
	.align 8
.LC21:
	.long	0
	.long	1082081280
	.align 8
.LC22:
	.long	0
	.long	1076101120
	.align 8
.LC23:
	.long	0
	.long	1138753536
	.align 8
.LC40:
	.long	0
	.long	1071644672
	.align 8
.LC41:
	.long	0
	.long	1075052544
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
