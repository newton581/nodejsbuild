	.file	"objects-printer.cc"
	.text
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB27626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE27626:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB27752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27752:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB27627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27627:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB27753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE27753:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal10HeapNumber15HeapNumberPrintERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10HeapNumber15HeapNumberPrintERSo
	.type	_ZN2v88internal10HeapNumber15HeapNumberPrintERSo, @function
_ZN2v88internal10HeapNumber15HeapNumberPrintERSo:
.LFB21757:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	(%r8), %rax
	movq	7(%rax), %xmm0
	jmp	_ZNSo9_M_insertIdEERSoT_@PLT
	.cfi_endproc
.LFE21757:
	.size	_ZN2v88internal10HeapNumber15HeapNumberPrintERSo, .-_ZN2v88internal10HeapNumber15HeapNumberPrintERSo
	.section	.rodata._ZN2v88internal4Name14NameShortPrintEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s"
.LC1:
	.string	"#<%s>"
.LC2:
	.string	"<%s>"
	.section	.text._ZN2v88internal4Name14NameShortPrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Name14NameShortPrintEv
	.type	_ZN2v88internal4Name14NameShortPrintEv, @function
_ZN2v88internal4Name14NameShortPrintEv:
.LFB21758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L29
	movq	%rax, -32(%rbp)
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L30
.L15:
	leaq	-16(%rbp), %rdi
	leaq	-24(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-16(%rbp), %rsi
	leaq	.LC2(%rip), %rdi
.L28:
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdaPv@PLT
.L11:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	jne	.L15
	leaq	-32(%rbp), %rdi
	call	_ZNK2v88internal6Symbol19PrivateSymbolToNameEv@PLT
	leaq	.LC1(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	-16(%rbp), %rdi
	leaq	-24(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-16(%rbp), %rsi
	leaq	.LC0(%rip), %rdi
	jmp	.L28
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21758:
	.size	_ZN2v88internal4Name14NameShortPrintEv, .-_ZN2v88internal4Name14NameShortPrintEv
	.section	.text._ZN2v88internal4Name14NameShortPrintENS0_6VectorIcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Name14NameShortPrintENS0_6VectorIcEE
	.type	_ZN2v88internal4Name14NameShortPrintENS0_6VectorIcEE, @function
_ZN2v88internal4Name14NameShortPrintENS0_6VectorIcEE:
.LFB21759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L50
	movq	%rax, -48(%rbp)
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L51
.L36:
	movl	$1, %ecx
	leaq	-32(%rbp), %rdi
	leaq	-40(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-32(%rbp), %rcx
	leaq	.LC2(%rip), %rdx
.L49:
	movq	%r13, %rdi
	movq	%r12, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-32(%rbp), %rdi
	movl	%eax, %r12d
	testq	%rdi, %rdi
	je	.L32
	call	_ZdaPv@PLT
.L32:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	jne	.L36
	leaq	-48(%rbp), %rdi
	call	_ZNK2v88internal6Symbol19PrivateSymbolToNameEv@PLT
	movq	%r12, %rsi
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movl	%eax, %r12d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$1, %ecx
	leaq	-32(%rbp), %rdi
	leaq	-40(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-32(%rbp), %rcx
	leaq	.LC0(%rip), %rdx
	jmp	.L49
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21759:
	.size	_ZN2v88internal4Name14NameShortPrintENS0_6VectorIcEE, .-_ZN2v88internal4Name14NameShortPrintENS0_6VectorIcEE
	.section	.rodata._ZN2v88internal3Map8MapPrintERSo.str1.1,"aMS",@progbits,1
.LC3:
	.string	"(own) "
.LC4:
	.string	""
.LC5:
	.string	"Map="
.LC6:
	.string	"\n - type: "
.LC7:
	.string	"\n - instance size: "
.LC8:
	.string	"variable"
.LC9:
	.string	"\n - inobject properties: "
.LC10:
	.string	"\n - elements kind: "
.LC11:
	.string	"\n - unused property fields: "
.LC12:
	.string	"\n - enum length: "
.LC13:
	.string	"invalid"
.LC14:
	.string	"\n - deprecated_map"
.LC15:
	.string	"\n - stable_map"
.LC16:
	.string	"\n - migration_target"
.LC17:
	.string	"\n - dictionary_map"
.LC18:
	.string	"\n - named_interceptor"
.LC19:
	.string	"\n - indexed_interceptor"
	.section	.rodata._ZN2v88internal3Map8MapPrintERSo.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"\n - may_have_interesting_symbols"
	.section	.rodata._ZN2v88internal3Map8MapPrintERSo.str1.1
.LC21:
	.string	"\n - undetectable"
.LC22:
	.string	"\n - callable"
.LC23:
	.string	"\n - constructor"
.LC24:
	.string	"\n - has_prototype_slot"
.LC25:
	.string	" (non-instance prototype)"
.LC26:
	.string	"\n - access_check_needed"
.LC27:
	.string	"\n - non-extensible"
.LC28:
	.string	"\n - prototype_map"
.LC29:
	.string	"\n - prototype info: "
.LC30:
	.string	"\n - back pointer: "
.LC31:
	.string	"\n - prototype_validity cell: "
.LC32:
	.string	"\n - instance descriptors "
.LC33:
	.string	"unreachable code"
.LC34:
	.string	"\n - transitions #"
.LC35:
	.string	": "
.LC36:
	.string	"\n - prototype: "
.LC37:
	.string	"\n - constructor: "
.LC38:
	.string	"\n - dependent code: "
.LC39:
	.string	"\n - construction counter: "
.LC40:
	.string	"\n"
.LC41:
	.string	"#"
.LC42:
	.string	"\n - layout descriptor: "
	.section	.text._ZN2v88internal3Map8MapPrintERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map8MapPrintERSo
	.type	_ZN2v88internal3Map8MapPrintERSo, @function
_ZN2v88internal3Map8MapPrintERSo:
.LFB21761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	leaq	.LC5(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$10, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movzwl	11(%rax), %esi
	call	_ZN2v88internallsERSoNS0_12InstanceTypeE@PLT
	movl	$19, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movzbl	7(%rax), %edx
	testb	%dl, %dl
	jne	.L54
	movl	$8, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	cmpw	$1024, 11(%rax)
	ja	.L141
.L56:
	movl	$19, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movzbl	14(%rax), %edi
	shrl	$3, %edi
	call	_ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L142
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L58:
	leaq	.LC11(%rip), %rsi
	movl	$28, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movzbl	9(%rax), %esi
	cmpl	$2, %esi
	jg	.L143
.L59:
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$17, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdx
	movl	15(%rdx), %eax
	andl	$1023, %eax
	cmpl	$1023, %eax
	je	.L144
	movl	15(%rdx), %esi
	movq	%r12, %rdi
	andl	$1023, %esi
	call	_ZNSolsEi@PLT
	movq	(%rbx), %rax
	movl	15(%rax), %edx
	andl	$16777216, %edx
	jne	.L145
.L62:
	movl	15(%rax), %edx
	andl	$33554432, %edx
	je	.L146
.L63:
	movl	15(%rax), %edx
	andl	$67108864, %edx
	jne	.L147
.L64:
	movl	15(%rax), %edx
	andl	$2097152, %edx
	jne	.L148
.L65:
	movzbl	13(%rax), %edx
	testb	$4, %dl
	jne	.L149
.L66:
	andl	$8, %edx
	jne	.L150
.L67:
	movl	15(%rax), %edx
	andl	$268435456, %edx
	jne	.L151
.L68:
	movzbl	13(%rax), %edx
	leaq	13(%rax), %rcx
	testb	$16, %dl
	jne	.L152
.L69:
	testb	$2, %dl
	jne	.L153
.L70:
	andl	$64, %edx
	jne	.L154
.L71:
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	js	.L155
.L73:
	testb	$32, 13(%rax)
	jne	.L156
.L75:
	movl	15(%rax), %edx
	andl	$134217728, %edx
	je	.L157
.L76:
	movl	15(%rax), %eax
	testl	$1048576, %eax
	je	.L77
	movl	$17, %edx
	leaq	.LC28(%rip), %rsi
	leaq	-80(%rbp), %r13
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	movl	$20, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	71(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
.L78:
	movl	$29, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	63(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$25, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movl	15(%rax), %eax
	andl	$4194304, %eax
	cmpl	$1, %eax
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$6, %edx
	testl	%eax, %eax
	leaq	.LC4(%rip), %rax
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	15(%rax), %esi
	shrl	$10, %esi
	andl	$1023, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC35(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	39(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$23, %edx
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	47(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintERSo@PLT
	movq	(%rbx), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	testb	$32, 10(%rdx)
	je	.L158
.L98:
	movl	$15, %edx
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	23(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$17, %edx
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
.L140:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L159
.L95:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$20, %edx
	leaq	.LC38(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	55(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$26, %edx
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	15(%rax), %esi
	shrl	$29, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L160
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movzbl	7(%rax), %esi
	movq	%r12, %rdi
	sall	$3, %esi
	call	_ZNSolsEi@PLT
	movq	(%rbx), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L56
.L141:
	movq	%r12, %rdi
	movl	$25, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movzbl	7(%rax), %esi
	movzbl	8(%rax), %eax
	subl	%eax, %esi
	call	_ZNSolsEi@PLT
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$18, %edx
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L161
.L79:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rdx
.L80:
	leaq	-80(%rbp), %r13
	movq	%r12, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r13, %rsi
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rsi
	movq	-1(%rdx), %rcx
	cmpq	%rcx, -37456(%rsi)
	jne	.L79
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L159:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L95
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$7, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	15(%rax), %edx
	andl	$16777216, %edx
	je	.L62
.L145:
	movl	$18, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	15(%rax), %edx
	andl	$33554432, %edx
	jne	.L63
.L146:
	movl	$14, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	15(%rax), %edx
	andl	$67108864, %edx
	je	.L64
.L147:
	movl	$20, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	15(%rax), %edx
	andl	$2097152, %edx
	je	.L65
.L148:
	movl	$18, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movzbl	13(%rax), %edx
	testb	$4, %dl
	je	.L66
.L149:
	movl	$21, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movzbl	13(%rax), %edx
	andl	$8, %edx
	je	.L67
.L150:
	movl	$23, %edx
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	15(%rax), %edx
	andl	$268435456, %edx
	je	.L68
.L151:
	movl	$32, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movzbl	13(%rax), %edx
	leaq	13(%rax), %rcx
	testb	$16, %dl
	je	.L69
.L152:
	movl	$16, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movzbl	13(%rax), %edx
	leaq	13(%rax), %rcx
	testb	$2, %dl
	je	.L70
.L153:
	movl	$12, %edx
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movzbl	13(%rax), %edx
	leaq	13(%rax), %rcx
	andl	$64, %edx
	je	.L71
.L154:
	movl	$15, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movzbl	13(%rax), %edx
	leaq	13(%rax), %rcx
	testb	%dl, %dl
	jns	.L73
.L155:
	movl	$22, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movzbl	13(%rax), %edx
	andl	$1, %edx
	je	.L73
	movl	$25, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L158:
	movq	24(%rdx), %rdx
	movq	$0, -56(%rbp)
	movq	$0, -72(%rbp)
	subq	$37592, %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, -80(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -56(%rbp)
	testb	$1, %al
	je	.L82
	cmpl	$3, %eax
	je	.L82
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L162
	cmpq	$1, %rdx
	je	.L163
	leaq	.LC33(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$1, -48(%rbp)
.L84:
	movq	%r13, %rdi
	call	_ZN2v88internal19TransitionsAccessor19NumberOfTransitionsEv@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jle	.L98
	movl	$17, %edx
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC35(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	71(%rax), %rax
	testb	$1, %al
	je	.L139
	cmpl	$3, %eax
	je	.L98
	andq	$-3, %rax
.L139:
	leaq	-88(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L157:
	movl	$18, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L143:
	movzbl	7(%rax), %eax
	subl	%esi, %eax
	movl	%eax, %esi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L156:
	movl	$23, %edx
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L142:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L162:
	movl	$3, -48(%rbp)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L163:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L164
	movl	$4, -48(%rbp)
	jmp	.L84
.L164:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -48(%rbp)
	jmp	.L84
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21761:
	.size	_ZN2v88internal3Map8MapPrintERSo, .-_ZN2v88internal3Map8MapPrintERSo
	.section	.rodata._ZN2v88internal15DescriptorArray22PrintDescriptorDetailsERSoiNS0_15PropertyDetails9PrintModeE.str1.1,"aMS",@progbits,1
.LC43:
	.string	" @ "
.LC44:
	.string	"(get: "
.LC45:
	.string	", set: "
.LC46:
	.string	")"
	.section	.text._ZN2v88internal15DescriptorArray22PrintDescriptorDetailsERSoiNS0_15PropertyDetails9PrintModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15DescriptorArray22PrintDescriptorDetailsERSoiNS0_15PropertyDetails9PrintModeE
	.type	_ZN2v88internal15DescriptorArray22PrintDescriptorDetailsERSoiNS0_15PropertyDetails9PrintModeE, @function
_ZN2v88internal15DescriptorArray22PrintDescriptorDetailsERSoiNS0_15PropertyDetails9PrintModeE:
.LFB21763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movl	%ecx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leal	3(%r8,%r8,2), %ebx
	sall	$3, %ebx
	movslq	%ebx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-60(%rbp), %rdi
	movq	7(%rbx,%rax), %rax
	sarq	$32, %rax
	movl	%eax, -60(%rbp)
	call	_ZN2v88internal15PropertyDetails13PrintAsFastToERSoNS1_9PrintModeE@PLT
	movl	$3, %edx
	leaq	.LC43(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	testb	$2, -60(%rbp)
	jne	.L166
	movq	15(%rbx,%rax), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9FieldType7PrintToERSo@PLT
.L165:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	15(%rbx,%rax), %rbx
	leaq	-48(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rbx, -48(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	testb	$1, %bl
	je	.L165
	movq	-1(%rbx), %rax
	cmpw	$79, 11(%rax)
	jne	.L165
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC44(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	7(%rbx), %rax
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$7, %edx
	leaq	.LC45(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	15(%rbx), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$1, %edx
	leaq	.LC46(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L165
.L172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21763:
	.size	_ZN2v88internal15DescriptorArray22PrintDescriptorDetailsERSoiNS0_15PropertyDetails9PrintModeE, .-_ZN2v88internal15DescriptorArray22PrintDescriptorDetailsERSoiNS0_15PropertyDetails9PrintModeE
	.section	.rodata._ZN2v88internal15DescriptorArray16PrintDescriptorsERSo.str1.1,"aMS",@progbits,1
.LC47:
	.string	"\n  ["
.LC48:
	.string	"]: "
.LC49:
	.string	" "
	.section	.text._ZN2v88internal15DescriptorArray16PrintDescriptorsERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15DescriptorArray16PrintDescriptorsERSo
	.type	_ZN2v88internal15DescriptorArray16PrintDescriptorsERSo, @function
_ZN2v88internal15DescriptorArray16PrintDescriptorsERSo:
.LFB21762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$24, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-64(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L178:
	movq	-1(%r14,%rdx), %rax
	movq	%r12, %rdi
	leal	1(%r13), %r15d
	addq	$24, %r14
	movl	$4, %edx
	leaq	.LC47(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$3, %edx
	leaq	.LC48(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal6Object10ShortPrintERSo@PLT
	movl	$1, %edx
	leaq	.LC49(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %edx
	movl	$-1, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%r15d, %r13d
	call	_ZN2v88internal15DescriptorArray22PrintDescriptorDetailsERSoiNS0_15PropertyDetails9PrintModeE
.L175:
	movq	(%rbx), %rdx
	movswl	9(%rdx), %eax
	cmpl	%r13d, %eax
	jg	.L178
	movl	$1, %edx
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L179:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21762:
	.size	_ZN2v88internal15DescriptorArray16PrintDescriptorsERSo, .-_ZN2v88internal15DescriptorArray16PrintDescriptorsERSo
	.section	.text._ZN2v88internal3Map15PrintMapDetailsERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map15PrintMapDetailsERSo
	.type	_ZN2v88internal3Map15PrintMapDetailsERSo, @function
_ZN2v88internal3Map15PrintMapDetailsERSo:
.LFB21760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal3Map8MapPrintERSo
	movq	(%rbx), %rax
	leaq	-32(%rbp), %rdi
	movq	%r12, %rsi
	movq	39(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal15DescriptorArray16PrintDescriptorsERSo
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L183
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L183:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21760:
	.size	_ZN2v88internal3Map15PrintMapDetailsERSo, .-_ZN2v88internal3Map15PrintMapDetailsERSo
	.section	.text._Z23_v8_internal_Get_ObjectPv,"ax",@progbits
	.p2align 4
	.globl	_Z23_v8_internal_Get_ObjectPv
	.type	_Z23_v8_internal_Get_ObjectPv, @function
_Z23_v8_internal_Get_ObjectPv:
.LFB21765:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE21765:
	.size	_Z23_v8_internal_Get_ObjectPv, .-_Z23_v8_internal_Get_ObjectPv
	.section	.text._Z25_v8_internal_Print_ObjectPv,"ax",@progbits
	.p2align 4
	.globl	_Z25_v8_internal_Print_ObjectPv
	.type	_Z25_v8_internal_Print_ObjectPv, @function
_Z25_v8_internal_Print_ObjectPv:
.LFB21766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	stdout(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -16(%rbp)
	leaq	-16(%rbp), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L188
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L188:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21766:
	.size	_Z25_v8_internal_Print_ObjectPv, .-_Z25_v8_internal_Print_ObjectPv
	.section	.rodata._Z23_v8_internal_Print_CodePv.str1.8,"aMS",@progbits,1
	.align 8
.LC50:
	.string	"%p is not within the current isolate's large object, code or embedded spaces\n"
	.align 8
.LC51:
	.string	"No code object found containing %p\n"
	.section	.text._Z23_v8_internal_Print_CodePv,"ax",@progbits
	.p2align 4
	.globl	_Z23_v8_internal_Print_CodePv
	.type	_Z23_v8_internal_Print_CodePv, @function
_Z23_v8_internal_Print_CodePv:
.LFB21767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-464(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal7Isolate12isolate_key_E(%rip), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	45752(%r13), %rax
	movq	%r12, %rsi
	leaq	280(%rax), %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm@PLT
	testq	%rax, %rax
	je	.L190
	leaq	-320(%rbp), %r13
	movq	%rax, %r14
	leaq	-400(%rbp), %rbx
	movq	%r13, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movw	%dx, -96(%rbp)
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r8
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r8, -320(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r12, %rcx
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r8
	movq	%r13, %rdi
	movq	%rax, -400(%rbp)
	movq	%r8, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
.L189:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movq	%r15, %rdi
	leaq	37592(%r13), %r14
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap11InSpaceSlowEmNS0_15AllocationSpaceE@PLT
	testb	%al, %al
	jne	.L192
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap11InSpaceSlowEmNS0_15AllocationSpaceE@PLT
	testb	%al, %al
	je	.L198
.L192:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate14FindCodeObjectEm@PLT
	movq	%rax, -464(%rbp)
	movq	-1(%rax), %rax
	cmpw	$69, 11(%rax)
	je	.L193
	movq	%r12, %rsi
	leaq	.LC51(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L193:
	leaq	-320(%rbp), %r13
	leaq	-400(%rbp), %r14
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	%rbx, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	xorl	%esi, %esi
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal4Code11DisassembleEPKcRSom@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -320(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm@PLT
	testb	%al, %al
	jne	.L192
	movq	%r12, %rsi
	leaq	.LC50(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L189
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21767:
	.size	_Z23_v8_internal_Print_CodePv, .-_Z23_v8_internal_Print_CodePv
	.section	.rodata._Z35_v8_internal_Print_LayoutDescriptorPv.str1.8,"aMS",@progbits,1
	.align 8
.LC52:
	.string	"Please provide a layout descriptor"
	.section	.text._Z35_v8_internal_Print_LayoutDescriptorPv,"ax",@progbits
	.p2align 4
	.globl	_Z35_v8_internal_Print_LayoutDescriptorPv
	.type	_Z35_v8_internal_Print_LayoutDescriptorPv, @function
_Z35_v8_internal_Print_LayoutDescriptorPv:
.LFB21768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testb	$1, %dil
	jne	.L205
.L201:
	movq	stdout(%rip), %rsi
	movq	%rdi, -16(%rbp)
	leaq	-16(%rbp), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
.L199:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movq	-1(%rdi), %rax
	cmpw	$70, 11(%rax)
	je	.L201
	leaq	.LC52(%rip), %rdi
	call	puts@PLT
	jmp	.L199
.L206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21768:
	.size	_Z35_v8_internal_Print_LayoutDescriptorPv, .-_Z35_v8_internal_Print_LayoutDescriptorPv
	.section	.text._Z29_v8_internal_Print_StackTracev,"ax",@progbits
	.p2align 4
	.globl	_Z29_v8_internal_Print_StackTracev
	.type	_Z29_v8_internal_Print_StackTracev, @function
_Z29_v8_internal_Print_StackTracev:
.LFB21769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal7Isolate12isolate_key_E(%rip), %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	stdout(%rip), %rsi
	movl	$1, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	.cfi_endproc
.LFE21769:
	.size	_Z29_v8_internal_Print_StackTracev, .-_Z29_v8_internal_Print_StackTracev
	.section	.rodata._Z33_v8_internal_Print_TransitionTreePv.str1.1,"aMS",@progbits,1
.LC53:
	.string	"Please provide a valid Map"
	.section	.text._Z33_v8_internal_Print_TransitionTreePv,"ax",@progbits
	.p2align 4
	.globl	_Z33_v8_internal_Print_TransitionTreePv
	.type	_Z33_v8_internal_Print_TransitionTreePv, @function
_Z33_v8_internal_Print_TransitionTreePv:
.LFB21770:
	.cfi_startproc
	endbr64
	testb	$1, %dil
	jne	.L210
.L211:
	leaq	.LC53(%rip), %rdi
	jmp	puts@PLT
	.p2align 4,,10
	.p2align 3
.L210:
	movq	-1(%rdi), %rax
	cmpw	$68, 11(%rax)
	jne	.L211
	ret
	.cfi_endproc
.LFE21770:
	.size	_Z33_v8_internal_Print_TransitionTreePv, .-_Z33_v8_internal_Print_TransitionTreePv
	.section	.text._Z23_v8_internal_Node_PrintPv,"ax",@progbits
	.p2align 4
	.globl	_Z23_v8_internal_Node_PrintPv
	.type	_Z23_v8_internal_Node_PrintPv, @function
_Z23_v8_internal_Node_PrintPv:
.LFB21771:
	.cfi_startproc
	endbr64
	jmp	_ZNK2v88internal8compiler4Node5PrintEv@PLT
	.cfi_endproc
.LFE21771:
	.size	_Z23_v8_internal_Node_PrintPv, .-_Z23_v8_internal_Node_PrintPv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10HeapNumber15HeapNumberPrintERSo,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10HeapNumber15HeapNumberPrintERSo, @function
_GLOBAL__sub_I__ZN2v88internal10HeapNumber15HeapNumberPrintERSo:
.LFB27685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27685:
	.size	_GLOBAL__sub_I__ZN2v88internal10HeapNumber15HeapNumberPrintERSo, .-_GLOBAL__sub_I__ZN2v88internal10HeapNumber15HeapNumberPrintERSo
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10HeapNumber15HeapNumberPrintERSo
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
