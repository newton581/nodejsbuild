	.file	"builtins-array.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal12_GLOBAL__N_117GetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117GetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE, @function
_ZN2v88internal12_GLOBAL__N_117GetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE:
.LFB20873:
	.cfi_startproc
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	jne	.L6
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L16
	movsd	7(%rax), %xmm0
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal6Object22GetLengthFromArrayLikeEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE@PLT
	pxor	%xmm0, %xmm0
	testq	%rax, %rax
	je	.L9
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L11
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L12:
	movl	$1, %eax
.L9:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movsd	7(%rax), %xmm0
	jmp	.L12
	.cfi_endproc
.LFE20873:
	.size	_ZN2v88internal12_GLOBAL__N_117GetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE, .-_ZN2v88internal12_GLOBAL__N_117GetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_117SetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEd,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117SetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEd, @function
_ZN2v88internal12_GLOBAL__N_117SetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEd:
.LFB20874:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	cmpw	$1061, 11(%rax)
	je	.L24
.L18:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	addq	$16, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	2768(%r12), %rdx
	movq	%rax, %rcx
	popq	%r12
	movl	$1, %r9d
	popq	%r13
	xorl	%r8d, %r8d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	%rsi, %rdi
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v88internal7JSArray17HasReadOnlyLengthENS0_6HandleIS1_EE@PLT
	movsd	-24(%rbp), %xmm0
	testb	%al, %al
	jne	.L18
	cvttsd2siq	%xmm0, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7JSArray9SetLengthENS0_6HandleIS1_EEj@PLT
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20874:
	.size	_ZN2v88internal12_GLOBAL__N_117SetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEd, .-_ZN2v88internal12_GLOBAL__N_117SetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEd
	.section	.text._ZN2v88internal12_GLOBAL__N_116GetRelativeIndexEPNS0_7IsolateEdNS0_6HandleINS0_6ObjectEEEd,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_116GetRelativeIndexEPNS0_7IsolateEdNS0_6HandleINS0_6ObjectEEEd, @function
_ZN2v88internal12_GLOBAL__N_116GetRelativeIndexEPNS0_7IsolateEdNS0_6HandleINS0_6ObjectEEEd:
.LFB20871:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L44
.L27:
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L31:
	pxor	%xmm2, %xmm2
	comisd	%xmm1, %xmm2
	ja	.L45
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	minsd	%xmm1, %xmm0
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addsd	%xmm1, %xmm0
	movl	$1, %eax
	maxsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	je	.L31
	movsd	%xmm0, -8(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movsd	-8(%rbp), %xmm0
	testq	%rax, %rax
	jne	.L41
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	pxor	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	(%rax), %rax
	testb	$1, %al
	je	.L27
	movsd	7(%rax), %xmm1
	jmp	.L31
	.cfi_endproc
.LFE20871:
	.size	_ZN2v88internal12_GLOBAL__N_116GetRelativeIndexEPNS0_7IsolateEdNS0_6HandleINS0_6ObjectEEEd, .-_ZN2v88internal12_GLOBAL__N_116GetRelativeIndexEPNS0_7IsolateEdNS0_6HandleINS0_6ObjectEEEd
	.section	.text._ZN2v88internal12_GLOBAL__N_115GenericArrayPopEPNS0_7IsolateEPNS0_16BuiltinArgumentsE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115GenericArrayPopEPNS0_7IsolateEPNS0_16BuiltinArgumentsE, @function
_ZN2v88internal12_GLOBAL__N_115GenericArrayPopEPNS0_7IsolateEPNS0_16BuiltinArgumentsE:
.LFB20884:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	8(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L47
.L50:
	movq	%r12, %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L93
.L49:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object22GetLengthFromArrayLikeEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L93
	movq	(%rax), %rax
	testb	$1, %al
	je	.L94
	movsd	7(%rax), %xmm0
.L54:
	ucomisd	.LC0(%rip), %xmm0
	jp	.L55
	jne	.L55
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L57
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L58:
	xorl	%r8d, %r8d
	leaq	2768(%rbx), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	$1, %r9d
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L93
	movq	88(%rbx), %rax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L55:
	subsd	.LC1(%rip), %xmm0
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L95
.L62:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L69
.L71:
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r15
.L70:
	movq	0(%r13), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L72
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L72:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	%rbx, -200(%rbp)
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r13, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L96
.L73:
	movq	%r15, -160(%rbp)
	leaq	-224(%rbp), %r15
	movq	%r15, %rdi
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r12, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L68:
	movq	%r15, %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L93
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver23DeletePropertyOrElementENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE@PLT
	testb	%al, %al
	je	.L93
	xorl	%r8d, %r8d
	leaq	2768(%rbx), %rdx
	movq	%r14, %rcx
	movq	%r12, %rsi
	movl	$1, %r9d
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	jne	.L76
	.p2align 4,,10
	.p2align 3
.L93:
	movq	312(%rbx), %rax
.L51:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L97
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L47:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L50
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L57:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L98
.L59:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	$0, (%rcx)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L63
	testb	$2, %al
	jne	.L62
.L63:
	leaq	-144(%rbp), %r8
	leaq	-228(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L62
	movq	(%r12), %rax
	movl	-228(%rbp), %r15d
	movq	-248(%rbp), %r8
	testb	$1, %al
	jne	.L65
.L67:
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-248(%rbp), %r8
.L66:
	movabsq	$824633720832, %rcx
	movq	%r8, %rdi
	movl	%r15d, -72(%rbp)
	leaq	-224(%rbp), %r15
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	%rbx, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, -112(%rbp)
	movdqa	-128(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L76:
	movq	(%r15), %rax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L65:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L67
	movq	%r12, %rax
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L69:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L71
	movq	%r12, %r15
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L73
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20884:
	.size	_ZN2v88internal12_GLOBAL__N_115GenericArrayPopEPNS0_7IsolateEPNS0_16BuiltinArgumentsE, .-_ZN2v88internal12_GLOBAL__N_115GenericArrayPopEPNS0_7IsolateEPNS0_16BuiltinArgumentsE
	.section	.text._ZN2v88internal12_GLOBAL__N_116GenericArrayPushEPNS0_7IsolateEPNS0_16BuiltinArgumentsE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_116GenericArrayPushEPNS0_7IsolateEPNS0_16BuiltinArgumentsE, @function
_ZN2v88internal12_GLOBAL__N_116GenericArrayPushEPNS0_7IsolateEPNS0_16BuiltinArgumentsE:
.LFB20880:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rax), %r12
	movq	%rsi, -184(%rbp)
	movq	(%r12), %rax
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	testb	$1, %al
	jne	.L100
.L103:
	movq	%r12, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L133
.L102:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object22GetLengthFromArrayLikeEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L133
	movq	-184(%rbp), %rax
	movq	(%rax), %rdx
	movq	(%r14), %rax
	leal	-5(%rdx), %esi
	testb	$1, %al
	je	.L134
	movsd	7(%rax), %xmm0
.L107:
	movsd	.LC2(%rip), %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	subsd	%xmm0, %xmm1
	comisd	%xmm1, %xmm2
	ja	.L135
	testl	%esi, %esi
	jle	.L110
	leal	-6(%rdx), %eax
	movl	$8, %r10d
	movq	%r12, -168(%rbp)
	leaq	-144(%rbp), %r14
	leaq	16(,%rax,8), %rax
	movq	%r10, %r12
	movq	%rax, -192(%rbp)
	leaq	-145(%rbp), %rax
	movq	%rax, -224(%rbp)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L137:
	movq	-168(%rbp), %rax
	cvttsd2siq	%xmm0, %rcx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L113
.L115:
	movq	-168(%rbp), %rsi
	movl	%ecx, %edx
	movq	%r13, %rdi
	movq	%r8, -216(%rbp)
	movq	%rcx, -200(%rbp)
	movsd	%xmm0, -208(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-200(%rbp), %rcx
	movsd	-208(%rbp), %xmm0
	movq	-216(%rbp), %r8
.L114:
	movabsq	$824633720832, %rdx
	movq	%r14, %rdi
	movb	$1, %bl
	movl	%ecx, -72(%rbp)
	movq	%rdx, -132(%rbp)
	movq	-168(%rbp), %rdx
	movl	%ebx, %ebx
	movq	%r8, -208(%rbp)
	movq	%rdx, -96(%rbp)
	movl	$3, -144(%rbp)
	movq	%r13, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	$-1, -68(%rbp)
	movsd	%xmm0, -200(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movq	-176(%rbp), %rsi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L133
	movq	-208(%rbp), %r8
	movsd	-200(%rbp), %xmm0
	cmpq	%r12, %r8
	je	.L133
.L117:
	addsd	.LC1(%rip), %xmm0
	addq	$8, %r12
	cmpq	-192(%rbp), %r12
	je	.L136
.L120:
	movq	-184(%rbp), %rax
	movsd	.LC3(%rip), %xmm3
	movq	8(%rax), %r8
	movq	%r8, %rax
	subq	%r12, %rax
	comisd	%xmm0, %xmm3
	movq	%rax, -176(%rbp)
	jnb	.L137
	xorl	%esi, %esi
	movq	%r13, %rdi
	movsd	%xmm0, -200(%rbp)
	movb	$1, %r15b
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	%r15d, %r15d
	movq	-224(%rbp), %r8
	movq	-168(%rbp), %rdx
	movq	%rax, %rcx
	movl	$3, %r9d
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	xorl	%edx, %edx
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	-176(%rbp), %rsi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movsd	-200(%rbp), %xmm0
	testb	%al, %al
	jne	.L117
	.p2align 4,,10
	.p2align 3
.L133:
	movq	312(%r13), %rax
.L104:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L138
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L113:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L115
	movq	-168(%rbp), %rax
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L103
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L136:
	movq	-168(%rbp), %r12
.L110:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	2768(%r13), %rdx
	movl	$1, %r9d
	movq	%rax, %rcx
	movq	%rax, %rbx
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L133
	movq	(%rbx), %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L135:
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rdx
	movl	$281, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L104
.L138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20880:
	.size	_ZN2v88internal12_GLOBAL__N_116GenericArrayPushEPNS0_7IsolateEPNS0_16BuiltinArgumentsE, .-_ZN2v88internal12_GLOBAL__N_116GenericArrayPushEPNS0_7IsolateEPNS0_16BuiltinArgumentsE
	.section	.text._ZN2v88internalL23Builtin_Impl_ArrayShiftENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL23Builtin_Impl_ArrayShiftENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL23Builtin_Impl_ArrayShiftENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20892:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -256(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L140
.L143:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L268
.L142:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_117GetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE
	movsd	%xmm0, -264(%rbp)
	testb	%al, %al
	je	.L268
	ucomisd	.LC0(%rip), %xmm0
	jp	.L146
	jne	.L146
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_117SetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEd
	testq	%rax, %rax
	je	.L268
	movq	88(%r15), %r12
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L146:
	movq	0(%r13), %rdi
	testb	$1, %dil
	jne	.L269
.L161:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
.L160:
	movabsq	$824633720832, %rcx
	movq	%rax, -80(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -72(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rcx, -132(%rbp)
	movl	$3, -144(%rbp)
	movq	%r15, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L162
	movq	-120(%rbp), %rax
	addq	$88, %rax
	movq	%rax, -312(%rbp)
.L163:
	movsd	-264(%rbp), %xmm6
	comisd	.LC1(%rip), %xmm6
	leaq	-224(%rbp), %r14
	movq	.LC1(%rip), %rax
	movq	%rax, -248(%rbp)
	jbe	.L165
	movq	%rbx, -304(%rbp)
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L208:
	movsd	-248(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movsd	-248(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	subsd	.LC1(%rip), %xmm0
	movq	%rax, %r12
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, -280(%rbp)
	movq	%rax, %r14
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r8
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	subq	$37592, %r8
	cmpw	$63, 11(%rdx)
	jbe	.L270
.L168:
	movq	(%r12), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L172
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L172:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	%r8, -200(%rbp)
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r12, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L271
.L173:
	movq	%rbx, %rdi
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%r13, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L171:
	movq	%rbx, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	movzbl	%ah, %edx
	testb	%al, %al
	je	.L174
	testb	%dl, %dl
	je	.L175
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L272
.L177:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L184
.L186:
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rdx
.L185:
	movq	(%r12), %rcx
	movl	$3, %eax
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L187
	movl	11(%rcx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L187:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	%r15, -200(%rbp)
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L273
.L188:
	movq	-280(%rbp), %rax
	movq	%rbx, %rdi
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	%rax, -192(%rbp)
	movq	$0, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L183:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L174
	movb	$1, -288(%rbp)
	movl	-288(%rbp), %eax
	movq	%rax, -288(%rbp)
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L274
.L192:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L199
.L201:
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rdx
.L200:
	movq	(%r14), %rcx
	movl	$3, %eax
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L202
	movl	11(%rcx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L202:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	%r15, -200(%rbp)
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L275
.L203:
	movq	%rbx, %rdi
	movq	%r14, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L198:
	movq	-288(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	jne	.L207
.L174:
	movq	-304(%rbp), %rbx
.L268:
	movq	312(%r15), %r12
.L144:
	movq	-256(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %rbx
	je	.L211
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L211:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	addq	$280, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L143
	movq	%rsi, %r13
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L269:
	movq	-1(%rdi), %rax
	cmpw	$1061, 11(%rax)
	je	.L277
.L158:
	testb	$1, %dil
	je	.L161
	movq	-1(%rdi), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L161
	movq	%r13, %rax
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L175:
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver23DeletePropertyOrElementENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE@PLT
	testb	%al, %al
	je	.L174
.L207:
	movsd	.LC1(%rip), %xmm1
	addsd	-248(%rbp), %xmm1
	movsd	-264(%rbp), %xmm3
	comisd	%xmm1, %xmm3
	movsd	%xmm1, -248(%rbp)
	ja	.L208
	movq	-304(%rbp), %rbx
.L165:
	movsd	-264(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	subsd	.LC1(%rip), %xmm0
	movsd	%xmm0, -248(%rbp)
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10JSReceiver23DeletePropertyOrElementENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE@PLT
	movsd	-248(%rbp), %xmm0
	testb	%al, %al
	je	.L268
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_117SetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEd
	testq	%rax, %rax
	je	.L268
	movq	-312(%rbp), %rax
	movq	(%rax), %r12
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L270:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L169
	testb	$2, %al
	jne	.L168
.L169:
	movq	-272(%rbp), %rdi
	leaq	-228(%rbp), %rsi
	movq	%r8, -296(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-296(%rbp), %r8
	testb	%al, %al
	je	.L168
	movabsq	$824633720832, %rax
	movq	-272(%rbp), %rdi
	movq	%r8, -120(%rbp)
	movq	%rax, -132(%rbp)
	movl	-228(%rbp), %eax
	movl	$3, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm4, -224(%rbp)
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L178
	testb	$2, %al
	jne	.L177
.L178:
	movq	-272(%rbp), %rdi
	leaq	-228(%rbp), %rsi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L177
	movq	0(%r13), %rax
	movl	-228(%rbp), %edx
	testb	$1, %al
	jne	.L180
.L182:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%edx, -280(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-280(%rbp), %edx
.L181:
	movq	-272(%rbp), %rdi
	movq	%r15, -120(%rbp)
	movabsq	$824633720832, %rcx
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movdqa	-128(%rbp), %xmm6
	movq	%r12, -112(%rbp)
	movdqa	-144(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm7
	movaps	%xmm6, -208(%rbp)
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm5, -224(%rbp)
	movdqa	-96(%rbp), %xmm5
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L193
	testb	$2, %al
	jne	.L192
.L193:
	movq	-272(%rbp), %rdi
	leaq	-228(%rbp), %rsi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L192
	movq	0(%r13), %rax
	movl	-228(%rbp), %edx
	testb	$1, %al
	jne	.L195
.L197:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%edx, -280(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-280(%rbp), %edx
.L196:
	movq	-272(%rbp), %rdi
	movq	%r15, -120(%rbp)
	movabsq	$824633720832, %rcx
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r14, -112(%rbp)
	movdqa	-128(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm7, -224(%rbp)
	movdqa	-96(%rbp), %xmm7
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L271:
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L162:
	movq	-272(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, -312(%rbp)
	testq	%rax, %rax
	jne	.L163
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L277:
	movq	-1(%rdi), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$12, %eax
	je	.L158
	movq	-1(%rdi), %rax
	movl	15(%rax), %eax
	testl	$134217728, %eax
	je	.L158
	movq	-1(%rdi), %rax
	movq	104(%r15), %rcx
	movq	288(%r15), %r10
	movq	1016(%r15), %rsi
	movq	23(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L157
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L278:
	movq	15(%rax), %rax
	cmpq	%rax, %rsi
	je	.L222
	cmpq	%rax, %r10
	jne	.L158
.L222:
	movq	23(%rdx), %rax
	cmpq	%rax, %rcx
	je	.L154
.L157:
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	ja	.L278
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L154:
	movq	%r13, %rdi
	call	_ZN2v88internal7JSArray17HasReadOnlyLengthENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	jne	.L279
	movq	0(%r13), %rax
	movq	%r13, %rsi
	movq	-1(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	(%rdi), %rax
	call	*168(%rax)
	movq	(%rax), %r12
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L186
	movq	%r13, %rdx
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L180:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L182
	movq	%r13, %rax
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L195:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L197
	movq	%r13, %rax
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L199:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L201
	movq	%r13, %rdx
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L273:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-296(%rbp), %rdx
	movq	%rax, -280(%rbp)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rdx, -280(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-280(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L203
.L279:
	movq	0(%r13), %rdi
	jmp	.L158
.L276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20892:
	.size	_ZN2v88internalL23Builtin_Impl_ArrayShiftENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL23Builtin_Impl_ArrayShiftENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL21Builtin_Impl_ArrayPopENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL21Builtin_Impl_ArrayPopENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL21Builtin_Impl_ArrayPopENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20887:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -160(%rbp)
	movq	41088(%rdx), %r15
	movq	%rsi, -152(%rbp)
	movq	41096(%rdx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L315
.L282:
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_115GenericArrayPopEPNS0_7IsolateEPNS0_16BuiltinArgumentsE
	movq	%rax, %r13
.L288:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L297
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L297:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L316
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	jne	.L282
	movq	-1(%rax), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	cmpl	$12, %edx
	je	.L282
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$134217728, %edx
	je	.L282
	movq	23(%rax), %rax
	movq	%rsi, %r13
	testb	$1, %al
	jne	.L317
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	cvttsd2siq	%xmm0, %rbx
	testl	%ebx, %ebx
	jne	.L290
.L318:
	movq	88(%r12), %r13
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L317:
	movsd	7(%rax), %xmm0
	cvttsd2siq	%xmm0, %rbx
	testl	%ebx, %ebx
	je	.L318
.L290:
	movq	%r13, %rdi
	call	_ZN2v88internal7JSArray17HasReadOnlyLengthENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	jne	.L282
	movq	0(%r13), %r8
	movq	-1(%r8), %rax
	movq	104(%r12), %rcx
	movq	288(%r12), %rdi
	movq	1016(%r12), %rsi
	movq	23(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L294
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L319:
	movq	15(%rax), %rax
	cmpq	%rax, %rsi
	je	.L302
	cmpq	%rax, %rdi
	jne	.L292
.L302:
	movq	23(%rdx), %rax
	cmpq	%rax, %rcx
	je	.L291
.L294:
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	ja	.L319
.L292:
	subl	$1, %ebx
	movq	%r12, -120(%rbp)
	movabsq	$824633720832, %rax
	leaq	-144(%rbp), %rdi
	movl	$3, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movl	%ebx, -72(%rbp)
	movl	$-1, -68(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L320
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L295:
	movl	%ebx, %esi
	movq	%r13, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal7JSArray9SetLengthENS0_6HandleIS1_EEj@PLT
	movq	-168(%rbp), %rax
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L291:
	movq	-1(%r8), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	%r13, %rsi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	(%rdi), %rax
	call	*160(%rax)
.L298:
	movq	(%rax), %r13
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L320:
	movq	-168(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L295
	movq	312(%r12), %r13
	jmp	.L288
.L316:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20887:
	.size	_ZN2v88internalL21Builtin_Impl_ArrayPopENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL21Builtin_Impl_ArrayPopENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal12_GLOBAL__N_116Fast_ArrayConcatEPNS0_7IsolateEPNS0_16BuiltinArgumentsE.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_116Fast_ArrayConcatEPNS0_7IsolateEPNS0_16BuiltinArgumentsE.part.0, @function
_ZN2v88internal12_GLOBAL__N_116Fast_ArrayConcatEPNS0_7IsolateEPNS0_16BuiltinArgumentsE.part.0:
.LFB25727:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	leal	-4(%rax), %r15d
	testl	%r15d, %r15d
	jle	.L341
	subl	$5, %eax
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	leaq	8(,%rax,8), %r8
.L340:
	movq	8(%r12), %rax
	subq	%rbx, %rax
	movq	(%rax), %r14
	testb	$1, %r14b
	jne	.L351
.L350:
	xorl	%eax, %eax
.L330:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	movq	-1(%r14), %rax
	leaq	-1(%r14), %r9
	cmpw	$1061, 11(%rax)
	jne	.L350
	movq	-1(%r14), %rax
	cmpw	$1041, 11(%rax)
	jbe	.L350
	movq	%r8, -72(%rbp)
	movq	%r14, %rsi
	movq	%r9, -64(%rbp)
	movl	%ecx, -56(%rbp)
	movq	-1(%r14), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	testb	%al, %al
	jne	.L350
	movq	-1(%r14), %rax
	movq	104(%r13), %rsi
	movq	288(%r13), %rdi
	movq	1016(%r13), %r10
	movq	23(%rax), %rax
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	cmpq	%rsi, %rax
	jne	.L329
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L352:
	movq	15(%rax), %rax
	cmpq	%rax, %rdi
	je	.L342
	cmpq	%rax, %r10
	jne	.L350
.L342:
	movq	23(%rdx), %rax
	cmpq	%rax, %rsi
	je	.L327
.L329:
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	ja	.L352
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L327:
	movq	-1(%r14), %rax
	cmpb	$47, 14(%rax)
	ja	.L350
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L331
	movq	%r14, %rsi
	movq	%r8, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %r8
	movq	%rax, %rdx
	movq	(%rax), %rax
	leaq	-1(%rax), %r9
.L332:
	movq	(%r9), %r14
	movq	12464(%r13), %rax
	movq	39(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L334
	movq	%r8, -72(%rbp)
	movl	%ecx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rdx
	movl	-64(%rbp), %ecx
	movq	(%rax), %rsi
	movq	-72(%rbp), %r8
.L335:
	movq	463(%rsi), %rax
	cmpq	%rax, 23(%r14)
	jne	.L350
	movl	15(%r14), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	$1, %eax
	jne	.L350
	movq	(%rdx), %rax
	addl	27(%rax), %ecx
	cmpl	$134217726, %ecx
	jg	.L353
	addq	$8, %rbx
	cmpq	%r8, %rbx
	jne	.L340
	.p2align 4,,10
	.p2align 3
.L322:
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal16ElementsAccessor6ConcatEPNS0_7IsolateEPNS0_9ArgumentsEjj@PLT
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L331:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L354
.L333:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rdx)
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L334:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L355
.L336:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L341:
	xorl	%ecx, %ecx
	jmp	.L322
.L355:
	movq	%r13, %rdi
	movq	%r8, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movl	%ecx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rsi
	movl	-64(%rbp), %ecx
	movq	-56(%rbp), %rdx
	jmp	.L336
.L354:
	movq	%r13, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movl	-56(%rbp), %ecx
	movq	%rax, %rdx
	jmp	.L333
.L353:
	xorl	%edx, %edx
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$189, %esi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L350
	.cfi_endproc
.LFE25727:
	.size	_ZN2v88internal12_GLOBAL__N_116Fast_ArrayConcatEPNS0_7IsolateEPNS0_16BuiltinArgumentsE.part.0, .-_ZN2v88internal12_GLOBAL__N_116Fast_ArrayConcatEPNS0_7IsolateEPNS0_16BuiltinArgumentsE.part.0
	.section	.text._ZN2v88internal12_GLOBAL__N_133MatchArrayElementsKindToArgumentsEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEEPNS0_16BuiltinArgumentsEii,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_133MatchArrayElementsKindToArgumentsEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEEPNS0_16BuiltinArgumentsEii, @function
_ZN2v88internal12_GLOBAL__N_133MatchArrayElementsKindToArgumentsEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEEPNS0_16BuiltinArgumentsEii:
.LFB20868:
	.cfi_startproc
	movl	(%rdx), %eax
	subl	$4, %eax
	cmpl	%eax, %ecx
	jl	.L369
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rsi), %rsi
	movq	-1(%rsi), %rsi
	movzbl	14(%rsi), %r9d
	shrl	$3, %r9d
	leal	-2(%r9), %esi
	cmpb	$1, %sil
	jbe	.L356
	addl	%ecx, %r8d
	cmpl	%eax, %r8d
	cmovg	%eax, %r8d
	cmpl	%r8d, %ecx
	jge	.L356
	movl	%ecx, %esi
	leal	0(,%rcx,8), %eax
	movslq	%ecx, %rcx
	notl	%esi
	cltq
	addl	%r8d, %esi
	movq	8(%rdx), %r8
	leaq	1(%rsi,%rcx), %rcx
	movzbl	%r9b, %esi
	salq	$3, %rcx
.L360:
	movq	%r8, %rdx
	subq	%rax, %rdx
	movq	(%rdx), %rdx
	testb	$1, %dl
	jne	.L370
.L358:
	addq	$8, %rax
	cmpq	%rax, %rcx
	jne	.L360
	cmpb	%r9b, %sil
	je	.L356
	.p2align 4,,10
	.p2align 3
.L372:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L356
	movq	%rbx, 41096(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	je	.L371
	movl	$2, %esi
	cmpb	%r9b, %sil
	jne	.L372
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L371:
	movl	$4, %esi
	jmp	.L358
	.cfi_endproc
.LFE20868:
	.size	_ZN2v88internal12_GLOBAL__N_133MatchArrayElementsKindToArgumentsEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEEPNS0_16BuiltinArgumentsEii, .-_ZN2v88internal12_GLOBAL__N_133MatchArrayElementsKindToArgumentsEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEEPNS0_16BuiltinArgumentsEii
	.section	.text._ZN2v88internalL22Builtin_Impl_ArrayPushENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22Builtin_Impl_ArrayPushENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL22Builtin_Impl_ArrayPushENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20883:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %rax
	movq	%rdi, -64(%rbp)
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %r14
	movq	%rsi, -56(%rbp)
	movq	%rax, -72(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L374
.L407:
	leaq	-64(%rbp), %r15
.L375:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_116GenericArrayPushEPNS0_7IsolateEPNS0_16BuiltinArgumentsE
	movq	%rax, %r13
.L385:
	subl	$1, 41104(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L404
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L404:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	leaq	-64(%rbp), %r15
	cmpw	$1061, 11(%rdx)
	jne	.L375
	movq	-1(%rax), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	cmpl	$12, %edx
	je	.L375
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$134217728, %edx
	je	.L375
	movq	-1(%rax), %rax
	movq	104(%r12), %rcx
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movq	288(%r12), %rdi
	movq	1016(%r12), %rsi
	movq	23(%rax), %rax
	cmpq	%rcx, %rax
	je	.L379
	.p2align 4,,10
	.p2align 3
.L382:
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	jbe	.L407
	movq	15(%rax), %rax
	cmpq	%rax, %rsi
	je	.L391
	cmpq	%rax, %rdi
	jne	.L407
.L391:
	movq	23(%rdx), %rax
	cmpq	%rax, %rcx
	jne	.L382
.L379:
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-64(%rbp), %r15
	call	_ZN2v88internal7Isolate26IsAnyInitialArrayPrototypeENS0_6HandleINS0_7JSArrayEEE@PLT
	testb	%al, %al
	jne	.L375
	leal	-5(%rbx), %r8d
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_133MatchArrayElementsKindToArgumentsEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEEPNS0_16BuiltinArgumentsEii
	movl	-64(%rbp), %eax
	leal	-5(%rax), %ebx
	movq	0(%r13), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L408
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	testl	%ebx, %ebx
	jne	.L387
.L409:
	cvttsd2siq	%xmm0, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L408:
	movsd	7(%rax), %xmm0
	testl	%ebx, %ebx
	je	.L409
.L387:
	movq	%r13, %rdi
	call	_ZN2v88internal7JSArray17HasReadOnlyLengthENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	jne	.L375
	movq	0(%r13), %rax
	movq	%r13, %rsi
	movl	%ebx, %ecx
	movq	-1(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*144(%rax)
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L385
	.cfi_endproc
.LFE20883:
	.size	_ZN2v88internalL22Builtin_Impl_ArrayPushENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL22Builtin_Impl_ArrayPushENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Builtin_Impl_ArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"DoubleToUint32IfEqualToSelf(start_index, &start)"
	.section	.rodata._ZN2v88internalL31Builtin_Impl_ArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internalL31Builtin_Impl_ArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8
	.align 8
.LC8:
	.string	"DoubleToUint32IfEqualToSelf(end_index, &end)"
	.section	.text._ZN2v88internalL31Builtin_Impl_ArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_ArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_ArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20879:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -256(%rbp)
	movq	41088(%rdx), %r12
	movq	%rsi, -248(%rbp)
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	cmpl	$32, 41828(%rdx)
	je	.L511
.L411:
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L414
.L417:
	movq	%r14, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L418
.L416:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_117GetLengthPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE
	movapd	%xmm0, %xmm2
	testb	%al, %al
	je	.L418
	cmpl	$6, -256(%rbp)
	leaq	88(%r15), %rsi
	jg	.L512
.L420:
	movapd	%xmm2, %xmm0
	pxor	%xmm1, %xmm1
	movq	%r15, %rdi
	movsd	%xmm2, -272(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_116GetRelativeIndexEPNS0_7IsolateEdNS0_6HandleINS0_6ObjectEEEd
	movapd	%xmm0, %xmm3
	testb	%al, %al
	je	.L418
	cmpl	$7, -256(%rbp)
	movsd	-272(%rbp), %xmm2
	movsd	%xmm0, -264(%rbp)
	leaq	88(%r15), %rsi
	jg	.L513
.L423:
	movapd	%xmm2, %xmm0
	movapd	%xmm2, %xmm1
	movq	%r15, %rdi
	movsd	%xmm3, -280(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_116GetRelativeIndexEPNS0_7IsolateEdNS0_6HandleINS0_6ObjectEEEd
	movsd	%xmm0, -272(%rbp)
	testb	%al, %al
	je	.L418
	movsd	-280(%rbp), %xmm3
	movapd	%xmm0, %xmm4
	comisd	%xmm0, %xmm3
	jnb	.L510
	cmpl	$5, -256(%rbp)
	jle	.L514
	movq	-248(%rbp), %rax
	subq	$8, %rax
	movq	%rax, -296(%rbp)
.L428:
	comisd	.LC4(%rip), %xmm4
	jbe	.L515
.L433:
	movsd	-272(%rbp), %xmm6
	leaq	-224(%rbp), %rax
	movq	%rax, -280(%rbp)
	comisd	%xmm3, %xmm6
	jbe	.L510
	leaq	-228(%rbp), %rax
	movq	%r12, -312(%rbp)
	movq	%rax, -304(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	%rbx, -320(%rbp)
	movq	-296(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L452:
	movsd	-264(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	movb	$1, %r13b
	movl	%r13d, %r13d
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L516
.L455:
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L462
.L464:
	movl	$-1, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rdx
.L463:
	movq	(%r12), %rcx
	movl	$3, %eax
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L465
	movl	11(%rcx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L465:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	%r15, -200(%rbp)
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L517
.L466:
	movq	-280(%rbp), %rdi
	movq	%r12, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r14, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L461:
	movq	-280(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rcx
	movq	%rbx, %rsi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	jne	.L518
.L473:
	movq	-312(%rbp), %r12
	movq	-320(%rbp), %rbx
	movq	312(%r15), %r13
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L511:
	movq	41472(%rdx), %rdi
	call	_ZN2v88internal5Debug31PerformSideEffectCheckForObjectENS0_6HandleINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L519
.L418:
	movq	312(%r15), %r13
.L413:
	subl	$1, 41104(%r15)
	movq	%r12, 41088(%r15)
	cmpq	41096(%r15), %rbx
	je	.L471
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L471:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L520
	addq	$280, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L417
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L512:
	movq	-248(%rbp), %rax
	leaq	-16(%rax), %rsi
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L519:
	movq	-248(%rbp), %r14
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L513:
	movq	-248(%rbp), %rax
	leaq	-24(%rax), %rsi
	jmp	.L423
.L523:
	movl	%r8d, %eax
	pxor	%xmm0, %xmm0
	movsd	-272(%rbp), %xmm6
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm6
	jp	.L447
	jne	.L447
	movq	(%r14), %rax
	movq	%r14, %rsi
	movq	-1(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	-296(%rbp), %rdx
	movq	(%rdi), %rax
	call	*192(%rax)
.L510:
	movq	(%r14), %r13
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L514:
	leaq	88(%r15), %rax
	movq	%rax, -296(%rbp)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L518:
	testq	%rbx, %rbx
	je	.L473
	movsd	.LC1(%rip), %xmm5
	addsd	-264(%rbp), %xmm5
	movsd	-272(%rbp), %xmm7
	comisd	%xmm5, %xmm7
	movsd	%xmm5, -264(%rbp)
	ja	.L452
	movq	-312(%rbp), %r12
	movq	-320(%rbp), %rbx
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L516:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L456
	testb	$2, %al
	jne	.L455
.L456:
	movq	-304(%rbp), %rsi
	movq	-288(%rbp), %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L455
	movq	(%r14), %rax
	movl	-228(%rbp), %edx
	testb	$1, %al
	jne	.L458
.L460:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%edx, -296(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-296(%rbp), %edx
.L459:
	movq	-288(%rbp), %rdi
	movq	%r15, -120(%rbp)
	movabsq	$824633720832, %rcx
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movdqa	-128(%rbp), %xmm7
	movq	%r12, -112(%rbp)
	movdqa	-144(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm4
	movaps	%xmm7, -208(%rbp)
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm6, -224(%rbp)
	movdqa	-96(%rbp), %xmm6
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L515:
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L433
	testb	$1, %al
	je	.L433
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	jne	.L433
	movq	-1(%rax), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	cmpl	$12, %edx
	je	.L433
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$134217728, %edx
	je	.L433
	movq	-1(%rax), %rax
	movq	104(%r15), %rcx
	movq	288(%r15), %rdi
	movq	1016(%r15), %rsi
	movq	23(%rax), %rax
	cmpq	%rcx, %rax
	je	.L435
.L438:
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	jbe	.L433
	movq	15(%rax), %rax
	cmpq	%rax, %rsi
	je	.L478
	cmpq	%rax, %rdi
	jne	.L433
.L478:
	movq	23(%rdx), %rax
	cmpq	%rax, %rcx
	jne	.L438
.L435:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movsd	%xmm3, -280(%rbp)
	call	_ZN2v88internal7Isolate26IsAnyInitialArrayPrototypeENS0_6HandleINS0_7JSArrayEEE@PLT
	movsd	-280(%rbp), %xmm3
	testb	%al, %al
	jne	.L433
	leaq	-256(%rbp), %rdx
	movl	$1, %r8d
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	movsd	%xmm3, -264(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_133MatchArrayElementsKindToArgumentsEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEEPNS0_16BuiltinArgumentsEii
	cmpl	$5, -256(%rbp)
	movsd	-264(%rbp), %xmm3
	je	.L521
.L441:
	movsd	.LC5(%rip), %xmm0
	movapd	%xmm3, %xmm7
	addsd	%xmm0, %xmm7
	movq	%xmm7, %rax
	movq	%xmm7, %rcx
	shrq	$32, %rax
	cmpq	$1127219200, %rax
	je	.L522
.L442:
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L462:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L464
	movq	%r14, %rdx
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L458:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L460
	movq	%r14, %rax
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L517:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-296(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L466
.L522:
	movl	%ecx, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	ucomisd	%xmm1, %xmm3
	jp	.L442
	jne	.L442
	addsd	-272(%rbp), %xmm0
	movq	%xmm0, %rax
	movq	%xmm0, %r8
	shrq	$32, %rax
	cmpq	$1127219200, %rax
	je	.L523
.L447:
	leaq	.LC8(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L521:
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$2, %eax
	je	.L441
	addl	$1, 41104(%r15)
	movq	41088(%r15), %rax
	movl	$2, %esi
	movq	%r14, %rdi
	movq	41096(%r15), %r13
	movsd	%xmm3, -280(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	-264(%rbp), %rax
	subl	$1, 41104(%r15)
	cmpq	41096(%r15), %r13
	movsd	-280(%rbp), %xmm3
	movq	%rax, 41088(%r15)
	je	.L441
	movq	%r13, 41096(%r15)
	movq	%r15, %rdi
	movsd	%xmm3, -264(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movsd	-264(%rbp), %xmm3
	jmp	.L441
.L520:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20879:
	.size	_ZN2v88internalL31Builtin_Impl_ArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_ArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal12_GLOBAL__N_118ArrayConcatVisitor5visitEjNS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118ArrayConcatVisitor5visitEjNS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_118ArrayConcatVisitor5visitEjNS0_6HandleINS0_6ObjectEEE:
.LFB20903:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%rdx, -192(%rbp)
	movl	16(%rdi), %edx
	movl	%edx, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	notl	%ecx
	movl	20(%rdi), %eax
	cmpl	%esi, %ecx
	jbe	.L575
	addl	%edx, %esi
	movq	8(%rdi), %rdx
	movl	%esi, -176(%rbp)
	movq	%rdx, -160(%rbp)
	testb	$4, %al
	je	.L576
	movq	(%rdx), %r14
	andl	$1, %eax
	je	.L531
	movslq	11(%r14), %rsi
	movl	-176(%rbp), %edi
	cmpl	%edi, 11(%r14)
	ja	.L577
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	(%rbx), %r14
	movq	%rax, %r15
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movslq	11(%rax), %rax
	movl	%eax, -152(%rbp)
	testl	%eax, %eax
	je	.L536
	movl	$0, -148(%rbp)
	xorl	%r12d, %r12d
	movq	%rbx, -168(%rbp)
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L552:
	movl	41104(%rbx), %eax
	movq	41088(%rbx), %r14
	addl	$1024, -148(%rbp)
	movl	-148(%rbp), %ecx
	leal	1(%rax), %edx
	movq	41096(%rbx), %r13
	movl	%edx, 41104(%rbx)
	movl	-152(%rbp), %edx
	cmpl	%ecx, %edx
	cmovbe	%edx, %ecx
	movl	%ecx, -172(%rbp)
	cmpl	%r12d, %ecx
	ja	.L548
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L579:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L539:
	movq	-168(%rbp), %rax
	movq	(%rax), %rdi
	cmpq	%rsi, 96(%rdi)
	je	.L541
	xorl	%r8d, %r8d
	movl	$192, %r9d
	movl	%r12d, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal16NumberDictionary3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEENS4_INS0_8JSObjectEEENS0_15PropertyDetailsE@PLT
	cmpq	%rax, %r15
	je	.L541
	movq	(%rax), %rsi
	testq	%r15, %r15
	je	.L543
	testq	%rax, %rax
	je	.L543
	cmpq	%rsi, (%r15)
	je	.L541
.L543:
	subl	$1, 41104(%rbx)
	movq	%r14, 41088(%rbx)
	cmpq	%r13, 41096(%rbx)
	je	.L544
	movq	%r13, 41096(%rbx)
	movq	%rbx, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
.L544:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L545
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L546:
	addl	$1, 41104(%rbx)
	movq	41088(%rbx), %r14
	movq	41096(%rbx), %r13
.L541:
	addl	$1, %r12d
	cmpl	-172(%rbp), %r12d
	je	.L578
.L548:
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %rdi
	movq	(%rax), %rdx
	movq	(%rdi), %rcx
	leal	16(,%r12,8), %eax
	cltq
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L579
	movq	41088(%rdx), %rcx
	cmpq	41096(%rdx), %rcx
	je	.L580
.L540:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L576:
	movq	(%rdx), %rax
	movq	(%rdi), %rbx
	testb	$1, %al
	jne	.L528
.L530:
	movl	-176(%rbp), %edx
	movq	-160(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
.L529:
	movq	-160(%rbp), %rdi
	movq	%rax, -80(%rbp)
	leaq	-144(%rbp), %r12
	movabsq	$824633720832, %rdx
	movl	-176(%rbp), %eax
	movq	%rdx, -132(%rbp)
	movq	%rdi, -96(%rbp)
	movq	%r12, %rdi
	movl	$1, -144(%rbp)
	movq	%rbx, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	-192(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE@PLT
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L578:
	subl	$1, 41104(%rbx)
	movq	%r14, 41088(%rbx)
	cmpq	41096(%rbx), %r13
	je	.L549
	movq	%r13, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	cmpl	%r12d, -152(%rbp)
	ja	.L552
.L574:
	movq	-168(%rbp), %rbx
.L536:
	movq	8(%rbx), %rdi
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	(%r15), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	andl	$-2, 20(%rbx)
	movq	%rax, 8(%rbx)
	movq	(%rax), %r14
.L531:
	movq	(%rbx), %r13
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L553
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L554:
	movq	(%rbx), %rdi
	movq	-192(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movl	-176(%rbp), %edx
	movl	$192, %r9d
	call	_ZN2v88internal16NumberDictionary3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEENS4_INS0_8JSObjectEEENS0_15PropertyDetailsE@PLT
	movq	%rax, %r13
	cmpq	%rax, %r12
	je	.L561
	testq	%r12, %r12
	je	.L556
	testq	%rax, %rax
	je	.L556
	movq	(%r12), %rdx
	movl	$1, %eax
	cmpq	%rdx, 0(%r13)
	je	.L524
.L556:
	movq	8(%rbx), %rdi
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	0(%r13), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 8(%rbx)
	movl	$1, %eax
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L575:
	orl	$2, %eax
	movl	%eax, 20(%rdi)
	movl	$1, %eax
.L524:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L581
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L582
.L555:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%r12)
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L577:
	movq	-192(%rbp), %rsi
	leal	16(,%rdi,8), %edx
	movslq	%edx, %rdx
	movq	(%rsi), %r12
	leaq	-1(%r14,%rdx), %r13
	movq	%r12, 0(%r13)
	testb	$1, %r12b
	je	.L524
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rdx
	testl	$262144, %edx
	je	.L534
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movb	%al, -148(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rdx
	movzbl	-148(%rbp), %eax
.L534:
	andl	$24, %edx
	je	.L524
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L524
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movb	%al, -148(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movzbl	-148(%rbp), %eax
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L528:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L530
	movq	%rdx, %rax
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L545:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L583
.L547:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L580:
	movq	%rdx, %rdi
	movq	%rsi, -200(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	-184(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L537:
	movl	%eax, 41104(%rbx)
.L549:
	cmpl	%r12d, -152(%rbp)
	ja	.L552
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L583:
	movq	%rbx, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L561:
	movl	$1, %eax
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L582:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L555
.L581:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20903:
	.size	_ZN2v88internal12_GLOBAL__N_118ArrayConcatVisitor5visitEjNS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal12_GLOBAL__N_118ArrayConcatVisitor5visitEjNS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_119IterateElementsSlowEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEjPNS1_18ArrayConcatVisitorE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119IterateElementsSlowEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEjPNS1_18ArrayConcatVisitorE, @function
_ZN2v88internal12_GLOBAL__N_119IterateElementsSlowEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEjPNS1_18ArrayConcatVisitorE:
.LFB20923:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -152(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L585
	movl	$0, -160(%rbp)
	movq	%rdi, %r12
	movq	%rsi, %r14
	xorl	%r15d, %r15d
	movabsq	$824633720832, %r13
.L598:
	movq	41088(%r12), %rax
	movl	-152(%rbp), %esi
	addl	$1024, -160(%rbp)
	movl	-160(%rbp), %ecx
	movq	%rax, -184(%rbp)
	movq	41096(%r12), %rax
	cmpl	%ecx, %esi
	movq	%rax, -176(%rbp)
	movl	41104(%r12), %eax
	cmovbe	%esi, %ecx
	leal	1(%rax), %edx
	movl	%ecx, -148(%rbp)
	movl	%edx, 41104(%r12)
	cmpl	%ecx, %r15d
	jnb	.L586
	leaq	-144(%rbp), %rbx
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L588:
	addl	$1, %r15d
	cmpl	-148(%rbp), %r15d
	je	.L641
.L592:
	movq	(%r14), %rax
	movq	%rbx, %rdi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	%r13, -132(%rbp)
	movl	$3, -144(%rbp)
	subq	$37592, %rax
	movq	$0, -112(%rbp)
	movq	%rax, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r14, -80(%rbp)
	movl	%r15d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	movzbl	%ah, %edx
	testb	%al, %al
	je	.L587
	testb	%dl, %dl
	je	.L588
	movq	%rbx, %rdi
	movq	%r13, -132(%rbp)
	movl	$3, -144(%rbp)
	movq	%r12, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r14, -80(%rbp)
	movl	%r15d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L589
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rdx
.L590:
	movq	-168(%rbp), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal12_GLOBAL__N_118ArrayConcatVisitor5visitEjNS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	jne	.L588
	.p2align 4,,10
	.p2align 3
.L587:
	subl	$1, 41104(%r12)
	movq	-184(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-176(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L618
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L618:
	xorl	%r12d, %r12d
.L584:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L642
	addq	$168, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L589:
	.cfi_restore_state
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L590
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L641:
	subl	$1, 41104(%r12)
	movq	-184(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-176(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L594
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	cmpl	%r15d, -152(%rbp)
	ja	.L598
.L596:
	movq	-168(%rbp), %rax
	movl	16(%rax), %eax
	movl	%eax, %edx
	notl	%edx
	cmpl	%edx, -152(%rbp)
	jbe	.L619
	movl	$-1, %eax
.L599:
	movq	-168(%rbp), %rsi
	movl	20(%rsi), %ebx
	movl	%eax, 16(%rsi)
	movl	%ebx, -148(%rbp)
	movzbl	-148(%rbp), %r12d
	andl	$1, %r12d
	je	.L622
	movq	8(%rsi), %rbx
	movq	(%rbx), %rdx
	movslq	11(%rdx), %rsi
	cmpl	%eax, 11(%rdx)
	jnb	.L584
	movq	-168(%rbp), %r15
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	(%r15), %rdi
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	%r15, %rsi
	movq	(%r15), %r15
	movq	%rax, %r14
	movq	(%rbx), %rax
	movslq	11(%rax), %rax
	movl	%eax, -184(%rbp)
	testl	%eax, %eax
	je	.L600
	xorl	%r13d, %r13d
	movb	%r12b, -201(%rbp)
	movq	%rsi, %r12
	movl	$0, -152(%rbp)
	movl	%r13d, %eax
	movq	%rbx, %r13
	movl	%eax, %ebx
.L616:
	movq	41088(%r15), %rax
	addl	$1024, -152(%rbp)
	movl	-152(%rbp), %esi
	movq	%rax, -160(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -176(%rbp)
	movl	41104(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 41104(%r15)
	movl	-184(%rbp), %ecx
	cmpl	%esi, %ecx
	cmovbe	%ecx, %esi
	movl	%esi, -148(%rbp)
	cmpl	%ebx, %esi
	ja	.L612
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L644:
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L603:
	movq	(%r12), %rdi
	cmpq	%rsi, 96(%rdi)
	je	.L605
	xorl	%r8d, %r8d
	movl	$192, %r9d
	movl	%ebx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal16NumberDictionary3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEENS4_INS0_8JSObjectEEENS0_15PropertyDetailsE@PLT
	cmpq	%rax, %r14
	je	.L605
	movq	(%rax), %rsi
	testq	%r14, %r14
	je	.L607
	testq	%rax, %rax
	je	.L607
	cmpq	%rsi, (%r14)
	je	.L605
.L607:
	movq	-160(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-176(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L608
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
.L608:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L609
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L610:
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	%rax, -160(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -176(%rbp)
.L605:
	addl	$1, %ebx
	cmpl	%ebx, -148(%rbp)
	je	.L643
.L612:
	movq	(%r12), %rdi
	movq	0(%r13), %rcx
	leal	16(,%rbx,8), %eax
	cltq
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	jne	.L644
	movq	41088(%rdi), %rcx
	cmpq	41096(%rdi), %rcx
	je	.L645
.L604:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%rcx)
	jmp	.L603
.L586:
	movl	%eax, 41104(%r12)
.L594:
	cmpl	%r15d, -152(%rbp)
	ja	.L598
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L609:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L646
.L611:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L610
.L643:
	movq	-160(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-176(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L613
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	cmpl	-184(%rbp), %ebx
	jb	.L616
.L640:
	movzbl	-201(%rbp), %r12d
.L600:
	movq	-168(%rbp), %rbx
	movq	8(%rbx), %rdi
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	(%r14), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	andl	$-2, 20(%rbx)
	movq	%rax, 8(%rbx)
	jmp	.L584
.L645:
	movq	%rsi, -200(%rbp)
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	-192(%rbp), %rdi
	movq	%rax, %rcx
	jmp	.L604
.L601:
	movl	%eax, 41104(%r15)
.L613:
	cmpl	%ebx, -184(%rbp)
	ja	.L616
	jmp	.L640
.L585:
	movl	16(%rcx), %eax
.L619:
	addl	-152(%rbp), %eax
	jmp	.L599
.L622:
	movl	$1, %r12d
	jmp	.L584
.L646:
	movq	%r15, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L611
.L642:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20923:
	.size	_ZN2v88internal12_GLOBAL__N_119IterateElementsSlowEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEjPNS1_18ArrayConcatVisitorE, .-_ZN2v88internal12_GLOBAL__N_119IterateElementsSlowEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEjPNS1_18ArrayConcatVisitorE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L653
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L656
.L647:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L653:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L647
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"V8.Builtin_ArrayUnshift"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateE:
.LFB20893:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	movq	%r14, %r10
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L690
.L658:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateEE28trace_event_unique_atomic595(%rip), %rbx
	testq	%rbx, %rbx
	je	.L691
.L660:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L692
.L662:
	movl	$1, %ecx
	leal	-5(%r10), %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	addl	$1, 41104(%r12)
	leaq	-176(%rbp), %r15
	movq	41096(%r12), %rbx
	movq	%r15, %rdx
	movq	%r14, -176(%rbp)
	movq	41088(%r12), %r14
	movq	%r13, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_133MatchArrayElementsKindToArgumentsEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEEPNS0_16BuiltinArgumentsEii
	movl	-176(%rbp), %ecx
	movq	0(%r13), %rax
	subl	$5, %ecx
	jne	.L666
	movq	23(%rax), %r13
.L667:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L670
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L670:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L693
.L657:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L694
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L666:
	.cfi_restore_state
	movq	-1(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	%r13, %rsi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%rax, %r13
	salq	$32, %r13
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L692:
	pxor	%xmm0, %xmm0
	movl	%r10d, -180(%rbp)
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movl	-180(%rbp), %r10d
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L695
.L663:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L664
	movq	(%rdi), %rax
	movl	%r10d, -180(%rbp)
	call	*8(%rax)
	movl	-180(%rbp), %r10d
.L664:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L665
	movq	(%rdi), %rax
	movl	%r10d, -180(%rbp)
	call	*8(%rax)
	movl	-180(%rbp), %r10d
.L665:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L691:
	movl	%r10d, -180(%rbp)
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	movl	-180(%rbp), %r10d
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L696
.L661:
	movq	%rbx, _ZZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateEE28trace_event_unique_atomic595(%rip)
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L690:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$681, %edx
	movl	%r14d, -180(%rbp)
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movl	-180(%rbp), %r10d
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L693:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L696:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movl	-180(%rbp), %r10d
	movq	%rax, %rbx
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L695:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movl	-180(%rbp), %r10d
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L663
.L694:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20893:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_Stats_ArrayPrototypeFillEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"V8.Builtin_ArrayPrototypeFill"
	.section	.text._ZN2v88internalL37Builtin_Impl_Stats_ArrayPrototypeFillEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_Stats_ArrayPrototypeFillEiPmPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_Stats_ArrayPrototypeFillEiPmPNS0_7IsolateE:
.LFB20877:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L726
.L698:
	movq	_ZZN2v88internalL37Builtin_Impl_Stats_ArrayPrototypeFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic245(%rip), %rbx
	testq	%rbx, %rbx
	je	.L727
.L700:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L728
.L702:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL31Builtin_Impl_ArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L729
.L706:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L730
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L727:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L731
.L701:
	movq	%rbx, _ZZN2v88internalL37Builtin_Impl_Stats_ArrayPrototypeFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic245(%rip)
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L728:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L732
.L703:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L704
	movq	(%rdi), %rax
	call	*8(%rax)
.L704:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L705
	movq	(%rdi), %rax
	call	*8(%rax)
.L705:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L729:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L726:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$677, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L732:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L731:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L701
.L730:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20877:
	.size	_ZN2v88internalL37Builtin_Impl_Stats_ArrayPrototypeFillEiPmPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_Stats_ArrayPrototypeFillEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL28Builtin_Impl_Stats_ArrayPushEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"V8.Builtin_ArrayPush"
	.section	.text._ZN2v88internalL28Builtin_Impl_Stats_ArrayPushEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Builtin_Impl_Stats_ArrayPushEiPmPNS0_7IsolateE, @function
_ZN2v88internalL28Builtin_Impl_Stats_ArrayPushEiPmPNS0_7IsolateE:
.LFB20881:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L762
.L734:
	movq	_ZZN2v88internalL28Builtin_Impl_Stats_ArrayPushEiPmPNS0_7IsolateEE28trace_event_unique_atomic366(%rip), %rbx
	testq	%rbx, %rbx
	je	.L763
.L736:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L764
.L738:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL22Builtin_Impl_ArrayPushENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L765
.L742:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L766
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L763:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L767
.L737:
	movq	%rbx, _ZZN2v88internalL28Builtin_Impl_Stats_ArrayPushEiPmPNS0_7IsolateEE28trace_event_unique_atomic366(%rip)
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L764:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L768
.L739:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L740
	movq	(%rdi), %rax
	call	*8(%rax)
.L740:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L741
	movq	(%rdi), %rax
	call	*8(%rax)
.L741:
	leaq	.LC12(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L765:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L762:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$679, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L768:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L767:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L737
.L766:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20881:
	.size	_ZN2v88internalL28Builtin_Impl_Stats_ArrayPushEiPmPNS0_7IsolateE, .-_ZN2v88internalL28Builtin_Impl_Stats_ArrayPushEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL27Builtin_Impl_Stats_ArrayPopEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"V8.Builtin_ArrayPop"
	.section	.text._ZN2v88internalL27Builtin_Impl_Stats_ArrayPopEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Builtin_Impl_Stats_ArrayPopEiPmPNS0_7IsolateE, @function
_ZN2v88internalL27Builtin_Impl_Stats_ArrayPopEiPmPNS0_7IsolateE:
.LFB20885:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L798
.L770:
	movq	_ZZN2v88internalL27Builtin_Impl_Stats_ArrayPopEiPmPNS0_7IsolateEE28trace_event_unique_atomic452(%rip), %rbx
	testq	%rbx, %rbx
	je	.L799
.L772:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L800
.L774:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL21Builtin_Impl_ArrayPopENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L801
.L778:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L802
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L803
.L773:
	movq	%rbx, _ZZN2v88internalL27Builtin_Impl_Stats_ArrayPopEiPmPNS0_7IsolateEE28trace_event_unique_atomic452(%rip)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L800:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L804
.L775:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L776
	movq	(%rdi), %rax
	call	*8(%rax)
.L776:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L777
	movq	(%rdi), %rax
	call	*8(%rax)
.L777:
	leaq	.LC13(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L774
	.p2align 4,,10
	.p2align 3
.L801:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L798:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$678, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L804:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L803:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L773
.L802:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20885:
	.size	_ZN2v88internalL27Builtin_Impl_Stats_ArrayPopEiPmPNS0_7IsolateE, .-_ZN2v88internalL27Builtin_Impl_Stats_ArrayPopEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Builtin_Impl_Stats_ArrayShiftEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"V8.Builtin_ArrayShift"
	.section	.text._ZN2v88internalL29Builtin_Impl_Stats_ArrayShiftEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Builtin_Impl_Stats_ArrayShiftEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Builtin_Impl_Stats_ArrayShiftEiPmPNS0_7IsolateE:
.LFB20890:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L834
.L806:
	movq	_ZZN2v88internalL29Builtin_Impl_Stats_ArrayShiftEiPmPNS0_7IsolateEE28trace_event_unique_atomic564(%rip), %rbx
	testq	%rbx, %rbx
	je	.L835
.L808:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L836
.L810:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL23Builtin_Impl_ArrayShiftENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L837
.L814:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L838
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L835:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L839
.L809:
	movq	%rbx, _ZZN2v88internalL29Builtin_Impl_Stats_ArrayShiftEiPmPNS0_7IsolateEE28trace_event_unique_atomic564(%rip)
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L836:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L840
.L811:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L812
	movq	(%rdi), %rax
	call	*8(%rax)
.L812:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L813
	movq	(%rdi), %rax
	call	*8(%rax)
.L813:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L837:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L834:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$680, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L840:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L839:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L809
.L838:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20890:
	.size	_ZN2v88internalL29Builtin_Impl_Stats_ArrayShiftEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Builtin_Impl_Stats_ArrayShiftEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Builtin_ArrayPrototypeFillEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Builtin_ArrayPrototypeFillEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Builtin_ArrayPrototypeFillEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Builtin_ArrayPrototypeFillEiPmPNS0_7IsolateE:
.LFB20878:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L845
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL31Builtin_Impl_ArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L845:
	.cfi_restore 6
	jmp	_ZN2v88internalL37Builtin_Impl_Stats_ArrayPrototypeFillEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20878:
	.size	_ZN2v88internal26Builtin_ArrayPrototypeFillEiPmPNS0_7IsolateE, .-_ZN2v88internal26Builtin_ArrayPrototypeFillEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal17Builtin_ArrayPushEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Builtin_ArrayPushEiPmPNS0_7IsolateE
	.type	_ZN2v88internal17Builtin_ArrayPushEiPmPNS0_7IsolateE, @function
_ZN2v88internal17Builtin_ArrayPushEiPmPNS0_7IsolateE:
.LFB20882:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L850
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL22Builtin_Impl_ArrayPushENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L850:
	.cfi_restore 6
	jmp	_ZN2v88internalL28Builtin_Impl_Stats_ArrayPushEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20882:
	.size	_ZN2v88internal17Builtin_ArrayPushEiPmPNS0_7IsolateE, .-_ZN2v88internal17Builtin_ArrayPushEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal16Builtin_ArrayPopEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16Builtin_ArrayPopEiPmPNS0_7IsolateE
	.type	_ZN2v88internal16Builtin_ArrayPopEiPmPNS0_7IsolateE, @function
_ZN2v88internal16Builtin_ArrayPopEiPmPNS0_7IsolateE:
.LFB20886:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L855
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL21Builtin_Impl_ArrayPopENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L855:
	.cfi_restore 6
	jmp	_ZN2v88internalL27Builtin_Impl_Stats_ArrayPopEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20886:
	.size	_ZN2v88internal16Builtin_ArrayPopEiPmPNS0_7IsolateE, .-_ZN2v88internal16Builtin_ArrayPopEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Builtin_ArrayShiftEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Builtin_ArrayShiftEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Builtin_ArrayShiftEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Builtin_ArrayShiftEiPmPNS0_7IsolateE:
.LFB20891:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L860
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL23Builtin_Impl_ArrayShiftENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L860:
	.cfi_restore 6
	jmp	_ZN2v88internalL29Builtin_Impl_Stats_ArrayShiftEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20891:
	.size	_ZN2v88internal18Builtin_ArrayShiftEiPmPNS0_7IsolateE, .-_ZN2v88internal18Builtin_ArrayShiftEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_ArrayUnshiftEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_ArrayUnshiftEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_ArrayUnshiftEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_ArrayUnshiftEiPmPNS0_7IsolateE:
.LFB20894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L885
	movslq	%edi, %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	(%rsi), %r8
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	cmpl	$5, %edi
	jg	.L881
.L871:
	movl	%edi, %ecx
	subl	$5, %ecx
	jne	.L872
.L890:
	movq	23(%r8), %r13
.L873:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L861
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L861:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L886
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L881:
	.cfi_restore_state
	movq	-1(%r8), %rdx
	movzbl	14(%rdx), %ecx
	shrl	$3, %ecx
	leal	-2(%rcx), %edx
	cmpb	$1, %dl
	jbe	.L887
	leal	-6(%rdi), %eax
	movzbl	%cl, %esi
	movq	%r13, %rdi
	leaq	16(,%rax,8), %r9
	movl	$8, %eax
.L868:
	movq	%rdi, %rdx
	subq	%rax, %rdx
	movq	(%rdx), %rdx
	testb	$1, %dl
	jne	.L888
.L866:
	addq	$8, %rax
	cmpq	%rax, %r9
	jne	.L868
	cmpb	%sil, %cl
	jne	.L889
.L869:
	movl	-80(%rbp), %edi
.L892:
	movl	%edi, %ecx
	subl	$5, %ecx
	je	.L890
.L872:
	movq	-1(%r8), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	%r13, %rsi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	leaq	-80(%rbp), %rdx
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%rax, %r13
	salq	$32, %r13
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L888:
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	je	.L891
	movl	$2, %esi
	cmpb	%sil, %cl
	je	.L869
.L889:
	addl	$1, 41104(%r12)
	movq	41088(%r12), %rax
	movq	%r13, %rdi
	movq	41096(%r12), %r15
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	-88(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L884
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L884:
	movq	0(%r13), %r8
	movl	-80(%rbp), %edi
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L891:
	movl	$4, %esi
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L887:
	movl	%eax, %edi
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L885:
	call	_ZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L861
.L886:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20894:
	.size	_ZN2v88internal20Builtin_ArrayUnshiftEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_ArrayUnshiftEiPmPNS0_7IsolateE
	.section	.rodata._ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC15:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	.type	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_, @function
_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_:
.LFB23942:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L907
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L903
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L908
.L895:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L902:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L909
	testq	%r13, %r13
	jg	.L898
	testq	%r9, %r9
	jne	.L901
.L899:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L898
.L901:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L908:
	testq	%rsi, %rsi
	jne	.L896
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L898:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L899
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L903:
	movl	$4, %r14d
	jmp	.L895
.L907:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L896:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L895
	.cfi_endproc
.LFE23942:
	.size	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_, .-_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	.section	.text._ZN2v88internal12_GLOBAL__N_121CollectElementIndicesEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEjPSt6vectorIjSaIjEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121CollectElementIndicesEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEjPSt6vectorIjSaIjEE, @function
_ZN2v88internal12_GLOBAL__N_121CollectElementIndicesEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEjPSt6vectorIjSaIjEE:
.LFB20922:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movl	%edx, -68(%rbp)
	movq	(%rsi), %r14
	movq	%rsi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%r14), %rax
	movzbl	14(%rax), %eax
	movl	%eax, %edx
	shrl	$3, %edx
	cmpl	$223, %eax
	ja	.L974
	leaq	.L913(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12_GLOBAL__N_121CollectElementIndicesEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEjPSt6vectorIjSaIjEE,"a",@progbits
	.align 4
	.align 4
.L913:
	.long	.L917-.L913
	.long	.L917-.L913
	.long	.L917-.L913
	.long	.L917-.L913
	.long	.L918-.L913
	.long	.L918-.L913
	.long	.L917-.L913
	.long	.L917-.L913
	.long	.L917-.L913
	.long	.L917-.L913
	.long	.L917-.L913
	.long	.L917-.L913
	.long	.L916-.L913
	.long	.L915-.L913
	.long	.L915-.L913
	.long	.L914-.L913
	.long	.L914-.L913
	.long	.L912-.L913
	.long	.L912-.L913
	.long	.L912-.L913
	.long	.L912-.L913
	.long	.L912-.L913
	.long	.L912-.L913
	.long	.L912-.L913
	.long	.L912-.L913
	.long	.L912-.L913
	.long	.L912-.L913
	.long	.L912-.L913
	.section	.text._ZN2v88internal12_GLOBAL__N_121CollectElementIndicesEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEjPSt6vectorIjSaIjEE
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	-96(%rbp), %rax
	movl	%r15d, %r12d
	movq	-112(%rbp), %r15
	movq	%rax, 41088(%r15)
	movq	-104(%rbp), %rax
	subl	$1, 41104(%r15)
	cmpq	41096(%r15), %rax
	je	.L941
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	cmpl	%r12d, -72(%rbp)
	ja	.L944
.L1009:
	movq	-88(%rbp), %r12
.L1010:
	movq	-120(%rbp), %rax
	movq	(%rax), %r14
.L974:
	movq	-1(%r14), %rax
	cmpw	$1024, 11(%rax)
	je	.L910
	movq	-1(%r14), %rax
	movq	41112(%r15), %rdi
	movq	104(%r15), %r13
	movq	23(%rax), %rbx
	testq	%rdi, %rdi
	je	.L1011
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L969:
	cmpq	%r13, %rbx
	je	.L910
	movl	-68(%rbp), %edx
	movq	%r12, %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_121CollectElementIndicesEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEjPSt6vectorIjSaIjEE
.L910:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1012
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1011:
	.cfi_restore_state
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1013
.L970:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%rbx, (%rsi)
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L912:
	movq	47(%r14), %rax
	movl	%eax, %ebx
	cmpl	-68(%rbp), %eax
	jb	.L945
	movq	(%r12), %rsi
	cmpq	8(%r12), %rsi
	je	.L946
	movq	%rsi, 8(%r12)
.L946:
	movl	-68(%rbp), %ebx
	movl	$0, -60(%rbp)
	testl	%ebx, %ebx
	je	.L910
.L976:
	xorl	%eax, %eax
	leaq	-60(%rbp), %r13
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L1014:
	movl	%eax, (%rsi)
	movl	-60(%rbp), %eax
	addq	$4, 8(%r12)
	addl	$1, %eax
	movl	%eax, -60(%rbp)
	cmpl	%eax, %ebx
	jbe	.L949
.L950:
	movq	8(%r12), %rsi
.L951:
	cmpq	%rsi, 16(%r12)
	jne	.L1014
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	movl	-60(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -60(%rbp)
	cmpl	%ebx, %eax
	jb	.L950
	.p2align 4,,10
	.p2align 3
.L949:
	cmpl	%ebx, -68(%rbp)
	je	.L910
	movq	-120(%rbp), %rax
	movq	(%rax), %r14
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L917:
	movq	15(%r14), %r13
	movl	-68(%rbp), %eax
	movslq	11(%r13), %rbx
	movl	$0, -60(%rbp)
	cmpl	%ebx, %eax
	cmovbe	%eax, %ebx
	testl	%ebx, %ebx
	je	.L974
	xorl	%eax, %eax
	leaq	-60(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L922:
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%r13,%rax), %rax
	cmpq	%rax, 96(%r15)
	je	.L920
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L921
	movl	-60(%rbp), %eax
	movl	%eax, (%rsi)
	addq	$4, 8(%r12)
.L920:
	movl	-60(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -60(%rbp)
	cmpl	%eax, %ebx
	ja	.L922
	movq	-120(%rbp), %rax
	movq	(%rax), %r14
	jmp	.L974
.L915:
	movq	15(%r14), %rbx
	movq	-1(%r14), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %r13
	leaq	-60(%rbp), %rax
	movl	$0, -60(%rbp)
	xorl	%edx, %edx
	movq	%rax, -88(%rbp)
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	je	.L974
	movq	%r13, %r15
	movq	%rdi, -80(%rbp)
	movl	%eax, %r13d
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L954:
	movl	-60(%rbp), %eax
	leal	1(%rax), %edx
	movl	%edx, -60(%rbp)
	cmpl	%r13d, %edx
	jnb	.L1015
.L953:
	movq	(%r15), %rax
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*24(%rax)
	testb	%al, %al
	je	.L954
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L955
	movl	-60(%rbp), %eax
	movl	%eax, (%rsi)
	movl	-60(%rbp), %eax
	addq	$4, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, -60(%rbp)
	cmpl	%r13d, %edx
	jb	.L953
.L1015:
	movq	-120(%rbp), %rax
	movq	-80(%rbp), %r15
	movq	(%rax), %r14
	jmp	.L974
.L914:
	movq	41112(%rdi), %rdi
	movq	23(%r14), %rsi
	testq	%rdi, %rdi
	je	.L957
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L958:
	movl	11(%rsi), %ebx
	movl	-68(%rbp), %eax
	leaq	-60(%rbp), %r13
	movl	$0, -60(%rbp)
	cmpl	%ebx, %eax
	cmovbe	%eax, %ebx
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.L964
	.p2align 4,,10
	.p2align 3
.L960:
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L963
	movl	%eax, (%rsi)
	movl	-60(%rbp), %eax
	addq	$4, 8(%r12)
	addl	$1, %eax
	movl	%eax, -60(%rbp)
	cmpl	%eax, %ebx
	ja	.L960
.L964:
	movq	-120(%rbp), %rdi
	movq	(%rdi), %r14
	movq	-1(%r14), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %r13
	movl	-60(%rbp), %edx
	movl	-68(%rbp), %eax
	cmpl	%edx, %eax
	jbe	.L974
	leaq	-60(%rbp), %rbx
	movq	%r14, %rsi
	movq	%r13, %r14
	movq	%rdi, %r13
	movq	%rbx, -80(%rbp)
	movl	%eax, %ebx
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L966:
	movl	-60(%rbp), %eax
	leal	1(%rax), %edx
	movl	%edx, -60(%rbp)
	cmpl	%ebx, %edx
	jnb	.L1010
	movq	0(%r13), %rsi
.L962:
	movq	(%r14), %rax
	xorl	%r8d, %r8d
	movq	15(%rsi), %rcx
	movq	%r14, %rdi
	call	*24(%rax)
	testb	%al, %al
	je	.L966
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L967
	movl	-60(%rbp), %eax
	movl	%eax, (%rsi)
	addq	$4, 8(%r12)
	jmp	.L966
.L918:
	movq	15(%r14), %rbx
	movq	-1(%rbx), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	jbe	.L974
	movq	41112(%rdi), %rdi
	movq	%rbx, %r14
	testq	%rdi, %rdi
	je	.L925
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
	movq	%rax, %r13
.L926:
	movslq	11(%r14), %rbx
	movl	-68(%rbp), %eax
	movl	$0, -60(%rbp)
	movabsq	$-2251799814209537, %r14
	cmpl	%ebx, %eax
	cmovbe	%eax, %ebx
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.L1010
	.p2align 4,,10
	.p2align 3
.L928:
	leal	16(,%rax,8), %edx
	movq	0(%r13), %rcx
	movslq	%edx, %rdx
	cmpq	%r14, -1(%rdx,%rcx)
	je	.L929
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L930
	movl	%eax, (%rsi)
	movl	-60(%rbp), %eax
	addq	$4, 8(%r12)
.L929:
	addl	$1, %eax
	movl	%eax, -60(%rbp)
	cmpl	%ebx, %eax
	jb	.L928
	jmp	.L1010
.L916:
	movq	15(%r14), %rax
	leaq	56(%rdi), %rbx
	movq	%rax, -128(%rbp)
	movslq	35(%rax), %rax
	movl	%eax, -72(%rbp)
	testl	%eax, %eax
	je	.L974
	leaq	-60(%rbp), %rax
	movl	$0, -80(%rbp)
	xorl	%r14d, %r14d
	movq	%rax, -136(%rbp)
	movq	%r12, -88(%rbp)
	movl	%r14d, %r12d
	movq	%rbx, %r14
	.p2align 4,,10
	.p2align 3
.L944:
	movq	41088(%r15), %rax
	addl	$1024, -80(%rbp)
	movl	-72(%rbp), %ebx
	movl	-80(%rbp), %edi
	movq	%rax, -96(%rbp)
	movq	41096(%r15), %rax
	cmpl	%edi, %ebx
	movq	%rax, -104(%rbp)
	movl	41104(%r15), %eax
	cmovbe	%ebx, %edi
	leal	1(%rax), %edx
	movl	%edi, %r13d
	movl	%edx, 41104(%r15)
	cmpl	%r12d, %edi
	jbe	.L933
	movq	-128(%rbp), %rax
	movq	%r15, -112(%rbp)
	movl	%r12d, %r15d
	leaq	-1(%rax), %rbx
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	%r12, %rdx
	pxor	%xmm0, %xmm0
	sarq	$32, %rdx
	cvtsi2sdl	%edx, %xmm0
	cvttsd2siq	%xmm0, %rax
	movl	%eax, -60(%rbp)
	cmpl	-68(%rbp), %eax
	jb	.L1016
.L939:
	addl	$1, %r15d
	cmpl	%r13d, %r15d
	je	.L1017
.L936:
	leal	6(%r15,%r15,2), %eax
	movq	%r14, %rdi
	sall	$3, %eax
	cltq
	movq	(%rax,%rbx), %r12
	movq	%r12, %rsi
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE5IsKeyENS0_13ReadOnlyRootsENS0_6ObjectE@PLT
	testb	%al, %al
	je	.L939
	testb	$1, %r12b
	je	.L1018
	movsd	7(%r12), %xmm0
	cvttsd2siq	%xmm0, %rax
	movl	%eax, -60(%rbp)
	cmpl	-68(%rbp), %eax
	jnb	.L939
.L1016:
	movq	-88(%rbp), %rcx
	movq	8(%rcx), %rsi
	cmpq	16(%rcx), %rsi
	je	.L940
	movl	%eax, (%rsi)
	addq	$4, 8(%rcx)
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L933:
	movl	%eax, 41104(%r15)
.L941:
	cmpl	%r12d, -72(%rbp)
	ja	.L944
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L940:
	movq	-136(%rbp), %rdx
	movq	%rcx, %rdi
	call	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L963:
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	movl	-60(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -60(%rbp)
	cmpl	%ebx, %eax
	jb	.L960
	jmp	.L964
.L921:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	jmp	.L920
.L945:
	movl	$0, -60(%rbp)
	testl	%eax, %eax
	je	.L974
	movq	8(%r12), %rsi
	jmp	.L976
.L955:
	movq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	jmp	.L954
.L957:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L1019
.L959:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L958
.L967:
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	jmp	.L966
.L925:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L1020
.L927:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rbx, 0(%r13)
	jmp	.L926
.L930:
	leaq	-60(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	movl	-60(%rbp), %eax
	jmp	.L929
.L1013:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L970
.L1019:
	movq	%r15, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	jmp	.L959
.L1020:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L927
.L1012:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20922:
	.size	_ZN2v88internal12_GLOBAL__N_121CollectElementIndicesEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEjPSt6vectorIjSaIjEE, .-_ZN2v88internal12_GLOBAL__N_121CollectElementIndicesEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEjPSt6vectorIjSaIjEE
	.section	.text._ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEEljNS0_5__ops15_Iter_less_iterEEvT_T0_SA_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEEljNS0_5__ops15_Iter_less_iterEEvT_T0_SA_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEEljNS0_5__ops15_Iter_less_iterEEvT_T0_SA_T1_T2_
	.type	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEEljNS0_5__ops15_Iter_less_iterEEvT_T0_SA_T1_T2_, @function
_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEEljNS0_5__ops15_Iter_less_iterEEvT_T0_SA_T1_T2_:
.LFB24830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rax, %r13
	andl	$1, %r14d
	shrq	$63, %r13
	pushq	%r12
	addq	%rax, %r13
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	sarq	%r13
	cmpq	%r13, %rsi
	jge	.L1022
	movq	%rsi, %r11
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1036:
	movl	%ebx, (%rdi,%r11,4)
	cmpq	%rax, %r13
	jle	.L1024
.L1025:
	movq	%rax, %r11
.L1026:
	leaq	1(%r11), %r8
	leaq	(%r8,%r8), %rax
	leaq	(%rdi,%r8,8), %r9
	leaq	-1(%rax), %r10
	movl	(%r9), %ebx
	leaq	(%rdi,%r10,4), %r12
	movl	(%r12), %r8d
	cmpl	%ebx, %r8d
	jbe	.L1036
	movl	%r8d, (%rdi,%r11,4)
	cmpq	%r10, %r13
	jle	.L1031
	movq	%r10, %rax
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	%r12, %r9
	movq	%r10, %rax
.L1024:
	testq	%r14, %r14
	je	.L1030
.L1027:
	leaq	-1(%rax), %rdx
	movq	%rdx, %r8
	shrq	$63, %r8
	addq	%rdx, %r8
	sarq	%r8
	cmpq	%rsi, %rax
	jg	.L1029
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1038:
	movl	%edx, (%r9)
	leaq	-1(%r8), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, %rdx
	movq	%r8, %rax
	cmpq	%r8, %rsi
	jge	.L1037
	movq	%rdx, %r8
.L1029:
	leaq	(%rdi,%r8,4), %r10
	leaq	(%rdi,%rax,4), %r9
	movl	(%r10), %edx
	cmpl	%edx, %ecx
	ja	.L1038
.L1028:
	movl	%ecx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1022:
	.cfi_restore_state
	leaq	(%rdi,%rsi,4), %r9
	testq	%r14, %r14
	jne	.L1028
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L1030:
	leaq	-2(%rdx), %r8
	movq	%r8, %rdx
	shrq	$63, %rdx
	addq	%r8, %rdx
	sarq	%rdx
	cmpq	%rax, %rdx
	jne	.L1027
	leaq	1(%rax,%rax), %rax
	leaq	(%rdi,%rax,4), %rdx
	movl	(%rdx), %r8d
	movl	%r8d, (%r9)
	movq	%rdx, %r9
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1037:
	movq	%r10, %r9
	movl	%ecx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24830:
	.size	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEEljNS0_5__ops15_Iter_less_iterEEvT_T0_SA_T1_T2_, .-_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEEljNS0_5__ops15_Iter_less_iterEEvT_T0_SA_T1_T2_
	.section	.text._ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_,comdat
	.p2align 4
	.weak	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_
	.type	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_, @function
_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_:
.LFB24506:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$64, %rax
	jle	.L1068
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L1041
	leaq	4(%rdi), %r14
.L1043:
	movq	%rsi, %rax
	movl	-4(%rsi), %edi
	movl	(%rbx), %ecx
	subq	$1, %r15
	subq	%rbx, %rax
	movq	%rax, %rdx
	shrq	$63, %rax
	sarq	$2, %rdx
	addq	%rdx, %rax
	movl	4(%rbx), %edx
	sarq	%rax
	leaq	(%rbx,%rax,4), %r8
	movl	(%r8), %eax
	cmpl	%eax, %edx
	jnb	.L1048
	cmpl	%edi, %eax
	jb	.L1053
	cmpl	%edi, %edx
	jb	.L1071
.L1072:
	movl	%edx, (%rbx)
	movl	%ecx, 4(%rbx)
	movl	-4(%rsi), %ecx
.L1050:
	movq	%r14, %r12
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L1054:
	movl	(%r12), %r8d
	movq	%r12, %r13
	cmpl	%r8d, %edx
	ja	.L1055
	leaq	-4(%rax), %rdi
	cmpl	%ecx, %edx
	jnb	.L1056
	subq	$8, %rax
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	%rax, %rdi
	movl	(%rax), %ecx
	subq	$4, %rax
	cmpl	%ecx, %edx
	jb	.L1057
.L1056:
	cmpq	%r12, %rdi
	jbe	.L1073
	movl	%ecx, (%r12)
	movl	-4(%rdi), %ecx
	movq	%rdi, %rax
	movl	%r8d, (%rdi)
	movl	(%rbx), %edx
.L1055:
	addq	$4, %r12
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1048:
	cmpl	%edi, %edx
	jb	.L1072
	cmpl	%edi, %eax
	jnb	.L1053
.L1071:
	movl	%edi, (%rbx)
	movl	%ecx, -4(%rsi)
	movl	(%rbx), %edx
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$64, %rax
	jle	.L1039
	testq	%r15, %r15
	je	.L1041
	movq	%r12, %rsi
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1053:
	movl	%eax, (%rbx)
	movl	%ecx, (%r8)
	movl	(%rbx), %edx
	movl	-4(%rsi), %ecx
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1041:
	sarq	$2, %rax
	leaq	-2(%rax), %r12
	movq	%rax, %r14
	sarq	%r12
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1074:
	subq	$1, %r12
.L1045:
	movl	(%rbx,%r12,4), %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEEljNS0_5__ops15_Iter_less_iterEEvT_T0_SA_T1_T2_
	testq	%r12, %r12
	jne	.L1074
	subq	$4, %r13
	.p2align 4,,10
	.p2align 3
.L1046:
	movl	(%rbx), %eax
	movq	%r13, %r12
	movl	0(%r13), %ecx
	xorl	%esi, %esi
	subq	%rbx, %r12
	movq	%rbx, %rdi
	subq	$4, %r13
	movl	%eax, 4(%r13)
	movq	%r12, %rdx
	sarq	$2, %rdx
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEEljNS0_5__ops15_Iter_less_iterEEvT_T0_SA_T1_T2_
	cmpq	$4, %r12
	jg	.L1046
.L1039:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1068:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE24506:
	.size	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_, .-_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_
	.section	.rodata._ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"Array.prototype.concat"
.LC17:
	.string	"unreachable code"
.LC18:
	.string	"vector::reserve"
	.section	.text.unlikely._ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
.LCOLDB19:
	.section	.text._ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
.LHOTB19:
	.p2align 4
	.type	_ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20934:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -224(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -288(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -296(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1076
.L1079:
	leaq	.LC16(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1546
.L1078:
	movq	(%r12), %rdx
	movq	-216(%rbp), %rax
	movq	%rdx, (%rax)
	movq	(%r12), %rbx
	testb	$1, %bl
	jne	.L1550
.L1087:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object23ArraySpeciesConstructorEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, -336(%rbp)
	testq	%rax, %rax
	je	.L1546
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	103(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1090
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1091:
	movq	-336(%rbp), %rax
	cmpq	%rsi, (%rax)
	je	.L1551
.L1093:
	movq	-224(%rbp), %rax
	movq	%rax, -368(%rbp)
	leal	-4(%rax), %ebx
	movq	12464(%r15), %rax
	movl	%ebx, -240(%rbp)
	movq	103(%rax), %rax
	movq	%rax, -352(%rbp)
	movq	-336(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -344(%rbp)
	testl	%ebx, %ebx
	jle	.L1097
	movl	$0, -248(%rbp)
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	$0, -256(%rbp)
	movl	%r14d, -232(%rbp)
	movq	%r15, %r14
	movl	%r13d, %r15d
	.p2align 4,,10
	.p2align 3
.L1135:
	movq	41088(%r14), %rax
	movl	-240(%rbp), %ebx
	addl	$1024, -232(%rbp)
	movl	-232(%rbp), %ecx
	movq	%rax, -264(%rbp)
	movq	41096(%r14), %rax
	cmpl	%ecx, %ebx
	movq	%rax, -272(%rbp)
	movl	41104(%r14), %eax
	cmovle	%ebx, %ecx
	leal	1(%rax), %esi
	movl	%ecx, -280(%rbp)
	movl	%esi, 41104(%r14)
	cmpl	%r15d, %ecx
	jle	.L1098
	leal	0(,%r15,8), %r13d
	leaq	56(%r14), %rax
	movq	%rax, -304(%rbp)
	movslq	%r13d, %r13
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1553:
	movl	$1, %ebx
	movl	$1, %ecx
.L1100:
	movl	-256(%rbp), %edi
	movl	$-1, %eax
	movl	%edi, %esi
	movl	%edi, %edx
	movl	-248(%rbp), %edi
	notl	%esi
	addl	%ecx, %edx
	cmpl	%ecx, %esi
	movl	%edi, %ecx
	cmovb	%eax, %edx
	notl	%ecx
	movl	%edx, -256(%rbp)
	movl	%edi, %edx
	addl	%ebx, %edx
	cmpl	%ebx, %ecx
	cmovnb	%edx, %eax
	addl	$1, %r15d
	addq	$8, %r13
	movl	%eax, -248(%rbp)
	cmpl	-280(%rbp), %r15d
	je	.L1552
.L1131:
	movq	-216(%rbp), %rbx
	subq	%r13, %rbx
	movq	(%rbx), %rax
	testb	$1, %al
	je	.L1553
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	je	.L1101
	movq	-1(%rax), %rax
	movzbl	%r12b, %edi
	cmpw	$65, 11(%rax)
	sete	%sil
	sete	%bl
	movzbl	%sil, %esi
	leal	2(%rbx,%rbx), %ebx
	leal	2(%rsi,%rsi), %esi
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	movl	$1, %ecx
	testb	%al, %al
	cmovne	%ebx, %r12d
	movl	$1, %ebx
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	-1(%rbx), %rax
	cmpw	$1061, 11(%rax)
	jne	.L1087
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	463(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1082
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1083:
	movq	-1(%rbx), %rax
	cmpq	%rsi, 23(%rax)
	jne	.L1087
	movq	4520(%r15), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1087
	sarq	$32, %rax
	cmpq	$1, %rax
	jne	.L1087
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactEv@PLT
	testb	%al, %al
	jne	.L1085
.L1088:
	movq	12480(%r15), %rax
	cmpq	%rax, 96(%r15)
	je	.L1087
.L1546:
	movq	312(%r15), %r12
.L1080:
	movq	-288(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-296(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L1363
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1363:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1554
	addq	$328, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1076:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1079
	movq	%rsi, %r12
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1085:
	leaq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_116Fast_ArrayConcatEPNS0_7IsolateEPNS0_16BuiltinArgumentsE.part.0
	testq	%rax, %rax
	je	.L1088
.L1547:
	movq	(%rax), %r12
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L1555
.L1084:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1090:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L1556
.L1092:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	-264(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-272(%rbp), %rax
	cmpq	%rax, 41096(%r14)
	je	.L1132
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	cmpl	%r15d, -240(%rbp)
	jg	.L1135
.L1538:
	movq	-352(%rbp), %rbx
	movq	%r14, %r15
	cmpq	%rbx, -344(%rbp)
	je	.L1557
.L1136:
	movq	41112(%r15), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rsi
	testq	%rdi, %rdi
	je	.L1558
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1160:
	movq	-336(%rbp), %rdx
	leaq	-144(%rbp), %r8
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	movl	$1, %ecx
	movq	%rax, -144(%rbp)
	movq	%rdx, %rsi
	call	_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1546
.L1159:
	movq	%r15, -208(%rbp)
	movq	41152(%r15), %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movl	$0, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	(%rbx), %rsi
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	movq	-1(%rsi), %rdx
	subl	$123, %eax
	movzwl	11(%rdx), %ecx
	movl	$8, %edx
	cmpw	$14, %ax
	setbe	%al
	subl	$123, %ecx
	movzbl	%al, %eax
	sall	$2, %eax
	orl	%r12d, %eax
	cmpw	$14, %cx
	ja	.L1559
.L1163:
	movl	-240(%rbp), %esi
	orl	%edx, %eax
	movl	%eax, -188(%rbp)
	testl	%esi, %esi
	jle	.L1164
	movq	-368(%rbp), %rax
	movq	%r15, %r14
	movq	$0, -232(%rbp)
	subl	$5, %eax
	leaq	8(,%rax,8), %rax
	movq	%rax, -304(%rbp)
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1543:
	movl	%eax, 41104(%r14)
.L1176:
	xorl	%esi, %esi
	leaq	-208(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118ArrayConcatVisitor5visitEjNS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1529
	movl	-192(%rbp), %eax
	xorl	%edx, %edx
	cmpl	$-1, %eax
	setne	%dl
	addl	%edx, %eax
	movl	%eax, -192(%rbp)
	testb	$1, -188(%rbp)
	je	.L1324
	movq	-200(%rbp), %rbx
	movq	(%rbx), %rdx
	movq	%rbx, -272(%rbp)
	movslq	11(%rdx), %rsi
	cmpl	%eax, 11(%rdx)
	jb	.L1560
	.p2align 4,,10
	.p2align 3
.L1324:
	addq	$8, -232(%rbp)
	movq	-232(%rbp), %rax
	cmpq	%rax, -304(%rbp)
	je	.L1561
.L1343:
	movl	41104(%r14), %eax
	movq	-216(%rbp), %r15
	subq	-232(%rbp), %r15
	movq	41088(%r14), %r12
	leal	1(%rax), %edx
	movq	41096(%r14), %rbx
	movl	%edx, 41104(%r14)
	movq	(%r15), %rsi
	testb	$1, %sil
	je	.L1543
	movq	-1(%rsi), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L1543
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactENS0_10JSReceiverE@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L1562
.L1169:
	movq	(%r15), %rax
	testb	$1, %al
	je	.L1175
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	je	.L1400
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	je	.L1563
.L1175:
	movb	$0, -306(%rbp)
	movl	$1, %r13d
.L1172:
	subl	$1, 41104(%r14)
	movq	%r12, 41088(%r14)
	cmpq	41096(%r14), %rbx
	je	.L1374
	movq	%rbx, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1374:
	testb	%r13b, %r13b
	je	.L1564
	cmpb	$0, -306(%rbp)
	je	.L1176
	movq	(%r15), %r12
	movq	-1(%r12), %rax
	cmpw	$1061, 11(%rax)
	jne	.L1179
	movq	23(%r12), %rax
	testb	$1, %al
	jne	.L1180
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L1181:
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -240(%rbp)
	movl	%eax, %ebx
	.p2align 4,,10
	.p2align 3
.L1187:
	movq	-1(%r12), %rax
	leaq	-1(%r12), %r13
	cmpw	$1024, 11(%rax)
	je	.L1209
	movq	-1(%r12), %rax
	cmpw	$1041, 11(%rax)
	ja	.L1565
.L1209:
	movl	-240(%rbp), %edx
	leaq	-208(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119IterateElementsSlowEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEjPNS1_18ArrayConcatVisitorE
.L1207:
	testb	%al, %al
	jne	.L1324
	.p2align 4,,10
	.p2align 3
.L1529:
	movq	%r14, %r15
.L1377:
	movq	312(%r15), %r12
.L1344:
	movq	-200(%rbp), %rdi
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1101:
	movq	23(%rax), %rdi
	movq	%rdi, %rdx
	notq	%rdx
	movl	%edx, %r9d
	andl	$1, %r9d
	je	.L1103
	movq	%rdi, %rdx
	pxor	%xmm0, %xmm0
	leaq	-1(%rax), %rsi
	sarq	$32, %rdx
	cvtsi2sdl	%edx, %xmm0
	cvttsd2siq	%xmm0, %rdx
	movl	%edx, %ecx
	testl	%edx, %edx
	jne	.L1566
.L1105:
	testb	%r9b, %r9b
	je	.L1109
	sarq	$32, %rdi
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
.L1110:
	movq	(%rsi), %rsi
	cvttsd2siq	%xmm0, %rdx
	movzbl	14(%rsi), %edi
	movl	%edi, %esi
	movl	%edx, %ebx
	shrl	$3, %esi
	cmpl	$231, %edi
	ja	.L1388
	leaq	.L1113(%rip), %rdi
	movslq	(%rdi,%rsi,4), %rsi
	addq	%rdi, %rsi
	notrack jmp	*%rsi
	.section	.rodata._ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L1113:
	.long	.L1117-.L1113
	.long	.L1117-.L1113
	.long	.L1117-.L1113
	.long	.L1117-.L1113
	.long	.L1118-.L1113
	.long	.L1118-.L1113
	.long	.L1117-.L1113
	.long	.L1117-.L1113
	.long	.L1117-.L1113
	.long	.L1117-.L1113
	.long	.L1117-.L1113
	.long	.L1117-.L1113
	.long	.L1116-.L1113
	.long	.L1115-.L1113
	.long	.L1115-.L1113
	.long	.L1115-.L1113
	.long	.L1115-.L1113
	.long	.L1100-.L1113
	.long	.L1100-.L1113
	.long	.L1100-.L1113
	.long	.L1100-.L1113
	.long	.L1100-.L1113
	.long	.L1100-.L1113
	.long	.L1100-.L1113
	.long	.L1100-.L1113
	.long	.L1100-.L1113
	.long	.L1100-.L1113
	.long	.L1100-.L1113
	.long	.L1112-.L1113
	.section	.text._ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	15(%rax), %rsi
	testl	%edx, %edx
	jle	.L1122
	subl	$1, %edx
	leaq	15(%rsi), %rax
	xorl	%ebx, %ebx
	leaq	23(%rsi,%rdx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L1121:
	movq	(%rax), %rdx
	cmpq	96(%r14), %rdx
	setne	%dl
	addq	$8, %rax
	movzbl	%dl, %edx
	addl	%edx, %ebx
	cmpq	%rax, %rsi
	jne	.L1121
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1118:
	movq	15(%rax), %rsi
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	jbe	.L1122
	testl	%edx, %edx
	jle	.L1122
	subl	$1, %edx
	leaq	15(%rsi), %rax
	xorl	%ebx, %ebx
	leaq	23(%rsi,%rdx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L1124:
	movabsq	$-2251799814209537, %rdx
	cmpq	%rdx, (%rax)
	setne	%dl
	addq	$8, %rax
	movzbl	%dl, %edx
	addl	%edx, %ebx
	cmpq	%rax, %rsi
	jne	.L1124
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	15(%rax), %rax
	movslq	35(%rax), %rsi
	testq	%rsi, %rsi
	jle	.L1389
	subl	$1, %esi
	movl	%ecx, -320(%rbp)
	leaq	47(%rax), %rdx
	xorl	%ebx, %ebx
	leaq	(%rsi,%rsi,2), %rsi
	movl	%r15d, -328(%rbp)
	movq	-304(%rbp), %r15
	leaq	71(%rax,%rsi,8), %rax
	movb	%r12b, -307(%rbp)
	movq	%r13, -360(%rbp)
	movq	%rax, %r12
	movq	%rdx, %r13
	.p2align 4,,10
	.p2align 3
.L1127:
	movq	0(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE5IsKeyENS0_13ReadOnlyRootsENS0_6ObjectE@PLT
	cmpb	$1, %al
	sbbl	$-1, %ebx
	addq	$24, %r13
	cmpq	%r13, %r12
	jne	.L1127
	movl	-320(%rbp), %ecx
	movl	-328(%rbp), %r15d
	movzbl	-307(%rbp), %r12d
	movq	-360(%rbp), %r13
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1098:
	movl	%eax, 41104(%r14)
.L1132:
	cmpl	%r15d, -240(%rbp)
	jg	.L1135
	jmp	.L1538
	.p2align 4,,10
	.p2align 3
.L1109:
	movsd	7(%rdi), %xmm0
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1103:
	movsd	7(%rdi), %xmm0
	leaq	-1(%rax), %rsi
	cvttsd2siq	%xmm0, %rdx
	movl	%edx, %ecx
	testl	%edx, %edx
	je	.L1105
.L1566:
	movq	-1(%rax), %rax
	movzbl	14(%rax), %esi
	shrl	$3, %esi
	movl	%esi, %r8d
	cmpl	$1, %esi
	je	.L1384
	cmpl	$5, %esi
	je	.L1385
	cmpb	$3, %sil
	je	.L1386
	leal	-6(%rsi), %eax
	movl	$2, %edx
	cmpb	$6, %al
	movl	$2, %eax
	cmovb	%edx, %esi
	cmovb	%eax, %r8d
.L1107:
	movzbl	%r12b, %edi
	movb	%r8b, -328(%rbp)
	movl	%ecx, -320(%rbp)
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	movl	-320(%rbp), %ecx
	movzbl	-328(%rbp), %r8d
	testb	%al, %al
	movq	(%rbx), %rax
	movq	23(%rax), %rdi
	movq	%rdi, %rdx
	notq	%rdx
	movl	%edx, %r9d
	jne	.L1108
	andl	$1, %r9d
	leaq	-1(%rax), %rsi
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1108:
	andl	$1, %r9d
	leaq	-1(%rax), %rsi
	movl	%r8d, %r12d
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1122:
	xorl	%ebx, %ebx
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1384:
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1551:
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactEv@PLT
	testb	%al, %al
	jne	.L1094
.L1096:
	movq	12480(%r15), %rax
	cmpq	%rax, 96(%r15)
	jne	.L1546
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1094:
	leaq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_116Fast_ArrayConcatEPNS0_7IsolateEPNS0_16BuiltinArgumentsE.part.0
	testq	%rax, %rax
	jne	.L1547
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	%r15, %rdi
	movq	%rsi, -232(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	jmp	.L1084
.L1115:
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1112:
	xorl	%ebx, %ebx
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1558:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L1567
.L1161:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1565:
	movq	0(%r13), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	%r12, %rsi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	testb	%al, %al
	jne	.L1209
	andl	$1, %r12d
	jne	.L1185
.L1188:
	movq	0(%r13), %rax
	movq	23(%rax), %r12
	cmpq	104(%r14), %r12
	jne	.L1187
	.p2align 4,,10
	.p2align 3
.L1186:
	movl	-188(%rbp), %eax
	testb	$8, %al
	je	.L1209
	movq	(%r15), %rdi
	movq	-1(%rdi), %rdx
	movzbl	14(%rdx), %edx
	movl	%edx, %ecx
	shrl	$3, %ecx
	cmpl	$223, %edx
	ja	.L1208
	leaq	.L1210(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.align 4
	.align 4
.L1210:
	.long	.L1213-.L1210
	.long	.L1213-.L1210
	.long	.L1213-.L1210
	.long	.L1213-.L1210
	.long	.L1214-.L1210
	.long	.L1214-.L1210
	.long	.L1213-.L1210
	.long	.L1213-.L1210
	.long	.L1213-.L1210
	.long	.L1213-.L1210
	.long	.L1213-.L1210
	.long	.L1213-.L1210
	.long	.L1212-.L1210
	.long	.L1211-.L1210
	.long	.L1211-.L1210
	.long	.L1115-.L1210
	.long	.L1115-.L1210
	.long	.L1209-.L1210
	.long	.L1209-.L1210
	.long	.L1209-.L1210
	.long	.L1209-.L1210
	.long	.L1209-.L1210
	.long	.L1209-.L1210
	.long	.L1209-.L1210
	.long	.L1209-.L1210
	.long	.L1209-.L1210
	.long	.L1209-.L1210
	.long	.L1209-.L1210
	.section	.text._ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1213:
	movq	15(%rdi), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1215
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L1216:
	movq	-240(%rbp), %rax
	xorl	%r13d, %r13d
	movl	$0, -264(%rbp)
	movl	%eax, -272(%rbp)
	testl	%eax, %eax
	jle	.L1540
	movq	%r14, %rax
	movl	%r13d, %r14d
	movq	%r12, %r13
	movq	%rax, %r12
.L1218:
	movq	41088(%r12), %rbx
	addl	$1024, -264(%rbp)
	addl	$1, 41104(%r12)
	movl	-264(%rbp), %eax
	movq	%rbx, -280(%rbp)
	movq	41096(%r12), %rbx
	movq	%rbx, -320(%rbp)
	movl	-272(%rbp), %ebx
	cmpl	%eax, %ebx
	cmovle	%ebx, %eax
	movl	%eax, -248(%rbp)
	cmpl	%r14d, %eax
	jle	.L1402
	leal	16(,%r14,8), %ebx
	movq	%r12, %rax
	movl	%r14d, %r12d
	movslq	%ebx, %rbx
	movq	%rax, %r14
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1569:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L1221:
	cmpq	%rsi, 96(%r14)
	je	.L1223
.L1229:
	leaq	-208(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal12_GLOBAL__N_118ArrayConcatVisitor5visitEjNS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1533
.L1224:
	addl	$1, %r12d
	addq	$8, %rbx
	cmpl	%r12d, -248(%rbp)
	je	.L1568
.L1231:
	movq	0(%r13), %rax
	movq	-1(%rbx,%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L1569
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L1570
.L1222:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L1221
.L1211:
	movl	-240(%rbp), %edx
	testl	%edx, %edx
	je	.L1236
	movl	$0, -248(%rbp)
	xorl	%r13d, %r13d
.L1303:
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	addl	$1024, -248(%rbp)
	movq	%rax, -264(%rbp)
	movq	41096(%r14), %rax
	movq	%rax, -256(%rbp)
.L1302:
	cmpl	%r13d, -248(%rbp)
	jbe	.L1297
	leaq	-144(%rbp), %r12
	movq	%r14, -120(%rbp)
	movabsq	$824633720832, %rax
	movq	%r12, %rdi
	movq	%rax, -132(%rbp)
	movl	$3, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r15, -80(%rbp)
	movl	%r13d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L1298
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rdx
.L1299:
	leaq	-208(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal12_GLOBAL__N_118ArrayConcatVisitor5visitEjNS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1300
	addl	$1, %r13d
	cmpl	%r13d, %ebx
	ja	.L1302
	movq	-264(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-256(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L1540
.L1369:
	movq	-256(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, 41096(%r14)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1370:
	cmpl	%r13d, %ebx
	ja	.L1303
.L1540:
	movl	-188(%rbp), %eax
.L1208:
	movl	-192(%rbp), %ecx
	movl	$-1, %edx
	movl	%ecx, %esi
	notl	%esi
	cmpl	%esi, -240(%rbp)
	ja	.L1304
	jmp	.L1366
.L1214:
	movl	-240(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L1571
.L1236:
	movl	-192(%rbp), %ecx
.L1366:
	movl	-240(%rbp), %edx
	addl	%ecx, %edx
.L1304:
	movl	%edx, -192(%rbp)
	testb	$1, %al
	je	.L1324
	movq	-200(%rbp), %r12
	movq	(%r12), %rax
	movslq	11(%rax), %rsi
	cmpl	%edx, 11(%rax)
	jnb	.L1324
	movq	-208(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	-208(%rbp), %rbx
	movq	%rax, %r15
	movq	(%r12), %rax
	movslq	11(%rax), %rax
	movl	%eax, -264(%rbp)
	testl	%eax, %eax
	je	.L1327
	movl	$0, -256(%rbp)
	xorl	%r13d, %r13d
	movq	%r14, -320(%rbp)
	movq	%rbx, %r14
	.p2align 4,,10
	.p2align 3
.L1323:
	movq	41088(%r14), %rax
	addl	$1024, -256(%rbp)
	movl	-256(%rbp), %ebx
	movq	%rax, -240(%rbp)
	movq	41096(%r14), %rax
	movq	%rax, -248(%rbp)
	movl	41104(%r14), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 41104(%r14)
	movl	-264(%rbp), %ecx
	cmpl	%ebx, %ecx
	cmovbe	%ecx, %ebx
	cmpl	%ebx, %r13d
	jnb	.L1309
	movq	%r14, %rax
	movq	%r15, %r14
	movq	%rax, %r15
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L1311:
	movq	-208(%rbp), %rdi
	cmpq	%rsi, 96(%rdi)
	je	.L1313
	xorl	%r8d, %r8d
	movl	$192, %r9d
	movl	%r13d, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal16NumberDictionary3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEENS4_INS0_8JSObjectEEENS0_15PropertyDetailsE@PLT
	cmpq	%rax, %r14
	je	.L1313
	movq	(%rax), %rsi
	testq	%rax, %rax
	je	.L1315
	testq	%r14, %r14
	je	.L1315
	cmpq	%rsi, (%r14)
	je	.L1313
.L1315:
	movq	-240(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-248(%rbp), %rax
	cmpq	%rax, 41096(%r15)
	je	.L1316
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	movq	%rsi, -240(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-240(%rbp), %rsi
.L1316:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1317
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L1318:
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	%rax, -240(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -248(%rbp)
.L1313:
	addl	$1, %r13d
	cmpl	%ebx, %r13d
	je	.L1572
.L1320:
	movq	-208(%rbp), %rdi
	movq	(%r12), %rcx
	leal	16(,%r13,8), %eax
	cltq
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	jne	.L1573
	movq	41088(%rdi), %rcx
	cmpq	41096(%rdi), %rcx
	je	.L1574
.L1312:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%rcx)
	jmp	.L1311
.L1212:
	movq	15(%rdi), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1255
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1256:
	movq	$0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	(%rax), %rax
	movslq	35(%rax), %rax
	movl	%eax, %ebx
	shrl	$31, %ebx
	addl	%eax, %ebx
	movabsq	$2305843009213693951, %rax
	sarl	%ebx
	movslq	%ebx, %rbx
	cmpq	%rax, %rbx
	ja	.L1575
	testq	%rbx, %rbx
	jne	.L1576
.L1259:
	movl	-240(%rbp), %edx
	leaq	-176(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_121CollectElementIndicesEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEjPSt6vectorIjSaIjEE
	movq	-168(%rbp), %r8
	movq	-176(%rbp), %r12
	cmpq	%r12, %r8
	je	.L1295
	movq	%r8, %r13
	movl	$63, %edx
	movq	%r8, %rsi
	movq	%r12, %rdi
	subq	%r12, %r13
	movq	%r8, -248(%rbp)
	leaq	4(%r12), %rbx
	movq	%r13, %rax
	sarq	$2, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPjSt6vectorIjSaIjEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_
	cmpq	$64, %r13
	movq	-248(%rbp), %r8
	jle	.L1264
	leaq	64(%r12), %r13
	movq	%rbx, %rax
	movq	%r14, -256(%rbp)
	movq	%r12, %rbx
	movq	%r13, %r14
	movq	%rax, %r12
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1578:
	cmpq	%r12, %rbx
	je	.L1266
	movq	%r12, %rdx
	movl	$4, %eax
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	(%rbx,%rax), %rdi
	call	memmove@PLT
.L1266:
	movl	%r13d, (%rbx)
.L1267:
	addq	$4, %r12
	cmpq	%r12, %r14
	je	.L1577
.L1270:
	movl	(%r12), %r13d
	cmpl	(%rbx), %r13d
	jb	.L1578
	movl	-4(%r12), %edx
	leaq	-4(%r12), %rax
	cmpl	%edx, %r13d
	jnb	.L1404
	.p2align 4,,10
	.p2align 3
.L1269:
	movl	%edx, 4(%rax)
	movq	%rax, %rsi
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %r13d
	jb	.L1269
.L1268:
	movl	%r13d, (%rsi)
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1179:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object22GetLengthFromArrayLikeEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L1529
	movl	-192(%rbp), %edx
	movq	(%rax), %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	testb	$1, %al
	jne	.L1195
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	addsd	%xmm1, %xmm0
	comisd	.LC2(%rip), %xmm0
	ja	.L1539
	movl	%eax, %edx
	testq	%rax, %rax
	js	.L1415
.L1201:
	leaq	-208(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119IterateElementsSlowEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEjPNS1_18ArrayConcatVisitorE
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1185:
	movq	0(%r13), %rax
	cmpw	$1024, 11(%rax)
	jne	.L1188
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L1180:
	movsd	7(%rax), %xmm0
	jmp	.L1181
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	7(%rax), %rdx
	movq	%rdx, %xmm2
	addsd	%xmm2, %xmm0
	comisd	.LC2(%rip), %xmm0
	jbe	.L1520
.L1539:
	xorl	%edx, %edx
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$189, %esi
	movq	%r14, %r15
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1579:
	subl	$1, 41104(%r14)
	movq	%r13, 41088(%r14)
	cmpq	41096(%r14), %r12
	je	.L1340
	movq	%r12, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	cmpl	%ebx, -256(%rbp)
	ja	.L1342
.L1544:
	movq	-320(%rbp), %r14
.L1327:
	movq	-200(%rbp), %rdi
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	-208(%rbp), %rax
	movq	(%r15), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	andl	$-2, -188(%rbp)
	movq	%rax, -200(%rbp)
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	-208(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	-208(%rbp), %r13
	movq	%rax, %r15
	movq	-272(%rbp), %rax
	movq	(%rax), %rax
	movslq	11(%rax), %rax
	movl	%eax, -256(%rbp)
	testl	%eax, %eax
	je	.L1327
	movl	$0, -240(%rbp)
	xorl	%ebx, %ebx
	movq	%r14, -320(%rbp)
	movq	%r13, %r14
	.p2align 4,,10
	.p2align 3
.L1342:
	addl	$1024, -240(%rbp)
	movl	-256(%rbp), %edi
	movl	-240(%rbp), %ecx
	movl	41104(%r14), %eax
	movq	41088(%r14), %r13
	movq	41096(%r14), %r12
	cmpl	%ecx, %edi
	leal	1(%rax), %edx
	cmovbe	%edi, %ecx
	movl	%edx, 41104(%r14)
	movl	%ecx, -264(%rbp)
	cmpl	%ebx, %ecx
	ja	.L1339
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1580:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L1330:
	movq	-208(%rbp), %rdi
	cmpq	%rsi, 96(%rdi)
	je	.L1332
	xorl	%r8d, %r8d
	movl	$192, %r9d
	movl	%ebx, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal16NumberDictionary3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEENS4_INS0_8JSObjectEEENS0_15PropertyDetailsE@PLT
	cmpq	%rax, %r15
	je	.L1332
	movq	(%rax), %rsi
	testq	%r15, %r15
	je	.L1334
	testq	%rax, %rax
	je	.L1334
	cmpq	%rsi, (%r15)
	je	.L1332
.L1334:
	subl	$1, 41104(%r14)
	movq	%r13, 41088(%r14)
	cmpq	41096(%r14), %r12
	je	.L1335
	movq	%r12, 41096(%r14)
	movq	%r14, %rdi
	movq	%rsi, -248(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-248(%rbp), %rsi
.L1335:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1336
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1337:
	addl	$1, 41104(%r14)
	movq	41088(%r14), %r13
	movq	41096(%r14), %r12
.L1332:
	addl	$1, %ebx
	cmpl	-264(%rbp), %ebx
	je	.L1579
.L1339:
	movq	-272(%rbp), %rcx
	movq	-208(%rbp), %rdx
	leal	16(,%rbx,8), %eax
	cltq
	movq	(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L1580
	movq	41088(%rdx), %rcx
	cmpq	41096(%rdx), %rcx
	je	.L1581
.L1331:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1336:
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L1582
.L1338:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1581:
	movq	%rdx, %rdi
	movq	%rsi, -280(%rbp)
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	-248(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1328:
	movl	%eax, 41104(%r14)
.L1340:
	cmpl	%ebx, -256(%rbp)
	ja	.L1342
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1582:
	movq	%r14, %rdi
	movq	%rsi, -248(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-248(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	jne	.L1415
	movsd	.LC5(%rip), %xmm3
	addsd	%xmm2, %xmm3
	movq	%xmm3, %rdx
	movq	%xmm3, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1415
	movl	%eax, %eax
	pxor	%xmm1, %xmm1
	movd	%xmm3, %edx
	cvtsi2sdq	%rax, %xmm1
	ucomisd	%xmm1, %xmm2
	jp	.L1415
	je	.L1201
	.p2align 4,,10
	.p2align 3
.L1415:
	xorl	%edx, %edx
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1223:
	movq	(%r15), %rax
	leaq	-144(%rbp), %rdi
	movabsq	$824633720832, %rcx
	movq	%rdi, -256(%rbp)
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	%rcx, -132(%rbp)
	movl	$3, -144(%rbp)
	subq	$37592, %rax
	movq	$0, -112(%rbp)
	movq	%rax, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r15, -80(%rbp)
	movl	%r12d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	-256(%rbp), %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	movzbl	%ah, %edx
	testb	%al, %al
	je	.L1533
	testb	%dl, %dl
	movq	-256(%rbp), %rdi
	je	.L1224
	movabsq	$824633720832, %rax
	movq	%r14, -120(%rbp)
	movl	$3, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r15, -80(%rbp)
	movl	%r12d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movq	-256(%rbp), %rdi
	jne	.L1228
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rdx
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1562:
	xorl	%ecx, %ecx
	leaq	3944(%r14), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb@PLT
	testq	%rax, %rax
	je	.L1172
	movq	(%rax), %rax
	cmpq	88(%r14), %rax
	je	.L1169
	leaq	-144(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, -144(%rbp)
	movl	$1, %r13d
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movb	%al, -306(%rbp)
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1557:
	movl	-248(%rbp), %eax
	addl	%eax, %eax
	cmpl	-256(%rbp), %eax
	jnb	.L1583
.L1137:
	movl	-248(%rbp), %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	%rax, %rbx
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1561:
	movl	-188(%rbp), %eax
	movq	%r14, %r15
.L1164:
	testb	$2, %al
	jne	.L1584
	movq	-208(%rbp), %rdi
	movq	-352(%rbp), %rbx
	cmpq	%rbx, -344(%rbp)
	je	.L1585
	movl	-192(%rbp), %eax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	-200(%rbp), %rbx
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	-208(%rbp), %rdi
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	%rax, %rcx
	movl	$1, %r9d
	leaq	2768(%rdi), %rdx
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L1377
	testq	%rbx, %rbx
	je	.L1377
.L1362:
	movq	(%rbx), %r12
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1564:
	movq	312(%r14), %r12
	movq	%r14, %r15
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1559:
	movq	-1(%rsi), %rdx
	cmpw	$1041, 11(%rdx)
	seta	%dl
	movzbl	%dl, %edx
	sall	$3, %edx
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1385:
	movl	$4, %esi
	movl	$4, %r8d
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L1586
.L1319:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1400:
	movb	$1, -306(%rbp)
	movl	$1, %r13d
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1298:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L1299
.L1300:
	movq	-264(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%r14, %r15
	movq	%rax, 41088(%r14)
	movq	-256(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L1377
.L1371:
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1572:
	movq	%r15, %rax
	movq	%r14, %r15
	movq	%rax, %r14
	movq	-240(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-248(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L1321
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	cmpl	%r13d, -264(%rbp)
	ja	.L1323
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1386:
	movl	$2, %esi
	movl	$2, %r8d
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1570:
	movq	%r14, %rdi
	movq	%rsi, -256(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1574:
	movq	%rsi, -280(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	%rax, %rcx
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1563:
	movq	%r15, %rdi
	call	_ZN2v88internal7JSProxy7IsArrayENS0_6HandleIS1_EE@PLT
	movzbl	%ah, %ecx
	movl	%eax, %r13d
	movw	%cx, -306(%rbp)
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1389:
	xorl	%ebx, %ebx
	jmp	.L1100
.L1309:
	movl	%eax, 41104(%r14)
.L1321:
	cmpl	%r13d, -264(%rbp)
	ja	.L1323
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1556:
	movq	%r15, %rdi
	movq	%rsi, -232(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L1097:
	movq	-352(%rbp), %rbx
	cmpq	%rbx, -344(%rbp)
	jne	.L1136
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactEv@PLT
	testb	%al, %al
	jne	.L1414
	movl	$0, -248(%rbp)
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1228:
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L1229
.L1533:
	movq	-280(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%r14, %r15
	movq	%rax, 41088(%r14)
	movq	-320(%rbp), %rax
	cmpq	41096(%r14), %rax
	jne	.L1371
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1584:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$189, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r12
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1583:
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactEv@PLT
	testb	%al, %al
	je	.L1137
	movl	-256(%rbp), %eax
	movl	%eax, -232(%rbp)
	cmpb	$4, %r12b
	jne	.L1375
	xorl	%edx, %edx
	movl	%eax, %esi
	movq	%r15, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r10
	testl	%ebx, %ebx
	je	.L1396
	movl	-240(%rbp), %edi
	testl	%edi, %edi
	jle	.L1396
	movq	-368(%rbp), %rax
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	leaq	.L1149(%rip), %r12
	movabsq	$-2251799814209537, %r13
	movabsq	$9221120237041090560, %r14
	subl	$5, %eax
	leaq	8(,%rax,8), %r11
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1587:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	leal	16(,%rcx,8), %edx
	movq	(%r10), %rdi
	cvtsi2sdl	%eax, %xmm0
	movslq	%edx, %rdx
	addl	$1, %ecx
	movq	%xmm0, -1(%rdx,%rdi)
.L1141:
	addq	$8, %rsi
	cmpq	%rsi, %r11
	je	.L1139
.L1157:
	movq	-216(%rbp), %rax
	subq	%rsi, %rax
	movq	(%rax), %rax
	testb	$1, %al
	je	.L1587
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1142
	movq	7(%rax), %rax
	leal	16(,%rcx,8), %edx
	movq	(%r10), %rdi
	movslq	%edx, %rdx
	movq	%rax, %xmm0
	ucomisd	%xmm0, %xmm0
	cmovp	%r14, %rax
	addl	$1, %ecx
	movq	%rax, -1(%rdi,%rdx)
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	23(%rax), %rdx
	testb	$1, %dl
	jne	.L1147
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
.L1148:
	movq	-1(%rax), %rdx
	cvttsd2siq	%xmm0, %r8
	movzbl	14(%rdx), %edi
	movl	%edi, %edx
	shrl	$3, %edx
	cmpl	$231, %edi
	ja	.L1115
	movslq	(%r12,%rdx,4), %rdx
	addq	%r12, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.align 4
	.align 4
.L1149:
	.long	.L1151-.L1149
	.long	.L1151-.L1149
	.long	.L1141-.L1149
	.long	.L1141-.L1149
	.long	.L1150-.L1149
	.long	.L1150-.L1149
	.long	.L1141-.L1149
	.long	.L1141-.L1149
	.long	.L1141-.L1149
	.long	.L1141-.L1149
	.long	.L1141-.L1149
	.long	.L1141-.L1149
	.long	.L1141-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1115-.L1149
	.long	.L1141-.L1149
	.section	.text._ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE
.L1150:
	testl	%r8d, %r8d
	je	.L1141
	movq	15(%rax), %rdx
	movslq	%ecx, %rbx
	addl	%ecx, %r8d
	salq	$3, %rbx
	subq	%rdx, %rbx
	leaq	15(%rdx), %rax
	addq	$1, %rbx
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	(%rax), %rdx
	cmpq	%r13, %rdx
	je	.L1375
	movq	%rdx, %xmm0
	movq	(%r10), %rdi
	leaq	(%rbx,%rax), %r9
	ucomisd	%xmm0, %xmm0
	leaq	-1(%r9,%rdi), %rdi
	jp	.L1588
	addl	$1, %ecx
	movq	%rdx, (%rdi)
	addq	$8, %rax
	cmpl	%r8d, %ecx
	jne	.L1155
	jmp	.L1141
.L1151:
	movq	96(%r15), %r9
	movq	15(%rax), %rax
	testl	%r8d, %r8d
	je	.L1141
	movslq	%ecx, %rbx
	leaq	15(%rax), %rdx
	addl	%ecx, %r8d
	salq	$3, %rbx
	subq	%rax, %rbx
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1589:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	leaq	(%rbx,%rdx), %rdi
	addl	$1, %ecx
	cvtsi2sdl	%eax, %xmm0
	addq	(%r10), %rdi
	addq	$8, %rdx
	movq	%xmm0, (%rdi)
	cmpl	%r8d, %ecx
	je	.L1141
.L1156:
	movq	(%rdx), %rax
	cmpq	%rax, %r9
	jne	.L1589
.L1375:
	movl	-232(%rbp), %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	$1, %r12d
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1147:
	movsd	7(%rdx), %xmm0
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1396:
	xorl	%ecx, %ecx
.L1139:
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r10, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	jmp	.L1547
.L1585:
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movl	$3, %esi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	pxor	%xmm0, %xmm0
	movq	-208(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %rbx
	movl	-192(%rbp), %eax
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	movzbl	-188(%rbp), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$9, %esi
	addl	$3, %esi
	call	_ZN2v88internal8JSObject24GetElementsTransitionMapENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	(%rbx), %r14
	movq	(%r12), %r12
	movq	%rax, %r13
	movq	%r12, 23(%r14)
	leaq	23(%r14), %rsi
	testb	$1, %r12b
	je	.L1365
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -232(%rbp)
	testl	$262144, %eax
	je	.L1351
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rsi, -240(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-232(%rbp), %rcx
	movq	-240(%rbp), %rsi
	movq	8(%rcx), %rax
.L1351:
	testb	$24, %al
	je	.L1365
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1365
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L1365:
	movq	-200(%rbp), %rax
	movq	(%rbx), %r14
	movq	(%rax), %r12
	leaq	15(%r14), %rsi
	movq	%r12, 15(%r14)
	testb	$1, %r12b
	je	.L1364
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -232(%rbp)
	testl	$262144, %eax
	je	.L1354
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rsi, -240(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-232(%rbp), %rcx
	movq	-240(%rbp), %rsi
	movq	8(%rcx), %rax
.L1354:
	testb	$24, %al
	je	.L1364
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1364
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L1364:
	movq	(%rbx), %rdi
	movq	0(%r13), %rdx
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	je	.L1362
	testb	$1, %dl
	je	.L1362
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L1362
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1362
.L1215:
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L1590
.L1217:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L1216
.L1568:
	movq	%r14, %r12
.L1219:
	subl	$1, 41104(%r12)
	movq	-280(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-320(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L1233
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-248(%rbp), %ebx
	cmpl	%ebx, -272(%rbp)
	jle	.L1541
.L1235:
	movl	-248(%rbp), %r14d
	jmp	.L1218
.L1255:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1591
.L1257:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1256
.L1586:
	movq	%r15, %rdi
	movq	%rsi, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-240(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1319
.L1567:
	movq	%r15, %rdi
	movq	%rsi, -232(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	jmp	.L1161
.L1588:
	addl	$1, %ecx
	movq	%r14, (%rdi)
	addq	$8, %rax
	cmpl	%r8d, %ecx
	jne	.L1155
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1576:
	salq	$2, %rbx
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	-176(%rbp), %r13
	movq	-168(%rbp), %rdx
	movq	%rax, %r12
	subq	%r13, %rdx
	testq	%rdx, %rdx
	jg	.L1592
	testq	%r13, %r13
	jne	.L1261
.L1262:
	movq	%r12, %xmm0
	addq	%r12, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -160(%rbp)
	movaps	%xmm0, -176(%rbp)
	jmp	.L1259
.L1571:
	movq	15(%rdi), %r12
	movq	-1(%r12), %rdx
	movzwl	11(%rdx), %edx
	subl	$123, %edx
	cmpw	$14, %dx
	jbe	.L1208
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1237
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L1238:
	movq	-240(%rbp), %rax
	movl	%eax, -280(%rbp)
	testl	%eax, %eax
	jle	.L1540
	leaq	-144(%rbp), %rax
	xorl	%r12d, %r12d
	movl	$0, -272(%rbp)
	movq	%rax, -256(%rbp)
.L1253:
	movq	41088(%r14), %rbx
	addl	$1, 41104(%r14)
	addl	$1024, -272(%rbp)
	movl	-272(%rbp), %eax
	movq	%rbx, -320(%rbp)
	movq	41096(%r14), %rbx
	movq	%rbx, -328(%rbp)
	movl	-280(%rbp), %ebx
	cmpl	%eax, %ebx
	cmovle	%ebx, %eax
	movl	%eax, -248(%rbp)
	cmpl	%r12d, %eax
	jle	.L1403
	leal	16(,%r12,8), %r8d
	leaq	-208(%rbp), %rax
	movslq	%r8d, %rbx
	movq	%rax, -264(%rbp)
	movl	%r12d, %eax
	movq	%rbx, %r12
	movl	%eax, %ebx
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1593:
	xorl	%esi, %esi
	movq	%rax, %xmm0
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
.L1248:
	movq	-264(%rbp), %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal12_GLOBAL__N_118ArrayConcatVisitor5visitEjNS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1536
.L1244:
	addl	$1, %ebx
	addq	$8, %r12
	cmpl	%ebx, -248(%rbp)
	je	.L1240
.L1250:
	movabsq	$-2251799814209537, %rcx
	movq	0(%r13), %rax
	movq	-1(%r12,%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1593
	movq	(%r15), %rax
	movq	-256(%rbp), %rdi
	movabsq	$824633720832, %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	%rcx, -132(%rbp)
	movl	$3, -144(%rbp)
	subq	$37592, %rax
	movq	$0, -112(%rbp)
	movq	%rax, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r15, -80(%rbp)
	movl	%ebx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	-256(%rbp), %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	movzbl	%ah, %edx
	testb	%al, %al
	je	.L1536
	testb	%dl, %dl
	je	.L1244
	movq	-256(%rbp), %rdi
	movq	%r14, -120(%rbp)
	movabsq	$824633720832, %rax
	movl	$3, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r15, -80(%rbp)
	movl	%ebx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L1247
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rdx
	jmp	.L1248
.L1233:
	movl	-248(%rbp), %ebx
	cmpl	%ebx, -272(%rbp)
	jg	.L1235
.L1541:
	movl	-188(%rbp), %eax
	movq	%r12, %r14
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1264:
	cmpq	%rbx, %r8
	je	.L1272
	movq	%r14, -248(%rbp)
	movq	%r8, %r14
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1595:
	cmpq	%rbx, %r12
	je	.L1278
	movq	%rbx, %rdx
	movl	$4, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L1278:
	movl	%r13d, (%r12)
.L1279:
	addq	$4, %rbx
	cmpq	%rbx, %r14
	je	.L1594
.L1276:
	movl	(%rbx), %r13d
	cmpl	(%r12), %r13d
	jb	.L1595
	movl	-4(%rbx), %edx
	leaq	-4(%rbx), %rax
	cmpl	%edx, %r13d
	jnb	.L1406
	.p2align 4,,10
	.p2align 3
.L1281:
	movl	%edx, 4(%rax)
	movq	%rax, %rcx
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %r13d
	jb	.L1281
.L1280:
	movl	%r13d, (%rcx)
	jmp	.L1279
.L1594:
	movq	-248(%rbp), %r14
.L1272:
	movq	-168(%rbp), %r9
	subq	-176(%rbp), %r9
	sarq	$2, %r9
	movq	%r9, %r12
	je	.L1295
	movq	$0, -264(%rbp)
	xorl	%r13d, %r13d
	movq	%r15, -320(%rbp)
.L1283:
	movq	41088(%r14), %rbx
	addl	$1, 41104(%r14)
	addq	$1024, -264(%rbp)
	movq	-264(%rbp), %rax
	movq	%rbx, -280(%rbp)
	movq	41096(%r14), %rbx
	cmpq	%rax, %r12
	cmovbe	%r12, %rax
	movq	%rbx, -272(%rbp)
	movq	%rax, -256(%rbp)
	cmpq	%rax, %r13
	jnb	.L1286
	movq	%r14, -248(%rbp)
	movq	-176(%rbp), %rax
	leaq	-144(%rbp), %rbx
	movq	-320(%rbp), %r15
.L1292:
	movl	(%rax,%r13,4), %r14d
	movq	%rbx, %rdi
	movq	$0, -112(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	-248(%rbp), %rax
	movl	$3, -144(%rbp)
	movq	%rax, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r15, -80(%rbp)
	movl	%r14d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L1287
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rdx
.L1288:
	leaq	-208(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal12_GLOBAL__N_118ArrayConcatVisitor5visitEjNS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1289
	movq	-176(%rbp), %rax
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L1596:
	cmpl	(%rax,%r13,4), %r14d
	jne	.L1290
.L1291:
	addq	$1, %r13
	cmpq	%r13, %r12
	ja	.L1596
.L1290:
	cmpq	-256(%rbp), %r13
	jb	.L1292
	movq	-248(%rbp), %r14
.L1286:
	movq	-280(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-272(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L1294
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	cmpq	%r13, %r12
	ja	.L1283
.L1295:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1540
	call	_ZdlPv@PLT
	movl	-188(%rbp), %eax
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1287:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L1288
.L1289:
	movq	-248(%rbp), %r15
	movq	-280(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-272(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L1368
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1368:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1377
	call	_ZdlPv@PLT
	jmp	.L1377
.L1577:
	movq	-248(%rbp), %r8
	movq	%r14, %r13
	movq	-256(%rbp), %r14
	cmpq	%r13, %r8
	je	.L1272
	.p2align 4,,10
	.p2align 3
.L1271:
	movl	0(%r13), %ecx
	movl	-4(%r13), %edx
	leaq	-4(%r13), %rax
	cmpl	%edx, %ecx
	jnb	.L1405
	.p2align 4,,10
	.p2align 3
.L1274:
	movl	%edx, 4(%rax)
	movq	%rax, %rsi
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %ecx
	jb	.L1274
.L1273:
	addq	$4, %r13
	movl	%ecx, (%rsi)
	cmpq	%r13, %r8
	jne	.L1271
	jmp	.L1272
.L1405:
	movq	%r13, %rsi
	jmp	.L1273
.L1247:
	movq	-256(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L1248
.L1536:
	movq	-320(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%r14, %r15
	movq	%rax, 41088(%r14)
	movq	-328(%rbp), %rax
	cmpq	41096(%r14), %rax
	jne	.L1371
	jmp	.L1377
.L1592:
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	memmove@PLT
.L1261:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L1262
.L1294:
	cmpq	%r13, %r12
	ja	.L1283
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	%r14, %rdi
	movq	%rsi, -248(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-248(%rbp), %rsi
	jmp	.L1257
.L1403:
	movl	%r12d, -248(%rbp)
.L1240:
	movq	-320(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-328(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L1251
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-248(%rbp), %ebx
	cmpl	%ebx, -280(%rbp)
	jle	.L1540
.L1252:
	movl	-248(%rbp), %r12d
	jmp	.L1253
.L1297:
	movq	-264(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-256(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L1370
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1590:
	movq	%r14, %rdi
	movq	%rsi, -248(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-248(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1217
.L1237:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L1597
.L1239:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%r12, 0(%r13)
	jmp	.L1238
.L1404:
	movq	%r12, %rsi
	jmp	.L1268
.L1406:
	movq	%rbx, %rcx
	jmp	.L1280
.L1251:
	movl	-248(%rbp), %ebx
	cmpl	%ebx, -280(%rbp)
	jg	.L1252
	jmp	.L1540
.L1402:
	movl	%r14d, -248(%rbp)
	jmp	.L1219
.L1554:
	call	__stack_chk_fail@PLT
.L1597:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L1239
.L1575:
	leaq	.LC18(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1414:
	movl	$0, -232(%rbp)
	jmp	.L1375
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.cfi_startproc
	.type	_ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE.cold, @function
_ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE.cold:
.LFSB20934:
.L1388:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	xorl	%ebx, %ebx
	jmp	.L1100
	.cfi_endproc
.LFE20934:
	.section	.text._ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.size	_ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text.unlikely._ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.size	_ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE.cold, .-_ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE.cold
.LCOLDE19:
	.section	.text._ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE
.LHOTE19:
	.section	.rodata._ZN2v88internalL30Builtin_Impl_Stats_ArrayConcatEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"V8.Builtin_ArrayConcat"
	.section	.text._ZN2v88internalL30Builtin_Impl_Stats_ArrayConcatEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Builtin_Impl_Stats_ArrayConcatEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Builtin_Impl_Stats_ArrayConcatEiPmPNS0_7IsolateE:
.LFB20932:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1627
.L1599:
	movq	_ZZN2v88internalL30Builtin_Impl_Stats_ArrayConcatEiPmPNS0_7IsolateEE29trace_event_unique_atomic1458(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1628
.L1601:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1629
.L1603:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1630
.L1607:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1631
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1628:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1632
.L1602:
	movq	%rbx, _ZZN2v88internalL30Builtin_Impl_Stats_ArrayConcatEiPmPNS0_7IsolateEE29trace_event_unique_atomic1458(%rip)
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1629:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1633
.L1604:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1605
	movq	(%rdi), %rax
	call	*8(%rax)
.L1605:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1606
	movq	(%rdi), %rax
	call	*8(%rax)
.L1606:
	leaq	.LC20(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1630:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1627:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$676, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1599
	.p2align 4,,10
	.p2align 3
.L1633:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC20(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1632:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1602
.L1631:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20932:
	.size	_ZN2v88internalL30Builtin_Impl_Stats_ArrayConcatEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Builtin_Impl_Stats_ArrayConcatEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Builtin_ArrayConcatEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Builtin_ArrayConcatEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Builtin_ArrayConcatEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Builtin_ArrayConcatEiPmPNS0_7IsolateE:
.LFB20933:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1638
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL24Builtin_Impl_ArrayConcatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1638:
	.cfi_restore 6
	jmp	_ZN2v88internalL30Builtin_Impl_Stats_ArrayConcatEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20933:
	.size	_ZN2v88internal19Builtin_ArrayConcatEiPmPNS0_7IsolateE, .-_ZN2v88internal19Builtin_ArrayConcatEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal26Builtin_ArrayPrototypeFillEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal26Builtin_ArrayPrototypeFillEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal26Builtin_ArrayPrototypeFillEiPmPNS0_7IsolateE:
.LFB25658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25658:
	.size	_GLOBAL__sub_I__ZN2v88internal26Builtin_ArrayPrototypeFillEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal26Builtin_ArrayPrototypeFillEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal26Builtin_ArrayPrototypeFillEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL30Builtin_Impl_Stats_ArrayConcatEiPmPNS0_7IsolateEE29trace_event_unique_atomic1458,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Builtin_Impl_Stats_ArrayConcatEiPmPNS0_7IsolateEE29trace_event_unique_atomic1458, @object
	.size	_ZZN2v88internalL30Builtin_Impl_Stats_ArrayConcatEiPmPNS0_7IsolateEE29trace_event_unique_atomic1458, 8
_ZZN2v88internalL30Builtin_Impl_Stats_ArrayConcatEiPmPNS0_7IsolateEE29trace_event_unique_atomic1458:
	.zero	8
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateEE28trace_event_unique_atomic595,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateEE28trace_event_unique_atomic595, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateEE28trace_event_unique_atomic595, 8
_ZZN2v88internalL31Builtin_Impl_Stats_ArrayUnshiftEiPmPNS0_7IsolateEE28trace_event_unique_atomic595:
	.zero	8
	.section	.bss._ZZN2v88internalL29Builtin_Impl_Stats_ArrayShiftEiPmPNS0_7IsolateEE28trace_event_unique_atomic564,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Builtin_Impl_Stats_ArrayShiftEiPmPNS0_7IsolateEE28trace_event_unique_atomic564, @object
	.size	_ZZN2v88internalL29Builtin_Impl_Stats_ArrayShiftEiPmPNS0_7IsolateEE28trace_event_unique_atomic564, 8
_ZZN2v88internalL29Builtin_Impl_Stats_ArrayShiftEiPmPNS0_7IsolateEE28trace_event_unique_atomic564:
	.zero	8
	.section	.bss._ZZN2v88internalL27Builtin_Impl_Stats_ArrayPopEiPmPNS0_7IsolateEE28trace_event_unique_atomic452,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Builtin_Impl_Stats_ArrayPopEiPmPNS0_7IsolateEE28trace_event_unique_atomic452, @object
	.size	_ZZN2v88internalL27Builtin_Impl_Stats_ArrayPopEiPmPNS0_7IsolateEE28trace_event_unique_atomic452, 8
_ZZN2v88internalL27Builtin_Impl_Stats_ArrayPopEiPmPNS0_7IsolateEE28trace_event_unique_atomic452:
	.zero	8
	.section	.bss._ZZN2v88internalL28Builtin_Impl_Stats_ArrayPushEiPmPNS0_7IsolateEE28trace_event_unique_atomic366,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Builtin_Impl_Stats_ArrayPushEiPmPNS0_7IsolateEE28trace_event_unique_atomic366, @object
	.size	_ZZN2v88internalL28Builtin_Impl_Stats_ArrayPushEiPmPNS0_7IsolateEE28trace_event_unique_atomic366, 8
_ZZN2v88internalL28Builtin_Impl_Stats_ArrayPushEiPmPNS0_7IsolateEE28trace_event_unique_atomic366:
	.zero	8
	.section	.bss._ZZN2v88internalL37Builtin_Impl_Stats_ArrayPrototypeFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic245,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Builtin_Impl_Stats_ArrayPrototypeFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic245, @object
	.size	_ZZN2v88internalL37Builtin_Impl_Stats_ArrayPrototypeFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic245, 8
_ZZN2v88internalL37Builtin_Impl_Stats_ArrayPrototypeFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic245:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.align 8
.LC2:
	.long	4294967295
	.long	1128267775
	.align 8
.LC3:
	.long	4290772992
	.long	1106247679
	.align 8
.LC4:
	.long	4292870144
	.long	1106247679
	.align 8
.LC5:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
