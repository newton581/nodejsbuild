	.file	"scavenger.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5176:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5176:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5177:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5177:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5179:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5179:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,"axG",@progbits,_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.type	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, @function
_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE:
.LFB7099:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7099:
	.size	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, .-_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.text._ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.type	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, @function
_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_:
.LFB7100:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*16(%rax)
	.cfi_endproc
.LFE7100:
	.size	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, .-_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB7101:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rdx), %rcx
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7101:
	.size	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, @function
_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE:
.LFB7102:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rdx), %rcx
	movq	24(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7102:
	.size	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, .-_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE:
.LFB7105:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7105:
	.size	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB7106:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7106:
	.size	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.type	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm, @function
_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm:
.LFB7107:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7107:
	.size	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm, .-_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.section	.text._ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB7108:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7108:
	.size	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB7109:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7109:
	.size	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CancelableTask3RunEv
	.type	_ZN2v88internal14CancelableTask3RunEv, @function
_ZN2v88internal14CancelableTask3RunEv:
.LFB7944:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L14
	movq	(%rdi), %rax
	jmp	*24(%rax)
.L14:
	ret
	.cfi_endproc
.LFE7944:
	.size	_ZN2v88internal14CancelableTask3RunEv, .-_ZN2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal10PagedSpace24SupportsInlineAllocationEv,"axG",@progbits,_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.type	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv, @function
_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv:
.LFB8197:
	.cfi_startproc
	endbr64
	cmpl	$2, 72(%rdi)
	je	.L24
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*152(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE8197:
	.size	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv, .-_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.section	.text._ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.type	_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, @function
_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_:
.LFB22306:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	jnb	.L35
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jbe	.L38
.L29:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L27
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L27
	movq	8(%r13), %rdi
	movq	%rbx, %rsi
	addq	$8, %rbx
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	cmpq	%rbx, %r12
	ja	.L29
.L38:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE22306:
	.size	_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, .-_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_,"axG",@progbits,_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.type	_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_, @function
_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_:
.LFB22307:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	jnb	.L49
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L41:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jbe	.L52
.L43:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L41
	cmpl	$3, %edx
	je	.L41
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L41
	movq	8(%r13), %rdi
	movq	%rbx, %rsi
	andq	$-3, %rdx
	addq	$8, %rbx
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	cmpq	%rbx, %r12
	ja	.L43
.L52:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE22307:
	.size	_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_, .-_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.section	.rodata._ZN2v88internal15ScavengeVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"address < start || address >= end"
	.section	.rodata._ZN2v88internal15ScavengeVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal15ScavengeVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal15ScavengeVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15ScavengeVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal15ScavengeVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal15ScavengeVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB22308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	(%rbx), %rdx
	addq	%rdx, %rbx
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	leaq	4(%rbx), %r14
	movq	%rax, %r13
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %eax
	addq	%r13, %rax
	cmpq	%r14, %rax
	jbe	.L54
	cmpq	%r14, %r13
	jbe	.L67
.L54:
	leaq	-59(%rbx), %rdx
	movq	%rdx, %rax
	movq	%rdx, -48(%rbp)
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L68
.L53:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	8(%r12), %rdi
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22308:
	.size	_ZN2v88internal15ScavengeVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal15ScavengeVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal15ScavengeVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal15ScavengeVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15ScavengeVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal15ScavengeVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal15ScavengeVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE:
.LFB22309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	(%rax), %rdx
	movq	%rdx, %rax
	movq	%rdx, -16(%rbp)
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L77
.L70:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	leaq	-16(%rbp), %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L70
.L78:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22309:
	.size	_ZN2v88internal15ScavengeVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal15ScavengeVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.type	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, @function
_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_:
.LFB22351:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	jnb	.L91
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andq	$-262144, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L82:
	cmpb	$0, 16(%r14)
	jne	.L94
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jnb	.L95
.L85:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L81
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$8, %al
	je	.L82
	movq	8(%r14), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	testl	%eax, %eax
	jne	.L81
	movq	%rbx, %rsi
	movq	%r13, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	cmpq	%r12, %rbx
	jb	.L85
.L95:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	testb	$64, %al
	je	.L81
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE22351:
	.size	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, .-_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_,"axG",@progbits,_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.type	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_, @function
_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_:
.LFB22352:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rcx
	jbe	.L108
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andq	$-262144, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	.p2align 4,,10
	.p2align 3
.L102:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L98
	cmpl	$3, %edx
	je	.L98
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$8, %al
	jne	.L111
	cmpb	$0, 16(%r14)
	jne	.L112
	.p2align 4,,10
	.p2align 3
.L98:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	ja	.L102
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movq	8(%r14), %rdi
	andq	$-3, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	testl	%eax, %eax
	jne	.L98
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L112:
	testb	$64, %al
	je	.L98
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE22352:
	.size	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_, .-_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.section	.text._ZN2v88internal40IterateAndScavengePromotedObjectsVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB22353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	(%rbx), %rdx
	addq	%rdx, %rbx
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	leaq	4(%rbx), %r15
	movq	%rax, %r14
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %eax
	addq	%r14, %rax
	cmpq	%r15, %rax
	jbe	.L114
	cmpq	%r15, %r14
	jbe	.L129
.L114:
	leaq	-59(%rbx), %rdx
	leaq	-64(%rbp), %r14
	movq	%rdx, %rax
	movq	%rdx, -64(%rbp)
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$8, %al
	jne	.L130
	cmpb	$0, 16(%r12)
	jne	.L131
.L113:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movq	8(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	testl	%eax, %eax
	jne	.L113
	movq	%r13, %rdi
	movq	%r14, %rsi
	andq	$-262144, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L131:
	testb	$64, %al
	je	.L113
	movq	%r13, %rdi
	movq	%r14, %rsi
	andq	$-262144, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L129:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22353:
	.size	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal40IterateAndScavengePromotedObjectsVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE:
.LFB22354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-32(%rbp), %r12
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	(%rax), %rdx
	movq	%rdx, %rax
	movq	%rdx, -32(%rbp)
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$8, %al
	jne	.L142
	cmpb	$0, 16(%rdi)
	jne	.L143
.L133:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L144
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	testb	$64, %al
	je	.L133
	andq	$-262144, %rbx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L142:
	movq	8(%rdi), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	testl	%eax, %eax
	jne	.L133
	andq	$-262144, %rbx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L133
.L144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22354:
	.size	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZNSt17_Function_handlerIFbN2v88internal10HeapObjectEEZNS1_18ScavengerCollector14CollectGarbageEvEUlS2_E1_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFbN2v88internal10HeapObjectEEZNS1_18ScavengerCollector14CollectGarbageEvEUlS2_E1_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFbN2v88internal10HeapObjectEEZNS1_18ScavengerCollector14CollectGarbageEvEUlS2_E1_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB26757:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE26757:
	.size	_ZNSt17_Function_handlerIFbN2v88internal10HeapObjectEEZNS1_18ScavengerCollector14CollectGarbageEvEUlS2_E1_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFbN2v88internal10HeapObjectEEZNS1_18ScavengerCollector14CollectGarbageEvEUlS2_E1_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal18ScavengerCollector14CollectGarbageEvEUlNS2_10HeapObjectEE1_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal18ScavengerCollector14CollectGarbageEvEUlNS2_10HeapObjectEE1_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal18ScavengerCollector14CollectGarbageEvEUlNS2_10HeapObjectEE1_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation:
.LFB26758:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L148
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26758:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal18ScavengerCollector14CollectGarbageEvEUlNS2_10HeapObjectEE1_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal18ScavengerCollector14CollectGarbageEvEUlNS2_10HeapObjectEE1_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.section	.text._ZN2v88internal19RootScavengeVisitorD2Ev,"axG",@progbits,_ZN2v88internal19RootScavengeVisitorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19RootScavengeVisitorD2Ev
	.type	_ZN2v88internal19RootScavengeVisitorD2Ev, @function
_ZN2v88internal19RootScavengeVisitorD2Ev:
.LFB29711:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE29711:
	.size	_ZN2v88internal19RootScavengeVisitorD2Ev, .-_ZN2v88internal19RootScavengeVisitorD2Ev
	.weak	_ZN2v88internal19RootScavengeVisitorD1Ev
	.set	_ZN2v88internal19RootScavengeVisitorD1Ev,_ZN2v88internal19RootScavengeVisitorD2Ev
	.section	.text._ZN2v88internal26ScavengeWeakObjectRetainerD2Ev,"axG",@progbits,_ZN2v88internal26ScavengeWeakObjectRetainerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ScavengeWeakObjectRetainerD2Ev
	.type	_ZN2v88internal26ScavengeWeakObjectRetainerD2Ev, @function
_ZN2v88internal26ScavengeWeakObjectRetainerD2Ev:
.LFB29715:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE29715:
	.size	_ZN2v88internal26ScavengeWeakObjectRetainerD2Ev, .-_ZN2v88internal26ScavengeWeakObjectRetainerD2Ev
	.weak	_ZN2v88internal26ScavengeWeakObjectRetainerD1Ev
	.set	_ZN2v88internal26ScavengeWeakObjectRetainerD1Ev,_ZN2v88internal26ScavengeWeakObjectRetainerD2Ev
	.section	.text._ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD2Ev,"axG",@progbits,_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD2Ev
	.type	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD2Ev, @function
_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD2Ev:
.LFB29719:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE29719:
	.size	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD2Ev, .-_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD2Ev
	.weak	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD1Ev
	.set	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD1Ev,_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD2Ev
	.section	.text._ZN2v88internal18PageScavengingItemD2Ev,"axG",@progbits,_ZN2v88internal18PageScavengingItemD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18PageScavengingItemD2Ev
	.type	_ZN2v88internal18PageScavengingItemD2Ev, @function
_ZN2v88internal18PageScavengingItemD2Ev:
.LFB29727:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE29727:
	.size	_ZN2v88internal18PageScavengingItemD2Ev, .-_ZN2v88internal18PageScavengingItemD2Ev
	.weak	_ZN2v88internal18PageScavengingItemD1Ev
	.set	_ZN2v88internal18PageScavengingItemD1Ev,_ZN2v88internal18PageScavengingItemD2Ev
	.section	.text._ZN2v88internal15ScavengeVisitorD2Ev,"axG",@progbits,_ZN2v88internal15ScavengeVisitorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15ScavengeVisitorD2Ev
	.type	_ZN2v88internal15ScavengeVisitorD2Ev, @function
_ZN2v88internal15ScavengeVisitorD2Ev:
.LFB29739:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE29739:
	.size	_ZN2v88internal15ScavengeVisitorD2Ev, .-_ZN2v88internal15ScavengeVisitorD2Ev
	.weak	_ZN2v88internal15ScavengeVisitorD1Ev
	.set	_ZN2v88internal15ScavengeVisitorD1Ev,_ZN2v88internal15ScavengeVisitorD2Ev
	.section	.text._ZN2v88internal18PageScavengingItemD0Ev,"axG",@progbits,_ZN2v88internal18PageScavengingItemD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18PageScavengingItemD0Ev
	.type	_ZN2v88internal18PageScavengingItemD0Ev, @function
_ZN2v88internal18PageScavengingItemD0Ev:
.LFB29729:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29729:
	.size	_ZN2v88internal18PageScavengingItemD0Ev, .-_ZN2v88internal18PageScavengingItemD0Ev
	.section	.text._ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD0Ev,"axG",@progbits,_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD0Ev
	.type	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD0Ev, @function
_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD0Ev:
.LFB29721:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29721:
	.size	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD0Ev, .-_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD0Ev
	.section	.text._ZN2v88internal19RootScavengeVisitorD0Ev,"axG",@progbits,_ZN2v88internal19RootScavengeVisitorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19RootScavengeVisitorD0Ev
	.type	_ZN2v88internal19RootScavengeVisitorD0Ev, @function
_ZN2v88internal19RootScavengeVisitorD0Ev:
.LFB29713:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29713:
	.size	_ZN2v88internal19RootScavengeVisitorD0Ev, .-_ZN2v88internal19RootScavengeVisitorD0Ev
	.section	.text._ZN2v88internal26ScavengeWeakObjectRetainerD0Ev,"axG",@progbits,_ZN2v88internal26ScavengeWeakObjectRetainerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ScavengeWeakObjectRetainerD0Ev
	.type	_ZN2v88internal26ScavengeWeakObjectRetainerD0Ev, @function
_ZN2v88internal26ScavengeWeakObjectRetainerD0Ev:
.LFB29717:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29717:
	.size	_ZN2v88internal26ScavengeWeakObjectRetainerD0Ev, .-_ZN2v88internal26ScavengeWeakObjectRetainerD0Ev
	.section	.text._ZN2v88internal15ScavengeVisitorD0Ev,"axG",@progbits,_ZN2v88internal15ScavengeVisitorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15ScavengeVisitorD0Ev
	.type	_ZN2v88internal15ScavengeVisitorD0Ev, @function
_ZN2v88internal15ScavengeVisitorD0Ev:
.LFB29741:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29741:
	.size	_ZN2v88internal15ScavengeVisitorD0Ev, .-_ZN2v88internal15ScavengeVisitorD0Ev
	.section	.text._ZN2v88internal14ScavengingTaskD2Ev,"axG",@progbits,_ZN2v88internal14ScavengingTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14ScavengingTaskD2Ev
	.type	_ZN2v88internal14ScavengingTaskD2Ev, @function
_ZN2v88internal14ScavengingTaskD2Ev:
.LFB29723:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE29723:
	.size	_ZN2v88internal14ScavengingTaskD2Ev, .-_ZN2v88internal14ScavengingTaskD2Ev
	.weak	_ZN2v88internal14ScavengingTaskD1Ev
	.set	_ZN2v88internal14ScavengingTaskD1Ev,_ZN2v88internal14ScavengingTaskD2Ev
	.section	.text._ZN2v88internal14ScavengingTaskD0Ev,"axG",@progbits,_ZN2v88internal14ScavengingTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14ScavengingTaskD0Ev
	.type	_ZN2v88internal14ScavengingTaskD0Ev, @function
_ZN2v88internal14ScavengingTaskD0Ev:
.LFB29725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29725:
	.size	_ZN2v88internal14ScavengingTaskD0Ev, .-_ZN2v88internal14ScavengingTaskD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_127IsUnscavengedHeapObjectSlotEPNS0_4HeapENS0_14FullObjectSlotE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_127IsUnscavengedHeapObjectSlotEPNS0_4HeapENS0_14FullObjectSlotE, @function
_ZN2v88internal12_GLOBAL__N_127IsUnscavengedHeapObjectSlotEPNS0_4HeapENS0_14FullObjectSlotE:
.LFB22360:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jne	.L164
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	testb	$8, 8(%rcx)
	je	.L162
	movq	-1(%rdx), %rax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	xorl	%eax, %eax
.L162:
	ret
	.cfi_endproc
.LFE22360:
	.size	_ZN2v88internal12_GLOBAL__N_127IsUnscavengedHeapObjectSlotEPNS0_4HeapENS0_14FullObjectSlotE, .-_ZN2v88internal12_GLOBAL__N_127IsUnscavengedHeapObjectSlotEPNS0_4HeapENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal26ScavengeWeakObjectRetainer8RetainAsENS0_6ObjectE,"axG",@progbits,_ZN2v88internal26ScavengeWeakObjectRetainer8RetainAsENS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ScavengeWeakObjectRetainer8RetainAsENS0_6ObjectE
	.type	_ZN2v88internal26ScavengeWeakObjectRetainer8RetainAsENS0_6ObjectE, @function
_ZN2v88internal26ScavengeWeakObjectRetainer8RetainAsENS0_6ObjectE:
.LFB22361:
	.cfi_startproc
	endbr64
	testb	$1, %sil
	je	.L169
	movq	%rsi, %rax
	andq	$-262144, %rax
	testb	$8, 8(%rax)
	je	.L169
	movq	-1(%rsi), %rdx
	movl	$0, %esi
	leaq	1(%rdx), %rax
	andl	$1, %edx
	cmovne	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE22361:
	.size	_ZN2v88internal26ScavengeWeakObjectRetainer8RetainAsENS0_6ObjectE, .-_ZN2v88internal26ScavengeWeakObjectRetainer8RetainAsENS0_6ObjectE
	.section	.text._ZN2v88internal14ScavengingTaskD2Ev,"axG",@progbits,_ZN2v88internal14ScavengingTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14ScavengingTaskD1Ev
	.type	_ZThn32_N2v88internal14ScavengingTaskD1Ev, @function
_ZThn32_N2v88internal14ScavengingTaskD1Ev:
.LFB30391:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	subq	$32, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE30391:
	.size	_ZThn32_N2v88internal14ScavengingTaskD1Ev, .-_ZThn32_N2v88internal14ScavengingTaskD1Ev
	.section	.text._ZN2v88internal14ScavengingTaskD0Ev,"axG",@progbits,_ZN2v88internal14ScavengingTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14ScavengingTaskD0Ev
	.type	_ZThn32_N2v88internal14ScavengingTaskD0Ev, @function
_ZThn32_N2v88internal14ScavengingTaskD0Ev:
.LFB30392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE30392:
	.size	_ZThn32_N2v88internal14ScavengingTaskD0Ev, .-_ZThn32_N2v88internal14ScavengingTaskD0Ev
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14CancelableTask3RunEv
	.type	_ZThn32_N2v88internal14CancelableTask3RunEv, @function
_ZThn32_N2v88internal14CancelableTask3RunEv:
.LFB30393:
	.cfi_startproc
	endbr64
	leaq	-32(%rdi), %r8
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L179
	movq	-32(%rdi), %rax
	movq	%r8, %rdi
	movq	24(%rax), %rax
	jmp	*%rax
.L179:
	ret
	.cfi_endproc
.LFE30393:
	.size	_ZThn32_N2v88internal14CancelableTask3RunEv, .-_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB7103:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %r9
	leaq	8(%rdx), %rcx
	movq	32(%rax), %r8
	cmpq	%r9, %r8
	jne	.L182
	jmp	*16(%rax)
	.p2align 4,,10
	.p2align 3
.L182:
	jmp	*%r8
	.cfi_endproc
.LFE7103:
	.size	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.type	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, @function
_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_:
.LFB7104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	leaq	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE(%rip), %rbx
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L184
	leaq	8(%rdx), %rcx
	call	*16(%rax)
	movq	(%r12), %rax
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L186
.L188:
	popq	%rbx
	leaq	8(%r14), %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	call	*%rcx
	movq	(%r12), %rax
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	je	.L188
.L186:
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rcx
	.cfi_endproc
.LFE7104:
	.size	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, .-_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB9605:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L195
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L198
.L189:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L189
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE9605:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZNK2v88internal10HeapObject4SizeEv,"axG",@progbits,_ZNK2v88internal10HeapObject4SizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10HeapObject4SizeEv
	.type	_ZNK2v88internal10HeapObject4SizeEv, @function
_ZNK2v88internal10HeapObject4SizeEv:
.LFB14952:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-1(%rax), %rsi
	jmp	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	.cfi_endproc
.LFE14952:
	.size	_ZNK2v88internal10HeapObject4SizeEv, .-_ZNK2v88internal10HeapObject4SizeEv
	.section	.rodata._ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"!object.IsSmi()"
	.section	.text._ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE,"axG",@progbits,_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE
	.type	_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE, @function
_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE:
.LFB19196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	120(%rdi), %rbx
	movq	104(%rdi), %rax
	testq	%rbx, %rbx
	je	.L201
	cmpq	%rax, %rbx
	ja	.L227
.L202:
	movq	%rax, %rcx
	subq	%rbx, %rcx
	movq	%rcx, %rbx
.L201:
	movslq	%r14d, %r13
	leaq	0(%r13,%rax), %rdx
	cmpq	112(%r12), %rdx
	jbe	.L206
	movq	(%r12), %rax
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*184(%rax)
	testb	%al, %al
	jne	.L228
	movslq	72(%r12), %rax
	salq	$32, %rax
	movq	%rax, %r13
	notq	%rax
	testb	$1, %al
	jne	.L211
.L232:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*152(%rax)
	testb	%al, %al
	je	.L229
.L211:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	104(%r12), %rax
	leaq	0(%r13,%rax), %rdx
.L206:
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	movq	%rdx, 104(%r12)
	leaq	1(%rax), %r13
	jne	.L230
.L209:
	movq	%r13, %rax
	notq	%rax
	testb	$1, %r13b
	je	.L231
	testb	$1, %al
	jne	.L211
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L227:
	movq	(%rdi), %rdx
	leaq	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv(%rip), %rsi
	movq	128(%rdx), %rcx
	cmpq	%rsi, %rcx
	jne	.L203
	cmpl	$2, 72(%rdi)
	jne	.L202
	call	*152(%rdx)
	testb	%al, %al
	jne	.L226
.L204:
	movq	104(%r12), %rbx
	movq	%rbx, 120(%r12)
	movq	%rbx, %rax
.L205:
	testq	%rbx, %rbx
	jne	.L202
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L230:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%r12, %rdi
	leaq	-1(%r13), %rdx
	leal	(%r14,%rbx), %esi
	movl	%r14d, %ecx
	call	_ZN2v88internal5Space14AllocationStepEimi@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*48(%rax)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L231:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L203:
	call	*%rcx
	testb	%al, %al
	jne	.L204
.L226:
	movq	120(%r12), %rbx
	movq	104(%r12), %rax
	jmp	.L205
	.cfi_endproc
.LFE19196:
	.size	_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE, .-_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE
	.section	.text._ZN2v88internal14LocalAllocator18FreeLastInNewSpaceENS0_10HeapObjectEi,"axG",@progbits,_ZN2v88internal14LocalAllocator18FreeLastInNewSpaceENS0_10HeapObjectEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LocalAllocator18FreeLastInNewSpaceENS0_10HeapObjectEi
	.type	_ZN2v88internal14LocalAllocator18FreeLastInNewSpaceENS0_10HeapObjectEi, @function
_ZN2v88internal14LocalAllocator18FreeLastInNewSpaceENS0_10HeapObjectEi:
.LFB22252:
	.cfi_startproc
	endbr64
	movq	496(%rdi), %rax
	subq	$1, %rsi
	testq	%rax, %rax
	je	.L234
	movslq	%edx, %rcx
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	je	.L240
.L234:
	movq	(%rdi), %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	jmp	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%rax, 496(%rdi)
	ret
	.cfi_endproc
.LFE22252:
	.size	_ZN2v88internal14LocalAllocator18FreeLastInNewSpaceENS0_10HeapObjectEi, .-_ZN2v88internal14LocalAllocator18FreeLastInNewSpaceENS0_10HeapObjectEi
	.section	.text._ZN2v88internal14LocalAllocator18FreeLastInOldSpaceENS0_10HeapObjectEi,"axG",@progbits,_ZN2v88internal14LocalAllocator18FreeLastInOldSpaceENS0_10HeapObjectEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LocalAllocator18FreeLastInOldSpaceENS0_10HeapObjectEi
	.type	_ZN2v88internal14LocalAllocator18FreeLastInOldSpaceENS0_10HeapObjectEi, @function
_ZN2v88internal14LocalAllocator18FreeLastInOldSpaceENS0_10HeapObjectEi:
.LFB22253:
	.cfi_startproc
	endbr64
	movq	128(%rdi), %rax
	subq	$1, %rsi
	testq	%rax, %rax
	je	.L242
	movslq	%edx, %rcx
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	je	.L248
.L242:
	movq	(%rdi), %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	jmp	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	.p2align 4,,10
	.p2align 3
.L248:
	movq	%rax, 128(%rdi)
	ret
	.cfi_endproc
.LFE22253:
	.size	_ZN2v88internal14LocalAllocator18FreeLastInOldSpaceENS0_10HeapObjectEi, .-_ZN2v88internal14LocalAllocator18FreeLastInOldSpaceENS0_10HeapObjectEi
	.section	.rodata._ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"!allocation.IsRetry()"
	.section	.text._ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE,"axG",@progbits,_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE
	.type	_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE, @function
_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE:
.LFB22256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movslq	%esi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$8192, %r15d
	jle	.L250
	movq	8(%rdi), %r12
	movl	%edx, %r13d
	leaq	152(%r12), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	104(%r12), %rax
	cmpq	120(%r12), %rax
	jnb	.L251
	movq	%rax, 120(%r12)
.L251:
	movslq	%r15d, %rbx
	leaq	(%rax,%rbx), %rdx
	cmpq	%rdx, 112(%r12)
	jb	.L301
.L252:
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	movq	%rdx, 104(%r12)
	leaq	1(%rax), %rbx
	jne	.L302
.L254:
	testb	$1, %bl
	je	.L263
.L253:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%rbx, %rax
.L255:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L303
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movq	496(%rdi), %rbx
	movl	%ecx, %r12d
	testq	%rbx, %rbx
	je	.L304
	leaq	488(%rdi), %r13
.L268:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Heap14GetFillToAlignEmNS0_19AllocationAlignmentE@PLT
	movslq	%eax, %rcx
	leaq	(%r15,%rbx), %rax
	movq	%rcx, %rdx
	addq	%rcx, %rax
	cmpq	504(%r14), %rax
	ja	.L275
	movq	%rax, 496(%r14)
	leaq	1(%rbx), %rax
	testl	%ecx, %ecx
	jg	.L305
	movq	%rax, %rdx
	notq	%rdx
	testb	$1, %al
	je	.L263
.L274:
	andl	$1, %edx
	je	.L255
	.p2align 4,,10
	.p2align 3
.L275:
	cmpb	$0, 512(%r14)
	jne	.L258
	leaq	-112(%rbp), %rbx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal21LocalAllocationBufferC1ERKS1_@PLT
	movq	8(%r14), %r8
	leaq	152(%r8), %r9
	movq	%r8, -168(%rbp)
	movq	%r9, %rdi
	movq	%r9, -160(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-168(%rbp), %r8
	movq	-160(%rbp), %r9
	movq	104(%r8), %rax
	cmpq	120(%r8), %rax
	jnb	.L277
	movq	%rax, 120(%r8)
.L277:
	leaq	32768(%rax), %rdx
	cmpq	%rdx, 112(%r8)
	jb	.L306
.L278:
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	movq	%rdx, 104(%r8)
	leaq	1(%rax), %rcx
	jne	.L307
.L280:
	andl	$1, %ecx
	movq	%rdx, -168(%rbp)
	movq	%rax, -160(%rbp)
	je	.L263
	movq	%r9, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-160(%rbp), %rax
	leaq	-80(%rbp), %r8
	movq	(%r14), %rsi
	movq	-168(%rbp), %rdx
	movq	%r8, %rdi
	movq	%rax, %xmm2
	movq	%rdx, %xmm4
	punpcklqdq	%xmm4, %xmm2
	movaps	%xmm2, -160(%rbp)
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rcx
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal21LocalAllocationBufferC1EPNS0_4HeapENS0_20LinearAllocationAreaE@PLT
	movq	-160(%rbp), %r8
.L282:
	movq	%r8, %rsi
	movq	%r13, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal21LocalAllocationBufferaSERKS1_@PLT
	movq	-160(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal21LocalAllocationBuffer5CloseEv@PLT
	movq	496(%r14), %rax
	testq	%rax, %rax
	je	.L266
	cmpq	-96(%rbp), %rax
	je	.L284
.L300:
	movq	%rbx, %rdi
	call	_ZN2v88internal21LocalAllocationBuffer5CloseEv@PLT
	movq	496(%r14), %rbx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	addq	%rbx, %r15
	call	_ZN2v88internal4Heap14GetFillToAlignEmNS0_19AllocationAlignmentE@PLT
	movl	%eax, %edx
	cltq
	addq	%rax, %r15
	cmpq	504(%r14), %r15
	jbe	.L286
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L301:
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8NewSpace16EnsureAllocationEiNS0_19AllocationAlignmentE@PLT
	testb	%al, %al
	je	.L292
	movq	104(%r12), %rax
	leaq	(%rbx,%rax), %rdx
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L304:
	cmpb	$0, 512(%rdi)
	je	.L308
.L258:
	movabsq	$8589934592, %rax
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L308:
	leaq	488(%rdi), %r13
	leaq	-144(%rbp), %rbx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal21LocalAllocationBufferC1ERKS1_@PLT
	movq	8(%r14), %r8
	leaq	152(%r8), %r9
	movq	%r8, -168(%rbp)
	movq	%r9, %rdi
	movq	%r9, -160(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-168(%rbp), %r8
	movq	-160(%rbp), %r9
	movq	104(%r8), %rax
	cmpq	120(%r8), %rax
	jnb	.L259
	movq	%rax, 120(%r8)
.L259:
	leaq	32768(%rax), %rdx
	cmpq	%rdx, 112(%r8)
	jnb	.L260
	xorl	%edx, %edx
	movl	$32768, %esi
	movq	%r8, %rdi
	movq	%r9, -160(%rbp)
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal8NewSpace16EnsureAllocationEiNS0_19AllocationAlignmentE@PLT
	movq	-160(%rbp), %r9
	testb	%al, %al
	je	.L261
	movq	-168(%rbp), %r8
	movq	104(%r8), %rax
	leaq	32768(%rax), %rdx
.L260:
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	movq	%rdx, 104(%r8)
	leaq	1(%rax), %rcx
	jne	.L309
.L262:
	andl	$1, %ecx
	movq	%rdx, -168(%rbp)
	movq	%rax, -160(%rbp)
	je	.L263
	movq	%r9, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-160(%rbp), %rax
	leaq	-80(%rbp), %r8
	movq	(%r14), %rsi
	movq	-168(%rbp), %rdx
	movq	%r8, %rdi
	movq	%rax, %xmm1
	movq	%rdx, %xmm3
	punpcklqdq	%xmm3, %xmm1
	movaps	%xmm1, -160(%rbp)
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rcx
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal21LocalAllocationBufferC1EPNS0_4HeapENS0_20LinearAllocationAreaE@PLT
	movq	-160(%rbp), %r8
.L265:
	movq	%r8, %rsi
	movq	%r13, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal21LocalAllocationBufferaSERKS1_@PLT
	movq	-160(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal21LocalAllocationBuffer5CloseEv@PLT
	movq	496(%r14), %rax
	testq	%rax, %rax
	je	.L266
	cmpq	-128(%rbp), %rax
	je	.L267
.L299:
	movq	%rbx, %rdi
	call	_ZN2v88internal21LocalAllocationBuffer5CloseEv@PLT
	movq	496(%r14), %rbx
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L292:
	movabsq	$4294967296, %rbx
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L302:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L266:
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal21LocalAllocationBufferaSERKS1_@PLT
	movb	$1, 512(%r14)
	movq	%rbx, %rdi
	call	_ZN2v88internal21LocalAllocationBuffer5CloseEv@PLT
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L286:
	movq	%r15, 496(%r14)
	leaq	1(%rbx), %rax
	testl	%edx, %edx
	jg	.L310
	testb	$1, %al
	jne	.L255
	.p2align 4,,10
	.p2align 3
.L263:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L305:
	movq	488(%r14), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal4Heap17PrecedeWithFillerENS0_10HeapObjectEi@PLT
	movq	%rax, %rdx
	notq	%rdx
	testb	$1, %al
	jne	.L274
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L306:
	xorl	%edx, %edx
	movl	$32768, %esi
	movq	%r8, %rdi
	movq	%r9, -160(%rbp)
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal8NewSpace16EnsureAllocationEiNS0_19AllocationAlignmentE@PLT
	movq	-160(%rbp), %r9
	testb	%al, %al
	je	.L279
	movq	-168(%rbp), %r8
	movq	104(%r8), %rax
	leaq	32768(%rax), %rdx
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%r9, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	-80(%rbp), %r8
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal21LocalAllocationBufferC1EPNS0_4HeapENS0_20LinearAllocationAreaE@PLT
	movq	-160(%rbp), %r8
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L267:
	movq	-136(%rbp), %rax
	pxor	%xmm0, %xmm0
	movups	%xmm0, -136(%rbp)
	movq	%rax, 496(%r14)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L309:
	movl	$1, %esi
	movq	%r8, %rdi
	movq	%rcx, -184(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	movq	-184(%rbp), %rcx
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %r9
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L307:
	movl	$1, %esi
	movq	%r8, %rdi
	movq	%rcx, -184(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	movq	-184(%rbp), %rcx
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %r9
	jmp	.L280
.L279:
	movq	%r9, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	-80(%rbp), %r8
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal21LocalAllocationBufferC1EPNS0_4HeapENS0_20LinearAllocationAreaE@PLT
	movq	-160(%rbp), %r8
	jmp	.L282
.L310:
	movq	488(%r14), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal4Heap17PrecedeWithFillerENS0_10HeapObjectEi@PLT
	testb	$1, %al
	jne	.L255
	jmp	.L263
.L284:
	movq	-104(%rbp), %rax
	pxor	%xmm0, %xmm0
	movups	%xmm0, -104(%rbp)
	movq	%rax, 496(%r14)
	jmp	.L300
.L303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22256:
	.size	_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE, .-_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE
	.section	.text._ZN2v88internal18ScavengerCollectorC2EPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ScavengerCollectorC2EPNS0_4HeapE
	.type	_ZN2v88internal18ScavengerCollectorC2EPNS0_4HeapE, @function
_ZN2v88internal18ScavengerCollectorC2EPNS0_4HeapE:
.LFB22384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-37592(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	movq	%rsi, -8(%rdi)
	xorl	%esi, %esi
	call	_ZN2v84base9SemaphoreC1Ei@PLT
	leaq	96(%rbx), %rax
	movq	$1, 56(%rbx)
	movq	%rax, 48(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movl	$0x3f800000, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22384:
	.size	_ZN2v88internal18ScavengerCollectorC2EPNS0_4HeapE, .-_ZN2v88internal18ScavengerCollectorC2EPNS0_4HeapE
	.globl	_ZN2v88internal18ScavengerCollectorC1EPNS0_4HeapE
	.set	_ZN2v88internal18ScavengerCollectorC1EPNS0_4HeapE,_ZN2v88internal18ScavengerCollectorC2EPNS0_4HeapE
	.section	.text._ZN2v88internal18ScavengerCollector30HandleSurvivingNewLargeObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ScavengerCollector30HandleSurvivingNewLargeObjectsEv
	.type	_ZN2v88internal18ScavengerCollector30HandleSurvivingNewLargeObjectsEv, @function
_ZN2v88internal18ScavengerCollector30HandleSurvivingNewLargeObjectsEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	64(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L314
	.p2align 4,,10
	.p2align 3
.L315:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rax
	movq	%rax, -1(%rsi)
	andq	$-262144, %rsi
	movq	8(%r12), %rax
	movq	280(%rax), %rdi
	call	_ZN2v88internal16LargeObjectSpace21PromoteNewLargeObjectEPNS0_9LargePageE@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L315
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L314
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L316
.L314:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	popq	%rbx
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal18ScavengerCollector30HandleSurvivingNewLargeObjectsEv, .-_ZN2v88internal18ScavengerCollector30HandleSurvivingNewLargeObjectsEv
	.section	.text._ZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEv
	.type	_ZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEv, @function
_ZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEv:
.LFB22432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpb	$0, _ZN2v88internal22FLAG_parallel_scavengeE(%rip)
	jne	.L337
.L327:
	addq	$8, %rsp
	movl	$1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	%rdi, %r13
	movq	248(%rax), %rax
	movq	312(%rax), %rbx
	movzbl	_ZGVZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores(%rip), %eax
	testl	%ebx, %ebx
	leal	1048575(%rbx), %r12d
	cmovns	%ebx, %r12d
	sarl	$20, %r12d
	leal	1(%r12), %ebx
	testb	%al, %al
	je	.L338
.L329:
	cmpl	$8, _ZZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores(%rip)
	movl	$8, %r12d
	cmovle	_ZZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores(%rip), %r12d
	movl	$1, %eax
	movq	8(%r13), %rdi
	cmpl	%ebx, %r12d
	cmovg	%ebx, %r12d
	testl	%r12d, %r12d
	cmovle	%eax, %r12d
	movl	%r12d, %esi
	sall	$18, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal4Heap22CanExpandOldGenerationEm@PLT
	testb	%al, %al
	je	.L327
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L329
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*40(%rax)
	leaq	_ZGVZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores(%rip), %rdi
	addl	$1, %eax
	movl	%eax, _ZZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L329
	.cfi_endproc
.LFE22432:
	.size	_ZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEv, .-_ZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEv
	.section	.text._ZN2v88internal9ScavengerC2EPNS0_18ScavengerCollectorEPNS0_4HeapEbPNS0_8WorklistISt4pairINS0_10HeapObjectEiELi256EEEPNS1_13PromotionListEPNS6_INS0_18EphemeronHashTableELi128EEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScavengerC2EPNS0_18ScavengerCollectorEPNS0_4HeapEbPNS0_8WorklistISt4pairINS0_10HeapObjectEiELi256EEEPNS1_13PromotionListEPNS6_INS0_18EphemeronHashTableELi128EEEi
	.type	_ZN2v88internal9ScavengerC2EPNS0_18ScavengerCollectorEPNS0_4HeapEbPNS0_8WorklistISt4pairINS0_10HeapObjectEiELi256EEEPNS1_13PromotionListEPNS6_INS0_18EphemeronHashTableELi128EEEi, @function
_ZN2v88internal9ScavengerC2EPNS0_18ScavengerCollectorEPNS0_4HeapEbPNS0_8WorklistISt4pairINS0_10HeapObjectEiELi256EEEPNS1_13PromotionListEPNS6_INS0_18EphemeronHashTableELi128EEEi:
.LFB22452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	movl	$256, %esi
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	112(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$96, %rdi
	subq	$24, %rsp
	movl	24(%rbp), %eax
	movq	16(%rbp), %rdx
	movq	%r9, -80(%rdi)
	movq	%r8, -64(%rdi)
	movl	%eax, -72(%rdi)
	movl	%eax, -56(%rdi)
	movq	%rdx, -48(%rdi)
	movl	%eax, -40(%rdi)
	movq	%r15, -32(%rdi)
	movq	$1, -24(%rdi)
	movq	$0, -16(%rdi)
	movq	$0, -8(%rdi)
	movl	$0x3f800000, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movups	%xmm0, -96(%rdi)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	cmpq	72(%rbx), %rax
	jbe	.L340
	movq	%rax, %r13
	cmpq	$1, %rax
	je	.L347
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r13
	ja	.L348
	leaq	0(,%r13,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
.L342:
	movq	%r15, 64(%rbx)
	movq	%r13, 72(%rbx)
.L340:
	pxor	%xmm0, %xmm0
	movq	%r12, 136(%rbx)
	leaq	16+_ZTVN2v88internal15CompactionSpaceE(%rip), %r13
	movups	%xmm0, 120(%rbx)
	movq	248(%r12), %rax
	movq	%rax, 144(%rbx)
	call	_ZN2v88internal8FreeList14CreateFreeListEv@PLT
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%rax, %r8
	leaq	160(%rbx), %rdi
	call	_ZN2v88internal10PagedSpaceC2EPNS0_4HeapENS0_15AllocationSpaceENS0_13ExecutabilityEPNS0_8FreeListE@PLT
	movq	%r13, 160(%rbx)
	call	_ZN2v88internal8FreeList14CreateFreeListEv@PLT
	leaq	392(%rbx), %rdi
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%rax, %r8
	movl	$3, %edx
	call	_ZN2v88internal10PagedSpaceC2EPNS0_4HeapENS0_15AllocationSpaceENS0_13ExecutabilityEPNS0_8FreeListE@PLT
	movq	%r13, 392(%rbx)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	624(%rbx), %rdi
	call	_ZN2v88internal21LocalAllocationBufferC1EPNS0_4HeapENS0_20LinearAllocationAreaE@PLT
	leaq	704(%rbx), %rax
	movb	$0, 648(%rbx)
	movq	%rax, 656(%rbx)
	leaq	760(%rbx), %rax
	movq	$1, 664(%rbx)
	movq	$0, 672(%rbx)
	movq	$0, 680(%rbx)
	movl	$0x3f800000, 688(%rbx)
	movq	$0, 696(%rbx)
	movq	$0, 704(%rbx)
	movq	%rax, 712(%rbx)
	movq	$1, 720(%rbx)
	movq	$0, 728(%rbx)
	movq	$0, 736(%rbx)
	movl	$0x3f800000, 744(%rbx)
	movq	$0, 752(%rbx)
	movq	$0, 760(%rbx)
	movq	2064(%r12), %rax
	movb	%r14b, 768(%rbx)
	cmpl	$1, 80(%rax)
	setg	769(%rbx)
	xorl	%edx, %edx
	cmpl	$1, 80(%rax)
	jle	.L344
	movzbl	84(%rax), %edx
.L344:
	movb	%dl, 770(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	movq	$0, 112(%rbx)
	jmp	.L342
.L348:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE22452:
	.size	_ZN2v88internal9ScavengerC2EPNS0_18ScavengerCollectorEPNS0_4HeapEbPNS0_8WorklistISt4pairINS0_10HeapObjectEiELi256EEEPNS1_13PromotionListEPNS6_INS0_18EphemeronHashTableELi128EEEi, .-_ZN2v88internal9ScavengerC2EPNS0_18ScavengerCollectorEPNS0_4HeapEbPNS0_8WorklistISt4pairINS0_10HeapObjectEiELi256EEEPNS1_13PromotionListEPNS6_INS0_18EphemeronHashTableELi128EEEi
	.globl	_ZN2v88internal9ScavengerC1EPNS0_18ScavengerCollectorEPNS0_4HeapEbPNS0_8WorklistISt4pairINS0_10HeapObjectEiELi256EEEPNS1_13PromotionListEPNS6_INS0_18EphemeronHashTableELi128EEEi
	.set	_ZN2v88internal9ScavengerC1EPNS0_18ScavengerCollectorEPNS0_4HeapEbPNS0_8WorklistISt4pairINS0_10HeapObjectEiELi256EEEPNS1_13PromotionListEPNS6_INS0_18EphemeronHashTableELi128EEEi,_ZN2v88internal9ScavengerC2EPNS0_18ScavengerCollectorEPNS0_4HeapEbPNS0_8WorklistISt4pairINS0_10HeapObjectEiELi256EEEPNS1_13PromotionListEPNS6_INS0_18EphemeronHashTableELi128EEEi
	.section	.text._ZN2v88internal9Scavenger27AddPageToSweeperIfNecessaryEPNS0_11MemoryChunkE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Scavenger27AddPageToSweeperIfNecessaryEPNS0_11MemoryChunkE
	.type	_ZN2v88internal9Scavenger27AddPageToSweeperIfNecessaryEPNS0_11MemoryChunkE, @function
_ZN2v88internal9Scavenger27AddPageToSweeperIfNecessaryEPNS0_11MemoryChunkE:
.LFB22492:
	.cfi_startproc
	endbr64
	testb	$32, 10(%rsi)
	je	.L356
.L349:
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	movq	80(%rsi), %rax
	cmpl	$2, 72(%rax)
	jne	.L349
	movq	168(%rsi), %rax
	testq	%rax, %rax
	je	.L349
	movq	8(%rdi), %rax
	movq	%rsi, %rdx
	movl	$1, %ecx
	movl	$2, %esi
	movq	2016(%rax), %rax
	movq	9984(%rax), %rdi
	jmp	_ZN2v88internal7Sweeper7AddPageENS0_15AllocationSpaceEPNS0_4PageENS1_11AddPageModeE@PLT
	.cfi_endproc
.LFE22492:
	.size	_ZN2v88internal9Scavenger27AddPageToSweeperIfNecessaryEPNS0_11MemoryChunkE, .-_ZN2v88internal9Scavenger27AddPageToSweeperIfNecessaryEPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal18ScavengerCollector20ClearYoungEphemeronsEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ScavengerCollector20ClearYoungEphemeronsEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE
	.type	_ZN2v88internal18ScavengerCollector20ClearYoungEphemeronsEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE, @function
_ZN2v88internal18ScavengerCollector20ClearYoungEphemeronsEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE:
.LFB22512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movl	688(%rsi), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L371
	.p2align 4,,10
	.p2align 3
.L372:
	movq	8(%r12), %r8
	xorl	%r14d, %r14d
	movq	8(%r8), %rcx
	testq	%rcx, %rcx
	je	.L370
	.p2align 4,,10
	.p2align 3
.L361:
	movq	16(%r8,%r14,8), %rax
	movq	%rax, -64(%rbp)
	movl	35(%rax), %r9d
	testl	%r9d, %r9d
	jle	.L364
	xorl	%r15d, %r15d
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L437:
	leaq	1(%rdi), %rax
.L368:
	movq	%rax, (%rcx)
	movq	-64(%rbp), %rax
	addq	$1, %r15
	cmpl	%r15d, 35(%rax)
	jle	.L435
.L369:
	movq	%r15, %rcx
	salq	$4, %rcx
	leaq	39(%rax,%rcx), %rcx
	movq	(%rcx), %rax
	movq	%rax, %rsi
	leaq	-1(%rax), %rdi
	andq	$-262144, %rsi
	movq	8(%rsi), %rsi
	testb	$8, %sil
	jne	.L436
.L365:
	movq	(%rdi), %rdi
	testb	$1, %dil
	je	.L437
	andl	$8, %esi
	movl	$0, %edx
	cmovne	%rdx, %rax
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L436:
	movq	-1(%rax), %r9
	andl	$1, %r9d
	je	.L365
	movl	%r15d, %esi
	leaq	-64(%rbp), %rdi
	movq	%r8, -72(%rbp)
	addq	$1, %r15
	call	_ZN2v88internal19ObjectHashTableBaseINS0_18EphemeronHashTableENS0_23EphemeronHashTableShapeEE11RemoveEntryEi@PLT
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %r8
	cmpl	%r15d, 35(%rax)
	jg	.L369
	.p2align 4,,10
	.p2align 3
.L435:
	movq	8(%r8), %rcx
.L364:
	addq	$1, %r14
	cmpq	%rcx, %r14
	jb	.L361
.L370:
	movq	(%r12), %rcx
	movq	8(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L362
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L363:
	movq	16(%rcx,%r14,8), %rax
	movq	%rax, -64(%rbp)
	movl	35(%rax), %edi
	testl	%edi, %edi
	jle	.L373
	xorl	%r15d, %r15d
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L440:
	leaq	1(%r8), %rax
.L377:
	movq	%rax, (%rsi)
	movq	-64(%rbp), %rax
	addq	$1, %r15
	cmpl	%r15d, 35(%rax)
	jle	.L438
.L378:
	movq	%r15, %rsi
	salq	$4, %rsi
	leaq	39(%rax,%rsi), %rsi
	movq	(%rsi), %rax
	movq	%rax, %rdi
	leaq	-1(%rax), %r8
	andq	$-262144, %rdi
	movq	8(%rdi), %rdi
	testb	$8, %dil
	jne	.L439
.L374:
	movq	(%r8), %r8
	testb	$1, %r8b
	je	.L440
	andl	$8, %edi
	movl	$0, %edx
	cmovne	%rdx, %rax
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L439:
	movq	-1(%rax), %r9
	andl	$1, %r9d
	je	.L374
	movl	%r15d, %esi
	leaq	-64(%rbp), %rdi
	movq	%rcx, -72(%rbp)
	addq	$1, %r15
	call	_ZN2v88internal19ObjectHashTableBaseINS0_18EphemeronHashTableENS0_23EphemeronHashTableShapeEE11RemoveEntryEi@PLT
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rcx
	cmpl	%r15d, 35(%rax)
	jg	.L378
	.p2align 4,,10
	.p2align 3
.L438:
	movq	8(%rcx), %rsi
.L373:
	addq	$1, %r14
	cmpq	%rsi, %r14
	jb	.L363
.L362:
	addl	$1, %r13d
	addq	$80, %r12
	cmpl	%r13d, 688(%rbx)
	jg	.L372
.L371:
	leaq	640(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%rbx), %r15
	testq	%r15, %r15
	je	.L359
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L360:
	movq	8(%r15), %rcx
	xorl	%r13d, %r13d
	testq	%rcx, %rcx
	je	.L388
	.p2align 4,,10
	.p2align 3
.L381:
	movq	16(%r15,%r13,8), %rax
	movq	%rax, -64(%rbp)
	movl	35(%rax), %edx
	testl	%edx, %edx
	jle	.L382
	xorl	%r14d, %r14d
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L443:
	leaq	1(%rdi), %rax
.L386:
	movq	%rax, (%rcx)
	movq	-64(%rbp), %rax
	addq	$1, %r14
	cmpl	%r14d, 35(%rax)
	jle	.L441
.L387:
	movq	%r14, %rcx
	salq	$4, %rcx
	leaq	39(%rax,%rcx), %rcx
	movq	(%rcx), %rax
	movq	%rax, %rsi
	leaq	-1(%rax), %rdi
	andq	$-262144, %rsi
	movq	8(%rsi), %rsi
	testb	$8, %sil
	jne	.L442
.L383:
	movq	(%rdi), %rdi
	testb	$1, %dil
	je	.L443
	andl	$8, %esi
	cmovne	%r12, %rax
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L442:
	movq	-1(%rax), %r8
	andl	$1, %r8d
	je	.L383
	movl	%r14d, %esi
	leaq	-64(%rbp), %rdi
	addq	$1, %r14
	call	_ZN2v88internal19ObjectHashTableBaseINS0_18EphemeronHashTableENS0_23EphemeronHashTableShapeEE11RemoveEntryEi@PLT
	movq	-64(%rbp), %rax
	cmpl	%r14d, 35(%rax)
	jg	.L387
	.p2align 4,,10
	.p2align 3
.L441:
	movq	8(%r15), %rcx
.L382:
	addq	$1, %r13
	cmpq	%rcx, %r13
	jb	.L381
.L388:
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L360
.L359:
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	688(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L379
	movq	8(%rbx), %rax
	movq	$0, 8(%rax)
	movq	(%rbx), %rax
	movq	$0, 8(%rax)
	cmpl	$1, 688(%rbx)
	jle	.L379
	movq	88(%rbx), %rax
	movq	$0, 8(%rax)
	movq	80(%rbx), %rax
	movq	$0, 8(%rax)
	cmpl	$2, 688(%rbx)
	jle	.L379
	movq	168(%rbx), %rax
	movq	$0, 8(%rax)
	movq	160(%rbx), %rax
	movq	$0, 8(%rax)
	cmpl	$3, 688(%rbx)
	jle	.L379
	movq	248(%rbx), %rax
	movq	$0, 8(%rax)
	movq	240(%rbx), %rax
	movq	$0, 8(%rax)
	cmpl	$4, 688(%rbx)
	jle	.L379
	movq	328(%rbx), %rax
	movq	$0, 8(%rax)
	movq	320(%rbx), %rax
	movq	$0, 8(%rax)
	cmpl	$5, 688(%rbx)
	jle	.L379
	movq	408(%rbx), %rax
	movq	$0, 8(%rax)
	movq	400(%rbx), %rax
	movq	$0, 8(%rax)
	cmpl	$6, 688(%rbx)
	jle	.L379
	movq	488(%rbx), %rax
	movq	$0, 8(%rax)
	movq	480(%rbx), %rax
	movq	$0, 8(%rax)
	cmpl	$7, 688(%rbx)
	jle	.L379
	movq	568(%rbx), %rax
	movq	$0, 8(%rax)
	movq	560(%rbx), %rax
	movq	$0, 8(%rax)
.L379:
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%rbx), %r12
	testq	%r12, %r12
	je	.L389
	.p2align 4,,10
	.p2align 3
.L390:
	movq	%r12, %rdi
	movq	(%r12), %r12
	movl	$1040, %esi
	call	_ZdlPvm@PLT
	testq	%r12, %r12
	jne	.L390
.L389:
	movq	$0, 680(%rbx)
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L444
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L444:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22512:
	.size	_ZN2v88internal18ScavengerCollector20ClearYoungEphemeronsEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE, .-_ZN2v88internal18ScavengerCollector20ClearYoungEphemeronsEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE
	.section	.text._ZN2v88internal9Scavenger21AddEphemeronHashTableENS0_18EphemeronHashTableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Scavenger21AddEphemeronHashTableENS0_18EphemeronHashTableE
	.type	_ZN2v88internal9Scavenger21AddEphemeronHashTableENS0_18EphemeronHashTableE, @function
_ZN2v88internal9Scavenger21AddEphemeronHashTableENS0_18EphemeronHashTableE:
.LFB22520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	56(%rdi), %rax
	movq	48(%rdi), %r14
	leaq	(%rax,%rax,4), %rbx
	salq	$4, %rbx
	addq	%r14, %rbx
	movq	(%rbx), %r13
	movq	8(%r13), %r12
	cmpq	$128, %r12
	je	.L454
	leaq	1(%r12), %rax
	movq	%rax, 8(%r13)
	movq	%rsi, 16(%r13,%r12,8)
.L445:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	.cfi_restore_state
	leaq	640(%r14), %r15
	movq	%rsi, -56(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r14), %rax
	movq	%r15, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 680(%r14)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movq	%r12, %rcx
	movq	-56(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%rbx)
	movq	8(%rdx), %rax
	cmpq	$128, %rax
	je	.L445
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%rsi, 16(%rdx,%rax,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22520:
	.size	_ZN2v88internal9Scavenger21AddEphemeronHashTableENS0_18EphemeronHashTableE, .-_ZN2v88internal9Scavenger21AddEphemeronHashTableENS0_18EphemeronHashTableE
	.section	.text._ZN2v88internal19RootScavengeVisitorC2EPNS0_9ScavengerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19RootScavengeVisitorC2EPNS0_9ScavengerE
	.type	_ZN2v88internal19RootScavengeVisitorC2EPNS0_9ScavengerE, @function
_ZN2v88internal19RootScavengeVisitorC2EPNS0_9ScavengerE:
.LFB22525:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal19RootScavengeVisitorE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE22525:
	.size	_ZN2v88internal19RootScavengeVisitorC2EPNS0_9ScavengerE, .-_ZN2v88internal19RootScavengeVisitorC2EPNS0_9ScavengerE
	.globl	_ZN2v88internal19RootScavengeVisitorC1EPNS0_9ScavengerE
	.set	_ZN2v88internal19RootScavengeVisitorC1EPNS0_9ScavengerE,_ZN2v88internal19RootScavengeVisitorC2EPNS0_9ScavengerE
	.section	.text._ZN2v88internal15ScavengeVisitorC2EPNS0_9ScavengerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ScavengeVisitorC2EPNS0_9ScavengerE
	.type	_ZN2v88internal15ScavengeVisitorC2EPNS0_9ScavengerE, @function
_ZN2v88internal15ScavengeVisitorC2EPNS0_9ScavengerE:
.LFB22542:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal15ScavengeVisitorE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE22542:
	.size	_ZN2v88internal15ScavengeVisitorC2EPNS0_9ScavengerE, .-_ZN2v88internal15ScavengeVisitorC2EPNS0_9ScavengerE
	.globl	_ZN2v88internal15ScavengeVisitorC1EPNS0_9ScavengerE
	.set	_ZN2v88internal15ScavengeVisitorC1EPNS0_9ScavengerE,_ZN2v88internal15ScavengeVisitorC2EPNS0_9ScavengerE
	.section	.rodata._ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EED2Ev.str1.1,"aMS",@progbits,1
.LC5:
	.string	"IsEmpty()"
	.section	.text._ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EED2Ev,"axG",@progbits,_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EED2Ev
	.type	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EED2Ev, @function
_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EED2Ev:
.LFB23219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	688(%rdi), %eax
	testl	%eax, %eax
	jle	.L458
	movq	8(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	movq	(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	cmpl	$1, %eax
	je	.L458
	movq	88(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	movq	80(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	cmpl	$2, %eax
	je	.L458
	movq	168(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	movq	160(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	cmpl	$3, %eax
	je	.L458
	movq	248(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	movq	240(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	cmpl	$4, %eax
	je	.L458
	movq	328(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	movq	320(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	cmpl	$5, %eax
	je	.L458
	movq	408(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	movq	400(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	cmpl	$6, %eax
	je	.L458
	movq	488(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	movq	480(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L459
	cmpl	$7, %eax
	je	.L458
	movq	568(%rdi), %rax
	cmpq	$0, 8(%rax)
	jne	.L459
	movq	560(%rdi), %rax
	cmpq	$0, 8(%rax)
	jne	.L459
.L458:
	movq	680(%r13), %rax
	testq	%rax, %rax
	jne	.L459
	movl	688(%r13), %eax
	testl	%eax, %eax
	jle	.L460
	movq	%r13, %rbx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L464:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L461
	movl	$4112, %esi
	call	_ZdlPvm@PLT
.L461:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L462
	movl	$4112, %esi
	addl	$1, %r12d
	addq	$80, %rbx
	call	_ZdlPvm@PLT
	cmpl	688(%r13), %r12d
	jl	.L464
.L460:
	addq	$8, %rsp
	leaq	640(%r13), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L462:
	addl	$1, %r12d
	addq	$80, %rbx
	cmpl	%r12d, 688(%r13)
	jg	.L464
	jmp	.L460
	.cfi_endproc
.LFE23219:
	.size	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EED2Ev, .-_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EED2Ev
	.weak	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EED1Ev
	.set	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EED1Ev,_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EED2Ev
	.section	.text._ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EED2Ev,"axG",@progbits,_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EED2Ev
	.type	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EED2Ev, @function
_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EED2Ev:
.LFB23228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	688(%rdi), %eax
	testl	%eax, %eax
	jle	.L492
	movq	8(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	movq	(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	cmpl	$1, %eax
	je	.L492
	movq	88(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	movq	80(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	cmpl	$2, %eax
	je	.L492
	movq	168(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	movq	160(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	cmpl	$3, %eax
	je	.L492
	movq	248(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	movq	240(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	cmpl	$4, %eax
	je	.L492
	movq	328(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	movq	320(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	cmpl	$5, %eax
	je	.L492
	movq	408(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	movq	400(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	cmpl	$6, %eax
	je	.L492
	movq	488(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	movq	480(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L493
	cmpl	$7, %eax
	je	.L492
	movq	568(%rdi), %rax
	cmpq	$0, 8(%rax)
	jne	.L493
	movq	560(%rdi), %rax
	cmpq	$0, 8(%rax)
	jne	.L493
.L492:
	movq	680(%r13), %rax
	testq	%rax, %rax
	jne	.L493
	movl	688(%r13), %eax
	testl	%eax, %eax
	jle	.L494
	movq	%r13, %rbx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L498:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L495
	movl	$112, %esi
	call	_ZdlPvm@PLT
.L495:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L496
	movl	$112, %esi
	addl	$1, %r12d
	addq	$80, %rbx
	call	_ZdlPvm@PLT
	cmpl	688(%r13), %r12d
	jl	.L498
.L494:
	addq	$8, %rsp
	leaq	640(%r13), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L496:
	addl	$1, %r12d
	addq	$80, %rbx
	cmpl	%r12d, 688(%r13)
	jg	.L498
	jmp	.L494
	.cfi_endproc
.LFE23228:
	.size	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EED2Ev, .-_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EED2Ev
	.weak	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EED1Ev
	.set	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EED1Ev,_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EED2Ev
	.section	.text._ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m,"axG",@progbits,_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m,comdat
	.p2align 4
	.weak	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	.type	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m, @function
_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m:
.LFB23534:
	.cfi_startproc
	endbr64
	cmpq	$15, %rdx
	ja	.L526
	leaq	15(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$30, %rax
	jbe	.L533
	leaq	-1(%rdx), %rax
	cmpq	$3, %rax
	jbe	.L533
	leaq	-2(%rdx), %rcx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	shrq	%rcx
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L529:
	movdqu	(%rsi,%rax), %xmm0
	addq	$1, %r8
	movups	%xmm0, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%r8, %rcx
	ja	.L529
	leaq	(%rcx,%rcx), %rax
	salq	$4, %rcx
	addq	%rcx, %rdi
	addq	%rsi, %rcx
	cmpq	%rax, %rdx
	je	.L525
	movq	(%rcx), %rax
	movq	%rax, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	salq	$3, %rdx
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L533:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L527:
	movq	(%rsi,%rax,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L527
	ret
	.cfi_endproc
.LFE23534:
	.size	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m, .-_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	.section	.rodata._ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm.str1.1,"aMS",@progbits,1
.LC6:
	.string	"NewArray"
	.section	.text._ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm,"axG",@progbits,_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm,comdat
	.p2align 4
	.weak	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	.type	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm, @function
_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm:
.LFB25050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	112(%rdi), %r12
	testq	%r12, %r12
	je	.L554
.L542:
	subq	%rbx, %rsi
	movl	%esi, %edx
	shrq	$18, %rsi
	andl	$262143, %edx
	leaq	(%rsi,%rsi,2), %rcx
	movl	%edx, %ebx
	movl	%edx, %r13d
	sarl	$13, %edx
	salq	$7, %rcx
	movslq	%edx, %rdx
	sarl	$8, %ebx
	leaq	(%rcx,%rdx,8), %rax
	sarl	$3, %r13d
	andl	$31, %ebx
	addq	%rax, %r12
	andl	$31, %r13d
	movq	(%r12), %r8
	testq	%r8, %r8
	je	.L555
.L544:
	movl	%r13d, %ecx
	movl	$1, %edx
	movslq	%ebx, %rbx
	sall	%cl, %edx
	leaq	(%r8,%rbx,4), %rcx
	movl	(%rcx), %eax
	testl	%eax, %edx
	je	.L547
.L540:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L556:
	.cfi_restore_state
	movl	%edx, %edi
	movl	%esi, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %esi
	je	.L540
.L547:
	movl	(%rcx), %esi
	movl	%edx, %eax
	andl	%esi, %eax
	cmpl	%eax, %edx
	jne	.L556
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L555:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L557
.L545:
	leaq	8(%r8), %rdi
	movq	%r8, %rcx
	xorl	%eax, %eax
	movq	$0, (%r8)
	andq	$-8, %rdi
	movq	$0, 120(%r8)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r8, (%r12)
	testq	%rax, %rax
	je	.L544
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	(%r12), %r8
	jmp	.L544
.L557:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L545
	leaq	.LC6(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE25050:
	.size	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm, .-_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	.section	.text._ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_,"axG",@progbits,_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_
	.type	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_, @function
_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_:
.LFB25062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	(%rsi,%rsi,4), %r12
	pushq	%rbx
	salq	$4, %r12
	addq	%rdi, %r12
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%r12), %rbx
	movq	8(%rbx), %rax
	cmpq	$256, %rax
	je	.L562
	addq	$1, %rax
	movq	%rax, 8(%rbx)
	salq	$4, %rax
	addq	%rbx, %rax
	movq	%rdx, (%rax)
	movl	%ecx, 8(%rax)
.L560:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	movq	%rdi, %r15
	leaq	640(%rdi), %rdi
	movq	%rdi, -56(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r15), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 680(%r15)
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$4112, %edi
	call	_Znwm@PLT
	movl	$512, %ecx
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%r12)
	movq	$1, 8(%rdx)
	movq	%r14, 16(%rdx)
	movl	%r13d, 24(%rdx)
	jmp	.L560
	.cfi_endproc
.LFE25062:
	.size	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_, .-_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_
	.section	.text._ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EE4PushEiS3_,"axG",@progbits,_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EE4PushEiS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EE4PushEiS3_
	.type	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EE4PushEiS3_, @function
_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EE4PushEiS3_:
.LFB25063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rbx), %r13
	movdqu	16(%rbp), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	32(%rbp), %edx
	movq	8(%r13), %rax
	movaps	%xmm0, -64(%rbp)
	movl	%edx, -48(%rbp)
	cmpq	$4, %rax
	je	.L570
	leaq	1(%rax), %rcx
	leaq	(%rax,%rax,2), %rax
	leaq	0(%r13,%rax,8), %rax
	movq	%rcx, 8(%r13)
	movl	%edx, 32(%rax)
	movups	%xmm0, 16(%rax)
.L567:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L571
	addq	$32, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	.cfi_restore_state
	leaq	640(%rdi), %r14
	movq	%rdi, %r12
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r12), %rax
	movq	%r14, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 680(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$112, %edi
	call	_Znwm@PLT
	movdqu	16(%rbp), %xmm1
	movl	32(%rbp), %edx
	pxor	%xmm0, %xmm0
	movq	%rax, (%rbx)
	movq	$1, 8(%rax)
	movl	%edx, 32(%rax)
	movups	%xmm0, 40(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 88(%rax)
	movups	%xmm1, 16(%rax)
	jmp	.L567
.L571:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25063:
	.size	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EE4PushEiS3_, .-_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EE4PushEiS3_
	.section	.text._ZN2v88internal8WorklistINS0_18EphemeronHashTableELi128EED2Ev,"axG",@progbits,_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi128EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi128EED2Ev
	.type	_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi128EED2Ev, @function
_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi128EED2Ev:
.LFB25115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	688(%rdi), %eax
	testl	%eax, %eax
	jle	.L573
	movq	8(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	movq	(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	cmpl	$1, %eax
	je	.L573
	movq	88(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	movq	80(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	cmpl	$2, %eax
	je	.L573
	movq	168(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	movq	160(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	cmpl	$3, %eax
	je	.L573
	movq	248(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	movq	240(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	cmpl	$4, %eax
	je	.L573
	movq	328(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	movq	320(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	cmpl	$5, %eax
	je	.L573
	movq	408(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	movq	400(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	cmpl	$6, %eax
	je	.L573
	movq	488(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	movq	480(%rdi), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L574
	cmpl	$7, %eax
	je	.L573
	movq	568(%rdi), %rax
	cmpq	$0, 8(%rax)
	jne	.L574
	movq	560(%rdi), %rax
	cmpq	$0, 8(%rax)
	jne	.L574
.L573:
	movq	680(%r13), %rax
	testq	%rax, %rax
	jne	.L574
	movl	688(%r13), %eax
	testl	%eax, %eax
	jle	.L575
	movq	%r13, %rbx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L579:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L576
	movl	$1040, %esi
	call	_ZdlPvm@PLT
.L576:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L577
	movl	$1040, %esi
	addl	$1, %r12d
	addq	$80, %rbx
	call	_ZdlPvm@PLT
	cmpl	688(%r13), %r12d
	jl	.L579
.L575:
	addq	$8, %rsp
	leaq	640(%r13), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L574:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L577:
	addl	$1, %r12d
	addq	$80, %rbx
	cmpl	%r12d, 688(%r13)
	jg	.L579
	jmp	.L575
	.cfi_endproc
.LFE25115:
	.size	_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi128EED2Ev, .-_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi128EED2Ev
	.weak	_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi128EED1Ev
	.set	_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi128EED1Ev,_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi128EED2Ev
	.section	.text._ZNSt8__detail9_Map_baseIN2v88internal14AllocationSiteESt4pairIKS3_mESaIS6_ENS_10_Select1stESt8equal_toIS3_ENS2_6Object6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS3_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN2v88internal14AllocationSiteESt4pairIKS3_mESaIS6_ENS_10_Select1stESt8equal_toIS3_ENS2_6Object6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN2v88internal14AllocationSiteESt4pairIKS3_mESaIS6_ENS_10_Select1stESt8equal_toIS3_ENS2_6Object6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS3_
	.type	_ZNSt8__detail9_Map_baseIN2v88internal14AllocationSiteESt4pairIKS3_mESaIS6_ENS_10_Select1stESt8equal_toIS3_ENS2_6Object6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS3_, @function
_ZNSt8__detail9_Map_baseIN2v88internal14AllocationSiteESt4pairIKS3_mESaIS6_ENS_10_Select1stESt8equal_toIS3_ENS2_6Object6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS3_:
.LFB26430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %r13
	movq	8(%rdi), %rdi
	movq	%r13, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L607
	movq	(%rax), %rcx
	movq	%rdx, %rsi
	movq	24(%rcx), %r8
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L608:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L607
	movq	24(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %rsi
	jne	.L607
.L610:
	cmpq	%r8, %r13
	jne	.L608
	cmpq	8(%rcx), %r13
	jne	.L608
	addq	$24, %rsp
	leaq	16(%rcx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	movl	$32, %edi
	call	_Znwm@PLT
	movq	24(%r12), %rdx
	movq	8(%r12), %rsi
	leaq	32(%r12), %rdi
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	(%r14), %rax
	movl	$1, %ecx
	movq	$0, 16(%rbx)
	movq	%rax, 8(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L611
	movq	(%r12), %r8
	movq	%r13, 24(%rbx)
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L621
.L645:
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	(%rax), %rax
	movq	%rbx, (%rax)
.L622:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	leaq	16(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L643
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L644
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L614:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L616
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L618:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L619:
	testq	%rsi, %rsi
	je	.L616
.L617:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	24(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L618
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L625
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L617
	.p2align 4,,10
	.p2align 3
.L616:
	movq	(%r12), %rdi
	cmpq	%r10, %rdi
	je	.L620
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L620:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r14, 8(%r12)
	divq	%r14
	movq	%r8, (%r12)
	movq	%r13, 24(%rbx)
	leaq	0(,%rdx,8), %r15
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L645
.L621:
	movq	16(%r12), %rdx
	movq	%rbx, 16(%r12)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L623
	movq	24(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
	movq	(%r12), %rax
	addq	%r15, %rax
.L623:
	leaq	16(%r12), %rdx
	movq	%rdx, (%rax)
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L625:
	movq	%rdx, %rdi
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L643:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L614
.L644:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE26430:
	.size	_ZNSt8__detail9_Map_baseIN2v88internal14AllocationSiteESt4pairIKS3_mESaIS6_ENS_10_Select1stESt8equal_toIS3_ENS2_6Object6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS3_, .-_ZNSt8__detail9_Map_baseIN2v88internal14AllocationSiteESt4pairIKS3_mESaIS6_ENS_10_Select1stESt8equal_toIS3_ENS2_6Object6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS3_
	.section	.text._ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE,"axG",@progbits,_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	.type	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE, @function
_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE:
.LFB20050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal32FLAG_allocation_site_pretenuringE(%rip)
	je	.L646
	movzwl	11(%rsi), %eax
	andl	$-5, %eax
	cmpw	$1057, %ax
	je	.L660
.L646:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L661
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	.cfi_restore_state
	leaq	-48(%rbp), %r14
	movq	%rdi, %r12
	leaq	-1(%rdx), %rbx
	movq	%rdx, -48(%rbp)
	movq	%r14, %rdi
	movq	%rcx, %r13
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	%rbx, %rcx
	cltq
	andq	$-262144, %rcx
	addq	%rbx, %rax
	leaq	8(%rax), %rdx
	andq	$-262144, %rdx
	cmpq	%rdx, %rcx
	jne	.L646
	movq	(%rax), %rdx
	cmpq	%rdx, -33600(%r12)
	jne	.L646
	testb	$8, 10(%rcx)
	jne	.L651
.L653:
	cmpq	$-1, %rax
	je	.L646
	movq	8(%rax), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNSt8__detail9_Map_baseIN2v88internal14AllocationSiteESt4pairIKS3_mESaIS6_ENS_10_Select1stESt8equal_toIS3_ENS2_6Object6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS3_
	addq	$1, (%rax)
	jmp	.L646
.L651:
	movq	80(%rcx), %rdx
	movq	128(%rdx), %rdx
	cmpq	40(%rcx), %rdx
	jb	.L646
	cmpq	48(%rcx), %rdx
	jnb	.L646
	cmpq	%rdx, %rbx
	jb	.L646
	jmp	.L653
.L661:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20050:
	.size	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE, .-_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	.section	.rodata._ZNSt6vectorIPN2v88internal15ItemParallelJob4ItemESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal15ItemParallelJob4ItemESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal15ItemParallelJob4ItemESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal15ItemParallelJob4ItemESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal15ItemParallelJob4ItemESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal15ItemParallelJob4ItemESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB26456:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L676
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L672
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L677
.L664:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L671:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L678
	testq	%r13, %r13
	jg	.L667
	testq	%r9, %r9
	jne	.L670
.L668:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L678:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L667
.L670:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L677:
	testq	%rsi, %rsi
	jne	.L665
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L667:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L668
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L672:
	movl	$8, %r14d
	jmp	.L664
.L676:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L665:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L664
	.cfi_endproc
.LFE26456:
	.size	_ZNSt6vectorIPN2v88internal15ItemParallelJob4ItemESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal15ItemParallelJob4ItemESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm,"axG",@progbits,_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm,comdat
	.p2align 4
	.weak	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	.type	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm, @function
_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm:
.LFB26723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	104(%rdi), %r12
	testq	%r12, %r12
	je	.L693
.L681:
	subq	%rbx, %rsi
	movl	%esi, %edx
	shrq	$18, %rsi
	andl	$262143, %edx
	leaq	(%rsi,%rsi,2), %rcx
	movl	%edx, %ebx
	movl	%edx, %r13d
	sarl	$13, %edx
	salq	$7, %rcx
	movslq	%edx, %rdx
	sarl	$8, %ebx
	leaq	(%rcx,%rdx,8), %rax
	sarl	$3, %r13d
	andl	$31, %ebx
	addq	%rax, %r12
	andl	$31, %r13d
	movq	(%r12), %r8
	testq	%r8, %r8
	je	.L694
.L683:
	movl	%r13d, %ecx
	movl	$1, %edx
	movslq	%ebx, %rbx
	sall	%cl, %edx
	leaq	(%r8,%rbx,4), %rcx
	movl	(%rcx), %eax
	testl	%eax, %edx
	je	.L686
.L679:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	.cfi_restore_state
	movl	%edx, %edi
	movl	%esi, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %esi
	je	.L679
.L686:
	movl	(%rcx), %esi
	movl	%edx, %eax
	andl	%esi, %eax
	cmpl	%eax, %edx
	jne	.L695
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L693:
	.cfi_restore_state
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L694:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L696
.L684:
	leaq	8(%r8), %rdi
	movq	%r8, %rcx
	xorl	%eax, %eax
	movq	$0, (%r8)
	andq	$-8, %rdi
	movq	$0, 120(%r8)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r8, (%r12)
	testq	%rax, %rax
	je	.L683
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	(%r12), %r8
	jmp	.L683
.L696:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L684
	leaq	.LC6(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE26723:
	.size	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm, .-_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	.section	.rodata._ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_
	.type	_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_, @function
_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_:
.LFB27085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L706
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L707
.L699:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	(%r12), %rdx
	movq	48(%rbx), %rax
	movq	%rdx, (%rax)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L707:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L708
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L709
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L704
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L704:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L702:
	movq	%r15, 40(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r13), %r14
	movq	(%r15), %xmm0
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L708:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L701
	cmpq	%r14, %rsi
	je	.L702
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L701:
	cmpq	%r14, %rsi
	je	.L702
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L702
.L706:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L709:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE27085:
	.size	_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_, .-_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal15ItemParallelJob4TaskESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal15ItemParallelJob4TaskESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal15ItemParallelJob4TaskESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal15ItemParallelJob4TaskESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal15ItemParallelJob4TaskESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB27660:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L734
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L725
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L735
.L712:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L724:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L714
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L718:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L715
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*8(%rcx)
	cmpq	%r14, %r15
	jne	.L718
.L716:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L714:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L719
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L727
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L721:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L721
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L722
.L720:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L722:
	leaq	8(%rcx,%r9), %rcx
.L719:
	testq	%r12, %r12
	je	.L723
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L723:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L718
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L735:
	testq	%rdi, %rdi
	jne	.L713
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L725:
	movl	$8, %r13d
	jmp	.L712
.L727:
	movq	%rcx, %rdx
	jmp	.L720
.L713:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L712
.L734:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE27660:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal15ItemParallelJob4TaskESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal15ItemParallelJob4TaskESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB27768:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L750
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L746
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L751
.L738:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L745:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L752
	testq	%r13, %r13
	jg	.L741
	testq	%r9, %r9
	jne	.L744
.L742:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L752:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L741
.L744:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L751:
	testq	%rsi, %rsi
	jne	.L739
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L741:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L742
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L746:
	movl	$8, %r14d
	jmp	.L738
.L750:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L739:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L738
	.cfi_endproc
.LFE27768:
	.size	_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt10_HashtableIN2v88internal18EphemeronHashTableESt4pairIKS2_St13unordered_setIiSt4hashIiESt8equal_toIiESaIiEEESaISC_ENSt8__detail10_Select1stES8_IS2_ENS1_6Object6HasherENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJSC_EEES3_INSE_14_Node_iteratorISC_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIN2v88internal18EphemeronHashTableESt4pairIKS2_St13unordered_setIiSt4hashIiESt8equal_toIiESaIiEEESaISC_ENSt8__detail10_Select1stES8_IS2_ENS1_6Object6HasherENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJSC_EEES3_INSE_14_Node_iteratorISC_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal18EphemeronHashTableESt4pairIKS2_St13unordered_setIiSt4hashIiESt8equal_toIiESaIiEEESaISC_ENSt8__detail10_Select1stES8_IS2_ENS1_6Object6HasherENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJSC_EEES3_INSE_14_Node_iteratorISC_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIN2v88internal18EphemeronHashTableESt4pairIKS2_St13unordered_setIiSt4hashIiESt8equal_toIiESaIiEEESaISC_ENSt8__detail10_Select1stES8_IS2_ENS1_6Object6HasherENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJSC_EEES3_INSE_14_Node_iteratorISC_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIN2v88internal18EphemeronHashTableESt4pairIKS2_St13unordered_setIiSt4hashIiESt8equal_toIiESaIiEEESaISC_ENSt8__detail10_Select1stES8_IS2_ENS1_6Object6HasherENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJSC_EEES3_INSE_14_Node_iteratorISC_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB27853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$80, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	_Znwm@PLT
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rsi
	leaq	56(%rbx), %rdi
	movq	$0, (%rax)
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	32(%rbx), %rdx
	movdqu	40(%rbx), %xmm0
	movq	%rcx, 16(%r12)
	movq	%rax, 8(%r12)
	movq	24(%rbx), %rax
	movq	%rsi, 24(%r12)
	movq	%rax, 32(%r12)
	movq	%rdx, 40(%r12)
	movq	$0, 64(%r12)
	movups	%xmm0, 48(%r12)
	cmpq	%rdi, %rcx
	je	.L806
.L754:
	testq	%rax, %rax
	je	.L755
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	%rsi
	leaq	32(%r12), %rax
	movq	%rax, (%rcx,%rdx,8)
.L755:
	movq	8(%r12), %r14
	movq	%rdi, 8(%rbx)
	xorl	%edx, %edx
	movq	$0, 48(%rbx)
	movq	$1, 16(%rbx)
	movq	%r14, %rax
	movq	$0, 56(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	8(%r13), %rsi
	divq	%rsi
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rdi
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L756
	movq	(%rax), %rbx
	movq	72(%rbx), %rcx
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L757:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L756
	movq	72(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %rdi
	jne	.L756
.L759:
	cmpq	%rcx, %r14
	jne	.L757
	cmpq	8(%rbx), %r14
	jne	.L757
	movq	32(%r12), %r13
	testq	%r13, %r13
	je	.L762
	.p2align 4,,10
	.p2align 3
.L763:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L763
.L762:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r12), %rdi
	leaq	64(%r12), %rax
	movq	$0, 40(%r12)
	movq	$0, 32(%r12)
	cmpq	%rax, %rdi
	je	.L760
	call	_ZdlPv@PLT
.L760:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L756:
	.cfi_restore_state
	movq	24(%r13), %rdx
	leaq	32(%r13), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rbx
	testb	%al, %al
	jne	.L765
	movq	0(%r13), %r8
.L766:
	leaq	(%r8,%r15), %rax
	movq	%r14, 72(%r12)
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L775
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rax
	movq	%r12, (%rax)
.L776:
	addq	$1, 24(%r13)
	addq	$24, %rsp
	movq	%r12, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L807
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L808
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r13), %r10
	movq	%rax, %r8
.L768:
	movq	16(%r13), %rsi
	movq	$0, 16(%r13)
	testq	%rsi, %rsi
	je	.L770
	xorl	%edi, %edi
	leaq	16(%r13), %r9
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L772:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L773:
	testq	%rsi, %rsi
	je	.L770
.L771:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	72(%rcx), %rax
	divq	%rbx
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L772
	movq	16(%r13), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r13)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L778
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L771
	.p2align 4,,10
	.p2align 3
.L770:
	movq	0(%r13), %rdi
	cmpq	%r10, %rdi
	je	.L774
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L774:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%rbx, 8(%r13)
	divq	%rbx
	movq	%r8, 0(%r13)
	leaq	0(,%rdx,8), %r15
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L775:
	movq	16(%r13), %rdx
	movq	%r12, 16(%r13)
	movq	%rdx, (%r12)
	testq	%rdx, %rdx
	je	.L777
	movq	72(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r13)
	movq	%r12, (%r8,%rdx,8)
	movq	0(%r13), %rax
	addq	%r15, %rax
.L777:
	leaq	16(%r13), %rdx
	movq	%rdx, (%rax)
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L778:
	movq	%rdx, %rdi
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L806:
	movq	56(%rbx), %rdx
	leaq	64(%r12), %rcx
	movq	%rcx, 16(%r12)
	movq	%rdx, 64(%r12)
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L807:
	leaq	48(%r13), %r8
	movq	$0, 48(%r13)
	movq	%r8, %r10
	jmp	.L768
.L808:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE27853:
	.size	_ZNSt10_HashtableIN2v88internal18EphemeronHashTableESt4pairIKS2_St13unordered_setIiSt4hashIiESt8equal_toIiESaIiEEESaISC_ENSt8__detail10_Select1stES8_IS2_ENS1_6Object6HasherENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJSC_EEES3_INSE_14_Node_iteratorISC_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIN2v88internal18EphemeronHashTableESt4pairIKS2_St13unordered_setIiSt4hashIiESt8equal_toIiESaIiEEESaISC_ENSt8__detail10_Select1stES8_IS2_ENS1_6Object6HasherENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJSC_EEES3_INSE_14_Node_iteratorISC_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE5eraseENS1_20_Node_const_iteratorIiLb1ELb0EEE,"axG",@progbits,_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE5eraseENS1_20_Node_const_iteratorIiLb1ELb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE5eraseENS1_20_Node_const_iteratorIiLb1ELb0EEE
	.type	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE5eraseENS1_20_Node_const_iteratorIiLb1ELb0EEE, @function
_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE5eraseENS1_20_Node_const_iteratorIiLb1ELb0EEE:
.LFB28000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movslq	8(%rdi), %rax
	movq	8(%rbx), %rsi
	movq	(%rbx), %r9
	divq	%rsi
	leaq	0(,%rdx,8), %r11
	movq	%rdx, %r8
	leaq	(%r9,%r11), %r10
	movq	(%r10), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L810:
	movq	%rdx, %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, %rdi
	jne	.L810
	movq	(%rdi), %r12
	cmpq	%rcx, %rax
	je	.L821
	testq	%r12, %r12
	je	.L813
	movslq	8(%r12), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r8
	je	.L813
	movq	%rcx, (%r9,%rdx,8)
	movq	(%rdi), %r12
.L813:
	movq	%r12, (%rcx)
	call	_ZdlPv@PLT
	movq	%r12, %rax
	subq	$1, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L821:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L815
	movslq	8(%r12), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r8
	je	.L813
	movq	%rcx, (%r9,%rdx,8)
	addq	(%rbx), %r11
	movq	(%r11), %rax
	movq	%r11, %r10
.L812:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L822
.L814:
	movq	$0, (%r10)
	movq	(%rdi), %r12
	jmp	.L813
.L815:
	movq	%rcx, %rax
	jmp	.L812
.L822:
	movq	%r12, 16(%rbx)
	jmp	.L814
	.cfi_endproc
.LFE28000:
	.size	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE5eraseENS1_20_Node_const_iteratorIiLb1ELb0EEE, .-_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE5eraseENS1_20_Node_const_iteratorIiLb1ELb0EEE
	.section	.text._ZN2v88internal18ScavengerCollector18ClearOldEphemeronsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ScavengerCollector18ClearOldEphemeronsEv
	.type	_ZN2v88internal18ScavengerCollector18ClearOldEphemeronsEv, @function
_ZN2v88internal18ScavengerCollector18ClearOldEphemeronsEv:
.LFB22514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	%rdi, -72(%rbp)
	movq	16(%rax), %r15
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testq	%r15, %r15
	je	.L823
	.p2align 4,,10
	.p2align 3
.L824:
	movq	8(%r15), %rax
	movq	32(%r15), %r14
	leaq	16(%r15), %r12
	movq	%rax, -64(%rbp)
	testq	%r14, %r14
	jne	.L825
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L870:
	leaq	1(%rdi), %rdx
	movq	%rdx, %rax
	movq	%rdx, (%rsi)
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L868
.L831:
	movq	(%r14), %r14
.L828:
	testq	%r14, %r14
	je	.L826
.L825:
	movl	8(%r14), %r8d
	movq	-64(%rbp), %rax
	leal	5(%r8,%r8), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	leaq	-1(%rdx,%rax), %rsi
	movq	(%rsi), %rdx
	movq	%rdx, %rax
	leaq	-1(%rdx), %rdi
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	testb	$8, %cl
	jne	.L869
.L827:
	movq	(%rdi), %rdi
	testb	$1, %dil
	je	.L870
	andl	$8, %ecx
	cmovne	%rbx, %rdx
	cmovne	%rbx, %rax
	movq	%rdx, (%rsi)
	testb	$24, 8(%rax)
	jne	.L831
.L868:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE5eraseENS1_20_Node_const_iteratorIiLb1ELb0EEE
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.L825
.L826:
	cmpq	$0, 40(%r15)
	movq	(%r15), %r12
	je	.L871
	movq	%r12, %r15
.L833:
	testq	%r12, %r12
	jne	.L824
.L823:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L872
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L869:
	.cfi_restore_state
	movq	-1(%rdx), %r9
	andl	$1, %r9d
	je	.L827
	movl	%r8d, %esi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_18EphemeronHashTableENS0_23EphemeronHashTableShapeEE11RemoveEntryEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE5eraseENS1_20_Node_const_iteratorIiLb1ELb0EEE
	movq	%rax, %r14
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L871:
	movq	-72(%rbp), %rax
	xorl	%edx, %edx
	movq	8(%rax), %r14
	movq	72(%r15), %rax
	movq	8(%r14), %rsi
	movq	(%r14), %r8
	divq	%rsi
	leaq	0(,%rdx,8), %r9
	movq	%rdx, %rdi
	leaq	(%r8,%r9), %r10
	movq	(%r10), %rdx
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L834:
	movq	%rax, %rcx
	movq	(%rax), %rax
	cmpq	%r15, %rax
	jne	.L834
	cmpq	%rcx, %rdx
	je	.L873
	testq	%r12, %r12
	je	.L837
	movq	72(%r12), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L837
	movq	%rcx, (%r8,%rdx,8)
	movq	(%r15), %r12
.L837:
	movq	%r12, (%rcx)
	movq	32(%r15), %r13
	testq	%r13, %r13
	je	.L842
	.p2align 4,,10
	.p2align 3
.L839:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L839
.L842:
	movq	24(%r15), %rax
	movq	16(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r15), %rdi
	leaq	64(%r15), %rax
	movq	$0, 40(%r15)
	movq	$0, 32(%r15)
	cmpq	%rax, %rdi
	je	.L840
	call	_ZdlPv@PLT
.L840:
	movq	%r15, %rdi
	movq	%r12, %r15
	call	_ZdlPv@PLT
	subq	$1, 24(%r14)
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L873:
	testq	%r12, %r12
	je	.L846
	movq	72(%r12), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L837
	movq	%rcx, (%r8,%rdx,8)
	movq	(%r14), %r10
	leaq	16(%r14), %rdx
	addq	%r9, %r10
	movq	(%r10), %rax
	cmpq	%rdx, %rax
	je	.L874
.L838:
	movq	$0, (%r10)
	movq	(%r15), %r12
	jmp	.L837
.L846:
	movq	%rcx, %rax
	leaq	16(%r14), %rdx
	cmpq	%rdx, %rax
	jne	.L838
.L874:
	movq	%r12, 16(%r14)
	jmp	.L838
.L872:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22514:
	.size	_ZN2v88internal18ScavengerCollector18ClearOldEphemeronsEv, .-_ZN2v88internal18ScavengerCollector18ClearOldEphemeronsEv
	.section	.text._ZN2v88internal18ScavengerCollector21ProcessWeakReferencesEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ScavengerCollector21ProcessWeakReferencesEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE
	.type	_ZN2v88internal18ScavengerCollector21ProcessWeakReferencesEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE, @function
_ZN2v88internal18ScavengerCollector21ProcessWeakReferencesEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE:
.LFB22501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$16, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal26ScavengeWeakObjectRetainerE(%rip), %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal4Heap26ProcessYoungWeakReferencesEPNS0_18WeakObjectRetainerE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18ScavengerCollector20ClearYoungEphemeronsEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE
	movq	%r12, %rdi
	call	_ZN2v88internal18ScavengerCollector18ClearOldEphemeronsEv
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L878
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L878:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22501:
	.size	_ZN2v88internal18ScavengerCollector21ProcessWeakReferencesEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE, .-_ZN2v88internal18ScavengerCollector21ProcessWeakReferencesEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE
	.section	.text._ZNSt10_HashtableIN2v88internal10HeapObjectESt4pairIKS2_NS1_3MapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal10HeapObjectESt4pairIKS2_NS1_3MapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal10HeapObjectESt4pairIKS2_NS1_3MapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm
	.type	_ZNSt10_HashtableIN2v88internal10HeapObjectESt4pairIKS2_NS1_3MapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm, @function
_ZNSt10_HashtableIN2v88internal10HeapObjectESt4pairIKS2_NS1_3MapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm:
.LFB28449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L903
.L880:
	movq	%r14, 24(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L889
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L890:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L903:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L904
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L905
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L882:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L884
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L886:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L887:
	testq	%rsi, %rsi
	je	.L884
.L885:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	24(%rcx), %rax
	divq	%r13
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L886
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L892
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L885
	.p2align 4,,10
	.p2align 3
.L884:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L888
	call	_ZdlPv@PLT
.L888:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r15, (%rbx)
	divq	%r13
	movq	%r13, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L889:
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L891
	movq	24(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L891:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L892:
	movq	%rdx, %rdi
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L904:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L882
.L905:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE28449:
	.size	_ZNSt10_HashtableIN2v88internal10HeapObjectESt4pairIKS2_NS1_3MapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm, .-_ZNSt10_HashtableIN2v88internal10HeapObjectESt4pairIKS2_NS1_3MapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm
	.section	.text._ZN2v88internal18ScavengerCollector29MergeSurvivingNewLargeObjectsERKSt13unordered_mapINS0_10HeapObjectENS0_3MapENS0_6Object6HasherESt8equal_toIS3_ESaISt4pairIKS3_S4_EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ScavengerCollector29MergeSurvivingNewLargeObjectsERKSt13unordered_mapINS0_10HeapObjectENS0_3MapENS0_6Object6HasherESt8equal_toIS3_ESaISt4pairIKS3_S4_EEE
	.type	_ZN2v88internal18ScavengerCollector29MergeSurvivingNewLargeObjectsERKSt13unordered_mapINS0_10HeapObjectENS0_3MapENS0_6Object6HasherESt8equal_toIS3_ESaISt4pairIKS3_S4_EEE, @function
_ZN2v88internal18ScavengerCollector29MergeSurvivingNewLargeObjectsERKSt13unordered_mapINS0_10HeapObjectENS0_3MapENS0_6Object6HasherESt8equal_toIS3_ESaISt4pairIKS3_S4_EEE:
.LFB22427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rsi), %r15
	testq	%r15, %r15
	je	.L906
	movq	%rdi, %rbx
	leaq	48(%rdi), %r13
	.p2align 4,,10
	.p2align 3
.L912:
	movq	8(%r15), %r12
	movq	16(%r15), %r14
	movl	$32, %edi
	call	_Znwm@PLT
	movq	56(%rbx), %r8
	xorl	%edx, %edx
	movq	%r12, %xmm0
	movq	%r14, %xmm1
	movq	$0, (%rax)
	movq	%rax, %rdi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rax)
	movq	%r12, %rax
	divq	%r8
	movq	48(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L908
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L909:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L908
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L908
.L911:
	cmpq	%rsi, %r12
	jne	.L909
	cmpq	8(%rcx), %r12
	jne	.L909
	call	_ZdlPv@PLT
.L914:
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L912
.L906:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L908:
	.cfi_restore_state
	movq	%rdi, %rcx
	movl	$1, %r8d
	movq	%r12, %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	call	_ZNSt10_HashtableIN2v88internal10HeapObjectESt4pairIKS2_NS1_3MapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm
	jmp	.L914
	.cfi_endproc
.LFE22427:
	.size	_ZN2v88internal18ScavengerCollector29MergeSurvivingNewLargeObjectsERKSt13unordered_mapINS0_10HeapObjectENS0_3MapENS0_6Object6HasherESt8equal_toIS3_ESaISt4pairIKS3_S4_EEE, .-_ZN2v88internal18ScavengerCollector29MergeSurvivingNewLargeObjectsERKSt13unordered_mapINS0_10HeapObjectENS0_3MapENS0_6Object6HasherESt8equal_toIS3_ESaISt4pairIKS3_S4_EEE
	.section	.text._ZNSt13unordered_mapIN2v88internal10HeapObjectENS1_3MapENS1_6Object6HasherESt8equal_toIS2_ESaISt4pairIKS2_S3_EEE6insertEOSA_,"axG",@progbits,_ZNSt13unordered_mapIN2v88internal10HeapObjectENS1_3MapENS1_6Object6HasherESt8equal_toIS2_ESaISt4pairIKS2_S3_EEE6insertEOSA_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt13unordered_mapIN2v88internal10HeapObjectENS1_3MapENS1_6Object6HasherESt8equal_toIS2_ESaISt4pairIKS2_S3_EEE6insertEOSA_
	.type	_ZNSt13unordered_mapIN2v88internal10HeapObjectENS1_3MapENS1_6Object6HasherESt8equal_toIS2_ESaISt4pairIKS2_S3_EEE6insertEOSA_, @function
_ZNSt13unordered_mapIN2v88internal10HeapObjectENS1_3MapENS1_6Object6HasherESt8equal_toIS2_ESaISt4pairIKS2_S3_EEE6insertEOSA_:
.LFB25076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$32, %edi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	_Znwm@PLT
	movq	(%r12), %r9
	movq	8(%r13), %rcx
	xorl	%edx, %edx
	movdqu	(%r12), %xmm0
	movq	$0, (%rax)
	movq	%rax, %rdi
	movups	%xmm0, 8(%rax)
	movq	%r9, %rax
	divq	%rcx
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L926
	movq	(%rax), %r12
	movq	24(%r12), %rsi
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L927:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L926
	movq	24(%r12), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L926
.L929:
	cmpq	%rsi, %r9
	jne	.L927
	cmpq	8(%r12), %r9
	jne	.L927
	call	_ZdlPv@PLT
	xorl	%eax, %eax
	xorl	%edx, %edx
	movb	%al, %dl
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L926:
	.cfi_restore_state
	movq	%rdi, %rcx
	movq	%r9, %rdx
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	%r10, %rsi
	call	_ZNSt10_HashtableIN2v88internal10HeapObjectESt4pairIKS2_NS1_3MapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm
	xorl	%edx, %edx
	movq	%rax, %r12
	movl	$1, %eax
	movb	%al, %dl
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25076:
	.size	_ZNSt13unordered_mapIN2v88internal10HeapObjectENS1_3MapENS1_6Object6HasherESt8equal_toIS2_ESaISt4pairIKS2_S3_EEE6insertEOSA_, .-_ZNSt13unordered_mapIN2v88internal10HeapObjectENS1_3MapENS1_6Object6HasherESt8equal_toIS2_ESaISt4pairIKS2_S3_EEE6insertEOSA_
	.section	.rodata._ZN2v88internal9Scavenger18EvacuateThinStringINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ThinStringEi.str1.1,"aMS",@progbits,1
.LC9:
	.string	"Scavenger: semi-space copy"
.LC10:
	.string	"unreachable code"
	.section	.text._ZN2v88internal9Scavenger18EvacuateThinStringINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ThinStringEi,"axG",@progbits,_ZN2v88internal9Scavenger18EvacuateThinStringINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ThinStringEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9Scavenger18EvacuateThinStringINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ThinStringEi
	.type	_ZN2v88internal9Scavenger18EvacuateThinStringINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ThinStringEi, @function
_ZN2v88internal9Scavenger18EvacuateThinStringINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ThinStringEi:
.LFB28010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 769(%rdi)
	je	.L1093
	leaq	-1(%rcx), %rax
	cmpb	$0, _ZN2v88internal35FLAG_young_generation_large_objectsE(%rip)
	movq	%rdi, %rbx
	movl	%r8d, %r13d
	movq	%rax, -104(%rbp)
	jne	.L1094
.L943:
	leaq	-1(%r14), %rsi
	leaq	136(%rbx), %rcx
	movq	%rsi, %rax
	movq	%rcx, -104(%rbp)
	andq	$-262144, %rax
	testb	$8, 10(%rax)
	jne	.L1095
.L944:
	xorl	%ecx, %ecx
	movl	$2, %edx
	movl	%r13d, %esi
	leaq	136(%rbx), %rdi
	call	_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r15
	testb	$1, %al
	je	.L945
	movq	-96(%rbp), %rcx
	leaq	-1(%rax), %rax
	movl	%r13d, %esi
	leal	-1(%r13), %edx
	movq	%rax, -112(%rbp)
	subl	$8, %esi
	leaq	7(%r15), %rdi
	movq	%rcx, -1(%r15)
	cmovns	%esi, %edx
	leaq	7(%r14), %rsi
	sarl	$3, %edx
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-96(%rbp), %rax
	leaq	-1(%r14), %rsi
	leaq	-1(%r15), %rdi
	lock cmpxchgq	%rdi, (%rsi)
	cmpq	-96(%rbp), %rax
	jne	.L1069
	cmpb	$0, 768(%rbx)
	jne	.L1096
.L948:
	cmpb	$0, 769(%rbx)
	jne	.L1097
.L997:
	movq	8(%rbx), %rdi
	movq	-96(%rbp), %rsi
	leaq	64(%rbx), %rcx
	movq	%r14, %rdx
	call	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	movq	(%r12), %rax
	movl	%r13d, %ecx
	movq	%r15, %rdx
	andl	$2, %eax
	orq	%r15, %rax
	movq	%rax, (%r12)
	movq	32(%rbx), %rdi
	xorl	%r12d, %r12d
	movl	40(%rbx), %esi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_
	movslq	%r13d, %r9
	addq	%r9, 120(%rbx)
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	8(%rbx), %rdx
	movq	248(%rdx), %rdx
	movq	496(%rdx), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1098
.L945:
	leaq	160(%rbx), %rdi
	movl	$2, %ecx
	xorl	%edx, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE
	movq	%rax, %r15
	testb	$1, %al
	je	.L966
	leaq	-1(%rax), %r10
	movq	-96(%rbp), %rax
	movl	%r13d, %ecx
	leal	-1(%r13), %edx
	movq	%r10, -104(%rbp)
	subl	$8, %ecx
	leaq	7(%r15), %rdi
	leaq	7(%r14), %rsi
	movq	%rax, -1(%r15)
	cmovns	%ecx, %edx
	sarl	$3, %edx
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %r10
	leaq	-1(%r14), %rdi
	lock cmpxchgq	%r10, (%rdi)
	cmpq	-96(%rbp), %rax
	jne	.L1099
	cmpb	$0, 768(%rbx)
	jne	.L1100
.L974:
	cmpb	$0, 769(%rbx)
	jne	.L1101
.L976:
	movq	8(%rbx), %rdi
	movq	-96(%rbp), %rsi
	leaq	64(%rbx), %rcx
	movq	%r14, %rdx
	call	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	movq	(%r12), %rax
	movl	%r13d, %ecx
	movq	%r15, %rdx
	andl	$2, %eax
	orq	%r15, %rax
	movq	%rax, (%r12)
	movl	24(%rbx), %esi
	movl	$1, %r12d
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_
	movslq	%r13d, %r9
	addq	%r9, 128(%rbx)
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1093:
	movq	(%rdx), %rax
	movl	$1, %r12d
	andl	$2, %eax
	orq	15(%rcx), %rax
	movq	%rax, (%rdx)
.L940:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1102
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1098:
	.cfi_restore_state
	cmpq	%rsi, 48(%rax)
	cmovbe	48(%rax), %rsi
	cmpq	%rdx, %rsi
	jb	.L945
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	264(%rbx), %rax
	testq	%rax, %rax
	je	.L971
	movslq	%r13d, %rdx
	subq	%rdx, %rax
	cmpq	%rax, %r10
	jne	.L971
	movq	%r10, 264(%rbx)
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1069:
	movq	-104(%rbp), %rdi
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal14LocalAllocator18FreeLastInNewSpaceENS0_10HeapObjectEi
.L1091:
	movq	-1(%r14), %rdx
	addq	$1, %rdx
	movq	(%r12), %rax
	andl	$2, %eax
	orq	%rdx, %rax
	movq	%rax, (%r12)
	movl	$1, %r12d
	testb	$1, %al
	je	.L940
	cmpl	$3, %eax
	je	.L940
	andq	$-262144, %rax
	movq	8(%rax), %r12
	shrq	$4, %r12
	xorq	$1, %r12
	andl	$1, %r12d
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L971:
	movq	136(%rbx), %rdi
	movl	$1, %r8d
	movl	%r13d, %edx
	movq	%r10, %rsi
	movl	$1, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L966:
	xorl	%ecx, %ecx
	movl	$2, %edx
	movl	%r13d, %esi
	leaq	136(%rbx), %rdi
	call	_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r15
	testb	$1, %al
	je	.L1103
	movq	-96(%rbp), %rcx
	leaq	-1(%rax), %rax
	movl	%r13d, %esi
	leal	-1(%r13), %edx
	movq	%rax, -112(%rbp)
	subl	$8, %esi
	leaq	7(%r15), %rdi
	movq	%rcx, -1(%r15)
	cmovns	%esi, %edx
	leaq	7(%r14), %rsi
	sarl	$3, %edx
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-96(%rbp), %rax
	leaq	-1(%r14), %rsi
	leaq	-1(%r15), %rdi
	lock cmpxchgq	%rdi, (%rsi)
	cmpq	-96(%rbp), %rax
	jne	.L1069
	cmpb	$0, 768(%rbx)
	jne	.L1104
.L995:
	cmpb	$0, 769(%rbx)
	je	.L997
	movl	%r15d, %eax
	movq	%r15, %r9
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r9
	movl	%eax, %ecx
	shrl	$8, %eax
	leaq	0(,%rax,4), %r8
	movq	16(%r9), %rax
	shrl	$3, %ecx
	sall	%cl, %edx
	addq	%r8, %rax
	movl	(%rax), %ecx
	testl	%edx, %ecx
	jne	.L998
.L1003:
	movl	%r14d, %eax
	movq	%r14, %rsi
	movl	$1, %edi
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	salq	$2, %rax
	sall	%cl, %edi
	movl	%edi, %ecx
	movq	16(%rsi), %rdi
	addq	%rax, %rdi
	movl	(%rdi), %r10d
	testl	%ecx, %r10d
	jne	.L1105
.L1000:
	addq	16(%rsi), %rax
	movl	(%rax), %esi
	testl	%esi, %ecx
	je	.L997
	addl	%ecx, %ecx
	jne	.L1006
	addq	$4, %rax
	movl	$1, %ecx
.L1006:
	movl	(%rax), %eax
	testl	%eax, %ecx
	je	.L997
	movq	16(%r9), %rsi
	addq	%r8, %rsi
	.p2align 4,,10
	.p2align 3
.L1007:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L997
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L1007
	movl	-112(%rbp), %eax
	movl	$1, %edx
	movq	%r15, -80(%rbp)
	subl	%r9d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r9), %rcx
	leaq	(%rcx,%rax,4), %rcx
	movl	(%rcx), %eax
	testl	%edx, %eax
	je	.L997
	addl	%edx, %edx
	jne	.L1011
	addq	$4, %rcx
	movl	$1, %edx
.L1011:
	movl	(%rcx), %esi
	movl	%edx, %eax
	andl	%esi, %eax
	cmpl	%eax, %edx
	je	.L997
	movl	%edx, %edi
	movl	%esi, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %esi
	jne	.L1011
	leaq	-80(%rbp), %rdi
	movq	%r9, -104(%rbp)
	call	_ZNK2v88internal10HeapObject4SizeEv
	movq	-104(%rbp), %r9
	cltq
	lock addq	%rax, 96(%r9)
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1094:
	movq	%rcx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$24, %al
	je	.L943
	testb	$32, %al
	je	.L943
	movq	%rsi, %rax
	leaq	-1(%rcx), %rcx
	lock cmpxchgq	%rcx, (%rcx)
	xorl	%r12d, %r12d
	cmpq	-96(%rbp), %rax
	jne	.L940
	movq	%rax, %xmm1
	movq	%r14, %xmm0
	leaq	-80(%rbp), %rsi
	punpcklqdq	%xmm1, %xmm0
	leaq	656(%rdi), %rdi
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZNSt13unordered_mapIN2v88internal10HeapObjectENS1_3MapENS1_6Object6HasherESt8equal_toIS2_ESaISt4pairIKS2_S3_EEE6insertEOSA_
	movdqa	-96(%rbp), %xmm0
	movslq	%r13d, %rax
	movl	24(%rbx), %esi
	addq	%rax, 128(%rbx)
	movq	16(%rbx), %rdi
	subq	$8, %rsp
	movaps	%xmm0, -80(%rbp)
	movl	%r13d, -64(%rbp)
	addq	$696, %rdi
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EE4PushEiS3_
	addq	$32, %rsp
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1101:
	movl	%r15d, %eax
	movq	%r15, %r9
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r9
	movl	%eax, %ecx
	shrl	$8, %eax
	leaq	0(,%rax,4), %r11
	movq	16(%r9), %rax
	shrl	$3, %ecx
	sall	%cl, %edx
	addq	%r11, %rax
	movl	(%rax), %ecx
	testl	%edx, %ecx
	jne	.L977
.L982:
	movl	%r14d, %eax
	movq	%r14, %rsi
	movl	$1, %edi
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	salq	$2, %rax
	sall	%cl, %edi
	movl	%edi, %ecx
	movq	16(%rsi), %rdi
	addq	%rax, %rdi
	movl	(%rdi), %r8d
	testl	%ecx, %r8d
	jne	.L1106
.L979:
	addq	16(%rsi), %rax
	movl	(%rax), %esi
	testl	%esi, %ecx
	je	.L976
	addl	%ecx, %ecx
	jne	.L985
	addq	$4, %rax
	movl	$1, %ecx
.L985:
	movl	(%rax), %eax
	testl	%ecx, %eax
	je	.L976
	movq	16(%r9), %rsi
	addq	%r11, %rsi
	.p2align 4,,10
	.p2align 3
.L986:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L976
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L986
	movl	%r10d, %eax
	movl	$1, %edx
	movq	%r15, -80(%rbp)
	subl	%r9d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r9), %rcx
	leaq	(%rcx,%rax,4), %rsi
	movl	(%rsi), %eax
	testl	%edx, %eax
	je	.L976
	addl	%edx, %edx
	jne	.L990
	addq	$4, %rsi
	movl	$1, %edx
.L990:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L976
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L990
	leaq	-80(%rbp), %rdi
	movq	%r9, -104(%rbp)
	call	_ZNK2v88internal10HeapObject4SizeEv
	movq	-104(%rbp), %r9
	cltq
	lock addq	%rax, 96(%r9)
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L1097:
	movl	%r15d, %eax
	movq	%r15, %r10
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r10
	movl	%eax, %ecx
	shrl	$8, %eax
	leaq	0(,%rax,4), %r8
	movq	16(%r10), %rax
	shrl	$3, %ecx
	sall	%cl, %edx
	addq	%r8, %rax
	movl	(%rax), %ecx
	testl	%edx, %ecx
	jne	.L951
.L956:
	movl	%r14d, %eax
	movq	%r14, %rsi
	movl	$1, %edi
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	salq	$2, %rax
	sall	%cl, %edi
	movl	%edi, %ecx
	movq	16(%rsi), %rdi
	addq	%rax, %rdi
	movl	(%rdi), %r9d
	testl	%ecx, %r9d
	jne	.L1107
.L953:
	addq	16(%rsi), %rax
	movl	(%rax), %esi
	testl	%esi, %ecx
	je	.L997
	addl	%ecx, %ecx
	jne	.L959
	addq	$4, %rax
	movl	$1, %ecx
.L959:
	movl	(%rax), %eax
	testl	%eax, %ecx
	je	.L997
	movq	16(%r10), %rsi
	addq	%r8, %rsi
	.p2align 4,,10
	.p2align 3
.L960:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L997
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L960
	movl	-112(%rbp), %eax
	movl	$1, %edx
	movq	%r15, -80(%rbp)
	subl	%r10d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r10), %rcx
	leaq	(%rcx,%rax,4), %rsi
	movl	(%rsi), %eax
	testl	%edx, %eax
	je	.L997
	addl	%edx, %edx
	jne	.L964
	addq	$4, %rsi
	movl	$1, %edx
.L964:
	movl	(%rsi), %ecx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%edx, %eax
	je	.L997
	movl	%ecx, %edi
	movl	%ecx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L964
	leaq	-80(%rbp), %rdi
	movq	%r10, -104(%rbp)
	call	_ZNK2v88internal10HeapObject4SizeEv
	movq	-104(%rbp), %r10
	cltq
	lock addq	%rax, 96(%r10)
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L977:
	movl	%edx, %ecx
	addl	%ecx, %ecx
	jne	.L980
	addq	$4, %rax
	movl	$1, %ecx
.L980:
	movl	(%rax), %eax
	testl	%ecx, %eax
	jne	.L976
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L951:
	movl	%edx, %ecx
	addl	%ecx, %ecx
	jne	.L954
	addq	$4, %rax
	movl	$1, %ecx
.L954:
	movl	(%rax), %eax
	testl	%ecx, %eax
	jne	.L997
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L1103:
	movq	8(%rbx), %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	8(%rbx), %rdi
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	movq	-104(%rbp), %r10
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L1106:
	movl	%ecx, %r8d
	addl	%r8d, %r8d
	jne	.L983
	addq	$4, %rdi
	movl	$1, %r8d
.L983:
	movl	(%rdi), %edi
	testl	%r8d, %edi
	jne	.L979
	movq	16(%r9), %rsi
	addq	%r11, %rsi
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1108:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L976
.L984:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1108
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L1096:
	movq	8(%rbx), %rdi
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L1107:
	movl	%ecx, %r9d
	addl	%r9d, %r9d
	jne	.L957
	addq	$4, %rdi
	movl	$1, %r9d
.L957:
	movl	(%rdi), %edi
	testl	%r9d, %edi
	jne	.L953
	movq	16(%r10), %rsi
	addq	%r8, %rsi
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L1109:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L997
.L958:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1109
	jmp	.L997
.L998:
	movl	%edx, %ecx
	addl	%ecx, %ecx
	jne	.L1001
	addq	$4, %rax
	movl	$1, %ecx
.L1001:
	movl	(%rax), %eax
	testl	%eax, %ecx
	jne	.L997
	jmp	.L1003
.L1104:
	movq	8(%rbx), %rdi
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	jmp	.L995
.L1105:
	movl	%ecx, %r10d
	addl	%r10d, %r10d
	jne	.L1004
	addq	$4, %rdi
	movl	$1, %r10d
.L1004:
	movl	(%rdi), %edi
	testl	%edi, %r10d
	jne	.L1000
	movq	16(%r9), %rsi
	addq	%r8, %rsi
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1110:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L997
.L1005:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1110
	jmp	.L997
.L1102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28010:
	.size	_ZN2v88internal9Scavenger18EvacuateThinStringINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ThinStringEi, .-_ZN2v88internal9Scavenger18EvacuateThinStringINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ThinStringEi
	.section	.text._ZN2v88internal9Scavenger25EvacuateShortcutCandidateINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ConsStringEi,"axG",@progbits,_ZN2v88internal9Scavenger25EvacuateShortcutCandidateINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ConsStringEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9Scavenger25EvacuateShortcutCandidateINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ConsStringEi
	.type	_ZN2v88internal9Scavenger25EvacuateShortcutCandidateINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ConsStringEi, @function
_ZN2v88internal9Scavenger25EvacuateShortcutCandidateINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ConsStringEi:
.LFB28011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movslq	%r8d, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%rsi, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 769(%rdi)
	je	.L1418
.L1112:
	leaq	-1(%r13), %rax
	cmpb	$0, _ZN2v88internal35FLAG_young_generation_large_objectsE(%rip)
	movq	%rax, -176(%rbp)
	jne	.L1419
.L1116:
	leaq	-1(%r13), %rsi
	leaq	136(%rbx), %r14
	movq	%rsi, %rax
	andq	$-262144, %rax
	testb	$8, 10(%rax)
	jne	.L1420
.L1195:
	xorl	%ecx, %ecx
	movl	$2, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r15
	testb	$1, %al
	je	.L1196
	movq	-144(%rbp), %rdi
	leaq	-1(%rax), %rax
	movl	%r12d, %esi
	leal	-1(%r12), %edx
	movq	%rax, -176(%rbp)
	subl	$8, %esi
	movq	%rdi, -1(%r15)
	cmovns	%esi, %edx
	leaq	7(%r15), %rdi
	leaq	7(%r13), %rsi
	sarl	$3, %edx
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-144(%rbp), %rax
	leaq	-1(%r13), %rsi
	leaq	-1(%r15), %rcx
	lock cmpxchgq	%rcx, (%rsi)
	cmpq	%rax, -144(%rbp)
	jne	.L1375
	cmpb	$0, 768(%rbx)
	jne	.L1421
.L1199:
	cmpb	$0, 769(%rbx)
	jne	.L1422
.L1201:
	movq	8(%rbx), %rdi
	movq	-144(%rbp), %rsi
	leaq	64(%rbx), %rcx
	movq	%r13, %rdx
	call	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	movq	-152(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%rax, -144(%rbp)
	andl	$2, %eax
	orq	%r15, %rax
	movq	%rax, (%rdi)
.L1406:
	movq	32(%rbx), %rdi
	movl	40(%rbx), %esi
	movl	%r12d, %ecx
	movq	%r15, %rdx
	xorl	%r14d, %r14d
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_
	addq	%r12, 120(%rbx)
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1420:
	movq	8(%rbx), %rdx
	movq	248(%rdx), %rdx
	movq	496(%rdx), %rdx
	cmpq	40(%rax), %rdx
	jb	.L1196
	cmpq	%rsi, 48(%rax)
	cmovbe	48(%rax), %rsi
	cmpq	%rdx, %rsi
	jnb	.L1195
.L1196:
	leaq	160(%rbx), %rdi
	movl	$2, %ecx
	xorl	%edx, %edx
	movl	%r12d, %esi
	call	_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE
	movq	%rax, %r15
	testb	$1, %al
	je	.L1217
	leaq	-1(%rax), %r14
	movq	-144(%rbp), %rax
	movl	%r12d, %ecx
	leal	-1(%r12), %edx
	subl	$8, %ecx
	leaq	7(%r13), %rsi
	leaq	7(%r15), %rdi
	movq	%rax, -1(%r15)
	cmovns	%ecx, %edx
	sarl	$3, %edx
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-144(%rbp), %rax
	leaq	-1(%r13), %rcx
	lock cmpxchgq	%r14, (%rcx)
	cmpq	%rax, -144(%rbp)
	jne	.L1423
	cmpb	$0, 768(%rbx)
	jne	.L1424
.L1225:
	cmpb	$0, 769(%rbx)
	jne	.L1425
.L1227:
	movq	8(%rbx), %rdi
	leaq	64(%rbx), %rcx
	movq	%r13, %rdx
	movl	$1, %r14d
	movq	-144(%rbp), %rsi
	call	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	movq	-152(%rbp), %rcx
	movq	%r15, %rdx
	movq	(%rcx), %rax
	movq	%rax, -144(%rbp)
	andl	$2, %eax
	orq	%r15, %rax
	movq	%rax, (%rcx)
	movl	24(%rbx), %esi
	movl	%r12d, %ecx
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_
	addq	%r12, 128(%rbx)
.L1111:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1426
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1418:
	.cfi_restore_state
	movq	8(%rdi), %rdx
	movq	23(%rcx), %rax
	cmpq	%rax, -37464(%rdx)
	jne	.L1112
	movq	15(%rcx), %rdx
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, -144(%rbp)
	andl	$2, %eax
	orq	%rdx, %rax
	movq	%rax, (%rcx)
	movq	%rdx, %rax
	leaq	-1(%rdx), %rcx
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1427
	movq	-1(%rdx), %r12
	testb	$1, %r12b
	je	.L1428
	movzbl	10(%r12), %eax
	leaq	-120(%rbp), %rdi
	movq	%r12, %rsi
	movb	%al, -144(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-120(%rbp), %r14
	cmpb	$0, _ZN2v88internal35FLAG_young_generation_large_objectsE(%rip)
	movl	%eax, %r15d
	leaq	-1(%r14), %rax
	movq	%rax, -176(%rbp)
	jne	.L1429
.L1120:
	leaq	-1(%r14), %rdi
	leaq	136(%rbx), %rsi
	movq	%rdi, %rax
	movq	%rsi, -160(%rbp)
	andq	$-262144, %rax
	testb	$8, 10(%rax)
	je	.L1124
	movq	8(%rbx), %rdx
	movq	248(%rdx), %rdx
	movq	496(%rdx), %rdx
	cmpq	40(%rax), %rdx
	jb	.L1125
	cmpq	%rdi, 48(%rax)
	cmovbe	48(%rax), %rdi
	cmpq	%rdi, %rdx
	jbe	.L1124
.L1125:
	leaq	160(%rbx), %rdi
	movl	$2, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	call	_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE
	movq	%rax, %r10
	testb	$1, %al
	je	.L1147
	leaq	-1(%rax), %rax
	leal	-1(%r15), %edx
	movq	%rax, -184(%rbp)
	movl	%r15d, %eax
	leaq	7(%r14), %rsi
	leaq	7(%r10), %rdi
	movq	%r12, -1(%r10)
	subl	$8, %eax
	movq	%r10, -192(%rbp)
	cmovns	%eax, %edx
	sarl	$3, %edx
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-184(%rbp), %rdi
	leaq	-1(%r14), %rsi
	movq	%r12, %rax
	lock cmpxchgq	%rdi, (%rsi)
	movq	-192(%rbp), %r10
	cmpq	%rax, %r12
	jne	.L1430
	cmpb	$0, 768(%rbx)
	jne	.L1431
.L1153:
	cmpb	$0, 769(%rbx)
	jne	.L1432
.L1155:
	movq	8(%rbx), %rdi
	leaq	64(%rbx), %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, -160(%rbp)
	call	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	movq	-152(%rbp), %rdi
	movq	-160(%rbp), %r10
	movq	(%rdi), %rax
	movq	%rax, -176(%rbp)
	andl	$2, %eax
	orq	%r10, %rax
	cmpb	$5, -144(%rbp)
	movq	%rax, (%rdi)
	ja	.L1433
.L1171:
	movslq	%r15d, %rax
	addq	%rax, 128(%rbx)
	movl	$1, %r14d
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
.L1122:
	subq	$1, %rax
	movq	%rax, -1(%r13)
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1425:
	movq	%r15, %r9
	movl	%r15d, %r10d
	movl	$1, %edx
	andl	$262143, %r10d
	andq	$-262144, %r9
	movq	16(%r9), %rax
	movl	%r10d, %ecx
	shrl	$8, %r10d
	salq	$2, %r10
	shrl	$3, %ecx
	addq	%r10, %rax
	sall	%cl, %edx
	movl	(%rax), %ecx
	testl	%edx, %ecx
	jne	.L1228
.L1233:
	movl	%r13d, %eax
	movq	%r13, %rsi
	movl	$1, %edi
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	salq	$2, %rax
	sall	%cl, %edi
	movl	%edi, %ecx
	movq	16(%rsi), %rdi
	addq	%rax, %rdi
	movl	(%rdi), %r8d
	testl	%ecx, %r8d
	jne	.L1434
.L1230:
	addq	16(%rsi), %rax
	movl	(%rax), %esi
	testl	%esi, %ecx
	je	.L1227
	addl	%ecx, %ecx
	jne	.L1236
	addq	$4, %rax
	movl	$1, %ecx
.L1236:
	movl	(%rax), %eax
	testl	%eax, %ecx
	je	.L1227
	addq	16(%r9), %r10
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1436:
	movl	%edx, %esi
	movl	%ecx, %eax
	orl	%ecx, %esi
	lock cmpxchgl	%esi, (%r10)
	cmpl	%eax, %ecx
	je	.L1435
.L1237:
	movl	(%r10), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1436
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1422:
	movq	%r15, %r14
	movl	%r15d, %r11d
	movl	$1, %edx
	andl	$262143, %r11d
	andq	$-262144, %r14
	movq	16(%r14), %rax
	movl	%r11d, %ecx
	shrl	$8, %r11d
	salq	$2, %r11
	shrl	$3, %ecx
	addq	%r11, %rax
	sall	%cl, %edx
	movl	(%rax), %ecx
	testl	%edx, %ecx
	jne	.L1202
.L1207:
	movl	%r13d, %eax
	movq	%r13, %rsi
	movl	$1, %edi
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	salq	$2, %rax
	sall	%cl, %edi
	movl	%edi, %ecx
	movq	16(%rsi), %rdi
	addq	%rax, %rdi
	movl	(%rdi), %r10d
	testl	%ecx, %r10d
	jne	.L1437
.L1204:
	addq	16(%rsi), %rax
	movl	(%rax), %esi
	testl	%esi, %ecx
	je	.L1201
	addl	%ecx, %ecx
	jne	.L1210
	addq	$4, %rax
	movl	$1, %ecx
.L1210:
	movl	(%rax), %eax
	testl	%ecx, %eax
	je	.L1201
	addq	16(%r14), %r11
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1439:
	movl	%edx, %esi
	movl	%ecx, %eax
	orl	%ecx, %esi
	lock cmpxchgl	%esi, (%r11)
	cmpl	%eax, %ecx
	je	.L1438
.L1211:
	movl	(%r11), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1439
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1217:
	xorl	%ecx, %ecx
	movl	$2, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r15
	testb	$1, %al
	je	.L1220
	movq	-144(%rbp), %rsi
	leaq	-1(%rax), %rax
	movl	%r12d, %edi
	leal	-1(%r12), %edx
	movq	%rax, -176(%rbp)
	subl	$8, %edi
	movq	%rsi, -1(%r15)
	cmovns	%edi, %edx
	leaq	7(%r13), %rsi
	leaq	7(%r15), %rdi
	sarl	$3, %edx
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-144(%rbp), %rax
	leaq	-1(%r13), %rdi
	leaq	-1(%r15), %rcx
	lock cmpxchgq	%rcx, (%rdi)
	cmpq	%rax, -144(%rbp)
	jne	.L1375
	cmpb	$0, 768(%rbx)
	jne	.L1440
.L1246:
	cmpb	$0, 769(%rbx)
	jne	.L1441
.L1248:
	movq	8(%rbx), %rdi
	movq	-144(%rbp), %rsi
	leaq	64(%rbx), %rcx
	movq	%r13, %rdx
	call	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rax
	movq	%rax, -144(%rbp)
	andl	$2, %eax
	orq	%r15, %rax
	movq	%rax, (%rcx)
	jmp	.L1406
	.p2align 4,,10
	.p2align 3
.L1375:
	movl	%r12d, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14LocalAllocator18FreeLastInNewSpaceENS0_10HeapObjectEi
.L1416:
	movq	-1(%r13), %rdx
	movl	$1, %r14d
	addq	$1, %rdx
	movq	-152(%rbp), %rbx
	movq	(%rbx), %rax
	movq	%rax, -144(%rbp)
	andl	$2, %eax
	orq	%rdx, %rax
	movq	%rax, (%rbx)
	testb	$1, %al
	je	.L1111
	cmpl	$3, %eax
	je	.L1111
	andq	$-262144, %rax
	movq	8(%rax), %r14
	shrq	$4, %r14
	xorq	$1, %r14
	andl	$1, %r14d
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1423:
	movq	264(%rbx), %rax
	testq	%rax, %rax
	je	.L1222
	movslq	%r12d, %rdx
	subq	%rdx, %rax
	cmpq	%rax, %r14
	je	.L1442
.L1222:
	movq	136(%rbx), %rdi
	movl	$1, %r8d
	movl	%r12d, %edx
	movq	%r14, %rsi
	movl	$1, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	%r13, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$24, %al
	je	.L1116
	testb	$32, %al
	je	.L1116
	movq	-144(%rbp), %rax
	leaq	-1(%r13), %rdi
	lock cmpxchgq	%rdi, (%rdi)
	xorl	%r14d, %r14d
	cmpq	%rax, -144(%rbp)
	jne	.L1111
	movq	%r13, %xmm0
	leaq	-80(%rbp), %rsi
	leaq	656(%rbx), %rdi
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	_ZNSt13unordered_mapIN2v88internal10HeapObjectENS1_3MapENS1_6Object6HasherESt8equal_toIS2_ESaISt4pairIKS2_S3_EEE6insertEOSA_
	movslq	%r12d, %rax
	movq	16(%rbx), %rdi
	subq	$8, %rsp
	movdqa	-144(%rbp), %xmm0
	movl	24(%rbx), %esi
	movl	%r12d, -64(%rbp)
	addq	%rax, 128(%rbx)
	addq	$696, %rdi
	pushq	-64(%rbp)
	movaps	%xmm0, -80(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EE4PushEiS3_
	addq	$32, %rsp
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1220:
	movq	8(%rbx), %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1428:
	movq	-152(%rbp), %rbx
	leaq	1(%r12), %rdx
	xorl	%r14d, %r14d
	movq	(%rbx), %rax
	movq	%rax, -144(%rbp)
	andl	$2, %eax
	orq	%rdx, %rax
	andq	$-262144, %rdx
	movq	%rax, (%rbx)
	movq	%r12, -1(%r13)
	testb	$24, 8(%rdx)
	sete	%r14b
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1202:
	movl	%edx, %ecx
	addl	%ecx, %ecx
	jne	.L1205
	addq	$4, %rax
	movl	$1, %ecx
.L1205:
	movl	(%rax), %eax
	testl	%ecx, %eax
	jne	.L1201
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1434:
	movl	%ecx, %r8d
	addl	%r8d, %r8d
	jne	.L1234
	addq	$4, %rdi
	movl	$1, %r8d
.L1234:
	movl	(%rdi), %edi
	testl	%edi, %r8d
	jne	.L1230
	addq	16(%r9), %r10
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1443:
	movl	%edx, %esi
	movl	%ecx, %eax
	orl	%ecx, %esi
	lock cmpxchgl	%esi, (%r10)
	cmpl	%eax, %ecx
	je	.L1227
.L1235:
	movl	(%r10), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1443
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1421:
	movq	8(%rbx), %rdi
	movl	%r12d, %ecx
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1437:
	movl	%ecx, %r10d
	addl	%r10d, %r10d
	jne	.L1208
	addq	$4, %rdi
	movl	$1, %r10d
.L1208:
	movl	(%rdi), %edi
	testl	%r10d, %edi
	jne	.L1204
	addq	16(%r14), %r11
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1444:
	movl	%edx, %esi
	movl	%ecx, %eax
	orl	%ecx, %esi
	lock cmpxchgl	%esi, (%r11)
	cmpl	%eax, %ecx
	je	.L1201
.L1209:
	movl	(%r11), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1444
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1442:
	movq	%r14, 264(%rbx)
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1427:
	movl	$1, %r14d
	movq	%rcx, -1(%r13)
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	%r15, %r14
	movl	%r15d, %r11d
	movl	$1, %edx
	andl	$262143, %r11d
	andq	$-262144, %r14
	movq	16(%r14), %rax
	movl	%r11d, %ecx
	shrl	$8, %r11d
	salq	$2, %r11
	shrl	$3, %ecx
	addq	%r11, %rax
	sall	%cl, %edx
	movl	(%rax), %ecx
	testl	%edx, %ecx
	jne	.L1249
.L1254:
	movl	%r13d, %eax
	movq	%r13, %rsi
	movl	$1, %edi
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	salq	$2, %rax
	sall	%cl, %edi
	movl	%edi, %ecx
	movq	16(%rsi), %rdi
	addq	%rax, %rdi
	movl	(%rdi), %r10d
	testl	%ecx, %r10d
	jne	.L1445
.L1251:
	addq	16(%rsi), %rax
	movl	(%rax), %esi
	testl	%esi, %ecx
	je	.L1248
	addl	%ecx, %ecx
	jne	.L1257
	addq	$4, %rax
	movl	$1, %ecx
.L1257:
	movl	(%rax), %eax
	testl	%eax, %ecx
	je	.L1248
	addq	16(%r14), %r11
	.p2align 4,,10
	.p2align 3
.L1258:
	movl	(%r11), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L1248
	movl	%edx, %esi
	movl	%ecx, %eax
	orl	%ecx, %esi
	lock cmpxchgl	%esi, (%r11)
	cmpl	%eax, %ecx
	jne	.L1258
	movl	-176(%rbp), %eax
	movl	$1, %edx
	movq	%r15, -80(%rbp)
	subl	%r14d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r14), %rcx
	leaq	(%rcx,%rax,4), %rsi
	movl	(%rsi), %eax
	testl	%edx, %eax
	je	.L1248
	addl	%edx, %edx
	jne	.L1262
	addq	$4, %rsi
	movl	$1, %edx
.L1262:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L1248
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L1262
	leaq	-80(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject4SizeEv
	cltq
	lock addq	%rax, 96(%r14)
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1228:
	movl	%edx, %ecx
	addl	%ecx, %ecx
	jne	.L1231
	addq	$4, %rax
	movl	$1, %ecx
.L1231:
	movl	(%rax), %eax
	testl	%eax, %ecx
	jne	.L1227
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1424:
	movq	8(%rbx), %rdi
	movl	%r12d, %ecx
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1124:
	xorl	%ecx, %ecx
	movl	$2, %edx
	movl	%r15d, %esi
	leaq	136(%rbx), %rdi
	call	_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r10
	testb	$1, %al
	je	.L1125
	leaq	-1(%rax), %rax
	leal	-1(%r15), %edx
	movq	%rax, -184(%rbp)
	movl	%r15d, %eax
	leaq	7(%r14), %rsi
	leaq	7(%r10), %rdi
	movq	%r12, -1(%r10)
	subl	$8, %eax
	movq	%r10, -192(%rbp)
	cmovns	%eax, %edx
	sarl	$3, %edx
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-184(%rbp), %rdi
	leaq	-1(%r14), %rsi
	movq	%r12, %rax
	lock cmpxchgq	%rdi, (%rsi)
	movq	-192(%rbp), %r10
	cmpq	%rax, %r12
	jne	.L1446
	cmpb	$0, 768(%rbx)
	jne	.L1447
.L1128:
	cmpb	$0, 769(%rbx)
	jne	.L1448
.L1130:
	movq	8(%rbx), %rdi
	leaq	64(%rbx), %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, -160(%rbp)
	call	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	movq	-152(%rbp), %rdi
	movq	-160(%rbp), %r10
	movq	(%rdi), %rax
	movq	%rax, -176(%rbp)
	andl	$2, %eax
	orq	%r10, %rax
	cmpb	$5, -144(%rbp)
	movq	%rax, (%rdi)
	ja	.L1449
.L1146:
	movslq	%r15d, %rax
	addq	%rax, 120(%rbx)
.L1403:
	movq	-152(%rbp), %rax
	xorl	%r14d, %r14d
	movq	(%rax), %rax
	jmp	.L1122
.L1249:
	movl	%edx, %ecx
	addl	%ecx, %ecx
	jne	.L1252
	addq	$4, %rax
	movl	$1, %ecx
.L1252:
	movl	(%rax), %eax
	testl	%eax, %ecx
	jne	.L1248
	jmp	.L1254
.L1440:
	movq	8(%rbx), %rdi
	movl	%r12d, %ecx
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	jmp	.L1246
.L1147:
	xorl	%ecx, %ecx
	movl	$2, %edx
	movl	%r15d, %esi
	leaq	136(%rbx), %rdi
	call	_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r10
	testb	$1, %al
	je	.L1220
	leaq	-1(%rax), %rax
	leal	-1(%r15), %edx
	movq	%rax, -184(%rbp)
	movl	%r15d, %eax
	leaq	7(%r14), %rsi
	leaq	7(%r10), %rdi
	movq	%r12, -1(%r10)
	subl	$8, %eax
	movq	%r10, -192(%rbp)
	cmovns	%eax, %edx
	sarl	$3, %edx
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-184(%rbp), %rsi
	leaq	-1(%r14), %rdi
	movq	%r12, %rax
	lock cmpxchgq	%rsi, (%rdi)
	movq	-192(%rbp), %r10
	cmpq	%rax, %r12
	jne	.L1450
	cmpb	$0, 768(%rbx)
	jne	.L1451
.L1175:
	cmpb	$0, 769(%rbx)
	jne	.L1452
.L1177:
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	leaq	64(%rbx), %rcx
	movq	%r14, %rdx
	movq	%r10, -160(%rbp)
	call	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	movq	-152(%rbp), %rsi
	movq	-160(%rbp), %r10
	movq	(%rsi), %rax
	movq	%rax, -176(%rbp)
	andl	$2, %eax
	orq	%r10, %rax
	cmpb	$5, -144(%rbp)
	movq	%rax, (%rsi)
	ja	.L1453
.L1193:
	movslq	%r15d, %rax
	addq	%rax, 120(%rbx)
	xorl	%r14d, %r14d
.L1174:
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	jmp	.L1122
.L1445:
	movl	%ecx, %r10d
	addl	%r10d, %r10d
	jne	.L1255
	addq	$4, %rdi
	movl	$1, %r10d
.L1255:
	movl	(%rdi), %edi
	testl	%edi, %r10d
	jne	.L1251
	addq	16(%r14), %r11
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1454:
	movl	%edx, %esi
	movl	%ecx, %eax
	orl	%ecx, %esi
	lock cmpxchgl	%esi, (%r11)
	cmpl	%eax, %ecx
	je	.L1248
.L1256:
	movl	(%r11), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1454
	jmp	.L1248
.L1430:
	movq	-160(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r10, %rsi
	call	_ZN2v88internal14LocalAllocator18FreeLastInOldSpaceENS0_10HeapObjectEi
.L1414:
	movq	-176(%rbp), %rax
	movl	$1, %r14d
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	-152(%rbp), %rbx
	movq	(%rbx), %rax
	movq	%rax, -144(%rbp)
	andl	$2, %eax
	orq	%rdx, %rax
	movq	%rax, (%rbx)
	testb	$1, %al
	je	.L1122
	cmpl	$3, %eax
	je	.L1122
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %r14
	shrq	$4, %r14
	xorq	$1, %r14
	andl	$1, %r14d
	jmp	.L1122
.L1446:
	movq	-160(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r10, %rsi
	call	_ZN2v88internal14LocalAllocator18FreeLastInNewSpaceENS0_10HeapObjectEi
	jmp	.L1414
.L1429:
	movq	%r14, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$24, %al
	je	.L1120
	testb	$32, %al
	je	.L1120
	leaq	-1(%r14), %rsi
	movq	%r12, %rax
	lock cmpxchgq	%rsi, (%rsi)
	cmpq	%rax, %r12
	jne	.L1403
	movq	%r12, %xmm1
	movq	%r14, %xmm0
	leaq	-80(%rbp), %rsi
	punpcklqdq	%xmm1, %xmm0
	leaq	656(%rbx), %rdi
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZNSt13unordered_mapIN2v88internal10HeapObjectENS1_3MapENS1_6Object6HasherESt8equal_toIS2_ESaISt4pairIKS2_S3_EEE6insertEOSA_
	movslq	%r15d, %rax
	addq	%rax, 128(%rbx)
	cmpb	$5, -144(%rbp)
	movdqa	-176(%rbp), %xmm0
	jbe	.L1403
	movq	16(%rbx), %rdi
	movl	24(%rbx), %esi
	movaps	%xmm0, -112(%rbp)
	xorl	%r14d, %r14d
	pushq	%rax
	movl	%r15d, -96(%rbp)
	addq	$696, %rdi
	pushq	-96(%rbp)
	pushq	-104(%rbp)
	pushq	-112(%rbp)
	call	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EE4PushEiS3_
	movq	-152(%rbp), %rax
	addq	$32, %rsp
	movq	(%rax), %rax
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1432:
	movl	%r10d, %eax
	movq	%r10, %r8
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r8
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	salq	$2, %rax
	sall	%cl, %edx
	movq	16(%r8), %rcx
	addq	%rax, %rcx
	movl	(%rcx), %esi
	testl	%edx, %esi
	jne	.L1156
.L1161:
	movl	%r14d, %esi
	movq	%r14, %r11
	movl	$1, %edi
	andl	$262143, %esi
	andq	$-262144, %r11
	movl	%esi, %ecx
	shrl	$8, %esi
	leaq	0(,%rsi,4), %r9
	movq	16(%r11), %rsi
	shrl	$3, %ecx
	sall	%cl, %edi
	addq	%r9, %rsi
	movl	%edi, %ecx
	movl	(%rsi), %edi
	testl	%ecx, %edi
	jne	.L1455
.L1158:
	movq	16(%r11), %rdi
	addq	%r9, %rdi
	movl	(%rdi), %esi
	testl	%esi, %ecx
	je	.L1155
	addl	%ecx, %ecx
	jne	.L1164
	addq	$4, %rdi
	movl	$1, %ecx
.L1164:
	movl	(%rdi), %esi
	testl	%esi, %ecx
	je	.L1155
	addq	16(%r8), %rax
	movq	%rax, %rsi
.L1165:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L1155
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L1165
	movl	-184(%rbp), %edx
	movl	$1, %eax
	movq	%r10, -80(%rbp)
	subl	%r8d, %edx
	movl	%edx, %ecx
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movl	%edx, %eax
	movq	16(%r8), %rdx
	shrl	$8, %eax
	leaq	(%rdx,%rax,4), %rdx
	movl	(%rdx), %eax
	testl	%ecx, %eax
	je	.L1155
	addl	%ecx, %ecx
	jne	.L1169
	addq	$4, %rdx
	movl	$1, %ecx
.L1169:
	movl	(%rdx), %esi
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L1155
	movl	%esi, %edi
	movl	%esi, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %esi
	jne	.L1169
	leaq	-80(%rbp), %rdi
	movq	%r8, -176(%rbp)
	movq	%r10, -160(%rbp)
	call	_ZNK2v88internal10HeapObject4SizeEv
	movq	-176(%rbp), %r8
	cltq
	lock addq	%rax, 96(%r8)
	movq	-160(%rbp), %r10
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1435:
	subl	%r9d, %r14d
	movl	$1, %edx
	movq	%r15, -80(%rbp)
	movl	%r14d, %ecx
	shrl	$8, %r14d
	shrl	$3, %ecx
	movl	%r14d, %eax
	sall	%cl, %edx
	movq	16(%r9), %rcx
	leaq	(%rcx,%rax,4), %rsi
	movl	(%rsi), %eax
	testl	%edx, %eax
	je	.L1227
	addl	%edx, %edx
	jne	.L1241
	addq	$4, %rsi
	movl	$1, %edx
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1457:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L1456
.L1241:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1457
	jmp	.L1227
.L1448:
	movl	%r10d, %eax
	movq	%r10, %r8
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r8
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	salq	$2, %rax
	sall	%cl, %edx
	movq	16(%r8), %rcx
	addq	%rax, %rcx
	movl	(%rcx), %esi
	testl	%edx, %esi
	jne	.L1131
.L1136:
	movl	%r14d, %esi
	movq	%r14, %r11
	movl	$1, %edi
	andl	$262143, %esi
	andq	$-262144, %r11
	movl	%esi, %ecx
	shrl	$8, %esi
	leaq	0(,%rsi,4), %r9
	movq	16(%r11), %rsi
	shrl	$3, %ecx
	sall	%cl, %edi
	addq	%r9, %rsi
	movl	%edi, %ecx
	movl	(%rsi), %edi
	testl	%ecx, %edi
	jne	.L1458
.L1133:
	movq	16(%r11), %rdi
	addq	%r9, %rdi
	movl	(%rdi), %esi
	testl	%esi, %ecx
	je	.L1130
	addl	%ecx, %ecx
	jne	.L1139
	addq	$4, %rdi
	movl	$1, %ecx
.L1139:
	movl	(%rdi), %esi
	testl	%ecx, %esi
	je	.L1130
	addq	16(%r8), %rax
	movq	%rax, %rsi
.L1140:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L1130
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L1140
	movl	-184(%rbp), %edx
	movl	$1, %eax
	movq	%r10, -80(%rbp)
	subl	%r8d, %edx
	movl	%edx, %ecx
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movl	%edx, %eax
	movq	16(%r8), %rdx
	shrl	$8, %eax
	leaq	(%rdx,%rax,4), %rdx
	movl	(%rdx), %eax
	testl	%ecx, %eax
	je	.L1130
	addl	%ecx, %ecx
	jne	.L1144
	addq	$4, %rdx
	movl	$1, %ecx
.L1144:
	movl	(%rdx), %esi
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L1130
	movl	%esi, %edi
	movl	%esi, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %esi
	jne	.L1144
	leaq	-80(%rbp), %rdi
	movq	%r8, -176(%rbp)
	movq	%r10, -160(%rbp)
	call	_ZNK2v88internal10HeapObject4SizeEv
	movq	-176(%rbp), %r8
	cltq
	lock addq	%rax, 96(%r8)
	movq	-160(%rbp), %r10
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1438:
	movl	-176(%rbp), %edx
	movl	$1, %eax
	movq	%r15, -80(%rbp)
	subl	%r14d, %edx
	movl	%edx, %ecx
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movl	%edx, %eax
	movq	16(%r14), %rdx
	shrl	$8, %eax
	leaq	(%rdx,%rax,4), %rsi
	movl	(%rsi), %eax
	testl	%ecx, %eax
	je	.L1201
	addl	%ecx, %ecx
	jne	.L1215
	addq	$4, %rsi
	movl	$1, %ecx
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1460:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1459
.L1215:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L1460
	jmp	.L1201
.L1433:
	movl	24(%rbx), %esi
	movq	16(%rbx), %rdi
	movl	%r15d, %ecx
	movq	%r10, %rdx
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_
	jmp	.L1171
.L1450:
	movq	-160(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r10, %rsi
	movl	$1, %r14d
	call	_ZN2v88internal14LocalAllocator18FreeLastInNewSpaceENS0_10HeapObjectEi
	movq	-176(%rbp), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	-152(%rbp), %rbx
	movq	(%rbx), %rax
	movq	%rax, -144(%rbp)
	andl	$2, %eax
	orq	%rdx, %rax
	movq	%rax, (%rbx)
	testb	$1, %al
	je	.L1174
	cmpl	$3, %eax
	je	.L1174
	andq	$-262144, %rax
	movq	8(%rax), %r14
	shrq	$4, %r14
	xorq	$1, %r14
	andl	$1, %r14d
	jmp	.L1174
.L1449:
	movq	32(%rbx), %rdi
	movl	40(%rbx), %esi
	movl	%r15d, %ecx
	movq	%r10, %rdx
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_
	jmp	.L1146
.L1452:
	movl	%r10d, %eax
	movq	%r10, %r8
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r8
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	salq	$2, %rax
	sall	%cl, %edx
	movq	16(%r8), %rcx
	addq	%rax, %rcx
	movl	(%rcx), %esi
	testl	%edx, %esi
	jne	.L1178
.L1183:
	movl	%r14d, %esi
	movq	%r14, %r11
	movl	$1, %edi
	andl	$262143, %esi
	andq	$-262144, %r11
	movl	%esi, %ecx
	shrl	$8, %esi
	leaq	0(,%rsi,4), %r9
	movq	16(%r11), %rsi
	shrl	$3, %ecx
	sall	%cl, %edi
	addq	%r9, %rsi
	movl	%edi, %ecx
	movl	(%rsi), %edi
	testl	%ecx, %edi
	jne	.L1461
.L1180:
	movq	16(%r11), %rdi
	addq	%r9, %rdi
	movl	(%rdi), %esi
	testl	%esi, %ecx
	je	.L1177
	addl	%ecx, %ecx
	jne	.L1186
	addq	$4, %rdi
	movl	$1, %ecx
.L1186:
	movl	(%rdi), %esi
	testl	%ecx, %esi
	je	.L1177
	addq	16(%r8), %rax
	movq	%rax, %rsi
.L1187:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L1177
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L1187
	movl	-184(%rbp), %edx
	movl	$1, %eax
	movq	%r10, -80(%rbp)
	subl	%r8d, %edx
	movl	%edx, %ecx
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movl	%edx, %eax
	movq	16(%r8), %rdx
	shrl	$8, %eax
	leaq	(%rdx,%rax,4), %rdx
	movl	(%rdx), %eax
	testl	%ecx, %eax
	je	.L1177
	addl	%ecx, %ecx
	jne	.L1191
	addq	$4, %rdx
	movl	$1, %ecx
.L1191:
	movl	(%rdx), %esi
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L1177
	movl	%esi, %edi
	movl	%esi, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %esi
	jne	.L1191
	leaq	-80(%rbp), %rdi
	movq	%r8, -160(%rbp)
	movq	%r10, -176(%rbp)
	call	_ZNK2v88internal10HeapObject4SizeEv
	movq	-160(%rbp), %r8
	cltq
	lock addq	%rax, 96(%r8)
	movq	-176(%rbp), %r10
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1156:
	movl	%edx, %esi
	addl	%esi, %esi
	jne	.L1159
	addq	$4, %rcx
	movl	$1, %esi
.L1159:
	movl	(%rcx), %ecx
	testl	%ecx, %esi
	jne	.L1155
	jmp	.L1161
.L1131:
	movl	%edx, %esi
	addl	%esi, %esi
	jne	.L1134
	addq	$4, %rcx
	movl	$1, %esi
.L1134:
	movl	(%rcx), %ecx
	testl	%esi, %ecx
	jne	.L1130
	jmp	.L1136
.L1431:
	movq	8(%rbx), %rdi
	movq	%r10, %rsi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r10, -176(%rbp)
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	movq	-176(%rbp), %r10
	jmp	.L1153
.L1455:
	movl	%ecx, %edi
	addl	%edi, %edi
	jne	.L1162
	addq	$4, %rsi
	movl	$1, %edi
.L1162:
	movl	(%rsi), %esi
	testl	%edi, %esi
	jne	.L1158
	addq	16(%r8), %rax
	movq	%rax, %rsi
	jmp	.L1163
.L1462:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L1155
.L1163:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1462
	jmp	.L1155
.L1447:
	movq	8(%rbx), %rdi
	movq	%r10, %rsi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r10, -176(%rbp)
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	movq	-176(%rbp), %r10
	jmp	.L1128
.L1458:
	movl	%ecx, %edi
	addl	%edi, %edi
	jne	.L1137
	addq	$4, %rsi
	movl	$1, %edi
.L1137:
	movl	(%rsi), %esi
	testl	%edi, %esi
	jne	.L1133
	addq	16(%r8), %rax
	movq	%rax, %rsi
	jmp	.L1138
.L1463:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L1130
.L1138:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1463
	jmp	.L1130
.L1453:
	movq	32(%rbx), %rdi
	movl	40(%rbx), %esi
	movl	%r15d, %ecx
	movq	%r10, %rdx
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_
	jmp	.L1193
.L1456:
	leaq	-80(%rbp), %rdi
	movq	%r9, -176(%rbp)
	call	_ZNK2v88internal10HeapObject4SizeEv
	movq	-176(%rbp), %r9
	cltq
	lock addq	%rax, 96(%r9)
	jmp	.L1227
.L1178:
	movl	%edx, %esi
	addl	%esi, %esi
	jne	.L1181
	addq	$4, %rcx
	movl	$1, %esi
.L1181:
	movl	(%rcx), %ecx
	testl	%esi, %ecx
	jne	.L1177
	jmp	.L1183
.L1459:
	leaq	-80(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject4SizeEv
	cltq
	lock addq	%rax, 96(%r14)
	jmp	.L1201
.L1426:
	call	__stack_chk_fail@PLT
.L1451:
	movq	8(%rbx), %rdi
	movq	%r10, %rsi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r10, -176(%rbp)
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	movq	-176(%rbp), %r10
	jmp	.L1175
.L1461:
	movl	%ecx, %edi
	addl	%edi, %edi
	jne	.L1184
	addq	$4, %rsi
	movl	$1, %edi
.L1184:
	movl	(%rsi), %esi
	testl	%edi, %esi
	jne	.L1180
	addq	16(%r8), %rax
	movq	%rax, %rsi
	jmp	.L1185
.L1464:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L1177
.L1185:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1464
	jmp	.L1177
	.cfi_endproc
.LFE28011:
	.size	_ZN2v88internal9Scavenger25EvacuateShortcutCandidateINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ConsStringEi, .-_ZN2v88internal9Scavenger25EvacuateShortcutCandidateINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ConsStringEi
	.section	.text._ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE,"axG",@progbits,_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE
	.type	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE, @function
_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE:
.LFB25232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdx), %r12
	testb	$1, %r12b
	jne	.L1466
	movq	(%rsi), %rax
	addq	$1, %r12
	andl	$2, %eax
	orq	%r12, %rax
	andq	$-262144, %r12
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	testb	$24, 8(%r12)
	sete	%al
.L1465:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1623
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1466:
	.cfi_restore_state
	movq	%rdi, %r14
	movq	%r12, %rsi
	leaq	-88(%rbp), %rdi
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movzbl	10(%r12), %ebx
	movl	%eax, %r15d
	cmpb	$39, %bl
	je	.L1468
	cmpb	$48, %bl
	je	.L1624
	movq	-88(%rbp), %rax
	movq	%rax, -112(%rbp)
	subq	$1, %rax
	cmpb	$0, _ZN2v88internal35FLAG_young_generation_large_objectsE(%rip)
	movq	%rax, -120(%rbp)
	jne	.L1625
.L1471:
	movq	-120(%rbp), %rcx
	leaq	136(%r14), %rsi
	movq	%rsi, -128(%rbp)
	movq	%rcx, %rax
	andq	$-262144, %rax
	testb	$8, 10(%rax)
	je	.L1474
	movq	8(%r14), %rdx
	movq	248(%rdx), %rdx
	movq	496(%rdx), %rdx
	cmpq	40(%rax), %rdx
	jb	.L1475
	cmpq	%rcx, 48(%rax)
	cmovbe	48(%rax), %rcx
	cmpq	%rdx, %rcx
	jnb	.L1474
.L1475:
	leaq	160(%r14), %rdi
	movl	$2, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	call	_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE
	movq	%rax, %r11
	testb	$1, %al
	je	.L1497
	leaq	-1(%rax), %rax
	leal	-1(%r15), %edx
	movq	%rax, -136(%rbp)
	movl	%r15d, %eax
	leaq	7(%r11), %rdi
	movq	%r12, -1(%r11)
	subl	$8, %eax
	movq	%r11, -144(%rbp)
	cmovns	%eax, %edx
	movq	-112(%rbp), %rsi
	sarl	$3, %edx
	addq	$7, %rsi
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-120(%rbp), %rsi
	movq	-136(%rbp), %rcx
	movq	%r12, %rax
	lock cmpxchgq	%rcx, (%rsi)
	movq	-144(%rbp), %r11
	cmpq	%rax, %r12
	jne	.L1626
	cmpb	$0, 768(%r14)
	jne	.L1627
.L1503:
	cmpb	$0, 769(%r14)
	jne	.L1628
.L1505:
	movq	8(%r14), %rdi
	movq	-112(%rbp), %rdx
	leaq	64(%r14), %rcx
	movq	%r12, %rsi
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	movq	0(%r13), %rax
	movq	-120(%rbp), %r11
	andl	$2, %eax
	orq	%r11, %rax
	movq	%rax, 0(%r13)
	cmpb	$5, %bl
	ja	.L1629
.L1521:
	movslq	%r15d, %r8
	movl	$1, %eax
	addq	%r8, 128(%r14)
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1624:
	movq	-88(%rbp), %rcx
	movl	%eax, %r8d
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Scavenger18EvacuateThinStringINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ThinStringEi
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1468:
	movq	-88(%rbp), %rcx
	movl	%eax, %r8d
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Scavenger25EvacuateShortcutCandidateINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ConsStringEi
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1474:
	xorl	%ecx, %ecx
	movl	$2, %edx
	movl	%r15d, %esi
	leaq	136(%r14), %rdi
	call	_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r11
	testb	$1, %al
	je	.L1475
	leaq	-1(%rax), %rax
	leal	-1(%r15), %edx
	movq	%rax, -136(%rbp)
	movl	%r15d, %eax
	leaq	7(%r11), %rdi
	movq	%r12, -1(%r11)
	subl	$8, %eax
	movq	%r11, -144(%rbp)
	cmovns	%eax, %edx
	movq	-112(%rbp), %rsi
	sarl	$3, %edx
	addq	$7, %rsi
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-120(%rbp), %rsi
	movq	-136(%rbp), %rcx
	movq	%r12, %rax
	lock cmpxchgq	%rcx, (%rsi)
	movq	-144(%rbp), %r11
	cmpq	%rax, %r12
	jne	.L1596
	cmpb	$0, 768(%r14)
	jne	.L1630
.L1478:
	cmpb	$0, 769(%r14)
	jne	.L1631
.L1527:
	movq	8(%r14), %rdi
	movq	-112(%rbp), %rdx
	leaq	64(%r14), %rcx
	movq	%r12, %rsi
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	movq	0(%r13), %rax
	movq	-120(%rbp), %r11
	andl	$2, %eax
	orq	%r11, %rax
	movq	%rax, 0(%r13)
	cmpb	$5, %bl
	ja	.L1632
.L1543:
	movslq	%r15d, %r8
	xorl	%eax, %eax
	addq	%r8, 120(%r14)
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1497:
	xorl	%ecx, %ecx
	movl	$2, %edx
	movl	%r15d, %esi
	leaq	136(%r14), %rdi
	call	_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r11
	testb	$1, %al
	je	.L1633
	leaq	-1(%rax), %rax
	leal	-1(%r15), %edx
	movq	%rax, -136(%rbp)
	movl	%r15d, %eax
	leaq	7(%r11), %rdi
	movq	%r12, -1(%r11)
	subl	$8, %eax
	movq	%r11, -144(%rbp)
	cmovns	%eax, %edx
	movq	-112(%rbp), %rsi
	sarl	$3, %edx
	addq	$7, %rsi
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-120(%rbp), %rsi
	movq	-136(%rbp), %rcx
	movq	%r12, %rax
	lock cmpxchgq	%rcx, (%rsi)
	movq	-144(%rbp), %r11
	cmpq	%rax, %r12
	jne	.L1596
	cmpb	$0, 768(%r14)
	jne	.L1634
.L1525:
	cmpb	$0, 769(%r14)
	je	.L1527
	movl	%r11d, %eax
	movq	%r11, %r8
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r8
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	salq	$2, %rax
	sall	%cl, %edx
	movq	16(%r8), %rcx
	addq	%rax, %rcx
	movl	(%rcx), %esi
	testl	%edx, %esi
	jne	.L1528
.L1533:
	movq	-112(%rbp), %rcx
	movl	$1, %edi
	movl	%ecx, %esi
	movq	%rcx, %r10
	andl	$262143, %esi
	andq	$-262144, %r10
	movl	%esi, %ecx
	shrl	$8, %esi
	leaq	0(,%rsi,4), %r9
	movq	16(%r10), %rsi
	shrl	$3, %ecx
	sall	%cl, %edi
	addq	%r9, %rsi
	movl	%edi, %ecx
	movl	(%rsi), %edi
	testl	%ecx, %edi
	jne	.L1635
.L1530:
	movq	16(%r10), %rsi
	addq	%r9, %rsi
	movl	(%rsi), %edi
	testl	%edi, %ecx
	je	.L1527
	addl	%ecx, %ecx
	jne	.L1536
	addq	$4, %rsi
	movl	$1, %ecx
.L1536:
	movl	(%rsi), %esi
	testl	%ecx, %esi
	je	.L1527
	addq	16(%r8), %rax
	movq	%rax, %rsi
.L1537:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L1527
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L1537
	movl	-136(%rbp), %eax
	movl	$1, %edx
	movq	%r11, -80(%rbp)
	subl	%r8d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r8), %rdx
	leaq	(%rdx,%rax,4), %rdx
	movl	(%rdx), %eax
	testl	%ecx, %eax
	je	.L1527
	addl	%ecx, %ecx
	jne	.L1541
	addq	$4, %rdx
	movl	$1, %ecx
.L1541:
	movl	(%rdx), %esi
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%eax, %ecx
	je	.L1527
	movl	%ecx, %edi
	movl	%esi, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %esi
	jne	.L1541
.L1591:
	leaq	-80(%rbp), %rdi
	movq	%r8, -120(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZNK2v88internal10HeapObject4SizeEv
	movq	-120(%rbp), %r8
	cltq
	lock addq	%rax, 96(%r8)
	movq	-128(%rbp), %r11
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1596:
	movq	-128(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r11, %rsi
	call	_ZN2v88internal14LocalAllocator18FreeLastInNewSpaceENS0_10HeapObjectEi
.L1620:
	movq	-112(%rbp), %rax
	movq	-1(%rax), %rax
	addq	$1, %rax
	movq	0(%r13), %rdx
	andl	$2, %edx
	orq	%rax, %rdx
	movl	$1, %eax
	movq	%rdx, 0(%r13)
	testb	$1, %dl
	je	.L1465
	cmpl	$3, %edx
	je	.L1465
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	shrq	$4, %rax
	xorq	$1, %rax
	andl	$1, %eax
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1626:
	movq	-128(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r11, %rsi
	call	_ZN2v88internal14LocalAllocator18FreeLastInOldSpaceENS0_10HeapObjectEi
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1625:
	movq	-112(%rbp), %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$24, %al
	je	.L1471
	testb	$32, %al
	je	.L1471
	movq	-120(%rbp), %rcx
	movq	%r12, %rax
	lock cmpxchgq	%rcx, (%rcx)
	cmpq	%rax, %r12
	je	.L1472
.L1612:
	xorl	%eax, %eax
	jmp	.L1465
.L1472:
	movq	-112(%rbp), %xmm0
	movq	%r12, %xmm1
	leaq	-80(%rbp), %rsi
	leaq	656(%r14), %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZNSt13unordered_mapIN2v88internal10HeapObjectENS1_3MapENS1_6Object6HasherESt8equal_toIS2_ESaISt4pairIKS2_S3_EEE6insertEOSA_
	movslq	%r15d, %rax
	addq	%rax, 128(%r14)
	cmpb	$5, %bl
	movdqa	-112(%rbp), %xmm0
	jbe	.L1612
	movq	16(%r14), %rdi
	movl	24(%r14), %esi
	movaps	%xmm0, -80(%rbp)
	movl	%r15d, -64(%rbp)
	pushq	%rax
	addq	$696, %rdi
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EE4PushEiS3_
	addq	$32, %rsp
	xorl	%eax, %eax
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1628:
	movl	%r11d, %eax
	movq	%r11, %r8
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r8
	movl	%eax, %ecx
	shrl	$8, %eax
	leaq	0(,%rax,4), %r9
	movq	16(%r8), %rax
	shrl	$3, %ecx
	sall	%cl, %edx
	addq	%r9, %rax
	movl	(%rax), %ecx
	testl	%edx, %ecx
	jne	.L1506
.L1511:
	movq	-112(%rbp), %rax
	movl	$1, %edi
	movq	%rax, %rsi
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	movq	16(%rsi), %r10
	shrl	$3, %ecx
	salq	$2, %rax
	sall	%cl, %edi
	addq	%rax, %r10
	movl	%edi, %ecx
	movl	(%r10), %edi
	testl	%ecx, %edi
	jne	.L1636
.L1508:
	addq	16(%rsi), %rax
	movl	(%rax), %esi
	testl	%esi, %ecx
	je	.L1505
	addl	%ecx, %ecx
	jne	.L1514
	addq	$4, %rax
	movl	$1, %ecx
.L1514:
	movl	(%rax), %eax
	testl	%ecx, %eax
	je	.L1505
	movq	16(%r8), %rsi
	addq	%r9, %rsi
	.p2align 4,,10
	.p2align 3
.L1515:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L1505
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L1515
	movl	-136(%rbp), %eax
	movl	$1, %edx
	movq	%r11, -80(%rbp)
	subl	%r8d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r8), %rdx
	leaq	(%rdx,%rax,4), %rdx
	movl	(%rdx), %eax
	testl	%ecx, %eax
	je	.L1505
	addl	%ecx, %ecx
	jne	.L1519
	addq	$4, %rdx
	movl	$1, %ecx
.L1519:
	movl	(%rdx), %esi
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%eax, %ecx
	je	.L1505
	movl	%ecx, %edi
	movl	%esi, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %esi
	jne	.L1519
	leaq	-80(%rbp), %rdi
	movq	%r8, -120(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZNK2v88internal10HeapObject4SizeEv
	movq	-120(%rbp), %r8
	cltq
	lock addq	%rax, 96(%r8)
	movq	-128(%rbp), %r11
	jmp	.L1505
	.p2align 4,,10
	.p2align 3
.L1631:
	movl	%r11d, %eax
	movq	%r11, %r8
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r8
	movl	%eax, %ecx
	shrl	$8, %eax
	leaq	0(,%rax,4), %r9
	movq	16(%r8), %rax
	shrl	$3, %ecx
	sall	%cl, %edx
	addq	%r9, %rax
	movl	(%rax), %ecx
	testl	%edx, %ecx
	jne	.L1481
.L1486:
	movq	-112(%rbp), %rax
	movl	%eax, %edi
	movq	%rax, %rsi
	movl	$1, %eax
	andl	$262143, %edi
	andq	$-262144, %rsi
	movl	%edi, %ecx
	shrl	$8, %edi
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%edi, %ecx
	leaq	0(,%rcx,4), %r10
	movq	16(%rsi), %rcx
	addq	%r10, %rcx
	movl	(%rcx), %edi
	testl	%eax, %edi
	jne	.L1637
.L1483:
	addq	16(%rsi), %r10
	movl	(%r10), %ecx
	movq	%r10, %rsi
	testl	%ecx, %eax
	je	.L1527
	addl	%eax, %eax
	jne	.L1489
	addq	$4, %rsi
	movl	$1, %eax
.L1489:
	movl	(%rsi), %ecx
	testl	%ecx, %eax
	je	.L1527
	movq	16(%r8), %rsi
	addq	%r9, %rsi
.L1490:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L1527
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L1490
	movl	-136(%rbp), %eax
	movl	$1, %edx
	movq	%r11, -80(%rbp)
	subl	%r8d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r8), %rdx
	leaq	(%rdx,%rax,4), %rdx
	movl	(%rdx), %eax
	testl	%ecx, %eax
	je	.L1527
	addl	%ecx, %ecx
	jne	.L1494
	addq	$4, %rdx
	movl	$1, %ecx
.L1494:
	movl	(%rdx), %esi
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L1527
	movl	%esi, %edi
	movl	%esi, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %esi
	jne	.L1494
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L1633:
	movq	8(%r14), %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1632:
	movq	32(%r14), %rdi
	movl	40(%r14), %esi
	movl	%r15d, %ecx
	movq	%r11, %rdx
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_
	jmp	.L1543
.L1629:
	movl	24(%r14), %esi
	movq	16(%r14), %rdi
	movl	%r15d, %ecx
	movq	%r11, %rdx
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_
	jmp	.L1521
.L1506:
	movl	%edx, %ecx
	addl	%ecx, %ecx
	jne	.L1509
	addq	$4, %rax
	movl	$1, %ecx
.L1509:
	movl	(%rax), %eax
	testl	%ecx, %eax
	jne	.L1505
	jmp	.L1511
.L1627:
	movq	8(%r14), %rdi
	movq	-112(%rbp), %rdx
	movq	%r11, %rsi
	movl	%r15d, %ecx
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	movq	-120(%rbp), %r11
	jmp	.L1503
.L1481:
	movl	%edx, %ecx
	addl	%ecx, %ecx
	jne	.L1484
	addq	$4, %rax
	movl	$1, %ecx
.L1484:
	movl	(%rax), %eax
	testl	%eax, %ecx
	jne	.L1527
	jmp	.L1486
.L1630:
	movq	8(%r14), %rdi
	movq	-112(%rbp), %rdx
	movq	%r11, %rsi
	movl	%r15d, %ecx
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	movq	-120(%rbp), %r11
	jmp	.L1478
.L1636:
	movl	%ecx, %edi
	addl	%edi, %edi
	movl	%edi, -120(%rbp)
	jne	.L1512
	movl	$1, -120(%rbp)
	addq	$4, %r10
.L1512:
	movl	(%r10), %edi
	testl	%edi, -120(%rbp)
	jne	.L1508
	movq	16(%r8), %rsi
	addq	%r9, %rsi
	jmp	.L1513
	.p2align 4,,10
	.p2align 3
.L1638:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L1505
.L1513:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1638
	jmp	.L1505
.L1637:
	movl	%eax, %edi
	addl	%edi, %edi
	jne	.L1487
	addq	$4, %rcx
	movl	$1, %edi
.L1487:
	movl	(%rcx), %ecx
	testl	%edi, %ecx
	jne	.L1483
	movq	16(%r8), %rsi
	addq	%r9, %rsi
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1639:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L1527
.L1488:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1639
	jmp	.L1527
.L1528:
	movl	%edx, %esi
	addl	%esi, %esi
	jne	.L1531
	addq	$4, %rcx
	movl	$1, %esi
.L1531:
	movl	(%rcx), %ecx
	testl	%ecx, %esi
	jne	.L1527
	jmp	.L1533
.L1634:
	movq	8(%r14), %rdi
	movq	-112(%rbp), %rdx
	movq	%r11, %rsi
	movl	%r15d, %ecx
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	movq	-120(%rbp), %r11
	jmp	.L1525
.L1635:
	movl	%ecx, %edi
	addl	%edi, %edi
	jne	.L1534
	addq	$4, %rsi
	movl	$1, %edi
.L1534:
	movl	(%rsi), %esi
	testl	%edi, %esi
	jne	.L1530
	addq	16(%r8), %rax
	movq	%rax, %rsi
	jmp	.L1535
.L1640:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L1527
.L1535:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1640
	jmp	.L1527
.L1623:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25232:
	.size	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE, .-_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE
	.section	.text._ZN2v88internal9Scavenger12ScavengePageEPNS0_11MemoryChunkE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Scavenger12ScavengePageEPNS0_11MemoryChunkE
	.type	_ZN2v88internal9Scavenger12ScavengePageEPNS0_11MemoryChunkE, @function
_ZN2v88internal9Scavenger12ScavengePageEPNS0_11MemoryChunkE:
.LFB22493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movzbl	376(%rax), %eax
	movb	%al, -113(%rbp)
	testb	%al, %al
	je	.L1642
	movzbl	8(%rsi), %eax
	andl	$1, %eax
	movb	%al, -113(%rbp)
	jne	.L1781
.L1642:
	movq	104(%r14), %r11
	testq	%r11, %r11
	je	.L1646
	movq	(%r14), %rax
	addq	$262143, %rax
	shrq	$18, %rax
	je	.L1646
	leaq	(%rax,%rax,2), %rax
	movq	%r14, -184(%rbp)
	movq	%r11, %r13
	salq	$7, %rax
	addq	%r11, %rax
	movq	%rax, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L1659:
	leaq	256(%r13), %rax
	movq	%r15, %rdi
	xorl	%ebx, %ebx
	movq	%r13, %r11
	movq	%rax, -128(%rbp)
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L1649:
	movq	(%r15), %r12
	leal	1024(%rbx), %r8d
	testq	%r12, %r12
	jne	.L1647
	.p2align 4,,10
	.p2align 3
.L1651:
	addq	$8, %r15
	movl	%r8d, %ebx
	cmpq	%r15, -128(%rbp)
	jne	.L1649
	movq	%r11, %r13
	movq	%rdi, %r15
	addq	$384, %r13
	cmpq	%r13, -176(%rbp)
	jne	.L1659
	movq	-184(%rbp), %r14
.L1646:
	movq	120(%r14), %r13
	testq	%r13, %r13
	jne	.L1782
.L1660:
	testb	$32, 10(%r14)
	je	.L1783
.L1708:
	cmpb	$0, -113(%rbp)
	jne	.L1784
.L1641:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1785
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1783:
	.cfi_restore_state
	movq	80(%r14), %rax
	cmpl	$2, 72(%rax)
	jne	.L1708
	movq	168(%r14), %rax
	testq	%rax, %rax
	je	.L1708
	movq	8(%r15), %rax
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$2, %esi
	movq	2016(%rax), %rax
	movq	9984(%rax), %rdi
	call	_ZN2v88internal7Sweeper7AddPageENS0_15AllocationSpaceEPNS0_4PageENS1_11AddPageModeE@PLT
	jmp	.L1708
	.p2align 4,,10
	.p2align 3
.L1647:
	movq	%r15, %r10
.L1652:
	movl	(%r12), %r15d
	testl	%r15d, %r15d
	jne	.L1715
.L1657:
	addl	$32, %ebx
	addq	$4, %r12
	cmpl	%r8d, %ebx
	jne	.L1652
	movq	%r10, %r15
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1715:
	movl	%r15d, -136(%rbp)
	movl	%r15d, %r14d
	xorl	%r13d, %r13d
	movq	%r11, %r15
	movq	%r12, -144(%rbp)
	movl	%ebx, %r12d
.L1650:
	xorl	%ecx, %ecx
	movl	$1, %ebx
	rep bsfl	%r14d, %ecx
	leal	(%rcx,%r12), %esi
	sall	%cl, %ebx
	sall	$3, %esi
	movslq	%esi, %rsi
	addq	256(%r15), %rsi
	movq	(%rsi), %rdx
	testb	$1, %dl
	je	.L1656
	cmpl	$3, %edx
	je	.L1656
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$8, %al
	je	.L1654
	andq	$-3, %rdx
	movl	%r8d, -168(%rbp)
	movq	%r10, -160(%rbp)
	movq	%rdi, -152(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE
	movq	-152(%rbp), %rdi
	movq	-160(%rbp), %r10
	testl	%eax, %eax
	movl	-168(%rbp), %r8d
	jne	.L1656
.L1655:
	movl	%ebx, %eax
	xorl	%r14d, %eax
	cmpl	%r14d, %ebx
	jne	.L1716
.L1787:
	movq	%r15, %r11
	movl	-136(%rbp), %r15d
	movl	%r13d, %ecx
	movl	%r12d, %ebx
	notl	%ecx
	movq	-144(%rbp), %r12
	testl	%r15d, %r13d
	je	.L1657
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1786:
	movl	%ecx, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%r12)
	cmpl	%eax, %edx
	je	.L1657
.L1658:
	movl	(%r12), %edx
	testl	%edx, %r13d
	jne	.L1786
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1654:
	testb	$16, %al
	jne	.L1655
	.p2align 4,,10
	.p2align 3
.L1656:
	movl	%ebx, %eax
	orl	%ebx, %r13d
	xorl	%r14d, %eax
	cmpl	%r14d, %ebx
	je	.L1787
.L1716:
	movl	%eax, %r14d
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1782:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1706
	movl	$0, -144(%rbp)
	movq	%r13, -152(%rbp)
	movq	%r14, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L1705:
	movl	20(%r12), %eax
	movq	8(%r12), %rbx
	testl	%eax, %eax
	jle	.L1662
	subl	$1, %eax
	leaq	4(%rbx,%rax,4), %rax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L1703:
	movl	(%rbx), %r14d
	movl	%r14d, %eax
	shrl	$29, %eax
	cmpl	$5, %eax
	je	.L1663
	movq	-152(%rbp), %rcx
	andl	$536870911, %r14d
	addq	24(%rcx), %r14
	cmpl	$4, %eax
	ja	.L1664
	leaq	.L1666(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal9Scavenger12ScavengePageEPNS0_11MemoryChunkE,"a",@progbits
	.align 4
	.align 4
.L1666:
	.long	.L1670-.L1666
	.long	.L1669-.L1666
	.long	.L1668-.L1666
	.long	.L1667-.L1666
	.long	.L1665-.L1666
	.section	.text._ZN2v88internal9Scavenger12ScavengePageEPNS0_11MemoryChunkE
	.p2align 4,,10
	.p2align 3
.L1667:
	pxor	%xmm0, %xmm0
	movq	%r14, -96(%rbp)
	movups	%xmm0, -72(%rbp)
	movb	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movslq	(%r14), %rax
	addq	%rax, %r14
	leaq	4(%r14), %rdx
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movq	%rax, %r13
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movq	-136(%rbp), %rdx
	movl	%eax, %eax
	addq	%r13, %rax
	cmpq	%rax, %rdx
	jnb	.L1671
	cmpq	%rdx, %r13
	jbe	.L1788
.L1671:
	subq	$59, %r14
	movq	%r14, -104(%rbp)
	testb	$1, %r14b
	je	.L1702
	cmpl	$3, %r14d
	je	.L1702
	movq	%r14, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r13
	testb	$8, %r13b
	je	.L1679
	movq	%r14, %rdx
	leaq	-104(%rbp), %rsi
	movq	%r15, %rdi
	andq	$-3, %rdx
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE
	movq	-104(%rbp), %rsi
	movl	%eax, %r13d
	cmpq	%r14, %rsi
	je	.L1676
	addq	$63, %rsi
	leaq	-96(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$4, %edx
	call	_ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE@PLT
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1668:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L1702
	cmpl	$3, %edx
	je	.L1702
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$8, %al
	je	.L1700
	andq	$-3, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE
	movl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L1676:
	testl	%r13d, %r13d
	jne	.L1702
.L1701:
	addl	$1, -144(%rbp)
.L1663:
	addq	$4, %rbx
	cmpq	%rbx, -128(%rbp)
	jne	.L1703
.L1662:
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L1705
	movl	-144(%rbp), %eax
	movq	-160(%rbp), %r14
	testl	%eax, %eax
	jne	.L1660
.L1706:
	movq	%r14, %rdi
	call	_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE0EEEvv@PLT
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1669:
	pxor	%xmm0, %xmm0
	movq	%r14, -96(%rbp)
	movb	$2, -88(%rbp)
	movq	$0, -80(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	(%r14), %r14
	movq	%r14, -104(%rbp)
	testb	$1, %r14b
	je	.L1702
	cmpl	$3, %r14d
	je	.L1702
	movq	%r14, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r13
	testb	$8, %r13b
	je	.L1679
	movq	%r14, %rdx
	leaq	-104(%rbp), %rsi
	movq	%r15, %rdi
	andq	$-3, %rdx
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE
	movq	-104(%rbp), %rdx
	movl	%eax, %r13d
	cmpq	%rdx, %r14
	je	.L1676
.L1780:
	movq	-96(%rbp), %rax
	movl	$8, %esi
	movq	%rdx, (%rax)
	movq	-96(%rbp), %rdi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	movq	-72(%rbp), %r14
	movq	-136(%rbp), %rdx
	testq	%r14, %r14
	je	.L1676
	testb	$1, %dl
	je	.L1676
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testb	$24, %al
	jne	.L1789
	testl	$262144, %eax
	je	.L1676
.L1790:
	leaq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal30Heap_MarkingBarrierForCodeSlowENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE@PLT
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1670:
	pxor	%xmm0, %xmm0
	movq	%r14, -96(%rbp)
	movb	$3, -88(%rbp)
	movq	$0, -80(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	(%r14), %r14
	movq	%r14, -104(%rbp)
	testb	$1, %r14b
	je	.L1702
	cmpl	$3, %r14d
	je	.L1702
	movq	%r14, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r13
	testb	$8, %r13b
	je	.L1679
	movq	%r14, %rdx
	leaq	-104(%rbp), %rsi
	movq	%r15, %rdi
	andq	$-3, %rdx
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE
	movq	-104(%rbp), %rdx
	movl	%eax, %r13d
	cmpq	%r14, %rdx
	jne	.L1780
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1665:
	movq	(%r14), %rax
	leaq	-63(%rax), %rcx
	movq	%rcx, -96(%rbp)
	testb	$1, %cl
	je	.L1702
	cmpl	$3, %ecx
	je	.L1702
	movq	%rcx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r13
	testb	$8, %r13b
	je	.L1679
	movq	%rcx, %rdx
	leaq	-96(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rcx, -136(%rbp)
	andq	$-3, %rdx
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE
	movq	-136(%rbp), %rcx
	movl	%eax, %r13d
	movq	-96(%rbp), %rax
	cmpq	%rax, %rcx
	je	.L1676
	addq	$63, %rax
	movq	%rax, (%r14)
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1679:
	shrq	$4, %r13
	movq	%r13, %rax
	xorq	$1, %rax
	movl	%eax, %r13d
	andl	$1, %r13d
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1700:
	testb	$16, %al
	jne	.L1701
	.p2align 4,,10
	.p2align 3
.L1702:
	movl	$-1610612736, (%rbx)
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L1784:
	cmpb	$0, _ZN2v88internal12FLAG_jitlessE(%rip)
	movq	%r14, %rdi
	je	.L1711
	call	_ZN2v88internal11MemoryChunk11SetReadableEv@PLT
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1781:
	movq	%rsi, %rdi
	call	_ZN2v88internal11MemoryChunk18SetReadAndWritableEv@PLT
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1711:
	call	_ZN2v88internal11MemoryChunk20SetReadAndExecutableEv@PLT
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1789:
	leaq	-96(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal35Heap_GenerationalBarrierForCodeSlowENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE@PLT
	movq	-136(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	8(%rcx), %rax
	testl	$262144, %eax
	je	.L1676
	jmp	.L1790
	.p2align 4,,10
	.p2align 3
.L1788:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1664:
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1785:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22493:
	.size	_ZN2v88internal9Scavenger12ScavengePageEPNS0_11MemoryChunkE, .-_ZN2v88internal9Scavenger12ScavengePageEPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0, @function
_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0:
.LFB30403:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdx), %r12
	testb	$1, %r12b
	jne	.L1792
	movq	(%rsi), %rax
	addq	$1, %r12
	andl	$2, %eax
	orq	%r12, %rax
	andq	$-262144, %r12
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	testb	$24, 8(%r12)
	sete	%al
.L1791:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1949
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1792:
	.cfi_restore_state
	movq	%rdi, %r14
	movq	%r12, %rsi
	leaq	-88(%rbp), %rdi
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movzbl	10(%r12), %ebx
	movl	%eax, %r15d
	cmpb	$39, %bl
	je	.L1794
	cmpb	$48, %bl
	je	.L1950
	movq	-88(%rbp), %rax
	movq	%rax, -112(%rbp)
	subq	$1, %rax
	cmpb	$0, _ZN2v88internal35FLAG_young_generation_large_objectsE(%rip)
	movq	%rax, -120(%rbp)
	jne	.L1951
.L1797:
	movq	-120(%rbp), %rcx
	leaq	136(%r14), %rsi
	movq	%rsi, -128(%rbp)
	movq	%rcx, %rax
	andq	$-262144, %rax
	testb	$8, 10(%rax)
	je	.L1800
	movq	8(%r14), %rdx
	movq	248(%rdx), %rdx
	movq	496(%rdx), %rdx
	cmpq	40(%rax), %rdx
	jb	.L1801
	cmpq	%rcx, 48(%rax)
	cmovbe	48(%rax), %rcx
	cmpq	%rdx, %rcx
	jnb	.L1800
.L1801:
	leaq	160(%r14), %rdi
	movl	$2, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	call	_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE
	movq	%rax, %r11
	testb	$1, %al
	je	.L1823
	leaq	-1(%rax), %rax
	leal	-1(%r15), %edx
	movq	%rax, -136(%rbp)
	movl	%r15d, %eax
	leaq	7(%r11), %rdi
	movq	%r12, -1(%r11)
	subl	$8, %eax
	movq	%r11, -144(%rbp)
	cmovns	%eax, %edx
	movq	-112(%rbp), %rsi
	sarl	$3, %edx
	addq	$7, %rsi
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-120(%rbp), %rsi
	movq	-136(%rbp), %rcx
	movq	%r12, %rax
	lock cmpxchgq	%rcx, (%rsi)
	movq	-144(%rbp), %r11
	cmpq	%rax, %r12
	jne	.L1952
	cmpb	$0, 768(%r14)
	jne	.L1953
.L1829:
	cmpb	$0, 769(%r14)
	jne	.L1954
.L1831:
	movq	8(%r14), %rdi
	movq	-112(%rbp), %rdx
	leaq	64(%r14), %rcx
	movq	%r12, %rsi
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	movq	0(%r13), %rax
	movq	-120(%rbp), %r11
	andl	$2, %eax
	orq	%r11, %rax
	movq	%rax, 0(%r13)
	cmpb	$5, %bl
	ja	.L1955
.L1847:
	movslq	%r15d, %r8
	movl	$1, %eax
	addq	%r8, 128(%r14)
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1950:
	movq	-88(%rbp), %rcx
	movl	%eax, %r8d
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Scavenger18EvacuateThinStringINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ThinStringEi
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1794:
	movq	-88(%rbp), %rcx
	movl	%eax, %r8d
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Scavenger25EvacuateShortcutCandidateINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultENS0_3MapET_NS0_10ConsStringEi
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1800:
	xorl	%ecx, %ecx
	movl	$2, %edx
	movl	%r15d, %esi
	leaq	136(%r14), %rdi
	call	_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r11
	testb	$1, %al
	je	.L1801
	leaq	-1(%rax), %rax
	leal	-1(%r15), %edx
	movq	%rax, -136(%rbp)
	movl	%r15d, %eax
	leaq	7(%r11), %rdi
	movq	%r12, -1(%r11)
	subl	$8, %eax
	movq	%r11, -144(%rbp)
	cmovns	%eax, %edx
	movq	-112(%rbp), %rsi
	sarl	$3, %edx
	addq	$7, %rsi
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-120(%rbp), %rsi
	movq	-136(%rbp), %rcx
	movq	%r12, %rax
	lock cmpxchgq	%rcx, (%rsi)
	movq	-144(%rbp), %r11
	cmpq	%rax, %r12
	jne	.L1922
	cmpb	$0, 768(%r14)
	jne	.L1956
.L1804:
	cmpb	$0, 769(%r14)
	jne	.L1957
.L1853:
	movq	8(%r14), %rdi
	movq	-112(%rbp), %rdx
	leaq	64(%r14), %rcx
	movq	%r12, %rsi
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal4Heap20UpdateAllocationSiteENS0_3MapENS0_10HeapObjectEPSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS5_ESaISt4pairIKS5_mEEE
	movq	0(%r13), %rax
	movq	-120(%rbp), %r11
	andl	$2, %eax
	orq	%r11, %rax
	movq	%rax, 0(%r13)
	cmpb	$5, %bl
	ja	.L1958
.L1869:
	movslq	%r15d, %r8
	xorl	%eax, %eax
	addq	%r8, 120(%r14)
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1823:
	xorl	%ecx, %ecx
	movl	$2, %edx
	movl	%r15d, %esi
	leaq	136(%r14), %rdi
	call	_ZN2v88internal14LocalAllocator18AllocateInNewSpaceEiNS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r11
	testb	$1, %al
	je	.L1959
	leaq	-1(%rax), %rax
	leal	-1(%r15), %edx
	movq	%rax, -136(%rbp)
	movl	%r15d, %eax
	leaq	7(%r11), %rdi
	movq	%r12, -1(%r11)
	subl	$8, %eax
	movq	%r11, -144(%rbp)
	cmovns	%eax, %edx
	movq	-112(%rbp), %rsi
	sarl	$3, %edx
	addq	$7, %rsi
	movslq	%edx, %rdx
	call	_ZN2v88internal8CopyImplILm16EmEEvPT0_PKS2_m
	movq	-120(%rbp), %rsi
	movq	-136(%rbp), %rcx
	movq	%r12, %rax
	lock cmpxchgq	%rcx, (%rsi)
	movq	-144(%rbp), %r11
	cmpq	%rax, %r12
	jne	.L1922
	cmpb	$0, 768(%r14)
	jne	.L1960
.L1851:
	cmpb	$0, 769(%r14)
	je	.L1853
	movl	%r11d, %eax
	movq	%r11, %r8
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r8
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	salq	$2, %rax
	sall	%cl, %edx
	movq	16(%r8), %rcx
	addq	%rax, %rcx
	movl	(%rcx), %esi
	testl	%esi, %edx
	jne	.L1854
.L1859:
	movq	-112(%rbp), %rcx
	movl	$1, %edi
	movl	%ecx, %esi
	movq	%rcx, %r10
	andl	$262143, %esi
	andq	$-262144, %r10
	movl	%esi, %ecx
	shrl	$8, %esi
	leaq	0(,%rsi,4), %r9
	movq	16(%r10), %rsi
	shrl	$3, %ecx
	sall	%cl, %edi
	addq	%r9, %rsi
	movl	%edi, %ecx
	movl	(%rsi), %edi
	testl	%edi, %ecx
	jne	.L1961
.L1856:
	movq	16(%r10), %rsi
	addq	%r9, %rsi
	movl	(%rsi), %edi
	testl	%edi, %ecx
	je	.L1853
	addl	%ecx, %ecx
	jne	.L1862
	addq	$4, %rsi
	movl	$1, %ecx
.L1862:
	movl	(%rsi), %esi
	testl	%ecx, %esi
	je	.L1853
	addq	16(%r8), %rax
	movq	%rax, %rsi
.L1863:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L1853
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L1863
	movl	-136(%rbp), %eax
	movl	$1, %edx
	movq	%r11, -80(%rbp)
	subl	%r8d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r8), %rdx
	leaq	(%rdx,%rax,4), %rdx
	movl	(%rdx), %eax
	testl	%eax, %ecx
	je	.L1853
	addl	%ecx, %ecx
	jne	.L1867
	addq	$4, %rdx
	movl	$1, %ecx
.L1867:
	movl	(%rdx), %esi
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %ecx
	je	.L1853
	movl	%esi, %edi
	movl	%esi, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %esi
	jne	.L1867
.L1917:
	leaq	-80(%rbp), %rdi
	movq	%r8, -128(%rbp)
	movq	%r11, -120(%rbp)
	call	_ZNK2v88internal10HeapObject4SizeEv
	movq	-128(%rbp), %r8
	cltq
	lock addq	%rax, 96(%r8)
	movq	-120(%rbp), %r11
	jmp	.L1853
	.p2align 4,,10
	.p2align 3
.L1922:
	movq	-128(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r11, %rsi
	call	_ZN2v88internal14LocalAllocator18FreeLastInNewSpaceENS0_10HeapObjectEi
.L1946:
	movq	-112(%rbp), %rax
	movq	-1(%rax), %rax
	addq	$1, %rax
	movq	0(%r13), %rdx
	andl	$2, %edx
	orq	%rax, %rdx
	movl	$1, %eax
	movq	%rdx, 0(%r13)
	testb	$1, %dl
	je	.L1791
	cmpl	$3, %edx
	je	.L1791
	andq	$-262144, %rdx
	movq	8(%rdx), %rax
	shrq	$4, %rax
	xorq	$1, %rax
	andl	$1, %eax
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1952:
	movq	-128(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r11, %rsi
	call	_ZN2v88internal14LocalAllocator18FreeLastInOldSpaceENS0_10HeapObjectEi
	jmp	.L1946
	.p2align 4,,10
	.p2align 3
.L1951:
	movq	-112(%rbp), %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$24, %al
	je	.L1797
	testb	$32, %al
	je	.L1797
	movq	-120(%rbp), %rcx
	movq	%r12, %rax
	lock cmpxchgq	%rcx, (%rcx)
	cmpq	%rax, %r12
	je	.L1798
.L1938:
	xorl	%eax, %eax
	jmp	.L1791
.L1798:
	movq	-112(%rbp), %xmm0
	movq	%r12, %xmm1
	leaq	-80(%rbp), %rsi
	leaq	656(%r14), %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZNSt13unordered_mapIN2v88internal10HeapObjectENS1_3MapENS1_6Object6HasherESt8equal_toIS2_ESaISt4pairIKS2_S3_EEE6insertEOSA_
	movslq	%r15d, %rax
	addq	%rax, 128(%r14)
	cmpb	$5, %bl
	movdqa	-112(%rbp), %xmm0
	jbe	.L1938
	movq	16(%r14), %rdi
	movl	24(%r14), %esi
	movaps	%xmm0, -80(%rbp)
	movl	%r15d, -64(%rbp)
	pushq	%rax
	addq	$696, %rdi
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EE4PushEiS3_
	addq	$32, %rsp
	xorl	%eax, %eax
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1954:
	movl	%r11d, %eax
	movq	%r11, %r8
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r8
	movl	%eax, %ecx
	shrl	$8, %eax
	leaq	0(,%rax,4), %r9
	movq	16(%r8), %rax
	shrl	$3, %ecx
	sall	%cl, %edx
	addq	%r9, %rax
	movl	(%rax), %ecx
	testl	%ecx, %edx
	jne	.L1832
.L1837:
	movq	-112(%rbp), %rax
	movl	$1, %edi
	movq	%rax, %rsi
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	movq	16(%rsi), %r10
	shrl	$3, %ecx
	salq	$2, %rax
	sall	%cl, %edi
	addq	%rax, %r10
	movl	%edi, %ecx
	movl	(%r10), %edi
	testl	%edi, %ecx
	jne	.L1962
.L1834:
	addq	16(%rsi), %rax
	movl	(%rax), %esi
	testl	%esi, %ecx
	je	.L1831
	addl	%ecx, %ecx
	jne	.L1840
	addq	$4, %rax
	movl	$1, %ecx
.L1840:
	movl	(%rax), %eax
	testl	%ecx, %eax
	je	.L1831
	movq	16(%r8), %rsi
	addq	%r9, %rsi
	.p2align 4,,10
	.p2align 3
.L1841:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L1831
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L1841
	movl	-136(%rbp), %eax
	movl	$1, %edx
	movq	%r11, -80(%rbp)
	subl	%r8d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r8), %rdx
	leaq	(%rdx,%rax,4), %rdx
	movl	(%rdx), %eax
	testl	%eax, %ecx
	je	.L1831
	addl	%ecx, %ecx
	jne	.L1845
	addq	$4, %rdx
	movl	$1, %ecx
.L1845:
	movl	(%rdx), %esi
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %ecx
	je	.L1831
	movl	%esi, %edi
	movl	%esi, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %esi
	jne	.L1845
	leaq	-80(%rbp), %rdi
	movq	%r8, -128(%rbp)
	movq	%r11, -120(%rbp)
	call	_ZNK2v88internal10HeapObject4SizeEv
	movq	-128(%rbp), %r8
	cltq
	lock addq	%rax, 96(%r8)
	movq	-120(%rbp), %r11
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1957:
	movl	%r11d, %eax
	movq	%r11, %r8
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r8
	movl	%eax, %ecx
	shrl	$8, %eax
	leaq	0(,%rax,4), %r9
	movq	16(%r8), %rax
	shrl	$3, %ecx
	sall	%cl, %edx
	addq	%r9, %rax
	movl	(%rax), %ecx
	testl	%ecx, %edx
	jne	.L1807
.L1812:
	movq	-112(%rbp), %rax
	movl	%eax, %edi
	movq	%rax, %rsi
	movl	$1, %eax
	andl	$262143, %edi
	andq	$-262144, %rsi
	movl	%edi, %ecx
	shrl	$8, %edi
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%edi, %ecx
	leaq	0(,%rcx,4), %r10
	movq	16(%rsi), %rcx
	addq	%r10, %rcx
	movl	(%rcx), %edi
	testl	%edi, %eax
	jne	.L1963
.L1809:
	addq	16(%rsi), %r10
	movl	(%r10), %ecx
	movq	%r10, %rsi
	testl	%ecx, %eax
	je	.L1853
	addl	%eax, %eax
	jne	.L1815
	addq	$4, %rsi
	movl	$1, %eax
.L1815:
	movl	(%rsi), %ecx
	testl	%eax, %ecx
	je	.L1853
	movq	16(%r8), %rsi
	addq	%r9, %rsi
.L1816:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L1853
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L1816
	movl	-136(%rbp), %eax
	movl	$1, %edx
	movq	%r11, -80(%rbp)
	subl	%r8d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r8), %rdx
	leaq	(%rdx,%rax,4), %rdx
	movl	(%rdx), %eax
	testl	%eax, %ecx
	je	.L1853
	addl	%ecx, %ecx
	jne	.L1820
	addq	$4, %rdx
	movl	$1, %ecx
.L1820:
	movl	(%rdx), %esi
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %ecx
	je	.L1853
	movl	%esi, %edi
	movl	%esi, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %esi
	jne	.L1820
	jmp	.L1917
	.p2align 4,,10
	.p2align 3
.L1959:
	movq	8(%r14), %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1958:
	movq	32(%r14), %rdi
	movl	40(%r14), %esi
	movl	%r15d, %ecx
	movq	%r11, %rdx
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_
	jmp	.L1869
.L1955:
	movl	24(%r14), %esi
	movq	16(%r14), %rdi
	movl	%r15d, %ecx
	movq	%r11, %rdx
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EE4PushEiS4_
	jmp	.L1847
.L1832:
	movl	%edx, %ecx
	addl	%ecx, %ecx
	jne	.L1835
	addq	$4, %rax
	movl	$1, %ecx
.L1835:
	movl	(%rax), %eax
	testl	%ecx, %eax
	jne	.L1831
	jmp	.L1837
.L1953:
	movq	8(%r14), %rdi
	movq	-112(%rbp), %rdx
	movq	%r11, %rsi
	movl	%r15d, %ecx
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	movq	-120(%rbp), %r11
	jmp	.L1829
.L1807:
	movl	%edx, %ecx
	addl	%ecx, %ecx
	jne	.L1810
	addq	$4, %rax
	movl	$1, %ecx
.L1810:
	movl	(%rax), %eax
	testl	%ecx, %eax
	jne	.L1853
	jmp	.L1812
.L1956:
	movq	8(%r14), %rdi
	movq	-112(%rbp), %rdx
	movq	%r11, %rsi
	movl	%r15d, %ecx
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	movq	-120(%rbp), %r11
	jmp	.L1804
.L1962:
	movl	%ecx, %edi
	addl	%edi, %edi
	movl	%edi, -120(%rbp)
	jne	.L1838
	movl	$1, -120(%rbp)
	addq	$4, %r10
.L1838:
	movl	(%r10), %edi
	testl	%edi, -120(%rbp)
	jne	.L1834
	movq	16(%r8), %rsi
	addq	%r9, %rsi
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1964:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L1831
.L1839:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1964
	jmp	.L1831
.L1963:
	movl	%eax, %edi
	addl	%edi, %edi
	jne	.L1813
	addq	$4, %rcx
	movl	$1, %edi
.L1813:
	movl	(%rcx), %ecx
	testl	%edi, %ecx
	jne	.L1809
	movq	16(%r8), %rsi
	addq	%r9, %rsi
	jmp	.L1814
	.p2align 4,,10
	.p2align 3
.L1965:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L1853
.L1814:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1965
	jmp	.L1853
.L1854:
	movl	%edx, %esi
	addl	%esi, %esi
	jne	.L1857
	addq	$4, %rcx
	movl	$1, %esi
.L1857:
	movl	(%rcx), %ecx
	testl	%esi, %ecx
	jne	.L1853
	jmp	.L1859
.L1960:
	movq	8(%r14), %rdi
	movq	-112(%rbp), %rdx
	movq	%r11, %rsi
	movl	%r15d, %ecx
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal4Heap11OnMoveEventENS0_10HeapObjectES2_i@PLT
	movq	-120(%rbp), %r11
	jmp	.L1851
.L1961:
	movl	%ecx, %edi
	addl	%edi, %edi
	jne	.L1860
	addq	$4, %rsi
	movl	$1, %edi
.L1860:
	movl	(%rsi), %esi
	testl	%edi, %esi
	jne	.L1856
	addq	16(%r8), %rax
	movq	%rax, %rsi
	jmp	.L1861
.L1966:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L1853
.L1861:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1966
	jmp	.L1853
.L1949:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30403:
	.size	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0, .-_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	.section	.text._ZN2v88internal19RootScavengeVisitor15ScavengePointerENS0_14FullObjectSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19RootScavengeVisitor15ScavengePointerENS0_14FullObjectSlotE
	.type	_ZN2v88internal19RootScavengeVisitor15ScavengePointerENS0_14FullObjectSlotE, @function
_ZN2v88internal19RootScavengeVisitor15ScavengePointerENS0_14FullObjectSlotE:
.LFB22523:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	testb	$1, %dl
	je	.L1967
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1967
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	.p2align 4,,10
	.p2align 3
.L1967:
	ret
	.cfi_endproc
.LFE22523:
	.size	_ZN2v88internal19RootScavengeVisitor15ScavengePointerENS0_14FullObjectSlotE, .-_ZN2v88internal19RootScavengeVisitor15ScavengePointerENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal19RootScavengeVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19RootScavengeVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.type	_ZN2v88internal19RootScavengeVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, @function
_ZN2v88internal19RootScavengeVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_:
.LFB22522:
	.cfi_startproc
	endbr64
	cmpq	%r8, %rcx
	jnb	.L1981
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L1975:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L1974
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1974
	movq	8(%r13), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
.L1974:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	ja	.L1975
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1981:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE22522:
	.size	_ZN2v88internal19RootScavengeVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, .-_ZN2v88internal19RootScavengeVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.section	.text._ZN2v88internal19RootScavengeVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19RootScavengeVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal19RootScavengeVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal19RootScavengeVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB22521:
	.cfi_startproc
	endbr64
	movq	(%rcx), %rdx
	testb	$1, %dl
	je	.L1984
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1984
	movq	8(%rdi), %rdi
	movq	%rcx, %rsi
	jmp	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	.p2align 4,,10
	.p2align 3
.L1984:
	ret
	.cfi_endproc
.LFE22521:
	.size	_ZN2v88internal19RootScavengeVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal19RootScavengeVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_15ScavengeVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_15ScavengeVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_.constprop.0, @function
_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_15ScavengeVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_.constprop.0:
.LFB30410:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	47(%rdi), %rax
	testq	%rax, %rax
	jne	.L1990
	movslq	%edx, %r8
	leaq	7(%rsi), %r12
	leaq	-1(%rsi,%r8), %rbx
	cmpq	%r12, %rbx
	ja	.L1991
	.p2align 4,,10
	.p2align 3
.L1989:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2021
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2022:
	.cfi_restore_state
	movq	8(%r13), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	.p2align 4,,10
	.p2align 3
.L1993:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jbe	.L1989
.L1991:
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L1993
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1993
	jmp	.L2022
	.p2align 4,,10
	.p2align 3
.L1990:
	movq	$0, -72(%rbp)
	movb	$1, -80(%rbp)
	movl	$0, -76(%rbp)
	movq	47(%rdi), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L2023
.L1995:
	leaq	-1(%rsi), %rax
	movl	$8, %ebx
	leaq	-84(%rbp), %r15
	movq	%rax, -112(%rbp)
	leaq	-80(%rbp), %r14
	cmpl	$8, %r12d
	jg	.L1996
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L2020:
	movslq	-84(%rbp), %rbx
.L1998:
	cmpl	%ebx, %r12d
	jle	.L1989
.L1996:
	movq	%r15, %rcx
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L2020
	movslq	-84(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	%rcx, %rax
	addq	%rsi, %rbx
	addq	%rsi, %rcx
	cmpq	%rbx, %rcx
	ja	.L2001
	jmp	.L2005
	.p2align 4,,10
	.p2align 3
.L2000:
	addq	$8, %rbx
	cmpq	%rbx, %rcx
	jbe	.L2020
.L2001:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2000
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2000
	movq	8(%r13), %rdi
	movq	%rbx, %rsi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-104(%rbp), %rcx
	jmp	.L2000
	.p2align 4,,10
	.p2align 3
.L2023:
	movzbl	8(%rdi), %eax
	movb	$0, -80(%rbp)
	sall	$3, %eax
	movl	%eax, -76(%rbp)
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L2005:
	movslq	%eax, %rbx
	jmp	.L1998
.L2021:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30410:
	.size	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_15ScavengeVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_.constprop.0, .-_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_15ScavengeVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_.constprop.0
	.section	.text._ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm
	.type	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm, @function
_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm:
.LFB28584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L2025
	movq	(%rbx), %r8
.L2026:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L2035
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L2036:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2025:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L2049
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2050
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L2028:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L2030
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2032:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2033:
	testq	%rsi, %rsi
	je	.L2030
.L2031:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L2032
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L2038
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L2031
	.p2align 4,,10
	.p2align 3
.L2030:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L2034
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L2034:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2035:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L2037
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L2037:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L2036
	.p2align 4,,10
	.p2align 3
.L2038:
	movq	%rdx, %rdi
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2049:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L2028
.L2050:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE28584:
	.size	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm, .-_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm
	.section	.text._ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKiNS1_10_AllocNodeISaINS1_10_Hash_nodeIiLb0EEEEEEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKiNS1_10_AllocNodeISaINS1_10_Hash_nodeIiLb0EEEEEEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0, @function
_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKiNS1_10_AllocNodeISaINS1_10_Hash_nodeIiLb0EEEEEEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0:
.LFB30406:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	(%rsi), %r14
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r14, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L2052
	movq	(%rax), %rcx
	movq	%r14, %r8
	movl	8(%rcx), %esi
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L2065:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2052
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%rdx, %r13
	jne	.L2052
.L2054:
	cmpl	%esi, %r8d
	jne	.L2065
	popq	%rbx
	movq	%rcx, %rax
	popq	%r12
	xorl	%edx, %edx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2052:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	(%rbx), %eax
	movl	$1, %r8d
	movl	%eax, 8(%rcx)
	call	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30406:
	.size	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKiNS1_10_AllocNodeISaINS1_10_Hash_nodeIiLb0EEEEEEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0, .-_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKiNS1_10_AllocNodeISaINS1_10_Hash_nodeIiLb0EEEEEEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0
	.section	.text._ZN2v88internal9Scavenger25RememberPromotedEphemeronENS0_18EphemeronHashTableEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Scavenger25RememberPromotedEphemeronENS0_18EphemeronHashTableEi
	.type	_ZN2v88internal9Scavenger25RememberPromotedEphemeronENS0_18EphemeronHashTableEi, @function
_ZN2v88internal9Scavenger25RememberPromotedEphemeronENS0_18EphemeronHashTableEi:
.LFB22455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$712, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-56(%rbp), %r14
	leaq	-128(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$160, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, -180(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -112(%rbp)
	leaq	-112(%rbp), %rsi
	movl	$0x3f800000, -144(%rbp)
	movq	$0, -136(%rbp)
	movdqa	-144(%rbp), %xmm0
	movq	%r13, -176(%rbp)
	movq	$1, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -128(%rbp)
	movq	$1, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -56(%rbp)
	movq	%r14, -104(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZNSt10_HashtableIN2v88internal18EphemeronHashTableESt4pairIKS2_St13unordered_setIiSt4hashIiESt8equal_toIiESaIiEEESaISC_ENSt8__detail10_Select1stES8_IS2_ENS1_6Object6HasherENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJSC_EEES3_INSE_14_Node_iteratorISC_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	movq	-88(%rbp), %r12
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.L2067
	.p2align 4,,10
	.p2align 3
.L2068:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L2068
.L2067:
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-104(%rbp), %rdi
	movq	$0, -80(%rbp)
	movq	$0, -88(%rbp)
	cmpq	%r14, %rdi
	je	.L2069
	call	_ZdlPv@PLT
.L2069:
	movq	-160(%rbp), %r12
	testq	%r12, %r12
	je	.L2070
	.p2align 4,,10
	.p2align 3
.L2071:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L2071
.L2070:
	movq	-168(%rbp), %rax
	movq	-176(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-176(%rbp), %rdi
	movq	$0, -152(%rbp)
	movq	$0, -160(%rbp)
	cmpq	%r13, %rdi
	je	.L2072
	call	_ZdlPv@PLT
.L2072:
	leaq	-180(%rbp), %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKiNS1_10_AllocNodeISaINS1_10_Hash_nodeIiLb0EEEEEEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2083
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2083:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22455:
	.size	_ZN2v88internal9Scavenger25RememberPromotedEphemeronENS0_18EphemeronHashTableEi, .-_ZN2v88internal9Scavenger25RememberPromotedEphemeronENS0_18EphemeronHashTableEi
	.section	.text._ZN2v88internal40IterateAndScavengePromotedObjectsVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.type	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, @function
_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_:
.LFB22355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	$-9, %r8
	ja	.L2085
	movq	(%r8), %rdx
	movq	%r8, %r12
	testb	$1, %dl
	jne	.L2117
.L2085:
	movq	0(%r13), %rdx
	testb	$1, %dl
	je	.L2084
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$24, %al
	jne	.L2118
	cmpq	$-9, %r13
	ja	.L2084
	testb	$8, %al
	jne	.L2119
	cmpb	$0, 16(%rbx)
	jne	.L2120
.L2084:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2117:
	.cfi_restore_state
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$8, %al
	jne	.L2121
	cmpb	$0, 16(%rdi)
	je	.L2085
	testb	$64, %al
	je	.L2085
	movq	%rsi, %rdi
	movq	%r8, %rsi
	andq	$-262144, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2121:
	movq	8(%rdi), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	testl	%eax, %eax
	jne	.L2085
	movq	%r14, %rdx
	andq	$-262144, %rdx
	movq	104(%rdx), %rax
	testq	%rax, %rax
	je	.L2122
.L2091:
	movq	%r12, %rcx
	andl	$262143, %r12d
	subq	%rdx, %rcx
	movl	%r12d, %r9d
	movl	%r12d, %edx
	sarl	$13, %r12d
	shrq	$18, %rcx
	movslq	%r12d, %r12
	sarl	$8, %edx
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$3, %r9d
	andl	$31, %edx
	salq	$7, %rcx
	andl	$31, %r9d
	leaq	(%rcx,%r12,8), %r12
	addq	%rax, %r12
	movq	(%r12), %r8
	testq	%r8, %r8
	je	.L2123
.L2093:
	movl	%r9d, %ecx
	movl	$1, %eax
	movslq	%edx, %rdx
	sall	%cl, %eax
	leaq	(%r8,%rdx,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L2085
	jmp	.L2096
	.p2align 4,,10
	.p2align 3
.L2118:
	movq	8(%rbx), %rdi
	addq	$24, %rsp
	movl	%r15d, %edx
	movq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Scavenger25RememberPromotedEphemeronENS0_18EphemeronHashTableEi
	.p2align 4,,10
	.p2align 3
.L2124:
	.cfi_restore_state
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L2085
.L2096:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L2124
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2119:
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	testl	%eax, %eax
	jne	.L2084
	addq	$24, %rsp
	movq	%r14, %rdi
	movq	%r13, %rsi
	popq	%rbx
	andq	$-262144, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	.p2align 4,,10
	.p2align 3
.L2120:
	.cfi_restore_state
	testb	$64, %al
	je	.L2084
	addq	$24, %rsp
	movq	%r14, %rdi
	movq	%r13, %rsi
	popq	%rbx
	andq	$-262144, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	.p2align 4,,10
	.p2align 3
.L2123:
	.cfi_restore_state
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movl	%r9d, -60(%rbp)
	movl	%edx, -56(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-56(%rbp), %edx
	movl	-60(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L2125
.L2094:
	leaq	8(%r8), %rdi
	movq	%r8, %rcx
	xorl	%eax, %eax
	movq	$0, (%r8)
	andq	$-8, %rdi
	movq	$0, 120(%r8)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r8, (%r12)
	testq	%rax, %rax
	je	.L2093
	movq	%r8, %rdi
	movl	%r9d, -60(%rbp)
	movl	%edx, -56(%rbp)
	call	_ZdaPv@PLT
	movq	(%r12), %r8
	movl	-60(%rbp), %r9d
	movl	-56(%rbp), %edx
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2122:
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L2091
.L2125:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-56(%rbp), %edx
	movl	-60(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r8
	jne	.L2094
	leaq	.LC6(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE22355:
	.size	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, .-_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal9Scavenger8FinalizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Scavenger8FinalizeEv
	.type	_ZN2v88internal9Scavenger8FinalizeEv, @function
_ZN2v88internal9Scavenger8FinalizeEv:
.LFB22515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4Heap38MergeAllocationSitePretenuringFeedbackERKSt13unordered_mapINS0_14AllocationSiteEmNS0_6Object6HasherESt8equal_toIS3_ESaISt4pairIKS3_mEEE@PLT
	movq	8(%r14), %rax
	movq	120(%r14), %rdx
	leaq	656(%r14), %rsi
	addq	%rdx, 1944(%rax)
	movq	8(%r14), %rax
	movq	128(%r14), %rdx
	addq	%rdx, 1920(%rax)
	movq	(%r14), %rdi
	call	_ZN2v88internal18ScavengerCollector29MergeSurvivingNewLargeObjectsERKSt13unordered_mapINS0_10HeapObjectENS0_3MapENS0_6Object6HasherESt8equal_toIS3_ESaISt4pairIKS3_S4_EEE
	movq	136(%r14), %rax
	leaq	160(%r14), %rsi
	movq	256(%rax), %rdi
	call	_ZN2v88internal10PagedSpace20MergeCompactionSpaceEPNS0_15CompactionSpaceE@PLT
	movq	136(%r14), %rax
	leaq	392(%r14), %rsi
	movq	264(%rax), %rdi
	call	_ZN2v88internal10PagedSpace20MergeCompactionSpaceEPNS0_15CompactionSpaceE@PLT
	leaq	624(%r14), %rdi
	call	_ZN2v88internal21LocalAllocationBuffer5CloseEv@PLT
	movq	144(%r14), %rcx
	cmpq	%rdx, 104(%rcx)
	jne	.L2127
	testq	%rdx, %rdx
	jne	.L2165
.L2127:
	movslq	56(%r14), %rax
	movq	48(%r14), %r12
	leaq	(%rax,%rax,4), %rbx
	salq	$4, %rbx
	addq	%r12, %rbx
	movq	(%rbx), %r13
	cmpq	$0, 8(%r13)
	jne	.L2166
.L2128:
	movq	8(%rbx), %r13
	cmpq	$0, 8(%r13)
	jne	.L2167
.L2129:
	movq	728(%r14), %r13
	testq	%r13, %r13
	je	.L2126
	leaq	-144(%rbp), %rax
	leaq	-128(%rbp), %r12
	movq	%rax, -208(%rbp)
	leaq	-72(%rbp), %rax
	movq	%rax, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L2130:
	movq	-208(%rbp), %rax
	movq	8(%r14), %rdi
	movl	$0x3f800000, -160(%rbp)
	movq	%r12, %rsi
	movq	$0, -152(%rbp)
	movdqa	-160(%rbp), %xmm0
	movq	%rax, -192(%rbp)
	movq	$1, -184(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -144(%rbp)
	movq	8(%r13), %rax
	movq	$1, -112(%rbp)
	movq	%rax, -128(%rbp)
	movq	-200(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -120(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZNSt10_HashtableIN2v88internal18EphemeronHashTableESt4pairIKS2_St13unordered_setIiSt4hashIiESt8equal_toIiESaIiEEESaISC_ENSt8__detail10_Select1stES8_IS2_ENS1_6Object6HasherENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJSC_EEES3_INSE_14_Node_iteratorISC_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	movq	-104(%rbp), %r15
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.L2134
	.p2align 4,,10
	.p2align 3
.L2131:
	movq	%r15, %rdi
	movq	(%r15), %r15
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L2131
.L2134:
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, -96(%rbp)
	movq	-120(%rbp), %rdi
	movq	$0, -104(%rbp)
	cmpq	-200(%rbp), %rdi
	je	.L2132
	call	_ZdlPv@PLT
.L2132:
	movq	-176(%rbp), %r15
	testq	%r15, %r15
	je	.L2138
	.p2align 4,,10
	.p2align 3
.L2135:
	movq	%r15, %rdi
	movq	(%r15), %r15
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L2135
.L2138:
	movq	-184(%rbp), %rax
	movq	-192(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-192(%rbp), %rdi
	movq	$0, -168(%rbp)
	movq	$0, -176(%rbp)
	cmpq	-208(%rbp), %rdi
	je	.L2136
	call	_ZdlPv@PLT
.L2136:
	movq	32(%r13), %r15
	addq	$16, %rbx
	testq	%r15, %r15
	je	.L2142
	.p2align 4,,10
	.p2align 3
.L2141:
	movl	8(%r15), %eax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%eax, -128(%rbp)
	call	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKiNS1_10_AllocNodeISaINS1_10_Hash_nodeIiLb0EEEEEEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L2141
.L2142:
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L2130
.L2126:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2168
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2167:
	.cfi_restore_state
	leaq	640(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r12), %rax
	movq	%r15, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 680(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 8(%rbx)
	jmp	.L2129
.L2166:
	leaq	640(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r12), %rax
	movq	%r15, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 680(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%rbx)
	jmp	.L2128
.L2165:
	movq	%rax, 104(%rcx)
	jmp	.L2127
.L2168:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22515:
	.size	_ZN2v88internal9Scavenger8FinalizeEv, .-_ZN2v88internal9Scavenger8FinalizeEv
	.section	.text._ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_,"axG",@progbits,_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	.type	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_, @function
_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_:
.LFB29007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	andq	$-262144, %r13
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-1(%rdi), %rbx
	leaq	(%rdx,%rbx), %r12
	addq	%rsi, %rbx
	subq	$24, %rsp
	cmpq	%r12, %rbx
	jnb	.L2169
	movq	%rcx, %r14
	jmp	.L2170
	.p2align 4,,10
	.p2align 3
.L2173:
	cmpb	$0, 16(%r14)
	jne	.L2206
.L2174:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jbe	.L2169
.L2170:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2174
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$8, %al
	je	.L2173
	movq	8(%r14), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	testl	%eax, %eax
	jne	.L2174
	movq	104(%r13), %r15
	testq	%r15, %r15
	je	.L2207
.L2176:
	movq	%rbx, %rcx
	movl	%ebx, %edx
	subq	%r13, %rcx
	andl	$262143, %edx
	shrq	$18, %rcx
	movl	%edx, %r9d
	movl	%edx, %r10d
	sarl	$13, %edx
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$8, %r9d
	movslq	%edx, %rdx
	sarl	$3, %r10d
	salq	$7, %rcx
	andl	$31, %r9d
	andl	$31, %r10d
	leaq	(%rcx,%rdx,8), %rax
	addq	%rax, %r15
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L2208
.L2178:
	movl	%r10d, %ecx
	movl	$1, %eax
	movslq	%r9d, %r9
	sall	%cl, %eax
	leaq	(%r8,%r9,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	je	.L2180
	addq	$8, %rbx
	cmpq	%rbx, %r12
	ja	.L2170
.L2169:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2206:
	.cfi_restore_state
	testb	$64, %al
	je	.L2174
	movq	112(%r13), %r15
	testq	%r15, %r15
	je	.L2209
.L2182:
	movq	%rbx, %rcx
	movl	%ebx, %edx
	subq	%r13, %rcx
	andl	$262143, %edx
	shrq	$18, %rcx
	movl	%edx, %r8d
	movl	%edx, %r9d
	sarl	$13, %edx
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$8, %r8d
	movslq	%edx, %rdx
	sarl	$3, %r9d
	salq	$7, %rcx
	andl	$31, %r8d
	andl	$31, %r9d
	leaq	(%rcx,%rdx,8), %rax
	addq	%rax, %r15
	movq	(%r15), %r10
	testq	%r10, %r10
	je	.L2210
.L2184:
	movl	%r9d, %ecx
	movl	$1, %eax
	movslq	%r8d, %r8
	sall	%cl, %eax
	leaq	(%r10,%r8,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L2174
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2211:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L2174
.L2180:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L2211
	jmp	.L2174
	.p2align 4,,10
	.p2align 3
.L2212:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L2174
.L2187:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L2212
	jmp	.L2174
	.p2align 4,,10
	.p2align 3
.L2210:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movl	%r9d, -56(%rbp)
	movl	%r8d, -52(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-52(%rbp), %r8d
	movl	-56(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L2213
.L2185:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, (%r15)
	testq	%rax, %rax
	je	.L2184
	movq	%r10, %rdi
	movl	%r9d, -56(%rbp)
	movl	%r8d, -52(%rbp)
	call	_ZdaPv@PLT
	movq	(%r15), %r10
	movl	-56(%rbp), %r9d
	movl	-52(%rbp), %r8d
	jmp	.L2184
	.p2align 4,,10
	.p2align 3
.L2209:
	movq	%r13, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	%rax, %r15
	jmp	.L2182
	.p2align 4,,10
	.p2align 3
.L2208:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movl	%r10d, -56(%rbp)
	movl	%r9d, -52(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-52(%rbp), %r9d
	movl	-56(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L2214
.L2179:
	leaq	8(%r8), %rdi
	movq	%r8, %rcx
	xorl	%eax, %eax
	movq	$0, (%r8)
	andq	$-8, %rdi
	movq	$0, 120(%r8)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r8, (%r15)
	testq	%rax, %rax
	je	.L2178
	movq	%r8, %rdi
	movl	%r10d, -56(%rbp)
	movl	%r9d, -52(%rbp)
	call	_ZdaPv@PLT
	movq	(%r15), %r8
	movl	-56(%rbp), %r10d
	movl	-52(%rbp), %r9d
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2207:
	movq	%r13, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv@PLT
	movq	%rax, %r15
	jmp	.L2176
.L2213:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-52(%rbp), %r8d
	movl	-56(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L2185
.L2186:
	leaq	.LC6(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L2214:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-52(%rbp), %r9d
	movl	-56(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %r8
	jne	.L2179
	jmp	.L2186
	.cfi_endproc
.LFE29007:
	.size	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_, .-_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	.section	.text._ZN2v88internal18EphemeronHashTable14BodyDescriptor11IterateBodyINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_,"axG",@progbits,_ZN2v88internal18EphemeronHashTable14BodyDescriptor11IterateBodyINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_,comdat
	.p2align 4
	.weak	_ZN2v88internal18EphemeronHashTable14BodyDescriptor11IterateBodyINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_
	.type	_ZN2v88internal18EphemeronHashTable14BodyDescriptor11IterateBodyINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_, @function
_ZN2v88internal18EphemeronHashTable14BodyDescriptor11IterateBodyINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_:
.LFB28490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$40, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rbx, %rdi
	subq	$40, %rsp
	movq	%rsi, -64(%rbp)
	movl	$16, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movslq	35(%rbx), %rax
	movq	%rbx, %rsi
	andq	$-262144, %rsi
	movl	%eax, -52(%rbp)
	testq	%rax, %rax
	jle	.L2215
	leaq	47(%rbx), %r15
	xorl	%r12d, %r12d
	addq	$39, %rbx
	movq	%rsi, %r13
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2217:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2239
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$24, %al
	jne	.L2295
	cmpq	%rbx, %r15
	jbe	.L2239
	testb	$8, %al
	jne	.L2296
	cmpb	$0, 16(%r14)
	jne	.L2297
.L2239:
	addl	$1, %r12d
	addq	$16, %r15
	addq	$16, %rbx
	cmpl	-52(%rbp), %r12d
	je	.L2215
.L2257:
	cmpq	$-9, %r15
	ja	.L2217
	movq	(%r15), %rdx
	testb	$1, %dl
	je	.L2217
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$8, %al
	jne	.L2298
	cmpb	$0, 16(%r14)
	je	.L2217
	testb	$64, %al
	je	.L2217
	movq	112(%r13), %rdx
	testq	%rdx, %rdx
	je	.L2299
.L2231:
	movq	%r15, %rax
	movl	%r15d, %ecx
	subq	%r13, %rax
	andl	$262143, %ecx
	shrq	$18, %rax
	movl	%ecx, %r11d
	movl	%ecx, %esi
	sarl	$13, %ecx
	leaq	(%rax,%rax,2), %rax
	sarl	$3, %esi
	movslq	%ecx, %rcx
	sarl	$8, %r11d
	salq	$7, %rax
	andl	$31, %esi
	andl	$31, %r11d
	leaq	(%rax,%rcx,8), %rax
	movl	%esi, %r8d
	addq	%rax, %rdx
	movq	(%rdx), %r10
	testq	%r10, %r10
	je	.L2300
.L2233:
	movl	%r8d, %ecx
	movl	$1, %eax
	movslq	%r11d, %r11
	sall	%cl, %eax
	leaq	(%r10,%r11,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L2217
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L2217
	.p2align 4,,10
	.p2align 3
.L2301:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L2217
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L2301
	jmp	.L2217
	.p2align 4,,10
	.p2align 3
.L2298:
	movq	8(%r14), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	testl	%eax, %eax
	jne	.L2217
	movq	104(%r13), %rdx
	testq	%rdx, %rdx
	je	.L2302
.L2223:
	movq	%r15, %rax
	movl	%r15d, %ecx
	subq	%r13, %rax
	andl	$262143, %ecx
	shrq	$18, %rax
	movl	%ecx, %r11d
	movl	%ecx, %esi
	sarl	$13, %ecx
	leaq	(%rax,%rax,2), %rax
	sarl	$3, %esi
	movslq	%ecx, %rcx
	sarl	$8, %r11d
	salq	$7, %rax
	andl	$31, %esi
	andl	$31, %r11d
	leaq	(%rax,%rcx,8), %rax
	movl	%esi, %r8d
	addq	%rax, %rdx
	movq	(%rdx), %r10
	testq	%r10, %r10
	je	.L2303
.L2225:
	movl	%r8d, %ecx
	movl	$1, %eax
	movslq	%r11d, %r11
	sall	%cl, %eax
	leaq	(%r10,%r11,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L2217
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L2217
	.p2align 4,,10
	.p2align 3
.L2304:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L2217
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L2304
	jmp	.L2217
	.p2align 4,,10
	.p2align 3
.L2296:
	movq	8(%r14), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	testl	%eax, %eax
	jne	.L2239
	movq	104(%r13), %rdx
	testq	%rdx, %rdx
	je	.L2305
.L2243:
	movq	%rbx, %rax
	movl	%ebx, %ecx
	subq	%r13, %rax
	andl	$262143, %ecx
	shrq	$18, %rax
	movl	%ecx, %r11d
	movl	%ecx, %esi
	sarl	$13, %ecx
	leaq	(%rax,%rax,2), %rax
	sarl	$3, %esi
	movslq	%ecx, %rcx
	sarl	$8, %r11d
	salq	$7, %rax
	andl	$31, %esi
	andl	$31, %r11d
	leaq	(%rax,%rcx,8), %rax
	movl	%esi, %r8d
	addq	%rax, %rdx
	movq	(%rdx), %r10
	testq	%r10, %r10
	je	.L2306
.L2245:
	movl	%r8d, %ecx
	movl	$1, %eax
	movslq	%r11d, %r11
	sall	%cl, %eax
	leaq	(%r10,%r11,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L2239
	jmp	.L2248
	.p2align 4,,10
	.p2align 3
.L2307:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L2239
.L2248:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L2307
	jmp	.L2239
	.p2align 4,,10
	.p2align 3
.L2297:
	testb	$64, %al
	je	.L2239
	movq	112(%r13), %rdx
	testq	%rdx, %rdx
	je	.L2308
.L2251:
	movq	%rbx, %rax
	movl	%ebx, %ecx
	subq	%r13, %rax
	andl	$262143, %ecx
	shrq	$18, %rax
	movl	%ecx, %r11d
	movl	%ecx, %esi
	sarl	$13, %ecx
	leaq	(%rax,%rax,2), %rax
	sarl	$3, %esi
	movslq	%ecx, %rcx
	sarl	$8, %r11d
	salq	$7, %rax
	andl	$31, %esi
	andl	$31, %r11d
	leaq	(%rax,%rcx,8), %rax
	movl	%esi, %r8d
	addq	%rax, %rdx
	movq	(%rdx), %r10
	testq	%r10, %r10
	je	.L2309
.L2253:
	movl	%r8d, %ecx
	movl	$1, %eax
	movslq	%r11d, %r11
	sall	%cl, %eax
	leaq	(%r10,%r11,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L2239
	jmp	.L2255
	.p2align 4,,10
	.p2align 3
.L2310:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L2239
.L2255:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L2310
	jmp	.L2239
	.p2align 4,,10
	.p2align 3
.L2295:
	movq	8(%r14), %rdi
	movq	-64(%rbp), %rsi
	movl	%r12d, %edx
	call	_ZN2v88internal9Scavenger25RememberPromotedEphemeronENS0_18EphemeronHashTableEi
	jmp	.L2239
	.p2align 4,,10
	.p2align 3
.L2215:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2303:
	.cfi_restore_state
	movl	%esi, -56(%rbp)
	movl	$128, %edi
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rdx, -80(%rbp)
	movl	%r11d, -72(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-72(%rbp), %r11d
	movl	-56(%rbp), %r8d
	testq	%rax, %rax
	movq	-80(%rbp), %rdx
	movq	%rax, %r10
	je	.L2311
.L2226:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, (%rdx)
	movq	%rdx, -72(%rbp)
	testq	%rax, %rax
	je	.L2225
	movq	%r10, %rdi
	movl	%r8d, -80(%rbp)
	movl	%r11d, -56(%rbp)
	call	_ZdaPv@PLT
	movq	-72(%rbp), %rdx
	movq	(%rdx), %r10
	movl	-80(%rbp), %r8d
	movl	-56(%rbp), %r11d
	jmp	.L2225
	.p2align 4,,10
	.p2align 3
.L2302:
	movq	%r13, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv@PLT
	movq	%rax, %rdx
	jmp	.L2223
	.p2align 4,,10
	.p2align 3
.L2305:
	movq	%r13, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv@PLT
	movq	%rax, %rdx
	jmp	.L2243
	.p2align 4,,10
	.p2align 3
.L2306:
	movl	%esi, -56(%rbp)
	movl	$128, %edi
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rdx, -80(%rbp)
	movl	%r11d, -72(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-72(%rbp), %r11d
	movl	-56(%rbp), %r8d
	testq	%rax, %rax
	movq	-80(%rbp), %rdx
	movq	%rax, %r10
	je	.L2312
.L2246:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, (%rdx)
	movq	%rdx, -72(%rbp)
	testq	%rax, %rax
	je	.L2245
	movq	%r10, %rdi
	movl	%r8d, -80(%rbp)
	movl	%r11d, -56(%rbp)
	call	_ZdaPv@PLT
	movq	-72(%rbp), %rdx
	movq	(%rdx), %r10
	movl	-80(%rbp), %r8d
	movl	-56(%rbp), %r11d
	jmp	.L2245
	.p2align 4,,10
	.p2align 3
.L2300:
	movl	%esi, -56(%rbp)
	movl	$128, %edi
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rdx, -80(%rbp)
	movl	%r11d, -72(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-72(%rbp), %r11d
	movl	-56(%rbp), %r8d
	testq	%rax, %rax
	movq	-80(%rbp), %rdx
	movq	%rax, %r10
	je	.L2313
.L2234:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, (%rdx)
	movq	%rdx, -72(%rbp)
	testq	%rax, %rax
	je	.L2233
	movq	%r10, %rdi
	movl	%r8d, -80(%rbp)
	movl	%r11d, -56(%rbp)
	call	_ZdaPv@PLT
	movq	-72(%rbp), %rdx
	movq	(%rdx), %r10
	movl	-80(%rbp), %r8d
	movl	-56(%rbp), %r11d
	jmp	.L2233
	.p2align 4,,10
	.p2align 3
.L2299:
	movq	%r13, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	%rax, %rdx
	jmp	.L2231
	.p2align 4,,10
	.p2align 3
.L2309:
	movl	%esi, -56(%rbp)
	movl	$128, %edi
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rdx, -80(%rbp)
	movl	%r11d, -72(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-72(%rbp), %r11d
	movl	-56(%rbp), %r8d
	testq	%rax, %rax
	movq	-80(%rbp), %rdx
	movq	%rax, %r10
	je	.L2314
.L2254:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, (%rdx)
	movq	%rdx, -72(%rbp)
	testq	%rax, %rax
	je	.L2253
	movq	%r10, %rdi
	movl	%r8d, -80(%rbp)
	movl	%r11d, -56(%rbp)
	call	_ZdaPv@PLT
	movq	-72(%rbp), %rdx
	movq	(%rdx), %r10
	movl	-80(%rbp), %r8d
	movl	-56(%rbp), %r11d
	jmp	.L2253
	.p2align 4,,10
	.p2align 3
.L2308:
	movq	%r13, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	%rax, %rdx
	jmp	.L2251
.L2314:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-72(%rbp), %r11d
	movl	-56(%rbp), %r8d
	testq	%rax, %rax
	movq	-80(%rbp), %rdx
	movq	%rax, %r10
	jne	.L2254
.L2235:
	leaq	.LC6(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L2313:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-72(%rbp), %r11d
	movl	-56(%rbp), %r8d
	testq	%rax, %rax
	movq	-80(%rbp), %rdx
	movq	%rax, %r10
	jne	.L2234
	jmp	.L2235
.L2311:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-72(%rbp), %r11d
	movl	-56(%rbp), %r8d
	testq	%rax, %rax
	movq	-80(%rbp), %rdx
	movq	%rax, %r10
	jne	.L2226
	jmp	.L2235
.L2312:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-72(%rbp), %r11d
	movl	-56(%rbp), %r8d
	testq	%rax, %rax
	movq	-80(%rbp), %rdx
	movq	%rax, %r10
	jne	.L2246
	jmp	.L2235
	.cfi_endproc
.LFE28490:
	.size	_ZN2v88internal18EphemeronHashTable14BodyDescriptor11IterateBodyINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_, .-_ZN2v88internal18EphemeronHashTable14BodyDescriptor11IterateBodyINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_
	.section	.text._ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_,"axG",@progbits,_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	.type	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_, @function
_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_:
.LFB29010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	andq	$-262144, %r13
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leaq	-1(%rdi), %rbx
	leaq	(%rdx,%rbx), %r12
	addq	%rsi, %rbx
	cmpq	%r12, %rbx
	jnb	.L2315
	movq	%rcx, %r14
	.p2align 4,,10
	.p2align 3
.L2316:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2318
	cmpl	$3, %edx
	je	.L2318
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$8, %al
	jne	.L2328
	cmpb	$0, 16(%r14)
	jne	.L2329
	.p2align 4,,10
	.p2align 3
.L2318:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	ja	.L2316
.L2315:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2328:
	.cfi_restore_state
	movq	8(%r14), %rdi
	andq	$-3, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	testl	%eax, %eax
	jne	.L2318
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2318
	.p2align 4,,10
	.p2align 3
.L2329:
	testb	$64, %al
	je	.L2318
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE6InsertILNS0_10AccessModeE0EEEvPNS0_11MemoryChunkEm
	jmp	.L2318
	.cfi_endproc
.LFE29010:
	.size	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_, .-_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	.section	.text._ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_,"axG",@progbits,_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	.type	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_, @function
_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_:
.LFB29014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%edx, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	47(%rdi), %rax
	testq	%rax, %rax
	jne	.L2331
	movq	%r8, %rcx
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
.L2330:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2385
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2331:
	.cfi_restore_state
	movq	$0, -72(%rbp)
	movb	$1, -80(%rbp)
	movl	$0, -76(%rbp)
	movq	47(%rdi), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L2333
	movzbl	8(%rdi), %eax
	movb	$0, -80(%rbp)
	sall	$3, %eax
	movl	%eax, -76(%rbp)
.L2333:
	leaq	-1(%r9), %rax
	andq	$-262144, %r9
	leaq	-84(%rbp), %r15
	movq	%rax, -120(%rbp)
	leaq	-80(%rbp), %r14
	movq	%r9, -104(%rbp)
	cmpl	%r12d, %ebx
	jl	.L2334
	jmp	.L2330
	.p2align 4,,10
	.p2align 3
.L2383:
	movslq	-84(%rbp), %rbx
.L2336:
	cmpl	%ebx, %r12d
	jle	.L2330
.L2334:
	movq	%r15, %rcx
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L2383
	movslq	-84(%rbp), %r8
	movq	-120(%rbp), %rsi
	movq	%r8, %rax
	addq	%rsi, %rbx
	addq	%rsi, %r8
	cmpq	%rbx, %r8
	jbe	.L2358
	movl	%r12d, -108(%rbp)
	movq	%r13, %r12
	movq	%r8, %r13
	jmp	.L2339
	.p2align 4,,10
	.p2align 3
.L2341:
	cmpb	$0, 16(%r12)
	jne	.L2386
.L2342:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2387
.L2339:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2342
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$8, %al
	je	.L2341
	movq	8(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	testl	%eax, %eax
	jne	.L2342
	movq	-104(%rbp), %rax
	movq	104(%rax), %rax
	testq	%rax, %rax
	je	.L2388
.L2344:
	movq	%rbx, %rcx
	movl	%ebx, %edx
	subq	-104(%rbp), %rcx
	andl	$262143, %edx
	shrq	$18, %rcx
	movl	%edx, %r9d
	movl	%edx, %r10d
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$13, %edx
	salq	$7, %rcx
	movslq	%edx, %rdx
	sarl	$8, %r9d
	leaq	(%rcx,%rdx,8), %rdx
	sarl	$3, %r10d
	andl	$31, %r9d
	addq	%rax, %rdx
	andl	$31, %r10d
	movq	(%rdx), %r11
	testq	%r11, %r11
	je	.L2389
.L2346:
	movl	%r10d, %ecx
	movl	$1, %eax
	movslq	%r9d, %r9
	sall	%cl, %eax
	leaq	(%r11,%r9,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	je	.L2348
	addq	$8, %rbx
	cmpq	%rbx, %r13
	ja	.L2339
	.p2align 4,,10
	.p2align 3
.L2387:
	movq	%r12, %r13
	movl	-108(%rbp), %r12d
	jmp	.L2383
	.p2align 4,,10
	.p2align 3
.L2390:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L2342
.L2348:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L2390
	jmp	.L2342
	.p2align 4,,10
	.p2align 3
.L2386:
	testb	$64, %al
	je	.L2342
	movq	-104(%rbp), %rax
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L2391
.L2350:
	movq	%rbx, %rcx
	movl	%ebx, %edx
	subq	-104(%rbp), %rcx
	andl	$262143, %edx
	shrq	$18, %rcx
	movl	%edx, %r9d
	movl	%edx, %r10d
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$13, %edx
	salq	$7, %rcx
	movslq	%edx, %rdx
	sarl	$8, %r9d
	leaq	(%rcx,%rdx,8), %rdx
	sarl	$3, %r10d
	andl	$31, %r9d
	addq	%rax, %rdx
	andl	$31, %r10d
	movq	(%rdx), %r11
	testq	%r11, %r11
	je	.L2392
.L2352:
	movl	%r10d, %ecx
	movl	$1, %eax
	movslq	%r9d, %r9
	sall	%cl, %eax
	leaq	(%r11,%r9,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L2342
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L2342
	.p2align 4,,10
	.p2align 3
.L2393:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L2342
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L2393
	jmp	.L2342
	.p2align 4,,10
	.p2align 3
.L2388:
	movq	-104(%rbp), %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv@PLT
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2389:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%rdx, -136(%rbp)
	movl	%r10d, -112(%rbp)
	movl	%r9d, -128(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-128(%rbp), %r9d
	movl	-112(%rbp), %r10d
	testq	%rax, %rax
	movq	-136(%rbp), %rdx
	movq	%rax, %r11
	je	.L2394
.L2347:
	leaq	8(%r11), %rdi
	movq	%r11, %rcx
	xorl	%eax, %eax
	movq	$0, (%r11)
	andq	$-8, %rdi
	movq	$0, 120(%r11)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r11, (%rdx)
	movq	%rdx, -128(%rbp)
	testq	%rax, %rax
	je	.L2346
	movq	%r11, %rdi
	movl	%r10d, -136(%rbp)
	movl	%r9d, -112(%rbp)
	call	_ZdaPv@PLT
	movq	-128(%rbp), %rdx
	movq	(%rdx), %r11
	movl	-136(%rbp), %r10d
	movl	-112(%rbp), %r9d
	jmp	.L2346
	.p2align 4,,10
	.p2align 3
.L2358:
	movslq	%eax, %rbx
	jmp	.L2336
	.p2align 4,,10
	.p2align 3
.L2391:
	movq	-104(%rbp), %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	jmp	.L2350
	.p2align 4,,10
	.p2align 3
.L2392:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%rdx, -136(%rbp)
	movl	%r10d, -112(%rbp)
	movl	%r9d, -128(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-128(%rbp), %r9d
	movl	-112(%rbp), %r10d
	testq	%rax, %rax
	movq	-136(%rbp), %rdx
	movq	%rax, %r11
	je	.L2395
.L2353:
	leaq	8(%r11), %rdi
	movq	%r11, %rcx
	xorl	%eax, %eax
	movq	$0, (%r11)
	andq	$-8, %rdi
	movq	$0, 120(%r11)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r11, (%rdx)
	movq	%rdx, -128(%rbp)
	testq	%rax, %rax
	je	.L2352
	movq	%r11, %rdi
	movl	%r10d, -136(%rbp)
	movl	%r9d, -112(%rbp)
	call	_ZdaPv@PLT
	movq	-128(%rbp), %rdx
	movq	(%rdx), %r11
	movl	-136(%rbp), %r10d
	movl	-112(%rbp), %r9d
	jmp	.L2352
.L2385:
	call	__stack_chk_fail@PLT
.L2395:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-128(%rbp), %r9d
	movl	-112(%rbp), %r10d
	testq	%rax, %rax
	movq	-136(%rbp), %rdx
	movq	%rax, %r11
	jne	.L2353
.L2354:
	leaq	.LC6(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L2394:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-128(%rbp), %r9d
	movl	-112(%rbp), %r10d
	testq	%rax, %rax
	movq	-136(%rbp), %rdx
	movq	%rax, %r11
	jne	.L2347
	jmp	.L2354
	.cfi_endproc
.LFE29014:
	.size	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_, .-_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	.section	.rodata._ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_.str1.1,"aMS",@progbits,1
.LC11:
	.string	"Unknown type: %d\n"
	.section	.text._ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,"axG",@progbits,_ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,comdat
	.p2align 4
	.weak	_ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_
	.type	_ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_, @function
_ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_:
.LFB26781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpw	$63, %di
	ja	.L2397
	andl	$7, %edi
	cmpw	$5, %di
	ja	.L2477
	leaq	.L2400(%rip), %rdx
	movzwl	%di, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,"aG",@progbits,_ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,comdat
	.align 4
	.align 4
.L2400:
	.long	.L2396-.L2400
	.long	.L2401-.L2400
	.long	.L2396-.L2400
	.long	.L2401-.L2400
	.long	.L2477-.L2400
	.long	.L2417-.L2400
	.section	.text._ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,"axG",@progbits,_ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,comdat
	.p2align 4,,10
	.p2align 3
.L2397:
	movq	%rsi, %r14
	movl	%ecx, %r13d
	cmpw	$168, %di
	ja	.L2480
	leal	-65(%rdi), %eax
	cmpw	$103, %ax
	ja	.L2417
	leaq	.L2419(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,"aG",@progbits,_ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,comdat
	.align 4
	.align 4
.L2419:
	.long	.L2396-.L2419
	.long	.L2396-.L2419
	.long	.L2452-.L2419
	.long	.L2451-.L2419
	.long	.L2450-.L2419
	.long	.L2396-.L2419
	.long	.L2449-.L2419
	.long	.L2448-.L2419
	.long	.L2396-.L2419
	.long	.L2396-.L2419
	.long	.L2396-.L2419
	.long	.L2396-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2447-.L2419
	.long	.L2446-.L2419
	.long	.L2445-.L2419
	.long	.L2444-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2442-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2440-.L2419
	.long	.L2474-.L2419
	.long	.L2474-.L2419
	.long	.L2437-.L2419
	.long	.L2437-.L2419
	.long	.L2436-.L2419
	.long	.L2422-.L2419
	.long	.L2434-.L2419
	.long	.L2433-.L2419
	.long	.L2422-.L2419
	.long	.L2431-.L2419
	.long	.L2423-.L2419
	.long	.L2430-.L2419
	.long	.L2474-.L2419
	.long	.L2428-.L2419
	.long	.L2427-.L2419
	.long	.L2426-.L2419
	.long	.L2425-.L2419
	.long	.L2424-.L2419
	.long	.L2423-.L2419
	.long	.L2422-.L2419
	.long	.L2421-.L2419
	.long	.L2437-.L2419
	.long	.L2418-.L2419
	.section	.text._ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,"axG",@progbits,_ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,comdat
.L2447:
	cmpw	$95, %di
	je	.L2481
	cmpw	$104, %di
	je	.L2474
	movq	%r8, %rcx
	movl	%r13d, %edx
	cmpw	$108, %di
	je	.L2482
.L2473:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
.L2396:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2483
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2480:
	.cfi_restore_state
	leal	-1024(%rdi), %eax
	cmpw	$81, %ax
	ja	.L2406
	leaq	.L2408(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,"aG",@progbits,_ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,comdat
	.align 4
	.align 4
.L2408:
	.long	.L2416-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2406-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2415-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2414-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2412-.L2408
	.long	.L2411-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2410-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.long	.L2407-.L2408
	.section	.text._ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,"axG",@progbits,_ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_,comdat
	.p2align 4,,10
	.p2align 3
.L2401:
	movq	%r8, %rcx
	movl	$32, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2417:
	movq	%r15, %rcx
	movl	$24, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2474:
	movq	%r15, %rcx
	movl	%r13d, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2407:
	movq	%r15, %r8
	movl	%r13d, %ecx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2437:
	movq	%r8, %rcx
	movl	%r13d, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2422:
	movq	%r8, %rcx
	movl	$16, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2442:
	movq	%r8, %rcx
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18EphemeronHashTable14BodyDescriptor11IterateBodyINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_
	jmp	.L2396
.L2423:
	movq	%r8, %rcx
	movq	%r12, %rdi
	movl	$24, %edx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	%r15, %rcx
	movl	%r13d, %edx
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2424:
	movzbl	17(%r12), %eax
	movq	%r8, %rcx
	movl	$24, %esi
	movq	%r12, %rdi
	leal	(%rax,%rax,2), %edx
	sall	$4, %edx
	addl	$24, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2446:
	movq	%r8, %rcx
	movq	%r12, %rdi
	movl	$48, %edx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	%r15, %rcx
	movl	$112, %edx
	movq	%r12, %rdi
	movl	$48, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2418:
	movq	%r8, %rcx
	movq	%r12, %rdi
	movl	$16, %edx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	(%r15), %rax
	leaq	15(%r12), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*56(%rax)
	movq	%r15, %rcx
	movl	%r13d, %edx
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2449:
	movq	(%r8), %rax
	leaq	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm(%rip), %rdx
	movq	104(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2396
	leaq	7(%r12), %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	*%rax
	jmp	.L2396
.L2415:
	movq	%r15, %rcx
	movq	%r12, %rdi
	movl	$24, %edx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	%r15, %r8
	movl	%r13d, %ecx
	movl	$48, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2425:
	movzbl	9(%r12), %edx
	movq	%r8, %rcx
	movl	$16, %esi
	movq	%r12, %rdi
	addl	$1, %edx
	sall	$4, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2426:
	movzbl	9(%r12), %edx
	movq	%r8, %rcx
	movl	$16, %esi
	movq	%r12, %rdi
	sall	$5, %edx
	addl	$16, %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2436:
	movq	%r8, %rcx
	movl	%r13d, %edx
	jmp	.L2473
.L2428:
	movq	%r8, %rcx
	movl	$40, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2430:
	movl	7(%r12), %eax
	movq	%r8, %rcx
	movq	%r12, %rdi
	leal	23(%rax), %esi
	movl	11(%r12), %eax
	andl	$-8, %esi
	leal	(%rsi,%rax,8), %edx
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2440:
	movq	%r8, %rcx
	movl	$1944, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	(%r15), %rax
	leaq	1967(%r12), %rcx
	leaq	1943(%r12), %r10
	movq	32(%rax), %r9
.L2479:
	leaq	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	cmpq	%rdx, %r9
	movq	%r10, %rdx
	jne	.L2463
.L2476:
	call	*16(%rax)
	jmp	.L2396
.L2431:
	movq	(%r8), %rax
	leaq	7(%r12), %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	leaq	23(%r12), %r14
	call	*40(%rax)
	movq	(%r15), %rax
	leaq	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE(%rip), %rdx
	leaq	15(%r12), %r9
	movq	48(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L2456
	movq	%r14, %rcx
	movq	%r9, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*24(%rax)
.L2457:
	movq	(%r15), %rax
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*40(%rax)
	movq	%r15, %rcx
	movl	%r13d, %edx
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2412:
	movq	%r15, %rcx
	movq	%r12, %rdi
	movl	$32, %edx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	(%r15), %rax
	leaq	63(%r12), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*40(%rax)
	movq	%r15, %r8
	movl	%r13d, %ecx
	movl	$72, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2444:
	movq	%r8, %rcx
	movl	$32, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	cmpl	$48, %r13d
	jne	.L2396
	movq	(%r15), %rax
	leaq	47(%r12), %rcx
	leaq	39(%r12), %rdx
	movq	%r15, %rdi
	leaq	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %rsi
	movq	32(%rax), %r9
	cmpq	%rsi, %r9
	movq	%r12, %rsi
	je	.L2476
.L2463:
	call	*%r9
	jmp	.L2396
.L2445:
	movq	%r8, %rcx
	movq	%r12, %rdi
	movl	$48, %edx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	%r15, %rcx
	movl	$72, %edx
	movq	%r12, %rdi
	movl	$56, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2416:
	movq	%r15, %rcx
	movl	$32, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2411:
	movq	%r15, %rcx
	movq	%r12, %rdi
	movl	$32, %edx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	%r15, %r8
	movl	%r13d, %ecx
	movl	$56, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2433:
	movq	%r8, %rcx
	movl	%r13d, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase24IterateMaybeWeakPointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2427:
	movq	(%r8), %rax
	leaq	7(%r12), %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	*56(%rax)
	movq	%r15, %rcx
	movl	$40, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2434:
	movq	%r8, %rcx
	movl	$8, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	(%r15), %rax
	leaq	15(%r12), %rcx
	leaq	7(%r12), %r10
	movq	32(%rax), %r9
	jmp	.L2479
.L2451:
	movq	%r8, %rcx
	movl	$72, %edx
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	(%r15), %rax
	leaq	71(%r12), %r9
	leaq	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE(%rip), %rdx
	movq	48(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L2461
	leaq	79(%r12), %rcx
	movq	%r9, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L2396
.L2452:
	movq	%r8, %rcx
	movl	$48, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2414:
	movq	%r15, %rcx
	movq	%r12, %rdi
	movl	$24, %edx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	(%r15), %rax
	leaq	23(%r12), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*56(%rax)
	movq	%r15, %r8
	movl	%r13d, %ecx
	movl	$32, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2450:
	movq	%r8, %rcx
	movq	%r12, %rdi
	leaq	-128(%rbp), %r13
	movl	$40, %edx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1999, %edx
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	movq	(%r15), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*128(%rax)
	jmp	.L2396
.L2410:
	movq	%r15, %rcx
	movl	$24, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	leaq	-1(%r12), %rax
	leaq	_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE(%rip), %rbx
	movq	%rax, -136(%rbp)
	jmp	.L2460
	.p2align 4,,10
	.p2align 3
.L2485:
	leaq	8(%rdx), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*16(%r10)
.L2459:
	addq	$2, %rbx
	leaq	30+_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE(%rip), %rax
	cmpq	%rbx, %rax
	je	.L2484
.L2460:
	movq	(%r15), %r10
	leaq	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE(%rip), %rax
	movzwl	(%rbx), %edx
	addq	-136(%rbp), %rdx
	movq	40(%r10), %rcx
	cmpq	%rax, %rcx
	je	.L2485
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rcx
	jmp	.L2459
.L2421:
	movq	%r8, %rcx
	movq	%r12, %rdi
	movl	$16, %edx
	movl	$8, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	%r15, %rcx
	movl	$32, %edx
	movq	%r12, %rdi
	movl	$24, %esi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2448:
	movq	(%r8), %rax
	movq	%r8, %rdi
	leaq	15(%r12), %rdx
	movq	%r12, %rsi
	call	*40(%rax)
	movq	(%r15), %rax
	leaq	23(%r12), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*40(%rax)
	movq	(%r15), %rax
	leaq	31(%r12), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*40(%rax)
	jmp	.L2396
.L2406:
	movzwl	%di, %esi
	xorl	%eax, %eax
	leaq	.LC11(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L2477:
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2484:
	movq	%r15, %r8
	movl	%r13d, %ecx
	movl	$280, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2482:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2456:
	movq	%r9, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rcx
	jmp	.L2457
.L2461:
	movq	%r9, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rcx
	jmp	.L2396
.L2481:
	movq	%r8, %rcx
	movl	$40, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	movq	(%r15), %r9
	leaq	39(%r12), %rdx
	leaq	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE(%rip), %rcx
	movq	48(%r9), %rax
	cmpq	%rcx, %rax
	jne	.L2465
	leaq	47(%r12), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*24(%r9)
.L2466:
	movq	%r15, %rcx
	movl	%r13d, %edx
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18BodyDescriptorBase15IteratePointersINS0_40IterateAndScavengePromotedObjectsVisitorEEEvNS0_10HeapObjectEiiPT_
	jmp	.L2396
.L2483:
	call	__stack_chk_fail@PLT
.L2465:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2466
	.cfi_endproc
.LFE26781:
	.size	_ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_, .-_ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_
	.section	.text._ZN2v88internal9Scavenger32IterateAndScavengePromotedObjectENS0_10HeapObjectENS0_3MapEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Scavenger32IterateAndScavengePromotedObjectENS0_10HeapObjectENS0_3MapEi
	.type	_ZN2v88internal9Scavenger32IterateAndScavengePromotedObjectENS0_10HeapObjectENS0_3MapEi, @function
_ZN2v88internal9Scavenger32IterateAndScavengePromotedObjectENS0_10HeapObjectENS0_3MapEi:
.LFB22454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movl	%ecx, %r9d
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 770(%rdi)
	jne	.L2487
.L2489:
	xorl	%eax, %eax
.L2488:
	movq	%rdi, -24(%rbp)
	movzwl	11(%rsi), %edi
	leaq	-32(%rbp), %r8
	movl	%r9d, %ecx
	leaq	16+_ZTVN2v88internal40IterateAndScavengePromotedObjectsVisitorE(%rip), %rdx
	movb	%al, -16(%rbp)
	movq	%rdx, -32(%rbp)
	movq	%r10, %rdx
	call	_ZN2v88internal19BodyDescriptorApplyINS0_15CallIterateBodyEvNS0_3MapENS0_10HeapObjectEiPNS0_40IterateAndScavengePromotedObjectsVisitorEEET0_NS0_12InstanceTypeET1_T2_T3_T4_
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2496
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2487:
	.cfi_restore_state
	movl	%r10d, %eax
	movq	%r10, %r8
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r8
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r8), %rcx
	leaq	(%rcx,%rax,4), %rax
	movl	(%rax), %ecx
	testl	%edx, %ecx
	je	.L2489
	addl	%edx, %edx
	jne	.L2490
	addq	$4, %rax
	movl	$1, %edx
.L2490:
	movl	(%rax), %eax
	testl	%edx, %eax
	setne	%al
	jmp	.L2488
.L2496:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22454:
	.size	_ZN2v88internal9Scavenger32IterateAndScavengePromotedObjectENS0_10HeapObjectENS0_3MapEi, .-_ZN2v88internal9Scavenger32IterateAndScavengePromotedObjectENS0_10HeapObjectENS0_3MapEi
	.section	.text._ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE
	.type	_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE, @function
_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE:
.LFB22497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal15ScavengeVisitorE(%rip), %rax
	movq	%rdi, -136(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L2869:
	movslq	24(%r15), %r13
	movq	16(%r15), %r14
	leaq	0(%r13,%r13,4), %rax
	movq	%r14, %rdx
	movq	%r13, %rcx
	salq	$4, %rax
	addq	%r14, %rax
	movq	(%rax), %rsi
	movq	696(%rax), %rax
	movq	8(%rax), %rax
	addq	8(%rsi), %rax
	cmpq	$127, %rax
	ja	.L3342
	leaq	-128(%rbp), %rax
	xorl	%ebx, %ebx
	movl	$1, %esi
	movq	%rax, -184(%rbp)
	movl	%esi, %r13d
	movq	%rbx, %r12
	leaq	-144(%rbp), %rax
	movq	%rax, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L2498:
	movslq	40(%r15), %rax
	movq	32(%r15), %r14
	leaq	(%rax,%rax,4), %rsi
	salq	$4, %rsi
	leaq	(%r14,%rsi), %rbx
	movq	8(%rbx), %rdi
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L3343
	leaq	-1(%rax), %rdx
	salq	$4, %rax
	movq	%rdx, 8(%rdi)
	movq	(%rdi,%rax), %r12
.L2503:
	movq	-1(%r12), %r14
	leaq	-1(%r12), %r13
	movzbl	10(%r14), %eax
	cmpb	$55, %al
	ja	.L2508
	leaq	.L2510(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE,"a",@progbits
	.align 4
	.align 4
.L2510:
	.long	.L2560-.L2510
	.long	.L2558-.L2510
	.long	.L2559-.L2510
	.long	.L2558-.L2510
	.long	.L2557-.L2510
	.long	.L2557-.L2510
	.long	.L2508-.L2510
	.long	.L2556-.L2510
	.long	.L2508-.L2510
	.long	.L2555-.L2510
	.long	.L2554-.L2510
	.long	.L2553-.L2510
	.long	.L2552-.L2510
	.long	.L2551-.L2510
	.long	.L2550-.L2510
	.long	.L2549-.L2510
	.long	.L2548-.L2510
	.long	.L2547-.L2510
	.long	.L2546-.L2510
	.long	.L2545-.L2510
	.long	.L2544-.L2510
	.long	.L2543-.L2510
	.long	.L2542-.L2510
	.long	.L2541-.L2510
	.long	.L2540-.L2510
	.long	.L2539-.L2510
	.long	.L2538-.L2510
	.long	.L2537-.L2510
	.long	.L2536-.L2510
	.long	.L2535-.L2510
	.long	.L2534-.L2510
	.long	.L2533-.L2510
	.long	.L2532-.L2510
	.long	.L2531-.L2510
	.long	.L2530-.L2510
	.long	.L2529-.L2510
	.long	.L2528-.L2510
	.long	.L2527-.L2510
	.long	.L2508-.L2510
	.long	.L2526-.L2510
	.long	.L2525-.L2510
	.long	.L2524-.L2510
	.long	.L2523-.L2510
	.long	.L2522-.L2510
	.long	.L2521-.L2510
	.long	.L2520-.L2510
	.long	.L2519-.L2510
	.long	.L2518-.L2510
	.long	.L2517-.L2510
	.long	.L2516-.L2510
	.long	.L2515-.L2510
	.long	.L2514-.L2510
	.long	.L2513-.L2510
	.long	.L2512-.L2510
	.long	.L2511-.L2510
	.long	.L2509-.L2510
	.section	.text._ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE
	.p2align 4,,10
	.p2align 3
.L2557:
	movl	11(%r12), %eax
	.p2align 4,,10
	.p2align 3
.L2543:
	cmpq	$0, -176(%rbp)
	je	.L2845
	addq	$1, -168(%rbp)
	movq	-168(%rbp), %rax
	testb	$127, %al
	je	.L3344
.L2845:
	movslq	24(%r15), %rsi
	movq	16(%r15), %rdx
	xorl	%r13d, %r13d
	leaq	(%rsi,%rsi,4), %rax
	movq	%rdx, %r14
	movq	%rsi, %rcx
	salq	$4, %rax
	addq	%rdx, %rax
	movq	(%rax), %rdi
	movq	696(%rax), %rax
	movq	8(%rax), %rax
	addq	8(%rdi), %rax
	cmpq	$127, %rax
	jbe	.L2498
	movl	%r13d, %esi
	movq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L2499:
	xorl	%r12d, %r12d
	movq	%r13, %r10
	movq	$0, -120(%rbp)
	movq	-168(%rbp), %rbx
	movq	%r12, %r13
	movl	-208(%rbp), %r12d
	jmp	.L2561
	.p2align 4,,10
	.p2align 3
.L2847:
	leaq	-1(%rax), %rdx
	salq	$4, %rax
	addq	%rdi, %rax
	movq	%rdx, 8(%rdi)
	movq	(%rax), %r13
	movl	8(%rax), %r12d
	leaq	-1(%r13), %rax
.L2850:
	movq	(%rax), %rax
	movq	%rax, -120(%rbp)
.L2856:
	movq	-120(%rbp), %rdx
	movl	%r12d, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal9Scavenger32IterateAndScavengePromotedObjectENS0_10HeapObjectENS0_3MapEi
	cmpq	$0, -176(%rbp)
	je	.L3341
	addq	$1, %rbx
	movq	16(%r15), %r14
	testb	$127, %bl
	je	.L3345
.L2866:
	movslq	24(%r15), %r10
	xorl	%esi, %esi
.L2561:
	leaq	(%r10,%r10,4), %rdx
	salq	$4, %rdx
	leaq	(%r14,%rdx), %rcx
	movq	8(%rcx), %rdi
	movq	8(%rdi), %rax
	testq	%rax, %rax
	jne	.L2847
	movq	(%rcx), %rax
	cmpq	$0, 8(%rax)
	je	.L3346
	movq	%rax, %xmm3
	movq	%rdi, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r14,%rdx)
	movq	8(%rcx), %rax
.L2851:
	movq	8(%rax), %r13
	testq	%r13, %r13
	je	.L2884
	leaq	-1(%r13), %rdx
	salq	$4, %r13
	movq	%rdx, 8(%rax)
	addq	%r13, %rax
	movq	(%rax), %r13
	movl	8(%rax), %r12d
	leaq	-1(%r13), %rax
	jmp	.L2850
	.p2align 4,,10
	.p2align 3
.L2558:
	movq	7(%r12), %rax
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2508:
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2553:
	leaq	7(%r12), %rsi
	leaq	15(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2554:
	leaq	7(%r12), %rsi
	leaq	39(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2573
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2574
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3347
.L2574:
	movq	15(%r12), %rdx
	leaq	15(%r12), %rsi
	testb	$1, %dl
	je	.L2575
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3348
.L2575:
	movq	23(%r12), %rdx
	leaq	23(%r12), %rsi
	testb	$1, %dl
	je	.L2576
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3349
.L2576:
	movq	31(%r12), %rdx
	leaq	31(%r12), %rsi
	testb	$1, %dl
	je	.L2573
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3350
.L2573:
	movq	-184(%rbp), %rbx
	movq	%r12, %rsi
	movl	$1999, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	movq	-200(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE@PLT
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2560:
	movl	7(%r12), %eax
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2559:
	movzbl	7(%r14), %eax
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2509:
	movq	-184(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r12, -128(%rbp)
	leaq	7(%r12), %rbx
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2842
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2843:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2842:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2843
	cmpl	$3, %edx
	je	.L2843
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2843
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2843
	.p2align 4,,10
	.p2align 3
.L2511:
	movzbl	7(%r14), %ebx
	leaq	7(%r12), %rsi
	leal	0(,%rbx,8), %eax
	movl	%eax, -192(%rbp)
	leaq	23(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2811
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2812
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3351
.L2812:
	movq	15(%r12), %rdx
	leaq	15(%r12), %rsi
	testb	$1, %dl
	je	.L2811
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3352
.L2811:
	leaq	_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE(%rip), %rbx
	jmp	.L2816
.L2819:
	addq	$2, %rbx
	leaq	30+_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE(%rip), %rax
	cmpq	%rbx, %rax
	je	.L3353
	.p2align 4,,10
	.p2align 3
.L2816:
	movzwl	(%rbx), %eax
	addq	%r12, %rax
	leaq	-1(%rax), %rsi
	leaq	7(%rax), %rdx
	cmpq	%rdx, %rsi
	jnb	.L2819
	movq	-1(%rax), %rdx
	testb	$1, %dl
	je	.L2819
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2819
	movq	-136(%rbp), %rdi
	addq	$2, %rbx
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	leaq	30+_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE(%rip), %rax
	cmpq	%rbx, %rax
	jne	.L2816
.L3353:
	movq	47(%r14), %rax
	testq	%rax, %rax
	jne	.L2820
	movslq	-192(%rbp), %rbx
	addq	%rbx, %r13
	leaq	279(%r12), %rbx
	cmpq	%rbx, %r13
	ja	.L2821
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2822:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2821:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2822
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2822
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2822
	.p2align 4,,10
	.p2align 3
.L2517:
	leaq	15(%r12), %rsi
	leaq	23(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	15(%r12), %rdx
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2518:
	leaq	7(%r12), %rsi
	leaq	47(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2787
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2788
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3354
.L2788:
	movq	15(%r12), %rdx
	leaq	15(%r12), %rsi
	testb	$1, %dl
	je	.L2789
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3355
.L2789:
	movq	23(%r12), %rdx
	leaq	23(%r12), %rsi
	testb	$1, %dl
	je	.L2790
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3356
.L2790:
	movq	31(%r12), %rdx
	leaq	31(%r12), %rsi
	testb	$1, %dl
	je	.L2791
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3357
.L2791:
	movq	39(%r12), %rdx
	leaq	39(%r12), %rsi
	testb	$1, %dl
	je	.L2787
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3358
.L2787:
	leaq	55(%r12), %rsi
	leaq	71(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	55(%r12), %rdx
	testb	$1, %dl
	je	.L2795
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3359
.L2795:
	movq	63(%r12), %rdx
	leaq	63(%r12), %rsi
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2521:
	leaq	47(%r12), %r13
	leaq	7(%r12), %rsi
	cmpq	%rsi, %r13
	jbe	.L2769
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2770
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3360
.L2770:
	movq	15(%r12), %rdx
	leaq	15(%r12), %rsi
	testb	$1, %dl
	je	.L2771
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3361
.L2771:
	movq	23(%r12), %rdx
	leaq	23(%r12), %rsi
	testb	$1, %dl
	je	.L2772
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3362
.L2772:
	movq	31(%r12), %rdx
	leaq	31(%r12), %rsi
	testb	$1, %dl
	je	.L2773
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3363
.L2773:
	movq	39(%r12), %rdx
	leaq	39(%r12), %rsi
	testb	$1, %dl
	je	.L2769
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3364
.L2769:
	leaq	111(%r12), %rax
	cmpq	%r13, %rax
	jbe	.L2543
	movq	0(%r13), %rdx
	testb	$1, %dl
	je	.L2777
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3365
.L2777:
	movq	55(%r12), %rdx
	leaq	55(%r12), %rsi
	testb	$1, %dl
	je	.L2778
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3366
.L2778:
	movq	63(%r12), %rdx
	leaq	63(%r12), %rsi
	testb	$1, %dl
	je	.L2779
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3367
.L2779:
	movq	71(%r12), %rdx
	leaq	71(%r12), %rsi
	testb	$1, %dl
	je	.L2780
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3368
.L2780:
	movq	79(%r12), %rdx
	leaq	79(%r12), %rsi
	testb	$1, %dl
	je	.L2781
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3369
.L2781:
	movq	87(%r12), %rdx
	leaq	87(%r12), %rsi
	testb	$1, %dl
	je	.L2782
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3370
.L2782:
	movq	95(%r12), %rdx
	leaq	95(%r12), %rsi
	testb	$1, %dl
	je	.L2783
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3371
.L2783:
	movq	103(%r12), %rdx
	leaq	103(%r12), %rsi
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2525:
	leaq	15(%r12), %rsi
	leaq	31(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	15(%r12), %rdx
	testb	$1, %dl
	je	.L2757
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3372
.L2757:
	movq	23(%r12), %rdx
	leaq	23(%r12), %rsi
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2513:
	movq	-184(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r12, -128(%rbp)
	leaq	15(%r12), %rbx
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2804
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2805:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2804:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2805
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2805
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2805
	.p2align 4,,10
	.p2align 3
.L2519:
	leaq	15(%r12), %rsi
	leaq	23(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	15(%r12), %rdx
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2520:
	movzbl	7(%r14), %eax
	leaq	7(%r12), %rbx
	salq	$3, %rax
	andl	$2040, %eax
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2839
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2840:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2839:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2840
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2840
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2840
	.p2align 4,,10
	.p2align 3
.L2523:
	movzbl	9(%r12), %eax
	leaq	15(%r12), %rbx
	addl	$1, %eax
	sall	$4, %eax
	cltq
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2762
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2763:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2762:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2763
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2763
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2763
	.p2align 4,,10
	.p2align 3
.L2524:
	movzbl	9(%r12), %eax
	leaq	15(%r12), %rbx
	sall	$5, %eax
	addl	$16, %eax
	cltq
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2759
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2760:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2759:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2760
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2760
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2760
	.p2align 4,,10
	.p2align 3
.L2522:
	movzbl	17(%r12), %eax
	leaq	23(%r12), %rbx
	leal	(%rax,%rax,2), %eax
	sall	$4, %eax
	addl	$24, %eax
	cltq
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2765
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2766:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2765:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2766
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2766
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2766
	.p2align 4,,10
	.p2align 3
.L2512:
	movq	-184(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r12, -128(%rbp)
	leaq	31(%r12), %rbx
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2807
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2808:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2807:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2808
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2808
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2808
	.p2align 4,,10
	.p2align 3
.L2515:
	leaq	7(%r12), %rsi
	leaq	15(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2516:
	movq	-184(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r12, -128(%rbp)
	leaq	7(%r12), %rbx
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2798
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2799:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2798:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2799
	cmpl	$3, %edx
	je	.L2799
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2799
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2799
	.p2align 4,,10
	.p2align 3
.L2541:
	movzbl	7(%r14), %ebx
	leaq	7(%r12), %rsi
	leaq	23(%r12), %rax
	sall	$3, %ebx
	cmpq	%rsi, %rax
	jbe	.L2626
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2627
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3373
.L2627:
	movq	15(%r12), %rdx
	leaq	15(%r12), %rsi
	testb	$1, %dl
	je	.L2626
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3374
.L2626:
	movq	47(%r14), %rax
	testq	%rax, %rax
	jne	.L2629
	movslq	%ebx, %rbx
	addq	%rbx, %r13
	leaq	47(%r12), %rbx
	cmpq	%rbx, %r13
	ja	.L2630
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2632:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2630:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2632
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2632
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2632
	.p2align 4,,10
	.p2align 3
.L2542:
	movzbl	7(%r14), %edx
	movq	-200(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	sall	$3, %edx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_15ScavengeVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2544:
	movq	-184(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r12, -128(%rbp)
	leaq	15(%r12), %rbx
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2622
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2623:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2622:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2623
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2623
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2514:
	leaq	7(%r12), %rsi
	leaq	15(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2802
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2802
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3375
.L2802:
	leaq	23(%r12), %rsi
	leaq	31(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	23(%r12), %rdx
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2533:
	leaq	71(%r12), %r13
	leaq	23(%r12), %rsi
	cmpq	%rsi, %r13
	jbe	.L2702
	movq	23(%r12), %rdx
	testb	$1, %dl
	je	.L2703
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3376
.L2703:
	movq	31(%r12), %rdx
	leaq	31(%r12), %rsi
	testb	$1, %dl
	je	.L2704
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3377
.L2704:
	movq	39(%r12), %rdx
	leaq	39(%r12), %rsi
	testb	$1, %dl
	je	.L2705
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3378
.L2705:
	movq	47(%r12), %rdx
	leaq	47(%r12), %rsi
	testb	$1, %dl
	je	.L2706
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3379
.L2706:
	movq	55(%r12), %rdx
	leaq	55(%r12), %rsi
	testb	$1, %dl
	je	.L2707
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3380
.L2707:
	movq	63(%r12), %rdx
	leaq	63(%r12), %rsi
	testb	$1, %dl
	je	.L2702
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3381
.L2702:
	leaq	79(%r12), %rax
	cmpq	%r13, %rax
	jbe	.L2543
	movq	0(%r13), %rdx
	testb	$1, %dl
	je	.L2543
	cmpl	$3, %edx
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2549:
	movq	-184(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r12, -128(%rbp)
	leaq	15(%r12), %rbx
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2595
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2596:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2595:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2596
	cmpl	$3, %edx
	je	.L2596
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2596
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2596
	.p2align 4,,10
	.p2align 3
.L2550:
	movq	-184(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r12, -128(%rbp)
	leaq	23(%r12), %rbx
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	7(%r12), %rsi
	movslq	%eax, %r14
	cmpq	%rsi, %rbx
	jbe	.L2589
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2590
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3382
.L2590:
	movq	15(%r12), %rdx
	leaq	15(%r12), %rsi
	testb	$1, %dl
	je	.L2589
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3383
.L2589:
	addq	%r14, %r13
	cmpq	%rbx, %r13
	ja	.L2592
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2593:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2592:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2593
	cmpl	$3, %edx
	je	.L2593
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2593
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2593
	.p2align 4,,10
	.p2align 3
.L2551:
	movq	-184(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r12, -128(%rbp)
	leaq	15(%r12), %rbx
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2585
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2586:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2585:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2586
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2586
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2586
	.p2align 4,,10
	.p2align 3
.L2552:
	leaq	15(%r12), %rsi
	leaq	31(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	15(%r12), %rdx
	testb	$1, %dl
	je	.L2583
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3384
.L2583:
	movq	23(%r12), %rdx
	leaq	23(%r12), %rsi
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2529:
	movq	-184(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r12, -128(%rbp)
	leaq	15(%r12), %rbx
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2735
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2736:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2735:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2736
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2736
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2736
	.p2align 4,,10
	.p2align 3
.L2545:
	leaq	31(%r12), %rax
	leaq	7(%r12), %rsi
	movq	%rax, -192(%rbp)
	movl	31(%r12), %eax
	leaq	15(%r12), %rbx
	leal	48(,%rax,8), %eax
	movl	%eax, -216(%rbp)
	cmpq	%rbx, %rsi
	jnb	.L2610
	movq	7(%r12), %rdx
	testb	$1, %dl
	jne	.L3385
.L2610:
	leaq	23(%r12), %r14
	cmpq	%rbx, %r14
	jbe	.L2613
	movq	(%rbx), %rdx
	testb	$1, %dl
	jne	.L3386
.L2613:
	cmpq	%r14, -192(%rbp)
	jbe	.L2616
	movq	(%r14), %rdx
	testb	$1, %dl
	jne	.L3387
	.p2align 4,,10
	.p2align 3
.L2616:
	movslq	-216(%rbp), %rbx
	leaq	47(%r12), %r14
	addq	%rbx, %r13
	cmpq	%r14, %r13
	ja	.L2619
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2620:
	addq	$8, %r14
	cmpq	%r14, %r13
	jbe	.L2543
.L2619:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L2620
	cmpl	$3, %edx
	je	.L2620
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2620
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2620
	.p2align 4,,10
	.p2align 3
.L2546:
	leaq	7(%r12), %rsi
	leaq	15(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2537:
	movzbl	7(%r14), %eax
	leaq	7(%r12), %rbx
	salq	$3, %rax
	andl	$2040, %eax
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2836
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2837:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2836:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2837
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2837
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2837
	.p2align 4,,10
	.p2align 3
.L2555:
	leaq	7(%r12), %rsi
	leaq	15(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2556:
	movzbl	7(%r14), %ebx
	leaq	7(%r12), %rsi
	leaq	31(%r12), %rax
	sall	$3, %ebx
	cmpq	%rsi, %rax
	jbe	.L2563
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2564
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3388
.L2564:
	movq	15(%r12), %rdx
	leaq	15(%r12), %rsi
	testb	$1, %dl
	je	.L2565
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3389
.L2565:
	movq	23(%r12), %rdx
	leaq	23(%r12), %rsi
	testb	$1, %dl
	je	.L2563
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3390
.L2563:
	cmpl	$48, %ebx
	jne	.L2543
	leaq	39(%r12), %rsi
	leaq	47(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	39(%r12), %rdx
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2527:
	movq	-184(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r12, -128(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	39(%r12), %r9
	leaq	7(%r12), %rsi
	movslq	%eax, %r14
	cmpq	%rsi, %r9
	jbe	.L2745
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2746
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3391
.L2746:
	movq	15(%r12), %rdx
	leaq	15(%r12), %rsi
	testb	$1, %dl
	je	.L2747
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3392
.L2747:
	movq	23(%r12), %rdx
	leaq	23(%r12), %rsi
	testb	$1, %dl
	je	.L2748
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3393
.L2748:
	movq	31(%r12), %rdx
	leaq	31(%r12), %rsi
	testb	$1, %dl
	je	.L2745
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3394
.L2745:
	leaq	47(%r12), %rbx
	cmpq	%r9, %rbx
	jbe	.L2750
	movq	(%r9), %rdx
	testb	$1, %dl
	jne	.L3395
.L2750:
	addq	%r14, %r13
	cmpq	%rbx, %r13
	ja	.L2754
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2753:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2754:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2753
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2753
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2547:
	movq	-136(%rbp), %rax
	movq	%r12, -128(%rbp)
	movq	48(%rax), %r13
	movslq	56(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$4, %rax
	leaq	0(%r13,%rax), %rdx
	movq	(%rdx), %rbx
	movq	8(%rbx), %rcx
	cmpq	$128, %rcx
	je	.L3396
	leaq	1(%rcx), %rax
	movq	%rax, 8(%rbx)
	movq	%r12, 16(%rbx,%rcx,8)
.L2602:
	movq	-128(%rbp), %rcx
	xorl	%ebx, %ebx
	movq	31(%rcx), %rdx
	movq	%rdx, %rax
	sarq	$32, %rax
	testq	%rax, %rax
	jg	.L2604
	jmp	.L2608
	.p2align 4,,10
	.p2align 3
.L2606:
	movq	31(%rcx), %rdx
.L2605:
	movq	%rdx, %rax
	addq	$1, %rbx
	sarq	$32, %rax
	cmpl	%ebx, %eax
	jle	.L2608
.L2604:
	movq	%rbx, %rax
	salq	$4, %rax
	leaq	48(%rax,%rcx), %rax
	leaq	-1(%rax), %rsi
	leaq	7(%rax), %rdi
	cmpq	%rsi, %rdi
	jbe	.L2605
	movq	-1(%rax), %rdx
	testb	$1, %dl
	je	.L2606
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2606
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-128(%rbp), %rcx
	jmp	.L2606
	.p2align 4,,10
	.p2align 3
.L2548:
	movq	-184(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r12, -128(%rbp)
	leaq	15(%r12), %rbx
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L2598
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2599:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2598:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2599
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2599
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2599
	.p2align 4,,10
	.p2align 3
.L2535:
	movzbl	7(%r14), %ebx
	leaq	23(%r12), %r9
	leaq	7(%r12), %rsi
	sall	$3, %ebx
	cmpq	%rsi, %r9
	jbe	.L2683
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2684
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3397
.L2684:
	movq	15(%r12), %rdx
	leaq	15(%r12), %rsi
	testb	$1, %dl
	je	.L2683
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3398
.L2683:
	leaq	31(%r12), %rcx
	cmpq	%r9, %rcx
	jbe	.L2686
	movq	(%r9), %rdx
	testb	$1, %dl
	jne	.L3399
.L2686:
	movq	47(%r14), %rax
	testq	%rax, %rax
	jne	.L2689
	.p2align 4,,10
	.p2align 3
.L3420:
	movslq	%ebx, %rbx
	addq	%rbx, %r13
	cmpq	%r13, %rcx
	jnb	.L2543
	movq	%rcx, %rbx
	jmp	.L2690
	.p2align 4,,10
	.p2align 3
.L2691:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2690:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2691
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2691
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2691
	.p2align 4,,10
	.p2align 3
.L2534:
	movzbl	7(%r14), %edx
	movq	-200(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	sall	$3, %edx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_15ScavengeVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2536:
	movzbl	7(%r14), %ebx
	leaq	7(%r12), %rsi
	leaq	31(%r12), %rax
	sall	$3, %ebx
	cmpq	%rsi, %rax
	jbe	.L2661
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2662
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3400
.L2662:
	movq	15(%r12), %rdx
	leaq	15(%r12), %rsi
	testb	$1, %dl
	je	.L2663
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3401
.L2663:
	movq	23(%r12), %rdx
	leaq	23(%r12), %rsi
	testb	$1, %dl
	je	.L2661
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3402
.L2661:
	leaq	63(%r12), %rsi
	leaq	71(%r12), %rcx
	cmpq	%rcx, %rsi
	jnb	.L2665
	movq	63(%r12), %rdx
	testb	$1, %dl
	jne	.L3403
.L2665:
	movq	47(%r14), %rax
	testq	%rax, %rax
	jne	.L2668
	.p2align 4,,10
	.p2align 3
.L3422:
	movslq	%ebx, %rbx
	addq	%rbx, %r13
	cmpq	%r13, %rcx
	jnb	.L2543
	movq	%rcx, %rbx
	jmp	.L2669
	.p2align 4,,10
	.p2align 3
.L2671:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2669:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2671
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2671
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2671
	.p2align 4,,10
	.p2align 3
.L2539:
	movzbl	7(%r14), %edx
	movq	-200(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	sall	$3, %edx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_15ScavengeVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2540:
	movzbl	7(%r14), %ebx
	leaq	7(%r12), %rsi
	leaq	31(%r12), %rax
	sall	$3, %ebx
	cmpq	%rsi, %rax
	jbe	.L2643
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2644
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3404
.L2644:
	movq	15(%r12), %rdx
	leaq	15(%r12), %rsi
	testb	$1, %dl
	je	.L2645
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3405
.L2645:
	movq	23(%r12), %rdx
	leaq	23(%r12), %rsi
	testb	$1, %dl
	je	.L2643
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3406
.L2643:
	movq	47(%r14), %rax
	testq	%rax, %rax
	jne	.L2647
	movslq	%ebx, %rbx
	addq	%rbx, %r13
	leaq	55(%r12), %rbx
	cmpq	%rbx, %r13
	ja	.L2648
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2650:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L2543
.L2648:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2650
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2650
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2650
	.p2align 4,,10
	.p2align 3
.L2538:
	movzbl	7(%r14), %edx
	movq	-200(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	sall	$3, %edx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_15ScavengeVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2531:
	leaq	15(%r12), %rsi
	leaq	47(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	15(%r12), %rdx
	testb	$1, %dl
	je	.L2728
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3407
.L2728:
	movq	23(%r12), %rdx
	leaq	23(%r12), %rsi
	testb	$1, %dl
	je	.L2729
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3408
.L2729:
	movq	31(%r12), %rdx
	leaq	31(%r12), %rsi
	testb	$1, %dl
	je	.L2730
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3409
.L2730:
	movq	39(%r12), %rdx
	leaq	39(%r12), %rsi
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2530:
	movl	7(%r12), %eax
	movl	11(%r12), %edx
	addl	$23, %eax
	andl	$-8, %eax
	leal	(%rax,%rdx,8), %ebx
	cltq
	movslq	%ebx, %rbx
	addq	%r13, %rbx
	addq	%rax, %r13
	cmpq	%r13, %rbx
	ja	.L2732
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2733:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jbe	.L2543
.L2732:
	movq	0(%r13), %rdx
	testb	$1, %dl
	je	.L2733
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2733
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2733
	.p2align 4,,10
	.p2align 3
.L2532:
	leaq	1943(%r12), %r14
	leaq	7(%r12), %rbx
	cmpq	%rbx, %r14
	jbe	.L2717
	leaq	15(%r12), %rax
	cmpq	%rax, %r14
	sbbq	%rax, %rax
	andb	$15, %al
	leaq	1936(%rbx,%rax,8), %r13
	jmp	.L2716
	.p2align 4,,10
	.p2align 3
.L2715:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L2717
.L2716:
	movq	(%rbx), %rdx
	testb	$1, %dl
	je	.L2715
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2715
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	addq	$8, %rbx
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	cmpq	%rbx, %r13
	jne	.L2716
	.p2align 4,,10
	.p2align 3
.L2717:
	leaq	1967(%r12), %rax
	cmpq	%r14, %rax
	jbe	.L2543
	leaq	1951(%r12), %r13
	movq	(%r14), %rdx
	cmpq	%r13, %rax
	sbbq	%rbx, %rbx
	andq	$-2, %rbx
	addq	$3, %rbx
	testb	$1, %dl
	je	.L2718
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3410
.L2718:
	subq	$1, %rbx
	je	.L2543
	movq	0(%r13), %rdx
	testb	$1, %dl
	je	.L2721
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3411
.L2721:
	leaq	1959(%r12), %rsi
	cmpq	$1, %rbx
	je	.L2543
	movq	1959(%r12), %rdx
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2526:
	leaq	15(%r12), %rsi
	leaq	31(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	15(%r12), %rdx
	testb	$1, %dl
	je	.L2834
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3412
.L2834:
	movq	23(%r12), %rdx
	leaq	23(%r12), %rsi
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2528:
	leaq	7(%r12), %rsi
	leaq	39(%r12), %rax
	cmpq	%rsi, %rax
	jbe	.L2543
	movq	7(%r12), %rdx
	testb	$1, %dl
	je	.L2740
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3413
.L2740:
	movq	15(%r12), %rdx
	leaq	15(%r12), %rsi
	testb	$1, %dl
	je	.L2741
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3414
.L2741:
	movq	23(%r12), %rdx
	leaq	23(%r12), %rsi
	testb	$1, %dl
	je	.L2742
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3415
.L2742:
	movq	31(%r12), %rdx
	leaq	31(%r12), %rsi
	testb	$1, %dl
	je	.L2543
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2543
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L3343:
	movq	(%rbx), %rax
	cmpq	$0, 8(%rax)
	je	.L3416
	movq	%rdi, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r14,%rsi)
	movq	8(%rbx), %rdx
.L2504:
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L2503
	leaq	-1(%rax), %rcx
	salq	$4, %rax
	movq	%rcx, 8(%rdx)
	movq	(%rdx,%rax), %r12
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L3344:
	movq	32(%r15), %rax
	movq	680(%rax), %rax
	testq	%rax, %rax
	je	.L2845
	movq	-176(%rbp), %rbx
	leaq	48(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	100(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L2846
	movq	%rbx, %rdi
	call	_ZN2v84base17ConditionVariable9NotifyAllEv@PLT
.L2846:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L2845
	.p2align 4,,10
	.p2align 3
.L3345:
	movq	680(%r14), %rax
	testq	%rax, %rax
	je	.L3417
.L2867:
	movq	-176(%rbp), %rax
	leaq	48(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-176(%rbp), %rax
	movl	100(%rax), %edx
	testl	%edx, %edx
	jg	.L3418
.L2868:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L3341:
	movq	16(%r15), %r14
	jmp	.L2866
	.p2align 4,,10
	.p2align 3
.L3416:
	movq	680(%r14), %rax
	leaq	640(%r14), %rdi
	testq	%rax, %rax
	je	.L3419
	movq	%rdi, -192(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r14), %rdx
	movq	-192(%rbp), %rdi
	testq	%rdx, %rdx
	jne	.L2506
	movb	%r13b, -184(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	16(%r15), %r14
	movslq	24(%r15), %r13
	movzbl	-184(%rbp), %esi
	jmp	.L2499
	.p2align 4,,10
	.p2align 3
.L3385:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2610
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2610
	.p2align 4,,10
	.p2align 3
.L3386:
	cmpl	$3, %edx
	je	.L2613
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2613
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2613
	.p2align 4,,10
	.p2align 3
.L3395:
	cmpl	$3, %edx
	je	.L2750
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2750
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2750
.L3387:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2616
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2616
	.p2align 4,,10
	.p2align 3
.L3399:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2686
	movq	-136(%rbp), %rdi
	movq	%r9, %rsi
	movq	%rcx, -192(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-192(%rbp), %rcx
	movq	47(%r14), %rax
	testq	%rax, %rax
	je	.L3420
.L2689:
	movq	$0, -120(%rbp)
	movb	$1, -128(%rbp)
	movl	$0, -124(%rbp)
	movq	47(%r14), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2693
	movzbl	8(%r14), %eax
	movb	$0, -128(%rbp)
	sall	$3, %eax
	movl	%eax, -124(%rbp)
.L2693:
	movl	$32, %esi
	leaq	-148(%rbp), %r14
	cmpl	$32, %ebx
	jle	.L2543
	movq	%r13, -216(%rbp)
	movl	%esi, %r13d
	movq	%r15, -224(%rbp)
	movq	-184(%rbp), %r15
	movq	%r12, -192(%rbp)
	jmp	.L2694
	.p2align 4,,10
	.p2align 3
.L3339:
	movl	-148(%rbp), %r13d
.L2696:
	cmpl	%r13d, %ebx
	jle	.L3421
.L2694:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L3339
	movq	-192(%rbp), %rdi
	movslq	-148(%rbp), %rdx
	movslq	%r13d, %rcx
	leaq	-1(%rdi,%rdx), %r12
	movq	-216(%rbp), %rdi
	leaq	(%rcx,%rdi), %r13
	cmpq	%r13, %r12
	ja	.L2699
	jmp	.L2881
	.p2align 4,,10
	.p2align 3
.L2698:
	addq	$8, %r13
	cmpq	%r13, %r12
	jbe	.L3339
.L2699:
	movq	0(%r13), %rdx
	testb	$1, %dl
	je	.L2698
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2698
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2698
	.p2align 4,,10
	.p2align 3
.L3403:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2665
	movq	-136(%rbp), %rdi
	movq	%rcx, -192(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-192(%rbp), %rcx
	movq	47(%r14), %rax
	testq	%rax, %rax
	je	.L3422
.L2668:
	movq	$0, -120(%rbp)
	movb	$1, -128(%rbp)
	movl	$0, -124(%rbp)
	movq	47(%r14), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2674
	movzbl	8(%r14), %eax
	movb	$0, -128(%rbp)
	sall	$3, %eax
	movl	%eax, -124(%rbp)
.L2674:
	movl	$72, %esi
	leaq	-148(%rbp), %r14
	cmpl	$72, %ebx
	jle	.L2543
	movq	%r13, -216(%rbp)
	movl	%esi, %r13d
	movq	%r15, -224(%rbp)
	movq	-184(%rbp), %r15
	movq	%r12, -192(%rbp)
	jmp	.L2675
	.p2align 4,,10
	.p2align 3
.L3338:
	movl	-148(%rbp), %r13d
.L2677:
	cmpl	%r13d, %ebx
	jle	.L3423
.L2675:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L3338
	movq	-192(%rbp), %rdi
	movslq	-148(%rbp), %rdx
	movslq	%r13d, %rcx
	leaq	-1(%rdi,%rdx), %r12
	movq	-216(%rbp), %rdi
	leaq	(%rcx,%rdi), %r13
	cmpq	%r13, %r12
	ja	.L2680
	jmp	.L2879
	.p2align 4,,10
	.p2align 3
.L2679:
	addq	$8, %r13
	cmpq	%r13, %r12
	jbe	.L3338
.L2680:
	movq	0(%r13), %rdx
	testb	$1, %dl
	je	.L2679
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2679
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2679
	.p2align 4,,10
	.p2align 3
.L3346:
	leaq	680(%r14), %rax
	leaq	640(%r14), %rdi
	movq	%rax, -208(%rbp)
	movq	680(%r14), %rax
	testq	%rax, %rax
	je	.L2852
	movq	%r10, -200(%rbp)
	movb	%sil, -192(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r14), %rax
	movq	-168(%rbp), %rdi
	movq	-184(%rbp), %rcx
	movzbl	-192(%rbp), %esi
	testq	%rax, %rax
	movq	-200(%rbp), %r10
	jne	.L3424
	movq	%r10, -192(%rbp)
	movb	%sil, -184(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-168(%rbp), %rcx
	movzbl	-184(%rbp), %esi
	movq	-192(%rbp), %r10
.L2852:
	movq	704(%rcx), %rdx
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L3425
	subq	$1, %rax
	movq	%rax, 8(%rdx)
	leaq	(%rax,%rax,2), %rax
	leaq	16(%rdx,%rax,8), %rax
	movdqu	(%rax), %xmm2
	movaps	%xmm2, -128(%rbp)
	movl	16(%rax), %edx
	movl	%edx, -112(%rbp)
	movq	(%rax), %r13
	movl	16(%rax), %r12d
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L3425:
	movq	696(%rcx), %rax
	cmpq	$0, 8(%rax)
	je	.L3426
	movq	%rax, %xmm4
	movq	%rdx, %xmm0
	leaq	(%r10,%r10,4), %rax
	punpcklqdq	%xmm4, %xmm0
	salq	$4, %rax
	movups	%xmm0, 696(%r14,%rax)
	movq	704(%rcx), %r14
.L2860:
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.L2856
	subq	$1, %rax
	movq	%rax, 8(%r14)
	leaq	(%rax,%rax,2), %rax
	leaq	16(%r14,%rax,8), %rax
	movdqu	(%rax), %xmm5
	movaps	%xmm5, -128(%rbp)
	movl	16(%rax), %edx
	movl	%edx, -112(%rbp)
	movq	(%rax), %r13
	movl	16(%rax), %r12d
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L3419:
	movl	%r13d, %esi
	movq	%rdx, %r14
	movslq	%ecx, %r13
	jmp	.L2499
	.p2align 4,,10
	.p2align 3
.L2884:
	movq	$-1, %rax
	xorl	%r12d, %r12d
	jmp	.L2850
	.p2align 4,,10
	.p2align 3
.L2506:
	movq	(%rdx), %rax
	movq	%rdx, -192(%rbp)
	movq	%rax, 680(%r14)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	8(%rbx), %rdi
	movq	-192(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L2507
	movl	$4112, %esi
	call	_ZdlPvm@PLT
	movq	-192(%rbp), %rdx
.L2507:
	movq	%rdx, 8(%rbx)
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L3426:
	leaq	1376(%r14), %rax
	movq	%rcx, -168(%rbp)
	leaq	1336(%r14), %rdi
	movq	%rax, -200(%rbp)
	movq	1376(%r14), %rax
	testq	%rax, %rax
	je	.L3334
	movb	%sil, -192(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	1376(%r14), %r14
	movq	-184(%rbp), %rdi
	movzbl	-192(%rbp), %esi
	testq	%r14, %r14
	jne	.L3427
	movb	%sil, -184(%rbp)
	movq	%rbx, -168(%rbp)
	movl	%r12d, -208(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movzbl	-184(%rbp), %esi
.L2861:
	testb	%sil, %sil
	je	.L2869
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3428
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2608:
	.cfi_restore_state
	movq	-184(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L3417:
	movq	1376(%r14), %rax
	testq	%rax, %rax
	jne	.L2867
	jmp	.L2866
	.p2align 4,,10
	.p2align 3
.L3424:
	movq	%rax, -168(%rbp)
	movq	(%rax), %rdx
	movq	-208(%rbp), %rax
	movq	%rdx, (%rax)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-184(%rbp), %rcx
	movq	-168(%rbp), %rax
	movq	8(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L2855
	movl	$4112, %esi
	movq	%rcx, -184(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZdlPvm@PLT
	movq	-184(%rbp), %rcx
	movq	-168(%rbp), %rax
.L2855:
	movq	%rax, 8(%rcx)
	jmp	.L2851
	.p2align 4,,10
	.p2align 3
.L3418:
	movq	%rax, %rdi
	call	_ZN2v84base17ConditionVariable9NotifyAllEv@PLT
	jmp	.L2868
.L2820:
	movq	$0, -120(%rbp)
	movb	$1, -128(%rbp)
	movl	$0, -124(%rbp)
	movq	47(%r14), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2824
	movzbl	8(%r14), %eax
	movb	$0, -128(%rbp)
	sall	$3, %eax
	movl	%eax, -124(%rbp)
.L2824:
	movl	-192(%rbp), %ebx
	movl	$280, %esi
	leaq	-148(%rbp), %r14
	cmpl	$280, %ebx
	jle	.L2543
	movq	%r13, -216(%rbp)
	movl	%esi, %r13d
	movq	%r15, -224(%rbp)
	movq	-184(%rbp), %r15
	movq	%r12, -192(%rbp)
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L3340:
	movl	-148(%rbp), %r13d
.L2827:
	cmpl	%r13d, %ebx
	jle	.L3429
.L2825:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L3340
	movq	-192(%rbp), %rdi
	movslq	-148(%rbp), %rdx
	movslq	%r13d, %rcx
	leaq	-1(%rdi,%rdx), %r12
	movq	-216(%rbp), %rdi
	leaq	(%rcx,%rdi), %r13
	cmpq	%r13, %r12
	ja	.L2830
	jmp	.L2883
	.p2align 4,,10
	.p2align 3
.L2829:
	addq	$8, %r13
	cmpq	%r13, %r12
	jbe	.L3340
.L2830:
	movq	0(%r13), %rdx
	testb	$1, %dl
	je	.L2829
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2829
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2829
.L2629:
	movq	$0, -120(%rbp)
	movb	$1, -128(%rbp)
	movl	$0, -124(%rbp)
	movq	47(%r14), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2634
	movzbl	8(%r14), %eax
	movb	$0, -128(%rbp)
	sall	$3, %eax
	movl	%eax, -124(%rbp)
.L2634:
	movl	$48, %esi
	leaq	-148(%rbp), %r14
	cmpl	$48, %ebx
	jle	.L2543
	movq	%r13, -216(%rbp)
	movl	%esi, %r13d
	movq	%r15, -224(%rbp)
	movq	-184(%rbp), %r15
	movq	%r12, -192(%rbp)
	jmp	.L2635
	.p2align 4,,10
	.p2align 3
.L3336:
	movl	-148(%rbp), %r13d
.L2637:
	cmpl	%r13d, %ebx
	jle	.L3430
.L2635:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L3336
	movq	-192(%rbp), %rdi
	movslq	-148(%rbp), %rdx
	movslq	%r13d, %rcx
	leaq	-1(%rdi,%rdx), %r12
	movq	-216(%rbp), %rdi
	leaq	(%rcx,%rdi), %r13
	cmpq	%r13, %r12
	ja	.L2640
	jmp	.L2875
	.p2align 4,,10
	.p2align 3
.L2639:
	addq	$8, %r13
	cmpq	%r13, %r12
	jbe	.L3336
.L2640:
	movq	0(%r13), %rdx
	testb	$1, %dl
	je	.L2639
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2639
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2639
.L2647:
	movq	$0, -120(%rbp)
	movb	$1, -128(%rbp)
	movl	$0, -124(%rbp)
	movq	47(%r14), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2652
	movzbl	8(%r14), %eax
	movb	$0, -128(%rbp)
	sall	$3, %eax
	movl	%eax, -124(%rbp)
.L2652:
	movl	$56, %esi
	leaq	-148(%rbp), %r14
	cmpl	$56, %ebx
	jle	.L2543
	movq	%r13, -216(%rbp)
	movl	%esi, %r13d
	movq	%r15, -224(%rbp)
	movq	-184(%rbp), %r15
	movq	%r12, -192(%rbp)
	jmp	.L2653
	.p2align 4,,10
	.p2align 3
.L3337:
	movl	-148(%rbp), %r13d
.L2655:
	cmpl	%r13d, %ebx
	jle	.L3431
.L2653:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L3337
	movq	-192(%rbp), %rdi
	movslq	-148(%rbp), %rdx
	movslq	%r13d, %rcx
	leaq	-1(%rdi,%rdx), %r12
	movq	-216(%rbp), %rdi
	leaq	(%rcx,%rdi), %r13
	cmpq	%r13, %r12
	ja	.L2658
	jmp	.L2877
	.p2align 4,,10
	.p2align 3
.L2657:
	addq	$8, %r13
	cmpq	%r13, %r12
	jbe	.L3337
.L2658:
	movq	0(%r13), %rdx
	testb	$1, %dl
	je	.L2657
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2657
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2657
.L3334:
	movq	%rbx, -168(%rbp)
	movl	%r12d, -208(%rbp)
	jmp	.L2861
.L3429:
	movq	-192(%rbp), %r12
	movq	-224(%rbp), %r15
	jmp	.L2543
.L3423:
	movq	-192(%rbp), %r12
	movq	-224(%rbp), %r15
	jmp	.L2543
.L3421:
	movq	-192(%rbp), %r12
	movq	-224(%rbp), %r15
	jmp	.L2543
.L3431:
	movq	-192(%rbp), %r12
	movq	-224(%rbp), %r15
	jmp	.L2543
.L3430:
	movq	-192(%rbp), %r12
	movq	-224(%rbp), %r15
	jmp	.L2543
.L3427:
	movq	-200(%rbp), %rcx
	movq	(%r14), %rax
	movq	%rax, (%rcx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-168(%rbp), %rcx
	movq	704(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L2864
	movl	$112, %esi
	movq	%rcx, -168(%rbp)
	call	_ZdlPvm@PLT
	movq	-168(%rbp), %rcx
.L2864:
	movq	%r14, 704(%rcx)
	jmp	.L2860
.L3396:
	leaq	640(%r13), %rdi
	movq	%rdx, -224(%rbp)
	movq	%rcx, -216(%rbp)
	movq	%rdi, -192(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 680(%r13)
	movq	-192(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movq	-216(%rbp), %rcx
	movq	-224(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rdx)
	movq	8(%rsi), %rax
	cmpq	$128, %rax
	je	.L2602
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%r12, 16(%rsi,%rax,8)
	jmp	.L2602
.L2877:
	movl	%edx, %r13d
	jmp	.L2655
.L2875:
	movl	%edx, %r13d
	jmp	.L2637
.L2883:
	movl	%edx, %r13d
	jmp	.L2827
.L3352:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2811
.L3412:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2834
.L3351:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2812
.L3350:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2573
.L3347:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2574
.L3348:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2575
.L3349:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2576
.L3397:
	movq	-136(%rbp), %rdi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-192(%rbp), %r9
	jmp	.L2684
.L3375:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2802
.L3401:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2663
.L3400:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2662
.L3383:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2589
.L3398:
	movq	-136(%rbp), %rdi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-192(%rbp), %r9
	jmp	.L2683
.L3373:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2627
.L3374:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2626
.L3381:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2702
.L3376:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2703
.L3408:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2729
.L3409:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2730
.L3388:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2564
.L3390:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2563
.L3389:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2565
.L3391:
	movq	-136(%rbp), %rdi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-192(%rbp), %r9
	jmp	.L2746
.L3402:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2661
.L3407:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2728
.L3413:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2740
.L3414:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2741
.L3415:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2742
.L3393:
	movq	-136(%rbp), %rdi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-192(%rbp), %r9
	jmp	.L2748
.L3392:
	movq	-136(%rbp), %rdi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-192(%rbp), %r9
	jmp	.L2747
.L3394:
	movq	-136(%rbp), %rdi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-192(%rbp), %r9
	jmp	.L2745
.L3405:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2645
.L2881:
	movl	%edx, %r13d
	jmp	.L2696
.L3410:
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2718
.L2879:
	movl	%edx, %r13d
	jmp	.L2677
.L3384:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2583
.L3404:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2644
.L3406:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2643
.L3382:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2590
.L3354:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2788
.L3371:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2783
.L3372:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2757
.L3377:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2704
.L3378:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2705
.L3379:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2706
.L3380:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2707
.L3362:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2772
.L3363:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2773
.L3359:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2795
.L3360:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2770
.L3356:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2790
.L3357:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2791
.L3355:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2789
.L3358:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2787
.L3364:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2769
.L3361:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2771
.L3365:
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2777
.L3366:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2778
.L3367:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2779
.L3368:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2780
.L3369:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2781
.L3370:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2782
.L3411:
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L2721
.L3342:
	movl	$1, %esi
	jmp	.L2499
.L3428:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22497:
	.size	_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE, .-_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE
	.section	.text._ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE.constprop.0, @function
_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE.constprop.0:
.LFB30421:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal15ScavengeVisitorE(%rip), %rax
	movq	%rdi, -136(%rbp)
	movq	%rax, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L3799:
	xorl	%r13d, %r13d
	movl	$1, %r15d
	.p2align 4,,10
	.p2align 3
.L3434:
	movslq	24(%rbx), %r12
	movq	16(%rbx), %rcx
	leaq	(%r12,%r12,4), %rax
	salq	$4, %rax
	addq	%rcx, %rax
	movq	(%rax), %rdx
	movq	696(%rax), %rax
	movq	8(%rax), %rax
	addq	8(%rdx), %rax
	cmpq	$127, %rax
	ja	.L3435
	movslq	40(%rbx), %rax
	movq	32(%rbx), %r8
	leaq	(%rax,%rax,4), %rdx
	salq	$4, %rdx
	leaq	(%r8,%rdx), %r14
	movq	8(%r14), %rsi
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L4266
	leaq	-1(%rax), %rdx
	salq	$4, %rax
	movq	%rdx, 8(%rsi)
	movq	(%rsi,%rax), %r13
.L3439:
	movq	-1(%r13), %r14
	leaq	-1(%r13), %r12
	xorl	%r15d, %r15d
	movzbl	10(%r14), %eax
	cmpb	$55, %al
	ja	.L3444
	leaq	.L3446(%rip), %rdi
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE.constprop.0,"a",@progbits
	.align 4
	.align 4
.L3446:
	.long	.L3495-.L3446
	.long	.L3493-.L3446
	.long	.L3494-.L3446
	.long	.L3493-.L3446
	.long	.L3492-.L3446
	.long	.L3492-.L3446
	.long	.L3444-.L3446
	.long	.L3491-.L3446
	.long	.L3444-.L3446
	.long	.L3490-.L3446
	.long	.L3489-.L3446
	.long	.L3488-.L3446
	.long	.L3487-.L3446
	.long	.L3486-.L3446
	.long	.L3485-.L3446
	.long	.L3484-.L3446
	.long	.L3483-.L3446
	.long	.L3482-.L3446
	.long	.L3481-.L3446
	.long	.L3480-.L3446
	.long	.L3479-.L3446
	.long	.L3434-.L3446
	.long	.L3478-.L3446
	.long	.L3477-.L3446
	.long	.L3476-.L3446
	.long	.L3475-.L3446
	.long	.L3474-.L3446
	.long	.L3473-.L3446
	.long	.L3472-.L3446
	.long	.L3471-.L3446
	.long	.L3470-.L3446
	.long	.L3469-.L3446
	.long	.L3468-.L3446
	.long	.L3467-.L3446
	.long	.L3466-.L3446
	.long	.L3465-.L3446
	.long	.L3464-.L3446
	.long	.L3463-.L3446
	.long	.L3444-.L3446
	.long	.L3462-.L3446
	.long	.L3461-.L3446
	.long	.L3460-.L3446
	.long	.L3459-.L3446
	.long	.L3458-.L3446
	.long	.L3457-.L3446
	.long	.L3456-.L3446
	.long	.L3455-.L3446
	.long	.L3454-.L3446
	.long	.L3453-.L3446
	.long	.L3452-.L3446
	.long	.L3451-.L3446
	.long	.L3450-.L3446
	.long	.L3449-.L3446
	.long	.L3448-.L3446
	.long	.L3447-.L3446
	.long	.L3445-.L3446
	.section	.text._ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE.constprop.0
	.p2align 4,,10
	.p2align 3
.L3492:
	movl	11(%r13), %eax
	.p2align 4,,10
	.p2align 3
.L3506:
	xorl	%r15d, %r15d
	jmp	.L3434
	.p2align 4,,10
	.p2align 3
.L3493:
	movq	7(%r13), %rax
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3444:
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3488:
	leaq	7(%r13), %rsi
	leaq	15(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3489:
	leaq	7(%r13), %rsi
	leaq	39(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3509
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3510
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4267
.L3510:
	movq	15(%r13), %rdx
	leaq	15(%r13), %rsi
	testb	$1, %dl
	je	.L3511
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4268
.L3511:
	movq	23(%r13), %rdx
	leaq	23(%r13), %rsi
	testb	$1, %dl
	je	.L3512
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4269
.L3512:
	movq	31(%r13), %rdx
	leaq	31(%r13), %rsi
	testb	$1, %dl
	je	.L3509
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4270
.L3509:
	leaq	-128(%rbp), %r15
	movq	%r13, %rsi
	movl	$1999, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	leaq	-144(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE@PLT
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3445:
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, -128(%rbp)
	leaq	7(%r13), %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3778
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3779:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3778:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3779
	cmpl	$3, %edx
	je	.L3779
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3779
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3779
	.p2align 4,,10
	.p2align 3
.L3447:
	movzbl	7(%r14), %r15d
	leaq	7(%r13), %rsi
	leal	0(,%r15,8), %eax
	movl	%eax, -168(%rbp)
	leaq	23(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3747
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3748
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4271
.L3748:
	movq	15(%r13), %rdx
	leaq	15(%r13), %rsi
	testb	$1, %dl
	je	.L3747
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4272
.L3747:
	leaq	_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE(%rip), %r15
	jmp	.L3752
.L3755:
	addq	$2, %r15
	leaq	30+_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE(%rip), %rax
	cmpq	%r15, %rax
	je	.L4273
	.p2align 4,,10
	.p2align 3
.L3752:
	movzwl	(%r15), %eax
	addq	%r13, %rax
	leaq	-1(%rax), %rsi
	leaq	7(%rax), %rdx
	cmpq	%rdx, %rsi
	jnb	.L3755
	movq	-1(%rax), %rdx
	testb	$1, %dl
	je	.L3755
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3755
	movq	-136(%rbp), %rdi
	addq	$2, %r15
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	leaq	30+_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE(%rip), %rax
	cmpq	%r15, %rax
	jne	.L3752
.L4273:
	movq	47(%r14), %rax
	testq	%rax, %rax
	jne	.L3756
	movslq	-168(%rbp), %r14
	addq	%r12, %r14
	leaq	279(%r13), %r12
	cmpq	%r12, %r14
	ja	.L3757
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3758:
	addq	$8, %r12
	cmpq	%r12, %r14
	jbe	.L3506
.L3757:
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L3758
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3758
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3758
	.p2align 4,,10
	.p2align 3
.L3448:
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, -128(%rbp)
	leaq	31(%r13), %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3743
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3744:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3743:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3744
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3744
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3744
	.p2align 4,,10
	.p2align 3
.L3449:
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, -128(%rbp)
	leaq	15(%r13), %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3740
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3741:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3740:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3741
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3741
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3741
	.p2align 4,,10
	.p2align 3
.L3450:
	leaq	7(%r13), %rsi
	leaq	15(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3738
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3738
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4274
.L3738:
	leaq	23(%r13), %rsi
	leaq	31(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	23(%r13), %rdx
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3451:
	leaq	7(%r13), %rsi
	leaq	15(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3452:
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, -128(%rbp)
	leaq	7(%r13), %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3734
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3735:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3734:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3735
	cmpl	$3, %edx
	je	.L3735
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3735
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3735
	.p2align 4,,10
	.p2align 3
.L3456:
	movzbl	7(%r14), %eax
	leaq	7(%r13), %r14
	salq	$3, %rax
	andl	$2040, %eax
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3775
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3776:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3775:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3776
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3776
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3776
	.p2align 4,,10
	.p2align 3
.L3457:
	leaq	47(%r13), %r12
	leaq	7(%r13), %rsi
	cmpq	%rsi, %r12
	jbe	.L3705
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3706
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4275
.L3706:
	movq	15(%r13), %rdx
	leaq	15(%r13), %rsi
	testb	$1, %dl
	je	.L3707
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4276
.L3707:
	movq	23(%r13), %rdx
	leaq	23(%r13), %rsi
	testb	$1, %dl
	je	.L3708
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4277
.L3708:
	movq	31(%r13), %rdx
	leaq	31(%r13), %rsi
	testb	$1, %dl
	je	.L3709
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4278
.L3709:
	movq	39(%r13), %rdx
	leaq	39(%r13), %rsi
	testb	$1, %dl
	je	.L3705
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4279
.L3705:
	leaq	111(%r13), %rax
	cmpq	%r12, %rax
	jbe	.L3506
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L3713
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4280
.L3713:
	movq	55(%r13), %rdx
	leaq	55(%r13), %rsi
	testb	$1, %dl
	je	.L3714
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4281
.L3714:
	movq	63(%r13), %rdx
	leaq	63(%r13), %rsi
	testb	$1, %dl
	je	.L3715
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4282
.L3715:
	movq	71(%r13), %rdx
	leaq	71(%r13), %rsi
	testb	$1, %dl
	je	.L3716
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4283
.L3716:
	movq	79(%r13), %rdx
	leaq	79(%r13), %rsi
	testb	$1, %dl
	je	.L3717
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4284
.L3717:
	movq	87(%r13), %rdx
	leaq	87(%r13), %rsi
	testb	$1, %dl
	je	.L3718
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4285
.L3718:
	movq	95(%r13), %rdx
	leaq	95(%r13), %rsi
	testb	$1, %dl
	je	.L3719
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4286
.L3719:
	movq	103(%r13), %rdx
	leaq	103(%r13), %rsi
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3458:
	movzbl	17(%r13), %eax
	leaq	23(%r13), %r14
	leal	(%rax,%rax,2), %eax
	sall	$4, %eax
	addl	$24, %eax
	cltq
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3701
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3702:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3701:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3702
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3702
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3702
	.p2align 4,,10
	.p2align 3
.L3494:
	movzbl	7(%r14), %eax
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3495:
	movl	7(%r13), %eax
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3490:
	leaq	7(%r13), %rsi
	leaq	15(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3491:
	movzbl	7(%r14), %r12d
	leaq	7(%r13), %rsi
	leaq	31(%r13), %rax
	sall	$3, %r12d
	cmpq	%rsi, %rax
	jbe	.L3498
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3499
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4287
.L3499:
	movq	15(%r13), %rdx
	leaq	15(%r13), %rsi
	testb	$1, %dl
	je	.L3500
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4288
.L3500:
	movq	23(%r13), %rdx
	leaq	23(%r13), %rsi
	testb	$1, %dl
	je	.L3498
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4289
.L3498:
	cmpl	$48, %r12d
	jne	.L3506
	leaq	39(%r13), %rsi
	leaq	47(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	39(%r13), %rdx
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3454:
	leaq	7(%r13), %rsi
	leaq	47(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3723
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3724
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4290
.L3724:
	movq	15(%r13), %rdx
	leaq	15(%r13), %rsi
	testb	$1, %dl
	je	.L3725
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4291
.L3725:
	movq	23(%r13), %rdx
	leaq	23(%r13), %rsi
	testb	$1, %dl
	je	.L3726
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4292
.L3726:
	movq	31(%r13), %rdx
	leaq	31(%r13), %rsi
	testb	$1, %dl
	je	.L3727
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4293
.L3727:
	movq	39(%r13), %rdx
	leaq	39(%r13), %rsi
	testb	$1, %dl
	je	.L3723
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4294
.L3723:
	leaq	55(%r13), %rsi
	leaq	71(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	55(%r13), %rdx
	testb	$1, %dl
	je	.L3731
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4295
.L3731:
	movq	63(%r13), %rdx
	leaq	63(%r13), %rsi
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3460:
	movzbl	9(%r13), %eax
	leaq	15(%r13), %r14
	sall	$5, %eax
	addl	$16, %eax
	cltq
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3695
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3696:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3695:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3696
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3696
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3696
	.p2align 4,,10
	.p2align 3
.L3453:
	leaq	15(%r13), %rsi
	leaq	23(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	15(%r13), %rdx
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3455:
	leaq	15(%r13), %rsi
	leaq	23(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	15(%r13), %rdx
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3476:
	movzbl	7(%r14), %r15d
	leaq	7(%r13), %rsi
	leal	0(,%r15,8), %eax
	movl	%eax, -168(%rbp)
	leaq	31(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3579
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3580
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4296
.L3580:
	movq	15(%r13), %rdx
	leaq	15(%r13), %rsi
	testb	$1, %dl
	je	.L3581
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4297
.L3581:
	movq	23(%r13), %rdx
	leaq	23(%r13), %rsi
	testb	$1, %dl
	je	.L3579
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4298
.L3579:
	movq	47(%r14), %rax
	testq	%rax, %rax
	jne	.L3583
	movslq	-168(%rbp), %r14
	addq	%r12, %r14
	leaq	55(%r13), %r12
	cmpq	%r12, %r14
	ja	.L3584
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3586:
	addq	$8, %r12
	cmpq	%r12, %r14
	jbe	.L3506
.L3584:
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L3586
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3586
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3586
	.p2align 4,,10
	.p2align 3
.L3459:
	movzbl	9(%r13), %eax
	leaq	15(%r13), %r14
	addl	$1, %eax
	sall	$4, %eax
	cltq
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3698
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3699:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3698:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3699
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3699
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3699
	.p2align 4,,10
	.p2align 3
.L3468:
	leaq	1943(%r13), %r15
	leaq	7(%r13), %r12
	cmpq	%r12, %r15
	jbe	.L3653
	leaq	15(%r13), %rax
	cmpq	%rax, %r15
	sbbq	%rax, %rax
	andb	$15, %al
	leaq	1936(%r12,%rax,8), %r14
	jmp	.L3652
	.p2align 4,,10
	.p2align 3
.L3651:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L3653
.L3652:
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L3651
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3651
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	addq	$8, %r12
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	cmpq	%r12, %r14
	jne	.L3652
	.p2align 4,,10
	.p2align 3
.L3653:
	leaq	1967(%r13), %rax
	cmpq	%r15, %rax
	jbe	.L3506
	leaq	1951(%r13), %r14
	movq	(%r15), %rdx
	cmpq	%r14, %rax
	sbbq	%r12, %r12
	andq	$-2, %r12
	addq	$3, %r12
	testb	$1, %dl
	je	.L3654
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4299
.L3654:
	subq	$1, %r12
	je	.L3506
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3657
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4300
.L3657:
	leaq	1959(%r13), %rsi
	cmpq	$1, %r12
	je	.L3506
	movq	1959(%r13), %rdx
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3472:
	movzbl	7(%r14), %r15d
	leaq	7(%r13), %rsi
	leal	0(,%r15,8), %eax
	movl	%eax, -168(%rbp)
	leaq	31(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3597
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3598
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4301
.L3598:
	movq	15(%r13), %rdx
	leaq	15(%r13), %rsi
	testb	$1, %dl
	je	.L3599
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4302
.L3599:
	movq	23(%r13), %rdx
	leaq	23(%r13), %rsi
	testb	$1, %dl
	je	.L3597
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4303
.L3597:
	leaq	63(%r13), %rsi
	leaq	71(%r13), %r15
	cmpq	%r15, %rsi
	jnb	.L3601
	movq	63(%r13), %rdx
	testb	$1, %dl
	jne	.L4304
.L3601:
	movq	47(%r14), %rax
	testq	%rax, %rax
	jne	.L3604
	.p2align 4,,10
	.p2align 3
.L4338:
	movslq	-168(%rbp), %rax
	addq	%rax, %r12
	cmpq	%r12, %r15
	jb	.L3605
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3607:
	addq	$8, %r15
	cmpq	%r15, %r12
	jbe	.L3506
.L3605:
	movq	(%r15), %rdx
	testb	$1, %dl
	je	.L3607
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3607
	movq	-136(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3607
	.p2align 4,,10
	.p2align 3
.L3473:
	movzbl	7(%r14), %eax
	leaq	7(%r13), %r14
	salq	$3, %rax
	andl	$2040, %eax
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3772
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3773:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3772:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3773
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3773
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3773
	.p2align 4,,10
	.p2align 3
.L3474:
	movzbl	7(%r14), %edx
	leaq	-144(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	sall	$3, %edx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_15ScavengeVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3475:
	movzbl	7(%r14), %edx
	leaq	-144(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	sall	$3, %edx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_15ScavengeVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3470:
	movzbl	7(%r14), %edx
	leaq	-144(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	sall	$3, %edx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_15ScavengeVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3469:
	leaq	71(%r13), %r12
	leaq	23(%r13), %rsi
	cmpq	%rsi, %r12
	jbe	.L3638
	movq	23(%r13), %rdx
	testb	$1, %dl
	je	.L3639
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4305
.L3639:
	movq	31(%r13), %rdx
	leaq	31(%r13), %rsi
	testb	$1, %dl
	je	.L3640
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4306
.L3640:
	movq	39(%r13), %rdx
	leaq	39(%r13), %rsi
	testb	$1, %dl
	je	.L3641
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4307
.L3641:
	movq	47(%r13), %rdx
	leaq	47(%r13), %rsi
	testb	$1, %dl
	je	.L3642
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4308
.L3642:
	movq	55(%r13), %rdx
	leaq	55(%r13), %rsi
	testb	$1, %dl
	je	.L3643
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4309
.L3643:
	movq	63(%r13), %rdx
	leaq	63(%r13), %rsi
	testb	$1, %dl
	je	.L3638
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4310
.L3638:
	leaq	79(%r13), %rax
	cmpq	%r12, %rax
	jbe	.L3506
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L3506
	cmpl	$3, %edx
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3471:
	movzbl	7(%r14), %r15d
	leaq	23(%r13), %r8
	leaq	7(%r13), %rsi
	leal	0(,%r15,8), %eax
	movl	%eax, -168(%rbp)
	cmpq	%rsi, %r8
	jbe	.L3619
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3620
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4311
.L3620:
	movq	15(%r13), %rdx
	leaq	15(%r13), %rsi
	testb	$1, %dl
	je	.L3619
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4312
.L3619:
	leaq	31(%r13), %r15
	cmpq	%r8, %r15
	jbe	.L3622
	movq	(%r8), %rdx
	testb	$1, %dl
	jne	.L4313
.L3622:
	movq	47(%r14), %rax
	testq	%rax, %rax
	jne	.L3625
	.p2align 4,,10
	.p2align 3
.L4340:
	movslq	-168(%rbp), %rax
	addq	%rax, %r12
	cmpq	%r12, %r15
	jb	.L3626
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3627:
	addq	$8, %r15
	cmpq	%r15, %r12
	jbe	.L3506
.L3626:
	movq	(%r15), %rdx
	testb	$1, %dl
	je	.L3627
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3627
	movq	-136(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3627
	.p2align 4,,10
	.p2align 3
.L3464:
	leaq	7(%r13), %rsi
	leaq	39(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3676
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4314
.L3676:
	movq	15(%r13), %rdx
	leaq	15(%r13), %rsi
	testb	$1, %dl
	je	.L3677
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4315
.L3677:
	movq	23(%r13), %rdx
	leaq	23(%r13), %rsi
	testb	$1, %dl
	je	.L3678
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4316
.L3678:
	movq	31(%r13), %rdx
	leaq	31(%r13), %rsi
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3465:
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, -128(%rbp)
	leaq	15(%r13), %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3671
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3672:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3671:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3672
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3672
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3672
	.p2align 4,,10
	.p2align 3
.L3466:
	movl	7(%r13), %eax
	movl	11(%r13), %edx
	addl	$23, %eax
	andl	$-8, %eax
	leal	(%rax,%rdx,8), %r14d
	cltq
	movslq	%r14d, %r14
	addq	%r12, %r14
	addq	%rax, %r12
	cmpq	%r12, %r14
	ja	.L3668
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3669:
	addq	$8, %r12
	cmpq	%r12, %r14
	jbe	.L3506
.L3668:
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L3669
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3669
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3669
	.p2align 4,,10
	.p2align 3
.L3467:
	leaq	15(%r13), %rsi
	leaq	47(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	15(%r13), %rdx
	testb	$1, %dl
	je	.L3664
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4317
.L3664:
	movq	23(%r13), %rdx
	leaq	23(%r13), %rsi
	testb	$1, %dl
	je	.L3665
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4318
.L3665:
	movq	31(%r13), %rdx
	leaq	31(%r13), %rsi
	testb	$1, %dl
	je	.L3666
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4319
.L3666:
	movq	39(%r13), %rdx
	leaq	39(%r13), %rsi
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3462:
	leaq	15(%r13), %rsi
	leaq	31(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	15(%r13), %rdx
	testb	$1, %dl
	je	.L3770
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4320
.L3770:
	movq	23(%r13), %rdx
	leaq	23(%r13), %rsi
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3484:
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, -128(%rbp)
	leaq	15(%r13), %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3531
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3532:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3531:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3532
	cmpl	$3, %edx
	je	.L3532
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3532
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3532
	.p2align 4,,10
	.p2align 3
.L3461:
	leaq	15(%r13), %rsi
	leaq	31(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	15(%r13), %rdx
	testb	$1, %dl
	je	.L3693
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4321
.L3693:
	movq	23(%r13), %rdx
	leaq	23(%r13), %rsi
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3463:
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, -128(%rbp)
	movq	%r15, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	39(%r13), %r8
	leaq	7(%r13), %rsi
	movslq	%eax, %r15
	cmpq	%rsi, %r8
	jbe	.L3681
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3682
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4322
.L3682:
	movq	15(%r13), %rdx
	leaq	15(%r13), %rsi
	testb	$1, %dl
	je	.L3683
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4323
.L3683:
	movq	23(%r13), %rdx
	leaq	23(%r13), %rsi
	testb	$1, %dl
	je	.L3684
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4324
.L3684:
	movq	31(%r13), %rdx
	leaq	31(%r13), %rsi
	testb	$1, %dl
	je	.L3681
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4325
.L3681:
	leaq	47(%r13), %r14
	cmpq	%r8, %r14
	jbe	.L3686
	movq	(%r8), %rdx
	testb	$1, %dl
	jne	.L4326
.L3686:
	addq	%r15, %r12
	cmpq	%r14, %r12
	ja	.L3690
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3689:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3690:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3689
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3689
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3689
	.p2align 4,,10
	.p2align 3
.L3480:
	leaq	31(%r13), %rax
	leaq	7(%r13), %rsi
	movq	%rax, -168(%rbp)
	movl	31(%r13), %eax
	leaq	15(%r13), %r14
	leal	48(,%rax,8), %eax
	movl	%eax, -176(%rbp)
	cmpq	%r14, %rsi
	jnb	.L3546
	movq	7(%r13), %rdx
	testb	$1, %dl
	jne	.L4327
.L3546:
	leaq	23(%r13), %r15
	cmpq	%r14, %r15
	jbe	.L3549
	movq	(%r14), %rdx
	testb	$1, %dl
	jne	.L4328
.L3549:
	cmpq	%r15, -168(%rbp)
	jbe	.L3552
	movq	(%r15), %rdx
	testb	$1, %dl
	jne	.L4329
	.p2align 4,,10
	.p2align 3
.L3552:
	movslq	-176(%rbp), %r14
	addq	%r14, %r12
	leaq	47(%r13), %r14
	cmpq	%r14, %r12
	ja	.L3555
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3556:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3555:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3556
	cmpl	$3, %edx
	je	.L3556
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3556
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3556
	.p2align 4,,10
	.p2align 3
.L3482:
	movq	-136(%rbp), %rax
	movq	%r13, -128(%rbp)
	movq	48(%rax), %rdx
	movslq	56(%rax), %rax
	leaq	(%rax,%rax,4), %r12
	salq	$4, %r12
	addq	%rdx, %r12
	movq	(%r12), %rax
	movq	8(%rax), %r15
	cmpq	$128, %r15
	je	.L4330
	leaq	1(%r15), %rdx
	movq	%rdx, 8(%rax)
	movq	%r13, 16(%rax,%r15,8)
.L3538:
	movq	-128(%rbp), %rcx
	xorl	%r12d, %r12d
	movq	31(%rcx), %rdx
	movq	%rdx, %rax
	sarq	$32, %rax
	testq	%rax, %rax
	jg	.L3540
	jmp	.L3544
	.p2align 4,,10
	.p2align 3
.L3542:
	movq	31(%rcx), %rdx
.L3541:
	movq	%rdx, %rax
	addq	$1, %r12
	sarq	$32, %rax
	cmpl	%r12d, %eax
	jle	.L3544
.L3540:
	movq	%r12, %rax
	salq	$4, %rax
	leaq	48(%rax,%rcx), %rax
	leaq	-1(%rax), %rsi
	leaq	7(%rax), %rdi
	cmpq	%rdi, %rsi
	jnb	.L3541
	movq	-1(%rax), %rdx
	testb	$1, %dl
	je	.L3542
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3542
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-128(%rbp), %rcx
	jmp	.L3542
	.p2align 4,,10
	.p2align 3
.L3483:
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, -128(%rbp)
	leaq	15(%r13), %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3534
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3535:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3534:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3535
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3535
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3535
	.p2align 4,,10
	.p2align 3
.L3481:
	leaq	7(%r13), %rsi
	leaq	15(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3478:
	movzbl	7(%r14), %edx
	leaq	-144(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	sall	$3, %edx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_15ScavengeVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3486:
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, -128(%rbp)
	leaq	15(%r13), %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3521
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3522:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3521:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3522
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3522
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3522
	.p2align 4,,10
	.p2align 3
.L3487:
	leaq	15(%r13), %rsi
	leaq	31(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3506
	movq	15(%r13), %rdx
	testb	$1, %dl
	je	.L3519
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4331
.L3519:
	movq	23(%r13), %rdx
	leaq	23(%r13), %rsi
	testb	$1, %dl
	je	.L3506
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3506
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3485:
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, -128(%rbp)
	leaq	23(%r13), %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	7(%r13), %rsi
	movslq	%eax, %r15
	cmpq	%rsi, %r14
	jbe	.L3525
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3526
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4332
.L3526:
	movq	15(%r13), %rdx
	leaq	15(%r13), %rsi
	testb	$1, %dl
	je	.L3525
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4333
.L3525:
	addq	%r15, %r12
	cmpq	%r14, %r12
	ja	.L3528
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3529:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3528:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3529
	cmpl	$3, %edx
	je	.L3529
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3529
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3529
	.p2align 4,,10
	.p2align 3
.L3477:
	movzbl	7(%r14), %r15d
	leaq	7(%r13), %rsi
	leal	0(,%r15,8), %eax
	movl	%eax, -168(%rbp)
	leaq	23(%r13), %rax
	cmpq	%rsi, %rax
	jbe	.L3562
	movq	7(%r13), %rdx
	testb	$1, %dl
	je	.L3563
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4334
.L3563:
	movq	15(%r13), %rdx
	leaq	15(%r13), %rsi
	testb	$1, %dl
	je	.L3562
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4335
.L3562:
	movq	47(%r14), %rax
	testq	%rax, %rax
	jne	.L3565
	movslq	-168(%rbp), %r14
	addq	%r12, %r14
	leaq	47(%r13), %r12
	cmpq	%r12, %r14
	ja	.L3566
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3568:
	addq	$8, %r12
	cmpq	%r12, %r14
	jbe	.L3506
.L3566:
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L3568
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3568
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3568
	.p2align 4,,10
	.p2align 3
.L3479:
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, -128(%rbp)
	leaq	15(%r13), %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r12
	cmpq	%r14, %r12
	ja	.L3558
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3559:
	addq	$8, %r14
	cmpq	%r14, %r12
	jbe	.L3506
.L3558:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L3559
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3559
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3559
	.p2align 4,,10
	.p2align 3
.L4266:
	movq	(%r14), %rax
	cmpq	$0, 8(%rax)
	je	.L4336
	movq	%rsi, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r8,%rdx)
	movq	8(%r14), %r12
.L3440:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L3439
	leaq	-1(%rax), %rdx
	salq	$4, %rax
	movq	%rdx, 8(%r12)
	movq	(%r12,%rax), %r13
	jmp	.L3439
	.p2align 4,,10
	.p2align 3
.L4336:
	movq	680(%r8), %rax
	leaq	640(%r8), %rdi
	movq	%r8, -168(%rbp)
	testq	%rax, %rax
	je	.L3435
	movq	%rdi, -176(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-168(%rbp), %r8
	movq	-176(%rbp), %rdi
	movq	680(%r8), %r12
	testq	%r12, %r12
	jne	.L3442
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	16(%rbx), %rcx
	movslq	24(%rbx), %r12
.L3435:
	movq	%r12, %r9
	movq	$0, -120(%rbp)
	xorl	%r14d, %r14d
	movl	-184(%rbp), %r12d
	jmp	.L3496
	.p2align 4,,10
	.p2align 3
.L3781:
	leaq	-1(%rdx), %rcx
	salq	$4, %rdx
	movq	%rcx, 8(%rax)
	addq	%rdx, %rax
	movq	(%rax), %r14
	movl	8(%rax), %r12d
	leaq	-1(%r14), %rax
.L3784:
	movq	(%rax), %rax
	movq	%rax, -120(%rbp)
.L3790:
	movq	-120(%rbp), %rdx
	movl	%r12d, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal9Scavenger32IterateAndScavengePromotedObjectENS0_10HeapObjectENS0_3MapEi
	movq	16(%rbx), %rcx
	movslq	24(%rbx), %r9
.L3496:
	leaq	(%r9,%r9,4), %rsi
	salq	$4, %rsi
	leaq	(%rcx,%rsi), %r13
	movq	8(%r13), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L3781
	movq	0(%r13), %rdx
	cmpq	$0, 8(%rdx)
	je	.L4337
	movq	%rax, %xmm0
	movq	%rdx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%rcx,%rsi)
	movq	8(%r13), %rax
.L3785:
	movq	8(%rax), %r14
	testq	%r14, %r14
	je	.L3812
	leaq	-1(%r14), %rdx
	salq	$4, %r14
	movq	%rdx, 8(%rax)
	addq	%r14, %rax
	movq	(%rax), %r14
	movl	8(%rax), %r12d
	leaq	-1(%r14), %rax
	jmp	.L3784
	.p2align 4,,10
	.p2align 3
.L4326:
	cmpl	$3, %edx
	je	.L3686
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3686
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3686
	.p2align 4,,10
	.p2align 3
.L4327:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3546
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3546
	.p2align 4,,10
	.p2align 3
.L4328:
	cmpl	$3, %edx
	je	.L3549
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3549
	movq	-136(%rbp), %rdi
	andq	$-3, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3549
.L4329:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3552
	movq	-136(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3552
	.p2align 4,,10
	.p2align 3
.L4304:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3601
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	47(%r14), %rax
	testq	%rax, %rax
	je	.L4338
.L3604:
	movq	$0, -120(%rbp)
	movb	$1, -128(%rbp)
	movl	$0, -124(%rbp)
	movq	47(%r14), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L3610
	movzbl	8(%r14), %eax
	movb	$0, -128(%rbp)
	sall	$3, %eax
	movl	%eax, -124(%rbp)
.L3610:
	movl	-168(%rbp), %eax
	movl	$72, %esi
	leaq	-128(%rbp), %r15
	leaq	-148(%rbp), %r14
	cmpl	$72, %eax
	jle	.L3506
	movq	%r12, -176(%rbp)
	movl	%esi, %r12d
	movq	%rbx, -192(%rbp)
	movl	%eax, %ebx
	movq	%r13, -168(%rbp)
	jmp	.L3611
	.p2align 4,,10
	.p2align 3
.L4263:
	movl	-148(%rbp), %r12d
.L3613:
	cmpl	%r12d, %ebx
	jle	.L4339
.L3611:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L4263
	movq	-168(%rbp), %rcx
	movslq	-148(%rbp), %rdx
	movq	-176(%rbp), %rdi
	leaq	-1(%rcx,%rdx), %r13
	movslq	%r12d, %rcx
	leaq	(%rcx,%rdi), %r12
	cmpq	%r12, %r13
	ja	.L3616
	jmp	.L3807
	.p2align 4,,10
	.p2align 3
.L3615:
	addq	$8, %r12
	cmpq	%r12, %r13
	jbe	.L4263
.L3616:
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L3615
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3615
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3615
	.p2align 4,,10
	.p2align 3
.L4313:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3622
	movq	-136(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	47(%r14), %rax
	testq	%rax, %rax
	je	.L4340
.L3625:
	movq	$0, -120(%rbp)
	movb	$1, -128(%rbp)
	movl	$0, -124(%rbp)
	movq	47(%r14), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L3629
	movzbl	8(%r14), %eax
	movb	$0, -128(%rbp)
	sall	$3, %eax
	movl	%eax, -124(%rbp)
.L3629:
	movl	-168(%rbp), %eax
	movl	$32, %esi
	leaq	-128(%rbp), %r15
	leaq	-148(%rbp), %r14
	cmpl	$32, %eax
	jle	.L3506
	movq	%r12, -176(%rbp)
	movl	%esi, %r12d
	movq	%rbx, -192(%rbp)
	movl	%eax, %ebx
	movq	%r13, -168(%rbp)
	jmp	.L3630
	.p2align 4,,10
	.p2align 3
.L4264:
	movl	-148(%rbp), %r12d
.L3632:
	cmpl	%r12d, %ebx
	jle	.L4341
.L3630:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L4264
	movq	-168(%rbp), %rcx
	movslq	-148(%rbp), %rdx
	movq	-176(%rbp), %rdi
	leaq	-1(%rcx,%rdx), %r13
	movslq	%r12d, %rcx
	leaq	(%rcx,%rdi), %r12
	cmpq	%r12, %r13
	ja	.L3635
	jmp	.L3809
	.p2align 4,,10
	.p2align 3
.L3634:
	addq	$8, %r12
	cmpq	%r12, %r13
	jbe	.L4264
.L3635:
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L3634
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3634
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3634
	.p2align 4,,10
	.p2align 3
.L4337:
	leaq	680(%rcx), %rax
	leaq	640(%rcx), %rdi
	movq	%rax, -192(%rbp)
	movq	680(%rcx), %rax
	testq	%rax, %rax
	je	.L3786
	movq	%r9, -184(%rbp)
	movq	%rcx, -176(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-176(%rbp), %rcx
	movq	-168(%rbp), %rdi
	movq	-184(%rbp), %r9
	movq	680(%rcx), %rax
	testq	%rax, %rax
	jne	.L4342
	movq	%r9, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %r9
.L3786:
	movq	704(%r13), %rdx
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L4343
	subq	$1, %rax
	movq	%rax, 8(%rdx)
	leaq	(%rax,%rax,2), %rax
	leaq	16(%rdx,%rax,8), %rax
	movdqu	(%rax), %xmm2
	movaps	%xmm2, -128(%rbp)
	movl	16(%rax), %edx
	movl	%edx, -112(%rbp)
	movq	(%rax), %r14
	movl	16(%rax), %r12d
	jmp	.L3790
	.p2align 4,,10
	.p2align 3
.L4343:
	movq	696(%r13), %rax
	cmpq	$0, 8(%rax)
	je	.L4344
	movq	%rdx, %xmm0
	movq	%rax, %xmm4
	leaq	(%r9,%r9,4), %rax
	punpcklqdq	%xmm4, %xmm0
	salq	$4, %rax
	movups	%xmm0, 696(%rcx,%rax)
	movq	704(%r13), %rdx
.L3794:
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L3790
	subq	$1, %rax
	movq	%rax, 8(%rdx)
	leaq	(%rax,%rax,2), %rax
	leaq	16(%rdx,%rax,8), %rax
	movdqu	(%rax), %xmm5
	movaps	%xmm5, -128(%rbp)
	movl	16(%rax), %edx
	movl	%edx, -112(%rbp)
	movq	(%rax), %r14
	movl	16(%rax), %r12d
	jmp	.L3790
	.p2align 4,,10
	.p2align 3
.L3812:
	movq	$-1, %rax
	xorl	%r12d, %r12d
	jmp	.L3784
	.p2align 4,,10
	.p2align 3
.L3442:
	movq	(%r12), %rax
	movq	%rax, 680(%r8)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3443
	movl	$4112, %esi
	call	_ZdlPvm@PLT
.L3443:
	movq	%r12, 8(%r14)
	jmp	.L3440
	.p2align 4,,10
	.p2align 3
.L4344:
	leaq	1376(%rcx), %rax
	leaq	1336(%rcx), %rdi
	movq	%rax, -184(%rbp)
	movq	1376(%rcx), %rax
	movq	%rcx, -168(%rbp)
	testq	%rax, %rax
	je	.L4259
	movq	%rdi, -176(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %rdi
	movq	1376(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L4345
	movl	%r12d, -184(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L3795:
	testb	%r15b, %r15b
	je	.L3799
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4346
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3544:
	.cfi_restore_state
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L4342:
	movq	%rax, -168(%rbp)
	movq	(%rax), %rdx
	movq	-192(%rbp), %rax
	movq	%rdx, (%rax)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	8(%r13), %rdi
	movq	-168(%rbp), %rax
	testq	%rdi, %rdi
	je	.L3789
	movl	$4112, %esi
	movq	%rax, -168(%rbp)
	call	_ZdlPvm@PLT
	movq	-168(%rbp), %rax
.L3789:
	movq	%rax, 8(%r13)
	jmp	.L3785
.L3756:
	movq	$0, -120(%rbp)
	movb	$1, -128(%rbp)
	movl	$0, -124(%rbp)
	movq	47(%r14), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L3760
	movzbl	8(%r14), %eax
	movb	$0, -128(%rbp)
	sall	$3, %eax
	movl	%eax, -124(%rbp)
.L3760:
	movl	-168(%rbp), %eax
	movl	$280, %esi
	leaq	-128(%rbp), %r15
	leaq	-148(%rbp), %r14
	cmpl	$280, %eax
	jle	.L3506
	movq	%r12, -176(%rbp)
	movl	%esi, %r12d
	movq	%rbx, -192(%rbp)
	movl	%eax, %ebx
	movq	%r13, -168(%rbp)
	jmp	.L3761
	.p2align 4,,10
	.p2align 3
.L4265:
	movl	-148(%rbp), %r12d
.L3763:
	cmpl	%r12d, %ebx
	jle	.L4347
.L3761:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L4265
	movq	-168(%rbp), %rcx
	movslq	-148(%rbp), %rdx
	movq	-176(%rbp), %rdi
	leaq	-1(%rcx,%rdx), %r13
	movslq	%r12d, %rcx
	leaq	(%rcx,%rdi), %r12
	cmpq	%r12, %r13
	ja	.L3766
	jmp	.L3811
	.p2align 4,,10
	.p2align 3
.L3765:
	addq	$8, %r12
	cmpq	%r12, %r13
	jbe	.L4265
.L3766:
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L3765
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3765
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3765
.L3565:
	movq	$0, -120(%rbp)
	movb	$1, -128(%rbp)
	movl	$0, -124(%rbp)
	movq	47(%r14), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L3570
	movzbl	8(%r14), %eax
	movb	$0, -128(%rbp)
	sall	$3, %eax
	movl	%eax, -124(%rbp)
.L3570:
	movl	-168(%rbp), %eax
	movl	$48, %esi
	leaq	-128(%rbp), %r15
	leaq	-148(%rbp), %r14
	cmpl	$48, %eax
	jle	.L3506
	movq	%r12, -176(%rbp)
	movl	%esi, %r12d
	movq	%rbx, -192(%rbp)
	movl	%eax, %ebx
	movq	%r13, -168(%rbp)
	jmp	.L3571
	.p2align 4,,10
	.p2align 3
.L4261:
	movl	-148(%rbp), %r12d
.L3573:
	cmpl	%r12d, %ebx
	jle	.L4348
.L3571:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L4261
	movq	-168(%rbp), %rcx
	movslq	-148(%rbp), %rdx
	movq	-176(%rbp), %rdi
	leaq	-1(%rcx,%rdx), %r13
	movslq	%r12d, %rcx
	leaq	(%rcx,%rdi), %r12
	cmpq	%r12, %r13
	ja	.L3576
	jmp	.L3803
	.p2align 4,,10
	.p2align 3
.L3575:
	addq	$8, %r12
	cmpq	%r12, %r13
	jbe	.L4261
.L3576:
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L3575
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3575
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3575
.L3583:
	movq	$0, -120(%rbp)
	movb	$1, -128(%rbp)
	movl	$0, -124(%rbp)
	movq	47(%r14), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L3588
	movzbl	8(%r14), %eax
	movb	$0, -128(%rbp)
	sall	$3, %eax
	movl	%eax, -124(%rbp)
.L3588:
	movl	-168(%rbp), %eax
	movl	$56, %esi
	leaq	-128(%rbp), %r15
	leaq	-148(%rbp), %r14
	cmpl	$56, %eax
	jle	.L3506
	movq	%r12, -176(%rbp)
	movl	%esi, %r12d
	movq	%rbx, -192(%rbp)
	movl	%eax, %ebx
	movq	%r13, -168(%rbp)
	jmp	.L3589
	.p2align 4,,10
	.p2align 3
.L4262:
	movl	-148(%rbp), %r12d
.L3591:
	cmpl	%r12d, %ebx
	jle	.L4349
.L3589:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L4262
	movq	-168(%rbp), %rcx
	movslq	-148(%rbp), %rdx
	movq	-176(%rbp), %rdi
	leaq	-1(%rcx,%rdx), %r13
	movslq	%r12d, %rcx
	leaq	(%rcx,%rdi), %r12
	cmpq	%r12, %r13
	ja	.L3594
	jmp	.L3805
	.p2align 4,,10
	.p2align 3
.L3593:
	addq	$8, %r12
	cmpq	%r12, %r13
	jbe	.L4262
.L3594:
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L3593
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3593
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3593
.L4259:
	movl	%r12d, -184(%rbp)
	jmp	.L3795
.L4341:
	movq	-168(%rbp), %r13
	movq	-192(%rbp), %rbx
	jmp	.L3506
.L4349:
	movq	-168(%rbp), %r13
	movq	-192(%rbp), %rbx
	jmp	.L3506
.L4339:
	movq	-168(%rbp), %r13
	movq	-192(%rbp), %rbx
	jmp	.L3506
.L4348:
	movq	-168(%rbp), %r13
	movq	-192(%rbp), %rbx
	jmp	.L3506
.L4347:
	movq	-168(%rbp), %r13
	movq	-192(%rbp), %rbx
	jmp	.L3506
.L4345:
	movq	(%rdx), %rax
	movq	-184(%rbp), %rcx
	movq	%rdx, -168(%rbp)
	movq	%rax, (%rcx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	704(%r13), %rdi
	movq	-168(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L3798
	movl	$112, %esi
	movq	%rdx, -168(%rbp)
	call	_ZdlPvm@PLT
	movq	-168(%rbp), %rdx
.L3798:
	movq	%rdx, 704(%r13)
	jmp	.L3794
.L4330:
	leaq	640(%rdx), %rdi
	movq	%rax, -192(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-176(%rbp), %rdx
	movq	-192(%rbp), %rax
	movq	680(%rdx), %rcx
	movq	%rcx, (%rax)
	movq	%rax, 680(%rdx)
	movq	-168(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movq	%r15, %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%r12)
	movq	8(%rdx), %rax
	cmpq	$128, %rax
	je	.L3538
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%r13, 16(%rdx,%rax,8)
	jmp	.L3538
.L4296:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3580
.L4298:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3579
.L4297:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3581
.L4272:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3747
.L4335:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3562
.L4295:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3731
.L3811:
	movl	%edx, %r12d
	jmp	.L3763
.L4334:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3563
.L4315:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3677
.L4316:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3678
.L3803:
	movl	%edx, %r12d
	jmp	.L3573
.L4311:
	movq	-136(%rbp), %rdi
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-176(%rbp), %r8
	jmp	.L3620
.L4299:
	movq	-136(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3654
.L4286:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3719
.L4333:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3525
.L4308:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3642
.L4309:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3643
.L4319:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3666
.L4320:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3770
.L4321:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3693
.L4322:
	movq	-136(%rbp), %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-168(%rbp), %r8
	jmp	.L3682
.L4292:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3726
.L4293:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3727
.L4291:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3725
.L4294:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3723
.L3805:
	movl	%edx, %r12d
	jmp	.L3591
.L3807:
	movl	%edx, %r12d
	jmp	.L3613
.L4302:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3599
.L4312:
	movq	-136(%rbp), %rdi
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-176(%rbp), %r8
	jmp	.L3619
.L4306:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3640
.L4307:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3641
.L4305:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3639
.L4310:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3638
.L3809:
	movl	%edx, %r12d
	jmp	.L3632
.L4274:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3738
.L4317:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3664
.L4318:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3665
.L4279:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3705
.L4276:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3707
.L4277:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3708
.L4278:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3709
.L4280:
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3713
.L4281:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3714
.L4282:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3715
.L4283:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3716
.L4287:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3499
.L4289:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3498
.L4288:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3500
.L4290:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3724
.L4301:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3598
.L4303:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3597
.L4323:
	movq	-136(%rbp), %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-168(%rbp), %r8
	jmp	.L3683
.L4325:
	movq	-136(%rbp), %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-168(%rbp), %r8
	jmp	.L3681
.L4324:
	movq	-136(%rbp), %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	movq	-168(%rbp), %r8
	jmp	.L3684
.L4314:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3676
.L4331:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3519
.L4332:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3526
.L4284:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3717
.L4285:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3718
.L4275:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3706
.L4271:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3748
.L4268:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3511
.L4269:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3512
.L4267:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3510
.L4270:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3509
.L4300:
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Scavenger14ScavengeObjectINS0_18FullHeapObjectSlotEEENS0_18SlotCallbackResultET_NS0_10HeapObjectE.constprop.0
	jmp	.L3657
.L4346:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30421:
	.size	_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE.constprop.0, .-_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE.constprop.0
	.section	.rodata._ZN2v88internal18ScavengerCollector14CollectGarbageEv.str1.1,"aMS",@progbits,1
.LC12:
	.string	"disabled-by-default-v8.gc"
	.section	.text._ZN2v88internal18ScavengerCollector14CollectGarbageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ScavengerCollector14CollectGarbageEv
	.type	_ZN2v88internal18ScavengerCollector14CollectGarbageEv, @function
_ZN2v88internal18ScavengerCollector14CollectGarbageEv:
.LFB22386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$3400, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	45640(%rax), %rsi
	leaq	-3200(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -3400(%rbp)
	call	_ZN2v88internal15ItemParallelJobC1EPNS0_21CancelableTaskManagerEPNS_4base9SemaphoreE@PLT
	movq	(%r15), %rdi
	call	_ZN2v88internal7Isolate19LogObjectRelocationEv@PLT
	cmpb	$0, _ZN2v88internal22FLAG_parallel_scavengeE(%rip)
	movzbl	%al, %r14d
	jne	.L4351
.L4356:
	movl	$1, -3336(%rbp)
.L4352:
	leaq	-2976(%rbp), %rax
	leaq	-2864(%rbp), %rbx
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movq	%rax, %rdi
	movq	%rax, -3368(%rbp)
	call	_ZN2v84base17ConditionVariableC1Ev@PLT
	leaq	-2928(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -3424(%rbp)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	-2224(%rbp), %rdi
	movq	$2000, -2888(%rbp)
	movq	$0, -2880(%rbp)
	movb	$0, -2872(%rbp)
	movq	%rbx, -3352(%rbp)
	call	_ZN2v84base5MutexC1Ev@PLT
	movl	-3336(%rbp), %eax
	movq	$0, -2184(%rbp)
	movl	%eax, -2176(%rbp)
	.p2align 4,,10
	.p2align 3
.L4357:
	movl	$4112, %edi
	addl	$1, %r13d
	addq	$80, %rbx
	call	_Znwm@PLT
	movl	$512, %ecx
	movq	%rax, %rdx
	movq	$0, 8(%rax)
	leaq	16(%rax), %rdi
	movq	%r12, %rax
	rep stosq
	movq	%rdx, -80(%rbx)
	movl	$4112, %edi
	call	_Znwm@PLT
	movl	$512, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	movq	%r12, %rax
	rep stosq
	movq	%rdx, -72(%rbx)
	cmpl	%r13d, -2176(%rbp)
	jg	.L4357
	leaq	-1456(%rbp), %rbx
	leaq	-816(%rbp), %rdi
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movq	%rbx, -3360(%rbp)
	call	_ZN2v84base5MutexC1Ev@PLT
	movl	-3336(%rbp), %eax
	movq	$0, -776(%rbp)
	movl	%eax, -768(%rbp)
	.p2align 4,,10
	.p2align 3
.L4358:
	movl	$4112, %edi
	addl	$1, %r13d
	addq	$80, %rbx
	call	_Znwm@PLT
	movl	$512, %ecx
	movq	%rax, %rdx
	movq	$0, 8(%rax)
	leaq	16(%rax), %rdi
	movq	%r12, %rax
	rep stosq
	movq	%rdx, -80(%rbx)
	movl	$4112, %edi
	call	_Znwm@PLT
	movl	$512, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	movq	%r12, %rax
	rep stosq
	movq	%rdx, -72(%rbx)
	cmpl	%r13d, -768(%rbp)
	jg	.L4358
	leaq	-120(%rbp), %rdi
	xorl	%r12d, %r12d
	call	_ZN2v84base5MutexC1Ev@PLT
	movl	-3336(%rbp), %eax
	movq	$0, -80(%rbp)
	movl	%eax, -72(%rbp)
	leaq	-760(%rbp), %rax
	movq	%rax, -3440(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L4359:
	movl	$112, %edi
	addl	$1, %r12d
	addq	$80, %rbx
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movq	%rax, -80(%rbx)
	movq	$0, 24(%rax)
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 40(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 88(%rax)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -72(%rbx)
	movq	$0, 24(%rax)
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 40(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 88(%rax)
	cmpl	%r12d, -72(%rbp)
	jg	.L4359
	leaq	-2160(%rbp), %rbx
	leaq	-1520(%rbp), %rdi
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movq	%rbx, -3344(%rbp)
	call	_ZN2v84base5MutexC1Ev@PLT
	movl	-3336(%rbp), %eax
	movq	$0, -1480(%rbp)
	movl	%eax, -1472(%rbp)
	.p2align 4,,10
	.p2align 3
.L4360:
	movl	$1040, %edi
	addl	$1, %r13d
	addq	$80, %rbx
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	%rax, %rdx
	movq	$0, 8(%rax)
	leaq	16(%rax), %rdi
	movq	%r12, %rax
	rep stosq
	movq	%rdx, -80(%rbx)
	movl	$1040, %edi
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	movq	%r12, %rax
	rep stosq
	movq	%rdx, -72(%rbx)
	cmpl	%r13d, -1472(%rbp)
	jg	.L4360
	leaq	-3040(%rbp), %rax
	xorl	%r12d, %r12d
	movq	%rax, -3376(%rbp)
	leaq	-3136(%rbp), %rax
	movq	%rax, -3320(%rbp)
	leaq	-3176(%rbp), %rax
	movq	%rax, -3384(%rbp)
	.p2align 4,,10
	.p2align 3
.L4366:
	movl	$776, %edi
	call	_Znwm@PLT
	movq	8(%r15), %rdx
	pushq	%r12
	movl	%r14d, %ecx
	pushq	-3344(%rbp)
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	-3360(%rbp), %r9
	movq	-3352(%rbp), %r8
	call	_ZN2v88internal9ScavengerC1EPNS0_18ScavengerCollectorEPNS0_4HeapEbPNS0_8WorklistISt4pairINS0_10HeapObjectEiELi256EEEPNS1_13PromotionListEPNS6_INS0_18EphemeronHashTableELi128EEEi
	movq	-3376(%rbp), %rax
	movl	$104, %edi
	movq	%rbx, (%rax,%r12,8)
	call	_Znwm@PLT
	movq	8(%r15), %r13
	movq	%rax, %rdi
	movq	%rax, -3328(%rbp)
	leaq	-37592(%r13), %rsi
	call	_ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE@PLT
	movq	%r13, %xmm0
	movq	%rbx, %xmm1
	movq	-3328(%rbp), %rax
	leaq	16+_ZTVN2v88internal14ScavengingTaskE(%rip), %rcx
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, (%rax)
	leaq	56(%rcx), %rsi
	movq	-3368(%rbp), %rcx
	movq	%rsi, 32(%rax)
	movq	-3168(%rbp), %rsi
	movq	%rcx, 96(%rax)
	movups	%xmm0, 80(%rax)
	popq	%rdx
	popq	%rcx
	movq	%rax, -3136(%rbp)
	cmpq	-3160(%rbp), %rsi
	je	.L4361
	movq	$0, -3136(%rbp)
	movq	%rax, (%rsi)
	addq	$8, -3168(%rbp)
.L4362:
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4363
	movq	(%rdi), %rax
	addq	$1, %r12
	call	*8(%rax)
	cmpl	%r12d, -3336(%rbp)
	jg	.L4366
.L4364:
	movq	8(%r15), %rax
	leaq	-3304(%rbp), %rbx
	movq	%rbx, %rdi
	movq	%rbx, -3432(%rbp)
	movq	2016(%rax), %rax
	movq	9984(%rax), %r12
	movq	%r12, %rsi
	call	_ZN2v88internal7Sweeper20PauseOrCompleteScopeC1EPS1_@PLT
	leaq	-3248(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3408(%rbp)
	call	_ZN2v88internal7Sweeper24FilterSweepingPagesScopeC1EPS1_RKNS1_20PauseOrCompleteScopeE@PLT
	cmpb	$0, -3208(%rbp)
	je	.L4369
	movq	-3248(%rbp), %r12
	movq	-3240(%rbp), %rbx
	leaq	192(%r12), %r13
	cmpq	-3232(%rbp), %rbx
	jne	.L4374
	jmp	.L4369
	.p2align 4,,10
	.p2align 3
.L4371:
	addq	$8, %rbx
	cmpq	%rbx, -3232(%rbp)
	je	.L4369
.L4374:
	movq	(%rbx), %rax
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L4371
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L4371
	cmpq	$0, 136(%rax)
	jne	.L4371
	movq	200(%r12), %rsi
	cmpq	208(%r12), %rsi
	je	.L4373
	movq	(%rbx), %rax
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, 200(%r12)
	cmpq	%rbx, -3232(%rbp)
	jne	.L4374
	.p2align 4,,10
	.p2align 3
.L4369:
	movq	8(%r15), %rax
	xorl	%r12d, %r12d
	leaq	.L4377(%rip), %r13
	movq	256(%rax), %rdx
	movq	32(%rdx), %rsi
	movq	264(%rax), %rdx
	movq	%rsi, -3416(%rbp)
	movq	32(%rdx), %rsi
	movq	272(%rax), %rdx
	movq	%rsi, -3384(%rbp)
	movq	32(%rdx), %rsi
	movq	280(%rax), %rdx
	movq	288(%rax), %rax
	movq	%rsi, -3392(%rbp)
	movq	32(%rdx), %rsi
	movq	32(%rax), %rbx
	movq	%rsi, -3328(%rbp)
.L4368:
	cmpl	$4, %r12d
	ja	.L4375
.L4706:
	movslq	0(%r13,%r12,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal18ScavengerCollector14CollectGarbageEv,"a",@progbits
	.align 4
	.align 4
.L4377:
	.long	.L4375-.L4377
	.long	.L4380-.L4377
	.long	.L4379-.L4377
	.long	.L4378-.L4377
	.long	.L4376-.L4377
	.section	.text._ZN2v88internal18ScavengerCollector14CollectGarbageEv
	.p2align 4,,10
	.p2align 3
.L4363:
	addq	$1, %r12
	cmpl	%r12d, -3336(%rbp)
	jg	.L4366
	jmp	.L4364
	.p2align 4,,10
	.p2align 3
.L4361:
	movq	-3320(%rbp), %rdx
	movq	-3384(%rbp), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal15ItemParallelJob4TaskESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L4362
	.p2align 4,,10
	.p2align 3
.L4376:
	testq	%rbx, %rbx
	je	.L4382
	movq	%rbx, %r14
	movq	224(%rbx), %rbx
	movl	$4, %r12d
.L4381:
	movq	104(%r14), %rdx
	movq	120(%r14), %rax
	orq	%rax, %rdx
	jne	.L4506
	cmpq	$0, 136(%r14)
	je	.L4368
.L4506:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal18PageScavengingItemE(%rip), %rsi
	movq	%rsi, (%rax)
	movq	-3192(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	%r14, 16(%rax)
	movq	%rax, -3136(%rbp)
	cmpq	-3184(%rbp), %rsi
	je	.L4384
	movq	%rax, (%rsi)
	addq	$8, -3192(%rbp)
	cmpl	$4, %r12d
	jbe	.L4706
.L4375:
	movq	-3416(%rbp), %rax
	testq	%rax, %rax
	je	.L4380
	movq	%rax, %r14
	movq	224(%rax), %rax
	xorl	%r12d, %r12d
	movq	%rax, -3416(%rbp)
	jmp	.L4381
	.p2align 4,,10
	.p2align 3
.L4378:
	movq	-3328(%rbp), %rax
	testq	%rax, %rax
	je	.L4376
	movq	%rax, %r14
	movq	224(%rax), %rax
	movl	$3, %r12d
	movq	%rax, -3328(%rbp)
	jmp	.L4381
	.p2align 4,,10
	.p2align 3
.L4379:
	movq	-3384(%rbp), %rax
	testq	%rax, %rax
	je	.L4378
	movq	%rax, %r14
	movq	224(%rax), %rax
	movl	$2, %r12d
	movq	%rax, -3384(%rbp)
	jmp	.L4381
	.p2align 4,,10
	.p2align 3
.L4380:
	movq	-3392(%rbp), %rax
	testq	%rax, %rax
	je	.L4379
	movq	%rax, %r14
	movq	224(%rax), %rax
	movl	$1, %r12d
	movq	%rax, -3392(%rbp)
	jmp	.L4381
	.p2align 4,,10
	.p2align 3
.L4384:
	movq	-3320(%rbp), %rdx
	movq	-3400(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal15ItemParallelJob4ItemESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L4368
	.p2align 4,,10
	.p2align 3
.L4382:
	leaq	16+_ZTVN2v88internal19RootScavengeVisitorE(%rip), %rax
	movq	-3040(%rbp), %r14
	movl	$91, %edx
	movq	-3320(%rbp), %rdi
	movq	%rax, -3296(%rbp)
	movq	8(%r15), %rax
	movq	%r14, -3288(%rbp)
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic253(%rip), %r13
	testq	%r13, %r13
	je	.L4707
.L4387:
	movq	$0, -3280(%rbp)
	movzbl	0(%r13), %eax
	leaq	-3072(%rbp), %rcx
	movq	%rcx, -3392(%rbp)
	testb	$5, %al
	jne	.L4708
.L4389:
	movq	(%r15), %rax
	movq	_ZN2v88internal8JSObject21IsUnmodifiedApiObjectENS0_14FullObjectSlotE@GOTPCREL(%rip), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles29IdentifyWeakUnmodifiedObjectsEPFbNS0_14FullObjectSlotEE@PLT
	leaq	-3280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -3384(%rbp)
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-3320(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	movq	8(%r15), %rax
	movl	$94, %edx
	movq	%rbx, %rdi
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic261(%rip), %r13
	testq	%r13, %r13
	je	.L4709
.L4394:
	movq	$0, -3280(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L4710
.L4396:
	movq	8(%r15), %rdi
	leaq	-3296(%rbp), %r12
	movl	$3, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal4Heap12IterateRootsEPNS0_11RootVisitorENS0_9VisitModeE@PLT
	movq	-3384(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-3320(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	movq	8(%r15), %rax
	movl	$93, %edx
	movq	%rbx, %rdi
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic266(%rip), %rax
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4711
.L4401:
	movq	$0, -3280(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L4712
.L4403:
	movq	-3400(%rbp), %rdi
	call	_ZN2v88internal15ItemParallelJob3RunEv@PLT
	movq	-3384(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-3320(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	movq	8(%r15), %rax
	movl	$92, %edx
	movq	%rbx, %rdi
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic273(%rip), %rax
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4713
.L4408:
	movq	$0, -3280(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L4714
.L4410:
	movq	(%r15), %rax
	leaq	_ZN2v88internal12_GLOBAL__N_127IsUnscavengedHeapObjectSlotEPNS0_4HeapENS0_14FullObjectSlotE(%rip), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles37MarkYoungWeakUnmodifiedObjectsPendingEPFbPNS0_4HeapENS0_14FullObjectSlotEE@PLT
	movq	(%r15), %rax
	movq	%r12, %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles44IterateYoungWeakUnmodifiedRootsForFinalizersEPNS0_11RootVisitorE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE.constprop.0
	movq	(%r15), %rax
	leaq	_ZN2v88internal12_GLOBAL__N_127IsUnscavengedHeapObjectSlotEPNS0_4HeapENS0_14FullObjectSlotE(%rip), %rdx
	movq	%r12, %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles48IterateYoungWeakUnmodifiedRootsForPhantomHandlesEPNS0_11RootVisitorEPFbPNS0_4HeapENS0_14FullObjectSlotEE@PLT
	movq	-3384(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-3320(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	movq	8(%r15), %rax
	movl	$97, %edx
	movq	%rbx, %rdi
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic290(%rip), %r13
	testq	%r13, %r13
	je	.L4715
.L4415:
	movq	$0, -3280(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L4716
.L4417:
	movl	-3336(%rbp), %eax
	movq	%r15, -3336(%rbp)
	movq	-3376(%rbp), %rbx
	subl	$1, %eax
	leaq	-3032(%rbp,%rax,8), %rax
	movq	%rax, -3328(%rbp)
	.p2align 4,,10
	.p2align 3
.L4445:
	movq	(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal9Scavenger8FinalizeEv
	testq	%r12, %r12
	je	.L4421
	movq	728(%r12), %r15
	testq	%r15, %r15
	je	.L4429
	.p2align 4,,10
	.p2align 3
.L4422:
	movq	32(%r15), %r13
	movq	(%r15), %r14
	testq	%r13, %r13
	je	.L4428
	.p2align 4,,10
	.p2align 3
.L4425:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L4425
.L4428:
	movq	24(%r15), %rax
	movq	16(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r15), %rdi
	leaq	64(%r15), %rax
	movq	$0, 40(%r15)
	movq	$0, 32(%r15)
	cmpq	%rax, %rdi
	je	.L4717
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L4429
.L4430:
	movq	%r14, %r15
	jmp	.L4422
	.p2align 4,,10
	.p2align 3
.L4717:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L4430
	.p2align 4,,10
	.p2align 3
.L4429:
	movq	720(%r12), %rax
	movq	712(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	712(%r12), %rdi
	leaq	760(%r12), %rax
	movq	$0, 736(%r12)
	movq	$0, 728(%r12)
	cmpq	%rax, %rdi
	je	.L4423
	call	_ZdlPv@PLT
.L4423:
	movq	672(%r12), %r13
	testq	%r13, %r13
	je	.L4434
	.p2align 4,,10
	.p2align 3
.L4431:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L4431
.L4434:
	movq	664(%r12), %rax
	movq	656(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	656(%r12), %rdi
	leaq	704(%r12), %rax
	movq	$0, 680(%r12)
	movq	$0, 672(%r12)
	cmpq	%rax, %rdi
	je	.L4432
	call	_ZdlPv@PLT
.L4432:
	leaq	624(%r12), %rdi
	call	_ZN2v88internal21LocalAllocationBuffer5CloseEv@PLT
	leaq	16+_ZTVN2v88internal10PagedSpaceE(%rip), %rax
	leaq	392(%r12), %rdi
	movq	%rax, 392(%r12)
	call	_ZN2v88internal10PagedSpace8TearDownEv@PLT
	leaq	584(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	440(%r12), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, 392(%r12)
	testq	%rdi, %rdi
	je	.L4435
	call	_ZdaPv@PLT
.L4435:
	movq	488(%r12), %rdi
	movq	$0, 440(%r12)
	testq	%rdi, %rdi
	je	.L4436
	movq	(%rdi), %rax
	call	*8(%rax)
.L4436:
	movq	400(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4437
	call	_ZdlPv@PLT
.L4437:
	leaq	16+_ZTVN2v88internal10PagedSpaceE(%rip), %rax
	leaq	160(%r12), %rdi
	movq	%rax, 160(%r12)
	call	_ZN2v88internal10PagedSpace8TearDownEv@PLT
	leaq	352(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	208(%r12), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, 160(%r12)
	testq	%rdi, %rdi
	je	.L4438
	call	_ZdaPv@PLT
.L4438:
	movq	256(%r12), %rdi
	movq	$0, 208(%r12)
	testq	%rdi, %rdi
	je	.L4439
	movq	(%rdi), %rax
	call	*8(%rax)
.L4439:
	movq	168(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4440
	call	_ZdlPv@PLT
.L4440:
	movq	80(%r12), %r13
	testq	%r13, %r13
	je	.L4444
	.p2align 4,,10
	.p2align 3
.L4441:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L4441
.L4444:
	movq	72(%r12), %rax
	movq	64(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	64(%r12), %rdi
	leaq	112(%r12), %rax
	movq	$0, 88(%r12)
	movq	$0, 80(%r12)
	cmpq	%rax, %rdi
	je	.L4442
	call	_ZdlPv@PLT
.L4442:
	movl	$776, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4421:
	addq	$8, %rbx
	cmpq	%rbx, -3328(%rbp)
	jne	.L4445
	movq	-3336(%rbp), %r15
	movq	64(%r15), %rbx
	testq	%rbx, %rbx
	je	.L4449
	.p2align 4,,10
	.p2align 3
.L4446:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rax
	movq	%rax, -1(%rsi)
	andq	$-262144, %rsi
	movq	8(%r15), %rax
	movq	280(%rax), %rdi
	call	_ZN2v88internal16LargeObjectSpace21PromoteNewLargeObjectEPNS0_9LargePageE@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L4446
	movq	64(%r15), %rbx
	testq	%rbx, %rbx
	je	.L4449
	.p2align 4,,10
	.p2align 3
.L4450:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L4450
.L4449:
	movq	56(%r15), %rax
	movq	48(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, 72(%r15)
	movq	-3384(%rbp), %rdi
	movq	$0, 64(%r15)
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-3320(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	movq	-3408(%rbp), %rdi
	call	_ZN2v88internal7Sweeper24FilterSweepingPagesScopeD1Ev@PLT
	movq	-3432(%rbp), %rdi
	call	_ZN2v88internal7Sweeper20PauseOrCompleteScopeD1Ev@PLT
	movq	8(%r15), %rax
	movl	$95, %edx
	movq	%rbx, %rdi
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic305(%rip), %r13
	testq	%r13, %r13
	je	.L4447
.L4448:
	movq	$0, -3248(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L4718
.L4452:
	movq	_ZN2v88internal4Heap46UpdateYoungReferenceInExternalStringTableEntryEPS1_NS0_14FullObjectSlotE@GOTPCREL(%rip), %rsi
	movq	8(%r15), %rdi
	call	_ZN2v88internal4Heap42UpdateYoungReferencesInExternalStringTableEPFNS0_6StringEPS1_NS0_14FullObjectSlotEE@PLT
	movq	8(%r15), %rax
	movq	2064(%rax), %rdi
	call	_ZN2v88internal18IncrementalMarking34UpdateMarkingWorklistAfterScavengeEv@PLT
	movq	-3408(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-3320(%rbp), %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	cmpb	$0, _ZN2v88internal23FLAG_concurrent_markingE(%rip)
	movq	8(%r15), %rdi
	jne	.L4719
.L4457:
	movq	-3320(%rbp), %rbx
	leaq	16+_ZTVN2v88internal26ScavengeWeakObjectRetainerE(%rip), %rax
	movq	%rax, -3136(%rbp)
	movq	%rbx, %rsi
	call	_ZN2v88internal4Heap26ProcessYoungWeakReferencesEPNS0_18WeakObjectRetainerE@PLT
	movq	-3344(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18ScavengerCollector20ClearYoungEphemeronsEPNS0_8WorklistINS0_18EphemeronHashTableELi128EEE
	movq	%r15, %rdi
	call	_ZN2v88internal18ScavengerCollector18ClearOldEphemeronsEv
	movq	8(%r15), %rax
	movq	248(%rax), %rdi
	movq	104(%rdi), %rsi
	addq	$208, %rdi
	call	_ZN2v88internal9SemiSpace12set_age_markEm@PLT
	movq	8(%r15), %rax
	movl	$90, %edx
	movq	%rbx, %rdi
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic327(%rip), %r13
	testq	%r13, %r13
	je	.L4720
.L4461:
	movq	$0, -3248(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L4721
.L4463:
	movq	8(%r15), %rdi
	call	_ZN2v88internal18ArrayBufferTracker27PrepareToFreeDeadInNewSpaceEPNS0_4HeapE@PLT
	movq	-3408(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-3320(%rbp), %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	movq	8(%r15), %rax
	movq	2040(%rax), %rdi
	call	_ZN2v88internal20ArrayBufferCollector15FreeAllocationsEv@PLT
	movq	8(%r15), %rax
	movq	-3392(%rbp), %rbx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal18ScavengerCollector14CollectGarbageEvEUlNS2_10HeapObjectEE1_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation(%rip), %rsi
	movq	%rsi, %xmm0
	movq	296(%rax), %rdi
	leaq	_ZNSt17_Function_handlerIFbN2v88internal10HeapObjectEEZNS1_18ScavengerCollector14CollectGarbageEvEUlS2_E1_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rbx, %rsi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -3056(%rbp)
	call	_ZN2v88internal19NewLargeObjectSpace15FreeDeadObjectsERKSt8functionIFbNS0_10HeapObjectEEE@PLT
	movq	-3056(%rbp), %rax
	testq	%rax, %rax
	je	.L4467
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L4467:
	movq	8(%r15), %rax
	movq	%r15, -3416(%rbp)
	xorl	%ebx, %ebx
	movq	256(%rax), %rdx
	movq	32(%rdx), %rcx
	movq	264(%rax), %rdx
	movq	32(%rdx), %rsi
	movq	272(%rax), %rdx
	movq	%rcx, -3392(%rbp)
	movq	32(%rdx), %rcx
	movq	280(%rax), %rdx
	movq	%rsi, -3376(%rbp)
	movq	288(%rax), %rax
	movq	32(%rdx), %rsi
	movq	%rcx, -3384(%rbp)
	movq	32(%rax), %rax
	movq	%rsi, -3336(%rbp)
	movq	%rax, -3328(%rbp)
	.p2align 4,,10
	.p2align 3
.L4468:
	cmpl	$4, %ebx
	ja	.L4469
.L4724:
	leaq	.L4471(%rip), %rcx
	movslq	(%rcx,%rbx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal18ScavengerCollector14CollectGarbageEv
	.align 4
	.align 4
.L4471:
	.long	.L4469-.L4471
	.long	.L4474-.L4471
	.long	.L4473-.L4471
	.long	.L4472-.L4471
	.long	.L4470-.L4471
	.section	.text._ZN2v88internal18ScavengerCollector14CollectGarbageEv
	.p2align 4,,10
	.p2align 3
.L4470:
	movq	-3328(%rbp), %rax
	testq	%rax, %rax
	je	.L4476
	movq	%rax, %rdx
	movq	224(%rax), %rax
	movl	$4, %ebx
	movq	%rax, -3328(%rbp)
.L4475:
	movq	104(%rdx), %rcx
	leaq	104(%rdx), %rsi
	movq	120(%rdx), %rax
	orq	%rax, %rcx
	jne	.L4504
	cmpq	$0, 136(%rdx)
	je	.L4468
.L4504:
	movq	168(%rdx), %rax
	testq	%rax, %rax
	jne	.L4478
	movq	(%rsi), %r12
	testq	%r12, %r12
	je	.L4468
	movq	(%rdx), %rax
	addq	$262143, %rax
	shrq	$18, %rax
	je	.L4468
	leaq	(%rax,%rax,2), %r14
	salq	$7, %r14
	addq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L4491:
	movq	%r12, %r13
	leaq	256(%r12), %r15
	jmp	.L4484
	.p2align 4,,10
	.p2align 3
.L4480:
	addq	$8, %r13
	cmpq	%r15, %r13
	je	.L4722
.L4484:
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L4480
	leaq	128(%rax), %rdx
.L4483:
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L4480
	addq	$4, %rax
	cmpq	%rax, %rdx
	jne	.L4483
	movq	0(%r13), %rdi
	movq	$0, 0(%r13)
	testq	%rdi, %rdi
	je	.L4480
	call	_ZdaPv@PLT
	addq	$8, %r13
	cmpq	%r15, %r13
	jne	.L4484
	.p2align 4,,10
	.p2align 3
.L4722:
	leaq	264(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	jmp	.L4490
	.p2align 4,,10
	.p2align 3
.L4486:
	movq	-8(%rdi), %r15
	subq	$8, %rdi
	movq	%rdi, 352(%r12)
	testq	%r15, %r15
	je	.L4489
.L4487:
	movq	%r15, %rdi
	call	_ZdaPv@PLT
.L4490:
	movq	352(%r12), %rdi
.L4489:
	cmpq	320(%r12), %rdi
	je	.L4485
.L4723:
	cmpq	360(%r12), %rdi
	jne	.L4486
	movq	376(%r12), %rax
	movq	-8(%rax), %rax
	movq	504(%rax), %r15
	call	_ZdlPv@PLT
	movq	376(%r12), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 376(%r12)
	movq	-8(%rax), %rdi
	leaq	512(%rdi), %rax
	movq	%rdi, 360(%r12)
	addq	$504, %rdi
	movq	%rax, 368(%r12)
	movq	%rdi, 352(%r12)
	testq	%r15, %r15
	jne	.L4487
	cmpq	320(%r12), %rdi
	jne	.L4723
	.p2align 4,,10
	.p2align 3
.L4485:
	movq	%r13, %rdi
	addq	$384, %r12
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpq	%r12, %r14
	jne	.L4491
	cmpl	$4, %ebx
	jbe	.L4724
	.p2align 4,,10
	.p2align 3
.L4469:
	movq	-3392(%rbp), %rax
	testq	%rax, %rax
	je	.L4474
	movq	%rax, %rdx
	movq	224(%rax), %rax
	xorl	%ebx, %ebx
	movq	%rax, -3392(%rbp)
	jmp	.L4475
	.p2align 4,,10
	.p2align 3
.L4472:
	movq	-3336(%rbp), %rax
	testq	%rax, %rax
	je	.L4470
	movq	%rax, %rdx
	movq	224(%rax), %rax
	movl	$3, %ebx
	movq	%rax, -3336(%rbp)
	jmp	.L4475
	.p2align 4,,10
	.p2align 3
.L4473:
	movq	-3376(%rbp), %rax
	testq	%rax, %rax
	je	.L4472
	movq	%rax, %rdx
	movq	224(%rax), %rax
	movl	$2, %ebx
	movq	%rax, -3376(%rbp)
	jmp	.L4475
	.p2align 4,,10
	.p2align 3
.L4474:
	movq	-3384(%rbp), %rax
	testq	%rax, %rax
	je	.L4473
	movq	%rax, %rdx
	movq	224(%rax), %rax
	movl	$1, %ebx
	movq	%rax, -3384(%rbp)
	jmp	.L4475
	.p2align 4,,10
	.p2align 3
.L4478:
	movq	(%rsi), %r15
	testq	%r15, %r15
	je	.L4468
	movq	(%rdx), %rdx
	addq	$262143, %rdx
	shrq	$18, %rdx
	je	.L4468
	leaq	(%rdx,%rdx,2), %r14
	movl	%ebx, -3408(%rbp)
	movq	%r15, %rbx
	salq	$7, %r14
	addq	%r15, %r14
	.p2align 4,,10
	.p2align 3
.L4500:
	movq	%rbx, %r15
	leaq	256(%rbx), %r13
	leaq	264(%rbx), %r12
	jmp	.L4499
	.p2align 4,,10
	.p2align 3
.L4493:
	addq	$8, %r15
	cmpq	%r15, %r13
	je	.L4725
.L4499:
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L4493
	leaq	128(%rax), %rdx
.L4496:
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L4493
	addq	$4, %rax
	cmpq	%rdx, %rax
	jne	.L4496
	movq	(%r15), %rax
	movq	%rax, -3136(%rbp)
	testq	%rax, %rax
	je	.L4493
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	368(%rbx), %rax
	movq	352(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L4497
	movq	-3136(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 352(%rbx)
.L4498:
	movq	%r12, %rdi
	addq	$8, %r15
	movq	$0, -8(%r15)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpq	%r15, %r13
	jne	.L4499
	.p2align 4,,10
	.p2align 3
.L4725:
	addq	$384, %rbx
	cmpq	%rbx, %r14
	jne	.L4500
	movl	-3408(%rbp), %ebx
	jmp	.L4468
	.p2align 4,,10
	.p2align 3
.L4497:
	movq	-3320(%rbp), %rsi
	leaq	304(%rbx), %rdi
	call	_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_
	jmp	.L4498
	.p2align 4,,10
	.p2align 3
.L4476:
	movq	-3416(%rbp), %r15
	movq	-3344(%rbp), %rdi
	movq	8(%r15), %rax
	movq	1944(%rax), %rdx
	addq	1920(%rax), %rdx
	addq	%rdx, 184(%rax)
	movq	%rdx, 192(%rax)
	call	_ZN2v88internal8WorklistINS0_18EphemeronHashTableELi128EED1Ev
	movq	-3440(%rbp), %rdi
	call	_ZN2v88internal8WorklistINS0_9Scavenger18PromotionListEntryELi4EED1Ev
	movq	-3360(%rbp), %rdi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EED1Ev
	movq	-3352(%rbp), %rdi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectEiELi256EED1Ev
	movq	-3424(%rbp), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	-3368(%rbp), %rdi
	call	_ZN2v84base17ConditionVariableD1Ev@PLT
	movq	-3400(%rbp), %rdi
	call	_ZN2v88internal15ItemParallelJobD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4726
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4719:
	.cfi_restore_state
	movq	248(%rdi), %rax
	movq	400(%rax), %rbx
	testq	%rbx, %rbx
	je	.L4457
	.p2align 4,,10
	.p2align 3
.L4459:
	movq	2072(%rdi), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17ConcurrentMarking20ClearMemoryChunkDataEPNS0_11MemoryChunkE@PLT
	movq	224(%rbx), %rbx
	movq	8(%r15), %rdi
	testq	%rbx, %rbx
	jne	.L4459
	jmp	.L4457
	.p2align 4,,10
	.p2align 3
.L4351:
	movq	8(%r15), %rax
	movq	248(%rax), %rax
	movq	312(%rax), %rax
	testl	%eax, %eax
	leal	1048575(%rax), %ebx
	cmovns	%eax, %ebx
	movzbl	_ZGVZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores(%rip), %eax
	sarl	$20, %ebx
	addl	$1, %ebx
	testb	%al, %al
	je	.L4727
.L4354:
	cmpl	$8, _ZZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores(%rip)
	movl	$8, %eax
	movq	8(%r15), %rdi
	cmovle	_ZZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores(%rip), %eax
	cmpl	%ebx, %eax
	cmovle	%eax, %ebx
	movl	$1, %eax
	testl	%ebx, %ebx
	cmovg	%ebx, %eax
	movl	%eax, %esi
	movl	%eax, -3336(%rbp)
	sall	$18, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal4Heap22CanExpandOldGenerationEm@PLT
	testb	%al, %al
	je	.L4356
	jmp	.L4352
	.p2align 4,,10
	.p2align 3
.L4718:
	movl	$95, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	movaps	%xmm0, -3072(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4728
.L4453:
	movq	-3064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4454
	movq	(%rdi), %rax
	call	*8(%rax)
.L4454:
	movq	-3072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4455
	movq	(%rdi), %rax
	call	*8(%rax)
.L4455:
	movl	$95, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%r13, -3240(%rbp)
	movq	%rax, -3232(%rbp)
	leaq	-3240(%rbp), %rax
	movq	%rbx, -3224(%rbp)
	movq	%rax, -3248(%rbp)
	jmp	.L4452
	.p2align 4,,10
	.p2align 3
.L4721:
	movl	$90, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	movaps	%xmm0, -3072(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4729
.L4464:
	movq	-3064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4465
	movq	(%rdi), %rax
	call	*8(%rax)
.L4465:
	movq	-3072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4466
	movq	(%rdi), %rax
	call	*8(%rax)
.L4466:
	movl	$90, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%r13, -3240(%rbp)
	movq	%rax, -3232(%rbp)
	leaq	-3240(%rbp), %rax
	movq	%rbx, -3224(%rbp)
	movq	%rax, -3248(%rbp)
	jmp	.L4463
	.p2align 4,,10
	.p2align 3
.L4720:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4730
.L4462:
	movq	%r13, _ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic327(%rip)
	jmp	.L4461
	.p2align 4,,10
	.p2align 3
.L4709:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4731
.L4395:
	movq	%r13, _ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic261(%rip)
	jmp	.L4394
	.p2align 4,,10
	.p2align 3
.L4708:
	movl	$91, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	movaps	%xmm0, -3072(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4732
.L4390:
	movq	-3064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4391
	movq	(%rdi), %rax
	call	*8(%rax)
.L4391:
	movq	-3072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4392
	movq	(%rdi), %rax
	call	*8(%rax)
.L4392:
	movl	$91, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%r13, -3272(%rbp)
	movq	%rax, -3264(%rbp)
	leaq	-3272(%rbp), %rax
	movq	%rbx, -3256(%rbp)
	movq	%rax, -3280(%rbp)
	jmp	.L4389
	.p2align 4,,10
	.p2align 3
.L4707:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4733
.L4388:
	movq	%r13, _ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic253(%rip)
	jmp	.L4387
	.p2align 4,,10
	.p2align 3
.L4447:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4734
.L4451:
	movq	%r13, _ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic305(%rip)
	jmp	.L4448
	.p2align 4,,10
	.p2align 3
.L4716:
	movl	$97, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	movaps	%xmm0, -3072(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4735
.L4418:
	movq	-3064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4419
	movq	(%rdi), %rax
	call	*8(%rax)
.L4419:
	movq	-3072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4420
	movq	(%rdi), %rax
	call	*8(%rax)
.L4420:
	movl	$97, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%r13, -3272(%rbp)
	movq	%rax, -3264(%rbp)
	leaq	-3272(%rbp), %rax
	movq	%rbx, -3256(%rbp)
	movq	%rax, -3280(%rbp)
	jmp	.L4417
	.p2align 4,,10
	.p2align 3
.L4715:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4736
.L4416:
	movq	%r13, _ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic290(%rip)
	jmp	.L4415
	.p2align 4,,10
	.p2align 3
.L4714:
	movl	$92, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -3328(%rbp)
	movaps	%xmm0, -3072(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4737
.L4411:
	movq	-3064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4412
	movq	(%rdi), %rax
	call	*8(%rax)
.L4412:
	movq	-3072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4413
	movq	(%rdi), %rax
	call	*8(%rax)
.L4413:
	movl	$92, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%r13, -3272(%rbp)
	movq	%rax, -3264(%rbp)
	leaq	-3272(%rbp), %rax
	movq	%rbx, -3256(%rbp)
	movq	%rax, -3280(%rbp)
	jmp	.L4410
	.p2align 4,,10
	.p2align 3
.L4713:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4738
.L4409:
	movq	%r13, _ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic273(%rip)
	jmp	.L4408
	.p2align 4,,10
	.p2align 3
.L4712:
	movl	$93, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -3328(%rbp)
	movaps	%xmm0, -3072(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4739
.L4404:
	movq	-3064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4405
	movq	(%rdi), %rax
	call	*8(%rax)
.L4405:
	movq	-3072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4406
	movq	(%rdi), %rax
	call	*8(%rax)
.L4406:
	movl	$93, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%r13, -3272(%rbp)
	movq	%rax, -3264(%rbp)
	leaq	-3272(%rbp), %rax
	movq	%rbx, -3256(%rbp)
	movq	%rax, -3280(%rbp)
	jmp	.L4403
	.p2align 4,,10
	.p2align 3
.L4711:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4740
.L4402:
	movq	%r13, _ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic266(%rip)
	jmp	.L4401
	.p2align 4,,10
	.p2align 3
.L4710:
	movl	$94, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	movaps	%xmm0, -3072(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4741
.L4397:
	movq	-3064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4398
	movq	(%rdi), %rax
	call	*8(%rax)
.L4398:
	movq	-3072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4399
	movq	(%rdi), %rax
	call	*8(%rax)
.L4399:
	movl	$94, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%r13, -3272(%rbp)
	movq	%rax, -3264(%rbp)
	leaq	-3272(%rbp), %rax
	movq	%rbx, -3256(%rbp)
	movq	%rax, -3280(%rbp)
	jmp	.L4396
	.p2align 4,,10
	.p2align 3
.L4373:
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L4371
	.p2align 4,,10
	.p2align 3
.L4727:
	leaq	_ZGVZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L4354
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*40(%rax)
	leaq	_ZGVZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores(%rip), %rdi
	addl	$1, %eax
	movl	%eax, _ZZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L4354
.L4741:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	pushq	$0
	movq	%r13, %rdx
	movl	$88, %esi
	pushq	-3392(%rbp)
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L4397
.L4740:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L4402
.L4739:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	pushq	$0
	movq	-3328(%rbp), %rcx
	movl	$88, %esi
	pushq	-3392(%rbp)
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L4404
.L4738:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L4409
.L4737:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	pushq	$0
	movq	-3328(%rbp), %rcx
	movl	$88, %esi
	pushq	-3392(%rbp)
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L4411
.L4736:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L4416
.L4735:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	pushq	$0
	movq	%r13, %rdx
	movl	$88, %esi
	pushq	-3392(%rbp)
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L4418
.L4734:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L4451
.L4733:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L4388
.L4732:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	pushq	$0
	movq	%r13, %rdx
	movl	$88, %esi
	pushq	-3392(%rbp)
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L4390
.L4731:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L4395
.L4730:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L4462
.L4729:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	pushq	$0
	movq	%r13, %rdx
	movl	$88, %esi
	pushq	-3392(%rbp)
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L4464
.L4728:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	pushq	$0
	movq	%r13, %rdx
	movl	$88, %esi
	pushq	-3392(%rbp)
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L4453
.L4726:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22386:
	.size	_ZN2v88internal18ScavengerCollector14CollectGarbageEv, .-_ZN2v88internal18ScavengerCollector14CollectGarbageEv
	.section	.rodata._ZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"kProcessing == state_.exchange(kFinished)"
	.align 8
.LC15:
	.string	"scavenge[%p]: time=%.2f copied=%zu promoted=%zu\n"
	.section	.text._ZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerE,"axG",@progbits,_ZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerE
	.type	_ZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerE, @function
_ZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerE:
.LFB22346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	movq	2008(%rax), %rax
	testl	%esi, %esi
	jne	.L4743
	leaq	-208(%rbp), %r13
	movl	$93, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE@PLT
	movq	_ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic47(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4818
.L4745:
	movq	$0, -272(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L4819
.L4747:
	movq	96(%r12), %rbx
	movl	$1, %r15d
	leaq	48(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	addl	$1, 96(%rbx)
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*120(%rax)
	movsd	%xmm0, -296(%rbp)
	jmp	.L4755
	.p2align 4,,10
	.p2align 3
.L4820:
	leaq	1(%rcx), %rax
	movq	%rax, 48(%r12)
	movq	(%rdx), %rax
	movq	(%rax,%rcx,8), %rbx
	movq	%r14, %rax
	lock cmpxchgq	%r15, 8(%rbx)
	jne	.L4755
	movq	88(%r12), %rdi
	testq	%rbx, %rbx
	je	.L4757
.L4821:
	movq	16(%rbx), %rsi
	call	_ZN2v88internal9Scavenger12ScavengePageEPNS0_11MemoryChunkE
	movl	$2, %eax
	xchgq	8(%rbx), %rax
	cmpq	$1, %rax
	jne	.L4780
.L4755:
	movq	56(%r12), %rcx
	movq	40(%r12), %rdx
	leaq	1(%rcx), %rax
	movq	%rax, 56(%r12)
	movq	8(%rdx), %rax
	subq	(%rdx), %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	je	.L4751
	movq	48(%r12), %rcx
	cmpq	%rcx, %rax
	jne	.L4820
	movq	$1, 48(%r12)
	movq	(%rdx), %rax
	movq	(%rax), %rbx
	movq	%r14, %rax
	lock cmpxchgq	%r15, 8(%rbx)
	jne	.L4755
	movq	88(%r12), %rdi
	testq	%rbx, %rbx
	jne	.L4821
	.p2align 4,,10
	.p2align 3
.L4757:
	movq	96(%r12), %rsi
	call	_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE
	movq	96(%r12), %rbx
	leaq	48(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpb	$0, 104(%rbx)
	jne	.L4816
	movl	100(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 100(%rbx)
	cmpl	96(%rbx), %eax
	je	.L4822
	leaq	88(%rbx), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v84base17ConditionVariable7WaitForEPNS0_5MutexERKNS0_9TimeDeltaE@PLT
	testb	%al, %al
	je	.L4763
	movzbl	104(%rbx), %r15d
.L4762:
	subl	$1, 100(%rbx)
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testb	%r15b, %r15b
	jne	.L4760
.L4751:
	movq	88(%r12), %rdi
	jmp	.L4757
	.p2align 4,,10
	.p2align 3
.L4763:
	subl	$1, 100(%rbx)
	movb	$1, 104(%rbx)
.L4816:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L4760:
	movq	88(%r12), %rdi
	call	_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE.constprop.0
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*120(%rax)
	cmpb	$0, _ZN2v88internal28FLAG_trace_parallel_scavengeE(%rip)
	jne	.L4823
.L4764:
	leaq	-272(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8GCTracer5ScopeD1Ev@PLT
	jmp	.L4742
	.p2align 4,,10
	.p2align 3
.L4822:
	movb	$1, 104(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v84base17ConditionVariable9NotifyAllEv@PLT
	movzbl	104(%rbx), %r15d
	jmp	.L4762
	.p2align 4,,10
	.p2align 3
.L4743:
	movq	%rax, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv@PLT
	movq	%rax, %rsi
	leaq	-280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	80(%r12), %rax
	movl	$10, %edx
	movq	%r14, %rdi
	movq	-280(%rbp), %rcx
	movq	2008(%rax), %rsi
	call	_ZN2v88internal8GCTracer15BackgroundScopeC1EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE@PLT
	movq	_ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic50(%rip), %rdx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L4824
.L4767:
	movq	$0, -240(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L4825
.L4769:
	movq	96(%r12), %rbx
	xorl	%r13d, %r13d
	leaq	48(%rbx), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	addl	$1, 96(%rbx)
	movq	%r15, %rdi
	movl	$1, %r15d
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*120(%rax)
	movsd	%xmm0, -304(%rbp)
	jmp	.L4777
	.p2align 4,,10
	.p2align 3
.L4826:
	leaq	1(%rsi), %rax
	movq	%rax, 48(%r12)
	movq	(%rdx), %rax
	movq	(%rax,%rsi,8), %rbx
	movq	%r13, %rax
	lock cmpxchgq	%r15, 8(%rbx)
	jne	.L4777
	movq	88(%r12), %rdi
	testq	%rbx, %rbx
	je	.L4779
.L4827:
	movq	16(%rbx), %rsi
	call	_ZN2v88internal9Scavenger12ScavengePageEPNS0_11MemoryChunkE
	movl	$2, %eax
	xchgq	8(%rbx), %rax
	cmpq	$1, %rax
	jne	.L4780
.L4777:
	movq	56(%r12), %rsi
	movq	40(%r12), %rdx
	leaq	1(%rsi), %rax
	movq	%rax, 56(%r12)
	movq	8(%rdx), %rax
	subq	(%rdx), %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	je	.L4773
	movq	48(%r12), %rsi
	cmpq	%rsi, %rax
	jne	.L4826
	movq	$1, 48(%r12)
	movq	(%rdx), %rax
	movq	(%rax), %rbx
	movq	%r13, %rax
	lock cmpxchgq	%r15, 8(%rbx)
	jne	.L4777
	movq	88(%r12), %rdi
	testq	%rbx, %rbx
	jne	.L4827
	.p2align 4,,10
	.p2align 3
.L4779:
	movq	96(%r12), %rsi
	call	_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE
	movq	96(%r12), %rbx
	leaq	48(%rbx), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpb	$0, 104(%rbx)
	jne	.L4817
	movl	100(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 100(%rbx)
	cmpl	96(%rbx), %eax
	je	.L4828
	leaq	88(%rbx), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v84base17ConditionVariable7WaitForEPNS0_5MutexERKNS0_9TimeDeltaE@PLT
	testb	%al, %al
	je	.L4786
	movzbl	104(%rbx), %r13d
.L4785:
	subl	$1, 100(%rbx)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testb	%r13b, %r13b
	jne	.L4783
.L4773:
	movq	88(%r12), %rdi
	jmp	.L4779
	.p2align 4,,10
	.p2align 3
.L4786:
	subl	$1, 100(%rbx)
	movb	$1, 104(%rbx)
.L4817:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L4783:
	movq	88(%r12), %rdi
	call	_ZN2v88internal9Scavenger7ProcessEPNS0_14OneshotBarrierE.constprop.0
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*120(%rax)
	cmpb	$0, _ZN2v88internal28FLAG_trace_parallel_scavengeE(%rip)
	jne	.L4829
.L4787:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8GCTracer15BackgroundScopeD1Ev@PLT
	movq	-296(%rbp), %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
.L4742:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4830
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4828:
	.cfi_restore_state
	movb	$1, 104(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v84base17ConditionVariable9NotifyAllEv@PLT
	movzbl	104(%rbx), %r13d
	jmp	.L4785
	.p2align 4,,10
	.p2align 3
.L4780:
	leaq	.LC13(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4824:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4831
.L4768:
	movq	%r15, _ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic50(%rip)
	jmp	.L4767
	.p2align 4,,10
	.p2align 3
.L4818:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4832
.L4746:
	movq	%rbx, _ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic47(%rip)
	jmp	.L4745
	.p2align 4,,10
	.p2align 3
.L4819:
	movl	$93, %edi
	xorl	%r15d, %r15d
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r14
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4833
.L4748:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4749
	movq	(%rdi), %rax
	call	*8(%rax)
.L4749:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4750
	movq	(%rdi), %rax
	call	*8(%rax)
.L4750:
	movl	$93, %edi
	call	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE@PLT
	movq	%rbx, -264(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-264(%rbp), %rax
	movq	%r15, -248(%rbp)
	movq	%rax, -272(%rbp)
	jmp	.L4747
	.p2align 4,,10
	.p2align 3
.L4825:
	movl	$10, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r13
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4834
.L4770:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4771
	movq	(%rdi), %rax
	call	*8(%rax)
.L4771:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4772
	movq	(%rdi), %rax
	call	*8(%rax)
.L4772:
	movl	$10, %edi
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	movq	%r15, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rbx, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L4769
	.p2align 4,,10
	.p2align 3
.L4829:
	movsd	.LC14(%rip), %xmm1
	movq	88(%r12), %rax
	leaq	.LC15(%rip), %rsi
	movq	80(%r12), %rdx
	mulsd	%xmm1, %xmm0
	movq	120(%rax), %rcx
	movq	128(%rax), %r8
	movl	$1, %eax
	mulsd	-304(%rbp), %xmm1
	leaq	-37592(%rdx), %rdi
	movq	%r12, %rdx
	subsd	%xmm1, %xmm0
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	jmp	.L4787
	.p2align 4,,10
	.p2align 3
.L4823:
	movsd	.LC14(%rip), %xmm1
	movq	88(%r12), %rax
	leaq	.LC15(%rip), %rsi
	movq	80(%r12), %rdx
	mulsd	%xmm1, %xmm0
	movq	120(%rax), %rcx
	movq	128(%rax), %r8
	movl	$1, %eax
	mulsd	-296(%rbp), %xmm1
	leaq	-37592(%rdx), %rdi
	movq	%r12, %rdx
	subsd	%xmm1, %xmm0
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	jmp	.L4764
	.p2align 4,,10
	.p2align 3
.L4831:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L4768
	.p2align 4,,10
	.p2align 3
.L4834:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r15, %rdx
	movl	$88, %esi
	pushq	%rcx
	movq	%r13, %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L4770
	.p2align 4,,10
	.p2align 3
.L4833:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r14, %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L4748
	.p2align 4,,10
	.p2align 3
.L4832:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L4746
.L4830:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22346:
	.size	_ZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerE, .-_ZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal18ScavengerCollectorC2EPNS0_4HeapE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal18ScavengerCollectorC2EPNS0_4HeapE, @function
_GLOBAL__sub_I__ZN2v88internal18ScavengerCollectorC2EPNS0_4HeapE:
.LFB29783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29783:
	.size	_GLOBAL__sub_I__ZN2v88internal18ScavengerCollectorC2EPNS0_4HeapE, .-_GLOBAL__sub_I__ZN2v88internal18ScavengerCollectorC2EPNS0_4HeapE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal18ScavengerCollectorC2EPNS0_4HeapE
	.weak	_ZTVN2v88internal14CancelableTaskE
	.section	.data.rel.ro._ZTVN2v88internal14CancelableTaskE,"awG",@progbits,_ZTVN2v88internal14CancelableTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal14CancelableTaskE, @object
	.size	_ZTVN2v88internal14CancelableTaskE, 88
_ZTVN2v88internal14CancelableTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal15ScavengeVisitorE
	.section	.data.rel.ro._ZTVN2v88internal15ScavengeVisitorE,"awG",@progbits,_ZTVN2v88internal15ScavengeVisitorE,comdat
	.align 8
	.type	_ZTVN2v88internal15ScavengeVisitorE, @object
	.size	_ZTVN2v88internal15ScavengeVisitorE, 152
_ZTVN2v88internal15ScavengeVisitorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15ScavengeVisitorD1Ev
	.quad	_ZN2v88internal15ScavengeVisitorD0Ev
	.quad	_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal15ScavengeVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.quad	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal15ScavengeVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal15ScavengeVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.quad	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE
	.weak	_ZTVN2v88internal18PageScavengingItemE
	.section	.data.rel.ro.local._ZTVN2v88internal18PageScavengingItemE,"awG",@progbits,_ZTVN2v88internal18PageScavengingItemE,comdat
	.align 8
	.type	_ZTVN2v88internal18PageScavengingItemE, @object
	.size	_ZTVN2v88internal18PageScavengingItemE, 32
_ZTVN2v88internal18PageScavengingItemE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal18PageScavengingItemD1Ev
	.quad	_ZN2v88internal18PageScavengingItemD0Ev
	.weak	_ZTVN2v88internal14ScavengingTaskE
	.section	.data.rel.ro._ZTVN2v88internal14ScavengingTaskE,"awG",@progbits,_ZTVN2v88internal14ScavengingTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal14ScavengingTaskE, @object
	.size	_ZTVN2v88internal14ScavengingTaskE, 96
_ZTVN2v88internal14ScavengingTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14ScavengingTaskD1Ev
	.quad	_ZN2v88internal14ScavengingTaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal15ItemParallelJob4Task11RunInternalEv
	.quad	_ZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerE
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal14ScavengingTaskD1Ev
	.quad	_ZThn32_N2v88internal14ScavengingTaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal40IterateAndScavengePromotedObjectsVisitorE
	.section	.data.rel.ro._ZTVN2v88internal40IterateAndScavengePromotedObjectsVisitorE,"awG",@progbits,_ZTVN2v88internal40IterateAndScavengePromotedObjectsVisitorE,comdat
	.align 8
	.type	_ZTVN2v88internal40IterateAndScavengePromotedObjectsVisitorE, @object
	.size	_ZTVN2v88internal40IterateAndScavengePromotedObjectsVisitorE, 152
_ZTVN2v88internal40IterateAndScavengePromotedObjectsVisitorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD1Ev
	.quad	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitorD0Ev
	.quad	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.quad	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal40IterateAndScavengePromotedObjectsVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.quad	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE
	.weak	_ZTVN2v88internal26ScavengeWeakObjectRetainerE
	.section	.data.rel.ro.local._ZTVN2v88internal26ScavengeWeakObjectRetainerE,"awG",@progbits,_ZTVN2v88internal26ScavengeWeakObjectRetainerE,comdat
	.align 8
	.type	_ZTVN2v88internal26ScavengeWeakObjectRetainerE, @object
	.size	_ZTVN2v88internal26ScavengeWeakObjectRetainerE, 40
_ZTVN2v88internal26ScavengeWeakObjectRetainerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal26ScavengeWeakObjectRetainerD1Ev
	.quad	_ZN2v88internal26ScavengeWeakObjectRetainerD0Ev
	.quad	_ZN2v88internal26ScavengeWeakObjectRetainer8RetainAsENS0_6ObjectE
	.weak	_ZTVN2v88internal19RootScavengeVisitorE
	.section	.data.rel.ro.local._ZTVN2v88internal19RootScavengeVisitorE,"awG",@progbits,_ZTVN2v88internal19RootScavengeVisitorE,comdat
	.align 8
	.type	_ZTVN2v88internal19RootScavengeVisitorE, @object
	.size	_ZTVN2v88internal19RootScavengeVisitorE, 56
_ZTVN2v88internal19RootScavengeVisitorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19RootScavengeVisitorD1Ev
	.quad	_ZN2v88internal19RootScavengeVisitorD0Ev
	.quad	_ZN2v88internal19RootScavengeVisitor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal19RootScavengeVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.bss._ZGVZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores, @object
	.size	_ZGVZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores, 8
_ZGVZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores:
	.zero	8
	.section	.bss._ZZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores,"aw",@nobits
	.align 4
	.type	_ZZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores, @object
	.size	_ZZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores, 4
_ZZN2v88internal18ScavengerCollector21NumberOfScavengeTasksEvE9num_cores:
	.zero	4
	.section	.bss._ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic327,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic327, @object
	.size	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic327, 8
_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic327:
	.zero	8
	.section	.bss._ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic305,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic305, @object
	.size	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic305, 8
_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic305:
	.zero	8
	.section	.bss._ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic290,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic290, @object
	.size	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic290, 8
_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic290:
	.zero	8
	.section	.bss._ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic273,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic273, @object
	.size	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic273, 8
_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic273:
	.zero	8
	.section	.bss._ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic266,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic266, @object
	.size	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic266, 8
_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic266:
	.zero	8
	.section	.bss._ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic261,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic261, @object
	.size	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic261, 8
_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic261:
	.zero	8
	.section	.bss._ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic253,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic253, @object
	.size	_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic253, 8
_ZZN2v88internal18ScavengerCollector14CollectGarbageEvE28trace_event_unique_atomic253:
	.zero	8
	.weak	_ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic50
	.section	.bss._ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic50,"awG",@nobits,_ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic50,comdat
	.align 8
	.type	_ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic50, @gnu_unique_object
	.size	_ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic50, 8
_ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic50:
	.zero	8
	.weak	_ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic47
	.section	.bss._ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic47,"awG",@nobits,_ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic47,comdat
	.align 8
	.type	_ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic47, @gnu_unique_object
	.size	_ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic47, 8
_ZZN2v88internal14ScavengingTask13RunInParallelENS0_15ItemParallelJob4Task6RunnerEE27trace_event_unique_atomic47:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC14:
	.long	0
	.long	1083129856
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
