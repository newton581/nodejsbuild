	.file	"runtime-generator.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Runtime_Runtime_AsyncGeneratorHasCatchHandlerForPC"
	.align 8
.LC2:
	.string	"args[0].IsJSAsyncGeneratorObject()"
	.section	.rodata._ZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE.isra.0:
.LFB25139:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$144, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L53
.L16:
	movq	_ZZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateEE28trace_event_unique_atomic128(%rip), %rbx
	testq	%rbx, %rbx
	je	.L54
.L18:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L55
.L20:
	movq	(%r12), %rbx
	testb	$1, %bl
	jne	.L24
.L25:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L56
.L19:
	movq	%rbx, _ZZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateEE28trace_event_unique_atomic128(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L24:
	movq	-1(%rbx), %rax
	cmpw	$1064, 11(%rax)
	jne	.L25
	movl	67(%rbx), %eax
	testl	%eax, %eax
	jle	.L31
	movq	23(%rbx), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L57
.L28:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L58
.L30:
	movq	7(%rax), %rax
	movq	7(%rax), %rsi
.L29:
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12HandlerTableC1ENS0_13BytecodeArrayE@PLT
	movslq	51(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-164(%rbp), %rcx
	movl	$4, -164(%rbp)
	call	_ZN2v88internal12HandlerTable11LookupRangeEiPiPNS1_15CatchPredictionE@PLT
	cmpl	$1, -164(%rbp)
	jne	.L31
	movq	112(%r13), %r12
.L27:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L59
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	120(%r13), %r12
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L55:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L61
.L21:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L57:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L28
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L28
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L28
	movq	31(%rdx), %rsi
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L58:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L30
	movq	7(%rax), %rsi
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L53:
	movq	40960(%rsi), %rax
	movl	$309, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L61:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L19
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25139:
	.size	_ZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"V8.Runtime_Runtime_GeneratorGetFunction"
	.section	.rodata._ZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"args[0].IsJSGeneratorObject()"
	.section	.text._ZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE:
.LFB20086:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L96
.L63:
	movq	_ZZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic82(%rip), %rbx
	testq	%rbx, %rbx
	je	.L97
.L65:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L98
.L67:
	movq	41088(%r12), %rcx
	movq	41096(%r12), %rdx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L71
.L73:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L97:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L99
.L66:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic82(%rip)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L71:
	movq	-1(%rax), %rsi
	cmpw	$1068, 11(%rsi)
	jne	.L100
.L72:
	movq	23(%rax), %r13
	movq	%rcx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rdx
	je	.L76
	movq	%rdx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L76:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L101
.L62:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L103
.L68:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	movq	(%rdi), %rax
	call	*8(%rax)
.L69:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	movq	(%rdi), %rax
	call	*8(%rax)
.L70:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-1(%rax), %rsi
	cmpw	$1063, 11(%rsi)
	je	.L72
	movq	-1(%rax), %rsi
	cmpw	$1064, 11(%rsi)
	jne	.L73
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L96:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$315, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L99:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L103:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L68
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20086:
	.size	_ZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Runtime_Runtime_CreateJSGeneratorObject"
	.section	.rodata._ZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC7:
	.string	"args[0].IsJSFunction()"
	.section	.rodata._ZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC8:
	.string	"IsAsyncFunction(function->shared().kind()) implies IsAsyncGeneratorFunction(function->shared().kind())"
	.align 8
.LC9:
	.string	"IsResumableFunction(function->shared().kind())"
	.section	.text._ZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE.isra.0:
.LFB25161:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L183
.L105:
	movq	_ZZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateEE27trace_event_unique_atomic46(%rip), %r13
	testq	%r13, %r13
	je	.L184
.L107:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L185
.L109:
	movq	41088(%r12), %rax
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	%rax, -168(%rbp)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L186
.L113:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L184:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L187
.L108:
	movq	%r13, _ZZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateEE27trace_event_unique_atomic46(%rip)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L186:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L113
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$31, %edx
	subl	$9, %edx
	cmpb	$4, %dl
	jbe	.L188
.L115:
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$31, %edx
	leal	-9(%rdx), %ecx
	cmpb	$6, %cl
	jbe	.L116
	cmpb	$1, %dl
	je	.L116
	leaq	.LC9(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L116:
	movq	23(%rax), %rdx
	movzwl	41(%rdx), %ecx
	movq	23(%rax), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L189
.L117:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L190
.L119:
	movq	7(%rax), %rax
	movq	7(%rax), %rax
.L118:
	movl	39(%rax), %eax
	movq	%r12, %rdi
	testl	%eax, %eax
	leal	7(%rax), %esi
	cmovns	%eax, %esi
	xorl	%edx, %edx
	sarl	$3, %esi
	addl	%ecx, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal7Factory20NewJSGeneratorObjectENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	(%rbx), %rdx
	movq	(%rax), %rdi
	movq	%rax, %r15
	movq	%rdx, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %dl
	je	.L139
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rcx
	movq	%r8, -176(%rbp)
	testl	$262144, %ecx
	je	.L121
	movq	%rdx, -200(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-176(%rbp), %r8
	movq	-200(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rdi
	movq	8(%r8), %rcx
.L121:
	andl	$24, %ecx
	je	.L139
	movq	%rdi, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	je	.L191
	.p2align 4,,10
	.p2align 3
.L139:
	movq	(%r15), %rdi
	movq	12464(%r12), %rdx
	movq	%rdx, 31(%rdi)
	leaq	31(%rdi), %rsi
	testb	$1, %dl
	je	.L138
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rcx
	movq	%r8, -176(%rbp)
	testl	$262144, %ecx
	je	.L124
	movq	%rdx, -200(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-176(%rbp), %r8
	movq	-200(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rdi
	movq	8(%r8), %rcx
.L124:
	andl	$24, %ecx
	je	.L138
	movq	%rdi, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	je	.L192
	.p2align 4,,10
	.p2align 3
.L138:
	movq	(%r15), %rdi
	movq	-8(%rbx), %rbx
	movq	%rbx, 39(%rdi)
	leaq	39(%rdi), %rsi
	testb	$1, %bl
	je	.L137
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -176(%rbp)
	testl	$262144, %edx
	je	.L127
	movq	%rbx, %rdx
	movq	%rsi, -192(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-176(%rbp), %rcx
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rdi
	movq	8(%rcx), %rdx
.L127:
	andl	$24, %edx
	je	.L137
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L193
	.p2align 4,,10
	.p2align 3
.L137:
	movq	(%r15), %rbx
	movq	0(%r13), %r13
	movq	%r13, 71(%rbx)
	leaq	71(%rbx), %rsi
	testb	$1, %r13b
	je	.L136
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -176(%rbp)
	testl	$262144, %edx
	je	.L130
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-176(%rbp), %rcx
	movq	-184(%rbp), %rsi
	movq	8(%rcx), %rdx
.L130:
	andl	$24, %edx
	je	.L136
	movq	%rbx, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L194
	.p2align 4,,10
	.p2align 3
.L136:
	movabsq	$-8589934592, %rcx
	movq	(%r15), %rdx
	movq	$0, 55(%rdx)
	movq	(%r15), %rdx
	movq	%rcx, 63(%rdx)
	movq	(%r15), %r13
	movq	-1(%r13), %rdx
	cmpw	$1064, 11(%rdx)
	je	.L195
.L132:
	subl	$1, 41104(%r12)
	movq	-168(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L135
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L135:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L196
.L104:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L198
.L110:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L111
	movq	(%rdi), %rax
	call	*8(%rax)
.L111:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L112
	movq	(%rdi), %rax
	call	*8(%rax)
.L112:
	leaq	.LC6(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L189:
	movq	-1(%rdx), %rsi
	cmpw	$86, 11(%rsi)
	jne	.L117
	movq	39(%rdx), %rsi
	testb	$1, %sil
	je	.L117
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L117
	movq	31(%rdx), %rax
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L192:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L191:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%rbx, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L190:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L119
	movq	7(%rax), %rax
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L195:
	movq	$0, 87(%r13)
	movq	(%r15), %r13
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L183:
	movq	40960(%rsi), %rax
	movl	$313, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L188:
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$31, %edx
	subl	$12, %edx
	cmpb	$1, %dl
	jbe	.L115
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L196:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L198:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L110
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25161:
	.size	_ZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE,"axG",@progbits,_ZN2v88internal21RuntimeCallTimerScopeC5EPNS0_7IsolateENS0_20RuntimeCallCounterIdE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.type	_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE, @function
_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE:
.LFB9148:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L201
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	movq	40960(%rsi), %r8
	leaq	8(%rdi), %rsi
	addq	$23240, %r8
	movq	%r8, (%rdi)
	movq	%r8, %rdi
	jmp	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	.cfi_endproc
.LFE9148:
	.size	_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE, .-_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.weak	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.set	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE,_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.section	.text._ZN2v88internal31Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE:
.LFB20081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L255
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L256
.L204:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L256:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L204
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$31, %edx
	subl	$9, %edx
	cmpb	$4, %dl
	jbe	.L257
.L206:
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$31, %edx
	leal	-9(%rdx), %ecx
	cmpb	$6, %cl
	jbe	.L207
	cmpb	$1, %dl
	je	.L207
	leaq	.LC9(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L207:
	movq	23(%rax), %rdx
	movzwl	41(%rdx), %ecx
	movq	23(%rax), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L258
.L208:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L259
.L210:
	movq	7(%rax), %rax
	movq	7(%rax), %rax
.L209:
	movl	39(%rax), %eax
	movq	%r12, %rdi
	testl	%eax, %eax
	leal	7(%rax), %esi
	cmovns	%eax, %esi
	xorl	%edx, %edx
	sarl	$3, %esi
	addl	%ecx, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory20NewJSGeneratorObjectENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	0(%r13), %rdx
	movq	(%rax), %rdi
	movq	%rax, %rbx
	movq	%rdx, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %dl
	je	.L229
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	testl	$262144, %eax
	je	.L212
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	8(%rcx), %rax
.L212:
	testb	$24, %al
	je	.L229
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L260
	.p2align 4,,10
	.p2align 3
.L229:
	movq	(%rbx), %rdi
	movq	12464(%r12), %rdx
	movq	%rdx, 31(%rdi)
	leaq	31(%rdi), %rsi
	testb	$1, %dl
	je	.L228
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	testl	$262144, %eax
	je	.L215
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	8(%rcx), %rax
.L215:
	testb	$24, %al
	je	.L228
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L261
	.p2align 4,,10
	.p2align 3
.L228:
	movq	(%rbx), %rdi
	movq	-8(%r13), %rdx
	movq	%rdx, 39(%rdi)
	leaq	39(%rdi), %rsi
	testb	$1, %dl
	je	.L227
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	testl	$262144, %eax
	je	.L218
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	8(%rcx), %rax
.L218:
	testb	$24, %al
	je	.L227
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L262
	.p2align 4,,10
	.p2align 3
.L227:
	movq	-56(%rbp), %rax
	movq	(%rbx), %rdi
	movq	(%rax), %r13
	leaq	71(%rdi), %rsi
	movq	%r13, 71(%rdi)
	testb	$1, %r13b
	je	.L226
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L221
	movq	%r13, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
.L221:
	testb	$24, %al
	je	.L226
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L263
	.p2align 4,,10
	.p2align 3
.L226:
	movabsq	$-8589934592, %rdx
	movq	(%rbx), %rax
	movq	$0, 55(%rax)
	movq	(%rbx), %rax
	movq	%rdx, 63(%rax)
	movq	(%rbx), %r13
	movq	-1(%r13), %rax
	cmpw	$1064, 11(%rax)
	je	.L264
.L223:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L202
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L202:
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movq	-1(%rdx), %rsi
	cmpw	$86, 11(%rsi)
	jne	.L208
	movq	39(%rdx), %rsi
	testb	$1, %sil
	je	.L208
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L208
	movq	31(%rdx), %rax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L263:
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L260:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L262:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L261:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L259:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L210
	movq	7(%rax), %rax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L264:
	movq	$0, 87(%r13)
	movq	(%rbx), %r13
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L255:
	addq	$56, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$31, %edx
	subl	$12, %edx
	cmpb	$1, %dl
	jbe	.L206
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20081:
	.size	_ZN2v88internal31Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE:
.LFB20087:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movl	%edi, %r8d
	movq	%rdx, %rdi
	testl	%eax, %eax
	jne	.L274
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	41088(%rdx), %rcx
	movq	41096(%rdx), %rdx
	addl	$1, 41104(%rdi)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L267
.L269:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L267:
	movq	-1(%rax), %rsi
	cmpw	$1068, 11(%rsi)
	jne	.L275
.L268:
	movq	23(%rax), %r12
	subl	$1, 41104(%rdi)
	movq	%rcx, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L265
	movq	%rdx, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L265:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movq	-1(%rax), %rsi
	cmpw	$1063, 11(%rsi)
	je	.L268
	movq	-1(%rax), %rsi
	cmpw	$1064, 11(%rsi)
	jne	.L269
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	movl	%r8d, %edi
	jmp	_ZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20087:
	.size	_ZN2v88internal28Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal42Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal42Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE
	.type	_ZN2v88internal42Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE, @function
_ZN2v88internal42Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE:
.LFB20108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L290
	movq	(%rsi), %rbx
	testb	$1, %bl
	jne	.L279
.L280:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L279:
	movq	-1(%rbx), %rax
	cmpw	$1064, 11(%rax)
	jne	.L280
	movl	67(%rbx), %eax
	testl	%eax, %eax
	jle	.L286
	movq	23(%rbx), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L291
.L283:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L292
.L285:
	movq	7(%rax), %rax
	movq	7(%rax), %rsi
.L284:
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal12HandlerTableC1ENS0_13BytecodeArrayE@PLT
	movslq	51(%rbx), %rsi
	xorl	%edx, %edx
	leaq	-68(%rbp), %rcx
	movq	%r13, %rdi
	movl	$4, -68(%rbp)
	call	_ZN2v88internal12HandlerTable11LookupRangeEiPiPNS1_15CatchPredictionE@PLT
	cmpl	$1, -68(%rbp)
	jne	.L286
	movq	112(%r12), %rax
.L276:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L293
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	movq	120(%r12), %rax
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L291:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L283
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L283
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L283
	movq	31(%rdx), %rsi
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L292:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L285
	movq	7(%rax), %rsi
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L290:
	movq	%rdx, %rsi
	call	_ZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE.isra.0
	jmp	.L276
.L293:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20108:
	.size	_ZN2v88internal42Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE, .-_ZN2v88internal42Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateE
	.section	.text._ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev,"axG",@progbits,_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.type	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev, @function
_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev:
.LFB20744:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L294
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L294:
	ret
	.cfi_endproc
.LFE20744:
	.size	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev, .-_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.weak	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	.set	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev,_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.section	.rodata._ZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"V8.Runtime_Runtime_AsyncFunctionAwaitCaught"
	.section	.rodata._ZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC11:
	.string	"unreachable code"
	.section	.text._ZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE.isra.0:
.LFB25121:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$302, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic16(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L305
.L298:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L306
.L299:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L305:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic16(%rip)
	movq	%rax, %r12
	jmp	.L298
.L306:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L299
	.cfi_endproc
.LFE25121:
	.size	_ZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal32Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE:
.LFB20066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L310
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L310:
	movq	%rdx, %rdi
	call	_ZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20066:
	.size	_ZN2v88internal32Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Stats_Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"V8.Runtime_Runtime_AsyncFunctionAwaitUncaught"
	.section	.text._ZN2v88internalL40Stats_Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateE.isra.0:
.LFB25122:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$303, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL40Stats_Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic22(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L320
.L313:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L321
.L314:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L320:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL40Stats_Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic22(%rip)
	movq	%rax, %r12
	jmp	.L313
.L321:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L314
	.cfi_endproc
.LFE25122:
	.size	_ZN2v88internalL40Stats_Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal34Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateE:
.LFB20069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L325
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L325:
	movq	%rdx, %rdi
	call	_ZN2v88internalL40Stats_Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20069:
	.size	_ZN2v88internal34Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Stats_Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8.Runtime_Runtime_AsyncFunctionEnter"
	.section	.text._ZN2v88internalL32Stats_Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateE.isra.0:
.LFB25123:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$304, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL32Stats_Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateEE27trace_event_unique_atomic28(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L335
.L328:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L336
.L329:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L335:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL32Stats_Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateEE27trace_event_unique_atomic28(%rip)
	movq	%rax, %r12
	jmp	.L328
.L336:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L329
	.cfi_endproc
.LFE25123:
	.size	_ZN2v88internalL32Stats_Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal26Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateE:
.LFB20072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L340
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L340:
	movq	%rdx, %rdi
	call	_ZN2v88internalL32Stats_Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20072:
	.size	_ZN2v88internal26Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Stats_Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"V8.Runtime_Runtime_AsyncFunctionReject"
	.section	.text._ZN2v88internalL33Stats_Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateE.isra.0:
.LFB25124:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$305, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL33Stats_Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateEE27trace_event_unique_atomic34(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L350
.L343:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L351
.L344:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L350:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL33Stats_Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateEE27trace_event_unique_atomic34(%rip)
	movq	%rax, %r12
	jmp	.L343
.L351:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L344
	.cfi_endproc
.LFE25124:
	.size	_ZN2v88internalL33Stats_Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal27Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateE:
.LFB20075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L355
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L355:
	movq	%rdx, %rdi
	call	_ZN2v88internalL33Stats_Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20075:
	.size	_ZN2v88internal27Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Stats_Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"V8.Runtime_Runtime_AsyncFunctionResolve"
	.section	.text._ZN2v88internalL34Stats_Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateE.isra.0:
.LFB25125:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$306, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL34Stats_Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateEE27trace_event_unique_atomic40(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L365
.L358:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L366
.L359:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L365:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL34Stats_Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateEE27trace_event_unique_atomic40(%rip)
	movq	%rax, %r12
	jmp	.L358
.L366:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC15(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L359
	.cfi_endproc
.LFE25125:
	.size	_ZN2v88internalL34Stats_Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal28Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateE:
.LFB20078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L370
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L370:
	movq	%rdx, %rdi
	call	_ZN2v88internalL34Stats_Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20078:
	.size	_ZN2v88internal28Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL28Stats_Runtime_GeneratorCloseEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"V8.Runtime_Runtime_GeneratorClose"
	.section	.text._ZN2v88internalL28Stats_Runtime_GeneratorCloseEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_GeneratorCloseEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_GeneratorCloseEiPmPNS0_7IsolateE.isra.0:
.LFB25126:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$314, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL28Stats_Runtime_GeneratorCloseEiPmPNS0_7IsolateEE27trace_event_unique_atomic76(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L380
.L373:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L381
.L374:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L380:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL28Stats_Runtime_GeneratorCloseEiPmPNS0_7IsolateEE27trace_event_unique_atomic76(%rip)
	movq	%rax, %r12
	jmp	.L373
.L381:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC16(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L374
	.cfi_endproc
.LFE25126:
	.size	_ZN2v88internalL28Stats_Runtime_GeneratorCloseEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_GeneratorCloseEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal22Runtime_GeneratorCloseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_GeneratorCloseEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_GeneratorCloseEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_GeneratorCloseEiPmPNS0_7IsolateE:
.LFB20084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L385
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L385:
	movq	%rdx, %rdi
	call	_ZN2v88internalL28Stats_Runtime_GeneratorCloseEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20084:
	.size	_ZN2v88internal22Runtime_GeneratorCloseEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_GeneratorCloseEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Stats_Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Runtime_Runtime_AsyncGeneratorAwaitCaught"
	.section	.text._ZN2v88internalL39Stats_Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL39Stats_Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateE.isra.0:
.LFB25127:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$307, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL39Stats_Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic90(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L395
.L388:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L396
.L389:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L395:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL39Stats_Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic90(%rip)
	movq	%rax, %r12
	jmp	.L388
.L396:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L389
	.cfi_endproc
.LFE25127:
	.size	_ZN2v88internalL39Stats_Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL39Stats_Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal33Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateE:
.LFB20090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L400
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L400:
	movq	%rdx, %rdi
	call	_ZN2v88internalL39Stats_Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20090:
	.size	_ZN2v88internal33Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Stats_Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"V8.Runtime_Runtime_AsyncGeneratorAwaitUncaught"
	.section	.text._ZN2v88internalL41Stats_Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Stats_Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL41Stats_Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateE.isra.0:
.LFB25128:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$308, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL41Stats_Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic96(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L410
.L403:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L411
.L404:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L410:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL41Stats_Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic96(%rip)
	movq	%rax, %r12
	jmp	.L403
.L411:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC18(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L404
	.cfi_endproc
.LFE25128:
	.size	_ZN2v88internalL41Stats_Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL41Stats_Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal35Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateE:
.LFB20093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L415
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L415:
	movq	%rdx, %rdi
	call	_ZN2v88internalL41Stats_Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20093:
	.size	_ZN2v88internal35Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateE, .-_ZN2v88internal35Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Stats_Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"V8.Runtime_Runtime_AsyncGeneratorResolve"
	.section	.text._ZN2v88internalL35Stats_Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateE.isra.0:
.LFB25129:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$311, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL35Stats_Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L425
.L418:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L426
.L419:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L425:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL35Stats_Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	movq	%rax, %r12
	jmp	.L418
.L426:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC19(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L419
	.cfi_endproc
.LFE25129:
	.size	_ZN2v88internalL35Stats_Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal29Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateE:
.LFB20096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L430
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L430:
	movq	%rdx, %rdi
	call	_ZN2v88internalL35Stats_Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20096:
	.size	_ZN2v88internal29Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Stats_Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"V8.Runtime_Runtime_AsyncGeneratorReject"
	.section	.text._ZN2v88internalL34Stats_Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateE.isra.0:
.LFB25130:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$310, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL34Stats_Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateEE28trace_event_unique_atomic108(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L440
.L433:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L441
.L434:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L440:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL34Stats_Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateEE28trace_event_unique_atomic108(%rip)
	movq	%rax, %r12
	jmp	.L433
.L441:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC20(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L434
	.cfi_endproc
.LFE25130:
	.size	_ZN2v88internalL34Stats_Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal28Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateE:
.LFB20099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L445
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L445:
	movq	%rdx, %rdi
	call	_ZN2v88internalL34Stats_Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20099:
	.size	_ZN2v88internal28Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Stats_Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"V8.Runtime_Runtime_AsyncGeneratorYield"
	.section	.text._ZN2v88internalL33Stats_Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateE.isra.0:
.LFB25131:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$312, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL33Stats_Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateEE28trace_event_unique_atomic114(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L455
.L448:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L456
.L449:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L455:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL33Stats_Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateEE28trace_event_unique_atomic114(%rip)
	movq	%rax, %r12
	jmp	.L448
.L456:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC21(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L449
	.cfi_endproc
.LFE25131:
	.size	_ZN2v88internalL33Stats_Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal27Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateE:
.LFB20102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L460
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L460:
	movq	%rdx, %rdi
	call	_ZN2v88internalL33Stats_Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20102:
	.size	_ZN2v88internal27Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Stats_Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"V8.Runtime_Runtime_GeneratorGetResumeMode"
	.section	.text._ZN2v88internalL36Stats_Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateE.isra.0:
.LFB25132:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$316, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL36Stats_Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateEE28trace_event_unique_atomic120(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L470
.L463:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L471
.L464:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L470:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL36Stats_Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateEE28trace_event_unique_atomic120(%rip)
	movq	%rax, %r12
	jmp	.L463
.L471:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC22(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L464
	.cfi_endproc
.LFE25132:
	.size	_ZN2v88internalL36Stats_Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal30Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateE:
.LFB20105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L475
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L475:
	movq	%rdx, %rdi
	call	_ZN2v88internalL36Stats_Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20105:
	.size	_ZN2v88internal30Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal32Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal32Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal32Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE:
.LFB25115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25115:
	.size	_GLOBAL__sub_I__ZN2v88internal32Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal32Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal32Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateEE28trace_event_unique_atomic128,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateEE28trace_event_unique_atomic128, @object
	.size	_ZZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateEE28trace_event_unique_atomic128, 8
_ZZN2v88internalL48Stats_Runtime_AsyncGeneratorHasCatchHandlerForPCEiPmPNS0_7IsolateEE28trace_event_unique_atomic128:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateEE28trace_event_unique_atomic120,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateEE28trace_event_unique_atomic120, @object
	.size	_ZZN2v88internalL36Stats_Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateEE28trace_event_unique_atomic120, 8
_ZZN2v88internalL36Stats_Runtime_GeneratorGetResumeModeEiPmPNS0_7IsolateEE28trace_event_unique_atomic120:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateEE28trace_event_unique_atomic114,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateEE28trace_event_unique_atomic114, @object
	.size	_ZZN2v88internalL33Stats_Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateEE28trace_event_unique_atomic114, 8
_ZZN2v88internalL33Stats_Runtime_AsyncGeneratorYieldEiPmPNS0_7IsolateEE28trace_event_unique_atomic114:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateEE28trace_event_unique_atomic108,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateEE28trace_event_unique_atomic108, @object
	.size	_ZZN2v88internalL34Stats_Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateEE28trace_event_unique_atomic108, 8
_ZZN2v88internalL34Stats_Runtime_AsyncGeneratorRejectEiPmPNS0_7IsolateEE28trace_event_unique_atomic108:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL35Stats_Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL35Stats_Runtime_AsyncGeneratorResolveEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL41Stats_Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic96,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Stats_Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic96, @object
	.size	_ZZN2v88internalL41Stats_Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic96, 8
_ZZN2v88internalL41Stats_Runtime_AsyncGeneratorAwaitUncaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic96:
	.zero	8
	.section	.bss._ZZN2v88internalL39Stats_Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic90,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic90, @object
	.size	_ZZN2v88internalL39Stats_Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic90, 8
_ZZN2v88internalL39Stats_Runtime_AsyncGeneratorAwaitCaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic90:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic82,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic82, @object
	.size	_ZZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic82, 8
_ZZN2v88internalL34Stats_Runtime_GeneratorGetFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic82:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_GeneratorCloseEiPmPNS0_7IsolateEE27trace_event_unique_atomic76,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_GeneratorCloseEiPmPNS0_7IsolateEE27trace_event_unique_atomic76, @object
	.size	_ZZN2v88internalL28Stats_Runtime_GeneratorCloseEiPmPNS0_7IsolateEE27trace_event_unique_atomic76, 8
_ZZN2v88internalL28Stats_Runtime_GeneratorCloseEiPmPNS0_7IsolateEE27trace_event_unique_atomic76:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateEE27trace_event_unique_atomic46,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateEE27trace_event_unique_atomic46, @object
	.size	_ZZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateEE27trace_event_unique_atomic46, 8
_ZZN2v88internalL37Stats_Runtime_CreateJSGeneratorObjectEiPmPNS0_7IsolateEE27trace_event_unique_atomic46:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateEE27trace_event_unique_atomic40,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateEE27trace_event_unique_atomic40, @object
	.size	_ZZN2v88internalL34Stats_Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateEE27trace_event_unique_atomic40, 8
_ZZN2v88internalL34Stats_Runtime_AsyncFunctionResolveEiPmPNS0_7IsolateEE27trace_event_unique_atomic40:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateEE27trace_event_unique_atomic34,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateEE27trace_event_unique_atomic34, @object
	.size	_ZZN2v88internalL33Stats_Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateEE27trace_event_unique_atomic34, 8
_ZZN2v88internalL33Stats_Runtime_AsyncFunctionRejectEiPmPNS0_7IsolateEE27trace_event_unique_atomic34:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateEE27trace_event_unique_atomic28,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateEE27trace_event_unique_atomic28, @object
	.size	_ZZN2v88internalL32Stats_Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateEE27trace_event_unique_atomic28, 8
_ZZN2v88internalL32Stats_Runtime_AsyncFunctionEnterEiPmPNS0_7IsolateEE27trace_event_unique_atomic28:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic22,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic22, @object
	.size	_ZZN2v88internalL40Stats_Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic22, 8
_ZZN2v88internalL40Stats_Runtime_AsyncFunctionAwaitUncaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic22:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic16,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic16, @object
	.size	_ZZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic16, 8
_ZZN2v88internalL38Stats_Runtime_AsyncFunctionAwaitCaughtEiPmPNS0_7IsolateEE27trace_event_unique_atomic16:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
