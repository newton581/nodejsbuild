	.file	"identity-map.cc"
	.text
	.section	.text._ZN2v88internal15IdentityMapBaseD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15IdentityMapBaseD2Ev
	.type	_ZN2v88internal15IdentityMapBaseD2Ev, @function
_ZN2v88internal15IdentityMapBaseD2Ev:
.LFB10221:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10221:
	.size	_ZN2v88internal15IdentityMapBaseD2Ev, .-_ZN2v88internal15IdentityMapBaseD2Ev
	.globl	_ZN2v88internal15IdentityMapBaseD1Ev
	.set	_ZN2v88internal15IdentityMapBaseD1Ev,_ZN2v88internal15IdentityMapBaseD2Ev
	.section	.text._ZN2v88internal15IdentityMapBaseD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15IdentityMapBaseD0Ev
	.type	_ZN2v88internal15IdentityMapBaseD0Ev, @function
_ZN2v88internal15IdentityMapBaseD0Ev:
.LFB10223:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10223:
	.size	_ZN2v88internal15IdentityMapBaseD0Ev, .-_ZN2v88internal15IdentityMapBaseD0Ev
	.section	.text._ZN2v88internal15IdentityMapBase5ClearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15IdentityMapBase5ClearEv
	.type	_ZN2v88internal15IdentityMapBase5ClearEv, @function
_ZN2v88internal15IdentityMapBase5ClearEv:
.LFB10224:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L10
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v88internal4Heap21UnregisterStrongRootsENS0_14FullObjectSlotE@PLT
	movq	(%rbx), %rax
	movq	40(%rbx), %rsi
	movq	%rbx, %rdi
	call	*24(%rax)
	movq	(%rbx), %rax
	movq	48(%rbx), %rsi
	movq	%rbx, %rdi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	movl	$0, 28(%rbx)
	movq	$0, 32(%rbx)
	movups	%xmm0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE10224:
	.size	_ZN2v88internal15IdentityMapBase5ClearEv, .-_ZN2v88internal15IdentityMapBase5ClearEv
	.section	.rodata._ZN2v88internal15IdentityMapBase15EnableIterationEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"!is_iterable()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal15IdentityMapBase15EnableIterationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15IdentityMapBase15EnableIterationEv
	.type	_ZN2v88internal15IdentityMapBase15EnableIterationEv, @function
_ZN2v88internal15IdentityMapBase15EnableIterationEv:
.LFB10225:
	.cfi_startproc
	endbr64
	cmpb	$0, 56(%rdi)
	jne	.L18
	movb	$1, 56(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10225:
	.size	_ZN2v88internal15IdentityMapBase15EnableIterationEv, .-_ZN2v88internal15IdentityMapBase15EnableIterationEv
	.section	.rodata._ZN2v88internal15IdentityMapBase16DisableIterationEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"is_iterable()"
	.section	.text._ZN2v88internal15IdentityMapBase16DisableIterationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15IdentityMapBase16DisableIterationEv
	.type	_ZN2v88internal15IdentityMapBase16DisableIterationEv, @function
_ZN2v88internal15IdentityMapBase16DisableIterationEv:
.LFB10226:
	.cfi_startproc
	endbr64
	cmpb	$0, 56(%rdi)
	je	.L24
	movb	$0, 56(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10226:
	.size	_ZN2v88internal15IdentityMapBase16DisableIterationEv, .-_ZN2v88internal15IdentityMapBase16DisableIterationEv
	.section	.rodata._ZNK2v88internal15IdentityMapBase11ScanKeysForEm.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"address != ReadOnlyRoots(heap_).not_mapped_symbol().ptr()"
	.section	.text._ZNK2v88internal15IdentityMapBase11ScanKeysForEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15IdentityMapBase11ScanKeysForEm
	.type	_ZNK2v88internal15IdentityMapBase11ScanKeysForEm, @function
_ZNK2v88internal15IdentityMapBase11ScanKeysForEm:
.LFB10227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %rax
	cmpq	-33832(%rax), %rsi
	je	.L38
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	%rsi, %rbx
	call	_ZN2v84base10hash_valueEm@PLT
	movq	16(%r12), %rdx
	movl	32(%r12), %edi
	andl	36(%r12), %eax
	movq	-33832(%rdx), %rsi
	cmpl	%edi, %eax
	jge	.L27
	movq	40(%r12), %r9
	movslq	%eax, %rdx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L39:
	cmpq	%rsi, %rcx
	je	.L33
	addq	$1, %rdx
	cmpl	%edx, %edi
	jle	.L27
.L29:
	movq	(%r9,%rdx,8), %rcx
	movl	%edx, %r8d
	cmpq	%rbx, %rcx
	jne	.L39
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movl	$-1, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	testl	%eax, %eax
	jle	.L33
	leal	-1(%rax), %edi
	movq	40(%r12), %rcx
	xorl	%eax, %eax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L40:
	cmpq	%rsi, %rdx
	je	.L33
	leaq	1(%rax), %rdx
	cmpq	%rax, %rdi
	je	.L33
	movq	%rdx, %rax
.L30:
	movq	(%rcx,%rax,8), %rdx
	movl	%eax, %r8d
	cmpq	%rbx, %rdx
	jne	.L40
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10227:
	.size	_ZNK2v88internal15IdentityMapBase11ScanKeysForEm, .-_ZNK2v88internal15IdentityMapBase11ScanKeysForEm
	.section	.text._ZNK2v88internal15IdentityMapBase4HashEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15IdentityMapBase4HashEm
	.type	_ZNK2v88internal15IdentityMapBase4HashEm, @function
_ZNK2v88internal15IdentityMapBase4HashEm:
.LFB10232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	-33832(%rax), %rsi
	je	.L44
	movq	%rsi, %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10232:
	.size	_ZNK2v88internal15IdentityMapBase4HashEm, .-_ZNK2v88internal15IdentityMapBase4HashEm
	.section	.text._ZNK2v88internal15IdentityMapBase10KeyAtIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15IdentityMapBase10KeyAtIndexEi
	.type	_ZNK2v88internal15IdentityMapBase10KeyAtIndexEi, @function
_ZNK2v88internal15IdentityMapBase10KeyAtIndexEi:
.LFB10236:
	.cfi_startproc
	endbr64
	cmpb	$0, 56(%rdi)
	je	.L50
	movq	40(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10236:
	.size	_ZNK2v88internal15IdentityMapBase10KeyAtIndexEi, .-_ZNK2v88internal15IdentityMapBase10KeyAtIndexEi
	.section	.text._ZNK2v88internal15IdentityMapBase12EntryAtIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15IdentityMapBase12EntryAtIndexEi
	.type	_ZNK2v88internal15IdentityMapBase12EntryAtIndexEi, @function
_ZNK2v88internal15IdentityMapBase12EntryAtIndexEi:
.LFB10237:
	.cfi_startproc
	endbr64
	cmpb	$0, 56(%rdi)
	je	.L56
	movq	48(%rdi), %rax
	movslq	%esi, %rsi
	leaq	(%rax,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10237:
	.size	_ZNK2v88internal15IdentityMapBase12EntryAtIndexEi, .-_ZNK2v88internal15IdentityMapBase12EntryAtIndexEi
	.section	.text._ZNK2v88internal15IdentityMapBase9NextIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15IdentityMapBase9NextIndexEi
	.type	_ZNK2v88internal15IdentityMapBase9NextIndexEi, @function
_ZNK2v88internal15IdentityMapBase9NextIndexEi:
.LFB10238:
	.cfi_startproc
	endbr64
	cmpb	$0, 56(%rdi)
	je	.L66
	movq	16(%rdi), %rax
	movl	32(%rdi), %r8d
	movq	-33832(%rax), %rdx
	leal	1(%rsi), %eax
	cmpl	%eax, %r8d
	jle	.L57
	movq	40(%rdi), %rcx
	cltq
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L67:
	addq	$1, %rax
	cmpl	%eax, %r8d
	jle	.L57
.L60:
	cmpq	%rdx, (%rcx,%rax,8)
	je	.L67
	movl	%eax, %r8d
.L57:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10238:
	.size	_ZNK2v88internal15IdentityMapBase9NextIndexEi, .-_ZNK2v88internal15IdentityMapBase9NextIndexEi
	.section	.text._ZN2v88internal15IdentityMapBase6ResizeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15IdentityMapBase6ResizeEi
	.type	_ZN2v88internal15IdentityMapBase6ResizeEi, @function
_ZN2v88internal15IdentityMapBase6ResizeEi:
.LFB10261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpb	$0, 56(%rdi)
	jne	.L81
	movl	32(%rdi), %eax
	movq	%rdi, %r12
	movq	40(%rdi), %r13
	movl	%esi, 32(%rdi)
	movq	48(%rdi), %r14
	movl	%eax, %r15d
	leal	-1(%rsi), %eax
	movslq	%esi, %rsi
	movl	%eax, 36(%rdi)
	movq	16(%rdi), %rax
	movl	452(%rax), %eax
	movl	$0, 28(%rdi)
	movl	%eax, 24(%rdi)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	16(%r12), %rdx
	movslq	32(%r12), %rsi
	movq	%rax, 40(%r12)
	movq	-33832(%rdx), %rbx
	testl	%esi, %esi
	jle	.L70
	movq	%rbx, (%rax)
	movslq	32(%r12), %rsi
	movl	$8, %edx
	movl	$1, %eax
	cmpl	$1, %esi
	jle	.L70
	.p2align 4,,10
	.p2align 3
.L71:
	movq	40(%r12), %rsi
	addl	$1, %eax
	movq	%rbx, (%rsi,%rdx)
	movslq	32(%r12), %rsi
	addq	$8, %rdx
	cmpl	%eax, %esi
	jg	.L71
.L70:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movslq	32(%r12), %rdx
	xorl	%esi, %esi
	movq	%rax, 48(%r12)
	movq	%rax, %rdi
	salq	$3, %rdx
	call	memset@PLT
	leal	-1(%r15), %ecx
	movl	%r15d, %eax
	xorl	%r15d, %r15d
	movq	%rcx, -56(%rbp)
	testl	%eax, %eax
	jg	.L76
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%rax, %r15
.L76:
	movq	0(%r13,%r15,8), %rsi
	cmpq	%rbx, %rsi
	je	.L74
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBase9InsertKeyEm
	movq	(%r14,%r15,8), %rdi
	movq	48(%r12), %rsi
	cltq
	movq	%rdi, (%rsi,%rax,8)
.L74:
	leaq	1(%r15), %rax
	cmpq	-56(%rbp), %r15
	jne	.L82
.L75:
	movq	16(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4Heap21UnregisterStrongRootsENS0_14FullObjectSlotE@PLT
	movq	40(%r12), %rsi
	movslq	32(%r12), %rax
	movq	16(%r12), %rdi
	leaq	(%rsi,%rax,8), %rdx
	call	_ZN2v88internal4Heap19RegisterStrongRootsENS0_14FullObjectSlotES2_@PLT
	movq	(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*24(%rax)
	movq	(%r12), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10261:
	.size	_ZN2v88internal15IdentityMapBase6ResizeEi, .-_ZN2v88internal15IdentityMapBase6ResizeEi
	.section	.text._ZN2v88internal15IdentityMapBase9InsertKeyEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15IdentityMapBase9InsertKeyEm
	.type	_ZN2v88internal15IdentityMapBase9InsertKeyEm, @function
_ZN2v88internal15IdentityMapBase9InsertKeyEm:
.LFB10228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	-33832(%rax), %r12
	cmpq	%r12, %rsi
	je	.L89
.L84:
	movq	%rbx, %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	movl	32(%r13), %r8d
	movl	36(%r13), %edi
	movl	%r8d, %edx
	andl	%edi, %eax
	shrl	$31, %edx
	addl	%r8d, %edx
	sarl	%edx
	subl	$1, %edx
	testl	%edx, %edx
	jle	.L85
	movq	40(%r13), %r9
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L95:
	cmpq	%r12, %rcx
	je	.L94
	addl	$1, %eax
	andl	%edi, %eax
	subl	$1, %edx
	je	.L85
.L88:
	movslq	%eax, %rcx
	leaq	(%r9,%rcx,8), %rsi
	movq	(%rsi), %rcx
	cmpq	%rbx, %rcx
	jne	.L95
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	leal	(%r8,%r8), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal15IdentityMapBase6ResizeEi
	movq	16(%r13), %rax
	cmpq	-33832(%rax), %rbx
	jne	.L84
.L89:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	addl	$1, 28(%r13)
	movq	%rbx, (%rsi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10228:
	.size	_ZN2v88internal15IdentityMapBase9InsertKeyEm, .-_ZN2v88internal15IdentityMapBase9InsertKeyEm
	.section	.text._ZN2v88internal15IdentityMapBase11DeleteIndexEiPPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15IdentityMapBase11DeleteIndexEiPPv
	.type	_ZN2v88internal15IdentityMapBase11DeleteIndexEiPPv, @function
_ZN2v88internal15IdentityMapBase11DeleteIndexEiPPv:
.LFB10229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rax, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L97
	movq	48(%rdi), %rcx
	movq	(%rcx,%rax,8), %rcx
	movq	%rcx, (%rdx)
.L97:
	movq	16(%r12), %rdx
	movq	-33832(%rdx), %r14
	movq	40(%r12), %rdx
	movq	%r14, (%rdx,%rax,8)
	movq	48(%r12), %rdx
	movq	$0, (%rdx,%rax,8)
	movl	28(%r12), %eax
	movl	32(%r12), %esi
	subl	$1, %eax
	movl	%eax, 28(%r12)
	cmpl	$4, %esi
	jle	.L101
	sarl	%esi
	addl	%eax, %eax
	cmpl	%esi, %eax
	jl	.L117
.L101:
	movq	40(%r12), %rdx
	leal	1(%r13), %ebx
	andl	36(%r12), %ebx
	movslq	%ebx, %rax
	movq	(%rdx,%rax,8), %rdi
	leaq	0(,%rax,8), %r15
	cmpq	%rdi, %r14
	jne	.L100
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L119:
	testb	%cl, %cl
	je	.L107
.L116:
	testb	%al, %al
	jne	.L105
.L107:
	movq	40(%r12), %rax
	leaq	0(,%r13,8), %rsi
	leaq	(%rax,%r15), %rdx
	addq	%rsi, %rax
	movq	(%rax), %rcx
	movq	(%rdx), %rdi
	movq	%rdi, (%rax)
	movq	%rcx, (%rdx)
	movq	48(%r12), %r13
	addq	%r13, %r15
	addq	%rsi, %r13
	movq	0(%r13), %rax
	movq	(%r15), %rdx
	movq	%rdx, 0(%r13)
	movslq	%ebx, %r13
	movq	%rax, (%r15)
	movl	36(%r12), %edx
.L105:
	addl	$1, %ebx
	andl	%edx, %ebx
	movq	40(%r12), %rdx
	movslq	%ebx, %rax
	movq	(%rdx,%rax,8), %rdi
	leaq	0(,%rax,8), %r15
	cmpq	%r14, %rdi
	je	.L102
.L100:
	movq	16(%r12), %rax
	cmpq	%rdi, -33832(%rax)
	je	.L118
	call	_ZN2v84base10hash_valueEm@PLT
	movl	36(%r12), %edx
	andl	%edx, %eax
	cmpl	%r13d, %eax
	setg	%cl
	cmpl	%ebx, %eax
	setle	%al
	cmpl	%r13d, %ebx
	jg	.L119
	testb	%cl, %cl
	je	.L116
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBase6ResizeEi
.L102:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10229:
	.size	_ZN2v88internal15IdentityMapBase11DeleteIndexEiPPv, .-_ZN2v88internal15IdentityMapBase11DeleteIndexEiPPv
	.section	.rodata._ZNSt6vectorISt4pairImPvESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt4pairImPvESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairImPvESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairImPvESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorISt4pairImPvESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorISt4pairImPvESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB11575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$576460752303423487, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L139
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L130
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L140
.L122:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	16(%r14), %r8
.L129:
	movq	8(%rdx), %rsi
	movq	(%rdx), %rdx
	addq	%r14, %rcx
	movq	%rdx, (%rcx)
	movq	%rsi, 8(%rcx)
	cmpq	%r15, %rbx
	je	.L124
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L125:
	movq	(%rdx), %rdi
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L125
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	16(%r14,%rdx), %r8
.L124:
	cmpq	%r12, %rbx
	je	.L126
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L127:
	movq	8(%rdx), %rsi
	movq	(%rdx), %rdi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L127
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L126:
	testq	%r15, %r15
	je	.L128
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L128:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L123
	movl	$16, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L130:
	movl	$16, %esi
	jmp	.L122
.L123:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$4, %rax
	movq	%rax, %rsi
	jmp	.L122
.L139:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11575:
	.size	_ZNSt6vectorISt4pairImPvESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorISt4pairImPvESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZN2v88internal15IdentityMapBase6RehashEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15IdentityMapBase6RehashEv
	.type	_ZN2v88internal15IdentityMapBase6RehashEv, @function
_ZN2v88internal15IdentityMapBase6RehashEv:
.LFB10239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 56(%rdi)
	jne	.L160
	movq	16(%rdi), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movq	%rdi, %r12
	movaps	%xmm0, -80(%rbp)
	movl	452(%rax), %edx
	movl	%edx, 24(%rdi)
	movl	32(%rdi), %edx
	movq	-33832(%rax), %r15
	testl	%edx, %edx
	jle	.L141
	leaq	-96(%rbp), %rax
	xorl	%ebx, %ebx
	movl	$-1, %r13d
	movq	%rax, -112(%rbp)
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L163:
	movq	16(%r12), %rax
	cmpq	-33832(%rax), %rdi
	je	.L161
	movl	%ebx, -100(%rbp)
	call	_ZN2v84base10hash_valueEm@PLT
	andl	36(%r12), %eax
	movl	-100(%rbp), %ecx
	cmpl	%r13d, %eax
	jle	.L146
	cmpl	%ebx, %eax
	jg	.L146
	movl	32(%r12), %edx
.L144:
	addq	$1, %rbx
	cmpl	%ebx, %edx
	jle	.L162
.L149:
	movq	40(%r12), %rax
	leaq	0(,%rbx,8), %r14
	movq	(%rax,%rbx,8), %rdi
	cmpq	%r15, %rdi
	jne	.L163
	movl	%ebx, %r13d
	addq	$1, %rbx
	cmpl	%ebx, %edx
	jg	.L149
.L162:
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %r13
	cmpq	%rdi, %r13
	je	.L150
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L151:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	movq	8(%rbx), %r14
	addq	$16, %rbx
	call	_ZN2v88internal15IdentityMapBase9InsertKeyEm
	movq	48(%r12), %rdx
	cltq
	movq	%r14, (%rdx,%rax,8)
	cmpq	%rbx, %r13
	jne	.L151
	movq	-80(%rbp), %rdi
.L150:
	testq	%rdi, %rdi
	je	.L141
	call	_ZdlPv@PLT
.L141:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movq	40(%r12), %rdx
	movq	48(%r12), %rax
	movq	-72(%rbp), %rsi
	addq	%r14, %rdx
	addq	%r14, %rax
	movq	(%rdx), %rdx
	movq	%rdx, -96(%rbp)
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	cmpq	-64(%rbp), %rsi
	je	.L147
	movq	%rdx, (%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, -72(%rbp)
.L148:
	movq	40(%r12), %rax
	movl	%ecx, %r13d
	movq	%r15, (%rax,%r14)
	movq	48(%r12), %rax
	movq	$0, (%rax,%r14)
	movl	32(%r12), %edx
	subl	$1, 28(%r12)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L147:
	movq	-112(%rbp), %rdx
	leaq	-80(%rbp), %rdi
	movl	%ecx, -100(%rbp)
	call	_ZNSt6vectorISt4pairImPvESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movl	-100(%rbp), %ecx
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10239:
	.size	_ZN2v88internal15IdentityMapBase6RehashEv, .-_ZN2v88internal15IdentityMapBase6RehashEv
	.section	.text._ZNK2v88internal15IdentityMapBase6LookupEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15IdentityMapBase6LookupEm
	.type	_ZNK2v88internal15IdentityMapBase6LookupEm, @function
_ZNK2v88internal15IdentityMapBase6LookupEm:
.LFB10230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZNK2v88internal15IdentityMapBase11ScanKeysForEm
	testl	%eax, %eax
	js	.L168
.L165:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	16(%r12), %rdx
	movl	452(%rdx), %ecx
	cmpl	%ecx, 24(%r12)
	je	.L165
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBase6RehashEv
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88internal15IdentityMapBase11ScanKeysForEm
	.cfi_endproc
.LFE10230:
	.size	_ZNK2v88internal15IdentityMapBase6LookupEm, .-_ZNK2v88internal15IdentityMapBase6LookupEm
	.section	.text._ZN2v88internal15IdentityMapBase14LookupOrInsertEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15IdentityMapBase14LookupOrInsertEm
	.type	_ZN2v88internal15IdentityMapBase14LookupOrInsertEm, @function
_ZN2v88internal15IdentityMapBase14LookupOrInsertEm:
.LFB10231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZNK2v88internal15IdentityMapBase11ScanKeysForEm
	testl	%eax, %eax
	js	.L173
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	16(%r12), %rax
	movl	452(%rax), %eax
	cmpl	%eax, 24(%r12)
	je	.L171
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBase6RehashEv
.L171:
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15IdentityMapBase9InsertKeyEm
	.cfi_endproc
.LFE10231:
	.size	_ZN2v88internal15IdentityMapBase14LookupOrInsertEm, .-_ZN2v88internal15IdentityMapBase14LookupOrInsertEm
	.section	.text._ZN2v88internal15IdentityMapBase8GetEntryEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15IdentityMapBase8GetEntryEm
	.type	_ZN2v88internal15IdentityMapBase8GetEntryEm, @function
_ZN2v88internal15IdentityMapBase8GetEntryEm:
.LFB10233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpb	$0, 56(%rdi)
	jne	.L185
	movl	32(%rdi), %eax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	testl	%eax, %eax
	jne	.L176
	movabsq	$12884901892, %rax
	movl	$4, %esi
	movq	%rax, 32(%rdi)
	movq	16(%rdi), %rax
	movl	452(%rax), %eax
	movl	%eax, 24(%rdi)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rsi
	movq	%rax, 40(%rbx)
	movq	-33832(%rdx), %rdi
	testl	%esi, %esi
	jle	.L177
	movq	%rdi, (%rax)
	movslq	32(%rbx), %rsi
	movl	$8, %edx
	movl	$1, %eax
	cmpl	$1, %esi
	jle	.L177
	.p2align 4,,10
	.p2align 3
.L178:
	movq	40(%rbx), %rcx
	addl	$1, %eax
	movq	%rdi, (%rcx,%rdx)
	movslq	32(%rbx), %rsi
	addq	$8, %rdx
	cmpl	%eax, %esi
	jg	.L178
.L177:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*16(%rax)
	movslq	32(%rbx), %rdx
	xorl	%esi, %esi
	movq	%rax, 48(%rbx)
	movq	%rax, %rdi
	salq	$3, %rdx
	call	memset@PLT
	movq	40(%rbx), %rsi
	movslq	32(%rbx), %rax
	movq	16(%rbx), %rdi
	leaq	(%rsi,%rax,8), %rdx
	call	_ZN2v88internal4Heap19RegisterStrongRootsENS0_14FullObjectSlotES2_@PLT
.L176:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal15IdentityMapBase11ScanKeysForEm
	testl	%eax, %eax
	js	.L186
	movq	48(%rbx), %rdx
	cltq
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movl	452(%rax), %eax
	cmpl	%eax, 24(%rbx)
	jne	.L187
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15IdentityMapBase9InsertKeyEm
.L188:
	movq	48(%rbx), %rdx
	cltq
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal15IdentityMapBase6RehashEv
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15IdentityMapBase9InsertKeyEm
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10233:
	.size	_ZN2v88internal15IdentityMapBase8GetEntryEm, .-_ZN2v88internal15IdentityMapBase8GetEntryEm
	.section	.text._ZNK2v88internal15IdentityMapBase9FindEntryEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15IdentityMapBase9FindEntryEm
	.type	_ZNK2v88internal15IdentityMapBase9FindEntryEm, @function
_ZNK2v88internal15IdentityMapBase9FindEntryEm:
.LFB10234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpb	$0, 56(%rdi)
	jne	.L197
	movl	28(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L194
	movq	%rsi, %r12
	call	_ZNK2v88internal15IdentityMapBase11ScanKeysForEm
	testl	%eax, %eax
	js	.L198
.L193:
	movq	48(%rbx), %rdx
	cltq
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movl	452(%rax), %eax
	cmpl	%eax, 24(%rbx)
	jne	.L199
.L194:
	xorl	%eax, %eax
.L200:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal15IdentityMapBase6RehashEv
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal15IdentityMapBase11ScanKeysForEm
	testl	%eax, %eax
	jns	.L193
	xorl	%eax, %eax
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L197:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10234:
	.size	_ZNK2v88internal15IdentityMapBase9FindEntryEm, .-_ZNK2v88internal15IdentityMapBase9FindEntryEm
	.section	.text._ZN2v88internal15IdentityMapBase11DeleteEntryEmPPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15IdentityMapBase11DeleteEntryEmPPv
	.type	_ZN2v88internal15IdentityMapBase11DeleteEntryEmPPv, @function
_ZN2v88internal15IdentityMapBase11DeleteEntryEmPPv:
.LFB10235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	cmpb	$0, 56(%rdi)
	jne	.L208
	movl	28(%rdi), %eax
	movq	%rdi, %r12
	testl	%eax, %eax
	jne	.L209
.L201:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movq	%rsi, %r13
	movq	%rdx, %r14
	call	_ZNK2v88internal15IdentityMapBase11ScanKeysForEm
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L210
.L205:
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15IdentityMapBase11DeleteIndexEiPPv
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	movq	16(%r12), %rax
	movl	452(%rax), %eax
	cmpl	%eax, 24(%r12)
	je	.L201
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBase6RehashEv
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal15IdentityMapBase11ScanKeysForEm
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L201
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L208:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10235:
	.size	_ZN2v88internal15IdentityMapBase11DeleteEntryEmPPv, .-_ZN2v88internal15IdentityMapBase11DeleteEntryEmPPv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15IdentityMapBaseD2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15IdentityMapBaseD2Ev, @function
_GLOBAL__sub_I__ZN2v88internal15IdentityMapBaseD2Ev:
.LFB11825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11825:
	.size	_GLOBAL__sub_I__ZN2v88internal15IdentityMapBaseD2Ev, .-_GLOBAL__sub_I__ZN2v88internal15IdentityMapBaseD2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15IdentityMapBaseD2Ev
	.weak	_ZTVN2v88internal15IdentityMapBaseE
	.section	.data.rel.ro._ZTVN2v88internal15IdentityMapBaseE,"awG",@progbits,_ZTVN2v88internal15IdentityMapBaseE,comdat
	.align 8
	.type	_ZTVN2v88internal15IdentityMapBaseE, @object
	.size	_ZTVN2v88internal15IdentityMapBaseE, 48
_ZTVN2v88internal15IdentityMapBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
