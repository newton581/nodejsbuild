	.file	"code-generator-x64.cc"
	.text
	.section	.text._ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,"axG",@progbits,_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.type	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, @function
_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv:
.LFB6897:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6897:
	.size	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, .-_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID2Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID2Ev:
.LFB32656:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToIE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal8compiler13OutOfLineCodeD2Ev@PLT
	.cfi_endproc
.LFE32656:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID2Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID2Ev
	.set	_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID1Ev,_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID2Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD2Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD2Ev:
.LFB32652:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal8compiler13OutOfLineCodeD2Ev@PLT
	.cfi_endproc
.LFE32652:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD2Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD2Ev
	.set	_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD1Ev,_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD2Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND2Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND2Ev:
.LFB32664:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaNE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal8compiler13OutOfLineCodeD2Ev@PLT
	.cfi_endproc
.LFE32664:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND2Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND2Ev
	.set	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND1Ev,_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND2Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND2Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND2Ev:
.LFB32660:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaNE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal8compiler13OutOfLineCodeD2Ev@PLT
	.cfi_endproc
.LFE32660:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND2Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND2Ev
	.set	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND1Ev,_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND2Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD2Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD2Ev:
.LFB26241:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal8compiler13OutOfLineCodeD2Ev@PLT
	.cfi_endproc
.LFE26241:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD2Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD2Ev
	.set	_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD1Ev,_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD2Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD2Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD2Ev:
.LFB32648:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal8compiler13OutOfLineCodeD2Ev@PLT
	.cfi_endproc
.LFE32648:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD2Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD2Ev
	.set	_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD1Ev,_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD2Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWrite8GenerateEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWrite8GenerateEv, @function
_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWrite8GenerateEv:
.LFB26232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	32(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 76(%rbx)
	jle	.L10
	movl	64(%rbx), %esi
	movl	$1, %ecx
	movq	%r12, %rdx
	call	_ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE@PLT
	movq	32(%rbx), %rdi
.L10:
	subq	$8, %rsp
	movl	68(%rbx), %edx
	movl	64(%rbx), %esi
	movq	%r12, %r9
	pushq	$1
	movl	$4, %r8d
	movl	$2, %ecx
	call	_ZN2v88internal14TurboAssembler13CheckPageFlagENS0_8RegisterES2_iNS0_9ConditionEPNS0_5LabelENS4_8DistanceE@PLT
	movq	52(%rbx), %rdx
	movl	60(%rbx), %ecx
	movl	$8, %r8d
	movl	72(%rbx), %esi
	movq	32(%rbx), %rdi
	movq	%rdx, -36(%rbp)
	movl	%ecx, -28(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	popq	%rax
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	movl	76(%rbx), %esi
	popq	%rdx
	movq	24(%rax), %rax
	testl	%esi, %esi
	movl	4(%rax), %edx
	setle	%cl
	cmpl	$1, %edx
	je	.L26
	testl	%edx, %edx
	jle	.L20
	movq	8(%rax), %rax
	subl	$1, %edx
	leaq	8(%rax,%rdx,8), %rdx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$8, %rax
	cmpq	%rax, %rdx
	je	.L20
.L13:
	cmpq	$0, (%rax)
	je	.L27
	movl	$1, %r8d
.L12:
	movq	32(%rbx), %rdi
	cmpl	$2, %esi
	je	.L28
.L14:
	cmpl	$1, 80(%rbx)
	je	.L29
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	movl	72(%rbx), %edx
	movl	48(%rbx), %esi
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeE@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	xorl	%r8d, %r8d
	cmpl	$2, %esi
	jne	.L14
.L28:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	movl	72(%rbx), %edx
	movl	48(%rbx), %esi
	leaq	-16(%rbp), %rsp
	movl	%r8d, %ecx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler23CallEphemeronKeyBarrierENS0_8RegisterES2_NS0_14SaveFPRegsModeE@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	movl	72(%rbx), %edx
	movl	48(%rbx), %esi
	leaq	-16(%rbp), %rsp
	movl	$20, %r9d
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeEm@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	xorl	%r8d, %r8d
	cmpq	$0, 8(%rax)
	setne	%r8b
	jmp	.L12
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26232:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWrite8GenerateEv, .-_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWrite8GenerateEv
	.section	.rodata._ZN2v88internal10ZoneObjectdlEPvm.isra.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal10ZoneObjectdlEPvm.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal10ZoneObjectdlEPvm.isra.0, @function
_ZN2v88internal10ZoneObjectdlEPvm.isra.0:
.LFB33770:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE33770:
	.size	_ZN2v88internal10ZoneObjectdlEPvm.isra.0, .-_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID0Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID0Ev:
.LFB32658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToIE(%rip), %rax
	movq	%rax, (%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler13OutOfLineCodeD2Ev@PLT
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE32658:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID0Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID0Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD0Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD0Ev:
.LFB32654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteE(%rip), %rax
	movq	%rax, (%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler13OutOfLineCodeD2Ev@PLT
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE32654:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD0Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD0Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND0Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND0Ev:
.LFB32666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaNE(%rip), %rax
	movq	%rax, (%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler13OutOfLineCodeD2Ev@PLT
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE32666:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND0Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND0Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND0Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND0Ev:
.LFB32662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaNE(%rip), %rax
	movq	%rax, (%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler13OutOfLineCodeD2Ev@PLT
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE32662:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND0Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND0Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD0Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD0Ev:
.LFB32650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapE(%rip), %rax
	movq	%rax, (%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler13OutOfLineCodeD2Ev@PLT
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE32650:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD0Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD0Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD0Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD0Ev:
.LFB26243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapE(%rip), %rax
	movq	%rax, (%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler13OutOfLineCodeD2Ev@PLT
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE26243:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD0Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD0Ev
	.section	.text._ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0, @function
_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0:
.LFB33804:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	addl	24(%r12), %ebx
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	%ebx, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
	.cfi_endproc
.LFE33804:
	.size	_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0, .-_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_129EmitWordLoadPoisoningIfNeededEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterE.isra.0.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_129EmitWordLoadPoisoningIfNeededEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterE.isra.0.part.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_129EmitWordLoadPoisoningIfNeededEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterE.isra.0.part.0:
.LFB33856:
	.cfi_startproc
	movq	8(%rsi), %rax
	movl	$8, %r8d
	movl	$12, %ecx
	addq	$200, %rdi
	movl	$35, %esi
	movq	40(%rax), %rdx
	sarq	$35, %rdx
	jmp	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	.cfi_endproc
.LFE33856:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_129EmitWordLoadPoisoningIfNeededEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterE.isra.0.part.0, .-_ZN2v88internal8compiler12_GLOBAL__N_129EmitWordLoadPoisoningIfNeededEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterE.isra.0.part.0
	.section	.text._ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S4_EEXadL_ZNS7_5movsdES3_S4_EEEEvS3_S4_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S4_EEXadL_ZNS7_5movsdES3_S4_EEEEvS3_S4_.isra.0, @function
_ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S4_EEXadL_ZNS7_5movsdES3_S4_EEEEvS3_S4_.isra.0:
.LFB33872:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L48
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rsi, %r8
	movl	%edx, %r9d
	pushq	$0
	movl	%r10d, %edx
	movl	$17, %esi
	pushq	$1
	pushq	$3
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
.L52:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE33872:
	.size	_ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S4_EEXadL_ZNS7_5movsdES3_S4_EEEEvS3_S4_.isra.0, .-_ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S4_EEXadL_ZNS7_5movsdES3_S4_EEEEvS3_S4_.isra.0
	.section	.text._ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomissES3_S3_EEXadL_ZNS6_7ucomissES3_S3_EEEEvS3_S3_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomissES3_S3_EEXadL_ZNS6_7ucomissES3_S3_EEEEvS3_S3_.isra.0, @function
_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomissES3_S3_EEXadL_ZNS6_7ucomissES3_S3_EEEEvS3_S3_.isra.0:
.LFB33874:
	.cfi_startproc
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L55
	jmp	_ZN2v88internal9Assembler8vucomissENS0_11XMMRegisterES2_@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	jmp	_ZN2v88internal9Assembler7ucomissENS0_11XMMRegisterES2_@PLT
	.cfi_endproc
.LFE33874:
	.size	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomissES3_S3_EEXadL_ZNS6_7ucomissES3_S3_EEEEvS3_S3_.isra.0, .-_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomissES3_S3_EEXadL_ZNS6_7ucomissES3_S3_EEEEvS3_S3_.isra.0
	.section	.text._ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomisdES3_S3_EEXadL_ZNS6_7ucomisdES3_S3_EEEEvS3_S3_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomisdES3_S3_EEXadL_ZNS6_7ucomisdES3_S3_EEEEvS3_S3_.isra.0, @function
_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomisdES3_S3_EEXadL_ZNS6_7ucomisdES3_S3_EEEEvS3_S3_.isra.0:
.LFB33879:
	.cfi_startproc
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L57
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r8d
	movl	$1, %r9d
	movl	%esi, %edx
	movl	$46, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$0
	pushq	$1
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore 6
	jmp	_ZN2v88internal9Assembler7ucomisdENS0_11XMMRegisterES2_@PLT
	.cfi_endproc
.LFE33879:
	.size	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomisdES3_S3_EEXadL_ZNS6_7ucomisdES3_S3_EEEEvS3_S3_.isra.0, .-_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomisdES3_S3_EEXadL_ZNS6_7ucomisdES3_S3_EEEEvS3_S3_.isra.0
	.section	.text._ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovssES3_S3_S3_EEXadL_ZNS6_5movssES3_S3_EEEEvS3_S3_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovssES3_S3_S3_EEXadL_ZNS6_5movssES3_S3_EEEEvS3_S3_.isra.0, @function
_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovssES3_S3_S3_EEXadL_ZNS6_5movssES3_S3_EEEEvS3_S3_.isra.0:
.LFB33884:
	.cfi_startproc
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L64
	movl	%edx, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$16, %esi
	jmp	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_S2_@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	jmp	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterES2_@PLT
	.cfi_endproc
.LFE33884:
	.size	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovssES3_S3_S3_EEXadL_ZNS6_5movssES3_S3_EEEEvS3_S3_.isra.0, .-_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovssES3_S3_S3_EEXadL_ZNS6_5movssES3_S3_EEEEvS3_S3_.isra.0
	.section	.text._ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S3_S3_EEXadL_ZNS6_5movsdES3_S3_EEEEvS3_S3_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S3_S3_EEXadL_ZNS6_5movsdES3_S3_EEEEvS3_S3_.isra.0, @function
_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S3_S3_EEXadL_ZNS6_5movsdES3_S3_EEEEvS3_S3_.isra.0:
.LFB33887:
	.cfi_startproc
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L66
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$3, %r9d
	movl	$16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$0
	pushq	$1
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore 6
	jmp	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterES2_@PLT
	.cfi_endproc
.LFE33887:
	.size	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S3_S3_EEXadL_ZNS6_5movsdES3_S3_EEEEvS3_S3_.isra.0, .-_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S3_S3_EEXadL_ZNS6_5movsdES3_S3_EEEEvS3_S3_.isra.0
	.section	.text._ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovdES3_S4_EEXadL_ZNS7_4movdES3_S4_EEEEvS3_S4_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovdES3_S4_EEXadL_ZNS7_4movdES3_S4_EEEEvS3_S4_.isra.0, @function
_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovdES3_S4_EEXadL_ZNS7_4movdES3_S4_EEEEvS3_S4_.isra.0:
.LFB33893:
	.cfi_startproc
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L73
	jmp	_ZN2v88internal9Assembler5vmovdENS0_11XMMRegisterENS0_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L73:
	jmp	_ZN2v88internal9Assembler4movdENS0_11XMMRegisterENS0_8RegisterE@PLT
	.cfi_endproc
.LFE33893:
	.size	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovdES3_S4_EEXadL_ZNS7_4movdES3_S4_EEEEvS3_S4_.isra.0, .-_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovdES3_S4_EEXadL_ZNS7_4movdES3_S4_EEEEvS3_S4_.isra.0
	.section	.text._ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovqES3_S4_EEXadL_ZNS7_4movqES3_S4_EEEEvS3_S4_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovqES3_S4_EEXadL_ZNS7_4movqES3_S4_EEEEvS3_S4_.isra.0, @function
_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovqES3_S4_EEXadL_ZNS7_4movqES3_S4_EEEEvS3_S4_.isra.0:
.LFB33898:
	.cfi_startproc
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L75
	jmp	_ZN2v88internal9Assembler5vmovqENS0_11XMMRegisterENS0_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L75:
	jmp	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	.cfi_endproc
.LFE33898:
	.size	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovqES3_S4_EEXadL_ZNS7_4movqES3_S4_EEEEvS3_S4_.isra.0, .-_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovqES3_S4_EEXadL_ZNS7_4movqES3_S4_EEEEvS3_S4_.isra.0
	.section	.text._ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler7vmovupsES3_S4_EEXadL_ZNS7_6movupsES3_S4_EEEEvS3_S4_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler7vmovupsES3_S4_EEXadL_ZNS7_6movupsES3_S4_EEEEvS3_S4_.isra.0, @function
_ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler7vmovupsES3_S4_EEXadL_ZNS7_6movupsES3_S4_EEEEvS3_S4_.isra.0:
.LFB33902:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L77
	movl	%edx, -12(%rbp)
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	movl	-12(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rsi, %r8
	movl	%r10d, %edx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$17, %esi
	jmp	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler6movupsENS0_7OperandENS0_11XMMRegisterE@PLT
.L82:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE33902:
	.size	_ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler7vmovupsES3_S4_EEXadL_ZNS7_6movupsES3_S4_EEEEvS3_S4_.isra.0, .-_ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler7vmovupsES3_S4_EEXadL_ZNS7_6movupsES3_S4_EEEEvS3_S4_.isra.0
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaN8GenerateEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaN8GenerateEv, @function
_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaN8GenerateEv:
.LFB26220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	48(%rdi), %edx
	movq	32(%rdi), %rdi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L84
	movl	%edx, %r8d
	movl	%edx, %ecx
	movl	$87, %esi
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	movl	48(%rbx), %edx
	movq	32(%rbx), %rdi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L86
.L88:
	addq	$8, %rsp
	movl	%edx, %r8d
	movl	%edx, %ecx
	movl	$94, %esi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_S2_@PLT
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movl	%edx, %esi
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	movl	48(%rbx), %edx
	movq	32(%rbx), %rdi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L88
.L86:
	addq	$8, %rsp
	movl	%edx, %esi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5divssENS0_11XMMRegisterES2_@PLT
	.cfi_endproc
.LFE26220:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaN8GenerateEv, .-_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaN8GenerateEv
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaN8GenerateEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaN8GenerateEv, @function
_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaN8GenerateEv:
.LFB26224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	48(%rdi), %edx
	movq	32(%rdi), %rdi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L90
	movl	%edx, %r8d
	movl	%edx, %ecx
	movl	$87, %esi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	movl	48(%rbx), %edx
	movq	32(%rbx), %rdi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L92
.L96:
	pushq	$0
	movl	%edx, %r8d
	movl	%edx, %ecx
	movl	$3, %r9d
	pushq	$1
	movl	$94, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	movq	-8(%rbp), %rbx
	popq	%rdx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	%edx, %esi
	call	_ZN2v88internal9Assembler5xorpdENS0_11XMMRegisterES2_@PLT
	movl	48(%rbx), %edx
	movq	32(%rbx), %rdi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L96
.L92:
	movq	-8(%rbp), %rbx
	movl	%edx, %esi
	leave
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5divsdENS0_11XMMRegisterES2_@PLT
	.cfi_endproc
.LFE26224:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaN8GenerateEv, .-_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaN8GenerateEv
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0:
.LFB33866:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	movq	%rdx, -48(%rbp)
	subq	%rbx, %rax
	cmpq	$71, %rax
	jbe	.L101
	leaq	72(%rbx), %rax
	movq	%rax, 16(%rdi)
.L99:
	movq	%r12, %xmm0
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movhps	-48(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE@PLT
	movdqa	-48(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapE(%rip), %rax
	movl	%r13d, 64(%rbx)
	movq	%rax, (%rbx)
	movups	%xmm0, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	$72, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L99
	.cfi_endproc
.LFE33866:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0, .-_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrap8GenerateEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrap8GenerateEv, @function
_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrap8GenerateEv:
.LFB26246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rax
	movl	64(%rdi), %esi
	movq	48(%rdi), %rdi
	movq	32(%rax), %rdx
	subq	16(%rax), %rdx
	call	_ZN2v88internal8compiler13CodeGenerator30AddProtectedInstructionLandingEjj@PLT
	movq	48(%rbx), %rdi
	call	_ZNK2v88internal8compiler13CodeGenerator30wasm_runtime_exception_supportEv@PLT
	testb	%al, %al
	jne	.L103
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	movq	32(%rbx), %r12
	call	_ZN2v88internal17ExternalReference35wasm_call_trap_callback_for_testingEv@PLT
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	movq	32(%rbx), %rdi
	movl	$5, %esi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movq	48(%rbx), %rax
	movq	32(%rbx), %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %edx
	movq	32(%rax), %rax
	movq	(%rax), %rax
	movq	32(%rax), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	salq	$3, %rsi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler3RetEiNS0_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	56(%rbx), %rsi
	movq	48(%rbx), %rdi
	call	_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionEPNS1_11InstructionE@PLT
	movq	32(%rbx), %rdi
	movl	$5, %edx
	movl	$1, %esi
	call	_ZN2v88internal9Assembler9near_callElNS0_9RelocInfo4ModeE@PLT
	movq	48(%rbx), %rax
	movq	8(%rax), %r13
	movq	16(%r13), %r12
	movq	24(%r13), %rax
	subq	%r12, %rax
	cmpq	$39, %rax
	jbe	.L109
	leaq	40(%r12), %rax
	movq	%rax, 16(%r13)
.L105:
	movq	%r13, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$63, %rdx
	jbe	.L110
	leaq	64(%rax), %rdx
	movq	%rdx, 16(%r13)
.L107:
	pxor	%xmm0, %xmm0
	movq	%rdx, 24(%r12)
	movq	%r12, %rsi
	movq	%rax, 8(%r12)
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movq	%rdx, 16(%r12)
	xorl	%edx, %edx
	movl	$-1, 32(%r12)
	movq	48(%rbx), %rdi
	call	_ZN2v88internal8compiler13CodeGenerator15RecordSafepointEPNS1_12ReferenceMapENS0_9Safepoint9DeoptModeE@PLT
	movq	32(%rbx), %rdi
	addq	$8, %rsp
	movl	$47, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler17AssertUnreachableENS0_11AbortReasonE@PLT
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	64(%rax), %rdx
	jmp	.L107
	.cfi_endproc
.LFE26246:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrap8GenerateEv, .-_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrap8GenerateEv
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToI8GenerateEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToI8GenerateEv, @function
_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToI8GenerateEv:
.LFB26228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %r8d
	movl	$4, %edx
	movabsq	$81604378632, %rcx
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	32(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	movq	32(%rbx), %r13
	je	.L112
	movq	64(%rbx), %r12
	cmpb	$0, 64(%r12)
	je	.L121
.L112:
	movl	_ZN2v88internalL3rspE(%rip), %r12d
	xorl	%edx, %edx
	leaq	-100(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-100(%rbp), %r8
	movl	-92(%rbp), %edx
	movl	52(%rbx), %r10d
	movq	%r8, -88(%rbp)
	movl	%edx, -80(%rbp)
	movq	%r8, -76(%rbp)
	movl	%edx, -68(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L113
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, -56(%rbp)
	movl	%edx, %r9d
	pushq	$0
	movq	%r13, %rdi
	movl	$17, %esi
	pushq	$1
	pushq	$3
	movl	%edx, -44(%rbp)
	movl	%r10d, %edx
	movq	%r8, -64(%rbp)
	movq	%r8, -52(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	cmpl	$1, 56(%rbx)
	movq	32(%rbx), %r13
	je	.L122
.L115:
	cmpb	$0, 181(%r13)
	jne	.L123
	movq	72(%rbx), %rax
	movl	$709, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
.L116:
	xorl	%edx, %edx
	movq	32(%rbx), %r13
	leaq	-64(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rdx
	movl	-56(%rbp), %ecx
	movl	$4, %r8d
	movl	48(%rbx), %esi
	movq	%r13, %rdi
	movq	%rdx, -52(%rbp)
	movl	%ecx, -44(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movl	$4, %edx
	movabsq	$81604378632, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L111
	movq	64(%rbx), %r12
	cmpb	$0, 64(%r12)
	je	.L124
.L111:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movl	$709, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal14TurboAssembler11CallBuiltinEi@PLT
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r13, %rdi
	movl	%r10d, %ecx
	movq	%r8, %rsi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	cmpl	$1, 56(%rbx)
	movq	32(%rbx), %r13
	jne	.L115
.L122:
	movl	$5, %edx
	movl	$26, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler9near_callElNS0_9RelocInfo4ModeE@PLT
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L121:
	leaq	8(%r12), %r14
	movq	32(%r13), %rsi
	subq	16(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	24(%r12), %esi
	movq	%r14, %rdi
	addl	$8, %esi
	call	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
	movq	32(%rbx), %r13
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L124:
	movq	32(%rbx), %rax
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	movq	32(%rax), %rsi
	subq	16(%rax), %rsi
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	24(%r12), %esi
	movq	%r13, %rdi
	subl	$8, %esi
	call	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
	jmp	.L111
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26228:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToI8GenerateEv, .-_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToI8GenerateEv
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrap8GenerateEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrap8GenerateEv, @function
_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrap8GenerateEv:
.LFB26236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	56(%rdi), %rcx
	movq	48(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	4(%rcx), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	4(%rdx,%rax), %rax
	movq	(%rcx,%rax,8), %rsi
	movq	40(%rdi), %rax
	movl	%esi, %edx
	movq	%rsi, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L148
	leaq	104(%rax), %rdx
	movq	112(%rax), %rax
	movl	%r8d, %esi
	testq	%rax, %rax
	je	.L131
	movq	%rdx, %rcx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L133
.L132:
	cmpl	32(%rax), %esi
	jle	.L149
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L132
.L133:
	cmpq	%rcx, %rdx
	je	.L131
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdx
.L131:
	movq	48(%rdx), %r12
.L130:
	call	_ZNK2v88internal8compiler13CodeGenerator30wasm_runtime_exception_supportEv@PLT
	testb	%al, %al
	jne	.L136
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	movq	32(%rbx), %r12
	call	_ZN2v88internal17ExternalReference35wasm_call_trap_callback_for_testingEv@PLT
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	movq	32(%rbx), %rdi
	movl	$5, %esi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movq	48(%rbx), %rax
	movq	32(%rbx), %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %edx
	movq	32(%rax), %rax
	movq	(%rax), %rax
	movq	32(%rax), %rsi
	salq	$3, %rsi
	call	_ZN2v88internal14TurboAssembler3RetEiNS0_8RegisterE@PLT
.L126:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L150
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movq	56(%rbx), %rsi
	movq	48(%rbx), %rdi
	call	_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionEPNS1_11InstructionE@PLT
	movq	32(%rbx), %rdi
	movl	%r12d, %esi
	movl	$5, %edx
	call	_ZN2v88internal9Assembler9near_callElNS0_9RelocInfo4ModeE@PLT
	movq	48(%rbx), %rax
	movq	8(%rax), %r13
	movq	16(%r13), %r12
	movq	24(%r13), %rax
	subq	%r12, %rax
	cmpq	$39, %rax
	jbe	.L151
	leaq	40(%r12), %rax
	movq	%rax, 16(%r13)
.L139:
	movq	%r13, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$63, %rdx
	jbe	.L152
	leaq	64(%rax), %rdx
	movq	%rdx, 16(%r13)
.L141:
	pxor	%xmm0, %xmm0
	movq	%rdx, 24(%r12)
	movq	%r12, %rsi
	movq	%rax, 8(%r12)
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movq	%rdx, 16(%r12)
	xorl	%edx, %edx
	movl	$-1, 32(%r12)
	movq	48(%rbx), %rdi
	call	_ZN2v88internal8compiler13CodeGenerator15RecordSafepointEPNS1_12ReferenceMapENS0_9Safepoint9DeoptModeE@PLT
	movq	32(%rbx), %rdi
	movl	$47, %esi
	call	_ZN2v88internal14TurboAssembler17AssertUnreachableENS0_11AbortReasonE@PLT
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L148:
	sarq	$32, %rsi
	andl	$1, %r8d
	je	.L153
	salq	$4, %rsi
	addq	152(%rax), %rsi
	movq	8(%rsi), %r12
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L153:
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-56(%rbp), %r12
	movq	48(%rbx), %rdi
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L152:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	64(%rax), %rdx
	jmp	.L141
.L150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26236:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrap8GenerateEv, .-_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrap8GenerateEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator12AssembleSwapEPNS1_18InstructionOperandES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator12AssembleSwapEPNS1_18InstructionOperandES4_
	.type	_ZN2v88internal8compiler13CodeGenerator12AssembleSwapEPNS1_18InstructionOperandES4_, @function
_ZN2v88internal8compiler13CodeGenerator12AssembleSwapEPNS1_18InstructionOperandES4_:
.LFB26301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeGenerator8MoveType9InferSwapEPNS1_18InstructionOperandES5_@PLT
	cmpl	$1, %eax
	je	.L155
	cmpl	$3, %eax
	je	.L156
	testl	%eax, %eax
	je	.L212
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L155:
	movq	(%r14), %rax
	leaq	200(%rbx), %r13
	movq	%rax, %r15
	sarq	$35, %r15
	testb	$4, %al
	je	.L166
	testb	$24, %al
	jne	.L166
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L166
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movq	24(%rbx), %rax
	addl	$1, 12(%rax)
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L167
	cmpb	$0, 112(%rbx)
	je	.L213
.L167:
	movq	(%r12), %rsi
	movq	24(%rbx), %rdi
	leaq	_ZN2v88internalL3rbpE(%rip), %r14
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rspE(%rip), %r9
	leaq	-68(%rbp), %rcx
	movl	%eax, %edx
	movq	%rcx, %rdi
	movq	%rcx, -280(%rbp)
	andl	$-2, %edx
	testb	$1, %al
	movq	%r14, %rax
	cmovne	%r9, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movl	%r15d, %esi
	movl	$8, %r8d
	movq	%r13, %rdi
	movl	%ecx, -72(%rbp)
	movl	%ecx, -204(%rbp)
	movl	%ecx, -264(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rdx, -212(%rbp)
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	24(%rbx), %rax
	subl	$1, 12(%rax)
	movq	(%r12), %rsi
	movq	24(%rbx), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rspE(%rip), %r9
	movq	-280(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	cmove	%r14, %r9
	movl	(%r9), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_7OperandE@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L154
	cmpb	$0, 112(%rbx)
	jne	.L154
	leaq	56(%rbx), %r12
	movq	232(%rbx), %rsi
	subq	216(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	72(%rbx), %esi
	movq	%r12, %rdi
	subl	$8, %esi
	call	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L166:
	movq	(%r12), %rsi
	movq	24(%rbx), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-68(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rax
	movq	%rax, -116(%rbp)
	movl	-60(%rbp), %eax
	movl	%eax, -108(%rbp)
	movq	(%r14), %rax
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L171
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L172
	pushq	$0
	movl	$15, %edx
	movl	$3, %r9d
	movl	%r15d, %r8d
	pushq	$1
	movl	$15, %ecx
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
.L173:
	movl	-108(%rbp), %r9d
	movq	-116(%rbp), %r8
	movl	%r9d, -84(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L174
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%r15d, %edx
	movq	%r13, %rdi
	pushq	$0
	movl	$16, %esi
	pushq	$1
	pushq	$3
	movq	%r8, -80(%rbp)
	movl	%r9d, -72(%rbp)
	movq	%r8, -68(%rbp)
	movl	%r9d, -60(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
.L175:
	movq	-116(%rbp), %r8
	movl	-108(%rbp), %edx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L176
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, -72(%rbp)
	movl	%edx, %r9d
	pushq	$0
	movl	$17, %esi
	movq	%r13, %rdi
	pushq	$1
	pushq	$3
	movl	%edx, -60(%rbp)
	movl	$15, %edx
	movq	%r8, -80(%rbp)
	movq	%r8, -68(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L212:
	movq	(%r14), %rax
	movq	(%r12), %r12
	leaq	200(%rbx), %r13
	movq	%rax, %rbx
	sarq	$35, %r12
	sarq	$35, %rbx
	testb	$4, %al
	je	.L158
	testb	$24, %al
	jne	.L158
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L158
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	movl	%ebx, %edx
	movq	%r13, %rdi
	movl	$8, %ecx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	movl	$8, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$8, %ecx
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L158:
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L214
	movl	%ebx, %edx
	movl	$15, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler6movapdENS0_11XMMRegisterES2_@PLT
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L162
.L218:
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%r12d, %r8d
	movl	%ebx, %edx
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L164
.L219:
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%r12d, %edx
	movl	$40, %esi
	movq	%r13, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L156:
	movq	(%r14), %rsi
	movq	24(%rbx), %rdi
	leaq	_ZN2v88internalL3rbpE(%rip), %r15
	leaq	_ZN2v88internalL3rspE(%rip), %r13
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	-68(%rbp), %rcx
	movl	%eax, %edx
	movq	%rcx, %rdi
	movq	%rcx, -280(%rbp)
	andl	$-2, %edx
	testb	$1, %al
	movq	%r15, %rax
	cmovne	%r13, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	(%r12), %rsi
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %eax
	movq	24(%rbx), %rdi
	sarq	$35, %rsi
	movq	%rdx, -104(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rdx, -140(%rbp)
	movl	%eax, -96(%rbp)
	movl	%eax, -168(%rbp)
	movl	%eax, -132(%rbp)
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	movq	-280(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	cmove	%r15, %r13
	movl	0(%r13), %esi
	leaq	200(%rbx), %r13
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %eax
	movq	-68(%rbp), %rdx
	movl	%eax, -108(%rbp)
	movl	%eax, -180(%rbp)
	movl	%eax, -120(%rbp)
	movq	(%r14), %rax
	movq	%rdx, -116(%rbp)
	shrq	$5, %rax
	movq	%rdx, -188(%rbp)
	movq	%rdx, -128(%rbp)
	cmpb	$14, %al
	je	.L187
	movl	-60(%rbp), %ecx
	movl	$8, %r8d
	movl	$10, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-140(%rbp), %rsi
	movl	-132(%rbp), %edx
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_7OperandE@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L188
	cmpb	$0, 112(%rbx)
	je	.L215
.L188:
	movq	-128(%rbp), %rsi
	movl	-120(%rbp), %edx
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_7OperandE@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	jne	.L216
.L189:
	movq	-140(%rbp), %rsi
	movl	-132(%rbp), %edx
	movl	$10, %ecx
	movq	%r13, %rdi
	movl	$8, %r8d
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
.L154:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movl	%ebx, %r8d
	movl	$15, %edx
	movl	$40, %esi
	movq	%r13, %rdi
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L218
.L162:
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler6movapdENS0_11XMMRegisterES2_@PLT
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L219
.L164:
	movl	$15, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler6movapdENS0_11XMMRegisterES2_@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L216:
	cmpb	$0, 112(%rbx)
	jne	.L189
	leaq	56(%rbx), %r12
	movq	232(%rbx), %rsi
	subq	216(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	72(%rbx), %esi
	movq	%r12, %rdi
	subl	$8, %esi
	call	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L171:
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L220
	movl	%r15d, %edx
	movl	$15, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterES2_@PLT
.L180:
	movq	-116(%rbp), %r8
	movl	-108(%rbp), %r9d
	movq	%r8, -80(%rbp)
	movl	%r9d, -72(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L181
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%r15d, %edx
	movq	%r13, %rdi
	movq	%r8, -68(%rbp)
	movl	$16, %esi
	movl	%r9d, -60(%rbp)
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
.L182:
	movq	-116(%rbp), %r8
	movl	-108(%rbp), %edx
	movq	%r8, -80(%rbp)
	movl	%edx, -72(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L183
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, -60(%rbp)
	movl	%edx, %r9d
	movq	%r13, %rdi
	movl	$15, %edx
	movl	$17, %esi
	movq	%r8, -68(%rbp)
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L187:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r15d
	movl	-120(%rbp), %ecx
	movq	%r13, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterENS0_7OperandE@PLT
	movq	-140(%rbp), %rsi
	movl	-132(%rbp), %edx
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_7OperandE@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L191
	cmpb	$0, 112(%rbx)
	je	.L221
.L191:
	movq	-128(%rbp), %rsi
	movl	-120(%rbp), %edx
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_7OperandE@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L192
	cmpb	$0, 112(%rbx)
	je	.L222
.L192:
	movq	(%r14), %rsi
	movq	24(%rbx), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	movq	-280(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	addl	$8, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r13, %rdi
	movq	%rsi, -92(%rbp)
	movl	%edx, -84(%rbp)
	movq	%rsi, -152(%rbp)
	movl	%edx, -144(%rbp)
	movq	%rsi, -248(%rbp)
	movl	%edx, -240(%rbp)
	call	_ZN2v88internal9Assembler5pushqENS0_7OperandE@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L194
	cmpb	$0, 112(%rbx)
	je	.L223
.L194:
	movq	(%r12), %rsi
	movq	24(%rbx), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-80(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	addl	$8, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movq	%r13, %rdi
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler4popqENS0_7OperandE@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L196
	cmpb	$0, 112(%rbx)
	je	.L224
.L196:
	movq	-140(%rbp), %rsi
	movl	-132(%rbp), %edx
	movl	%r15d, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler6movupsENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$15, %ecx
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L174:
	movl	-84(%rbp), %ecx
	movq	%r8, %rdx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L172:
	movl	%r15d, %edx
	movl	$15, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterES2_@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L220:
	movl	%r15d, %r8d
	movl	$15, %edx
	movl	$16, %esi
	movq	%r13, %rdi
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L215:
	leaq	56(%rbx), %r12
	movq	232(%rbx), %rsi
	subq	216(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	72(%rbx), %eax
	movq	%r12, %rdi
	leal	8(%rax), %esi
	call	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	56(%rbx), %r12
	movq	232(%rbx), %rsi
	subq	216(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	72(%rbx), %esi
	movq	%r12, %rdi
	subl	$8, %esi
	call	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	56(%rbx), %r14
	movq	232(%rbx), %rsi
	subq	216(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	72(%rbx), %eax
	movq	%r14, %rdi
	leal	8(%rax), %esi
	call	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	56(%rbx), %rdi
	movq	232(%rbx), %rsi
	subq	216(%rbx), %rsi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	72(%rbx), %eax
	movq	-288(%rbp), %rdi
	leal	-8(%rax), %esi
	call	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	56(%rbx), %rdi
	movq	232(%rbx), %rsi
	subq	216(%rbx), %rsi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	72(%rbx), %eax
	movq	-288(%rbp), %rdi
	leal	8(%rax), %esi
	call	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L183:
	movl	$15, %ecx
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler6movupsENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L181:
	movl	-72(%rbp), %ecx
	movq	%r8, %rdx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L182
.L213:
	leaq	56(%rbx), %r14
	movq	232(%rbx), %rsi
	subq	216(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	72(%rbx), %eax
	movq	%r14, %rdi
	leal	8(%rax), %esi
	call	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
	jmp	.L167
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26301:
	.size	_ZN2v88internal8compiler13CodeGenerator12AssembleSwapEPNS1_18InstructionOperandES4_, .-_ZN2v88internal8compiler13CodeGenerator12AssembleSwapEPNS1_18InstructionOperandES4_
	.section	.text._ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_
	.type	_ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_, @function
_ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_:
.LFB26298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeGenerator8MoveType9InferMoveEPNS1_18InstructionOperandES5_@PLT
	cmpl	$5, %eax
	ja	.L226
	leaq	.L228(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_,"a",@progbits
	.align 4
	.align 4
.L228:
	.long	.L233-.L228
	.long	.L232-.L228
	.long	.L231-.L228
	.long	.L230-.L228
	.long	.L229-.L228
	.long	.L227-.L228
	.section	.text._ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_
	.p2align 4,,10
	.p2align 3
.L229:
	movq	(%r12), %rsi
	movq	40(%rbx), %rax
	movl	%esi, %edx
	movq	%rsi, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L369
	leaq	104(%rax), %rsi
	movq	112(%rax), %rax
	movl	%edi, %edx
	testq	%rax, %rax
	je	.L274
	movq	%rsi, %rcx
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L370:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L276
.L275:
	cmpl	32(%rax), %edx
	jle	.L370
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L275
.L276:
	cmpq	%rcx, %rsi
	je	.L274
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L274:
	movl	40(%rsi), %ecx
	movzbl	44(%rsi), %edi
	movq	48(%rsi), %rdx
.L273:
	movq	0(%r13), %rax
	movq	%rax, %r12
	sarq	$35, %r12
	testb	$4, %al
	je	.L279
	testb	$24, %al
	jne	.L279
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L279
	movl	%ecx, -240(%rbp)
	movb	%dil, -236(%rbp)
	movq	%rdx, -232(%rbp)
	cmpl	$8, %ecx
	ja	.L225
	leaq	.L282(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_
	.align 4
	.align 4
.L282:
	.long	.L289-.L282
	.long	.L288-.L282
	.long	.L287-.L282
	.long	.L286-.L282
	.long	.L285-.L282
	.long	.L284-.L282
	.long	.L283-.L282
	.long	.L226-.L282
	.long	.L281-.L282
	.section	.text._ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_
	.p2align 4,,10
	.p2align 3
.L227:
	movq	(%r12), %rsi
	movq	40(%rbx), %rax
	movl	%esi, %edx
	movq	%rsi, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L371
	leaq	104(%rax), %rsi
	movq	112(%rax), %rax
	movl	%edi, %edx
	testq	%rax, %rax
	je	.L303
	movq	%rsi, %rcx
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L305
.L304:
	cmpl	32(%rax), %edx
	jle	.L372
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L304
.L305:
	cmpq	%rcx, %rsi
	je	.L303
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L303:
	movl	40(%rsi), %r12d
	movzbl	44(%rsi), %r14d
	movq	48(%rsi), %r15
.L302:
	movq	0(%r13), %rsi
	movq	24(%rbx), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-68(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	0(%r13), %rax
	movq	%rsi, -104(%rbp)
	movl	%edx, -96(%rbp)
	testb	$4, %al
	je	.L309
	movq	%rax, %rcx
	shrq	$3, %rcx
	andl	$3, %ecx
	cmpl	$1, %ecx
	je	.L373
.L309:
	leaq	200(%rbx), %r13
	cmpl	$2, %r12d
	je	.L374
	movq	%r15, %rdx
	movq	%r13, %rdi
	movl	$19, %ecx
	movl	$10, %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movq	-104(%rbp), %rsi
	movl	-96(%rbp), %edx
	movq	%r13, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ecx
	movl	$8, %r8d
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L233:
	movq	(%r12), %rax
	movq	0(%r13), %rsi
	leaq	200(%rbx), %rdi
	movq	%rax, %rdx
	sarq	$35, %rsi
	sarq	$35, %rdx
	testb	$4, %al
	je	.L234
	testb	$24, %al
	jne	.L234
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L234
	movl	$8, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L232:
	movq	0(%r13), %rsi
	movq	24(%rbx), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-68(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %r9d
	leaq	200(%rbx), %rdi
	movq	(%r12), %rax
	movq	%rsi, -116(%rbp)
	movl	%r9d, -108(%rbp)
	testb	$4, %al
	je	.L239
	movq	%rax, %r8
	movq	%rax, %rcx
	shrq	$5, %r8
	sarq	$35, %rcx
	movl	%r8d, %edx
	movl	%ecx, %r10d
	testb	$24, %al
	jne	.L240
	cmpb	$11, %r8b
	ja	.L240
	movl	$8, %r8d
	movl	%r9d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L231:
	movq	(%r12), %rsi
	movq	24(%rbx), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-68(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %eax
	movq	-68(%rbp), %rdx
	leaq	200(%rbx), %rdi
	movl	%eax, -108(%rbp)
	movq	(%r12), %rax
	movq	%rdx, -116(%rbp)
	testb	$4, %al
	je	.L248
	movq	0(%r13), %r9
	movq	%rax, %r8
	shrq	$3, %rax
	shrq	$5, %r8
	andl	$3, %eax
	sarq	$35, %r9
	movl	%r8d, %ecx
	movl	%r9d, %esi
	cmpl	$1, %eax
	je	.L375
.L249:
	movl	_ZN2v88internal11CpuFeatures10supported_E(%rip), %edx
	movq	-116(%rbp), %r8
	movl	-108(%rbp), %r9d
	shrl	$5, %edx
	andl	$1, %edx
	cmpb	$14, %cl
	je	.L251
	movl	%r9d, -84(%rbp)
	testb	%dl, %dl
	je	.L252
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movq	%r8, -68(%rbp)
	pushq	$0
	movl	$16, %esi
	pushq	$1
	pushq	$3
	movl	%r9d, -60(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	.p2align 4,,10
	.p2align 3
.L225:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L376
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	(%r12), %rsi
	movq	24(%rbx), %rdi
	leaq	_ZN2v88internalL3rspE(%rip), %r14
	leaq	-68(%rbp), %r15
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	movq	%r15, %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	movq	%rcx, %rax
	cmovne	%r14, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	0(%r13), %rsi
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %eax
	movq	24(%rbx), %rdi
	sarq	$35, %rsi
	movq	%rdx, -104(%rbp)
	movq	%rdx, -164(%rbp)
	movq	%rdx, -152(%rbp)
	movl	%eax, -96(%rbp)
	movl	%eax, -156(%rbp)
	movl	%eax, -144(%rbp)
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	movq	%r15, %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	cmove	%rcx, %r14
	movl	(%r14), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rax
	movq	%rax, -140(%rbp)
	movl	-60(%rbp), %eax
	movl	%eax, -132(%rbp)
	movq	(%r12), %rax
	leaq	200(%rbx), %r12
	testb	$4, %al
	je	.L258
	movq	%rax, %rsi
	shrq	$3, %rax
	movq	%rax, %rdx
	shrq	$5, %rsi
	andl	$3, %edx
	movl	%esi, %ecx
	cmpl	$1, %edx
	je	.L377
.L259:
	movl	_ZN2v88internal11CpuFeatures10supported_E(%rip), %edx
	movq	-152(%rbp), %r8
	movl	-144(%rbp), %r9d
	shrl	$5, %edx
	andl	$1, %edx
	cmpb	$14, %cl
	je	.L261
	movl	%r9d, -84(%rbp)
	testb	%dl, %dl
	je	.L262
	subq	$8, %rsp
	movl	$15, %edx
	movq	%r12, %rdi
	movq	%r8, -68(%rbp)
	pushq	$0
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$16, %esi
	pushq	$1
	pushq	$3
	movl	%r9d, -60(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
.L263:
	movq	-140(%rbp), %r8
	movl	-132(%rbp), %edx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L264
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, -60(%rbp)
	movl	%edx, %r9d
	pushq	$0
	movl	$15, %edx
	movl	$17, %esi
	movq	%r12, %rdi
	pushq	$1
	pushq	$3
	movq	%r8, -68(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L234:
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L236
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r8d
	movl	%esi, %edx
	movl	$40, %esi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L239:
	movq	%rax, %rdx
	sarq	$35, %rax
	shrq	$5, %rdx
	movq	%rax, %r10
.L240:
	movl	_ZN2v88internal11CpuFeatures10supported_E(%rip), %eax
	movq	-116(%rbp), %r8
	shrl	$5, %eax
	andl	$1, %eax
	cmpb	$14, %dl
	movl	-108(%rbp), %edx
	je	.L242
	testb	%al, %al
	je	.L243
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, -60(%rbp)
	movl	%edx, %r9d
	pushq	$0
	movl	%r10d, %edx
	movl	$17, %esi
	pushq	$1
	pushq	$3
	movq	%r8, -68(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	200(%rbx), %rdi
	movl	%r12d, %esi
	cmpl	$2, %ecx
	je	.L378
	call	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEm@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L248:
	movq	0(%r13), %rsi
	shrq	$5, %rax
	movl	%eax, %ecx
	sarq	$35, %rsi
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L258:
	shrq	$5, %rax
	movl	%eax, %ecx
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L236:
	call	_ZN2v88internal9Assembler6movapdENS0_11XMMRegisterES2_@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L261:
	movl	%r9d, -72(%rbp)
	testb	%dl, %dl
	je	.L266
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$15, %edx
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%r8, -68(%rbp)
	movl	%r9d, -60(%rbp)
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
.L267:
	movq	-140(%rbp), %r8
	movl	-132(%rbp), %edx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L268
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, -60(%rbp)
	movl	%edx, %r9d
	movq	%r12, %rdi
	movl	$15, %edx
	movl	$17, %esi
	movq	%r8, -68(%rbp)
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L374:
	movq	-104(%rbp), %rsi
	movl	-96(%rbp), %edx
	movl	%r15d, %ecx
	movl	$4, %r8d
	movabsq	$81604378624, %r15
	movq	%r13, %rdi
	orq	%r15, %rcx
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L251:
	movl	%r9d, -72(%rbp)
	testb	%dl, %dl
	je	.L254
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movl	$16, %esi
	movq	%r8, -68(%rbp)
	movl	%r9d, -60(%rbp)
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$15, %ecx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L262:
	movl	-84(%rbp), %ecx
	movq	%r8, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L252:
	movl	-84(%rbp), %ecx
	movq	%r8, %rdx
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L243:
	movl	%r10d, %ecx
	movq	%r8, %rsi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L254:
	movl	-72(%rbp), %ecx
	movq	%r8, %rdx
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L266:
	movl	-72(%rbp), %ecx
	movq	%r8, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L267
.L283:
	leaq	-240(%rbp), %rdi
	call	_ZNK2v88internal8compiler8Constant12ToHeapObjectEv@PLT
	movq	%rbx, %rdi
	leaq	-224(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeGenerator24IsMaterializableFromRootENS0_6HandleINS0_10HeapObjectEEEPNS0_9RootIndexE@PLT
	leaq	200(%rbx), %rdi
	testb	%al, %al
	je	.L293
.L368:
	movzwl	-224(%rbp), %edx
	movl	%r12d, %esi
	call	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE@PLT
	jmp	.L225
.L284:
	leaq	-240(%rbp), %rdi
	call	_ZNK2v88internal8compiler8Constant12ToHeapObjectEv@PLT
	movq	%rbx, %rdi
	leaq	-224(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeGenerator24IsMaterializableFromRootENS0_6HandleINS0_10HeapObjectEEEPNS0_9RootIndexE@PLT
	leaq	200(%rbx), %rdi
	testb	%al, %al
	jne	.L368
	movl	$2, %ecx
	movq	%r13, %rdx
	movl	%r12d, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE@PLT
	jmp	.L225
.L285:
	movq	%rdx, %rdi
	call	_ZN2v88internal17ExternalReference14FromRawAddressEm@PLT
	leaq	200(%rbx), %rdi
	movl	%r12d, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	jmp	.L225
.L286:
	movsd	-232(%rbp), %xmm0
	leaq	200(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal14TurboAssembler10MoveNumberENS0_8RegisterEd@PLT
	jmp	.L225
.L287:
	movl	%edx, -244(%rbp)
	pxor	%xmm0, %xmm0
	leaq	200(%rbx), %rdi
	movl	%r12d, %esi
	cvtss2sd	-244(%rbp), %xmm0
	call	_ZN2v88internal14TurboAssembler10MoveNumberENS0_8RegisterEd@PLT
	jmp	.L225
.L288:
	leaq	200(%rbx), %r9
	cmpb	$4, %dil
	je	.L367
	movl	%r12d, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	jmp	.L225
.L289:
	leaq	200(%rbx), %r9
	movl	%edx, %eax
	cmpb	$4, %dil
	je	.L379
	testl	%edx, %edx
	jne	.L291
	movl	$4, %r8d
	movl	%r12d, %ecx
	movl	%r12d, %edx
	movq	%r9, %rdi
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L225
.L281:
	leaq	-240(%rbp), %rdi
	call	_ZNK2v88internal8compiler8Constant23ToDelayedStringConstantEv@PLT
	leaq	200(%rbx), %rdi
	movl	$3, %ecx
	movl	%r12d, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler18MoveStringConstantENS0_8RegisterEPKNS0_18StringConstantBaseENS0_9RelocInfo4ModeE@PLT
	jmp	.L225
.L226:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L242:
	testb	%al, %al
	je	.L245
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, -60(%rbp)
	movl	%edx, %r9d
	movl	$17, %esi
	movl	%r10d, %edx
	movq	%r8, -68(%rbp)
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L378:
	call	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEj@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L371:
	sarq	$32, %rsi
	andl	$1, %edi
	jne	.L300
	leaq	-224(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-224(%rbp), %r12d
	movzbl	-220(%rbp), %r14d
	movq	-216(%rbp), %r15
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L369:
	sarq	$32, %rsi
	andl	$1, %edi
	jne	.L271
	leaq	-224(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-224(%rbp), %ecx
	movzbl	-220(%rbp), %edi
	movq	-216(%rbp), %rdx
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L373:
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L309
	movq	%rsi, -80(%rbp)
	movl	%edx, -72(%rbp)
	cmpb	$4, %r14b
	je	.L310
	testl	%r12d, %r12d
	je	.L311
	cmpl	$1, %r12d
	je	.L312
	movl	%r12d, -224(%rbp)
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movb	%r14b, -220(%rbp)
	movq	%r15, -216(%rbp)
	cmpl	$8, %r12d
	ja	.L350
	leaq	.L334(%rip), %rdx
	movslq	(%rdx,%r12,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_
	.align 4
	.align 4
.L334:
	.long	.L350-.L334
	.long	.L350-.L334
	.long	.L319-.L334
	.long	.L320-.L334
	.long	.L321-.L334
	.long	.L325-.L334
	.long	.L322-.L334
	.long	.L226-.L334
	.long	.L328-.L334
	.section	.text._ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_
.L322:
	leaq	-224(%rbp), %rdi
	leaq	200(%rbx), %r12
	call	_ZNK2v88internal8compiler8Constant12ToHeapObjectEv@PLT
	leaq	-240(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeGenerator24IsMaterializableFromRootENS0_6HandleINS0_10HeapObjectEEEPNS0_9RootIndexE@PLT
	testb	%al, %al
	je	.L323
.L365:
	movzwl	-240(%rbp), %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE@PLT
.L316:
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movq	%rsi, -68(%rbp)
.L366:
	movl	$8, %r8d
	movl	%r13d, %ecx
	movq	%r12, %rdi
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L225
.L325:
	leaq	-224(%rbp), %rdi
	leaq	200(%rbx), %r12
	call	_ZNK2v88internal8compiler8Constant12ToHeapObjectEv@PLT
	leaq	-240(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeGenerator24IsMaterializableFromRootENS0_6HandleINS0_10HeapObjectEEEPNS0_9RootIndexE@PLT
	testb	%al, %al
	jne	.L365
	movl	$2, %ecx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE@PLT
	jmp	.L316
.L320:
	leaq	200(%rbx), %r12
	movsd	-216(%rbp), %xmm0
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler10MoveNumberENS0_8RegisterEd@PLT
	jmp	.L316
.L319:
	movl	%r15d, -244(%rbp)
	leaq	200(%rbx), %r12
	pxor	%xmm0, %xmm0
	movl	%r13d, %esi
	movq	%r12, %rdi
	cvtss2sd	-244(%rbp), %xmm0
	call	_ZN2v88internal14TurboAssembler10MoveNumberENS0_8RegisterEd@PLT
	jmp	.L316
.L321:
	movq	%r15, %rdi
	leaq	200(%rbx), %r12
	call	_ZN2v88internal17ExternalReference14FromRawAddressEm@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	jmp	.L316
.L328:
	leaq	-224(%rbp), %rdi
	leaq	200(%rbx), %r12
	call	_ZNK2v88internal8compiler8Constant23ToDelayedStringConstantEv@PLT
	movl	$3, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler18MoveStringConstantENS0_8RegisterEPKNS0_18StringConstantBaseENS0_9RelocInfo4ModeE@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L377:
	cmpb	$11, %sil
	ja	.L259
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movq	-152(%rbp), %rdx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	-144(%rbp), %ecx
	movl	%r13d, %esi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-140(%rbp), %rsi
	movl	-132(%rbp), %edx
	movq	%rsi, -68(%rbp)
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L375:
	cmpb	$11, %r8b
	ja	.L249
	movl	-60(%rbp), %ecx
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L268:
	movl	$15, %ecx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6movupsENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L245:
	movl	%r10d, %ecx
	movq	%r8, %rsi
	call	_ZN2v88internal9Assembler6movupsENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L271:
	salq	$4, %rsi
	addq	152(%rax), %rsi
	movl	(%rsi), %ecx
	movzbl	4(%rsi), %edi
	movq	8(%rsi), %rdx
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L300:
	salq	$4, %rsi
	addq	152(%rax), %rsi
	movl	(%rsi), %r12d
	movzbl	4(%rsi), %r14d
	movq	8(%rsi), %r15
	jmp	.L302
.L312:
	leaq	200(%rbx), %rdi
	movq	%r15, %rcx
	call	_ZN2v88internal14TurboAssembler3SetENS0_7OperandEl@PLT
	jmp	.L225
.L311:
	movl	%r15d, %ecx
	leaq	200(%rbx), %rdi
	movl	$8, %r8d
	movabsq	$81604378624, %r15
	orq	%r15, %rcx
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L225
.L310:
	movl	%r12d, -224(%rbp)
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movb	$4, -220(%rbp)
	movq	%r15, -216(%rbp)
	cmpl	$8, %r12d
	ja	.L350
	leaq	.L333(%rip), %rdx
	movslq	(%rdx,%r12,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_
	.align 4
	.align 4
.L333:
	.long	.L315-.L333
	.long	.L317-.L333
	.long	.L319-.L333
	.long	.L320-.L333
	.long	.L321-.L333
	.long	.L325-.L333
	.long	.L322-.L333
	.long	.L226-.L333
	.long	.L328-.L333
	.section	.text._ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_
.L317:
	leaq	200(%rbx), %r12
	movq	%r15, %rdx
	movl	$4, %ecx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	jmp	.L316
.L315:
	leaq	200(%rbx), %r12
	movslq	%r15d, %rdx
	movl	$4, %ecx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	jmp	.L316
.L379:
	movslq	%edx, %rdx
.L367:
	movl	$8, %r8d
	movl	$4, %ecx
	movl	%r12d, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	jmp	.L225
.L293:
	movl	$3, %ecx
	movq	%r13, %rdx
	movl	%r12d, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE@PLT
	jmp	.L225
.L291:
	movl	$19, %edx
	movl	$4, %ecx
	movl	%r12d, %esi
	movq	%r9, %rdi
	salq	$32, %rdx
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L225
.L376:
	call	__stack_chk_fail@PLT
.L323:
	movl	$3, %ecx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE@PLT
	jmp	.L316
.L350:
	leaq	200(%rbx), %r12
	jmp	.L316
	.cfi_endproc
.LFE26298:
	.size	_ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_, .-_ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_
	.section	.text._ZN2v88internal8compiler27InstructionOperandConverter9InputCodeEm,"axG",@progbits,_ZN2v88internal8compiler27InstructionOperandConverter9InputCodeEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler27InstructionOperandConverter9InputCodeEm
	.type	_ZN2v88internal8compiler27InstructionOperandConverter9InputCodeEm, @function
_ZN2v88internal8compiler27InstructionOperandConverter9InputCodeEm:
.LFB15019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movzbl	4(%rax), %edx
	leaq	5(%rdx,%rsi), %rdx
	movq	(%rax,%rdx,8), %rsi
	movq	(%rdi), %rax
	movl	%esi, %edx
	movq	%rsi, %rdi
	movq	40(%rax), %rax
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L396
	leaq	104(%rax), %rdx
	movq	112(%rax), %rax
	movl	%edi, %esi
	testq	%rax, %rax
	je	.L385
	movq	%rdx, %rcx
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L387
.L386:
	cmpl	32(%rax), %esi
	jle	.L397
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L386
.L387:
	cmpq	%rcx, %rdx
	je	.L385
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rdx
.L385:
	movl	40(%rdx), %r8d
	movzbl	44(%rdx), %ecx
	leaq	-32(%rbp), %rdi
	movq	48(%rdx), %rax
.L384:
	movb	%cl, -28(%rbp)
	movl	%r8d, -32(%rbp)
	movq	%rax, -24(%rbp)
	call	_ZNK2v88internal8compiler8Constant6ToCodeEv@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L398
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	sarq	$32, %rsi
	andl	$1, %edi
	je	.L399
	salq	$4, %rsi
	addq	152(%rax), %rsi
	leaq	-32(%rbp), %rdi
	movl	(%rsi), %r8d
	movzbl	4(%rsi), %ecx
	movq	8(%rsi), %rax
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L399:
	leaq	-32(%rbp), %rdi
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-32(%rbp), %r8d
	movzbl	-28(%rbp), %ecx
	movq	-24(%rbp), %rax
	movq	-40(%rbp), %rdi
	jmp	.L384
.L398:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15019:
	.size	_ZN2v88internal8compiler27InstructionOperandConverter9InputCodeEm, .-_ZN2v88internal8compiler27InstructionOperandConverter9InputCodeEm
	.section	.text._ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE,"axG",@progbits,_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	.type	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE, @function
_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE:
.LFB15034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	%esi, %edx
	movq	%rsi, %rdi
	andl	$7, %edx
	movq	40(%rax), %rax
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L416
	leaq	104(%rax), %rsi
	movq	112(%rax), %rax
	movl	%edi, %edx
	testq	%rax, %rax
	je	.L405
	movq	%rsi, %rcx
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L417:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L407
.L406:
	cmpl	%edx, 32(%rax)
	jge	.L417
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L406
.L407:
	cmpq	%rcx, %rsi
	je	.L405
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L405:
	movzbl	44(%rsi), %eax
	salq	$32, %rax
	movq	%rax, %rdx
	movl	40(%rsi), %eax
	orq	%rdx, %rax
	movq	48(%rsi), %rdx
.L404:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L418
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	sarq	$32, %rsi
	andl	$1, %edi
	je	.L419
	salq	$4, %rsi
	addq	152(%rax), %rsi
	movq	%rsi, %rax
	movl	(%rsi), %ecx
	movzbl	4(%rsi), %esi
	movq	8(%rax), %rdx
.L403:
	movzbl	%sil, %eax
	salq	$32, %rax
	movq	%rax, %rsi
	movl	%ecx, %eax
	orq	%rsi, %rax
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L419:
	leaq	-32(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-32(%rbp), %ecx
	movzbl	-28(%rbp), %esi
	movq	-24(%rbp), %rdx
	jmp	.L403
.L418:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15034:
	.size	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE, .-_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	.section	.text._ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi,"axG",@progbits,_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	.type	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi, @function
_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi:
.LFB26205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edx, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movzbl	4(%rax), %edx
	leaq	5(%rdx,%rsi), %rdx
	movq	(%rax,%rdx,8), %rsi
	movq	(%rdi), %rax
	movq	24(%rax), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-48(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	addl	%ebx, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-48(%rbp), %rax
	movl	-40(%rbp), %edx
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L425
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L425:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26205:
	.size	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi, .-_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	.section	.text._ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE,"axG",@progbits,_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	.type	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE, @function
_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE:
.LFB26207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	%esi, %edx
	movq	%rsi, %rdi
	andl	$7, %edx
	movq	40(%rax), %rax
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L445
	leaq	104(%rax), %rdx
	movq	112(%rax), %rax
	movl	%edi, %esi
	testq	%rax, %rax
	je	.L431
	movq	%rdx, %rcx
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L446:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L433
.L432:
	cmpl	32(%rax), %esi
	jle	.L446
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L432
.L433:
	cmpq	%rcx, %rdx
	je	.L431
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rdx
.L431:
	movl	40(%rdx), %eax
	movzbl	44(%rdx), %ecx
	movq	48(%rdx), %rdx
.L430:
	movabsq	$81604378624, %r8
	cmpl	$3, %eax
	je	.L437
	movabsq	$81604378624, %rsi
	movl	%edx, %eax
	movl	%edx, %edx
	movq	%rax, %rdi
	btsq	$34, %rdx
	orq	%rsi, %rdi
	cmpb	$4, %cl
	cmovne	%rdi, %rdx
	movq	%rdx, %r8
.L437:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L447
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	sarq	$32, %rsi
	andl	$1, %edi
	je	.L448
	salq	$4, %rsi
	addq	152(%rax), %rsi
	movl	(%rsi), %eax
	movzbl	4(%rsi), %ecx
	movq	8(%rsi), %rdx
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L448:
	leaq	-32(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-32(%rbp), %eax
	movzbl	-28(%rbp), %ecx
	movq	-24(%rbp), %rdx
	jmp	.L430
.L447:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26207:
	.size	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE, .-_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	.section	.text._ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii,"axG",@progbits,_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii
	.type	_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii, @function
_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii:
.LFB26209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-48(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	addl	%ebx, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-48(%rbp), %rax
	movl	-40(%rbp), %edx
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L454
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L454:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26209:
	.size	_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii, .-_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii
	.section	.text._ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm,"axG",@progbits,_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	.type	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm, @function
_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm:
.LFB26212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movl	(%rax), %ecx
	shrl	$9, %ecx
	andl	$31, %ecx
	cmpl	$19, %ecx
	ja	.L456
	leaq	.L458(%rip), %r8
	movl	%ecx, %edx
	movslq	(%r8,%rdx,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm,"aG",@progbits,_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm,comdat
	.align 4
	.align 4
.L458:
	.long	.L456-.L458
	.long	.L461-.L458
	.long	.L464-.L458
	.long	.L463-.L458
	.long	.L463-.L458
	.long	.L463-.L458
	.long	.L463-.L458
	.long	.L462-.L458
	.long	.L462-.L458
	.long	.L462-.L458
	.long	.L462-.L458
	.long	.L461-.L458
	.long	.L456-.L458
	.long	.L460-.L458
	.long	.L460-.L458
	.long	.L459-.L458
	.long	.L459-.L458
	.long	.L459-.L458
	.long	.L459-.L458
	.long	.L457-.L458
	.section	.text._ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm,"axG",@progbits,_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm,comdat
	.p2align 4,,10
	.p2align 3
.L461:
	movq	(%rsi), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, (%rsi)
	movq	8(%rdi), %rax
	leaq	-48(%rbp), %rdi
	movzbl	4(%rax), %ecx
	leaq	5(%rdx,%rcx), %rdx
	movq	(%rax,%rdx,8), %rsi
	xorl	%edx, %edx
	sarq	$35, %rsi
.L522:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-48(%rbp), %rax
	movq	%rax, -36(%rbp)
	movl	-40(%rbp), %eax
	movl	%eax, -28(%rbp)
.L466:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	movq	-36(%rbp), %rax
	movl	-28(%rbp), %edx
	jne	.L524
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	movq	(%rsi), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, (%rsi)
	movq	8(%rdi), %rdx
	movzbl	4(%rdx), %r8d
	leaq	5(%rax,%r8), %r8
	movq	(%rdx,%r8,8), %rbx
	leal	-15(%rcx), %edx
	leaq	2(%rax), %rcx
	movq	%rcx, (%rsi)
	movq	8(%rdi), %rcx
	sarq	$35, %rbx
	movzbl	4(%rcx), %esi
	leaq	6(%rax,%rsi), %rax
	movq	(%rcx,%rax,8), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L525
	movq	(%rdi), %rcx
	shrq	$3, %rax
	movq	%rax, %rdi
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movq	112(%rsi), %rax
	leaq	104(%rsi), %r8
	testq	%rax, %rax
	je	.L489
	movq	%r8, %rsi
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L526:
	movq	%rax, %rsi
	movq	16(%rax), %rax
.L493:
	testq	%rax, %rax
	je	.L491
.L490:
	cmpl	32(%rax), %ecx
	jle	.L526
	movq	24(%rax), %rax
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L463:
	movq	(%rsi), %rax
	subl	$3, %ecx
	leaq	1(%rax), %rdx
	movq	%rdx, (%rsi)
	movq	8(%rdi), %rdx
	movzbl	4(%rdx), %r8d
	leaq	5(%rax,%r8), %r8
	movq	(%rdx,%r8,8), %r9
	leaq	2(%rax), %rdx
	xorl	%r8d, %r8d
	movq	%rdx, (%rsi)
	movq	8(%rdi), %rdx
	leaq	-48(%rbp), %rdi
	sarq	$35, %r9
	movzbl	4(%rdx), %esi
	leaq	6(%rax,%rsi), %rax
	movl	%r9d, %esi
	movq	(%rdx,%rax,8), %rdx
	sarq	$35, %rdx
.L523:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-48(%rbp), %rax
	movq	%rax, -36(%rbp)
	movl	-40(%rbp), %eax
	movl	%eax, -28(%rbp)
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L462:
	movq	(%rsi), %rax
	subl	$7, %ecx
	leaq	1(%rax), %rdx
	movq	%rdx, (%rsi)
	movq	8(%rdi), %rdx
	movzbl	4(%rdx), %r8d
	leaq	5(%rax,%r8), %r8
	movq	(%rdx,%r8,8), %rbx
	leaq	2(%rax), %rdx
	movq	%rdx, (%rsi)
	movq	8(%rdi), %rdx
	sarq	$35, %rbx
	movzbl	4(%rdx), %r8d
	leaq	6(%rax,%r8), %r8
	movq	(%rdx,%r8,8), %rdx
	leaq	3(%rax), %r8
	movq	%r8, (%rsi)
	movq	8(%rdi), %rsi
	sarq	$35, %rdx
	movzbl	4(%rsi), %r8d
	leaq	7(%rax,%r8), %rax
	movq	(%rsi,%rax,8), %rax
	movl	%eax, %esi
	andl	$7, %esi
	cmpl	$3, %esi
	je	.L527
	movq	(%rdi), %rsi
	shrq	$3, %rax
	movq	%rax, %r8
	movq	40(%rsi), %rdi
	movl	%eax, %esi
	movq	112(%rdi), %rax
	leaq	104(%rdi), %r9
	testq	%rax, %rax
	je	.L480
	movq	%r9, %rdi
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L528:
	movq	%rax, %rdi
	movq	16(%rax), %rax
.L484:
	testq	%rax, %rax
	je	.L482
.L481:
	cmpl	32(%rax), %esi
	jle	.L528
	movq	24(%rax), %rax
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L460:
	movq	(%rsi), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, (%rsi)
	movq	8(%rdi), %rax
	leaq	-48(%rbp), %rdi
	movzbl	4(%rax), %esi
	leaq	5(%rdx,%rsi), %rdx
	movq	(%rax,%rdx,8), %rsi
	leal	-11(%rcx), %edx
	xorl	%ecx, %ecx
	sarq	$35, %rsi
.L521:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterENS0_11ScaleFactorEi@PLT
	movq	-48(%rbp), %rax
	movq	%rax, -36(%rbp)
	movl	-40(%rbp), %eax
	movl	%eax, -28(%rbp)
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L457:
	movq	(%rsi), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, (%rsi)
	movq	8(%rdi), %rax
	movzbl	4(%rax), %ecx
	leaq	5(%rdx,%rcx), %rdx
	movq	(%rax,%rdx,8), %rax
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L529
	movq	(%rdi), %rdx
	shrq	$3, %rax
	movq	%rax, %rsi
	movq	40(%rdx), %rcx
	movl	%eax, %edx
	movq	112(%rcx), %rax
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L498
	movq	%rdi, %rcx
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L530:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L502:
	testq	%rax, %rax
	je	.L500
.L499:
	cmpl	32(%rax), %edx
	jle	.L530
	movq	24(%rax), %rax
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L464:
	movq	(%rsi), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, (%rsi)
	movq	8(%rdi), %rdx
	movzbl	4(%rdx), %ecx
	leaq	5(%rax,%rcx), %rcx
	movq	(%rdx,%rcx,8), %rbx
	leaq	2(%rax), %rdx
	movq	%rdx, (%rsi)
	movq	8(%rdi), %rdx
	sarq	$35, %rbx
	movzbl	4(%rdx), %ecx
	leaq	6(%rax,%rcx), %rax
	movq	(%rdx,%rax,8), %rax
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L531
	movq	(%rdi), %rdx
	shrq	$3, %rax
	movq	%rax, %rsi
	movq	40(%rdx), %rcx
	movl	%eax, %edx
	movq	112(%rcx), %rax
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L471
	movq	%rdi, %rcx
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L532:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L475:
	testq	%rax, %rax
	je	.L473
.L472:
	cmpl	32(%rax), %edx
	jle	.L532
	movq	24(%rax), %rax
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L473:
	cmpq	%rcx, %rdi
	je	.L471
	cmpl	%esi, 32(%rcx)
	cmovle	%rcx, %rdi
.L471:
	movq	48(%rdi), %rdx
.L470:
	leaq	-36(%rbp), %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-36(%rbp), %rax
	movq	%rax, -48(%rbp)
	movl	-28(%rbp), %eax
	movl	%eax, -40(%rbp)
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L500:
	cmpq	%rcx, %rdi
	je	.L498
	cmpl	%esi, 32(%rcx)
	cmovle	%rcx, %rdi
.L498:
	movq	48(%rdi), %rdx
.L497:
	leaq	-48(%rbp), %rdi
	movl	$13, %esi
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L482:
	cmpq	%rdi, %r9
	je	.L480
	cmpl	%r8d, 32(%rdi)
	cmovle	%rdi, %r9
.L480:
	movq	48(%r9), %r8
.L479:
	leaq	-48(%rbp), %rdi
	movl	%ebx, %esi
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L491:
	cmpq	%rsi, %r8
	je	.L489
	cmpl	%edi, 32(%rsi)
	cmovle	%rsi, %r8
.L489:
	movq	48(%r8), %rcx
.L488:
	leaq	-48(%rbp), %rdi
	movl	%ebx, %esi
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L456:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L531:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	jne	.L468
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-56(%rbp), %rdx
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	jne	.L495
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-56(%rbp), %rdx
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L525:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	jne	.L486
	leaq	-64(%rbp), %rdi
	movl	%edx, -84(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-56(%rbp), %rcx
	movl	-84(%rbp), %edx
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L527:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	jne	.L477
	leaq	-64(%rbp), %rdi
	movq	%rdx, -96(%rbp)
	movl	%ecx, -84(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-56(%rbp), %r8
	movl	-84(%rbp), %ecx
	movq	-96(%rbp), %rdx
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L495:
	movq	(%rdi), %rax
	salq	$4, %rsi
	movq	40(%rax), %rax
	addq	152(%rax), %rsi
	movq	8(%rsi), %rdx
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L468:
	movq	(%rdi), %rax
	salq	$4, %rsi
	movq	40(%rax), %rax
	addq	152(%rax), %rsi
	movq	8(%rsi), %rdx
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L486:
	movq	(%rdi), %rax
	salq	$4, %rsi
	movq	40(%rax), %rax
	addq	152(%rax), %rsi
	movq	8(%rsi), %rcx
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L477:
	movq	(%rdi), %rax
	salq	$4, %rsi
	movq	40(%rax), %rax
	addq	152(%rax), %rsi
	movq	8(%rsi), %r8
	jmp	.L479
.L524:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26212:
	.size	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm, .-_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	.section	.text._ZN2v88internal8compiler13CodeGenerator24AssembleDeconstructFrameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator24AssembleDeconstructFrameEv
	.type	_ZN2v88internal8compiler13CodeGenerator24AssembleDeconstructFrameEv, @function
_ZN2v88internal8compiler13CodeGenerator24AssembleDeconstructFrameEv:
.LFB26250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	232(%rdi), %rsi
	leaq	48(%rdi), %rdi
	subq	168(%rdi), %rsi
	addq	$200, %r12
	call	_ZN2v88internal8compiler19UnwindingInfoWriter22MarkFrameDeconstructedEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movq	%r12, %rdi
	movl	$8, %ecx
	movl	%r13d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	.cfi_endproc
.LFE26250:
	.size	_ZN2v88internal8compiler13CodeGenerator24AssembleDeconstructFrameEv, .-_ZN2v88internal8compiler13CodeGenerator24AssembleDeconstructFrameEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator23AssemblePrepareTailCallEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator23AssemblePrepareTailCallEv
	.type	_ZN2v88internal8compiler13CodeGenerator23AssemblePrepareTailCallEv, @function
_ZN2v88internal8compiler13CodeGenerator23AssemblePrepareTailCallEv:
.LFB26251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	cmpb	$0, 16(%rax)
	jne	.L539
.L536:
	movb	$0, 8(%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L540
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	.cfi_restore_state
	movl	_ZN2v88internalL3rbpE(%rip), %r12d
	movq	%rdi, %rbx
	xorl	%edx, %edx
	leaq	-48(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movl	%r12d, %esi
	leaq	200(%rbx), %rdi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	24(%rbx), %rax
	jmp	.L536
.L540:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26251:
	.size	_ZN2v88internal8compiler13CodeGenerator23AssemblePrepareTailCallEv, .-_ZN2v88internal8compiler13CodeGenerator23AssemblePrepareTailCallEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator32AssemblePopArgumentsAdaptorFrameENS0_8RegisterES3_S3_S3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator32AssemblePopArgumentsAdaptorFrameENS0_8RegisterES3_S3_S3_
	.type	_ZN2v88internal8compiler13CodeGenerator32AssemblePopArgumentsAdaptorFrameENS0_8RegisterES3_S3_S3_, @function
_ZN2v88internal8compiler13CodeGenerator32AssemblePopArgumentsAdaptorFrameENS0_8RegisterES3_S3_S3_:
.LFB26252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	movl	$-8, %edx
	pushq	%r12
	.cfi_offset 12, -48
	leaq	200(%rdi), %r12
	leaq	-92(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$88, %rsp
	movl	%esi, -124(%rbp)
	movl	_ZN2v88internalL3rbpE(%rip), %r15d
	movl	%r15d, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -108(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rdx
	movl	-84(%rbp), %ecx
	movq	%r12, %rdi
	movabsq	$81604378662, %r8
	movl	$8, %r9d
	movl	$7, %esi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	leaq	-108(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$5, %esi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-68(%rbp), %rdi
	movl	$-24, %edx
	movl	%r15d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterENS0_7OperandE@PLT
	movl	-124(%rbp), %eax
	leaq	-100(%rbp), %rsi
	movl	%ebx, %r8d
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	%eax, -100(%rbp)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	call	_ZN2v88internal14TurboAssembler18PrepareForTailCallERKNS0_14ParameterCountENS0_8RegisterES5_S5_@PLT
	movq	-120(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L544
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L544:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26252:
	.size	_ZN2v88internal8compiler13CodeGenerator32AssemblePopArgumentsAdaptorFrameENS0_8RegisterES3_S3_S3_, .-_ZN2v88internal8compiler13CodeGenerator32AssemblePopArgumentsAdaptorFrameENS0_8RegisterES3_S3_S3_
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator25AssembleTailCallBeforeGapEPNS1_11InstructionEi.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unimplemented code"
	.section	.text._ZN2v88internal8compiler13CodeGenerator25AssembleTailCallBeforeGapEPNS1_11InstructionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator25AssembleTailCallBeforeGapEPNS1_11InstructionEi
	.type	_ZN2v88internal8compiler13CodeGenerator25AssembleTailCallBeforeGapEPNS1_11InstructionEi, @function
_ZN2v88internal8compiler13CodeGenerator25AssembleTailCallBeforeGapEPNS1_11InstructionEi:
.LFB26255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rsi, %rdi
	pushq	%r14
	movl	$7, %esi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$152, %rsp
	movl	%edx, -172(%rbp)
	leaq	-128(%rbp), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%r15), %rax
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeGenerator22GetPushCompatibleMovesEPNS1_11InstructionENS_4base5FlagsINS2_12PushTypeFlagEiEEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE@PLT
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %r10
	movq	%rax, -160(%rbp)
	cmpq	%rax, %r10
	je	.L546
	movq	-8(%rax), %rax
	movq	8(%rax), %rax
	sarq	$35, %rax
	addl	$1, %eax
	cmpl	%ebx, %eax
	je	.L567
.L546:
	movq	24(%r15), %r12
	movl	$-1, %eax
	cmpb	$0, 16(%r12)
	jne	.L568
.L556:
	movl	-172(%rbp), %ebx
	addl	12(%r12), %eax
	addl	$2, %eax
	subl	%eax, %ebx
	testl	%ebx, %ebx
	jle	.L545
	movl	$8, %r8d
	movl	$4, %edx
	movl	$5, %esi
	movabsq	$81604378624, %rax
	leal	0(,%rbx,8), %ecx
	leaq	200(%r15), %rdi
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	addl	%ebx, 12(%r12)
.L545:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L569
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L568:
	.cfi_restore_state
	movq	(%r12), %rax
	movl	4(%rax), %eax
	subl	$2, %eax
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L567:
	leaq	200(%r15), %rax
	movq	%r15, %r11
	movq	%r13, %r14
	movq	%r10, %r15
	movq	%rax, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L555:
	movq	(%r15), %r9
	movq	24(%r11), %rax
	movl	$-1, %edx
	movq	8(%r9), %rbx
	movq	(%r9), %r13
	sarq	$35, %rbx
	cmpb	$0, 16(%rax)
	je	.L547
	movq	(%rax), %rdx
	movl	4(%rdx), %edx
	subl	$2, %edx
.L547:
	addl	12(%rax), %edx
	addl	$2, %edx
	subl	%edx, %ebx
	testl	%ebx, %ebx
	jg	.L570
	jne	.L571
.L549:
	movl	%r13d, %eax
	andl	$7, %eax
	testb	$4, %r13b
	je	.L550
	movq	%r13, %rax
	shrq	$3, %rax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L572
	movq	%r11, -152(%rbp)
	movq	%r9, -144(%rbp)
	testl	%eax, %eax
	jne	.L552
	movq	%r13, %rax
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L573
.L552:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L550:
	movq	%r11, -152(%rbp)
	movq	%r9, -144(%rbp)
	cmpl	$3, %eax
	jne	.L552
	movabsq	$-4294967296, %rax
	shrq	$32, %r13
	movq	-136(%rbp), %rdi
	andq	%rax, %r12
	movabsq	$-1095216660481, %rax
	orq	%r13, %r12
	andq	%rax, %r12
	movabsq	$81604378624, %rax
	orq	%rax, %r12
	movq	%r12, %rsi
	call	_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE@PLT
	movq	-144(%rbp), %r9
	movq	-152(%rbp), %r11
.L554:
	movq	24(%r11), %rax
	addq	$8, %r15
	addl	$1, 12(%rax)
	movq	$0, 8(%r9)
	movq	$0, (%r9)
	cmpq	%r15, -160(%rbp)
	jne	.L555
	movq	%r11, %r15
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L570:
	movq	%rax, -152(%rbp)
	leal	0(,%rbx,8), %edx
	movl	$5, %esi
	movabsq	$-4294967296, %rax
	andq	%rax, %r14
	movq	-136(%rbp), %rdi
	movabsq	$-1095216660481, %rax
	movl	$8, %r8d
	orq	%rdx, %r14
	movl	$4, %edx
	movq	%r11, -168(%rbp)
	andq	%rax, %r14
	movq	%r9, -144(%rbp)
	movabsq	$81604378624, %rax
	orq	%rax, %r14
	movq	%r14, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-152(%rbp), %rax
	movq	-144(%rbp), %r9
	movq	-168(%rbp), %r11
	addl	%ebx, 12(%rax)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L571:
	movl	%ebx, %edx
	movq	%rax, -152(%rbp)
	movl	$8, %r8d
	xorl	%esi, %esi
	negl	%edx
	movq	-136(%rbp), %rdi
	movabsq	$-1095216660481, %rax
	movq	%r11, -168(%rbp)
	leal	0(,%rdx,8), %ecx
	movq	%r9, -144(%rbp)
	movabsq	$-4294967296, %rdx
	andq	-184(%rbp), %rdx
	orq	%rcx, %rdx
	andq	%rax, %rdx
	movabsq	$81604378624, %rax
	orq	%rax, %rdx
	movq	%rdx, -184(%rbp)
	movq	%rdx, %rcx
	movl	$4, %edx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %r11
	movq	-144(%rbp), %r9
	addl	%ebx, 12(%rax)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L572:
	movq	%r13, %rax
	movq	%r9, -144(%rbp)
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L552
	movq	24(%r11), %rdi
	movq	%r13, %rsi
	movq	%r11, -152(%rbp)
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rspE(%rip), %rcx
	leaq	-68(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rbpE(%rip), %rax
	cmovne	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	-136(%rbp), %rdi
	movq	%rsi, -80(%rbp)
	movl	%edx, -72(%rbp)
	movq	%rsi, -92(%rbp)
	movl	%edx, -84(%rbp)
	call	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE@PLT
	movq	-152(%rbp), %r11
	movq	-144(%rbp), %r9
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L573:
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movq	-144(%rbp), %r9
	movq	-152(%rbp), %r11
	jmp	.L554
.L569:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26255:
	.size	_ZN2v88internal8compiler13CodeGenerator25AssembleTailCallBeforeGapEPNS1_11InstructionEi, .-_ZN2v88internal8compiler13CodeGenerator25AssembleTailCallBeforeGapEPNS1_11InstructionEi
	.section	.text._ZN2v88internal8compiler13CodeGenerator24AssembleTailCallAfterGapEPNS1_11InstructionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator24AssembleTailCallAfterGapEPNS1_11InstructionEi
	.type	_ZN2v88internal8compiler13CodeGenerator24AssembleTailCallAfterGapEPNS1_11InstructionEi, @function
_ZN2v88internal8compiler13CodeGenerator24AssembleTailCallAfterGapEPNS1_11InstructionEi:
.LFB26259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r12
	movl	%edx, %ebx
	cmpb	$0, 16(%r12)
	je	.L575
	movq	(%r12), %rax
	movl	4(%rax), %eax
	subl	$2, %eax
.L575:
	addl	12(%r12), %eax
	addl	$2, %eax
	subl	%eax, %ebx
	testl	%ebx, %ebx
	jg	.L584
	jne	.L585
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L584:
	.cfi_restore_state
	movl	$8, %r8d
	movl	$4, %edx
	movl	$5, %esi
	movabsq	$81604378624, %rax
	leal	0(,%rbx,8), %ecx
	addq	$200, %rdi
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	addl	%ebx, 12(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	.cfi_restore_state
	movl	%ebx, %ecx
	addq	$200, %rdi
	movl	$4, %edx
	xorl	%esi, %esi
	negl	%ecx
	movl	$8, %r8d
	movabsq	$81604378624, %rax
	sall	$3, %ecx
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	addl	%ebx, 12(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26259:
	.size	_ZN2v88internal8compiler13CodeGenerator24AssembleTailCallAfterGapEPNS1_11InstructionEi, .-_ZN2v88internal8compiler13CodeGenerator24AssembleTailCallAfterGapEPNS1_11InstructionEi
	.section	.text._ZN2v88internal8compiler13CodeGenerator30AssembleCodeStartRegisterCheckEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator30AssembleCodeStartRegisterCheckEv
	.type	_ZN2v88internal8compiler13CodeGenerator30AssembleCodeStartRegisterCheckEv, @function
_ZN2v88internal8compiler13CodeGenerator30AssembleCodeStartRegisterCheckEv:
.LFB26260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	200(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	call	_ZN2v88internal14TurboAssembler23ComputeCodeStartAddressENS0_8RegisterE@PLT
	movq	%r12, %rdi
	movl	$3, %edx
	movl	$59, %esi
	movl	$8, %r8d
	movl	$1, %ecx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$54, %edx
	popq	%r12
	movl	$4, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler6AssertENS0_9ConditionENS0_11AbortReasonE@PLT
	.cfi_endproc
.LFE26260:
	.size	_ZN2v88internal8compiler13CodeGenerator30AssembleCodeStartRegisterCheckEv, .-_ZN2v88internal8compiler13CodeGenerator30AssembleCodeStartRegisterCheckEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator20BailoutIfDeoptimizedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator20BailoutIfDeoptimizedEv
	.type	_ZN2v88internal8compiler13CodeGenerator20BailoutIfDeoptimizedEv, @function
_ZN2v88internal8compiler13CodeGenerator20BailoutIfDeoptimizedEv:
.LFB26261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-32, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	200(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-52(%rbp), %rdi
	subq	$56, %rsp
	movl	_ZN2v88internalL32kJavaScriptCallCodeStartRegisterE(%rip), %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-44(%rbp), %ecx
	movq	-52(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r13d
	movl	%r13d, %esi
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	leaq	-64(%rbp), %rdi
	movl	$15, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$81604378625, %rcx
	movl	$4, %r8d
	movl	%edx, -44(%rbp)
	movq	%rsi, -52(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_9ImmediateEi@PLT
	movq	16(%rbx), %rdi
	movl	$68, %esi
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$5, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L591
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L591:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26261:
	.size	_ZN2v88internal8compiler13CodeGenerator20BailoutIfDeoptimizedEv, .-_ZN2v88internal8compiler13CodeGenerator20BailoutIfDeoptimizedEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator46GenerateSpeculationPoisonFromCodeStartRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator46GenerateSpeculationPoisonFromCodeStartRegisterEv
	.type	_ZN2v88internal8compiler13CodeGenerator46GenerateSpeculationPoisonFromCodeStartRegisterEv, @function
_ZN2v88internal8compiler13CodeGenerator46GenerateSpeculationPoisonFromCodeStartRegisterEv:
.LFB26262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	_ZN2v88internalL3rbxE(%rip), %r13d
	pushq	%r12
	.cfi_offset 12, -32
	leaq	200(%rdi), %r12
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler23ComputeCodeStartAddressENS0_8RegisterE@PLT
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	$12, %ecx
	movl	$12, %edx
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$3, %ecx
	movl	$1, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$8, %ecx
	movabsq	$85899345919, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r13d, %ecx
	movq	%r12, %rdi
	movl	$4, %esi
	movl	_ZN2v88internalL26kSpeculationPoisonRegisterE(%rip), %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5cmovqENS0_9ConditionENS0_8RegisterES3_@PLT
	.cfi_endproc
.LFE26262:
	.size	_ZN2v88internal8compiler13CodeGenerator46GenerateSpeculationPoisonFromCodeStartRegisterEv, .-_ZN2v88internal8compiler13CodeGenerator46GenerateSpeculationPoisonFromCodeStartRegisterEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator33AssembleRegisterArgumentPoisoningEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator33AssembleRegisterArgumentPoisoningEv
	.type	_ZN2v88internal8compiler13CodeGenerator33AssembleRegisterArgumentPoisoningEv, @function
_ZN2v88internal8compiler13CodeGenerator33AssembleRegisterArgumentPoisoningEv:
.LFB26263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$12, %ecx
	movl	$7, %edx
	movl	$35, %esi
	movl	$8, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	200(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$12, %ecx
	movl	$6, %edx
	movl	$35, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$8, %r8d
	popq	%r12
	movl	$12, %ecx
	movl	$4, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$35, %esi
	jmp	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	.cfi_endproc
.LFE26263:
	.size	_ZN2v88internal8compiler13CodeGenerator33AssembleRegisterArgumentPoisoningEv, .-_ZN2v88internal8compiler13CodeGenerator33AssembleRegisterArgumentPoisoningEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator18AssembleArchBranchEPNS1_11InstructionEPNS1_10BranchInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator18AssembleArchBranchEPNS1_11InstructionEPNS1_10BranchInfoE
	.type	_ZN2v88internal8compiler13CodeGenerator18AssembleArchBranchEPNS1_11InstructionEPNS1_10BranchInfoE, @function
_ZN2v88internal8compiler13CodeGenerator18AssembleArchBranchEPNS1_11InstructionEPNS1_10BranchInfoE:
.LFB26267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	200(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movzbl	24(%rdx), %r12d
	movl	(%rdx), %eax
	movq	8(%rdx), %r15
	movq	16(%rdx), %r13
	xorl	$1, %r12d
	movzbl	%r12b, %r12d
	cmpl	$18, %eax
	je	.L616
	cmpl	$19, %eax
	je	.L617
.L598:
	cmpl	$21, %eax
	ja	.L599
.L618:
	leaq	.L601(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator18AssembleArchBranchEPNS1_11InstructionEPNS1_10BranchInfoE,"a",@progbits
	.align 4
	.align 4
.L601:
	.long	.L604-.L601
	.long	.L603-.L601
	.long	.L614-.L601
	.long	.L611-.L601
	.long	.L610-.L601
	.long	.L609-.L601
	.long	.L608-.L601
	.long	.L607-.L601
	.long	.L606-.L601
	.long	.L605-.L601
	.long	.L599-.L601
	.long	.L599-.L601
	.long	.L599-.L601
	.long	.L599-.L601
	.long	.L599-.L601
	.long	.L599-.L601
	.long	.L599-.L601
	.long	.L599-.L601
	.long	.L604-.L601
	.long	.L603-.L601
	.long	.L602-.L601
	.long	.L600-.L601
	.section	.text._ZN2v88internal8compiler13CodeGenerator18AssembleArchBranchEPNS1_11InstructionEPNS1_10BranchInfoE
	.p2align 4,,10
	.p2align 3
.L616:
	movl	%r12d, %ecx
	movq	%r13, %rdx
	movl	$10, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	(%rbx), %eax
	cmpl	$21, %eax
	jbe	.L618
.L599:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L603:
	movl	$5, %esi
	.p2align 4,,10
	.p2align 3
.L612:
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	cmpb	$0, 24(%rbx)
	je	.L619
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L604:
	.cfi_restore_state
	movl	$4, %esi
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L610:
	movl	$14, %esi
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L609:
	movl	$15, %esi
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L608:
	movl	$2, %esi
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L607:
	movl	$3, %esi
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L611:
	movl	$13, %esi
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L606:
	movl	$6, %esi
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L605:
	movl	$7, %esi
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L602:
	xorl	%esi, %esi
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L600:
	movl	$1, %esi
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L614:
	movl	$12, %esi
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L619:
	addq	$8, %rsp
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	.p2align 4,,10
	.p2align 3
.L617:
	.cfi_restore_state
	movl	$1, %ecx
	movq	%r15, %rdx
	movl	$10, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	(%rbx), %eax
	jmp	.L598
	.cfi_endproc
.LFE26267:
	.size	_ZN2v88internal8compiler13CodeGenerator18AssembleArchBranchEPNS1_11InstructionEPNS1_10BranchInfoE, .-_ZN2v88internal8compiler13CodeGenerator18AssembleArchBranchEPNS1_11InstructionEPNS1_10BranchInfoE
	.section	.text._ZN2v88internal8compiler13CodeGenerator23AssembleBranchPoisoningENS1_14FlagsConditionEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator23AssembleBranchPoisoningENS1_14FlagsConditionEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler13CodeGenerator23AssembleBranchPoisoningENS1_14FlagsConditionEPNS1_11InstructionE, @function
_ZN2v88internal8compiler13CodeGenerator23AssembleBranchPoisoningENS1_14FlagsConditionEPNS1_11InstructionE:
.LFB26268:
	.cfi_startproc
	endbr64
	leal	-18(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L620
	movabsq	$81604378624, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	200(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	movq	%r12, %rdi
	xorl	$1, %ebx
	subq	$8, %rsp
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	cmpl	$21, %ebx
	ja	.L622
	leaq	.L624(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator23AssembleBranchPoisoningENS1_14FlagsConditionEPNS1_11InstructionE,"a",@progbits
	.align 4
	.align 4
.L624:
	.long	.L627-.L624
	.long	.L626-.L624
	.long	.L636-.L624
	.long	.L634-.L624
	.long	.L633-.L624
	.long	.L632-.L624
	.long	.L631-.L624
	.long	.L630-.L624
	.long	.L629-.L624
	.long	.L628-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L627-.L624
	.long	.L626-.L624
	.long	.L625-.L624
	.long	.L623-.L624
	.section	.text._ZN2v88internal8compiler13CodeGenerator23AssembleBranchPoisoningENS1_14FlagsConditionEPNS1_11InstructionE
	.p2align 4,,10
	.p2align 3
.L620:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$5, %esi
.L635:
	movl	_ZN2v88internalL26kSpeculationPoisonRegisterE(%rip), %edx
	addq	$8, %rsp
	movl	%r13d, %ecx
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5cmovqENS0_9ConditionENS0_8RegisterES3_@PLT
.L622:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L627:
	movl	$4, %esi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L633:
	movl	$14, %esi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L632:
	movl	$15, %esi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L634:
	movl	$13, %esi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L625:
	xorl	%esi, %esi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L623:
	movl	$1, %esi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L631:
	movl	$2, %esi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L630:
	movl	$3, %esi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L629:
	movl	$6, %esi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L628:
	movl	$7, %esi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L636:
	movl	$12, %esi
	jmp	.L635
	.cfi_endproc
.LFE26268:
	.size	_ZN2v88internal8compiler13CodeGenerator23AssembleBranchPoisoningENS1_14FlagsConditionEPNS1_11InstructionE, .-_ZN2v88internal8compiler13CodeGenerator23AssembleBranchPoisoningENS1_14FlagsConditionEPNS1_11InstructionE
	.section	.text._ZN2v88internal8compiler13CodeGenerator23AssembleArchDeoptBranchEPNS1_11InstructionEPNS1_10BranchInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator23AssembleArchDeoptBranchEPNS1_11InstructionEPNS1_10BranchInfoE
	.type	_ZN2v88internal8compiler13CodeGenerator23AssembleArchDeoptBranchEPNS1_11InstructionEPNS1_10BranchInfoE, @function
_ZN2v88internal8compiler13CodeGenerator23AssembleArchDeoptBranchEPNS1_11InstructionEPNS1_10BranchInfoE:
.LFB26269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	200(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movzbl	24(%rdx), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	xorl	$1, %r13d
	movzbl	%r13b, %eax
	movq	8(%rdx), %r13
	movl	%eax, -68(%rbp)
	movq	16(%rdx), %rax
	movq	%rax, -80(%rbp)
	movl	(%rdx), %eax
	cmpl	$18, %eax
	je	.L662
	cmpl	$19, %eax
	je	.L663
.L642:
	cmpl	$21, %eax
	ja	.L643
.L664:
	leaq	.L645(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator23AssembleArchDeoptBranchEPNS1_11InstructionEPNS1_10BranchInfoE,"a",@progbits
	.align 4
	.align 4
.L645:
	.long	.L648-.L645
	.long	.L647-.L645
	.long	.L660-.L645
	.long	.L655-.L645
	.long	.L654-.L645
	.long	.L653-.L645
	.long	.L652-.L645
	.long	.L651-.L645
	.long	.L650-.L645
	.long	.L649-.L645
	.long	.L643-.L645
	.long	.L643-.L645
	.long	.L643-.L645
	.long	.L643-.L645
	.long	.L643-.L645
	.long	.L643-.L645
	.long	.L643-.L645
	.long	.L643-.L645
	.long	.L648-.L645
	.long	.L647-.L645
	.long	.L646-.L645
	.long	.L644-.L645
	.section	.text._ZN2v88internal8compiler13CodeGenerator23AssembleArchDeoptBranchEPNS1_11InstructionEPNS1_10BranchInfoE
	.p2align 4,,10
	.p2align 3
.L662:
	movl	-68(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	(%rbx), %eax
	cmpl	$21, %eax
	jbe	.L664
.L643:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L647:
	movl	$5, %esi
	.p2align 4,,10
	.p2align 3
.L656:
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internal24FLAG_deopt_every_n_timesE(%rip), %eax
	testl	%eax, %eax
	jle	.L657
	movq	16(%r14), %rdi
	call	_ZN2v88internal17ExternalReference18stress_deopt_countEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal9Assembler6pushfqEv@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r14d
	movq	%r12, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8load_raxENS0_17ExternalReferenceE@PLT
	movl	$4, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_decENS0_8RegisterEi@PLT
	leaq	-64(%rbp), %r10
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r10, %rdx
	movl	$5, %esi
	movq	%r10, -88(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movslq	_ZN2v88internal24FLAG_deopt_every_n_timesE(%rip), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9store_raxENS0_17ExternalReferenceE@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5popfqEv@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-88(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9store_raxENS0_17ExternalReferenceE@PLT
	movq	%r12, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5popfqEv@PLT
.L657:
	cmpb	$0, 24(%rbx)
	je	.L665
.L640:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L666
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L648:
	.cfi_restore_state
	movl	$4, %esi
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L654:
	movl	$14, %esi
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L653:
	movl	$15, %esi
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L652:
	movl	$2, %esi
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L651:
	movl	$3, %esi
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L655:
	movl	$13, %esi
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L650:
	movl	$6, %esi
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L649:
	movl	$7, %esi
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L646:
	xorl	%esi, %esi
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L644:
	movl	$1, %esi
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L660:
	movl	$12, %esi
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L665:
	movl	-68(%rbp), %edx
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L663:
	movl	$1, %ecx
	movq	%r13, %rdx
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	(%rbx), %eax
	jmp	.L642
.L666:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26269:
	.size	_ZN2v88internal8compiler13CodeGenerator23AssembleArchDeoptBranchEPNS1_11InstructionEPNS1_10BranchInfoE, .-_ZN2v88internal8compiler13CodeGenerator23AssembleArchDeoptBranchEPNS1_11InstructionEPNS1_10BranchInfoE
	.section	.text._ZN2v88internal8compiler13CodeGenerator16AssembleArchJumpENS1_9RpoNumberE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator16AssembleArchJumpENS1_9RpoNumberE
	.type	_ZN2v88internal8compiler13CodeGenerator16AssembleArchJumpENS1_9RpoNumberE, @function
_ZN2v88internal8compiler13CodeGenerator16AssembleArchJumpENS1_9RpoNumberE:
.LFB26270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movslq	%esi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%r12d, %esi
	movq	%rdi, %rbx
	call	_ZNK2v88internal8compiler13CodeGenerator21IsNextInAssemblyOrderENS1_9RpoNumberE@PLT
	testb	%al, %al
	je	.L670
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L670:
	.cfi_restore_state
	movq	160(%rbx), %rax
	leaq	200(%rbx), %rdi
	movl	$1, %edx
	popq	%rbx
	leaq	(%rax,%r12,8), %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	.cfi_endproc
.LFE26270:
	.size	_ZN2v88internal8compiler13CodeGenerator16AssembleArchJumpENS1_9RpoNumberE, .-_ZN2v88internal8compiler13CodeGenerator16AssembleArchJumpENS1_9RpoNumberE
	.section	.text._ZN2v88internal8compiler13CodeGenerator16AssembleArchTrapEPNS1_11InstructionENS1_14FlagsConditionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator16AssembleArchTrapEPNS1_11InstructionENS1_14FlagsConditionE
	.type	_ZN2v88internal8compiler13CodeGenerator16AssembleArchTrapEPNS1_11InstructionENS1_14FlagsConditionE, @function
_ZN2v88internal8compiler13CodeGenerator16AssembleArchTrapEPNS1_11InstructionENS1_14FlagsConditionE:
.LFB26271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%rsi, -64(%rbp)
	movq	8(%rdi), %rdi
	movq	16(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$63, %rax
	jbe	.L693
	leaq	64(%r13), %rax
	movq	%rax, 16(%rdi)
.L673:
	movq	%rbx, %xmm0
	movq	%r13, %rdi
	movq	%rbx, %rsi
	addq	$8, %r13
	movhps	-64(%rbp), %xmm0
	leaq	200(%rbx), %r12
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE@PLT
	movdqa	-64(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapE(%rip), %rax
	movq	%rax, -8(%r13)
	movups	%xmm0, 40(%r13)
	movq	$0, -48(%rbp)
	cmpl	$18, %r14d
	je	.L694
	cmpl	$19, %r14d
	je	.L695
	cmpl	$21, %r14d
	ja	.L678
	leaq	.L680(%rip), %rdx
	movslq	(%rdx,%r14,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator16AssembleArchTrapEPNS1_11InstructionENS1_14FlagsConditionE,"a",@progbits
	.align 4
	.align 4
.L680:
	.long	.L689-.L680
	.long	.L677-.L680
	.long	.L688-.L680
	.long	.L691-.L680
	.long	.L687-.L680
	.long	.L686-.L680
	.long	.L685-.L680
	.long	.L684-.L680
	.long	.L683-.L680
	.long	.L682-.L680
	.long	.L678-.L680
	.long	.L678-.L680
	.long	.L678-.L680
	.long	.L678-.L680
	.long	.L678-.L680
	.long	.L678-.L680
	.long	.L678-.L680
	.long	.L678-.L680
	.long	.L678-.L680
	.long	.L678-.L680
	.long	.L681-.L680
	.long	.L679-.L680
	.section	.text._ZN2v88internal8compiler13CodeGenerator16AssembleArchTrapEPNS1_11InstructionENS1_14FlagsConditionE
.L691:
	movl	$13, %esi
	leaq	-48(%rbp), %r14
.L675:
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L696
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L688:
	.cfi_restore_state
	movl	$12, %esi
	leaq	-48(%rbp), %r14
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L694:
	leaq	-48(%rbp), %r14
	movl	$10, %esi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$4, %esi
	jmp	.L675
.L679:
	movl	$1, %esi
	leaq	-48(%rbp), %r14
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L695:
	movl	$1, %ecx
	movq	%r13, %rdx
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L677:
	movl	$5, %esi
	leaq	-48(%rbp), %r14
	jmp	.L675
.L687:
	movl	$14, %esi
	leaq	-48(%rbp), %r14
	jmp	.L675
.L686:
	movl	$15, %esi
	leaq	-48(%rbp), %r14
	jmp	.L675
.L685:
	movl	$2, %esi
	leaq	-48(%rbp), %r14
	jmp	.L675
.L684:
	movl	$3, %esi
	leaq	-48(%rbp), %r14
	jmp	.L675
.L683:
	movl	$6, %esi
	leaq	-48(%rbp), %r14
	jmp	.L675
.L682:
	movl	$7, %esi
	leaq	-48(%rbp), %r14
	jmp	.L675
.L681:
	xorl	%esi, %esi
	leaq	-48(%rbp), %r14
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L693:
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L673
.L678:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L689:
	movl	$4, %esi
	leaq	-48(%rbp), %r14
	jmp	.L675
.L696:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26271:
	.size	_ZN2v88internal8compiler13CodeGenerator16AssembleArchTrapEPNS1_11InstructionENS1_14FlagsConditionE, .-_ZN2v88internal8compiler13CodeGenerator16AssembleArchTrapEPNS1_11InstructionENS1_14FlagsConditionE
	.section	.text._ZN2v88internal8compiler13CodeGenerator19AssembleArchBooleanEPNS1_11InstructionENS1_14FlagsConditionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator19AssembleArchBooleanEPNS1_11InstructionENS1_14FlagsConditionE
	.type	_ZN2v88internal8compiler13CodeGenerator19AssembleArchBooleanEPNS1_11InstructionENS1_14FlagsConditionE, @function
_ZN2v88internal8compiler13CodeGenerator19AssembleArchBooleanEPNS1_11InstructionENS1_14FlagsConditionE:
.LFB26272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	200(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	4(%rsi), %eax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	32(%rsi,%rax,8), %r13
	sarq	$35, %r13
	cmpl	$18, %ebx
	je	.L718
	cmpl	$19, %ebx
	je	.L719
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	cmpl	$21, %ebx
	ja	.L702
	leaq	.L704(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator19AssembleArchBooleanEPNS1_11InstructionENS1_14FlagsConditionE,"a",@progbits
	.align 4
	.align 4
.L704:
	.long	.L713-.L704
	.long	.L716-.L704
	.long	.L712-.L704
	.long	.L715-.L704
	.long	.L711-.L704
	.long	.L710-.L704
	.long	.L709-.L704
	.long	.L708-.L704
	.long	.L707-.L704
	.long	.L706-.L704
	.long	.L702-.L704
	.long	.L702-.L704
	.long	.L702-.L704
	.long	.L702-.L704
	.long	.L702-.L704
	.long	.L702-.L704
	.long	.L702-.L704
	.long	.L702-.L704
	.long	.L702-.L704
	.long	.L702-.L704
	.long	.L705-.L704
	.long	.L703-.L704
	.section	.text._ZN2v88internal8compiler13CodeGenerator19AssembleArchBooleanEPNS1_11InstructionENS1_14FlagsConditionE
	.p2align 4,,10
	.p2align 3
.L718:
	leaq	-64(%rbp), %r15
	xorl	%ecx, %ecx
	movl	$11, %esi
	movq	%r12, %rdi
	movq	%r15, %rdx
	leaq	-72(%rbp), %r14
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$4, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movabsq	$81604378624, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$4, %esi
	.p2align 4,,10
	.p2align 3
.L699:
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5setccENS0_9ConditionENS0_8RegisterE@PLT
	movl	$4, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L720
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L716:
	.cfi_restore_state
	leaq	-72(%rbp), %r14
.L701:
	movl	$5, %esi
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L711:
	movl	$14, %esi
	leaq	-72(%rbp), %r14
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L710:
	movl	$15, %esi
	leaq	-72(%rbp), %r14
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L705:
	xorl	%esi, %esi
	leaq	-72(%rbp), %r14
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L703:
	movl	$1, %esi
	leaq	-72(%rbp), %r14
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L709:
	movl	$2, %esi
	leaq	-72(%rbp), %r14
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L708:
	movl	$3, %esi
	leaq	-72(%rbp), %r14
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L707:
	movl	$6, %esi
	leaq	-72(%rbp), %r14
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L706:
	movl	$7, %esi
	leaq	-72(%rbp), %r14
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L719:
	leaq	-64(%rbp), %r15
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$11, %esi
	movq	%r15, %rdx
	leaq	-72(%rbp), %r14
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$4, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movabsq	$81604378625, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L701
.L702:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L713:
	movl	$4, %esi
	leaq	-72(%rbp), %r14
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L715:
	movl	$13, %esi
	leaq	-72(%rbp), %r14
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L712:
	movl	$12, %esi
	leaq	-72(%rbp), %r14
	jmp	.L699
.L720:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26272:
	.size	_ZN2v88internal8compiler13CodeGenerator19AssembleArchBooleanEPNS1_11InstructionENS1_14FlagsConditionE, .-_ZN2v88internal8compiler13CodeGenerator19AssembleArchBooleanEPNS1_11InstructionENS1_14FlagsConditionE
	.section	.text._ZN2v88internal8compiler13CodeGenerator24AssembleArchLookupSwitchEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator24AssembleArchLookupSwitchEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler13CodeGenerator24AssembleArchLookupSwitchEPNS1_11InstructionE, @function
_ZN2v88internal8compiler13CodeGenerator24AssembleArchLookupSwitchEPNS1_11InstructionE:
.LFB26291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	4(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	%dl, %ecx
	shrl	$8, %edx
	movq	40(%rsi,%rcx,8), %rdi
	sarq	$35, %rdi
	movq	%rdi, -88(%rbp)
	cmpw	$2, %dx
	jbe	.L722
	leaq	-80(%rbp), %rdi
	movl	$2, %r13d
	leaq	200(%r14), %r12
	movl	%ecx, %edx
	movq	%rdi, -96(%rbp)
	movq	%r14, %rdi
	movq	%r13, %r14
	movq	%rdi, %r13
	.p2align 4,,10
	.p2align 3
.L741:
	leaq	5(%r14,%rdx), %rax
	movq	40(%r13), %rcx
	movq	(%r15,%rax,8), %rax
	movl	%eax, %edx
	movq	%rax, %rsi
	andl	$7, %edx
	shrq	$3, %rsi
	cmpl	$3, %edx
	je	.L768
	movq	112(%rcx), %rax
	movl	%esi, %edi
	leaq	104(%rcx), %rdx
	testq	%rax, %rax
	je	.L727
	movq	%rdx, %rcx
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L769:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L729
.L728:
	cmpl	32(%rax), %edi
	jle	.L769
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L728
.L729:
	cmpq	%rcx, %rdx
	je	.L727
	cmpl	%esi, 32(%rcx)
	cmovle	%rcx, %rdx
.L727:
	movq	48(%rdx), %rax
.L726:
	movl	%eax, %eax
	movl	-88(%rbp), %edx
	movl	$4, %r8d
	movl	$7, %esi
	movabsq	$-4294967296, %rdi
	andq	%rdi, %rbx
	movq	%r12, %rdi
	orq	%rax, %rbx
	movabsq	$-1095216660481, %rax
	andq	%rax, %rbx
	movabsq	$81604378624, %rax
	orq	%rax, %rbx
	movq	%rbx, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movzbl	4(%r15), %eax
	leaq	6(%r14,%rax), %rax
	movq	(%r15,%rax,8), %rax
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L770
	movq	40(%r13), %rsi
	shrq	$3, %rax
	movq	%rax, %rdx
	movl	%eax, %edi
	movq	112(%rsi), %rax
	leaq	104(%rsi), %rcx
	testq	%rax, %rax
	je	.L736
	movq	%rcx, %rsi
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L771:
	movq	%rax, %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L738
.L737:
	cmpl	32(%rax), %edi
	jle	.L771
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L737
.L738:
	cmpq	%rsi, %rcx
	je	.L736
	cmpl	%edx, 32(%rsi)
	cmovle	%rsi, %rcx
.L736:
	movq	48(%rcx), %rax
.L735:
	movq	160(%r13), %rdx
	cltq
	movq	%r12, %rdi
	addq	$2, %r14
	movl	$1, %ecx
	movl	$4, %esi
	leaq	(%rdx,%rax,8), %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	4(%r15), %edx
	movl	%edx, %eax
	shrl	$8, %eax
	movzwl	%ax, %eax
	cmpq	%rax, %r14
	jnb	.L766
	movzbl	%dl, %edx
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L770:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	jne	.L733
	movq	-96(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-72(%rbp), %rax
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L768:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	jne	.L724
	movq	-96(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-72(%rbp), %rax
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L766:
	movq	%r13, %r14
	movzbl	%dl, %ecx
.L722:
	movq	48(%r15,%rcx,8), %rsi
	movq	40(%r14), %rax
	movl	%esi, %edx
	movq	%rsi, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L772
	leaq	104(%rax), %rsi
	movq	112(%rax), %rax
	movl	%edi, %edx
	testq	%rax, %rax
	je	.L746
	movq	%rsi, %rcx
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L773:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L748
.L747:
	cmpl	32(%rax), %edx
	jle	.L773
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L747
.L748:
	cmpq	%rcx, %rsi
	je	.L746
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L746:
	movq	48(%rsi), %rbx
.L745:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeGenerator21IsNextInAssemblyOrderENS1_9RpoNumberE@PLT
	testb	%al, %al
	je	.L774
.L721:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L775
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L724:
	.cfi_restore_state
	salq	$4, %rsi
	addq	152(%rcx), %rsi
	movq	8(%rsi), %rax
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L733:
	movq	40(%r13), %rax
	salq	$4, %rsi
	addq	152(%rax), %rsi
	movq	8(%rsi), %rax
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L774:
	movq	160(%r14), %rax
	movslq	%ebx, %rbx
	movl	$1, %edx
	leaq	200(%r14), %rdi
	leaq	(%rax,%rbx,8), %rsi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L772:
	sarq	$32, %rsi
	andl	$1, %edi
	je	.L776
	salq	$4, %rsi
	addq	152(%rax), %rsi
	movq	8(%rsi), %rbx
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L776:
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-72(%rbp), %rbx
	jmp	.L745
.L775:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26291:
	.size	_ZN2v88internal8compiler13CodeGenerator24AssembleArchLookupSwitchEPNS1_11InstructionE, .-_ZN2v88internal8compiler13CodeGenerator24AssembleArchLookupSwitchEPNS1_11InstructionE
	.section	.text._ZN2v88internal8compiler13CodeGenerator23AssembleArchTableSwitchEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator23AssembleArchTableSwitchEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler13CodeGenerator23AssembleArchTableSwitchEPNS1_11InstructionE, @function
_ZN2v88internal8compiler13CodeGenerator23AssembleArchTableSwitchEPNS1_11InstructionE:
.LFB26292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	movq	40(%rsi,%rdx,8), %r13
	movzwl	%ax, %ebx
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	leal	-2(%rbx), %r12d
	sarq	$35, %r13
	movq	%r13, -104(%rbp)
	movslq	%r12d, %r13
	subq	%rax, %rdx
	leaq	0(,%r13,8), %rsi
	cmpq	%rdx, %rsi
	ja	.L813
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L779:
	movq	%rax, %r9
	testl	%r12d, %r12d
	jle	.L792
	leal	-3(%rbx), %r10d
	leaq	-96(%rbp), %r8
	movl	$2, %ebx
	addq	$3, %r10
	movq	%r10, %rdi
	movl	%r12d, %r10d
	movq	%rax, %r12
	movq	%r13, %rax
	movq	%rdi, %r13
	.p2align 4,,10
	.p2align 3
.L793:
	movzbl	4(%r15), %edx
	movq	40(%r14), %rsi
	leaq	5(%rbx,%rdx), %rdx
	movq	(%r15,%rdx,8), %rdx
	movl	%edx, %ecx
	movq	%rdx, %rdi
	andl	$7, %ecx
	shrq	$3, %rdi
	cmpl	$3, %ecx
	je	.L814
	movq	112(%rsi), %rdx
	movl	%edi, %r11d
	leaq	104(%rsi), %rcx
	testq	%rdx, %rdx
	je	.L787
	movq	%rcx, %rsi
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L815:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L789
.L788:
	cmpl	32(%rdx), %r11d
	jle	.L815
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L788
.L789:
	cmpq	%rsi, %rcx
	je	.L787
	cmpl	%edi, 32(%rsi)
	cmovle	%rsi, %rcx
.L787:
	movq	48(%rcx), %rdx
.L786:
	movq	160(%r14), %rcx
	movslq	%edx, %rdx
	addq	$1, %rbx
	addq	$8, %r12
	leaq	(%rcx,%rdx,8), %rdx
	movq	%rdx, -8(%r12)
	cmpq	%rbx, %r13
	jne	.L793
	movq	%rax, %r13
	movl	%r10d, %r12d
.L792:
	movq	%r13, %rdx
	movq	%r9, %rsi
	leaq	200(%r14), %r13
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator12AddJumpTableEPPNS0_5LabelEm@PLT
	movl	-104(%rbp), %edx
	movl	%r12d, %ecx
	movl	$7, %esi
	movabsq	$81604378624, %r12
	movl	$4, %r8d
	movq	%r13, %rdi
	movq	%rax, %rbx
	orq	%r12, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movzbl	4(%r15), %eax
	movq	48(%r15,%rax,8), %rax
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L816
	movq	40(%r14), %rcx
	shrq	$3, %rax
	movq	%rax, %rsi
	movl	%eax, %edx
	movq	112(%rcx), %rax
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L797
	movq	%rdi, %rcx
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L817:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L799
.L798:
	cmpl	32(%rax), %edx
	jle	.L817
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L798
.L799:
	cmpq	%rcx, %rdi
	je	.L797
	cmpl	%esi, 32(%rcx)
	cmovle	%rcx, %rdi
.L797:
	movq	48(%rdi), %rax
.L796:
	cltq
	movl	$1, %ecx
	movl	$3, %esi
	movq	%r13, %rdi
	movq	160(%r14), %rdx
	leaq	(%rdx,%rax,8), %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%edx, %edx
	leaq	-80(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal7OperandC1EPNS0_5LabelEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r13, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r12d
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%r12d, %esi
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	-104(%rbp), %edx
	xorl	%r8d, %r8d
	leaq	-68(%rbp), %rdi
	movl	$3, %ecx
	movl	%r12d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler3jmpENS0_7OperandE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L818
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L814:
	.cfi_restore_state
	movq	%rdx, %rcx
	sarq	$32, %rcx
	andl	$8, %edx
	jne	.L784
	movq	%r8, %rdi
	movl	%ecx, %esi
	movq	%r9, -136(%rbp)
	movl	%r10d, -124(%rbp)
	movq	%rax, -120(%rbp)
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-88(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %rax
	movl	-124(%rbp), %r10d
	movq	-136(%rbp), %r9
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L784:
	salq	$4, %rcx
	addq	152(%rsi), %rcx
	movq	8(%rcx), %rdx
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L816:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	je	.L819
	movq	40(%r14), %rax
	salq	$4, %rsi
	addq	152(%rax), %rsi
	movq	8(%rsi), %rax
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L819:
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-88(%rbp), %rax
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L813:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L779
.L818:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26292:
	.size	_ZN2v88internal8compiler13CodeGenerator23AssembleArchTableSwitchEPNS1_11InstructionE, .-_ZN2v88internal8compiler13CodeGenerator23AssembleArchTableSwitchEPNS1_11InstructionE
	.globl	__popcountdi2
	.section	.text._ZN2v88internal8compiler13CodeGenerator11FinishFrameEPNS1_5FrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator11FinishFrameEPNS1_5FrameE
	.type	_ZN2v88internal8compiler13CodeGenerator11FinishFrameEPNS1_5FrameE, @function
_ZN2v88internal8compiler13CodeGenerator11FinishFrameEPNS1_5FrameE:
.LFB26293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	32(%rdi), %rax
	movq	(%rax), %rbx
	movl	56(%rbx), %edi
	testl	%edi, %edi
	je	.L821
	addl	$1, 8(%rsi)
	movl	4(%rsi), %r13d
	call	__popcountdi2@PLT
	leal	0(%r13,%rax,2), %eax
	movl	%eax, 4(%r12)
.L821:
	movl	52(%rbx), %edx
	testl	%edx, %edx
	je	.L820
	movl	%edx, %eax
	movl	%edx, %ecx
	shrl	$15, %eax
	andl	$16384, %ecx
	andl	$1, %eax
	cmpl	$1, %ecx
	movl	%edx, %ecx
	sbbl	$-1, %eax
	andl	$8192, %ecx
	cmpl	$1, %ecx
	movl	%edx, %ecx
	sbbl	$-1, %eax
	andl	$4096, %ecx
	cmpl	$1, %ecx
	movl	%edx, %ecx
	sbbl	$-1, %eax
	andl	$2048, %ecx
	cmpl	$1, %ecx
	movl	%edx, %ecx
	sbbl	$-1, %eax
	andl	$1024, %ecx
	cmpl	$1, %ecx
	movl	%edx, %ecx
	sbbl	$-1, %eax
	andl	$512, %ecx
	cmpl	$1, %ecx
	movl	%edx, %ecx
	sbbl	$-1, %eax
	andl	$256, %ecx
	cmpl	$1, %ecx
	movl	%edx, %ecx
	sbbl	$-1, %eax
	andl	$128, %ecx
	cmpl	$1, %ecx
	movl	%edx, %ecx
	sbbl	$-1, %eax
	andl	$64, %ecx
	cmpl	$1, %ecx
	movl	%edx, %ecx
	sbbl	$-1, %eax
	andl	$32, %ecx
	cmpl	$1, %ecx
	movl	%edx, %ecx
	sbbl	$-1, %eax
	andl	$16, %ecx
	cmpl	$1, %ecx
	movl	%edx, %ecx
	sbbl	$-1, %eax
	andl	$8, %ecx
	cmpl	$1, %ecx
	movl	%edx, %ecx
	sbbl	$-1, %eax
	andl	$4, %ecx
	cmpl	$1, %ecx
	movl	%edx, %ecx
	sbbl	$-1, %eax
	andl	$2, %ecx
	cmpl	$1, %ecx
	sbbl	$-1, %eax
	andl	$1, %edx
	cmpl	$1, %edx
	sbbl	$-1, %eax
	addl	%eax, 4(%r12)
.L820:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26293:
	.size	_ZN2v88internal8compiler13CodeGenerator11FinishFrameEPNS1_5FrameE, .-_ZN2v88internal8compiler13CodeGenerator11FinishFrameEPNS1_5FrameE
	.section	.text._ZN2v88internal8compiler13CodeGenerator22AssembleConstructFrameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator22AssembleConstructFrameEv
	.type	_ZN2v88internal8compiler13CodeGenerator22AssembleConstructFrameEv, @function
_ZN2v88internal8compiler13CodeGenerator22AssembleConstructFrameEv:
.LFB26294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	(%rax), %r14
	movq	24(%rdi), %rax
	cmpb	$0, 16(%rax)
	je	.L876
	movl	8(%r14), %eax
	movq	232(%rdi), %r12
	leaq	200(%rdi), %r13
	subq	216(%rdi), %r12
	cmpl	$2, %eax
	je	.L987
	cmpl	$1, %eax
	je	.L988
	movq	152(%rdi), %rdi
	call	_ZNK2v88internal24OptimizedCompilationInfo23GetOutputStackFrameTypeEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal14TurboAssembler12StubPrologueENS0_10StackFrame4TypeE@PLT
	movl	8(%r14), %eax
	cmpl	$4, %eax
	je	.L989
	subl	$3, %eax
	andl	$-3, %eax
	je	.L990
.L879:
	leaq	48(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal8compiler19UnwindingInfoWriter20MarkFrameConstructedEi@PLT
	movq	24(%rbx), %rax
.L876:
	movq	(%rax), %rax
	movl	4(%rax), %r13d
	subl	(%rax), %r13d
	movq	152(%rbx), %rax
	cmpl	$-1, 56(%rax)
	jne	.L991
.L883:
	movl	52(%r14), %r12d
	movl	56(%r14), %r14d
	testl	%r13d, %r13d
	jle	.L887
	movq	152(%rbx), %rax
	cmpl	$5, 8(%rax)
	jne	.L888
	cmpl	$128, %r13d
	jg	.L992
.L888:
	movl	%r12d, %edi
	call	__popcountdi2@PLT
	movl	%r14d, %edi
	movl	%eax, %r15d
	call	__popcountdi2@PLT
	subl	%r15d, %r13d
	addl	%eax, %eax
	subl	%eax, %r13d
	movq	24(%rbx), %rax
	movq	(%rax), %rax
	subl	12(%rax), %r13d
	testl	%r13d, %r13d
	jg	.L993
.L887:
	testl	%r14d, %r14d
	jne	.L895
.L901:
	testl	%r12d, %r12d
	je	.L897
	testl	$32768, %r12d
	jne	.L994
.L905:
	testl	$16384, %r12d
	je	.L906
	leaq	200(%rbx), %rdi
	movl	$14, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L906:
	testl	$8192, %r12d
	je	.L907
	leaq	200(%rbx), %rdi
	movl	$13, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L907:
	testl	$4096, %r12d
	je	.L908
	leaq	200(%rbx), %rdi
	movl	$12, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L908:
	testl	$2048, %r12d
	je	.L909
	leaq	200(%rbx), %rdi
	movl	$11, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L909:
	testl	$1024, %r12d
	je	.L910
	leaq	200(%rbx), %rdi
	movl	$10, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L910:
	testl	$512, %r12d
	je	.L911
	leaq	200(%rbx), %rdi
	movl	$9, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L911:
	testl	$256, %r12d
	je	.L912
	leaq	200(%rbx), %rdi
	movl	$8, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L912:
	testb	$-128, %r12b
	je	.L913
	leaq	200(%rbx), %rdi
	movl	$7, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L913:
	testb	$64, %r12b
	je	.L914
	leaq	200(%rbx), %rdi
	movl	$6, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L914:
	testb	$32, %r12b
	je	.L915
	leaq	200(%rbx), %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L915:
	testb	$16, %r12b
	je	.L916
	leaq	200(%rbx), %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L916:
	testb	$8, %r12b
	je	.L917
	leaq	200(%rbx), %rdi
	movl	$3, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L917:
	testb	$4, %r12b
	je	.L918
	leaq	200(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L918:
	testb	$2, %r12b
	je	.L919
	leaq	200(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L919:
	andl	$1, %r12d
	je	.L897
	leaq	200(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L897:
	movq	24(%rbx), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jle	.L875
	movl	$8, %r8d
	movl	$4, %edx
	movl	$5, %esi
	leal	0(,%rax,8), %ecx
	leaq	200(%rbx), %rdi
	movabsq	$81604378624, %rax
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
.L875:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L995
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L994:
	.cfi_restore_state
	leaq	200(%rbx), %rdi
	movl	$15, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L993:
	movl	$8, %r8d
	movl	$4, %edx
	movl	$5, %esi
	leal	0(,%r13,8), %ecx
	leaq	200(%rbx), %rdi
	movabsq	$81604378624, %r13
	orq	%r13, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	testl	%r14d, %r14d
	je	.L901
.L895:
	movl	%r14d, %edi
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	call	__popcountdi2@PLT
	movl	$8, %r8d
	movl	$4, %edx
	leaq	200(%rbx), %rdi
	sall	$4, %eax
	movl	$5, %esi
	movq	%rdi, -216(%rbp)
	movl	%eax, %ecx
	movabsq	$81604378624, %rax
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L996:
	call	_ZN2v88internal9Assembler7vmovdquENS0_7OperandENS0_11XMMRegisterE@PLT
.L900:
	addl	$1, %r13d
.L898:
	addl	$1, %r15d
	cmpl	$16, %r15d
	je	.L901
.L902:
	btl	%r15d, %r14d
	jnc	.L898
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	%r13d, %edx
	leaq	-192(%rbp), %rdi
	sall	$4, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-192(%rbp), %rsi
	movl	-184(%rbp), %edx
	movl	%r15d, %ecx
	movq	-216(%rbp), %rdi
	movq	%rsi, -108(%rbp)
	movl	%edx, -100(%rbp)
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L996
	call	_ZN2v88internal9Assembler6movdquENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L991:
	leaq	200(%rbx), %rdi
	movl	$34, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L997
.L884:
	movq	232(%rbx), %rax
	subq	216(%rbx), %rax
	movq	%rbx, %rdi
	movl	%eax, 1144(%rbx)
	subl	1136(%rbx), %r13d
	call	_ZN2v88internal8compiler13CodeGenerator22ResetSpeculationPoisonEv@PLT
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L987:
	movl	_ZN2v88internalL3rbpE(%rip), %r15d
	movq	%r13, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movq	%r13, %rdi
	movl	$8, %ecx
	movl	%r15d, %esi
	movl	_ZN2v88internalL3rspE(%rip), %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	152(%rbx), %rdi
	call	_ZNK2v88internal24OptimizedCompilationInfo23GetOutputStackFrameTypeEv@PLT
	cmpl	$9, %eax
	jne	.L879
	movabsq	$81604378624, %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	orq	$18, %rsi
	call	_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE@PLT
	movq	%r15, %rcx
	movl	$4, %edx
	movq	%r13, %rdi
	orq	$8, %rcx
	movl	$8, %r8d
	movl	$5, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L992:
	movl	_ZN2v88internal15FLAG_stack_sizeE(%rip), %eax
	leal	0(,%r13,8), %r10d
	movq	$0, -200(%rbp)
	leaq	200(%rbx), %r15
	leaq	-200(%rbp), %r9
	sall	$10, %eax
	cmpl	%eax, %r10d
	jl	.L998
.L889:
	movq	%r15, %rdi
	movl	$5, %edx
	movl	$22, %esi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal9Assembler9near_callElNS0_9RelocInfo4ModeE@PLT
	movq	8(%rbx), %rdi
	movq	-216(%rbp), %r9
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$39, %rax
	jbe	.L999
	leaq	40(%r8), %rax
	movq	%rax, 16(%rdi)
.L891:
	movq	%rdi, (%r8)
	movq	$0, 8(%r8)
	movq	$0, 16(%r8)
	movq	$0, 24(%r8)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$63, %rdx
	jbe	.L1000
	leaq	64(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L893:
	pxor	%xmm0, %xmm0
	movq	%rdx, 24(%r8)
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movq	%rax, 8(%r8)
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movq	%rdx, 16(%r8)
	xorl	%edx, %edx
	movl	$-1, 32(%r8)
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler13CodeGenerator15RecordSafepointEPNS1_12ReferenceMapENS0_9Safepoint9DeoptModeE@PLT
	movq	%r15, %rdi
	movl	$47, %esi
	call	_ZN2v88internal14TurboAssembler17AssertUnreachableENS0_11AbortReasonE@PLT
	movq	-216(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L990:
	movl	_ZN2v88internalL21kWasmInstanceRegisterE(%rip), %r15d
	leaq	-96(%rbp), %r8
	movl	$15, %edx
	movq	%r8, %rdi
	movq	%r8, -216(%rbp)
	movl	%r15d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movl	_ZN2v88internalL19kJSFunctionRegisterE(%rip), %esi
	movl	%ecx, -124(%rbp)
	movl	%ecx, -172(%rbp)
	movq	%rdx, -132(%rbp)
	movq	%rdx, -180(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	-216(%rbp), %r8
	movl	$7, %edx
	movl	%r15d, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %ecx
	movl	%r15d, %esi
	movq	%r13, %rdi
	movq	%rdx, -144(%rbp)
	movl	%ecx, -136(%rbp)
	movq	%rdx, -168(%rbp)
	movl	%ecx, -160(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	cmpl	$3, 8(%r14)
	jne	.L879
	movl	$8, %r8d
	movl	$4, %edx
	movl	$5, %esi
	movq	%r13, %rdi
	movabsq	$81604378632, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L997:
	leaq	-200(%rbp), %r9
	leaq	-96(%rbp), %r15
	xorl	%edx, %edx
	movq	$20, -200(%rbp)
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-80(%rbp), %r12
	movq	%r12, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-200(%rbp), %rdx
	movdqa	.LC2(%rip), %xmm0
	leaq	240(%rbx), %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$757932148, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-200(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r15, %rdx
	movq	232(%rbx), %rsi
	subq	216(%rbx), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L884
	call	_ZdlPv@PLT
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L988:
	movq	%r13, %rdi
	call	_ZN2v88internal14TurboAssembler8PrologueEv@PLT
	testb	$32, 64(%r14)
	je	.L879
	movl	_ZN2v88internalL31kJavaScriptCallArgCountRegisterE(%rip), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L879
.L989:
	movl	_ZN2v88internalL21kWasmInstanceRegisterE(%rip), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L879
.L998:
	movl	_ZN2v88internalL21kWasmInstanceRegisterE(%rip), %esi
	leaq	-96(%rbp), %rdi
	movl	$239, %edx
	movq	%r9, -224(%rbp)
	movl	%r10d, -216(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	movq	%r15, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -112(%rbp)
	movl	%ecx, -148(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rdx, -156(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	leaq	-108(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-108(%rbp), %rdx
	movl	-100(%rbp), %ecx
	movq	%r15, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -96(%rbp)
	movl	%ecx, -88(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	-216(%rbp), %ecx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movabsq	$81604378624, %r10
	movl	$8, %r8d
	movl	$10, %edx
	orq	%r10, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$8, %r8d
	movl	$10, %ecx
	movq	%r15, %rdi
	movl	$4, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	-224(%rbp), %r9
	movl	$1, %ecx
	movq	%r15, %rdi
	movl	$3, %esi
	movq	%r9, %rdx
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-216(%rbp), %r9
	jmp	.L889
.L999:
	movl	$40, %esi
	movq	%r9, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-216(%rbp), %rdi
	movq	-224(%rbp), %r9
	movq	%rax, %r8
	jmp	.L891
.L1000:
	movl	$64, %esi
	movq	%r9, -224(%rbp)
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-216(%rbp), %r8
	movq	-224(%rbp), %r9
	leaq	64(%rax), %rdx
	jmp	.L893
.L995:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26294:
	.size	_ZN2v88internal8compiler13CodeGenerator22AssembleConstructFrameEv, .-_ZN2v88internal8compiler13CodeGenerator22AssembleConstructFrameEv
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator14AssembleReturnEPNS1_18InstructionOperandE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"pop_size < static_cast<size_t>(std::numeric_limits<int>::max())"
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator14AssembleReturnEPNS1_18InstructionOperandE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler13CodeGenerator14AssembleReturnEPNS1_18InstructionOperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator14AssembleReturnEPNS1_18InstructionOperandE
	.type	_ZN2v88internal8compiler13CodeGenerator14AssembleReturnEPNS1_18InstructionOperandE, @function
_ZN2v88internal8compiler13CodeGenerator14AssembleReturnEPNS1_18InstructionOperandE:
.LFB26295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rsi, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	movl	52(%rax), %r12d
	movq	%rax, -120(%rbp)
	testl	%r12d, %r12d
	je	.L1022
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1102
	testb	$1, %r12b
	jne	.L1103
.L1006:
	testb	$2, %r12b
	jne	.L1104
.L1007:
	testb	$4, %r12b
	jne	.L1105
.L1008:
	testb	$8, %r12b
	jne	.L1106
.L1009:
	testb	$16, %r12b
	jne	.L1107
.L1010:
	testb	$32, %r12b
	jne	.L1108
.L1011:
	testb	$64, %r12b
	jne	.L1109
.L1012:
	testb	$-128, %r12b
	jne	.L1110
.L1013:
	testl	$256, %r12d
	jne	.L1111
.L1014:
	testl	$512, %r12d
	jne	.L1112
.L1015:
	testl	$1024, %r12d
	jne	.L1113
.L1016:
	testl	$2048, %r12d
	jne	.L1114
.L1017:
	testl	$4096, %r12d
	jne	.L1115
.L1018:
	testl	$8192, %r12d
	jne	.L1116
.L1019:
	testl	$16384, %r12d
	jne	.L1117
.L1020:
	andl	$32768, %r12d
	jne	.L1118
	.p2align 4,,10
	.p2align 3
.L1022:
	movq	-120(%rbp), %rax
	movl	56(%rax), %r13d
	testl	%r13d, %r13d
	je	.L1004
	movl	%r13d, %edi
	leaq	200(%rbx), %r14
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	call	__popcountdi2@PLT
	sall	$4, %eax
	movl	%eax, -132(%rbp)
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1120:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler7vmovdquENS0_11XMMRegisterENS0_7OperandE@PLT
.L1025:
	addl	$1, %r12d
.L1023:
	addl	$1, %r15d
	cmpl	$16, %r15d
	je	.L1119
.L1026:
	btl	%r15d, %r13d
	jnc	.L1023
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	%r12d, %edx
	leaq	-92(%rbp), %rdi
	sall	$4, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rdx
	movl	-84(%rbp), %ecx
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L1120
	movl	-60(%rbp), %ecx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler6movdquENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1119:
	movl	-132(%rbp), %ecx
	movl	$4, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movabsq	$81604378624, %rax
	movl	$8, %r8d
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
.L1004:
	movq	-120(%rbp), %rax
	movb	$1, 113(%rbx)
	movq	32(%rax), %rsi
	cmpl	$2, 8(%rax)
	movq	%rsi, -120(%rbp)
	leaq	0(,%rsi,8), %r13
	je	.L1121
	movq	-128(%rbp), %rax
	movq	24(%rbx), %rcx
	movq	(%rax), %rdx
	movl	%edx, %eax
	andl	$7, %eax
	cmpb	$0, 16(%rcx)
	jne	.L1122
	cmpl	$3, %eax
	je	.L1123
.L1035:
	sarq	$35, %rdx
	movl	_ZN2v88internalL3rdxE(%rip), %r15d
	leaq	200(%rbx), %r12
	cmpq	$1, %rdx
	cmovne	_ZN2v88internalL3rcxE(%rip), %r15d
	movq	%r12, %rdi
	movq	%rdx, %r14
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	leaq	-80(%rbp), %rdi
	movl	%r13d, %r8d
	movl	%r14d, %edx
	movl	_ZN2v88internalL3rspE(%rip), %ebx
	movl	$3, %ecx
	movl	%ebx, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
.L1001:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1124
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1122:
	.cfi_restore_state
	leaq	200(%rbx), %r12
	cmpl	$3, %eax
	je	.L1125
.L1101:
	movq	232(%rbx), %rsi
	leaq	48(%rbx), %rdi
	subq	216(%rbx), %rsi
	call	_ZN2v88internal8compiler19UnwindingInfoWriter22MarkFrameDeconstructedEi@PLT
.L1100:
	movl	_ZN2v88internalL3rbpE(%rip), %r14d
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	%r14d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movq	-128(%rbp), %rax
	movq	(%rax), %rdx
	movl	%edx, %eax
	andl	$7, %eax
	cmpl	$3, %eax
	jne	.L1035
.L1123:
	movq	%rdx, %rsi
	sarq	$32, %rsi
	andl	$8, %edx
	jne	.L1036
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-104(%rbp), %rsi
.L1037:
	sall	$3, %esi
	movslq	%esi, %rsi
	addq	%r13, %rsi
	cmpq	$2147483646, %rsi
	ja	.L1126
	movl	_ZN2v88internalL3rcxE(%rip), %edx
	leaq	200(%rbx), %rdi
	call	_ZN2v88internal14TurboAssembler3RetEiNS0_8RegisterE@PLT
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1118:
	leaq	200(%rbx), %rdi
	movl	$15, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1117:
	leaq	200(%rbx), %rdi
	movl	$14, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	andl	$32768, %r12d
	je	.L1022
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1116:
	leaq	200(%rbx), %rdi
	movl	$13, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testl	$16384, %r12d
	je	.L1020
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1115:
	leaq	200(%rbx), %rdi
	movl	$12, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testl	$8192, %r12d
	je	.L1019
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1114:
	leaq	200(%rbx), %rdi
	movl	$11, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testl	$4096, %r12d
	je	.L1018
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1113:
	leaq	200(%rbx), %rdi
	movl	$10, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testl	$2048, %r12d
	je	.L1017
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1112:
	leaq	200(%rbx), %rdi
	movl	$9, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testl	$1024, %r12d
	je	.L1016
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1111:
	leaq	200(%rbx), %rdi
	movl	$8, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testl	$512, %r12d
	je	.L1015
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1110:
	leaq	200(%rbx), %rdi
	movl	$7, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testl	$256, %r12d
	je	.L1014
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1109:
	leaq	200(%rbx), %rdi
	movl	$6, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$-128, %r12b
	je	.L1013
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1108:
	leaq	200(%rbx), %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$64, %r12b
	je	.L1012
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1107:
	leaq	200(%rbx), %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$32, %r12b
	je	.L1011
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1106:
	leaq	200(%rbx), %rdi
	movl	$3, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$16, %r12b
	je	.L1010
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1105:
	leaq	200(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$8, %r12b
	je	.L1009
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1104:
	leaq	200(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$4, %r12b
	je	.L1008
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1103:
	xorl	%esi, %esi
	leaq	200(%rbx), %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$2, %r12b
	je	.L1007
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1121:
	movq	232(%rbx), %rsi
	leaq	48(%rbx), %rdi
	subq	216(%rbx), %rsi
	leaq	200(%rbx), %r12
	call	_ZN2v88internal8compiler19UnwindingInfoWriter22MarkFrameDeconstructedEi@PLT
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1102:
	leal	0(,%rax,8), %ecx
	xorl	%esi, %esi
	movl	$4, %edx
	movabsq	$81604378624, %rax
	orq	%rax, %rcx
	leaq	200(%rdi), %rdi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	testb	$1, %r12b
	je	.L1006
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1125:
	movq	%rdx, %rsi
	sarq	$32, %rsi
	andl	$8, %edx
	jne	.L1030
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-104(%rbp), %rax
.L1031:
	testl	%eax, %eax
	jne	.L1101
	movl	168(%rbx), %eax
	leaq	168(%rbx), %rsi
	testl	%eax, %eax
	js	.L1127
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L1101
.L1036:
	movq	40(%rbx), %rax
	salq	$4, %rsi
	addq	152(%rax), %rsi
	movq	8(%rsi), %rsi
	jmp	.L1037
.L1126:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1127:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	jmp	.L1001
.L1030:
	movq	40(%rbx), %rdx
	movq	%rsi, %rax
	salq	$4, %rax
	addq	152(%rdx), %rax
	movq	8(%rax), %rax
	jmp	.L1031
.L1124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26295:
	.size	_ZN2v88internal8compiler13CodeGenerator14AssembleReturnEPNS1_18InstructionOperandE, .-_ZN2v88internal8compiler13CodeGenerator14AssembleReturnEPNS1_18InstructionOperandE
	.section	.text._ZN2v88internal8compiler13CodeGenerator10FinishCodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator10FinishCodeEv
	.type	_ZN2v88internal8compiler13CodeGenerator10FinishCodeEv, @function
_ZN2v88internal8compiler13CodeGenerator10FinishCodeEv:
.LFB26296:
	.cfi_startproc
	endbr64
	addq	$200, %rdi
	jmp	_ZN2v88internal9Assembler14PatchConstPoolEv@PLT
	.cfi_endproc
.LFE26296:
	.size	_ZN2v88internal8compiler13CodeGenerator10FinishCodeEv, .-_ZN2v88internal8compiler13CodeGenerator10FinishCodeEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator29PrepareForDeoptimizationExitsEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator29PrepareForDeoptimizationExitsEi
	.type	_ZN2v88internal8compiler13CodeGenerator29PrepareForDeoptimizationExitsEi, @function
_ZN2v88internal8compiler13CodeGenerator29PrepareForDeoptimizationExitsEi:
.LFB26297:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE26297:
	.size	_ZN2v88internal8compiler13CodeGenerator29PrepareForDeoptimizationExitsEi, .-_ZN2v88internal8compiler13CodeGenerator29PrepareForDeoptimizationExitsEi
	.section	.text._ZN2v88internal8compiler13CodeGenerator17AssembleJumpTableEPPNS0_5LabelEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator17AssembleJumpTableEPPNS0_5LabelEm
	.type	_ZN2v88internal8compiler13CodeGenerator17AssembleJumpTableEPPNS0_5LabelEm, @function
_ZN2v88internal8compiler13CodeGenerator17AssembleJumpTableEPPNS0_5LabelEm:
.LFB26302:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L1135
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	(%rsi,%rdx,8), %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	200(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal9Assembler2dqEPNS0_5LabelE@PLT
	cmpq	%r13, %rbx
	jne	.L1133
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1135:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE26302:
	.size	_ZN2v88internal8compiler13CodeGenerator17AssembleJumpTableEPPNS0_5LabelEm, .-_ZN2v88internal8compiler13CodeGenerator17AssembleJumpTableEPPNS0_5LabelEm
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB29053:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1146
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1140:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1140
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1146:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE29053:
	.size	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.rodata._ZNSt6vectorISt4pairIiPN2v88internal5LabelEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC5:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt4pairIiPN2v88internal5LabelEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIiPN2v88internal5LabelEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIiPN2v88internal5LabelEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorISt4pairIiPN2v88internal5LabelEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorISt4pairIiPN2v88internal5LabelEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB31394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$576460752303423487, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L1168
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L1159
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L1169
.L1151:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	16(%r14), %r8
.L1158:
	movq	8(%rdx), %rsi
	movl	(%rdx), %edx
	addq	%r14, %rcx
	movl	%edx, (%rcx)
	movq	%rsi, 8(%rcx)
	cmpq	%r15, %rbx
	je	.L1153
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L1154:
	movl	(%rdx), %edi
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movl	%edi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1154
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	16(%r14,%rdx), %r8
.L1153:
	cmpq	%r12, %rbx
	je	.L1155
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L1156:
	movq	8(%rdx), %rsi
	movl	(%rdx), %edi
	addq	$16, %rdx
	addq	$16, %rcx
	movl	%edi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L1156
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L1155:
	testq	%r15, %r15
	je	.L1157
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L1157:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1169:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L1152
	movl	$16, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1159:
	movl	$16, %esi
	jmp	.L1151
.L1152:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$4, %rax
	movq	%rax, %rsi
	jmp	.L1151
.L1168:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE31394:
	.size	_ZNSt6vectorISt4pairIiPN2v88internal5LabelEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorISt4pairIiPN2v88internal5LabelEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler13CodeGenerator30AssembleArchBinarySearchSwitchEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator30AssembleArchBinarySearchSwitchEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler13CodeGenerator30AssembleArchBinarySearchSwitchEPNS1_11InstructionE, @function
_ZN2v88internal8compiler13CodeGenerator30AssembleArchBinarySearchSwitchEPNS1_11InstructionE:
.LFB26273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	4(%rsi), %eax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movzbl	%al, %ecx
	shrl	$8, %eax
	movq	40(%rsi,%rcx,8), %rdi
	sarq	$35, %rdi
	movq	%rdi, -104(%rbp)
	cmpw	$2, %ax
	jbe	.L1205
	movq	%rcx, %rdx
	movl	$2, %r13d
	leaq	-96(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L1193:
	movl	%edx, %eax
	movq	40(%r15), %rcx
	leaq	5(%r13,%rax), %rax
	movq	(%rbx,%rax,8), %rax
	movl	%eax, %esi
	movq	%rax, %r8
	andl	$7, %esi
	shrq	$3, %r8
	cmpl	$3, %esi
	je	.L1223
	movq	112(%rcx), %rax
	movl	%r8d, %r10d
	leaq	104(%rcx), %rsi
	testq	%rax, %rax
	je	.L1176
	movq	%rsi, %rdi
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1178
.L1177:
	cmpl	32(%rax), %r10d
	jle	.L1224
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1177
.L1178:
	cmpq	%rdi, %rsi
	je	.L1176
	cmpl	%r8d, 32(%rdi)
	cmovle	%rdi, %rsi
.L1176:
	movq	48(%rsi), %r14
.L1175:
	leaq	6(%r13,%rdx), %rax
	movq	(%rbx,%rax,8), %rax
	movl	%eax, %edx
	movq	%rax, %rsi
	andl	$7, %edx
	shrq	$3, %rsi
	cmpl	$3, %edx
	je	.L1225
	movq	112(%rcx), %rax
	movl	%esi, %edi
	leaq	104(%rcx), %rdx
	testq	%rax, %rax
	je	.L1185
	movq	%rdx, %rcx
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L1226:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1187
.L1186:
	cmpl	32(%rax), %edi
	jle	.L1226
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1186
.L1187:
	cmpq	%rcx, %rdx
	je	.L1185
	cmpl	%esi, 32(%rcx)
	cmovle	%rcx, %rdx
.L1185:
	movq	48(%rdx), %rax
.L1184:
	movq	160(%r15), %rdx
	cltq
	movq	-72(%rbp), %rsi
	movl	%r14d, -96(%rbp)
	leaq	(%rdx,%rax,8), %rax
	movq	%rax, -88(%rbp)
	cmpq	-64(%rbp), %rsi
	je	.L1190
	movl	%r14d, (%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, -72(%rbp)
.L1222:
	movl	4(%rbx), %eax
	addq	$2, %r13
	movl	%eax, %edx
	shrl	$8, %edx
	movzwl	%dx, %edx
	cmpq	%rdx, %r13
	jnb	.L1192
	movzbl	%al, %edx
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1225:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	jne	.L1182
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-88(%rbp), %rax
	jmp	.L1184
	.p2align 4,,10
	.p2align 3
.L1223:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	jne	.L1173
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-88(%rbp), %r14
	movq	40(%r15), %rcx
	movzbl	4(%rbx), %edx
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1190:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorISt4pairIiPN2v88internal5LabelEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r12
	movzbl	%al, %ecx
.L1171:
	movq	48(%rbx,%rcx,8), %rsi
	movq	40(%r15), %rax
	movl	%esi, %edx
	movq	%rsi, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L1227
	leaq	104(%rax), %rsi
	movq	112(%rax), %rax
	movl	%edi, %edx
	testq	%rax, %rax
	je	.L1198
	movq	%rsi, %rcx
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1228:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1200
.L1199:
	cmpl	32(%rax), %edx
	jle	.L1228
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1199
.L1200:
	cmpq	%rcx, %rsi
	je	.L1198
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L1198:
	movq	48(%rsi), %rdx
.L1197:
	movl	-104(%rbp), %esi
	movq	%r15, %rdi
	movq	%r12, %rcx
	call	_ZN2v88internal8compiler13CodeGenerator35AssembleArchBinarySearchSwitchRangeENS0_8RegisterENS1_9RpoNumberEPSt4pairIiPNS0_5LabelEES9_@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1170
	call	_ZdlPv@PLT
.L1170:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1229
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1182:
	.cfi_restore_state
	salq	$4, %rsi
	addq	152(%rcx), %rsi
	movq	8(%rsi), %rax
	jmp	.L1184
	.p2align 4,,10
	.p2align 3
.L1173:
	salq	$4, %rsi
	addq	152(%rcx), %rsi
	movq	8(%rsi), %r14
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1227:
	sarq	$32, %rsi
	andl	$1, %edi
	je	.L1230
	salq	$4, %rsi
	addq	152(%rax), %rsi
	movq	8(%rsi), %rdx
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1230:
	leaq	-96(%rbp), %rdi
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-88(%rbp), %rdx
	movq	-112(%rbp), %r8
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1205:
	xorl	%r12d, %r12d
	xorl	%r8d, %r8d
	jmp	.L1171
.L1229:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26273:
	.size	_ZN2v88internal8compiler13CodeGenerator30AssembleArchBinarySearchSwitchEPNS1_11InstructionE, .-_ZN2v88internal8compiler13CodeGenerator30AssembleArchBinarySearchSwitchEPNS1_11InstructionE
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator23AssembleArchInstructionEPNS1_11InstructionE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"!HasImmediateInput(instr, 0)"
.LC7:
	.string	"instr->HasOutput()"
.LC8:
	.string	"!instr->HasOutput()"
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator23AssembleArchInstructionEPNS1_11InstructionE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v88internal8compiler13CodeGenerator23AssembleArchInstructionEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator23AssembleArchInstructionEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler13CodeGenerator23AssembleArchInstructionEPNS1_11InstructionE, @function
_ZN2v88internal8compiler13CodeGenerator23AssembleArchInstructionEPNS1_11InstructionE:
.LFB26264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$2312, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %ebx
	movl	%ebx, %r13d
	andl	$511, %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %xmm0
	movq	%rsi, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -10480(%rbp)
	cmpl	$452, %r13d
	ja	.L2698
	leaq	.L1234(%rip), %rcx
	movl	%r13d, %edx
	movq	%rdi, %r15
	movq	%rsi, %r11
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator23AssembleArchInstructionEPNS1_11InstructionE,"a",@progbits
	.align 4
	.align 4
.L1234:
	.long	.L1662-.L1234
	.long	.L1661-.L1234
	.long	.L1661-.L1234
	.long	.L1660-.L1234
	.long	.L1659-.L1234
	.long	.L1658-.L1234
	.long	.L1657-.L1234
	.long	.L1656-.L1234
	.long	.L1655-.L1234
	.long	.L1654-.L1234
	.long	.L1653-.L1234
	.long	.L1652-.L1234
	.long	.L1651-.L1234
	.long	.L1650-.L1234
	.long	.L1649-.L1234
	.long	.L1648-.L1234
	.long	.L1647-.L1234
	.long	.L2698-.L1234
	.long	.L1646-.L1234
	.long	.L1645-.L1234
	.long	.L1644-.L1234
	.long	.L1643-.L1234
	.long	.L1642-.L1234
	.long	.L1641-.L1234
	.long	.L1640-.L1234
	.long	.L1639-.L1234
	.long	.L1638-.L1234
	.long	.L1637-.L1234
	.long	.L1636-.L1234
	.long	.L1635-.L1234
	.long	.L1634-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1633-.L1234
	.long	.L1632-.L1234
	.long	.L1631-.L1234
	.long	.L1630-.L1234
	.long	.L1629-.L1234
	.long	.L1628-.L1234
	.long	.L1627-.L1234
	.long	.L1626-.L1234
	.long	.L1625-.L1234
	.long	.L1624-.L1234
	.long	.L1623-.L1234
	.long	.L1622-.L1234
	.long	.L1621-.L1234
	.long	.L1620-.L1234
	.long	.L1619-.L1234
	.long	.L1618-.L1234
	.long	.L1617-.L1234
	.long	.L1616-.L1234
	.long	.L1615-.L1234
	.long	.L1614-.L1234
	.long	.L1613-.L1234
	.long	.L1612-.L1234
	.long	.L1611-.L1234
	.long	.L1610-.L1234
	.long	.L1609-.L1234
	.long	.L1608-.L1234
	.long	.L1607-.L1234
	.long	.L1606-.L1234
	.long	.L1605-.L1234
	.long	.L1604-.L1234
	.long	.L1603-.L1234
	.long	.L1602-.L1234
	.long	.L1601-.L1234
	.long	.L1600-.L1234
	.long	.L1599-.L1234
	.long	.L1598-.L1234
	.long	.L1597-.L1234
	.long	.L1596-.L1234
	.long	.L1595-.L1234
	.long	.L1594-.L1234
	.long	.L1593-.L1234
	.long	.L1592-.L1234
	.long	.L1591-.L1234
	.long	.L1590-.L1234
	.long	.L1589-.L1234
	.long	.L1588-.L1234
	.long	.L1587-.L1234
	.long	.L1586-.L1234
	.long	.L1585-.L1234
	.long	.L1584-.L1234
	.long	.L1583-.L1234
	.long	.L1582-.L1234
	.long	.L1581-.L1234
	.long	.L1580-.L1234
	.long	.L1579-.L1234
	.long	.L1578-.L1234
	.long	.L1577-.L1234
	.long	.L1576-.L1234
	.long	.L1575-.L1234
	.long	.L1574-.L1234
	.long	.L1573-.L1234
	.long	.L1572-.L1234
	.long	.L1571-.L1234
	.long	.L1570-.L1234
	.long	.L1569-.L1234
	.long	.L1568-.L1234
	.long	.L1567-.L1234
	.long	.L1566-.L1234
	.long	.L1565-.L1234
	.long	.L1564-.L1234
	.long	.L1563-.L1234
	.long	.L1562-.L1234
	.long	.L1561-.L1234
	.long	.L1560-.L1234
	.long	.L1559-.L1234
	.long	.L1558-.L1234
	.long	.L1557-.L1234
	.long	.L1556-.L1234
	.long	.L1555-.L1234
	.long	.L1554-.L1234
	.long	.L1553-.L1234
	.long	.L1552-.L1234
	.long	.L1551-.L1234
	.long	.L1550-.L1234
	.long	.L1549-.L1234
	.long	.L1548-.L1234
	.long	.L1547-.L1234
	.long	.L1546-.L1234
	.long	.L1545-.L1234
	.long	.L1544-.L1234
	.long	.L1543-.L1234
	.long	.L1542-.L1234
	.long	.L1541-.L1234
	.long	.L1540-.L1234
	.long	.L1539-.L1234
	.long	.L1538-.L1234
	.long	.L1537-.L1234
	.long	.L1536-.L1234
	.long	.L1535-.L1234
	.long	.L1534-.L1234
	.long	.L1533-.L1234
	.long	.L1532-.L1234
	.long	.L1531-.L1234
	.long	.L1530-.L1234
	.long	.L1529-.L1234
	.long	.L1528-.L1234
	.long	.L1527-.L1234
	.long	.L1526-.L1234
	.long	.L1525-.L1234
	.long	.L1524-.L1234
	.long	.L1523-.L1234
	.long	.L1522-.L1234
	.long	.L1521-.L1234
	.long	.L1520-.L1234
	.long	.L1519-.L1234
	.long	.L1518-.L1234
	.long	.L1517-.L1234
	.long	.L1516-.L1234
	.long	.L1515-.L1234
	.long	.L1514-.L1234
	.long	.L1513-.L1234
	.long	.L1512-.L1234
	.long	.L1433-.L1234
	.long	.L1432-.L1234
	.long	.L1511-.L1234
	.long	.L1510-.L1234
	.long	.L1509-.L1234
	.long	.L1508-.L1234
	.long	.L1507-.L1234
	.long	.L1506-.L1234
	.long	.L1505-.L1234
	.long	.L1504-.L1234
	.long	.L1503-.L1234
	.long	.L1502-.L1234
	.long	.L1501-.L1234
	.long	.L1500-.L1234
	.long	.L1499-.L1234
	.long	.L1498-.L1234
	.long	.L1497-.L1234
	.long	.L1496-.L1234
	.long	.L1495-.L1234
	.long	.L1494-.L1234
	.long	.L1493-.L1234
	.long	.L1492-.L1234
	.long	.L1491-.L1234
	.long	.L1490-.L1234
	.long	.L1489-.L1234
	.long	.L1488-.L1234
	.long	.L1487-.L1234
	.long	.L1486-.L1234
	.long	.L1485-.L1234
	.long	.L1484-.L1234
	.long	.L1483-.L1234
	.long	.L1482-.L1234
	.long	.L1481-.L1234
	.long	.L1480-.L1234
	.long	.L1479-.L1234
	.long	.L1478-.L1234
	.long	.L1477-.L1234
	.long	.L1476-.L1234
	.long	.L1475-.L1234
	.long	.L1474-.L1234
	.long	.L1473-.L1234
	.long	.L1472-.L1234
	.long	.L1471-.L1234
	.long	.L1470-.L1234
	.long	.L1469-.L1234
	.long	.L1468-.L1234
	.long	.L1467-.L1234
	.long	.L1466-.L1234
	.long	.L1465-.L1234
	.long	.L1464-.L1234
	.long	.L1463-.L1234
	.long	.L1462-.L1234
	.long	.L1461-.L1234
	.long	.L1460-.L1234
	.long	.L1459-.L1234
	.long	.L1458-.L1234
	.long	.L1457-.L1234
	.long	.L1456-.L1234
	.long	.L1455-.L1234
	.long	.L1454-.L1234
	.long	.L1453-.L1234
	.long	.L1452-.L1234
	.long	.L1451-.L1234
	.long	.L1450-.L1234
	.long	.L1449-.L1234
	.long	.L1448-.L1234
	.long	.L1447-.L1234
	.long	.L1446-.L1234
	.long	.L1445-.L1234
	.long	.L1444-.L1234
	.long	.L1443-.L1234
	.long	.L1442-.L1234
	.long	.L1441-.L1234
	.long	.L1440-.L1234
	.long	.L1439-.L1234
	.long	.L1438-.L1234
	.long	.L1437-.L1234
	.long	.L1436-.L1234
	.long	.L1435-.L1234
	.long	.L1434-.L1234
	.long	.L1433-.L1234
	.long	.L1432-.L1234
	.long	.L1431-.L1234
	.long	.L1430-.L1234
	.long	.L1429-.L1234
	.long	.L1428-.L1234
	.long	.L1427-.L1234
	.long	.L1426-.L1234
	.long	.L1425-.L1234
	.long	.L1424-.L1234
	.long	.L1423-.L1234
	.long	.L1422-.L1234
	.long	.L1421-.L1234
	.long	.L1420-.L1234
	.long	.L1419-.L1234
	.long	.L1418-.L1234
	.long	.L1417-.L1234
	.long	.L1416-.L1234
	.long	.L1415-.L1234
	.long	.L1414-.L1234
	.long	.L1413-.L1234
	.long	.L1412-.L1234
	.long	.L1411-.L1234
	.long	.L1410-.L1234
	.long	.L1409-.L1234
	.long	.L1408-.L1234
	.long	.L1407-.L1234
	.long	.L1406-.L1234
	.long	.L1405-.L1234
	.long	.L1404-.L1234
	.long	.L1403-.L1234
	.long	.L1402-.L1234
	.long	.L1401-.L1234
	.long	.L1400-.L1234
	.long	.L1399-.L1234
	.long	.L1398-.L1234
	.long	.L1397-.L1234
	.long	.L1396-.L1234
	.long	.L1395-.L1234
	.long	.L1394-.L1234
	.long	.L1393-.L1234
	.long	.L1392-.L1234
	.long	.L1391-.L1234
	.long	.L1390-.L1234
	.long	.L1389-.L1234
	.long	.L1388-.L1234
	.long	.L1387-.L1234
	.long	.L1386-.L1234
	.long	.L1385-.L1234
	.long	.L1384-.L1234
	.long	.L1383-.L1234
	.long	.L1382-.L1234
	.long	.L1381-.L1234
	.long	.L1380-.L1234
	.long	.L1379-.L1234
	.long	.L1378-.L1234
	.long	.L1377-.L1234
	.long	.L1376-.L1234
	.long	.L1375-.L1234
	.long	.L1374-.L1234
	.long	.L1373-.L1234
	.long	.L1372-.L1234
	.long	.L1371-.L1234
	.long	.L1370-.L1234
	.long	.L1369-.L1234
	.long	.L1368-.L1234
	.long	.L1367-.L1234
	.long	.L1366-.L1234
	.long	.L1365-.L1234
	.long	.L1364-.L1234
	.long	.L1363-.L1234
	.long	.L1362-.L1234
	.long	.L1361-.L1234
	.long	.L1360-.L1234
	.long	.L1359-.L1234
	.long	.L1358-.L1234
	.long	.L1357-.L1234
	.long	.L1356-.L1234
	.long	.L1355-.L1234
	.long	.L1354-.L1234
	.long	.L1353-.L1234
	.long	.L1352-.L1234
	.long	.L1351-.L1234
	.long	.L1350-.L1234
	.long	.L1349-.L1234
	.long	.L1348-.L1234
	.long	.L1347-.L1234
	.long	.L1346-.L1234
	.long	.L1345-.L1234
	.long	.L1344-.L1234
	.long	.L1343-.L1234
	.long	.L1342-.L1234
	.long	.L1341-.L1234
	.long	.L1340-.L1234
	.long	.L1339-.L1234
	.long	.L1338-.L1234
	.long	.L1337-.L1234
	.long	.L1336-.L1234
	.long	.L1335-.L1234
	.long	.L1334-.L1234
	.long	.L1333-.L1234
	.long	.L1332-.L1234
	.long	.L1331-.L1234
	.long	.L1330-.L1234
	.long	.L1329-.L1234
	.long	.L1328-.L1234
	.long	.L1327-.L1234
	.long	.L1326-.L1234
	.long	.L1325-.L1234
	.long	.L1324-.L1234
	.long	.L1323-.L1234
	.long	.L1322-.L1234
	.long	.L1321-.L1234
	.long	.L1320-.L1234
	.long	.L1319-.L1234
	.long	.L1318-.L1234
	.long	.L1317-.L1234
	.long	.L1316-.L1234
	.long	.L1315-.L1234
	.long	.L1314-.L1234
	.long	.L1313-.L1234
	.long	.L1312-.L1234
	.long	.L1311-.L1234
	.long	.L1310-.L1234
	.long	.L1309-.L1234
	.long	.L1308-.L1234
	.long	.L1307-.L1234
	.long	.L1306-.L1234
	.long	.L1305-.L1234
	.long	.L1304-.L1234
	.long	.L1303-.L1234
	.long	.L1302-.L1234
	.long	.L1301-.L1234
	.long	.L1300-.L1234
	.long	.L1299-.L1234
	.long	.L1298-.L1234
	.long	.L1297-.L1234
	.long	.L1296-.L1234
	.long	.L1295-.L1234
	.long	.L1294-.L1234
	.long	.L1293-.L1234
	.long	.L1292-.L1234
	.long	.L1291-.L1234
	.long	.L1290-.L1234
	.long	.L1289-.L1234
	.long	.L1288-.L1234
	.long	.L1287-.L1234
	.long	.L1286-.L1234
	.long	.L1285-.L1234
	.long	.L1284-.L1234
	.long	.L1283-.L1234
	.long	.L1282-.L1234
	.long	.L1281-.L1234
	.long	.L1280-.L1234
	.long	.L1279-.L1234
	.long	.L1278-.L1234
	.long	.L1277-.L1234
	.long	.L1276-.L1234
	.long	.L1275-.L1234
	.long	.L1274-.L1234
	.long	.L1273-.L1234
	.long	.L1272-.L1234
	.long	.L1271-.L1234
	.long	.L1270-.L1234
	.long	.L1269-.L1234
	.long	.L1268-.L1234
	.long	.L1268-.L1234
	.long	.L1268-.L1234
	.long	.L1264-.L1234
	.long	.L1267-.L1234
	.long	.L1264-.L1234
	.long	.L1266-.L1234
	.long	.L1264-.L1234
	.long	.L1265-.L1234
	.long	.L1264-.L1234
	.long	.L1263-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1262-.L1234
	.long	.L1261-.L1234
	.long	.L1260-.L1234
	.long	.L1259-.L1234
	.long	.L1258-.L1234
	.long	.L1257-.L1234
	.long	.L1256-.L1234
	.long	.L1255-.L1234
	.long	.L1254-.L1234
	.long	.L1253-.L1234
	.long	.L1252-.L1234
	.long	.L1251-.L1234
	.long	.L1250-.L1234
	.long	.L1249-.L1234
	.long	.L1248-.L1234
	.long	.L1247-.L1234
	.long	.L1246-.L1234
	.long	.L1245-.L1234
	.long	.L1244-.L1234
	.long	.L1243-.L1234
	.long	.L1242-.L1234
	.long	.L1241-.L1234
	.long	.L1240-.L1234
	.long	.L1239-.L1234
	.long	.L1238-.L1234
	.long	.L1237-.L1234
	.long	.L1236-.L1234
	.long	.L1235-.L1234
	.long	.L1233-.L1234
	.section	.text._ZN2v88internal8compiler13CodeGenerator23AssembleArchInstructionEPNS1_11InstructionE
.L1262:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1264:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	$4, %r8d
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	sarq	$35, %rbx
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %r13
	movq	(%rsi,%rax,8), %r14
	movl	$51, %esi
	sarq	$35, %r13
	sarq	$35, %r14
	movl	%r14d, %ecx
	movl	%r14d, %edx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	%ebx, %esi
	movl	$8, %ecx
	movq	%r15, %rdi
	movabsq	$81604378625, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	subq	$8, %rsp
	movl	%r13d, %edx
	movl	%r13d, %esi
	pushq	$23
	movl	$56, %r9d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	$15, %r8d
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%ebx, %edx
	movl	%r14d, %ecx
	movl	$4, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5cmovqENS0_9ConditionENS0_8RegisterES3_@PLT
	popq	%r11
	popq	%rbx
	.p2align 4,,10
	.p2align 3
.L1231:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3649
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1268:
	.cfi_restore_state
	movq	40(%rsi), %rbx
	addq	$200, %r15
	sarq	$35, %rbx
	cmpl	$408, %r13d
	je	.L2683
	cmpl	$407, %r13d
	movl	$27, %eax
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$177, %r13d
	movq	%r15, %rdi
	cmovne	%eax, %r13d
	movl	%r13d, %ecx
	call	_ZN2v88internal9Assembler7pshuflwENS0_11XMMRegisterES2_h@PLT
	movl	%r13d, %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler7pshufhwENS0_11XMMRegisterES2_h@PLT
.L2683:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %esi
	movl	$8, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrlwENS0_11XMMRegisterEh@PLT
	movl	$8, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psllwENS0_11XMMRegisterEh@PLT
	movl	%r13d, %edx
	movl	$235, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1433:
	movl	4(%rsi), %eax
	addq	$200, %r15
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %rbx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2017
	pushq	$0
	movl	$1, %r9d
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	pushq	$1
	movl	%ebx, %edx
	movl	$118, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%r9
	popq	%r10
.L2018:
	movl	%ebx, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler5PsrlqENS0_11XMMRegisterEh@PLT
	movq	-10472(%rbp), %rax
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2019
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	%ebx, %r8d
	movl	$84, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	.p2align 4,,10
	.p2align 3
.L1716:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1661:
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	cmpl	$1, %r13d
	je	.L3650
.L1675:
	movq	40(%r11,%rdx,8), %rax
	leaq	200(%r15), %r12
	andl	$7, %eax
	cmpl	$3, %eax
	je	.L3651
	movq	-10472(%rbp), %rax
	movq	%r12, %rdi
	movq	%r11, -10504(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rbx
	sarq	$35, %rbx
	movl	%ebx, %esi
	movl	%ebx, %edx
	call	_ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_@PLT
	movq	-10504(%rbp), %r11
	movl	%ebx, %esi
	movq	%r12, %rdi
	movl	(%r11), %eax
	testl	$268435456, %eax
	je	.L1678
	call	_ZN2v88internal14TurboAssembler13RetpolineJumpENS0_8RegisterE@PLT
.L1677:
	movq	24(%r15), %rax
	movb	$1, 113(%r15)
	xorl	%r13d, %r13d
	movl	$0, 12(%rax)
	movq	24(%r15), %rdi
	call	_ZN2v88internal8compiler16FrameAccessState23SetFrameAccessToDefaultEv@PLT
	jmp	.L1231
.L1432:
	movl	4(%rsi), %eax
	addq	$200, %r15
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %rbx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2020
	pushq	$0
	movl	%ebx, %r8d
	movq	%r15, %rdi
	movl	$1, %r9d
	pushq	$1
	movl	%ebx, %ecx
	movl	%ebx, %edx
	movl	$118, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rdi
	popq	%r8
.L2021:
	movl	%ebx, %esi
	movl	$63, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler5PsllqENS0_11XMMRegisterEh@PLT
	movq	-10472(%rbp), %rax
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2022
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	%ebx, %r8d
	movl	$87, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L1716
.L1501:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2043
	testb	$24, %al
	jne	.L2043
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2043
	movq	%xmm2, -10504(%rbp)
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	movq	-10504(%rbp), %r11
.L2044:
	testb	$-2, 4(%r11)
	je	.L1716
	movq	-10472(%rbp), %rax
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%r11, -10504(%rbp)
	movq	48(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	movl	$1567, %edx
	movq	%r12, %rdi
	movq	$0, -10488(%rbp)
	salq	$53, %rdx
	movq	$0, -10464(%rbp)
	movl	%r13d, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEm@PLT
	movq	-10504(%rbp), %r11
	movzbl	4(%r11), %eax
	movq	40(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L2046
	testb	$24, %al
	jne	.L2046
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2046
	movq	-10472(%rbp), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomisdES3_S3_EEXadL_ZNS6_7ucomisdES3_S3_EEEEvS3_S3_.isra.0
.L2047:
	leaq	-10464(%rbp), %r14
	leaq	-10488(%rbp), %r13
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r14, %rdx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-10472(%rbp), %rax
	movl	$7, %esi
	movq	%r12, %rdi
	movabsq	$81604378625, %rcx
	movl	$8, %r8d
	movq	40(%rax), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-10472(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	48(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L1231
.L1502:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2036
	testb	$24, %al
	jne	.L2036
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2036
	movq	%xmm2, -10504(%rbp)
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	movq	-10504(%rbp), %r11
.L2037:
	testb	$-2, 4(%r11)
	je	.L1716
	movq	-10472(%rbp), %rax
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%r11, -10504(%rbp)
	movq	48(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	movl	$-553648128, %edx
	movq	%r12, %rdi
	movq	$0, -10488(%rbp)
	movq	$0, -10464(%rbp)
	movl	%r13d, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEj@PLT
	movq	-10504(%rbp), %r11
	movzbl	4(%r11), %eax
	movq	40(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L2039
	testb	$24, %al
	jne	.L2039
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2039
	movq	-10472(%rbp), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomissES3_S3_EEXadL_ZNS6_7ucomissES3_S3_EEEEvS3_S3_.isra.0
.L2040:
	leaq	-10464(%rbp), %r14
	leaq	-10488(%rbp), %r13
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r14, %rdx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-10472(%rbp), %rax
	movl	$7, %esi
	movq	%r12, %rdi
	movabsq	$81604378625, %rcx
	movl	$8, %r8d
	movq	40(%rax), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-10472(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	48(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L1231
.L1503:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2033
	testb	$24, %al
	jne	.L2033
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2033
	movq	%xmm2, -10504(%rbp)
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	movq	-10504(%rbp), %r11
.L2034:
	movl	(%r11), %eax
	shrl	$22, %eax
	je	.L1716
	movq	-10472(%rbp), %rax
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler18AssertZeroExtendedENS0_8RegisterE@PLT
	jmp	.L1231
.L1338:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$238, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1342:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$249, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1343:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	movl	$56, %r9d
	leaq	200(%rdi), %rdi
	movl	$15, %r8d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$1
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler11ssse3_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r9
	popq	%r10
	jmp	.L1231
.L1344:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$237, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1345:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$253, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1340:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$213, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1341:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$233, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1339:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$234, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1346:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$107, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1347:
	movl	4(%rsi), %eax
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rdx,%rax), %rax
	movq	48(%rsi,%rdx,8), %rdx
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %rdx
	sarq	$35, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	$225, %r9d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1348:
	movl	4(%rsi), %eax
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rdx,%rax), %rax
	movq	48(%rsi,%rdx,8), %rdx
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %rdx
	sarq	$35, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	$241, %r9d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1349:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %r13
	sarq	$35, %rbx
	sarq	$35, %r13
	cmpl	%ebx, %r13d
	je	.L3652
	movl	$239, %r9d
	movl	$15, %r8d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	$249, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1350:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	movl	$8, %ecx
	addq	$200, %r15
	movq	%r15, %rdi
	movq	40(%rsi,%rax,8), %rdx
	sarq	$35, %rbx
	movl	%ebx, %esi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler7palignrENS0_11XMMRegisterES2_h@PLT
	subq	$8, %rsp
	movl	$102, %ecx
	movl	%ebx, %edx
	pushq	$32
	movl	$56, %r9d
	movl	$15, %r8d
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r13
	xorl	%r13d, %r13d
	popq	%r14
	jmp	.L1231
.L1352:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$7, %rax
	movq	(%rsi,%rax,8), %rsi
	leaq	-8(%r11,%rax,8), %r8
	testb	$4, %sil
	je	.L2405
	movq	%rsi, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2405
	movq	%rsi, %rax
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2405
	leaq	-10480(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r12, %rdi
	movq	%rdx, %rcx
	movzbl	4(%rax), %edx
	movq	40(%rax), %rsi
	movsbl	%cl, %ecx
	movq	56(%rax,%rdx,8), %rdx
	sarq	$35, %rsi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler6pinsrwENS0_11XMMRegisterENS0_8RegisterEa@PLT
	jmp	.L1231
.L1353:
	movq	40(%rsi), %rbx
	movzbl	4(%rsi), %esi
	leaq	200(%rdi), %r12
	movq	40(%rdi), %rcx
	movq	48(%r11,%rsi,8), %rax
	sarq	$35, %rbx
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3653
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2400
	movq	%rdi, %rcx
	jmp	.L2401
.L3654:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2404:
	testq	%rax, %rax
	je	.L2402
.L2401:
	cmpl	32(%rax), %edx
	jle	.L3654
	movq	24(%rax), %rax
	jmp	.L2404
.L1351:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	leaq	200(%rdi), %rdi
	movl	$56, %r9d
	movl	$15, %r8d
	movq	40(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$32
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r15
	popq	%rax
	jmp	.L1231
.L1354:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	sarq	$35, %rbx
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2393
	testb	$24, %al
	jne	.L2393
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2393
	movl	%esi, %edx
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler4movdENS0_11XMMRegisterENS0_8RegisterE@PLT
.L2394:
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7pshuflwENS0_11XMMRegisterES2_h@PLT
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler6pshufdENS0_11XMMRegisterES2_h@PLT
	jmp	.L1231
.L1355:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	$56, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	movq	48(%rsi,%rax,8), %r13
	pushq	$59
	sarq	$35, %rbx
	movl	%ebx, %esi
	sarq	$35, %r13
	movl	%r13d, %edx
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %edx
	movl	$118, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	popq	%rax
	popq	%rdx
	jmp	.L1231
.L1362:
	movl	4(%rsi), %eax
	movq	40(%rsi), %r14
	addq	$200, %r15
	movl	$239, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	%eax, %edx
	movzbl	%al, %eax
	sarq	$35, %r14
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movq	8(%rsi,%rax,8), %rbx
	movq	(%rsi,%rax,8), %r13
	sarq	$35, %rbx
	sarq	$35, %r13
	movl	%ebx, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	%ebx, %edx
	call	_ZN2v88internal9Assembler5maxpsENS0_11XMMRegisterES2_@PLT
	movl	$118, %r9d
	movq	%r15, %rdi
	movl	%ebx, %edx
	movl	$15, %r8d
	movl	$102, %ecx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movq	%r15, %rdi
	movl	$1, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler5psrldENS0_11XMMRegisterEh@PLT
	movq	%r15, %rdi
	movl	%ebx, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler8cvtdq2psENS0_11XMMRegisterES2_@PLT
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	%ebx, %edx
	call	_ZN2v88internal9Assembler5subpsENS0_11XMMRegisterES2_@PLT
	movl	$2, %ecx
	movl	%r13d, %edx
	movq	%r15, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler5cmppsENS0_11XMMRegisterES2_a@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler9cvttps2dqENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	$102, %ecx
	movl	$239, %r9d
	movl	$15, %r8d
	movl	%ebx, %edx
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movq	%r15, %rdi
	movl	$239, %r9d
	movl	%ebx, %edx
	movl	$15, %r8d
	movl	$102, %ecx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	subq	$8, %rsp
	movl	$102, %ecx
	movl	%ebx, %edx
	pushq	$61
	movl	$56, %r9d
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r14d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler9cvttps2dqENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	$254, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	popq	%r14
	popq	%r15
	jmp	.L1231
.L1363:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	$56, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	movq	48(%rsi,%rax,8), %r13
	pushq	$57
	sarq	$35, %rbx
	movl	%ebx, %esi
	sarq	$35, %r13
	movl	%r13d, %edx
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %edx
	movl	$118, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	popq	%rax
	popq	%rdx
	jmp	.L1231
.L1364:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$102, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1365:
	movl	4(%rsi), %eax
	addq	$200, %r15
	movl	$102, %ecx
	movl	$118, %r9d
	movl	$15, %r8d
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rdx,%rax), %rax
	movq	48(%rsi,%rdx,8), %rdx
	movq	(%rsi,%rax,8), %rbx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rbx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$118, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$239, %r9d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1366:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$118, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1367:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	leaq	200(%rdi), %rdi
	movl	$56, %r9d
	movl	$15, %r8d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$61
	sarq	$35, %rsi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L1231
.L1368:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	leaq	200(%rdi), %rdi
	movl	$15, %r8d
	movl	$56, %r9d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$57
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%rdi
	popq	%r8
	jmp	.L1231
.L1369:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	movl	$56, %r9d
	leaq	200(%rdi), %rdi
	movl	$15, %r8d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$64
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r9
	popq	%r10
	jmp	.L1231
.L1370:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$250, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1371:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	leaq	200(%rdi), %rdi
	movl	$56, %r9d
	movl	$15, %r8d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$2
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler11ssse3_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r11
	popq	%rbx
	jmp	.L1231
.L1372:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$254, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1373:
	movl	4(%rsi), %eax
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rdx,%rax), %rax
	movq	48(%rsi,%rdx,8), %rdx
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %rdx
	sarq	$35, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	$226, %r9d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1374:
	movl	4(%rsi), %eax
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rdx,%rax), %rax
	movq	48(%rsi,%rdx,8), %rdx
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %rdx
	sarq	$35, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	$242, %r9d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1375:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %r13
	sarq	$35, %rbx
	sarq	$35, %r13
	cmpl	%ebx, %r13d
	je	.L3655
	movl	$239, %r9d
	movl	$15, %r8d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	$250, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1376:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	movl	$8, %ecx
	xorl	%r13d, %r13d
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %rdx
	sarq	$35, %rbx
	movq	%r15, %rdi
	movl	%ebx, %esi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler7palignrENS0_11XMMRegisterES2_h@PLT
	subq	$8, %rsp
	movq	%r15, %rdi
	movl	%ebx, %edx
	pushq	$35
	movl	$56, %r9d
	movl	$15, %r8d
	movl	%ebx, %esi
	movl	$102, %ecx
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r15
	popq	%rax
	jmp	.L1231
.L1377:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	leaq	200(%rdi), %rdi
	movl	$56, %r9d
	movl	$15, %r8d
	movq	40(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$35
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%rax
	popq	%rdx
	jmp	.L1231
.L1358:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	movl	$56, %r9d
	leaq	200(%rdi), %rdi
	movl	$15, %r8d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$59
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r9
	popq	%r10
	jmp	.L1231
.L1359:
	movl	4(%rsi), %eax
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rdx,%rax), %rax
	movq	48(%rsi,%rdx,8), %rdx
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %rdx
	sarq	$35, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	$210, %r9d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1360:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	movl	$8, %ecx
	xorl	%r13d, %r13d
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %rdx
	sarq	$35, %rbx
	movq	%r15, %rdi
	movl	%ebx, %esi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler7palignrENS0_11XMMRegisterES2_h@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movl	%ebx, %esi
	pushq	$51
	movl	$56, %r9d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r11
	popq	%rbx
	jmp	.L1231
.L1361:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	leaq	200(%rdi), %rdi
	movl	$56, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	40(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$51
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r12
	popq	%r13
	xorl	%r13d, %r13d
	jmp	.L1231
.L1356:
	movl	4(%rsi), %eax
	subq	$8, %rsp
	movq	40(%rsi), %rbx
	movl	$102, %ecx
	addq	$200, %r15
	movl	$56, %r9d
	movl	$15, %r8d
	movzbl	%al, %edx
	shrl	$8, %eax
	movq	%r15, %rdi
	movzwl	%ax, %eax
	movq	48(%rsi,%rdx,8), %r14
	sarq	$35, %rbx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %r13
	pushq	$63
	sarq	$35, %r14
	movl	%ebx, %esi
	movl	%r14d, %edx
	sarq	$35, %r13
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	$118, %r9d
	movl	%r14d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$102, %ecx
	movl	$118, %r9d
	movl	$15, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$102, %ecx
	movl	%r13d, %edx
	movl	%ebx, %esi
	movl	$239, %r9d
	movl	$15, %r8d
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L1231
.L1357:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	leaq	200(%rdi), %rdi
	movl	$15, %r8d
	movl	$56, %r9d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$63
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%rdi
	popq	%r8
	jmp	.L1231
.L1634:
	shrl	$9, %ebx
	addq	$200, %r15
	movl	%ebx, %r13d
	andl	$31, %r13d
	je	.L1724
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$0, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r9d
	movl	$59, %esi
	movq	%r15, %rdi
	movl	%edx, -104(%rbp)
	movq	%rdx, %r8
	movq	%rax, %rcx
	movl	%edx, -88(%rbp)
	movl	$4, %edx
	movq	%rax, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1267:
	movl	4(%rsi), %eax
	movq	40(%rsi), %r13
	movl	$8, %ecx
	addq	$200, %r15
	movq	%r15, %rdi
	movl	%eax, %edx
	movzbl	%al, %eax
	sarq	$35, %r13
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movabsq	$81604378625, %rdx
	movq	(%rsi,%rax,8), %r14
	movq	8(%rsi,%rax,8), %rbx
	sarq	$35, %r14
	sarq	$35, %rbx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movl	$51, %esi
	movl	$4, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$239, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	subq	$8, %rsp
	movl	$102, %ecx
	movl	%ebx, %esi
	movq	-10472(%rbp), %rax
	movl	$56, %r9d
	movl	$15, %r8d
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	pushq	$41
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	$56, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	$23, (%rsp)
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %edx
	movl	%r14d, %ecx
	movl	$4, %esi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler5cmovqENS0_9ConditionENS0_8RegisterES3_@PLT
	popq	%r9
	popq	%r10
	jmp	.L1231
.L1570:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3656
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3657
	testb	$4, %al
	je	.L1769
	testb	$24, %al
	jne	.L1769
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1769
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	movl	$58, %esi
	movq	%r12, %rdi
	movl	%eax, %ecx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler15arithmetic_op_8EhNS0_8RegisterES2_@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1571:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3658
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3659
	testb	$4, %al
	je	.L1775
	testb	$24, %al
	jne	.L1775
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1775
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	movl	$59, %esi
	movq	%r12, %rdi
	movl	%eax, %ecx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler16arithmetic_op_16EhNS0_8RegisterES2_@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1572:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3660
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3661
	testb	$4, %al
	je	.L1781
	testb	$24, %al
	jne	.L1781
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1781
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	movl	$59, %esi
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	%eax, %ecx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1578:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference21ieee754_tanh_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1580:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference21ieee754_sinh_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1581:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference20ieee754_sin_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1574:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3662
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3663
	testb	$4, %al
	je	.L1759
	testb	$24, %al
	jne	.L1759
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1759
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	movl	$35, %esi
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	%eax, %ecx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1575:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3664
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3665
	testb	$4, %al
	je	.L1763
	testb	$24, %al
	jne	.L1763
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1763
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	movl	$35, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	%eax, %ecx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1576:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3666
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3667
	testb	$4, %al
	je	.L1743
	testb	$24, %al
	jne	.L1743
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1743
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	movl	$3, %esi
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	%eax, %ecx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1577:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3668
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3669
	testb	$4, %al
	je	.L1747
	testb	$24, %al
	jne	.L1747
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1747
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	movl	$3, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	%eax, %ecx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1582:
	addq	$200, %r15
	movl	$2, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference20ieee754_pow_functionEv@PLT
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1584:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference22ieee754_log10_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1585:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference22ieee754_log1p_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1573:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3670
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3671
	testb	$4, %al
	je	.L1787
	testb	$24, %al
	jne	.L1787
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1787
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	movl	$59, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	%eax, %ecx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1579:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference20ieee754_tan_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1583:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference21ieee754_log2_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1586:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference20ieee754_log_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1587:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference22ieee754_expm1_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1588:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference20ieee754_exp_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1589:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference21ieee754_cosh_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1253:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4712(%rbp)
	movl	%r13d, %esi
	movl	%edx, -6836(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4720(%rbp)
	movq	%rax, -6844(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$35, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1254:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %r15
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r15, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4736(%rbp)
	movl	%ebx, %esi
	movl	%edx, -6860(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4744(%rbp)
	movq	%rax, -6868(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$43, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r8d
	movq	%r12, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1255:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %r15
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r15, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4760(%rbp)
	movl	%ebx, %esi
	movl	%edx, -6884(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4768(%rbp)
	movq	%rax, -6892(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$43, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r12, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1256:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4784(%rbp)
	movl	%r13d, %esi
	movl	%edx, -6908(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4792(%rbp)
	movq	%rax, -6916(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$43, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1257:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4808(%rbp)
	movl	%r13d, %esi
	movl	%edx, -6932(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4816(%rbp)
	movq	%rax, -6940(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$43, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1258:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %r15
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r15, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4832(%rbp)
	movl	%ebx, %esi
	movl	%edx, -6956(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4840(%rbp)
	movq	%rax, -6964(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$3, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r8d
	movq	%r12, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1259:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %r15
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r15, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4856(%rbp)
	movl	%ebx, %esi
	movl	%edx, -6980(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4864(%rbp)
	movq	%rax, -6988(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$3, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r12, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1260:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4880(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7004(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4888(%rbp)
	movq	%rax, -7012(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$3, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1261:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4904(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7028(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4912(%rbp)
	movq	%rax, -7036(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$3, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1263:
	movl	4(%rsi), %eax
	movq	40(%rsi), %r13
	movl	$8, %ecx
	addq	$200, %r15
	movq	%r15, %rdi
	movl	%eax, %edx
	movzbl	%al, %eax
	sarq	$35, %r13
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movabsq	$81604378625, %rdx
	movq	(%rsi,%rax,8), %r14
	movq	8(%rsi,%rax,8), %rbx
	sarq	$35, %r14
	sarq	$35, %rbx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movl	$51, %esi
	movl	$4, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$239, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$116, %r9d
	movl	%ebx, %esi
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	subq	$8, %rsp
	movl	$102, %ecx
	movl	%ebx, %edx
	pushq	$23
	movl	$56, %r9d
	movl	$15, %r8d
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %edx
	movl	%r14d, %ecx
	movl	$4, %esi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler5cmovqENS0_9ConditionENS0_8RegisterES3_@PLT
	popq	%rax
	popq	%rdx
	jmp	.L1231
.L1265:
	movl	4(%rsi), %eax
	movq	40(%rsi), %r13
	movl	$8, %ecx
	addq	$200, %r15
	movq	%r15, %rdi
	movl	%eax, %edx
	movzbl	%al, %eax
	sarq	$35, %r13
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movabsq	$81604378625, %rdx
	movq	(%rsi,%rax,8), %r14
	movq	8(%rsi,%rax,8), %rbx
	sarq	$35, %r14
	sarq	$35, %rbx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movl	$51, %esi
	movl	$4, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$239, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$117, %r9d
	movl	%ebx, %esi
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	subq	$8, %rsp
	movl	$102, %ecx
	movl	%ebx, %edx
	pushq	$23
	movl	$56, %r9d
	movl	$15, %r8d
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movl	$4, %esi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler5cmovqENS0_9ConditionENS0_8RegisterES3_@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L1231
.L1266:
	movl	4(%rsi), %eax
	movq	40(%rsi), %r13
	movl	$8, %ecx
	addq	$200, %r15
	movq	%r15, %rdi
	movl	%eax, %edx
	movzbl	%al, %eax
	sarq	$35, %r13
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movabsq	$81604378625, %rdx
	movq	(%rsi,%rax,8), %r14
	movq	8(%rsi,%rax,8), %rbx
	sarq	$35, %r14
	sarq	$35, %rbx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movl	$51, %esi
	movl	$4, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$239, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$118, %r9d
	movl	%ebx, %esi
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	subq	$8, %rsp
	movl	$102, %ecx
	movl	%ebx, %edx
	pushq	$23
	movl	$15, %r8d
	movl	$56, %r9d
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %edx
	movq	%r15, %rdi
	movl	%r14d, %ecx
	movl	$4, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler5cmovqENS0_9ConditionENS0_8RegisterES3_@PLT
	popq	%rdi
	popq	%r8
	jmp	.L1231
.L1282:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	%eax, %edx
	sarq	$35, %rbx
	shrl	$8, %edx
	movl	%ebx, %r13d
	cmpw	$2, %dx
	je	.L3672
.L2662:
	movq	%r15, %rdi
	movl	$16, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler5psrldENS0_11XMMRegisterEh@PLT
	subq	$8, %rsp
	movl	%r13d, %edx
	movq	%r15, %rdi
	pushq	$43
	movl	$56, %r9d
	movl	$15, %r8d
	movl	%ebx, %esi
	movl	$102, %ecx
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r14
	popq	%r15
	jmp	.L1231
.L1283:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	movq	40(%r15), %rcx
	movq	48(%rsi,%rax,8), %rax
	sarq	$35, %rbx
	movl	%eax, %edx
	movq	%rax, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L3673
	movq	112(%rcx), %rax
	movl	%edi, %edx
	leaq	104(%rcx), %rsi
	testq	%rax, %rax
	je	.L2638
	movq	%rsi, %rcx
	jmp	.L2639
.L3674:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2642:
	testq	%rax, %rax
	je	.L2640
.L2639:
	cmpl	32(%rax), %edx
	jle	.L3674
	movq	24(%rax), %rax
	jmp	.L2642
.L1286:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	addq	$6, %rax
	sarq	$35, %rbx
	movq	(%rsi,%rax,8), %rsi
	leaq	8(%r11,%rax,8), %r8
	testb	$4, %sil
	je	.L2557
	testb	$24, %sil
	jne	.L2557
	movq	%rsi, %rax
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3675
.L2557:
	movq	(%r8), %rax
	movq	40(%r15), %rcx
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3676
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2563
	movq	%rdi, %rcx
	jmp	.L2564
.L3677:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2567:
	testq	%rax, %rax
	je	.L2565
.L2564:
	cmpl	32(%rax), %edx
	jle	.L3677
	movq	24(%rax), %rax
	jmp	.L2567
.L1287:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	addq	$5, %rax
	sarq	$35, %rbx
	movq	(%rsi,%rax,8), %rsi
	leaq	8(%r11,%rax,8), %r8
	testb	$4, %sil
	je	.L2536
	testb	$24, %sil
	jne	.L2536
	movq	%rsi, %rax
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3678
.L2536:
	movq	(%r8), %rax
	movq	40(%r15), %rcx
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3679
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2542
	movq	%rdi, %rcx
	jmp	.L2543
.L3680:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2546:
	testq	%rax, %rax
	je	.L2544
.L2543:
	cmpl	32(%rax), %edx
	jle	.L3680
	movq	24(%rax), %rax
	jmp	.L2546
.L1288:
	movzbl	4(%rsi), %esi
	movq	40(%rdi), %rcx
	leaq	200(%rdi), %r12
	movq	56(%r11,%rsi,8), %rax
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3681
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2531
	movq	%rdi, %rcx
	jmp	.L2532
.L3682:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2535:
	testq	%rax, %rax
	je	.L2533
.L2532:
	cmpl	32(%rax), %edx
	jle	.L3682
	movq	24(%rax), %rax
	jmp	.L2535
.L1284:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	movq	40(%r15), %rcx
	movzbl	%al, %r8d
	sarq	$35, %rbx
	movq	48(%rsi,%r8,8), %rdx
	movl	%edx, %esi
	movq	%rdx, %rdi
	andl	$7, %esi
	shrq	$3, %rdi
	cmpl	$3, %esi
	je	.L3683
	movq	112(%rcx), %rax
	movl	%edi, %edx
	leaq	104(%rcx), %rsi
	testq	%rax, %rax
	je	.L2621
	movq	%rsi, %rcx
	jmp	.L2622
.L3684:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2625:
	testq	%rax, %rax
	je	.L2623
.L2622:
	cmpl	32(%rax), %edx
	jle	.L3684
	movq	24(%rax), %rax
	jmp	.L2625
.L1285:
	movzbl	4(%rsi), %esi
	movq	40(%rdi), %rcx
	leaq	200(%rdi), %r12
	movq	56(%r11,%rsi,8), %rax
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3685
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2612
	movq	%rdi, %rcx
	jmp	.L2613
.L3686:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2616:
	testq	%rax, %rax
	je	.L2614
.L2613:
	cmpl	32(%rax), %edx
	jle	.L3686
	movq	24(%rax), %rax
	jmp	.L2616
.L1289:
	movl	4(%rsi), %eax
	movq	40(%rdi), %rcx
	movzbl	%al, %esi
	movq	56(%r11,%rsi,8), %rdx
	movl	%edx, %edi
	movq	%rdx, %r8
	andl	$7, %edi
	shrq	$3, %r8
	cmpl	$3, %edi
	je	.L3687
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2507
	movq	%rdi, %rcx
	jmp	.L2508
.L3688:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2511:
	testq	%rax, %rax
	je	.L2509
.L2508:
	cmpl	32(%rax), %edx
	jle	.L3688
	movq	24(%rax), %rax
	jmp	.L2511
.L1290:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$5, %rax
	movq	(%rsi,%rax,8), %rsi
	leaq	8(%r11,%rax,8), %r8
	testb	$4, %sil
	je	.L2492
	movq	%rsi, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2492
	movq	%rsi, %rax
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3689
.L2492:
	movq	(%r8), %rax
	movq	40(%r15), %rcx
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3690
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2497
	movq	%rdi, %rcx
	jmp	.L2498
.L3691:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2501:
	testq	%rax, %rax
	je	.L2499
.L2498:
	cmpl	32(%rax), %edx
	jle	.L3691
	movq	24(%rax), %rax
	jmp	.L2501
.L1294:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$235, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1295:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$219, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1296:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %r13
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %rbx
	sarq	$35, %r13
	sarq	$35, %rbx
	cmpl	%r13d, %ebx
	je	.L3692
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$118, %r9d
	movq	%r15, %rdi
	movl	$15, %r8d
	movl	$102, %ecx
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %esi
	movl	$239, %r9d
	movl	%ebx, %edx
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1297:
	movq	40(%rsi), %rsi
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	sarq	$35, %rsi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1292:
	movzbl	4(%rsi), %eax
	addq	$200, %r15
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	movq	40(%rsi), %rbx
	movq	%r15, %rdi
	movq	48(%rsi,%rax,8), %rdx
	movl	%r13d, %esi
	sarq	$35, %rbx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movq	-10472(%rbp), %rax
	movl	%r13d, %esi
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	56(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5andpsENS0_11XMMRegisterES2_@PLT
	movl	%ebx, %esi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	56(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1293:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$239, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1291:
	movq	40(%rsi), %rax
	movl	_ZN2v88internalL3rspE(%rip), %ebx
	leaq	200(%rdi), %r12
	movq	%rsi, -10512(%rbp)
	movl	$8, %ecx
	movq	%r12, %rdi
	sarq	$35, %rax
	movq	%rax, -10504(%rbp)
	movl	4(%rsi), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movl	%ebx, %edx
	movq	(%rsi,%rax,8), %rax
	sarq	$35, %rax
	movl	%eax, %esi
	movq	%rax, -10528(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$4, %edx
	movl	$4, %esi
	movq	%r12, %rdi
	movabsq	$85899345904, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-10512(%rbp), %r11
	movl	4(%r11), %eax
	movl	%eax, %edx
	shrl	$8, %edx
	cmpw	$5, %dx
	je	.L3693
	movq	-10472(%rbp), %rdx
	movzbl	%al, %eax
	movq	40(%r11,%rax,8), %rax
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2454
	testb	$24, %al
	jne	.L2454
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3694
.L2454:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	%r11, -10512(%rbp)
	leaq	-96(%rbp), %r13
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	movq	%r13, %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movq	%rdx, -784(%rbp)
	movl	%r14d, %esi
	movl	%ecx, -776(%rbp)
	movq	%rdx, -796(%rbp)
	movl	%ecx, -788(%rbp)
	movq	%rdx, -808(%rbp)
	movl	%ecx, -800(%rbp)
	movq	%rdx, -8140(%rbp)
	movl	%ecx, -8132(%rbp)
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterENS0_7OperandE@PLT
	movq	-10512(%rbp), %r11
.L2455:
	leaq	-10464(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$5, %r15d
	movq	%rax, -10512(%rbp)
	leaq	-112(%rbp), %rdx
	movaps	%xmm0, -112(%rbp)
.L2472:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %ecx
	leaq	5(%r15,%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	-10480(%rbp), %rcx
	movq	40(%rcx), %r8
	movl	%eax, %ecx
	movq	%rax, %r9
	andl	$7, %ecx
	shrq	$3, %r9
	cmpl	$3, %ecx
	je	.L3695
	movq	112(%r8), %rax
	movl	%r9d, %ecx
	leaq	104(%r8), %rdi
	testq	%rax, %rax
	je	.L2461
	movq	%rdi, %rsi
	jmp	.L2462
	.p2align 4,,10
	.p2align 3
.L3696:
	movq	%rax, %rsi
	movq	16(%rax), %rax
.L2465:
	testq	%rax, %rax
	je	.L2463
.L2462:
	cmpl	32(%rax), %ecx
	jle	.L3696
	movq	24(%rax), %rax
	jmp	.L2465
.L1298:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	$218, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	movq	48(%rsi,%rax,8), %r13
	sarq	$35, %rbx
	movl	%ebx, %esi
	sarq	$35, %r13
	movl	%r13d, %edx
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	$116, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1299:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	$222, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movq	48(%rsi,%rdx,8), %r14
	movzwl	%ax, %eax
	sarq	$35, %rbx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %r13
	sarq	$35, %r14
	movl	%ebx, %esi
	movl	%r14d, %edx
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	sarq	$35, %r13
	movl	$102, %ecx
	movl	%ebx, %esi
	movl	$116, %r9d
	movl	$15, %r8d
	movl	%r14d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$102, %ecx
	movl	$116, %r9d
	movl	$15, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	$239, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1300:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$222, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1301:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$218, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1302:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movl	$104, %r9d
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r15d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	movl	%eax, %edx
	movzbl	%al, %eax
	sarq	$35, %rbx
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movl	%ebx, %edx
	movq	(%rsi,%rax,8), %r13
	movq	8(%rsi,%rax,8), %r14
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$96, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	sarq	$35, %r13
	movl	$102, %ecx
	movq	%r12, %rdi
	sarq	$35, %r14
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$8, %r8d
	movl	%r13d, %edx
	xorl	%esi, %esi
	movabsq	$81604378632, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	$102, %ecx
	movl	%r14d, %edx
	movl	%r15d, %esi
	movl	$209, %r9d
	movl	$15, %r8d
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$209, %r9d
	movl	%r14d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$103, %r9d
	movl	%r15d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1303:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$216, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1304:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$220, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1305:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	$117, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	%r13d, %edx
	movl	%r13d, %esi
	sarq	$35, %rbx
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrlwENS0_11XMMRegisterEh@PLT
	subq	$8, %rsp
	movl	%r13d, %edx
	movl	%ebx, %esi
	pushq	$58
	movl	$56, %r9d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %esi
	movl	$102, %ecx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$56, %r9d
	movl	$15, %r8d
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rdx
	movl	$58, (%rsp)
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %edx
	movl	%ebx, %esi
	movl	$103, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	popq	%r11
	popq	%rbx
	jmp	.L1231
.L1306:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	$56, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	movq	48(%rsi,%rax,8), %r13
	pushq	$56
	sarq	$35, %rbx
	movl	%ebx, %esi
	sarq	$35, %r13
	movl	%r13d, %edx
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %edx
	movl	$116, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	popq	%r12
	popq	%r13
	xorl	%r13d, %r13d
	jmp	.L1231
.L1307:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$100, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1308:
	movl	4(%rsi), %eax
	addq	$200, %r15
	movl	$102, %ecx
	movl	$116, %r9d
	movl	$15, %r8d
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rdx,%rax), %rax
	movq	48(%rsi,%rdx,8), %rdx
	movq	(%rsi,%rax,8), %rbx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rbx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$116, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$239, %r9d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1309:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$116, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1310:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	leaq	200(%rdi), %rdi
	movl	$56, %r9d
	movl	$15, %r8d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$60
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r14
	popq	%r15
	jmp	.L1231
.L1311:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	leaq	200(%rdi), %rdi
	movl	$56, %r9d
	movl	$15, %r8d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$56
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%rax
	popq	%rdx
	jmp	.L1231
.L1312:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movq	%r12, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	sarq	$35, %rbx
	movq	48(%rsi,%rdx,8), %r13
	leaq	5(%rdx,%rax), %rax
	movl	%ebx, %edx
	movq	(%rsi,%rax,8), %r14
	sarq	$35, %r13
	sarq	$35, %r14
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r15d
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	$8, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5psrlwENS0_11XMMRegisterEh@PLT
	movl	$8, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5psrlwENS0_11XMMRegisterEh@PLT
	movl	$8, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5psllwENS0_11XMMRegisterEh@PLT
	movl	$102, %ecx
	movl	%r15d, %edx
	movl	%r14d, %esi
	movl	$213, %r9d
	movl	$15, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$213, %r9d
	movl	%r13d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$8, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5psllwENS0_11XMMRegisterEh@PLT
	movl	$8, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5psrlwENS0_11XMMRegisterEh@PLT
	movl	$235, %r9d
	movl	%r14d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1313:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$232, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1538:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1884
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1884
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1884
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterES2_@PLT
	jmp	.L1231
.L1539:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1882
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1882
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1882
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterES2_@PLT
	jmp	.L1231
.L1540:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	leaq	48(%rsi,%rax,8), %rsi
	movq	40(%r11), %rax
	movl	(%rsi), %edx
	movl	%eax, %ecx
	andl	$7, %edx
	andl	$4, %ecx
	cmpl	$3, %edx
	je	.L3697
	movq	%rax, %rsi
	sarq	$35, %rsi
	testl	%ecx, %ecx
	je	.L1876
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1876
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1876
	movl	$4, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterEii@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1522:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1913
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1913
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1913
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler6sqrtssENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1523:
	movl	4(%rsi), %eax
	addq	$200, %r15
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %rbx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1910
	pushq	$0
	movl	$1, %r9d
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	pushq	$1
	movl	%ebx, %edx
	movl	$118, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%r11
	popq	%r12
.L1911:
	movl	%ebx, %esi
	movl	$31, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler5PsllqENS0_11XMMRegisterEh@PLT
	movq	-10472(%rbp), %rax
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1912
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	%ebx, %r8d
	movl	$87, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L1716
.L1554:
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler3cdqEv@PLT
	movq	-10472(%rbp), %rax
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rsi
	movl	$4, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler9emit_idivENS0_8RegisterEi@PLT
	jmp	.L1231
.L1555:
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler3cqoEv@PLT
	movq	-10472(%rbp), %rax
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rsi
	movl	$8, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler9emit_idivENS0_8RegisterEi@PLT
	jmp	.L1231
.L1514:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1930
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1930
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1930
	movq	-8(%rsi,%rcx), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5mulsdENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1515:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1928
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1928
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1928
	movq	-8(%rsi,%rcx), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5subsdENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1516:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1926
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1926
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1926
	movq	-8(%rsi,%rcx), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5addsdENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1546:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	leaq	48(%rsi,%rax,8), %rsi
	movq	40(%r11), %rax
	movl	(%rsi), %edx
	movl	%eax, %ecx
	andl	$7, %edx
	andl	$4, %ecx
	cmpl	$3, %edx
	je	.L3698
	movq	%rax, %rsi
	sarq	$35, %rsi
	testl	%ecx, %ecx
	je	.L1852
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1852
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1852
	movl	$4, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterEii@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1530:
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler6lfenceEv@PLT
	jmp	.L1231
.L1531:
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler6mfenceEv@PLT
	jmp	.L1231
.L1562:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3699
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3700
	testb	$4, %al
	je	.L1843
	testb	$24, %al
	jne	.L1843
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1843
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	movl	$51, %esi
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	%eax, %ecx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1563:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3701
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3702
	testb	$4, %al
	je	.L1847
	testb	$24, %al
	jne	.L1847
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1847
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	sarq	$35, %rdx
	cmpl	%eax, %edx
	je	.L3703
	movl	$8, %r8d
	movl	%eax, %ecx
	movl	$51, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
.L1849:
	xorl	%r13d, %r13d
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1564:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3704
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3705
	testb	$4, %al
	je	.L1835
	testb	$24, %al
	jne	.L1835
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1835
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	movl	$11, %esi
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	%eax, %ecx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1565:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3706
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3707
	testb	$4, %al
	je	.L1839
	testb	$24, %al
	jne	.L1839
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1839
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	movl	$11, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	%eax, %ecx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1508:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r14
	movq	$0, -10488(%rbp)
	movq	$0, -10464(%rbp)
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1982
	testb	$24, %al
	jne	.L1982
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1982
	movq	-8(%rsi,%rcx), %rsi
	movq	%r14, %rdi
	movq	%xmm2, -10504(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomisdES3_S3_EEXadL_ZNS6_7ucomisdES3_S3_EEEEvS3_S3_.isra.0
	movq	-10504(%rbp), %r11
.L1983:
	movq	-10472(%rbp), %rax
	movq	8(%r15), %rdi
	movq	40(%rax), %r12
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	sarq	$35, %r12
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L3708
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1988:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r11, -10504(%rbp)
	leaq	-10488(%rbp), %r13
	call	_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE@PLT
	movl	%r12d, 48(%rbx)
	leaq	8(%rbx), %rdx
	movq	%r14, %rdi
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaNE(%rip), %rax
	movl	$1, %ecx
	movl	$10, %esi
	movq	%rax, (%rbx)
	leaq	-10464(%rbp), %r12
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$7, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-10472(%rbp), %rax
	movq	-10504(%rbp), %r11
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1989
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r8d
	movl	$80, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	movq	-10504(%rbp), %r11
.L1990:
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movl	$4, %ecx
	movq	%r14, %rdi
	movabsq	$81604378625, %rdx
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-10504(%rbp), %r11
	movzbl	4(%r11), %eax
	movq	48(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L1991
	testb	$24, %al
	jne	.L1991
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1991
	movq	-10472(%rbp), %rcx
	movq	%r14, %rdi
	movzbl	4(%rcx), %eax
	addq	$6, %rax
	movq	(%rcx,%rax,8), %rdx
	movq	-8(%rcx,%rax,8), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S3_S3_EEXadL_ZNS6_5movsdES3_S3_EEEEvS3_S3_.isra.0
.L1992:
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	16(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L1231
.L1510:
	movzbl	4(%rsi), %eax
	shrl	$22, %ebx
	leaq	200(%rdi), %rdi
	movq	40(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2028
	pushq	$0
	movl	%edx, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	pushq	$3
	movl	$1, %r9d
	movl	$11, %esi
.L3629:
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	movq	232(%r15), %rax
	orl	$8, %ebx
	leaq	1(%rax), %rdx
	movq	%rdx, 232(%r15)
	movb	%bl, (%rax)
	popq	%rax
	popq	%rdx
	jmp	.L1716
.L1511:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2023
	testb	$24, %al
	jne	.L2023
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2023
	movq	40(%rsi), %rsi
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2024
	pushq	$0
	movl	%esi, %ecx
	movl	%edx, %r8d
	movl	$3, %r9d
	pushq	$1
	movl	%esi, %edx
	movq	%r12, %rdi
	movl	$81, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1542:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	leaq	48(%rsi,%rax,8), %rsi
	movq	40(%r11), %rax
	movl	(%rsi), %edx
	movl	%eax, %ecx
	andl	$7, %edx
	andl	$4, %ecx
	cmpl	$3, %edx
	je	.L3709
	movq	%rax, %rsi
	sarq	$35, %rsi
	testl	%ecx, %ecx
	je	.L1868
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1868
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1868
	movl	$4, %ecx
	movl	$7, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterEii@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1543:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	leaq	48(%rsi,%rax,8), %rsi
	movq	40(%r11), %rax
	movl	(%rsi), %edx
	movl	%eax, %ecx
	andl	$7, %edx
	andl	$4, %ecx
	cmpl	$3, %edx
	je	.L3710
	movq	%rax, %rsi
	sarq	$35, %rsi
	testl	%ecx, %ecx
	je	.L1872
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1872
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1872
	movl	$8, %ecx
	movl	$7, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterEii@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1526:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1902
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1902
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1902
	movq	-8(%rsi,%rcx), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5mulssENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1527:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1900
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1900
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1900
	movq	-8(%rsi,%rcx), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5subssENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1558:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3711
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1814
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1814
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1814
	movq	40(%r11), %rax
	movl	$4, %ecx
	movq	%r12, %rdi
	sarq	$35, %rax
	movl	%eax, %esi
	call	_ZN2v88internal9Assembler9emit_imulENS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1518:
	movzbl	4(%rsi), %eax
	shrl	$22, %ebx
	leaq	200(%rdi), %rdi
	movq	40(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1917
	pushq	$0
	movl	%edx, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	pushq	$3
	movl	$1, %r9d
	movl	$10, %esi
	jmp	.L3629
.L1519:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1920
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1920
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1920
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	jmp	.L1231
.L1550:
	movq	40(%rsi), %rax
	leaq	200(%rdi), %r12
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L1826
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1826
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1826
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_notENS0_8RegisterEi@PLT
	jmp	.L1231
.L1534:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1892
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1892
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1892
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterES2_@PLT
	jmp	.L1231
.L1566:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3712
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3713
	testb	$4, %al
	je	.L1793
	testb	$24, %al
	jne	.L1793
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1793
	movq	-8(%r11,%rdx), %rsi
	sarq	$35, %rax
	movq	%r12, %rdi
	movl	%eax, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterES2_@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1512:
	movl	$8, %r8d
	movl	$4, %edx
	movl	$5, %esi
	movabsq	$81604378632, %rcx
	leaq	200(%rdi), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	leaq	48(%r15), %rax
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	movq	%rax, -10528(%rbp)
	je	.L1936
	cmpb	$0, 112(%r15)
	je	.L3714
.L1936:
	movq	-10472(%rbp), %rax
	movl	_ZN2v88internalL3rspE(%rip), %ebx
	movzbl	4(%rax), %edx
	movl	%ebx, %esi
	movq	48(%rax,%rdx,8), %r13
	leaq	-148(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rax, -10504(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-148(%rbp), %r8
	movl	-140(%rbp), %edx
	sarq	$35, %r13
	movq	%r8, -136(%rbp)
	movl	%edx, -128(%rbp)
	movq	%r8, -124(%rbp)
	movl	%edx, -116(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1937
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, -104(%rbp)
	movl	%edx, %r9d
	pushq	$0
	movl	$17, %esi
	movq	%r14, %rdi
	pushq	$1
	pushq	$3
	movl	%edx, -88(%rbp)
	movl	%r13d, %edx
	movq	%r8, -112(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
.L1938:
	leaq	-96(%rbp), %r13
	xorl	%edx, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5fld_dENS0_7OperandE@PLT
	movq	-10472(%rbp), %rax
	movq	-10504(%rbp), %rdi
	movl	%ebx, %esi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %r12
	xorl	%edx, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-148(%rbp), %r8
	movl	-140(%rbp), %edx
	sarq	$35, %r12
	movq	%r8, -136(%rbp)
	movl	%edx, -128(%rbp)
	movq	%r8, -124(%rbp)
	movl	%edx, -116(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1939
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, -104(%rbp)
	movl	%edx, %r9d
	pushq	$0
	movl	$17, %esi
	movq	%r14, %rdi
	pushq	$1
	pushq	$3
	movl	%edx, -88(%rbp)
	movl	%r12d, %edx
	movq	%r8, -112(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
.L1940:
	xorl	%edx, %edx
	movl	%ebx, %esi
	leaq	-10464(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %edx
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5fld_dENS0_7OperandE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	$0, -10464(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5fpremEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler9fnstsw_axEv@PLT
	testb	$16, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1941
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4sahfEv@PLT
.L1942:
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$10, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4fstpEi@PLT
	xorl	%edx, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler6fstp_dENS0_7OperandE@PLT
	movq	-10504(%rbp), %rdi
	movl	%ebx, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rax
	movq	-148(%rbp), %r8
	movl	-140(%rbp), %r9d
	movq	40(%rax), %rsi
	movq	%r8, -136(%rbp)
	movl	%r9d, -128(%rbp)
	movq	%r8, -124(%rbp)
	sarq	$35, %rsi
	movl	%r9d, -116(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1944
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movq	%r14, %rdi
	pushq	$0
	movl	$16, %esi
	pushq	$1
	pushq	$3
	movq	%r8, -112(%rbp)
	movl	%r9d, -104(%rbp)
	movq	%r8, -96(%rbp)
	movl	%r9d, -88(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
.L1945:
	xorl	%esi, %esi
	movl	$8, %r8d
	movl	$4, %edx
	movq	%r14, %rdi
	movabsq	$81604378632, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L1946
	cmpb	$0, 112(%r15)
	je	.L3715
.L1946:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1541:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	leaq	48(%rsi,%rax,8), %rsi
	movq	40(%r11), %rax
	movl	(%rsi), %edx
	movl	%eax, %ecx
	andl	$7, %edx
	andl	$4, %ecx
	cmpl	$3, %edx
	je	.L3716
	movq	%rax, %rsi
	sarq	$35, %rsi
	testl	%ecx, %ecx
	je	.L1880
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1880
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1880
	movl	$8, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterEii@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1544:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	leaq	48(%rsi,%rax,8), %rsi
	movq	40(%r11), %rax
	movl	(%rsi), %edx
	movl	%eax, %ecx
	andl	$7, %edx
	andl	$4, %ecx
	cmpl	$3, %edx
	je	.L3717
	movq	%rax, %rsi
	sarq	$35, %rsi
	testl	%ecx, %ecx
	je	.L1860
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1860
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1860
	movl	$4, %ecx
	movl	$5, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterEii@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1545:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	leaq	48(%rsi,%rax,8), %rsi
	movq	40(%r11), %rax
	movl	(%rsi), %edx
	movl	%eax, %ecx
	andl	$7, %edx
	andl	$4, %ecx
	cmpl	$3, %edx
	je	.L3718
	movq	%rax, %rsi
	sarq	$35, %rsi
	testl	%ecx, %ecx
	je	.L1864
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1864
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1864
	movl	$8, %ecx
	movl	$5, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterEii@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1556:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	48(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L1822
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1822
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1822
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4mullENS0_8RegisterE@PLT
	jmp	.L1231
.L1557:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	48(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L1820
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1820
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1820
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_imulENS0_8RegisterEi@PLT
	jmp	.L1231
.L1548:
	movq	40(%rsi), %rax
	leaq	200(%rdi), %r12
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L1830
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1830
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1830
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_negENS0_8RegisterEi@PLT
	jmp	.L1231
.L1568:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3719
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3720
	testb	$4, %al
	je	.L1805
	testb	$24, %al
	jne	.L1805
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1805
	movq	-8(%r11,%rdx), %rsi
	sarq	$35, %rax
	movl	$4, %ecx
	movq	%r12, %rdi
	movl	%eax, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1552:
	addq	$200, %r15
	movl	$4, %r8d
	movl	$2, %ecx
	xorl	%r13d, %r13d
	movl	$2, %edx
	movl	$51, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rax
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rsi
	movl	$4, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_divENS0_8RegisterEi@PLT
	jmp	.L1231
.L1551:
	movq	40(%rsi), %rax
	leaq	200(%rdi), %r12
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L1824
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1824
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1824
	movl	$8, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_notENS0_8RegisterEi@PLT
	jmp	.L1231
.L1560:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3721
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3722
	testb	$4, %al
	je	.L1751
	testb	$24, %al
	jne	.L1751
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1751
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	movl	$43, %esi
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	%eax, %ecx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1547:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	leaq	48(%rsi,%rax,8), %rsi
	movq	40(%r11), %rax
	movl	(%rsi), %edx
	movl	%eax, %ecx
	andl	$7, %edx
	andl	$4, %ecx
	cmpl	$3, %edx
	je	.L3723
	movq	%rax, %rsi
	sarq	$35, %rsi
	testl	%ecx, %ecx
	je	.L1856
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1856
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1856
	movl	$8, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterEii@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	40(%rsi), %rax
	leaq	200(%rdi), %r12
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L1828
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1828
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1828
	movl	$8, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_negENS0_8RegisterEi@PLT
	jmp	.L1231
.L1567:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3724
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3725
	testb	$4, %al
	je	.L1799
	testb	$24, %al
	jne	.L1799
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1799
	movq	-8(%r11,%rdx), %rsi
	sarq	$35, %rax
	movq	%r12, %rdi
	movl	%eax, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5testwENS0_8RegisterES2_@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1553:
	addq	$200, %r15
	movl	$4, %r8d
	movl	$2, %ecx
	xorl	%r13d, %r13d
	movl	$2, %edx
	movl	$51, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rax
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rsi
	movl	$8, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_divENS0_8RegisterEi@PLT
	jmp	.L1231
.L1559:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3726
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1818
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1818
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1818
	movq	40(%r11), %rax
	movl	$8, %ecx
	movq	%r12, %rdi
	sarq	$35, %rax
	movl	%eax, %esi
	call	_ZN2v88internal9Assembler9emit_imulENS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1561:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3727
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3728
	testb	$4, %al
	je	.L1755
	testb	$24, %al
	jne	.L1755
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1755
	movq	-8(%r11,%rdx), %rdx
	sarq	$35, %rax
	movl	$43, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	%eax, %ecx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1569:
	shrl	$9, %ebx
	leaq	200(%rdi), %r12
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3729
	movzbl	4(%rsi), %eax
	leaq	48(,%rax,8), %rdx
	leaq	(%rsi,%rdx), %rsi
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3730
	testb	$4, %al
	je	.L1811
	testb	$24, %al
	jne	.L1811
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L1811
	movq	-8(%r11,%rdx), %rsi
	sarq	$35, %rax
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	%eax, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1524:
	movl	4(%rsi), %eax
	addq	$200, %r15
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %rbx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1907
	pushq	$0
	movl	$1, %r9d
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	pushq	$1
	movl	%ebx, %edx
	movl	$118, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%r13
	popq	%r14
.L1908:
	movl	%ebx, %esi
	movl	$33, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler5PsrlqENS0_11XMMRegisterEh@PLT
	movq	-10472(%rbp), %rax
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1909
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	%ebx, %r8d
	movl	$84, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L1716
.L1517:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1922
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1922
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1922
	movq	-8(%rsi,%rcx), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomisdES3_S3_EEXadL_ZNS6_7ucomisdES3_S3_EEEEvS3_S3_.isra.0
	jmp	.L1231
.L1532:
	movq	40(%rsi), %rsi
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler6bswaplENS0_8RegisterE@PLT
	jmp	.L1231
.L1509:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r14
	movq	$0, -10488(%rbp)
	movq	$0, -10464(%rbp)
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1947
	testb	$24, %al
	jne	.L1947
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1947
	movq	-8(%rsi,%rcx), %rsi
	movq	%r14, %rdi
	movq	%xmm2, -10504(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomissES3_S3_EEXadL_ZNS6_7ucomissES3_S3_EEEEvS3_S3_.isra.0
	movq	-10504(%rbp), %r11
.L1948:
	movq	-10472(%rbp), %rax
	movq	8(%r15), %rdi
	movq	40(%rax), %r12
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	sarq	$35, %r12
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L3731
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1953:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r11, -10504(%rbp)
	leaq	-10488(%rbp), %r13
	call	_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE@PLT
	movl	%r12d, 48(%rbx)
	leaq	8(%rbx), %rdx
	movq	%r14, %rdi
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaNE(%rip), %rax
	movl	$1, %ecx
	movl	$10, %esi
	movq	%rax, (%rbx)
	leaq	-10464(%rbp), %r12
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$7, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-10472(%rbp), %rax
	movq	-10504(%rbp), %r11
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1954
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r8d
	movl	$80, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	movq	-10504(%rbp), %r11
.L1955:
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movl	$4, %ecx
	movq	%r14, %rdi
	movabsq	$81604378625, %rdx
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-10504(%rbp), %r11
	movzbl	4(%r11), %eax
	movq	48(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L1956
	testb	$24, %al
	jne	.L1956
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1956
	movq	-10472(%rbp), %rcx
	movq	%r14, %rdi
	movzbl	4(%rcx), %eax
	addq	$6, %rax
	movq	(%rcx,%rax,8), %rdx
	movq	-8(%rcx,%rax,8), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovssES3_S3_S3_EEXadL_ZNS6_5movssES3_S3_EEEEvS3_S3_.isra.0
.L1957:
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	16(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L1231
.L1513:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1932
	testb	$24, %al
	jne	.L1932
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1932
	movq	-8(%rsi,%rcx), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5divsdENS0_11XMMRegisterES2_@PLT
.L2109:
	movq	-10472(%rbp), %rax
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2111
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %r8d
	movl	%esi, %edx
	movq	%r12, %rdi
	movl	$40, %esi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L1716
.L1528:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1898
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1898
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1898
	movq	-8(%rsi,%rcx), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5addssENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1529:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1894
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1894
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1894
	movq	-8(%rsi,%rcx), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomissES3_S3_EEXadL_ZNS6_7ucomissES3_S3_EEEEvS3_S3_.isra.0
	jmp	.L1231
.L1520:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1918
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1918
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1918
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_11XMMRegisterE@PLT
	jmp	.L1231
.L1536:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1888
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1888
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1888
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterES2_@PLT
	jmp	.L1231
.L1521:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1915
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1915
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1915
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1525:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1904
	testb	$24, %al
	jne	.L1904
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1904
	movq	-8(%rsi,%rcx), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5divssENS0_11XMMRegisterES2_@PLT
.L1905:
	movq	-10472(%rbp), %rax
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1533:
	movq	40(%rsi), %rsi
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler6bswapqENS0_8RegisterE@PLT
	jmp	.L1231
.L1535:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1890
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1890
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1890
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterES2_@PLT
	jmp	.L1231
.L1537:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1886
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1886
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1886
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterES2_@PLT
	jmp	.L1231
.L1314:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$248, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1379:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$7, %rax
	movq	(%rsi,%rax,8), %rsi
	leaq	-8(%r11,%rax,8), %r8
	testb	$4, %sil
	je	.L2381
	movq	%rsi, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2381
	movq	%rsi, %rax
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2381
	leaq	-10480(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r12, %rdi
	movq	%rdx, %rcx
	movzbl	4(%rax), %edx
	movq	40(%rax), %rsi
	movsbl	%cl, %ecx
	movq	56(%rax,%rdx,8), %rdx
	sarq	$35, %rsi
	sarq	$35, %rdx
	call	_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_8RegisterEa@PLT
	jmp	.L1231
.L1380:
	movzbl	4(%rsi), %esi
	movq	40(%rdi), %rcx
	leaq	200(%rdi), %r12
	movq	48(%r11,%rsi,8), %rax
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3732
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2376
	movq	%rdi, %rcx
	jmp	.L2377
.L3733:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2380:
	testq	%rax, %rax
	je	.L2378
.L2377:
	cmpl	32(%rax), %edx
	jle	.L3733
	movq	24(%rax), %rax
	jmp	.L2380
.L1654:
	movq	24(%rdi), %rax
	cmpb	$0, 16(%rax)
	jne	.L3734
.L1691:
	movb	$0, 8(%rax)
	xorl	%r13d, %r13d
	jmp	.L1231
.L1233:
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rax
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rbx
	movq	$2, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r8d
	movq	%r15, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L1231
.L1445:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2229
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2229
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2229
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovdES3_S4_EEXadL_ZNS7_4movdES3_S4_EEEEvS3_S4_.isra.0
	jmp	.L1231
.L1446:
	movzbl	4(%rsi), %eax
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %rdx
	testb	$4, %dl
	je	.L2226
	movq	%rdx, %rax
	shrq	$3, %rax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L3735
.L2226:
	movq	40(%r11), %rsi
	sarq	$35, %rdx
	movq	%r15, %rdi
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2227
	call	_ZN2v88internal9Assembler5vmovqENS0_8RegisterENS0_11XMMRegisterE@PLT
.L2228:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1453:
	movzbl	4(%rsi), %eax
	testl	%eax, %eax
	je	.L2185
	leaq	200(%rdi), %r12
	testb	$62, %bh
	jne	.L3736
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2195
	testb	$24, %al
	jne	.L2195
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2195
	movq	40(%rsi), %rax
	movq	%r12, %rdi
	sarq	$35, %rax
	movl	%eax, %esi
	call	_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterES2_@PLT
.L2198:
	shrl	$22, %ebx
	cmpl	$2, %ebx
	jne	.L1716
.L3634:
	leaq	-10480(%rbp), %r14
.L2133:
	movq	%r14, %rsi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal8compiler12_GLOBAL__N_129EmitWordLoadPoisoningIfNeededEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterE.isra.0.part.0
	jmp	.L1231
.L1449:
	shrl	$22, %ebx
	cmpl	$1, %ebx
	je	.L3737
.L2206:
	movzbl	4(%r11), %r13d
	addq	$200, %r15
	movq	$0, -10464(%rbp)
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	testl	%r13d, %r13d
	je	.L2207
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%rax, %r8
	movq	%rax, -5836(%rbp)
	movq	%rax, -8500(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -5828(%rbp)
	movq	40(%rax), %rsi
	movl	%edx, -8492(%rbp)
	movq	%r8, -124(%rbp)
	movl	%edx, -116(%rbp)
	sarq	$35, %rsi
	movq	%r8, -112(%rbp)
	movl	%edx, -104(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2208
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, -88(%rbp)
	movl	%edx, %r9d
	movq	%r15, %rdi
	movl	%esi, %edx
	movl	$16, %esi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
.L2209:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1451:
	shrl	$22, %ebx
	cmpl	$1, %ebx
	je	.L3738
.L2201:
	cmpb	$0, 4(%r11)
	leaq	200(%r15), %r12
	je	.L2202
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rax, %r9
	movl	%edx, -5840(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8504(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r9, -5848(%rbp)
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -8512(%rbp)
	movq	%r9, -96(%rbp)
	sarq	$35, %rsi
	movl	%eax, -88(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
.L2203:
	cmpl	$2, %ebx
	jne	.L1716
	jmp	.L2133
.L1452:
	movzbl	4(%rsi), %eax
	testl	%eax, %eax
	je	.L2185
	leaq	200(%rdi), %r12
	testb	$62, %bh
	jne	.L3739
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2199
	testb	$24, %al
	jne	.L2199
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2199
	movq	40(%rsi), %rax
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ecx
	movq	%r12, %rdi
	sarq	$35, %rax
	movl	%eax, %esi
	call	_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterES2_S2_@PLT
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L1457:
	cmpb	$0, 4(%rsi)
	je	.L2185
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	shrl	$22, %ebx
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	leaq	200(%r15), %rdi
	movq	%rax, %r8
	movq	%rax, -5908(%rbp)
	movl	%edx, %ecx
	movq	%rax, -8608(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -5900(%rbp)
	movq	40(%rax), %rsi
	movl	%edx, -8600(%rbp)
	movq	%r8, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterENS0_7OperandE@PLT
	cmpl	$2, %ebx
	jne	.L1716
	jmp	.L2133
.L1458:
	cmpb	$0, 4(%rsi)
	je	.L2185
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	shrl	$22, %ebx
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	leaq	200(%r15), %rdi
	movq	%rax, %r8
	movq	%rax, -5920(%rbp)
	movl	%edx, %ecx
	movq	%rax, -8620(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -5912(%rbp)
	movq	40(%rax), %rsi
	movl	%edx, -8612(%rbp)
	movq	%r8, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterENS0_7OperandE@PLT
	cmpl	$2, %ebx
	jne	.L1716
	jmp	.L2133
.L1447:
	movzbl	4(%rsi), %eax
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %rdx
	testb	$4, %dl
	je	.L2223
	movq	%rdx, %rax
	shrq	$3, %rax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L3740
.L2223:
	movq	40(%r11), %rsi
	sarq	$35, %rdx
	movq	%r15, %rdi
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2224
	call	_ZN2v88internal9Assembler5vmovdENS0_8RegisterENS0_11XMMRegisterE@PLT
.L2225:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1448:
	shrl	$22, %ebx
	cmpl	$1, %ebx
	je	.L3741
.L2217:
	movzbl	4(%r11), %r13d
	addq	$200, %r15
	movq	$0, -10464(%rbp)
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	testl	%r13d, %r13d
	je	.L2218
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%rax, %r8
	movl	%edx, -5792(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8456(%rbp)
	movq	-10472(%rbp), %rdx
	movq	%r8, -5800(%rbp)
	movq	40(%rdx), %rsi
	movq	%r8, -8464(%rbp)
	movq	%r8, -112(%rbp)
	movl	%eax, -104(%rbp)
	sarq	$35, %rsi
	movq	%r8, -96(%rbp)
	movl	%eax, -88(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2219
	movl	%eax, %ecx
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler7vmovdquENS0_11XMMRegisterENS0_7OperandE@PLT
.L2220:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1455:
	movzbl	4(%rsi), %r13d
	testl	%r13d, %r13d
	jne	.L3742
	leaq	-10480(%rbp), %r14
	movq	%rsi, -10504(%rbp)
	leaq	-10464(%rbp), %rsi
	addq	$200, %r15
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %r11
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movq	-10464(%rbp), %rax
	movzbl	4(%r11), %ecx
	movl	%edx, -88(%rbp)
	addq	$5, %rax
	addq	%rax, %rcx
	movq	(%r11,%rcx,8), %rcx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3743
	movq	-10472(%rbp), %rdi
	movzbl	4(%rdi), %ecx
	addq	%rcx, %rax
	movq	(%rdi,%rax,8), %rcx
	movq	%r15, %rdi
	sarq	$35, %rcx
	call	_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_8RegisterE@PLT
	jmp	.L1231
.L1454:
	movzbl	4(%rsi), %eax
	testl	%eax, %eax
	je	.L2185
	leaq	200(%rdi), %r12
	testb	$62, %bh
	jne	.L3744
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2191
	testb	$24, %al
	jne	.L2191
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2191
	movq	40(%rsi), %rax
	movq	%r12, %rdi
	sarq	$35, %rax
	movl	%eax, %esi
	call	_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterES2_@PLT
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L1450:
	shrl	$22, %ebx
	leaq	200(%rdi), %r12
	cmpl	$1, %ebx
	je	.L3745
	cmpb	$0, 4(%rsi)
	je	.L2214
	cmpl	$2, %ebx
	je	.L3746
.L2213:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movq	$0, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%rax, %r8
	movq	%rax, -5812(%rbp)
	movq	%rax, -8476(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -5804(%rbp)
	movq	40(%rax), %rsi
	movl	%edx, -8468(%rbp)
	movq	%r8, -136(%rbp)
	movl	%edx, -128(%rbp)
	sarq	$35, %rsi
	movq	%r8, -124(%rbp)
	movl	%edx, -116(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2215
	pushq	%rdi
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r9d
	movq	%r12, %rdi
	pushq	$0
	pushq	$1
	pushq	$3
	movl	%edx, -104(%rbp)
	movl	%edx, -88(%rbp)
	movl	%esi, %edx
	movl	$16, %esi
	movq	%r8, -112(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
.L2216:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1459:
	movl	%ebx, %r12d
	shrl	$22, %r12d
	cmpl	$1, %r12d
	je	.L3747
.L2179:
	leaq	200(%r15), %r13
	andb	$62, %bh
	jne	.L3748
	movq	-10472(%rbp), %rcx
	movzbl	4(%r11), %eax
	movzbl	4(%rcx), %edx
	movq	40(%r11,%rax,8), %rax
	movq	40(%rcx,%rdx,8), %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2182
	testb	$24, %al
	jne	.L2182
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2182
	movq	40(%rcx), %rsi
	movq	%r13, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterES2_@PLT
	.p2align 4,,10
	.p2align 3
.L3643:
	cmpl	$2, %r12d
	jne	.L1716
	jmp	.L3634
.L1460:
	shrl	$22, %ebx
	cmpl	$1, %ebx
	je	.L3749
.L2171:
	movzbl	4(%r11), %eax
	leaq	200(%r15), %r12
	testl	%eax, %eax
	je	.L2172
	testl	$15872, (%r11)
	jne	.L3750
	movq	40(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L2175
	testb	$24, %al
	jne	.L2175
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2175
	movq	-10472(%rbp), %rax
	movl	$4, %ecx
	movq	%r12, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax), %rsi
	movq	40(%rax,%rdx,8), %rdx
	sarq	$35, %rsi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
.L2174:
	movq	-10472(%rbp), %rax
	movq	%r12, %rdi
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler18AssertZeroExtendedENS0_8RegisterE@PLT
.L2176:
	cmpl	$2, %ebx
	jne	.L1716
	jmp	.L3634
.L1456:
	cmpb	$0, 4(%rsi)
	je	.L2185
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	shrl	$22, %ebx
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r8d
	leaq	200(%r15), %rdi
	movq	%rax, %r9
	movq	%rax, -5896(%rbp)
	movl	%edx, %ecx
	movq	%rax, -8596(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -5888(%rbp)
	movq	40(%rax), %rsi
	movl	%edx, -8588(%rbp)
	movq	%r9, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterENS0_7OperandES2_@PLT
	cmpl	$2, %ebx
	jne	.L1716
	jmp	.L2133
.L1461:
	shrl	$22, %ebx
	cmpl	$1, %ebx
	je	.L3751
.L2167:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	%r11, -10504(%rbp)
	movq	%r14, %rdi
	leaq	200(%r15), %r12
	movq	$0, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %r11
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movq	-10464(%rbp), %rax
	movzbl	4(%r11), %ecx
	movl	%edx, -88(%rbp)
	addq	$5, %rax
	addq	%rax, %rcx
	movq	(%r11,%rcx,8), %rcx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3752
	movq	-10472(%rbp), %rdi
	movzbl	4(%rdi), %ecx
	addq	%rcx, %rax
	movq	(%rdi,%rax,8), %rcx
	movq	%r12, %rdi
	sarq	$35, %rcx
	call	_ZN2v88internal9Assembler4movwENS0_7OperandENS0_8RegisterE@PLT
.L2169:
	cmpl	$2, %ebx
	je	.L3753
.L2170:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1462:
	movl	%ebx, %r12d
	shrl	$22, %r12d
	cmpl	$1, %r12d
	je	.L3754
.L2162:
	leaq	200(%r15), %r13
	andb	$62, %bh
	jne	.L3755
	movq	-10472(%rbp), %rcx
	movzbl	4(%r11), %eax
	movzbl	4(%rcx), %edx
	movq	40(%r11,%rax,8), %rax
	movq	40(%rcx,%rdx,8), %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2165
	testb	$24, %al
	jne	.L2165
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2165
	movq	40(%rcx), %rsi
	movq	%r13, %rdi
	movl	$8, %ecx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	-10472(%rbp), %rax
	movq	%r13, %rdi
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler18AssertZeroExtendedENS0_8RegisterE@PLT
	jmp	.L3643
.L1463:
	movl	%ebx, %eax
	shrl	$22, %eax
	cmpl	$1, %eax
	je	.L3756
.L2158:
	shrl	$9, %ebx
	addq	$200, %r15
	movl	%ebx, %r13d
	andl	$31, %r13d
	jne	.L3757
	movq	-10472(%rbp), %rcx
	movzbl	4(%r11), %eax
	movzbl	4(%rcx), %edx
	movq	40(%r11,%rax,8), %rax
	movq	40(%rcx,%rdx,8), %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2160
	testb	$24, %al
	jne	.L2160
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2160
	movq	40(%rcx), %rsi
	movq	%r15, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7movsxwqENS0_8RegisterES2_@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1464:
	movl	%ebx, %r12d
	shrl	$22, %r12d
	cmpl	$1, %r12d
	je	.L3758
.L2153:
	leaq	200(%r15), %r13
	andb	$62, %bh
	jne	.L3759
	movq	-10472(%rbp), %rcx
	movzbl	4(%r11), %eax
	movzbl	4(%rcx), %edx
	movq	40(%r11,%rax,8), %rax
	movq	40(%rcx,%rdx,8), %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2156
	testb	$24, %al
	jne	.L2156
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2156
	movq	40(%rcx), %rsi
	movq	%r13, %rdi
	movl	$4, %ecx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L1635:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$12, %ecx
	xorl	%r13d, %r13d
	movl	$8, %r8d
	movq	40(%rsi,%rax,8), %rdx
	movl	$35, %esi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1637:
	movzbl	4(%rsi), %eax
	shrl	$22, %ebx
	leaq	-10480(%rbp), %rdi
	movq	$0, -10464(%rbp)
	movl	%ebx, -10528(%rbp)
	movq	40(%rsi,%rax,8), %r12
	leaq	-10464(%rbp), %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10472(%rbp), %rsi
	movq	-10464(%rbp), %r8
	movl	%edx, -116(%rbp)
	movq	%rax, %rcx
	sarq	$35, %r12
	movq	%rax, -124(%rbp)
	movl	4(%rsi), %eax
	movzbl	%al, %edi
	shrl	$8, %eax
	movzwl	%ax, %eax
	addq	%rdi, %r8
	leaq	5(%rdi,%rax), %rax
	movq	%r15, %rdi
	movq	40(%rsi,%r8,8), %r14
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %r14
	sarq	$35, %rbx
	movq	%rbx, -10504(%rbp)
	movq	8(%rsi,%rax,8), %rbx
	movl	%edx, -104(%rbp)
	sarq	$35, %rbx
	movq	%rcx, -112(%rbp)
	movq	%rbx, -10512(%rbp)
	call	_ZNK2v88internal8compiler13CodeGenerator21DetermineStubCallModeEv@PLT
	movq	8(%r15), %rdi
	movl	%eax, %r13d
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rdx
	subq	%rbx, %rdx
	cmpq	$95, %rdx
	jbe	.L3760
	leaq	96(%rbx), %rdx
	movq	%rdx, 16(%rdi)
.L1728:
	movq	-112(%rbp), %rdx
	movd	%r14d, %xmm0
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movd	-10528(%rbp), %xmm3
	movd	-10504(%rbp), %xmm4
	addq	$200, %r15
	movd	-10512(%rbp), %xmm1
	movq	%rdx, -96(%rbp)
	punpckldq	%xmm4, %xmm0
	movl	-104(%rbp), %edx
	punpckldq	%xmm3, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movl	%edx, -88(%rbp)
	movaps	%xmm0, -10528(%rbp)
	call	_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE@PLT
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteE(%rip), %rax
	movl	%r12d, 48(%rbx)
	movl	%r14d, %ecx
	movq	%rax, (%rbx)
	movq	-96(%rbp), %rdx
	movdqa	-10528(%rbp), %xmm0
	movq	%rdx, 52(%rbx)
	movl	-88(%rbp), %edx
	movups	%xmm0, 64(%rbx)
	movl	%edx, 60(%rbx)
	movq	-192(%r15), %rdx
	movq	%r15, %rdi
	movl	%r13d, 80(%rbx)
	xorl	%r13d, %r13d
	movq	%rdx, 88(%rbx)
	movq	-124(%rbp), %rsi
	movl	-116(%rbp), %edx
	call	_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_8RegisterE@PLT
	subq	$8, %rsp
	movq	%r15, %rdi
	leaq	8(%rbx), %r9
	pushq	$1
	movl	-10504(%rbp), %edx
	movl	$4, %ecx
	movl	%r12d, %esi
	movl	$5, %r8d
	call	_ZN2v88internal14TurboAssembler13CheckPageFlagENS0_8RegisterES2_iNS0_9ConditionEPNS0_5LabelENS4_8DistanceE@PLT
	movq	%r15, %rdi
	leaq	16(%rbx), %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	popq	%r15
	popq	%rax
	jmp	.L1231
.L1638:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %r13
	movq	40(%rsi,%rax,8), %r14
	sarq	$35, %r13
	call	_ZNK2v88internal8compiler13CodeGenerator21DetermineStubCallModeEv@PLT
	movq	8(%r15), %rdi
	movl	%eax, %r12d
	sarq	$35, %r14
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$87, %rax
	jbe	.L3761
	leaq	88(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1726:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE@PLT
	movl	%r13d, 48(%rbx)
	movl	%r13d, %esi
	movl	%r14d, %edx
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToIE(%rip), %rax
	movl	%r14d, 52(%rbx)
	movq	%rax, (%rbx)
	leaq	48(%r15), %rax
	addq	$200, %r15
	movq	%rax, 64(%rbx)
	movq	-184(%r15), %rax
	movl	%r12d, 56(%rbx)
	movq	%rax, 72(%rbx)
	movq	-192(%r15), %rax
	movq	%r15, %rdi
	movq	%rax, 80(%rbx)
	call	_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	movl	%r13d, %edx
	movl	$7, %esi
	movq	%r15, %rdi
	movabsq	$81604378625, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	leaq	8(%rbx), %rdx
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	16(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$4, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1411:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler6haddpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1603:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5120(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7244(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5128(%rbp)
	movq	%rax, -7252(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$51, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movsxblENS0_8RegisterES2_@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1604:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %r15
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r15, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5144(%rbp)
	movl	%ebx, %esi
	movl	%edx, -7268(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5152(%rbp)
	movq	%rax, -7276(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$11, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r12, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1605:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5168(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7292(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5176(%rbp)
	movq	%rax, -7300(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$11, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$4, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1606:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5192(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7316(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5200(%rbp)
	movq	%rax, -7324(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$11, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movsxwlENS0_8RegisterES2_@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1607:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5216(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7340(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5224(%rbp)
	movq	%rax, -7348(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$11, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$4, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1608:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5240(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7364(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5248(%rbp)
	movq	%rax, -7372(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$11, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movsxblENS0_8RegisterES2_@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1609:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %r15
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r15, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5264(%rbp)
	movl	%ebx, %esi
	movl	%edx, -7388(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5272(%rbp)
	movq	%rax, -7396(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$35, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r12, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1610:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5288(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7412(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5296(%rbp)
	movq	%rax, -7420(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$35, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$4, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1611:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5312(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7436(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5320(%rbp)
	movq	%rax, -7444(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$35, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movsxwlENS0_8RegisterES2_@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1612:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5336(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7460(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5344(%rbp)
	movq	%rax, -7468(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$35, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$4, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1613:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5360(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7484(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5368(%rbp)
	movq	%rax, -7492(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$35, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movsxblENS0_8RegisterES2_@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1614:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %r15
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r15, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5384(%rbp)
	movl	%ebx, %esi
	movl	%edx, -7508(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5392(%rbp)
	movq	%rax, -7516(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$43, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r12, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1615:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5408(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7532(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5416(%rbp)
	movq	%rax, -7540(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$43, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$4, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1616:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5432(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7556(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5440(%rbp)
	movq	%rax, -7564(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$43, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movsxwlENS0_8RegisterES2_@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1617:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5456(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7580(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5464(%rbp)
	movq	%rax, -7588(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$43, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$4, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1618:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5480(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7604(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5488(%rbp)
	movq	%rax, -7612(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$43, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movsxblENS0_8RegisterES2_@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1619:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %r15
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r15, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5504(%rbp)
	movl	%ebx, %esi
	movl	%edx, -7628(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5512(%rbp)
	movq	%rax, -7636(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rcx
	movq	%r12, %rdi
	movl	4(%rcx), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movl	%ebx, %edx
	movq	(%rcx,%rax,8), %rsi
	movl	$4, %ecx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$3, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r12, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1620:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5528(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7652(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5536(%rbp)
	movq	%rax, -7660(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rcx
	movq	%r12, %rdi
	movl	4(%rcx), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movl	%r13d, %edx
	movq	(%rcx,%rax,8), %rsi
	movl	$4, %ecx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$3, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$4, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1621:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5552(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7676(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5560(%rbp)
	movq	%rax, -7684(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rcx
	movq	%r12, %rdi
	movl	4(%rcx), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movl	%r13d, %edx
	movq	(%rcx,%rax,8), %rsi
	movl	$4, %ecx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$3, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rcx), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rcx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movsxwlENS0_8RegisterES2_@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1622:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5576(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7700(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5584(%rbp)
	movq	%rax, -7708(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rcx
	movq	%r12, %rdi
	movl	4(%rcx), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movl	%r13d, %edx
	movq	(%rcx,%rax,8), %rsi
	movl	$4, %ecx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$3, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rcx), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rcx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$4, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1623:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5600(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7724(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5608(%rbp)
	movq	%rax, -7732(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rcx
	movq	%r12, %rdi
	movl	4(%rcx), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movl	%r13d, %edx
	movq	(%rcx,%rax,8), %rsi
	movl	$4, %ecx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$3, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rcx), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rcx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movsxblENS0_8RegisterES2_@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1624:
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rax
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rbx
	movq	$2, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r15, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L1231
.L1625:
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rax
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rbx
	movq	$2, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r15, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movl	%ebx, %ecx
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %ecx
	movq	%r15, %rdi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1626:
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rax
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rbx
	movq	$2, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r15, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movl	%ebx, %ecx
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%r15, %rdi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler7movsxwlENS0_8RegisterES2_@PLT
	jmp	.L1231
.L1627:
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rax
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rbx
	movq	$2, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r15, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movl	%ebx, %ecx
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %ecx
	movq	%r15, %rdi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1628:
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rax
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rbx
	movq	$2, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r15, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movl	%ebx, %ecx
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%r15, %rdi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler7movsxblENS0_8RegisterES2_@PLT
	jmp	.L1231
.L1629:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	leaq	200(%r15), %rdi
	movl	$4, %r8d
	movq	%rax, %r9
	movq	%rax, -5692(%rbp)
	movq	%rax, -7804(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -5684(%rbp)
	movzbl	4(%rax), %ecx
	movl	%edx, -7796(%rbp)
	movq	40(%rax,%rcx,8), %rsi
	movl	%edx, -88(%rbp)
	movl	%edx, %ecx
	movq	%r9, %rdx
	movq	%r9, -96(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler9emit_xchgENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1630:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	addq	$200, %r15
	movq	%rax, %r8
	movq	%rax, -5704(%rbp)
	movq	%r15, %rdi
	movq	%rax, -7816(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -5696(%rbp)
	movzbl	4(%rax), %ecx
	movl	%edx, -7808(%rbp)
	movq	40(%rax,%rcx,8), %rsi
	movl	%edx, %ecx
	movq	%r8, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5xchgwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rax
	movl	$4, %ecx
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rsi
	sarq	$35, %rsi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1631:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	addq	$200, %r15
	movq	%rax, %r8
	movq	%rax, -5716(%rbp)
	movq	%r15, %rdi
	movq	%rax, -7828(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -5708(%rbp)
	movzbl	4(%rax), %ecx
	movl	%edx, -7820(%rbp)
	movq	40(%rax,%rcx,8), %rsi
	movl	%edx, %ecx
	movq	%r8, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5xchgwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rax
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rsi
	sarq	$35, %rsi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler7movsxwlENS0_8RegisterES2_@PLT
	jmp	.L1231
.L1632:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	addq	$200, %r15
	movq	%rax, %r8
	movq	%rax, -5728(%rbp)
	movq	%r15, %rdi
	movq	%rax, -7840(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -5720(%rbp)
	movzbl	4(%rax), %ecx
	movl	%edx, -7832(%rbp)
	movq	40(%rax,%rcx,8), %rsi
	movl	%edx, %ecx
	movq	%r8, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5xchgbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rax
	movl	$4, %ecx
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rsi
	sarq	$35, %rsi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1633:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	addq	$200, %r15
	movq	%rax, %r8
	movq	%rax, -5740(%rbp)
	movq	%r15, %rdi
	movq	%rax, -7852(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -5732(%rbp)
	movzbl	4(%rax), %ecx
	movl	%edx, -7844(%rbp)
	movq	40(%rax,%rcx,8), %rsi
	movl	%edx, %ecx
	movq	%r8, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5xchgbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rax
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rsi
	sarq	$35, %rsi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler7movsxblENS0_8RegisterES2_@PLT
	jmp	.L1231
.L1378:
	movl	4(%rsi), %eax
	movq	40(%rsi), %r13
	addq	$200, %r15
	movq	%r15, %rdi
	movl	%eax, %edx
	movzbl	%al, %eax
	sarq	$35, %r13
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movl	%r13d, %edx
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5cmppsENS0_11XMMRegisterES2_a@PLT
	movl	%r13d, %esi
	movl	$219, %r9d
	movl	%ebx, %edx
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$239, %r9d
	movl	%r13d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler9cvttps2dqENS0_11XMMRegisterES2_@PLT
	movl	$219, %r9d
	movl	%r13d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$31, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psradENS0_11XMMRegisterEh@PLT
	movl	%r13d, %esi
	movl	$239, %r9d
	movl	%ebx, %edx
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1640:
	movq	40(%rsi), %rsi
	movl	_ZN2v88internalL3rbpE(%rip), %edx
	movl	$8, %ecx
	xorl	%r13d, %r13d
	leaq	200(%rdi), %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1641:
	movzbl	4(%rsi), %eax
	xorl	%r13d, %r13d
	leaq	40(%rsi,%rax,8), %rsi
	call	_ZN2v88internal8compiler13CodeGenerator14AssembleReturnEPNS1_18InstructionOperandE
	jmp	.L1231
.L1639:
	movq	24(%rdi), %rax
	leaq	200(%rdi), %r12
	cmpb	$0, 16(%rax)
	je	.L1723
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-112(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-112(%rbp), %rdx
	movl	-104(%rbp), %ecx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rax
	movl	$8, %r8d
	movq	40(%rax), %rsi
	movq	%rdx, -96(%rbp)
	movl	%ecx, -88(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1590:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference20ieee754_cos_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1646:
	movzbl	736(%rdi), %ebx
	movq	16(%rdi), %rax
	movl	$164, %esi
	xorl	%r13d, %r13d
	movb	$1, 736(%rdi)
	leaq	200(%rdi), %r12
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
	movb	%bl, 736(%r15)
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movb	$1, 113(%r15)
	jmp	.L1231
.L1269:
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	$8, %edx
	movq	%rsi, -10504(%rbp)
	movq	%r15, %rdi
	sarq	$35, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler5psrlwENS0_11XMMRegisterEh@PLT
	movq	-10504(%rbp), %r11
	movl	4(%r11), %eax
	movl	%eax, %edx
	shrl	$8, %edx
	cmpw	$1, %dx
	je	.L3762
	movq	-10472(%rbp), %rdx
	movzbl	%al, %eax
	movq	48(%r11,%rax,8), %rax
	movzbl	4(%rdx), %ecx
	movq	48(%rdx,%rcx,8), %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2680
	testb	$24, %al
	jne	.L2680
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3763
.L2680:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %ecx
	movq	%r15, %rdi
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movq	%rdx, -112(%rbp)
	movl	%r14d, %esi
	movl	%ecx, -104(%rbp)
	movq	%rdx, -124(%rbp)
	movl	%ecx, -116(%rbp)
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterENS0_7OperandE@PLT
.L2681:
	movl	$8, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrlwENS0_11XMMRegisterEh@PLT
.L2679:
	movl	$8, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler5psllwENS0_11XMMRegisterEh@PLT
	movl	$235, %r9d
	movl	%r14d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1662:
	movzbl	4(%rsi), %eax
	movq	%rsi, -10504(%rbp)
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rbx
	movl	%ebx, %eax
	andl	$7, %eax
	cmpl	$3, %eax
	je	.L3764
	sarq	$35, %rbx
	movq	%r12, %rdi
	movl	%ebx, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_@PLT
	movq	-10504(%rbp), %r11
	testl	$268435456, (%r11)
	je	.L1665
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler13RetpolineCallENS0_8RegisterE@PLT
	movq	-10504(%rbp), %r11
.L1664:
	movq	%r11, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator18RecordCallPositionEPNS1_11InstructionE@PLT
	movq	24(%r15), %rax
	movl	$0, 12(%rax)
	jmp	.L1231
.L1659:
	movzbl	4(%rsi), %eax
	movq	40(%rsi,%rax,8), %rsi
	movl	%esi, %eax
	andl	$7, %eax
	cmpl	$3, %eax
	je	.L3765
	sarq	$35, %rsi
	andl	$268435456, %ebx
	leaq	200(%rdi), %rdi
	je	.L1688
	call	_ZN2v88internal14TurboAssembler13RetpolineJumpENS0_8RegisterE@PLT
.L1689:
	movq	24(%r15), %rax
	movb	$1, 113(%r15)
	xorl	%r13d, %r13d
	movl	$0, 12(%rax)
	movq	24(%r15), %rdi
	call	_ZN2v88internal8compiler16FrameAccessState23SetFrameAccessToDefaultEv@PLT
	jmp	.L1231
.L1504:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2031
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2031
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2031
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_11XMMRegisterE@PLT
	jmp	.L1231
.L1505:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2029
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2029
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2029
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1595:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference22ieee754_asinh_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1642:
	movq	$-1, %r8
	xorl	%ecx, %ecx
	movl	$-1, %edx
	call	_ZN2v88internal8compiler13CodeGenerator16BuildTranslationEPNS1_11InstructionEimNS1_23OutputFrameStateCombineE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeGenerator23AssembleDeoptimizerCallEPNS1_18DeoptimizationExitE@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L1231
	movb	$1, 113(%r15)
	jmp	.L1231
.L1643:
	movb	$1, 113(%rdi)
	xorl	%r13d, %r13d
	jmp	.L1231
.L1644:
	movzbl	4(%rsi), %eax
	movq	40(%r15), %rcx
	movq	40(%rsi,%rax,8), %rax
	movl	%eax, %edx
	movq	%rax, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L3766
	movq	112(%rcx), %rax
	movl	%edi, %edx
	leaq	104(%rcx), %rsi
	testq	%rax, %rax
	je	.L1710
	movq	%rsi, %rcx
	jmp	.L1711
.L3767:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L1714:
	testq	%rax, %rax
	je	.L1712
.L1711:
	cmpl	32(%rax), %edx
	jle	.L3767
	movq	24(%rax), %rax
	jmp	.L1714
.L1645:
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	jmp	.L1231
.L1235:
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rax
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rbx
	movq	$2, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r15, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L1231
.L1236:
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rax
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rbx
	movq	$2, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r15, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movl	%ebx, %ecx
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %ecx
	movq	%r15, %rdi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1506:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r14
	movq	$0, -10488(%rbp)
	movq	$0, -10464(%rbp)
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1996
	testb	$24, %al
	jne	.L1996
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1996
	movq	-8(%rsi,%rcx), %rsi
	movq	%r14, %rdi
	movq	%xmm2, -10504(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomisdES3_S3_EEXadL_ZNS6_7ucomisdES3_S3_EEEEvS3_S3_.isra.0
	movq	-10504(%rbp), %r11
.L1997:
	movq	-10472(%rbp), %rax
	movq	8(%r15), %rdi
	movq	40(%rax), %r12
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	sarq	$35, %r12
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L3768
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L2002:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r11, -10504(%rbp)
	leaq	-10488(%rbp), %r13
	call	_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE@PLT
	movl	%r12d, 48(%rbx)
	leaq	8(%rbx), %rdx
	movq	%r14, %rdi
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaNE(%rip), %rax
	movl	$1, %ecx
	movl	$10, %esi
	movq	%rax, (%rbx)
	leaq	-10464(%rbp), %r12
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$7, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-10504(%rbp), %r11
	movzbl	4(%r11), %eax
	movq	48(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L2003
	testb	$24, %al
	jne	.L2003
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2003
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	movl	%edx, %r8d
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L3632
	movl	$10, %esi
	movq	%r14, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler8movmskpdENS0_8RegisterENS0_11XMMRegisterE@PLT
	movq	-10504(%rbp), %r11
.L2006:
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movl	$4, %ecx
	movq	%r14, %rdi
	movabsq	$81604378625, %rdx
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-10504(%rbp), %r11
	movzbl	4(%r11), %eax
	movq	48(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L2012
	testb	$24, %al
	jne	.L2012
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2012
	movq	-10472(%rbp), %rcx
	movq	%r14, %rdi
	movzbl	4(%rcx), %eax
	addq	$6, %rax
	movq	(%rcx,%rax,8), %rdx
	movq	-8(%rcx,%rax,8), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S3_S3_EEXadL_ZNS6_5movsdES3_S3_EEEEvS3_S3_.isra.0
.L2013:
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	16(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L1231
.L1507:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r14
	movq	$0, -10488(%rbp)
	movq	$0, -10464(%rbp)
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L1961
	testb	$24, %al
	jne	.L1961
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1961
	movq	-8(%rsi,%rcx), %rsi
	movq	%r14, %rdi
	movq	%xmm2, -10504(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler8vucomissES3_S3_EEXadL_ZNS6_7ucomissES3_S3_EEEEvS3_S3_.isra.0
	movq	-10504(%rbp), %r11
.L1962:
	movq	-10472(%rbp), %rax
	movq	8(%r15), %rdi
	movq	40(%rax), %r12
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	sarq	$35, %r12
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L3769
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1967:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r11, -10504(%rbp)
	leaq	-10488(%rbp), %r13
	call	_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE@PLT
	movl	%r12d, 48(%rbx)
	leaq	8(%rbx), %rdx
	movq	%r14, %rdi
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaNE(%rip), %rax
	movl	$1, %ecx
	movl	$10, %esi
	movq	%rax, (%rbx)
	leaq	-10464(%rbp), %r12
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$7, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-10504(%rbp), %r11
	movzbl	4(%r11), %eax
	movq	48(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L1968
	testb	$24, %al
	jne	.L1968
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1968
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	movl	%edx, %r8d
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L3631
	movl	$10, %esi
	movq	%r14, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler8movmskpsENS0_8RegisterENS0_11XMMRegisterE@PLT
	movq	-10504(%rbp), %r11
.L1971:
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movl	$4, %ecx
	movq	%r14, %rdi
	movabsq	$81604378625, %rdx
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-10504(%rbp), %r11
	movzbl	4(%r11), %eax
	movq	48(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L1977
	testb	$24, %al
	jne	.L1977
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L1977
	movq	-10472(%rbp), %rcx
	movq	%r14, %rdi
	movzbl	4(%rcx), %eax
	addq	$6, %rax
	movq	(%rcx,%rax,8), %rdx
	movq	-8(%rcx,%rax,8), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJS3_EE4emitIXadL_ZNS0_9Assembler6vmovssES3_S3_S3_EEXadL_ZNS6_5movssES3_S3_EEEEvS3_S3_.isra.0
.L1978:
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	16(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L1231
.L1591:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference21ieee754_cbrt_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1599:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %r15
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r15, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5024(%rbp)
	movl	%ebx, %esi
	movl	%edx, -7148(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5032(%rbp)
	movq	%rax, -7156(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$51, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r12, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1600:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5048(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7172(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5056(%rbp)
	movq	%rax, -7180(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$51, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$4, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1601:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5072(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7196(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5080(%rbp)
	movq	%rax, -7204(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$51, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movsxwlENS0_8RegisterES2_@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1602:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5096(%rbp)
	movl	%r13d, %esi
	movl	%edx, -7220(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5104(%rbp)
	movq	%rax, -7228(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$4, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$51, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$4, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1597:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference22ieee754_acosh_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1598:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference21ieee754_acos_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1596:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference21ieee754_asin_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1593:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference22ieee754_atanh_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1594:
	addq	$200, %r15
	movl	$1, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference21ieee754_atan_functionEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1592:
	addq	$200, %r15
	movl	$2, %esi
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	call	_ZN2v88internal17ExternalReference22ieee754_atan2_functionEv@PLT
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	jmp	.L1231
.L1330:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	$118, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	%r13d, %edx
	movl	%r13d, %esi
	sarq	$35, %rbx
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrldENS0_11XMMRegisterEh@PLT
	subq	$8, %rsp
	movl	%r13d, %edx
	movl	%ebx, %esi
	pushq	$59
	movl	$56, %r9d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %esi
	movl	$102, %ecx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$56, %r9d
	movl	$15, %r8d
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rdx
	movl	$59, (%rsp)
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %edx
	movl	$56, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movl	$43, (%rsp)
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%rax
	popq	%rdx
	jmp	.L1231
.L1331:
	movl	4(%rsi), %eax
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rdx,%rax), %rax
	movq	48(%rsi,%rdx,8), %rdx
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %rdx
	sarq	$35, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	$209, %r9d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1332:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	movl	$8, %ecx
	xorl	%r13d, %r13d
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %rdx
	sarq	$35, %rbx
	movq	%r15, %rdi
	movl	%ebx, %esi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler7palignrENS0_11XMMRegisterES2_h@PLT
	subq	$8, %rsp
	movl	$102, %ecx
	movl	%ebx, %esi
	pushq	$48
	movl	$56, %r9d
	movl	$15, %r8d
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L1231
.L1333:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	leaq	200(%rdi), %rdi
	movl	$15, %r8d
	movl	$56, %r9d
	movq	40(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$48
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%rdi
	popq	%r8
	jmp	.L1231
.L1237:
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rax
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rbx
	movq	$2, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r15, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movl	%ebx, %ecx
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %ecx
	movq	%r15, %rdi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1238:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	leaq	200(%r15), %rdi
	movl	$8, %r8d
	movq	%rax, %r9
	movq	%rax, -4984(%rbp)
	movq	%rax, -7096(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -4976(%rbp)
	movzbl	4(%rax), %ecx
	movl	%edx, -7088(%rbp)
	movq	40(%rax,%rcx,8), %rsi
	movl	%edx, -88(%rbp)
	movl	%edx, %ecx
	movq	%r9, %rdx
	movq	%r9, -96(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler9emit_xchgENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1239:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	leaq	200(%r15), %rdi
	movl	$4, %r8d
	movq	%rax, %r9
	movq	%rax, -4996(%rbp)
	movq	%rax, -7108(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -4988(%rbp)
	movzbl	4(%rax), %ecx
	movl	%edx, -7100(%rbp)
	movq	40(%rax,%rcx,8), %rsi
	movl	%edx, -88(%rbp)
	movl	%edx, %ecx
	movq	%r9, %rdx
	movq	%r9, -96(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler9emit_xchgENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1240:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	addq	$200, %r15
	movq	%rax, %r8
	movq	%rax, -5008(%rbp)
	movq	%r15, %rdi
	movq	%rax, -7120(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -5000(%rbp)
	movzbl	4(%rax), %ecx
	movl	%edx, -7112(%rbp)
	movq	40(%rax,%rcx,8), %rsi
	movl	%edx, %ecx
	movq	%r8, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5xchgwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rax
	movl	$8, %ecx
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rsi
	sarq	$35, %rsi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1241:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	addq	$200, %r15
	movq	%rax, %r8
	movq	%rax, -5020(%rbp)
	movq	%r15, %rdi
	movq	%rax, -7132(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -5012(%rbp)
	movzbl	4(%rax), %ecx
	movl	%edx, -7124(%rbp)
	movq	40(%rax,%rcx,8), %rsi
	movl	%edx, %ecx
	movq	%r8, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5xchgbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rax
	movl	$8, %ecx
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rsi
	sarq	$35, %rsi
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1242:
	addq	$200, %r15
	leaq	-10464(%rbp), %r12
	leaq	-10488(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-10480(%rbp), %r14
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$8, %r8d
	movq	%r15, %rdi
	movl	%edx, %ecx
	movl	%edx, -4448(%rbp)
	movl	%edx, -6572(%rbp)
	movl	%ebx, %esi
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4456(%rbp)
	movq	%rax, -6580(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	sarq	$35, %rcx
	sarq	$35, %rdx
	cmpl	%ecx, %edx
	je	.L3770
	movl	$8, %r8d
	movl	$51, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
.L2692:
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r8d
	movq	%r15, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1243:
	addq	$200, %r15
	leaq	-10464(%rbp), %r12
	leaq	-10488(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-10480(%rbp), %r14
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$4, %r8d
	movq	%r15, %rdi
	movl	%edx, %ecx
	movl	%edx, -4472(%rbp)
	movl	%edx, -6596(%rbp)
	movl	%ebx, %esi
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4480(%rbp)
	movq	%rax, -6604(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	sarq	$35, %rcx
	sarq	$35, %rdx
	cmpl	%ecx, %edx
	je	.L3771
	movl	$8, %r8d
	movl	$51, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
.L2690:
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r15, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1477:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rdx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2104
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2104
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2104
	movq	-8(%r11,%rdx), %rcx
	movq	40(%r11), %rdx
	pushq	$0
	movl	%esi, %r8d
	pushq	$1
	movl	$3, %r9d
	movl	$92, %esi
	movq	%r12, %rdi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%r11
	popq	%rbx
	jmp	.L1231
.L1249:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4616(%rbp)
	movl	%r13d, %esi
	movl	%edx, -6740(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4624(%rbp)
	movq	%rax, -6748(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$11, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1395:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$212, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1396:
	movl	4(%rsi), %eax
	addq	$200, %r15
	movq	40(%rsi), %r13
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	sarq	$35, %r13
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %r14
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %r14
	sarq	$35, %rbx
	movl	%r14d, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler6pextrqENS0_8RegisterENS0_11XMMRegisterEa@PLT
	movl	$8, %ecx
	movl	%ebx, %esi
	movq	%r15, %rdi
	movl	$7, %edx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterEii@PLT
	movl	%r13d, %esi
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6pinsrqENS0_11XMMRegisterENS0_8RegisterEa@PLT
	movl	$1, %ecx
	movl	%r14d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6pextrqENS0_8RegisterENS0_11XMMRegisterEa@PLT
	movl	$8, %ecx
	movl	%ebx, %esi
	movq	%r15, %rdi
	movl	$7, %edx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterEii@PLT
	movl	%r13d, %esi
	movl	$1, %ecx
	movl	%ebx, %edx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler6pinsrqENS0_11XMMRegisterENS0_8RegisterEa@PLT
	jmp	.L1231
.L1397:
	movl	4(%rsi), %eax
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rdx,%rax), %rax
	movq	48(%rsi,%rdx,8), %rdx
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %rdx
	sarq	$35, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	$243, %r9d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1398:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %rdx
	sarq	$35, %rbx
	sarq	$35, %rdx
	movl	%edx, %r13d
	cmpl	%ebx, %edx
	je	.L3772
.L2367:
	movl	$239, %r9d
	movl	$15, %r8d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	$251, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1399:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$7, %rax
	movq	(%rsi,%rax,8), %rsi
	leaq	-8(%r11,%rax,8), %r8
	testb	$4, %sil
	je	.L2356
	movq	%rsi, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2356
	movq	%rsi, %rax
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2356
	leaq	-10480(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r12, %rdi
	movq	%rdx, %rcx
	movzbl	4(%rax), %edx
	movq	40(%rax), %rsi
	movsbl	%cl, %ecx
	movq	56(%rax,%rdx,8), %rdx
	sarq	$35, %rsi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler6pinsrqENS0_11XMMRegisterENS0_8RegisterEa@PLT
	jmp	.L1231
.L1322:
	movq	40(%rsi), %rbx
	movzbl	4(%rsi), %esi
	leaq	200(%rdi), %r12
	movq	40(%rdi), %rcx
	movq	48(%r11,%rsi,8), %rax
	sarq	$35, %rbx
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3773
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2424
	movq	%rdi, %rcx
	jmp	.L2425
.L3774:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2428:
	testq	%rax, %rax
	je	.L2426
.L2425:
	cmpl	32(%rax), %edx
	jle	.L3774
	movq	24(%rax), %rax
	jmp	.L2428
.L1469:
	movl	%ebx, %r12d
	shrl	$22, %r12d
	cmpl	$1, %r12d
	je	.L3775
.L2128:
	leaq	200(%r15), %r13
	andb	$62, %bh
	jne	.L3776
	movq	-10472(%rbp), %rcx
	movzbl	4(%r11), %eax
	movzbl	4(%rcx), %edx
	movq	40(%r11,%rax,8), %rax
	movq	40(%rcx,%rdx,8), %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2131
	testb	$24, %al
	jne	.L2131
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2131
	movq	40(%rcx), %rsi
	movq	%r13, %rdi
	movl	$4, %ecx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L1470:
	movl	%ebx, %r12d
	shrl	$22, %r12d
	cmpl	$1, %r12d
	je	.L3777
.L2123:
	leaq	200(%r15), %r13
	andb	$62, %bh
	jne	.L3778
	movq	-10472(%rbp), %rcx
	movzbl	4(%r11), %eax
	movzbl	4(%rcx), %edx
	movq	40(%r11,%rax,8), %rax
	movq	40(%rcx,%rdx,8), %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2126
	testb	$24, %al
	jne	.L2126
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2126
	movq	40(%rcx), %rsi
	movq	%r13, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7movsxblENS0_8RegisterES2_@PLT
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L1471:
	movl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	%rsi, -10504(%rbp)
	movl	$1, %r9d
	movq	%r12, %rdi
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rbx
	pushq	$0
	movl	$118, %esi
	pushq	$1
	sarq	$35, %rbx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	movl	%ebx, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	movl	_ZN2v88internalL4xmm6E(%rip), %edx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	movl	$115, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	movq	232(%r15), %rax
	movq	-10504(%rbp), %r11
	leaq	1(%rax), %rdx
	movq	%rdx, 232(%r15)
	movb	$31, (%rax)
	movzbl	4(%r11), %eax
	popq	%r15
	popq	%rdx
	movq	40(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L2114
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2114
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2114
	movq	-10472(%rbp), %rax
	movl	%ebx, %ecx
	movl	$87, %esi
	movq	%r12, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %r8
	movq	40(%rax), %rdx
	sarq	$35, %r8
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L1231
.L1472:
	movl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	%rsi, -10504(%rbp)
	movl	$1, %r9d
	movq	%r12, %rdi
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rbx
	pushq	$0
	movl	$118, %esi
	pushq	$1
	sarq	$35, %rbx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	movl	%ebx, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	movl	_ZN2v88internalL4xmm2E(%rip), %edx
	movl	%ebx, %ecx
	movl	%ebx, %r8d
	movl	$115, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	movq	232(%r15), %rax
	movq	-10504(%rbp), %r11
	leaq	1(%rax), %rdx
	movq	%rdx, 232(%r15)
	movb	$33, (%rax)
	movzbl	4(%r11), %eax
	popq	%rcx
	popq	%rsi
	movq	40(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L2112
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2112
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2112
	movq	-10472(%rbp), %rax
	movl	%ebx, %ecx
	movl	$84, %esi
	movq	%r12, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %r8
	movq	40(%rax), %rdx
	sarq	$35, %r8
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L1231
.L1473:
	movl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	%rsi, -10504(%rbp)
	movl	$1, %r9d
	movq	%r12, %rdi
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rbx
	pushq	$0
	movl	$118, %esi
	pushq	$1
	sarq	$35, %rbx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	movl	%ebx, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	movl	_ZN2v88internalL4xmm6E(%rip), %edx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	movl	$115, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	movq	232(%r15), %rax
	movq	-10504(%rbp), %r11
	leaq	1(%rax), %rdx
	movq	%rdx, 232(%r15)
	movb	$63, (%rax)
	movzbl	4(%r11), %eax
	popq	%r10
	movq	40(%r11,%rax,8), %rax
	popq	%r11
	testb	$4, %al
	je	.L2118
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2118
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2118
	movq	-10472(%rbp), %rax
	movl	%ebx, %ecx
	movl	$87, %esi
	movq	%r12, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %r8
	movq	40(%rax), %rdx
	sarq	$35, %r8
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L1231
.L1274:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movl	%eax, %edx
	sarq	$35, %rbx
	movzbl	%al, %eax
	shrl	$8, %edx
	cmpw	$2, %dx
	sete	%dl
	movzbl	%dl, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2654
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2654
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3779
.L2654:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	subq	$8, %rsp
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	pushq	$108
	movl	$15, %r9d
	movl	$102, %r8d
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterENS0_7OperandEhhh@PLT
	popq	%r9
	popq	%r10
	jmp	.L1231
.L1427:
	movzbl	4(%rsi), %eax
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	addq	$200, %r15
	movq	40(%rsi), %rbx
	movq	%r15, %rdi
	movq	48(%rsi,%rax,8), %r14
	movl	%r13d, %esi
	sarq	$35, %rbx
	sarq	$35, %r14
	movl	%r14d, %edx
	call	_ZN2v88internal9Assembler6movapdENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %esi
	movl	$93, %r9d
	movl	%ebx, %edx
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$93, %r9d
	movl	%r14d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4orpdENS0_11XMMRegisterES2_@PLT
	movl	$3, %ecx
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5cmppdENS0_11XMMRegisterES2_a@PLT
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4orpdENS0_11XMMRegisterES2_@PLT
	movl	$13, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrlqENS0_11XMMRegisterEh@PLT
	movl	%r13d, %edx
	movl	$85, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1428:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2317
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2317
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2317
	movq	-8(%rsi,%rcx), %rsi
	movl	$94, %r9d
	movl	$102, %ecx
	movq	%r12, %rdi
	movl	$15, %r8d
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1429:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2315
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2315
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2315
	movq	-8(%rsi,%rcx), %rsi
	movl	$89, %r9d
	movl	$102, %ecx
	movq	%r12, %rdi
	movl	$15, %r8d
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1430:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2313
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2313
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2313
	movq	-8(%rsi,%rcx), %rsi
	movl	$92, %r9d
	movl	$102, %ecx
	movq	%r12, %rdi
	movl	$15, %r8d
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1431:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2311
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2311
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2311
	movq	-8(%rsi,%rcx), %rsi
	movl	$88, %r9d
	movl	$102, %ecx
	movq	%r12, %rdi
	movl	$15, %r8d
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1435:
	movzbl	4(%rsi), %esi
	movq	40(%rdi), %rcx
	leaq	200(%rdi), %r12
	movq	48(%r11,%rsi,8), %rax
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3780
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2306
	movq	%rdi, %rcx
	jmp	.L2307
.L3781:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2310:
	testq	%rax, %rax
	je	.L2308
.L2307:
	cmpl	32(%rax), %edx
	jle	.L3781
	movq	24(%rax), %rax
	jmp	.L2310
.L1434:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$7, %rax
	movq	(%rsi,%rax,8), %rsi
	leaq	0(,%rax,8), %rdx
	testb	$4, %sil
	je	.L2291
	movq	%rsi, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2291
	movq	%rsi, %rax
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2291
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	sarq	$35, %rsi
	movq	%r12, %rdi
	movq	%rsi, %rdx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler4movqENS0_8RegisterENS0_11XMMRegisterE@PLT
	movq	-10472(%rbp), %rax
	leaq	-10480(%rbp), %rdi
	movzbl	4(%rax), %edx
	leaq	48(%rax,%rdx,8), %rsi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r12, %rdi
	movsbl	%dl, %ecx
	movl	%r14d, %edx
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler6pinsrqENS0_11XMMRegisterENS0_8RegisterEa@PLT
	jmp	.L1231
.L1436:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	sarq	$35, %rbx
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2288
	testb	$24, %al
	jne	.L2288
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2288
	movl	%esi, %edx
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler7movddupENS0_11XMMRegisterES2_@PLT
.L2289:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1437:
	movzbl	4(%rsi), %eax
	movq	40(%r15), %rcx
	movq	40(%rsi,%rax,8), %rax
	movl	%eax, %edx
	movq	%rax, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L3782
	movq	112(%rcx), %rax
	movl	%edi, %edx
	leaq	104(%rcx), %rsi
	testq	%rax, %rax
	je	.L2277
	movq	%rsi, %rcx
	jmp	.L2278
.L3783:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2281:
	testq	%rax, %rax
	je	.L2279
.L2278:
	cmpl	32(%rax), %edx
	jle	.L3783
	movq	24(%rax), %rax
	jmp	.L2281
.L1245:
	leaq	200(%rdi), %rbx
	leaq	-10464(%rbp), %r12
	movq	$0, -10464(%rbp)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-10488(%rbp), %r13
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r13, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r15d
	movq	%rbx, %rdi
	movl	%edx, %ecx
	movl	%edx, -4520(%rbp)
	movl	%edx, -6644(%rbp)
	movl	%r15d, %esi
	movq	%rax, %rdx
	movq	%rax, -4528(%rbp)
	movq	%rax, -6652(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r15d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	sarq	$35, %rcx
	sarq	$35, %rdx
	cmpl	%ecx, %edx
	je	.L3784
	movl	$8, %r8d
	movl	$51, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
.L2686:
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	movl	4(%rcx), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rcx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgbENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$8, %ecx
	movl	%r15d, %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1246:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %r15
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r15, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4544(%rbp)
	movl	%ebx, %esi
	movl	%edx, -6668(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4552(%rbp)
	movq	%rax, -6676(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$11, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r8d
	movq	%r12, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1493:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2070
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2070
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2070
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_8RegisterE@PLT
	jmp	.L1231
.L1494:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2068
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2068
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2068
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_8RegisterE@PLT
	jmp	.L1231
.L1495:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2066
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2066
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2066
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_8RegisterE@PLT
	jmp	.L1231
.L1658:
	movq	24(%rdi), %rax
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	movb	$1, 8(%rax)
	movl	(%rsi), %esi
	shrl	$22, %esi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	jmp	.L1231
.L1387:
	movl	4(%rsi), %eax
	addq	$200, %r15
	movq	40(%rsi), %rbx
	movq	%r15, %rdi
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	movq	48(%rsi,%rcx,8), %rdx
	sarq	$35, %rbx
	leaq	5(%rcx,%rax), %rax
	movq	(%rsi,%rax,8), %r13
	sarq	$35, %rdx
	sarq	$35, %r13
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	subq	$8, %rsp
	movl	%r13d, %esi
	movl	%ebx, %edx
	pushq	$55
	movl	$56, %r9d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler12sse4_2_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	$118, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	$239, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	popq	%r12
	popq	%r13
	xorl	%r13d, %r13d
	jmp	.L1231
.L1388:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	leaq	200(%rdi), %rdi
	movl	$56, %r9d
	movl	$15, %r8d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$55
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler12sse4_2_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r14
	popq	%r15
	jmp	.L1231
.L1389:
	movl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$56, %r9d
	xorl	%r13d, %r13d
	addq	$200, %r15
	movl	$15, %r8d
	movl	$102, %ecx
	movzbl	%al, %edx
	shrl	$8, %eax
	movq	%r15, %rdi
	movzwl	%ax, %eax
	leaq	5(%rdx,%rax), %rax
	movq	48(%rsi,%rdx,8), %rdx
	movq	(%rsi,%rax,8), %rbx
	movq	40(%rsi), %rsi
	pushq	$41
	sarq	$35, %rdx
	sarq	$35, %rbx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	$56, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	$41, (%rsp)
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%ebx, %edx
	movl	$239, %r9d
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	popq	%rax
	popq	%rdx
	jmp	.L1231
.L1390:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	leaq	200(%rdi), %rdi
	movl	$56, %r9d
	movl	$15, %r8d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$41
	sarq	$35, %rsi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L1231
.L1391:
	movl	4(%rsi), %eax
	addq	$200, %r15
	movq	40(%rsi), %rbx
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	movq	48(%rsi,%rdx,8), %r14
	sarq	$35, %rbx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %r13
	sarq	$35, %r14
	movl	%r14d, %edx
	sarq	$35, %r13
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	subq	$8, %rsp
	movl	$102, %ecx
	movl	%ebx, %edx
	pushq	$55
	movl	$56, %r9d
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler12sse4_2_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	$15, %r8d
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	movl	$56, %r9d
	movl	$102, %ecx
	xorl	%r13d, %r13d
	movl	$21, (%rsp)
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%rdi
	popq	%r8
	jmp	.L1231
.L1392:
	movl	4(%rsi), %eax
	movq	40(%rsi), %r13
	leaq	200(%rdi), %r12
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	sarq	$35, %r13
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rbx
	leaq	0(,%rax,8), %rcx
	movq	48(%rsi,%rdx,8), %rax
	sarq	$35, %rbx
	sarq	$35, %rax
	testb	$1, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2368
	movl	%eax, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%rax, -10504(%rbp)
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	subq	$8, %rsp
	movl	%r13d, %edx
	movq	%r12, %rdi
	pushq	$55
	movq	-10504(%rbp), %rax
	movl	$102, %ecx
	movl	$56, %r9d
	movl	$15, %r8d
	movl	%eax, %esi
	call	_ZN2v88internal9Assembler12sse4_2_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	$56, %r9d
	movl	%r13d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	movl	$21, (%rsp)
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	xorl	%r13d, %r13d
	popq	%r9
	popq	%r10
	jmp	.L1231
.L1393:
	movl	4(%rsi), %eax
	addq	$200, %r15
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rdx), %rcx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rcx,8), %rbx
	movq	8(%rsi,%rcx,8), %r14
	movq	(%rsi,%rax,8), %r13
	movq	8(%rsi,%rax,8), %rax
	sarq	$35, %rbx
	sarq	$35, %r14
	sarq	$35, %rax
	sarq	$35, %r13
	movl	%ebx, %edx
	movq	%rax, %r12
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %esi
	movl	$32, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrlqENS0_11XMMRegisterEh@PLT
	movl	$102, %ecx
	movl	%r13d, %esi
	movl	%r14d, %edx
	movl	$244, %r9d
	movl	$15, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$32, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrlqENS0_11XMMRegisterEh@PLT
	movl	$244, %r9d
	movl	%ebx, %edx
	movl	%r12d, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$102, %ecx
	movl	%r13d, %edx
	movl	%r12d, %esi
	movl	$212, %r9d
	movl	$15, %r8d
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$32, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psllqENS0_11XMMRegisterEh@PLT
	movl	$244, %r9d
	movl	%r14d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$212, %r9d
	movl	%r12d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1394:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$251, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1383:
	movl	4(%rsi), %eax
	movl	$118, %r9d
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r15d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	movq	48(%rsi,%rdx,8), %r13
	sarq	$35, %rbx
	leaq	5(%rdx,%rax), %rax
	movl	%r15d, %edx
	movq	(%rsi,%rax,8), %r14
	movl	%r15d, %esi
	sarq	$35, %r13
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$63, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5psllqENS0_11XMMRegisterEh@PLT
	sarq	$35, %r14
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	%r14d, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	$102, %ecx
	movl	%r15d, %edx
	movl	%r14d, %esi
	movl	$239, %r9d
	movl	$15, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$239, %r9d
	movl	%r15d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	movl	%r14d, %edx
	pushq	$55
	movl	$15, %r8d
	movl	$56, %r9d
	movl	%ebx, %esi
	movl	$102, %ecx
	call	_ZN2v88internal9Assembler12sse4_2_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%rdi
	popq	%r8
	jmp	.L1231
.L1384:
	movl	4(%rsi), %eax
	movq	40(%rsi), %r14
	addq	$200, %r15
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	movq	48(%rsi,%rdx,8), %rbx
	sarq	$35, %r14
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %r13
	movq	8(%rsi,%rax,8), %rax
	sarq	$35, %rbx
	movl	%ebx, %edx
	sarq	$35, %rax
	sarq	$35, %r13
	movq	%rax, %r12
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	$118, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$63, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psllqENS0_11XMMRegisterEh@PLT
	movl	$239, %r9d
	movl	%ebx, %edx
	movl	%r12d, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	$239, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	subq	$8, %rsp
	movl	$102, %ecx
	movl	%ebx, %esi
	pushq	$55
	movl	$56, %r9d
	movl	%r12d, %edx
	movq	%r15, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler12sse4_2_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %edx
	movl	$102, %ecx
	movl	%r14d, %esi
	movl	$56, %r9d
	movl	$15, %r8d
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movl	$21, (%rsp)
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r9
	popq	%r10
	jmp	.L1231
.L1385:
	movl	4(%rsi), %eax
	movq	40(%rsi), %r14
	addq	$200, %r15
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	movq	48(%rsi,%rdx,8), %rbx
	sarq	$35, %r14
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %r13
	movq	8(%rsi,%rax,8), %rax
	sarq	$35, %rbx
	movl	%ebx, %edx
	sarq	$35, %rax
	sarq	$35, %r13
	movq	%rax, %r12
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	$118, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%ebx, %esi
	movl	$63, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psllqENS0_11XMMRegisterEh@PLT
	movl	%ebx, %edx
	movl	$239, %r9d
	movl	%r12d, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	%ebx, %esi
	movl	$239, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	subq	$8, %rsp
	movl	%ebx, %esi
	movl	$102, %ecx
	pushq	$55
	movl	$56, %r9d
	movl	%r12d, %edx
	movq	%r15, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler12sse4_2_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %esi
	movl	$102, %ecx
	movl	%r14d, %edx
	movl	$56, %r9d
	movl	$15, %r8d
	movq	%r15, %rdi
	movl	$21, (%rsp)
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	xorl	%r13d, %r13d
	popq	%r11
	popq	%rbx
	jmp	.L1231
.L1386:
	movl	4(%rsi), %eax
	addq	$200, %r15
	xorl	%r13d, %r13d
	movq	%r15, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rdx,%rax), %rax
	movq	48(%rsi,%rdx,8), %rdx
	movq	(%rsi,%rax,8), %rbx
	sarq	$35, %rdx
	sarq	$35, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	$211, %r9d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1381:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	sarq	$35, %rbx
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2369
	testb	$24, %al
	jne	.L2369
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2369
	movl	%esi, %edx
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler4movdENS0_11XMMRegisterENS0_8RegisterE@PLT
.L2370:
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6pshufdENS0_11XMMRegisterES2_h@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1382:
	movl	4(%rsi), %eax
	movl	$118, %r9d
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r15d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	movq	48(%rsi,%rdx,8), %r13
	sarq	$35, %rbx
	leaq	5(%rdx,%rax), %rax
	movl	%r15d, %edx
	movq	(%rsi,%rax,8), %r14
	movl	%r15d, %esi
	sarq	$35, %r13
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$63, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5psllqENS0_11XMMRegisterEh@PLT
	sarq	$35, %r14
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	%r14d, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	$239, %r9d
	movl	%r15d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$102, %ecx
	movl	%r15d, %edx
	movl	%r14d, %esi
	movl	$239, %r9d
	movl	$15, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	subq	$8, %rsp
	movl	$102, %ecx
	movl	%ebx, %edx
	pushq	$55
	movl	$56, %r9d
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler12sse4_2_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	$118, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$102, %ecx
	movl	%ebx, %esi
	movl	%r14d, %edx
	movl	$239, %r9d
	movl	$15, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L1231
.L1403:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$1, %ecx
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5cmppsENS0_11XMMRegisterES2_a@PLT
	jmp	.L1231
.L1404:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$4, %ecx
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5cmppsENS0_11XMMRegisterES2_a@PLT
	jmp	.L1231
.L1405:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5cmppsENS0_11XMMRegisterES2_a@PLT
	jmp	.L1231
.L1406:
	movzbl	4(%rsi), %eax
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	addq	$200, %r15
	movq	40(%rsi), %rbx
	movq	%r15, %rdi
	movq	48(%rsi,%rax,8), %r14
	movl	%r13d, %esi
	sarq	$35, %rbx
	sarq	$35, %r14
	movl	%r14d, %edx
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5maxpsENS0_11XMMRegisterES2_@PLT
	movl	%r14d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5maxpsENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4orpsENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5subpsENS0_11XMMRegisterES2_@PLT
	movl	$3, %ecx
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5cmppsENS0_11XMMRegisterES2_a@PLT
	movl	$10, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrldENS0_11XMMRegisterEh@PLT
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6andnpsENS0_11XMMRegisterES2_@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1407:
	movzbl	4(%rsi), %eax
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	addq	$200, %r15
	movq	40(%rsi), %rbx
	movq	%r15, %rdi
	movq	48(%rsi,%rax,8), %r14
	movl	%r13d, %esi
	sarq	$35, %rbx
	sarq	$35, %r14
	movl	%r14d, %edx
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5minpsENS0_11XMMRegisterES2_@PLT
	movl	%r14d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5minpsENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4orpsENS0_11XMMRegisterES2_@PLT
	movl	$3, %ecx
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5cmppsENS0_11XMMRegisterES2_a@PLT
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4orpsENS0_11XMMRegisterES2_@PLT
	movl	$10, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrldENS0_11XMMRegisterEh@PLT
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6andnpsENS0_11XMMRegisterES2_@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1408:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5divpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1409:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5mulpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1410:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5subpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1401:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	sarq	$35, %rbx
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2344
	testb	$24, %al
	jne	.L2344
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2344
	movl	%esi, %edx
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
.L2345:
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler7movddupENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1402:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$2, %ecx
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5cmppsENS0_11XMMRegisterES2_a@PLT
	jmp	.L1231
.L1318:
	movl	4(%rsi), %eax
	movl	$117, %r9d
	movq	40(%rsi), %r13
	leaq	200(%rdi), %r12
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r15d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	movl	%eax, %edx
	movzbl	%al, %eax
	sarq	$35, %r13
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movl	%r15d, %edx
	movq	(%rsi,%rax,8), %rbx
	movq	8(%rsi,%rax,8), %r14
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movq	-10472(%rbp), %rax
	movl	$8, %ecx
	movq	%r12, %rdi
	sarq	$35, %rbx
	sarq	$35, %r14
	movzbl	4(%rax), %edx
	movl	%ebx, %esi
	movq	48(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$8, %r8d
	movl	%ebx, %edx
	xorl	%esi, %esi
	movabsq	$81604378632, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%ebx, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	$102, %ecx
	movl	%r14d, %edx
	movl	%r15d, %esi
	movl	$209, %r9d
	movl	$15, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$102, %ecx
	movl	%r15d, %edx
	movl	%r15d, %esi
	movl	$103, %r9d
	movl	$15, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$102, %ecx
	movl	%r13d, %esi
	movl	%r15d, %edx
	movl	$219, %r9d
	movl	$15, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movq	-10472(%rbp), %rax
	movl	%r14d, %esi
	movq	%r12, %rdi
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movl	$102, %ecx
	movl	%r14d, %edx
	movl	$241, %r9d
	movl	$15, %r8d
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1319:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %r13
	sarq	$35, %rbx
	sarq	$35, %r13
	cmpl	%ebx, %r13d
	je	.L3785
	movl	$239, %r9d
	movl	$15, %r8d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	$248, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1320:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$99, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1321:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$7, %rax
	movq	(%rsi,%rax,8), %rsi
	leaq	-8(%r11,%rax,8), %r8
	testb	$4, %sil
	je	.L2429
	movq	%rsi, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2429
	movq	%rsi, %rax
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2429
	leaq	-10480(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r12, %rdi
	movq	%rdx, %rcx
	movzbl	4(%rax), %edx
	movq	40(%rax), %rsi
	movsbl	%cl, %ecx
	movq	56(%rax,%rdx,8), %rdx
	sarq	$35, %rsi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler6pinsrbENS0_11XMMRegisterENS0_8RegisterEa@PLT
	jmp	.L1231
.L1316:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$252, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1465:
	movl	%ebx, %r12d
	shrl	$22, %r12d
	cmpl	$1, %r12d
	je	.L3786
.L2148:
	leaq	200(%r15), %r13
	andb	$62, %bh
	jne	.L3787
	movq	-10472(%rbp), %rcx
	movzbl	4(%r11), %eax
	movzbl	4(%rcx), %edx
	movq	40(%r11,%rax,8), %rax
	movq	40(%rcx,%rdx,8), %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2151
	testb	$24, %al
	jne	.L2151
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2151
	movq	40(%rcx), %rsi
	movq	%r13, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7movsxwlENS0_8RegisterES2_@PLT
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L1466:
	shrl	$22, %ebx
	cmpl	$1, %ebx
	je	.L3788
.L2144:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	%r11, -10504(%rbp)
	movq	%r14, %rdi
	leaq	200(%r15), %r12
	movq	$0, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %r11
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movq	-10464(%rbp), %rax
	movzbl	4(%r11), %ecx
	movl	%edx, -88(%rbp)
	addq	$5, %rax
	addq	%rax, %rcx
	movq	(%r11,%rcx,8), %rcx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3789
	movq	-10472(%rbp), %rdi
	movzbl	4(%rdi), %ecx
	addq	%rcx, %rax
	movq	(%rdi,%rax,8), %rcx
	movq	%r12, %rdi
	sarq	$35, %rcx
	call	_ZN2v88internal9Assembler4movbENS0_7OperandENS0_8RegisterE@PLT
.L2146:
	cmpl	$2, %ebx
	je	.L3790
.L2147:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1467:
	movl	%ebx, %r12d
	shrl	$22, %r12d
	cmpl	$1, %r12d
	je	.L3791
.L2139:
	leaq	200(%r15), %r13
	andb	$62, %bh
	jne	.L3792
	movq	-10472(%rbp), %rcx
	movzbl	4(%r11), %eax
	movzbl	4(%rcx), %edx
	movq	40(%r11,%rax,8), %rax
	movq	40(%rcx,%rdx,8), %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2142
	testb	$24, %al
	jne	.L2142
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2142
	movq	40(%rcx), %rsi
	movq	%r13, %rdi
	movl	$8, %ecx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterES2_i@PLT
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L1468:
	movl	%ebx, %r12d
	shrl	$22, %r12d
	cmpl	$1, %r12d
	je	.L3793
.L2134:
	leaq	200(%r15), %r13
	andb	$62, %bh
	jne	.L3794
	movq	-10472(%rbp), %rcx
	movzbl	4(%r11), %eax
	movzbl	4(%rcx), %edx
	movq	40(%r11,%rax,8), %rax
	movq	40(%rcx,%rdx,8), %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2137
	testb	$24, %al
	jne	.L2137
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2137
	movq	40(%rcx), %rsi
	movq	%r13, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7movsxbqENS0_8RegisterES2_@PLT
	jmp	.L3643
	.p2align 4,,10
	.p2align 3
.L1270:
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	$8, %edx
	movq	%rsi, -10504(%rbp)
	movq	%r15, %rdi
	sarq	$35, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler5psllwENS0_11XMMRegisterEh@PLT
	movq	-10504(%rbp), %r11
	movl	4(%r11), %eax
	movl	%eax, %edx
	shrl	$8, %edx
	cmpw	$1, %dx
	je	.L3795
	movq	-10472(%rbp), %rdx
	movzbl	%al, %eax
	movq	48(%r11,%rax,8), %rax
	movzbl	4(%rdx), %ecx
	movq	48(%rdx,%rcx,8), %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2675
	testb	$24, %al
	jne	.L2675
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3796
.L2675:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %ecx
	movq	%r15, %rdi
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movq	%rdx, -136(%rbp)
	movl	%r14d, %esi
	movl	%ecx, -128(%rbp)
	movq	%rdx, -148(%rbp)
	movl	%ecx, -140(%rbp)
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterENS0_7OperandE@PLT
.L2676:
	movl	$8, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psllwENS0_11XMMRegisterEh@PLT
.L2674:
	movl	$8, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler5psrlwENS0_11XMMRegisterEh@PLT
	movl	$235, %r9d
	movl	%r14d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1272:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movl	%eax, %edx
	sarq	$35, %rbx
	movzbl	%al, %eax
	shrl	$8, %edx
	cmpw	$2, %dx
	sete	%dl
	movzbl	%dl, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2658
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2658
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3797
.L2658:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	subq	$8, %rsp
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	pushq	$97
	movl	%ebx, %esi
	movl	$15, %r9d
	movl	$102, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterENS0_7OperandEhhh@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L1231
.L1273:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movl	%eax, %edx
	sarq	$35, %rbx
	movzbl	%al, %eax
	shrl	$8, %edx
	cmpw	$2, %dx
	sete	%dl
	movzbl	%dl, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2656
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2656
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3798
.L2656:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	subq	$8, %rsp
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	pushq	$98
	movl	$102, %r8d
	movq	%r12, %rdi
	movl	%ebx, %esi
	movl	$15, %r9d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterENS0_7OperandEhhh@PLT
	popq	%rdi
	popq	%r8
	jmp	.L1231
.L1419:
	movl	4(%rsi), %eax
	movq	40(%r15), %rcx
	movzbl	%al, %r8d
	movq	48(%rsi,%r8,8), %rdx
	movl	%edx, %esi
	movq	%rdx, %rdi
	andl	$7, %esi
	shrq	$3, %rdi
	cmpl	$3, %esi
	je	.L3799
	movq	112(%rcx), %rax
	movl	%edi, %edx
	leaq	104(%rcx), %rsi
	testq	%rax, %rax
	je	.L2335
	movq	%rsi, %rcx
	jmp	.L2336
.L3800:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2339:
	testq	%rax, %rax
	je	.L2337
.L2336:
	cmpl	32(%rax), %edx
	jle	.L3800
	movq	24(%rax), %rax
	jmp	.L2339
.L1420:
	movzbl	4(%rsi), %esi
	movq	40(%rdi), %rcx
	leaq	200(%rdi), %r12
	movq	48(%r11,%rsi,8), %rax
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3801
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2326
	movq	%rdi, %rcx
	jmp	.L2327
.L3802:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2330:
	testq	%rax, %rax
	je	.L2328
.L2327:
	cmpl	32(%rax), %edx
	jle	.L3802
	movq	24(%rax), %rax
	jmp	.L2330
.L1421:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	sarq	$35, %rbx
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2319
	testb	$24, %al
	jne	.L2319
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2319
	movl	%esi, %edx
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterES2_@PLT
.L2320:
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6shufpsENS0_11XMMRegisterES2_h@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1422:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$2, %ecx
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5cmppdENS0_11XMMRegisterES2_a@PLT
	jmp	.L1231
.L1423:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$1, %ecx
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5cmppdENS0_11XMMRegisterES2_a@PLT
	jmp	.L1231
.L1424:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$4, %ecx
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5cmppdENS0_11XMMRegisterES2_a@PLT
	jmp	.L1231
.L1425:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5cmppdENS0_11XMMRegisterES2_a@PLT
	jmp	.L1231
.L1426:
	movzbl	4(%rsi), %eax
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	addq	$200, %r15
	movq	40(%rsi), %rbx
	movq	%r15, %rdi
	movq	48(%rsi,%rax,8), %r14
	movl	%r13d, %esi
	sarq	$35, %rbx
	sarq	$35, %r14
	movl	%r14d, %edx
	call	_ZN2v88internal9Assembler6movapdENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %esi
	movl	$95, %r9d
	movl	%ebx, %edx
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$95, %r9d
	movl	%r14d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5xorpdENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler4orpdENS0_11XMMRegisterES2_@PLT
	movl	$92, %r9d
	movl	%r13d, %esi
	movl	%ebx, %edx
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$3, %ecx
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5cmppdENS0_11XMMRegisterES2_a@PLT
	movl	$13, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrlqENS0_11XMMRegisterEh@PLT
	movl	%r13d, %edx
	movl	$85, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1415:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %rax
	sarq	$35, %rbx
	sarq	$35, %rax
	cmpl	%eax, %ebx
	je	.L3803
	movl	$118, %r9d
	movl	$15, %r8d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$31, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5pslldENS0_11XMMRegisterEh@PLT
	movq	-10472(%rbp), %rax
	movl	%ebx, %esi
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1416:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %rax
	sarq	$35, %rbx
	sarq	$35, %rax
	cmpl	%eax, %ebx
	je	.L3804
	movl	$118, %r9d
	movl	$15, %r8d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$1, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrldENS0_11XMMRegisterEh@PLT
	movq	-10472(%rbp), %rax
	movl	%ebx, %esi
	movq	%r15, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler5andpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1244:
	leaq	200(%rdi), %rbx
	leaq	-10464(%rbp), %r12
	movq	$0, -10464(%rbp)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-10488(%rbp), %r13
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r13, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r15d
	movq	%rbx, %rdi
	movl	%edx, %ecx
	movl	%edx, -4496(%rbp)
	movl	%edx, -6620(%rbp)
	movl	%r15d, %esi
	movq	%rax, %rdx
	movq	%rax, -4504(%rbp)
	movq	%rax, -6628(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r15d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	sarq	$35, %rcx
	sarq	$35, %rdx
	cmpl	%ecx, %edx
	je	.L3805
	movl	$8, %r8d
	movl	$51, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
.L2688:
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$8, %ecx
	movl	%r15d, %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1485:
	addq	$200, %r15
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2120
	movl	$15, %r8d
	movl	$15, %ecx
	movl	$15, %edx
	movq	%r15, %rdi
	movl	$87, %esi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
.L2121:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rsi
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2122
	pushq	$0
	movl	$3, %r9d
	movl	$15, %r8d
	movl	%esi, %ecx
	pushq	$1
	movl	%esi, %edx
	movq	%r15, %rdi
	movl	$92, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%r8
	popq	%r9
	jmp	.L1716
.L1660:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	leaq	-96(%rbp), %r13
	movq	40(%rsi,%rax,8), %rbx
	sarq	$35, %rbx
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L3806
.L1690:
	movq	%r13, %rdi
	movl	$47, %edx
	movl	%ebx, %esi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r13d
	movq	%rdx, -6544(%rbp)
	movl	%r13d, %esi
	movl	%ecx, -6536(%rbp)
	movq	%rdx, -10432(%rbp)
	movl	%ecx, -10424(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE@PLT
	movq	24(%r15), %rax
	movq	-10504(%rbp), %r11
	movq	%r15, %rdi
	movl	$0, 12(%rax)
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeGenerator18RecordCallPositionEPNS1_11InstructionE@PLT
	jmp	.L1231
.L1655:
	movq	32(%rdi), %rax
	shrl	$22, %ebx
	movq	$0, -10464(%rbp)
	leaq	200(%rdi), %r14
	movl	%ebx, %r13d
	leaq	-10464(%rbp), %r12
	movq	(%rax), %rdx
	movq	%rsi, %rax
	cmpl	$3, 8(%rdx)
	je	.L3807
.L1692:
	movzbl	4(%rax), %edx
	movq	%r11, -10504(%rbp)
	leaq	40(%rax,%rdx,8), %rsi
	movzbl	4(%r11), %eax
	movq	40(%r11,%rax,8), %rax
	andl	$7, %eax
	cmpl	$3, %eax
	je	.L3808
	movq	(%rsi), %rsi
	movl	%r13d, %edx
	movq	%r14, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_8RegisterEi@PLT
	movq	-10504(%rbp), %r11
.L1694:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-10504(%rbp), %r11
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	24(%r11), %rsi
	call	_ZN2v88internal8compiler13CodeGenerator15RecordSafepointEPNS1_12ReferenceMapENS0_9Safepoint9DeoptModeE@PLT
	movq	24(%r15), %rdi
	call	_ZN2v88internal8compiler16FrameAccessState23SetFrameAccessToDefaultEv@PLT
	movq	24(%r15), %rax
	movl	$0, 12(%rax)
	cmpb	$0, 1096(%r15)
	jne	.L3809
.L1695:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1400:
	movzbl	4(%rsi), %esi
	movq	40(%rdi), %rcx
	leaq	200(%rdi), %r12
	movq	48(%r11,%rsi,8), %rax
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3810
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2351
	movq	%rdi, %rcx
	jmp	.L2352
.L3811:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2355:
	testq	%rax, %rax
	je	.L2353
.L2352:
	cmpl	32(%rax), %edx
	jle	.L3811
	movq	24(%rax), %rax
	jmp	.L2355
.L1326:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	leaq	200(%rdi), %rdi
	movl	$56, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$62
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r13
	xorl	%r13d, %r13d
	popq	%r14
	jmp	.L1231
.L1327:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movl	$102, %ecx
	xorl	%r13d, %r13d
	leaq	200(%rdi), %rdi
	movl	$56, %r9d
	movl	$15, %r8d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	pushq	$58
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r15
	popq	%rax
	jmp	.L1231
.L1328:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$217, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1329:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$221, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1324:
	movzbl	4(%rsi), %eax
	subq	$8, %rsp
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	$56, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	movq	48(%rsi,%rax,8), %r13
	pushq	$58
	sarq	$35, %rbx
	movl	%ebx, %esi
	sarq	$35, %r13
	movl	%r13d, %edx
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%r13d, %edx
	movl	$117, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	popq	%r10
	popq	%r11
	jmp	.L1231
.L1325:
	movl	4(%rsi), %eax
	subq	$8, %rsp
	movq	40(%rsi), %rbx
	movl	$102, %ecx
	addq	$200, %r15
	movl	$56, %r9d
	movl	$15, %r8d
	movzbl	%al, %edx
	shrl	$8, %eax
	movq	%r15, %rdi
	movzwl	%ax, %eax
	movq	48(%rsi,%rdx,8), %r14
	sarq	$35, %rbx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %r13
	pushq	$62
	sarq	$35, %r14
	movl	%ebx, %esi
	movl	%r14d, %edx
	sarq	$35, %r13
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	movl	%ebx, %esi
	movl	$117, %r9d
	movl	%r14d, %edx
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$102, %ecx
	movl	$117, %r9d
	movl	$15, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	%ebx, %esi
	movl	$239, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	popq	%rbx
	popq	%r12
	jmp	.L1231
.L1323:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	sarq	$35, %rbx
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2417
	testb	$24, %al
	jne	.L2417
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2417
	movl	%esi, %edx
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler4movdENS0_11XMMRegisterENS0_8RegisterE@PLT
.L2418:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	movq	%r12, %rdi
	movl	%r13d, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	subq	$8, %rsp
	movl	%r13d, %edx
	movl	%ebx, %esi
	pushq	$0
	movl	$56, %r9d
	movl	$102, %ecx
	movq	%r12, %rdi
	movl	$15, %r8d
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11ssse3_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r8
	popq	%r9
	jmp	.L1231
.L1315:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$236, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1475:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rdx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2108
	testb	$24, %al
	jne	.L2108
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2108
	movq	-8(%r11,%rdx), %rcx
	movq	40(%r11), %rdx
	pushq	$0
	movl	%esi, %r8d
	pushq	$1
	movq	%r12, %rdi
	movl	$3, %r9d
	movl	$94, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rdi
	popq	%r8
	jmp	.L2109
.L1476:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rdx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2106
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2106
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2106
	movq	-8(%r11,%rdx), %rcx
	movq	40(%r11), %rdx
	pushq	$0
	movl	$3, %r9d
	pushq	$1
	movl	%esi, %r8d
	movq	%r12, %rdi
	movl	$89, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%r9
	popq	%r10
	jmp	.L1231
.L1278:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movl	%eax, %edx
	sarq	$35, %rbx
	movzbl	%al, %eax
	shrl	$8, %edx
	cmpw	$2, %dx
	sete	%dl
	movzbl	%dl, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2646
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2646
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3812
.L2646:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	subq	$8, %rsp
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %ecx
	pushq	$109
	movl	$15, %r9d
	movl	$102, %r8d
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterENS0_7OperandEhhh@PLT
	popq	%rax
	popq	%rdx
	jmp	.L1231
.L1441:
	movq	40(%rsi), %rsi
	leaq	200(%rdi), %rdi
	movl	$4, %edx
	xorl	%r13d, %r13d
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_decENS0_8RegisterEi@PLT
	jmp	.L1231
.L1442:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rdx
	shrl	$9, %ebx
	movl	%ebx, %r13d
	addq	$5, %rax
	sarq	$35, %rdx
	andl	$31, %r13d
	movq	(%rsi,%rax,8), %rsi
	leaq	0(,%rax,8), %rcx
	sarq	$35, %rsi
	cmpl	%edx, %esi
	je	.L3813
	addq	$200, %r15
	cmpl	$3, %r13d
	jne	.L2257
	movq	8(%r11,%rcx), %rax
	sarq	$35, %rax
	cmpl	%eax, %edx
	je	.L2258
.L2257:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$0, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r8d
	movq	%r15, %rdi
	movq	%rax, %r9
	movl	%edx, -5744(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8360(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r9, -5752(%rbp)
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -8368(%rbp)
	movq	%r9, -96(%rbp)
	sarq	$35, %rsi
	movl	%eax, -88(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1443:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rdx
	shrl	$9, %ebx
	addq	$200, %r15
	movl	%ebx, %r13d
	addq	$5, %rax
	sarq	$35, %rdx
	andl	$31, %r13d
	movq	(%rsi,%rax,8), %rsi
	leaq	0(,%rax,8), %rcx
	sarq	$35, %rsi
	cmpl	%edx, %esi
	je	.L3814
	cmpl	$3, %r13d
	jne	.L2247
	movq	8(%r11,%rcx), %rax
	sarq	$35, %rax
	cmpl	%eax, %edx
	je	.L2248
.L2247:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movq	$0, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%rax, %r9
	movl	%edx, -5768(%rbp)
	movq	%rdx, %rax
	movq	%r9, -5776(%rbp)
	movq	%r9, -8392(%rbp)
	movl	%edx, -8384(%rbp)
.L3637:
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movl	$4, %r8d
	movq	%r15, %rdi
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -96(%rbp)
	movl	%eax, -88(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
.L2240:
	movq	-10472(%rbp), %rax
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler18AssertZeroExtendedENS0_8RegisterE@PLT
	jmp	.L1231
.L1444:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2233
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2233
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2233
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovqES3_S4_EEXadL_ZNS7_4movqES3_S4_EEEEvS3_S4_.isra.0
	jmp	.L1231
.L1439:
	leaq	48(%rdi), %r14
	leaq	200(%rdi), %r12
	andb	$62, %bh
	jne	.L3815
	movzbl	4(%rsi), %eax
	leaq	40(%rsi,%rax,8), %rsi
	movq	(%rsi), %rax
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L3816
	testb	$4, %al
	je	.L2262
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	je	.L3817
	subl	$1, %r13d
	jne	.L2262
	shrq	$5, %rax
	cmpb	$13, %al
	ja	.L2262
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -8356(%rbp)
	movl	%edx, -8348(%rbp)
	call	_ZN2v88internal9Assembler5pushqENS0_7OperandE@PLT
	movq	24(%r15), %rax
	addl	$1, 12(%rax)
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L1716
	cmpb	$0, 112(%r15)
	jne	.L1716
	movq	232(%r15), %rsi
	movl	$8, %edx
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	subq	216(%r15), %rsi
	call	_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1438:
	movzbl	4(%rsi), %eax
	shrl	$22, %ebx
	addq	$200, %r15
	movl	%ebx, %r13d
	leaq	40(%rsi,%rax,8), %rsi
	sall	$3, %r13d
	movq	(%rsi), %r14
	movl	%r14d, %eax
	andl	$7, %eax
	cmpl	$3, %eax
	je	.L3818
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	%r13d, %edx
	leaq	-112(%rbp), %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-112(%rbp), %rsi
	movl	-104(%rbp), %edx
	movq	%r14, %rcx
	sarq	$35, %rcx
	movl	$8, %r8d
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L1231
.L1440:
	movq	40(%rsi), %rsi
	leaq	200(%rdi), %rdi
	movl	$4, %edx
	xorl	%r13d, %r13d
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_incENS0_8RegisterEi@PLT
	jmp	.L1231
.L1413:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	movq	40(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7rsqrtpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1414:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	movq	40(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5rcppsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1412:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5addpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1417:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	$239, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	sarq	$35, %rbx
	movl	%r13d, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %esi
	movl	$85, %ecx
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler7pblendwENS0_11XMMRegisterES2_h@PLT
	movl	$250, %r9d
	movl	%r13d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler8cvtdq2psENS0_11XMMRegisterES2_@PLT
	movl	$1, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrldENS0_11XMMRegisterEh@PLT
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler8cvtdq2psENS0_11XMMRegisterES2_@PLT
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5addpsENS0_11XMMRegisterES2_@PLT
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5addpsENS0_11XMMRegisterES2_@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L1418:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	movq	40(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8cvtdq2psENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1334:
	movzbl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	$234, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	movq	48(%rsi,%rax,8), %r13
	sarq	$35, %rbx
	movl	%ebx, %esi
	sarq	$35, %r13
	movl	%r13d, %edx
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	$117, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1335:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$101, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1336:
	movl	4(%rsi), %eax
	addq	$200, %r15
	movl	$102, %ecx
	movl	$117, %r9d
	movl	$15, %r8d
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movzbl	%al, %edx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rdx,%rax), %rax
	movq	48(%rsi,%rdx,8), %rdx
	movq	(%rsi,%rax,8), %rbx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rbx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$117, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$239, %r9d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$15, %r8d
	movl	$102, %ecx
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1337:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	movl	$102, %ecx
	movl	$117, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	movq	48(%rsi,%rax,8), %rdx
	movq	40(%rsi), %rsi
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1497:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2062
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2062
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2062
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_8RegisterE@PLT
	jmp	.L1231
.L1498:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2060
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2060
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2060
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_8RegisterE@PLT
	jmp	.L1231
.L1247:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %r15
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r15, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4568(%rbp)
	movl	%ebx, %esi
	movl	%edx, -6692(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4576(%rbp)
	movq	%rax, -6700(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$11, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r12, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1474:
	movl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	%rsi, -10504(%rbp)
	movl	$1, %r9d
	movq	%r12, %rdi
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rbx
	pushq	$0
	movl	$118, %esi
	pushq	$1
	sarq	$35, %rbx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	movl	%ebx, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	movl	_ZN2v88internalL4xmm2E(%rip), %edx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	movl	$115, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	movq	232(%r15), %rax
	movq	-10504(%rbp), %r11
	leaq	1(%rax), %rdx
	movq	%rdx, 232(%r15)
	movb	$1, (%rax)
	movzbl	4(%r11), %eax
	popq	%r13
	popq	%r14
	movq	40(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L2116
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2116
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2116
	movq	-10472(%rbp), %rax
	movl	%ebx, %ecx
	movl	$84, %esi
	movq	%r12, %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %r8
	movq	40(%rax), %rdx
	sarq	$35, %r8
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L1231
.L1481:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rdx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2094
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2094
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2094
	movq	-8(%r11,%rdx), %rcx
	movq	40(%r11), %rdx
	movl	%esi, %r8d
	movq	%r12, %rdi
	movl	$89, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L1231
.L1496:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2064
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2064
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2064
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_8RegisterE@PLT
	jmp	.L1231
.L1489:
	movzbl	4(%rsi), %eax
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %rdx
	testb	$4, %dl
	je	.L2079
	movq	%rdx, %rax
	shrq	$3, %rax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L3819
.L2079:
	movq	40(%r11), %rsi
	sarq	$35, %rdx
	movl	$1, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler6PextrdENS0_8RegisterENS0_11XMMRegisterEa@PLT
	jmp	.L1231
.L1499:
	movl	4(%rsi), %eax
	addq	$200, %r15
	movq	%rsi, %rcx
	movq	$0, -10464(%rbp)
	movl	%eax, %edx
	testb	$-2, %al
	jne	.L3820
.L2055:
	movzbl	%al, %eax
	movzbl	%dl, %edx
	movq	40(%rcx,%rdx,8), %rdx
	movq	40(%r11,%rax,8), %rax
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2056
	testb	$24, %al
	jne	.L2056
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2056
	movq	40(%rcx), %rsi
	leaq	-10464(%rbp), %r12
	movq	%r15, %rdi
	movq	%r11, -10504(%rbp)
	movq	%r12, %rcx
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE@PLT
	movq	-10504(%rbp), %r11
.L2057:
	testb	$-2, 4(%r11)
	jne	.L3821
.L2059:
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L1231
.L1500:
	movl	4(%rsi), %eax
	addq	$200, %r15
	movq	%rsi, %rcx
	movq	$0, -10464(%rbp)
	movl	%eax, %edx
	testb	$-2, %al
	jne	.L3822
.L2050:
	movzbl	%al, %eax
	movzbl	%dl, %edx
	movq	40(%rcx,%rdx,8), %rdx
	movq	40(%r11,%rax,8), %rax
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2051
	testb	$24, %al
	jne	.L2051
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2051
	movq	40(%rcx), %rsi
	leaq	-10464(%rbp), %r12
	movq	%r15, %rdi
	movq	%r11, -10504(%rbp)
	movq	%r12, %rcx
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE@PLT
	movq	-10504(%rbp), %r11
.L2052:
	testb	$-2, 4(%r11)
	jne	.L3823
.L2054:
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L1231
.L1479:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rdx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2100
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2100
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2100
	movq	-8(%r11,%rdx), %rdx
	pushq	$0
	movl	%esi, %r8d
	movq	%r12, %rdi
	pushq	$1
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$1, %r9d
	movl	$46, %esi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%r15
	popq	%rax
	jmp	.L1231
.L1478:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rdx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2102
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2102
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2102
	movq	-8(%r11,%rdx), %rcx
	movq	40(%r11), %rdx
	pushq	$0
	movl	%esi, %r8d
	pushq	$1
	movq	%r12, %rdi
	movl	$3, %r9d
	movl	$88, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%r12
	popq	%r14
	jmp	.L1231
.L1487:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	48(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2082
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2082
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2082
	movq	40(%rsi), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_8RegisterEa@PLT
	jmp	.L1231
.L1483:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rdx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2090
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2090
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2090
	movq	-8(%r11,%rdx), %rcx
	movq	40(%r11), %rdx
	movl	%esi, %r8d
	movq	%r12, %rdi
	movl	$88, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L1231
.L1491:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2074
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2074
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2074
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_8RegisterE@PLT
	jmp	.L1231
.L1480:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rdx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2096
	testb	$24, %al
	jne	.L2096
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2096
	movq	-8(%r11,%rdx), %rcx
	movq	40(%r11), %rdx
	movl	%esi, %r8d
	movq	%r12, %rdi
	movl	$94, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_S2_@PLT
.L2097:
	movq	-10472(%rbp), %rax
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2099
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %r8d
	movl	%esi, %edx
	movq	%r12, %rdi
	movl	$40, %esi
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L1716
.L1486:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2084
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2084
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2084
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovdES3_S4_EEXadL_ZNS7_4movdES3_S4_EEEEvS3_S4_.isra.0
	jmp	.L1231
.L1482:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rdx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2092
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2092
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2092
	movq	-8(%r11,%rdx), %rcx
	movq	40(%r11), %rdx
	movl	%esi, %r8d
	movq	%r12, %rdi
	movl	$92, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_S2_@PLT
	jmp	.L1231
.L1490:
	movzbl	4(%rsi), %eax
	addq	$200, %r15
	movq	40(%rsi,%rax,8), %rdx
	testb	$4, %dl
	je	.L2076
	movq	%rdx, %rax
	shrq	$3, %rax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L3824
.L2076:
	movq	40(%r11), %rsi
	sarq	$35, %rdx
	movq	%r15, %rdi
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2077
	call	_ZN2v88internal9Assembler5vmovdENS0_8RegisterENS0_11XMMRegisterE@PLT
.L2078:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1484:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	addq	$6, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2088
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2088
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2088
	movq	-8(%rsi,%rcx), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8vucomissENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1488:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	48(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2080
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2080
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2080
	movq	40(%rsi), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_8RegisterEa@PLT
	jmp	.L1231
.L1492:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	movq	40(%rsi,%rax,8), %rax
	movq	%rax, %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2072
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2072
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2072
	movq	40(%rsi), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_8RegisterE@PLT
	jmp	.L1231
.L1650:
	movzbl	4(%rsi), %eax
	movq	40(%r15), %rcx
	movq	40(%rsi,%rax,8), %rax
	movl	%eax, %edx
	movq	%rax, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L3825
	movq	112(%rcx), %rax
	movl	%edi, %edx
	leaq	104(%rcx), %rsi
	testq	%rax, %rax
	je	.L1700
	movq	%rsi, %rcx
	jmp	.L1701
.L3826:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L1704:
	testq	%rax, %rax
	je	.L1702
.L1701:
	cmpl	32(%rax), %edx
	jle	.L3826
	movq	24(%rax), %rax
	jmp	.L1704
.L1317:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movl	$104, %r9d
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r15d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	movl	%eax, %edx
	movzbl	%al, %eax
	sarq	$35, %rbx
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	5(%rdx,%rax), %rax
	movl	%ebx, %edx
	movq	(%rsi,%rax,8), %r13
	movq	8(%rsi,%rax,8), %r14
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$96, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	sarq	$35, %r13
	movl	$102, %ecx
	movq	%r12, %rdi
	sarq	$35, %r14
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$8, %r8d
	movl	%r13d, %edx
	xorl	%esi, %esi
	movabsq	$81604378632, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	$102, %ecx
	movl	%r14d, %edx
	movl	%r15d, %esi
	movl	$225, %r9d
	movl	$15, %r8d
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$225, %r9d
	movl	%r14d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	$99, %r9d
	movl	%r15d, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1248:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4592(%rbp)
	movl	%r13d, %esi
	movl	%edx, -6716(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4600(%rbp)
	movq	%rax, -6724(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$11, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1636:
	movzbl	4(%rsi), %eax
	movq	24(%rdi), %r12
	movq	40(%r15), %rcx
	movq	40(%rsi,%rax,8), %rax
	movl	%eax, %edx
	movq	%rax, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L3827
	movq	112(%rcx), %rax
	movl	%edi, %edx
	leaq	104(%rcx), %rsi
	testq	%rax, %rax
	je	.L1733
	movq	%rsi, %rcx
	jmp	.L1734
.L3828:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L1737:
	testq	%rax, %rax
	je	.L1735
.L1734:
	cmpl	32(%rax), %edx
	jle	.L3828
	movq	24(%rax), %rax
	jmp	.L1737
.L1271:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movl	%eax, %edx
	sarq	$35, %rbx
	movzbl	%al, %eax
	shrl	$8, %edx
	cmpw	$2, %dx
	sete	%dl
	movzbl	%dl, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2660
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2660
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3829
.L2660:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	subq	$8, %rsp
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %ecx
	pushq	$96
	movl	$15, %r9d
	movl	$102, %r8d
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterENS0_7OperandEhhh@PLT
	popq	%rax
	popq	%rdx
	jmp	.L1231
.L1276:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movl	%eax, %edx
	sarq	$35, %rbx
	movzbl	%al, %eax
	shrl	$8, %edx
	cmpw	$2, %dx
	sete	%dl
	movzbl	%dl, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2650
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2650
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3830
.L2650:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	subq	$8, %rsp
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	pushq	$105
	movq	%r12, %rdi
	movl	$15, %r9d
	movl	%ebx, %esi
	movl	$102, %r8d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterENS0_7OperandEhhh@PLT
	popq	%r12
	popq	%r13
	xorl	%r13d, %r13d
	jmp	.L1231
.L1280:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	%eax, %edx
	sarq	$35, %rbx
	shrl	$8, %edx
	movl	%ebx, %r13d
	cmpw	$2, %dx
	je	.L3831
.L2667:
	movl	$8, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrlwENS0_11XMMRegisterEh@PLT
	movl	%r13d, %edx
	movl	$103, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1281:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movq	%rsi, -10504(%rbp)
	movl	$239, %r9d
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	%r14d, %edx
	movl	%r14d, %esi
	sarq	$35, %rbx
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movq	-10504(%rbp), %r11
	movl	%ebx, %r13d
	movl	4(%r11), %eax
	movl	%eax, %edx
	shrl	$8, %edx
	cmpw	$2, %dx
	je	.L3832
.L2665:
	movl	$170, %ecx
	movl	%r14d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler7pblendwENS0_11XMMRegisterES2_h@PLT
	subq	$8, %rsp
	movl	%r13d, %edx
	movl	%ebx, %esi
	pushq	$43
	movl	$56, %r9d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler10sse4_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r12
	popq	%r13
	xorl	%r13d, %r13d
	jmp	.L1231
.L1279:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	addq	$200, %r15
	movl	%eax, %edx
	sarq	$35, %rbx
	shrl	$8, %edx
	movl	%ebx, %r13d
	cmpw	$2, %dx
	je	.L3833
.L2670:
	movl	$8, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psllwENS0_11XMMRegisterEh@PLT
	movl	$8, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrlwENS0_11XMMRegisterEh@PLT
	movl	%r13d, %edx
	movl	$103, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1275:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movl	%eax, %edx
	sarq	$35, %rbx
	movzbl	%al, %eax
	shrl	$8, %edx
	cmpw	$2, %dx
	sete	%dl
	movzbl	%dl, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2652
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2652
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3834
.L2652:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	subq	$8, %rsp
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	pushq	$104
	movl	%ebx, %esi
	movl	$15, %r9d
	movl	$102, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterENS0_7OperandEhhh@PLT
	popq	%r11
	popq	%rbx
	jmp	.L1231
.L1277:
	movl	4(%rsi), %eax
	movq	40(%rsi), %rbx
	leaq	200(%rdi), %r12
	movl	%eax, %edx
	sarq	$35, %rbx
	movzbl	%al, %eax
	shrl	$8, %edx
	cmpw	$2, %dx
	sete	%dl
	movzbl	%dl, %edx
	leaq	5(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rax
	movq	%rax, %rsi
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2648
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2648
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3835
.L2648:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	subq	$8, %rsp
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	pushq	$106
	movl	$15, %r9d
	movl	$102, %r8d
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterENS0_7OperandEhhh@PLT
	popq	%r14
	popq	%r15
	jmp	.L1231
.L1251:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %r15
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r15, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4664(%rbp)
	movl	%ebx, %esi
	movl	%edx, -6788(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4672(%rbp)
	movq	%rax, -6796(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$35, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r12, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1252:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r14
	movq	$0, -10464(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r15
	movq	%rbx, %rsi
	movq	$1, -10488(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4688(%rbp)
	movl	%r13d, %esi
	movl	%edx, -6812(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4696(%rbp)
	movq	%rax, -6820(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_8RegisterENS0_7OperandE@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%r13d, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$35, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	$1, -10488(%rbp)
	sarq	$35, %rcx
	movq	%rcx, -10504(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8cmpxchgwENS0_7OperandENS0_8RegisterE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L1250:
	leaq	200(%rdi), %r12
	leaq	-10464(%rbp), %r13
	movq	$0, -10464(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-10488(%rbp), %r15
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-10480(%rbp), %r14
	movq	%r15, %rsi
	movq	$1, -10488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL3raxE(%rip), %ebx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -4640(%rbp)
	movl	%ebx, %esi
	movl	%edx, -6764(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -4648(%rbp)
	movq	%rax, -6772(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movl	$8, %ecx
	movq	(%rdx,%rax,8), %rsi
	movl	%ebx, %edx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	4(%rsi), %eax
	movzbl	%al, %edx
	shrl	$8, %eax
	addq	$5, %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	movq	(%rsi,%rdx,8), %rcx
	movq	(%rsi,%rax,8), %rdx
	movl	$35, %esi
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4lockEv@PLT
	movq	-10472(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	4(%rdx), %eax
	movzbl	%al, %ecx
	shrl	$8, %eax
	movzwl	%ax, %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rdx,%rax,8), %rbx
	movq	$1, -10488(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r8d
	movq	%r12, %rdi
	sarq	$35, %rbx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%ebx, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler12emit_cmpxchgENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L1231
.L1656:
	movl	_ZN2v88internalL6no_regE(%rip), %ecx
	movl	1100(%rdi), %esi
	leaq	200(%rdi), %rdi
	movl	_ZN2v88internalL16kReturnRegister0E(%rip), %edx
	movl	%ecx, %r8d
	call	_ZN2v88internal14TurboAssembler14PopCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_@PLT
	movq	24(%r15), %rcx
	movl	%eax, %edx
	leal	7(%rax), %eax
	testl	%edx, %edx
	cmovns	%edx, %eax
	xorl	%r13d, %r13d
	sarl	$3, %eax
	subl	%eax, 12(%rcx)
	movb	$0, 1096(%r15)
	jmp	.L1231
.L1657:
	shrl	$22, %ebx
	movl	_ZN2v88internalL6no_regE(%rip), %ecx
	movl	_ZN2v88internalL16kReturnRegister0E(%rip), %edx
	leaq	200(%rdi), %rdi
	movl	%ebx, 1100(%r15)
	movl	%ebx, %esi
	movl	%ecx, %r8d
	call	_ZN2v88internal14TurboAssembler15PushCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_@PLT
	movq	24(%r15), %rcx
	movl	%eax, %edx
	leal	7(%rax), %eax
	testl	%edx, %edx
	cmovns	%edx, %eax
	xorl	%r13d, %r13d
	sarl	$3, %eax
	addl	%eax, 12(%rcx)
	movb	$1, 1096(%r15)
	jmp	.L1231
.L1648:
	call	_ZN2v88internal8compiler13CodeGenerator24AssembleArchLookupSwitchEPNS1_11InstructionE
	xorl	%r13d, %r13d
	jmp	.L1231
.L1649:
	call	_ZN2v88internal8compiler13CodeGenerator30AssembleArchBinarySearchSwitchEPNS1_11InstructionE
	xorl	%r13d, %r13d
	jmp	.L1231
.L1647:
	call	_ZN2v88internal8compiler13CodeGenerator23AssembleArchTableSwitchEPNS1_11InstructionE
	xorl	%r13d, %r13d
	jmp	.L1231
.L1652:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r12
	leaq	40(%rsi,%rax,8), %rsi
	movq	(%rsi), %rax
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L3836
	sarq	$35, %rax
	andl	$268435456, %ebx
	movq	%r12, %rdi
	movq	%rax, %rsi
	je	.L1685
	call	_ZN2v88internal14TurboAssembler13RetpolineJumpENS0_8RegisterE@PLT
.L1684:
	movq	24(%r15), %rax
	movb	$1, 113(%r15)
	xorl	%r13d, %r13d
	movl	$0, 12(%rax)
	movq	24(%r15), %rdi
	call	_ZN2v88internal8compiler16FrameAccessState23SetFrameAccessToDefaultEv@PLT
	jmp	.L1231
.L1653:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %r13
	leaq	40(%rsi,%rax,8), %rsi
	movq	(%rsi), %rax
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L3837
	movq	%xmm2, -10504(%rbp)
	sarq	$35, %rax
	movq	%r13, %rdi
	andl	$268435456, %ebx
	movq	%rax, %rsi
	je	.L1673
	call	_ZN2v88internal14TurboAssembler13RetpolineCallENS0_8RegisterE@PLT
	movq	-10504(%rbp), %r11
.L1672:
	movq	%r11, %rsi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal8compiler13CodeGenerator18RecordCallPositionEPNS1_11InstructionE@PLT
	movq	24(%r15), %rax
	movl	$0, 12(%rax)
	jmp	.L1231
.L1651:
	movzbl	4(%rsi), %eax
	leaq	200(%rdi), %rdi
	xorl	%r13d, %r13d
	movq	%xmm2, -10504(%rbp)
	movq	40(%rsi,%rax,8), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler18CallBuiltinByIndexENS0_8RegisterE@PLT
	movq	-10504(%rbp), %r11
	movq	%r15, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeGenerator18RecordCallPositionEPNS1_11InstructionE@PLT
	movq	24(%r15), %rax
	movl	$0, 12(%rax)
	jmp	.L1231
.L2698:
	xorl	%r13d, %r13d
	jmp	.L1231
.L2463:
	cmpq	%rsi, %rdi
	je	.L2461
	cmpl	%r9d, 32(%rsi)
	cmovle	%rsi, %rdi
.L2461:
	movq	48(%rdi), %rsi
.L2460:
	movl	%esi, %ecx
	cmpb	$16, %sil
	movzbl	%sil, %eax
	movl	$128, %esi
	cmovnb	%esi, %eax
	movl	%ecx, %esi
	orl	-8(%rdx,%r15,4), %eax
	movl	$32768, %edi
	andl	$65280, %esi
	cmpb	$15, %ch
	cmova	%edi, %esi
	movl	%ecx, %edi
	shrl	$16, %edi
	orl	%esi, %eax
	movl	%ecx, %esi
	andl	$16711680, %esi
	cmpb	$16, %dil
	movl	$8388608, %edi
	cmovnb	%edi, %esi
	orl	%esi, %eax
	movl	%ecx, %esi
	shrl	$24, %esi
	cmpl	$268435455, %ecx
	ja	.L2469
	sall	$24, %esi
	orl	%esi, %eax
	movl	%eax, -8(%rdx,%r15,4)
	subq	$1, %r15
	cmpq	$1, %r15
	jne	.L2472
.L2470:
	movq	-104(%rbp), %rdx
	movl	$8, %r8d
	movl	$19, %ecx
	movq	%r12, %rdi
	movl	$10, %esi
	movq	%r11, -10536(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r15d
	movq	%r12, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movq	-112(%rbp), %rdx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	$19, %ecx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	leaq	-124(%rbp), %r10
	xorl	%edx, %edx
	movl	%ebx, %esi
	movq	%r10, %rdi
	movq	%r10, -10512(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	pushq	$0
	movq	-124(%rbp), %rdx
	movq	%r12, %rdi
	pushq	$56
	movl	-116(%rbp), %ecx
	movl	$102, %r8d
	movl	%r14d, %esi
	movl	$15, %r9d
	movq	%rdx, -96(%rbp)
	movl	%ecx, -88(%rbp)
	call	_ZN2v88internal9Assembler11ssse3_instrENS0_11XMMRegisterENS0_7OperandEhhhh@PLT
	movq	-10536(%rbp), %r11
	pxor	%xmm0, %xmm0
	popq	%rdi
	movq	-10512(%rbp), %r10
	movaps	%xmm0, -96(%rbp)
	popq	%r8
	movzbl	4(%r11), %eax
	movq	48(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L2473
	testb	$24, %al
	jne	.L2473
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3838
.L2473:
	movq	-10472(%rbp), %rax
	movq	%r10, -10512(%rbp)
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rsi
	movq	-10480(%rbp), %rax
	movq	24(%rax), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	movq	-10512(%rbp), %r10
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	movq	%r10, %rdi
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-124(%rbp), %rdx
	movl	-116(%rbp), %ecx
	movq	%r12, %rdi
	movl	-10504(%rbp), %esi
	movq	%rdx, -748(%rbp)
	movl	%ecx, -740(%rbp)
	movq	%rdx, -760(%rbp)
	movl	%ecx, -752(%rbp)
	movq	%rdx, -772(%rbp)
	movl	%ecx, -764(%rbp)
	movq	%rdx, -8128(%rbp)
	movl	%ecx, -8120(%rbp)
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterENS0_7OperandE@PLT
.L2474:
	movl	$5, %edx
	leaq	-10464(%rbp), %r10
.L2491:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %ecx
	leaq	5(%rdx,%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	-10480(%rbp), %rcx
	movq	40(%rcx), %r8
	movl	%eax, %ecx
	movq	%rax, %r9
	andl	$7, %ecx
	shrq	$3, %r9
	cmpl	$3, %ecx
	je	.L3839
	movq	112(%r8), %rax
	movl	%r9d, %ecx
	leaq	104(%r8), %rdi
	testq	%rax, %rax
	je	.L2480
	movq	%rdi, %rsi
	jmp	.L2481
	.p2align 4,,10
	.p2align 3
.L3840:
	movq	%rax, %rsi
	movq	16(%rax), %rax
.L2484:
	testq	%rax, %rax
	je	.L2482
.L2481:
	cmpl	32(%rax), %ecx
	jle	.L3840
	movq	24(%rax), %rax
	jmp	.L2484
.L2482:
	cmpq	%rsi, %rdi
	je	.L2480
	cmpl	%r9d, 32(%rsi)
	cmovle	%rsi, %rdi
.L2480:
	movq	48(%rdi), %rsi
.L2479:
	movl	%esi, %eax
	movl	%esi, %ecx
	movl	$32768, %edi
	andl	$15, %eax
	cmpb	$15, %sil
	movl	$128, %esi
	cmovbe	%esi, %eax
	movl	%ecx, %esi
	orl	-8(%r13,%rdx,4), %eax
	andl	$3840, %esi
	cmpb	$15, %ch
	cmovbe	%edi, %esi
	movl	%ecx, %edi
	shrl	$16, %edi
	orl	%esi, %eax
	movl	%ecx, %esi
	andl	$983040, %esi
	cmpb	$15, %dil
	movl	$8388608, %edi
	cmovbe	%edi, %esi
	orl	%esi, %eax
	movl	%ecx, %esi
	shrl	$24, %esi
	cmpl	$268435455, %ecx
	jbe	.L2488
	sall	$24, %esi
	andl	$251658240, %esi
	orl	%esi, %eax
	movl	%eax, -8(%r13,%rdx,4)
	subq	$1, %rdx
	cmpq	$1, %rdx
	jne	.L2491
.L2489:
	movq	-88(%rbp), %rdx
	movl	$8, %r8d
	movl	$19, %ecx
	movq	%r12, %rdi
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movq	-96(%rbp), %rdx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	$19, %ecx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	leaq	-136(%rbp), %rdi
	xorl	%edx, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	pushq	$0
	movl	-128(%rbp), %ecx
	movl	$15, %r9d
	pushq	$56
	movq	-10504(%rbp), %r15
	movl	$102, %r8d
	movq	%r12, %rdi
	movq	-136(%rbp), %rdx
	movl	%ecx, -116(%rbp)
	movl	%r15d, %esi
	movq	%rdx, -124(%rbp)
	call	_ZN2v88internal9Assembler11ssse3_instrENS0_11XMMRegisterENS0_7OperandEhhhh@PLT
	movl	$102, %ecx
	movl	%r15d, %esi
	movl	%r14d, %edx
	movl	$235, %r9d
	movl	$15, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	popq	%rcx
	popq	%rsi
.L2453:
	movl	-10528(%rbp), %edx
	movl	$8, %ecx
	movl	%ebx, %esi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L2469:
	orl	$-2147483648, %eax
	movl	%eax, -8(%rdx,%r15,4)
	subq	$1, %r15
	cmpq	$1, %r15
	jne	.L2472
	jmp	.L2470
.L2488:
	orl	$-2147483648, %eax
	movl	%eax, -8(%r13,%rdx,4)
	subq	$1, %rdx
	cmpq	$1, %rdx
	jne	.L2491
	jmp	.L2489
.L2111:
	movl	%esi, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6movapdENS0_11XMMRegisterES2_@PLT
	jmp	.L1716
.L1932:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3448(%rbp)
	movzbl	4(%rcx), %esi
	movl	%eax, -3440(%rbp)
	movq	%rdx, -3460(%rbp)
	movq	40(%rcx,%rsi,8), %rsi
	movl	%eax, %ecx
	movl	%eax, -3452(%rbp)
	movq	%rdx, -3472(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3464(%rbp)
	movq	%rdx, -9436(%rbp)
	movl	%eax, -9428(%rbp)
	call	_ZN2v88internal9Assembler5divsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2109
.L1915:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3700(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -3692(%rbp)
	movq	%rdx, -3712(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3704(%rbp)
	movq	%rdx, -3724(%rbp)
	movl	%eax, -3716(%rbp)
	movq	%rdx, -9520(%rbp)
	movl	%eax, -9512(%rbp)
	call	_ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1918:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3664(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -3656(%rbp)
	movq	%rdx, -3676(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3668(%rbp)
	movq	%rdx, -3688(%rbp)
	movl	%eax, -3680(%rbp)
	movq	%rdx, -9508(%rbp)
	movl	%eax, -9500(%rbp)
	call	_ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1890:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3988(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -3980(%rbp)
	movq	%rdx, -4000(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3992(%rbp)
	movq	%rdx, -4012(%rbp)
	movl	%eax, -4004(%rbp)
	movq	%rdx, -9616(%rbp)
	movl	%eax, -9608(%rbp)
	call	_ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1922:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rdx
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %eax
	movzbl	4(%rdx), %ecx
	movq	%r8, -3592(%rbp)
	movl	%eax, -3584(%rbp)
	movq	40(%rdx,%rcx,8), %rsi
	movq	%r8, -3604(%rbp)
	movl	%eax, -3596(%rbp)
	movq	%r8, -3616(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3608(%rbp)
	movq	%r8, -9484(%rbp)
	movl	%eax, -9476(%rbp)
	movq	%r8, -124(%rbp)
	movl	%eax, -116(%rbp)
	movq	%r8, -112(%rbp)
	movl	%eax, -104(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1924
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movl	%eax, %r9d
	pushq	$0
	movl	$46, %esi
	movq	%r12, %rdi
	pushq	$1
	pushq	$1
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
.L1925:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1904:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3772(%rbp)
	movzbl	4(%rcx), %esi
	movl	%eax, -3764(%rbp)
	movq	%rdx, -3784(%rbp)
	movq	40(%rcx,%rsi,8), %rsi
	movl	%eax, %ecx
	movl	%eax, -3776(%rbp)
	movq	%rdx, -3796(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3788(%rbp)
	movq	%rdx, -9544(%rbp)
	movl	%eax, -9536(%rbp)
	call	_ZN2v88internal9Assembler5divssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1905
.L1977:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rsi
	movq	-10480(%rbp), %rax
	movq	24(%rax), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rdx
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %eax
	movzbl	4(%rdx), %ecx
	movq	%r8, -3268(%rbp)
	movl	%eax, -3260(%rbp)
	movq	40(%rdx,%rcx,8), %rsi
	movq	%r8, -3280(%rbp)
	movl	%eax, -3272(%rbp)
	movq	%r8, -3292(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3284(%rbp)
	movq	%r8, -9376(%rbp)
	movl	%eax, -9368(%rbp)
	movq	%r8, -124(%rbp)
	movl	%eax, -116(%rbp)
	movq	%r8, -112(%rbp)
	movl	%eax, -104(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1980
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movl	%eax, %r9d
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1978
.L2356:
	movq	(%r8), %rax
	movq	40(%r15), %rcx
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3841
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2361
	movq	%rdi, %rcx
	jmp	.L2362
.L3842:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2365:
	testq	%rax, %rax
	je	.L2363
.L2362:
	cmpl	32(%rax), %edx
	jle	.L3842
	movq	24(%rax), %rax
	jmp	.L2365
.L2112:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rsi
	movq	-10480(%rbp), %rax
	movq	24(%rax), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %r8
	movl	%ebx, %ecx
	movl	-88(%rbp), %r9d
	movl	$84, %esi
	movq	%r12, %rdi
	movl	%eax, -2000(%rbp)
	movl	%eax, -2012(%rbp)
	movl	%eax, -2024(%rbp)
	movl	%eax, -8900(%rbp)
	movq	-10472(%rbp), %rax
	movq	%r8, -2008(%rbp)
	movq	40(%rax), %rdx
	movq	%r8, -2020(%rbp)
	movq	%r8, -2032(%rbp)
	sarq	$35, %rdx
	movq	%r8, -8908(%rbp)
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1231
.L2108:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	subq	$8, %rsp
	movq	-96(%rbp), %r8
	movq	%r12, %rdi
	movq	-10472(%rbp), %rdx
	movl	-88(%rbp), %r9d
	movl	$94, %esi
	movq	%r8, -2044(%rbp)
	movzbl	4(%rdx), %ecx
	movl	%r9d, -2036(%rbp)
	movq	%r8, -2056(%rbp)
	movq	40(%rdx,%rcx,8), %rcx
	movq	40(%rdx), %rdx
	pushq	$0
	pushq	$1
	pushq	$3
	sarq	$35, %rcx
	sarq	$35, %rdx
	movl	%r9d, -2048(%rbp)
	movq	%r8, -2068(%rbp)
	movl	%r9d, -2060(%rbp)
	movq	%r8, -8920(%rbp)
	movl	%r9d, -8912(%rbp)
	movq	%r8, -112(%rbp)
	movl	%r9d, -104(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L2109
.L2072:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -2548(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2540(%rbp)
	movq	%rdx, -2560(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2552(%rbp)
	movq	%rdx, -2572(%rbp)
	movl	%eax, -2564(%rbp)
	movq	%rdx, -9112(%rbp)
	movl	%eax, -9104(%rbp)
	call	_ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L2319:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2320
.L2056:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	%r11, -10504(%rbp)
	leaq	-10464(%rbp), %r12
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %r8
	movq	-10472(%rbp), %rcx
	movq	%r15, %rdi
	movq	%rdx, -2800(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2792(%rbp)
	movq	%rdx, -2812(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2804(%rbp)
	movq	%rdx, -2824(%rbp)
	movl	%eax, -2816(%rbp)
	movq	%rdx, -9196(%rbp)
	movl	%eax, -9188(%rbp)
	call	_ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L2057
.L2062:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -2728(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2720(%rbp)
	movq	%rdx, -2740(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2732(%rbp)
	movq	%rdx, -2752(%rbp)
	movl	%eax, -2744(%rbp)
	movq	%rdx, -9172(%rbp)
	movl	%eax, -9164(%rbp)
	call	_ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L2068:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -2620(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2612(%rbp)
	movq	%rdx, -2632(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2624(%rbp)
	movq	%rdx, -2644(%rbp)
	movl	%eax, -2636(%rbp)
	movq	%rdx, -9136(%rbp)
	movl	%eax, -9128(%rbp)
	call	_ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L2094:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %r8
	movq	%r12, %rdi
	movl	-88(%rbp), %r9d
	movl	$89, %esi
	movl	%eax, -2252(%rbp)
	movl	%eax, -2264(%rbp)
	movl	%eax, -2276(%rbp)
	movl	%eax, -8984(%rbp)
	movq	-10472(%rbp), %rax
	movq	%r8, -2260(%rbp)
	movzbl	4(%rax), %edx
	movq	%r8, -2272(%rbp)
	movq	%r8, -2284(%rbp)
	movq	40(%rax,%rdx,8), %rcx
	movq	40(%rax), %rdx
	movq	%r8, -8992(%rbp)
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1231
.L2051:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	%r11, -10504(%rbp)
	leaq	-10464(%rbp), %r12
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %r8
	movq	-10472(%rbp), %rcx
	movq	%r15, %rdi
	movq	%rdx, -2836(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2828(%rbp)
	movq	%rdx, -2848(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2840(%rbp)
	movq	%rdx, -2860(%rbp)
	movl	%eax, -2852(%rbp)
	movq	%rdx, -9208(%rbp)
	movl	%eax, -9200(%rbp)
	call	_ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L2052
.L2074:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -2512(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2504(%rbp)
	movq	%rdx, -2524(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2516(%rbp)
	movq	%rdx, -2536(%rbp)
	movl	%eax, -2528(%rbp)
	movq	%rdx, -9100(%rbp)
	movl	%eax, -9092(%rbp)
	call	_ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L2116:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rsi
	movq	-10480(%rbp), %rax
	movq	24(%rax), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %r8
	movl	%ebx, %ecx
	movl	-88(%rbp), %r9d
	movl	$84, %esi
	movq	%r12, %rdi
	movl	%eax, -1928(%rbp)
	movl	%eax, -1940(%rbp)
	movl	%eax, -1952(%rbp)
	movl	%eax, -8876(%rbp)
	movq	-10472(%rbp), %rax
	movq	%r8, -1936(%rbp)
	movq	40(%rax), %rdx
	movq	%r8, -1948(%rbp)
	movq	%r8, -1960(%rbp)
	sarq	$35, %rdx
	movq	%r8, -8884(%rbp)
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1231
.L2084:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %eax
	movq	40(%rcx), %rsi
	movq	%rdx, -2404(%rbp)
	movl	%eax, -2396(%rbp)
	movq	%rdx, -2416(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2408(%rbp)
	movq	%rdx, -2428(%rbp)
	movl	%eax, -2420(%rbp)
	movq	%rdx, -9040(%rbp)
	movl	%eax, -9032(%rbp)
	movq	%rdx, -112(%rbp)
	movl	%eax, -104(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2086
	movl	%eax, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5vmovdENS0_11XMMRegisterENS0_7OperandE@PLT
.L2087:
	xorl	%r13d, %r13d
	jmp	.L1231
.L2082:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movl	$1, %r8d
	movq	%rdx, -2440(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2432(%rbp)
	movq	%rdx, -2452(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2444(%rbp)
	movq	%rdx, -2464(%rbp)
	movl	%eax, -2456(%rbp)
	movq	%rdx, -9052(%rbp)
	movl	%eax, -9044(%rbp)
	call	_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_7OperandEa@PLT
	jmp	.L1231
.L2096:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %r8
	movq	%r12, %rdi
	movl	-88(%rbp), %r9d
	movl	$94, %esi
	movl	%eax, -2216(%rbp)
	movl	%eax, -2228(%rbp)
	movl	%eax, -2240(%rbp)
	movl	%eax, -8972(%rbp)
	movq	-10472(%rbp), %rax
	movq	%r8, -2224(%rbp)
	movzbl	4(%rax), %edx
	movq	%r8, -2236(%rbp)
	movq	%r8, -2248(%rbp)
	movq	40(%rax,%rdx,8), %rcx
	movq	40(%rax), %rdx
	movq	%r8, -8980(%rbp)
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L2097
.L2102:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	subq	$8, %rsp
	movq	-96(%rbp), %r8
	movq	%r12, %rdi
	movq	-10472(%rbp), %rdx
	movl	-88(%rbp), %r9d
	movl	$88, %esi
	movq	%r8, -2152(%rbp)
	movzbl	4(%rdx), %ecx
	movl	%r9d, -2144(%rbp)
	movq	%r8, -2164(%rbp)
	movq	40(%rdx,%rcx,8), %rcx
	movq	40(%rdx), %rdx
	pushq	$0
	pushq	$1
	pushq	$3
	sarq	$35, %rcx
	sarq	$35, %rdx
	movl	%r9d, -2156(%rbp)
	movq	%r8, -2176(%rbp)
	movl	%r9d, -2168(%rbp)
	movq	%r8, -8956(%rbp)
	movl	%r9d, -8948(%rbp)
	movq	%r8, -112(%rbp)
	movl	%r9d, -104(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L1231
.L2088:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -2368(%rbp)
	movzbl	4(%rcx), %esi
	movl	%eax, -2360(%rbp)
	movq	%rdx, -2380(%rbp)
	movq	40(%rcx,%rsi,8), %rsi
	movl	%eax, %ecx
	movl	%eax, -2372(%rbp)
	movq	%rdx, -2392(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2384(%rbp)
	movq	%rdx, -9028(%rbp)
	movl	%eax, -9020(%rbp)
	call	_ZN2v88internal9Assembler8vucomissENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L2064:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -2692(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2684(%rbp)
	movq	%rdx, -2704(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2696(%rbp)
	movq	%rdx, -2716(%rbp)
	movl	%eax, -2708(%rbp)
	movq	%rdx, -9160(%rbp)
	movl	%eax, -9152(%rbp)
	call	_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L2092:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %r8
	movq	%r12, %rdi
	movl	-88(%rbp), %r9d
	movl	$92, %esi
	movl	%eax, -2288(%rbp)
	movl	%eax, -2300(%rbp)
	movl	%eax, -2312(%rbp)
	movl	%eax, -8996(%rbp)
	movq	-10472(%rbp), %rax
	movq	%r8, -2296(%rbp)
	movzbl	4(%rax), %edx
	movq	%r8, -2308(%rbp)
	movq	%r8, -2320(%rbp)
	movq	40(%rax,%rdx,8), %rcx
	movq	40(%rax), %rdx
	movq	%r8, -9004(%rbp)
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1231
.L2090:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %r8
	movq	%r12, %rdi
	movl	-88(%rbp), %r9d
	movl	$88, %esi
	movl	%eax, -2324(%rbp)
	movl	%eax, -2336(%rbp)
	movl	%eax, -2348(%rbp)
	movl	%eax, -9008(%rbp)
	movq	-10472(%rbp), %rax
	movq	%r8, -2332(%rbp)
	movzbl	4(%rax), %edx
	movq	%r8, -2344(%rbp)
	movq	%r8, -2356(%rbp)
	movq	40(%rax,%rdx,8), %rcx
	movq	40(%rax), %rdx
	movq	%r8, -9016(%rbp)
	sarq	$35, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1231
.L2080:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	-10472(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rdx, -2476(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2468(%rbp)
	movq	%rdx, -2488(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2480(%rbp)
	movq	%rdx, -2500(%rbp)
	movl	%eax, -2492(%rbp)
	movq	%rdx, -9064(%rbp)
	movl	%eax, -9056(%rbp)
	call	_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_7OperandEa@PLT
	jmp	.L1231
.L2288:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movddupENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2289
.L2233:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rdx
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %eax
	movq	40(%rdx), %rsi
	movq	%r8, -1396(%rbp)
	movl	%eax, -1388(%rbp)
	movq	%r8, -1408(%rbp)
	sarq	$35, %rsi
	movl	%eax, -1400(%rbp)
	movq	%r8, -1420(%rbp)
	movl	%eax, -1412(%rbp)
	movq	%r8, -8416(%rbp)
	movl	%eax, -8408(%rbp)
	movq	%r8, -136(%rbp)
	movl	%eax, -128(%rbp)
	movq	%r8, -124(%rbp)
	movl	%eax, -116(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2235
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movl	%eax, %r9d
	pushq	$0
	movl	$16, %esi
	movq	%r12, %rdi
	pushq	$1
	pushq	$3
	movq	%r8, -112(%rbp)
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
.L2236:
	xorl	%r13d, %r13d
	jmp	.L1231
.L2369:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4movdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2370
.L2031:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -2980(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2972(%rbp)
	movq	%rdx, -2992(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2984(%rbp)
	movq	%rdx, -3004(%rbp)
	movl	%eax, -2996(%rbp)
	movq	%rdx, -9280(%rbp)
	movl	%eax, -9272(%rbp)
	call	_ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1231
.L2317:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	subq	$8, %rsp
	movq	-96(%rbp), %rdx
	movl	$15, %r9d
	movl	$102, %r8d
	movq	%r12, %rdi
	movl	%eax, -1172(%rbp)
	movl	%eax, -1184(%rbp)
	movl	%eax, -1196(%rbp)
	movl	%eax, -8264(%rbp)
	movq	-10472(%rbp), %rax
	movq	%rdx, -1180(%rbp)
	movzbl	4(%rax), %ecx
	movq	%rdx, -1192(%rbp)
	movq	%rdx, -1204(%rbp)
	movq	40(%rax,%rcx,8), %rsi
	movl	-88(%rbp), %ecx
	pushq	$94
	movq	%rdx, -8272(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterENS0_7OperandEhhh@PLT
	popq	%r11
	popq	%rbx
	jmp	.L1231
.L2315:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	subq	$8, %rsp
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movl	$15, %r9d
	movl	$102, %r8d
	movl	%eax, -1208(%rbp)
	movl	%eax, -1220(%rbp)
	movl	%eax, -1232(%rbp)
	movl	%eax, -8276(%rbp)
	movq	-10472(%rbp), %rax
	movq	%rdx, -1216(%rbp)
	movzbl	4(%rax), %ecx
	movq	%rdx, -1228(%rbp)
	movq	%rdx, -1240(%rbp)
	movq	40(%rax,%rcx,8), %rsi
	movl	-88(%rbp), %ecx
	pushq	$89
	movq	%rdx, -8284(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterENS0_7OperandEhhh@PLT
	popq	%r12
	popq	%r13
	xorl	%r13d, %r13d
	jmp	.L1231
.L2313:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	subq	$8, %rsp
	movq	-96(%rbp), %rdx
	movl	$15, %r9d
	movl	$102, %r8d
	movq	%r12, %rdi
	movl	%eax, -1244(%rbp)
	movl	%eax, -1256(%rbp)
	movl	%eax, -1268(%rbp)
	movl	%eax, -8288(%rbp)
	movq	-10472(%rbp), %rax
	movq	%rdx, -1252(%rbp)
	movzbl	4(%rax), %ecx
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1276(%rbp)
	movq	40(%rax,%rcx,8), %rsi
	movl	-88(%rbp), %ecx
	pushq	$92
	movq	%rdx, -8296(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterENS0_7OperandEhhh@PLT
	popq	%r14
	popq	%r15
	jmp	.L1231
.L2311:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	subq	$8, %rsp
	movq	-96(%rbp), %rdx
	movl	$15, %r9d
	movl	$102, %r8d
	movq	%r12, %rdi
	movl	%eax, -1280(%rbp)
	movl	%eax, -1292(%rbp)
	movl	%eax, -1304(%rbp)
	movl	%eax, -8300(%rbp)
	movq	-10472(%rbp), %rax
	movq	%rdx, -1288(%rbp)
	movzbl	4(%rax), %ecx
	movq	%rdx, -1300(%rbp)
	movq	%rdx, -1312(%rbp)
	movq	40(%rax,%rcx,8), %rsi
	movl	-88(%rbp), %ecx
	pushq	$88
	movq	%rdx, -8308(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterENS0_7OperandEhhh@PLT
	popq	%rax
	popq	%rdx
	jmp	.L1231
.L2291:
	movq	-8(%r11,%rdx), %rax
	movq	40(%r15), %rcx
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3843
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2296
	movq	%rdi, %rcx
	jmp	.L2297
.L3844:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2300:
	testq	%rax, %rax
	je	.L2298
.L2297:
	cmpl	32(%rax), %edx
	jle	.L3844
	movq	24(%rax), %rax
	jmp	.L2300
.L1968:
	movq	-10472(%rbp), %rax
	movq	%r11, -10504(%rbp)
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rsi
	movq	-10480(%rbp), %rax
	movq	24(%rax), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	movq	-10504(%rbp), %r11
	movq	%r8, -3304(%rbp)
	movl	%eax, -3296(%rbp)
	movq	%r8, -3316(%rbp)
	movl	%eax, -3308(%rbp)
	movq	%r8, -3328(%rbp)
	movl	%eax, -3320(%rbp)
	movq	%r8, -9388(%rbp)
	movl	%eax, -9380(%rbp)
	movq	%r8, -124(%rbp)
	movl	%eax, -116(%rbp)
	movq	%r8, -112(%rbp)
	movl	%eax, -104(%rbp)
	je	.L1973
	movl	%eax, %r9d
	movl	$15, %edx
	movl	$16, %esi
	movq	%r14, %rdi
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	movq	-10504(%rbp), %r11
.L1974:
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	movq	%r11, -10504(%rbp)
	je	.L1975
	movl	$15, %r8d
.L3631:
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$10, %edx
	movl	$80, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1971
.L1961:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	movq	%r11, -10504(%rbp)
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %eax
	movq	-10504(%rbp), %r11
	movzbl	4(%rcx), %esi
	movq	%rdx, -3340(%rbp)
	movl	%eax, -3332(%rbp)
	movq	40(%rcx,%rsi,8), %rsi
	movq	%rdx, -3352(%rbp)
	movl	%eax, -3344(%rbp)
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	movq	%rdx, -3364(%rbp)
	movl	%eax, -3356(%rbp)
	movq	%rdx, -9400(%rbp)
	movl	%eax, -9392(%rbp)
	movq	%rdx, -112(%rbp)
	movl	%eax, -104(%rbp)
	je	.L1964
	movl	%eax, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler8vucomissENS0_11XMMRegisterENS0_7OperandE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1962
.L2114:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rsi
	movq	-10480(%rbp), %rax
	movq	24(%rax), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %r8
	movl	%ebx, %ecx
	movl	-88(%rbp), %r9d
	movl	$87, %esi
	movq	%r12, %rdi
	movl	%eax, -1964(%rbp)
	movl	%eax, -1976(%rbp)
	movl	%eax, -1988(%rbp)
	movl	%eax, -8888(%rbp)
	movq	-10472(%rbp), %rax
	movq	%r8, -1972(%rbp)
	movq	40(%rax), %rdx
	movq	%r8, -1984(%rbp)
	movq	%r8, -1996(%rbp)
	sarq	$35, %rdx
	movq	%r8, -8896(%rbp)
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1231
.L2344:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2345
.L2429:
	movq	(%r8), %rax
	movq	40(%r15), %rcx
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3845
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2434
	movq	%rdi, %rcx
	jmp	.L2435
.L3846:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2438:
	testq	%rax, %rax
	je	.L2436
.L2435:
	cmpl	32(%rax), %edx
	jle	.L3846
	movq	24(%rax), %rax
	jmp	.L2438
.L2060:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -2764(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2756(%rbp)
	movq	%rdx, -2776(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2768(%rbp)
	movq	%rdx, -2788(%rbp)
	movl	%eax, -2780(%rbp)
	movq	%rdx, -9184(%rbp)
	movl	%eax, -9176(%rbp)
	call	_ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L2012:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rsi
	movq	-10480(%rbp), %rax
	movq	24(%rax), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rdx
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %eax
	movzbl	4(%rdx), %ecx
	movq	%r8, -3088(%rbp)
	movl	%eax, -3080(%rbp)
	movq	40(%rdx,%rcx,8), %rsi
	movq	%r8, -3100(%rbp)
	movl	%eax, -3092(%rbp)
	movq	%r8, -3112(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3104(%rbp)
	movq	%r8, -9316(%rbp)
	movl	%eax, -9308(%rbp)
	movq	%r8, -136(%rbp)
	movl	%eax, -128(%rbp)
	movq	%r8, -124(%rbp)
	movl	%eax, -116(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2015
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movl	%eax, %r9d
	pushq	$0
	movl	$16, %esi
	movq	%r14, %rdi
	pushq	$1
	pushq	$3
	movq	%r8, -112(%rbp)
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L2013
.L2029:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3016(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -3008(%rbp)
	movq	%rdx, -3028(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3020(%rbp)
	movq	%rdx, -3040(%rbp)
	movl	%eax, -3032(%rbp)
	movq	%rdx, -9292(%rbp)
	movl	%eax, -9284(%rbp)
	call	_ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1991:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rsi
	movq	-10480(%rbp), %rax
	movq	24(%rax), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rdx
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %eax
	movzbl	4(%rdx), %ecx
	movq	%r8, -3196(%rbp)
	movl	%eax, -3188(%rbp)
	movq	40(%rdx,%rcx,8), %rsi
	movq	%r8, -3208(%rbp)
	movl	%eax, -3200(%rbp)
	movq	%r8, -3220(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3212(%rbp)
	movq	%r8, -9352(%rbp)
	movl	%eax, -9344(%rbp)
	movq	%r8, -136(%rbp)
	movl	%eax, -128(%rbp)
	movq	%r8, -124(%rbp)
	movl	%eax, -116(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1994
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movl	%eax, %r9d
	pushq	$0
	movl	$16, %esi
	movq	%r14, %rdi
	pushq	$1
	pushq	$3
	movq	%r8, -112(%rbp)
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L1992
.L1982:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	movq	%r11, -10504(%rbp)
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rdx
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %eax
	movq	-10504(%rbp), %r11
	movzbl	4(%rdx), %ecx
	movq	%r8, -3232(%rbp)
	movl	%eax, -3224(%rbp)
	movq	40(%rdx,%rcx,8), %rsi
	movq	%r8, -3244(%rbp)
	movl	%eax, -3236(%rbp)
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	movq	%r8, -3256(%rbp)
	movl	%eax, -3248(%rbp)
	movq	%r8, -9364(%rbp)
	movl	%eax, -9356(%rbp)
	movq	%r8, -124(%rbp)
	movl	%eax, -116(%rbp)
	movq	%r8, -112(%rbp)
	movl	%eax, -104(%rbp)
	je	.L1985
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movl	%eax, %r9d
	pushq	$0
	movl	$46, %esi
	movq	%r14, %rdi
	pushq	$1
	pushq	$1
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	movq	-10504(%rbp), %r11
	addq	$32, %rsp
	jmp	.L1983
.L2229:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rdx
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %eax
	movq	40(%rdx), %rsi
	movq	%r8, -1432(%rbp)
	movl	%eax, -1424(%rbp)
	movq	%r8, -1444(%rbp)
	sarq	$35, %rsi
	movl	%eax, -1436(%rbp)
	movq	%r8, -1456(%rbp)
	movl	%eax, -1448(%rbp)
	movq	%r8, -8428(%rbp)
	movl	%eax, -8420(%rbp)
	movq	%r8, -124(%rbp)
	movl	%eax, -116(%rbp)
	movq	%r8, -112(%rbp)
	movl	%eax, -104(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2231
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movl	%eax, %r9d
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
.L2232:
	xorl	%r13d, %r13d
	jmp	.L1231
.L2033:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	movq	%r11, -10504(%rbp)
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -2944(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2936(%rbp)
	movq	%rdx, -2956(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2948(%rbp)
	movq	%rdx, -2968(%rbp)
	movl	%eax, -2960(%rbp)
	movq	%rdx, -9268(%rbp)
	movl	%eax, -9260(%rbp)
	call	_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_7OperandE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L2034
.L1886:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -4060(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -4052(%rbp)
	movq	%rdx, -4072(%rbp)
	sarq	$35, %rsi
	movl	%eax, -4064(%rbp)
	movq	%rdx, -4084(%rbp)
	movl	%eax, -4076(%rbp)
	movq	%rdx, -9640(%rbp)
	movl	%eax, -9632(%rbp)
	call	_ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1231
.L2417:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4movdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2418
.L2070:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -2584(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2576(%rbp)
	movq	%rdx, -2596(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2588(%rbp)
	movq	%rdx, -2608(%rbp)
	movl	%eax, -2600(%rbp)
	movq	%rdx, -9124(%rbp)
	movl	%eax, -9116(%rbp)
	call	_ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1826:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movl	$4, %ecx
	call	_ZN2v88internal9Assembler8emit_notENS0_7OperandEi@PLT
	jmp	.L1231
.L2381:
	movq	(%r8), %rax
	movq	40(%r15), %rcx
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3847
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2386
	movq	%rdi, %rcx
	jmp	.L2387
.L3848:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2390:
	testq	%rax, %rax
	je	.L2388
.L2387:
	cmpl	32(%rax), %edx
	jle	.L3848
	movq	24(%rax), %rax
	jmp	.L2390
.L2106:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	subq	$8, %rsp
	movq	-96(%rbp), %r8
	movq	%r12, %rdi
	movq	-10472(%rbp), %rdx
	movl	-88(%rbp), %r9d
	movl	$89, %esi
	movq	%r8, -2080(%rbp)
	movzbl	4(%rdx), %ecx
	movl	%r9d, -2072(%rbp)
	movq	%r8, -2092(%rbp)
	movq	40(%rdx,%rcx,8), %rcx
	movq	40(%rdx), %rdx
	pushq	$0
	pushq	$1
	pushq	$3
	sarq	$35, %rcx
	sarq	$35, %rdx
	movl	%r9d, -2084(%rbp)
	movq	%r8, -2104(%rbp)
	movl	%r9d, -2096(%rbp)
	movq	%r8, -8932(%rbp)
	movl	%r9d, -8924(%rbp)
	movq	%r8, -112(%rbp)
	movl	%r9d, -104(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L1231
.L2118:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rsi
	movq	-10480(%rbp), %rax
	movq	24(%rax), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %r8
	movl	%ebx, %ecx
	movl	-88(%rbp), %r9d
	movl	$87, %esi
	movq	%r12, %rdi
	movl	%eax, -1892(%rbp)
	movl	%eax, -1904(%rbp)
	movl	%eax, -1916(%rbp)
	movl	%eax, -8864(%rbp)
	movq	-10472(%rbp), %rax
	movq	%r8, -1900(%rbp)
	movq	40(%rax), %rdx
	movq	%r8, -1912(%rbp)
	movq	%r8, -1924(%rbp)
	sarq	$35, %rdx
	movq	%r8, -8872(%rbp)
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1231
.L2104:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	subq	$8, %rsp
	movq	-96(%rbp), %r8
	movq	%r12, %rdi
	movq	-10472(%rbp), %rdx
	movl	-88(%rbp), %r9d
	movl	$92, %esi
	movq	%r8, -2116(%rbp)
	movzbl	4(%rdx), %ecx
	movl	%r9d, -2108(%rbp)
	movq	%r8, -2128(%rbp)
	movq	40(%rdx,%rcx,8), %rcx
	movq	40(%rdx), %rdx
	pushq	$0
	pushq	$1
	pushq	$3
	sarq	$35, %rcx
	sarq	$35, %rdx
	movl	%r9d, -2120(%rbp)
	movq	%r8, -2140(%rbp)
	movl	%r9d, -2132(%rbp)
	movq	%r8, -8944(%rbp)
	movl	%r9d, -8936(%rbp)
	movq	%r8, -112(%rbp)
	movl	%r9d, -104(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L1231
.L2066:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -2656(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2648(%rbp)
	movq	%rdx, -2668(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2660(%rbp)
	movq	%rdx, -2680(%rbp)
	movl	%eax, -2672(%rbp)
	movq	%rdx, -9148(%rbp)
	movl	%eax, -9140(%rbp)
	call	_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L2100:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	subq	$8, %rsp
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$46, %esi
	movq	%r12, %rdi
	movl	%eax, -2180(%rbp)
	movl	%eax, -2192(%rbp)
	movl	%eax, -2204(%rbp)
	movl	%eax, -8960(%rbp)
	movq	-10472(%rbp), %rax
	movq	%r8, -2188(%rbp)
	movzbl	4(%rax), %edx
	movq	%r8, -2200(%rbp)
	movq	%r8, -2212(%rbp)
	movq	40(%rax,%rdx,8), %rdx
	pushq	$0
	pushq	$1
	pushq	$1
	sarq	$35, %rdx
	movq	%r8, -8968(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L1231
.L2003:
	movq	-10472(%rbp), %rax
	movq	%r11, -10504(%rbp)
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rsi
	movq	-10480(%rbp), %rax
	movq	24(%rax), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	movq	-10504(%rbp), %r11
	movq	%r8, -3124(%rbp)
	movl	%eax, -3116(%rbp)
	movq	%r8, -3136(%rbp)
	movl	%eax, -3128(%rbp)
	movq	%r8, -3148(%rbp)
	movl	%eax, -3140(%rbp)
	movq	%r8, -9328(%rbp)
	movl	%eax, -9320(%rbp)
	movq	%r8, -136(%rbp)
	movl	%eax, -128(%rbp)
	movq	%r8, -124(%rbp)
	movl	%eax, -116(%rbp)
	je	.L2008
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%eax, %r9d
	movq	%r14, %rdi
	pushq	$0
	movl	$15, %edx
	movl	$16, %esi
	pushq	$1
	pushq	$3
	movq	%r8, -112(%rbp)
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	movq	-10504(%rbp), %r11
	addq	$32, %rsp
.L2009:
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	movq	%r11, -10504(%rbp)
	je	.L2010
	movl	$15, %r8d
.L3632:
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$10, %edx
	movl	$80, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	movq	-10504(%rbp), %r11
	jmp	.L2006
.L1996:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	movq	%r11, -10504(%rbp)
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rdx
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %eax
	movq	-10504(%rbp), %r11
	movzbl	4(%rdx), %ecx
	movq	%r8, -3160(%rbp)
	movl	%eax, -3152(%rbp)
	movq	40(%rdx,%rcx,8), %rsi
	movq	%r8, -3172(%rbp)
	movl	%eax, -3164(%rbp)
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	movq	%r8, -3184(%rbp)
	movl	%eax, -3176(%rbp)
	movq	%r8, -9340(%rbp)
	movl	%eax, -9332(%rbp)
	movq	%r8, -124(%rbp)
	movl	%eax, -116(%rbp)
	movq	%r8, -112(%rbp)
	movl	%eax, -104(%rbp)
	je	.L1999
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movl	%eax, %r9d
	pushq	$0
	movl	$46, %esi
	movq	%r14, %rdi
	pushq	$1
	pushq	$1
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	movq	-10504(%rbp), %r11
	addq	$32, %rsp
	jmp	.L1997
.L1882:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -4132(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -4124(%rbp)
	movq	%rdx, -4144(%rbp)
	sarq	$35, %rsi
	movl	%eax, -4136(%rbp)
	movq	%rdx, -4156(%rbp)
	movl	%eax, -4148(%rbp)
	movq	%rdx, -9664(%rbp)
	movl	%eax, -9656(%rbp)
	call	_ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1900:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3844(%rbp)
	movzbl	4(%rcx), %esi
	movl	%eax, -3836(%rbp)
	movq	%rdx, -3856(%rbp)
	movq	40(%rcx,%rsi,8), %rsi
	movl	%eax, %ecx
	movl	%eax, -3848(%rbp)
	movq	%rdx, -3868(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3860(%rbp)
	movq	%rdx, -9568(%rbp)
	movl	%eax, -9560(%rbp)
	call	_ZN2v88internal9Assembler5subssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1824:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movl	$8, %ecx
	call	_ZN2v88internal9Assembler8emit_notENS0_7OperandEi@PLT
	jmp	.L1231
.L1892:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3952(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -3944(%rbp)
	movq	%rdx, -3964(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3956(%rbp)
	movq	%rdx, -3976(%rbp)
	movl	%eax, -3968(%rbp)
	movq	%rdx, -9604(%rbp)
	movl	%eax, -9596(%rbp)
	call	_ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1828:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movl	$8, %ecx
	call	_ZN2v88internal9Assembler8emit_negENS0_7OperandEi@PLT
	jmp	.L1231
.L1898:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3880(%rbp)
	movzbl	4(%rcx), %esi
	movl	%eax, -3872(%rbp)
	movq	%rdx, -3892(%rbp)
	movq	40(%rcx,%rsi,8), %rsi
	movl	%eax, %ecx
	movl	%eax, -3884(%rbp)
	movq	%rdx, -3904(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3896(%rbp)
	movq	%rdx, -9580(%rbp)
	movl	%eax, -9572(%rbp)
	call	_ZN2v88internal9Assembler5addssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1928:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3520(%rbp)
	movzbl	4(%rcx), %esi
	movl	%eax, -3512(%rbp)
	movq	%rdx, -3532(%rbp)
	movq	40(%rcx,%rsi,8), %rsi
	movl	%eax, %ecx
	movl	%eax, -3524(%rbp)
	movq	%rdx, -3544(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3536(%rbp)
	movq	%rdx, -9460(%rbp)
	movl	%eax, -9452(%rbp)
	call	_ZN2v88internal9Assembler5subsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1956:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rsi
	movq	-10480(%rbp), %rax
	movq	24(%rax), %rdi
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rdx
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %eax
	movzbl	4(%rdx), %ecx
	movq	%r8, -3376(%rbp)
	movl	%eax, -3368(%rbp)
	movq	40(%rdx,%rcx,8), %rsi
	movq	%r8, -3388(%rbp)
	movl	%eax, -3380(%rbp)
	movq	%r8, -3400(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3392(%rbp)
	movq	%r8, -9412(%rbp)
	movl	%eax, -9404(%rbp)
	movq	%r8, -124(%rbp)
	movl	%eax, -116(%rbp)
	movq	%r8, -112(%rbp)
	movl	%eax, -104(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1959
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movl	%eax, %r9d
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1957
.L1947:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	movq	%r11, -10504(%rbp)
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %eax
	movq	-10504(%rbp), %r11
	movzbl	4(%rcx), %esi
	movq	%rdx, -3412(%rbp)
	movl	%eax, -3404(%rbp)
	movq	40(%rcx,%rsi,8), %rsi
	movq	%rdx, -3424(%rbp)
	movl	%eax, -3416(%rbp)
	sarq	$35, %rsi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	movq	%rdx, -3436(%rbp)
	movl	%eax, -3428(%rbp)
	movq	%rdx, -9424(%rbp)
	movl	%eax, -9416(%rbp)
	movq	%rdx, -112(%rbp)
	movl	%eax, -104(%rbp)
	je	.L1950
	movl	%eax, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler8vucomissENS0_11XMMRegisterENS0_7OperandE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1948
.L1926:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3556(%rbp)
	movzbl	4(%rcx), %esi
	movl	%eax, -3548(%rbp)
	movq	%rdx, -3568(%rbp)
	movq	40(%rcx,%rsi,8), %rsi
	movl	%eax, %ecx
	movl	%eax, -3560(%rbp)
	movq	%rdx, -3580(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3572(%rbp)
	movq	%rdx, -9472(%rbp)
	movl	%eax, -9464(%rbp)
	call	_ZN2v88internal9Assembler5addsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L2043:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	movq	%r11, -10504(%rbp)
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -2872(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2864(%rbp)
	movq	%rdx, -2884(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2876(%rbp)
	movq	%rdx, -2896(%rbp)
	movl	%eax, -2888(%rbp)
	movq	%rdx, -9232(%rbp)
	movl	%eax, -9224(%rbp)
	call	_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_7OperandE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L2044
.L2036:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	movq	%r11, -10504(%rbp)
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -2908(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -2900(%rbp)
	movq	%rdx, -2920(%rbp)
	sarq	$35, %rsi
	movl	%eax, -2912(%rbp)
	movq	%rdx, -2932(%rbp)
	movl	%eax, -2924(%rbp)
	movq	%rdx, -9256(%rbp)
	movl	%eax, -9248(%rbp)
	call	_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_7OperandE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L2037
.L2405:
	movq	(%r8), %rax
	movq	40(%r15), %rcx
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3849
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2410
	movq	%rdi, %rcx
	jmp	.L2411
.L3850:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2414:
	testq	%rax, %rax
	je	.L2412
.L2411:
	cmpl	32(%rax), %edx
	jle	.L3850
	movq	24(%rax), %rax
	jmp	.L2414
.L2393:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4movdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2394
.L1902:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3808(%rbp)
	movzbl	4(%rcx), %esi
	movl	%eax, -3800(%rbp)
	movq	%rdx, -3820(%rbp)
	movq	40(%rcx,%rsi,8), %rsi
	movl	%eax, %ecx
	movl	%eax, -3812(%rbp)
	movq	%rdx, -3832(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3824(%rbp)
	movq	%rdx, -9556(%rbp)
	movl	%eax, -9548(%rbp)
	call	_ZN2v88internal9Assembler5mulssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1884:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -4096(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -4088(%rbp)
	movq	%rdx, -4108(%rbp)
	sarq	$35, %rsi
	movl	%eax, -4100(%rbp)
	movq	%rdx, -4120(%rbp)
	movl	%eax, -4112(%rbp)
	movq	%rdx, -9652(%rbp)
	movl	%eax, -9644(%rbp)
	call	_ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1913:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3736(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -3728(%rbp)
	movq	%rdx, -3748(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3740(%rbp)
	movq	%rdx, -3760(%rbp)
	movl	%eax, -3752(%rbp)
	movq	%rdx, -9532(%rbp)
	movl	%eax, -9524(%rbp)
	call	_ZN2v88internal9Assembler6sqrtssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1894:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %eax
	movzbl	4(%rcx), %esi
	movq	%rdx, -3916(%rbp)
	movl	%eax, -3908(%rbp)
	movq	40(%rcx,%rsi,8), %rsi
	movq	%rdx, -3928(%rbp)
	movl	%eax, -3920(%rbp)
	movq	%rdx, -3940(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3932(%rbp)
	movq	%rdx, -9592(%rbp)
	movl	%eax, -9584(%rbp)
	movq	%rdx, -112(%rbp)
	movl	%eax, -104(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1896
	movl	%eax, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8vucomissENS0_11XMMRegisterENS0_7OperandE@PLT
.L1897:
	xorl	%r13d, %r13d
	jmp	.L1231
.L1888:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -4024(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -4016(%rbp)
	movq	%rdx, -4036(%rbp)
	sarq	$35, %rsi
	movl	%eax, -4028(%rbp)
	movq	%rdx, -4048(%rbp)
	movl	%eax, -4040(%rbp)
	movq	%rdx, -9628(%rbp)
	movl	%eax, -9620(%rbp)
	call	_ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1930:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3484(%rbp)
	movzbl	4(%rcx), %esi
	movl	%eax, -3476(%rbp)
	movq	%rdx, -3496(%rbp)
	movq	40(%rcx,%rsi,8), %rsi
	movl	%eax, %ecx
	movl	%eax, -3488(%rbp)
	movq	%rdx, -3508(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3500(%rbp)
	movq	%rdx, -9448(%rbp)
	movl	%eax, -9440(%rbp)
	call	_ZN2v88internal9Assembler5mulsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1830:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movl	$4, %ecx
	call	_ZN2v88internal9Assembler8emit_negENS0_7OperandEi@PLT
	jmp	.L1231
.L1920:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -3628(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -3620(%rbp)
	movq	%rdx, -3640(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3632(%rbp)
	movq	%rdx, -3652(%rbp)
	movl	%eax, -3644(%rbp)
	movq	%rdx, -9496(%rbp)
	movl	%eax, -9488(%rbp)
	call	_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1820:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movl	$4, %ecx
	call	_ZN2v88internal9Assembler9emit_imulENS0_7OperandEi@PLT
	jmp	.L1231
.L1822:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4mullENS0_7OperandE@PLT
	jmp	.L1231
.L2023:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rdx
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %eax
	movq	40(%rdx), %rsi
	movq	%r8, -3052(%rbp)
	movl	%eax, -3044(%rbp)
	movq	%r8, -3064(%rbp)
	sarq	$35, %rsi
	movl	%eax, -3056(%rbp)
	movq	%r8, -3076(%rbp)
	movl	%eax, -3068(%rbp)
	movq	%r8, -9304(%rbp)
	movl	%eax, -9296(%rbp)
	movq	%r8, -136(%rbp)
	movl	%eax, -128(%rbp)
	movq	%r8, -124(%rbp)
	movl	%eax, -116(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2026
	subq	$8, %rsp
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	%eax, %r9d
	pushq	$0
	movl	$81, %esi
	movq	%r12, %rdi
	pushq	$1
	pushq	$3
	movq	%r8, -112(%rbp)
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
.L2027:
	xorl	%r13d, %r13d
	jmp	.L1231
.L2337:
	cmpq	%rcx, %rsi
	je	.L2335
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L2335:
	movq	48(%rsi), %r14
	movq	%r8, %rdx
	movq	%r11, %rcx
.L2334:
	movq	56(%rcx,%rdx,8), %rdx
	movq	56(%r11,%r8,8), %rax
	sall	$4, %r14d
	addq	$200, %r15
	andl	$48, %r14d
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2340
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2340
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2340
	movq	40(%rcx), %rsi
	movq	%r15, %rdi
	movl	%r14d, %ecx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8insertpsENS0_11XMMRegisterES2_h@PLT
	jmp	.L1231
.L2340:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movl	%r14d, %r8d
	movq	-10472(%rbp), %rcx
	movq	%r15, %rdi
	movq	%rdx, -1108(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -1100(%rbp)
	movq	%rdx, -1120(%rbp)
	sarq	$35, %rsi
	movl	%eax, -1112(%rbp)
	movq	%rdx, -1132(%rbp)
	movl	%eax, -1124(%rbp)
	movq	%rdx, -8248(%rbp)
	movl	%eax, -8240(%rbp)
	call	_ZN2v88internal9Assembler8insertpsENS0_11XMMRegisterENS0_7OperandEh@PLT
	jmp	.L1231
.L2308:
	cmpq	%rcx, %rdi
	je	.L2306
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2306:
	movq	48(%rdi), %rax
.L2305:
	movq	40(%r11,%rsi,8), %rdx
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movsbl	%al, %ecx
	movq	%r12, %rdi
	movl	%r13d, %esi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler6pextrqENS0_8RegisterENS0_11XMMRegisterEa@PLT
	movq	-10472(%rbp), %rax
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	jmp	.L1231
.L1712:
	cmpq	%rsi, %rcx
	je	.L1710
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L1710:
	movl	40(%rsi), %edx
	movq	48(%rsi), %r12
.L1709:
	testl	%edx, %edx
	movslq	%r12d, %rax
	cmove	%rax, %r12
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	je	.L1716
	leaq	-80(%rbp), %rbx
	leaq	240(%r15), %r14
	movq	%rbx, -96(%rbp)
	leaq	-96(%rbp), %r13
	testq	%r12, %r12
	je	.L1717
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r12, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %r8
	movq	%r8, -10464(%rbp)
	cmpq	$15, %r8
	ja	.L3851
	cmpq	$1, %r8
	jne	.L1720
	movzbl	(%r12), %eax
	movb	%al, -80(%rbp)
.L1721:
	movq	-10464(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r13, %rdx
	movq	232(%r15), %rsi
	subq	216(%r15), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1722
	call	_ZdlPv@PLT
.L1722:
	xorl	%r13d, %r13d
	jmp	.L1231
.L2378:
	cmpq	%rcx, %rdi
	je	.L2376
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2376:
	movq	48(%rdi), %rax
.L2375:
	movq	40(%r11,%rsi,8), %rdx
	movq	40(%r11), %rsi
	movsbl	%al, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler6PextrdENS0_8RegisterENS0_11XMMRegisterEa@PLT
	jmp	.L1231
.L2328:
	cmpq	%rcx, %rdi
	je	.L2326
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2326:
	movq	48(%rdi), %rax
.L2325:
	movq	40(%r11,%rsi,8), %rdx
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movzbl	%al, %ecx
	movq	%r12, %rdi
	movl	%r13d, %esi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler9extractpsENS0_8RegisterENS0_11XMMRegisterEh@PLT
	movq	-10472(%rbp), %rax
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler4movdENS0_11XMMRegisterENS0_8RegisterE@PLT
	jmp	.L1231
.L2279:
	cmpq	%rsi, %rcx
	je	.L2277
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L2277:
	movq	48(%rsi), %rax
.L2276:
	movq	24(%r15), %rdx
	addq	$200, %r15
	movq	(%rdx), %rdx
	subl	4(%rdx), %eax
	leal	8(,%rax,8), %edx
	movq	40(%r11), %rax
	testb	$4, %al
	je	.L2282
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L2282
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2282
	cmpb	$13, %al
	je	.L3852
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-136(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rax
	movq	-136(%rbp), %r8
	movl	-128(%rbp), %r9d
	movq	40(%rax), %rsi
	movq	%r8, -124(%rbp)
	movl	%r9d, -116(%rbp)
	movq	%r8, -112(%rbp)
	sarq	$35, %rsi
	movl	%r9d, -104(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2286
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movq	%r15, %rdi
	movl	$16, %esi
	movq	%r8, -96(%rbp)
	movl	%r9d, -88(%rbp)
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L2282:
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-112(%rbp), %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-112(%rbp), %rdx
	movl	-104(%rbp), %ecx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movl	$8, %r8d
	movq	40(%rax), %rsi
	movq	%rdx, -96(%rbp)
	movl	%ecx, -88(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1702:
	cmpq	%rsi, %rcx
	je	.L1700
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L1700:
	movq	48(%rsi), %rbx
.L1699:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13CodeGenerator21IsNextInAssemblyOrderENS1_9RpoNumberE@PLT
	testb	%al, %al
	je	.L3853
.L1705:
	xorl	%r13d, %r13d
	jmp	.L1231
.L2426:
	cmpq	%rcx, %rdi
	je	.L2424
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2424:
	movq	48(%rdi), %rax
.L2423:
	movq	40(%r11,%rsi,8), %rdx
	movsbl	%al, %ecx
	movl	%ebx, %esi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler6pextrbENS0_8RegisterENS0_11XMMRegisterEa@PLT
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movsxblENS0_8RegisterES2_@PLT
	jmp	.L1231
.L2402:
	cmpq	%rcx, %rdi
	je	.L2400
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2400:
	movq	48(%rdi), %rax
.L2399:
	movq	40(%r11,%rsi,8), %rdx
	movsbl	%al, %ecx
	movl	%ebx, %esi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler6pextrwENS0_8RegisterENS0_11XMMRegisterEa@PLT
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movsxwlENS0_8RegisterES2_@PLT
	jmp	.L1231
.L1735:
	cmpq	%rsi, %rcx
	je	.L1733
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L1733:
	movq	48(%rsi), %rsi
.L1732:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	leaq	-112(%rbp), %rdi
	testb	$1, %al
	cmove	_ZN2v88internalL3rbpE(%rip), %esi
	andl	$-2, %eax
	xorl	%r13d, %r13d
	movl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rax
	movq	-112(%rbp), %rdx
	leaq	200(%r15), %rdi
	movl	-104(%rbp), %ecx
	movl	$8, %r8d
	movq	40(%rax), %rsi
	movq	%rdx, -96(%rbp)
	movl	%ecx, -88(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L2614:
	cmpq	%rcx, %rdi
	je	.L2612
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2612:
	movq	48(%rdi), %rax
.L2611:
	movq	48(%r11,%rsi,8), %rdx
	movq	40(%r11), %rsi
	movzbl	%al, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7palignrENS0_11XMMRegisterES2_h@PLT
	jmp	.L1231
.L2640:
	cmpq	%rcx, %rsi
	je	.L2638
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L2638:
	movq	48(%rsi), %rax
.L2637:
	movl	%eax, %r13d
	addq	$200, %r15
	movl	$96, %r9d
	testb	$8, %al
	je	.L3638
	movl	$104, %r9d
.L3638:
	movl	$102, %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %eax
	movl	%r13d, %edx
	movq	%r15, %rdi
	andl	$3, %eax
	andl	$3, %edx
	movl	%eax, %esi
	leal	0(,%rax,4), %ecx
	sall	$6, %eax
	sall	$4, %esi
	orl	%esi, %ecx
	movl	%ebx, %esi
	orl	%edx, %ecx
	movl	%ebx, %edx
	orl	%eax, %ecx
	andl	$4, %r13d
	movzbl	%cl, %ecx
	jne	.L2645
	call	_ZN2v88internal9Assembler7pshuflwENS0_11XMMRegisterES2_h@PLT
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler6pshufdENS0_11XMMRegisterES2_h@PLT
	jmp	.L1231
.L2533:
	cmpq	%rcx, %rdi
	je	.L2531
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2531:
	movq	48(%rdi), %rax
.L2530:
	movq	48(%r11,%rsi,8), %rdx
	movq	40(%r11), %rsi
	movzbl	%al, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7pblendwENS0_11XMMRegisterES2_h@PLT
	jmp	.L1231
.L2353:
	cmpq	%rcx, %rdi
	je	.L2351
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2351:
	movq	48(%rdi), %rax
.L2350:
	movq	40(%r11,%rsi,8), %rdx
	movq	40(%r11), %rsi
	movsbl	%al, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	sarq	$35, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler6pextrqENS0_8RegisterENS0_11XMMRegisterEa@PLT
	jmp	.L1231
.L2509:
	cmpq	%rcx, %rdi
	je	.L2507
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2507:
	movq	48(%rdi), %r13
	movq	%rsi, %rcx
	movq	%r11, %rdx
.L2506:
	movq	48(%r11,%rsi,8), %rax
	movq	48(%rdx,%rcx,8), %rsi
	movzbl	%r13b, %r13d
	addq	$200, %r15
	sarq	$35, %rsi
	testb	$4, %al
	je	.L2512
	testb	$24, %al
	jne	.L2512
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3854
.L2512:
	movq	-10480(%rbp), %rax
	movq	%r11, -10504(%rbp)
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %ecx
	movl	%r13d, %r8d
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movq	%r15, %rdi
	movq	%rdx, -676(%rbp)
	movl	%r14d, %esi
	movl	%ecx, -668(%rbp)
	movq	%rdx, -688(%rbp)
	movl	%ecx, -680(%rbp)
	movq	%rdx, -700(%rbp)
	movl	%ecx, -692(%rbp)
	movq	%rdx, -8104(%rbp)
	movl	%ecx, -8096(%rbp)
	call	_ZN2v88internal9Assembler6pshufdENS0_11XMMRegisterENS0_7OperandEh@PLT
	movq	-10504(%rbp), %r11
.L2513:
	movq	-10472(%rbp), %rcx
	movzbl	4(%r11), %eax
	movzbl	4(%rcx), %edx
	movq	40(%r11,%rax,8), %rax
	movq	40(%rcx,%rdx,8), %rdx
	sarq	$35, %rdx
	testb	$4, %al
	je	.L2515
	testb	$24, %al
	jne	.L2515
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3855
.L2515:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movl	%r13d, %r8d
	movq	-10472(%rbp), %rcx
	movq	%r15, %rdi
	movq	%rdx, -640(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -632(%rbp)
	movq	%rdx, -652(%rbp)
	sarq	$35, %rsi
	movl	%eax, -644(%rbp)
	movq	%rdx, -664(%rbp)
	movl	%eax, -656(%rbp)
	movq	%rdx, -8092(%rbp)
	movl	%eax, -8084(%rbp)
	call	_ZN2v88internal9Assembler6pshufdENS0_11XMMRegisterENS0_7OperandEh@PLT
.L2516:
	movq	-10472(%rbp), %rsi
	movq	-10480(%rbp), %rdx
	movzbl	4(%rsi), %eax
	movq	40(%rdx), %rcx
	movq	64(%rsi,%rax,8), %rax
	movl	%eax, %edx
	movq	%rax, %r8
	andl	$7, %edx
	shrq	$3, %r8
	cmpl	$3, %edx
	je	.L3856
	movq	112(%rcx), %rax
	movl	%r8d, %edx
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2522
	movq	%rdi, %rcx
	jmp	.L2523
.L3857:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2526:
	testq	%rax, %rax
	je	.L2524
.L2523:
	cmpl	32(%rax), %edx
	jle	.L3857
	movq	24(%rax), %rax
	jmp	.L2526
.L2524:
	cmpq	%rcx, %rdi
	je	.L2522
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2522:
	movq	48(%rdi), %rax
.L2521:
	movq	40(%rsi), %rsi
	movzbl	%al, %ecx
	movl	%r14d, %edx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7pblendwENS0_11XMMRegisterES2_h@PLT
	jmp	.L1231
.L2623:
	cmpq	%rcx, %rsi
	je	.L2621
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L2621:
	movq	48(%rsi), %rcx
	movq	%r8, %rdi
	movq	%r11, %rsi
.L2620:
	movl	%ecx, %edx
	movq	40(%r11,%r8,8), %rax
	movl	%ecx, %r9d
	addq	$200, %r15
	andl	$3, %edx
	andl	$3, %r9d
	movl	%edx, %r11d
	leal	0(,%rdx,4), %r13d
	sall	$6, %edx
	movl	%eax, %r8d
	sall	$4, %r11d
	andl	$4, %r8d
	orl	%r11d, %r13d
	orl	%r9d, %r13d
	orl	%edx, %r13d
	movq	40(%rsi,%rdi,8), %rdx
	movzbl	%r13b, %r13d
	sarq	$35, %rdx
	andl	$4, %ecx
	jne	.L2626
	testl	%r8d, %r8d
	je	.L2627
	testb	$24, %al
	jne	.L2627
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3858
.L2627:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	movl	%r13d, %r8d
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler7pshuflwENS0_11XMMRegisterENS0_7OperandEh@PLT
.L2628:
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6pshufdENS0_11XMMRegisterES2_h@PLT
.L2630:
	xorl	%r13d, %r13d
	jmp	.L1231
.L2544:
	cmpq	%rcx, %rdi
	je	.L2542
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2542:
	movq	48(%rdi), %r13
.L2541:
	movq	24(%r15), %rdi
	sarq	$35, %rsi
	movzbl	%r13b, %r13d
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %ecx
	movl	%r13d, %r8d
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%rdx, -604(%rbp)
	movl	%ecx, -596(%rbp)
	movq	%rdx, -616(%rbp)
	movl	%ecx, -608(%rbp)
	movq	%rdx, -628(%rbp)
	movl	%ecx, -620(%rbp)
	movq	%rdx, -8080(%rbp)
	movl	%ecx, -8072(%rbp)
	call	_ZN2v88internal9Assembler7pshuflwENS0_11XMMRegisterENS0_7OperandEh@PLT
.L2537:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	56(%rax,%rdx,8), %rax
	movq	-10480(%rbp), %rdx
	movq	40(%rdx), %rcx
	movl	%eax, %edx
	movq	%rax, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L3859
	movq	112(%rcx), %rax
	movl	%edi, %edx
	leaq	104(%rcx), %rsi
	testq	%rax, %rax
	je	.L2552
	movq	%rsi, %rcx
	jmp	.L2553
.L3860:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2556:
	testq	%rax, %rax
	je	.L2554
.L2553:
	cmpl	32(%rax), %edx
	jle	.L3860
	movq	24(%rax), %rax
	jmp	.L2556
.L2554:
	cmpq	%rcx, %rsi
	je	.L2552
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L2552:
	movq	48(%rsi), %rax
.L2551:
	movzbl	%al, %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7pshufhwENS0_11XMMRegisterES2_h@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L2565:
	cmpq	%rcx, %rdi
	je	.L2563
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2563:
	movq	48(%rdi), %r13
.L2562:
	movq	24(%r15), %rdi
	sarq	$35, %rsi
	movq	%r11, -10504(%rbp)
	movzbl	%r13b, %r13d
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %ecx
	movl	%r13d, %r8d
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movq	%r12, %rdi
	movq	%rdx, -568(%rbp)
	movl	%r14d, %esi
	movl	%ecx, -560(%rbp)
	movq	%rdx, -580(%rbp)
	movl	%ecx, -572(%rbp)
	movq	%rdx, -592(%rbp)
	movl	%ecx, -584(%rbp)
	movq	%rdx, -8068(%rbp)
	movl	%ecx, -8060(%rbp)
	call	_ZN2v88internal9Assembler7pshuflwENS0_11XMMRegisterENS0_7OperandEh@PLT
	movq	-10504(%rbp), %r11
.L2558:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	64(%rax,%rdx,8), %rax
	movq	-10480(%rbp), %rdx
	movq	40(%rdx), %rcx
	movl	%eax, %edx
	movq	%rax, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L3861
	movq	112(%rcx), %rax
	movl	%edi, %edx
	leaq	104(%rcx), %rsi
	testq	%rax, %rax
	je	.L2573
	movq	%rsi, %rcx
	jmp	.L2574
.L3862:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2577:
	testq	%rax, %rax
	je	.L2575
.L2574:
	cmpl	32(%rax), %edx
	jle	.L3862
	movq	24(%rax), %rax
	jmp	.L2577
.L2575:
	cmpq	%rcx, %rsi
	je	.L2573
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L2573:
	movq	48(%rsi), %rax
.L2572:
	movzbl	%al, %ecx
	movl	%r14d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler7pshufhwENS0_11XMMRegisterES2_h@PLT
	movq	-10504(%rbp), %r11
	movzbl	4(%r11), %eax
	movq	40(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L2578
	testb	$24, %al
	jne	.L2578
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3863
.L2578:
	movq	-10472(%rbp), %rcx
	movq	-10480(%rbp), %r9
	movzbl	4(%rcx), %r8d
	movq	40(%r9), %rsi
	movq	56(%rcx,%r8,8), %rax
	movl	%eax, %edx
	movq	%rax, %r10
	andl	$7, %edx
	shrq	$3, %r10
	cmpl	$3, %edx
	je	.L3864
	movq	112(%rsi), %rax
	movl	%r10d, %edx
	leaq	104(%rsi), %rdi
	testq	%rax, %rax
	je	.L2584
	movq	%rdi, %rsi
	jmp	.L2585
.L3865:
	movq	%rax, %rsi
	movq	16(%rax), %rax
.L2588:
	testq	%rax, %rax
	je	.L2586
.L2585:
	cmpl	32(%rax), %edx
	jle	.L3865
	movq	24(%rax), %rax
	jmp	.L2588
.L2586:
	cmpq	%rsi, %rdi
	je	.L2584
	cmpl	%r10d, 32(%rsi)
	cmovle	%rsi, %rdi
.L2584:
	movq	48(%rdi), %r13
.L2583:
	movq	40(%rcx,%r8,8), %rsi
	movq	24(%r9), %rdi
	movzbl	%r13b, %r13d
	sarq	$35, %rsi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %ecx
	movl	%r13d, %r8d
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%rdx, -532(%rbp)
	movl	%ecx, -524(%rbp)
	movq	%rdx, -544(%rbp)
	movl	%ecx, -536(%rbp)
	movq	%rdx, -556(%rbp)
	movl	%ecx, -548(%rbp)
	movq	%rdx, -8056(%rbp)
	movl	%ecx, -8048(%rbp)
	call	_ZN2v88internal9Assembler7pshuflwENS0_11XMMRegisterENS0_7OperandEh@PLT
.L2579:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	64(%rax,%rdx,8), %rax
	movq	-10480(%rbp), %rdx
	movq	40(%rdx), %rcx
	movl	%eax, %edx
	movq	%rax, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L3866
	movq	112(%rcx), %rax
	movl	%edi, %edx
	leaq	104(%rcx), %rsi
	testq	%rax, %rax
	je	.L2594
	movq	%rsi, %rcx
	jmp	.L2595
.L3867:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2598:
	testq	%rax, %rax
	je	.L2596
.L2595:
	cmpl	32(%rax), %edx
	jle	.L3867
	movq	24(%rax), %rax
	jmp	.L2598
.L2596:
	cmpq	%rcx, %rsi
	je	.L2594
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L2594:
	movq	48(%rsi), %rax
.L2593:
	movzbl	%al, %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7pshufhwENS0_11XMMRegisterES2_h@PLT
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	72(%rax,%rdx,8), %rax
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L3868
	movq	-10480(%rbp), %rdx
	shrq	$3, %rax
	movq	%rax, %rsi
	movq	40(%rdx), %rcx
	movl	%eax, %edx
	movq	112(%rcx), %rax
	leaq	104(%rcx), %rdi
	testq	%rax, %rax
	je	.L2603
	movq	%rdi, %rcx
	jmp	.L2604
.L3869:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2607:
	testq	%rax, %rax
	je	.L2605
.L2604:
	cmpl	32(%rax), %edx
	jle	.L3869
	movq	24(%rax), %rax
	jmp	.L2607
.L2605:
	cmpq	%rcx, %rdi
	je	.L2603
	cmpl	%esi, 32(%rcx)
	cmovle	%rcx, %rdi
.L2603:
	movq	48(%rdi), %rax
.L2602:
	movzbl	%al, %ecx
	movl	%r14d, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7pblendwENS0_11XMMRegisterES2_h@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L2499:
	cmpq	%rcx, %rdi
	je	.L2497
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2497:
	movq	48(%rdi), %rbx
.L2496:
	movq	24(%r15), %rdi
	sarq	$35, %rsi
	movzbl	%bl, %ebx
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movl	%ebx, %r8d
	movq	-10472(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rdx, -712(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -704(%rbp)
	movq	%rdx, -724(%rbp)
	sarq	$35, %rsi
	movl	%eax, -716(%rbp)
	movq	%rdx, -736(%rbp)
	movl	%eax, -728(%rbp)
	movq	%rdx, -8116(%rbp)
	movl	%eax, -8108(%rbp)
	call	_ZN2v88internal9Assembler6pshufdENS0_11XMMRegisterENS0_7OperandEh@PLT
	jmp	.L1231
.L2298:
	cmpq	%rcx, %rdi
	je	.L2296
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2296:
	movq	48(%rdi), %rbx
.L2295:
	movq	24(%r15), %rdi
	sarq	$35, %rsi
	movsbl	%bl, %ebx
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movl	%ebx, %r8d
	movq	-10472(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rdx, -1324(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -1316(%rbp)
	movq	%rdx, -1336(%rbp)
	sarq	$35, %rsi
	movl	%eax, -1328(%rbp)
	movq	%rdx, -1348(%rbp)
	movl	%eax, -1340(%rbp)
	movq	%rdx, -8320(%rbp)
	movl	%eax, -8312(%rbp)
	call	_ZN2v88internal9Assembler6pinsrqENS0_11XMMRegisterENS0_7OperandEa@PLT
	jmp	.L1231
.L2436:
	cmpq	%rcx, %rdi
	je	.L2434
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2434:
	movq	48(%rdi), %rbx
.L2433:
	movq	24(%r15), %rdi
	sarq	$35, %rsi
	movsbl	%bl, %ebx
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movl	%ebx, %r8d
	movq	-10472(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rdx, -820(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -812(%rbp)
	movq	%rdx, -832(%rbp)
	sarq	$35, %rsi
	movl	%eax, -824(%rbp)
	movq	%rdx, -844(%rbp)
	movl	%eax, -836(%rbp)
	movq	%rdx, -8152(%rbp)
	movl	%eax, -8144(%rbp)
	call	_ZN2v88internal9Assembler6pinsrbENS0_11XMMRegisterENS0_7OperandEa@PLT
	jmp	.L1231
.L2412:
	cmpq	%rcx, %rdi
	je	.L2410
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2410:
	movq	48(%rdi), %rbx
.L2409:
	movq	24(%r15), %rdi
	sarq	$35, %rsi
	movsbl	%bl, %ebx
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movl	%ebx, %r8d
	movq	-10472(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rdx, -892(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -884(%rbp)
	movq	%rdx, -904(%rbp)
	sarq	$35, %rsi
	movl	%eax, -896(%rbp)
	movq	%rdx, -916(%rbp)
	movl	%eax, -908(%rbp)
	movq	%rdx, -8176(%rbp)
	movl	%eax, -8168(%rbp)
	call	_ZN2v88internal9Assembler6pinsrwENS0_11XMMRegisterENS0_7OperandEa@PLT
	jmp	.L1231
.L2388:
	cmpq	%rcx, %rdi
	je	.L2386
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2386:
	movq	48(%rdi), %rbx
.L2385:
	movq	24(%r15), %rdi
	sarq	$35, %rsi
	movsbl	%bl, %ebx
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movl	%ebx, %r8d
	movq	-10472(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rdx, -964(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -956(%rbp)
	movq	%rdx, -976(%rbp)
	sarq	$35, %rsi
	movl	%eax, -968(%rbp)
	movq	%rdx, -988(%rbp)
	movl	%eax, -980(%rbp)
	movq	%rdx, -8200(%rbp)
	movl	%eax, -8192(%rbp)
	call	_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_7OperandEa@PLT
	jmp	.L1231
.L2363:
	cmpq	%rcx, %rdi
	je	.L2361
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdi
.L2361:
	movq	48(%rdi), %rbx
.L2360:
	movq	24(%r15), %rdi
	sarq	$35, %rsi
	movsbl	%bl, %ebx
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movl	%ebx, %r8d
	movq	-10472(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rdx, -1036(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -1028(%rbp)
	movq	%rdx, -1048(%rbp)
	sarq	$35, %rsi
	movl	%eax, -1040(%rbp)
	movq	%rdx, -1060(%rbp)
	movl	%eax, -1052(%rbp)
	movq	%rdx, -8224(%rbp)
	movl	%eax, -8216(%rbp)
	call	_ZN2v88internal9Assembler6pinsrqENS0_11XMMRegisterENS0_7OperandEa@PLT
	jmp	.L1231
.L2185:
	leaq	.LC7(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1868:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandEii@PLT
	jmp	.L1231
.L2182:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -1576(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -1568(%rbp)
	movq	%rdx, -1588(%rbp)
	sarq	$35, %rsi
	movl	%eax, -1580(%rbp)
	movq	%rdx, -1600(%rbp)
	movl	%eax, -1592(%rbp)
	movq	%rdx, -8632(%rbp)
	movl	%eax, -8624(%rbp)
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L3643
.L1864:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$5, %ecx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandEii@PLT
	jmp	.L1231
.L1856:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$4, %ecx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandEii@PLT
	jmp	.L1231
.L1814:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movl	-88(%rbp), %ecx
	movl	$4, %r8d
	movl	%eax, -4412(%rbp)
	movl	%eax, -4424(%rbp)
	movl	%eax, -4436(%rbp)
	movl	%eax, -10052(%rbp)
	movq	-10472(%rbp), %rax
	movq	%rdx, -4420(%rbp)
	movq	40(%rax), %rsi
	movq	%rdx, -4432(%rbp)
	movq	%rdx, -4444(%rbp)
	sarq	$35, %rsi
	movq	%rdx, -10060(%rbp)
	call	_ZN2v88internal9Assembler9emit_imulENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L2151:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -1720(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -1712(%rbp)
	movq	%rdx, -1732(%rbp)
	sarq	$35, %rsi
	movl	%eax, -1724(%rbp)
	movq	%rdx, -1744(%rbp)
	movl	%eax, -1736(%rbp)
	movq	%rdx, -8752(%rbp)
	movl	%eax, -8744(%rbp)
	call	_ZN2v88internal9Assembler7movsxwlENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L2125
.L1872:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandEii@PLT
	jmp	.L1231
.L1852:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	$4, %ecx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandEii@PLT
	jmp	.L1231
.L2156:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movl	-88(%rbp), %ecx
	movl	$4, %r8d
	movl	%eax, -1676(%rbp)
	movl	%eax, -1688(%rbp)
	movl	%eax, -1700(%rbp)
	movl	%eax, -8720(%rbp)
	movq	-10472(%rbp), %rax
	movq	%rdx, -1684(%rbp)
	movq	40(%rax), %rsi
	movq	%rdx, -1696(%rbp)
	movq	%rdx, -1708(%rbp)
	sarq	$35, %rsi
	movq	%rdx, -8728(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L2125
.L2142:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movl	-88(%rbp), %ecx
	movl	$8, %r8d
	movl	%eax, -1748(%rbp)
	movl	%eax, -1760(%rbp)
	movl	%eax, -1772(%rbp)
	movl	%eax, -8768(%rbp)
	movq	-10472(%rbp), %rax
	movq	%rdx, -1756(%rbp)
	movq	40(%rax), %rsi
	movq	%rdx, -1768(%rbp)
	movq	%rdx, -1780(%rbp)
	sarq	$35, %rsi
	movq	%rdx, -8776(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L2125
.L2131:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movl	-88(%rbp), %ecx
	movl	$4, %r8d
	movl	%eax, -1820(%rbp)
	movl	%eax, -1832(%rbp)
	movl	%eax, -1844(%rbp)
	movl	%eax, -8816(%rbp)
	movq	-10472(%rbp), %rax
	movq	%rdx, -1828(%rbp)
	movq	40(%rax), %rsi
	movq	%rdx, -1840(%rbp)
	movq	%rdx, -1852(%rbp)
	sarq	$35, %rsi
	movq	%rdx, -8824(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L2125
.L2126:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -1864(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -1856(%rbp)
	movq	%rdx, -1876(%rbp)
	sarq	$35, %rsi
	movl	%eax, -1868(%rbp)
	movq	%rdx, -1888(%rbp)
	movl	%eax, -1880(%rbp)
	movq	%rdx, -8848(%rbp)
	movl	%eax, -8840(%rbp)
	call	_ZN2v88internal9Assembler7movsxblENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L2125
.L1880:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$1, %ecx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandEii@PLT
	jmp	.L1231
.L1860:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	$5, %ecx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandEii@PLT
	jmp	.L1231
.L2165:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movl	-88(%rbp), %ecx
	movl	$8, %r8d
	movl	%eax, -1604(%rbp)
	movl	%eax, -1616(%rbp)
	movl	%eax, -1628(%rbp)
	movl	%eax, -8672(%rbp)
	movq	-10472(%rbp), %rax
	movq	%rdx, -1612(%rbp)
	movq	40(%rax), %rsi
	movq	%rdx, -1624(%rbp)
	movq	%rdx, -1636(%rbp)
	sarq	$35, %rsi
	movq	%rdx, -8680(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L2125
.L2160:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r15, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -1648(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -1640(%rbp)
	movq	%rdx, -1660(%rbp)
	sarq	$35, %rsi
	movl	%eax, -1652(%rbp)
	movq	%rdx, -1672(%rbp)
	movl	%eax, -1664(%rbp)
	movq	%rdx, -8704(%rbp)
	movl	%eax, -8696(%rbp)
	call	_ZN2v88internal9Assembler7movsxwqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1818:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movl	-88(%rbp), %ecx
	movl	$8, %r8d
	movl	%eax, -4376(%rbp)
	movl	%eax, -4388(%rbp)
	movl	%eax, -4400(%rbp)
	movl	%eax, -10028(%rbp)
	movq	-10472(%rbp), %rax
	movq	%rdx, -4384(%rbp)
	movq	40(%rax), %rsi
	movq	%rdx, -4396(%rbp)
	movq	%rdx, -4408(%rbp)
	sarq	$35, %rsi
	movq	%rdx, -10036(%rbp)
	call	_ZN2v88internal9Assembler9emit_imulENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1876:
	movq	24(%r15), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	xorl	%r13d, %r13d
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	$1, %ecx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandEii@PLT
	jmp	.L1231
.L2137:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -1792(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -1784(%rbp)
	movq	%rdx, -1804(%rbp)
	sarq	$35, %rsi
	movl	%eax, -1796(%rbp)
	movq	%rdx, -1816(%rbp)
	movl	%eax, -1808(%rbp)
	movq	%rdx, -8800(%rbp)
	movl	%eax, -8792(%rbp)
	call	_ZN2v88internal9Assembler7movsxbqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L3643
.L3695:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	jne	.L2458
	movq	-10512(%rbp), %rdi
	movq	%rdx, -10544(%rbp)
	movq	%r11, -10536(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rsi
	movq	-10536(%rbp), %r11
	movq	-10544(%rbp), %rdx
	jmp	.L2460
.L3839:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	jne	.L2477
	movq	%r10, %rdi
	movq	%rdx, -10536(%rbp)
	movq	%r10, -10512(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rsi
	movq	-10512(%rbp), %r10
	movq	-10536(%rbp), %rdx
	jmp	.L2479
.L2195:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -1504(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -1496(%rbp)
	movq	%rdx, -1516(%rbp)
	sarq	$35, %rsi
	movl	%eax, -1508(%rbp)
	movq	%rdx, -1528(%rbp)
	movl	%eax, -1520(%rbp)
	movq	%rdx, -8548(%rbp)
	movl	%eax, -8540(%rbp)
	call	_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L2198
.L2199:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r8d
	movq	%rdx, -1468(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -1460(%rbp)
	movq	%rdx, -1480(%rbp)
	sarq	$35, %rsi
	movl	%eax, -1472(%rbp)
	movq	%rdx, -1492(%rbp)
	movl	%eax, -1484(%rbp)
	movq	%rdx, -8524(%rbp)
	movl	%eax, -8516(%rbp)
	call	_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterENS0_7OperandES2_@PLT
	jmp	.L2198
.L2191:
	movq	24(%r15), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-10472(%rbp), %rcx
	movq	%rdx, -1540(%rbp)
	movq	40(%rcx), %rsi
	movl	%eax, %ecx
	movl	%eax, -1532(%rbp)
	movq	%rdx, -1552(%rbp)
	sarq	$35, %rsi
	movl	%eax, -1544(%rbp)
	movq	%rdx, -1564(%rbp)
	movl	%eax, -1556(%rbp)
	movq	%rdx, -8572(%rbp)
	movl	%eax, -8564(%rbp)
	call	_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L2198
.L2122:
	movl	$15, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5subsdENS0_11XMMRegisterES2_@PLT
	jmp	.L1716
.L2120:
	movl	$15, %edx
	movl	$15, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5xorpdENS0_11XMMRegisterES2_@PLT
	jmp	.L2121
.L1723:
	movq	40(%rsi), %rsi
	movl	_ZN2v88internalL3rbpE(%rip), %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$8, %ecx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	jmp	.L1231
.L2224:
	call	_ZN2v88internal9Assembler4movdENS0_8RegisterENS0_11XMMRegisterE@PLT
	jmp	.L2225
.L2077:
	call	_ZN2v88internal9Assembler4movdENS0_8RegisterENS0_11XMMRegisterE@PLT
	jmp	.L2078
.L2227:
	call	_ZN2v88internal9Assembler4movqENS0_8RegisterENS0_11XMMRegisterE@PLT
	jmp	.L2228
.L2207:
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	%edx, -128(%rbp)
	movq	%rdx, %r9
	movq	%rax, %r8
	movq	-10472(%rbp), %rdx
	movq	%rax, -136(%rbp)
	movzbl	4(%rdx), %eax
	addq	-10464(%rbp), %rax
	movq	40(%rdx,%rax,8), %rdx
	movq	%r8, -124(%rbp)
	movl	%r9d, -116(%rbp)
	movq	%r8, -112(%rbp)
	sarq	$35, %rdx
	movl	%r9d, -104(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2210
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%r9d, -88(%rbp)
	movl	$17, %esi
	movl	%r9d, %r9d
	movq	%r15, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1231
.L2202:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	%r11, -10504(%rbp)
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %r11
	movq	%rax, -112(%rbp)
	movq	%rax, %rsi
	movq	-10464(%rbp), %rax
	movzbl	4(%r11), %ecx
	movl	%edx, -104(%rbp)
	addq	$5, %rax
	addq	%rax, %rcx
	movq	(%r11,%rcx,8), %rcx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3870
	movq	-10472(%rbp), %rdi
	movl	$8, %r8d
	movzbl	4(%rdi), %ecx
	addq	%rcx, %rax
	movq	(%rdi,%rax,8), %rcx
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	sarq	$35, %rcx
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L2203
.L1989:
	movl	$10, %esi
	movq	%r14, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler8movmskpdENS0_8RegisterENS0_11XMMRegisterE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1990
.L2218:
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10472(%rbp), %rcx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -124(%rbp)
	movl	%edx, -116(%rbp)
	movzbl	4(%rcx), %eax
	addq	-10464(%rbp), %rax
	movq	40(%rcx,%rax,8), %rcx
	movq	%rsi, -112(%rbp)
	movl	%edx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	sarq	$35, %rcx
	movl	%edx, -88(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2221
	call	_ZN2v88internal9Assembler7vmovdquENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L1231
.L2019:
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5andpdENS0_11XMMRegisterES2_@PLT
	jmp	.L1716
.L2099:
	movl	%esi, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	jmp	.L1716
.L2022:
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5xorpdENS0_11XMMRegisterES2_@PLT
	jmp	.L1716
.L2020:
	movl	$118, %r9d
	movl	$15, %r8d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L2021
.L2645:
	call	_ZN2v88internal9Assembler7pshufhwENS0_11XMMRegisterES2_h@PLT
	movl	$170, %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler6pshufdENS0_11XMMRegisterES2_h@PLT
	jmp	.L1231
.L2626:
	testl	%r8d, %r8d
	je	.L2631
	testb	$24, %al
	jne	.L2631
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3871
.L2631:
	movq	-10480(%rbp), %rax
	movl	%edx, %esi
	movq	24(%rax), %rdi
	call	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi@PLT
	leaq	_ZN2v88internalL3rbpE(%rip), %rcx
	leaq	-96(%rbp), %rdi
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	leaq	_ZN2v88internalL3rspE(%rip), %rax
	cmove	%rcx, %rax
	movl	(%rax), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	movl	%r13d, %r8d
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler7pshufhwENS0_11XMMRegisterENS0_7OperandEh@PLT
.L2632:
	movl	$170, %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6pshufdENS0_11XMMRegisterES2_h@PLT
	jmp	.L2630
.L2368:
	movq	8(%rsi,%rcx), %r14
	movq	16(%rsi,%rcx), %r15
	movl	%eax, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	sarq	$35, %r14
	sarq	$35, %r15
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4movqENS0_8RegisterENS0_11XMMRegisterE@PLT
	movl	%ebx, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4movqENS0_8RegisterENS0_11XMMRegisterE@PLT
	movl	%r15d, %ecx
	movl	%r14d, %edx
	movl	$59, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	%r14d, %ecx
	movl	%r15d, %edx
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5cmovqENS0_9ConditionENS0_8RegisterES3_@PLT
	movl	$1, %ecx
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6pextrqENS0_8RegisterENS0_11XMMRegisterEa@PLT
	movl	%r13d, %esi
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	$1, %ecx
	movl	%ebx, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6pextrqENS0_8RegisterENS0_11XMMRegisterEa@PLT
	movl	%r15d, %ecx
	movl	%r14d, %edx
	movl	$59, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	%r14d, %ecx
	movl	%r15d, %edx
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5cmovqENS0_9ConditionENS0_8RegisterES3_@PLT
	movl	%r15d, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movl	$108, %r9d
	movl	%ebx, %edx
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L1909:
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5andpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1716
.L1907:
	movl	$118, %r9d
	movl	$15, %r8d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1908
.L1954:
	movl	$10, %esi
	movq	%r14, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler8movmskpsENS0_8RegisterENS0_11XMMRegisterE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1955
.L1912:
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1716
.L1910:
	movl	$118, %r9d
	movl	$15, %r8d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1911
.L1917:
	movl	%ebx, %ecx
	call	_ZN2v88internal9Assembler7roundssENS0_11XMMRegisterES2_NS0_12RoundingModeE@PLT
	jmp	.L1716
.L2028:
	movl	%ebx, %ecx
	call	_ZN2v88internal9Assembler7roundsdENS0_11XMMRegisterES2_NS0_12RoundingModeE@PLT
	jmp	.L1716
.L1944:
	movl	-116(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1945
.L1941:
	movl	_ZN2v88internalL3raxE(%rip), %r9d
	movl	$4, %r8d
	movl	$5, %ecx
	movq	%r14, %rdi
	movabsq	$81604378624, %rax
	movq	%rax, %rdx
	movl	%r9d, %esi
	orq	$8, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	xorl	%edx, %edx
	movl	$4, %r8d
	movq	%r14, %rdi
	movabsq	$81604378624, %rax
	movl	$4, %esi
	orb	$-1, %al
	movq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r9d
	movq	%r14, %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L1943
	cmpb	$0, 112(%r15)
	je	.L3872
.L1943:
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5popfqEv@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L1942
	cmpb	$0, 112(%r15)
	jne	.L1942
	movq	232(%r15), %rsi
	movl	$-8, %edx
	subq	216(%r15), %rsi
	leaq	48(%r15), %rdi
	call	_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0
	jmp	.L1942
.L1939:
	movl	%r12d, %ecx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L1940
.L1937:
	movl	%r13d, %ecx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L1938
.L1724:
	movzbl	4(%rsi), %eax
	movl	$8, %r8d
	movl	$4, %edx
	movq	%r15, %rdi
	movq	40(%rsi,%rax,8), %rcx
	movl	$59, %esi
	sarq	$35, %rcx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
.L2017:
	movl	$118, %r9d
	movl	$15, %r8d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L2018
.L1994:
	movl	-116(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1992
.L1980:
	movl	-104(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1978
.L1924:
	movl	-104(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7ucomisdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1925
.L2015:
	movl	-116(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2013
.L1959:
	movl	-104(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1957
.L1950:
	movl	-88(%rbp), %ecx
	movq	%r14, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler7ucomissENS0_11XMMRegisterENS0_7OperandE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1948
.L1896:
	movl	-88(%rbp), %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7ucomissENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1897
.L1999:
	movl	-104(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r14, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler7ucomisdENS0_11XMMRegisterENS0_7OperandE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1997
.L2010:
	movl	$15, %edx
	movl	$10, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler8movmskpdENS0_8RegisterENS0_11XMMRegisterE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L2006
.L2008:
	movl	-116(%rbp), %ecx
	movq	%r8, %rdx
	movl	$15, %esi
	movq	%r14, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L2009
.L1688:
	call	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	jmp	.L1689
.L1975:
	movl	$15, %edx
	movl	$10, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler8movmskpsENS0_8RegisterENS0_11XMMRegisterE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1971
.L1973:
	movl	-104(%rbp), %ecx
	movq	%r8, %rdx
	movl	$15, %esi
	movq	%r14, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1974
.L2086:
	movl	-88(%rbp), %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4movdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2087
.L1985:
	movl	-104(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r14, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler7ucomisdENS0_11XMMRegisterENS0_7OperandE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1983
.L2231:
	movl	-104(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2232
.L2026:
	movl	-116(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6sqrtsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2027
.L2235:
	movl	-116(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2236
.L1964:
	movl	-88(%rbp), %ecx
	movq	%r14, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler7ucomissENS0_11XMMRegisterENS0_7OperandE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1962
.L1811:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -10084(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -10076(%rbp)
	movzbl	4(%rax), %ecx
	movq	40(%rax,%rcx,8), %rcx
	movq	%rsi, -112(%rbp)
	movl	%edx, -104(%rbp)
	sarq	$35, %rcx
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L1231
.L1793:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -10156(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -10148(%rbp)
	movzbl	4(%rax), %ecx
	movq	40(%rax,%rcx,8), %rcx
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	sarq	$35, %rcx
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_8RegisterE@PLT
	jmp	.L1231
.L1839:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$8, %r9d
	movl	$11, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -9916(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -9908(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1835:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$4, %r9d
	movl	$11, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -9940(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -9932(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1847:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$8, %r9d
	movl	$51, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -9868(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -9860(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1843:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$4, %r9d
	movl	$51, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -9892(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -9884(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1755:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$8, %r9d
	movl	$43, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -10324(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -10316(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1751:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$4, %r9d
	movl	$43, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -10348(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -10340(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1805:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$4, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -10108(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -10100(%rbp)
	movzbl	4(%rax), %ecx
	movq	40(%rax,%rcx,8), %rcx
	movq	%rsi, -112(%rbp)
	movl	%edx, -104(%rbp)
	sarq	$35, %rcx
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L1231
.L1799:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -10132(%rbp)
	movq	-10472(%rbp), %rax
	movl	%edx, -10124(%rbp)
	movzbl	4(%rax), %ecx
	movq	40(%rax,%rcx,8), %rcx
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	sarq	$35, %rcx
	call	_ZN2v88internal9Assembler5testwENS0_7OperandENS0_8RegisterE@PLT
	jmp	.L1231
.L2175:
	leaq	-10480(%rbp), %r14
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movq	%rax, %r9
	movl	%edx, -8648(%rbp)
	movq	%rdx, %rax
	movq	%r9, -8656(%rbp)
.L3635:
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movl	$4, %r8d
	movq	%r12, %rdi
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -96(%rbp)
	movl	%eax, -88(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L2174
.L1769:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$58, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -10252(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -10244(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler15arithmetic_op_8EhNS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1759:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$4, %r9d
	movl	$35, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -10300(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -10292(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1685:
	call	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	jmp	.L1684
.L1673:
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1672
.L1775:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$59, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -10228(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -10220(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler16arithmetic_op_16EhNS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1231
.L1747:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$8, %r9d
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -10372(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -10364(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1781:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$4, %r9d
	movl	$59, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -10204(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -10196(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1787:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$8, %r9d
	movl	$59, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -10180(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -10172(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1763:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$8, %r9d
	movl	$35, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -10276(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -10268(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L1743:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$4, %r9d
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -10396(%rbp)
	movq	%rdx, %r8
	movq	-10472(%rbp), %rax
	movl	%edx, -10388(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3666:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r9d
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -124(%rbp)
	movq	%rax, %rcx
	movq	-10472(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -116(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3704:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r9d
	movl	$11, %esi
	movq	%r12, %rdi
	movq	%rax, -124(%rbp)
	movq	%rax, %rcx
	movq	-10472(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -116(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3748:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r13, %rdi
	movq	%rax, %r8
	movl	%edx, -5924(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8636(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r8, -5932(%rbp)
	movq	40(%rdx), %rsi
	movq	%r8, %rdx
	movq	%r8, -8644(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L3643
.L3727:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r9d
	movl	$43, %esi
	movq	%r12, %rdi
	movq	%rax, -124(%rbp)
	movq	%rax, %rcx
	movq	-10472(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -116(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3719:
	leaq	-10480(%rbp), %r13
	movq	%rsi, -10504(%rbp)
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %r11
	movq	%rax, -112(%rbp)
	movq	%rax, %rsi
	movq	-10464(%rbp), %rax
	movzbl	4(%r11), %ecx
	movl	%edx, -104(%rbp)
	addq	$5, %rax
	addq	%rax, %rcx
	movq	(%r11,%rcx,8), %rcx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3873
	movq	-10472(%rbp), %rdi
	movl	$4, %r8d
	movzbl	4(%rdi), %ecx
	addq	%rcx, %rax
	movq	(%rdi,%rax,8), %rcx
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	sarq	$35, %rcx
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_8RegisterEi@PLT
.L1802:
	xorl	%r13d, %r13d
	jmp	.L1231
.L3785:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movl	$102, %ecx
	movl	$118, %r9d
	movq	%r15, %rdi
	movl	$15, %r8d
	movl	%r14d, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	%r14d, %edx
	pushq	%rcx
	movl	$56, %r9d
	movl	$15, %r8d
	xorl	%r13d, %r13d
	pushq	$8
	movl	$102, %ecx
	call	_ZN2v88internal9Assembler11ssse3_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%rsi
	popq	%rdi
	jmp	.L1231
.L3723:
	testl	%ecx, %ecx
	je	.L1855
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1855
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1855
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movl	$4, %ecx
	movq	%r12, %rdi
	andl	$63, %edx
	movl	$8, %r8d
	movq	40(%rax), %rsi
	movl	$19, %eax
	salq	$32, %rax
	sarq	$35, %rsi
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L1231
.L3711:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1813
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1813
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1813
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rcx
	movl	$4, %r8d
	movq	%r12, %rdi
	movzbl	4(%rcx), %edx
	movq	40(%rcx), %rsi
	movq	40(%rcx,%rdx,8), %rdx
	sarq	$35, %rsi
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler9emit_imulENS0_8RegisterES2_NS0_9ImmediateEi@PLT
	jmp	.L1231
.L3729:
	leaq	-10480(%rbp), %r13
	movq	%rsi, -10504(%rbp)
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %r11
	movq	%rax, -112(%rbp)
	movq	%rax, %rsi
	movq	-10464(%rbp), %rax
	movzbl	4(%r11), %ecx
	movl	%edx, -104(%rbp)
	addq	$5, %rax
	addq	%rax, %rcx
	movq	(%r11,%rcx,8), %rcx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3874
	movq	-10472(%rbp), %rdi
	movl	$8, %r8d
	movzbl	4(%rdi), %ecx
	addq	%rcx, %rax
	movq	(%rdi,%rax,8), %rcx
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	sarq	$35, %rcx
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_8RegisterEi@PLT
.L1808:
	xorl	%r13d, %r13d
	jmp	.L1231
.L3710:
	testl	%ecx, %ecx
	je	.L1871
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1871
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1871
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movl	$7, %ecx
	movq	%r12, %rdi
	andl	$63, %edx
	movl	$8, %r8d
	movq	40(%rax), %rsi
	movl	$19, %eax
	salq	$32, %rax
	sarq	$35, %rsi
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L1231
.L1665:
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1664
.L3701:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r9d
	movl	$51, %esi
	movq	%r12, %rdi
	movq	%rax, -124(%rbp)
	movq	%rax, %rcx
	movq	-10472(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -116(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3693:
	pxor	%xmm0, %xmm0
	movl	$4, %r14d
	leaq	-96(%rbp), %r13
	movaps	%xmm0, -96(%rbp)
	leaq	-10464(%rbp), %r15
.L2452:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	leaq	5(%r14,%rdx), %rdx
	movq	(%rax,%rdx,8), %rax
	movq	-10480(%rbp), %rdx
	movq	40(%rdx), %rcx
	movl	%eax, %edx
	movq	%rax, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L3875
	movq	112(%rcx), %rax
	movl	%edi, %edx
	leaq	104(%rcx), %rsi
	testq	%rax, %rax
	je	.L2447
	movq	%rsi, %rcx
	jmp	.L2448
	.p2align 4,,10
	.p2align 3
.L3876:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2451:
	testq	%rax, %rax
	je	.L2449
.L2448:
	cmpl	32(%rax), %edx
	jle	.L3876
	movq	24(%rax), %rax
	jmp	.L2451
.L2449:
	cmpq	%rcx, %rsi
	je	.L2447
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rsi
.L2447:
	movq	48(%rsi), %rax
.L2446:
	movl	%eax, -4(%r13,%r14,4)
	subq	$1, %r14
	jne	.L2452
	movq	-88(%rbp), %rdx
	movl	$8, %r8d
	movl	$19, %ecx
	movq	%r12, %rdi
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movq	-96(%rbp), %rdx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	$19, %ecx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	leaq	-124(%rbp), %rdi
	xorl	%edx, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	pushq	$0
	movq	-124(%rbp), %rdx
	movq	%r12, %rdi
	pushq	$56
	movl	-116(%rbp), %ecx
	movl	$15, %r9d
	movl	$102, %r8d
	movl	-10504(%rbp), %esi
	movq	%rdx, -112(%rbp)
	movl	%ecx, -104(%rbp)
	call	_ZN2v88internal9Assembler11ssse3_instrENS0_11XMMRegisterENS0_7OperandEhhhh@PLT
	popq	%r9
	popq	%r10
	jmp	.L2453
.L3822:
	movq	48(%rsi), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%xmm2, -10504(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movq	-10504(%rbp), %r11
	movq	-10472(%rbp), %rcx
	movl	4(%r11), %eax
	movl	4(%rcx), %edx
	jmp	.L2050
.L3823:
	movq	-10472(%rbp), %rax
	movl	$1, %edx
	movq	%r15, %rdi
	movq	48(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	jmp	.L2054
.L3820:
	movq	48(%rsi), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%xmm2, -10504(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movq	-10504(%rbp), %r11
	movq	-10472(%rbp), %rcx
	movl	4(%r11), %eax
	movl	4(%rcx), %edx
	jmp	.L2055
.L3806:
	movq	%rsi, -10504(%rbp)
	movl	$31, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rcx
	movl	-88(%rbp), %r8d
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$6, %edx
	movl	$59, %esi
	movq	%rcx, -6556(%rbp)
	movl	%r8d, -6548(%rbp)
	movq	%rcx, -10444(%rbp)
	movl	%r8d, -10436(%rbp)
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	movl	$55, %edx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler6AssertENS0_9ConditionENS0_11AbortReasonE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1690
.L3712:
	leaq	-10480(%rbp), %r13
	movq	%rsi, -10504(%rbp)
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %r11
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movq	-10464(%rbp), %rax
	movzbl	4(%r11), %ecx
	movl	%edx, -88(%rbp)
	addq	$5, %rax
	addq	%rax, %rcx
	movq	(%r11,%rcx,8), %rcx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3877
	movq	-10472(%rbp), %rdi
	movzbl	4(%rdi), %ecx
	addq	%rcx, %rax
	movq	(%rdi,%rax,8), %rcx
	movq	%r12, %rdi
	sarq	$35, %rcx
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_8RegisterE@PLT
.L1790:
	xorl	%r13d, %r13d
	jmp	.L1231
.L3752:
	movq	-10472(%rbp), %rcx
	movq	%r14, %rdi
	movzbl	4(%rcx), %edx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movl	$19, %eax
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movswl	%dx, %ecx
	salq	$32, %rax
	movl	-88(%rbp), %edx
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler4movwENS0_7OperandENS0_9ImmediateE@PLT
	jmp	.L2169
.L3716:
	testl	%ecx, %ecx
	je	.L1879
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1879
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1879
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movl	$1, %ecx
	movq	%r12, %rdi
	andl	$63, %edx
	movl	$8, %r8d
	movq	40(%rax), %rsi
	movl	$19, %eax
	salq	$32, %rax
	sarq	$35, %rsi
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L1231
.L3853:
	movq	160(%r15), %rax
	movslq	%ebx, %rbx
	movl	$1, %edx
	leaq	200(%r15), %rdi
	leaq	(%rax,%rbx,8), %rsi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	jmp	.L1705
.L3655:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movl	$118, %r9d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	$15, %r8d
	movl	%r14d, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	pushq	%r12
	movl	%r14d, %edx
	movl	%r13d, %esi
	pushq	$10
	movl	$56, %r9d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler11ssse3_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%r13
	xorl	%r13d, %r13d
	popq	%r14
	jmp	.L1231
.L3803:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	movl	$118, %r9d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	$15, %r8d
	movl	%r13d, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %esi
	movl	$31, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5pslldENS0_11XMMRegisterEh@PLT
	movq	-10472(%rbp), %rax
	movl	%r13d, %edx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L3652:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movl	$118, %r9d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	$15, %r8d
	movl	%r14d, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %esi
	movl	$102, %ecx
	movl	%r14d, %edx
	pushq	%r11
	movl	$56, %r9d
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	pushq	$9
	movl	$15, %r8d
	call	_ZN2v88internal9Assembler11ssse3_instrENS0_11XMMRegisterES2_hhhh@PLT
	popq	%rbx
	popq	%r12
	jmp	.L1231
.L3709:
	testl	%ecx, %ecx
	je	.L1867
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1867
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1867
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movl	$7, %ecx
	movq	%r12, %rdi
	andl	$31, %edx
	movl	$4, %r8d
	movq	40(%rax), %rsi
	movl	$19, %eax
	salq	$32, %rax
	sarq	$35, %rsi
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L1231
.L3764:
	leaq	-10480(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler27InstructionOperandConverter9InputCodeEm
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1664
.L3831:
	movzbl	%al, %eax
	movq	48(%rsi,%rax,8), %rax
	testb	$4, %al
	je	.L2668
	testb	$24, %al
	jne	.L2668
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$14, %dl
	je	.L3878
.L2668:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movq	%r15, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterENS0_7OperandE@PLT
.L2669:
	movl	$8, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	$15, %r13d
	call	_ZN2v88internal9Assembler5psrlwENS0_11XMMRegisterEh@PLT
	jmp	.L2667
.L3832:
	movzbl	%al, %eax
	movq	48(%r11,%rax,8), %rax
	testb	$4, %al
	je	.L2666
	testb	$24, %al
	jne	.L2666
	shrq	$5, %rax
	cmpb	$14, %al
	je	.L3879
.L2666:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	movl	$15, %r13d
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$85, %r8d
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal9Assembler7pblendwENS0_11XMMRegisterENS0_7OperandEh@PLT
	jmp	.L2665
.L3668:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r9d
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -124(%rbp)
	movq	%rax, %rcx
	movq	-10472(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -116(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3670:
	leaq	-10480(%rbp), %r13
	movq	%rsi, -10504(%rbp)
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %r11
	movq	%rax, -124(%rbp)
	movq	%rax, %rcx
	movq	-10464(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -116(%rbp)
	movzbl	4(%r11), %edx
	addq	$5, %rax
	addq	%rax, %rdx
	movq	(%r11,%rdx,8), %rdx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L3880
	movq	-10472(%rbp), %rsi
	movl	$8, %r9d
	movq	%r12, %rdi
	movzbl	4(%rsi), %edx
	addq	%rdx, %rax
	movq	(%rsi,%rax,8), %rdx
	movl	$57, %esi
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
.L1784:
	xorl	%r13d, %r13d
	jmp	.L1231
.L3656:
	leaq	-10480(%rbp), %r13
	movq	%rsi, -10504(%rbp)
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %r11
	movq	%rax, -112(%rbp)
	movq	%rax, %rcx
	movq	-10464(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -104(%rbp)
	movzbl	4(%r11), %edx
	addq	$5, %rax
	addq	%rax, %rdx
	movq	(%r11,%rdx,8), %rdx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L3881
	movq	-10472(%rbp), %rsi
	movq	%r12, %rdi
	movzbl	4(%rsi), %edx
	addq	%rdx, %rax
	movq	(%rsi,%rax,8), %rdx
	movl	$56, %esi
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler15arithmetic_op_8EhNS0_8RegisterENS0_7OperandE@PLT
.L1766:
	xorl	%r13d, %r13d
	jmp	.L1231
.L3664:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r9d
	movl	$35, %esi
	movq	%r12, %rdi
	movq	%rax, -124(%rbp)
	movq	%rax, %rcx
	movq	-10472(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -116(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3787:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r13, %rdi
	movq	%rax, %r8
	movl	%edx, -5984(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8756(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r8, -5992(%rbp)
	movq	40(%rdx), %rsi
	movq	%r8, %rdx
	movq	%r8, -8764(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7movsxwlENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L2125
.L3718:
	testl	%ecx, %ecx
	je	.L1863
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1863
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1863
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movl	$5, %ecx
	movq	%r12, %rdi
	andl	$63, %edx
	movl	$8, %r8d
	movq	40(%rax), %rsi
	movl	$19, %eax
	salq	$32, %rax
	sarq	$35, %rsi
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L1231
.L3672:
	movzbl	%al, %eax
	movq	48(%rsi,%rax,8), %rax
	testb	$4, %al
	je	.L2663
	testb	$24, %al
	jne	.L2663
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$14, %dl
	je	.L3882
.L2663:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movq	%r15, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterENS0_7OperandE@PLT
.L2664:
	movl	$16, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	$15, %r13d
	call	_ZN2v88internal9Assembler5psrldENS0_11XMMRegisterEh@PLT
	jmp	.L2662
.L3662:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r9d
	movl	$35, %esi
	movq	%r12, %rdi
	movq	%rax, -124(%rbp)
	movq	%rax, %rcx
	movq	-10472(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -116(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3697:
	testl	%ecx, %ecx
	je	.L1875
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1875
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1875
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movl	$1, %ecx
	movq	%r12, %rdi
	andl	$31, %edx
	movl	$4, %r8d
	movq	40(%rax), %rsi
	movl	$19, %eax
	salq	$32, %rax
	sarq	$35, %rsi
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L1231
.L3794:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r13, %rdi
	movq	%rax, %r8
	movl	%edx, -6008(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8804(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r8, -6016(%rbp)
	movq	40(%rdx), %rsi
	movq	%r8, %rdx
	movq	%r8, -8812(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7movsxbqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L3643
.L3698:
	testl	%ecx, %ecx
	je	.L1851
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1851
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1851
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movl	$4, %ecx
	movq	%r12, %rdi
	andl	$31, %edx
	movl	$4, %r8d
	movq	40(%rax), %rsi
	movl	$19, %eax
	salq	$32, %rax
	sarq	$35, %rsi
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L1231
.L3833:
	movzbl	%al, %eax
	movq	48(%rsi,%rax,8), %rax
	testb	$4, %al
	je	.L2671
	testb	$24, %al
	jne	.L2671
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$14, %dl
	je	.L3883
.L2671:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movq	%r15, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterENS0_7OperandE@PLT
.L2672:
	movl	$8, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	$15, %r13d
	call	_ZN2v88internal9Assembler5psllwENS0_11XMMRegisterEh@PLT
	movl	$8, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrlwENS0_11XMMRegisterEh@PLT
	jmp	.L2670
.L3821:
	movq	-10472(%rbp), %rax
	movl	$1, %edx
	movq	%r15, %rdi
	movq	48(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	jmp	.L2059
.L3721:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r9d
	movl	$43, %esi
	movq	%r12, %rdi
	movq	%rax, -124(%rbp)
	movq	%rax, %rcx
	movq	-10472(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -116(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3724:
	leaq	-10480(%rbp), %r13
	movq	%rsi, -10504(%rbp)
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %r11
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movq	-10464(%rbp), %rax
	movzbl	4(%r11), %ecx
	movl	%edx, -88(%rbp)
	addq	$5, %rax
	addq	%rax, %rcx
	movq	(%r11,%rcx,8), %rcx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3884
	movq	-10472(%rbp), %rdi
	movzbl	4(%rdi), %ecx
	addq	%rcx, %rax
	movq	(%rdi,%rax,8), %rcx
	movq	%r12, %rdi
	sarq	$35, %rcx
	call	_ZN2v88internal9Assembler5testwENS0_7OperandENS0_8RegisterE@PLT
.L1796:
	xorl	%r13d, %r13d
	jmp	.L1231
.L3658:
	leaq	-10480(%rbp), %r13
	movq	%rsi, -10504(%rbp)
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %r11
	movq	%rax, -112(%rbp)
	movq	%rax, %rcx
	movq	-10464(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -104(%rbp)
	movzbl	4(%r11), %edx
	addq	$5, %rax
	addq	%rax, %rdx
	movq	(%r11,%rdx,8), %rdx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L3885
	movq	-10472(%rbp), %rsi
	movq	%r12, %rdi
	movzbl	4(%rsi), %edx
	addq	%rdx, %rax
	movq	(%rsi,%rax,8), %rdx
	movl	$57, %esi
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler16arithmetic_op_16EhNS0_8RegisterENS0_7OperandE@PLT
.L1772:
	xorl	%r13d, %r13d
	jmp	.L1231
.L3759:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r13, %rdi
	movq	%rax, %r9
	movl	%edx, -5972(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8732(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r9, -5980(%rbp)
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -8740(%rbp)
	movq	%r9, -96(%rbp)
	sarq	$35, %rsi
	movl	%eax, -88(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L2125
.L2172:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	%r11, -10504(%rbp)
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %r11
	movq	%rax, -112(%rbp)
	movq	%rax, %rsi
	movq	-10464(%rbp), %rax
	movzbl	4(%r11), %ecx
	movl	%edx, -104(%rbp)
	addq	$5, %rax
	addq	%rax, %rcx
	movq	(%r11,%rcx,8), %rcx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L3886
	movq	-10472(%rbp), %rdi
	movl	$4, %r8d
	movzbl	4(%rdi), %ecx
	addq	%rcx, %rax
	movq	(%rdi,%rax,8), %rcx
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	sarq	$35, %rcx
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L2176
.L3745:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	cmpb	$0, 4(%r11)
	jne	.L2213
.L2214:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$0, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10472(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	movq	%rax, %rsi
	movl	%edx, -104(%rbp)
	movzbl	4(%rcx), %eax
	addq	-10464(%rbp), %rax
	movq	40(%rcx,%rax,8), %rcx
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	sarq	$35, %rcx
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S4_EEXadL_ZNS7_5movsdES3_S4_EEEEvS3_S4_.isra.0
	jmp	.L1231
.L3717:
	testl	%ecx, %ecx
	je	.L1859
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1859
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1859
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movl	$5, %ecx
	movq	%r12, %rdi
	andl	$31, %edx
	movl	$4, %r8d
	movq	40(%rax), %rsi
	movl	$19, %eax
	salq	$32, %rax
	sarq	$35, %rsi
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L1231
.L3726:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1817
	movq	%rax, %r13
	shrq	$3, %r13
	andl	$3, %r13d
	jne	.L1817
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1817
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rcx
	movl	$8, %r8d
	movq	%r12, %rdi
	movzbl	4(%rcx), %edx
	movq	40(%rcx), %rsi
	movq	40(%rcx,%rdx,8), %rdx
	sarq	$35, %rsi
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler9emit_imulENS0_8RegisterES2_NS0_9ImmediateEi@PLT
	jmp	.L1231
.L3757:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$0, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r15, %rdi
	movq	%rax, %r8
	movl	%edx, -5960(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8708(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r8, -5968(%rbp)
	movq	40(%rdx), %rsi
	movq	%r8, %rdx
	movq	%r8, -8716(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7movsxwqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1231
.L3660:
	leaq	-10480(%rbp), %r13
	movq	%rsi, -10504(%rbp)
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	-10504(%rbp), %r11
	movq	%rax, -124(%rbp)
	movq	%rax, %rcx
	movq	-10464(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -116(%rbp)
	movzbl	4(%r11), %edx
	addq	$5, %rax
	addq	%rax, %rdx
	movq	(%r11,%rdx,8), %rdx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L3887
	movq	-10472(%rbp), %rsi
	movl	$4, %r9d
	movq	%r12, %rdi
	movzbl	4(%rsi), %edx
	addq	%rdx, %rax
	movq	(%rsi,%rax,8), %rdx
	movl	$57, %esi
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
.L1778:
	xorl	%r13d, %r13d
	jmp	.L1231
.L3699:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r9d
	movl	$51, %esi
	movq	%r12, %rdi
	movq	%rax, -124(%rbp)
	movq	%rax, %rcx
	movq	-10472(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -116(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3814:
	cmpl	$2, %r13d
	je	.L3888
	cmpl	$3, %r13d
	je	.L3889
	cmpl	$12, %r13d
	je	.L3890
	cmpl	$13, %r13d
	je	.L3891
	cmpl	$14, %r13d
	je	.L3892
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movq	$0, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%rax, %r9
	movl	%edx, -5780(%rbp)
	movq	%rdx, %rax
	movq	%r9, -5788(%rbp)
	movq	%r9, -8404(%rbp)
	movl	%edx, -8396(%rbp)
	jmp	.L3637
.L3792:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r8d
	movq	%r13, %rdi
	movq	%rax, %r9
	movl	%edx, -5996(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8780(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r9, -6004(%rbp)
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -8788(%rbp)
	movq	%r9, -96(%rbp)
	sarq	$35, %rsi
	movl	%eax, -88(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L2125
.L3776:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$4, %r8d
	movq	%r13, %rdi
	movq	%rax, %r9
	movl	%edx, -6020(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8828(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r9, -6028(%rbp)
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -8836(%rbp)
	movq	%r9, -96(%rbp)
	sarq	$35, %rsi
	movl	%eax, -88(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L2125
.L3813:
	cmpl	$2, %r13d
	je	.L3893
	addq	$200, %r15
	cmpl	$3, %r13d
	je	.L3894
	cmpl	$12, %r13d
	je	.L3895
	cmpl	$13, %r13d
	je	.L3896
	cmpl	$14, %r13d
	je	.L3897
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$0, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r8d
	movq	%r15, %rdi
	movq	%rax, %r9
	movl	%edx, -5756(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8372(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r9, -5764(%rbp)
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -8380(%rbp)
	movq	%r9, -96(%rbp)
	sarq	$35, %rsi
	movl	%eax, -88(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3815:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movq	$0, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler5pushqENS0_7OperandE@PLT
	movq	24(%r15), %rax
	addl	$1, 12(%rax)
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L2260
	cmpb	$0, 112(%r15)
	je	.L3898
.L2260:
	xorl	%r13d, %r13d
	jmp	.L1231
.L3734:
	movl	_ZN2v88internalL3rbpE(%rip), %r12d
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r12d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %ecx
	movl	%r12d, %esi
	leaq	200(%r15), %rdi
	movl	$8, %r8d
	movq	%rdx, -112(%rbp)
	movl	%ecx, -104(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	24(%r15), %rax
	jmp	.L1691
.L3809:
	movl	_ZN2v88internalL6no_regE(%rip), %ecx
	movl	1100(%r15), %esi
	movq	%r14, %rdi
	movl	_ZN2v88internalL16kReturnRegister0E(%rip), %edx
	movl	%ecx, %r8d
	call	_ZNK2v88internal14TurboAssembler31RequiredStackSizeForCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_@PLT
	movl	$8, %esi
	movq	24(%r15), %rcx
	cltd
	idivl	%esi
	addl	%eax, 12(%rcx)
	jmp	.L1695
.L3808:
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	%rdx, %rdi
	call	_ZN2v88internal17ExternalReference14FromRawAddressEm@PLT
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1694
.L3789:
	movq	-10472(%rbp), %rcx
	movq	%r14, %rdi
	movzbl	4(%rcx), %edx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movl	$19, %eax
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movsbl	%dl, %ecx
	salq	$32, %rax
	movl	-88(%rbp), %edx
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler4movbENS0_7OperandENS0_9ImmediateE@PLT
	jmp	.L2146
.L3818:
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	%r13d, %edx
	leaq	-112(%rbp), %rdi
	movq	%rax, %rbx
	salq	$24, %rax
	xorl	%r13d, %r13d
	sarq	$56, %rax
	movq	%rax, %r14
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movzbl	%r14b, %eax
	movq	-112(%rbp), %rsi
	movl	-104(%rbp), %edx
	salq	$32, %rax
	movl	%ebx, %ecx
	movl	$8, %r8d
	movq	%r15, %rdi
	orq	%rax, %rcx
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3755:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r8d
	movq	%r13, %rdi
	movq	%rax, %r9
	movl	%edx, -5948(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8684(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r9, -5956(%rbp)
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -8692(%rbp)
	movq	%r9, -96(%rbp)
	sarq	$35, %rsi
	movl	%eax, -88(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L2125
.L3804:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	movl	$118, %r9d
	movl	$102, %ecx
	movq	%r15, %rdi
	movl	$15, %r8d
	movl	%r13d, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psrldENS0_11XMMRegisterEh@PLT
	movq	-10472(%rbp), %rax
	movl	%r13d, %edx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5andpsENS0_11XMMRegisterES2_@PLT
	jmp	.L1231
.L1678:
	call	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	jmp	.L1677
.L3692:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r13d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler6movapsENS0_11XMMRegisterES2_@PLT
	movl	$118, %r9d
	movl	%ebx, %edx
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	movl	%r13d, %edx
	movl	$239, %r9d
	movl	%ebx, %esi
	movl	$15, %r8d
	movl	$102, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L3706:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	$1, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	$8, %r9d
	movl	$11, %esi
	movq	%r12, %rdi
	movq	%rax, -124(%rbp)
	movq	%rax, %rcx
	movq	-10472(%rbp), %rax
	movq	%rdx, %r8
	movl	%edx, -116(%rbp)
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rdx
	movq	%rcx, -112(%rbp)
	movl	%r8d, -104(%rbp)
	sarq	$35, %rdx
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3651:
	leaq	-10480(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler27InstructionOperandConverter9InputCodeEm
	movl	$16, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	jmp	.L1677
.L3778:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r13, %rdi
	movq	%rax, %r8
	movl	%edx, -6032(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8852(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r8, -6040(%rbp)
	movq	40(%rdx), %rsi
	movq	%r8, %rdx
	movq	%r8, -8860(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler7movsxblENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L2125
.L1875:
	leaq	-10480(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movq	%rdx, %rbx
	xorl	%edx, %edx
	movq	40(%rax), %rsi
	andl	$31, %ebx
	sarq	$35, %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii
	movq	%rbx, %rcx
	movl	$4, %r9d
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movl	$19, %eax
	movl	$1, %r8d
	salq	$32, %rax
	movl	%edx, -88(%rbp)
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandENS0_9ImmediateEii@PLT
	jmp	.L1231
.L1859:
	leaq	-10480(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movq	%rdx, %rbx
	xorl	%edx, %edx
	movq	40(%rax), %rsi
	andl	$31, %ebx
	sarq	$35, %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii
	movq	%rbx, %rcx
	movl	$4, %r9d
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movl	$19, %eax
	movl	$5, %r8d
	salq	$32, %rax
	movl	%edx, -88(%rbp)
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandENS0_9ImmediateEii@PLT
	jmp	.L1231
.L1851:
	leaq	-10480(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movq	%rdx, %rbx
	xorl	%edx, %edx
	movq	40(%rax), %rsi
	andl	$31, %ebx
	sarq	$35, %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii
	movq	%rbx, %rcx
	movl	$4, %r9d
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movl	$19, %eax
	movl	$4, %r8d
	salq	$32, %rax
	movl	%edx, -88(%rbp)
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandENS0_9ImmediateEii@PLT
	jmp	.L1231
.L1863:
	leaq	-10480(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movq	%rdx, %rbx
	xorl	%edx, %edx
	movq	40(%rax), %rsi
	andl	$63, %ebx
	sarq	$35, %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii
	movq	%rbx, %rcx
	movl	$8, %r9d
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movl	$19, %eax
	movl	$5, %r8d
	salq	$32, %rax
	movl	%edx, -88(%rbp)
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandENS0_9ImmediateEii@PLT
	jmp	.L1231
.L1817:
	leaq	-10480(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	salq	$24, %rdx
	movq	%rdx, %rbx
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	sarq	$56, %rbx
	movl	%r13d, %r8d
	movq	%r12, %rdi
	movq	%rax, %r10
	movl	%edx, -10040(%rbp)
	movq	%rdx, %rax
	xorl	%r13d, %r13d
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movl	$8, %r9d
	movq	%r10, -10048(%rbp)
	movq	40(%rdx), %rsi
	movzbl	%bl, %edx
	movq	%r10, -96(%rbp)
	salq	$32, %rdx
	movl	%eax, -88(%rbp)
	orq	%rdx, %r8
	sarq	$35, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal9Assembler9emit_imulENS0_8RegisterENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3743:
	movq	-10472(%rbp), %rcx
	movq	%r14, %rdi
	movzbl	4(%rcx), %edx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_9ImmediateE@PLT
	jmp	.L1231
.L2046:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movq	%rax, -9220(%rbp)
	movq	%rax, %r8
	movl	%edx, -9212(%rbp)
	movq	%rax, -124(%rbp)
	movl	%edx, -116(%rbp)
	movq	%rax, -112(%rbp)
	movl	%edx, -104(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2048
	movq	%rax, -96(%rbp)
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r9d
	movq	%r12, %rdi
	pushq	%rax
	movl	$46, %esi
	pushq	$0
	pushq	$1
	pushq	$1
	movl	%edx, -88(%rbp)
	movl	$15, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L2047
.L2039:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movq	%rax, -9244(%rbp)
	movq	%rax, %r8
	movl	%edx, -9236(%rbp)
	movq	%rax, -112(%rbp)
	movl	%edx, -104(%rbp)
	movq	%rax, -96(%rbp)
	movl	%edx, -88(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2041
	movl	%edx, %ecx
	movl	$15, %esi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8vucomissENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2040
.L3739:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r8d
	movq	%r12, %rdi
	movq	%rax, %r9
	movl	%edx, -5852(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8528(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r9, -5860(%rbp)
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -8536(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterENS0_7OperandES2_@PLT
	jmp	.L2198
.L3736:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r12, %rdi
	movq	%rax, %r8
	movl	%edx, -5864(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8552(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r8, -5872(%rbp)
	movq	40(%rdx), %rsi
	movq	%r8, %rdx
	movq	%r8, -8560(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L2198
.L1879:
	leaq	-10480(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movq	%rdx, %rbx
	xorl	%edx, %edx
	movq	40(%rax), %rsi
	andl	$63, %ebx
	sarq	$35, %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii
	movq	%rbx, %rcx
	movl	$8, %r9d
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movl	$19, %eax
	movl	$1, %r8d
	salq	$32, %rax
	movl	%edx, -88(%rbp)
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandENS0_9ImmediateEii@PLT
	jmp	.L1231
.L1855:
	leaq	-10480(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movq	%rdx, %rbx
	xorl	%edx, %edx
	movq	40(%rax), %rsi
	andl	$63, %ebx
	sarq	$35, %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii
	movq	%rbx, %rcx
	movl	$8, %r9d
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movl	$19, %eax
	movl	$4, %r8d
	salq	$32, %rax
	movl	%edx, -88(%rbp)
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandENS0_9ImmediateEii@PLT
	jmp	.L1231
.L1813:
	leaq	-10480(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	salq	$24, %rdx
	movq	%rdx, %rbx
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	sarq	$56, %rbx
	movl	%r13d, %r8d
	movq	%r12, %rdi
	movq	%rax, %r10
	movl	%edx, -10064(%rbp)
	movq	%rdx, %rax
	xorl	%r13d, %r13d
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movl	$4, %r9d
	movq	%r10, -10072(%rbp)
	movq	40(%rdx), %rsi
	movzbl	%bl, %edx
	movq	%r10, -96(%rbp)
	salq	$32, %rdx
	movl	%eax, -88(%rbp)
	orq	%rdx, %r8
	sarq	$35, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal9Assembler9emit_imulENS0_8RegisterENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L1871:
	leaq	-10480(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movq	%rdx, %rbx
	xorl	%edx, %edx
	movq	40(%rax), %rsi
	andl	$63, %ebx
	sarq	$35, %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii
	movq	%rbx, %rcx
	movl	$8, %r9d
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movl	$19, %eax
	movl	$7, %r8d
	salq	$32, %rax
	movl	%edx, -88(%rbp)
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandENS0_9ImmediateEii@PLT
	jmp	.L1231
.L1867:
	leaq	-10480(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movq	%rdx, %rbx
	xorl	%edx, %edx
	movq	40(%rax), %rsi
	andl	$31, %ebx
	sarq	$35, %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter13SlotToOperandEii
	movq	%rbx, %rcx
	movl	$4, %r9d
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movl	$19, %eax
	movl	$7, %r8d
	salq	$32, %rax
	movl	%edx, -88(%rbp)
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler5shiftENS0_7OperandENS0_9ImmediateEii@PLT
	jmp	.L1231
.L3744:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%r12, %rdi
	movq	%rax, %r8
	movl	%edx, -5876(%rbp)
	movq	%rdx, %rax
	movl	%edx, -8576(%rbp)
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r8, -5884(%rbp)
	movq	40(%rdx), %rsi
	movq	%r8, %rdx
	movq	%r8, -8584(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L2198
.L2262:
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movq	%rax, -8344(%rbp)
	movq	%rax, %r8
	movl	%edx, -8336(%rbp)
	movq	%rax, -124(%rbp)
	movl	%edx, -116(%rbp)
	movq	%rax, -112(%rbp)
	movl	%edx, -104(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2268
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, -88(%rbp)
	movl	%edx, %r9d
	movq	%r12, %rdi
	movl	$15, %edx
	movl	$16, %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
.L2269:
	movl	$8, %r8d
	movl	$4, %edx
	movl	$5, %esi
	movq	%r12, %rdi
	movabsq	$81604378640, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	24(%r15), %rax
	addl	$2, 12(%rax)
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L2271
	cmpb	$0, 112(%r15)
	je	.L3899
.L2271:
	movl	_ZN2v88internalL3rspE(%rip), %esi
	leaq	-112(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-112(%rbp), %rsi
	movl	-104(%rbp), %edx
	movq	%r12, %rdi
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %ecx
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler7vmovupsES3_S4_EEXadL_ZNS7_6movupsES3_S4_EEEEvS3_S4_.isra.0
	jmp	.L1231
.L2477:
	movq	%rsi, %rax
	salq	$4, %rax
	addq	152(%r8), %rax
	movq	8(%rax), %rsi
	jmp	.L2479
.L2219:
	movl	-88(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6movdquENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2220
.L2221:
	call	_ZN2v88internal9Assembler6movdquENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L1231
.L3875:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	jne	.L2444
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
	jmp	.L2446
.L2208:
	movl	-104(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2209
.L2210:
	movl	%edx, %ecx
	movq	%r8, %rsi
	movl	%r9d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L1231
.L2458:
	movq	%rsi, %rax
	salq	$4, %rax
	addq	152(%r8), %rax
	movq	8(%rax), %rsi
	jmp	.L2460
.L3702:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1846
	testb	$24, %al
	jne	.L1846
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1846
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movl	$6, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3738:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	jmp	.L2201
.L3683:
	sarq	$32, %rdx
	andb	$1, %dil
	jne	.L2618
	leaq	-10464(%rbp), %rdi
	movl	%edx, %esi
	movq	%xmm2, -10504(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10504(%rbp), %r11
	movq	-10456(%rbp), %rcx
	movl	4(%r11), %eax
.L2619:
	movq	-10472(%rbp), %rsi
	movzbl	%al, %r8d
	movzbl	4(%rsi), %edi
	jmp	.L2620
.L3861:
	sarq	$32, %rax
	andb	$1, %dil
	jne	.L2570
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
	movq	-10504(%rbp), %r11
	jmp	.L2572
.L3737:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	jmp	.L2206
.L1846:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %r10
	movl	%edx, -104(%rbp)
	movq	%rdx, %rax
	movl	%edx, -88(%rbp)
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$8, %r9d
	salq	$32, %rdx
	movl	$6, %esi
	movq	%r10, -112(%rbp)
	orq	%rdx, %r8
	movq	%r10, %rdx
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3782:
	sarq	$32, %rax
	andb	$1, %dil
	jne	.L2274
	movq	%rsi, -10504(%rbp)
	leaq	-10464(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
	movq	-10504(%rbp), %r11
	jmp	.L2276
.L3667:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1742
	testb	$24, %al
	jne	.L1742
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1742
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movl	$4, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-10472(%rbp), %rdx
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3868:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	jne	.L2600
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
	jmp	.L2602
.L3747:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	movl	(%r11), %ebx
	jmp	.L2179
.L1742:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	xorl	%esi, %esi
	sarq	$56, %rbx
	movq	%rax, %r10
	movl	%edx, -104(%rbp)
	movq	%rdx, %rax
	movl	%edx, -88(%rbp)
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$4, %r9d
	salq	$32, %rdx
	movq	%r12, %rdi
	movq	%r10, -112(%rbp)
	orq	%rdx, %r8
	movq	%r10, %rdx
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3705:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1834
	testb	$24, %al
	jne	.L1834
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1834
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	$4, %r8d
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3728:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1754
	testb	$24, %al
	jne	.L1754
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1754
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L1834:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %r10
	movl	%edx, -104(%rbp)
	movq	%rdx, %rax
	movl	%edx, -88(%rbp)
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$4, %r9d
	salq	$32, %rdx
	movl	$1, %esi
	movq	%r10, -112(%rbp)
	orq	%rdx, %r8
	movq	%r10, %rdx
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L1754:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %r10
	movl	%edx, -104(%rbp)
	movq	%rdx, %rax
	movl	%edx, -88(%rbp)
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$8, %r9d
	salq	$32, %rdx
	movl	$5, %esi
	movq	%r10, -112(%rbp)
	orq	%rdx, %r8
	movq	%r10, %rdx
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3771:
	movl	$4, %r8d
	movl	%edx, %ecx
	movl	$51, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L2690
.L3856:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2519
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
.L2520:
	movq	-10472(%rbp), %rsi
	jmp	.L2521
.L3669:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1746
	testb	$24, %al
	jne	.L1746
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1746
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movl	$8, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-10472(%rbp), %rdx
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3836:
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	%rax, %rbx
	movq	%rdx, %r13
	salq	$24, %rbx
	sarq	$56, %rbx
	movl	%ebx, %r14d
	testl	%eax, %eax
	jne	.L1681
	movslq	%edx, %r13
.L1681:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13CodeGenerator21DetermineStubCallModeEv@PLT
	subl	$1, %eax
	je	.L3900
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$8, %r8d
	movq	%r13, %rdx
	movb	%r14b, %cl
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	jmp	.L1684
.L3753:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_129EmitWordLoadPoisoningIfNeededEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterE.isra.0.part.0
	jmp	.L2170
.L3762:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterES2_@PLT
	jmp	.L2679
.L3730:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1810
	testb	$24, %al
	jne	.L1810
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1810
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rsi
	movl	$8, %ecx
	movabsq	$1099511627775, %rdx
	andq	%rax, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3754:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	movl	(%r11), %ebx
	jmp	.L2162
.L3663:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1758
	testb	$24, %al
	jne	.L1758
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1758
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	movl	$4, %r8d
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3805:
	movl	$4, %r8d
	movl	%edx, %ecx
	movl	$51, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L2688
.L3807:
	leaq	-112(%rbp), %rbx
	movq	%rsi, -10504(%rbp)
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1EPNS0_5LabelEi@PLT
	movl	-104(%rbp), %ecx
	movq	-112(%rbp), %rdx
	movq	%r14, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r9d
	movl	$8, %r8d
	movl	%ecx, -88(%rbp)
	movl	%r9d, %esi
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$-24, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-112(%rbp), %rsi
	movl	-104(%rbp), %edx
	movq	%r14, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r9d
	movl	$8, %r8d
	movq	%rsi, -96(%rbp)
	movl	%r9d, %ecx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rax
	movq	-10504(%rbp), %r11
	jmp	.L1692
.L3788:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	jmp	.L2144
.L3795:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movl	%ebx, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterES2_@PLT
	jmp	.L2674
.L3770:
	movl	$4, %r8d
	movl	%edx, %ecx
	movl	$51, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L2692
.L3653:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2397
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
.L2398:
	movq	-10472(%rbp), %r11
	movzbl	4(%r11), %esi
	jmp	.L2399
.L3827:
	sarq	$32, %rax
	andb	$1, %dil
	jne	.L1730
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rsi
	jmp	.L1732
.L3866:
	sarq	$32, %rax
	andb	$1, %dil
	jne	.L2591
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
	jmp	.L2593
.L3707:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1838
	testb	$24, %al
	jne	.L1838
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1838
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3650:
	shrl	$8, %eax
	movq	%rsi, -10504(%rbp)
	movzwl	%ax, %eax
	leaq	7(%rdx,%rax), %rdx
	leaq	0(,%rdx,8), %rax
	movq	(%rsi,%rdx,8), %r8
	movq	-8(%rsi,%rax), %rcx
	movq	-16(%rsi,%rax), %rdx
	movl	_ZN2v88internalL31kJavaScriptCallArgCountRegisterE(%rip), %esi
	sarq	$35, %r8
	sarq	$35, %rdx
	sarq	$35, %rcx
	call	_ZN2v88internal8compiler13CodeGenerator32AssemblePopArgumentsAdaptorFrameENS0_8RegisterES3_S3_S3_
	movq	-10504(%rbp), %r11
	movzbl	4(%r11), %edx
	jmp	.L1675
.L3766:
	sarq	$32, %rax
	andb	$1, %dil
	jne	.L1707
	leaq	-10464(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-10464(%rbp), %edx
	movq	-10456(%rbp), %r12
	jmp	.L1709
.L3713:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1792
	testb	$24, %al
	jne	.L1792
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1792
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %ecx
	movq	40(%rax,%rcx,8), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	jmp	.L1231
.L3773:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2421
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
.L2422:
	movq	-10472(%rbp), %r11
	movzbl	4(%r11), %esi
	jmp	.L2423
.L3801:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2323
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
.L2324:
	movq	-10472(%rbp), %r11
	movzbl	4(%r11), %esi
	jmp	.L2325
.L3681:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2528
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
.L2529:
	movq	-10472(%rbp), %r11
	movzbl	4(%r11), %esi
	jmp	.L2530
.L3756:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	movl	(%r11), %ebx
	jmp	.L2158
.L3685:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2609
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
.L2610:
	movq	-10472(%rbp), %r11
	movzbl	4(%r11), %esi
	jmp	.L2611
.L3673:
	sarq	$32, %rax
	andb	$1, %dil
	jne	.L2635
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
	jmp	.L2637
.L3700:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1842
	testb	$24, %al
	jne	.L1842
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1842
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movl	$6, %esi
	movq	%r12, %rdi
	movl	$4, %r8d
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3725:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1798
	testb	$24, %al
	jne	.L1798
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1798
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %ecx
	movq	40(%rax,%rcx,8), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler5testwENS0_8RegisterENS0_9ImmediateE@PLT
	jmp	.L1231
.L3791:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	movl	(%r11), %ebx
	jmp	.L2139
.L3657:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1768
	testb	$24, %al
	jne	.L1768
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1768
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movl	$7, %esi
	movq	%r12, %rdi
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_8RegisterENS0_9ImmediateE@PLT
	jmp	.L1231
.L3665:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1762
	testb	$24, %al
	jne	.L1762
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1762
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3775:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	movl	(%r11), %ebx
	jmp	.L2128
.L3671:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1786
	testb	$24, %al
	jne	.L1786
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1786
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movl	$7, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3786:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	movl	(%r11), %ebx
	jmp	.L2148
.L3837:
	movq	%xmm2, -10504(%rbp)
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10504(%rbp), %r11
	movq	%rax, %rbx
	movq	%rdx, %r12
	salq	$24, %rbx
	sarq	$56, %rbx
	testl	%eax, %eax
	jne	.L1668
	movslq	%edx, %r12
.L1668:
	movq	%r15, %rdi
	movq	%r11, -10504(%rbp)
	call	_ZNK2v88internal8compiler13CodeGenerator21DetermineStubCallModeEv@PLT
	movq	-10504(%rbp), %r11
	movsbl	%bl, %edx
	subl	$1, %eax
	je	.L3901
	testl	$268435456, (%r11)
	movq	%r11, -10504(%rbp)
	movq	%r12, %rsi
	movq	%r13, %rdi
	je	.L1671
	call	_ZN2v88internal14TurboAssembler13RetpolineCallEmNS0_9RelocInfo4ModeE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1672
.L1746:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	xorl	%esi, %esi
	sarq	$56, %rbx
	movq	%rax, %r10
	movl	%edx, -104(%rbp)
	movq	%rdx, %rax
	movl	%edx, -88(%rbp)
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$8, %r9d
	salq	$32, %rdx
	movq	%r12, %rdi
	movq	%r10, -112(%rbp)
	orq	%rdx, %r8
	movq	%r10, %rdx
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3799:
	sarq	$32, %rdx
	andb	$1, %dil
	jne	.L2332
	leaq	-10464(%rbp), %rdi
	movl	%edx, %esi
	movq	%xmm2, -10504(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10504(%rbp), %r11
	movq	-10456(%rbp), %r14
	movl	4(%r11), %eax
.L2333:
	movq	-10472(%rbp), %rcx
	movzbl	%al, %r8d
	movzbl	4(%rcx), %edx
	jmp	.L2334
.L3777:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	movl	(%r11), %ebx
	jmp	.L2123
.L3751:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	jmp	.L2167
.L3732:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2373
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
.L2374:
	movq	-10472(%rbp), %r11
	movzbl	4(%r11), %esi
	jmp	.L2375
.L3810:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2348
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
.L2349:
	movq	-10472(%rbp), %r11
	movzbl	4(%r11), %esi
	jmp	.L2350
.L1810:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %ecx
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %rsi
	movl	$8, %r8d
	movl	%edx, -88(%rbp)
	movq	%rax, -96(%rbp)
	movzbl	%bl, %eax
	salq	$32, %rax
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3793:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	movl	(%r11), %ebx
	jmp	.L2134
.L3661:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1780
	testb	$24, %al
	jne	.L1780
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1780
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movl	$7, %esi
	movq	%r12, %rdi
	movl	$4, %r8d
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3659:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1774
	testb	$24, %al
	jne	.L1774
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1774
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movl	$7, %esi
	movq	%r12, %rdi
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler26immediate_arithmetic_op_16EhNS0_8RegisterENS0_9ImmediateE@PLT
	jmp	.L1231
.L1780:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %r10
	movl	%edx, -104(%rbp)
	movq	%rdx, %rax
	movl	%edx, -88(%rbp)
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$4, %r9d
	salq	$32, %rdx
	movl	$7, %esi
	movq	%r10, -112(%rbp)
	orq	%rdx, %r8
	movq	%r10, %rdx
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L1774:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %r9
	movl	%edx, -88(%rbp)
	movq	%rdx, %rax
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$7, %esi
	movq	%r9, -96(%rbp)
	salq	$32, %rdx
	orq	%rdx, %r8
	movq	%r9, %rdx
	call	_ZN2v88internal9Assembler26immediate_arithmetic_op_16EhNS0_7OperandENS0_9ImmediateE@PLT
	jmp	.L1231
.L3722:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1750
	testb	$24, %al
	jne	.L1750
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1750
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movl	$4, %r8d
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rdx
	movabsq	$1099511627775, %rcx
	andq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L1758:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %r10
	movl	%edx, -104(%rbp)
	movq	%rdx, %rax
	movl	%edx, -88(%rbp)
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$4, %r9d
	salq	$32, %rdx
	movl	$4, %esi
	movq	%r10, -112(%rbp)
	orq	%rdx, %r8
	movq	%r10, %rdx
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L1750:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %r10
	movl	%edx, -104(%rbp)
	movq	%rdx, %rax
	movl	%edx, -88(%rbp)
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$4, %r9d
	salq	$32, %rdx
	movl	$5, %esi
	movq	%r10, -112(%rbp)
	orq	%rdx, %r8
	movq	%r10, %rdx
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L2215:
	movl	-116(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2216
.L3746:
	leaq	-10464(%rbp), %rsi
	leaq	-10480(%rbp), %rdi
	movq	$0, -10464(%rbp)
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -5816(%rbp)
	movl	%r13d, %esi
	movl	%edx, -8480(%rbp)
	movl	%edx, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -5824(%rbp)
	movq	%rax, -8488(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %r8d
	movl	$12, %ecx
	movq	%r12, %rdi
	movl	$10, %edx
	movl	$35, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	-10472(%rbp), %rax
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movq	40(%rax), %rsi
	sarq	$35, %rsi
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_11XMMRegisterEJNS0_8RegisterEEE4emitIXadL_ZNS0_9Assembler5vmovqES3_S4_EEXadL_ZNS7_4movqES3_S4_EEEEvS3_S4_.isra.0
	jmp	.L1231
.L3750:
	leaq	-10480(%rbp), %r14
	leaq	-10464(%rbp), %rsi
	movq	$0, -10464(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter13MemoryOperandEPm
	movq	%rax, %r9
	movl	%edx, -5936(%rbp)
	movq	%rdx, %rax
	movq	%r9, -5944(%rbp)
	movq	%r9, -8668(%rbp)
	movl	%edx, -8660(%rbp)
	jmp	.L3635
.L3749:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	jmp	.L2171
.L1842:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %r10
	movl	%edx, -104(%rbp)
	movq	%rdx, %rax
	movl	%edx, -88(%rbp)
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$4, %r9d
	salq	$32, %rdx
	movl	$6, %esi
	movq	%r10, -112(%rbp)
	orq	%rdx, %r8
	movq	%r10, %rdx
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3784:
	movl	$4, %r8d
	movl	%edx, %ecx
	movl	$51, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L2686
.L3825:
	sarq	$32, %rax
	andb	$1, %dil
	jne	.L1697
	leaq	-10464(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rbx
	jmp	.L1699
.L3859:
	sarq	$32, %rax
	andb	$1, %dil
	jne	.L2549
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
	jmp	.L2551
.L1786:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %r10
	movl	%edx, -104(%rbp)
	movq	%rdx, %rax
	movl	%edx, -88(%rbp)
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$8, %r9d
	salq	$32, %rdx
	movl	$7, %esi
	movq	%r10, -112(%rbp)
	orq	%rdx, %r8
	movq	%r10, %rdx
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L1768:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %r9
	movl	%edx, -88(%rbp)
	movq	%rdx, %rax
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$7, %esi
	movq	%r9, -96(%rbp)
	salq	$32, %rdx
	orq	%rdx, %r8
	movq	%r9, %rdx
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_7OperandENS0_9ImmediateE@PLT
	jmp	.L1231
.L3720:
	movq	-8(%r11,%rdx), %rax
	testb	$4, %al
	je	.L1804
	testb	$24, %al
	jne	.L1804
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1804
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rdx
	movq	%r12, %rdi
	movzbl	4(%rdx), %ecx
	movq	40(%rdx,%rcx,8), %rsi
	movl	$4, %ecx
	movabsq	$1099511627775, %rdx
	andq	%rax, %rdx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L1792:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%esi, %esi
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_9ImmediateE@PLT
	jmp	.L1231
.L3687:
	sarq	$32, %rdx
	andb	$1, %r8b
	jne	.L2504
	leaq	-10464(%rbp), %rdi
	movl	%edx, %esi
	movq	%xmm2, -10504(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10504(%rbp), %r11
	movq	-10456(%rbp), %r13
	movl	4(%r11), %eax
.L2505:
	movq	-10472(%rbp), %rdx
	movzbl	%al, %esi
	movzbl	4(%rdx), %ecx
	jmp	.L2506
.L1804:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %ecx
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %rsi
	movl	$4, %r8d
	movl	%edx, -88(%rbp)
	movq	%rax, -96(%rbp)
	movzbl	%bl, %eax
	salq	$32, %rax
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L1798:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%esi, %esi
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler5testwENS0_7OperandENS0_9ImmediateE@PLT
	jmp	.L1231
.L1838:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %r10
	movl	%edx, -104(%rbp)
	movq	%rdx, %rax
	movl	%edx, -88(%rbp)
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$8, %r9d
	salq	$32, %rdx
	movl	$1, %esi
	movq	%r10, -112(%rbp)
	orq	%rdx, %r8
	movq	%r10, %rdx
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3780:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2303
	movl	%eax, %esi
	leaq	-10464(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rax
.L2304:
	movq	-10472(%rbp), %r11
	movzbl	4(%r11), %esi
	jmp	.L2305
.L3741:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	jmp	.L2217
.L3758:
	movq	232(%rdi), %rcx
	subq	216(%rdi), %rcx
	movq	%rsi, -10504(%rbp)
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119EmitOOLTrapIfNeededEPNS0_4ZoneEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterEi.isra.0.part.0
	movq	-10504(%rbp), %r11
	movl	(%r11), %ebx
	jmp	.L2153
.L3790:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_129EmitWordLoadPoisoningIfNeededEPNS1_13CodeGeneratorEiPNS1_11InstructionERNS1_19X64OperandConverterE.isra.0.part.0
	jmp	.L2147
.L1762:
	leaq	-10480(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	salq	$24, %rbx
	movl	%r14d, %r8d
	movq	%r12, %rdi
	sarq	$56, %rbx
	movq	%rax, %r10
	movl	%edx, -104(%rbp)
	movq	%rdx, %rax
	movl	%edx, -88(%rbp)
	movzbl	%bl, %edx
	movl	%eax, %ecx
	movl	$8, %r9d
	salq	$32, %rdx
	movl	$4, %esi
	movq	%r10, -112(%rbp)
	orq	%rdx, %r8
	movq	%r10, %rdx
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3772:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %esi
	movq	%r15, %rdi
	movl	$15, %r13d
	call	_ZN2v88internal9Assembler6movapdENS0_11XMMRegisterES2_@PLT
	jmp	.L2367
.L3870:
	movq	-10472(%rbp), %rcx
	movq	%r14, %rdi
	movzbl	4(%rcx), %edx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-112(%rbp), %rsi
	movl	-104(%rbp), %edx
	movq	%r12, %rdi
	movabsq	$1099511627775, %rcx
	movl	$8, %r8d
	andq	%rax, %rcx
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L2203
.L3843:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2293
	leaq	-10464(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rbx
.L2294:
	movq	-10472(%rbp), %rax
	movq	-10480(%rbp), %r15
	movzbl	4(%rax), %edx
	movq	56(%rax,%rdx,8), %rsi
	jmp	.L2295
.L3819:
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2079
	movl	$4, %edx
	leaq	-10480(%rbp), %rdi
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$4, %r8d
	movq	%r15, %rdi
	movq	%rax, %r9
	movl	%edx, -9068(%rbp)
	movq	%rdx, %rax
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r9, -9076(%rbp)
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -96(%rbp)
	movl	%eax, -88(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3735:
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2226
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$8, %r8d
	movq	%r15, %rdi
	movq	%rax, %r9
	movl	%edx, -8432(%rbp)
	movq	%rdx, %rax
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r9, -8440(%rbp)
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -96(%rbp)
	movl	%eax, -88(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3841:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2358
	leaq	-10464(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rbx
.L2359:
	movq	-10472(%rbp), %rax
	movq	-10480(%rbp), %r15
	movzbl	4(%rax), %edx
	movq	56(%rax,%rdx,8), %rsi
	jmp	.L2360
.L3649:
	call	__stack_chk_fail@PLT
.L3676:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2560
	leaq	-10464(%rbp), %rdi
	movl	%eax, %esi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %r13
	movq	-10504(%rbp), %r11
.L2561:
	movq	-10472(%rbp), %rax
	movq	-10480(%rbp), %r15
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rsi
	jmp	.L2562
.L3679:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2539
	leaq	-10464(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %r13
.L2540:
	movq	-10472(%rbp), %rax
	movq	-10480(%rbp), %r15
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rsi
	jmp	.L2541
.L3847:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2383
	leaq	-10464(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rbx
.L2384:
	movq	-10472(%rbp), %rax
	movq	-10480(%rbp), %r15
	movzbl	4(%rax), %edx
	movq	56(%rax,%rdx,8), %rsi
	jmp	.L2385
.L3845:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2431
	leaq	-10464(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rbx
.L2432:
	movq	-10472(%rbp), %rax
	movq	-10480(%rbp), %r15
	movzbl	4(%rax), %edx
	movq	56(%rax,%rdx,8), %rsi
	jmp	.L2433
.L3740:
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2223
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$4, %r8d
	movq	%r15, %rdi
	movq	%rax, %r9
	movl	%edx, -8444(%rbp)
	movq	%rdx, %rax
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r9, -8452(%rbp)
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -96(%rbp)
	movl	%eax, -88(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3849:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2407
	leaq	-10464(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rbx
.L2408:
	movq	-10472(%rbp), %rax
	movq	-10480(%rbp), %r15
	movzbl	4(%rax), %edx
	movq	56(%rax,%rdx,8), %rsi
	jmp	.L2409
.L3714:
	movq	232(%r15), %rsi
	movl	$8, %edx
	subq	216(%r15), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0
	jmp	.L1936
.L3864:
	sarq	$32, %rax
	andb	$1, %r10b
	jne	.L2581
	leaq	-10464(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %r13
.L2582:
	movq	-10472(%rbp), %rcx
	movq	-10480(%rbp), %r9
	movzbl	4(%rcx), %r8d
	jmp	.L2583
.L3824:
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2076
	xorl	%edx, %edx
	leaq	-10480(%rbp), %rdi
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal8compiler19X64OperandConverter12InputOperandEmi
	movl	$4, %r8d
	movq	%r15, %rdi
	movq	%rax, %r9
	movl	%edx, -9080(%rbp)
	movq	%rdx, %rax
	movq	-10472(%rbp), %rdx
	movl	%eax, %ecx
	movq	%r9, -9088(%rbp)
	movq	40(%rdx), %rsi
	movq	%r9, %rdx
	movq	%r9, -96(%rbp)
	movl	%eax, -88(%rbp)
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1231
.L3690:
	sarq	$32, %rax
	andb	$1, %r8b
	jne	.L2494
	leaq	-10464(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-10456(%rbp), %rbx
.L2495:
	movq	-10472(%rbp), %rax
	movq	-10480(%rbp), %r15
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rsi
	jmp	.L2496
.L3715:
	movq	-10528(%rbp), %rdi
	movq	232(%r15), %rsi
	movl	$-8, %edx
	subq	216(%r15), %rsi
	call	_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0
	jmp	.L1946
.L3885:
	movq	-10472(%rbp), %rcx
	movq	%r13, %rdi
	movzbl	4(%rcx), %edx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-112(%rbp), %r9
	movl	-104(%rbp), %ecx
	movq	%r12, %rdi
	movabsq	$1099511627775, %r8
	movl	$7, %esi
	andq	%rax, %r8
	movq	%r9, %rdx
	movq	%r9, -96(%rbp)
	movl	%ecx, -88(%rbp)
	call	_ZN2v88internal9Assembler26immediate_arithmetic_op_16EhNS0_7OperandENS0_9ImmediateE@PLT
	jmp	.L1772
.L3873:
	movq	-10472(%rbp), %rcx
	movq	%r13, %rdi
	movzbl	4(%rcx), %edx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-112(%rbp), %rsi
	movl	-104(%rbp), %edx
	movq	%r12, %rdi
	movabsq	$1099511627775, %rcx
	movl	$4, %r8d
	andq	%rax, %rcx
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1802
.L3874:
	movq	-10472(%rbp), %rcx
	movq	%r13, %rdi
	movzbl	4(%rcx), %edx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-112(%rbp), %rsi
	movl	-104(%rbp), %edx
	movq	%r12, %rdi
	movabsq	$1099511627775, %rcx
	movl	$8, %r8d
	andq	%rax, %rcx
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1808
.L3881:
	movq	-10472(%rbp), %rcx
	movq	%r13, %rdi
	movzbl	4(%rcx), %edx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-112(%rbp), %r9
	movl	-104(%rbp), %ecx
	movq	%r12, %rdi
	movabsq	$1099511627775, %r8
	movl	$7, %esi
	andq	%rax, %r8
	movq	%r9, %rdx
	movq	%r9, -96(%rbp)
	movl	%ecx, -88(%rbp)
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_7OperandENS0_9ImmediateE@PLT
	jmp	.L1766
.L3884:
	movq	-10472(%rbp), %rcx
	movq	%r13, %rdi
	movzbl	4(%rcx), %edx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal9Assembler5testwENS0_7OperandENS0_9ImmediateE@PLT
	jmp	.L1796
.L3816:
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler5pushqENS0_9ImmediateE@PLT
	movq	24(%r15), %rax
	addl	$1, 12(%rax)
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L1716
	cmpb	$0, 112(%r15)
	jne	.L1716
	movq	232(%r15), %rsi
	movl	$8, %edx
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	subq	216(%r15), %rsi
	call	_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0
	jmp	.L1231
.L2268:
	movl	-104(%rbp), %ecx
	movq	%rax, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2269
.L3877:
	movq	-10472(%rbp), %rcx
	movq	%r13, %rdi
	movzbl	4(%rcx), %edx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_9ImmediateE@PLT
	jmp	.L1790
.L3817:
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$11, %dl
	ja	.L2264
	sarq	$35, %rax
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movq	24(%r15), %rax
	addl	$1, 12(%rax)
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L1716
	cmpb	$0, 112(%r15)
	jne	.L1716
	movq	232(%r15), %rsi
	movl	$8, %edx
	subq	216(%r15), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0
	jmp	.L1231
.L2048:
	movl	-104(%rbp), %ecx
	movq	%rax, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7ucomisdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2047
.L2264:
	leal	-12(%rdx), %eax
	cmpb	$1, %al
	jbe	.L3902
	cmpb	$14, %dl
	jne	.L2262
	movl	$8, %r8d
	movl	$4, %edx
	movl	$5, %esi
	movq	%r12, %rdi
	movabsq	$81604378640, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	24(%r15), %rax
	addl	$2, 12(%rax)
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L2266
	cmpb	$0, 112(%r15)
	je	.L3903
.L2266:
	movq	-10472(%rbp), %rax
	movl	_ZN2v88internalL3rspE(%rip), %esi
	leaq	-112(%rbp), %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rbx
	xorl	%edx, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-112(%rbp), %rsi
	movl	-104(%rbp), %edx
	movq	%r12, %rdi
	sarq	$35, %rbx
	movl	%ebx, %ecx
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler7vmovupsES3_S4_EEXadL_ZNS7_6movupsES3_S4_EEEEvS3_S4_.isra.0
	jmp	.L1231
.L2444:
	movq	%rsi, %rax
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2446
.L2041:
	movl	-88(%rbp), %ecx
	movq	%r8, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7ucomissENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L2040
.L3886:
	movq	-10472(%rbp), %rcx
	movq	%r14, %rdi
	movzbl	4(%rcx), %edx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-112(%rbp), %rsi
	movl	-104(%rbp), %edx
	movq	%r12, %rdi
	movabsq	$1099511627775, %rcx
	movl	$4, %r8d
	andq	%rax, %rcx
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L2176
.L3880:
	movq	-10472(%rbp), %rcx
	movq	%r13, %rdi
	movzbl	4(%rcx), %edx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-124(%rbp), %r10
	movl	-116(%rbp), %ecx
	movq	%r12, %rdi
	movabsq	$1099511627775, %r8
	movl	$8, %r9d
	movl	$7, %esi
	andq	%rax, %r8
	movq	%r10, %rdx
	movq	%r10, -112(%rbp)
	movl	%ecx, -104(%rbp)
	movq	%r10, -96(%rbp)
	movl	%ecx, -88(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1784
.L1720:
	testq	%r8, %r8
	je	.L1721
	movq	%rbx, %rdi
.L1719:
	movq	%r8, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	jmp	.L1721
.L3851:
	movq	%r13, %rdi
	leaq	-10464(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -10504(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-10504(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-10464(%rbp), %rax
	movq	%rax, -80(%rbp)
	jmp	.L1719
.L3887:
	movq	-10472(%rbp), %rcx
	movq	%r13, %rdi
	movzbl	4(%rcx), %edx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal8compiler19X64OperandConverter11ToImmediateEPNS1_18InstructionOperandE
	movq	-124(%rbp), %r10
	movl	-116(%rbp), %ecx
	movq	%r12, %rdi
	movabsq	$1099511627775, %r8
	movl	$4, %r9d
	movl	$7, %esi
	andq	%rax, %r8
	movq	%r10, %rdx
	movq	%r10, -112(%rbp)
	movl	%ecx, -104(%rbp)
	movq	%r10, -96(%rbp)
	movl	%ecx, -88(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1778
.L2635:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2637
.L3678:
	leaq	-10480(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%rdx, %rcx
	movzbl	4(%rax), %edx
	movzbl	%cl, %ecx
	movq	40(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler7pshuflwENS0_11XMMRegisterES2_h@PLT
	jmp	.L2537
.L1697:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rbx
	jmp	.L1699
.L3763:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movq	%r15, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterES2_@PLT
	jmp	.L2681
.L3760:
	movl	$96, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1728
.L1717:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L2348:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2349
.L3834:
	movl	%esi, %edx
	movl	$104, %r9d
	movl	$15, %r8d
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L2397:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2398
.L1707:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movl	(%rax), %edx
	movq	8(%rax), %r12
	jmp	.L1709
.L2609:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2610
.L3768:
	movl	$56, %esi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-10504(%rbp), %r11
	movq	%rax, %rbx
	jmp	.L2002
.L2358:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rbx
	jmp	.L2359
.L1671:
	call	_ZN2v88internal14TurboAssembler4CallEmNS0_9RelocInfo4ModeE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1672
.L3769:
	movl	$56, %esi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-10504(%rbp), %r11
	movq	%rax, %rbx
	jmp	.L1967
.L3742:
	leaq	.LC8(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2528:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2529
.L3708:
	movl	$56, %esi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-10504(%rbp), %r11
	movq	%rax, %rbx
	jmp	.L1988
.L2293:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rbx
	jmp	.L2294
.L3863:
	movq	-10472(%rbp), %rax
	leaq	-10480(%rbp), %rdi
	movzbl	4(%rax), %edx
	leaq	56(%rax,%rdx,8), %rsi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%rdx, %rcx
	movzbl	4(%rax), %edx
	movzbl	%cl, %ecx
	movq	40(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler7pshuflwENS0_11XMMRegisterES2_h@PLT
	jmp	.L2579
.L2618:
	salq	$4, %rdx
	addq	152(%rcx), %rdx
	movq	8(%rdx), %rcx
	jmp	.L2619
.L3689:
	leaq	-10480(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movq	%r12, %rdi
	movq	%rdx, %rcx
	movzbl	4(%rax), %edx
	movq	40(%rax), %rsi
	movzbl	%cl, %ecx
	movq	40(%rax,%rdx,8), %rdx
	sarq	$35, %rsi
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler6pshufdENS0_11XMMRegisterES2_h@PLT
	jmp	.L1231
.L2581:
	salq	$4, %rax
	addq	152(%rsi), %rax
	movq	8(%rax), %r13
	jmp	.L2582
.L2373:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2374
.L3894:
	movq	8(%r11,%rcx), %rcx
	movl	$8, %r8d
	sarq	$35, %rcx
	cmpl	%ecx, %esi
	je	.L3904
	movl	%esi, %edx
	movq	%r15, %rdi
	movl	$3, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1231
.L3893:
	leaq	8(%r11,%rcx), %rsi
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	%rdx, %rax
	testl	%edx, %edx
	jg	.L3905
	je	.L1716
	movl	%eax, %ecx
	movl	$19, %eax
	movl	$8, %r8d
	xorl	%r13d, %r13d
	movq	-10472(%rbp), %rdx
	negl	%ecx
	salq	$32, %rax
	leaq	200(%r15), %rdi
	movl	%ecx, %ecx
	movl	$5, %esi
	movq	40(%rdx), %rdx
	orq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3675:
	movq	%r8, %rsi
	leaq	-10480(%rbp), %rdi
	movq	%xmm2, -10504(%rbp)
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	-10472(%rbp), %rax
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movq	%r12, %rdi
	movq	%rdx, %rcx
	movzbl	4(%rax), %edx
	movzbl	%cl, %ecx
	movl	%r14d, %esi
	movq	48(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler7pshuflwENS0_11XMMRegisterES2_h@PLT
	movq	-10504(%rbp), %r11
	jmp	.L2558
.L2407:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rbx
	jmp	.L2408
.L2494:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rbx
	jmp	.L2495
.L2383:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rbx
	jmp	.L2384
.L3731:
	movl	$56, %esi
	movq	%r11, -10504(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-10504(%rbp), %r11
	movq	%rax, %rbx
	jmp	.L1953
.L2549:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2551
.L2519:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2520
.L2274:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2276
.L2600:
	movq	-10480(%rbp), %rax
	movq	40(%rax), %rdx
	movq	%rsi, %rax
	salq	$4, %rax
	addq	152(%rdx), %rax
	movq	8(%rax), %rax
	jmp	.L2602
.L3854:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movl	%esi, %edx
	movl	%r13d, %ecx
	movq	%r15, %rdi
	movq	%r11, -10504(%rbp)
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler6pshufdENS0_11XMMRegisterES2_h@PLT
	movq	-10504(%rbp), %r11
	jmp	.L2513
.L3855:
	movq	40(%rcx), %rsi
	movq	%r15, %rdi
	movl	%r13d, %ecx
	sarq	$35, %rsi
	call	_ZN2v88internal9Assembler6pshufdENS0_11XMMRegisterES2_h@PLT
	jmp	.L2516
.L3900:
	movsbl	%bl, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8near_jmpElNS0_9RelocInfo4ModeE@PLT
	jmp	.L1684
.L3779:
	movl	%esi, %edx
	movl	$108, %r9d
	movl	$15, %r8d
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L2504:
	salq	$4, %rdx
	addq	152(%rcx), %rdx
	movq	8(%rdx), %r13
	jmp	.L2505
.L3829:
	movl	%esi, %edx
	movl	$96, %r9d
	movl	$15, %r8d
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L3796:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movq	%r15, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterES2_@PLT
	jmp	.L2676
.L2323:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2324
.L2560:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %r13
	jmp	.L2561
.L3765:
	leaq	.LC6(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1730:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rsi
	jmp	.L1732
.L2591:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2593
.L3812:
	movl	%esi, %edx
	movl	$109, %r9d
	movl	$15, %r8d
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L2258:
	movl	%esi, %ecx
	movl	$8, %r8d
	movl	$3, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	xorl	%r13d, %r13d
	jmp	.L1231
.L3889:
	movq	8(%r11,%rcx), %rcx
	movl	$4, %r8d
	sarq	$35, %rcx
	cmpl	%ecx, %esi
	je	.L3636
	movl	%esi, %edx
	movq	%r15, %rdi
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L2240
.L3888:
	leaq	8(%r11,%rcx), %rsi
	leaq	-10480(%rbp), %rdi
	call	_ZN2v88internal8compiler27InstructionOperandConverter10ToConstantEPNS1_18InstructionOperandE
	movq	%rdx, %rax
	movl	%edx, %ecx
	testl	%edx, %edx
	jg	.L3906
	cmpl	$-2147483648, %edx
	je	.L2241
	negl	%ecx
.L2241:
	movl	%ecx, %ecx
	movl	$4, %r8d
	movl	$5, %esi
	movq	%r15, %rdi
	movq	-10472(%rbp), %rax
	movq	40(%rax), %rdx
	movl	$19, %eax
	salq	$32, %rax
	sarq	$35, %rdx
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L2240
.L3890:
	movl	$4, %r8d
.L3636:
	movl	$4, %ecx
	movq	%r15, %rdi
	movabsq	$81604378625, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L2240
.L3830:
	movl	%esi, %edx
	movl	$105, %r9d
	movl	$15, %r8d
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L3835:
	movl	%esi, %edx
	movl	$106, %r9d
	movl	$15, %r8d
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L2539:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %r13
	jmp	.L2540
.L3902:
	movl	$8, %r8d
	movl	$4, %edx
	movl	$5, %esi
	movq	%r12, %rdi
	movabsq	$81604378632, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	24(%r15), %rax
	addl	$1, 12(%rax)
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L2265
	cmpb	$0, 112(%r15)
	je	.L3907
.L2265:
	movq	-10472(%rbp), %rax
	movl	_ZN2v88internalL3rspE(%rip), %esi
	leaq	-112(%rbp), %rdi
	movzbl	4(%rax), %edx
	movq	40(%rax,%rdx,8), %rbx
	xorl	%edx, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-112(%rbp), %rsi
	movl	-104(%rbp), %edx
	movq	%r12, %rdi
	sarq	$35, %rbx
	movl	%ebx, %ecx
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal14TurboAssembler9AvxHelperINS0_7OperandEJNS0_11XMMRegisterEEE4emitIXadL_ZNS0_9Assembler6vmovsdES3_S4_EEXadL_ZNS7_5movsdES3_S4_EEEEvS3_S4_.isra.0
	jmp	.L1231
.L3872:
	movq	232(%r15), %rsi
	movl	$8, %edx
	subq	216(%r15), %rsi
	leaq	48(%r15), %rdi
	call	_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0
	jmp	.L1943
.L3761:
	movl	$88, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1726
.L2570:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2572
.L2431:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rbx
	jmp	.L2432
.L2332:
	salq	$4, %rdx
	addq	152(%rcx), %rdx
	movq	8(%rdx), %r14
	jmp	.L2333
.L2303:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2304
.L2248:
	movl	%esi, %ecx
	movl	$4, %r8d
	movl	$3, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L2240
.L2421:
	salq	$4, %rax
	addq	152(%rcx), %rax
	movq	8(%rax), %rax
	jmp	.L2422
.L3797:
	movl	%esi, %edx
	movl	$97, %r9d
	movl	$15, %r8d
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L3798:
	movl	%esi, %edx
	movl	$98, %r9d
	movl	$15, %r8d
	movl	%ebx, %esi
	movl	$102, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1231
.L3871:
	movl	%r13d, %ecx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler7pshufhwENS0_11XMMRegisterES2_h@PLT
	jmp	.L2632
.L3898:
	movq	232(%r15), %rsi
	movl	$8, %edx
	subq	216(%r15), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0
	jmp	.L2260
.L3858:
	movl	%r13d, %ecx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler7pshuflwENS0_11XMMRegisterES2_h@PLT
	jmp	.L2628
.L3906:
	movq	-10472(%rbp), %rdx
	movl	$19, %ecx
	movl	%eax, %eax
	xorl	%esi, %esi
	salq	$32, %rcx
	movl	$4, %r8d
	movq	%r15, %rdi
	movq	40(%rdx), %rdx
	orq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L2240
.L3883:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	sarq	$35, %rax
	movq	%r15, %rdi
	movl	%eax, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterES2_@PLT
	jmp	.L2672
.L3891:
	movl	$4, %r8d
	movl	$4, %ecx
	movq	%r15, %rdi
	movabsq	$81604378626, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L2240
.L3901:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler9near_callElNS0_9RelocInfo4ModeE@PLT
	movq	-10504(%rbp), %r11
	jmp	.L1672
.L3882:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	sarq	$35, %rax
	movq	%r15, %rdi
	movl	%eax, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterES2_@PLT
	jmp	.L2664
.L3899:
	movq	232(%r15), %rsi
	movl	$16, %edx
	subq	216(%r15), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0
	jmp	.L2271
.L3878:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	sarq	$35, %rax
	movq	%r15, %rdi
	movl	%eax, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterES2_@PLT
	jmp	.L2669
.L3896:
	movl	$8, %r8d
	movl	$4, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movabsq	$81604378626, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L1231
.L3895:
	movl	$8, %r8d
	movl	$4, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movabsq	$81604378625, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L1231
.L3892:
	movl	$4, %r8d
	movl	$4, %ecx
	movq	%r15, %rdi
	movabsq	$81604378627, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L2240
.L3904:
	movl	$4, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movabsq	$81604378625, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L1231
.L3897:
	movl	$8, %r8d
	movl	$4, %ecx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movabsq	$81604378627, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L1231
.L3838:
	movq	-10472(%rbp), %rax
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rdx
	movq	-10504(%rbp), %rax
	sarq	$35, %rdx
	cmpl	%edx, %eax
	je	.L2474
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterES2_@PLT
	jmp	.L2474
.L2286:
	movl	-104(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L3852:
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-148(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-10472(%rbp), %rax
	movq	-148(%rbp), %r8
	movl	-140(%rbp), %r9d
	movq	40(%rax), %rsi
	movq	%r8, -136(%rbp)
	movl	%r9d, -128(%rbp)
	movq	%r8, -124(%rbp)
	sarq	$35, %rsi
	movl	%r9d, -116(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2284
	pushq	%rcx
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movq	%r15, %rdi
	pushq	$0
	movl	$16, %esi
	pushq	$1
	pushq	$3
	movq	%r8, -112(%rbp)
	movl	%r9d, -104(%rbp)
	movq	%r8, -96(%rbp)
	movl	%r9d, -88(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L1231
.L3905:
	movq	-10472(%rbp), %rdx
	movl	$19, %ecx
	movl	%eax, %eax
	xorl	%esi, %esi
	salq	$32, %rcx
	leaq	200(%r15), %rdi
	movl	$8, %r8d
	xorl	%r13d, %r13d
	movq	40(%rdx), %rdx
	orq	%rax, %rcx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1231
.L3694:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r14d
	movq	%r12, %rdi
	movq	%r11, -10512(%rbp)
	leaq	-96(%rbp), %r13
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler6movupsENS0_11XMMRegisterES2_@PLT
	movq	-10512(%rbp), %r11
	jmp	.L2455
.L2024:
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6sqrtsdENS0_11XMMRegisterES2_@PLT
	jmp	.L1716
.L3879:
	movq	-10472(%rbp), %rax
	movl	$85, %ecx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	$15, %r13d
	movzbl	4(%rax), %edx
	movq	48(%rax,%rdx,8), %rdx
	sarq	$35, %rdx
	call	_ZN2v88internal9Assembler7pblendwENS0_11XMMRegisterES2_h@PLT
	jmp	.L2665
.L3703:
	movl	$4, %r8d
	movl	%edx, %ecx
	movl	$51, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1849
.L2284:
	movl	-116(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1231
.L3903:
	movq	232(%r15), %rsi
	movl	$16, %edx
	subq	216(%r15), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0
	jmp	.L2266
.L3907:
	movq	232(%r15), %rsi
	movl	$8, %edx
	subq	216(%r15), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19UnwindingInfoWriter25MaybeIncreaseBaseOffsetAtEii.part.0
	jmp	.L2265
	.cfi_endproc
.LFE26264:
	.size	_ZN2v88internal8compiler13CodeGenerator23AssembleArchInstructionEPNS1_11InstructionE, .-_ZN2v88internal8compiler13CodeGenerator23AssembleArchInstructionEPNS1_11InstructionE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.type	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, @function
_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E:
.LFB33554:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L3923
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L3912:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L3910
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L3908
.L3911:
	movq	%rbx, %r12
	jmp	.L3912
	.p2align 4,,10
	.p2align 3
.L3910:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3911
.L3908:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3923:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE33554:
	.size	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, .-_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.section	.text._ZN2v88internal9AssemblerD0Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD0Ev
	.type	_ZN2v88internal9AssemblerD0Ev, @function
_ZN2v88internal9AssemblerD0Ev:
.LFB32616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L3930
.L3927:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3927
.L3930:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L3928
.L3929:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L3933
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L3928
.L3934:
	movq	%rbx, %r13
	jmp	.L3929
	.p2align 4,,10
	.p2align 3
.L3933:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3934
.L3928:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3932
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L3935
	.p2align 4,,10
	.p2align 3
.L3936:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L3936
	movq	328(%r12), %rdi
.L3935:
	call	_ZdlPv@PLT
.L3932:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3937
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L3938
	.p2align 4,,10
	.p2align 3
.L3939:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L3939
	movq	240(%r12), %rdi
.L3938:
	call	_ZdlPv@PLT
.L3937:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE32616:
	.size	_ZN2v88internal9AssemblerD0Ev, .-_ZN2v88internal9AssemblerD0Ev
	.section	.text._ZN2v88internal9AssemblerD2Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD2Ev
	.type	_ZN2v88internal9AssemblerD2Ev, @function
_ZN2v88internal9AssemblerD2Ev:
.LFB32614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L3964
.L3961:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3961
.L3964:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L3962
.L3963:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L3967
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L3962
.L3968:
	movq	%rbx, %r13
	jmp	.L3963
	.p2align 4,,10
	.p2align 3
.L3967:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3968
.L3962:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3966
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L3969
	.p2align 4,,10
	.p2align 3
.L3970:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L3970
	movq	328(%r12), %rdi
.L3969:
	call	_ZdlPv@PLT
.L3966:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3971
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L3972
	.p2align 4,,10
	.p2align 3
.L3973:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L3973
	movq	240(%r12), %rdi
.L3972:
	call	_ZdlPv@PLT
.L3971:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.cfi_endproc
.LFE32614:
	.size	_ZN2v88internal9AssemblerD2Ev, .-_ZN2v88internal9AssemblerD2Ev
	.weak	_ZN2v88internal9AssemblerD1Ev
	.set	_ZN2v88internal9AssemblerD1Ev,_ZN2v88internal9AssemblerD2Ev
	.section	.text._ZN2v88internal8compiler13CodeGeneratorD0Ev,"axG",@progbits,_ZN2v88internal8compiler13CodeGeneratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler13CodeGeneratorD0Ev
	.type	_ZN2v88internal8compiler13CodeGeneratorD0Ev, @function
_ZN2v88internal8compiler13CodeGeneratorD0Ev:
.LFB32646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler13CodeGeneratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	1160(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3995
	call	_ZdlPv@PLT
.L3995:
	movq	960(%r12), %rax
	testq	%rax, %rax
	je	.L3996
	movq	1032(%r12), %rcx
	movq	1000(%r12), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L3997
	movq	952(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L4000:
	testq	%rax, %rax
	je	.L3998
	cmpq	$16, 8(%rax)
	ja	.L3999
.L3998:
	movq	(%rdx), %rax
	movq	$16, 8(%rax)
	movq	952(%r12), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 952(%r12)
.L3999:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L4000
	movq	960(%r12), %rax
.L3997:
	movq	968(%r12), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L3996
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L3996:
	movq	864(%r12), %rax
	testq	%rax, %rax
	je	.L4002
	movq	936(%r12), %rcx
	movq	904(%r12), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L4003
	movq	856(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L4006:
	testq	%rax, %rax
	je	.L4004
	cmpq	$64, 8(%rax)
	ja	.L4005
.L4004:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	856(%r12), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 856(%r12)
.L4005:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L4006
	movq	864(%r12), %rax
.L4003:
	movq	872(%r12), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L4002
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L4002:
	movq	680(%r12), %r13
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	leaq	200(%r12), %r14
	movq	%rax, 200(%r12)
	leaq	664(%r12), %rbx
	testq	%r13, %r13
	je	.L4011
.L4008:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L4008
.L4011:
	movq	624(%r12), %r13
	leaq	608(%r12), %r15
	testq	%r13, %r13
	je	.L4009
.L4010:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L4014
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L4009
.L4015:
	movq	%rbx, %r13
	jmp	.L4010
	.p2align 4,,10
	.p2align 3
.L4014:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L4015
.L4009:
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4013
	movq	600(%r12), %rax
	movq	568(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L4016
	.p2align 4,,10
	.p2align 3
.L4017:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L4017
	movq	528(%r12), %rdi
.L4016:
	call	_ZdlPv@PLT
.L4013:
	movq	440(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4018
	movq	512(%r12), %rax
	movq	480(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L4019
	.p2align 4,,10
	.p2align 3
.L4020:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L4020
	movq	440(%r12), %rdi
.L4019:
	call	_ZdlPv@PLT
.L4018:
	movq	%r14, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1344, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE32646:
	.size	_ZN2v88internal8compiler13CodeGeneratorD0Ev, .-_ZN2v88internal8compiler13CodeGeneratorD0Ev
	.section	.text._ZN2v88internal8compiler13CodeGeneratorD2Ev,"axG",@progbits,_ZN2v88internal8compiler13CodeGeneratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler13CodeGeneratorD2Ev
	.type	_ZN2v88internal8compiler13CodeGeneratorD2Ev, @function
_ZN2v88internal8compiler13CodeGeneratorD2Ev:
.LFB32644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler13CodeGeneratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	1160(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4058
	call	_ZdlPv@PLT
.L4058:
	movq	960(%rbx), %rax
	testq	%rax, %rax
	je	.L4059
	movq	1032(%rbx), %rcx
	movq	1000(%rbx), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L4060
	movq	952(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L4063:
	testq	%rax, %rax
	je	.L4061
	cmpq	$16, 8(%rax)
	ja	.L4062
.L4061:
	movq	(%rdx), %rax
	movq	$16, 8(%rax)
	movq	952(%rbx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 952(%rbx)
.L4062:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L4063
	movq	960(%rbx), %rax
.L4060:
	movq	968(%rbx), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L4059
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L4059:
	movq	864(%rbx), %rax
	testq	%rax, %rax
	je	.L4065
	movq	936(%rbx), %rcx
	movq	904(%rbx), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L4066
	movq	856(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L4069:
	testq	%rax, %rax
	je	.L4067
	cmpq	$64, 8(%rax)
	ja	.L4068
.L4067:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	856(%rbx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 856(%rbx)
.L4068:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L4069
	movq	864(%rbx), %rax
.L4066:
	movq	872(%rbx), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L4065
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L4065:
	movq	680(%rbx), %r12
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	leaq	200(%rbx), %r13
	movq	%rax, 200(%rbx)
	leaq	664(%rbx), %r14
	testq	%r12, %r12
	je	.L4074
.L4071:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L4071
.L4074:
	movq	624(%rbx), %r12
	leaq	608(%rbx), %r15
	testq	%r12, %r12
	je	.L4072
.L4073:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r12), %rdi
	movq	16(%r12), %r14
	testq	%rdi, %rdi
	je	.L4077
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L4072
.L4078:
	movq	%r14, %r12
	jmp	.L4073
	.p2align 4,,10
	.p2align 3
.L4077:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L4078
.L4072:
	movq	528(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4076
	movq	600(%rbx), %rax
	movq	568(%rbx), %r12
	leaq	8(%rax), %r14
	cmpq	%r12, %r14
	jbe	.L4079
	.p2align 4,,10
	.p2align 3
.L4080:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r14
	ja	.L4080
	movq	528(%rbx), %rdi
.L4079:
	call	_ZdlPv@PLT
.L4076:
	movq	440(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4081
	movq	512(%rbx), %rax
	movq	480(%rbx), %r12
	leaq	8(%rax), %r14
	cmpq	%r12, %r14
	jbe	.L4082
	.p2align 4,,10
	.p2align 3
.L4083:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r14
	ja	.L4083
	movq	440(%rbx), %rdi
.L4082:
	call	_ZdlPv@PLT
.L4081:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.cfi_endproc
.LFE32644:
	.size	_ZN2v88internal8compiler13CodeGeneratorD2Ev, .-_ZN2v88internal8compiler13CodeGeneratorD2Ev
	.weak	_ZN2v88internal8compiler13CodeGeneratorD1Ev
	.set	_ZN2v88internal8compiler13CodeGeneratorD1Ev,_ZN2v88internal8compiler13CodeGeneratorD2Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler13CodeGenerator24AssembleDeconstructFrameEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler13CodeGenerator24AssembleDeconstructFrameEv, @function
_GLOBAL__sub_I__ZN2v88internal8compiler13CodeGenerator24AssembleDeconstructFrameEv:
.LFB33761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE33761:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler13CodeGenerator24AssembleDeconstructFrameEv, .-_GLOBAL__sub_I__ZN2v88internal8compiler13CodeGenerator24AssembleDeconstructFrameEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler13CodeGenerator24AssembleDeconstructFrameEv
	.weak	_ZTVN2v88internal9AssemblerE
	.section	.data.rel.ro.local._ZTVN2v88internal9AssemblerE,"awG",@progbits,_ZTVN2v88internal9AssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal9AssemblerE, @object
	.size	_ZTVN2v88internal9AssemblerE, 40
_ZTVN2v88internal9AssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal9AssemblerD1Ev
	.quad	_ZN2v88internal9AssemblerD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaNE,"aw"
	.align 8
	.type	_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaNE, @object
	.size	_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaNE, 40
_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaNE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND1Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaND0Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat32NaN8GenerateEv
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaNE,"aw"
	.align 8
	.type	_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaNE, @object
	.size	_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaNE, 40
_ZTVN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaNE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND1Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaND0Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_123OutOfLineLoadFloat64NaN8GenerateEv
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToIE,"aw"
	.align 8
	.type	_ZTVN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToIE, @object
	.size	_ZTVN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToIE, 40
_ZTVN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToIE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID1Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToID0Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_126OutOfLineTruncateDoubleToI8GenerateEv
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteE,"aw"
	.align 8
	.type	_ZTVN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteE, @object
	.size	_ZTVN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteE, 40
_ZTVN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD1Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWriteD0Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_120OutOfLineRecordWrite8GenerateEv
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapE,"aw"
	.align 8
	.type	_ZTVN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapE, @object
	.size	_ZTVN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapE, 40
_ZTVN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD1Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrapD0Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_117WasmOutOfLineTrap8GenerateEv
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapE,"aw"
	.align 8
	.type	_ZTVN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapE, @object
	.size	_ZTVN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapE, 40
_ZTVN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD1Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrapD0Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_128WasmProtectedInstructionTrap8GenerateEv
	.weak	_ZTVN2v88internal8compiler13CodeGeneratorE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler13CodeGeneratorE,"awG",@progbits,_ZTVN2v88internal8compiler13CodeGeneratorE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler13CodeGeneratorE, @object
	.size	_ZTVN2v88internal8compiler13CodeGeneratorE, 48
_ZTVN2v88internal8compiler13CodeGeneratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler13CodeGeneratorD1Ev
	.quad	_ZN2v88internal8compiler13CodeGeneratorD0Ev
	.quad	_ZN2v88internal8compiler13CodeGenerator12AssembleMoveEPNS1_18InstructionOperandES4_
	.quad	_ZN2v88internal8compiler13CodeGenerator12AssembleSwapEPNS1_18InstructionOperandES4_
	.section	.rodata._ZN2v88internalL17kScratchDoubleRegE,"a"
	.align 4
	.type	_ZN2v88internalL17kScratchDoubleRegE, @object
	.size	_ZN2v88internalL17kScratchDoubleRegE, 4
_ZN2v88internalL17kScratchDoubleRegE:
	.long	15
	.section	.rodata._ZN2v88internalL16kScratchRegisterE,"a"
	.align 4
	.type	_ZN2v88internalL16kScratchRegisterE, @object
	.size	_ZN2v88internalL16kScratchRegisterE, 4
_ZN2v88internalL16kScratchRegisterE:
	.long	10
	.section	.rodata._ZN2v88internalL21kWasmInstanceRegisterE,"a"
	.align 4
	.type	_ZN2v88internalL21kWasmInstanceRegisterE, @object
	.size	_ZN2v88internalL21kWasmInstanceRegisterE, 4
_ZN2v88internalL21kWasmInstanceRegisterE:
	.long	6
	.section	.rodata._ZN2v88internalL26kSpeculationPoisonRegisterE,"a"
	.align 4
	.type	_ZN2v88internalL26kSpeculationPoisonRegisterE, @object
	.size	_ZN2v88internalL26kSpeculationPoisonRegisterE, 4
_ZN2v88internalL26kSpeculationPoisonRegisterE:
	.long	12
	.section	.rodata._ZN2v88internalL19kJSFunctionRegisterE,"a"
	.align 4
	.type	_ZN2v88internalL19kJSFunctionRegisterE, @object
	.size	_ZN2v88internalL19kJSFunctionRegisterE, 4
_ZN2v88internalL19kJSFunctionRegisterE:
	.long	7
	.section	.rodata._ZN2v88internalL4xmm6E,"a"
	.align 4
	.type	_ZN2v88internalL4xmm6E, @object
	.size	_ZN2v88internalL4xmm6E, 4
_ZN2v88internalL4xmm6E:
	.long	6
	.section	.rodata._ZN2v88internalL4xmm2E,"a"
	.align 4
	.type	_ZN2v88internalL4xmm2E, @object
	.size	_ZN2v88internalL4xmm2E, 4
_ZN2v88internalL4xmm2E:
	.long	2
	.section	.rodata._ZN2v88internalL4xmm0E,"a"
	.align 4
	.type	_ZN2v88internalL4xmm0E, @object
	.size	_ZN2v88internalL4xmm0E, 4
_ZN2v88internalL4xmm0E:
	.zero	4
	.section	.rodata._ZN2v88internalL6no_regE,"a"
	.align 4
	.type	_ZN2v88internalL6no_regE, @object
	.size	_ZN2v88internalL6no_regE, 4
_ZN2v88internalL6no_regE:
	.long	-1
	.section	.rodata._ZN2v88internalL3rbpE,"a"
	.align 4
	.type	_ZN2v88internalL3rbpE, @object
	.size	_ZN2v88internalL3rbpE, 4
_ZN2v88internalL3rbpE:
	.long	5
	.section	.rodata._ZN2v88internalL3rspE,"a"
	.align 4
	.type	_ZN2v88internalL3rspE, @object
	.size	_ZN2v88internalL3rspE, 4
_ZN2v88internalL3rspE:
	.long	4
	.section	.rodata._ZN2v88internalL3rbxE,"a"
	.align 4
	.type	_ZN2v88internalL3rbxE, @object
	.size	_ZN2v88internalL3rbxE, 4
_ZN2v88internalL3rbxE:
	.long	3
	.section	.rodata._ZN2v88internalL3rdxE,"a"
	.align 4
	.type	_ZN2v88internalL3rdxE, @object
	.size	_ZN2v88internalL3rdxE, 4
_ZN2v88internalL3rdxE:
	.long	2
	.section	.rodata._ZN2v88internalL3rcxE,"a"
	.align 4
	.type	_ZN2v88internalL3rcxE, @object
	.size	_ZN2v88internalL3rcxE, 4
_ZN2v88internalL3rcxE:
	.long	1
	.set	_ZN2v88internalL32kJavaScriptCallCodeStartRegisterE,_ZN2v88internalL3rcxE
	.section	.rodata._ZN2v88internalL3raxE,"a"
	.align 4
	.type	_ZN2v88internalL3raxE, @object
	.size	_ZN2v88internalL3raxE, 4
_ZN2v88internalL3raxE:
	.zero	4
	.set	_ZN2v88internalL31kJavaScriptCallArgCountRegisterE,_ZN2v88internalL3raxE
	.set	_ZN2v88internalL16kReturnRegister0E,_ZN2v88internalL3raxE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	7286914714848734509
	.quad	7956012745591256174
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
