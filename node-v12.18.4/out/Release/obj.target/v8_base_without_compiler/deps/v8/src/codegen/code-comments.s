	.file	"code-comments.cc"
	.text
	.section	.text._ZNK2v88internal16CodeCommentEntry14comment_lengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16CodeCommentEntry14comment_lengthEv
	.type	_ZNK2v88internal16CodeCommentEntry14comment_lengthEv, @function
_ZNK2v88internal16CodeCommentEntry14comment_lengthEv:
.LFB19252:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	addl	$1, %eax
	ret
	.cfi_endproc
.LFE19252:
	.size	_ZNK2v88internal16CodeCommentEntry14comment_lengthEv, .-_ZNK2v88internal16CodeCommentEntry14comment_lengthEv
	.section	.text._ZNK2v88internal16CodeCommentEntry4sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16CodeCommentEntry4sizeEv
	.type	_ZNK2v88internal16CodeCommentEntry4sizeEv, @function
_ZNK2v88internal16CodeCommentEntry4sizeEv:
.LFB19253:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	addl	$9, %eax
	ret
	.cfi_endproc
.LFE19253:
	.size	_ZNK2v88internal16CodeCommentEntry4sizeEv, .-_ZNK2v88internal16CodeCommentEntry4sizeEv
	.section	.text._ZN2v88internal20CodeCommentsIteratorC2Emj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CodeCommentsIteratorC2Emj
	.type	_ZN2v88internal20CodeCommentsIteratorC2Emj, @function
_ZN2v88internal20CodeCommentsIteratorC2Emj:
.LFB19255:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	addq	$4, %rsi
	movl	%edx, 8(%rdi)
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE19255:
	.size	_ZN2v88internal20CodeCommentsIteratorC2Emj, .-_ZN2v88internal20CodeCommentsIteratorC2Emj
	.globl	_ZN2v88internal20CodeCommentsIteratorC1Emj
	.set	_ZN2v88internal20CodeCommentsIteratorC1Emj,_ZN2v88internal20CodeCommentsIteratorC2Emj
	.section	.text._ZNK2v88internal20CodeCommentsIterator4sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20CodeCommentsIterator4sizeEv
	.type	_ZNK2v88internal20CodeCommentsIterator4sizeEv, @function
_ZNK2v88internal20CodeCommentsIterator4sizeEv:
.LFB19257:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE19257:
	.size	_ZNK2v88internal20CodeCommentsIterator4sizeEv, .-_ZNK2v88internal20CodeCommentsIterator4sizeEv
	.section	.rodata._ZNK2v88internal20CodeCommentsIterator10GetCommentEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"GetCommentSize() == strlen(comment_string) + 1"
	.section	.rodata._ZNK2v88internal20CodeCommentsIterator10GetCommentEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZNK2v88internal20CodeCommentsIterator10GetCommentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20CodeCommentsIterator10GetCommentEv
	.type	_ZNK2v88internal20CodeCommentsIterator10GetCommentEv, @function
_ZNK2v88internal20CodeCommentsIterator10GetCommentEv:
.LFB19258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %rbx
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	movl	4(%rbx), %edx
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L9
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19258:
	.size	_ZNK2v88internal20CodeCommentsIterator10GetCommentEv, .-_ZNK2v88internal20CodeCommentsIterator10GetCommentEv
	.section	.text._ZNK2v88internal20CodeCommentsIterator14GetCommentSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20CodeCommentsIterator14GetCommentSizeEv
	.type	_ZNK2v88internal20CodeCommentsIterator14GetCommentSizeEv, @function
_ZNK2v88internal20CodeCommentsIterator14GetCommentSizeEv:
.LFB19259:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	4(%rax), %eax
	ret
	.cfi_endproc
.LFE19259:
	.size	_ZNK2v88internal20CodeCommentsIterator14GetCommentSizeEv, .-_ZNK2v88internal20CodeCommentsIterator14GetCommentSizeEv
	.section	.text._ZNK2v88internal20CodeCommentsIterator11GetPCOffsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20CodeCommentsIterator11GetPCOffsetEv
	.type	_ZNK2v88internal20CodeCommentsIterator11GetPCOffsetEv, @function
_ZNK2v88internal20CodeCommentsIterator11GetPCOffsetEv:
.LFB19260:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	(%rax), %eax
	ret
	.cfi_endproc
.LFE19260:
	.size	_ZNK2v88internal20CodeCommentsIterator11GetPCOffsetEv, .-_ZNK2v88internal20CodeCommentsIterator11GetPCOffsetEv
	.section	.text._ZN2v88internal20CodeCommentsIterator4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CodeCommentsIterator4NextEv
	.type	_ZN2v88internal20CodeCommentsIterator4NextEv, @function
_ZN2v88internal20CodeCommentsIterator4NextEv:
.LFB19261:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movl	4(%rdx), %eax
	addl	$8, %eax
	addq	%rdx, %rax
	movq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE19261:
	.size	_ZN2v88internal20CodeCommentsIterator4NextEv, .-_ZN2v88internal20CodeCommentsIterator4NextEv
	.section	.text._ZNK2v88internal20CodeCommentsIterator10HasCurrentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20CodeCommentsIterator10HasCurrentEv
	.type	_ZNK2v88internal20CodeCommentsIterator10HasCurrentEv, @function
_ZNK2v88internal20CodeCommentsIterator10HasCurrentEv:
.LFB19262:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	addq	(%rdi), %rax
	cmpq	%rax, 16(%rdi)
	setb	%al
	ret
	.cfi_endproc
.LFE19262:
	.size	_ZNK2v88internal20CodeCommentsIterator10HasCurrentEv, .-_ZNK2v88internal20CodeCommentsIterator10HasCurrentEv
	.section	.text._ZN2v88internal18CodeCommentsWriter4EmitEPNS0_9AssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CodeCommentsWriter4EmitEPNS0_9AssemblerE
	.type	_ZN2v88internal18CodeCommentsWriter4EmitEPNS0_9AssemblerE, @function
_ZN2v88internal18CodeCommentsWriter4EmitEPNS0_9AssemblerE:
.LFB19263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	(%rdi), %eax
	movq	%rdi, -56(%rbp)
	movq	%r14, %rdi
	leal	4(%rax), %esi
	movl	%eax, -60(%rbp)
	call	_ZN2v88internal9Assembler2ddEj@PLT
	movq	8(%rbx), %r13
	cmpq	16(%rbx), %r13
	je	.L14
	.p2align 4,,10
	.p2align 3
.L20:
	movl	0(%r13), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler2ddEj@PLT
	movl	16(%r13), %eax
	movq	%r14, %rdi
	leal	1(%rax), %esi
	call	_ZN2v88internal9Assembler2ddEj@PLT
	movq	8(%r13), %rbx
	movq	16(%r13), %r12
	addq	%rbx, %r12
	cmpq	%rbx, %r12
	jne	.L19
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L17:
	movzbl	%r15b, %esi
	movq	%r14, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal9Assembler2dbEh@PLT
	cmpq	%rbx, %r12
	je	.L16
.L19:
	movq	224(%r14), %rax
	movzbl	(%rbx), %r15d
	subq	$32, %rax
	cmpq	%rax, 32(%r14)
	jb	.L17
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler10GrowBufferEv@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%esi, %esi
	movq	%r14, %rdi
	addq	$40, %r13
	call	_ZN2v88internal9Assembler2dbEh@PLT
	movq	-56(%rbp), %rax
	cmpq	%r13, 16(%rax)
	jne	.L20
.L14:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19263:
	.size	_ZN2v88internal18CodeCommentsWriter4EmitEPNS0_9AssemblerE, .-_ZN2v88internal18CodeCommentsWriter4EmitEPNS0_9AssemblerE
	.section	.text._ZNK2v88internal18CodeCommentsWriter11entry_countEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18CodeCommentsWriter11entry_countEv
	.type	_ZNK2v88internal18CodeCommentsWriter11entry_countEv, @function
_ZNK2v88internal18CodeCommentsWriter11entry_countEv:
.LFB19269:
	.cfi_startproc
	endbr64
	movabsq	$-3689348814741910323, %rdx
	movq	16(%rdi), %rax
	subq	8(%rdi), %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	ret
	.cfi_endproc
.LFE19269:
	.size	_ZNK2v88internal18CodeCommentsWriter11entry_countEv, .-_ZNK2v88internal18CodeCommentsWriter11entry_countEv
	.section	.text._ZNK2v88internal18CodeCommentsWriter12section_sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18CodeCommentsWriter12section_sizeEv
	.type	_ZNK2v88internal18CodeCommentsWriter12section_sizeEv, @function
_ZNK2v88internal18CodeCommentsWriter12section_sizeEv:
.LFB19270:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	addl	$4, %eax
	ret
	.cfi_endproc
.LFE19270:
	.size	_ZNK2v88internal18CodeCommentsWriter12section_sizeEv, .-_ZNK2v88internal18CodeCommentsWriter12section_sizeEv
	.section	.rodata._ZN2v88internal24PrintCodeCommentsSectionERSomj.str1.1,"aMS",@progbits,1
.LC2:
	.string	"CodeComments (size = "
.LC3:
	.string	")\n"
.LC4:
	.string	"pc"
.LC5:
	.string	"len"
.LC6:
	.string	" comment\n"
.LC7:
	.string	" ("
	.section	.text._ZN2v88internal24PrintCodeCommentsSectionERSomj,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24PrintCodeCommentsSectionERSomj
	.type	_ZN2v88internal24PrintCodeCommentsSectionERSomj, @function
_ZN2v88internal24PrintCodeCommentsSectionERSomj:
.LFB19271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	movl	$21, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	4(%rsi), %rbx
	leaq	.LC2(%rip), %rsi
	subq	$24, %rsp
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	(%r12,%r13), %rax
	movq	%rax, -64(%rbp)
	cmpq	%rbx, %rax
	ja	.L32
.L25:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	(%r15), %rax
	movl	$2, %edx
	movq	%r15, %rdi
	leaq	.LC4(%rip), %rsi
	leaq	.LC7(%rip), %r13
	movq	-24(%rax), %rax
	movq	$6, 16(%r15,%rax)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movl	$3, %edx
	movq	%r15, %rdi
	leaq	.LC5(%rip), %rsi
	movq	-24(%rax), %rax
	movq	$6, 16(%r15,%rax)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	movq	(%r15), %rax
	movq	%r15, %rdi
	leaq	4(%rbx), %r14
	movq	-24(%rax), %rdx
	addq	%r15, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	$6, 16(%r15,%rax)
	movl	(%rbx), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	-24(%rax), %rdx
	addq	%rdi, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$2, %eax
	movl	%eax, 24(%rdx)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	movq	$6, 16(%rdi,%rax)
	movl	4(%rbx), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	8(%rbx), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -56(%rbp)
	call	strlen@PLT
	movq	-56(%rbp), %rsi
	leaq	1(%rax), %rcx
	movq	%rax, %rdx
	movl	4(%rbx), %eax
	cmpq	%rax, %rcx
	jne	.L33
	cmpq	$-8, %rbx
	je	.L34
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%r14), %eax
	addl	$8, %eax
	addq	%rax, %rbx
	cmpq	%rbx, -64(%rbp)
	ja	.L30
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L34:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	movl	$2, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%r14), %eax
	leal	8(%rax), %ebx
	subq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	ja	.L30
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19271:
	.size	_ZN2v88internal24PrintCodeCommentsSectionERSomj, .-_ZN2v88internal24PrintCodeCommentsSectionERSomj
	.section	.rodata._ZNSt6vectorIN2v88internal16CodeCommentEntryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_.str1.1,"aMS",@progbits,1
.LC8:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal16CodeCommentEntryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal16CodeCommentEntryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal16CodeCommentEntryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal16CodeCommentEntryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal16CodeCommentEntryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB22704:
	.cfi_startproc
	endbr64
	movabsq	$-3689348814741910323, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$230584300921369395, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rdi, %rax
	je	.L62
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L53
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L63
.L37:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	40(%r14), %r8
.L52:
	movl	(%rdx), %esi
	addq	%r14, %rcx
	movq	8(%rdx), %rdi
	movl	%esi, (%rcx)
	leaq	24(%rcx), %rsi
	movq	%rsi, 8(%rcx)
	leaq	24(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L64
	movq	%rdi, 8(%rcx)
	movq	24(%rdx), %rdi
	movq	%rdi, 24(%rcx)
.L40:
	movq	16(%rdx), %rdi
	movq	%rsi, 8(%rdx)
	movq	$0, 16(%rdx)
	movq	%rdi, 16(%rcx)
	movb	$0, 24(%rdx)
	cmpq	%r15, %rbx
	je	.L41
	movq	%r14, %rcx
	movq	%r15, %rdx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rdi, 8(%rcx)
	movq	24(%rdx), %rsi
	movq	%rsi, 24(%rcx)
.L61:
	movq	16(%rdx), %rsi
	addq	$40, %rdx
	addq	$40, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %rbx
	je	.L65
.L45:
	movl	(%rdx), %esi
	movl	%esi, (%rcx)
	leaq	24(%rcx), %rsi
	movq	%rsi, 8(%rcx)
	movq	8(%rdx), %rdi
	leaq	24(%rdx), %rsi
	cmpq	%rsi, %rdi
	jne	.L42
	movdqu	24(%rdx), %xmm1
	movups	%xmm1, 24(%rcx)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	-40(%rbx), %rdx
	subq	%r15, %rdx
	shrq	$3, %rdx
	leaq	80(%r14,%rdx,8), %r8
.L41:
	cmpq	%r12, %rbx
	je	.L46
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L50:
	movl	(%rdx), %esi
	movq	8(%rdx), %rdi
	movl	%esi, (%rcx)
	leaq	24(%rcx), %rsi
	movq	%rsi, 8(%rcx)
	leaq	24(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L66
	movq	24(%rdx), %rsi
	addq	$40, %rdx
	movq	%rdi, 8(%rcx)
	addq	$40, %rcx
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	cmpq	%r12, %rdx
	jne	.L50
.L48:
	subq	%rbx, %r12
	leaq	-40(%r12), %rdx
	shrq	$3, %rdx
	leaq	40(%r8,%rdx,8), %r8
.L46:
	testq	%r15, %r15
	je	.L51
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L51:
	movq	%r14, %xmm0
	movq	%r8, %xmm3
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movdqu	24(%rdx), %xmm2
	movq	16(%rdx), %rsi
	addq	$40, %rdx
	addq	$40, %rcx
	movups	%xmm2, -16(%rcx)
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r12
	jne	.L50
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L63:
	testq	%r8, %r8
	jne	.L38
	movl	$40, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$40, %esi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L64:
	movdqu	24(%rdx), %xmm4
	movups	%xmm4, 24(%rcx)
	jmp	.L40
.L38:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	imulq	$40, %rax, %rsi
	jmp	.L37
.L62:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22704:
	.size	_ZNSt6vectorIN2v88internal16CodeCommentEntryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal16CodeCommentEntryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB19264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdx), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	-40(%rbp), %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movl	%esi, -64(%rbp)
	movq	%rbx, -56(%rbp)
	cmpq	%rcx, %rax
	je	.L76
	movq	%rax, -56(%rbp)
	movq	16(%rdx), %rax
	movq	%rax, -40(%rbp)
.L69:
	movq	8(%rdx), %rax
	movq	%rcx, (%rdx)
	movq	$0, 8(%rdx)
	movq	16(%rdi), %rsi
	movb	$0, 16(%rdx)
	movl	(%rdi), %edx
	movq	%rax, -48(%rbp)
	leal	9(%rdx,%rax), %eax
	movl	%eax, (%rdi)
	cmpq	24(%rdi), %rsi
	je	.L70
	movl	-64(%rbp), %eax
	movl	%eax, (%rsi)
	leaq	24(%rsi), %rax
	movq	%rax, 8(%rsi)
	movq	-56(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L77
	movq	%rax, 8(%rsi)
	movq	-40(%rbp), %rax
	movq	%rax, 24(%rsi)
.L72:
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$40, 16(%rdi)
.L67:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	addq	$8, %rdi
	leaq	-64(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal16CodeCommentEntryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	-56(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L67
	call	_ZdlPv@PLT
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L76:
	movdqu	16(%rdx), %xmm0
	movups	%xmm0, -40(%rbp)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L77:
	movdqu	-40(%rbp), %xmm1
	movups	%xmm1, 24(%rsi)
	jmp	.L72
.L78:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19264:
	.size	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal16CodeCommentEntry14comment_lengthEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal16CodeCommentEntry14comment_lengthEv, @function
_GLOBAL__sub_I__ZNK2v88internal16CodeCommentEntry14comment_lengthEv:
.LFB23076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23076:
	.size	_GLOBAL__sub_I__ZNK2v88internal16CodeCommentEntry14comment_lengthEv, .-_GLOBAL__sub_I__ZNK2v88internal16CodeCommentEntry14comment_lengthEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal16CodeCommentEntry14comment_lengthEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
