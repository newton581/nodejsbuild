	.file	"api-natives.cc"
	.text
	.section	.rodata._ZN2v88internal12_GLOBAL__N_118EnableAccessChecksEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"EnableAccessChecks"
	.section	.text._ZN2v88internal12_GLOBAL__N_118EnableAccessChecksEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118EnableAccessChecksEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_118EnableAccessChecksEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE:
.LFB18041:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	-1(%rax), %r14
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3:
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	(%rax), %rax
	orb	$32, 13(%rax)
	movq	(%rdx), %rcx
	movl	15(%rcx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rcx)
	addq	$8, %rsp
	xorl	%ecx, %ecx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L7
.L4:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4
	.cfi_endproc
.LFE18041:
	.size	_ZN2v88internal12_GLOBAL__N_118EnableAccessChecksEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal12_GLOBAL__N_118EnableAccessChecksEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_124ProbeInstantiationsCacheEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_124ProbeInstantiationsCacheEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeE, @function
_ZN2v88internal12_GLOBAL__N_124ProbeInstantiationsCacheEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeE:
.LFB18050:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$24, %rsp
	cmpl	$1024, %edx
	jle	.L36
	cmpl	$1048576, %edx
	jle	.L27
	andl	$1, %ecx
	jne	.L27
.L15:
	xorl	%eax, %eax
.L14:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	(%rsi), %rax
	movl	%ebx, %edi
	movq	1231(%rax), %r13
	movq	1176(%r12), %rax
	movq	15(%rax), %rsi
	call	_Z11halfsiphashjm@PLT
	movslq	35(%r13), %r8
	movq	88(%r12), %r11
	leaq	-1(%r13), %r9
	movq	96(%r12), %r10
	movl	$1, %esi
	subl	$1, %r8d
	andl	%r8d, %eax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L37:
	sarq	$32, %rcx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
.L20:
	cvttsd2siq	%xmm0, %rcx
	cmpl	%ecx, %ebx
	je	.L21
.L18:
	addl	%esi, %eax
	addl	$1, %esi
	andl	%r8d, %eax
.L22:
	leal	(%rax,%rax), %edi
	leal	40(,%rdi,8), %edx
	movslq	%edx, %rdx
	movq	(%rdx,%r9), %rcx
	cmpq	%rcx, %r11
	je	.L15
	cmpq	%rcx, %r10
	je	.L18
	testb	$1, %cl
	je	.L37
	movsd	7(%rcx), %xmm0
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L36:
	movq	(%rsi), %rax
	movq	391(%rax), %rdx
	leal	8(,%rbx,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L11:
	cmpq	%rsi, 88(%r12)
	je	.L15
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L38
.L12:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L21:
	cmpl	$-1, %eax
	je	.L15
	leal	48(,%rdi,8), %eax
	cltq
	movq	-1(%r13,%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L24:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L39
.L26:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L14
.L39:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L12
	.cfi_endproc
.LFE18050:
	.size	_ZN2v88internal12_GLOBAL__N_124ProbeInstantiationsCacheEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeE, .-_ZN2v88internal12_GLOBAL__N_124ProbeInstantiationsCacheEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeE
	.section	.text._ZN2v88internal12_GLOBAL__N_112GetIntrinsicEPNS0_7IsolateENS_9IntrinsicE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_112GetIntrinsicEPNS0_7IsolateENS_9IntrinsicE, @function
_ZN2v88internal12_GLOBAL__N_112GetIntrinsicEPNS0_7IsolateENS_9IntrinsicE:
.LFB18048:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L42:
	cmpl	$5, %ebx
	ja	.L44
	leaq	.L46(%rip), %rcx
	movq	(%rax), %rax
	movslq	(%rcx,%rbx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal12_GLOBAL__N_112GetIntrinsicEPNS0_7IsolateENS_9IntrinsicE,"a",@progbits
	.align 4
	.align 4
.L46:
	.long	.L51-.L46
	.long	.L50-.L46
	.long	.L49-.L46
	.long	.L48-.L46
	.long	.L47-.L46
	.long	.L45-.L46
	.section	.text._ZN2v88internal12_GLOBAL__N_112GetIntrinsicEPNS0_7IsolateENS_9IntrinsicE
	.p2align 4,,10
	.p2align 3
.L41:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L54
.L43:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L47:
	movq	471(%rax), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	495(%rax), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	1575(%rax), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	1583(%rax), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	1591(%rax), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	1599(%rax), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L43
	.cfi_endproc
.LFE18048:
	.size	_ZN2v88internal12_GLOBAL__N_112GetIntrinsicEPNS0_7IsolateENS_9IntrinsicE, .-_ZN2v88internal12_GLOBAL__N_112GetIntrinsicEPNS0_7IsolateENS_9IntrinsicE
	.section	.text._ZN2v88internal12_GLOBAL__N_125AddPropertyToPropertyListEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEEiPNS4_INS0_6ObjectEEE.constprop.1,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_125AddPropertyToPropertyListEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEEiPNS4_INS0_6ObjectEEE.constprop.1, @function
_ZN2v88internal12_GLOBAL__N_125AddPropertyToPropertyListEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEEiPNS4_INS0_6ObjectEEE.constprop.1:
.LFB22302:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	31(%rax), %r12
	cmpq	88(%rdi), %r12
	je	.L75
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L58
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L57:
	movq	0(%r13), %rdx
	leaq	32(%rbx), %r12
	leaq	88(%r15), %r14
	movslq	27(%rdx), %rax
	addl	$1, %eax
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	.p2align 4,,10
	.p2align 3
.L63:
	cmpq	$0, (%rbx)
	je	.L76
	movq	(%rbx), %rdx
	movq	%r15, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal12TemplateList3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %rsi
	cmpq	%rbx, %r12
	jne	.L63
.L61:
	movq	0(%r13), %r13
	movq	(%rsi), %r12
	movq	%r12, 31(%r13)
	leaq	31(%r13), %r14
	testb	$1, %r12b
	je	.L55
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L65
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L65:
	testb	$24, %al
	je	.L55
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L77
.L55:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L78
.L59:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r12, (%rsi)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r14, %rdx
	movq	%r15, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal12TemplateList3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %rsi
	cmpq	%r12, %rbx
	jne	.L63
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$4, %esi
	call	_ZN2v88internal12TemplateList3NewEPNS0_7IsolateEi@PLT
	movq	%rax, %rsi
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L77:
	addq	$8, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L78:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L59
	.cfi_endproc
.LFE22302:
	.size	_ZN2v88internal12_GLOBAL__N_125AddPropertyToPropertyListEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEEiPNS4_INS0_6ObjectEEE.constprop.1, .-_ZN2v88internal12_GLOBAL__N_125AddPropertyToPropertyListEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEEiPNS4_INS0_6ObjectEEE.constprop.1
	.section	.text._ZN2v88internal12_GLOBAL__N_126CacheTemplateInstantiationEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeENS4_INS0_8JSObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_126CacheTemplateInstantiationEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeENS4_INS0_8JSObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_126CacheTemplateInstantiationEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeENS4_INS0_8JSObjectEEE:
.LFB18054:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	cmpl	$1024, %edx
	jle	.L122
	cmpl	$1048576, %edx
	jle	.L99
	andl	$1, %ecx
	jne	.L99
.L79:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	1231(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L92:
	movl	%r12d, %edx
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal22SimpleNumberDictionary3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE@PLT
	movq	(%rax), %r12
	cmpq	(%r15), %r12
	je	.L79
	movq	(%rbx), %r13
	movq	%r12, 1231(%r13)
	leaq	1231(%r13), %r14
	testb	$1, %r12b
	jne	.L121
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L122:
	movq	(%rsi), %rax
	movq	391(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L81
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L82:
	leal	-1(%r12), %edx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %r12
	cmpq	(%r15), %r12
	je	.L79
	movq	(%rbx), %r13
	movq	%r12, 391(%r13)
	leaq	391(%r13), %r14
	testb	$1, %r12b
	je	.L79
.L121:
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L95
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L95:
	testb	$24, %al
	je	.L79
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L79
	addq	$24, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L123
.L83:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L91:
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L124
.L93:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L93
	.cfi_endproc
.LFE18054:
	.size	_ZN2v88internal12_GLOBAL__N_126CacheTemplateInstantiationEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeENS4_INS0_8JSObjectEEE, .-_ZN2v88internal12_GLOBAL__N_126CacheTemplateInstantiationEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeENS4_INS0_8JSObjectEEE
	.section	.rodata._ZN2v88internal10ApiNatives23InstantiateRemoteObjectENS0_6HandleINS0_18ObjectTemplateInfoEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"!constructor_or_backpointer().IsMap()"
	.section	.rodata._ZN2v88internal10ApiNatives23InstantiateRemoteObjectENS0_6HandleINS0_18ObjectTemplateInfoEEE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal10ApiNatives23InstantiateRemoteObjectENS0_6HandleINS0_18ObjectTemplateInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ApiNatives23InstantiateRemoteObjectENS0_6HandleINS0_18ObjectTemplateInfoEEE
	.type	_ZN2v88internal10ApiNatives23InstantiateRemoteObjectENS0_6HandleINS0_18ObjectTemplateInfoEEE, @function
_ZN2v88internal10ApiNatives23InstantiateRemoteObjectENS0_6HandleINS0_18ObjectTemplateInfoEEE:
.LFB18067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-88(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	subq	$37592, %rbx
	movq	%rbx, %rsi
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal11SaveContextC1EPNS0_7IsolateE@PLT
	movq	(%r12), %rax
	movq	41112(%rbx), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L126
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L127:
	movq	(%r12), %rax
	movl	$1040, %esi
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movl	$3, %ecx
	movslq	59(%rax), %rax
	shrl	%eax
	leal	24(,%rax,8), %edx
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	(%r14), %r14
	movq	(%rax), %r15
	movq	%rax, %r12
	movq	31(%r15), %rax
	leaq	31(%r15), %rsi
	testb	$1, %al
	jne	.L144
.L129:
	movq	%r14, 31(%r15)
	testb	$1, %r14b
	je	.L135
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	testl	$262144, %eax
	jne	.L145
	testb	$24, %al
	je	.L135
.L149:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L146
	.p2align 4,,10
	.p2align 3
.L135:
	movq	(%r12), %rax
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	orb	$32, 13(%rax)
	movq	(%r12), %rdx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	leaq	104(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	-96(%rbp), %rdi
	movq	96(%rdi), %rax
	cmpq	12480(%rdi), %rax
	jne	.L147
	movq	%rax, 12536(%rdi)
.L134:
	movq	%r13, %rdi
	call	_ZN2v88internal11SaveContextD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	call	_ZN2v88internal7Isolate21ReportPendingMessagesEv@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L149
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L126:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L150
.L128:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L144:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L129
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L146:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L150:
	movq	%rbx, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L128
.L148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18067:
	.size	_ZN2v88internal10ApiNatives23InstantiateRemoteObjectENS0_6HandleINS0_18ObjectTemplateInfoEEE, .-_ZN2v88internal10ApiNatives23InstantiateRemoteObjectENS0_6HandleINS0_18ObjectTemplateInfoEEE
	.section	.text._ZN2v88internal10ApiNatives15AddDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ApiNatives15AddDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE
	.type	_ZN2v88internal10ApiNatives15AddDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE, @function
_ZN2v88internal10ApiNatives15AddDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE:
.LFB18068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	%r8d, %esi
	pushq	%rbx
	sall	$4, %esi
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	sarl	%esi
	orb	$-64, %sil
	subq	$56, %rsp
	movq	41112(%rdi), %rdi
	salq	$32, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L152
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L153:
	movq	%rax, -72(%rbp)
	movq	(%r12), %rax
	movq	%r13, -80(%rbp)
	movq	%r14, -64(%rbp)
	movq	31(%rax), %r13
	cmpq	88(%rbx), %r13
	je	.L177
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L157
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L156:
	movq	(%r12), %rdx
	leaq	-80(%rbp), %r15
	leaq	-56(%rbp), %r13
	leaq	88(%rbx), %r14
	movslq	27(%rdx), %rax
	addl	$1, %eax
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	.p2align 4,,10
	.p2align 3
.L162:
	cmpq	$0, (%r15)
	je	.L178
	movq	(%r15), %rdx
	movq	%rbx, %rdi
	addq	$8, %r15
	call	_ZN2v88internal12TemplateList3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %rsi
	cmpq	%r15, %r13
	jne	.L162
.L160:
	movq	(%r12), %r13
	movq	(%rsi), %r12
	movq	%r12, 31(%r13)
	leaq	31(%r13), %r14
	testb	$1, %r12b
	je	.L151
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L164
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L164:
	testb	$24, %al
	je	.L151
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L179
.L151:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L176
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L180
.L158:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%r14, %rdx
	movq	%rbx, %rdi
	addq	$8, %r15
	call	_ZN2v88internal12TemplateList3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %rsi
	cmpq	%r13, %r15
	jne	.L162
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L152:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L181
.L154:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$3, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal12TemplateList3NewEPNS0_7IsolateEi@PLT
	movq	%rax, %rsi
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L179:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L176
	addq	$56, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L180:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L158
.L176:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18068:
	.size	_ZN2v88internal10ApiNatives15AddDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE, .-_ZN2v88internal10ApiNatives15AddDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal10ApiNatives15AddDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS_9IntrinsicENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ApiNatives15AddDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS_9IntrinsicENS0_18PropertyAttributesE
	.type	_ZN2v88internal10ApiNatives15AddDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS_9IntrinsicENS0_18PropertyAttributesE, @function
_ZN2v88internal10ApiNatives15AddDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS_9IntrinsicENS0_18PropertyAttributesE:
.LFB18069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rcx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	salq	$32, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$72, %rsp
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L183
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L184:
	sall	$4, %ebx
	movq	41112(%r13), %rdi
	leaq	112(%r13), %rdx
	movl	%ebx, %esi
	sarl	%esi
	orb	$-64, %sil
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L186
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %rdx
.L187:
	movq	%rdx, -88(%rbp)
	movq	%r14, %rsi
	leaq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	movq	%r12, -96(%rbp)
	movq	%r15, -72(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_125AddPropertyToPropertyListEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEEiPNS4_INS0_6ObjectEEE.constprop.1
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L191
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L192
.L185:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L186:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L193
.L188:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%r13, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%r13, %rdi
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	jmp	.L188
.L191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18069:
	.size	_ZN2v88internal10ApiNatives15AddDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS_9IntrinsicENS0_18PropertyAttributesE, .-_ZN2v88internal10ApiNatives15AddDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS_9IntrinsicENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal10ApiNatives19AddAccessorPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS4_INS0_20FunctionTemplateInfoEEESA_NS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ApiNatives19AddAccessorPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS4_INS0_20FunctionTemplateInfoEEESA_NS0_18PropertyAttributesE
	.type	_ZN2v88internal10ApiNatives19AddAccessorPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS4_INS0_20FunctionTemplateInfoEEESA_NS0_18PropertyAttributesE, @function
_ZN2v88internal10ApiNatives19AddAccessorPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS4_INS0_20FunctionTemplateInfoEEESA_NS0_18PropertyAttributesE:
.LFB18070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	%r9d, %esi
	pushq	%r12
	sall	$4, %esi
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	sarl	%esi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	orb	$-63, %sil
	salq	$32, %rsi
	subq	$72, %rsp
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L195
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L196:
	leaq	-96(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	movq	%rbx, -96(%rbp)
	movq	%r14, -80(%rbp)
	movq	%r15, -72(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_125AddPropertyToPropertyListEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEEiPNS4_INS0_6ObjectEEE.constprop.1
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L201
.L197:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L201:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	jmp	.L197
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18070:
	.size	_ZN2v88internal10ApiNatives19AddAccessorPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS4_INS0_20FunctionTemplateInfoEEESA_NS0_18PropertyAttributesE, .-_ZN2v88internal10ApiNatives19AddAccessorPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_4NameEEENS4_INS0_20FunctionTemplateInfoEEESA_NS0_18PropertyAttributesE
	.section	.text._ZN2v88internal10ApiNatives21AddNativeDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_12AccessorInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ApiNatives21AddNativeDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_12AccessorInfoEEE
	.type	_ZN2v88internal10ApiNatives21AddNativeDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_12AccessorInfoEEE, @function
_ZN2v88internal10ApiNatives21AddNativeDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_12AccessorInfoEEE:
.LFB18071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rsi, %rbx
	movq	39(%rax), %r14
	cmpq	%r14, 88(%rdi)
	je	.L218
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L205
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L204:
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12TemplateList3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	39(%r13), %r14
	movq	%r12, 39(%r13)
	testb	$1, %r12b
	je	.L202
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L208
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L208:
	testb	$24, %al
	je	.L202
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L219
.L202:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L220
.L206:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L218:
	movl	$1, %esi
	call	_ZN2v88internal12TemplateList3NewEPNS0_7IsolateEi@PLT
	movq	%rax, %rsi
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L219:
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L206
	.cfi_endproc
.LFE18071:
	.size	_ZN2v88internal10ApiNatives21AddNativeDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_12AccessorInfoEEE, .-_ZN2v88internal10ApiNatives21AddNativeDataPropertyEPNS0_7IsolateENS0_6HandleINS0_12TemplateInfoEEENS4_INS0_12AccessorInfoEEE
	.section	.rodata._ZN2v88internal10ApiNatives17CreateApiFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS4_INS0_6ObjectEEENS0_12InstanceTypeENS0_11MaybeHandleINS0_4NameEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"!obj->GetInstanceCallHandler().IsUndefined(isolate)"
	.section	.text._ZN2v88internal10ApiNatives17CreateApiFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS4_INS0_6ObjectEEENS0_12InstanceTypeENS0_11MaybeHandleINS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ApiNatives17CreateApiFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS4_INS0_6ObjectEEENS0_12InstanceTypeENS0_11MaybeHandleINS0_4NameEEE
	.type	_ZN2v88internal10ApiNatives17CreateApiFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS4_INS0_6ObjectEEENS0_12InstanceTypeENS0_11MaybeHandleINS0_4NameEEE, @function
_ZN2v88internal10ApiNatives17CreateApiFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS4_INS0_6ObjectEEENS0_12InstanceTypeENS0_11MaybeHandleINS0_4NameEEE:
.LFB18072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movq	%r9, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%r10, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%r10, -56(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal20FunctionTemplateInfo29GetOrCreateSharedFunctionInfoEPNS0_7IsolateENS0_6HandleIS1_EENS0_11MaybeHandleINS0_4NameEEE@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE@PLT
	movq	-56(%rbp), %r10
	movq	%rax, %r15
	movq	(%r10), %rax
	movslq	91(%rax), %rax
	movl	%eax, %r13d
	sarl	$3, %r13d
	andl	$1, %r13d
	jne	.L269
	testb	$4, %al
	jne	.L278
.L224:
	movq	96(%rbx), %rax
	cmpq	%rax, (%r14)
	je	.L279
	movq	(%r10), %rdx
	movq	71(%rdx), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	%rax, %rcx
	movq	-37504(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L233
	movq	15(%rax), %rcx
.L233:
	movq	88(%rbx), %rdx
	cmpq	%rcx, %rdx
	je	.L280
.L232:
	cmpq	%rsi, %rax
	je	.L234
	movq	47(%rax), %rsi
	cmpq	%rdx, %rsi
	jne	.L251
.L253:
	xorl	%r14d, %r14d
.L236:
	movzwl	%r12w, %r12d
	xorl	%esi, %esi
	movq	%r10, -56(%rbp)
	movl	%r12d, %edi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movl	%r12d, %esi
	xorl	%r8d, %r8d
	movl	$3, %ecx
	leal	(%rax,%r14), %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	-64(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE@PLT
	movq	-56(%rbp), %r10
	movq	(%r10), %rax
	movslq	91(%rax), %rdx
	testb	$1, %dl
	jne	.L281
	andl	$2, %edx
	jne	.L282
.L243:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	%rdx, %rcx
	movq	-37504(%rax), %rax
	cmpq	%rax, %rdx
	je	.L244
	movq	31(%rdx), %rcx
.L244:
	cmpq	%rcx, 88(%rbx)
	je	.L245
	movq	(%r12), %rax
	orb	$4, 13(%rax)
	movq	(%r12), %rdx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	movq	(%r10), %rax
	movq	88(%rbx), %rcx
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
.L245:
	movq	%rdx, %rsi
	cmpq	%rax, %rdx
	je	.L246
	movq	39(%rdx), %rsi
.L246:
	cmpq	%rcx, %rsi
	je	.L247
	movq	(%r12), %rax
	orb	$8, 13(%rax)
	movq	(%r10), %rax
	movq	88(%rbx), %rcx
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
.L247:
	cmpq	%rax, %rdx
	je	.L248
	movq	55(%rdx), %rax
.L248:
	cmpq	%rcx, %rax
	je	.L249
	movq	(%r12), %rax
	orb	$2, 13(%rax)
	movq	(%r10), %rax
	movq	(%r12), %rcx
	movslq	91(%rax), %rax
	movzbl	13(%rcx), %edx
	andl	$1, %eax
	xorl	$1, %eax
	andl	$-65, %edx
	movzbl	%al, %eax
	sall	$6, %eax
	orl	%edx, %eax
	movb	%al, 13(%rcx)
.L249:
	testb	%r13b, %r13b
	je	.L269
	movq	(%r12), %rax
	orb	$2, 14(%rax)
.L269:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	cmpq	%rsi, %rdx
	je	.L253
.L251:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L237
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %r10
	movq	(%rax), %rsi
.L238:
	movslq	59(%rsi), %r14
	movl	%r14d, %r13d
	shrl	%r14d
	andl	$1, %r13d
	sall	$3, %r14d
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L279:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal7Factory20NewFunctionPrototypeENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	%rax, -64(%rbp)
.L277:
	movq	-56(%rbp), %r10
	movq	88(%rbx), %rdx
	movq	(%r10), %rcx
	movq	71(%rcx), %rax
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	movq	-37504(%rcx), %rsi
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L278:
	movq	12464(%rbx), %rax
	movq	(%r15), %r8
	movq	39(%rax), %rax
	movq	1271(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L225
	movq	%r10, -72(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %r8
	movq	-72(%rbp), %r10
	movq	(%rax), %rsi
.L226:
	movq	%rsi, -1(%r8)
	testq	%rsi, %rsi
	je	.L224
	testb	$1, %sil
	je	.L224
	movq	%rsi, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L224
	movq	%rsi, %rdx
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r10
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L237:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L283
.L239:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L281:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	cmpq	%rax, %rdx
	je	.L241
	movq	55(%rdx), %rax
.L241:
	cmpq	%rax, 88(%rbx)
	je	.L284
	movq	(%r12), %rax
	orb	$16, 13(%rax)
	movq	(%r10), %rax
	movslq	91(%rax), %rdx
	andl	$2, %edx
	je	.L243
.L282:
	movq	(%r12), %rax
	orb	$32, 13(%rax)
	movq	(%r12), %rdx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	movq	(%r10), %rax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L225:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L285
.L227:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L280:
	leaq	2304(%rbx), %rdx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	$2, %r8d
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%rbx, %rdi
	movq	%r10, -72(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r10
	movq	-56(%rbp), %rsi
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L284:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%rbx, %rdi
	movq	%r10, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %rsi
	movq	-56(%rbp), %r8
	jmp	.L227
	.cfi_endproc
.LFE18072:
	.size	_ZN2v88internal10ApiNatives17CreateApiFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS4_INS0_6ObjectEEENS0_12InstanceTypeENS0_11MaybeHandleINS0_4NameEEE, .-_ZN2v88internal10ApiNatives17CreateApiFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS4_INS0_6ObjectEEENS0_12InstanceTypeENS0_11MaybeHandleINS0_4NameEEE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"parent_prototype->IsHeapObject()"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"DisableAccessChecks"
	.section	.text._ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE, @function
_ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE:
.LFB18062:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$136, %rsp
	movq	%rdx, -88(%rbp)
	movq	(%rdx), %rdx
	movq	%rsi, -128(%rbp)
	movslq	19(%rdx), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	jne	.L438
.L287:
	movslq	91(%rdx), %rax
	xorl	%r13d, %r13d
	movl	%eax, %ecx
	testb	$8, %al
	je	.L439
	andl	$2, %ecx
	je	.L307
.L311:
	movl	$1040, %r8d
.L308:
	movq	-88(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%rbx, %r9
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal10ApiNatives17CreateApiFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS4_INS0_6ObjectEEENS0_12InstanceTypeENS0_11MaybeHandleINS0_4NameEEE
	cmpq	$0, -120(%rbp)
	movq	%rax, %rbx
	jne	.L440
.L312:
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movq	(%rbx), %rdx
	movq	%rax, -144(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -152(%rbp)
	movq	-1(%rdx), %rax
	movq	%rbx, -64(%rbp)
	movzbl	13(%rax), %eax
	andl	$32, %eax
	movb	%al, -153(%rbp)
	jne	.L441
.L313:
	movq	-88(%rbp), %rax
	movq	88(%r12), %rdx
	movq	(%rax), %rdi
	movq	%rdx, %r8
	movq	%rdi, %rax
	testq	%rdi, %rdi
	je	.L317
	xorl	%esi, %esi
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L442:
	testq	%rax, %rax
	je	.L320
.L321:
	movq	39(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L318
	movq	15(%rcx), %rcx
	movq	%rdx, %r8
	sarq	$32, %rcx
	addl	%ecx, %esi
.L318:
	movq	71(%rax), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	cmpq	%rax, %rcx
	je	.L319
	movq	23(%rcx), %rax
.L319:
	cmpq	%rdx, %rax
	jne	.L442
.L320:
	movq	%rdi, %rax
	testl	%esi, %esi
	jg	.L443
.L317:
	movq	31(%rax), %rsi
	movq	%rbx, %r13
	cmpq	%r8, %rsi
	je	.L346
.L466:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L347
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -96(%rbp)
	movq	(%rax), %rsi
.L348:
	movq	15(%rsi), %rax
	movq	%rbx, %r13
	shrq	$32, %rax
	je	.L346
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movl	27(%rax), %eax
	testl	%eax, %eax
	jle	.L350
	movl	$0, -112(%rbp)
	movq	%r12, %r14
	movl	$0, -104(%rbp)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%r13, %rsi
	movl	%r8d, -136(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-136(%rbp), %r8d
	movq	%rax, %r15
.L352:
	movq	-96(%rbp), %rax
	leal	8(%r8), %ecx
	addl	$4, -112(%rbp)
	movslq	%ecx, %rcx
	movq	(%rax), %rax
	movq	-1(%rcx,%rax), %rsi
	leal	16(%r8), %ecx
	movslq	%ecx, %rcx
	testb	$1, %sil
	jne	.L354
	movq	%rsi, %rdx
	leaq	-1(%rcx,%rax), %rax
	shrq	$35, %rdx
	movq	%rdx, %r13
	andl	$7, %r13d
	btq	$32, %rsi
	movq	(%rax), %rsi
	movq	41112(%r14), %rdi
	jc	.L355
	testq	%rdi, %rdi
	je	.L356
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L357:
	movl	%r13d, %r8d
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118DefineDataPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE
	testq	%rax, %rax
	je	.L434
	movl	%r12d, -112(%rbp)
.L361:
	movq	-88(%rbp), %rax
	addl	$1, -104(%rbp)
	movl	-104(%rbp), %edx
	movq	(%rax), %rax
	cmpl	%edx, 27(%rax)
	jle	.L444
.L373:
	movl	-112(%rbp), %eax
	leal	3(%rax), %r12d
	movq	-96(%rbp), %rax
	leal	0(,%r12,8), %r8d
	movq	(%rax), %rax
	movslq	%r8d, %rdx
	movq	-1(%rdx,%rax), %r13
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L445
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L446
.L353:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%r13, (%r15)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L444:
	movq	%r14, %r12
.L350:
	movq	%rbx, %r13
	.p2align 4,,10
	.p2align 3
.L346:
	cmpb	$0, -153(%rbp)
	jne	.L447
.L374:
	subl	$1, 41104(%r12)
	movq	-144(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-152(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L375
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L375:
	movq	%rbx, %rax
	testq	%r13, %r13
	je	.L448
.L417:
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r14
	movq	-88(%rbp), %rax
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L323
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	testq	%rsi, %rsi
	je	.L326
.L324:
	xorl	%r13d, %r13d
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L450:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	testq	%rsi, %rsi
	je	.L449
.L337:
	movq	39(%rsi), %r8
	movq	88(%r12), %rdx
	movq	41112(%r12), %rdi
	cmpq	%rdx, %r8
	je	.L328
	testq	%rdi, %rdi
	je	.L329
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L330:
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12AccessorInfo12AppendUniqueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10FixedArrayEEEi@PLT
	movq	88(%r12), %rdx
	movq	41112(%r12), %rdi
	movl	%eax, %r13d
.L328:
	movq	(%r15), %rax
	movq	71(%rax), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rcx
	je	.L332
	movq	23(%rcx), %rsi
.L332:
	cmpq	%rdx, %rsi
	movl	$0, %eax
	cmove	%rax, %rsi
	testq	%rdi, %rdi
	jne	.L450
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L451
.L336:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	testq	%rsi, %rsi
	jne	.L337
.L449:
	testl	%r13d, %r13d
	jle	.L326
	leal	-1(%r13), %eax
	movl	$16, %r13d
	leaq	24(,%rax,8), %rax
	movq	%rax, -96(%rbp)
	movq	%r12, %rax
	movq	%r14, %r12
	movq	%rax, %r14
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L453:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	41112(%r14), %rdi
	movq	(%rax), %rsi
	movq	%rax, %r15
	movq	7(%rsi), %r9
	testq	%rdi, %rdi
	je	.L342
.L455:
	movq	%r9, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L343:
	movq	(%r15), %rax
	movq	%r15, %rdx
	movq	%rbx, %rdi
	addq	$8, %r13
	movslq	19(%rax), %rcx
	shrl	$9, %ecx
	andl	$7, %ecx
	call	_ZN2v88internal8JSObject11SetAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_12AccessorInfoEEENS0_18PropertyAttributesE@PLT
	cmpq	-96(%rbp), %r13
	je	.L452
.L345:
	movq	(%r12), %rax
	movq	-1(%r13,%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L453
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L454
.L341:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	movq	41112(%r14), %rdi
	movq	7(%rsi), %r9
	testq	%rdi, %rdi
	jne	.L455
.L342:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L456
.L344:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r9, (%rsi)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L439:
	movq	71(%rdx), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-37504(%rdx), %rsi
	movq	88(%r12), %rdx
	cmpq	%rsi, %rax
	je	.L291
	movq	7(%rax), %rsi
	cmpq	%rdx, %rsi
	je	.L457
.L293:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L299
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L300:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_117InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEEb
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L377
.L297:
	movq	-88(%rbp), %rax
	movq	(%rax), %rdx
	movq	%rdx, %rcx
	movq	71(%rdx), %rax
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	movq	-37504(%rcx), %rsi
	cmpq	%rsi, %rax
	je	.L303
	movq	23(%rax), %rsi
.L303:
	cmpq	%rsi, 88(%r12)
	je	.L437
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112_GLOBAL__N_120GetInstancePrototypeEPNS0_7IsolateENS0_6ObjectE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L377
	testb	$1, (%rax)
	je	.L458
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	-88(%rbp), %rax
	movq	(%rax), %rdx
.L437:
	movl	91(%rdx), %ecx
	andl	$2, %ecx
	jne	.L311
.L307:
	movq	71(%rdx), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rcx
	movq	88(%r12), %rdx
	cmpq	-37504(%rcx), %rax
	je	.L309
	movq	31(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L311
	cmpq	39(%rax), %rcx
	jne	.L311
	.p2align 4,,10
	.p2align 3
.L400:
	movl	$1056, %r8d
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L441:
	movq	-1(%rdx), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L314
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L315:
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	(%rax), %rax
	andb	$-33, 13(%rax)
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L440:
	movl	-120(%rbp), %edx
	movq	-128(%rbp), %rsi
	movq	%rax, %r8
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_126CacheTemplateInstantiationEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeENS4_INS0_8JSObjectEEE
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L447:
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118EnableAccessChecksEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L355:
	testq	%rdi, %rdi
	je	.L362
	movl	%r8d, -136(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-136(%rbp), %r8d
	movq	%rax, %r12
.L363:
	movq	-96(%rbp), %rax
	addl	$24, %r8d
	movslq	%r8d, %r8
	movq	(%rax), %rax
	movq	-1(%r8,%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L365
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L366:
	movl	%r13d, %r9d
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122DefineAccessorPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEESA_NS0_18PropertyAttributesEb.constprop.0
	testq	%rax, %rax
	jne	.L361
.L434:
	xorl	%r13d, %r13d
	cmpb	$0, -153(%rbp)
	movq	%r14, %r12
	je	.L374
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L354:
	addl	$24, %r8d
	movq	-1(%rcx,%rax), %r13
	movq	%r14, %rdi
	movslq	%r8d, %r8
	movq	-1(%r8,%rax), %rsi
	shrq	$35, %r13
	andl	$7, %r13d
	sarq	$32, %rsi
	call	_ZN2v88internal12_GLOBAL__N_112GetIntrinsicEPNS0_7IsolateENS_9IntrinsicE
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L369
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L370:
	movl	%r13d, %r8d
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118DefineDataPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE
	testq	%rax, %rax
	jne	.L361
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L365:
	movq	41088(%r14), %r8
	cmpq	41096(%r14), %r8
	je	.L459
.L367:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r8)
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L362:
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L460
.L364:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L356:
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L461
.L358:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L369:
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L462
.L371:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L438:
	movl	-120(%rbp), %edx
	movl	$1, %ecx
	call	_ZN2v88internal12_GLOBAL__N_124ProbeInstantiationsCacheEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeE
	testq	%rax, %rax
	jne	.L417
	movq	-88(%rbp), %rax
	movq	(%rax), %rdx
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L446:
	movq	%r14, %rdi
	movl	%r8d, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-136(%rbp), %r8d
	movq	%rax, %r15
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L347:
	movq	41088(%r12), %rax
	movq	%rax, -96(%rbp)
	cmpq	41096(%r12), %rax
	je	.L463
.L349:
	movq	-96(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L448:
	cmpq	$0, -120(%rbp)
	jne	.L464
.L377:
	xorl	%eax, %eax
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L323:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L465
.L325:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	testq	%rsi, %rsi
	jne	.L324
.L326:
	movq	-88(%rbp), %rax
	movq	88(%r12), %r8
	movq	(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L471:
	movq	31(%rax), %rsi
	movq	%rbx, %r13
	cmpq	%r8, %rsi
	jne	.L466
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L314:
	movq	-144(%rbp), %rax
	movq	%rax, %rsi
	cmpq	41096(%r12), %rax
	je	.L467
.L316:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L291:
	cmpq	%rdx, %rsi
	jne	.L293
.L392:
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L294
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L295:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L309:
	cmpq	%rdx, %rax
	jne	.L311
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L464:
	movq	-128(%rbp), %rax
	cmpq	$1024, -120(%rbp)
	movq	(%rax), %rax
	jle	.L468
	movq	1231(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L379
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %r14
.L380:
	movq	1176(%r12), %rax
	movq	-120(%rbp), %rdx
	movq	15(%rax), %rsi
	movl	%edx, %edi
	movl	%edx, %ebx
	call	_Z11halfsiphashjm@PLT
	movslq	35(%r13), %rdi
	movq	88(%r12), %r9
	leaq	-1(%r13), %rsi
	movq	96(%r12), %r11
	movl	$1, %ecx
	subl	$1, %edi
	andl	%edi, %eax
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L469:
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
.L385:
	cvttsd2siq	%xmm0, %rdx
	cmpl	%edx, %ebx
	je	.L382
.L383:
	addl	%ecx, %eax
	addl	$1, %ecx
	andl	%edi, %eax
.L386:
	leal	5(%rax,%rax), %edx
	movl	%eax, %r8d
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	(%rdx,%rsi), %rdx
	cmpq	%rdx, %r9
	je	.L399
	cmpq	%rdx, %r11
	je	.L383
	testb	$1, %dl
	je	.L469
	movsd	7(%rdx), %xmm0
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L329:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L470
.L331:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L452:
	movq	-88(%rbp), %rax
	movq	%r14, %r12
	movq	88(%r12), %r8
	movq	(%rax), %rax
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L454:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%r14, %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L470:
	movq	%r12, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L461:
	movq	%r14, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%r14, %rdi
	movq	%rsi, -168(%rbp)
	movl	%r8d, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movl	-136(%rbp), %r8d
	movq	%rax, %r12
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L459:
	movq	%r14, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L462:
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L299:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L472
.L301:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L463:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, -96(%rbp)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L468:
	movq	391(%rax), %rdx
	movq	-120(%rbp), %rax
	movq	%rdx, %rcx
	leal	8(,%rax,8), %eax
	andq	$-262144, %rcx
	cltq
	movq	24(%rcx), %rcx
	movq	-37504(%rcx), %rcx
	movq	%rcx, -1(%rdx,%rax)
	xorl	%eax, %eax
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L457:
	movq	15(%rax), %r8
	cmpq	%r8, %rsi
	je	.L392
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112_GLOBAL__N_120GetInstancePrototypeEPNS0_7IsolateENS0_6ObjectE
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L297
	xorl	%eax, %eax
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L465:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L467:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L301
.L399:
	movl	$-1, %r8d
.L382:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%r8d, %edx
	call	_ZN2v88internal10DictionaryINS0_22SimpleNumberDictionaryENS0_27SimpleNumberDictionaryShapeEE11DeleteEntryEPNS0_7IsolateENS0_6HandleIS2_EEi@PLT
	movq	-128(%rbp), %rbx
	movq	(%rax), %r12
	movq	(%rbx), %r13
	movq	%r12, 1231(%r13)
	leaq	1231(%r13), %r14
	testb	$1, %r12b
	je	.L377
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L388
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L388:
	testb	$24, %al
	je	.L377
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L377
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	xorl	%eax, %eax
	jmp	.L417
.L458:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L294:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L473
.L296:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L379:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L474
.L381:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%r14)
	jmp	.L380
.L473:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L296
.L474:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L381
	.cfi_endproc
.LFE18062:
	.size	_ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE, .-_ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	.section	.text._ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	.type	_ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE, @function
_ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE:
.LFB18064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-88(%rbp), %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -96(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal11SaveContextC1EPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	movq	96(%rdi), %rax
	cmpq	12480(%rdi), %rax
	jne	.L480
	movq	%rax, 12536(%rdi)
.L477:
	movq	%rbx, %rdi
	call	_ZN2v88internal11SaveContextD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L481
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore_state
	call	_ZN2v88internal7Isolate21ReportPendingMessagesEv@PLT
	jmp	.L477
.L481:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18064:
	.size	_ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE, .-_ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_112_GLOBAL__N_120GetInstancePrototypeEPNS0_7IsolateENS0_6ObjectE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_112_GLOBAL__N_120GetInstancePrototypeEPNS0_7IsolateENS0_6ObjectE, @function
_ZN2v88internal12_GLOBAL__N_112_GLOBAL__N_120GetInstancePrototypeEPNS0_7IsolateENS0_6ObjectE:
.LFB18058:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdi), %r14
	movq	41096(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdi)
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L483
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L484:
	movq	12464(%r12), %rax
	movq	39(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L486
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L487:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L507
	movq	3104(%r12), %rax
	leaq	3104(%r12), %rsi
	movl	$3, %edx
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L491
	movl	11(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$3, %edx
.L491:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	3104(%r12), %rax
	movq	%r12, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L508
.L492:
	leaq	-144(%rbp), %r15
	movq	%rsi, -112(%rbp)
	movq	%r15, %rdi
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L493
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L494:
	movq	(%rax), %r15
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L495
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L495:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L496
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L497:
	movq	%rax, %r15
.L499:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L509
	addq	$120, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L494
.L507:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rcx
	movq	%r14, 41088(%r12)
	xorl	%r15d, %r15d
	leal	-1(%rax), %edx
	movl	%edx, 41104(%r12)
	cmpq	%r13, %rcx
	je	.L499
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L486:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L510
.L488:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%r14, %r15
	cmpq	%r13, %r14
	je	.L511
.L485:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L496:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L512
.L498:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r15, (%rax)
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L508:
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L511:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L510:
	movq	%r12, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L512:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L498
.L509:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18058:
	.size	_ZN2v88internal12_GLOBAL__N_112_GLOBAL__N_120GetInstancePrototypeEPNS0_7IsolateENS0_6ObjectE, .-_ZN2v88internal12_GLOBAL__N_112_GLOBAL__N_120GetInstancePrototypeEPNS0_7IsolateENS0_6ObjectE
	.section	.text._ZN2v88internal10ApiNatives19InstantiateFunctionENS0_6HandleINS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ApiNatives19InstantiateFunctionENS0_6HandleINS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	.type	_ZN2v88internal10ApiNatives19InstantiateFunctionENS0_6HandleINS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE, @function
_ZN2v88internal10ApiNatives19InstantiateFunctionENS0_6HandleINS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE:
.LFB18065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-72(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r15, %rdi
	andq	$-262144, %rax
	movq	24(%rax), %r12
	subq	$37592, %r12
	movq	%r12, %rsi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal11SaveContextC1EPNS0_7IsolateE@PLT
	movq	12464(%r12), %rax
	movq	39(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L514
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L515:
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	_ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	movq	-80(%rbp), %rdi
	movq	%rax, %r12
	movq	96(%rdi), %rax
	cmpq	12480(%rdi), %rax
	jne	.L521
	movq	%rax, 12536(%rdi)
.L518:
	movq	%r15, %rdi
	call	_ZN2v88internal11SaveContextD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L522
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L521:
	.cfi_restore_state
	call	_ZN2v88internal7Isolate21ReportPendingMessagesEv@PLT
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L514:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L523
.L516:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L523:
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L516
.L522:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18065:
	.size	_ZN2v88internal10ApiNatives19InstantiateFunctionENS0_6HandleINS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE, .-_ZN2v88internal10ApiNatives19InstantiateFunctionENS0_6HandleINS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_122DefineAccessorPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEESA_NS0_18PropertyAttributesEb.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122DefineAccessorPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEESA_NS0_18PropertyAttributesEb.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_122DefineAccessorPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEESA_NS0_18PropertyAttributesEb.constprop.0:
.LFB22301:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	movl	%r9d, %r8d
	movq	%rcx, %r9
	pushq	%r12
	movq	%r13, %r10
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	testb	$1, %al
	jne	.L558
.L526:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L559
.L536:
	movq	%r10, %rcx
	movq	%r9, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	je	.L542
	movq	%rbx, %rax
.L533:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L560
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$88, 11(%rdx)
	jne	.L526
	movq	79(%rax), %rax
	testb	$1, %al
	je	.L526
	movq	-1(%rax), %rdx
	cmpw	$160, 11(%rdx)
	jne	.L526
	leaq	-64(%rbp), %rdi
	movl	%r8d, -84(%rbp)
	movq	%rcx, %r12
	movq	%r13, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo12BreakAtEntryEv@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r10
	testb	%al, %al
	movl	-84(%rbp), %r8d
	je	.L526
	movq	12464(%r15), %rax
	movq	39(%rax), %r9
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L530
	movq	%r9, %rsi
	movl	%r8d, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r10
	movl	-80(%rbp), %r8d
	movq	%rax, %rsi
.L531:
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	%r8d, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	movq	-72(%rbp), %r10
	movl	-80(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r9
	jne	.L526
	.p2align 4,,10
	.p2align 3
.L542:
	xorl	%eax, %eax
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L559:
	movq	-1(%rax), %rdx
	cmpw	$88, 11(%rdx)
	jne	.L536
	movq	79(%rax), %rax
	testb	$1, %al
	je	.L536
	movq	-1(%rax), %rdx
	cmpw	$160, 11(%rdx)
	jne	.L536
	leaq	-64(%rbp), %rdi
	movl	%r8d, -84(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo12BreakAtEntryEv@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r10
	testb	%al, %al
	movl	-84(%rbp), %r8d
	je	.L536
	movq	12464(%r15), %rax
	movq	39(%rax), %r12
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L539
	movq	%r12, %rsi
	movl	%r8d, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r9
	movl	-80(%rbp), %r8d
	movq	%rax, %rsi
.L540:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movl	%r8d, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	movq	-72(%rbp), %r9
	movl	-80(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L536
	xorl	%eax, %eax
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L530:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L561
.L532:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r9, (%rsi)
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L539:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L562
.L541:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r12, (%rsi)
	jmp	.L540
.L561:
	movq	%r15, %rdi
	movl	%r8d, -84(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-84(%rbp), %r8d
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r10
	movq	%rax, %rsi
	jmp	.L532
.L562:
	movq	%r15, %rdi
	movl	%r8d, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-80(%rbp), %r8d
	movq	-72(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L541
.L560:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22301:
	.size	_ZN2v88internal12_GLOBAL__N_122DefineAccessorPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEESA_NS0_18PropertyAttributesEb.constprop.0, .-_ZN2v88internal12_GLOBAL__N_122DefineAccessorPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEESA_NS0_18PropertyAttributesEb.constprop.0
	.section	.rodata._ZN2v88internal12_GLOBAL__N_117InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEEb.str1.1,"aMS",@progbits,1
.LC6:
	.string	"ApiNatives::InstantiateObject"
	.section	.text._ZN2v88internal12_GLOBAL__N_117InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEEb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEEb, @function
_ZN2v88internal12_GLOBAL__N_117InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEEb:
.LFB18057:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	(%rsi), %rax
	movq	%rsi, -88(%rbp)
	movl	%ecx, -108(%rbp)
	movslq	19(%rax), %r13
	testq	%rdx, %rdx
	je	.L728
	movq	(%rdx), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$1105, 11(%rcx)
	je	.L567
.L724:
	movl	$0, -112(%rbp)
.L566:
	movq	47(%rax), %rsi
	cmpq	%rsi, 88(%rbx)
	je	.L729
	movq	41112(%rbx), %rdi
	addl	$1, 41104(%rbx)
	movq	41088(%rbx), %r13
	movq	41096(%rbx), %r12
	testq	%rdi, %rdi
	je	.L582
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L583:
	movq	12464(%rbx), %rax
	movq	39(%rax), %r8
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L585
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L586:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	testq	%rax, %rax
	je	.L730
	movq	(%rax), %r15
	subl	$1, 41104(%rbx)
	movq	%r13, 41088(%rbx)
	cmpq	41096(%rbx), %r12
	je	.L588
	movq	%r12, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L588:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L589
	movq	%r15, %rsi
.L726:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L581:
	testq	%r14, %r14
	movq	%r14, %r15
	cmove	%r12, %r15
.L571:
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L727
	cmpb	$0, -108(%rbp)
	jne	.L731
.L593:
	movq	41088(%rbx), %rax
	addl	$1, 41104(%rbx)
	movq	(%r12), %rdx
	movq	%rax, -128(%rbp)
	movq	41096(%rbx), %rax
	movq	%rax, -144(%rbp)
	movq	-1(%rdx), %rax
	movq	%r12, -64(%rbp)
	movzbl	13(%rax), %eax
	andl	$32, %eax
	movb	%al, -145(%rbp)
	jne	.L732
.L594:
	movq	-88(%rbp), %rax
	movq	88(%rbx), %rcx
	movq	(%rax), %r9
	movq	%rcx, %rdi
	movq	%r9, %rdx
	testq	%r9, %r9
	je	.L598
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L606:
	movq	39(%rdx), %rax
	cmpq	%rax, %rcx
	je	.L599
	movq	15(%rax), %rax
	movq	%rcx, %rdi
	sarq	$32, %rax
	addl	%eax, %r8d
.L599:
	movq	47(%rdx), %rax
	cmpq	%rcx, %rax
	je	.L607
	movq	71(%rax), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L601:
	cmpq	%rax, %rsi
	je	.L602
	movq	23(%rsi), %rax
.L602:
	cmpq	%rcx, %rax
	je	.L607
	testq	%rax, %rax
	je	.L607
	movq	71(%rax), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	cmpq	%rax, %rsi
	je	.L603
	movq	47(%rsi), %rdx
	cmpq	%rcx, %rdx
	je	.L601
	testq	%rdx, %rdx
	jne	.L606
	.p2align 4,,10
	.p2align 3
.L607:
	movq	%r9, %rdx
	testl	%r8d, %r8d
	jle	.L598
	movl	%r8d, %esi
	movq	%rbx, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %r14
	movq	-88(%rbp), %rax
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L608
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	testq	%rsi, %rsi
	je	.L611
.L609:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L627:
	movq	39(%rsi), %r8
	movq	88(%rbx), %rdx
	movq	41112(%rbx), %rdi
	cmpq	%rdx, %r8
	je	.L613
	testq	%rdi, %rdi
	je	.L614
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L615:
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal12AccessorInfo12AppendUniqueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10FixedArrayEEEi@PLT
	movq	41112(%rbx), %rdi
	movq	88(%rbx), %rdx
	movl	%eax, %r13d
.L613:
	movq	(%r15), %rax
	movq	47(%rax), %rax
	cmpq	%rax, %rdx
	je	.L621
	movq	71(%rax), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L619:
	cmpq	%rax, %rcx
	je	.L620
	movq	23(%rcx), %rax
.L620:
	cmpq	%rax, %rdx
	je	.L621
	testq	%rax, %rax
	je	.L621
	movq	71(%rax), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	cmpq	%rax, %rcx
	je	.L622
	movq	47(%rcx), %rsi
	cmpq	%rsi, %rdx
	je	.L619
	.p2align 4,,10
	.p2align 3
.L618:
	testq	%rdi, %rdi
	je	.L624
.L751:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L625:
	testq	%rsi, %rsi
	jne	.L627
	testl	%r13d, %r13d
	jle	.L611
	leal	-1(%r13), %eax
	movl	$16, %r13d
	leaq	24(,%rax,8), %rax
	movq	%rax, -96(%rbp)
	movq	%rbx, %rax
	movq	%r14, %rbx
	movq	%rax, %r14
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L734:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	41112(%r14), %rdi
	movq	(%rax), %rsi
	movq	%rax, %r15
	movq	7(%rsi), %r9
	testq	%rdi, %rdi
	je	.L632
.L736:
	movq	%r9, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L633:
	movq	(%r15), %rax
	movq	%r15, %rdx
	movq	%r12, %rdi
	addq	$8, %r13
	movslq	19(%rax), %rcx
	shrl	$9, %ecx
	andl	$7, %ecx
	call	_ZN2v88internal8JSObject11SetAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_12AccessorInfoEEENS0_18PropertyAttributesE@PLT
	cmpq	-96(%rbp), %r13
	je	.L733
.L635:
	movq	(%rbx), %rax
	movq	-1(%r13,%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L734
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L735
.L631:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	movq	41112(%r14), %rdi
	movq	7(%rsi), %r9
	testq	%rdi, %rdi
	jne	.L736
.L632:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L737
.L634:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r9, (%rsi)
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L728:
	testq	%r13, %r13
	je	.L724
	movq	12464(%rdi), %rax
	xorl	%r12d, %r12d
.L565:
	movq	39(%rax), %r8
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L572
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L573:
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	movq	%rbx, %rdi
	movl	%r13d, -112(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_124ProbeInstantiationsCacheEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L738
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory12CopyJSObjectENS0_6HandleINS0_8JSObjectEEE@PLT
.L711:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	movq	47(%rax), %rsi
	movq	23(%rdx), %rcx
	movq	7(%rcx), %rcx
	cmpq	%rcx, %rsi
	jne	.L725
	testb	$1, 59(%rax)
	je	.L569
.L725:
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L582:
	movq	%r13, %r15
	cmpq	%r12, %r13
	je	.L739
.L584:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L729:
	movq	12464(%rbx), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L726
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L740
.L580:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L603:
	cmpq	%rcx, %rax
	je	.L601
	movq	%rax, %rdx
	testq	%rdx, %rdx
	jne	.L606
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L733:
	movq	%r14, %rbx
.L611:
	movq	-88(%rbp), %rax
	movq	88(%rbx), %rdi
	movq	(%rax), %rdx
.L598:
	movq	31(%rdx), %rsi
	movq	%r12, %r13
	cmpq	%rdi, %rsi
	je	.L636
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L637
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L638:
	movq	15(%rsi), %rax
	movq	%r12, %r13
	shrq	$32, %rax
	je	.L636
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movl	27(%rax), %edx
	testl	%edx, %edx
	jle	.L640
	movq	%rbx, %rax
	movl	$0, -104(%rbp)
	movq	%r14, %rbx
	movl	$0, -96(%rbp)
	movq	%rax, %r14
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L742:
	movl	%r11d, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-120(%rbp), %r11d
	movq	%rax, %r15
.L642:
	movq	(%rbx), %rax
	leal	8(%r11), %ecx
	addl	$4, -104(%rbp)
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rax), %rsi
	leal	16(%r11), %ecx
	movslq	%ecx, %rcx
	testb	$1, %sil
	jne	.L644
	movq	%rsi, %rdx
	leaq	-1(%rcx,%rax), %rax
	shrq	$35, %rdx
	movq	%rdx, %r9
	andl	$7, %r9d
	btq	$32, %rsi
	movq	(%rax), %rsi
	movq	41112(%r14), %rdi
	jc	.L645
	testq	%rdi, %rdi
	je	.L646
	movl	%r9d, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-104(%rbp), %r9d
	movq	%rax, %rcx
.L647:
	movl	%r9d, %r8d
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118DefineDataPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE
	testq	%rax, %rax
	je	.L722
	movl	%r13d, -104(%rbp)
.L651:
	movq	-88(%rbp), %rax
	addl	$1, -96(%rbp)
	movl	-96(%rbp), %edx
	movq	(%rax), %rax
	cmpl	%edx, 27(%rax)
	jle	.L741
.L663:
	movl	-104(%rbp), %eax
	leal	3(%rax), %r13d
	movq	(%rbx), %rax
	leal	0(,%r13,8), %r11d
	movslq	%r11d, %rdx
	movq	-1(%rdx,%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L742
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L743
.L643:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L645:
	testq	%rdi, %rdi
	je	.L652
	movl	%r9d, -136(%rbp)
	movl	%r11d, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-120(%rbp), %r11d
	movl	-136(%rbp), %r9d
	movq	%rax, %r13
.L653:
	movq	(%rbx), %rax
	addl	$24, %r11d
	movslq	%r11d, %r11
	movq	-1(%r11,%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L655
	movl	%r9d, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-120(%rbp), %r9d
	movq	%rax, %r8
.L656:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122DefineAccessorPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEESA_NS0_18PropertyAttributesEb.constprop.0
	testq	%rax, %rax
	jne	.L651
.L722:
	movq	%r14, %rbx
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L636:
	cmpb	$0, -145(%rbp)
	jne	.L744
.L664:
	movq	-128(%rbp), %rax
	subl	$1, 41104(%rbx)
	movq	%rax, 41088(%rbx)
	movq	-144(%rbp), %rax
	cmpq	41096(%rbx), %rax
	je	.L665
	movq	%rax, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L665:
	testq	%r13, %r13
	je	.L727
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	testb	$1, 59(%rax)
	jne	.L745
.L667:
	cmpb	$0, -108(%rbp)
	je	.L746
.L668:
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L644:
	.cfi_restore_state
	addl	$24, %r11d
	movq	-1(%rcx,%rax), %r13
	movq	%r14, %rdi
	movslq	%r11d, %r11
	movq	-1(%r11,%rax), %rsi
	shrq	$35, %r13
	andl	$7, %r13d
	sarq	$32, %rsi
	call	_ZN2v88internal12_GLOBAL__N_112GetIntrinsicEPNS0_7IsolateENS_9IntrinsicE
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L659
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L660:
	movl	%r13d, %r8d
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118DefineDataPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE
	testq	%rax, %rax
	jne	.L651
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L655:
	movq	41088(%r14), %r8
	cmpq	41096(%r14), %r8
	je	.L747
.L657:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r8)
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L646:
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L748
.L648:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L652:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L749
.L654:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L659:
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L750
.L661:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L622:
	cmpq	%rax, %rdx
	je	.L619
	movq	%rax, %rsi
	testq	%rdi, %rdi
	jne	.L751
	.p2align 4,,10
	.p2align 3
.L624:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L752
.L626:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L621:
	xorl	%esi, %esi
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L614:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L753
.L616:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r8, (%rsi)
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L743:
	movq	%r14, %rdi
	movq	%rsi, -136(%rbp)
	movl	%r11d, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movl	-120(%rbp), %r11d
	movq	%rax, %r15
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L727:
	addq	$120, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L744:
	.cfi_restore_state
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118EnableAccessChecksEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L731:
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb@PLT
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L732:
	movq	-1(%rdx), %r13
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L595
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L596:
	movq	%rbx, %rdi
	leaq	.LC5(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	(%rax), %rax
	andb	$-33, 13(%rax)
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi@PLT
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L738:
	testq	%r12, %r12
	jne	.L571
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L637:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L754
.L639:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L746:
	leaq	.LC6(%rip), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L668
	movq	12464(%rbx), %rax
	movq	39(%rax), %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L669
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L670:
	movl	-112(%rbp), %edx
	movq	%r13, %r8
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_126CacheTemplateInstantiationEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEEiNS1_11CachingModeENS4_INS0_8JSObjectEEE
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory12CopyJSObjectENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	%rax, %r13
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L745:
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject17SetImmutableProtoENS0_6HandleIS1_EE@PLT
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L752:
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L737:
	movq	%r14, %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L735:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L585:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L755
.L587:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r8, (%rsi)
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L608:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L756
.L610:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	testq	%rsi, %rsi
	jne	.L609
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L589:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L757
.L591:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%r15, (%r12)
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L572:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L758
.L574:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r8, (%rsi)
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L753:
	movq	%rbx, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L741:
	movq	%r14, %rbx
.L640:
	movq	%r12, %r13
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L747:
	movq	%r14, %rdi
	movq	%rsi, -136(%rbp)
	movl	%r9d, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movl	-120(%rbp), %r9d
	movq	%rax, %r8
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L748:
	movq	%r14, %rdi
	movq	%rsi, -120(%rbp)
	movl	%r9d, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movl	-104(%rbp), %r9d
	movq	%rax, %rcx
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L749:
	movq	%r14, %rdi
	movq	%rsi, -160(%rbp)
	movl	%r9d, -136(%rbp)
	movl	%r11d, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movl	-136(%rbp), %r9d
	movl	-120(%rbp), %r11d
	movq	%rax, %r13
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L750:
	movq	%r14, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L595:
	movq	-128(%rbp), %rax
	movq	%rax, %rsi
	cmpq	41096(%rbx), %rax
	je	.L759
.L597:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L569:
	movq	12464(%rdi), %rax
	movq	39(%rax), %rcx
	movq	31(%rdx), %rdx
	movq	39(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L725
	movl	$0, -112(%rbp)
	movq	%r14, %r12
	testq	%r13, %r13
	je	.L571
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L669:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L760
.L671:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L730:
	subl	$1, 41104(%rbx)
	xorl	%eax, %eax
	movq	%r13, 41088(%rbx)
	cmpq	41096(%rbx), %r12
	je	.L711
	movq	%r12, 41096(%rbx)
	movq	%rbx, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rax
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L754:
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L739:
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L755:
	movq	%rbx, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L756:
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L757:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L758:
	movq	%rbx, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L759:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L597
.L740:
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L580
.L760:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L671
	.cfi_endproc
.LFE18057:
	.size	_ZN2v88internal12_GLOBAL__N_117InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEEb, .-_ZN2v88internal12_GLOBAL__N_117InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEEb
	.section	.text._ZN2v88internal12_GLOBAL__N_118DefineDataPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118DefineDataPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE, @function
_ZN2v88internal12_GLOBAL__N_118DefineDataPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE:
.LFB18039:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	testb	$1, %al
	jne	.L800
.L771:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L801
.L774:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L781
.L783:
	movl	$-1, %edx
	movq	%r9, %rdi
	movq	%rbx, %rsi
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-248(%rbp), %r9
	movq	%rax, %rdx
.L782:
	movabsq	$824633720832, %rsi
	movq	0(%r13), %rax
	movq	-1(%rax), %rcx
	movq	%rsi, -212(%rbp)
	movl	$0, -224(%rbp)
	movq	%r9, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L802
.L784:
	leaq	-224(%rbp), %r13
	movq	%r15, -192(%rbp)
	movq	%r13, %rdi
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L780:
	movl	$1, %r8d
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal6Object15AddDataPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS0_11StoreOriginE@PLT
	testb	%al, %al
	je	.L785
	movq	%r12, %rax
.L772:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L803
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L800:
	.cfi_restore_state
	movq	%rcx, %rdx
	movq	-1(%rax), %rcx
	cmpw	$88, 11(%rcx)
	je	.L764
	movq	-1(%rax), %rax
	cmpw	$92, 11(%rax)
	jne	.L771
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rdi, -248(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_117InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEEb
	movq	-248(%rbp), %r9
	movq	%rax, %r12
.L769:
	testq	%r12, %r12
	jne	.L771
.L785:
	xorl	%eax, %eax
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L801:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L775
	testb	$2, %al
	jne	.L774
.L775:
	leaq	-144(%rbp), %r8
	leaq	-228(%rbp), %rsi
	movq	%r9, -256(%rbp)
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-256(%rbp), %r9
	testb	%al, %al
	je	.L774
	movq	(%rbx), %rax
	movl	-228(%rbp), %r15d
	movq	-248(%rbp), %r8
	testb	$1, %al
	jne	.L777
.L779:
	movq	%r9, %rdi
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r8, -256(%rbp)
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-248(%rbp), %r9
	movq	-256(%rbp), %r8
.L778:
	movabsq	$824633720832, %rcx
	movq	%r8, %rdi
	movq	%r9, -120(%rbp)
	movl	$0, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%r15d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	leaq	-224(%rbp), %r13
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L781:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L783
	movq	%rbx, %rdx
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L777:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L779
	movq	%rbx, %rax
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L764:
	movq	12464(%rdi), %rax
	movq	39(%rax), %r12
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L766
	movq	%r12, %rsi
	movq	%rdx, -256(%rbp)
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-248(%rbp), %r9
	movq	-256(%rbp), %rdx
	movq	%rax, %rsi
.L767:
	movq	%r9, %rdi
	movq	%r13, %rcx
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	movq	-248(%rbp), %r9
	movq	%rax, %r12
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L802:
	movq	%r13, %rsi
	movq	%r9, %rdi
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-248(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L766:
	movq	41088(%r9), %rsi
	cmpq	41096(%r9), %rsi
	je	.L804
.L768:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r9)
	movq	%r12, (%rsi)
	jmp	.L767
.L804:
	movq	%r9, %rdi
	movq	%rdx, -256(%rbp)
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rdx
	movq	-248(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L768
.L803:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18039:
	.size	_ZN2v88internal12_GLOBAL__N_118DefineDataPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE, .-_ZN2v88internal12_GLOBAL__N_118DefineDataPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal10ApiNatives17InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ApiNatives17InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEE
	.type	_ZN2v88internal10ApiNatives17InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEE, @function
_ZN2v88internal10ApiNatives17InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEE:
.LFB18066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-72(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -80(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal11SaveContextC1EPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_117InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEEb
	movq	-80(%rbp), %rdi
	movq	%rax, %r12
	movq	96(%rdi), %rax
	cmpq	12480(%rdi), %rax
	jne	.L810
	movq	%rax, 12536(%rdi)
.L807:
	movq	%r15, %rdi
	call	_ZN2v88internal11SaveContextD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L811
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L810:
	.cfi_restore_state
	call	_ZN2v88internal7Isolate21ReportPendingMessagesEv@PLT
	jmp	.L807
.L811:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18066:
	.size	_ZN2v88internal10ApiNatives17InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEE, .-_ZN2v88internal10ApiNatives17InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE, @function
_GLOBAL__sub_I__ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE:
.LFB22212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22212:
	.size	_GLOBAL__sub_I__ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE, .-_GLOBAL__sub_I__ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
