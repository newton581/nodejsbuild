	.file	"read-only-serializer.cc"
	.text
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB6715:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE6715:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.rodata._ZN2v88internal18ReadOnlySerializerD2Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ReadOnlySerializer"
	.section	.text._ZN2v88internal18ReadOnlySerializerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ReadOnlySerializerD2Ev
	.type	_ZN2v88internal18ReadOnlySerializerD2Ev, @function
_ZN2v88internal18ReadOnlySerializerD2Ev:
.LFB18409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal18ReadOnlySerializerE(%rip), %rax
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10Serializer16OutputStatisticsEPKc@PLT
	leaq	16+_ZTVN2v88internal15RootsSerializerE(%rip), %rax
	movq	456(%r12), %rdi
	movq	%rax, (%r12)
	call	free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10SerializerD2Ev@PLT
	.cfi_endproc
.LFE18409:
	.size	_ZN2v88internal18ReadOnlySerializerD2Ev, .-_ZN2v88internal18ReadOnlySerializerD2Ev
	.globl	_ZN2v88internal18ReadOnlySerializerD1Ev
	.set	_ZN2v88internal18ReadOnlySerializerD1Ev,_ZN2v88internal18ReadOnlySerializerD2Ev
	.section	.text._ZN2v88internal18ReadOnlySerializer14MustBeDeferredENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ReadOnlySerializer14MustBeDeferredENS0_10HeapObjectE
	.type	_ZN2v88internal18ReadOnlySerializer14MustBeDeferredENS0_10HeapObjectE, @function
_ZN2v88internal18ReadOnlySerializer14MustBeDeferredENS0_10HeapObjectE:
.LFB18415:
	.cfi_startproc
	endbr64
	movq	368(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L9
.L6:
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	setne	%r8b
.L5:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%r8d, %r8d
	testb	$4, %al
	je	.L6
	jmp	.L5
	.cfi_endproc
.LFE18415:
	.size	_ZN2v88internal18ReadOnlySerializer14MustBeDeferredENS0_10HeapObjectE, .-_ZN2v88internal18ReadOnlySerializer14MustBeDeferredENS0_10HeapObjectE
	.section	.rodata._ZN2v88internal18ReadOnlySerializer15SerializeObjectENS0_10HeapObjectE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"ReadOnlyHeap::Contains(obj)"
.LC2:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal18ReadOnlySerializer15SerializeObjectENS0_10HeapObjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"obj.IsString() implies obj.IsInternalizedString()"
	.section	.rodata._ZN2v88internal18ReadOnlySerializer15SerializeObjectENS0_10HeapObjectE.str1.1
.LC4:
	.string	"bitset::test"
	.section	.rodata._ZN2v88internal18ReadOnlySerializer15SerializeObjectENS0_10HeapObjectE.str1.8
	.align 8
.LC5:
	.string	"%s: __position (which is %zu) >= _Nb (which is %zu)"
	.section	.text._ZN2v88internal18ReadOnlySerializer15SerializeObjectENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ReadOnlySerializer15SerializeObjectENS0_10HeapObjectE
	.type	_ZN2v88internal18ReadOnlySerializer15SerializeObjectENS0_10HeapObjectE, @function
_ZN2v88internal18ReadOnlySerializer15SerializeObjectENS0_10HeapObjectE:
.LFB18412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L29
	movq	-1(%rbx), %rax
	cmpw	$63, 11(%rax)
	jbe	.L30
.L12:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer18SerializeHotObjectENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L31
.L10:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L32
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	144(%r12), %rax
	movl	8(%rax), %ecx
	movq	(%rax), %rsi
	subl	$1, %ecx
	movl	%ecx, %eax
	andl	%ebx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	jne	.L16
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L18
.L16:
	cmpq	(%rdx), %rbx
	jne	.L33
	movl	8(%rdx), %ecx
	movzwl	%cx, %edx
	cmpw	$599, %cx
	ja	.L34
	movl	$1, %eax
	shrq	$6, %rdx
	salq	%cl, %rax
	testq	%rax, 368(%r12,%rdx,8)
	je	.L18
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer13SerializeRootENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L10
.L18:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer22SerializeBackReferenceENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L10
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal15RootsSerializer18CheckRehashabilityENS0_10HeapObjectE@PLT
	movq	%r12, -56(%rbp)
	leaq	16+_ZTVN2v88internal10Serializer16ObjectSerializerE(%rip), %rax
	addq	$80, %r12
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	movq	%rbx, -48(%rbp)
	movq	%r12, -40(%rbp)
	movl	$0, -32(%rbp)
	call	_ZN2v88internal10Serializer16ObjectSerializer9SerializeEv@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	movq	-1(%rbx), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L12
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L32:
	call	__stack_chk_fail@PLT
.L34:
	movl	$600, %ecx
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE18412:
	.size	_ZN2v88internal18ReadOnlySerializer15SerializeObjectENS0_10HeapObjectE, .-_ZN2v88internal18ReadOnlySerializer15SerializeObjectENS0_10HeapObjectE
	.section	.text._ZN2v88internal18ReadOnlySerializerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ReadOnlySerializerD0Ev
	.type	_ZN2v88internal18ReadOnlySerializerD0Ev, @function
_ZN2v88internal18ReadOnlySerializerD0Ev:
.LFB18411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal18ReadOnlySerializerE(%rip), %rax
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10Serializer16OutputStatisticsEPKc@PLT
	leaq	16+_ZTVN2v88internal15RootsSerializerE(%rip), %rax
	movq	456(%r12), %rdi
	movq	%rax, (%r12)
	call	free@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10SerializerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$496, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18411:
	.size	_ZN2v88internal18ReadOnlySerializerD0Ev, .-_ZN2v88internal18ReadOnlySerializerD0Ev
	.section	.text._ZN2v88internal18ReadOnlySerializerC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ReadOnlySerializerC2EPNS0_7IsolateE
	.type	_ZN2v88internal18ReadOnlySerializerC2EPNS0_7IsolateE, @function
_ZN2v88internal18ReadOnlySerializerC2EPNS0_7IsolateE:
.LFB18406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE@PLT
	leaq	16+_ZTVN2v88internal18ReadOnlySerializerE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18406:
	.size	_ZN2v88internal18ReadOnlySerializerC2EPNS0_7IsolateE, .-_ZN2v88internal18ReadOnlySerializerC2EPNS0_7IsolateE
	.globl	_ZN2v88internal18ReadOnlySerializerC1EPNS0_7IsolateE
	.set	_ZN2v88internal18ReadOnlySerializerC1EPNS0_7IsolateE,_ZN2v88internal18ReadOnlySerializerC2EPNS0_7IsolateE
	.section	.rodata._ZN2v88internal18ReadOnlySerializer22SerializeReadOnlyRootsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"(isolate()->thread_manager()->FirstThreadStateInUse()) == nullptr"
	.align 8
.LC7:
	.string	"isolate()->handle_scope_implementer()->blocks()->empty()"
	.section	.text._ZN2v88internal18ReadOnlySerializer22SerializeReadOnlyRootsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ReadOnlySerializer22SerializeReadOnlyRootsEv
	.type	_ZN2v88internal18ReadOnlySerializer22SerializeReadOnlyRootsEv, @function
_ZN2v88internal18ReadOnlySerializer22SerializeReadOnlyRootsEv:
.LFB18413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	104(%rdi), %rax
	movq	41168(%rax), %rdi
	call	_ZN2v88internal13ThreadManager21FirstThreadStateInUseEv@PLT
	testq	%rax, %rax
	jne	.L44
	movq	104(%r12), %rax
	movq	41120(%rax), %rdx
	cmpq	$0, 24(%rdx)
	jne	.L45
	addq	$56, %rax
	leaq	-32(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal13ReadOnlyRoots7IterateEPNS0_11RootVisitorE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	.LC7(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L46:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18413:
	.size	_ZN2v88internal18ReadOnlySerializer22SerializeReadOnlyRootsEv, .-_ZN2v88internal18ReadOnlySerializer22SerializeReadOnlyRootsEv
	.section	.text._ZN2v88internal18ReadOnlySerializer21FinalizeSerializationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ReadOnlySerializer21FinalizeSerializationEv
	.type	_ZN2v88internal18ReadOnlySerializer21FinalizeSerializationEv, @function
_ZN2v88internal18ReadOnlySerializer21FinalizeSerializationEv:
.LFB18414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rcx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	104(%rdi), %rax
	movq	88(%rax), %rax
	movq	%rax, -32(%rbp)
	movq	(%rdi), %rax
	movq	24(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L48
	leaq	-24(%rbp), %r8
	xorl	%edx, %edx
	movl	$20, %esi
	call	*16(%rax)
.L49:
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer24SerializeDeferredObjectsEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer3PadEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$20, %esi
	call	*%r8
	jmp	.L49
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18414:
	.size	_ZN2v88internal18ReadOnlySerializer21FinalizeSerializationEv, .-_ZN2v88internal18ReadOnlySerializer21FinalizeSerializationEv
	.section	.rodata._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC8:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.type	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, @function
_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_:
.LFB20690:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L67
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L62
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L68
.L64:
	movq	%rcx, %r14
.L55:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L61:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L69
	testq	%rsi, %rsi
	jg	.L57
	testq	%r15, %r15
	jne	.L60
.L58:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L57
.L60:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L68:
	testq	%r14, %r14
	js	.L64
	jne	.L55
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L58
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$1, %r14d
	jmp	.L55
.L67:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20690:
	.size	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, .-_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.section	.rodata._ZN2v88internal18ReadOnlySerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"read_only_object_cache_index"
	.section	.text._ZN2v88internal18ReadOnlySerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ReadOnlySerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE
	.type	_ZN2v88internal18ReadOnlySerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE, @function
_ZN2v88internal18ReadOnlySerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE:
.LFB18416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE@PLT
	movl	%eax, %r13d
	testb	%al, %al
	jne	.L79
.L70:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15RootsSerializer22SerializeInObjectCacheENS0_10HeapObjectE@PLT
	movb	$19, -41(%rbp)
	movq	8(%r12), %rsi
	movl	%eax, %r14d
	cmpq	16(%r12), %rsi
	je	.L72
	movb	$19, (%rsi)
	addq	$1, 8(%r12)
.L73:
	movslq	%r14d, %rsi
	leaq	.LC9(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L72:
	leaq	-41(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L73
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18416:
	.size	_ZN2v88internal18ReadOnlySerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE, .-_ZN2v88internal18ReadOnlySerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal18ReadOnlySerializerC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal18ReadOnlySerializerC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal18ReadOnlySerializerC2EPNS0_7IsolateE:
.LFB22591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22591:
	.size	_GLOBAL__sub_I__ZN2v88internal18ReadOnlySerializerC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal18ReadOnlySerializerC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal18ReadOnlySerializerC2EPNS0_7IsolateE
	.weak	_ZTVN2v88internal18ReadOnlySerializerE
	.section	.data.rel.ro._ZTVN2v88internal18ReadOnlySerializerE,"awG",@progbits,_ZTVN2v88internal18ReadOnlySerializerE,comdat
	.align 8
	.type	_ZTVN2v88internal18ReadOnlySerializerE, @object
	.size	_ZTVN2v88internal18ReadOnlySerializerE, 72
_ZTVN2v88internal18ReadOnlySerializerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal18ReadOnlySerializerD1Ev
	.quad	_ZN2v88internal18ReadOnlySerializerD0Ev
	.quad	_ZN2v88internal15RootsSerializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal15RootsSerializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.quad	_ZN2v88internal18ReadOnlySerializer15SerializeObjectENS0_10HeapObjectE
	.quad	_ZN2v88internal18ReadOnlySerializer14MustBeDeferredENS0_10HeapObjectE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
