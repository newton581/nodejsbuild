	.file	"wasm-module.cc"
	.text
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_117ToValueTypeStringEPNS0_7IsolateENS1_9ValueTypeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"i32"
.LC1:
	.string	"i64"
.LC2:
	.string	"f32"
.LC3:
	.string	"f64"
.LC4:
	.string	"anyref"
.LC5:
	.string	"anyfunc"
.LC6:
	.string	"exnref"
.LC7:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_117ToValueTypeStringEPNS0_7IsolateENS1_9ValueTypeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_117ToValueTypeStringEPNS0_7IsolateENS1_9ValueTypeE, @function
_ZN2v88internal4wasm12_GLOBAL__N_117ToValueTypeStringEPNS0_7IsolateENS1_9ValueTypeE:
.LFB20571:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rcx
	movq	%rcx, -8(%rbp)
	xorl	%ecx, %ecx
	cmpb	$9, %sil
	ja	.L2
	leaq	.L4(%rip), %rdx
	movzbl	%sil, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_117ToValueTypeStringEPNS0_7IsolateENS1_9ValueTypeE,"a",@progbits
	.align 4
	.align 4
.L4:
	.long	.L2-.L4
	.long	.L10-.L4
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L2-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L2-.L4
	.long	.L3-.L4
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_117ToValueTypeStringEPNS0_7IsolateENS1_9ValueTypeE
	.p2align 4,,10
	.p2align 3
.L3:
	leaq	.LC6(%rip), %rax
	leaq	-32(%rbp), %rsi
	movq	$6, -24(%rbp)
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L15
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	leaq	.LC0(%rip), %rax
	leaq	-32(%rbp), %rsi
	movq	$3, -24(%rbp)
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	.LC1(%rip), %rax
	leaq	-32(%rbp), %rsi
	movq	$3, -24(%rbp)
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC2(%rip), %rax
	leaq	-32(%rbp), %rsi
	movq	$3, -24(%rbp)
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	.LC3(%rip), %rax
	leaq	-32(%rbp), %rsi
	movq	$3, -24(%rbp)
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	.LC4(%rip), %rax
	leaq	-32(%rbp), %rsi
	movq	$6, -24(%rbp)
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	.LC5(%rip), %rax
	leaq	-32(%rbp), %rsi
	movq	$7, -24(%rbp)
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	jmp	.L11
.L2:
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L15:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20571:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_117ToValueTypeStringEPNS0_7IsolateENS1_9ValueTypeE, .-_ZN2v88internal4wasm12_GLOBAL__N_117ToValueTypeStringEPNS0_7IsolateENS1_9ValueTypeE
	.section	.text._ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE,"axG",@progbits,_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE
	.type	_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE, @function
_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE:
.LFB19857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	(%rsi), %r12
	movq	%r13, %rax
	movslq	11(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-1(%r12), %rdx
	cmpq	%rdx, -37104(%rax)
	je	.L17
	movq	-1(%r13), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	cmpl	$3, %edx
	je	.L24
	movq	-37496(%rax), %r8
	testl	%esi, %esi
	je	.L24
	cmpb	$5, %dl
	leaq	15(%r12), %rax
	setbe	%cl
	subl	$1, %esi
	leaq	23(%r12,%rsi,8), %r9
	andl	%edx, %ecx
	movzbl	%dl, %esi
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L20:
	andl	$1, %edi
	je	.L21
	testb	%cl, %cl
	jne	.L38
	movl	$2, %esi
.L21:
	addq	$8, %rax
	cmpq	%rax, %r9
	je	.L50
.L23:
	movq	(%rax), %rdi
	cmpq	%rdi, %r8
	jne	.L20
	movl	$1, %ecx
	testb	%sil, %sil
	je	.L34
	cmpb	$4, %sil
	je	.L35
	cmpb	$2, %sil
	je	.L36
	cmpb	$6, %sil
	movl	$7, %edi
	cmove	%edi, %esi
	addq	$8, %rax
	cmpq	%rax, %r9
	jne	.L23
	.p2align 4,,10
	.p2align 3
.L50:
	cmpb	%sil, %dl
	jne	.L48
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r12, 15(%r13)
	leaq	15(%r13), %rsi
	testb	$1, %r12b
	je	.L33
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L51
	testb	$24, %al
	je	.L33
.L53:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L52
.L33:
	movq	(%r14), %rax
	movq	(%rbx), %rdx
	movslq	11(%rax), %rax
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L53
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$1, %esi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L17:
	movq	-1(%r13), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$1, %eax
	je	.L49
	movq	-1(%r13), %rax
	testb	$-8, 14(%rax)
	jne	.L24
	testl	%esi, %esi
	je	.L27
	subq	$1, %r12
	addl	$2, %esi
	movl	$2, %ecx
	movabsq	$-2251799814209537, %rdi
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L28:
	addl	$1, %ecx
	cmpl	%ecx, %esi
	je	.L27
.L29:
	leal	0(,%rcx,8), %eax
	cltq
	cmpq	%rdi, (%rax,%r12)
	jne	.L28
.L49:
	movl	$5, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	(%rbx), %r13
	movq	(%r14), %r12
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$4, %esi
.L48:
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	(%rbx), %r13
	movq	(%r14), %r12
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$5, %esi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$3, %esi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$3, %esi
	jmp	.L48
	.cfi_endproc
.LFE19857:
	.size	_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE, .-_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE
	.section	.text._ZNK2v88internal4wasm10WasmModule18LookupFunctionNameERKNS1_15ModuleWireBytesEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm10WasmModule18LookupFunctionNameERKNS1_15ModuleWireBytesEj
	.type	_ZNK2v88internal4wasm10WasmModule18LookupFunctionNameERKNS1_15ModuleWireBytesEj, @function
_ZNK2v88internal4wasm10WasmModule18LookupFunctionNameERKNS1_15ModuleWireBytesEj:
.LFB20403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	movq	400(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L82
.L55:
	movq	8(%rcx), %rsi
	movl	%ebx, %eax
	xorl	%edx, %edx
	divq	%rsi
	movq	(%rcx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L61
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L83:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L61
	movl	8(%rcx), %eax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L61
.L63:
	cmpl	%ebx, %edi
	jne	.L83
	movq	12(%rcx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	%rdi, %r12
	movl	$56, %edi
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	400(%r12), %r14
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	movq	$0, 48(%rax)
	leaq	48(%rax), %rax
	movups	%xmm0, -16(%rax)
	movq	%rdx, 400(%r12)
	movq	%rax, (%rdx)
	movq	$1, 8(%rdx)
	movq	$0, 16(%rdx)
	movq	$0, 24(%rdx)
	movl	$0x3f800000, 32(%rdx)
	movq	$0, 40(%rdx)
	testq	%r14, %r14
	je	.L56
	movq	16(%r14), %r15
	testq	%r15, %r15
	je	.L60
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%r15, %rdi
	movq	(%r15), %r15
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L57
.L60:
	movq	8(%r14), %rax
	movq	(%r14), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r14), %rdi
	leaq	48(%r14), %rax
	movq	$0, 24(%r14)
	movq	$0, 16(%r14)
	cmpq	%rax, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movl	$56, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	movq	400(%r12), %rdx
.L56:
	movq	0(%r13), %rdi
	movq	8(%r13), %rsi
	addq	%rdi, %rsi
	call	_ZN2v88internal4wasm19DecodeFunctionNamesEPKhS3_PSt13unordered_mapIjNS1_12WireBytesRefESt4hashIjESt8equal_toIjESaISt4pairIKjS5_EEE@PLT
	movq	400(%r12), %rcx
	jmp	.L55
	.cfi_endproc
.LFE20403:
	.size	_ZNK2v88internal4wasm10WasmModule18LookupFunctionNameERKNS1_15ModuleWireBytesEj, .-_ZNK2v88internal4wasm10WasmModule18LookupFunctionNameERKNS1_15ModuleWireBytesEj
	.section	.text._ZN2v88internal4wasm20MaxNumExportWrappersEPKNS1_10WasmModuleE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm20MaxNumExportWrappersEPKNS1_10WasmModuleE
	.type	_ZN2v88internal4wasm20MaxNumExportWrappersEPKNS1_10WasmModuleE, @function
_ZN2v88internal4wasm20MaxNumExportWrappersEPKNS1_10WasmModuleE:
.LFB20423:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	addl	%eax, %eax
	ret
	.cfi_endproc
.LFE20423:
	.size	_ZN2v88internal4wasm20MaxNumExportWrappersEPKNS1_10WasmModuleE, .-_ZN2v88internal4wasm20MaxNumExportWrappersEPKNS1_10WasmModuleE
	.section	.rodata._ZN2v88internal4wasm21GetExportWrapperIndexEPKNS1_10WasmModuleEPKNS0_9SignatureINS1_9ValueTypeEEEb.str1.1,"aMS",@progbits,1
.LC9:
	.string	"result >= 0"
.LC10:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal4wasm21GetExportWrapperIndexEPKNS1_10WasmModuleEPKNS0_9SignatureINS1_9ValueTypeEEEb,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm21GetExportWrapperIndexEPKNS1_10WasmModuleEPKNS0_9SignatureINS1_9ValueTypeEEEb
	.type	_ZN2v88internal4wasm21GetExportWrapperIndexEPKNS1_10WasmModuleEPKNS0_9SignatureINS1_9ValueTypeEEEb, @function
_ZN2v88internal4wasm21GetExportWrapperIndexEPKNS1_10WasmModuleEPKNS0_9SignatureINS1_9ValueTypeEEEb:
.LFB20424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$328, %rdi
	call	_ZNK2v88internal4wasm12SignatureMap4FindERKNS0_9SignatureINS1_9ValueTypeEEE@PLT
	testl	%eax, %eax
	js	.L90
	xorl	%edx, %edx
	testb	%r12b, %r12b
	je	.L87
	movl	360(%rbx), %edx
.L87:
	popq	%rbx
	addl	%edx, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20424:
	.size	_ZN2v88internal4wasm21GetExportWrapperIndexEPKNS1_10WasmModuleEPKNS0_9SignatureINS1_9ValueTypeEEEb, .-_ZN2v88internal4wasm21GetExportWrapperIndexEPKNS1_10WasmModuleEPKNS0_9SignatureINS1_9ValueTypeEEEb
	.section	.rodata._ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullENS1_12WireBytesRefE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"BoundsCheck(ref.offset(), ref.length())"
	.section	.text._ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullENS1_12WireBytesRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullENS1_12WireBytesRefE
	.type	_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullENS1_12WireBytesRefE, @function
_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullENS1_12WireBytesRefE:
.LFB20445:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L92
	xorl	%eax, %eax
	xorl	%edx, %edx
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	movq	8(%rdi), %rax
	cmpl	%eax, %esi
	ja	.L94
	movq	%rsi, %rdx
	subl	%esi, %eax
	shrq	$32, %rdx
	cmpl	%edx, %eax
	jb	.L94
	movl	%esi, %ecx
	movq	(%rdi), %rax
	addl	%esi, %edx
	subq	%rcx, %rdx
	addq	%rcx, %rax
	movslq	%edx, %rdx
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20445:
	.size	_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullENS1_12WireBytesRefE, .-_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullENS1_12WireBytesRefE
	.section	.text._ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullEPKNS1_12WasmFunctionEPKNS1_10WasmModuleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullEPKNS1_12WasmFunctionEPKNS1_10WasmModuleE
	.type	_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullEPKNS1_12WasmFunctionEPKNS1_10WasmModuleE, @function
_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullEPKNS1_12WasmFunctionEPKNS1_10WasmModuleE:
.LFB20446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$8, %rsp
	movl	8(%rsi), %edx
	movq	%rbx, %rsi
	call	_ZNK2v88internal4wasm10WasmModule18LookupFunctionNameERKNS1_15ModuleWireBytesEj
	testl	%eax, %eax
	je	.L100
	movq	8(%rbx), %rdx
	cmpl	%eax, %edx
	jb	.L99
	movq	%rax, %rsi
	subl	%eax, %edx
	shrq	$32, %rsi
	cmpl	%esi, %edx
	jb	.L99
	movq	(%rbx), %r8
	movl	%eax, %ecx
	leal	(%rsi,%rax), %edx
	addq	$8, %rsp
	subq	%rcx, %rdx
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	%rcx, %r8
	movslq	%edx, %rdx
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20446:
	.size	_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullEPKNS1_12WasmFunctionEPKNS1_10WasmModuleE, .-_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullEPKNS1_12WasmFunctionEPKNS1_10WasmModuleE
	.section	.rodata._ZN2v88internal4wasmlsERSoRKNS1_16WasmFunctionNameE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"#"
.LC13:
	.string	":"
.LC14:
	.string	"?"
	.section	.text._ZN2v88internal4wasmlsERSoRKNS1_16WasmFunctionNameE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasmlsERSoRKNS1_16WasmFunctionNameE
	.type	_ZN2v88internal4wasmlsERSoRKNS1_16WasmFunctionNameE, @function
_ZN2v88internal4wasmlsERSoRKNS1_16WasmFunctionNameE:
.LFB20447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	8(%rax), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	cmpq	$0, 16(%rbx)
	je	.L103
	cmpq	$0, 8(%rbx)
	je	.L104
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movslq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo5writeEPKcl@PLT
.L104:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20447:
	.size	_ZN2v88internal4wasmlsERSoRKNS1_16WasmFunctionNameE, .-_ZN2v88internal4wasmlsERSoRKNS1_16WasmFunctionNameE
	.section	.text._ZN2v88internal4wasm10WasmModuleC2ESt10unique_ptrINS0_4ZoneESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmModuleC2ESt10unique_ptrINS0_4ZoneESt14default_deleteIS4_EE
	.type	_ZN2v88internal4wasm10WasmModuleC2ESt10unique_ptrINS0_4ZoneESt14default_deleteIS4_EE, @function
_ZN2v88internal4wasm10WasmModuleC2ESt10unique_ptrINS0_4ZoneESt14default_deleteIS4_EE:
.LFB20568:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	$0, (%rsi)
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movabsq	$-4294967296, %rax
	movq	%rax, 16(%rdi)
	leaq	384(%rdi), %rax
	movq	%rax, 336(%rdi)
	leaq	424(%rdi), %rax
	movq	$0, 40(%rdi)
	movq	$0, 80(%rdi)
	movb	$0, 328(%rdi)
	movq	$1, 344(%rdi)
	movq	$0, 352(%rdi)
	movq	$0, 360(%rdi)
	movl	$0x3f800000, 368(%rdi)
	movq	$0, 376(%rdi)
	movups	%xmm0, 24(%rdi)
	movups	%xmm1, 48(%rdi)
	movups	%xmm1, 64(%rdi)
	movups	%xmm0, 88(%rdi)
	movups	%xmm0, 104(%rdi)
	movups	%xmm0, 120(%rdi)
	movups	%xmm0, 136(%rdi)
	movups	%xmm0, 152(%rdi)
	movups	%xmm0, 168(%rdi)
	movups	%xmm0, 184(%rdi)
	movups	%xmm0, 200(%rdi)
	movups	%xmm0, 216(%rdi)
	movups	%xmm0, 232(%rdi)
	movups	%xmm0, 248(%rdi)
	movups	%xmm0, 264(%rdi)
	movups	%xmm0, 280(%rdi)
	movups	%xmm0, 296(%rdi)
	movups	%xmm0, 312(%rdi)
	movq	$0, 384(%rdi)
	movb	$0, 392(%rdi)
	movq	$0, 400(%rdi)
	movq	%rax, 408(%rdi)
	movq	$0, 416(%rdi)
	movb	$0, 424(%rdi)
	ret
	.cfi_endproc
.LFE20568:
	.size	_ZN2v88internal4wasm10WasmModuleC2ESt10unique_ptrINS0_4ZoneESt14default_deleteIS4_EE, .-_ZN2v88internal4wasm10WasmModuleC2ESt10unique_ptrINS0_4ZoneESt14default_deleteIS4_EE
	.globl	_ZN2v88internal4wasm10WasmModuleC1ESt10unique_ptrINS0_4ZoneESt14default_deleteIS4_EE
	.set	_ZN2v88internal4wasm10WasmModuleC1ESt10unique_ptrINS0_4ZoneESt14default_deleteIS4_EE,_ZN2v88internal4wasm10WasmModuleC2ESt10unique_ptrINS0_4ZoneESt14default_deleteIS4_EE
	.section	.text._ZN2v88internal4wasm20IsWasmCodegenAllowedEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm20IsWasmCodegenAllowedEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE
	.type	_ZN2v88internal4wasm20IsWasmCodegenAllowedEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE, @function
_ZN2v88internal4wasm20IsWasmCodegenAllowedEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE:
.LFB20570:
	.cfi_startproc
	endbr64
	movq	41656(%rdi), %rax
	movq	%rsi, %r8
	testq	%rax, %rax
	je	.L108
.L110:
	leaq	128(%rdi), %rsi
	movq	%r8, %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L108:
	movq	41640(%rdi), %rax
	testq	%rax, %rax
	jne	.L110
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE20570:
	.size	_ZN2v88internal4wasm20IsWasmCodegenAllowedEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE, .-_ZN2v88internal4wasm20IsWasmCodegenAllowedEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE
	.section	.rodata._ZN2v88internal4wasm18GetTypeForFunctionEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"parameters"
.LC16:
	.string	"results"
	.section	.text._ZN2v88internal4wasm18GetTypeForFunctionEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm18GetTypeForFunctionEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEE
	.type	_ZN2v88internal4wasm18GetTypeForFunctionEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEE, @function
_ZN2v88internal4wasm18GetTypeForFunctionEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEE:
.LFB20572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%r15), %rsi
	movq	16(%r15), %r13
	movq	%rax, %r14
	movq	8(%r15), %rax
	leaq	0(%r13,%rsi), %rcx
	addq	%rsi, %rax
	addq	%r13, %rax
	movq	%rax, -88(%rbp)
	cmpq	%rax, %rcx
	je	.L112
	movq	%rcx, %r13
	movl	$16, %ebx
	.p2align 4,,10
	.p2align 3
.L116:
	movzbl	0(%r13), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_117ToValueTypeStringEPNS0_7IsolateENS1_9ValueTypeE
	movq	(%r14), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%rbx), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L128
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -96(%rbp)
	testl	$262144, %eax
	je	.L114
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %r8
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%r8), %rax
.L114:
	testb	$24, %al
	je	.L128
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L128
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L128:
	addq	$1, %r13
	addq	$8, %rbx
	cmpq	%r13, -88(%rbp)
	jne	.L116
	movq	(%r15), %rsi
.L112:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$16, %ebx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	16(%r15), %rcx
	movq	%rax, %r13
	movq	(%r15), %rax
	movq	%rcx, %r15
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	cmpq	%rax, %rcx
	je	.L123
	.p2align 4,,10
	.p2align 3
.L124:
	movzbl	(%r15), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_117ToValueTypeStringEPNS0_7IsolateENS1_9ValueTypeE
	movq	0(%r13), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%rbx), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L127
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -96(%rbp)
	testl	$262144, %eax
	je	.L121
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %r8
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%r8), %rax
.L121:
	testb	$24, %al
	je	.L127
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L127
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	addq	$1, %r15
	addq	$8, %rbx
	cmpq	%r15, -88(%rbp)
	jne	.L124
.L123:
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L146
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L125:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$3, %edx
	movq	%rax, %r15
	movq	(%r14), %rax
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%rax, -88(%rbp)
	movq	0(%r13), %rax
	movq	%r12, %rdi
	leaq	-80(%rbp), %r13
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$10, -72(%rbp)
	movq	%rax, %r14
	leaq	.LC15(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$7, -72(%rbp)
	movq	%rax, %rbx
	leaq	.LC16(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	movq	%rax, %r13
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r9, %rcx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L147
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L148
.L126:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L125
.L148:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L126
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20572:
	.size	_ZN2v88internal4wasm18GetTypeForFunctionEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEE, .-_ZN2v88internal4wasm18GetTypeForFunctionEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEE
	.section	.rodata._ZN2v88internal4wasm16GetTypeForGlobalEPNS0_7IsolateEbNS1_9ValueTypeE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"mutable"
.LC18:
	.string	"value"
	.section	.text._ZN2v88internal4wasm16GetTypeForGlobalEPNS0_7IsolateEbNS1_9ValueTypeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm16GetTypeForGlobalEPNS0_7IsolateEbNS1_9ValueTypeE
	.type	_ZN2v88internal4wasm16GetTypeForGlobalEPNS0_7IsolateEbNS1_9ValueTypeE, @function
_ZN2v88internal4wasm16GetTypeForGlobalEPNS0_7IsolateEbNS1_9ValueTypeE:
.LFB20573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r14
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L150
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L151:
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-80(%rbp), %r15
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	$7, -72(%rbp)
	movq	%rax, %r14
	leaq	.LC17(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	$5, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC18(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movzbl	%r13b, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	-88(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movzbl	%bl, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_117ToValueTypeStringEPNS0_7IsolateENS1_9ValueTypeE
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L155
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L156
.L152:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L152
.L155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20573:
	.size	_ZN2v88internal4wasm16GetTypeForGlobalEPNS0_7IsolateEbNS1_9ValueTypeE, .-_ZN2v88internal4wasm16GetTypeForGlobalEPNS0_7IsolateEbNS1_9ValueTypeE
	.section	.rodata._ZN2v88internal4wasm16GetTypeForMemoryEPNS0_7IsolateEjNS_4base8OptionalIjEE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"minimum"
.LC20:
	.string	"maximum"
	.section	.text._ZN2v88internal4wasm16GetTypeForMemoryEPNS0_7IsolateEjNS_4base8OptionalIjEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm16GetTypeForMemoryEPNS0_7IsolateEjNS_4base8OptionalIjEE
	.type	_ZN2v88internal4wasm16GetTypeForMemoryEPNS0_7IsolateEjNS_4base8OptionalIjEE, @function
_ZN2v88internal4wasm16GetTypeForMemoryEPNS0_7IsolateEjNS_4base8OptionalIjEE:
.LFB20574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L158
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L159:
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-80(%rbp), %r15
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	$7, -72(%rbp)
	movq	%rax, %r13
	leaq	.LC19(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	$7, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC20(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	testb	%bl, %bl
	jne	.L164
.L161:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L166
.L160:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L164:
	shrq	$32, %rbx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L160
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20574:
	.size	_ZN2v88internal4wasm16GetTypeForMemoryEPNS0_7IsolateEjNS_4base8OptionalIjEE, .-_ZN2v88internal4wasm16GetTypeForMemoryEPNS0_7IsolateEjNS_4base8OptionalIjEE
	.section	.rodata._ZN2v88internal4wasm15GetTypeForTableEPNS0_7IsolateENS1_9ValueTypeEjNS_4base8OptionalIjEE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"element"
	.section	.text._ZN2v88internal4wasm15GetTypeForTableEPNS0_7IsolateENS1_9ValueTypeEjNS_4base8OptionalIjEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm15GetTypeForTableEPNS0_7IsolateENS1_9ValueTypeEjNS_4base8OptionalIjEE
	.type	_ZN2v88internal4wasm15GetTypeForTableEPNS0_7IsolateENS1_9ValueTypeEjNS_4base8OptionalIjEE, @function
_ZN2v88internal4wasm15GetTypeForTableEPNS0_7IsolateENS1_9ValueTypeEjNS_4base8OptionalIjEE:
.LFB20576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$72, %rsp
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$7, %sil
	je	.L177
	leaq	.LC4(%rip), %rax
	movq	$6, -72(%rbp)
	movq	%rax, -80(%rbp)
.L176:
	leaq	-80(%rbp), %r13
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%rax, %r15
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r12
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L170
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L171:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$7, -72(%rbp)
	movq	%rax, %r12
	leaq	.LC21(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$7, -72(%rbp)
	movq	%rax, -104(%rbp)
	leaq	.LC19(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$7, -72(%rbp)
	movq	%rax, -96(%rbp)
	leaq	.LC20(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-104(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	xorl	%edx, %edx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	-96(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	cmpb	$0, -88(%rbp)
	jne	.L178
.L173:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L180
.L172:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r12, (%rsi)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L177:
	leaq	.LC5(%rip), %rax
	movq	$7, -72(%rbp)
	movq	%rax, -80(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L178:
	movq	-88(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	shrq	$32, %rsi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L180:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L172
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20576:
	.size	_ZN2v88internal4wasm15GetTypeForTableEPNS0_7IsolateENS1_9ValueTypeEjNS_4base8OptionalIjEE, .-_ZN2v88internal4wasm15GetTypeForTableEPNS0_7IsolateENS1_9ValueTypeEjNS_4base8OptionalIjEE
	.section	.rodata._ZN2v88internal4wasm10GetImportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"module"
.LC23:
	.string	"name"
.LC24:
	.string	"kind"
.LC25:
	.string	"type"
.LC26:
	.string	"function"
.LC27:
	.string	"table"
.LC28:
	.string	"memory"
.LC29:
	.string	"global"
.LC30:
	.string	"exception"
.LC31:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal4wasm10GetImportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm10GetImportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE
	.type	_ZN2v88internal4wasm10GetImportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE, @function
_ZN2v88internal4wasm10GetImportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE:
.LFB20577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	%rsi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$6, -72(%rbp)
	movzbl	%dl, %r13d
	movb	%r13b, -85(%rbp)
	movq	%rax, -93(%rbp)
	movzbl	%dh, %eax
	movb	%al, -84(%rbp)
	movq	%rdx, %rax
	shrq	$16, %rax
	movb	%al, -83(%rbp)
	movq	%rdx, %rax
	shrq	$32, %rdx
	shrq	$24, %rax
	movb	%dl, -81(%rbp)
	movb	%al, -82(%rbp)
	leaq	.LC22(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$4, -72(%rbp)
	movq	%rax, -160(%rbp)
	leaq	.LC23(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$4, -72(%rbp)
	movq	%rax, -176(%rbp)
	leaq	.LC24(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$4, -72(%rbp)
	movq	%rax, -184(%rbp)
	leaq	.LC25(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$8, -72(%rbp)
	movq	%rax, -208(%rbp)
	leaq	.LC26(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$5, -72(%rbp)
	movq	%rax, -240(%rbp)
	leaq	.LC27(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$6, -72(%rbp)
	movq	%rax, -248(%rbp)
	leaq	.LC28(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$6, -72(%rbp)
	movq	%rax, -232(%rbp)
	leaq	.LC29(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$9, -72(%rbp)
	movq	%rax, -256(%rbp)
	leaq	.LC30(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, -264(%rbp)
	movq	(%r14), %rax
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movq	216(%rax), %r12
	subq	208(%rax), %r12
	movq	%rax, -144(%rbp)
	movabsq	$-6148914691236517205, %rax
	sarq	$3, %r12
	imulq	%rax, %r12
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %r15
	movq	%rax, -272(%rbp)
	movl	%r12d, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE
	movq	(%r15), %rax
	movq	%r12, %rdx
	salq	$32, %rdx
	movq	%rdx, 23(%rax)
	movq	12464(%rbx), %rax
	movq	39(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L182
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L183:
	movq	879(%rsi), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L185
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -152(%rbp)
.L186:
	testl	%r12d, %r12d
	jle	.L208
	leal	-1(%r12), %eax
	movb	%r13b, -193(%rbp)
	xorl	%r15d, %r15d
	leaq	8(,%rax,8), %rax
	movq	%rax, -192(%rbp)
.L209:
	movq	-144(%rbp), %rax
	xorl	%edx, %edx
	leaq	(%r15,%r15,2), %r13
	movq	%rbx, %rdi
	movq	-152(%rbp), %rsi
	addq	208(%rax), %r13
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movzbl	16(%r13), %edx
	movq	%rax, %r12
	cmpb	$4, %dl
	ja	.L189
	leaq	.L191(%rip), %rcx
	movzbl	%dl, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10GetImportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE,"a",@progbits
	.align 4
	.align 4
.L191:
	.long	.L195-.L191
	.long	.L194-.L191
	.long	.L193-.L191
	.long	.L192-.L191
	.long	.L212-.L191
	.section	.text._ZN2v88internal4wasm10GetImportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE
	.p2align 4,,10
	.p2align 3
.L212:
	movq	-264(%rbp), %rax
	xorl	%r14d, %r14d
	movq	%rax, -136(%rbp)
.L190:
	movq	0(%r13), %rdx
	movq	-120(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6HandleIS1_EENS0_4wasm12WireBytesRefE@PLT
	movq	8(%r13), %rdx
	movq	-120(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6HandleIS1_EENS0_4wasm12WireBytesRefE@PLT
	movq	-128(%rbp), %rcx
	movq	%rax, %r13
	testq	%rcx, %rcx
	je	.L203
	movq	-160(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	testq	%r13, %r13
	je	.L203
	movq	-176(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	-136(%rbp), %rcx
	movq	-184(%rbp), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	testq	%r14, %r14
	je	.L204
	movq	-208(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L204:
	movq	-168(%rbp), %rax
	movq	(%r12), %r12
	movq	(%rax), %r13
	leaq	15(%r13,%r15), %r14
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L210
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -128(%rbp)
	testl	$262144, %eax
	je	.L206
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rcx
	movq	8(%rcx), %rax
.L206:
	testb	$24, %al
	je	.L210
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L210
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L210:
	addq	$8, %r15
	cmpq	%r15, -192(%rbp)
	jne	.L209
.L208:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L237
	movq	-272(%rbp), %rax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	xorl	%r14d, %r14d
	cmpb	$0, -193(%rbp)
	jne	.L238
.L201:
	movq	-256(%rbp), %rax
	movq	%rax, -136(%rbp)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L193:
	xorl	%r14d, %r14d
	cmpb	$0, -193(%rbp)
	je	.L199
	xorl	%eax, %eax
	movzbl	-224(%rbp), %edx
	movb	$0, -106(%rbp)
	movw	%ax, -108(%rbp)
	movq	-144(%rbp), %rsi
	movabsq	$72057594037927935, %rax
	movl	$0, -112(%rbp)
	andq	-112(%rbp), %rax
	movq	%rdx, %rdi
	salq	$8, %rax
	orq	%rax, %rdi
	xorl	%eax, %eax
	cmpb	$0, 17(%rsi)
	movq	%rdi, -224(%rbp)
	je	.L200
	movl	12(%rsi), %eax
	movl	%edi, %edx
	salq	$32, %rax
	orq	%rax, %rdx
	movl	$1, %eax
	movq	%rdx, -224(%rbp)
.L200:
	movq	-144(%rbp), %rsi
	movb	%al, -224(%rbp)
	movq	%rbx, %rdi
	movq	-224(%rbp), %rdx
	movl	8(%rsi), %esi
	call	_ZN2v88internal4wasm16GetTypeForMemoryEPNS0_7IsolateEjNS_4base8OptionalIjEE
	movq	%rax, %r14
.L199:
	movq	-232(%rbp), %rax
	movq	%rax, -136(%rbp)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L194:
	xorl	%r14d, %r14d
	cmpb	$0, -193(%rbp)
	je	.L197
	movl	20(%r13), %eax
	movq	-144(%rbp), %rsi
	xorl	%ecx, %ecx
	salq	$4, %rax
	addq	184(%rsi), %rax
	movw	%cx, -108(%rbp)
	movabsq	$72057594037927935, %rcx
	movzbl	-216(%rbp), %esi
	movl	$0, -112(%rbp)
	movb	$0, -106(%rbp)
	andq	-112(%rbp), %rcx
	salq	$8, %rcx
	movq	%rsi, %rdi
	orq	%rcx, %rdi
	cmpb	$0, 12(%rax)
	movq	%rdi, -216(%rbp)
	je	.L215
	movl	8(%rax), %ecx
	movl	%edi, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	movq	%rsi, -216(%rbp)
.L198:
	movl	4(%rax), %r8d
	movzbl	(%rax), %esi
	movb	%dl, -216(%rbp)
	movq	%rbx, %rdi
	movq	-216(%rbp), %rcx
	movl	%r8d, %edx
	call	_ZN2v88internal4wasm15GetTypeForTableEPNS0_7IsolateENS1_9ValueTypeEjNS_4base8OptionalIjEE
	movq	%rax, %r14
.L197:
	movq	-248(%rbp), %rax
	movq	%rax, -136(%rbp)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L195:
	xorl	%r14d, %r14d
	cmpb	$0, -193(%rbp)
	jne	.L239
.L196:
	movq	-240(%rbp), %rax
	movq	%rax, -136(%rbp)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L203:
	leaq	.LC31(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L185:
	movq	41088(%rbx), %rax
	movq	%rax, -152(%rbp)
	cmpq	%rax, 41096(%rbx)
	je	.L240
.L187:
	movq	-152(%rbp), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L182:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L241
.L184:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L239:
	movq	-144(%rbp), %rdi
	movl	20(%r13), %eax
	salq	$5, %rax
	addq	136(%rdi), %rax
	movq	%rbx, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal4wasm18GetTypeForFunctionEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEE
	movq	%rax, %r14
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L238:
	movq	-144(%rbp), %rsi
	movl	20(%r13), %eax
	movq	%rbx, %rdi
	salq	$5, %rax
	addq	24(%rsi), %rax
	movzbl	(%rax), %edx
	movzbl	1(%rax), %esi
	call	_ZN2v88internal4wasm16GetTypeForGlobalEPNS0_7IsolateEbNS1_9ValueTypeE
	movq	%rax, %r14
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L215:
	xorl	%edx, %edx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L241:
	movq	%rbx, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%rbx, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, -152(%rbp)
	jmp	.L187
.L189:
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L237:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20577:
	.size	_ZN2v88internal4wasm10GetImportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE, .-_ZN2v88internal4wasm10GetImportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE
	.section	.text._ZN2v88internal4wasm10GetExportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm10GetExportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE
	.type	_ZN2v88internal4wasm10GetExportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE, @function
_ZN2v88internal4wasm10GetExportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE:
.LFB20590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rsi, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$4, -72(%rbp)
	movzbl	%dl, %r13d
	movb	%r13b, -85(%rbp)
	movq	%rax, -93(%rbp)
	movzbl	%dh, %eax
	movb	%al, -84(%rbp)
	movq	%rdx, %rax
	shrq	$16, %rax
	movb	%al, -83(%rbp)
	movq	%rdx, %rax
	shrq	$32, %rdx
	shrq	$24, %rax
	movb	%dl, -81(%rbp)
	movb	%al, -82(%rbp)
	leaq	.LC23(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$4, -72(%rbp)
	movq	%rax, -168(%rbp)
	leaq	.LC24(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$4, -72(%rbp)
	movq	%rax, -160(%rbp)
	leaq	.LC25(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$8, -72(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC26(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$5, -72(%rbp)
	movq	%rax, -224(%rbp)
	leaq	.LC27(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$6, -72(%rbp)
	movq	%rax, -232(%rbp)
	leaq	.LC28(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$6, -72(%rbp)
	movq	%rax, -240(%rbp)
	leaq	.LC29(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$9, -72(%rbp)
	movq	%rax, -216(%rbp)
	leaq	.LC30(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, -248(%rbp)
	movq	(%r15), %rax
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movq	240(%rax), %r12
	subq	232(%rax), %r12
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	sarq	$4, %r12
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movq	%rax, %r14
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE
	movq	(%r14), %rax
	movq	%r12, %rdx
	salq	$32, %rdx
	movq	%rdx, 23(%rax)
	movq	12464(%rbx), %rax
	movq	39(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L243
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L244:
	movq	879(%rsi), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L246
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -136(%rbp)
.L247:
	testl	%r12d, %r12d
	jle	.L268
	leal	-1(%r12), %eax
	movb	%r13b, -177(%rbp)
	xorl	%r13d, %r13d
	leaq	8(,%rax,8), %rax
	movq	%rax, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L269:
	movq	-128(%rbp), %rax
	movq	232(%rax), %rax
	leaq	(%rax,%r13,2), %r15
	movzbl	8(%r15), %edx
	cmpb	$4, %dl
	ja	.L250
	leaq	.L252(%rip), %rdi
	movzbl	%dl, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10GetExportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE,"a",@progbits
	.align 4
	.align 4
.L252:
	.long	.L256-.L252
	.long	.L255-.L252
	.long	.L254-.L252
	.long	.L253-.L252
	.long	.L272-.L252
	.section	.text._ZN2v88internal4wasm10GetExportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE
	.p2align 4,,10
	.p2align 3
.L272:
	movq	-248(%rbp), %rax
	xorl	%r14d, %r14d
	movq	%rax, -120(%rbp)
.L251:
	movq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%r15), %rdx
	movq	-144(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6HandleIS1_EENS0_4wasm12WireBytesRefE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L294
	movq	-168(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-120(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	-160(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	testq	%r14, %r14
	je	.L264
	movq	-192(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L264:
	movq	-152(%rbp), %rax
	movq	(%r12), %r12
	movq	(%rax), %r14
	leaq	15(%r14,%r13), %r15
	movq	%r12, (%r15)
	testb	$1, %r12b
	je	.L270
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -120(%rbp)
	testl	$262144, %eax
	je	.L266
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %rcx
	movq	8(%rcx), %rax
.L266:
	testb	$24, %al
	je	.L270
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L270
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L270:
	addq	$8, %r13
	cmpq	-176(%rbp), %r13
	jne	.L269
.L268:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L295
	movq	-256(%rbp), %rax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	xorl	%r14d, %r14d
	cmpb	$0, -177(%rbp)
	jne	.L296
.L262:
	movq	-216(%rbp), %rax
	movq	%rax, -120(%rbp)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L254:
	xorl	%r14d, %r14d
	cmpb	$0, -177(%rbp)
	je	.L260
	xorl	%eax, %eax
	movzbl	-208(%rbp), %edx
	movq	-128(%rbp), %rcx
	movb	$0, -106(%rbp)
	movw	%ax, -108(%rbp)
	movabsq	$72057594037927935, %rax
	movl	$0, -112(%rbp)
	andq	-112(%rbp), %rax
	movq	%rdx, %rsi
	salq	$8, %rax
	orq	%rax, %rsi
	xorl	%eax, %eax
	cmpb	$0, 17(%rcx)
	movq	%rsi, -208(%rbp)
	jne	.L297
.L261:
	movq	-128(%rbp), %rsi
	movb	%al, -208(%rbp)
	movq	%rbx, %rdi
	movq	-208(%rbp), %rdx
	movl	8(%rsi), %esi
	call	_ZN2v88internal4wasm16GetTypeForMemoryEPNS0_7IsolateEjNS_4base8OptionalIjEE
	movq	%rax, %r14
.L260:
	movq	-240(%rbp), %rax
	movq	%rax, -120(%rbp)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L255:
	xorl	%r14d, %r14d
	cmpb	$0, -177(%rbp)
	je	.L258
	movl	12(%r15), %eax
	movq	-128(%rbp), %rsi
	xorl	%ecx, %ecx
	salq	$4, %rax
	addq	184(%rsi), %rax
	movw	%cx, -108(%rbp)
	movabsq	$72057594037927935, %rcx
	movl	$0, -112(%rbp)
	movzbl	-200(%rbp), %esi
	movb	$0, -106(%rbp)
	andq	-112(%rbp), %rcx
	salq	$8, %rcx
	orq	%rcx, %rsi
	cmpb	$0, 12(%rax)
	movq	%rsi, -200(%rbp)
	jne	.L298
	xorl	%edx, %edx
.L259:
	movl	4(%rax), %r9d
	movzbl	(%rax), %esi
	movb	%dl, -200(%rbp)
	movq	%rbx, %rdi
	movq	-200(%rbp), %rcx
	movl	%r9d, %edx
	call	_ZN2v88internal4wasm15GetTypeForTableEPNS0_7IsolateENS1_9ValueTypeEjNS_4base8OptionalIjEE
	movq	%rax, %r14
.L258:
	movq	-232(%rbp), %rax
	movq	%rax, -120(%rbp)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L256:
	xorl	%r14d, %r14d
	cmpb	$0, -177(%rbp)
	jne	.L299
.L257:
	movq	-224(%rbp), %rax
	movq	%rax, -120(%rbp)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L294:
	leaq	.LC31(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L298:
	movl	8(%rax), %ecx
	movl	%esi, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	movq	%rsi, -200(%rbp)
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L297:
	movl	12(%rcx), %eax
	movl	%esi, %edx
	salq	$32, %rax
	orq	%rax, %rdx
	movl	$1, %eax
	movq	%rdx, -208(%rbp)
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L296:
	movq	-128(%rbp), %rsi
	movl	12(%r15), %eax
	movq	%rbx, %rdi
	salq	$5, %rax
	addq	24(%rsi), %rax
	movzbl	(%rax), %edx
	movzbl	1(%rax), %esi
	call	_ZN2v88internal4wasm16GetTypeForGlobalEPNS0_7IsolateEbNS1_9ValueTypeE
	movq	%rax, %r14
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L299:
	movq	-128(%rbp), %rsi
	movl	12(%r15), %eax
	movq	%rbx, %rdi
	salq	$5, %rax
	addq	136(%rsi), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal4wasm18GetTypeForFunctionEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEE
	movq	%rax, %r14
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L243:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L300
.L245:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L246:
	movq	41088(%rbx), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, 41096(%rbx)
	je	.L301
.L248:
	movq	-136(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%rbx, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L301:
	movq	%rbx, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, -136(%rbp)
	jmp	.L248
.L250:
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L295:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20590:
	.size	_ZN2v88internal4wasm10GetExportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE, .-_ZN2v88internal4wasm10GetExportsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE
	.section	.rodata._ZN2v88internal4wasm17GetCustomSectionsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEENS4_INS0_6StringEEEPNS1_12ErrorThrowerE.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"out of memory allocating custom section data"
	.section	.rodata._ZN2v88internal4wasm17GetCustomSectionsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEENS4_INS0_6StringEEEPNS1_12ErrorThrowerE.str1.1,"aMS",@progbits,1
.LC33:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal4wasm17GetCustomSectionsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEENS4_INS0_6StringEEEPNS1_12ErrorThrowerE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm17GetCustomSectionsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEENS4_INS0_6StringEEEPNS1_12ErrorThrowerE
	.type	_ZN2v88internal4wasm17GetCustomSectionsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEENS4_INS0_6StringEEEPNS1_12ErrorThrowerE, @function
_ZN2v88internal4wasm17GetCustomSectionsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEENS4_INS0_6StringEEEPNS1_12ErrorThrowerE:
.LFB20591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	232(%rax), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	addq	%rsi, %rdx
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal4wasm20DecodeCustomSectionsEPKhS3_@PLT
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -128(%rbp)
	cmpq	%rbx, %rax
	je	.L303
	movq	$0, -136(%rbp)
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L371:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L307
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L309
.L307:
	leaq	-88(%rbp), %rdi
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	testb	%al, %al
	jne	.L306
.L309:
	addq	$24, %rbx
	cmpq	%rbx, -128(%rbp)
	je	.L369
.L324:
	movq	8(%rbx), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6HandleIS1_EENS0_4wasm12WireBytesRefE@PLT
	movq	-120(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, -88(%rbp)
	testq	%rax, %rax
	je	.L370
	movq	(%rax), %rsi
	cmpq	%rsi, %rdx
	jne	.L371
.L306:
	movl	20(%rbx), %r8d
	testq	%r8, %r8
	je	.L336
	movq	45544(%r12), %rdi
	movq	%r8, -104(%rbp)
	movq	%r8, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-104(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L372
.L310:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE@PLT
	subq	$8, %rsp
	movq	-104(%rbp), %r8
	xorl	%r9d, %r9d
	pushq	$0
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb@PLT
	movl	20(%rbx), %edx
	movl	16(%rbx), %esi
	movq	%r15, %rdi
	addq	-144(%rbp), %rsi
	call	memcpy@PLT
	cmpq	%r13, -136(%rbp)
	popq	%rax
	movq	-104(%rbp), %r10
	popq	%rdx
	je	.L312
	movq	%r10, 0(%r13)
	addq	$24, %rbx
	addq	$8, %r13
	cmpq	%rbx, -128(%rbp)
	jne	.L324
.L369:
	subq	%r14, %r13
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	sarq	$3, %r13
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE
	movq	(%rbx), %rdx
	movq	%r13, %rcx
	salq	$32, %rcx
	movq	%rcx, 23(%rdx)
	testl	%r13d, %r13d
	jle	.L329
	leal	-1(%r13), %edx
	movl	$16, %ebx
	movq	%r15, %r9
	leaq	24(,%rdx,8), %r13
	movq	%r13, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L330:
	movq	-16(%r14,%rbx), %rax
	movq	(%r9), %r13
	movq	(%rax), %r12
	leaq	-1(%r13,%rbx), %r15
	movq	%r12, (%r15)
	testb	$1, %r12b
	je	.L334
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -112(%rbp)
	testl	$262144, %eax
	je	.L327
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r9
	movq	8(%rcx), %rax
.L327:
	testb	$24, %al
	je	.L334
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L334
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L334:
	addq	$8, %rbx
	cmpq	%rbx, -104(%rbp)
	jne	.L330
.L329:
	movq	-128(%rbp), %r15
.L311:
	testq	%r14, %r14
	je	.L331
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L331:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L332
	call	_ZdlPv@PLT
.L332:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L373
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	xorl	%r15d, %r15d
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L312:
	movq	-136(%rbp), %rcx
	movabsq	$1152921504606846975, %rdi
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L374
	testq	%rax, %rax
	je	.L375
	leaq	(%rax,%rax), %rdx
	cmpq	%rax, %rdx
	jb	.L376
	testq	%rdx, %rdx
	jne	.L377
	movq	$0, -136(%rbp)
	movl	$8, %edx
	xorl	%eax, %eax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L370:
	leaq	.LC31(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L375:
	movl	$8, %edx
.L315:
	movq	%rdx, %rdi
	movq	%r10, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%rdx, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-168(%rbp), %r10
	leaq	(%rax,%rdx), %rcx
	leaq	8(%rax), %rdx
	movq	%rcx, -136(%rbp)
	movq	-160(%rbp), %rcx
.L317:
	movq	%r10, (%rax,%rcx)
	cmpq	%r13, %r14
	je	.L339
	leaq	-8(%r13), %rsi
	leaq	15(%r14), %rdx
	subq	%r14, %rsi
	subq	%rax, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L340
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L340
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L320:
	movdqu	(%r14,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L320
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rcx
	leaq	(%r14,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%rdi, %r8
	je	.L322
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L322:
	leaq	16(%rax,%rsi), %r13
.L318:
	testq	%r14, %r14
	je	.L323
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rax
.L323:
	movq	%rax, %r14
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L377:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	salq	$3, %rdx
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L376:
	movabsq	$9223372036854775800, %rdx
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L372:
	movq	-152(%rbp), %rdi
	leaq	.LC32(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower10RangeErrorEPKcz@PLT
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%rax, %rcx
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L319:
	movq	(%rdx), %rdi
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%rdi, -8(%rcx)
	cmpq	%r13, %rdx
	jne	.L319
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L339:
	movq	%rdx, %r13
	jmp	.L318
.L303:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE
	movq	(%rbx), %rax
	movq	$0, 23(%rax)
	jmp	.L329
.L374:
	leaq	.LC33(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20591:
	.size	_ZN2v88internal4wasm17GetCustomSectionsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEENS4_INS0_6StringEEEPNS1_12ErrorThrowerE, .-_ZN2v88internal4wasm17GetCustomSectionsEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEENS4_INS0_6StringEEEPNS1_12ErrorThrowerE
	.section	.text._ZN2v88internal4wasm16DecodeLocalNamesEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm16DecodeLocalNamesEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE
	.type	_ZN2v88internal4wasm16DecodeLocalNamesEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE, @function
_ZN2v88internal4wasm16DecodeLocalNamesEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE:
.LFB20610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-96(%rbp), %rdx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	232(%rax), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	movl	$-1, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	addq	%rdi, %rsi
	movq	$0, -72(%rbp)
	call	_ZN2v88internal4wasm16DecodeLocalNamesEPKhS3_PNS1_10LocalNamesE@PLT
	movl	-96(%rbp), %eax
	xorl	%edx, %edx
	movq	%rbx, %rdi
	leal	1(%rax), %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-88(%rbp), %r12
	movq	%rax, -136(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -152(%rbp)
	cmpq	%r12, %rax
	je	.L392
.L384:
	movl	4(%r12), %eax
	movq	%rbx, %rdi
	xorl	%edx, %edx
	leal	1(%rax), %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	movq	-136(%rbp), %rax
	movq	0(%r13), %r15
	movq	(%rax), %rdi
	movl	(%r12), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L399
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	testl	$262144, %eax
	jne	.L422
.L381:
	testb	$24, %al
	je	.L399
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L423
	.p2align 4,,10
	.p2align 3
.L399:
	movq	16(%r12), %rax
	movq	8(%r12), %r15
	cmpq	%r15, %rax
	je	.L391
	movq	%r12, -144(%rbp)
	movq	%r15, %r12
	movq	%rax, %r15
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L386:
	movq	(%rax), %rdx
	movl	(%r12), %eax
	movq	0(%r13), %rdi
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L398
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -104(%rbp)
	testl	$262144, %eax
	je	.L388
	movq	%rdx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %r8
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	8(%r8), %rax
.L388:
	testb	$24, %al
	je	.L398
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L398
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L398:
	addq	$12, %r12
	cmpq	%r12, %r15
	je	.L424
.L390:
	movq	4(%r12), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6HandleIS1_EENS0_4wasm12WireBytesRefE@PLT
	testq	%rax, %rax
	jne	.L386
	leaq	.LC31(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L424:
	movq	-144(%rbp), %r12
.L391:
	addq	$32, %r12
	cmpq	%r12, -152(%rbp)
	jne	.L384
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %r12
	cmpq	%rbx, %r12
	je	.L392
	.p2align 4,,10
	.p2align 3
.L396:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L393
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L396
.L394:
	movq	-88(%rbp), %r12
.L392:
	testq	%r12, %r12
	je	.L397
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L397:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L425
	movq	-136(%rbp), %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%rsi, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L423:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L393:
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L396
	jmp	.L394
.L425:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20610:
	.size	_ZN2v88internal4wasm16DecodeLocalNamesEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE, .-_ZN2v88internal4wasm16DecodeLocalNamesEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE
	.section	.text._ZN2v88internal4wasm18EstimateStoredSizeEPKNS1_10WasmModuleE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm18EstimateStoredSizeEPKNS1_10WasmModuleE
	.type	_ZN2v88internal4wasm18EstimateStoredSizeEPKNS1_10WasmModuleE, @function
_ZN2v88internal4wasm18EstimateStoredSizeEPKNS1_10WasmModuleE:
.LFB20627:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movq	32(%rdi), %rdx
	xorl	%r11d, %r11d
	subq	24(%rdi), %rdx
	testq	%rcx, %rcx
	je	.L427
	movq	40(%rcx), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L428
	movq	16(%rcx), %rax
	subq	$24, %rax
	subq	%rsi, %rax
.L428:
	addq	(%rcx), %rax
	movq	%rax, %r11
.L427:
	movq	96(%rdi), %rax
	subq	88(%rdi), %rax
	leaq	440(%rdx,%rax), %rcx
	movq	120(%rdi), %r10
	subq	112(%rdi), %r10
	leaq	(%rcx,%r10), %rdx
	movq	144(%rdi), %r9
	subq	136(%rdi), %r9
	leaq	(%rdx,%r9), %r10
	movq	168(%rdi), %r8
	subq	160(%rdi), %r8
	movq	192(%rdi), %rsi
	leaq	(%r10,%r8), %r9
	subq	184(%rdi), %rsi
	movq	216(%rdi), %rcx
	leaq	(%r9,%rsi), %r8
	subq	208(%rdi), %rcx
	movq	240(%rdi), %rdx
	leaq	(%r8,%rcx), %rsi
	subq	232(%rdi), %rdx
	movq	264(%rdi), %rax
	leaq	(%rsi,%rdx), %rcx
	subq	256(%rdi), %rax
	leaq	(%rcx,%rax), %rdx
	movq	288(%rdi), %rax
	subq	280(%rdi), %rax
	addq	%rdx, %rax
	addq	%r11, %rax
	ret
	.cfi_endproc
.LFE20627:
	.size	_ZN2v88internal4wasm18EstimateStoredSizeEPKNS1_10WasmModuleE, .-_ZN2v88internal4wasm18EstimateStoredSizeEPKNS1_10WasmModuleE
	.section	.text._ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.type	_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, @function
_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm:
.LFB25608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L434
	movq	(%rbx), %r8
.L435:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L444
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L445:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L458
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L459
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L437:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L439
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L441:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L442:
	testq	%rsi, %rsi
	je	.L439
.L440:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movl	8(%rcx), %eax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L441
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L447
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L440
	.p2align 4,,10
	.p2align 3
.L439:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L443
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L443:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L444:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L446
	movl	8(%rax), %eax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L446:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L447:
	movq	%rdx, %rdi
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L458:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L437
.L459:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25608:
	.size	_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, .-_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.section	.text._ZN2v88internal4wasm10WasmModule25AddFunctionNameForTestingEiNS1_12WireBytesRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmModule25AddFunctionNameForTestingEiNS1_12WireBytesRefE
	.type	_ZN2v88internal4wasm10WasmModule25AddFunctionNameForTestingEiNS1_12WireBytesRefE, @function
_ZN2v88internal4wasm10WasmModule25AddFunctionNameForTestingEiNS1_12WireBytesRefE:
.LFB20425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	400(%rdi), %r12
	testq	%r12, %r12
	je	.L485
.L461:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	%ebx, %r10d
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movl	%ebx, 8(%rax)
	movq	%r14, 12(%rax)
	movq	8(%r12), %rsi
	movq	%r10, %rax
	divq	%rsi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L466
	movq	(%rax), %rcx
	movl	8(%rcx), %r8d
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L486:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L466
	movl	8(%rcx), %eax
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L466
.L468:
	cmpl	%r8d, %ebx
	jne	.L486
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rdi, %rcx
	movl	$1, %r8d
	movq	%r12, %rdi
	popq	%rbx
	movq	%r10, %rdx
	popq	%r12
	movq	%r9, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.p2align 4,,10
	.p2align 3
.L485:
	.cfi_restore_state
	movq	%rdi, %r13
	movl	$56, %edi
	call	_Znwm@PLT
	movq	400(%r13), %r15
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	movq	$0, 48(%rax)
	leaq	48(%rax), %rax
	movups	%xmm0, -16(%rax)
	movq	%r12, 400(%r13)
	movq	%rax, (%r12)
	movq	$1, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movl	$0x3f800000, 32(%r12)
	movq	$0, 40(%r12)
	testq	%r15, %r15
	je	.L461
	movq	16(%r15), %r12
	testq	%r12, %r12
	je	.L465
	.p2align 4,,10
	.p2align 3
.L462:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L462
.L465:
	movq	8(%r15), %rax
	movq	(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r15), %rdi
	leaq	48(%r15), %rax
	movq	$0, 24(%r15)
	movq	$0, 16(%r15)
	cmpq	%rax, %rdi
	je	.L463
	call	_ZdlPv@PLT
.L463:
	movl	$56, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	movq	400(%r13), %r12
	jmp	.L461
	.cfi_endproc
.LFE20425:
	.size	_ZN2v88internal4wasm10WasmModule25AddFunctionNameForTestingEiNS1_12WireBytesRefE, .-_ZN2v88internal4wasm10WasmModule25AddFunctionNameForTestingEiNS1_12WireBytesRefE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm15WasmElemSegment10kNullIndexE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm15WasmElemSegment10kNullIndexE, @function
_GLOBAL__sub_I__ZN2v88internal4wasm15WasmElemSegment10kNullIndexE:
.LFB26150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26150:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm15WasmElemSegment10kNullIndexE, .-_GLOBAL__sub_I__ZN2v88internal4wasm15WasmElemSegment10kNullIndexE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm15WasmElemSegment10kNullIndexE
	.globl	_ZN2v88internal4wasm15WasmElemSegment10kNullIndexE
	.section	.rodata._ZN2v88internal4wasm15WasmElemSegment10kNullIndexE,"a"
	.align 4
	.type	_ZN2v88internal4wasm15WasmElemSegment10kNullIndexE, @object
	.size	_ZN2v88internal4wasm15WasmElemSegment10kNullIndexE, 4
_ZN2v88internal4wasm15WasmElemSegment10kNullIndexE:
	.long	-1
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
