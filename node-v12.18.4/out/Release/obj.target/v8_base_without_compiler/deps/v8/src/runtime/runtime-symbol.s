	.file	"runtime-symbol.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Runtime_Runtime_SymbolIsPrivate"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"args[0].IsSymbol()"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE.isra.0:
.LFB25025:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L48
.L16:
	movq	_ZZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateEE27trace_event_unique_atomic50(%rip), %rbx
	testq	%rbx, %rbx
	je	.L49
.L18:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L50
.L20:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L24
.L25:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L51
.L19:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateEE27trace_event_unique_atomic50(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L24:
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	jne	.L25
	testb	$1, 11(%rax)
	je	.L26
	movq	112(%r12), %r12
.L27:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L52
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L50:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L54
.L21:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L48:
	movq	40960(%rsi), %rax
	movl	$549, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L54:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L21
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25025:
	.size	_ZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"V8.Runtime_Runtime_CreatePrivateSymbol"
	.align 8
.LC5:
	.string	"name->IsString() || name->IsUndefined(isolate)"
	.section	.text._ZN2v88internalL33Stats_Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Stats_Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE:
.LFB20062:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L104
.L56:
	movq	_ZZN2v88internalL33Stats_Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateEE27trace_event_unique_atomic16(%rip), %rbx
	testq	%rbx, %rbx
	je	.L105
.L58:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L106
.L60:
	movl	$1, %esi
	movq	%r12, %rdi
	addl	$1, 41104(%r12)
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r15
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	cmpl	$1, %r13d
	je	.L107
.L64:
	movq	(%rax), %r13
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r15
	je	.L77
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L77:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L108
.L55:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movq	(%r14), %r13
	movq	%r13, %rdx
	notq	%rdx
	testb	$1, %r13b
	jne	.L65
	cmpq	%r13, 88(%r12)
	je	.L64
.L66:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L106:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L110
.L61:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	movq	(%rdi), %rax
	call	*8(%rax)
.L62:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L63
	movq	(%rdi), %rax
	call	*8(%rax)
.L63:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L105:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L111
.L59:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateEE27trace_event_unique_atomic16(%rip)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L65:
	movq	-1(%r13), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L69
	cmpq	%r13, 88(%r12)
	jne	.L66
.L69:
	andl	$1, %edx
	jne	.L64
	movq	-1(%r13), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L64
	movq	(%rax), %r14
	movq	%r13, 15(%r14)
	leaq	15(%r14), %rsi
	testb	$1, %r13b
	je	.L64
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -168(%rbp)
	testl	$262144, %edx
	je	.L73
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %rcx
	movq	-184(%rbp), %rax
	movq	-176(%rbp), %rsi
	movq	8(%rcx), %rdx
.L73:
	andl	$24, %edx
	je	.L64
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L64
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %rax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L104:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$547, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L110:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L61
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20062:
	.size	_ZN2v88internalL33Stats_Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Stats_Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Runtime_Runtime_CreatePrivateNameSymbol"
	.section	.rodata._ZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC7:
	.string	"args[0].IsString()"
	.section	.text._ZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE.isra.0:
.LFB25029:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L144
.L113:
	movq	_ZZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateEE27trace_event_unique_atomic28(%rip), %rbx
	testq	%rbx, %rbx
	je	.L145
.L115:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L146
.L117:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L121
.L122:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L147
.L116:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateEE27trace_event_unique_atomic28(%rip)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L121:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L122
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewPrivateNameSymbolENS0_6HandleINS0_6StringEEE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L123
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L123:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L148
.L112:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L150
.L118:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L119
	movq	(%rdi), %rax
	call	*8(%rax)
.L119:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L120
	movq	(%rdi), %rax
	call	*8(%rax)
.L120:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L144:
	movq	40960(%rsi), %rax
	movl	$546, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L148:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L147:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L150:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L118
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25029:
	.size	_ZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"Symbol("
	.section	.rodata._ZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"V8.Runtime_Runtime_SymbolDescriptiveString"
	.section	.text._ZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE:
.LFB20068:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -176(%rbp)
	movq	$0, -144(%rbp)
	movaps	%xmm0, -160(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L215
.L152:
	movq	_ZZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic36(%rip), %r12
	testq	%r12, %r12
	je	.L216
.L154:
	movq	$0, -208(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L217
.L156:
	addl	$1, 41104(%r14)
	movq	(%rbx), %rax
	movq	41088(%r14), %r13
	movq	41096(%r14), %r12
	testb	$1, %al
	jne	.L160
.L161:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L218
.L155:
	movq	%r12, _ZZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic36(%rip)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L160:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jne	.L161
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movl	-120(%rbp), %edi
	testl	%edi, %edi
	je	.L187
	movl	-108(%rbp), %eax
	movl	$83, %edx
	leaq	.LC8(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L163:
	movq	-96(%rbp), %rsi
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$1, %rcx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -108(%rbp)
	movw	%dx, -1(%rsi,%rax)
	movl	-108(%rbp), %eax
	cmpl	-112(%rbp), %eax
	je	.L219
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L163
.L164:
	movq	(%rbx), %rax
	movq	15(%rax), %rsi
	testb	$1, %sil
	jne	.L220
.L170:
	movl	-108(%rbp), %eax
	movl	-120(%rbp), %esi
	movq	-96(%rbp), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, -108(%rbp)
	jne	.L175
	addl	$16, %eax
	cltq
	movb	$41, -1(%rdx,%rax)
	movl	-112(%rbp), %eax
	cmpl	%eax, -108(%rbp)
	je	.L176
.L177:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	je	.L221
.L179:
	movq	(%rax), %r15
.L180:
	subl	$1, 41104(%r14)
	movq	%r13, 41088(%r14)
	cmpq	41096(%r14), %r12
	je	.L183
	movq	%r12, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L183:
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L222
.L151:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%rcx, -216(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-216(%rbp), %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L164
	movl	-108(%rbp), %eax
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$83, %ecx
	leaq	.LC8(%rip), %rdx
.L214:
	movl	-108(%rbp), %eax
.L162:
	movq	-96(%rbp), %rsi
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$1, %rdx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -108(%rbp)
	movb	%cl, -1(%rsi,%rax)
	movl	-108(%rbp), %eax
	cmpl	-112(%rbp), %eax
	je	.L224
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L162
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L224:
	movq	%r15, %rdi
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-216(%rbp), %rdx
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L214
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L175:
	leal	16(%rax,%rax), %eax
	movl	$41, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	-112(%rbp), %eax
	cmpl	%eax, -108(%rbp)
	jne	.L177
.L176:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	jne	.L179
.L221:
	movq	312(%r14), %r15
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L217:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L225
.L157:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L158
	movq	(%rdi), %rax
	call	*8(%rax)
.L158:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L159
	movq	(%rdi), %rax
	call	*8(%rax)
.L159:
	leaq	.LC9(%rip), %rax
	movq	%r12, -200(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-200(%rbp), %rax
	movq	%r13, -184(%rbp)
	movq	%rax, -208(%rbp)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L220:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L170
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L172
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L173:
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L215:
	movq	40960(%rdx), %rax
	leaq	-168(%rbp), %rsi
	movl	$548, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -176(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	-168(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L172:
	movq	41088(%r14), %r8
	cmpq	41096(%r14), %r8
	je	.L226
.L174:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r8)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L225:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L218:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L155
.L226:
	movq	%r14, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L174
.L223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20068:
	.size	_ZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE:
.LFB20063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L250
	addl	$1, 41104(%rdx)
	movl	$1, %esi
	movq	%rdx, %rdi
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r15
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	cmpl	$1, %r13d
	je	.L251
.L229:
	movq	(%rax), %r13
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r15
	je	.L227
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L227:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movq	(%r14), %r13
	movq	%r13, %rdx
	notq	%rdx
	testb	$1, %r13b
	jne	.L230
	cmpq	%r13, 88(%r12)
	je	.L229
.L231:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L230:
	movq	-1(%r13), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L234
	cmpq	%r13, 88(%r12)
	jne	.L231
.L234:
	andl	$1, %edx
	jne	.L229
	movq	-1(%r13), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L229
	movq	(%rax), %r14
	movq	%r13, 15(%r14)
	leaq	15(%r14), %rsi
	testb	$1, %r13b
	je	.L229
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -56(%rbp)
	testl	$262144, %edx
	je	.L238
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rdx
.L238:
	andl	$24, %edx
	je	.L229
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L229
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L250:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20063:
	.size	_ZN2v88internal27Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE:
.LFB20066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L258
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L254
.L255:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L254:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L255
	movq	%rdx, %rdi
	call	_ZN2v88internal7Factory20NewPrivateNameSymbolENS0_6HandleINS0_6StringEEE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L252
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L252:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20066:
	.size	_ZN2v88internal31Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE:
.LFB20069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L299
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L262
.L263:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L262:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jne	.L263
	leaq	-96(%rbp), %r15
	movq	%rdx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movl	-88(%rbp), %edi
	testl	%edi, %edi
	je	.L286
	movl	-76(%rbp), %eax
	movl	$83, %ecx
	leaq	.LC8(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L265:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$1, %rdx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movw	%cx, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L300
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L265
.L266:
	movq	(%r12), %rax
	movq	15(%rax), %rsi
	testb	$1, %sil
	jne	.L301
.L272:
	movl	-76(%rbp), %eax
	movl	-88(%rbp), %esi
	movq	-64(%rbp), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, -76(%rbp)
	jne	.L277
	addl	$16, %eax
	cltq
	movb	$41, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	je	.L278
.L279:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	je	.L302
.L281:
	movq	(%rax), %r12
.L282:
	subl	$1, 41104(%r14)
	movq	%r13, 41088(%r14)
	cmpq	41096(%r14), %rbx
	je	.L259
	movq	%rbx, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L259:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-104(%rbp), %rdx
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	je	.L266
	movl	-76(%rbp), %eax
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L277:
	leal	16(%rax,%rax), %eax
	movl	$41, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L279
.L278:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	jne	.L281
.L302:
	movq	312(%r14), %r12
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L286:
	movl	$83, %ecx
	leaq	.LC8(%rip), %rdx
.L298:
	movl	-76(%rbp), %eax
.L264:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$1, %rdx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movb	%cl, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L304
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L264
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%r15, %rdi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-104(%rbp), %rdx
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L298
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L301:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L272
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L274
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L275:
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L299:
	call	_ZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE
	movq	%rax, %r12
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L274:
	movq	41088(%r14), %r8
	cmpq	41096(%r14), %r8
	je	.L305
.L276:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r8)
	jmp	.L275
.L305:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L276
.L303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20069:
	.size	_ZN2v88internal31Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE:
.LFB20072:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L314
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L308
.L309:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L309
	testb	$1, 11(%rax)
	je	.L310
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20072:
	.size	_ZN2v88internal23Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_SymbolIsPrivateEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal27Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal27Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal27Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE:
.LFB25014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25014:
	.size	_GLOBAL__sub_I__ZN2v88internal27Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal27Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal27Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateEE27trace_event_unique_atomic50,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateEE27trace_event_unique_atomic50, @object
	.size	_ZZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateEE27trace_event_unique_atomic50, 8
_ZZN2v88internalL29Stats_Runtime_SymbolIsPrivateEiPmPNS0_7IsolateEE27trace_event_unique_atomic50:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic36,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic36, @object
	.size	_ZZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic36, 8
_ZZN2v88internalL37Stats_Runtime_SymbolDescriptiveStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic36:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateEE27trace_event_unique_atomic28,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateEE27trace_event_unique_atomic28, @object
	.size	_ZZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateEE27trace_event_unique_atomic28, 8
_ZZN2v88internalL37Stats_Runtime_CreatePrivateNameSymbolEiPmPNS0_7IsolateEE27trace_event_unique_atomic28:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateEE27trace_event_unique_atomic16,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateEE27trace_event_unique_atomic16, @object
	.size	_ZZN2v88internalL33Stats_Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateEE27trace_event_unique_atomic16, 8
_ZZN2v88internalL33Stats_Runtime_CreatePrivateSymbolEiPmPNS0_7IsolateEE27trace_event_unique_atomic16:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
