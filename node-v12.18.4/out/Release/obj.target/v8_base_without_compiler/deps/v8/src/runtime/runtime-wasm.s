	.file	"runtime-wasm.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4446:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4446:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4447:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4447:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4449:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4449:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE, @function
_ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE:
.LFB18899:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	leaq	12448(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-1472(%rbp), %r12
	movq	%r12, %rdi
	subq	$1464, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L8
	addq	$1464, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18899:
	.size	_ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE, .-_ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE
	.section	.text._ZN2v88internal12_GLOBAL__N_114ThrowWasmErrorEPNS0_7IsolateENS0_15MessageTemplateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_114ThrowWasmErrorEPNS0_7IsolateENS0_15MessageTemplateE, @function
_ZN2v88internal12_GLOBAL__N_114ThrowWasmErrorEPNS0_7IsolateENS0_15MessageTemplateE:
.LFB18913:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	41088(%rdi), %r14
	addl	$1, 41104(%rdi)
	movq	41096(%rdi), %rbx
	call	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L10
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L10:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18913:
	.size	_ZN2v88internal12_GLOBAL__N_114ThrowWasmErrorEPNS0_7IsolateENS0_15MessageTemplateE, .-_ZN2v88internal12_GLOBAL__N_114ThrowWasmErrorEPNS0_7IsolateENS0_15MessageTemplateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB9750:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L18
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L21
.L12:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L12
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE9750:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Runtime_Runtime_ThrowWasmError"
	.section	.rodata._ZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"args[0].IsSmi()"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE.isra.0:
.LFB23011:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L54
.L23:
	movq	_ZZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic98(%rip), %rbx
	testq	%rbx, %rbx
	je	.L55
.L25:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L56
.L27:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L31
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
.L31:
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L57
	sarq	$32, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114ThrowWasmErrorEPNS0_7IsolateENS0_15MessageTemplateE
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	movq	%rax, %r12
	jne	.L58
.L33:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L59
.L22:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L56:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L61
.L28:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L29
	movq	(%rdi), %rax
	call	*8(%rax)
.L29:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	(%rdi), %rax
	call	*8(%rax)
.L30:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L55:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L62
.L26:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic98(%rip)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	movq	40960(%rsi), %rax
	movl	$646, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L61:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L28
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23011:
	.size	_ZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"V8.Runtime_Runtime_ThrowWasmStackOverflow"
	.section	.text._ZN2v88internalL36Stats_Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateE.isra.0:
.LFB23012:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L92
.L64:
	movq	_ZZN2v88internalL36Stats_Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateEE28trace_event_unique_atomic105(%rip), %rbx
	testq	%rbx, %rbx
	je	.L93
.L66:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L94
.L68:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	leaq	-144(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L95
.L63:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L97
.L67:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateEE28trace_event_unique_atomic105(%rip)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L94:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L98
.L69:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	movq	(%rdi), %rax
	call	*8(%rax)
.L70:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L71
	movq	(%rdi), %rax
	call	*8(%rax)
.L71:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L92:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$647, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L98:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L67
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23012:
	.size	_ZN2v88internalL36Stats_Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_WasmStackGuardEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"V8.Runtime_Runtime_WasmStackGuard"
	.section	.text._ZN2v88internalL28Stats_Runtime_WasmStackGuardEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_WasmStackGuardEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_WasmStackGuardEiPmPNS0_7IsolateE.isra.0:
.LFB23013:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L132
.L100:
	movq	_ZZN2v88internalL28Stats_Runtime_WasmStackGuardEiPmPNS0_7IsolateEE28trace_event_unique_atomic296(%rip), %rbx
	testq	%rbx, %rbx
	je	.L133
.L102:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L134
.L104:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L108
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
.L108:
	xorl	%esi, %esi
	leaq	-152(%rbp), %rdi
	movq	%r12, -152(%rbp)
	call	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm@PLT
	testb	%al, %al
	je	.L109
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	%rax, %r12
.L110:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L111
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L111:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L135
.L99:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L136
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	leaq	37512(%r12), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	%rax, %r12
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L133:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L137
.L103:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_WasmStackGuardEiPmPNS0_7IsolateEE28trace_event_unique_atomic296(%rip)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L134:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L138
.L105:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L106
	movq	(%rdi), %rax
	call	*8(%rax)
.L106:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L107
	movq	(%rdi), %rax
	call	*8(%rax)
.L107:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L135:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L132:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$655, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L138:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L103
.L136:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23013:
	.size	_ZN2v88internalL28Stats_Runtime_WasmStackGuardEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_WasmStackGuardEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Runtime_Runtime_WasmAtomicNotify"
	.align 8
.LC7:
	.string	"args[0].IsWasmInstanceObject()"
	.section	.rodata._ZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"args[1].IsNumber()"
.LC13:
	.string	"args[2].IsNumber()"
	.section	.text._ZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE:
.LFB18948:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L219
.L140:
	movq	_ZZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateEE28trace_event_unique_atomic359(%rip), %r13
	testq	%r13, %r13
	je	.L220
.L142:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L221
.L144:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L148
.L149:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L220:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L222
.L143:
	movq	%r13, _ZZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateEE28trace_event_unique_atomic359(%rip)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L148:
	movq	-1(%rax), %rdx
	cmpw	$1100, 11(%rdx)
	jne	.L149
	movq	-8(%rbx), %rsi
	testb	$1, %sil
	je	.L214
	movq	-1(%rsi), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L223
	movq	7(%rsi), %rdx
	movsd	.LC10(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L155
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L224
.L155:
	movabsq	$9218868437227405312, %rcx
	testq	%rcx, %rdx
	je	.L185
	movq	%rdx, %rsi
	xorl	%r15d, %r15d
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L225
	cmpl	$31, %ecx
	jg	.L154
	movabsq	$4503599627370495, %r15
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %r15
	addq	%rsi, %r15
	salq	%cl, %r15
	movl	%r15d, %r15d
.L161:
	sarq	$63, %rdx
	movq	%rdx, %rsi
	orl	$1, %esi
	imull	%esi, %r15d
.L154:
	movq	-16(%rbx), %rdx
	testb	$1, %dl
	je	.L216
.L231:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L226
	movq	7(%rdx), %rdx
	movsd	.LC10(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L227
.L168:
	movabsq	$9218868437227405312, %rcx
	testq	%rcx, %rdx
	je	.L189
	movq	%rdx, %rsi
	xorl	%ebx, %ebx
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L228
	cmpl	$31, %ecx
	jg	.L167
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %rbx
	addq	%rsi, %rbx
	salq	%cl, %rbx
	movl	%ebx, %ebx
.L174:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %ebx
.L167:
	movq	159(%rax), %rax
	movq	41112(%r12), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L176
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L177:
	movl	%r15d, %esi
	movl	%ebx, %edx
	call	_ZN2v88internal14FutexEmulation4WakeENS0_6HandleINS0_13JSArrayBufferEEEmj@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r15
	cmpq	41096(%r12), %r13
	je	.L181
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L181:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L229
.L139:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movq	-16(%rbx), %rdx
	shrq	$32, %rsi
	movq	%rsi, %r15
	testb	$1, %dl
	jne	.L231
.L216:
	shrq	$32, %rdx
	movq	%rdx, %rbx
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L176:
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L232
.L178:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L221:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L233
.L145:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L146
	movq	(%rdi), %rax
	call	*8(%rax)
.L146:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L147
	movq	(%rdi), %rax
	call	*8(%rax)
.L147:
	leaq	.LC6(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L227:
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L168
	comisd	.LC12(%rip), %xmm0
	jb	.L168
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L168
	je	.L167
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L224:
	comisd	.LC12(%rip), %xmm0
	jb	.L155
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L155
	jne	.L155
	movl	%esi, %r15d
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L225:
	cmpl	$-52, %ecx
	jl	.L154
	movabsq	$4503599627370495, %r15
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %r15
	addq	%rcx, %r15
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %r15
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L228:
	cmpl	$-52, %ecx
	jl	.L167
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rbx
	addq	%rcx, %rbx
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rbx
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L219:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$650, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L226:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L233:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L232:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L178
.L185:
	xorl	%r15d, %r15d
	jmp	.L154
.L189:
	xorl	%ebx, %ebx
	jmp	.L167
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18948:
	.size	_ZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"V8.Runtime_Runtime_WasmI32AtomicWait"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"args[3].IsNumber()"
	.section	.text._ZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE:
.LFB18952:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L320
.L235:
	movq	_ZZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic377(%rip), %r13
	testq	%r13, %r13
	je	.L321
.L237:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L322
.L239:
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	(%rbx), %rsi
	testb	$1, %sil
	jne	.L243
.L244:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L321:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L323
.L238:
	movq	%r13, _ZZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic377(%rip)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L243:
	movq	-1(%rsi), %rax
	cmpw	$1100, 11(%rax)
	jne	.L244
	movq	-8(%rbx), %rdx
	testb	$1, %dl
	je	.L314
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L324
	movq	7(%rdx), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L250
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L325
.L250:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L285
	movq	%rax, %rdi
	xorl	%edx, %edx
	shrq	$52, %rdi
	andl	$2047, %edi
	movl	%edi, %ecx
	subl	$1075, %ecx
	js	.L326
	cmpl	$31, %ecx
	jg	.L249
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rdi
	andq	%rax, %rdx
	addq	%rdi, %rdx
	salq	%cl, %rdx
	movl	%edx, %ecx
.L256:
	cqto
	orl	$1, %edx
	imull	%ecx, %edx
.L249:
	movq	-16(%rbx), %r13
	testb	$1, %r13b
	je	.L316
.L333:
	movq	-1(%r13), %rax
	cmpw	$65, 11(%rax)
	jne	.L327
	movq	7(%r13), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L328
.L263:
	movabsq	$9218868437227405312, %rcx
	testq	%rcx, %rax
	je	.L289
	movq	%rax, %rdi
	xorl	%r13d, %r13d
	shrq	$52, %rdi
	andl	$2047, %edi
	movl	%edi, %ecx
	subl	$1075, %ecx
	js	.L329
	cmpl	$31, %ecx
	jg	.L262
	movabsq	$4503599627370495, %r13
	movabsq	$4503599627370496, %rdi
	andq	%rax, %r13
	addq	%rdi, %r13
	salq	%cl, %r13
	movl	%r13d, %r13d
.L268:
	sarq	$63, %rax
	movq	%rax, %rcx
	orl	$1, %ecx
	imull	%ecx, %r13d
.L262:
	movq	-24(%rbx), %rax
	testb	$1, %al
	je	.L318
.L334:
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L330
	movsd	7(%rax), %xmm0
.L274:
	pxor	%xmm2, %xmm2
	movsd	.LC14(%rip), %xmm1
	comisd	%xmm0, %xmm2
	ja	.L275
	movapd	%xmm0, %xmm1
	divsd	.LC18(%rip), %xmm1
.L275:
	movq	159(%rsi), %rax
	movq	41112(%r12), %rdi
	movq	23(%rax), %r8
	testq	%rdi, %rdi
	je	.L276
	movq	%r8, %rsi
	movl	%edx, -172(%rbp)
	movsd	%xmm1, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movsd	-168(%rbp), %xmm1
	movl	-172(%rbp), %edx
	movq	%rax, %rsi
.L277:
	movl	%r13d, %ecx
	movl	%edx, %edx
	movapd	%xmm1, %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal14FutexEmulation6Wait32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid@PLT
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %r14
	je	.L281
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L281:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L331
.L234:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L332
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movq	-16(%rbx), %r13
	shrq	$32, %rdx
	testb	$1, %r13b
	jne	.L333
.L316:
	movq	-24(%rbx), %rax
	shrq	$32, %r13
	testb	$1, %al
	jne	.L334
.L318:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L322:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L335
.L240:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L241
	movq	(%rdi), %rax
	call	*8(%rax)
.L241:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L242
	movq	(%rdi), %rax
	call	*8(%rax)
.L242:
	leaq	.LC15(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L276:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L336
.L278:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L328:
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L263
	comisd	.LC12(%rip), %xmm0
	jb	.L263
	cvttsd2sil	%xmm0, %r13d
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%r13d, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L263
	je	.L262
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L325:
	comisd	.LC12(%rip), %xmm0
	jb	.L250
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L250
	je	.L249
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L326:
	cmpl	$-52, %ecx
	jl	.L249
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rdx
	addq	%rcx, %rdx
	movl	$1075, %ecx
	subl	%edi, %ecx
	shrq	%cl, %rdx
	movq	%rdx, %rcx
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L329:
	cmpl	$-52, %ecx
	jl	.L262
	movabsq	$4503599627370495, %r13
	movabsq	$4503599627370496, %rcx
	andq	%rax, %r13
	addq	%rcx, %r13
	movl	$1075, %ecx
	subl	%edi, %ecx
	shrq	%cl, %r13
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L320:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$648, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L324:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L327:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L330:
	leaq	.LC16(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L323:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L335:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC15(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L331:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L336:
	movq	%r12, %rdi
	movq	%r8, -184(%rbp)
	movl	%edx, -172(%rbp)
	movsd	%xmm1, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %r8
	movl	-172(%rbp), %edx
	movsd	-168(%rbp), %xmm1
	movq	%rax, %rsi
	jmp	.L278
.L285:
	xorl	%edx, %edx
	jmp	.L249
.L289:
	xorl	%r13d, %r13d
	jmp	.L262
.L332:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18952:
	.size	_ZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"V8.Runtime_Runtime_WasmI64AtomicWait"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"args[4].IsNumber()"
	.section	.text._ZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE:
.LFB18955:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L447
.L338:
	movq	_ZZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic391(%rip), %rax
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L448
.L340:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %edx
	andl	$5, %edx
	jne	.L449
.L342:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L346
.L347:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L448:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L450
.L341:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic391(%rip)
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L346:
	movq	-1(%rsi), %rax
	cmpw	$1100, 11(%rax)
	jne	.L347
	movq	-8(%r13), %rdx
	testb	$1, %dl
	je	.L439
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L451
	movq	7(%rdx), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L353
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L452
.L353:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L402
	movq	%rax, %rdx
	xorl	%r15d, %r15d
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L453
	cmpl	$31, %ecx
	jg	.L352
	movabsq	$4503599627370495, %r15
	movabsq	$4503599627370496, %rdx
	andq	%rax, %r15
	addq	%rdx, %r15
	salq	%cl, %r15
	movl	%r15d, %r15d
.L359:
	cqto
	orl	$1, %edx
	imull	%edx, %r15d
.L352:
	movq	-16(%r13), %rcx
	testb	$1, %cl
	je	.L441
.L460:
	movq	-1(%rcx), %rax
	cmpw	$65, 11(%rax)
	jne	.L454
	movq	7(%rcx), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L455
.L366:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L406
	movq	%rax, %rdi
	xorl	%edx, %edx
	shrq	$52, %rdi
	andl	$2047, %edi
	movl	%edi, %ecx
	subl	$1075, %ecx
	js	.L456
	cmpl	$31, %ecx
	jg	.L365
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rdi
	andq	%rax, %rdx
	addq	%rdi, %rdx
	salq	%cl, %rdx
	movl	%edx, %edx
.L372:
	sarq	$63, %rax
	movq	%rax, %rcx
	orl	$1, %ecx
	imull	%ecx, %edx
.L365:
	movq	-24(%r13), %rcx
	testb	$1, %cl
	je	.L443
.L461:
	movq	-1(%rcx), %rax
	cmpw	$65, 11(%rax)
	jne	.L457
	movq	7(%rcx), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L379
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L379
	comisd	.LC12(%rip), %xmm0
	jb	.L379
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L379
	jne	.L379
	movl	%ecx, %edi
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L379:
	movabsq	$9218868437227405312, %rcx
	testq	%rcx, %rax
	je	.L410
	movq	%rax, %r8
	xorl	%edi, %edi
	shrq	$52, %r8
	andl	$2047, %r8d
	movl	%r8d, %ecx
	subl	$1075, %ecx
	js	.L458
	cmpl	$31, %ecx
	jg	.L378
	movabsq	$4503599627370495, %rdi
	movabsq	$4503599627370496, %r8
	andq	%rax, %rdi
	addq	%r8, %rdi
	salq	%cl, %rdi
	movl	%edi, %edi
.L385:
	sarq	$63, %rax
	movq	%rax, %rcx
	movq	-32(%r13), %rax
	orl	$1, %ecx
	imull	%ecx, %edi
	testb	$1, %al
	jne	.L459
.L445:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L439:
	movq	-16(%r13), %rcx
	shrq	$32, %rdx
	movq	%rdx, %r15
	testb	$1, %cl
	jne	.L460
.L441:
	shrq	$32, %rcx
	movq	%rcx, %rdx
	movq	-24(%r13), %rcx
	testb	$1, %cl
	jne	.L461
.L443:
	shrq	$32, %rcx
	movq	%rcx, %rdi
.L378:
	movq	-32(%r13), %rax
	testb	$1, %al
	je	.L445
.L459:
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L462
	movsd	7(%rax), %xmm0
.L391:
	movq	%rdx, %rcx
	pxor	%xmm2, %xmm2
	movl	%edi, %edx
	movsd	.LC14(%rip), %xmm1
	salq	$32, %rcx
	orq	%rdx, %rcx
	comisd	%xmm0, %xmm2
	ja	.L392
	movapd	%xmm0, %xmm1
	divsd	.LC18(%rip), %xmm1
.L392:
	movq	159(%rsi), %rax
	movq	41112(%r12), %rdi
	movq	23(%rax), %r13
	testq	%rdi, %rdi
	je	.L393
	movq	%r13, %rsi
	movq	%rcx, -168(%rbp)
	movsd	%xmm1, -176(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %rcx
	movsd	-176(%rbp), %xmm1
	movq	%rax, %rsi
.L394:
	movl	%r15d, %edx
	movapd	%xmm1, %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal14FutexEmulation6Wait64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld@PLT
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %r14
	je	.L398
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L398:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L463
.L337:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L464
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L465
.L343:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L344
	movq	(%rdi), %rax
	call	*8(%rax)
.L344:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L345
	movq	(%rdi), %rax
	call	*8(%rax)
.L345:
	leaq	.LC19(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L393:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L466
.L395:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L455:
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L366
	comisd	.LC12(%rip), %xmm0
	jb	.L366
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L366
	jne	.L366
	movl	%ecx, %edx
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L458:
	cmpl	$-52, %ecx
	jl	.L378
	movabsq	$4503599627370495, %rdi
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rdi
	addq	%rcx, %rdi
	movl	$1075, %ecx
	subl	%r8d, %ecx
	shrq	%cl, %rdi
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L452:
	comisd	.LC12(%rip), %xmm0
	jb	.L353
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L353
	jne	.L353
	movl	%edx, %r15d
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L453:
	cmpl	$-52, %ecx
	jl	.L352
	movabsq	$4503599627370495, %r15
	movabsq	$4503599627370496, %rcx
	andq	%rax, %r15
	addq	%rcx, %r15
	movl	$1075, %ecx
	subl	%edx, %ecx
	shrq	%cl, %r15
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L456:
	cmpl	$-52, %ecx
	jl	.L365
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rdx
	addq	%rcx, %rdx
	movl	$1075, %ecx
	subl	%edi, %ecx
	shrq	%cl, %rdx
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L447:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$649, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L451:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L454:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L457:
	leaq	.LC16(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L450:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L465:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC19(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L462:
	leaq	.LC20(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L463:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L466:
	movq	%r12, %rdi
	movq	%rcx, -168(%rbp)
	movsd	%xmm1, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movsd	-176(%rbp), %xmm1
	movq	-168(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L395
.L402:
	xorl	%r15d, %r15d
	jmp	.L352
.L406:
	xorl	%edx, %edx
	jmp	.L365
.L410:
	xorl	%edi, %edi
	jmp	.L378
.L464:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18955:
	.size	_ZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Stats_Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"V8.Runtime_Runtime_WasmThrowTypeError"
	.section	.text._ZN2v88internalL32Stats_Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateE.isra.0:
.LFB23018:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L497
.L468:
	movq	_ZZN2v88internalL32Stats_Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic111(%rip), %rbx
	testq	%rbx, %rbx
	je	.L498
.L470:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L499
.L472:
	addl	$1, 41104(%r12)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$349, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L476
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L476:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L500
.L467:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L501
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L502
.L471:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic111(%rip)
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L499:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L503
.L473:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L474
	movq	(%rdi), %rax
	call	*8(%rax)
.L474:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L475
	movq	(%rdi), %rax
	call	*8(%rax)
.L475:
	leaq	.LC21(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L500:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L497:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$657, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L503:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC21(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L502:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L471
.L501:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23018:
	.size	_ZN2v88internalL32Stats_Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"V8.Runtime_Runtime_WasmExceptionGetTag"
	.section	.text._ZN2v88internalL33Stats_Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateE.isra.0:
.LFB23019:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L538
.L505:
	movq	_ZZN2v88internalL33Stats_Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateEE28trace_event_unique_atomic144(%rip), %rbx
	testq	%rbx, %rbx
	je	.L539
.L507:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L540
.L509:
	addl	$1, 41104(%r12)
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE
	movq	41112(%r12), %rdi
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
	movq	(%r14), %r14
	testq	%rdi, %rdi
	je	.L513
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L514:
	movq	%r12, %rdi
	call	_ZN2v88internal20WasmExceptionPackage15GetExceptionTagEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L518
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L518:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L541
.L504:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L542
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L543
.L515:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L540:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L544
.L510:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L511
	movq	(%rdi), %rax
	call	*8(%rax)
.L511:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L512
	movq	(%rdi), %rax
	call	*8(%rax)
.L512:
	leaq	.LC22(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L539:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L545
.L508:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateEE28trace_event_unique_atomic144(%rip)
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L538:
	movq	40960(%rsi), %rax
	movl	$652, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L541:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L543:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L544:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC22(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L545:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L508
.L542:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23019:
	.size	_ZN2v88internalL33Stats_Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"V8.Runtime_Runtime_WasmExceptionGetValues"
	.section	.text._ZN2v88internalL36Stats_Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateE.isra.0:
.LFB23020:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L580
.L547:
	movq	_ZZN2v88internalL36Stats_Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateEE28trace_event_unique_atomic156(%rip), %rbx
	testq	%rbx, %rbx
	je	.L581
.L549:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L582
.L551:
	addl	$1, 41104(%r12)
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE
	movq	41112(%r12), %rdi
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
	movq	(%r14), %r14
	testq	%rdi, %rdi
	je	.L555
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L556:
	movq	%r12, %rdi
	call	_ZN2v88internal20WasmExceptionPackage18GetExceptionValuesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L560
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L560:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L583
.L546:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L584
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L555:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L585
.L557:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L582:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L586
.L552:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L553
	movq	(%rdi), %rax
	call	*8(%rax)
.L553:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L554
	movq	(%rdi), %rax
	call	*8(%rax)
.L554:
	leaq	.LC23(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L581:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L587
.L550:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateEE28trace_event_unique_atomic156(%rip)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L580:
	movq	40960(%rsi), %rax
	movl	$651, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L583:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L585:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L586:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC23(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L587:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L550
.L584:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23020:
	.size	_ZN2v88internalL36Stats_Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"V8.Runtime_Runtime_WasmThrowCreate"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC25:
	.string	"args[0].IsWasmExceptionTag()"
.LC26:
	.string	"args[1].IsSmi()"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC27:
	.string	"!Object::SetProperty(isolate, exception, isolate->factory()->wasm_exception_tag_symbol(), tag, StoreOrigin::kMaybeKeyed, Just(ShouldThrow::kThrowOnError)) .is_null()"
	.align 8
.LC28:
	.string	"!Object::SetProperty(isolate, exception, isolate->factory()->wasm_exception_values_symbol(), values, StoreOrigin::kMaybeKeyed, Just(ShouldThrow::kThrowOnError)) .is_null()"
	.section	.text._ZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateE.isra.0:
.LFB23023:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L626
.L589:
	movq	_ZZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateEE28trace_event_unique_atomic118(%rip), %rbx
	testq	%rbx, %rbx
	je	.L627
.L591:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L628
.L593:
	addl	$1, 41104(%r12)
	movq	%r12, %rdi
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	call	_ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L597
.L598:
	leaq	.LC25(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L627:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L629
.L592:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateEE28trace_event_unique_atomic118(%rip)
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L597:
	movq	-1(%rsi), %rax
	cmpw	$106, 11(%rax)
	jne	.L598
	movq	-8(%r13), %r13
	testb	$1, %r13b
	jne	.L630
	movq	41112(%r12), %rdi
	sarq	$32, %r13
	movq	%r13, -168(%rbp)
	testq	%rdi, %rdi
	je	.L600
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L601:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$353, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	leaq	3824(%r12), %rdx
	movl	$1, %r9d
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L631
	movl	-168(%rbp), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	3832(%r12), %rdx
	movl	$1, %r9d
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L632
	movq	(%rbx), %r13
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L605
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L605:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L633
.L588:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L634
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L600:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L635
.L602:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L628:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L636
.L594:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L595
	movq	(%rdi), %rax
	call	*8(%rax)
.L595:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L596
	movq	(%rdi), %rax
	call	*8(%rax)
.L596:
	leaq	.LC24(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L626:
	movq	40960(%rsi), %rax
	movl	$656, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L630:
	leaq	.LC26(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L631:
	leaq	.LC27(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L632:
	leaq	.LC28(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L629:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L636:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC24(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L635:
	movq	%r12, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L588
.L634:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23023:
	.size	_ZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"V8.Runtime_Runtime_WasmNewMultiReturnFixedArray"
	.section	.rodata._ZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC30:
	.string	"args[0].IsNumber()"
.LC31:
	.string	"args[0].ToInt32(&size)"
	.section	.text._ZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE.isra.0:
.LFB23030:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L674
.L638:
	movq	_ZZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic572(%rip), %rbx
	testq	%rbx, %rbx
	je	.L675
.L640:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L676
.L642:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L677
.L647:
	leaq	-156(%rbp), %rsi
	leaq	-152(%rbp), %rdi
	movl	$0, -156(%rbp)
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L678
	movl	-156(%rbp), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L651
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L651:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L679
.L637:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L680
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L677:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L647
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L676:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L681
.L643:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L644
	movq	(%rdi), %rax
	call	*8(%rax)
.L644:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L645
	movq	(%rdi), %rax
	call	*8(%rax)
.L645:
	leaq	.LC29(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L675:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L682
.L641:
	movq	%rbx, _ZZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic572(%rip)
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L674:
	movq	40960(%rsi), %rax
	movl	$667, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L678:
	leaq	.LC31(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L679:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L682:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L681:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC29(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L643
.L680:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23030:
	.size	_ZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"V8.Runtime_Runtime_WasmNewMultiReturnJSArray"
	.section	.rodata._ZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC33:
	.string	"args[0].IsFixedArray()"
	.section	.text._ZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE.isra.0:
.LFB23031:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L718
.L684:
	movq	_ZZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic580(%rip), %rbx
	testq	%rbx, %rbx
	je	.L719
.L686:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L720
.L688:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%r14), %r14
	testb	$1, %r14b
	jne	.L692
.L693:
	leaq	.LC33(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L719:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L721
.L687:
	movq	%rbx, _ZZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic580(%rip)
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L692:
	movq	-1(%r14), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L693
	movq	41112(%r12), %rdi
	movq	%r14, %r15
	testq	%rdi, %rdi
	je	.L694
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
	movq	%rax, %rsi
.L695:
	movslq	11(%r15), %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L697
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L697:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L722
.L683:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L723
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L694:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L724
.L696:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L720:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L725
.L689:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L690
	movq	(%rdi), %rax
	call	*8(%rax)
.L690:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L691
	movq	(%rdi), %rax
	call	*8(%rax)
.L691:
	leaq	.LC32(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L718:
	movq	40960(%rsi), %rax
	movl	$668, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L722:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L724:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L725:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC32(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L721:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L687
.L723:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23031:
	.size	_ZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL37Stats_Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"V8.Runtime_Runtime_WasmIsValidFuncRefValue"
	.section	.text._ZN2v88internalL37Stats_Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE.isra.0:
.LFB23034:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L760
.L727:
	movq	_ZZN2v88internalL37Stats_Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateEE27trace_event_unique_atomic66(%rip), %rbx
	testq	%rbx, %rbx
	je	.L761
.L729:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L762
.L731:
	movl	41104(%r12), %eax
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %rdi
	cmpq	104(%r12), %rdi
	jne	.L763
	movl	%eax, 41104(%r12)
	movabsq	$4294967296, %r13
.L740:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L764
.L726:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L765
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L766
.L730:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateEE27trace_event_unique_atomic66(%rip)
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L763:
	call	_ZN2v88internal20WasmExportedFunction22IsWasmExportedFunctionENS0_6ObjectE@PLT
	testb	%al, %al
	movl	41104(%r12), %eax
	jne	.L767
	movq	41096(%r12), %rdx
	subl	$1, %eax
	xorl	%r13d, %r13d
.L737:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L740
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L762:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L768
.L732:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L733
	movq	(%rdi), %rax
	call	*8(%rax)
.L733:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L734
	movq	(%rdi), %rax
	call	*8(%rax)
.L734:
	leaq	.LC34(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L767:
	movq	41096(%r12), %rdx
	subl	$1, %eax
	movabsq	$4294967296, %r13
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L764:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L760:
	movq	40960(%rsi), %rax
	movl	$665, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L768:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC34(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L766:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L730
.L765:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23034:
	.size	_ZN2v88internalL37Stats_Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"V8.Runtime_Runtime_WasmTableCopy"
	.align 8
.LC37:
	.string	"args[0].ToUint32(&table_dst_index)"
	.align 8
.LC38:
	.string	"args[1].ToUint32(&table_src_index)"
	.section	.rodata._ZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC39:
	.string	"args[2].ToUint32(&dst)"
.LC40:
	.string	"args[3].ToUint32(&src)"
.LC41:
	.string	"args[4].ToUint32(&count)"
	.section	.text._ZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateE.isra.0:
.LFB23035:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1576, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L871
.L770:
	movq	_ZZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateEE28trace_event_unique_atomic503(%rip), %r13
	testq	%r13, %r13
	je	.L872
.L772:
	movq	$0, -1600(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L873
.L774:
	addl	$1, 41104(%r12)
	movq	41088(%r12), %rax
	movq	%r12, %rsi
	leaq	-1520(%rbp), %r15
	leaq	12448(%r12), %r14
	movq	%r15, %rdi
	movq	41096(%r12), %r13
	movq	%r14, %rdx
	movq	%rax, -1608(%rbp)
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L778
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L779:
	movq	(%rbx), %rdx
	testb	$1, %dl
	jne	.L874
	sarq	$32, %rdx
	movl	%edx, %r10d
	js	.L786
.L787:
	movq	-8(%rbx), %rdx
	testb	$1, %dl
	jne	.L875
	sarq	$32, %rdx
	movl	%edx, %ecx
	js	.L798
.L799:
	movq	-16(%rbx), %rax
	testb	$1, %al
	je	.L867
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L876
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L877
.L810:
	leaq	.LC39(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L875:
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L878
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L879
.L798:
	leaq	.LC38(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L874:
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L880
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L881
.L786:
	leaq	.LC37(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L778:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L882
.L780:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L867:
	sarq	$32, %rax
	movl	%eax, %r8d
	js	.L810
.L811:
	movq	-24(%rbx), %rax
	testb	$1, %al
	je	.L868
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L883
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L884
.L822:
	leaq	.LC40(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L873:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L885
.L775:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L776
	movq	(%rdi), %rax
	call	*8(%rax)
.L776:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L777
	movq	(%rdi), %rax
	call	*8(%rax)
.L777:
	leaq	.LC35(%rip), %rax
	movq	%r13, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%r14, -1576(%rbp)
	movq	%rax, -1600(%rbp)
	jmp	.L774
	.p2align 4,,10
	.p2align 3
.L872:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L886
.L773:
	movq	%r13, _ZZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateEE28trace_event_unique_atomic503(%rip)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L868:
	sarq	$32, %rax
	movl	%eax, %r9d
	js	.L822
.L823:
	movq	-32(%rbx), %rax
	testb	$1, %al
	je	.L869
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L887
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L888
.L834:
	leaq	.LC41(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L869:
	sarq	$32, %rax
	movl	%eax, %edx
	js	.L834
.L835:
	subq	$8, %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	%rdx
	movl	%r10d, %edx
	call	_ZN2v88internal18WasmInstanceObject16CopyTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj@PLT
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	je	.L889
	movq	88(%r12), %r15
.L843:
	subl	$1, 41104(%r12)
	movq	-1608(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L846
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L846:
	leaq	-1600(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L890
.L769:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L891
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L889:
	.cfi_restore_state
	cmpq	$0, 12464(%r12)
	jne	.L842
	movq	(%r15), %rax
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
.L842:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$352, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r15
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L877:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm4
	addsd	%xmm1, %xmm4
	movq	%xmm4, %rdx
	movq	%xmm4, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L810
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm4, %r8d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L810
	je	.L811
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L883:
	leaq	.LC16(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L884:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm5
	addsd	%xmm1, %xmm5
	movq	%xmm5, %rdx
	movq	%xmm5, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L822
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm5, %r9d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L822
	je	.L823
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L881:
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L786
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %r10d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L786
	je	.L787
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L880:
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L871:
	movq	40960(%rsi), %rax
	movl	$662, %edx
	leaq	-1560(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -1568(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L879:
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rdx
	movq	%xmm3, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L798
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm3, %ecx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L798
	je	.L799
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L882:
	movq	%r12, %rdi
	movq	%rax, -1616(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1616(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L878:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L886:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L885:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC35(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L876:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L887:
	leaq	.LC20(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L888:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm6
	addsd	%xmm1, %xmm6
	movq	%xmm6, %rdx
	movq	%xmm6, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L834
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm6, %edx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L834
	je	.L835
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L890:
	leaq	-1560(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L769
.L891:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23035:
	.size	_ZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL29Stats_Runtime_WasmCompileLazyEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC42:
	.string	"V8.Runtime_Runtime_WasmCompileLazy"
	.section	.text._ZN2v88internalL29Stats_Runtime_WasmCompileLazyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_WasmCompileLazyEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Stats_Runtime_WasmCompileLazyEiPmPNS0_7IsolateE:
.LFB18944:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L930
.L893:
	movq	_ZZN2v88internalL29Stats_Runtime_WasmCompileLazyEiPmPNS0_7IsolateEE28trace_event_unique_atomic311(%rip), %r13
	testq	%r13, %r13
	je	.L931
.L895:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L932
.L897:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L901
.L902:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L931:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L933
.L896:
	movq	%r13, _ZZN2v88internalL29Stats_Runtime_WasmCompileLazyEiPmPNS0_7IsolateEE28trace_event_unique_atomic311(%rip)
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L901:
	movq	-1(%rax), %rdx
	cmpw	$1100, 11(%rdx)
	jne	.L902
	movq	-8(%rbx), %rsi
	testb	$1, %sil
	jne	.L934
	sarq	$32, %rsi
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	movq	%rsi, %r15
	je	.L904
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	movq	(%rbx), %rax
.L904:
	movq	151(%rax), %rax
	movl	%r15d, %edx
	movq	%r12, %rdi
	movq	%rax, 12464(%r12)
	movq	(%rbx), %rax
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rbx
	movq	%rbx, %rsi
	call	_ZN2v88internal4wasm11CompileLazyEPNS0_7IsolateEPNS1_12NativeModuleEi@PLT
	testb	%al, %al
	jne	.L905
	movq	312(%r12), %r15
.L906:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L907
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L907:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L910
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L910:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L935
.L892:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L936
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L905:
	.cfi_restore_state
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal4wasm12NativeModule24GetCallTargetForFunctionEj@PLT
	movq	%rax, %r15
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L932:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L937
.L898:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L899
	movq	(%rdi), %rax
	call	*8(%rax)
.L899:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L900
	movq	(%rdi), %rax
	call	*8(%rax)
.L900:
	leaq	.LC42(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L930:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$666, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L934:
	leaq	.LC26(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L935:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L937:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC42(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L933:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L896
.L936:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18944:
	.size	_ZN2v88internalL29Stats_Runtime_WasmCompileLazyEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Stats_Runtime_WasmCompileLazyEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL25Stats_Runtime_WasmRefFuncEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC43:
	.string	"V8.Runtime_Runtime_WasmRefFunc"
	.align 8
.LC44:
	.string	"args[0].ToUint32(&function_index)"
	.section	.text._ZN2v88internalL25Stats_Runtime_WasmRefFuncEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_WasmRefFuncEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL25Stats_Runtime_WasmRefFuncEiPmPNS0_7IsolateE.isra.0:
.LFB23047:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$1576, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L987
.L939:
	movq	_ZZN2v88internalL25Stats_Runtime_WasmRefFuncEiPmPNS0_7IsolateEE28trace_event_unique_atomic422(%rip), %rbx
	testq	%rbx, %rbx
	je	.L988
.L941:
	movq	$0, -1600(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L989
.L943:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L947
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
.L947:
	addl	$1, 41104(%r12)
	leaq	-1520(%rbp), %r15
	movq	%r12, %rsi
	movq	41088(%r12), %r14
	leaq	12448(%r12), %rdx
	movq	%r15, %rdi
	movq	41096(%r12), %rbx
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L948
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L949:
	movq	151(%rsi), %rax
	movq	%rax, 12464(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L990
	sarq	$32, %rdx
	movl	%edx, %r9d
	js	.L956
.L957:
	movl	%r9d, %edx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18WasmInstanceObject31GetOrCreateWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L966
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L966:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	jne	.L991
.L963:
	leaq	-1600(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L992
.L938:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L993
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L991:
	.cfi_restore_state
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L990:
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L994
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L995
.L956:
	leaq	.LC44(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L948:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L996
.L950:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L989:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L997
.L944:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L945
	movq	(%rdi), %rax
	call	*8(%rax)
.L945:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L946
	movq	(%rdi), %rax
	call	*8(%rax)
.L946:
	leaq	.LC43(%rip), %rax
	movq	%rbx, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%r14, -1576(%rbp)
	movq	%rax, -1600(%rbp)
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L988:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L998
.L942:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_WasmRefFuncEiPmPNS0_7IsolateEE28trace_event_unique_atomic422(%rip)
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L995:
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L956
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %r9d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L956
	je	.L957
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L994:
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L987:
	movq	40960(%rsi), %rax
	movl	$658, %edx
	leaq	-1560(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -1568(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L996:
	movq	%r12, %rdi
	movq	%rax, -1608(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1608(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L992:
	leaq	-1560(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L998:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L997:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC43(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L944
.L993:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23047:
	.size	_ZN2v88internalL25Stats_Runtime_WasmRefFuncEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL25Stats_Runtime_WasmRefFuncEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL34Stats_Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC45:
	.string	"V8.Runtime_Runtime_WasmFunctionTableGet"
	.align 8
.LC46:
	.string	"args[1].ToUint32(&table_index)"
	.align 8
.LC47:
	.string	"args[2].ToUint32(&entry_index)"
	.section	.text._ZN2v88internalL34Stats_Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateE, @function
_ZN2v88internalL34Stats_Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateE:
.LFB18962:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1065
.L1000:
	movq	_ZZN2v88internalL34Stats_Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateEE28trace_event_unique_atomic440(%rip), %r13
	testq	%r13, %r13
	je	.L1066
.L1002:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1067
.L1004:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1008
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
.L1008:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rdx
	testb	$1, %dl
	jne	.L1009
.L1010:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1066:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1068
.L1003:
	movq	%r13, _ZZN2v88internalL34Stats_Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateEE28trace_event_unique_atomic440(%rip)
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	-1(%rdx), %rax
	cmpw	$1100, 11(%rax)
	jne	.L1010
	movq	-8(%rbx), %rax
	testb	$1, %al
	je	.L1062
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L1069
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	je	.L1070
.L1016:
	leaq	.LC46(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1062:
	sarq	$32, %rax
	movl	%eax, %ecx
	js	.L1016
.L1017:
	movq	-16(%rbx), %rax
	testb	$1, %al
	je	.L1063
	movq	-1(%rax), %rsi
	cmpw	$65, 11(%rsi)
	jne	.L1071
	movq	-1(%rax), %rsi
	cmpw	$65, 11(%rsi)
	je	.L1072
.L1028:
	leaq	.LC47(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1063:
	sarq	$32, %rax
	movl	%eax, %ebx
	js	.L1028
.L1029:
	movq	199(%rdx), %rdx
	leal	16(,%rcx,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1035
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1036:
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject10IsInBoundsEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	testb	%al, %al
	jne	.L1038
	movl	$352, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114ThrowWasmErrorEPNS0_7IsolateENS0_15MessageTemplateE
	movq	%rax, %r15
.L1039:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1043
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1043:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1040
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L1040:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1073
.L999:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1074
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1067:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1075
.L1005:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1006
	movq	(%rdi), %rax
	call	*8(%rax)
.L1006:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1007
	movq	(%rdi), %rax
	call	*8(%rax)
.L1007:
	leaq	.LC45(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1038:
	movq	%r15, %rsi
	movl	%ebx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject3GetEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	movq	(%rax), %r15
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1035:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L1076
.L1037:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1072:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rsi
	movq	%xmm3, %rax
	shrq	$32, %rsi
	cmpq	$1127219200, %rsi
	jne	.L1028
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm3, %ebx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1028
	je	.L1029
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$659, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1070:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rax
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L1016
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %ecx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1016
	je	.L1017
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1069:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1071:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1068:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1075:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC45(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1073:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1076:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1037
.L1074:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18962:
	.size	_ZN2v88internalL34Stats_Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateE, .-_ZN2v88internalL34Stats_Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Stats_Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC48:
	.string	"V8.Runtime_Runtime_WasmFunctionTableSet"
	.section	.text._ZN2v88internalL34Stats_Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateE, @function
_ZN2v88internalL34Stats_Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateE:
.LFB18965:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1146
.L1078:
	movq	_ZZN2v88internalL34Stats_Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateEE28trace_event_unique_atomic460(%rip), %r13
	testq	%r13, %r13
	je	.L1147
.L1080:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1148
.L1082:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1086
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
.L1086:
	movq	41088(%r12), %rax
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	%rax, -168(%rbp)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L1087
.L1088:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1147:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1149
.L1081:
	movq	%r13, _ZZN2v88internalL34Stats_Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateEE28trace_event_unique_atomic460(%rip)
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1087:
	movq	-1(%rax), %rax
	cmpw	$1100, 11(%rax)
	jne	.L1088
	movq	-8(%rbx), %rax
	testb	$1, %al
	je	.L1143
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1150
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1151
.L1094:
	leaq	.LC46(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1143:
	sarq	$32, %rax
	movl	%eax, %edx
	js	.L1094
.L1095:
	movq	-16(%rbx), %rax
	testb	$1, %al
	je	.L1144
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L1152
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	je	.L1153
.L1106:
	leaq	.LC47(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1144:
	sarq	$32, %rax
	movl	%eax, %r15d
	js	.L1106
.L1107:
	movq	41112(%r12), %rdi
	movq	-24(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L1113
	movl	%edx, -176(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-176(%rbp), %edx
	movq	%rax, %r14
.L1114:
	movq	(%rbx), %rsi
	leal	16(,%rdx,8), %eax
	cltq
	movq	199(%rsi), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1116
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L1117:
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject10IsInBoundsEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	testb	%al, %al
	jne	.L1119
	movl	$352, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114ThrowWasmErrorEPNS0_7IsolateENS0_15MessageTemplateE
	movq	%rax, %r15
.L1120:
	subl	$1, 41104(%r12)
	movq	-168(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1124
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1124:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1121
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L1121:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1154
.L1077:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1155
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1148:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1156
.L1083:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1084
	movq	(%rdi), %rax
	call	*8(%rax)
.L1084:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1085
	movq	(%rdi), %rax
	call	*8(%rax)
.L1085:
	leaq	.LC48(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1119:
	movl	%r15d, %edx
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE@PLT
	movq	88(%r12), %r15
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L1157
.L1118:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L1158
.L1115:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1153:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rcx
	movq	%xmm3, %rax
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L1106
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm3, %r15d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1106
	je	.L1107
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$660, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1151:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1094
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %edx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1094
	je	.L1095
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1150:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1152:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1149:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1156:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC48(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1154:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	%r12, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1158:
	movq	%r12, %rdi
	movl	%edx, -180(%rbp)
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-180(%rbp), %edx
	movq	-176(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1115
.L1155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18965:
	.size	_ZN2v88internalL34Stats_Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateE, .-_ZN2v88internalL34Stats_Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL27Stats_Runtime_WasmTableInitEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC49:
	.string	"V8.Runtime_Runtime_WasmTableInit"
	.align 8
.LC50:
	.string	"args[0].ToUint32(&table_index)"
	.align 8
.LC51:
	.string	"args[1].ToUint32(&elem_segment_index)"
	.section	.text._ZN2v88internalL27Stats_Runtime_WasmTableInitEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_WasmTableInitEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_WasmTableInitEiPmPNS0_7IsolateE.isra.0:
.LFB23048:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1576, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1261
.L1160:
	movq	_ZZN2v88internalL27Stats_Runtime_WasmTableInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic483(%rip), %r13
	testq	%r13, %r13
	je	.L1262
.L1162:
	movq	$0, -1600(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1263
.L1164:
	addl	$1, 41104(%r12)
	leaq	-1520(%rbp), %r15
	movq	%r12, %rsi
	movq	41088(%r12), %r14
	leaq	12448(%r12), %rdx
	movq	%r15, %rdi
	movq	41096(%r12), %r13
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1168
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1169:
	movq	(%rbx), %rdx
	testb	$1, %dl
	jne	.L1264
	sarq	$32, %rdx
	movl	%edx, %r10d
	js	.L1176
.L1177:
	movq	-8(%rbx), %rax
	testb	$1, %al
	jne	.L1265
	sarq	$32, %rax
	movl	%eax, %ecx
	js	.L1188
.L1189:
	movq	-16(%rbx), %rax
	testb	$1, %al
	je	.L1257
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1266
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1267
.L1200:
	leaq	.LC39(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1268
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1269
.L1188:
	leaq	.LC51(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1264:
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L1270
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L1271
.L1176:
	leaq	.LC50(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1168:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L1272
.L1170:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1257:
	sarq	$32, %rax
	movl	%eax, %r8d
	js	.L1200
.L1201:
	movq	-24(%rbx), %rax
	testb	$1, %al
	je	.L1258
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1273
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1274
.L1212:
	leaq	.LC40(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1263:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1275
.L1165:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1166
	movq	(%rdi), %rax
	call	*8(%rax)
.L1166:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1167
	movq	(%rdi), %rax
	call	*8(%rax)
.L1167:
	leaq	.LC49(%rip), %rax
	movq	%r13, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%r14, -1576(%rbp)
	movq	%rax, -1600(%rbp)
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1262:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1276
.L1163:
	movq	%r13, _ZZN2v88internalL27Stats_Runtime_WasmTableInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic483(%rip)
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1258:
	sarq	$32, %rax
	movl	%eax, %r9d
	js	.L1212
.L1213:
	movq	-32(%rbx), %rax
	testb	$1, %al
	je	.L1259
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1277
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1278
.L1224:
	leaq	.LC41(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1259:
	sarq	$32, %rax
	movl	%eax, %edx
	js	.L1224
.L1225:
	movq	(%r15), %rax
	subq	$8, %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	151(%rax), %rax
	pushq	%rdx
	movl	%r10d, %edx
	movq	%rax, 12464(%r12)
	call	_ZN2v88internal18WasmInstanceObject16InitTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj@PLT
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	je	.L1279
	movq	88(%r12), %r15
.L1233:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1236
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1236:
	leaq	-1600(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1280
.L1159:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1281
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1279:
	.cfi_restore_state
	cmpq	$0, 12464(%r12)
	jne	.L1232
	movq	(%r15), %rax
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
.L1232:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$352, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r15
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1267:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm4
	addsd	%xmm1, %xmm4
	movq	%xmm4, %rdx
	movq	%xmm4, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1200
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm4, %r8d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1200
	je	.L1201
	jmp	.L1200
	.p2align 4,,10
	.p2align 3
.L1273:
	leaq	.LC16(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1274:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm5
	addsd	%xmm1, %xmm5
	movq	%xmm5, %rdx
	movq	%xmm5, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1212
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm5, %r9d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1212
	je	.L1213
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1271:
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1176
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %r10d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1176
	je	.L1177
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1270:
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	40960(%rsi), %rax
	movl	$661, %edx
	leaq	-1560(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -1568(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1269:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rdx
	movq	%xmm3, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1188
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm3, %ecx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1188
	je	.L1189
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1272:
	movq	%r12, %rdi
	movq	%rax, -1608(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1608(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1268:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1276:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1275:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC49(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1266:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1277:
	leaq	.LC20(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1278:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm6
	addsd	%xmm1, %xmm6
	movq	%xmm6, %rdx
	movq	%xmm6, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1224
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm6, %edx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1224
	je	.L1225
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1280:
	leaq	-1560(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1159
.L1281:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23048:
	.size	_ZN2v88internalL27Stats_Runtime_WasmTableInitEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_WasmTableInitEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC52:
	.string	"V8.Runtime_Runtime_WasmTableGrow"
	.section	.rodata._ZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC53:
	.string	"args[2].ToUint32(&delta)"
	.section	.text._ZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateE.isra.0:
.LFB23049:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1576, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1348
.L1283:
	movq	_ZZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic522(%rip), %r13
	testq	%r13, %r13
	je	.L1349
.L1285:
	movq	$0, -1600(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1350
.L1287:
	addl	$1, 41104(%r12)
	leaq	-1520(%rbp), %r15
	movq	%r12, %rsi
	movq	41088(%r12), %r14
	leaq	12448(%r12), %rdx
	movq	%r15, %rdi
	movq	41096(%r12), %r13
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1291
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1292:
	movq	(%rbx), %rdx
	testb	$1, %dl
	jne	.L1351
	sarq	$32, %rdx
	movl	%edx, %r8d
	js	.L1299
.L1300:
	movq	41112(%r12), %rdi
	movq	-8(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L1306
	movl	%r8d, -1608(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-16(%rbx), %rdx
	movl	-1608(%rbp), %r8d
	movq	%rax, %rcx
	testb	$1, %dl
	je	.L1346
.L1355:
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L1352
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L1353
.L1314:
	leaq	.LC53(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1306:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L1354
.L1308:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	movq	-16(%rbx), %rdx
	testb	$1, %dl
	jne	.L1355
.L1346:
	sarq	$32, %rdx
	movl	%edx, %ebx
	js	.L1314
.L1315:
	movq	(%r15), %rdx
	leal	16(,%r8,8), %eax
	cltq
	movq	199(%rdx), %rdx
	movq	-1(%rax,%rdx), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1321
	movq	%r15, %rsi
	movq	%rcx, -1608(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-1608(%rbp), %rcx
	movq	%rax, %rsi
.L1322:
	movl	%ebx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r15
	salq	$32, %r15
	cmpq	41096(%r12), %r13
	je	.L1326
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1326:
	leaq	-1600(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1356
.L1282:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1357
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1351:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L1358
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L1359
.L1299:
	leaq	.LC50(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1291:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L1360
.L1293:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1350:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1361
.L1288:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1289
	movq	(%rdi), %rax
	call	*8(%rax)
.L1289:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1290
	movq	(%rdi), %rax
	call	*8(%rax)
.L1290:
	leaq	.LC52(%rip), %rax
	movq	%r13, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%r14, -1576(%rbp)
	movq	%rax, -1600(%rbp)
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1349:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1362
.L1286:
	movq	%r13, _ZZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic522(%rip)
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1321:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1363
.L1323:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1359:
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1299
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %r8d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1299
	je	.L1300
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1358:
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1348:
	movq	40960(%rsi), %rax
	movl	$663, %edx
	leaq	-1560(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -1568(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1283
	.p2align 4,,10
	.p2align 3
.L1353:
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rdx
	movq	%xmm3, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1314
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm3, %ebx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1314
	je	.L1315
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1360:
	movq	%r12, %rdi
	movq	%rax, -1608(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1608(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1352:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1361:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC52(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1362:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1354:
	movq	%r12, %rdi
	movl	%r8d, -1612(%rbp)
	movq	%rsi, -1608(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-1612(%rbp), %r8d
	movq	-1608(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1356:
	leaq	-1560(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1282
	.p2align 4,,10
	.p2align 3
.L1363:
	movq	%r12, %rdi
	movq	%rcx, -1608(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1608(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L1323
.L1357:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23049:
	.size	_ZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_WasmMemoryGrowEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC54:
	.string	"V8.Runtime_Runtime_WasmMemoryGrow"
	.align 8
.LC55:
	.string	"args[1].ToUint32(&delta_pages)"
	.section	.text._ZN2v88internalL28Stats_Runtime_WasmMemoryGrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_WasmMemoryGrowEiPmPNS0_7IsolateE, @function
_ZN2v88internalL28Stats_Runtime_WasmMemoryGrowEiPmPNS0_7IsolateE:
.LFB18917:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1414
.L1365:
	movq	_ZZN2v88internalL28Stats_Runtime_WasmMemoryGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic80(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1415
.L1367:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1416
.L1369:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L1417
.L1373:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1415:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1418
.L1368:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_WasmMemoryGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic80(%rip)
	jmp	.L1367
	.p2align 4,,10
	.p2align 3
.L1417:
	movq	-1(%rdx), %rax
	cmpw	$1100, 11(%rax)
	jne	.L1373
	movq	-8(%r13), %rax
	testb	$1, %al
	je	.L1374
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L1419
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	je	.L1420
.L1379:
	leaq	.LC55(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1374:
	sarq	$32, %rax
	movl	%eax, %r15d
	js	.L1379
.L1380:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1386
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	movq	0(%r13), %rdx
.L1386:
	movq	41112(%r12), %rdi
	movq	159(%rdx), %r13
	testq	%rdi, %rdi
	je	.L1387
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1388:
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	movq	%rax, %r13
	salq	$32, %r13
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1390
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L1390:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1393
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1393:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1421
.L1364:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1422
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1387:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1423
.L1389:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1416:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1424
.L1370:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1371
	movq	(%rdi), %rax
	call	*8(%rax)
.L1371:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1372
	movq	(%rdi), %rax
	call	*8(%rax)
.L1372:
	leaq	.LC54(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$653, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1420:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rax
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L1379
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %r15d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1379
	je	.L1380
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1419:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1421:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1364
	.p2align 4,,10
	.p2align 3
.L1418:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1424:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC54(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1423:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1389
.L1422:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18917:
	.size	_ZN2v88internalL28Stats_Runtime_WasmMemoryGrowEiPmPNS0_7IsolateE, .-_ZN2v88internalL28Stats_Runtime_WasmMemoryGrowEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC56:
	.string	"V8.Runtime_Runtime_WasmTableFill"
	.section	.rodata._ZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC57:
	.string	"args[1].ToUint32(&start)"
.LC58:
	.string	"args[3].ToUint32(&count)"
	.section	.text._ZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateE.isra.0:
.LFB23055:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1592, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1508
.L1426:
	movq	_ZZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic540(%rip), %r13
	testq	%r13, %r13
	je	.L1509
.L1428:
	movq	$0, -1600(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1510
.L1430:
	addl	$1, 41104(%r12)
	movq	41088(%r12), %rax
	movq	%r12, %rsi
	leaq	-1520(%rbp), %r15
	leaq	12448(%r12), %rdx
	movq	%r15, %rdi
	movq	41096(%r12), %r13
	movq	%rax, -1608(%rbp)
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1434
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1435:
	movq	(%rbx), %rdx
	testb	$1, %dl
	jne	.L1511
	sarq	$32, %rdx
	movl	%edx, %r10d
	js	.L1441
.L1442:
	movq	-8(%rbx), %rax
	testb	$1, %al
	jne	.L1512
	sarq	$32, %rax
	movl	%eax, %r14d
	js	.L1452
.L1453:
	movq	41112(%r12), %rdi
	movq	-16(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L1459
	movl	%r10d, -1616(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-1616(%rbp), %r10d
	movq	%rax, %rcx
.L1460:
	movq	-24(%rbx), %rdx
	testb	$1, %dl
	je	.L1462
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L1513
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L1514
.L1466:
	leaq	.LC58(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1512:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1515
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1516
.L1452:
	leaq	.LC57(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L1517
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L1518
.L1441:
	leaq	.LC50(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1434:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L1519
.L1436:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1462:
	sarq	$32, %rdx
	movl	%edx, %ebx
	js	.L1466
.L1467:
	movq	(%r15), %rdx
	leal	16(,%r10,8), %eax
	cltq
	movq	199(%rdx), %rdx
	movq	-1(%rax,%rdx), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1473
	movq	%r8, %rsi
	movq	%rcx, -1616(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-1616(%rbp), %rcx
	movq	(%rax), %r8
	movq	%rax, %rsi
.L1474:
	movq	23(%r8), %rax
	movslq	11(%rax), %r8
	cmpl	%r14d, %r8d
	jb	.L1507
	subl	%r14d, %r8d
	cmpl	%ebx, %r8d
	jb	.L1520
	movl	%ebx, %r8d
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject4FillEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEEj@PLT
	movq	88(%r12), %r15
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1520:
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject4FillEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEEj@PLT
.L1507:
	cmpq	$0, 12464(%r12)
	je	.L1484
.L1485:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$352, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r15
.L1478:
	subl	$1, 41104(%r12)
	movq	-1608(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1483
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1483:
	leaq	-1600(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1521
.L1425:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1522
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1510:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1523
.L1431:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1432
	movq	(%rdi), %rax
	call	*8(%rax)
.L1432:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1433
	movq	(%rdi), %rax
	call	*8(%rax)
.L1433:
	leaq	.LC56(%rip), %rax
	movq	%r13, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%r14, -1576(%rbp)
	movq	%rax, -1600(%rbp)
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1509:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1524
.L1429:
	movq	%r13, _ZZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic540(%rip)
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L1525
.L1461:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1473:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1526
.L1475:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1474
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	(%r15), %rax
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1514:
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm4
	addsd	%xmm1, %xmm4
	movq	%xmm4, %rdx
	movq	%xmm4, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1466
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm4, %ebx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1466
	je	.L1467
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1518:
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1441
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %r10d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1441
	je	.L1442
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1517:
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1508:
	movq	40960(%rsi), %rax
	movl	$664, %edx
	leaq	-1560(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -1568(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1516:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rdx
	movq	%xmm3, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1452
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm3, %r14d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1452
	je	.L1453
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1519:
	movq	%r12, %rdi
	movq	%rax, -1616(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1616(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1436
	.p2align 4,,10
	.p2align 3
.L1515:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1524:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1523:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC56(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1431
	.p2align 4,,10
	.p2align 3
.L1513:
	leaq	.LC16(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1525:
	movq	%r12, %rdi
	movl	%r10d, -1624(%rbp)
	movq	%rsi, -1616(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-1624(%rbp), %r10d
	movq	-1616(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1521:
	leaq	-1560(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1526:
	movq	%r12, %rdi
	movq	%r8, -1624(%rbp)
	movq	%rcx, -1616(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1624(%rbp), %r8
	movq	-1616(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L1475
.L1522:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23055:
	.size	_ZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal31Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE:
.LFB18915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1535
	movl	41104(%rdx), %eax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rsi), %rdi
	cmpq	%rdi, 104(%r12)
	jne	.L1536
	movl	%eax, 41104(%r12)
	movabsq	$4294967296, %r13
.L1527:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1536:
	.cfi_restore_state
	call	_ZN2v88internal20WasmExportedFunction22IsWasmExportedFunctionENS0_6ObjectE@PLT
	testb	%al, %al
	movl	41104(%r12), %eax
	jne	.L1537
	movq	41096(%r12), %rdx
	subl	$1, %eax
	xorl	%r13d, %r13d
.L1531:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L1527
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1537:
	.cfi_restore_state
	movq	41096(%r12), %rdx
	subl	$1, %eax
	movabsq	$4294967296, %r13
	jmp	.L1531
	.p2align 4,,10
	.p2align 3
.L1535:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Stats_Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18915:
	.size	_ZN2v88internal31Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_WasmMemoryGrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_WasmMemoryGrowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_WasmMemoryGrowEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_WasmMemoryGrowEiPmPNS0_7IsolateE:
.LFB18918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1562
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L1563
.L1540:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1563:
	movq	-1(%rdx), %rax
	cmpw	$1100, 11(%rax)
	jne	.L1540
	movq	-8(%rsi), %rax
	testb	$1, %al
	je	.L1541
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L1564
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	je	.L1565
.L1546:
	leaq	.LC55(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1541:
	sarq	$32, %rax
	movl	%eax, %r14d
	js	.L1546
.L1547:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1553
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	movq	(%rsi), %rdx
.L1553:
	movq	41112(%r12), %rdi
	movq	159(%rdx), %r15
	testq	%rdi, %rdi
	je	.L1554
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1555:
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	movq	%rax, %r14
	salq	$32, %r14
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1557
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L1557:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1538
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1538:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1554:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1566
.L1556:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1562:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_WasmMemoryGrowEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1565:
	.cfi_restore_state
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rax
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L1546
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %r14d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1546
	je	.L1547
	jmp	.L1546
	.p2align 4,,10
	.p2align 3
.L1564:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1566:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1556
	.cfi_endproc
.LFE18918:
	.size	_ZN2v88internal22Runtime_WasmMemoryGrowEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_WasmMemoryGrowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE:
.LFB18921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1574
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	jne	.L1575
.L1569:
	movq	(%rdi), %rsi
	testb	$1, %sil
	jne	.L1576
	addl	$1, 41104(%r12)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	sarq	$32, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1571
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1571:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1567
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L1567:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1575:
	.cfi_restore_state
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1574:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1576:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18921:
	.size	_ZN2v88internal22Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_ThrowWasmErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateE:
.LFB18924:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rdx, %rdi
	testl	%eax, %eax
	jne	.L1581
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1581:
	.cfi_restore 6
	jmp	_ZN2v88internalL36Stats_Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18924:
	.size	_ZN2v88internal30Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateE:
.LFB18927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1586
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	$349, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1584
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1584:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1586:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18927:
	.size	_ZN2v88internal26Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_WasmThrowCreateEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_WasmThrowCreateEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_WasmThrowCreateEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_WasmThrowCreateEiPmPNS0_7IsolateE:
.LFB18930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1599
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L1589
.L1590:
	leaq	.LC25(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	-1(%rsi), %rax
	cmpw	$106, 11(%rax)
	jne	.L1590
	movq	-8(%r13), %r15
	testb	$1, %r15b
	jne	.L1600
	movq	41112(%r12), %rdi
	sarq	$32, %r15
	movq	%r15, -56(%rbp)
	testq	%rdi, %rdi
	je	.L1592
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1593:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$353, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	leaq	3824(%r12), %rdx
	movl	$1, %r9d
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L1601
	movl	-56(%rbp), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	3832(%r12), %rdx
	movl	$1, %r9d
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L1602
	movq	0(%r13), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1587
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1587:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1592:
	.cfi_restore_state
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L1603
.L1594:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1599:
	addq	$24, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1600:
	.cfi_restore_state
	leaq	.LC26(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1601:
	leaq	.LC27(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1602:
	leaq	.LC28(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1603:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1594
	.cfi_endproc
.LFE18930:
	.size	_ZN2v88internal23Runtime_WasmThrowCreateEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_WasmThrowCreateEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateE:
.LFB18933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1612
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE
	movq	41112(%r12), %rdi
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
	movq	0(%r13), %r13
	testq	%rdi, %rdi
	je	.L1606
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1607:
	movq	%r12, %rdi
	call	_ZN2v88internal20WasmExceptionPackage15GetExceptionTagEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1604
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1604:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1606:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1613
.L1608:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1612:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rdx, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1613:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1608
	.cfi_endproc
.LFE18933:
	.size	_ZN2v88internal27Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateE:
.LFB18936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1622
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE
	movq	41112(%r12), %rdi
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
	movq	0(%r13), %r13
	testq	%rdi, %rdi
	je	.L1616
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1617:
	movq	%r12, %rdi
	call	_ZN2v88internal20WasmExceptionPackage18GetExceptionValuesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1614
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1614:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1616:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1623
.L1618:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1622:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rdx, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Stats_Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1623:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1618
	.cfi_endproc
.LFE18936:
	.size	_ZN2v88internal30Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_WasmStackGuardEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_WasmStackGuardEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_WasmStackGuardEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_WasmStackGuardEiPmPNS0_7IsolateE:
.LFB18942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1633
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1627
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
.L1627:
	xorl	%esi, %esi
	leaq	-32(%rbp), %rdi
	movq	%r12, -32(%rbp)
	call	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm@PLT
	testb	%al, %al
	jne	.L1634
	leaq	37512(%r12), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
.L1629:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1624
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rdx
	movl	$1, %fs:(%rdx)
.L1624:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1635
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1634:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1633:
	movq	%rdx, %rdi
	call	_ZN2v88internalL28Stats_Runtime_WasmStackGuardEiPmPNS0_7IsolateE.isra.0
	jmp	.L1624
.L1635:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18942:
	.size	_ZN2v88internal22Runtime_WasmStackGuardEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_WasmStackGuardEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_WasmCompileLazyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_WasmCompileLazyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_WasmCompileLazyEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_WasmCompileLazyEiPmPNS0_7IsolateE:
.LFB18945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1648
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1638
.L1639:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1638:
	movq	-1(%rax), %rdx
	cmpw	$1100, 11(%rdx)
	jne	.L1639
	movq	-8(%rsi), %rdx
	testb	$1, %dl
	jne	.L1649
	sarq	$32, %rdx
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	movq	%rdx, %r14
	je	.L1641
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	movq	(%rsi), %rax
.L1641:
	movq	151(%rax), %rax
	movl	%r14d, %edx
	movq	%r12, %rdi
	movq	%rax, 12464(%r12)
	movq	(%rsi), %rax
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm11CompileLazyEPNS0_7IsolateEPNS1_12NativeModuleEi@PLT
	testb	%al, %al
	jne	.L1642
	movq	312(%r12), %r14
.L1643:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1644
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L1644:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1636
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1636:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1642:
	.cfi_restore_state
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal4wasm12NativeModule24GetCallTargetForFunctionEj@PLT
	movq	%rax, %r14
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1648:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL29Stats_Runtime_WasmCompileLazyEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1649:
	.cfi_restore_state
	leaq	.LC26(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18945:
	.size	_ZN2v88internal23Runtime_WasmCompileLazyEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_WasmCompileLazyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20getSharedArrayBufferENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_7IsolateEj,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20getSharedArrayBufferENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_7IsolateEj
	.type	_ZN2v88internal20getSharedArrayBufferENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_7IsolateEj, @function
_ZN2v88internal20getSharedArrayBufferENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_7IsolateEj:
.LFB18947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	41112(%rbx), %rdi
	movq	159(%rax), %rax
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1651
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1651:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1655
.L1653:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1655:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1653
	.cfi_endproc
.LFE18947:
	.size	_ZN2v88internal20getSharedArrayBufferENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_7IsolateEj, .-_ZN2v88internal20getSharedArrayBufferENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_7IsolateEj
	.section	.text._ZN2v88internal24Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE:
.LFB18949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r15d
	testl	%r15d, %r15d
	jne	.L1711
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	testb	$1, %dil
	jne	.L1658
.L1659:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1658:
	movq	-1(%rdi), %rax
	cmpw	$1100, 11(%rax)
	jne	.L1659
	movq	-8(%rsi), %rbx
	testb	$1, %bl
	je	.L1705
	movq	-1(%rbx), %rax
	cmpw	$65, 11(%rax)
	jne	.L1712
	movq	7(%rbx), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L1713
.L1665:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L1691
	movq	%rax, %rdx
	xorl	%ebx, %ebx
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L1714
	cmpl	$31, %ecx
	jg	.L1664
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rdx
	andq	%rax, %rbx
	addq	%rdx, %rbx
	salq	%cl, %rbx
	movl	%ebx, %ebx
.L1671:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %ebx
.L1664:
	movq	-16(%rsi), %rax
	testb	$1, %al
	je	.L1707
.L1717:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1715
	movq	7(%rax), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L1678
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1678
	comisd	.LC12(%rip), %xmm0
	jb	.L1678
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1678
	jne	.L1678
	movl	%edx, %r15d
	.p2align 4,,10
	.p2align 3
.L1677:
	movq	159(%rdi), %rax
	movq	41112(%r12), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1686
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L1687:
	movl	%r15d, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal14FutexEmulation4WakeENS0_6HandleINS0_13JSArrayBufferEEEmj@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r15
	cmpq	41096(%r12), %r13
	je	.L1709
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1709:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1678:
	.cfi_restore_state
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L1677
	movq	%rax, %rdx
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L1716
	cmpl	$31, %ecx
	jg	.L1677
	movabsq	$4503599627370495, %r15
	movabsq	$4503599627370496, %rdx
	andq	%rax, %r15
	addq	%rdx, %r15
	salq	%cl, %r15
	movl	%r15d, %r15d
.L1684:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %r15d
	jmp	.L1677
	.p2align 4,,10
	.p2align 3
.L1705:
	movq	-16(%rsi), %rax
	shrq	$32, %rbx
	testb	$1, %al
	jne	.L1717
.L1707:
	shrq	$32, %rax
	movq	%rax, %r15
	jmp	.L1677
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L1718
.L1688:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1713:
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1665
	comisd	.LC12(%rip), %xmm0
	jb	.L1665
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1665
	je	.L1664
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1716:
	cmpl	$-52, %ecx
	jl	.L1677
	movabsq	$4503599627370495, %r15
	movabsq	$4503599627370496, %rcx
	andq	%rax, %r15
	addq	%rcx, %r15
	movl	$1075, %ecx
	subl	%edx, %ecx
	shrq	%cl, %r15
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1714:
	cmpl	$-52, %ecx
	jl	.L1664
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rbx
	addq	%rcx, %rbx
	movl	$1075, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rbx
	jmp	.L1671
	.p2align 4,,10
	.p2align 3
.L1711:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1712:
	.cfi_restore_state
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1715:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1718:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1688
.L1691:
	xorl	%ebx, %ebx
	jmp	.L1664
	.cfi_endproc
.LFE18949:
	.size	_ZN2v88internal24Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal15WaitTimeoutInMsEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15WaitTimeoutInMsEd
	.type	_ZN2v88internal15WaitTimeoutInMsEd, @function
_ZN2v88internal15WaitTimeoutInMsEd:
.LFB18951:
	.cfi_startproc
	endbr64
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L1721
	divsd	.LC18(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L1721:
	movsd	.LC14(%rip), %xmm0
	ret
	.cfi_endproc
.LFE18951:
	.size	_ZN2v88internal15WaitTimeoutInMsEd, .-_ZN2v88internal15WaitTimeoutInMsEd
	.section	.text._ZN2v88internal25Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE:
.LFB18953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %ebx
	testl	%ebx, %ebx
	jne	.L1783
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	testb	$1, %dil
	jne	.L1724
.L1725:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1724:
	movq	-1(%rdi), %rax
	cmpw	$1100, 11(%rax)
	jne	.L1725
	movq	-8(%rsi), %rax
	testb	$1, %al
	je	.L1776
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1784
	movq	7(%rax), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L1785
.L1731:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L1730
	movq	%rax, %rdx
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L1786
	cmpl	$31, %ecx
	jg	.L1730
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rdx
	andq	%rax, %rbx
	addq	%rdx, %rbx
	salq	%cl, %rbx
	movl	%ebx, %ebx
.L1737:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %ebx
.L1730:
	movq	-16(%rsi), %r15
	testb	$1, %r15b
	je	.L1778
.L1790:
	movq	-1(%r15), %rax
	cmpw	$65, 11(%rax)
	jne	.L1787
	movq	7(%r15), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L1744
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1744
	comisd	.LC12(%rip), %xmm0
	jb	.L1744
	cvttsd2sil	%xmm0, %r15d
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%r15d, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1744
	je	.L1743
	.p2align 4,,10
	.p2align 3
.L1744:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L1763
	movq	%rax, %rdx
	xorl	%r15d, %r15d
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L1788
	cmpl	$31, %ecx
	jg	.L1743
	movabsq	$4503599627370495, %r15
	movabsq	$4503599627370496, %rdx
	andq	%rax, %r15
	addq	%rdx, %r15
	salq	%cl, %r15
	movl	%r15d, %r15d
.L1749:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %r15d
	movq	-24(%rsi), %rax
	testb	$1, %al
	jne	.L1789
.L1780:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L1755
	.p2align 4,,10
	.p2align 3
.L1776:
	movq	-16(%rsi), %r15
	shrq	$32, %rax
	movq	%rax, %rbx
	testb	$1, %r15b
	jne	.L1790
.L1778:
	shrq	$32, %r15
.L1743:
	movq	-24(%rsi), %rax
	testb	$1, %al
	je	.L1780
.L1789:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1791
	movsd	7(%rax), %xmm0
.L1755:
	pxor	%xmm2, %xmm2
	movsd	.LC14(%rip), %xmm1
	comisd	%xmm0, %xmm2
	ja	.L1756
	divsd	.LC18(%rip), %xmm0
	movapd	%xmm0, %xmm1
.L1756:
	movq	159(%rdi), %rax
	movq	41112(%r12), %rdi
	movq	23(%rax), %r8
	testq	%rdi, %rdi
	je	.L1757
	movq	%r8, %rsi
	movsd	%xmm1, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movsd	-56(%rbp), %xmm1
	movq	%rax, %rsi
.L1758:
	movl	%r15d, %ecx
	movl	%ebx, %edx
	movapd	%xmm1, %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal14FutexEmulation6Wait32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r15
	cmpq	41096(%r12), %r13
	je	.L1781
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1781:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1757:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1792
.L1759:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1785:
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1731
	comisd	.LC12(%rip), %xmm0
	jb	.L1731
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1731
	jne	.L1731
	movl	%edx, %ebx
	jmp	.L1730
	.p2align 4,,10
	.p2align 3
.L1788:
	cmpl	$-52, %ecx
	jl	.L1743
	movabsq	$4503599627370495, %r15
	movabsq	$4503599627370496, %rcx
	andq	%rax, %r15
	addq	%rcx, %r15
	movl	$1075, %ecx
	subl	%edx, %ecx
	shrq	%cl, %r15
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L1786:
	cmpl	$-52, %ecx
	jl	.L1730
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rbx
	addq	%rcx, %rbx
	movl	$1075, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rbx
	jmp	.L1737
	.p2align 4,,10
	.p2align 3
.L1783:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1784:
	.cfi_restore_state
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1787:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1791:
	leaq	.LC16(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1792:
	movq	%r12, %rdi
	movq	%r8, -64(%rbp)
	movsd	%xmm1, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %r8
	movsd	-56(%rbp), %xmm1
	movq	%rax, %rsi
	jmp	.L1759
.L1763:
	xorl	%r15d, %r15d
	jmp	.L1743
	.cfi_endproc
.LFE18953:
	.size	_ZN2v88internal25Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE:
.LFB18956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r8d
	testl	%r8d, %r8d
	jne	.L1878
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rdi
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	testb	$1, %dil
	jne	.L1795
.L1796:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1795:
	movq	-1(%rdi), %rax
	cmpw	$1100, 11(%rax)
	jne	.L1796
	movq	-8(%rsi), %rdx
	testb	$1, %dl
	je	.L1869
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L1879
	movq	7(%rdx), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L1880
.L1802:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L1847
	movq	%rax, %rdx
	xorl	%r14d, %r14d
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L1881
	cmpl	$31, %ecx
	jg	.L1801
	movabsq	$4503599627370495, %r14
	movabsq	$4503599627370496, %rdx
	andq	%rax, %r14
	addq	%rdx, %r14
	salq	%cl, %r14
	movl	%r14d, %r14d
.L1808:
	cqto
	orl	$1, %edx
	imull	%edx, %r14d
.L1801:
	movq	-16(%rsi), %rcx
	testb	$1, %cl
	je	.L1871
.L1886:
	movq	-1(%rcx), %rax
	cmpw	$65, 11(%rax)
	jne	.L1882
	movq	7(%rcx), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L1815
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1815
	comisd	.LC12(%rip), %xmm0
	jb	.L1815
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1815
	jne	.L1815
	movl	%ecx, %edx
	jmp	.L1814
	.p2align 4,,10
	.p2align 3
.L1815:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L1851
	movq	%rax, %r9
	xorl	%edx, %edx
	shrq	$52, %r9
	andl	$2047, %r9d
	movl	%r9d, %ecx
	subl	$1075, %ecx
	js	.L1883
	cmpl	$31, %ecx
	jg	.L1814
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %r9
	andq	%rax, %rdx
	addq	%r9, %rdx
	salq	%cl, %rdx
	movl	%edx, %edx
.L1821:
	sarq	$63, %rax
	movq	%rax, %rcx
	movq	-24(%rsi), %rax
	orl	$1, %ecx
	imull	%ecx, %edx
	testb	$1, %al
	jne	.L1884
.L1873:
	shrq	$32, %rax
	movq	%rax, %r8
.L1827:
	movq	-32(%rsi), %rax
	testb	$1, %al
	je	.L1875
.L1889:
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L1885
	movsd	7(%rax), %xmm0
.L1840:
	salq	$32, %rdx
	movl	%r8d, %ecx
	pxor	%xmm2, %xmm2
	movsd	.LC14(%rip), %xmm1
	orq	%rdx, %rcx
	comisd	%xmm0, %xmm2
	ja	.L1841
	divsd	.LC18(%rip), %xmm0
	movapd	%xmm0, %xmm1
.L1841:
	movq	159(%rdi), %rax
	movq	41112(%r12), %rdi
	movq	23(%rax), %r15
	testq	%rdi, %rdi
	je	.L1842
	movq	%r15, %rsi
	movq	%rcx, -56(%rbp)
	movsd	%xmm1, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rcx
	movsd	-64(%rbp), %xmm1
	movq	%rax, %rsi
.L1843:
	movl	%r14d, %edx
	movapd	%xmm1, %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal14FutexEmulation6Wait64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld@PLT
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %r13
	je	.L1876
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1876:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1869:
	.cfi_restore_state
	movq	-16(%rsi), %rcx
	shrq	$32, %rdx
	movq	%rdx, %r14
	testb	$1, %cl
	jne	.L1886
.L1871:
	shrq	$32, %rcx
	movq	%rcx, %rdx
.L1814:
	movq	-24(%rsi), %rax
	testb	$1, %al
	je	.L1873
.L1884:
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L1887
	movq	7(%rax), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L1828
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1828
	comisd	.LC12(%rip), %xmm0
	jb	.L1828
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1828
	jne	.L1828
	movl	%ecx, %r8d
	jmp	.L1827
	.p2align 4,,10
	.p2align 3
.L1828:
	movabsq	$9218868437227405312, %rcx
	testq	%rcx, %rax
	je	.L1827
	movq	%rax, %r9
	shrq	$52, %r9
	andl	$2047, %r9d
	movl	%r9d, %ecx
	subl	$1075, %ecx
	js	.L1888
	cmpl	$31, %ecx
	jg	.L1827
	movabsq	$4503599627370495, %r8
	movabsq	$4503599627370496, %r9
	andq	%rax, %r8
	addq	%r9, %r8
	salq	%cl, %r8
	movl	%r8d, %ecx
.L1834:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%ecx, %eax
	movl	%eax, %r8d
	movq	-32(%rsi), %rax
	testb	$1, %al
	jne	.L1889
.L1875:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L1840
	.p2align 4,,10
	.p2align 3
.L1842:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1890
.L1844:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L1843
	.p2align 4,,10
	.p2align 3
.L1880:
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1802
	comisd	.LC12(%rip), %xmm0
	jb	.L1802
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1802
	jne	.L1802
	movl	%edx, %r14d
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1883:
	cmpl	$-52, %ecx
	jl	.L1814
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rdx
	addq	%rcx, %rdx
	movl	$1075, %ecx
	subl	%r9d, %ecx
	shrq	%cl, %rdx
	jmp	.L1821
	.p2align 4,,10
	.p2align 3
.L1888:
	cmpl	$-52, %ecx
	jl	.L1827
	movabsq	$4503599627370495, %r8
	movabsq	$4503599627370496, %rcx
	andq	%rax, %r8
	addq	%rcx, %r8
	movl	$1075, %ecx
	subl	%r9d, %ecx
	shrq	%cl, %r8
	movq	%r8, %rcx
	jmp	.L1834
	.p2align 4,,10
	.p2align 3
.L1885:
	leaq	.LC20(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1881:
	cmpl	$-52, %ecx
	jl	.L1801
	movabsq	$4503599627370495, %r14
	movabsq	$4503599627370496, %rcx
	andq	%rax, %r14
	addq	%rcx, %r14
	movl	$1075, %ecx
	subl	%edx, %ecx
	shrq	%cl, %r14
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1878:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1879:
	.cfi_restore_state
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1882:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1887:
	leaq	.LC16(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1890:
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	movsd	%xmm1, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movsd	-64(%rbp), %xmm1
	movq	-56(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L1844
.L1847:
	xorl	%r14d, %r14d
	jmp	.L1801
.L1851:
	xorl	%edx, %edx
	jmp	.L1814
	.cfi_endproc
.LFE18956:
	.size	_ZN2v88internal25Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Runtime_WasmRefFuncEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_WasmRefFuncEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_WasmRefFuncEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_WasmRefFuncEiPmPNS0_7IsolateE:
.LFB18960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1914
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1893
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
.L1893:
	addl	$1, 41104(%r12)
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1894
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L1895:
	movq	151(%rsi), %rax
	movq	%rax, 12464(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	je	.L1912
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L1915
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L1916
.L1902:
	leaq	.LC44(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1894:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L1917
.L1896:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1895
	.p2align 4,,10
	.p2align 3
.L1912:
	sarq	$32, %rdx
	movl	%edx, %r9d
	js	.L1902
.L1903:
	movl	%r9d, %edx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18WasmInstanceObject31GetOrCreateWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1911
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1911:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1891
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L1891:
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1914:
	.cfi_restore_state
	addq	$16, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL25Stats_Runtime_WasmRefFuncEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1916:
	.cfi_restore_state
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1902
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %r9d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1902
	je	.L1903
	jmp	.L1902
	.p2align 4,,10
	.p2align 3
.L1915:
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1917:
	movq	%r12, %rdi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1896
	.cfi_endproc
.LFE18960:
	.size	_ZN2v88internal19Runtime_WasmRefFuncEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_WasmRefFuncEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateE:
.LFB18963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1958
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	jne	.L1959
.L1920:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L1921
.L1922:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1959:
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	jmp	.L1920
	.p2align 4,,10
	.p2align 3
.L1921:
	movq	-1(%rdx), %rax
	cmpw	$1100, 11(%rax)
	jne	.L1922
	movq	-8(%rsi), %rax
	testb	$1, %al
	je	.L1955
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L1960
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	je	.L1961
.L1928:
	leaq	.LC46(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1955:
	sarq	$32, %rax
	movl	%eax, %ecx
	js	.L1928
.L1929:
	movq	-16(%rsi), %rax
	testb	$1, %al
	je	.L1956
	movq	-1(%rax), %rsi
	cmpw	$65, 11(%rsi)
	jne	.L1962
	movq	-1(%rax), %rsi
	cmpw	$65, 11(%rsi)
	je	.L1963
.L1940:
	leaq	.LC47(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1956:
	sarq	$32, %rax
	movl	%eax, %r15d
	js	.L1940
.L1941:
	movq	199(%rdx), %rdx
	leal	16(,%rcx,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1947
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L1948:
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject10IsInBoundsEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	testb	%al, %al
	jne	.L1950
	movl	$352, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114ThrowWasmErrorEPNS0_7IsolateENS0_15MessageTemplateE
	movq	%rax, %r14
.L1951:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1954
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1954:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1918
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L1918:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1950:
	.cfi_restore_state
	movq	%r14, %rsi
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject3GetEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	movq	(%rax), %r14
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1947:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L1964
.L1949:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L1948
	.p2align 4,,10
	.p2align 3
.L1963:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rsi
	movq	%xmm3, %rax
	shrq	$32, %rsi
	cmpq	$1127219200, %rsi
	jne	.L1940
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm3, %r15d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1940
	je	.L1941
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L1958:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL34Stats_Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1961:
	.cfi_restore_state
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rax
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L1928
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %ecx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1928
	je	.L1929
	jmp	.L1928
	.p2align 4,,10
	.p2align 3
.L1960:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1962:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1964:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1949
	.cfi_endproc
.LFE18963:
	.size	_ZN2v88internal28Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateE:
.LFB18966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2008
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	jne	.L2009
.L1967:
	movq	41088(%r12), %rax
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	%rax, -56(%rbp)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1968
.L1969:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2009:
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	jmp	.L1967
	.p2align 4,,10
	.p2align 3
.L1968:
	movq	-1(%rax), %rax
	cmpw	$1100, 11(%rax)
	jne	.L1969
	movq	-8(%r13), %rax
	testb	$1, %al
	je	.L2005
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2010
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2011
.L1975:
	leaq	.LC46(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2005:
	sarq	$32, %rax
	movl	%eax, %edx
	js	.L1975
.L1976:
	movq	-16(%r13), %rax
	testb	$1, %al
	je	.L2006
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L2012
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	je	.L2013
.L1987:
	leaq	.LC47(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2006:
	sarq	$32, %rax
	movl	%eax, %r15d
	js	.L1987
.L1988:
	movq	41112(%r12), %rdi
	movq	-24(%r13), %rsi
	testq	%rdi, %rdi
	je	.L1994
	movl	%edx, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-64(%rbp), %edx
	movq	%rax, %r14
.L1995:
	movq	0(%r13), %rsi
	leal	16(,%rdx,8), %eax
	cltq
	movq	199(%rsi), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1997
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L1998:
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject10IsInBoundsEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	testb	%al, %al
	jne	.L2000
	movl	$352, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114ThrowWasmErrorEPNS0_7IsolateENS0_15MessageTemplateE
	movq	%rax, %r13
.L2001:
	subl	$1, 41104(%r12)
	movq	-56(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2004
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2004:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1965
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L1965:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2000:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r14, %rcx
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE@PLT
	movq	88(%r12), %r13
	jmp	.L2001
	.p2align 4,,10
	.p2align 3
.L1997:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L2014
.L1999:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L1998
	.p2align 4,,10
	.p2align 3
.L1994:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L2015
.L1996:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L2013:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rcx
	movq	%xmm3, %rax
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L1987
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm3, %r15d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1987
	je	.L1988
	jmp	.L1987
	.p2align 4,,10
	.p2align 3
.L2008:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL34Stats_Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L2011:
	.cfi_restore_state
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1975
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %edx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1975
	je	.L1976
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L2010:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2012:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2014:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1999
	.p2align 4,,10
	.p2align 3
.L2015:
	movq	%r12, %rdi
	movl	%edx, -68(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-68(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1996
	.cfi_endproc
.LFE18966:
	.size	_ZN2v88internal28Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_WasmTableInitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_WasmTableInitEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_WasmTableInitEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_WasmTableInitEiPmPNS0_7IsolateE:
.LFB18969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1480, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2094
	addl	$1, 41104(%rdx)
	leaq	-1504(%rbp), %r15
	movq	%r12, %rsi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%r15, %rdi
	leaq	12448(%rdx), %rdx
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2019
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r15
	testb	$1, %dl
	je	.L2088
.L2098:
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L2095
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L2096
.L2027:
	leaq	.LC50(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2019:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L2097
.L2021:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L2098
.L2088:
	sarq	$32, %rdx
	movl	%edx, %r10d
	js	.L2027
.L2028:
	movq	-8(%r13), %rax
	testb	$1, %al
	je	.L2089
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2099
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2100
.L2039:
	leaq	.LC51(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2089:
	sarq	$32, %rax
	movl	%eax, %ecx
	js	.L2039
.L2040:
	movq	-16(%r13), %rax
	testb	$1, %al
	je	.L2090
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2101
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2102
.L2051:
	leaq	.LC39(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2090:
	sarq	$32, %rax
	movl	%eax, %r8d
	js	.L2051
.L2052:
	movq	-24(%r13), %rax
	testb	$1, %al
	je	.L2091
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2103
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2104
.L2063:
	leaq	.LC40(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2091:
	sarq	$32, %rax
	movl	%eax, %r9d
	js	.L2063
.L2064:
	movq	-32(%r13), %rax
	testb	$1, %al
	jne	.L2105
	sarq	$32, %rax
	movl	%eax, %edx
	js	.L2075
.L2076:
	movq	(%r15), %rax
	subq	$8, %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	151(%rax), %rax
	pushq	%rdx
	movl	%r10d, %edx
	movq	%rax, 12464(%r12)
	call	_ZN2v88internal18WasmInstanceObject16InitTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj@PLT
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	je	.L2106
	movq	88(%r12), %rax
.L2084:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2016
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-1512(%rbp), %rax
.L2016:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2107
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2105:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2108
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2109
.L2075:
	leaq	.LC41(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2106:
	cmpq	$0, 12464(%r12)
	jne	.L2083
	movq	(%r15), %rax
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
.L2083:
	xorl	%edx, %edx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$352, %esi
	call	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L2084
	.p2align 4,,10
	.p2align 3
.L2100:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rdx
	movq	%xmm3, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2039
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm3, %ecx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2039
	je	.L2040
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2101:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2102:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm4
	addsd	%xmm1, %xmm4
	movq	%xmm4, %rdx
	movq	%xmm4, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2051
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm4, %r8d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2051
	je	.L2052
	jmp	.L2051
	.p2align 4,,10
	.p2align 3
.L2094:
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internalL27Stats_Runtime_WasmTableInitEiPmPNS0_7IsolateE.isra.0
	jmp	.L2016
	.p2align 4,,10
	.p2align 3
.L2096:
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2027
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %r10d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2027
	je	.L2028
	jmp	.L2027
	.p2align 4,,10
	.p2align 3
.L2095:
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2097:
	movq	%r12, %rdi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1512(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L2021
	.p2align 4,,10
	.p2align 3
.L2099:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2103:
	leaq	.LC16(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2104:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm5
	addsd	%xmm1, %xmm5
	movq	%xmm5, %rdx
	movq	%xmm5, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2063
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm5, %r9d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2063
	je	.L2064
	jmp	.L2063
	.p2align 4,,10
	.p2align 3
.L2109:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm6
	addsd	%xmm1, %xmm6
	movq	%xmm6, %rdx
	movq	%xmm6, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2075
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm6, %edx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2075
	je	.L2076
	jmp	.L2075
	.p2align 4,,10
	.p2align 3
.L2108:
	leaq	.LC20(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18969:
	.size	_ZN2v88internal21Runtime_WasmTableInitEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_WasmTableInitEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_WasmTableCopyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_WasmTableCopyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_WasmTableCopyEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_WasmTableCopyEiPmPNS0_7IsolateE:
.LFB18972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1480, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2188
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rax
	movq	%r12, %rsi
	leaq	-1504(%rbp), %r15
	leaq	12448(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2113
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r15
	testb	$1, %dl
	je	.L2182
.L2192:
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L2189
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L2190
.L2121:
	leaq	.LC37(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2113:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L2191
.L2115:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L2192
.L2182:
	sarq	$32, %rdx
	movl	%edx, %r10d
	js	.L2121
.L2122:
	movq	-8(%r13), %rdx
	testb	$1, %dl
	je	.L2183
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L2193
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L2194
.L2133:
	leaq	.LC38(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2183:
	sarq	$32, %rdx
	movl	%edx, %ecx
	js	.L2133
.L2134:
	movq	-16(%r13), %rax
	testb	$1, %al
	je	.L2184
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2195
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2196
.L2145:
	leaq	.LC39(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2184:
	sarq	$32, %rax
	movl	%eax, %r8d
	js	.L2145
.L2146:
	movq	-24(%r13), %rax
	testb	$1, %al
	je	.L2185
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2197
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2198
.L2157:
	leaq	.LC40(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2185:
	sarq	$32, %rax
	movl	%eax, %r9d
	js	.L2157
.L2158:
	movq	-32(%r13), %rax
	testb	$1, %al
	jne	.L2199
	sarq	$32, %rax
	movl	%eax, %edx
	js	.L2169
.L2170:
	subq	$8, %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	%rdx
	movl	%r10d, %edx
	call	_ZN2v88internal18WasmInstanceObject16CopyTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj@PLT
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	je	.L2200
	movq	88(%r12), %rax
.L2178:
	subl	$1, 41104(%r12)
	movq	-1512(%rbp), %rcx
	movq	%rcx, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2110
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-1512(%rbp), %rax
.L2110:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2201
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2199:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2202
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2203
.L2169:
	leaq	.LC41(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2200:
	cmpq	$0, 12464(%r12)
	jne	.L2177
	movq	(%r15), %rax
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
.L2177:
	xorl	%edx, %edx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$352, %esi
	call	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2194:
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rdx
	movq	%xmm3, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2133
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm3, %ecx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2133
	je	.L2134
	jmp	.L2133
	.p2align 4,,10
	.p2align 3
.L2195:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2196:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm4
	addsd	%xmm1, %xmm4
	movq	%xmm4, %rdx
	movq	%xmm4, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2145
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm4, %r8d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2145
	je	.L2146
	jmp	.L2145
	.p2align 4,,10
	.p2align 3
.L2188:
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateE.isra.0
	jmp	.L2110
	.p2align 4,,10
	.p2align 3
.L2190:
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2121
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %r10d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2121
	je	.L2122
	jmp	.L2121
	.p2align 4,,10
	.p2align 3
.L2189:
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2191:
	movq	%r12, %rdi
	movq	%rax, -1520(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1520(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2193:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2197:
	leaq	.LC16(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2198:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm5
	addsd	%xmm1, %xmm5
	movq	%xmm5, %rdx
	movq	%xmm5, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2157
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm5, %r9d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2157
	je	.L2158
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2203:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm6
	addsd	%xmm1, %xmm6
	movq	%xmm6, %rdx
	movq	%xmm6, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2169
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm6, %edx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2169
	je	.L2170
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2202:
	leaq	.LC20(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2201:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18972:
	.size	_ZN2v88internal21Runtime_WasmTableCopyEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_WasmTableCopyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_WasmTableGrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_WasmTableGrowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_WasmTableGrowEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_WasmTableGrowEiPmPNS0_7IsolateE:
.LFB18975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2244
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2206
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r15
	testb	$1, %dl
	je	.L2241
.L2248:
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L2245
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L2246
.L2214:
	leaq	.LC50(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2206:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L2247
.L2208:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L2248
.L2241:
	sarq	$32, %rdx
	movl	%edx, %r8d
	js	.L2214
.L2215:
	movq	41112(%r12), %rdi
	movq	-8(%r13), %rsi
	testq	%rdi, %rdi
	je	.L2221
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-56(%rbp), %r8d
	movq	%rax, %rcx
.L2222:
	movq	-16(%r13), %rdx
	testb	$1, %dl
	je	.L2242
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L2249
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L2250
.L2229:
	leaq	.LC53(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2242:
	sarq	$32, %rdx
	movl	%edx, %r13d
	js	.L2229
.L2230:
	movq	(%r15), %rdx
	leal	16(,%r8,8), %eax
	cltq
	movq	199(%rdx), %rdx
	movq	-1(%rax,%rdx), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2236
	movq	%r15, %rsi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rsi
.L2237:
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	salq	$32, %r13
	cmpq	41096(%r12), %rbx
	je	.L2204
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2204:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2221:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L2251
.L2223:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2236:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2252
.L2238:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L2237
	.p2align 4,,10
	.p2align 3
.L2250:
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rdx
	movq	%xmm3, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2229
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm3, %r13d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2229
	je	.L2230
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2244:
	addq	$24, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2246:
	.cfi_restore_state
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2214
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %r8d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2214
	je	.L2215
	jmp	.L2214
	.p2align 4,,10
	.p2align 3
.L2245:
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2247:
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L2208
	.p2align 4,,10
	.p2align 3
.L2249:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2251:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movl	-56(%rbp), %r8d
	movq	%rax, %rcx
	jmp	.L2223
	.p2align 4,,10
	.p2align 3
.L2252:
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L2238
	.cfi_endproc
.LFE18975:
	.size	_ZN2v88internal21Runtime_WasmTableGrowEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_WasmTableGrowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_WasmTableFillEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_WasmTableFillEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_WasmTableFillEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_WasmTableFillEiPmPNS0_7IsolateE:
.LFB18978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2310
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rax
	movq	%rdx, %rdi
	movq	41096(%rdx), %rbx
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_125GetWasmInstanceOnStackTopEPNS0_7IsolateE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2255
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r15
	testb	$1, %dl
	je	.L2258
.L2314:
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L2311
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L2312
.L2262:
	leaq	.LC50(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2255:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L2313
.L2257:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L2314
.L2258:
	sarq	$32, %rdx
	movl	%edx, %r10d
	js	.L2262
.L2263:
	movq	-8(%r13), %rax
	testb	$1, %al
	je	.L2269
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2315
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2316
.L2273:
	leaq	.LC57(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2269:
	sarq	$32, %rax
	movl	%eax, %r14d
	js	.L2273
.L2274:
	movq	41112(%r12), %rdi
	movq	-16(%r13), %rsi
	testq	%rdi, %rdi
	je	.L2280
	movl	%r10d, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-64(%rbp), %r10d
	movq	%rax, %rcx
.L2281:
	movq	-24(%r13), %rdx
	testb	$1, %dl
	je	.L2283
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L2317
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L2318
.L2287:
	leaq	.LC58(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2283:
	sarq	$32, %rdx
	movl	%edx, %r13d
	js	.L2287
.L2288:
	movq	(%r15), %rdx
	leal	16(,%r10,8), %eax
	cltq
	movq	199(%rdx), %rdx
	movq	-1(%rax,%rdx), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2294
	movq	%r8, %rsi
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %rcx
	movq	(%rax), %r8
	movq	%rax, %rsi
.L2295:
	movq	23(%r8), %rax
	movslq	11(%rax), %r8
	cmpl	%r14d, %r8d
	jb	.L2309
	subl	%r14d, %r8d
	cmpl	%r13d, %r8d
	jb	.L2319
	movl	%r13d, %r8d
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject4FillEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEEj@PLT
	movq	88(%r12), %r13
	jmp	.L2299
	.p2align 4,,10
	.p2align 3
.L2319:
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject4FillEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEEj@PLT
.L2309:
	cmpq	$0, 12464(%r12)
	je	.L2304
.L2305:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$352, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2299:
	subl	$1, 41104(%r12)
	movq	-56(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2253
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2253:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2280:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L2320
.L2282:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L2281
	.p2align 4,,10
	.p2align 3
.L2294:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2321
.L2296:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2295
	.p2align 4,,10
	.p2align 3
.L2304:
	movq	(%r15), %rax
	movq	151(%rax), %rax
	movq	%rax, 12464(%r12)
	jmp	.L2305
	.p2align 4,,10
	.p2align 3
.L2316:
	movsd	7(%rax), %xmm1
	movsd	.LC36(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rdx
	movq	%xmm3, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2273
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm3, %r14d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2273
	je	.L2274
	jmp	.L2273
	.p2align 4,,10
	.p2align 3
.L2317:
	leaq	.LC16(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2318:
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm4
	addsd	%xmm1, %xmm4
	movq	%xmm4, %rdx
	movq	%xmm4, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2287
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm4, %r13d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2287
	je	.L2288
	jmp	.L2287
	.p2align 4,,10
	.p2align 3
.L2310:
	addq	$40, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2312:
	.cfi_restore_state
	movsd	7(%rdx), %xmm1
	movsd	.LC36(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2262
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %r10d
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2262
	je	.L2263
	jmp	.L2262
	.p2align 4,,10
	.p2align 3
.L2311:
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2313:
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2315:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2321:
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L2296
	.p2align 4,,10
	.p2align 3
.L2320:
	movq	%r12, %rdi
	movl	%r10d, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-72(%rbp), %r10d
	movq	-64(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L2282
	.cfi_endproc
.LFE18978:
	.size	_ZN2v88internal21Runtime_WasmTableFillEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_WasmTableFillEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE:
.LFB18981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2335
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2336
.L2326:
	leaq	-52(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	movl	$0, -52(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L2337
	movl	-52(%rbp), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2322
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2322:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2338
	addq	$32, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2336:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2326
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2335:
	movq	%rdx, %rsi
	call	_ZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r14
	jmp	.L2322
	.p2align 4,,10
	.p2align 3
.L2337:
	leaq	.LC31(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18981:
	.size	_ZN2v88internal36Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE, .-_ZN2v88internal36Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE:
.LFB18984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2348
	addl	$1, 41104(%rdx)
	movq	(%rsi), %r14
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	testb	$1, %r14b
	jne	.L2341
.L2342:
	leaq	.LC33(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2341:
	movq	-1(%r14), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L2342
	movq	41112(%rdx), %rdi
	movq	%r14, %r15
	testq	%rdi, %rdi
	je	.L2343
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
	movq	%rax, %rsi
.L2344:
	movslq	11(%r15), %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L2339
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2339:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2343:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L2349
.L2345:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2348:
	addq	$8, %rsp
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2349:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2345
	.cfi_endproc
.LFE18984:
	.size	_ZN2v88internal33Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8NewArrayINS0_4wasm9WasmValueEEEPT_m.str1.1,"aMS",@progbits,1
.LC59:
	.string	"NewArray"
	.section	.text._ZN2v88internal8NewArrayINS0_4wasm9WasmValueEEEPT_m,"axG",@progbits,_ZN2v88internal8NewArrayINS0_4wasm9WasmValueEEEPT_m,comdat
	.p2align 4
	.weak	_ZN2v88internal8NewArrayINS0_4wasm9WasmValueEEEPT_m
	.type	_ZN2v88internal8NewArrayINS0_4wasm9WasmValueEEEPT_m, @function
_ZN2v88internal8NewArrayINS0_4wasm9WasmValueEEEPT_m:
.LFB22008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movabsq	$542551296285575047, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	salq	$4, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	%rdi, %r12
	cmpq	%rax, %rdi
	movq	$-1, %rax
	cmova	%rax, %r12
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L2352
	subq	$1, %rbx
	movq	%rbx, %rdi
	js	.L2350
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2354:
	pxor	%xmm0, %xmm0
	subq	$1, %rdi
	movb	$0, (%rdx)
	addq	$17, %rdx
	movups	%xmm0, -16(%rdx)
	cmpq	$-1, %rdi
	jne	.L2354
.L2350:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2352:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L2365
	subq	$1, %rbx
	movq	%rbx, %rdi
	js	.L2350
	movq	%rax, %rdx
.L2355:
	pxor	%xmm0, %xmm0
	subq	$1, %rdi
	movb	$0, (%rdx)
	addq	$17, %rdx
	movups	%xmm0, -16(%rdx)
	cmpq	$-1, %rdi
	jne	.L2355
	jmp	.L2350
.L2365:
	leaq	.LC59(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE22008:
	.size	_ZN2v88internal8NewArrayINS0_4wasm9WasmValueEEEPT_m, .-_ZN2v88internal8NewArrayINS0_4wasm9WasmValueEEEPT_m
	.section	.rodata._ZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC60:
	.string	"V8.Runtime_Runtime_WasmRunInterpreter"
	.align 8
.LC61:
	.string	"!arg_buffer_obj->IsHeapObject()"
	.section	.rodata._ZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC62:
	.string	"unreachable code"
	.section	.text._ZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE.isra.0:
.LFB23046:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1672, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2462
.L2367:
	movq	_ZZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateEE28trace_event_unique_atomic168(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2463
.L2369:
	movq	$0, -1600(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2464
.L2371:
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	%rax, -1672(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -1656(%rbp)
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L2465
	shrq	$32, %rax
	movq	%rax, -1632(%rbp)
.L2379:
	movq	-8(%r12), %r14
	testb	$1, %r14b
	jne	.L2466
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L2388
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
.L2388:
	leaq	-1520(%rbp), %r12
	movq	%r15, %rsi
	leaq	12448(%r15), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal25WasmInterpreterEntryFrame13wasm_instanceEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2389
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L2390:
	movq	-104(%rbp), %rax
	movq	%r12, %rdi
	movq	%r9, -1624(%rbp)
	movq	32(%rax), %rax
	movq	%rax, -1680(%rbp)
	movq	(%r9), %rax
	movq	%rax, -1520(%rbp)
	call	_ZN2v88internal18WasmInstanceObject6moduleEv@PLT
	movq	%rax, %r8
	movslq	-1632(%rbp), %rax
	salq	$5, %rax
	addq	136(%r8), %rax
	movq	(%rax), %r13
	movq	8(%r13), %r12
	movslq	%r12d, %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8NewArrayINS0_4wasm9WasmValueEEEPT_m
	movq	%rbx, -1608(%rbp)
	movq	%rax, -1640(%rbp)
	movq	%rax, -1616(%rbp)
	movq	0(%r13), %rax
	movslq	%eax, %rbx
	movq	%rax, -1664(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal8NewArrayINS0_4wasm9WasmValueEEEPT_m
	testl	%r12d, %r12d
	movq	%rbx, -1512(%rbp)
	movq	-1624(%rbp), %r9
	movq	%rax, -1648(%rbp)
	movq	%rax, -1520(%rbp)
	jle	.L2406
	leal	-1(%r12), %r8d
	movq	%r14, %rdx
	xorl	%r12d, %r12d
	movq	-1640(%rbp), %rbx
	movq	%r9, -1688(%rbp)
	movq	%r12, %rax
	movq	%r15, %r9
	movq	%r13, %r12
	movq	%r14, -1696(%rbp)
	leaq	.L2397(%rip), %rcx
	movq	%r8, %r14
	movq	%rdx, %r15
	.p2align 4,,10
	.p2align 3
.L2461:
	movq	%rax, %r13
	movq	16(%r12), %rax
	addq	%r13, %rax
	addq	(%r12), %rax
	cmpb	$9, (%rax)
	ja	.L2395
	movzbl	(%rax), %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE.isra.0,"a",@progbits
	.align 4
	.align 4
.L2397:
	.long	.L2395-.L2397
	.long	.L2401-.L2397
	.long	.L2400-.L2397
	.long	.L2399-.L2397
	.long	.L2398-.L2397
	.long	.L2395-.L2397
	.long	.L2396-.L2397
	.long	.L2396-.L2397
	.long	.L2395-.L2397
	.long	.L2396-.L2397
	.section	.text._ZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2396:
	movq	41112(%r9), %rdi
	movq	(%r15), %rsi
	testq	%rdi, %rdi
	je	.L2403
	movq	%r9, -1624(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-1624(%rbp), %r9
	leaq	.L2397(%rip), %rcx
.L2404:
	movq	$0, -71(%rbp)
	addq	$8, %r15
	movq	%rax, -79(%rbp)
	movb	$6, -80(%rbp)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm7, (%rbx)
	movzbl	-64(%rbp), %eax
	movb	%al, 16(%rbx)
.L2402:
	leaq	1(%r13), %rax
	addq	$17, %rbx
	cmpq	%r13, %r14
	jne	.L2461
	movq	%r9, %r15
	movq	-1696(%rbp), %r14
	movq	-1688(%rbp), %r9
	movq	%r12, %r13
.L2406:
	movq	(%r9), %rax
	movq	%r9, %rdi
	movq	151(%rax), %rax
	movq	%rax, 12464(%r15)
	call	_ZN2v88internal18WasmInstanceObject20GetOrCreateDebugInfoENS0_6HandleIS1_EE@PLT
	pushq	-1512(%rbp)
	movq	%r15, %rdi
	movl	-1632(%rbp), %ecx
	movq	-1680(%rbp), %rdx
	pushq	-1520(%rbp)
	movq	%rax, %rsi
	movq	-1616(%rbp), %r8
	movq	-1608(%rbp), %r9
	call	_ZN2v88internal13WasmDebugInfo14RunInterpreterEPNS0_7IsolateENS0_6HandleIS1_EEmiNS0_6VectorINS0_4wasm9WasmValueEEES9_@PLT
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	je	.L2467
	movl	-1664(%rbp), %eax
	testl	%eax, %eax
	jle	.L2417
	movq	-1648(%rbp), %rax
	movq	16(%r13), %rdx
	leaq	.L2411(%rip), %rsi
	leaq	1(%rax), %rcx
	movq	-1664(%rbp), %rax
	leal	-1(%rax), %edi
	xorl	%eax, %eax
	cmpb	$9, (%rdx,%rax)
	ja	.L2395
	.p2align 4,,10
	.p2align 3
.L2468:
	movzbl	(%rdx,%rax), %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE.isra.0
	.align 4
	.align 4
.L2411:
	.long	.L2395-.L2411
	.long	.L2413-.L2411
	.long	.L2412-.L2411
	.long	.L2413-.L2411
	.long	.L2412-.L2411
	.long	.L2395-.L2411
	.long	.L2410-.L2411
	.long	.L2410-.L2411
	.long	.L2395-.L2411
	.long	.L2410-.L2411
	.section	.text._ZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2398:
	movq	(%r15), %rax
	movq	$0, -71(%rbp)
	addq	$8, %r15
	movb	$4, -80(%rbp)
	movq	%rax, -79(%rbp)
	movdqa	-80(%rbp), %xmm6
	movups	%xmm6, (%rbx)
	movzbl	-64(%rbp), %eax
	movb	%al, 16(%rbx)
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2399:
	movl	(%r15), %eax
	pxor	%xmm0, %xmm0
	movb	$3, -80(%rbp)
	addq	$4, %r15
	movups	%xmm0, -79(%rbp)
	movl	%eax, -79(%rbp)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm5, (%rbx)
	movzbl	-64(%rbp), %eax
	movb	%al, 16(%rbx)
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2400:
	movq	(%r15), %rax
	movq	$0, -71(%rbp)
	addq	$8, %r15
	movb	$2, -80(%rbp)
	movq	%rax, -79(%rbp)
	movdqa	-80(%rbp), %xmm4
	movups	%xmm4, (%rbx)
	movzbl	-64(%rbp), %eax
	movb	%al, 16(%rbx)
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2401:
	movl	(%r15), %eax
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	addq	$4, %r15
	movups	%xmm0, -79(%rbp)
	movl	%eax, -79(%rbp)
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, (%rbx)
	movzbl	-64(%rbp), %eax
	movb	%al, 16(%rbx)
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2410:
	movq	(%rcx), %rdx
	addq	$8, %r14
	movq	(%rdx), %rdx
	movq	%rdx, -8(%r14)
.L2416:
	leaq	1(%rax), %rdx
	addq	$17, %rcx
	cmpq	%rax, %rdi
	je	.L2417
.L2469:
	movq	%rdx, %rax
	movq	16(%r13), %rdx
	cmpb	$9, (%rdx,%rax)
	jbe	.L2468
	.p2align 4,,10
	.p2align 3
.L2395:
	leaq	.LC62(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2413:
	movl	(%rcx), %edx
	addq	$4, %r14
	addq	$17, %rcx
	movl	%edx, -4(%r14)
	leaq	1(%rax), %rdx
	cmpq	%rax, %rdi
	jne	.L2469
.L2417:
	movq	88(%r15), %r12
.L2408:
	movq	-1648(%rbp), %rax
	testq	%rax, %rax
	je	.L2419
	movq	%rax, %rdi
	call	_ZdaPv@PLT
.L2419:
	movq	-1640(%rbp), %rax
	testq	%rax, %rax
	je	.L2420
	movq	%rax, %rdi
	call	_ZdaPv@PLT
.L2420:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L2421
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L2421:
	movq	-1672(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-1656(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L2424
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2424:
	leaq	-1600(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2470
.L2366:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2471
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2412:
	.cfi_restore_state
	movq	(%rcx), %rdx
	addq	$8, %r14
	movq	%rdx, -8(%r14)
	jmp	.L2416
	.p2align 4,,10
	.p2align 3
.L2389:
	movq	41088(%r15), %r9
	cmpq	41096(%r15), %r9
	je	.L2472
.L2391:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r9)
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2403:
	movq	41088(%r9), %rax
	cmpq	41096(%r9), %rax
	je	.L2473
.L2405:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r9)
	movq	%rsi, (%rax)
	jmp	.L2404
	.p2align 4,,10
	.p2align 3
.L2465:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2474
	movq	7(%rax), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L2380
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L2475
.L2380:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L2428
	movl	$0, -1632(%rbp)
	movq	%rax, %rsi
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L2476
	cmpl	$31, %ecx
	jg	.L2379
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rsi
	andq	%rax, %rdx
	addq	%rsi, %rdx
	salq	%cl, %rdx
	movl	%edx, %edx
.L2385:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%edx, %eax
	movl	%eax, -1632(%rbp)
	jmp	.L2379
	.p2align 4,,10
	.p2align 3
.L2464:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2477
.L2372:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2373
	movq	(%rdi), %rax
	call	*8(%rax)
.L2373:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2374
	movq	(%rdi), %rax
	call	*8(%rax)
.L2374:
	leaq	.LC60(%rip), %rax
	movq	%rbx, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%r13, -1576(%rbp)
	movq	%rax, -1600(%rbp)
	jmp	.L2371
	.p2align 4,,10
	.p2align 3
.L2463:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2478
.L2370:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateEE28trace_event_unique_atomic168(%rip)
	jmp	.L2369
	.p2align 4,,10
	.p2align 3
.L2467:
	movq	312(%r15), %r12
	jmp	.L2408
	.p2align 4,,10
	.p2align 3
.L2473:
	movq	%r9, %rdi
	movq	%rsi, -1704(%rbp)
	movq	%r9, -1624(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1704(%rbp), %rsi
	movq	-1624(%rbp), %r9
	leaq	.L2397(%rip), %rcx
	jmp	.L2405
.L2475:
	comisd	.LC12(%rip), %xmm0
	jb	.L2380
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	movl	%esi, -1632(%rbp)
	ucomisd	%xmm1, %xmm0
	jp	.L2380
	je	.L2379
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2476:
	cmpl	$-52, %ecx
	jl	.L2379
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rdx
	addq	%rcx, %rdx
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rdx
	jmp	.L2385
.L2462:
	movq	40960(%rsi), %rax
	movl	$654, %edx
	leaq	-1560(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -1568(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2367
	.p2align 4,,10
	.p2align 3
.L2474:
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2466:
	leaq	.LC61(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2470:
	leaq	-1560(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2366
.L2472:
	movq	%r15, %rdi
	movq	%rax, -1624(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1624(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L2391
.L2478:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2370
.L2477:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC60(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2372
.L2428:
	movl	$0, -1632(%rbp)
	jmp	.L2379
.L2471:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23046:
	.size	_ZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal26Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE:
.LFB18939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1592, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2551
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -1592(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -1576(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2552
	shrq	$32, %rax
	movq	%rax, -1552(%rbp)
.L2486:
	movq	-8(%rdi), %r14
	testb	$1, %r14b
	jne	.L2553
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L2495
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
.L2495:
	leaq	-1520(%rbp), %r12
	movq	%r15, %rsi
	leaq	12448(%r15), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal25WasmInterpreterEntryFrame13wasm_instanceEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2496
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L2497:
	movq	-104(%rbp), %rax
	movq	%r12, %rdi
	movq	%r9, -1544(%rbp)
	movq	32(%rax), %rax
	movq	%rax, -1600(%rbp)
	movq	(%r9), %rax
	movq	%rax, -1520(%rbp)
	call	_ZN2v88internal18WasmInstanceObject6moduleEv@PLT
	movq	%rax, %r8
	movslq	-1552(%rbp), %rax
	salq	$5, %rax
	addq	136(%r8), %rax
	movq	(%rax), %r13
	movq	8(%r13), %r12
	movslq	%r12d, %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8NewArrayINS0_4wasm9WasmValueEEEPT_m
	movq	%rbx, -1528(%rbp)
	movq	%rax, -1560(%rbp)
	movq	%rax, -1536(%rbp)
	movq	0(%r13), %rax
	movslq	%eax, %rbx
	movq	%rax, -1584(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal8NewArrayINS0_4wasm9WasmValueEEEPT_m
	testl	%r12d, %r12d
	movq	%rbx, -1512(%rbp)
	movq	-1544(%rbp), %r9
	movq	%rax, -1568(%rbp)
	movq	%rax, -1520(%rbp)
	jle	.L2513
	leal	-1(%r12), %r8d
	movq	%r14, %rdx
	xorl	%r12d, %r12d
	movq	-1560(%rbp), %rbx
	movq	%r9, -1608(%rbp)
	movq	%r12, %rax
	movq	%r15, %r9
	movq	%r13, %r12
	movq	%r14, -1616(%rbp)
	leaq	.L2504(%rip), %rcx
	movq	%r8, %r14
	movq	%rdx, %r15
	.p2align 4,,10
	.p2align 3
.L2550:
	movq	%rax, %r13
	movq	16(%r12), %rax
	addq	%r13, %rax
	addq	(%r12), %rax
	cmpb	$9, (%rax)
	ja	.L2502
	movzbl	(%rax), %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal26Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L2504:
	.long	.L2502-.L2504
	.long	.L2508-.L2504
	.long	.L2507-.L2504
	.long	.L2506-.L2504
	.long	.L2505-.L2504
	.long	.L2502-.L2504
	.long	.L2503-.L2504
	.long	.L2503-.L2504
	.long	.L2502-.L2504
	.long	.L2503-.L2504
	.section	.text._ZN2v88internal26Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L2503:
	movq	41112(%r9), %rdi
	movq	(%r15), %rsi
	testq	%rdi, %rdi
	je	.L2510
	movq	%r9, -1544(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-1544(%rbp), %r9
	leaq	.L2504(%rip), %rcx
.L2511:
	movq	$0, -71(%rbp)
	addq	$8, %r15
	movq	%rax, -79(%rbp)
	movb	$6, -80(%rbp)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm7, (%rbx)
	movzbl	-64(%rbp), %eax
	movb	%al, 16(%rbx)
.L2509:
	leaq	1(%r13), %rax
	addq	$17, %rbx
	cmpq	%r13, %r14
	jne	.L2550
	movq	%r9, %r15
	movq	-1616(%rbp), %r14
	movq	-1608(%rbp), %r9
	movq	%r12, %r13
.L2513:
	movq	(%r9), %rax
	movq	%r9, %rdi
	movq	151(%rax), %rax
	movq	%rax, 12464(%r15)
	call	_ZN2v88internal18WasmInstanceObject20GetOrCreateDebugInfoENS0_6HandleIS1_EE@PLT
	pushq	-1512(%rbp)
	movq	%r15, %rdi
	movl	-1552(%rbp), %ecx
	movq	-1600(%rbp), %rdx
	pushq	-1520(%rbp)
	movq	%rax, %rsi
	movq	-1536(%rbp), %r8
	movq	-1528(%rbp), %r9
	call	_ZN2v88internal13WasmDebugInfo14RunInterpreterEPNS0_7IsolateENS0_6HandleIS1_EEmiNS0_6VectorINS0_4wasm9WasmValueEEES9_@PLT
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	je	.L2554
	movl	-1584(%rbp), %eax
	testl	%eax, %eax
	jle	.L2524
	movq	-1568(%rbp), %rax
	movq	16(%r13), %rdx
	leaq	.L2518(%rip), %rsi
	leaq	1(%rax), %rcx
	movq	-1584(%rbp), %rax
	leal	-1(%rax), %edi
	xorl	%eax, %eax
	cmpb	$9, (%rdx,%rax)
	ja	.L2502
	.p2align 4,,10
	.p2align 3
.L2555:
	movzbl	(%rdx,%rax), %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal26Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE
	.align 4
	.align 4
.L2518:
	.long	.L2502-.L2518
	.long	.L2520-.L2518
	.long	.L2519-.L2518
	.long	.L2520-.L2518
	.long	.L2519-.L2518
	.long	.L2502-.L2518
	.long	.L2517-.L2518
	.long	.L2517-.L2518
	.long	.L2502-.L2518
	.long	.L2517-.L2518
	.section	.text._ZN2v88internal26Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L2505:
	movq	(%r15), %rax
	movq	$0, -71(%rbp)
	addq	$8, %r15
	movb	$4, -80(%rbp)
	movq	%rax, -79(%rbp)
	movdqa	-80(%rbp), %xmm6
	movups	%xmm6, (%rbx)
	movzbl	-64(%rbp), %eax
	movb	%al, 16(%rbx)
	jmp	.L2509
	.p2align 4,,10
	.p2align 3
.L2506:
	movl	(%r15), %eax
	pxor	%xmm0, %xmm0
	movb	$3, -80(%rbp)
	addq	$4, %r15
	movups	%xmm0, -79(%rbp)
	movl	%eax, -79(%rbp)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm5, (%rbx)
	movzbl	-64(%rbp), %eax
	movb	%al, 16(%rbx)
	jmp	.L2509
	.p2align 4,,10
	.p2align 3
.L2507:
	movq	(%r15), %rax
	movq	$0, -71(%rbp)
	addq	$8, %r15
	movb	$2, -80(%rbp)
	movq	%rax, -79(%rbp)
	movdqa	-80(%rbp), %xmm4
	movups	%xmm4, (%rbx)
	movzbl	-64(%rbp), %eax
	movb	%al, 16(%rbx)
	jmp	.L2509
	.p2align 4,,10
	.p2align 3
.L2508:
	movl	(%r15), %eax
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	addq	$4, %r15
	movups	%xmm0, -79(%rbp)
	movl	%eax, -79(%rbp)
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, (%rbx)
	movzbl	-64(%rbp), %eax
	movb	%al, 16(%rbx)
	jmp	.L2509
	.p2align 4,,10
	.p2align 3
.L2517:
	movq	(%rcx), %rdx
	addq	$8, %r14
	movq	(%rdx), %rdx
	movq	%rdx, -8(%r14)
.L2523:
	leaq	1(%rax), %rdx
	addq	$17, %rcx
	cmpq	%rdi, %rax
	je	.L2524
.L2556:
	movq	%rdx, %rax
	movq	16(%r13), %rdx
	cmpb	$9, (%rdx,%rax)
	jbe	.L2555
	.p2align 4,,10
	.p2align 3
.L2502:
	leaq	.LC62(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2520:
	movl	(%rcx), %edx
	addq	$4, %r14
	addq	$17, %rcx
	movl	%edx, -4(%r14)
	leaq	1(%rax), %rdx
	cmpq	%rdi, %rax
	jne	.L2556
.L2524:
	movq	88(%r15), %r12
.L2515:
	movq	-1568(%rbp), %rax
	testq	%rax, %rax
	je	.L2526
	movq	%rax, %rdi
	call	_ZdaPv@PLT
.L2526:
	movq	-1560(%rbp), %rax
	testq	%rax, %rax
	je	.L2527
	movq	%rax, %rdi
	call	_ZdaPv@PLT
.L2527:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L2528
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L2528:
	movq	-1592(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-1576(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L2479
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2479:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2557
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2519:
	.cfi_restore_state
	movq	(%rcx), %rdx
	addq	$8, %r14
	movq	%rdx, -8(%r14)
	jmp	.L2523
	.p2align 4,,10
	.p2align 3
.L2552:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2558
	movq	7(%rax), %rax
	movsd	.LC10(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC9(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L2487
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L2559
.L2487:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L2532
	movl	$0, -1552(%rbp)
	movq	%rax, %rsi
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L2560
	cmpl	$31, %ecx
	jg	.L2486
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rsi
	andq	%rax, %rdx
	addq	%rsi, %rdx
	salq	%cl, %rdx
	movl	%edx, %edx
.L2492:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%edx, %eax
	movl	%eax, -1552(%rbp)
	jmp	.L2486
	.p2align 4,,10
	.p2align 3
.L2510:
	movq	41088(%r9), %rax
	cmpq	41096(%r9), %rax
	je	.L2561
.L2512:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r9)
	movq	%rsi, (%rax)
	jmp	.L2511
	.p2align 4,,10
	.p2align 3
.L2496:
	movq	41088(%r15), %r9
	cmpq	41096(%r15), %r9
	je	.L2562
.L2498:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r9)
	jmp	.L2497
	.p2align 4,,10
	.p2align 3
.L2554:
	movq	312(%r15), %r12
	jmp	.L2515
	.p2align 4,,10
	.p2align 3
.L2561:
	movq	%r9, %rdi
	movq	%rsi, -1624(%rbp)
	movq	%r9, -1544(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1624(%rbp), %rsi
	movq	-1544(%rbp), %r9
	leaq	.L2504(%rip), %rcx
	jmp	.L2512
.L2559:
	comisd	.LC12(%rip), %xmm0
	jb	.L2487
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	movl	%esi, -1552(%rbp)
	ucomisd	%xmm1, %xmm0
	jp	.L2487
	je	.L2486
	jmp	.L2487
	.p2align 4,,10
	.p2align 3
.L2560:
	cmpl	$-52, %ecx
	jl	.L2486
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rdx
	addq	%rcx, %rdx
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rdx
	jmp	.L2492
.L2551:
	movq	%rdx, %rsi
	call	_ZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r12
	jmp	.L2479
	.p2align 4,,10
	.p2align 3
.L2558:
	leaq	.LC30(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2553:
	leaq	.LC61(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2562:
	movq	%r15, %rdi
	movq	%rax, -1544(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1544(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L2498
.L2532:
	movl	$0, -1552(%rbp)
	jmp	.L2486
.L2557:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18939:
	.size	_ZN2v88internal26Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_WasmRunInterpreterEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal31Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal31Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal31Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE:
.LFB22998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22998:
	.size	_GLOBAL__sub_I__ZN2v88internal31Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal31Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal31Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic580,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic580, @object
	.size	_ZZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic580, 8
_ZZN2v88internalL39Stats_Runtime_WasmNewMultiReturnJSArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic580:
	.zero	8
	.section	.bss._ZZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic572,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic572, @object
	.size	_ZZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic572, 8
_ZZN2v88internalL42Stats_Runtime_WasmNewMultiReturnFixedArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic572:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic540,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic540, @object
	.size	_ZZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic540, 8
_ZZN2v88internalL27Stats_Runtime_WasmTableFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic540:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic522,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic522, @object
	.size	_ZZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic522, 8
_ZZN2v88internalL27Stats_Runtime_WasmTableGrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic522:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateEE28trace_event_unique_atomic503,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateEE28trace_event_unique_atomic503, @object
	.size	_ZZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateEE28trace_event_unique_atomic503, 8
_ZZN2v88internalL27Stats_Runtime_WasmTableCopyEiPmPNS0_7IsolateEE28trace_event_unique_atomic503:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_WasmTableInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic483,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_WasmTableInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic483, @object
	.size	_ZZN2v88internalL27Stats_Runtime_WasmTableInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic483, 8
_ZZN2v88internalL27Stats_Runtime_WasmTableInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic483:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateEE28trace_event_unique_atomic460,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateEE28trace_event_unique_atomic460, @object
	.size	_ZZN2v88internalL34Stats_Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateEE28trace_event_unique_atomic460, 8
_ZZN2v88internalL34Stats_Runtime_WasmFunctionTableSetEiPmPNS0_7IsolateEE28trace_event_unique_atomic460:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateEE28trace_event_unique_atomic440,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateEE28trace_event_unique_atomic440, @object
	.size	_ZZN2v88internalL34Stats_Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateEE28trace_event_unique_atomic440, 8
_ZZN2v88internalL34Stats_Runtime_WasmFunctionTableGetEiPmPNS0_7IsolateEE28trace_event_unique_atomic440:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_WasmRefFuncEiPmPNS0_7IsolateEE28trace_event_unique_atomic422,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_WasmRefFuncEiPmPNS0_7IsolateEE28trace_event_unique_atomic422, @object
	.size	_ZZN2v88internalL25Stats_Runtime_WasmRefFuncEiPmPNS0_7IsolateEE28trace_event_unique_atomic422, 8
_ZZN2v88internalL25Stats_Runtime_WasmRefFuncEiPmPNS0_7IsolateEE28trace_event_unique_atomic422:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic391,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic391, @object
	.size	_ZZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic391, 8
_ZZN2v88internalL31Stats_Runtime_WasmI64AtomicWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic391:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic377,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic377, @object
	.size	_ZZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic377, 8
_ZZN2v88internalL31Stats_Runtime_WasmI32AtomicWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic377:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateEE28trace_event_unique_atomic359,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateEE28trace_event_unique_atomic359, @object
	.size	_ZZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateEE28trace_event_unique_atomic359, 8
_ZZN2v88internalL30Stats_Runtime_WasmAtomicNotifyEiPmPNS0_7IsolateEE28trace_event_unique_atomic359:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_WasmCompileLazyEiPmPNS0_7IsolateEE28trace_event_unique_atomic311,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_WasmCompileLazyEiPmPNS0_7IsolateEE28trace_event_unique_atomic311, @object
	.size	_ZZN2v88internalL29Stats_Runtime_WasmCompileLazyEiPmPNS0_7IsolateEE28trace_event_unique_atomic311, 8
_ZZN2v88internalL29Stats_Runtime_WasmCompileLazyEiPmPNS0_7IsolateEE28trace_event_unique_atomic311:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_WasmStackGuardEiPmPNS0_7IsolateEE28trace_event_unique_atomic296,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_WasmStackGuardEiPmPNS0_7IsolateEE28trace_event_unique_atomic296, @object
	.size	_ZZN2v88internalL28Stats_Runtime_WasmStackGuardEiPmPNS0_7IsolateEE28trace_event_unique_atomic296, 8
_ZZN2v88internalL28Stats_Runtime_WasmStackGuardEiPmPNS0_7IsolateEE28trace_event_unique_atomic296:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateEE28trace_event_unique_atomic168,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateEE28trace_event_unique_atomic168, @object
	.size	_ZZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateEE28trace_event_unique_atomic168, 8
_ZZN2v88internalL32Stats_Runtime_WasmRunInterpreterEiPmPNS0_7IsolateEE28trace_event_unique_atomic168:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateEE28trace_event_unique_atomic156,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateEE28trace_event_unique_atomic156, @object
	.size	_ZZN2v88internalL36Stats_Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateEE28trace_event_unique_atomic156, 8
_ZZN2v88internalL36Stats_Runtime_WasmExceptionGetValuesEiPmPNS0_7IsolateEE28trace_event_unique_atomic156:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateEE28trace_event_unique_atomic144,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateEE28trace_event_unique_atomic144, @object
	.size	_ZZN2v88internalL33Stats_Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateEE28trace_event_unique_atomic144, 8
_ZZN2v88internalL33Stats_Runtime_WasmExceptionGetTagEiPmPNS0_7IsolateEE28trace_event_unique_atomic144:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateEE28trace_event_unique_atomic118,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateEE28trace_event_unique_atomic118, @object
	.size	_ZZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateEE28trace_event_unique_atomic118, 8
_ZZN2v88internalL29Stats_Runtime_WasmThrowCreateEiPmPNS0_7IsolateEE28trace_event_unique_atomic118:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic111,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic111, @object
	.size	_ZZN2v88internalL32Stats_Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic111, 8
_ZZN2v88internalL32Stats_Runtime_WasmThrowTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic111:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateEE28trace_event_unique_atomic105,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateEE28trace_event_unique_atomic105, @object
	.size	_ZZN2v88internalL36Stats_Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateEE28trace_event_unique_atomic105, 8
_ZZN2v88internalL36Stats_Runtime_ThrowWasmStackOverflowEiPmPNS0_7IsolateEE28trace_event_unique_atomic105:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic98,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic98, @object
	.size	_ZZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic98, 8
_ZZN2v88internalL28Stats_Runtime_ThrowWasmErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic98:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_WasmMemoryGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic80,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_WasmMemoryGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic80, @object
	.size	_ZZN2v88internalL28Stats_Runtime_WasmMemoryGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic80, 8
_ZZN2v88internalL28Stats_Runtime_WasmMemoryGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic80:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateEE27trace_event_unique_atomic66,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateEE27trace_event_unique_atomic66, @object
	.size	_ZZN2v88internalL37Stats_Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateEE27trace_event_unique_atomic66, 8
_ZZN2v88internalL37Stats_Runtime_WasmIsValidFuncRefValueEiPmPNS0_7IsolateEE27trace_event_unique_atomic66:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC9:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC10:
	.long	4294967295
	.long	2146435071
	.align 8
.LC11:
	.long	4290772992
	.long	1105199103
	.align 8
.LC12:
	.long	0
	.long	-1042284544
	.align 8
.LC14:
	.long	0
	.long	2146435072
	.align 8
.LC18:
	.long	0
	.long	1093567616
	.align 8
.LC36:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
