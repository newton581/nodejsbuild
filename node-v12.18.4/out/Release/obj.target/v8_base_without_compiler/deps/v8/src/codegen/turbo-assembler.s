	.file	"turbo-assembler.cc"
	.text
	.section	.text._ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,"axG",@progbits,_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.type	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, @function
_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv:
.LFB6897:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6897:
	.size	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, .-_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.section	.text._ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE
	.type	_ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE, @function
_ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE:
.LFB19101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movq	%rdx, %rsi
	leaq	-48(%rbp), %rdx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	movq	$0, (%r8)
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal9AssemblerC2ERKNS0_16AssemblerOptionsESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4
	movq	(%rdi), %rax
	call	*8(%rax)
.L4:
	leaq	16+_ZTVN2v88internal18TurboAssemblerBaseE(%rip), %rax
	movq	%r12, 512(%rbx)
	movq	%rax, (%rbx)
	movl	$1, %eax
	movq	$0, 520(%rbx)
	movw	%ax, 528(%rbx)
	movb	$0, 530(%rbx)
	movl	$-1, 532(%rbx)
	movb	$0, 536(%rbx)
	cmpl	$1, %r13d
	je	.L12
.L3:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L13
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	1128(%r12), %r13
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L14
.L6:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r13, (%rax)
	movq	%rax, 520(%rbx)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L6
.L13:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19101:
	.size	_ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE, .-_ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE
	.globl	_ZN2v88internal18TurboAssemblerBaseC1EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE
	.set	_ZN2v88internal18TurboAssemblerBaseC1EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE,_ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE
	.section	.rodata._ZN2v88internal18TurboAssemblerBase20IndirectLoadConstantENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"root_array_available_"
.LC1:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal18TurboAssemblerBase20IndirectLoadConstantENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"isolate()->IsGeneratingEmbeddedBuiltins()"
	.section	.text._ZN2v88internal18TurboAssemblerBase20IndirectLoadConstantENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18TurboAssemblerBase20IndirectLoadConstantENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal18TurboAssemblerBase20IndirectLoadConstantENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal18TurboAssemblerBase20IndirectLoadConstantENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEE:
.LFB19103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 528(%rdi)
	je	.L28
	movq	%rdi, %r13
	movq	512(%rdi), %rdi
	movl	%esi, %r14d
	movq	%rdx, %r12
	leaq	56(%rdi), %rax
	cmpq	%rdx, %rax
	ja	.L17
	leaq	4856(%rdi), %rdx
	cmpq	%r12, %rdx
	jbe	.L17
	movq	%r12, %rdx
	movq	%r13, %rdi
	subq	%rax, %rdx
	movq	0(%r13), %rax
	shrq	$3, %rdx
	call	*88(%rax)
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	addq	$41184, %rdi
	leaq	-44(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZNK2v88internal8Builtins15IsBuiltinHandleENS0_6HandleINS0_10HeapObjectEEEPi@PLT
	testb	%al, %al
	je	.L19
	movl	-44(%rbp), %eax
.L22:
	leal	24936(,%rax,8), %edx
	movq	0(%r13), %rax
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	*80(%rax)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L19:
	movq	520(%r13), %rax
	cmpq	%r12, %rax
	je	.L20
	testq	%rax, %rax
	je	.L21
	testq	%r12, %r12
	je	.L21
	movq	(%r12), %rcx
	cmpq	%rcx, (%rax)
	jne	.L21
.L20:
	movl	532(%r13), %eax
	cmpl	$1552, %eax
	jbe	.L22
.L21:
	movq	512(%r13), %rax
	movq	45520(%rax), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	%r12, %rsi
	call	_ZN2v88internal29BuiltinsConstantsTableBuilder9AddObjectENS0_6HandleINS0_6ObjectEEE@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	movl	%eax, %edx
	movq	0(%r13), %rax
	call	*64(%rax)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L29:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19103:
	.size	_ZN2v88internal18TurboAssemblerBase20IndirectLoadConstantENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal18TurboAssemblerBase20IndirectLoadConstantENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEE
	.section	.rodata._ZN2v88internal18TurboAssemblerBase29IndirectLoadExternalReferenceENS0_8RegisterENS0_17ExternalReferenceE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"!v.is_from_api()"
	.section	.text._ZN2v88internal18TurboAssemblerBase29IndirectLoadExternalReferenceENS0_8RegisterENS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18TurboAssemblerBase29IndirectLoadExternalReferenceENS0_8RegisterENS0_17ExternalReferenceE
	.type	_ZN2v88internal18TurboAssemblerBase29IndirectLoadExternalReferenceENS0_8RegisterENS0_17ExternalReferenceE, @function
_ZN2v88internal18TurboAssemblerBase29IndirectLoadExternalReferenceENS0_8RegisterENS0_17ExternalReferenceE:
.LFB19104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 528(%rdi)
	je	.L38
	movl	%esi, %r14d
	movq	512(%rdi), %rsi
	movq	%rdx, %r12
	movq	(%rdi), %rax
	movq	%rdi, %r13
	subq	%rsi, %rdx
	cmpq	$37583, %rdx
	ja	.L33
	leaq	-128(%r12), %rdx
	subq	%rsi, %rdx
	movl	%r14d, %esi
	call	*72(%rax)
.L31:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	leaq	-64(%rbp), %r15
	movq	80(%rax), %rbx
	movq	%r15, %rdi
	call	_ZN2v88internal24ExternalReferenceEncoderC1EPNS0_7IsolateE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24ExternalReferenceEncoder6EncodeEm@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L40
	movq	%r15, %rdi
	call	_ZN2v88internal24ExternalReferenceEncoderD1Ev@PLT
	leal	4728(,%r12,8), %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	*%rbx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19104:
	.size	_ZN2v88internal18TurboAssemblerBase29IndirectLoadExternalReferenceENS0_8RegisterENS0_17ExternalReferenceE, .-_ZN2v88internal18TurboAssemblerBase29IndirectLoadExternalReferenceENS0_8RegisterENS0_17ExternalReferenceE
	.section	.text._ZN2v88internal18TurboAssemblerBase30RootRegisterOffsetForRootIndexENS0_9RootIndexE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18TurboAssemblerBase30RootRegisterOffsetForRootIndexENS0_9RootIndexE
	.type	_ZN2v88internal18TurboAssemblerBase30RootRegisterOffsetForRootIndexENS0_9RootIndexE, @function
_ZN2v88internal18TurboAssemblerBase30RootRegisterOffsetForRootIndexENS0_9RootIndexE:
.LFB19105:
	.cfi_startproc
	endbr64
	movzwl	%di, %edi
	leal	-72(,%rdi,8), %eax
	ret
	.cfi_endproc
.LFE19105:
	.size	_ZN2v88internal18TurboAssemblerBase30RootRegisterOffsetForRootIndexENS0_9RootIndexE, .-_ZN2v88internal18TurboAssemblerBase30RootRegisterOffsetForRootIndexENS0_9RootIndexE
	.section	.text._ZN2v88internal18TurboAssemblerBase33RootRegisterOffsetForBuiltinIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18TurboAssemblerBase33RootRegisterOffsetForBuiltinIndexEi
	.type	_ZN2v88internal18TurboAssemblerBase33RootRegisterOffsetForBuiltinIndexEi, @function
_ZN2v88internal18TurboAssemblerBase33RootRegisterOffsetForBuiltinIndexEi:
.LFB19106:
	.cfi_startproc
	endbr64
	leal	24936(,%rdi,8), %eax
	ret
	.cfi_endproc
.LFE19106:
	.size	_ZN2v88internal18TurboAssemblerBase33RootRegisterOffsetForBuiltinIndexEi, .-_ZN2v88internal18TurboAssemblerBase33RootRegisterOffsetForBuiltinIndexEi
	.section	.text._ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE
	.type	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE, @function
_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE:
.LFB19107:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	addq	$-128, %rax
	subq	%rdi, %rax
	ret
	.cfi_endproc
.LFE19107:
	.size	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE, .-_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE
	.section	.text._ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE
	.type	_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE, @function
_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE:
.LFB19108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-32(%rbp), %r12
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal24ExternalReferenceEncoderC1EPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24ExternalReferenceEncoder6EncodeEm@PLT
	testl	%eax, %eax
	js	.L48
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal24ExternalReferenceEncoderD1Ev@PLT
	leal	4728(,%rbx,8), %eax
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L49
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19108:
	.size	_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE, .-_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE
	.section	.text._ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE
	.type	_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE, @function
_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE:
.LFB19109:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	subq	%rdi, %rax
	cmpq	$37583, %rax
	setbe	%al
	ret
	.cfi_endproc
.LFE19109:
	.size	_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE, .-_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE
	.section	.rodata._ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi.str1.1,"aMS",@progbits,1
.LC4:
	.string	"-- Inlined Trampoline to "
.LC5:
	.string	" --"
	.section	.rodata._ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi
	.type	_ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi, @function
_ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi:
.LFB19110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -520(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L71
.L51:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	.LC7(%rip), %xmm1
	leaq	-320(%rbp), %r14
	movq	%rdi, %rbx
	leaq	-432(%rbp), %r13
	movq	%r14, %rdi
	leaq	-368(%rbp), %r15
	movhps	.LC8(%rip), %xmm1
	movaps	%xmm1, -544(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r12
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	%r12, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%r12), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-544(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -544(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%r13, %rdi
	movl	$25, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-520(%rbp), %r8d
	movl	%r8d, %edi
	call	_ZN2v88internal8Builtins4nameEi@PLT
	testq	%rax, %rax
	je	.L73
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	call	strlen@PLT
	movq	-520(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L55:
	movq	%r13, %rdi
	movl	$3, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-384(%rbp), %rax
	leaq	-480(%rbp), %r13
	movq	$0, -488(%rbp)
	movq	%r13, -496(%rbp)
	leaq	-496(%rbp), %rdi
	movb	$0, -480(%rbp)
	testq	%rax, %rax
	je	.L56
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L57
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L58:
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	-496(%rbp), %r8
	jne	.L74
.L59:
	cmpq	%r13, %r8
	je	.L66
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L66:
	movq	.LC7(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC9(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-544(%rbp), %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r12, -432(%rbp)
	movq	-24(%r12), %rax
	movq	%r14, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L57:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	-496(%rbp), %r8
	je	.L59
.L74:
	leaq	40(%rbx), %rax
	movq	%rax, -552(%rbp)
	leaq	-464(%rbp), %rax
	movq	%rax, -528(%rbp)
	leaq	-448(%rbp), %rax
	movq	%rax, -520(%rbp)
	movq	%rax, -464(%rbp)
	testq	%r8, %r8
	je	.L75
	movq	%r8, %rdi
	movq	%r8, -560(%rbp)
	call	strlen@PLT
	movq	-560(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -504(%rbp)
	movq	%rax, %r9
	ja	.L76
	cmpq	$1, %rax
	jne	.L63
	movzbl	(%r8), %edx
	movb	%dl, -448(%rbp)
	movq	-520(%rbp), %rdx
.L64:
	movq	%rax, -456(%rbp)
	movq	-552(%rbp), %rdi
	movb	$0, (%rdx,%rax)
	movq	-528(%rbp), %rdx
	movq	32(%rbx), %rsi
	subq	16(%rbx), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-464(%rbp), %rdi
	cmpq	-520(%rbp), %rdi
	je	.L65
	call	_ZdlPv@PLT
.L65:
	movq	-496(%rbp), %r8
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L73:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L63:
	testq	%rax, %rax
	jne	.L77
	movq	-520(%rbp), %rdx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L76:
	movq	-528(%rbp), %rdi
	leaq	-504(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -568(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-560(%rbp), %r8
	movq	-568(%rbp), %r9
	movq	%rax, -464(%rbp)
	movq	%rax, %rdi
	movq	-504(%rbp), %rax
	movq	%rax, -448(%rbp)
.L62:
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-504(%rbp), %rax
	movq	-464(%rbp), %rdx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	.LC6(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L72:
	call	__stack_chk_fail@PLT
.L77:
	movq	-520(%rbp), %rdi
	jmp	.L62
	.cfi_endproc
.LFE19110:
	.size	_ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi, .-_ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE, @function
_GLOBAL__sub_I__ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE:
.LFB23078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23078:
	.size	_GLOBAL__sub_I__ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE, .-_GLOBAL__sub_I__ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE
	.weak	_ZTVN2v88internal18TurboAssemblerBaseE
	.section	.data.rel.ro._ZTVN2v88internal18TurboAssemblerBaseE,"awG",@progbits,_ZTVN2v88internal18TurboAssemblerBaseE,comdat
	.align 8
	.type	_ZTVN2v88internal18TurboAssemblerBaseE, @object
	.size	_ZTVN2v88internal18TurboAssemblerBaseE, 112
_ZTVN2v88internal18TurboAssemblerBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC7:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC8:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC9:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
