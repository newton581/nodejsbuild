	.file	"scanner-character-streams.cc"
	.text
	.section	.text._ZNK2v86String26ExternalStringResourceBase4LockEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase4LockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase4LockEv
	.type	_ZNK2v86String26ExternalStringResourceBase4LockEv, @function
_ZNK2v86String26ExternalStringResourceBase4LockEv:
.LFB2392:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2392:
	.size	_ZNK2v86String26ExternalStringResourceBase4LockEv, .-_ZNK2v86String26ExternalStringResourceBase4LockEv
	.section	.text._ZNK2v86String26ExternalStringResourceBase6UnlockEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase6UnlockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.type	_ZNK2v86String26ExternalStringResourceBase6UnlockEv, @function
_ZNK2v86String26ExternalStringResourceBase6UnlockEv:
.LFB2393:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2393:
	.size	_ZNK2v86String26ExternalStringResourceBase6UnlockEv, .-_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.section	.text._ZN2v88internal25RelocatingCharacterStream28UpdateBufferPointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv,"axG",@progbits,_ZN2v88internal25RelocatingCharacterStream28UpdateBufferPointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal25RelocatingCharacterStream28UpdateBufferPointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv
	.type	_ZN2v88internal25RelocatingCharacterStream28UpdateBufferPointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv, @function
_ZN2v88internal25RelocatingCharacterStream28UpdateBufferPointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv:
.LFB18530:
	.cfi_startproc
	endbr64
	movq	56(%rcx), %rax
	movq	72(%rcx), %rdi
	movq	64(%rcx), %rdx
	movq	8(%rcx), %r9
	movq	(%rax), %rax
	movq	%rdi, %rsi
	addq	$15, %rax
	cmpq	%rdi, 32(%rcx)
	cmovbe	32(%rcx), %rsi
	addq	%rdx, %rsi
	leaq	(%rax,%rsi,2), %rsi
	cmpq	%r9, %rsi
	je	.L4
	movq	16(%rcx), %r8
	addq	%rdi, %rdx
	movq	%rsi, 8(%rcx)
	leaq	(%rax,%rdx,2), %rax
	addq	%rsi, %r8
	movq	%rax, 24(%rcx)
	subq	%r9, %r8
	movq	%r8, 16(%rcx)
.L4:
	ret
	.cfi_endproc
.LFE18530:
	.size	_ZN2v88internal25RelocatingCharacterStream28UpdateBufferPointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv, .-_ZN2v88internal25RelocatingCharacterStream28UpdateBufferPointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv
	.section	.text._ZN2v88internal28BufferedUtf16CharacterStream9ReadBlockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28BufferedUtf16CharacterStream9ReadBlockEv
	.type	_ZN2v88internal28BufferedUtf16CharacterStream9ReadBlockEv, @function
_ZN2v88internal28BufferedUtf16CharacterStream9ReadBlockEv:
.LFB18535:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rsi
	movq	(%rdi), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	8(%rdi), %rsi
	sarq	%rsi
	addq	32(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	50(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, 16(%rdi)
	movq	%rsi, 32(%rdi)
	call	*48(%rax)
	leaq	(%r12,%rax,2), %rax
	cmpq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	popq	%rbx
	seta	%al
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18535:
	.size	_ZN2v88internal28BufferedUtf16CharacterStream9ReadBlockEv, .-_ZN2v88internal28BufferedUtf16CharacterStream9ReadBlockEv
	.section	.text._ZNK2v88internal27Utf8ExternalStreamingStream15can_access_heapEv,"axG",@progbits,_ZNK2v88internal27Utf8ExternalStreamingStream15can_access_heapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal27Utf8ExternalStreamingStream15can_access_heapEv
	.type	_ZNK2v88internal27Utf8ExternalStreamingStream15can_access_heapEv, @function
_ZNK2v88internal27Utf8ExternalStreamingStream15can_access_heapEv:
.LFB18556:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18556:
	.size	_ZNK2v88internal27Utf8ExternalStreamingStream15can_access_heapEv, .-_ZNK2v88internal27Utf8ExternalStreamingStream15can_access_heapEv
	.section	.text._ZNK2v88internal27Utf8ExternalStreamingStream13can_be_clonedEv,"axG",@progbits,_ZNK2v88internal27Utf8ExternalStreamingStream13can_be_clonedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal27Utf8ExternalStreamingStream13can_be_clonedEv
	.type	_ZNK2v88internal27Utf8ExternalStreamingStream13can_be_clonedEv, @function
_ZNK2v88internal27Utf8ExternalStreamingStream13can_be_clonedEv:
.LFB18557:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18557:
	.size	_ZNK2v88internal27Utf8ExternalStreamingStream13can_be_clonedEv, .-_ZNK2v88internal27Utf8ExternalStreamingStream13can_be_clonedEv
	.section	.text._ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED2Ev,"axG",@progbits,_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED2Ev
	.type	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED2Ev, @function
_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED2Ev:
.LFB22633:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22633:
	.size	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED2Ev, .-_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED2Ev
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED1Ev
	.set	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED1Ev,_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED2Ev
	.section	.text._ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED2Ev,"axG",@progbits,_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED2Ev
	.type	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED2Ev, @function
_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED2Ev:
.LFB22637:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22637:
	.size	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED2Ev, .-_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED2Ev
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED1Ev
	.set	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED1Ev,_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED2Ev
	.section	.text._ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv,"axG",@progbits,_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv
	.type	_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv, @function
_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv:
.LFB22673:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22673:
	.size	_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv, .-_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv
	.section	.text._ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv,"axG",@progbits,_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv
	.type	_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv, @function
_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv:
.LFB22675:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22675:
	.size	_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv, .-_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv
	.section	.text._ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv,"axG",@progbits,_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv
	.type	_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv, @function
_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv:
.LFB22678:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22678:
	.size	_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv, .-_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv
	.section	.text._ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv,"axG",@progbits,_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv
	.type	_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv, @function
_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv:
.LFB22680:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22680:
	.size	_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv, .-_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv
	.section	.text._ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE13can_be_clonedEv,"axG",@progbits,_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE13can_be_clonedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE13can_be_clonedEv
	.type	_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE13can_be_clonedEv, @function
_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE13can_be_clonedEv:
.LFB22683:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE22683:
	.size	_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE13can_be_clonedEv, .-_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE13can_be_clonedEv
	.section	.text._ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE15can_access_heapEv,"axG",@progbits,_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE15can_access_heapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE15can_access_heapEv
	.type	_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE15can_access_heapEv, @function
_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE15can_access_heapEv:
.LFB22685:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22685:
	.size	_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE15can_access_heapEv, .-_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE15can_access_heapEv
	.section	.text._ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE9ReadBlockEv,"axG",@progbits,_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE9ReadBlockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE9ReadBlockEv
	.type	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE9ReadBlockEv, @function
_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE9ReadBlockEv:
.LFB22686:
	.cfi_startproc
	endbr64
	movq	1088(%rdi), %rcx
	movq	16(%rdi), %rax
	leaq	50(%rdi), %r9
	subq	8(%rdi), %rax
	movq	1080(%rdi), %r8
	movq	%r9, %xmm0
	sarq	%rax
	addq	32(%rdi), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rcx, %rsi
	cmpq	%rax, %rcx
	movq	%rax, 32(%rdi)
	cmovbe	%rcx, %rax
	addq	%r8, %rcx
	movups	%xmm0, 8(%rdi)
	leaq	(%r8,%rax), %rdx
	subq	%rax, %rsi
	cmpq	%rcx, %rdx
	je	.L37
	cmpq	$512, %rsi
	movl	$512, %ecx
	cmova	%rcx, %rsi
	addq	%rsi, %rsi
	leaq	(%r9,%rsi), %rcx
	cmpq	%rcx, %r9
	jnb	.L34
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	subq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-51(%r10), %r12
	movl	$1, %r10d
	movq	%r12, %r11
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	51(%rdi), %rbx
	shrq	%r11
	cmpq	%rbx, %rcx
	leaq	1(%r11), %r13
	leaq	52(%r11,%r11), %r11
	cmovnb	%r13, %r10
	movl	$52, %r13d
	cmovb	%r13, %r11
	addq	%rdi, %r11
	cmpq	%r11, %rdx
	setnb	%r11b
	addq	%r10, %rax
	addq	%r8, %rax
	cmpq	%r9, %rax
	setbe	%al
	orb	%al, %r11b
	je	.L29
	cmpq	%rbx, %rcx
	setnb	%r8b
	cmpq	$29, %r12
	seta	%al
	testb	%al, %r8b
	je	.L29
	movq	%r10, %r8
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	andq	$-16, %r8
	.p2align 4,,10
	.p2align 3
.L23:
	movdqu	(%rdx,%rax), %xmm0
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	movups	%xmm0, 66(%rdi,%rax,2)
	movups	%xmm2, 50(%rdi,%rax,2)
	addq	$16, %rax
	cmpq	%rax, %r8
	jne	.L23
	movq	%r10, %r8
	andq	$-16, %r8
	addq	%r8, %rdx
	leaq	(%r9,%r8,2), %rax
	cmpq	%r8, %r10
	je	.L21
	movzbl	(%rdx), %r8d
	movw	%r8w, (%rax)
	leaq	2(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	1(%rdx), %r8d
	movw	%r8w, 2(%rax)
	leaq	4(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	2(%rdx), %r8d
	movw	%r8w, 4(%rax)
	leaq	6(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	3(%rdx), %r8d
	movw	%r8w, 6(%rax)
	leaq	8(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	4(%rdx), %r8d
	movw	%r8w, 8(%rax)
	leaq	10(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	5(%rdx), %r8d
	movw	%r8w, 10(%rax)
	leaq	12(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	6(%rdx), %r8d
	movw	%r8w, 12(%rax)
	leaq	14(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	7(%rdx), %r8d
	movw	%r8w, 14(%rax)
	leaq	16(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	8(%rdx), %r8d
	movw	%r8w, 16(%rax)
	leaq	18(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	9(%rdx), %r8d
	movw	%r8w, 18(%rax)
	leaq	20(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	10(%rdx), %r8d
	movw	%r8w, 20(%rax)
	leaq	22(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	11(%rdx), %r8d
	movw	%r8w, 22(%rax)
	leaq	24(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	12(%rdx), %r8d
	movw	%r8w, 24(%rax)
	leaq	26(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	13(%rdx), %r8d
	movw	%r8w, 26(%rax)
	leaq	28(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L21
	movzbl	14(%rdx), %edx
	movw	%dx, 28(%rax)
.L21:
	leaq	50(%rdi,%rsi), %rax
	popq	%rbx
	popq	%r12
	movq	%rax, 24(%rdi)
	popq	%r13
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movzbl	(%rdx), %eax
	addq	$2, %r9
	addq	$1, %rdx
	movw	%ax, -2(%r9)
	cmpq	%r9, %rcx
	ja	.L29
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movq	%r9, 24(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	50(%rdi,%rsi), %rax
	movq	%rax, 24(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE22686:
	.size	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE9ReadBlockEv, .-_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE9ReadBlockEv
	.section	.text._ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv,"axG",@progbits,_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv
	.type	_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv, @function
_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv:
.LFB22687:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22687:
	.size	_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv, .-_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv
	.section	.text._ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv,"axG",@progbits,_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv
	.type	_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv, @function
_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv:
.LFB22689:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE22689:
	.size	_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv, .-_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv
	.section	.text._ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv,"axG",@progbits,_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv
	.type	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv, @function
_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv:
.LFB22690:
	.cfi_startproc
	endbr64
	leaq	50(%rdi), %r9
	movq	16(%rdi), %rax
	movq	1080(%rdi), %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	8(%rdi), %rax
	movq	%r9, %xmm0
	movq	1096(%rdi), %r8
	sarq	%rax
	punpcklqdq	%xmm0, %xmm0
	addq	32(%rdi), %rax
	movq	1088(%rdi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rax, 32(%rdi)
	pushq	%r13
	movups	%xmm0, 8(%rdi)
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdx), %rsi
	movq	%r8, %rbx
	addq	$15, %rsi
	cmpq	%rax, %r8
	cmovbe	%r8, %rax
	leaq	(%rcx,%rax), %r10
	addq	%r8, %rcx
	subq	%rax, %rbx
	leaq	(%rsi,%r10), %rdx
	addq	%rsi, %rcx
	cmpq	%rcx, %rdx
	je	.L55
	movl	$512, %r8d
	movq	%rbx, %rax
	cmpq	$512, %rbx
	cmova	%r8, %rax
	leaq	(%rax,%rax), %r8
	leaq	(%r9,%r8), %rcx
	cmpq	%rcx, %r9
	jnb	.L43
	movq	%rcx, %rax
	leaq	51(%rdi), %rbx
	movl	$1, %r11d
	subq	%rdi, %rax
	leaq	-51(%rax), %r12
	movq	%r12, %rax
	shrq	%rax
	cmpq	%rbx, %rcx
	leaq	1(%rax), %r13
	leaq	52(%rax,%rax), %rax
	cmovnb	%r13, %r11
	movl	$52, %r13d
	cmovb	%r13, %rax
	addq	%rdi, %rax
	cmpq	%rax, %rdx
	setnb	%r13b
	addq	%r11, %r10
	addq	%r10, %rsi
	cmpq	%r9, %rsi
	setbe	%al
	orb	%al, %r13b
	je	.L51
	cmpq	%rbx, %rcx
	setnb	%sil
	cmpq	$29, %r12
	seta	%al
	testb	%al, %sil
	je	.L51
	movq	%r11, %rsi
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L45:
	movdqu	(%rdx,%rax), %xmm0
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	movups	%xmm0, 66(%rdi,%rax,2)
	movups	%xmm2, 50(%rdi,%rax,2)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L45
	movq	%r11, %rsi
	andq	$-16, %rsi
	addq	%rsi, %rdx
	leaq	(%r9,%rsi,2), %rax
	cmpq	%rsi, %r11
	je	.L43
	movzbl	(%rdx), %esi
	movw	%si, (%rax)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	1(%rdx), %esi
	movw	%si, 2(%rax)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	2(%rdx), %esi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	3(%rdx), %esi
	movw	%si, 6(%rax)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	4(%rdx), %esi
	movw	%si, 8(%rax)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	5(%rdx), %esi
	movw	%si, 10(%rax)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	6(%rdx), %esi
	movw	%si, 12(%rax)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	7(%rdx), %esi
	movw	%si, 14(%rax)
	leaq	16(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	8(%rdx), %esi
	movw	%si, 16(%rax)
	leaq	18(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	9(%rdx), %esi
	movw	%si, 18(%rax)
	leaq	20(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	10(%rdx), %esi
	movw	%si, 20(%rax)
	leaq	22(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	11(%rdx), %esi
	movw	%si, 22(%rax)
	leaq	24(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	12(%rdx), %esi
	movw	%si, 24(%rax)
	leaq	26(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	13(%rdx), %esi
	movw	%si, 26(%rax)
	leaq	28(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L43
	movzbl	14(%rdx), %edx
	movw	%dx, 28(%rax)
.L43:
	leaq	50(%rdi,%r8), %rax
	popq	%rbx
	popq	%r12
	movq	%rax, 24(%rdi)
	popq	%r13
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movzbl	(%rdx), %eax
	addq	$2, %r9
	addq	$1, %rdx
	movw	%ax, -2(%r9)
	cmpq	%r9, %rcx
	ja	.L51
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L55:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	movq	%r9, 24(%rdi)
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22690:
	.size	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv, .-_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv
	.section	.text._ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv,"axG",@progbits,_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv
	.type	_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv, @function
_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv:
.LFB22691:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE22691:
	.size	_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv, .-_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv
	.section	.text._ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv,"axG",@progbits,_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv
	.type	_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv, @function
_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv:
.LFB22693:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22693:
	.size	_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv, .-_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv
	.section	.text._ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv,"axG",@progbits,_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv
	.type	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv, @function
_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv:
.LFB22694:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdx
	movq	16(%rdi), %rax
	subq	8(%rdi), %rax
	movq	64(%rdi), %rcx
	sarq	%rax
	addq	32(%rdi), %rax
	cmpq	%rax, %rdx
	movq	%rax, 32(%rdi)
	cmovbe	%rdx, %rax
	leaq	(%rcx,%rdx,2), %rdx
	movq	%rdx, 24(%rdi)
	leaq	(%rcx,%rax,2), %rax
	cmpq	%rdx, %rax
	movq	%rax, 8(%rdi)
	movq	%rax, 16(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE22694:
	.size	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv, .-_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv
	.section	.text._ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv,"axG",@progbits,_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv
	.type	_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv, @function
_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv:
.LFB22695:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE22695:
	.size	_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv, .-_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv
	.section	.text._ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv,"axG",@progbits,_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv
	.type	_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv, @function
_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv:
.LFB22697:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22697:
	.size	_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv, .-_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv
	.section	.text._ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv,"axG",@progbits,_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv
	.type	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv, @function
_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv:
.LFB22698:
	.cfi_startproc
	endbr64
	movq	1096(%rdi), %rcx
	movq	16(%rdi), %rax
	leaq	50(%rdi), %r9
	subq	8(%rdi), %rax
	movq	1088(%rdi), %r8
	movq	%r9, %xmm0
	sarq	%rax
	addq	32(%rdi), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rcx, %rsi
	cmpq	%rax, %rcx
	movq	%rax, 32(%rdi)
	cmovbe	%rcx, %rax
	addq	%r8, %rcx
	movups	%xmm0, 8(%rdi)
	leaq	(%r8,%rax), %rdx
	subq	%rax, %rsi
	cmpq	%rcx, %rdx
	je	.L80
	cmpq	$512, %rsi
	movl	$512, %ecx
	cmova	%rcx, %rsi
	addq	%rsi, %rsi
	leaq	(%r9,%rsi), %rcx
	cmpq	%rcx, %r9
	jnb	.L77
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	subq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-51(%r10), %r12
	movl	$1, %r10d
	movq	%r12, %r11
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	51(%rdi), %rbx
	shrq	%r11
	cmpq	%rbx, %rcx
	leaq	1(%r11), %r13
	leaq	52(%r11,%r11), %r11
	cmovnb	%r13, %r10
	movl	$52, %r13d
	cmovb	%r13, %r11
	addq	%rdi, %r11
	cmpq	%r11, %rdx
	setnb	%r11b
	addq	%r10, %rax
	addq	%r8, %rax
	cmpq	%r9, %rax
	setbe	%al
	orb	%al, %r11b
	je	.L72
	cmpq	%rbx, %rcx
	setnb	%r8b
	cmpq	$29, %r12
	seta	%al
	testb	%al, %r8b
	je	.L72
	movq	%r10, %r8
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	andq	$-16, %r8
	.p2align 4,,10
	.p2align 3
.L66:
	movdqu	(%rdx,%rax), %xmm0
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	movups	%xmm0, 66(%rdi,%rax,2)
	movups	%xmm2, 50(%rdi,%rax,2)
	addq	$16, %rax
	cmpq	%rax, %r8
	jne	.L66
	movq	%r10, %r8
	andq	$-16, %r8
	addq	%r8, %rdx
	leaq	(%r9,%r8,2), %rax
	cmpq	%r8, %r10
	je	.L64
	movzbl	(%rdx), %r8d
	movw	%r8w, (%rax)
	leaq	2(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	1(%rdx), %r8d
	movw	%r8w, 2(%rax)
	leaq	4(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	2(%rdx), %r8d
	movw	%r8w, 4(%rax)
	leaq	6(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	3(%rdx), %r8d
	movw	%r8w, 6(%rax)
	leaq	8(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	4(%rdx), %r8d
	movw	%r8w, 8(%rax)
	leaq	10(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	5(%rdx), %r8d
	movw	%r8w, 10(%rax)
	leaq	12(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	6(%rdx), %r8d
	movw	%r8w, 12(%rax)
	leaq	14(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	7(%rdx), %r8d
	movw	%r8w, 14(%rax)
	leaq	16(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	8(%rdx), %r8d
	movw	%r8w, 16(%rax)
	leaq	18(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	9(%rdx), %r8d
	movw	%r8w, 18(%rax)
	leaq	20(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	10(%rdx), %r8d
	movw	%r8w, 20(%rax)
	leaq	22(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	11(%rdx), %r8d
	movw	%r8w, 22(%rax)
	leaq	24(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	12(%rdx), %r8d
	movw	%r8w, 24(%rax)
	leaq	26(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	13(%rdx), %r8d
	movw	%r8w, 26(%rax)
	leaq	28(%rax), %r8
	cmpq	%r8, %rcx
	jbe	.L64
	movzbl	14(%rdx), %edx
	movw	%dx, 28(%rax)
.L64:
	leaq	50(%rdi,%rsi), %rax
	popq	%rbx
	popq	%r12
	movq	%rax, 24(%rdi)
	popq	%r13
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movzbl	(%rdx), %eax
	addq	$2, %r9
	addq	$1, %rdx
	movw	%ax, -2(%r9)
	cmpq	%r9, %rcx
	ja	.L72
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movq	%r9, 24(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	50(%rdi,%rsi), %rax
	movq	%rax, 24(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE22698:
	.size	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv, .-_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv
	.section	.text._ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv,"axG",@progbits,_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv
	.type	_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv, @function
_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv:
.LFB22699:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22699:
	.size	_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv, .-_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv
	.section	.text._ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv,"axG",@progbits,_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv
	.type	_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv, @function
_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv:
.LFB22701:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE22701:
	.size	_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv, .-_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv
	.section	.text._ZN2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv,"axG",@progbits,_ZN2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv
	.type	_ZN2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv, @function
_ZN2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv:
.LFB22702:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	56(%rdi), %rdx
	subq	8(%rdi), %rax
	movq	72(%rdi), %rsi
	sarq	%rax
	addq	32(%rdi), %rax
	movq	64(%rdi), %rcx
	movq	%rax, 32(%rdi)
	movq	(%rdx), %rdx
	addq	$15, %rdx
	cmpq	%rax, %rsi
	cmovbe	%rsi, %rax
	addq	%rcx, %rax
	addq	%rsi, %rcx
	leaq	(%rdx,%rax,2), %rax
	leaq	(%rdx,%rcx,2), %rdx
	cmpq	%rdx, %rax
	movq	%rax, 8(%rdi)
	movq	%rax, 16(%rdi)
	setne	%al
	movq	%rdx, 24(%rdi)
	ret
	.cfi_endproc
.LFE22702:
	.size	_ZN2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv, .-_ZN2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv
	.section	.text._ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE5CloneEv,"axG",@progbits,_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE5CloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE5CloneEv
	.type	_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE5CloneEv, @function
_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE5CloneEv:
.LFB22684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$1096, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movdqu	1080(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEEE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movb	$0, 48(%rax)
	movq	%rdx, (%rax)
	movups	%xmm0, 1080(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22684:
	.size	_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE5CloneEv, .-_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE5CloneEv
	.section	.rodata._ZNK2v88internal27Utf8ExternalStreamingStream5CloneEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal27Utf8ExternalStreamingStream5CloneEv,"axG",@progbits,_ZNK2v88internal27Utf8ExternalStreamingStream5CloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal27Utf8ExternalStreamingStream5CloneEv
	.type	_ZNK2v88internal27Utf8ExternalStreamingStream5CloneEv, @function
_ZNK2v88internal27Utf8ExternalStreamingStream5CloneEv:
.LFB18558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18558:
	.size	_ZNK2v88internal27Utf8ExternalStreamingStream5CloneEv, .-_ZNK2v88internal27Utf8ExternalStreamingStream5CloneEv
	.section	.rodata._ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"can_be_cloned()"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv,"axG",@progbits,_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv
	.type	_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv, @function
_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv:
.LFB22674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22674:
	.size	_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv, .-_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv
	.section	.text._ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv,"axG",@progbits,_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv
	.type	_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv, @function
_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv:
.LFB22679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$96, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Znwm@PLT
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22679:
	.size	_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv, .-_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv
	.section	.text._ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv,"axG",@progbits,_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv
	.type	_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv, @function
_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv:
.LFB22688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22688:
	.size	_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv, .-_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv
	.section	.text._ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv,"axG",@progbits,_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv
	.type	_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv, @function
_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv:
.LFB22700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$80, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Znwm@PLT
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22700:
	.size	_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv, .-_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv
	.section	.text._ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED0Ev,"axG",@progbits,_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED0Ev
	.type	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED0Ev, @function
_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED0Ev:
.LFB22639:
	.cfi_startproc
	endbr64
	movl	$1104, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22639:
	.size	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED0Ev, .-_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED0Ev
	.section	.text._ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED0Ev,"axG",@progbits,_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED0Ev
	.type	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED0Ev, @function
_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED0Ev:
.LFB22635:
	.cfi_startproc
	endbr64
	movl	$1096, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22635:
	.size	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED0Ev, .-_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED0Ev
	.section	.text._ZN2v88internal25RelocatingCharacterStreamD2Ev,"axG",@progbits,_ZN2v88internal25RelocatingCharacterStreamD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25RelocatingCharacterStreamD2Ev
	.type	_ZN2v88internal25RelocatingCharacterStreamD2Ev, @function
_ZN2v88internal25RelocatingCharacterStreamD2Ev:
.LFB18527:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal25RelocatingCharacterStreamE(%rip), %rax
	movq	%rdi, %rdx
	leaq	_ZN2v88internal25RelocatingCharacterStream28UpdateBufferPointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv(%rip), %rsi
	movq	%rax, (%rdi)
	movq	80(%rdi), %rax
	leaq	37592(%rax), %rdi
	jmp	_ZN2v88internal4Heap24RemoveGCEpilogueCallbackEPFvPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvES6_@PLT
	.cfi_endproc
.LFE18527:
	.size	_ZN2v88internal25RelocatingCharacterStreamD2Ev, .-_ZN2v88internal25RelocatingCharacterStreamD2Ev
	.weak	_ZN2v88internal25RelocatingCharacterStreamD1Ev
	.set	_ZN2v88internal25RelocatingCharacterStreamD1Ev,_ZN2v88internal25RelocatingCharacterStreamD2Ev
	.section	.text._ZN2v88internal25RelocatingCharacterStreamD0Ev,"axG",@progbits,_ZN2v88internal25RelocatingCharacterStreamD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25RelocatingCharacterStreamD0Ev
	.type	_ZN2v88internal25RelocatingCharacterStreamD0Ev, @function
_ZN2v88internal25RelocatingCharacterStreamD0Ev:
.LFB18529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal25RelocatingCharacterStreamE(%rip), %rax
	leaq	_ZN2v88internal25RelocatingCharacterStream28UpdateBufferPointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%r12, %rdx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	80(%rdi), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap24RemoveGCEpilogueCallbackEPFvPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvES6_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18529:
	.size	_ZN2v88internal25RelocatingCharacterStreamD0Ev, .-_ZN2v88internal25RelocatingCharacterStreamD0Ev
	.section	.text._ZN2v88internal27Utf8ExternalStreamingStreamD2Ev,"axG",@progbits,_ZN2v88internal27Utf8ExternalStreamingStreamD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal27Utf8ExternalStreamingStreamD2Ev
	.type	_ZN2v88internal27Utf8ExternalStreamingStreamD2Ev, @function
_ZN2v88internal27Utf8ExternalStreamingStreamD2Ev:
.LFB18553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal27Utf8ExternalStreamingStreamE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	1080(%rdi), %rbx
	movq	1088(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %rbx
	je	.L102
	movq	%rdi, %r13
	.p2align 4,,10
	.p2align 3
.L104:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L103
	call	_ZdaPv@PLT
.L103:
	addq	$40, %rbx
	cmpq	%rbx, %r12
	jne	.L104
	movq	1080(%r13), %r12
.L102:
	testq	%r12, %r12
	je	.L101
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18553:
	.size	_ZN2v88internal27Utf8ExternalStreamingStreamD2Ev, .-_ZN2v88internal27Utf8ExternalStreamingStreamD2Ev
	.weak	_ZN2v88internal27Utf8ExternalStreamingStreamD1Ev
	.set	_ZN2v88internal27Utf8ExternalStreamingStreamD1Ev,_ZN2v88internal27Utf8ExternalStreamingStreamD2Ev
	.section	.text._ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED0Ev,"axG",@progbits,_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED0Ev
	.type	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED0Ev, @function
_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED0Ev:
.LFB22631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	72(%rdi), %rbx
	movq	80(%rdi), %r13
	movq	%rax, (%rdi)
	leaq	16+_ZTVN2v88internal13ChunkedStreamItEE(%rip), %rax
	movq	%rax, 56(%rdi)
	cmpq	%r13, %rbx
	je	.L112
	.p2align 4,,10
	.p2align 3
.L114:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L113
	call	_ZdaPv@PLT
.L113:
	addq	$24, %rbx
	cmpq	%rbx, %r13
	jne	.L114
	movq	72(%r12), %r13
.L112:
	testq	%r13, %r13
	je	.L115
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L115:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22631:
	.size	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED0Ev, .-_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED0Ev
	.section	.text._ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED0Ev,"axG",@progbits,_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED0Ev
	.type	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED0Ev, @function
_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED0Ev:
.LFB22627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	1096(%rdi), %rbx
	movq	1104(%rdi), %r13
	movq	%rax, (%rdi)
	leaq	16+_ZTVN2v88internal13ChunkedStreamIhEE(%rip), %rax
	movq	%rax, 1080(%rdi)
	cmpq	%r13, %rbx
	je	.L125
	.p2align 4,,10
	.p2align 3
.L127:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L126
	call	_ZdaPv@PLT
.L126:
	addq	$24, %rbx
	cmpq	%rbx, %r13
	jne	.L127
	movq	1096(%r12), %r13
.L125:
	testq	%r13, %r13
	je	.L128
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L128:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1120, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22627:
	.size	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED0Ev, .-_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED0Ev
	.section	.text._ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev,"axG",@progbits,_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev
	.type	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev, @function
_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev:
.LFB22647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEEE(%rip), %rax
	leaq	_ZNK2v86String26ExternalStringResourceBase6UnlockEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	1080(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L140
.L138:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1104, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	call	*%rax
	jmp	.L138
	.cfi_endproc
.LFE22647:
	.size	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev, .-_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev
	.section	.text._ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv,"axG",@progbits,_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv
	.type	_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv, @function
_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv:
.LFB22692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$80, %edi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_Znwm@PLT
	movq	56(%r12), %rdi
	leaq	_ZNK2v86String26ExternalStringResourceBase4LockEv(%rip), %rdx
	movq	$0, 32(%rax)
	movq	%rax, %rbx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movb	$0, 48(%rax)
	leaq	16+_ZTVN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEEE(%rip), %rax
	movq	%rax, (%rbx)
	movq	(%rdi), %rax
	movq	%rdi, 56(%rbx)
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L144
.L142:
	movq	64(%r12), %rax
	movq	%rax, 64(%rbx)
	movq	72(%r12), %rax
	movq	%rbx, 0(%r13)
	movq	%rax, 72(%rbx)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	call	*%rax
	jmp	.L142
	.cfi_endproc
.LFE22692:
	.size	_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv, .-_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv
	.section	.text._ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv,"axG",@progbits,_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv
	.type	_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv, @function
_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv:
.LFB22696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$1104, %edi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_Znwm@PLT
	movq	1080(%r12), %rdi
	leaq	_ZNK2v86String26ExternalStringResourceBase4LockEv(%rip), %rdx
	movq	$0, 32(%rax)
	movq	%rax, %rbx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movb	$0, 48(%rax)
	leaq	16+_ZTVN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEEE(%rip), %rax
	movq	%rax, (%rbx)
	movq	(%rdi), %rax
	movq	%rdi, 1080(%rbx)
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L148
.L146:
	movq	1088(%r12), %rax
	movq	%rax, 1088(%rbx)
	movq	1096(%r12), %rax
	movq	%rbx, 0(%r13)
	movq	%rax, 1096(%rbx)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	call	*%rax
	jmp	.L146
	.cfi_endproc
.LFE22696:
	.size	_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv, .-_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv
	.section	.text._ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev,"axG",@progbits,_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev
	.type	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev, @function
_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev:
.LFB22641:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEEE(%rip), %rax
	leaq	_ZNK2v86String26ExternalStringResourceBase6UnlockEv(%rip), %rdx
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L151
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	jmp	*%rax
	.cfi_endproc
.LFE22641:
	.size	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev, .-_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev
	.weak	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED1Ev
	.set	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED1Ev,_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev
	.section	.text._ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev,"axG",@progbits,_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev
	.type	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev, @function
_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev:
.LFB22643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEEE(%rip), %rax
	leaq	_ZNK2v86String26ExternalStringResourceBase6UnlockEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L155
.L153:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	call	*%rax
	jmp	.L153
	.cfi_endproc
.LFE22643:
	.size	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev, .-_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev
	.section	.text._ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev,"axG",@progbits,_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev
	.type	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev, @function
_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev:
.LFB22645:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEEE(%rip), %rax
	leaq	_ZNK2v86String26ExternalStringResourceBase6UnlockEv(%rip), %rdx
	movq	%rax, (%rdi)
	movq	1080(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L158
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	jmp	*%rax
	.cfi_endproc
.LFE22645:
	.size	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev, .-_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED1Ev
	.set	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED1Ev,_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED2Ev
	.section	.text._ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED2Ev,"axG",@progbits,_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED2Ev
	.type	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED2Ev, @function
_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED2Ev:
.LFB22629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	72(%rdi), %r12
	movq	80(%rdi), %r13
	movq	%rax, (%rdi)
	leaq	16+_ZTVN2v88internal13ChunkedStreamItEE(%rip), %rax
	movq	%rax, 56(%rdi)
	cmpq	%r13, %r12
	je	.L160
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L162:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L161
	call	_ZdaPv@PLT
.L161:
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L162
	movq	72(%rbx), %r13
.L160:
	testq	%r13, %r13
	je	.L159
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22629:
	.size	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED2Ev, .-_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED2Ev
	.weak	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED1Ev
	.set	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED1Ev,_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED2Ev
	.section	.text._ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED2Ev,"axG",@progbits,_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED2Ev
	.type	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED2Ev, @function
_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED2Ev:
.LFB22625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	1096(%rdi), %r12
	movq	1104(%rdi), %r13
	movq	%rax, (%rdi)
	leaq	16+_ZTVN2v88internal13ChunkedStreamIhEE(%rip), %rax
	movq	%rax, 1080(%rdi)
	cmpq	%r13, %r12
	je	.L170
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L172:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L171
	call	_ZdaPv@PLT
.L171:
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L172
	movq	1096(%rbx), %r13
.L170:
	testq	%r13, %r13
	je	.L169
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22625:
	.size	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED2Ev, .-_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED2Ev
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED1Ev
	.set	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED1Ev,_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED2Ev
	.section	.text._ZN2v88internal27Utf8ExternalStreamingStreamD0Ev,"axG",@progbits,_ZN2v88internal27Utf8ExternalStreamingStreamD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal27Utf8ExternalStreamingStreamD0Ev
	.type	_ZN2v88internal27Utf8ExternalStreamingStreamD0Ev, @function
_ZN2v88internal27Utf8ExternalStreamingStreamD0Ev:
.LFB18555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal27Utf8ExternalStreamingStreamE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	1080(%rdi), %rbx
	movq	1088(%rdi), %r13
	movq	%rax, (%rdi)
	cmpq	%r13, %rbx
	je	.L180
	.p2align 4,,10
	.p2align 3
.L182:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L181
	call	_ZdaPv@PLT
.L181:
	addq	$40, %rbx
	cmpq	%rbx, %r13
	jne	.L182
	movq	1080(%r12), %r13
.L180:
	testq	%r13, %r13
	je	.L183
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L183:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1144, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18555:
	.size	_ZN2v88internal27Utf8ExternalStreamingStreamD0Ev, .-_ZN2v88internal27Utf8ExternalStreamingStreamD0Ev
	.section	.text._ZN2v88internal28BufferedUtf16CharacterStreamC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28BufferedUtf16CharacterStreamC2Ev
	.type	_ZN2v88internal28BufferedUtf16CharacterStreamC2Ev, @function
_ZN2v88internal28BufferedUtf16CharacterStreamC2Ev:
.LFB18533:
	.cfi_startproc
	endbr64
	leaq	50(%rdi), %rax
	movq	$0, 32(%rdi)
	movq	%rax, 8(%rdi)
	movq	%rax, 16(%rdi)
	movq	%rax, 24(%rdi)
	leaq	16+_ZTVN2v88internal28BufferedUtf16CharacterStreamE(%rip), %rax
	movb	$0, 48(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE18533:
	.size	_ZN2v88internal28BufferedUtf16CharacterStreamC2Ev, .-_ZN2v88internal28BufferedUtf16CharacterStreamC2Ev
	.globl	_ZN2v88internal28BufferedUtf16CharacterStreamC1Ev
	.set	_ZN2v88internal28BufferedUtf16CharacterStreamC1Ev,_ZN2v88internal28BufferedUtf16CharacterStreamC2Ev
	.section	.text._ZN2v88internal27Utf8ExternalStreamingStream14SkipToPositionEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27Utf8ExternalStreamingStream14SkipToPositionEm
	.type	_ZN2v88internal27Utf8ExternalStreamingStream14SkipToPositionEm, @function
_ZN2v88internal27Utf8ExternalStreamingStream14SkipToPositionEm:
.LFB18559:
	.cfi_startproc
	endbr64
	movq	1120(%rdi), %rax
	cmpq	%rsi, %rax
	je	.L246
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	1104(%rdi), %r12
	movq	1080(%rdi), %rdx
	leaq	(%r12,%r12,4), %rcx
	leaq	(%rdx,%rcx,8), %rdx
	movq	1112(%rdi), %rcx
	movq	16(%rdx), %r14
	movq	(%rdx), %rbx
	movq	8(%rdx), %r11
	movq	%rcx, %r8
	movzbl	36(%rdx), %r9d
	subq	%r14, %r8
	movl	32(%rdx), %r10d
	addq	%rbx, %r8
	addq	%rbx, %r11
	testq	%rax, %rax
	jne	.L195
	cmpq	$2, %rcx
	jbe	.L252
.L195:
	cmpq	%r8, %r11
	jbe	.L221
	cmpq	%rax, %rsi
	ja	.L205
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L253:
	cmpq	%rax, %rsi
	jbe	.L196
	movq	%rdx, %r8
.L205:
	movzbl	(%r8), %ecx
	leaq	1(%r8), %rdx
	testb	%cl, %cl
	js	.L206
	cmpb	$12, %r9b
	jne	.L206
	addq	$1, %rax
.L207:
	cmpq	%r11, %rdx
	jb	.L253
.L196:
	xorl	%ecx, %ecx
	cmpq	%rdx, %r11
	movq	%rax, 1120(%rdi)
	sete	%cl
	subq	%rbx, %rdx
	movl	%r10d, 1128(%rdi)
	addq	%r12, %rcx
	addq	%r14, %rdx
	movb	%r9b, 1132(%rdi)
	cmpq	%rax, %rsi
	movq	%rcx, %xmm0
	movq	%rdx, %xmm1
	sete	%r8b
	punpcklqdq	%xmm1, %xmm0
	movl	%r8d, %eax
	movups	%xmm0, 1104(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions(%rip), %r15
	movzbl	%cl, %r13d
	sall	$6, %r10d
	movzbl	(%r15,%rcx), %ecx
	movzbl	%r9b, %r15d
	movl	%r13d, -52(%rbp)
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states(%rip), %r13
	addl	%ecx, %r15d
	sarl	%ecx
	movslq	%r15d, %r15
	movzbl	0(%r13,%r15), %r15d
	movl	$127, %r13d
	sarl	%cl, %r13d
	movl	%r13d, %ecx
	movl	-52(%rbp), %r13d
	andl	%ecx, %r13d
	orl	%r13d, %r10d
	testb	%r15b, %r15b
	je	.L208
	movl	%r15d, %r9d
	cmpb	$12, %r15b
	jne	.L207
	cmpl	$-4, %r10d
	jne	.L254
	movl	$12, %r9d
	xorl	%r10d, %r10d
	jmp	.L207
.L254:
	cmpl	$65535, %r10d
	jbe	.L255
	addq	$2, %rax
	movl	$12, %r9d
	xorl	%r10d, %r10d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L208:
	addq	$1, %rax
	xorl	%r10d, %r10d
	cmpb	$12, %r9b
	je	.L207
	movq	%r8, %rdx
	movl	$12, %r9d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r8, %rdx
	jmp	.L196
.L252:
	cmpq	%r11, %r8
	jnb	.L215
	movzbl	(%r8), %ecx
	addq	$1, %r8
	testb	%cl, %cl
	js	.L224
	cmpb	$12, %r9b
	jne	.L224
.L251:
	movl	$12, %r9d
	movl	$1, %eax
	jmp	.L195
.L257:
	xorl	%r10d, %r10d
.L215:
	movq	%r8, %rdx
	xorl	%eax, %eax
	jmp	.L196
.L224:
	movl	%r9d, %eax
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions(%rip), %r13
	movl	$127, %r15d
.L199:
	leaq	-1(%r8), %rdx
	sall	$6, %r10d
	movq	%rdx, -64(%rbp)
	movzbl	%cl, %edx
	movzbl	0(%r13,%rcx), %ecx
	movl	%edx, -52(%rbp)
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states(%rip), %rdx
	addl	%ecx, %r9d
	sarl	%ecx
	movslq	%r9d, %r9
	movzbl	(%rdx,%r9), %r9d
	movl	%r15d, %edx
	sarl	%cl, %edx
	movl	-52(%rbp), %ecx
	andl	%edx, %ecx
	orl	%ecx, %r10d
	testb	%r9b, %r9b
	je	.L201
	cmpb	$12, %r9b
	je	.L202
	cmpq	%r8, %r11
	je	.L215
	movzbl	(%r8), %ecx
	addq	$1, %r8
.L211:
	movl	%r9d, %eax
	jmp	.L199
.L202:
	cmpl	$-4, %r10d
	jne	.L256
	cmpq	%r8, %r11
	je	.L257
	movzbl	(%r8), %ecx
	xorl	%r10d, %r10d
	addq	$1, %r8
	testb	%cl, %cl
	jns	.L251
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L256:
	cmpl	$65279, %r10d
	jne	.L258
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	jmp	.L195
.L258:
	cmpl	$65535, %r10d
	movl	$0, %r10d
	jbe	.L219
	movl	$2, %eax
	jmp	.L195
.L219:
	movl	$1, %eax
	jmp	.L195
.L201:
	cmpb	$12, %al
	cmovne	-64(%rbp), %r8
	xorl	%r10d, %r10d
	jmp	.L251
.L255:
	addq	$1, %rax
	movl	$12, %r9d
	xorl	%r10d, %r10d
	jmp	.L207
	.cfi_endproc
.LFE18559:
	.size	_ZN2v88internal27Utf8ExternalStreamingStream14SkipToPositionEm, .-_ZN2v88internal27Utf8ExternalStreamingStream14SkipToPositionEm
	.section	.text._ZN2v88internal27Utf8ExternalStreamingStream26FillBufferFromCurrentChunkEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27Utf8ExternalStreamingStream26FillBufferFromCurrentChunkEv
	.type	_ZN2v88internal27Utf8ExternalStreamingStream26FillBufferFromCurrentChunkEv, @function
_ZN2v88internal27Utf8ExternalStreamingStream26FillBufferFromCurrentChunkEv:
.LFB18560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	1104(%rdi), %rax
	leaq	(%rax,%rax,4), %rdx
	movq	1080(%rdi), %rax
	leaq	(%rax,%rdx,8), %r13
	movq	24(%rdi), %rax
	movq	8(%r13), %rdx
	subq	%r9, %rax
	leaq	50(%rdi,%rax), %rbx
	movzbl	1132(%rdi), %edi
	movb	%dil, -57(%rbp)
	testq	%rdx, %rdx
	je	.L342
	movq	1112(%r12), %rcx
	movq	0(%r13), %r8
	movl	1128(%r12), %r11d
	movq	%rcx, %rax
	subq	16(%r13), %rax
	addq	%r8, %rax
	addq	%rdx, %r8
	cmpq	$2, %rcx
	jbe	.L343
.L263:
	addq	$1024, %r9
	cmpq	%rax, %r8
	jbe	.L274
	leaq	2(%rbx), %rdx
	cmpq	%r9, %rdx
	jnb	.L274
	movabsq	$-9187201950435737472, %r10
	pxor	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L275:
	movzbl	(%rax), %ecx
	leaq	1(%rax), %rsi
	testb	%cl, %cl
	js	.L276
	cmpb	$12, %dil
	jne	.L276
.L277:
	movw	%cx, (%rbx)
.L282:
	movq	%r9, %rdi
	movq	%r8, %rax
	subq	%rdx, %rdi
	subq	%rsi, %rax
	sarq	%rdi
	cmpq	%rax, %rdi
	cmova	%rax, %rdi
	movslq	%edi, %rdi
	leaq	(%rsi,%rdi), %rbx
	cmpq	$7, %rdi
	jbe	.L307
	movq	%rsi, %rax
	testb	$7, %sil
	jne	.L287
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L285:
	addq	$1, %rax
	testb	$7, %al
	je	.L291
.L287:
	cmpb	$0, (%rax)
	jns	.L285
.L340:
	subl	%esi, %eax
.L286:
	cltq
	leaq	(%rax,%rax), %rcx
	leaq	(%rdx,%rcx), %rbx
	cmpq	%rbx, %rdx
	jnb	.L297
	subq	$1, %rcx
	movq	%rcx, %rdi
	shrq	%rdi
	addq	$1, %rdi
	leaq	(%rdx,%rdi,2), %r14
	cmpq	%r14, %rsi
	leaq	(%rsi,%rdi), %r14
	setnb	%r15b
	cmpq	%r14, %rdx
	setnb	%r14b
	orb	%r14b, %r15b
	je	.L310
	cmpq	$29, %rcx
	jbe	.L310
	movq	%rdi, %r14
	xorl	%ecx, %ecx
	andq	$-16, %r14
	.p2align 4,,10
	.p2align 3
.L296:
	movdqu	(%rsi,%rcx), %xmm0
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	movups	%xmm0, 16(%rdx,%rcx,2)
	movups	%xmm2, (%rdx,%rcx,2)
	addq	$16, %rcx
	cmpq	%r14, %rcx
	jne	.L296
	movq	%rdi, %r14
	andq	$-16, %r14
	leaq	(%rsi,%r14), %rcx
	leaq	(%rdx,%r14,2), %rdx
	cmpq	%rdi, %r14
	je	.L297
	movzbl	(%rcx), %edi
	movw	%di, (%rdx)
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	1(%rcx), %edi
	movw	%di, 2(%rdx)
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	2(%rcx), %edi
	movw	%di, 4(%rdx)
	leaq	6(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	3(%rcx), %edi
	movw	%di, 6(%rdx)
	leaq	8(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	4(%rcx), %edi
	movw	%di, 8(%rdx)
	leaq	10(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	5(%rcx), %edi
	movw	%di, 10(%rdx)
	leaq	12(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	6(%rcx), %edi
	movw	%di, 12(%rdx)
	leaq	14(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	7(%rcx), %edi
	movw	%di, 14(%rdx)
	leaq	16(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	8(%rcx), %edi
	movw	%di, 16(%rdx)
	leaq	18(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	9(%rcx), %edi
	movw	%di, 18(%rdx)
	leaq	20(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	10(%rcx), %edi
	movw	%di, 20(%rdx)
	leaq	22(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	11(%rcx), %edi
	movw	%di, 22(%rdx)
	leaq	24(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	12(%rcx), %edi
	movw	%di, 24(%rdx)
	leaq	26(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	13(%rcx), %edi
	movw	%di, 26(%rdx)
	leaq	28(%rdx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L297
	movzbl	14(%rcx), %ecx
	movw	%cx, 28(%rdx)
	.p2align 4,,10
	.p2align 3
.L297:
	addq	%rsi, %rax
	movl	$12, %edi
.L280:
	cmpq	%r8, %rax
	jnb	.L274
	leaq	2(%rbx), %rdx
	cmpq	%r9, %rdx
	jb	.L275
.L274:
	movq	%rax, %rdx
	subq	0(%r13), %rdx
	addq	16(%r13), %rdx
	movl	%r11d, 1128(%r12)
	movq	%rdx, 1112(%r12)
	movq	%rbx, %rdx
	subq	24(%r12), %rdx
	sarq	%rdx
	addq	%rdx, 1120(%r12)
	cmpq	%r8, %rax
	sete	%al
	movb	%dil, 1132(%r12)
	movzbl	%al, %eax
	addq	%rax, 1104(%r12)
	movq	%rbx, 24(%r12)
.L259:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L344
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	testq	%r10, -8(%rax)
	jne	.L341
.L291:
	movq	%rax, %rcx
	addq	$8, %rax
	cmpq	%rax, %rbx
	jnb	.L288
	cmpq	%rcx, %rbx
	ja	.L290
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L292:
	addq	$1, %rcx
	cmpq	%rcx, %rbx
	je	.L289
.L290:
	cmpb	$0, (%rcx)
	jns	.L292
.L341:
	movl	%ecx, %eax
	subl	%esi, %eax
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%rsi, %rcx
	cmpq	%rcx, %rbx
	ja	.L290
.L345:
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L289:
	movl	%ebx, %eax
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L310:
	movq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L294:
	movzbl	(%rcx), %edi
	addq	$2, %rdx
	addq	$1, %rcx
	movw	%di, -2(%rdx)
	cmpq	%rdx, %rbx
	ja	.L294
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L276:
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions(%rip), %r15
	movzbl	%cl, %r14d
	sall	$6, %r11d
	movzbl	(%r15,%rcx), %ecx
	movzbl	%dil, %r15d
	movl	%r14d, -72(%rbp)
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states(%rip), %r14
	addl	%ecx, %r15d
	sarl	%ecx
	movslq	%r15d, %r15
	movzbl	(%r14,%r15), %r15d
	movl	$127, %r14d
	sarl	%cl, %r14d
	movl	%r14d, %ecx
	movl	-72(%rbp), %r14d
	movb	%r15b, -57(%rbp)
	andl	%ecx, %r14d
	orl	%r14d, %r11d
	testb	%r15b, %r15b
	je	.L278
	cmpb	$12, %r15b
	jne	.L346
	cmpl	$65535, %r11d
	jbe	.L347
	cmpl	$-4, %r11d
	je	.L306
	leal	-65536(%r11), %eax
	andw	$1023, %r11w
	leaq	4(%rbx), %rdx
	shrl	$10, %eax
	andw	$1023, %ax
	subw	$10240, %ax
	movw	%ax, (%rbx)
	leal	-9216(%r11), %eax
	xorl	%r11d, %r11d
	movw	%ax, 2(%rbx)
	jmp	.L282
.L346:
	movl	%r15d, %edi
	movq	%rsi, %rax
	jmp	.L280
.L306:
	movl	$12, %edi
	movq	%rsi, %rax
	xorl	%r11d, %r11d
	jmp	.L280
.L278:
	cmpb	$12, %dil
	movb	$12, -57(%rbp)
	movl	$-3, %ecx
	cmovne	%rax, %rsi
	xorl	%r11d, %r11d
	jmp	.L277
.L342:
	leaq	-57(%rbp), %rdi
	call	_ZN7unibrow4Utf824ValueOfIncrementalFinishEPN14Utf8DfaDecoder5StateE@PLT
	testl	%eax, %eax
	je	.L259
	movw	%ax, (%rbx)
	movzbl	-57(%rbp), %eax
	addq	$2, 24(%r12)
	addq	$1, 1120(%r12)
	movl	$0, 1128(%r12)
	movb	%al, 1132(%r12)
	jmp	.L259
.L343:
	cmpq	$0, 1120(%r12)
	jne	.L263
	cmpq	%r8, %rax
	jnb	.L264
	movl	%edi, %edx
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions(%rip), %r14
	movl	$127, %esi
.L265:
	movzbl	(%rax), %ecx
	movq	%rax, -72(%rbp)
	addq	$1, %rax
	testb	%cl, %cl
	js	.L266
	cmpb	$12, %dil
	jne	.L266
	movl	%r11d, %edx
	movzbl	%cl, %r11d
.L267:
	movw	%r11w, (%rbx)
	movl	$12, %edi
	movl	%edx, %r11d
	addq	$2, %rbx
	movq	8(%r12), %r9
	jmp	.L263
.L347:
	movl	%r11d, %ecx
	xorl	%r11d, %r11d
	jmp	.L277
.L344:
	call	__stack_chk_fail@PLT
.L266:
	movzbl	%cl, %r15d
	movzbl	(%r14,%rcx), %ecx
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states(%rip), %r10
	sall	$6, %r11d
	addl	%ecx, %edi
	sarl	%ecx
	movslq	%edi, %rdi
	movzbl	(%r10,%rdi), %edi
	movl	%esi, %r10d
	sarl	%cl, %r10d
	andl	%r10d, %r15d
	movb	%dil, -57(%rbp)
	orl	%r15d, %r11d
	testb	%dil, %dil
	je	.L268
	cmpb	$12, %dil
	je	.L269
.L270:
	cmpq	%rax, %r8
	je	.L301
	movl	%edi, %edx
	jmp	.L265
.L301:
	movq	%r8, %rax
.L264:
	movzbl	-57(%rbp), %edi
	jmp	.L274
.L269:
	cmpl	$65278, %r11d
	jbe	.L348
	cmpl	$-4, %r11d
	je	.L303
	cmpl	$65279, %r11d
	je	.L304
	cmpl	$65535, %r11d
	jbe	.L271
	leal	-65536(%r11), %edx
	addq	$4, %rbx
	shrl	$10, %edx
	andw	$1023, %dx
	subw	$10240, %dx
	movw	%dx, -4(%rbx)
	movl	%r11d, %edx
	xorl	%r11d, %r11d
	andw	$1023, %dx
	subw	$9216, %dx
	movw	%dx, -2(%rbx)
	movq	8(%r12), %r9
	jmp	.L263
.L268:
	movb	$12, -57(%rbp)
	cmpb	$12, %dl
	cmovne	-72(%rbp), %rax
	movl	$65533, %r11d
.L271:
	movw	%r11w, (%rbx)
	movzbl	-57(%rbp), %edi
	addq	$2, %rbx
	xorl	%r11d, %r11d
	movq	8(%r12), %r9
	jmp	.L263
.L304:
	xorl	%r11d, %r11d
	jmp	.L263
.L303:
	xorl	%r11d, %r11d
	jmp	.L270
.L348:
	xorl	%edx, %edx
	jmp	.L267
	.cfi_endproc
.LFE18560:
	.size	_ZN2v88internal27Utf8ExternalStreamingStream26FillBufferFromCurrentChunkEv, .-_ZN2v88internal27Utf8ExternalStreamingStream26FillBufferFromCurrentChunkEv
	.section	.text._ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEEii
	.type	_ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEEii, @function
_ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEEii:
.LFB18565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movslq	%ecx, %r15
	movq	%rsi, %rcx
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movslq	%edx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdx
	movq	-1(%rdx), %rdi
	cmpw	$63, 11(%rdi)
	jbe	.L399
.L351:
	movq	-1(%rdx), %rax
	movq	%rdx, %r14
	cmpw	$63, 11(%rax)
	jbe	.L400
.L357:
	movq	-1(%r14), %rax
	cmpw	$63, 11(%rax)
	jbe	.L366
.L398:
	movq	(%rcx), %rsi
.L365:
	xorl	%r8d, %r8d
.L354:
	movq	-1(%rsi), %rdx
	leaq	-1(%rsi), %rdi
	cmpw	$63, 11(%rdx)
	jbe	.L372
.L375:
	movq	(%rcx), %rbx
	movq	-1(%rbx), %rax
	leaq	-1(%rbx), %rdx
	cmpw	$63, 11(%rax)
	jbe	.L401
.L374:
	movq	-1(%rbx), %rax
	leaq	-1(%rbx), %rdx
	cmpw	$63, 11(%rax)
	jbe	.L402
.L383:
	movq	-1(%rbx), %rax
	leaq	-1(%rbx), %rdx
	cmpw	$63, 11(%rax)
	jbe	.L403
.L390:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L401:
	movq	-1(%rbx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L374
	movq	(%rdx), %rax
	movq	%rdx, -56(%rbp)
	testb	$8, 11(%rax)
	jne	.L374
	movl	$80, %edi
	movq	%r8, -64(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %r14
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movb	$0, 48(%rax)
	leaq	16+_ZTVN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEEE(%rip), %rax
	movq	%rax, (%r14)
	movq	(%rdx), %rax
	movq	-64(%rbp), %r8
	cmpw	$63, 11(%rax)
	jbe	.L404
.L384:
	movq	15(%rbx), %rdi
	leaq	_ZNK2v86String26ExternalStringResourceBase4LockEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	%rdi, 56(%r14)
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L405
.L388:
	movq	15(%rbx), %rdi
	movq	%r8, -56(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	-56(%rbp), %r8
	movq	%r15, 72(%r14)
	movq	%r13, 32(%r14)
	leaq	(%rax,%r8,2), %rax
	movq	%rax, 64(%r14)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L402:
	movq	-1(%rbx), %rax
	testb	$7, 11(%rax)
	jne	.L383
	movq	(%rdx), %rax
	testb	$8, 11(%rax)
	je	.L383
	movl	$1104, %edi
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
	movq	%rax, %r14
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movb	$0, 48(%rax)
	leaq	16+_ZTVN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEEE(%rip), %rax
	movq	%rcx, 1080(%r14)
	movslq	%r15d, %rcx
	movq	%rax, (%r14)
	movq	%r8, 1088(%r14)
	movq	%rcx, 1096(%r14)
	movq	%r13, 32(%r14)
.L349:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L357
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L360
	movq	23(%rdx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L360
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L366:
	movq	-1(%r14), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L398
	movq	(%rcx), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L369
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L403:
	movq	-1(%rbx), %rax
	testb	$7, 11(%rax)
	jne	.L390
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	movq	(%rdx), %rax
	testb	$8, 11(%rax)
	jne	.L390
	movl	$88, %edi
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
	leaq	37592(%r12), %rdi
	movq	%rax, %r14
	movq	%r13, 32(%rax)
	movl	$15, %edx
	leaq	_ZN2v88internal25RelocatingCharacterStream28UpdateBufferPointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv(%rip), %rsi
	movq	%rcx, 56(%rax)
	movslq	%r15d, %rcx
	movq	%r8, 64(%rax)
	movq	%rcx, 72(%rax)
	movq	%r14, %rcx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movb	$0, 48(%rax)
	leaq	16+_ZTVN2v88internal25RelocatingCharacterStreamE(%rip), %rax
	movq	%rax, (%r14)
	movq	%r12, 80(%r14)
	call	_ZN2v88internal4Heap21AddGCEpilogueCallbackEPFvPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvES4_S6_@PLT
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L372:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L375
	movq	(%rdi), %rax
	testb	$8, 11(%rax)
	je	.L375
	movl	$1104, %edi
	movq	%r8, -56(%rbp)
	movq	(%rcx), %rbx
	call	_Znwm@PLT
	movq	$0, 8(%rax)
	movq	%rax, %r14
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movb	$0, 48(%rax)
	leaq	16+_ZTVN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEEE(%rip), %rax
	movq	%rax, (%r14)
	movq	-1(%rbx), %rax
	movq	-56(%rbp), %r8
	cmpw	$63, 11(%rax)
	jbe	.L406
.L376:
	movq	15(%rbx), %rdi
	leaq	_ZNK2v86String26ExternalStringResourceBase4LockEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	%rdi, 1080(%r14)
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L407
.L380:
	movq	15(%rbx), %rdi
	movq	%r8, -56(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	-56(%rbp), %r8
	movq	%r15, 1096(%r14)
	movq	%r13, 32(%r14)
	addq	%rax, %r8
	movq	%r8, 1088(%r14)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L399:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$3, %ax
	jne	.L351
	movq	15(%rdx), %rsi
	movslq	27(%rdx), %r8
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L352
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L408
.L352:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L353
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %r8
	movq	(%rax), %rsi
	movq	%rax, %rcx
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L360:
	movq	41112(%r12), %rdi
	movq	15(%rdx), %r14
	testq	%rdi, %rdi
	je	.L362
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
	movq	%rax, %rcx
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L369:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L409
.L371:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L353:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L410
.L355:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L362:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L411
.L364:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rcx)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L408:
	movq	15(%rsi), %rsi
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L406:
	movq	-1(%rbx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L376
	movq	-1(%rbx), %rax
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L404:
	movq	(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L384
	movq	(%rdx), %rax
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L409:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L405:
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L355
.L411:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L364
	.cfi_endproc
.LFE18565:
	.size	_ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEEii, .-_ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEEii
	.section	.text._ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEE:
.LFB18564:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	xorl	%edx, %edx
	movl	11(%rax), %ecx
	jmp	_ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEEii
	.cfi_endproc
.LFE18564:
	.size	_ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal13ScannerStream10ForTestingEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScannerStream10ForTestingEPKc
	.type	_ZN2v88internal13ScannerStream10ForTestingEPKc, @function
_ZN2v88internal13ScannerStream10ForTestingEPKc:
.LFB18566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	strlen@PLT
	movl	$1096, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEEE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movb	$0, 48(%rax)
	movq	%rdx, (%rax)
	movq	%rbx, 1080(%rax)
	movq	%r13, 1088(%rax)
	movq	$0, 32(%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18566:
	.size	_ZN2v88internal13ScannerStream10ForTestingEPKc, .-_ZN2v88internal13ScannerStream10ForTestingEPKc
	.section	.text._ZN2v88internal13ScannerStream10ForTestingEPKcm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScannerStream10ForTestingEPKcm
	.type	_ZN2v88internal13ScannerStream10ForTestingEPKcm, @function
_ZN2v88internal13ScannerStream10ForTestingEPKcm:
.LFB18567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$1096, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEEE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movb	$0, 48(%rax)
	movq	%rcx, (%rax)
	movq	%r13, 1080(%rax)
	movq	%rbx, 1088(%rax)
	movq	$0, 32(%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18567:
	.size	_ZN2v88internal13ScannerStream10ForTestingEPKcm, .-_ZN2v88internal13ScannerStream10ForTestingEPKcm
	.section	.text._ZN2v88internal13ScannerStream3ForEPNS_14ScriptCompiler20ExternalSourceStreamENS2_14StreamedSource8EncodingE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScannerStream3ForEPNS_14ScriptCompiler20ExternalSourceStreamENS2_14StreamedSource8EncodingE
	.type	_ZN2v88internal13ScannerStream3ForEPNS_14ScriptCompiler20ExternalSourceStreamENS2_14StreamedSource8EncodingE, @function
_ZN2v88internal13ScannerStream3ForEPNS_14ScriptCompiler20ExternalSourceStreamENS2_14StreamedSource8EncodingE:
.LFB18568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$1, %esi
	je	.L418
	cmpl	$2, %esi
	je	.L419
	testl	%esi, %esi
	je	.L425
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L418:
	movl	$96, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEEE(%rip), %rcx
	movq	%rcx, (%rax)
	leaq	16+_ZTVN2v88internal13ChunkedStreamItEE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movb	$0, 48(%rax)
	movq	%rcx, 56(%rax)
	movq	%rbx, 64(%rax)
	movq	$0, 72(%rax)
	movq	$0, 80(%rax)
	movq	$0, 88(%rax)
	movq	$0, 32(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	movl	$1120, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEEE(%rip), %rdx
	leaq	16+_ZTVN2v88internal13ChunkedStreamIhEE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movb	$0, 48(%rax)
	movq	%rdx, (%rax)
	movq	%rcx, 1080(%rax)
	movq	%rbx, 1088(%rax)
	movq	$0, 1096(%rax)
	movq	$0, 1104(%rax)
	movq	$0, 1112(%rax)
	movq	$0, 32(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	movl	$1144, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	50(%rax), %rdx
	movb	$0, 48(%rax)
	movq	%rdx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rdx, 24(%rax)
	leaq	16+_ZTVN2v88internal27Utf8ExternalStreamingStreamE(%rip), %rdx
	movq	$0, 32(%rax)
	movq	%rdx, (%rax)
	movq	$0, 1096(%rax)
	movq	$0, 1120(%rax)
	movl	$0, 1128(%rax)
	movb	$12, 1132(%rax)
	movq	%rbx, 1136(%rax)
	movups	%xmm0, 1080(%rax)
	movups	%xmm0, 1104(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18568:
	.size	_ZN2v88internal13ScannerStream3ForEPNS_14ScriptCompiler20ExternalSourceStreamENS2_14StreamedSource8EncodingE, .-_ZN2v88internal13ScannerStream3ForEPNS_14ScriptCompiler20ExternalSourceStreamENS2_14StreamedSource8EncodingE
	.section	.rodata._ZNSt6vectorIN2v88internal27Utf8ExternalStreamingStream5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal27Utf8ExternalStreamingStream5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal27Utf8ExternalStreamingStream5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal27Utf8ExternalStreamingStream5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal27Utf8ExternalStreamingStream5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal27Utf8ExternalStreamingStream5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB22151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-3689348814741910323, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r10
	movq	(%rdi), %r9
	movq	%r10, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$230584300921369395, %rdx
	cmpq	%rdx, %rax
	je	.L440
	movq	%rsi, %r13
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %r13
	testq	%rax, %rax
	je	.L436
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L441
.L428:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L435:
	movq	32(%r15), %rax
	movdqu	(%r15), %xmm1
	subq	%r8, %r10
	leaq	40(%rbx,%r13), %r11
	movdqu	16(%r15), %xmm2
	movq	%r10, %r15
	movq	%rax, 32(%rbx,%r13)
	leaq	(%r11,%r10), %rax
	movq	%rax, -56(%rbp)
	movups	%xmm1, (%rbx,%r13)
	movups	%xmm2, 16(%rbx,%r13)
	testq	%r13, %r13
	jg	.L442
	testq	%r10, %r10
	jg	.L431
	testq	%r9, %r9
	jne	.L434
.L432:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r8, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r15, %r15
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r8
	jg	.L431
.L434:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L441:
	testq	%rcx, %rcx
	jne	.L429
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L431:
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%r11, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L432
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L436:
	movl	$40, %r14d
	jmp	.L428
.L440:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L429:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	imulq	$40, %rdx, %r14
	jmp	.L428
	.cfi_endproc
.LFE22151:
	.size	_ZNSt6vectorIN2v88internal27Utf8ExternalStreamingStream5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal27Utf8ExternalStreamingStream5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal27Utf8ExternalStreamingStream10FetchChunkEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27Utf8ExternalStreamingStream10FetchChunkEv
	.type	_ZN2v88internal27Utf8ExternalStreamingStream10FetchChunkEv, @function
_ZN2v88internal27Utf8ExternalStreamingStream10FetchChunkEv:
.LFB18561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$-128, %rsp
	movq	40(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testq	%rdi, %rdi
	je	.L444
	testl	%eax, %eax
	jne	.L453
.L444:
	movq	1136(%rbx), %rdi
	movq	$0, -120(%rbp)
	leaq	-120(%rbp), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movdqu	1112(%rbx), %xmm1
	movq	1088(%rbx), %rsi
	movq	%rax, %r12
	movq	-120(%rbp), %rax
	movq	%r12, -56(%rbp)
	movq	%rax, -64(%rbp)
	movq	1128(%rbx), %rax
	movaps	%xmm1, -48(%rbp)
	movq	%rax, -32(%rbp)
	cmpq	1096(%rbx), %rsi
	je	.L445
	movdqa	-64(%rbp), %xmm2
	movups	%xmm2, (%rsi)
	movdqa	-48(%rbp), %xmm3
	movq	-112(%rbp), %rdi
	movups	%xmm3, 16(%rsi)
	movq	-32(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 1088(%rbx)
	testq	%r12, %r12
	setne	%al
	testq	%rdi, %rdi
	jne	.L454
.L443:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L455
	subq	$-128, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	leaq	1080(%rbx), %rdi
	leaq	-64(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal27Utf8ExternalStreamingStream5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	-112(%rbp), %rdi
	testq	%r12, %r12
	setne	%al
	testq	%rdi, %rdi
	je	.L443
	.p2align 4,,10
	.p2align 3
.L454:
	leaq	-104(%rbp), %rsi
	movb	%al, -129(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movzbl	-129(%rbp), %eax
	jmp	.L443
.L453:
	leaq	-104(%rbp), %rsi
	movl	$153, %edx
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L444
.L455:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18561:
	.size	_ZN2v88internal27Utf8ExternalStreamingStream10FetchChunkEv, .-_ZN2v88internal27Utf8ExternalStreamingStream10FetchChunkEv
	.section	.text._ZN2v88internal27Utf8ExternalStreamingStream14SearchPositionEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27Utf8ExternalStreamingStream14SearchPositionEm
	.type	_ZN2v88internal27Utf8ExternalStreamingStream14SearchPositionEm, @function
_ZN2v88internal27Utf8ExternalStreamingStream14SearchPositionEm:
.LFB18562:
	.cfi_startproc
	endbr64
	cmpq	%rsi, 1120(%rdi)
	je	.L474
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	1088(%rdi), %rax
	movq	1080(%rdi), %rdi
	cmpq	%rdi, %rax
	je	.L478
.L458:
	movabsq	$-3689348814741910323, %rdx
	subq	%rdi, %rax
	movq	%rax, %rsi
	sarq	$3, %rsi
	imulq	%rdx, %rsi
	movq	%rsi, %rdx
	subq	$1, %rdx
	je	.L459
	leaq	-40(%rdi,%rax), %rax
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L480:
	subq	$40, %rax
	subq	$1, %rdx
	je	.L479
.L461:
	movq	24(%rax), %rcx
	cmpq	%r13, %rcx
	ja	.L480
	cmpq	$0, 8(%rax)
	movl	32(%rax), %r9d
	movq	16(%rax), %r8
	je	.L469
.L462:
	leaq	1(%rdx), %r10
	cmpq	%rsi, %r10
	jnb	.L464
	testl	%r9d, %r9d
	jne	.L465
	leaq	(%r10,%r10,4), %rsi
	leaq	(%rdi,%rsi,8), %rsi
	movq	16(%rsi), %rdi
	movq	24(%rsi), %rsi
	subq	%r8, %rdi
	subq	%rcx, %rsi
	cmpq	%rsi, %rdi
	jne	.L465
	subq	%rcx, %r8
	movq	%rdx, 1104(%r12)
	leaq	(%r8,%r13), %rax
	movq	%r13, 1120(%r12)
	movq	%rax, 1112(%r12)
	movl	$0, 1128(%r12)
	movb	$12, 1132(%r12)
	jmp	.L456
.L459:
	cmpq	$0, 8(%rdi)
	movl	32(%rdi), %r9d
	movq	%rdi, %rax
	movq	24(%rdi), %rcx
	movq	16(%rdi), %r8
	jne	.L464
	.p2align 4,,10
	.p2align 3
.L469:
	movzbl	36(%rax), %eax
	movq	%rdx, %xmm0
	movq	%r8, %xmm1
	movq	%rcx, 1120(%r12)
	movl	%r9d, 1128(%r12)
	punpcklqdq	%xmm1, %xmm0
	movb	%al, 1132(%r12)
	movups	%xmm0, 1104(%r12)
.L456:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L465:
	.cfi_restore_state
	movzbl	36(%rax), %eax
	movq	%rdx, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rcx, 1120(%r12)
	movq	%r8, %xmm3
	movl	%r9d, 1128(%r12)
	punpcklqdq	%xmm3, %xmm0
	movb	%al, 1132(%r12)
	movups	%xmm0, 1104(%r12)
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal27Utf8ExternalStreamingStream14SkipToPositionEm
	.p2align 4,,10
	.p2align 3
.L479:
	.cfi_restore_state
	movq	%rdi, %rax
	movq	24(%rdi), %rcx
	cmpq	$0, 8(%rax)
	movl	32(%rax), %r9d
	movq	16(%rax), %r8
	jne	.L462
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L464:
	movzbl	36(%rax), %eax
	movq	%rdx, %xmm0
	movq	%r8, %xmm2
	movq	%rcx, 1120(%r12)
	movl	%r9d, 1128(%r12)
	punpcklqdq	%xmm2, %xmm0
	movb	%al, 1132(%r12)
	movups	%xmm0, 1104(%r12)
.L477:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal27Utf8ExternalStreamingStream14SkipToPositionEm
	movl	%eax, %edx
	movl	$1, %eax
	cmpb	$1, %al
	jne	.L456
.L481:
	testb	%dl, %dl
	jne	.L456
	movq	%r12, %rdi
	call	_ZN2v88internal27Utf8ExternalStreamingStream10FetchChunkEv
	testb	%al, %al
	jne	.L477
	movl	%eax, %edx
	cmpb	$1, %al
	je	.L481
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r12, %rdi
	call	_ZN2v88internal27Utf8ExternalStreamingStream10FetchChunkEv
	movq	1080(%r12), %rdi
	movq	1088(%r12), %rax
	jmp	.L458
	.cfi_endproc
.LFE18562:
	.size	_ZN2v88internal27Utf8ExternalStreamingStream14SearchPositionEm, .-_ZN2v88internal27Utf8ExternalStreamingStream14SearchPositionEm
	.section	.text._ZN2v88internal27Utf8ExternalStreamingStream10FillBufferEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27Utf8ExternalStreamingStream10FillBufferEm
	.type	_ZN2v88internal27Utf8ExternalStreamingStream10FillBufferEm, @function
_ZN2v88internal27Utf8ExternalStreamingStream10FillBufferEm:
.LFB18563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	50(%rdi), %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movups	%xmm0, 16(%rdi)
	call	_ZN2v88internal27Utf8ExternalStreamingStream14SearchPositionEm
	movq	1088(%r12), %rdx
	movq	1080(%r12), %rdi
	movabsq	$-3689348814741910323, %rsi
	movq	1104(%r12), %rcx
	movq	%rdx, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	cmpq	%rax, %rcx
	je	.L486
	leaq	(%rcx,%rcx,4), %rax
	movq	8(%rdi,%rax,8), %rax
	testq	%rax, %rax
	je	.L491
.L486:
	movq	16(%r12), %rsi
	movq	24(%r12), %rax
	movabsq	$-3689348814741910323, %rbx
	cmpq	%rax, %rsi
	je	.L485
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L492:
	call	_ZN2v88internal27Utf8ExternalStreamingStream26FillBufferFromCurrentChunkEv
.L489:
	movq	16(%r12), %rsi
	movq	24(%r12), %rax
	cmpq	%rax, %rsi
	jne	.L484
	movq	1104(%r12), %rcx
	movq	1080(%r12), %rdi
	movq	1088(%r12), %rdx
.L485:
	subq	%rdi, %rdx
	movq	%r12, %rdi
	sarq	$3, %rdx
	imulq	%rbx, %rdx
	cmpq	%rcx, %rdx
	jne	.L492
	call	_ZN2v88internal27Utf8ExternalStreamingStream10FetchChunkEv
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal27Utf8ExternalStreamingStream26FillBufferFromCurrentChunkEv
	testb	%r13b, %r13b
	jne	.L489
	movq	24(%r12), %rax
	movq	16(%r12), %rsi
.L484:
	addq	$8, %rsp
	subq	%rsi, %rax
	popq	%rbx
	sarq	%rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L491:
	.cfi_restore_state
	movl	1128(%r12), %esi
	testl	%esi, %esi
	jne	.L486
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18563:
	.size	_ZN2v88internal27Utf8ExternalStreamingStream10FillBufferEm, .-_ZN2v88internal27Utf8ExternalStreamingStream10FillBufferEm
	.section	.text._ZNSt6vectorIN2v88internal13ChunkedStreamIhE5ChunkESaIS4_EE17_M_realloc_insertIJPKhRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal13ChunkedStreamIhE5ChunkESaIS4_EE17_M_realloc_insertIJPKhRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal13ChunkedStreamIhE5ChunkESaIS4_EE17_M_realloc_insertIJPKhRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal13ChunkedStreamIhE5ChunkESaIS4_EE17_M_realloc_insertIJPKhRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal13ChunkedStreamIhE5ChunkESaIS4_EE17_M_realloc_insertIJPKhRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB22742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movabsq	$-6148914691236517205, %rdi
	movq	%rsi, %rax
	subq	%r15, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movabsq	$384307168202282325, %rdi
	cmpq	%rdi, %rax
	je	.L510
	movq	%r12, %r10
	subq	%r15, %r10
	testq	%rax, %rax
	je	.L502
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L511
.L495:
	movq	%rbx, %rdi
	movq	%r8, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r10
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rcx
	movq	%rax, -56(%rbp)
	movq	-96(%rbp), %r8
	leaq	24(%r13), %rbx
.L501:
	movq	(%rcx), %xmm0
	movq	(%rdx), %rdx
	leaq	0(%r13,%r10), %rax
	movhps	(%r8), %xmm0
	movq	%rdx, (%rax)
	movups	%xmm0, 8(%rax)
	cmpq	%r15, %r12
	je	.L497
	movq	%r13, %rdx
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L498:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L498
	leaq	-24(%r12), %rax
	subq	%r15, %rax
	shrq	$3, %rax
	leaq	48(%r13,%rax,8), %rbx
.L497:
	cmpq	%rsi, %r12
	je	.L499
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-24(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	24(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L499:
	testq	%r15, %r15
	je	.L500
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L500:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r14)
	movups	%xmm0, (%r14)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L511:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L496
	movq	$0, -56(%rbp)
	movl	$24, %ebx
	xorl	%r13d, %r13d
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L502:
	movl	$24, %ebx
	jmp	.L495
.L496:
	cmpq	%rdi, %r9
	movq	%rdi, %rbx
	cmovbe	%r9, %rbx
	imulq	$24, %rbx, %rbx
	jmp	.L495
.L510:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22742:
	.size	_ZNSt6vectorIN2v88internal13ChunkedStreamIhE5ChunkESaIS4_EE17_M_realloc_insertIJPKhRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal13ChunkedStreamIhE5ChunkESaIS4_EE17_M_realloc_insertIJPKhRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal13ChunkedStreamIhE12ProcessChunkEPKhmm,"axG",@progbits,_ZN2v88internal13ChunkedStreamIhE12ProcessChunkEPKhmm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ChunkedStreamIhE12ProcessChunkEPKhmm
	.type	_ZN2v88internal13ChunkedStreamIhE12ProcessChunkEPKhmm, @function
_ZN2v88internal13ChunkedStreamIhE12ProcessChunkEPKhmm:
.LFB22677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	24(%rdi), %r9
	movq	%rdx, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -16(%rbp)
	movq	%rsi, -24(%rbp)
	cmpq	32(%rdi), %r9
	je	.L513
	movq	%rsi, (%r9)
	movq	%rdx, 8(%r9)
	movq	%rcx, 16(%r9)
	addq	$24, 24(%rdi)
.L512:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L517
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore_state
	leaq	-40(%rbp), %rcx
	leaq	-24(%rbp), %rdx
	addq	$16, %rdi
	movq	%r9, %rsi
	leaq	-16(%rbp), %r8
	call	_ZNSt6vectorIN2v88internal13ChunkedStreamIhE5ChunkESaIS4_EE17_M_realloc_insertIJPKhRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L512
.L517:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22677:
	.size	_ZN2v88internal13ChunkedStreamIhE12ProcessChunkEPKhmm, .-_ZN2v88internal13ChunkedStreamIhE12ProcessChunkEPKhmm
	.section	.text._ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv,"axG",@progbits,_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv
	.type	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv, @function
_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv:
.LFB22676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	1096(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	50(%rdi), %rax
	subq	8(%rdi), %rbx
	movq	%rax, %xmm0
	movq	%rax, -152(%rbp)
	sarq	%rbx
	addq	32(%rdi), %rbx
	movq	%rax, -144(%rbp)
	movq	40(%rdi), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, 32(%rdi)
	movq	%rax, -160(%rbp)
	movq	1104(%rdi), %rax
	movups	%xmm0, 8(%rdi)
	cmpq	%rsi, %rax
	je	.L582
	movq	-8(%rax), %rdx
	movq	-16(%rax), %r13
	addq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L537
	leaq	1096(%rdi), %r15
	leaq	-120(%rbp), %r12
	cmpq	%r13, %rbx
	jb	.L537
.L546:
	cmpq	$0, -160(%rbp)
	sete	-136(%rbp)
	.p2align 4,,10
	.p2align 3
.L532:
	pxor	%xmm3, %xmm3
	movq	$0, -120(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L526
	cmpb	$0, -136(%rbp)
	je	.L583
.L526:
	movq	1088(%r14), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L584
.L527:
	movq	-120(%rbp), %rdx
	movq	%r13, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	1104(%r14), %rsi
	movq	%rdx, -96(%rbp)
	cmpq	1112(%r14), %rsi
	je	.L528
	movq	%r13, %xmm0
	movq	%rax, %xmm4
	movq	%rdx, (%rsi)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%rsi)
	movq	1104(%r14), %rdx
	leaq	24(%rdx), %rax
	movq	%rax, 1104(%r14)
	movq	16(%rdx), %rsi
	movq	8(%rdx), %r13
	addq	%rsi, %r13
	cmpq	%r13, %rbx
	jb	.L531
	testq	%rsi, %rsi
	jne	.L532
.L531:
	movq	1096(%r14), %rsi
	cmpq	%rax, %rsi
	jne	.L537
.L533:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L534:
	subq	$24, %rax
	cmpq	%rax, %rsi
	je	.L533
.L537:
	movq	-16(%rax), %rdx
	cmpq	%rdx, %rbx
	jb	.L534
	movq	-24(%rax), %rdi
	movq	-8(%rax), %rax
	subq	%rdx, %rbx
	cmpq	%rax, %rbx
	movq	%rax, %rsi
	cmova	%rax, %rbx
	addq	%rdi, %rax
	leaq	(%rdi,%rbx), %rdx
	subq	%rbx, %rsi
	cmpq	%rax, %rdx
	je	.L585
	cmpq	$512, %rsi
	movl	$512, %eax
	cmova	%rax, %rsi
	movq	-152(%rbp), %rax
	addq	%rsi, %rsi
	leaq	(%rax,%rsi), %rcx
	cmpq	%rcx, %rax
	jnb	.L543
	movq	%rcx, %rax
	leaq	51(%r14), %r10
	movl	$1, %r8d
	subq	%r14, %rax
	leaq	-51(%rax), %r11
	movq	%r11, %rax
	shrq	%rax
	cmpq	%r10, %rcx
	leaq	1(%rax), %r12
	leaq	52(%rax,%rax), %rax
	cmovnb	%r12, %r8
	movl	$52, %r12d
	cmovb	%r12, %rax
	addq	%r14, %rax
	cmpq	%rax, %rdx
	setnb	%al
	addq	%r8, %rbx
	addq	%rdi, %rbx
	cmpq	%rbx, -152(%rbp)
	setnb	%dil
	orb	%dil, %al
	je	.L570
	cmpq	$29, %r11
	seta	%dil
	cmpq	%r10, %rcx
	setnb	%al
	testb	%al, %dil
	je	.L570
	movq	%r8, %rdi
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L542:
	movdqu	(%rdx,%rax), %xmm0
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	movups	%xmm0, 66(%r14,%rax,2)
	movups	%xmm2, 50(%r14,%rax,2)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L542
	movq	-152(%rbp), %rax
	movq	%r8, %rdi
	andq	$-16, %rdi
	addq	%rdi, %rdx
	leaq	(%rax,%rdi,2), %rax
	cmpq	%rdi, %r8
	je	.L543
	movzbl	(%rdx), %edi
	movw	%di, (%rax)
	leaq	2(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	1(%rdx), %edi
	movw	%di, 2(%rax)
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	2(%rdx), %edi
	movw	%di, 4(%rax)
	leaq	6(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	3(%rdx), %edi
	movw	%di, 6(%rax)
	leaq	8(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	4(%rdx), %edi
	movw	%di, 8(%rax)
	leaq	10(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	5(%rdx), %edi
	movw	%di, 10(%rax)
	leaq	12(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	6(%rdx), %edi
	movw	%di, 12(%rax)
	leaq	14(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	7(%rdx), %edi
	movw	%di, 14(%rax)
	leaq	16(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	8(%rdx), %edi
	movw	%di, 16(%rax)
	leaq	18(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	9(%rdx), %edi
	movw	%di, 18(%rax)
	leaq	20(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	10(%rdx), %edi
	movw	%di, 20(%rax)
	leaq	22(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	11(%rdx), %edi
	movw	%di, 22(%rax)
	leaq	24(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	12(%rdx), %edi
	movw	%di, 24(%rax)
	leaq	26(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	13(%rdx), %edi
	movw	%di, 26(%rax)
	leaq	28(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L543
	movzbl	14(%rdx), %edx
	movw	%dx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L543:
	leaq	50(%r14,%rsi), %rax
	movq	%rax, 24(%r14)
	movl	$1, %eax
.L518:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L586
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	.cfi_restore_state
	addq	$2, -144(%rbp)
	movzbl	(%rdx), %eax
	addq	$1, %rdx
	movq	-144(%rbp), %rbx
	movw	%ax, -2(%rbx)
	cmpq	%rbx, %rcx
	ja	.L570
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L528:
	leaq	-96(%rbp), %rdx
	leaq	-112(%rbp), %rcx
	movq	%r15, %rdi
	leaq	-104(%rbp), %r8
	call	_ZNSt6vectorIN2v88internal13ChunkedStreamIhE5ChunkESaIS4_EE17_M_realloc_insertIJPKhRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	1104(%r14), %rax
	movq	-8(%rax), %rdx
	movq	-16(%rax), %r13
	addq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L531
	cmpq	%r13, %rbx
	jnb	.L532
	movq	1096(%r14), %rsi
	cmpq	%rax, %rsi
	jne	.L537
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L584:
	leaq	-88(%rbp), %rsi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-168(%rbp), %rax
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L585:
	movq	8(%r14), %rax
	movq	%rax, 24(%r14)
	xorl	%eax, %eax
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L582:
	cmpq	$0, -160(%rbp)
	leaq	1096(%rdi), %r15
	leaq	-120(%rbp), %r12
	sete	%r13b
.L525:
	pxor	%xmm5, %xmm5
	movq	$0, -120(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L520
	testb	%r13b, %r13b
	je	.L587
.L520:
	movq	1088(%r14), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L588
.L521:
	movq	-120(%rbp), %rdx
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	1104(%r14), %rsi
	movq	%rdx, -96(%rbp)
	cmpq	1112(%r14), %rsi
	je	.L522
	movq	%rdx, (%rsi)
	movq	$0, 8(%rsi)
	movq	%rax, 16(%rsi)
	movq	1104(%r14), %rax
	movq	1096(%r14), %rsi
	addq	$24, %rax
	movq	%rax, 1104(%r14)
	cmpq	%rsi, %rax
	je	.L525
.L524:
	movq	-8(%rax), %rdx
	movq	-16(%rax), %r13
	addq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L537
	cmpq	%r13, %rbx
	jnb	.L546
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L583:
	movq	-160(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	movl	$153, %edx
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L526
.L522:
	leaq	-112(%rbp), %rcx
	leaq	-96(%rbp), %rdx
	movq	%r15, %rdi
	leaq	-104(%rbp), %r8
	call	_ZNSt6vectorIN2v88internal13ChunkedStreamIhE5ChunkESaIS4_EE17_M_realloc_insertIJPKhRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	1104(%r14), %rax
	movq	1096(%r14), %rsi
	cmpq	%rax, %rsi
	jne	.L524
	jmp	.L525
.L588:
	leaq	-88(%rbp), %rsi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-136(%rbp), %rax
	jmp	.L521
.L586:
	call	__stack_chk_fail@PLT
.L587:
	movq	-160(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	movl	$153, %edx
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L520
	.cfi_endproc
.LFE22676:
	.size	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv, .-_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv
	.section	.text._ZNSt6vectorIN2v88internal13ChunkedStreamItE5ChunkESaIS4_EE17_M_realloc_insertIJPKtRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal13ChunkedStreamItE5ChunkESaIS4_EE17_M_realloc_insertIJPKtRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal13ChunkedStreamItE5ChunkESaIS4_EE17_M_realloc_insertIJPKtRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal13ChunkedStreamItE5ChunkESaIS4_EE17_M_realloc_insertIJPKtRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal13ChunkedStreamItE5ChunkESaIS4_EE17_M_realloc_insertIJPKtRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB22750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movabsq	$-6148914691236517205, %rdi
	movq	%rsi, %rax
	subq	%r15, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movabsq	$384307168202282325, %rdi
	cmpq	%rdi, %rax
	je	.L606
	movq	%r12, %r10
	subq	%r15, %r10
	testq	%rax, %rax
	je	.L598
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L607
.L591:
	movq	%rbx, %rdi
	movq	%r8, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r10
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rcx
	movq	%rax, -56(%rbp)
	movq	-96(%rbp), %r8
	leaq	24(%r13), %rbx
.L597:
	movq	(%rcx), %xmm0
	movq	(%rdx), %rdx
	leaq	0(%r13,%r10), %rax
	movhps	(%r8), %xmm0
	movq	%rdx, (%rax)
	movups	%xmm0, 8(%rax)
	cmpq	%r15, %r12
	je	.L593
	movq	%r13, %rdx
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L594:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L594
	leaq	-24(%r12), %rax
	subq	%r15, %rax
	shrq	$3, %rax
	leaq	48(%r13,%rax,8), %rbx
.L593:
	cmpq	%rsi, %r12
	je	.L595
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-24(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	24(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L595:
	testq	%r15, %r15
	je	.L596
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L596:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r14)
	movups	%xmm0, (%r14)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L592
	movq	$0, -56(%rbp)
	movl	$24, %ebx
	xorl	%r13d, %r13d
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L598:
	movl	$24, %ebx
	jmp	.L591
.L592:
	cmpq	%rdi, %r9
	movq	%rdi, %rbx
	cmovbe	%r9, %rbx
	imulq	$24, %rbx, %rbx
	jmp	.L591
.L606:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22750:
	.size	_ZNSt6vectorIN2v88internal13ChunkedStreamItE5ChunkESaIS4_EE17_M_realloc_insertIJPKtRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal13ChunkedStreamItE5ChunkESaIS4_EE17_M_realloc_insertIJPKtRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal13ChunkedStreamItE12ProcessChunkEPKhmm,"axG",@progbits,_ZN2v88internal13ChunkedStreamItE12ProcessChunkEPKhmm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ChunkedStreamItE12ProcessChunkEPKhmm
	.type	_ZN2v88internal13ChunkedStreamItE12ProcessChunkEPKhmm, @function
_ZN2v88internal13ChunkedStreamItE12ProcessChunkEPKhmm:
.LFB22682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrq	%rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	24(%rdi), %r9
	movq	%rdx, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -16(%rbp)
	movq	%rsi, -24(%rbp)
	cmpq	32(%rdi), %r9
	je	.L609
	movq	%rsi, (%r9)
	movq	%rdx, 8(%r9)
	movq	%rcx, 16(%r9)
	addq	$24, 24(%rdi)
.L608:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L613
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L609:
	.cfi_restore_state
	leaq	-40(%rbp), %rcx
	leaq	-24(%rbp), %rdx
	addq	$16, %rdi
	movq	%r9, %rsi
	leaq	-16(%rbp), %r8
	call	_ZNSt6vectorIN2v88internal13ChunkedStreamItE5ChunkESaIS4_EE17_M_realloc_insertIJPKtRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L608
.L613:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22682:
	.size	_ZN2v88internal13ChunkedStreamItE12ProcessChunkEPKhmm, .-_ZN2v88internal13ChunkedStreamItE12ProcessChunkEPKhmm
	.section	.text._ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv,"axG",@progbits,_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv
	.type	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv, @function
_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv:
.LFB22681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	32(%rdi), %r15
	movq	16(%rdi), %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	subq	8(%rdi), %r9
	sarq	%r9
	movq	72(%rdi), %rdx
	movq	%rax, -136(%rbp)
	movq	80(%rdi), %rax
	addq	%r9, %r15
	movq	%r15, 32(%rdi)
	cmpq	%rdx, %rax
	je	.L663
	movq	-8(%rax), %rsi
	movq	-16(%rax), %r12
	addq	%rsi, %r12
	cmpq	%r12, %r15
	jb	.L631
	leaq	72(%rdi), %r14
	leaq	-120(%rbp), %r13
	testq	%rsi, %rsi
	je	.L631
.L632:
	cmpq	$0, -136(%rbp)
	sete	-144(%rbp)
	.p2align 4,,10
	.p2align 3
.L628:
	pxor	%xmm1, %xmm1
	movq	$0, -120(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L622
	cmpb	$0, -144(%rbp)
	je	.L664
.L622:
	movq	64(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L665
.L623:
	movq	-120(%rbp), %rdx
	shrq	%rax
	movq	80(%rbx), %rsi
	movq	%r12, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rdx, -96(%rbp)
	cmpq	88(%rbx), %rsi
	je	.L624
	movq	%r12, %xmm0
	movq	%rax, %xmm2
	movq	%rdx, (%rsi)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rsi)
	movq	80(%rbx), %rdx
	leaq	24(%rdx), %rax
	movq	%rax, 80(%rbx)
	movq	16(%rdx), %rsi
	movq	8(%rdx), %r12
	addq	%rsi, %r12
	cmpq	%r12, %r15
	jb	.L627
	testq	%rsi, %rsi
	jne	.L628
.L627:
	movq	72(%rbx), %rdx
	cmpq	%rax, %rdx
	jne	.L631
.L629:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L630:
	subq	$24, %rax
	cmpq	%rax, %rdx
	je	.L629
.L631:
	movq	-16(%rax), %rcx
	cmpq	%rcx, %r15
	jb	.L630
	movq	-8(%rax), %rdx
	subq	%rcx, %r15
	movq	-24(%rax), %rsi
	movq	%r15, %r9
	cmpq	%rdx, %r15
	cmova	%rdx, %r9
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rdx, 24(%rbx)
	leaq	(%rsi,%r9,2), %rax
	movq	%rax, 8(%rbx)
	cmpq	%rdx, %rax
	movq	%rax, 16(%rbx)
	setne	%al
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L666
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	.cfi_restore_state
	leaq	-96(%rbp), %rdx
	leaq	-112(%rbp), %rcx
	movq	%r14, %rdi
	leaq	-104(%rbp), %r8
	call	_ZNSt6vectorIN2v88internal13ChunkedStreamItE5ChunkESaIS4_EE17_M_realloc_insertIJPKtRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	80(%rbx), %rax
	movq	-8(%rax), %rdx
	movq	-16(%rax), %r12
	addq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L627
	cmpq	%r12, %r15
	jnb	.L628
	movq	72(%rbx), %rdx
	cmpq	%rax, %rdx
	jne	.L631
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L665:
	leaq	-88(%rbp), %rsi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-152(%rbp), %rax
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L663:
	cmpq	$0, -136(%rbp)
	leaq	72(%rdi), %r14
	leaq	-120(%rbp), %r13
	sete	%r12b
	pxor	%xmm0, %xmm0
.L621:
	movq	$0, -120(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L616
	testb	%r12b, %r12b
	je	.L667
.L616:
	movq	64(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-96(%rbp), %rdi
	pxor	%xmm0, %xmm0
	testq	%rdi, %rdi
	jne	.L668
.L617:
	movq	-120(%rbp), %rdx
	shrq	%rax
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	80(%rbx), %rsi
	movq	%rdx, -96(%rbp)
	cmpq	88(%rbx), %rsi
	je	.L618
	movq	%rdx, (%rsi)
	movq	$0, 8(%rsi)
	movq	%rax, 16(%rsi)
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdx
	addq	$24, %rax
	movq	%rax, 80(%rbx)
	cmpq	%rdx, %rax
	je	.L621
.L620:
	movq	-8(%rax), %rsi
	movq	-16(%rax), %r12
	addq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L631
	cmpq	%r12, %r15
	jnb	.L632
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L664:
	movq	-136(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	movl	$153, %edx
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L618:
	leaq	-96(%rbp), %rdx
	leaq	-112(%rbp), %rcx
	movq	%r14, %rdi
	leaq	-104(%rbp), %r8
	call	_ZNSt6vectorIN2v88internal13ChunkedStreamItE5ChunkESaIS4_EE17_M_realloc_insertIJPKtRmmEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdx
	pxor	%xmm0, %xmm0
	cmpq	%rax, %rdx
	jne	.L620
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L668:
	leaq	-88(%rbp), %rsi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-144(%rbp), %rax
	pxor	%xmm0, %xmm0
	jmp	.L617
.L667:
	movq	-136(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	movl	$153, %edx
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L616
.L666:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22681:
	.size	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv, .-_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal28BufferedUtf16CharacterStreamC2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal28BufferedUtf16CharacterStreamC2Ev, @function
_GLOBAL__sub_I__ZN2v88internal28BufferedUtf16CharacterStreamC2Ev:
.LFB22929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22929:
	.size	_GLOBAL__sub_I__ZN2v88internal28BufferedUtf16CharacterStreamC2Ev, .-_GLOBAL__sub_I__ZN2v88internal28BufferedUtf16CharacterStreamC2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal28BufferedUtf16CharacterStreamC2Ev
	.weak	_ZTVN2v88internal25RelocatingCharacterStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal25RelocatingCharacterStreamE,"awG",@progbits,_ZTVN2v88internal25RelocatingCharacterStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal25RelocatingCharacterStreamE, @object
	.size	_ZTVN2v88internal25RelocatingCharacterStreamE, 64
_ZTVN2v88internal25RelocatingCharacterStreamE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal25RelocatingCharacterStreamD1Ev
	.quad	_ZN2v88internal25RelocatingCharacterStreamD0Ev
	.quad	_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv
	.quad	_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv
	.quad	_ZNK2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv
	.quad	_ZN2v88internal25UnbufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv
	.weak	_ZTVN2v88internal28BufferedUtf16CharacterStreamE
	.section	.data.rel.ro._ZTVN2v88internal28BufferedUtf16CharacterStreamE,"awG",@progbits,_ZTVN2v88internal28BufferedUtf16CharacterStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal28BufferedUtf16CharacterStreamE, @object
	.size	_ZTVN2v88internal28BufferedUtf16CharacterStreamE, 72
_ZTVN2v88internal28BufferedUtf16CharacterStreamE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal28BufferedUtf16CharacterStream9ReadBlockEv
	.quad	__cxa_pure_virtual
	.weak	_ZTVN2v88internal27Utf8ExternalStreamingStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal27Utf8ExternalStreamingStreamE,"awG",@progbits,_ZTVN2v88internal27Utf8ExternalStreamingStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal27Utf8ExternalStreamingStreamE, @object
	.size	_ZTVN2v88internal27Utf8ExternalStreamingStreamE, 72
_ZTVN2v88internal27Utf8ExternalStreamingStreamE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal27Utf8ExternalStreamingStreamD1Ev
	.quad	_ZN2v88internal27Utf8ExternalStreamingStreamD0Ev
	.quad	_ZNK2v88internal27Utf8ExternalStreamingStream13can_be_clonedEv
	.quad	_ZNK2v88internal27Utf8ExternalStreamingStream5CloneEv
	.quad	_ZNK2v88internal27Utf8ExternalStreamingStream15can_access_heapEv
	.quad	_ZN2v88internal28BufferedUtf16CharacterStream9ReadBlockEv
	.quad	_ZN2v88internal27Utf8ExternalStreamingStream10FillBufferEm
	.weak	_ZTVN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEEE
	.section	.data.rel.ro.local._ZTVN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEEE,"awG",@progbits,_ZTVN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEEE,comdat
	.align 8
	.type	_ZTVN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEEE, @object
	.size	_ZTVN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEEE, 64
_ZTVN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED1Ev
	.quad	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev
	.quad	_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv
	.quad	_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv
	.quad	_ZNK2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv
	.quad	_ZN2v88internal23BufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv
	.weak	_ZTVN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEEE
	.section	.data.rel.ro.local._ZTVN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEEE,"awG",@progbits,_ZTVN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEEE,comdat
	.align 8
	.type	_ZTVN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEEE, @object
	.size	_ZTVN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEEE, 64
_ZTVN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED1Ev
	.quad	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEED0Ev
	.quad	_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE13can_be_clonedEv
	.quad	_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE5CloneEv
	.quad	_ZNK2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE15can_access_heapEv
	.quad	_ZN2v88internal25UnbufferedCharacterStreamINS0_20ExternalStringStreamEE9ReadBlockEv
	.weak	_ZTVN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEEE
	.section	.data.rel.ro.local._ZTVN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEEE,"awG",@progbits,_ZTVN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEEE,comdat
	.align 8
	.type	_ZTVN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEEE, @object
	.size	_ZTVN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEEE, 64
_ZTVN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED1Ev
	.quad	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEED0Ev
	.quad	_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE13can_be_clonedEv
	.quad	_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE5CloneEv
	.quad	_ZNK2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE15can_access_heapEv
	.quad	_ZN2v88internal23BufferedCharacterStreamINS0_12OnHeapStreamEE9ReadBlockEv
	.weak	_ZTVN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEEE
	.section	.data.rel.ro.local._ZTVN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEEE,"awG",@progbits,_ZTVN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEEE,comdat
	.align 8
	.type	_ZTVN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEEE, @object
	.size	_ZTVN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEEE, 64
_ZTVN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED1Ev
	.quad	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEED0Ev
	.quad	_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE13can_be_clonedEv
	.quad	_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE5CloneEv
	.quad	_ZNK2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE15can_access_heapEv
	.quad	_ZN2v88internal23BufferedCharacterStreamINS0_13TestingStreamEE9ReadBlockEv
	.weak	_ZTVN2v88internal13ChunkedStreamItEE
	.section	.data.rel.ro.local._ZTVN2v88internal13ChunkedStreamItEE,"awG",@progbits,_ZTVN2v88internal13ChunkedStreamItEE,comdat
	.align 8
	.type	_ZTVN2v88internal13ChunkedStreamItEE, @object
	.size	_ZTVN2v88internal13ChunkedStreamItEE, 24
_ZTVN2v88internal13ChunkedStreamItEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal13ChunkedStreamItE12ProcessChunkEPKhmm
	.weak	_ZTVN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEEE
	.section	.data.rel.ro.local._ZTVN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEEE,"awG",@progbits,_ZTVN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEEE,comdat
	.align 8
	.type	_ZTVN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEEE, @object
	.size	_ZTVN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEEE, 64
_ZTVN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED1Ev
	.quad	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEED0Ev
	.quad	_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv
	.quad	_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv
	.quad	_ZNK2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv
	.quad	_ZN2v88internal25UnbufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv
	.weak	_ZTVN2v88internal13ChunkedStreamIhEE
	.section	.data.rel.ro.local._ZTVN2v88internal13ChunkedStreamIhEE,"awG",@progbits,_ZTVN2v88internal13ChunkedStreamIhEE,comdat
	.align 8
	.type	_ZTVN2v88internal13ChunkedStreamIhEE, @object
	.size	_ZTVN2v88internal13ChunkedStreamIhEE, 24
_ZTVN2v88internal13ChunkedStreamIhEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal13ChunkedStreamIhE12ProcessChunkEPKhmm
	.weak	_ZTVN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEEE
	.section	.data.rel.ro.local._ZTVN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEEE,"awG",@progbits,_ZTVN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEEE,comdat
	.align 8
	.type	_ZTVN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEEE, @object
	.size	_ZTVN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEEE, 64
_ZTVN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED1Ev
	.quad	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEED0Ev
	.quad	_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE13can_be_clonedEv
	.quad	_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE5CloneEv
	.quad	_ZNK2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE15can_access_heapEv
	.quad	_ZN2v88internal23BufferedCharacterStreamINS0_13ChunkedStreamEE9ReadBlockEv
	.section	.rodata._ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states,"a"
	.align 32
	.type	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states, @object
	.size	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states, 108
_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\f"
	.string	""
	.string	""
	.string	"\030$0<H"
	.string	"T`"
	.string	"\f\f\f"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\030\030\030"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\030\030"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"$$$"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"$"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\030"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"$$"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata._ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions,"a"
	.align 32
	.type	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions, @object
	.size	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions, 256
_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\002\002\002\002\002\002\002\002\002\002\002\002\002\002"
	.ascii	"\002\002\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.ascii	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.ascii	"\003\003\003\003\t\t\004\004\004\004\004\004\004\004\004\004"
	.ascii	"\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004"
	.ascii	"\004\004\004\004\004\n\005\005\005\005\005\005\005\005\005\005"
	.ascii	"\005\005\006\005\005\013\007\007\007\b\t\t\t\t\t\t\t\t\t\t\t"
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
