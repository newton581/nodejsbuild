	.file	"js-locale.cc"
	.text
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB22870:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22870:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB22898:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L3:
	ret
	.cfi_endproc
.LFE22898:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB22900:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22900:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB22872:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22872:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB22899:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22899:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZZN2v88internal8JSLocale8MaximizeEPNS0_7IsolateENS0_6StringEENUlPN6icu_676LocaleEP10UErrorCodeE_4_FUNES7_S9_,"ax",@progbits
	.p2align 4
	.type	_ZZN2v88internal8JSLocale8MaximizeEPNS0_7IsolateENS0_6StringEENUlPN6icu_676LocaleEP10UErrorCodeE_4_FUNES7_S9_, @function
_ZZN2v88internal8JSLocale8MaximizeEPNS0_7IsolateENS0_6StringEENUlPN6icu_676LocaleEP10UErrorCodeE_4_FUNES7_S9_:
.LFB18417:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_676Locale16addLikelySubtagsER10UErrorCode@PLT
	.cfi_endproc
.LFE18417:
	.size	_ZZN2v88internal8JSLocale8MaximizeEPNS0_7IsolateENS0_6StringEENUlPN6icu_676LocaleEP10UErrorCodeE_4_FUNES7_S9_, .-_ZZN2v88internal8JSLocale8MaximizeEPNS0_7IsolateENS0_6StringEENUlPN6icu_676LocaleEP10UErrorCodeE_4_FUNES7_S9_
	.section	.text._ZZN2v88internal8JSLocale8MinimizeEPNS0_7IsolateENS0_6StringEENUlPN6icu_676LocaleEP10UErrorCodeE_4_FUNES7_S9_,"ax",@progbits
	.p2align 4
	.type	_ZZN2v88internal8JSLocale8MinimizeEPNS0_7IsolateENS0_6StringEENUlPN6icu_676LocaleEP10UErrorCodeE_4_FUNES7_S9_, @function
_ZZN2v88internal8JSLocale8MinimizeEPNS0_7IsolateENS0_6StringEENUlPN6icu_676LocaleEP10UErrorCodeE_4_FUNES7_S9_:
.LFB18421:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_676Locale15minimizeSubtagsER10UErrorCode@PLT
	.cfi_endproc
.LFE18421:
	.size	_ZZN2v88internal8JSLocale8MinimizeEPNS0_7IsolateENS0_6StringEENUlPN6icu_676LocaleEP10UErrorCodeE_4_FUNES7_S9_, .-_ZZN2v88internal8JSLocale8MinimizeEPNS0_7IsolateENS0_6StringEENUlPN6icu_676LocaleEP10UErrorCodeE_4_FUNES7_S9_
	.section	.rodata._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci.str1.1,"aMS",@progbits,1
.LC0:
	.string	"basic_string::append"
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci:
.LFB22901:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movslq	%edx, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%rdi), %rax
	cmpq	%rax, %rdx
	ja	.L15
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22901:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev:
.LFB20546:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678ByteSinkD2Ev@PLT
	.cfi_endproc
.LFE20546:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev
	.set	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev:
.LFB20548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE20548:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.section	.text._ZN2v88internal7ManagedIN6icu_676LocaleEE10DestructorEPv,"axG",@progbits,_ZN2v88internal7ManagedIN6icu_676LocaleEE10DestructorEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal7ManagedIN6icu_676LocaleEE10DestructorEPv
	.type	_ZN2v88internal7ManagedIN6icu_676LocaleEE10DestructorEPv, @function
_ZN2v88internal7ManagedIN6icu_676LocaleEE10DestructorEPv:
.LFB22470:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L19
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L22
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L23
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L24:
	cmpl	$1, %eax
	je	.L31
.L22:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L26
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L27:
	cmpl	$1, %eax
	jne	.L22
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L23:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L27
	.cfi_endproc
.LFE22470:
	.size	_ZN2v88internal7ManagedIN6icu_676LocaleEE10DestructorEPv, .-_ZN2v88internal7ManagedIN6icu_676LocaleEE10DestructorEPv
	.section	.rodata._ZN2v88internal8JSLocale8LanguageEPNS0_7IsolateENS0_6HandleIS1_EE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"(location_) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8JSLocale8LanguageEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale8LanguageEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSLocale8LanguageEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSLocale8LanguageEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	cmpb	$0, 8(%rax)
	jne	.L33
	leaq	88(%rdi), %rax
.L34:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L38
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	leaq	8(%rax), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	jne	.L34
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18423:
	.size	_ZN2v88internal8JSLocale8LanguageEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal8JSLocale8LanguageEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal8JSLocale6ScriptEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale6ScriptEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSLocale6ScriptEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSLocale6ScriptEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	cmpb	$0, 20(%rax)
	jne	.L40
	leaq	88(%rdi), %rax
.L41:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L45
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	leaq	20(%rax), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	jne	.L41
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18424:
	.size	_ZN2v88internal8JSLocale6ScriptEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal8JSLocale6ScriptEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal8JSLocale6RegionEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale6RegionEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSLocale6RegionEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSLocale6RegionEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	cmpb	$0, 26(%rax)
	jne	.L47
	leaq	88(%rdi), %rax
.L48:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L52
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	leaq	26(%rax), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	jne	.L48
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18425:
	.size	_ZN2v88internal8JSLocale6RegionEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal8JSLocale6RegionEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal8JSLocale8CalendarEPNS0_7IsolateENS0_6HandleIS1_EE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"ca"
.LC4:
	.string	""
.LC5:
	.string	"yes"
.LC6:
	.string	"true"
	.section	.text._ZN2v88internal8JSLocale8CalendarEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale8CalendarEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSLocale8CalendarEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSLocale8CalendarEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %r14
	pushq	%r13
	movq	%r14, %xmm0
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-128(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	(%rsi), %rax
	leaq	.LC3(%rip), %rsi
	movq	23(%rax), %rax
	movhps	-152(%rbp), %xmm0
	movq	7(%rax), %rax
	movaps	%xmm0, -176(%rbp)
	movq	24(%rax), %rax
	movq	(%rax), %r15
	movl	$0, -132(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r13, %rcx
	movdqa	-176(%rbp), %xmm0
	leaq	-132(%rbp), %r8
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movaps	%xmm0, -112(%rbp)
	movb	$0, -80(%rbp)
	call	_ZNK6icu_676Locale22getUnicodeKeywordValueENS_11StringPieceERNS_8ByteSinkER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%r14, -112(%rbp)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	cmpl	$1, -132(%rbp)
	jne	.L54
.L56:
	addq	$88, %r12
.L55:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	-152(%rbp), %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L56
	movq	-152(%rbp), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L66
.L57:
	movq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L55
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L66:
	movq	-88(%rbp), %rdx
	movq	-152(%rbp), %rdi
	movl	$4, %r8d
	xorl	%esi, %esi
	leaq	.LC6(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L57
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18427:
	.size	_ZN2v88internal8JSLocale8CalendarEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal8JSLocale8CalendarEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal8JSLocale9CaseFirstEPNS0_7IsolateENS0_6HandleIS1_EE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"kf"
	.section	.text._ZN2v88internal8JSLocale9CaseFirstEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale9CaseFirstEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSLocale9CaseFirstEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSLocale9CaseFirstEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %r14
	pushq	%r13
	movq	%r14, %xmm0
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-128(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	(%rsi), %rax
	leaq	.LC7(%rip), %rsi
	movq	23(%rax), %rax
	movhps	-152(%rbp), %xmm0
	movq	7(%rax), %rax
	movaps	%xmm0, -176(%rbp)
	movq	24(%rax), %rax
	movq	(%rax), %r15
	movl	$0, -132(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r13, %rcx
	movdqa	-176(%rbp), %xmm0
	leaq	-132(%rbp), %r8
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movaps	%xmm0, -112(%rbp)
	movb	$0, -80(%rbp)
	call	_ZNK6icu_676Locale22getUnicodeKeywordValueENS_11StringPieceERNS_8ByteSinkER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%r14, -112(%rbp)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	cmpl	$1, -132(%rbp)
	jne	.L68
.L70:
	addq	$88, %r12
.L69:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	-152(%rbp), %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L70
	movq	-152(%rbp), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L80
.L71:
	movq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L69
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	movq	-88(%rbp), %rdx
	movq	-152(%rbp), %rdi
	movl	$4, %r8d
	xorl	%esi, %esi
	leaq	.LC6(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L71
.L79:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18428:
	.size	_ZN2v88internal8JSLocale9CaseFirstEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal8JSLocale9CaseFirstEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal8JSLocale9CollationEPNS0_7IsolateENS0_6HandleIS1_EE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"co"
	.section	.text._ZN2v88internal8JSLocale9CollationEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale9CollationEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSLocale9CollationEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSLocale9CollationEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %r14
	pushq	%r13
	movq	%r14, %xmm0
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-128(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	(%rsi), %rax
	leaq	.LC8(%rip), %rsi
	movq	23(%rax), %rax
	movhps	-152(%rbp), %xmm0
	movq	7(%rax), %rax
	movaps	%xmm0, -176(%rbp)
	movq	24(%rax), %rax
	movq	(%rax), %r15
	movl	$0, -132(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r13, %rcx
	movdqa	-176(%rbp), %xmm0
	leaq	-132(%rbp), %r8
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movaps	%xmm0, -112(%rbp)
	movb	$0, -80(%rbp)
	call	_ZNK6icu_676Locale22getUnicodeKeywordValueENS_11StringPieceERNS_8ByteSinkER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%r14, -112(%rbp)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	cmpl	$1, -132(%rbp)
	jne	.L82
.L84:
	addq	$88, %r12
.L83:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L87
	call	_ZdlPv@PLT
.L87:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	-152(%rbp), %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L84
	movq	-152(%rbp), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L94
.L85:
	movq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L83
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	movq	-88(%rbp), %rdx
	movq	-152(%rbp), %rdi
	movl	$4, %r8d
	xorl	%esi, %esi
	leaq	.LC6(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L85
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18429:
	.size	_ZN2v88internal8JSLocale9CollationEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal8JSLocale9CollationEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal8JSLocale9HourCycleEPNS0_7IsolateENS0_6HandleIS1_EE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"hc"
	.section	.text._ZN2v88internal8JSLocale9HourCycleEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale9HourCycleEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSLocale9HourCycleEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSLocale9HourCycleEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %r14
	pushq	%r13
	movq	%r14, %xmm0
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-128(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	(%rsi), %rax
	leaq	.LC9(%rip), %rsi
	movq	23(%rax), %rax
	movhps	-152(%rbp), %xmm0
	movq	7(%rax), %rax
	movaps	%xmm0, -176(%rbp)
	movq	24(%rax), %rax
	movq	(%rax), %r15
	movl	$0, -132(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r13, %rcx
	movdqa	-176(%rbp), %xmm0
	leaq	-132(%rbp), %r8
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movaps	%xmm0, -112(%rbp)
	movb	$0, -80(%rbp)
	call	_ZNK6icu_676Locale22getUnicodeKeywordValueENS_11StringPieceERNS_8ByteSinkER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%r14, -112(%rbp)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	cmpl	$1, -132(%rbp)
	jne	.L96
.L98:
	addq	$88, %r12
.L97:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L101
	call	_ZdlPv@PLT
.L101:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	-152(%rbp), %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L98
	movq	-152(%rbp), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L108
.L99:
	movq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L97
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	movq	-88(%rbp), %rdx
	movq	-152(%rbp), %rdi
	movl	$4, %r8d
	xorl	%esi, %esi
	leaq	.LC6(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L99
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18430:
	.size	_ZN2v88internal8JSLocale9HourCycleEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal8JSLocale9HourCycleEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal8JSLocale7NumericEPNS0_7IsolateENS0_6HandleIS1_EE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"kn"
	.section	.text._ZN2v88internal8JSLocale7NumericEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale7NumericEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSLocale7NumericEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSLocale7NumericEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	movq	%r12, %xmm1
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-128(%rbp), %rdi
	subq	$120, %rsp
	movq	.LC11(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	.LC10(%rip), %rsi
	punpcklqdq	%xmm1, %xmm0
	movq	23(%rax), %rax
	movaps	%xmm0, -160(%rbp)
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r15
	movl	$0, -132(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r14, %rcx
	movdqa	-160(%rbp), %xmm0
	leaq	-132(%rbp), %r8
	movq	%r15, %rdi
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movaps	%xmm0, -112(%rbp)
	movb	$0, -80(%rbp)
	call	_ZNK6icu_676Locale22getUnicodeKeywordValueENS_11StringPieceERNS_8ByteSinkER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	leaq	112(%rbx), %r12
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-96(%rbp), %rdi
	addq	$120, %rbx
	testl	%eax, %eax
	cmovne	%rbx, %r12
	cmpq	%r13, %rdi
	je	.L112
	call	_ZdlPv@PLT
.L112:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L115:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18431:
	.size	_ZN2v88internal8JSLocale7NumericEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal8JSLocale7NumericEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal8JSLocale15NumberingSystemEPNS0_7IsolateENS0_6HandleIS1_EE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"nu"
	.section	.text._ZN2v88internal8JSLocale15NumberingSystemEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale15NumberingSystemEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSLocale15NumberingSystemEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSLocale15NumberingSystemEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %r14
	pushq	%r13
	movq	%r14, %xmm0
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-128(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	(%rsi), %rax
	leaq	.LC12(%rip), %rsi
	movq	23(%rax), %rax
	movhps	-152(%rbp), %xmm0
	movq	7(%rax), %rax
	movaps	%xmm0, -176(%rbp)
	movq	24(%rax), %rax
	movq	(%rax), %r15
	movl	$0, -132(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r13, %rcx
	movdqa	-176(%rbp), %xmm0
	leaq	-132(%rbp), %r8
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movaps	%xmm0, -112(%rbp)
	movb	$0, -80(%rbp)
	call	_ZNK6icu_676Locale22getUnicodeKeywordValueENS_11StringPieceERNS_8ByteSinkER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%r14, -112(%rbp)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	cmpl	$1, -132(%rbp)
	jne	.L117
.L119:
	addq	$88, %r12
.L118:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L122
	call	_ZdlPv@PLT
.L122:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L128
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movq	-152(%rbp), %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L119
	movq	-152(%rbp), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L129
.L120:
	movq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L118
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L129:
	movq	-88(%rbp), %rdx
	movq	-152(%rbp), %rdi
	movl	$4, %r8d
	xorl	%esi, %esi
	leaq	.LC6(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L120
.L128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18432:
	.size	_ZN2v88internal8JSLocale15NumberingSystemEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal8JSLocale15NumberingSystemEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal8JSLocale8ToStringB5cxx11ENS0_6HandleIS1_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v88internal8JSLocale8ToStringB5cxx11ENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale8ToStringB5cxx11ENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSLocale8ToStringB5cxx11ENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSLocale8ToStringB5cxx11ENS0_6HandleIS1_EE:
.LFB18433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal4Intl13ToLanguageTagB5cxx11ERKN6icu_676LocaleE@PLT
	cmpb	$0, -80(%rbp)
	je	.L149
.L131:
	movq	-72(%rbp), %r14
	movq	-64(%rbp), %r13
	leaq	16(%r12), %rdi
	movq	%rdi, (%r12)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L132
	testq	%r14, %r14
	je	.L150
.L132:
	movq	%r13, -88(%rbp)
	cmpq	$15, %r13
	ja	.L151
	cmpq	$1, %r13
	jne	.L135
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L136:
	movq	%r13, 8(%r12)
	leaq	-56(%rbp), %rax
	movb	$0, (%rdi,%r13)
	movq	-72(%rbp), %rdi
	cmpq	%rax, %rdi
	je	.L130
	call	_ZdlPv@PLT
.L130:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L152
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L136
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%r12, %rdi
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-88(%rbp), %rax
	movq	%rax, 16(%r12)
.L134:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L149:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L131
.L150:
	leaq	.LC13(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18433:
	.size	_ZN2v88internal8JSLocale8ToStringB5cxx11ENS0_6HandleIS1_EE, .-_ZN2v88internal8JSLocale8ToStringB5cxx11ENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal8JSLocale8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSLocale8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSLocale8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-96(%rbp), %rdi
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal4Intl13ToLanguageTagB5cxx11ERKN6icu_676LocaleE@PLT
	cmpb	$0, -96(%rbp)
	je	.L172
.L154:
	movq	-88(%rbp), %r14
	movq	-80(%rbp), %r12
	leaq	-112(%rbp), %rbx
	movq	%rbx, -128(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L155
	testq	%r14, %r14
	je	.L173
.L155:
	movq	%r12, -144(%rbp)
	cmpq	$15, %r12
	ja	.L174
	cmpq	$1, %r12
	jne	.L158
	movzbl	(%r14), %eax
	leaq	-144(%rbp), %r15
	movb	%al, -112(%rbp)
	movq	%rbx, %rax
.L159:
	movq	%r12, -120(%rbp)
	movb	$0, (%rax,%r12)
	movq	-88(%rbp), %rdi
	leaq	-72(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L160
	call	_ZdlPv@PLT
.L160:
	movq	-128(%rbp), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r12, -144(%rbp)
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L175
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L162
	call	_ZdlPv@PLT
.L162:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L176
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L177
	movq	%rbx, %rax
	leaq	-144(%rbp), %r15
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L174:
	leaq	-144(%rbp), %r15
	leaq	-128(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-144(%rbp), %rax
	movq	%rax, -112(%rbp)
.L157:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-144(%rbp), %r12
	movq	-128(%rbp), %rax
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L154
.L173:
	leaq	.LC13(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L176:
	call	__stack_chk_fail@PLT
.L177:
	movq	%rbx, %rdi
	leaq	-144(%rbp), %r15
	jmp	.L157
	.cfi_endproc
.LFE18434:
	.size	_ZN2v88internal8JSLocale8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal8JSLocale8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC14:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB21837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$288230376151711743, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L220
	movq	%rbx, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L200
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L221
	movabsq	$9223372036854775776, %rcx
.L180:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %r13
.L198:
	movq	(%rdx), %r10
	movq	8(%rdx), %r9
	addq	%r13, %r8
	leaq	16(%r8), %rdi
	movq	%r10, %rax
	movq	%rdi, (%r8)
	addq	%r9, %rax
	je	.L182
	testq	%r10, %r10
	je	.L222
.L182:
	movq	%r9, -64(%rbp)
	cmpq	$15, %r9
	ja	.L223
	cmpq	$1, %r9
	jne	.L185
	movzbl	(%r10), %eax
	movb	%al, 16(%r8)
.L186:
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	je	.L202
.L227:
	movq	%r13, %rdx
	movq	%r14, %rax
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
.L218:
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %rbx
	je	.L224
.L191:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	jne	.L188
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L221:
	testq	%rcx, %rcx
	jne	.L181
	xorl	%r13d, %r13d
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L224:
	movq	%rbx, %r8
	subq	%r14, %r8
	addq	%r13, %r8
.L187:
	addq	$32, %r8
	cmpq	%r12, %rbx
	je	.L192
	movq	%rbx, %rax
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L196:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	je	.L225
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -16(%rdx)
	movq	-24(%rax), %rsi
	movq	%rsi, -24(%rdx)
	cmpq	%r12, %rax
	jne	.L196
.L194:
	subq	%rbx, %r12
	addq	%r12, %r8
.L192:
	testq	%r14, %r14
	je	.L197
	movq	%r14, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r8
.L197:
	movq	%r13, %xmm0
	addq	%rcx, %r13
	movq	%r8, %xmm3
	movq	%r13, 16(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L226
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %r12
	jne	.L196
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L200:
	movl	$32, %ecx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L185:
	testq	%r9, %r9
	jne	.L184
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	jne	.L227
.L202:
	movq	%r13, %r8
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%r8, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	%rax, %rdi
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rcx
	movq	%rax, (%r8)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r8)
.L184:
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	(%r8), %rdi
	jmp	.L186
.L222:
	leaq	.LC13(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L181:
	cmpq	%rsi, %rcx
	cmova	%rsi, %rcx
	salq	$5, %rcx
	jmp	.L180
.L226:
	call	__stack_chk_fail@PLT
.L220:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21837:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE, @function
_GLOBAL__sub_I__ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE:
.LFB22903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22903:
	.size	_GLOBAL__sub_I__ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE, .-_GLOBAL__sub_I__ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_111MorphLocaleEPNS0_7IsolateENS0_6StringEPFvPN6icu_676LocaleEP10UErrorCodeE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"und"
.LC16:
	.string	"U_SUCCESS(status)"
.LC17:
	.string	"!icu_locale.isBogus()"
	.section	.text._ZN2v88internal12_GLOBAL__N_111MorphLocaleEPNS0_7IsolateENS0_6StringEPFvPN6icu_676LocaleEP10UErrorCodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_111MorphLocaleEPNS0_7IsolateENS0_6StringEPFvPN6icu_676LocaleEP10UErrorCodeE, @function
_ZN2v88internal12_GLOBAL__N_111MorphLocaleEPNS0_7IsolateENS0_6StringEPFvPN6icu_676LocaleEP10UErrorCodeE:
.LFB18411:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-568(%rbp), %rdi
	leaq	-560(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-512(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$1, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-572(%rbp), %rbx
	subq	$552, %rsp
	movq	%rsi, -584(%rbp)
	leaq	-584(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -572(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-568(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-560(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rbx, %rcx
	movq	-552(%rbp), %rdx
	call	_ZN6icu_676Locale14forLanguageTagENS_11StringPieceER10UErrorCode@PLT
	movq	-568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L231
	call	_ZdaPv@PLT
.L231:
	movq	-472(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L258
	movl	-572(%rbp), %edx
	testl	%edx, %edx
	jg	.L235
.L233:
	cmpb	$0, -296(%rbp)
	jne	.L236
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*%r12
	movl	-572(%rbp), %eax
	testl	%eax, %eax
	jg	.L235
	cmpb	$0, -296(%rbp)
	jne	.L236
	leaq	-288(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4Intl13ToLanguageTagB5cxx11ERKN6icu_676LocaleE@PLT
	cmpb	$0, -288(%rbp)
	je	.L259
.L237:
	movq	-280(%rbp), %r8
	movq	-272(%rbp), %r12
	leaq	-528(%rbp), %rbx
	movq	%rbx, -544(%rbp)
	movq	%r8, %rax
	addq	%r12, %rax
	je	.L238
	testq	%r8, %r8
	je	.L260
.L238:
	movq	%r12, -560(%rbp)
	cmpq	$15, %r12
	ja	.L261
	cmpq	$1, %r12
	jne	.L241
	movzbl	(%r8), %eax
	movb	%al, -528(%rbp)
	movq	%rbx, %rax
.L242:
	movq	%r12, -536(%rbp)
	movb	$0, (%rax,%r12)
	movq	-280(%rbp), %rdi
	leaq	-264(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L243
	call	_ZdlPv@PLT
.L243:
	movq	-544(%rbp), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r12, -560(%rbp)
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L262
	movq	-544(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L245
	call	_ZdlPv@PLT
.L245:
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	addq	$552, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	leaq	-544(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r8, -592(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-592(%rbp), %r8
	movq	%rax, -544(%rbp)
	movq	%rax, %rdi
	movq	-560(%rbp), %rax
	movq	%rax, -528(%rbp)
.L240:
	movq	%r12, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-560(%rbp), %r12
	movq	-544(%rbp), %rax
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L258:
	leaq	-288(%rbp), %r9
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	leaq	.LC15(%rip), %rsi
	movq	%r9, -592(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-592(%rbp), %r9
	movq	%r13, %rdi
	movq	%r9, %rsi
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	-592(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	-572(%rbp), %edx
	testl	%edx, %edx
	jle	.L233
	.p2align 4,,10
	.p2align 3
.L235:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L241:
	testq	%r12, %r12
	jne	.L264
	movq	%rbx, %rax
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L236:
	leaq	.LC17(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L259:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L262:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L260:
	leaq	.LC13(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L263:
	call	__stack_chk_fail@PLT
.L264:
	movq	%rbx, %rdi
	jmp	.L240
	.cfi_endproc
.LFE18411:
	.size	_ZN2v88internal12_GLOBAL__N_111MorphLocaleEPNS0_7IsolateENS0_6StringEPFvPN6icu_676LocaleEP10UErrorCodeE, .-_ZN2v88internal12_GLOBAL__N_111MorphLocaleEPNS0_7IsolateENS0_6StringEPFvPN6icu_676LocaleEP10UErrorCodeE
	.section	.text._ZN2v88internal8JSLocale8MaximizeEPNS0_7IsolateENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale8MaximizeEPNS0_7IsolateENS0_6StringE
	.type	_ZN2v88internal8JSLocale8MaximizeEPNS0_7IsolateENS0_6StringE, @function
_ZN2v88internal8JSLocale8MaximizeEPNS0_7IsolateENS0_6StringE:
.LFB18415:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v88internal8JSLocale8MaximizeEPNS0_7IsolateENS0_6StringEENUlPN6icu_676LocaleEP10UErrorCodeE_4_FUNES7_S9_(%rip), %rdx
	jmp	_ZN2v88internal12_GLOBAL__N_111MorphLocaleEPNS0_7IsolateENS0_6StringEPFvPN6icu_676LocaleEP10UErrorCodeE
	.cfi_endproc
.LFE18415:
	.size	_ZN2v88internal8JSLocale8MaximizeEPNS0_7IsolateENS0_6StringE, .-_ZN2v88internal8JSLocale8MaximizeEPNS0_7IsolateENS0_6StringE
	.section	.text._ZN2v88internal8JSLocale8MinimizeEPNS0_7IsolateENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale8MinimizeEPNS0_7IsolateENS0_6StringE
	.type	_ZN2v88internal8JSLocale8MinimizeEPNS0_7IsolateENS0_6StringE, @function
_ZN2v88internal8JSLocale8MinimizeEPNS0_7IsolateENS0_6StringE:
.LFB18419:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v88internal8JSLocale8MinimizeEPNS0_7IsolateENS0_6StringEENUlPN6icu_676LocaleEP10UErrorCodeE_4_FUNES7_S9_(%rip), %rdx
	jmp	_ZN2v88internal12_GLOBAL__N_111MorphLocaleEPNS0_7IsolateENS0_6StringEPFvPN6icu_676LocaleEP10UErrorCodeE
	.cfi_endproc
.LFE18419:
	.size	_ZN2v88internal8JSLocale8MinimizeEPNS0_7IsolateENS0_6StringE, .-_ZN2v88internal8JSLocale8MinimizeEPNS0_7IsolateENS0_6StringE
	.section	.text._ZN2v88internal8JSLocale8BaseNameEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale8BaseNameEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSLocale8BaseNameEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSLocale8BaseNameEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-288(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676Locale14createFromNameEPKc@PLT
	leaq	-336(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4Intl13ToLanguageTagB5cxx11ERKN6icu_676LocaleE@PLT
	cmpb	$0, -336(%rbp)
	je	.L286
.L268:
	movq	-328(%rbp), %r8
	movq	-320(%rbp), %r12
	leaq	-352(%rbp), %rbx
	movq	%rbx, -368(%rbp)
	movq	%r8, %rax
	addq	%r12, %rax
	je	.L269
	testq	%r8, %r8
	je	.L287
.L269:
	movq	%r12, -384(%rbp)
	cmpq	$15, %r12
	ja	.L288
	cmpq	$1, %r12
	jne	.L272
	movzbl	(%r8), %eax
	leaq	-384(%rbp), %r15
	movb	%al, -352(%rbp)
	movq	%rbx, %rax
.L273:
	movq	%r12, -360(%rbp)
	movb	$0, (%rax,%r12)
	movq	-328(%rbp), %rdi
	leaq	-312(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L274
	call	_ZdlPv@PLT
.L274:
	movq	-368(%rbp), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r12, -384(%rbp)
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L289
	movq	-368(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L276
	call	_ZdlPv@PLT
.L276:
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L290
	addq	$360, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L291
	movq	%rbx, %rax
	leaq	-384(%rbp), %r15
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L288:
	leaq	-384(%rbp), %r15
	leaq	-368(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, -392(%rbp)
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-392(%rbp), %r8
	movq	%rax, -368(%rbp)
	movq	%rax, %rdi
	movq	-384(%rbp), %rax
	movq	%rax, -352(%rbp)
.L271:
	movq	%r12, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-384(%rbp), %r12
	movq	-368(%rbp), %rax
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L289:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L286:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L268
.L287:
	leaq	.LC13(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L290:
	call	__stack_chk_fail@PLT
.L291:
	movq	%rbx, %rdi
	leaq	-384(%rbp), %r15
	jmp	.L271
	.cfi_endproc
.LFE18426:
	.size	_ZN2v88internal8JSLocale8BaseNameEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal8JSLocale8BaseNameEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"0 < bcp47_tag.length()"
.LC19:
	.string	"(*bcp47_tag) != nullptr"
.LC20:
	.string	"basic_string::basic_string"
	.section	.rodata._ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.section	.rodata._ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE.str1.1
.LC22:
	.string	"ApplyOptionsToTag"
.LC23:
	.string	"language"
.LC24:
	.string	"script"
.LC25:
	.string	"region"
.LC26:
	.string	"isolate"
.LC27:
	.string	"numberingSystem"
.LC28:
	.string	"numeric"
.LC29:
	.string	"caseFirst"
.LC30:
	.string	"hourCycle"
.LC31:
	.string	"collation"
.LC32:
	.string	"calendar"
.LC33:
	.string	"upper"
.LC34:
	.string	"lower"
.LC35:
	.string	"h11"
.LC36:
	.string	"h12"
.LC37:
	.string	"h23"
.LC38:
	.string	"h24"
.LC39:
	.string	"false"
.LC40:
	.string	"locale"
	.section	.text._ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE
	.type	_ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE, @function
_ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE:
.LFB18407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$840, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -848(%rbp)
	movq	%rcx, -800(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-496(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -776(%rbp)
	call	_ZN6icu_6713LocaleBuilderC1Ev@PLT
	movq	(%r12), %rax
	movl	11(%rax), %r9d
	testl	%r9d, %r9d
	jne	.L293
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$72, %esi
.L578:
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L577:
	xorl	%r12d, %r12d
.L388:
	movq	-776(%rbp), %rdi
	call	_ZN6icu_6713LocaleBuilderD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L579
	addq	$840, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	leaq	-656(%rbp), %r13
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movl	-648(%rbp), %edx
	movq	-656(%rbp), %rsi
	movq	-776(%rbp), %rdi
	call	_ZN6icu_6713LocaleBuilder14setLanguageTagENS_11StringPieceE@PLT
	movl	-648(%rbp), %r8d
	testl	%r8d, %r8d
	jle	.L580
	movq	-656(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L581
	leaq	-576(%rbp), %rax
	movq	%rbx, %rdi
	leaq	-592(%rbp), %r12
	movq	%rax, -832(%rbp)
	movq	%rax, -592(%rbp)
	call	strlen@PLT
	movq	%rax, -624(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L582
	cmpq	$1, %rax
	jne	.L299
	movzbl	(%rbx), %edx
	movb	%dl, -576(%rbp)
	movq	-832(%rbp), %rdx
.L300:
	movq	%rax, -584(%rbp)
	leaq	-328(%rbp), %r14
	pxor	%xmm0, %xmm0
	leaq	-448(%rbp), %rbx
	movb	$0, (%rdx,%rax)
	movq	%r14, %rdi
	leaq	-544(%rbp), %rax
	movq	%rax, -856(%rbp)
	movq	%rax, -560(%rbp)
	movq	%rbx, -784(%rbp)
	movaps	%xmm0, -624(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -552(%rbp)
	movb	$0, -544(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%edi, %edi
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -328(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movw	%di, -104(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -112(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	24+_ZTVNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -424(%rbp)
	movq	%rax, -448(%rbp)
	addq	$40, %rax
	movq	%rax, -328(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -432(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -864(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	movq	$0, -400(%rbp)
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-592(%rbp), %rbx
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-584(%rbp), %r12
	movq	%rax, -432(%rbp)
	leaq	-344(%rbp), %rax
	movq	%rax, -840(%rbp)
	movq	%rax, -360(%rbp)
	movq	%rbx, %rax
	movl	$0, -368(%rbp)
	addq	%r12, %rax
	setne	%dl
	testq	%rbx, %rbx
	sete	%al
	andb	%al, %dl
	movb	%dl, -865(%rbp)
	jne	.L309
	movq	%r12, -688(%rbp)
	cmpq	$15, %r12
	ja	.L583
	cmpq	$1, %r12
	jne	.L304
	movzbl	(%rbx), %eax
	movb	%al, -344(%rbp)
	movq	-840(%rbp), %rax
.L305:
	movq	%r12, -352(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movb	$0, (%rax,%r12)
	leaq	-432(%rbp), %rax
	movq	-360(%rbp), %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -880(%rbp)
	movl	$8, -368(%rbp)
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE7_M_syncEPcmm@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-560(%rbp), %rbx
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	-624(%rbp), %rax
	movq	%rax, -824(%rbp)
	.p2align 4,,10
	.p2align 3
.L306:
	movq	-784(%rbp), %rdi
	movl	$45, %edx
	movq	%rbx, %rsi
	call	_ZSt7getlineIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EES4_@PLT
	movq	(%rax), %rdx
	movq	-24(%rdx), %rdx
	testb	$5, 32(%rax,%rdx)
	jne	.L307
	movq	-616(%rbp), %r12
	cmpq	-608(%rbp), %r12
	je	.L308
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
	movq	-560(%rbp), %r9
	movq	-552(%rbp), %r8
	movq	%r9, %rax
	addq	%r8, %rax
	je	.L465
	testq	%r9, %r9
	je	.L309
.L465:
	movq	%r8, -688(%rbp)
	cmpq	$15, %r8
	ja	.L584
	movq	(%r12), %rdi
	cmpq	$1, %r8
	jne	.L313
	movzbl	(%r9), %eax
	movb	%al, (%rdi)
	movq	-688(%rbp), %r8
	movq	(%r12), %rdi
.L314:
	movq	%r8, 8(%r12)
	movb	$0, (%rdi,%r8)
	addq	$32, -616(%rbp)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L299:
	testq	%rax, %rax
	jne	.L585
	movq	-832(%rbp), %rdx
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L580:
	leaq	.LC18(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L308:
	movq	-824(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L313:
	testq	%r8, %r8
	je	.L314
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L584:
	movq	%r12, %rdi
	leaq	-688(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -816(%rbp)
	movq	%r9, -808(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-808(%rbp), %r9
	movq	-816(%rbp), %r8
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-688(%rbp), %rax
	movq	%rax, 16(%r12)
.L312:
	movq	%r8, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-688(%rbp), %r8
	movq	(%r12), %rdi
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L307:
	movq	-624(%rbp), %rdx
	movq	-616(%rbp), %rsi
	xorl	%ebx, %ebx
	cmpq	%rsi, %rdx
	je	.L316
	movq	8(%rdx), %rcx
	leaq	-2(%rcx), %rax
	cmpq	$1, %rax
	ja	.L317
	movq	(%rdx), %rdi
	movzbl	(%rdi), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L316
	movzbl	1(%rdi), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L316
	cmpq	$3, %rcx
	jne	.L318
	movzbl	2(%rdi), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L316
.L318:
	movq	%rsi, %rdi
	subq	%rdx, %rdi
	cmpq	$32, %rdi
	je	.L321
	movq	40(%rdx), %rbx
	cmpq	$1, %rbx
	jne	.L320
	movq	32(%rdx), %rax
	movzbl	(%rax), %ecx
	movl	%ecx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L321
	subl	$48, %ecx
	movl	$32, %eax
	cmpb	$9, %cl
	jbe	.L321
.L322:
	addq	%rdx, %rax
	movq	8(%rax), %rcx
	cmpq	$2, %rcx
	jne	.L323
	movq	(%rax), %rcx
	movzbl	(%rcx), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L586
.L324:
	movq	%rdi, %rax
	sarq	$5, %rax
	cmpq	%rbx, %rax
	jbe	.L321
	leaq	-512(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L347:
	movq	%rbx, %rax
	salq	$5, %rax
	addq	%rdx, %rax
	movq	8(%rax), %rcx
	cmpq	$1, %rcx
	jne	.L326
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movl	%eax, %edx
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$25, %dl
	jbe	.L321
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L573
	.p2align 4,,10
	.p2align 3
.L321:
	movl	$1, %ebx
.L316:
	leaq	24+_ZTVNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-360(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$40, %rax
	movq	%rax, -328(%rbp)
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	cmpq	-840(%rbp), %rdi
	je	.L348
	call	_ZdlPv@PLT
.L348:
	movq	-864(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -432(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -328(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-560(%rbp), %rdi
	cmpq	-856(%rbp), %rdi
	je	.L349
	call	_ZdlPv@PLT
.L349:
	movq	-616(%rbp), %r14
	movq	-624(%rbp), %r12
	cmpq	%r12, %r14
	je	.L350
	.p2align 4,,10
	.p2align 3
.L354:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L351
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r14, %r12
	jne	.L354
.L352:
	movq	-624(%rbp), %r12
.L350:
	testq	%r12, %r12
	je	.L355
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L355:
	movq	-592(%rbp), %rdi
	cmpq	-832(%rbp), %rdi
	je	.L356
	call	_ZdlPv@PLT
.L356:
	testb	%bl, %bl
	jne	.L357
.L359:
	movq	%r13, %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
.L358:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$73, %esi
	jmp	.L578
.L328:
	cmpq	$4, %rcx
	jne	.L573
	movq	(%rax), %rdx
	movzbl	(%rdx), %edx
	subl	$48, %edx
	cmpb	$9, %dl
	ja	.L573
	movq	%r12, -528(%rbp)
	movq	(%rax), %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	movq	$3, -688(%rbp)
	addq	$1, %rsi
	call	memcpy@PLT
	movq	-688(%rbp), %rax
	movq	-528(%rbp), %rdx
	movq	%rax, -520(%rbp)
	movb	$0, (%rdx,%rax)
	cmpq	$3, -520(%rbp)
	movq	-528(%rbp), %rdi
	jne	.L344
	movzbl	(%rdi), %edx
	movl	%edx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L343
	subl	$48, %edx
	cmpb	$9, %dl
	jbe	.L343
	.p2align 4,,10
	.p2align 3
.L344:
	cmpq	%r12, %rdi
	je	.L573
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L573:
	xorl	%ebx, %ebx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L586:
	movzbl	1(%rcx), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L324
.L325:
	addq	$1, %rbx
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L351:
	addq	$32, %r12
	cmpq	%r12, %r14
	jne	.L354
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L326:
	leaq	-5(%rcx), %rdi
	cmpq	$3, %rdi
	ja	.L328
	movq	(%rax), %rax
	movzbl	(%rax), %r8d
	movl	%r8d, %edi
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	jbe	.L329
	subl	$48, %r8d
	cmpb	$9, %r8b
	ja	.L573
	.p2align 4,,10
	.p2align 3
.L329:
	movzbl	1(%rax), %r8d
	movl	%r8d, %edi
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	jbe	.L330
	subl	$48, %r8d
	cmpb	$9, %r8b
	ja	.L573
.L330:
	movzbl	2(%rax), %r8d
	movl	%r8d, %edi
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	jbe	.L331
	subl	$48, %r8d
	cmpb	$9, %r8b
	ja	.L573
	.p2align 4,,10
	.p2align 3
.L331:
	movzbl	3(%rax), %r8d
	movl	%r8d, %edi
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	jbe	.L332
	subl	$48, %r8d
	cmpb	$9, %r8b
	ja	.L573
	.p2align 4,,10
	.p2align 3
.L332:
	movzbl	4(%rax), %r8d
	movl	%r8d, %edi
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	jbe	.L333
	subl	$48, %r8d
	cmpb	$9, %r8b
	ja	.L573
.L333:
	cmpq	$5, %rcx
	je	.L334
	movzbl	5(%rax), %r8d
	movl	%r8d, %edi
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	jbe	.L335
	subl	$48, %r8d
	cmpb	$9, %r8b
	ja	.L573
.L335:
	cmpq	$6, %rcx
	je	.L334
	movzbl	6(%rax), %r8d
	movl	%r8d, %edi
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	jbe	.L336
	subl	$48, %r8d
	cmpb	$9, %r8b
	ja	.L573
	.p2align 4,,10
	.p2align 3
.L336:
	cmpq	$8, %rcx
	jne	.L334
	movzbl	7(%rax), %ecx
	movl	%ecx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L334
	subl	$48, %ecx
	cmpb	$9, %cl
	ja	.L573
	.p2align 4,,10
	.p2align 3
.L334:
	movq	%rsi, %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$5, %rax
	cmpq	%rbx, %rax
	ja	.L347
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L304:
	testq	%r12, %r12
	jne	.L587
	movq	-840(%rbp), %rax
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L583:
	leaq	-360(%rbp), %rdi
	leaq	-688(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -360(%rbp)
	movq	%rax, %rdi
	movq	-688(%rbp), %rax
	movq	%rax, -344(%rbp)
.L303:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-688(%rbp), %r12
	movq	-360(%rbp), %rax
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L582:
	movq	%r12, %rdi
	leaq	-624(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -592(%rbp)
	movq	%rax, %rdi
	movq	-624(%rbp), %rax
	movq	%rax, -576(%rbp)
.L298:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-624(%rbp), %rax
	movq	-592(%rbp), %rdx
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L357:
	movq	-784(%rbp), %rbx
	leaq	-744(%rbp), %rax
	movq	-776(%rbp), %rsi
	movl	$0, -744(%rbp)
	movq	%rax, %rdx
	movq	%rax, -856(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_6713LocaleBuilder5buildER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	-744(%rbp), %esi
	testl	%esi, %esi
	jg	.L359
	leaq	-624(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	-800(%rbp), %rsi
	leaq	-736(%rbp), %r9
	leaq	.LC22(%rip), %r8
	movq	%rax, %rcx
	movq	$0, -736(%rbp)
	leaq	.LC23(%rip), %rdx
	movq	%r9, -840(%rbp)
	movq	$0, -608(%rbp)
	movq	%rax, -808(%rbp)
	movaps	%xmm0, -624(%rbp)
	call	_ZN2v88internal4Intl15GetStringOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcSt6vectorIS8_SaIS8_EES8_PSt10unique_ptrIA_cSt14default_deleteISD_EE@PLT
	movq	-624(%rbp), %rdi
	movl	%eax, %r12d
	testq	%rdi, %rdi
	je	.L360
	call	_ZdlPv@PLT
.L360:
	movl	%r12d, %ebx
	testb	%r12b, %r12b
	je	.L385
	movl	%r12d, %eax
	movzbl	%ah, %ebx
	testb	%bl, %bl
	je	.L363
	movq	-736(%rbp), %rsi
	movq	-808(%rbp), %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-776(%rbp), %r14
	movq	-624(%rbp), %rsi
	movq	-616(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6713LocaleBuilder11setLanguageENS_11StringPieceE@PLT
	movq	%r14, %rsi
	movq	-784(%rbp), %r14
	movq	-856(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6713LocaleBuilder5buildER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	-744(%rbp), %ecx
	movq	-736(%rbp), %r12
	testl	%ecx, %ecx
	jle	.L588
.L434:
	movb	$0, -866(%rbp)
	.p2align 4,,10
	.p2align 3
.L362:
	testq	%r12, %r12
	je	.L386
	movq	%r12, %rdi
	call	_ZdaPv@PLT
.L386:
	movq	%r13, %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	testb	%bl, %bl
	je	.L577
	cmpb	$0, -866(%rbp)
	je	.L358
	testq	%r15, %r15
	je	.L589
	leaq	.LC36(%rip), %rax
	movl	$32, %edi
	leaq	.LC35(%rip), %rcx
	movq	$0, -704(%rbp)
	movq	%rax, %xmm2
	movq	%rcx, %xmm0
	leaq	.LC38(%rip), %rax
	punpcklqdq	%xmm2, %xmm0
	leaq	.LC37(%rip), %rcx
	movq	%rax, %xmm3
	movaps	%xmm0, -448(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -720(%rbp)
	call	_Znwm@PLT
	leaq	.LC33(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-448(%rbp), %xmm4
	leaq	32(%rax), %rdx
	movq	%rcx, %xmm1
	movl	$24, %edi
	movdqa	-432(%rbp), %xmm5
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rax, -720(%rbp)
	leaq	.LC34(%rip), %rax
	movq	%rax, %xmm6
	leaq	.LC39(%rip), %rax
	movq	%rdx, -704(%rbp)
	punpcklqdq	%xmm6, %xmm1
	movq	%rdx, -712(%rbp)
	movaps	%xmm0, -688(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -672(%rbp)
	movaps	%xmm1, -448(%rbp)
	call	_Znwm@PLT
	movq	-432(%rbp), %rcx
	leaq	-424(%rbp), %rdi
	movdqa	-448(%rbp), %xmm7
	leaq	24(%rax), %rdx
	pxor	%xmm0, %xmm0
	movq	%rax, -688(%rbp)
	movq	-784(%rbp), %r12
	movq	%rcx, 16(%rax)
	movl	$21, %ecx
	movups	%xmm7, (%rax)
	xorl	%eax, %eax
	rep stosq
	leaq	.LC3(%rip), %rax
	leaq	.LC32(%rip), %rcx
	movaps	%xmm0, -656(%rbp)
	movq	%rax, %xmm2
	movq	%rcx, %xmm0
	leaq	.LC8(%rip), %rax
	movq	%rdx, -672(%rbp)
	punpcklqdq	%xmm2, %xmm0
	leaq	.LC31(%rip), %rcx
	movq	%rax, %xmm3
	movq	%rdx, -680(%rbp)
	movaps	%xmm0, -448(%rbp)
	movq	%rcx, %xmm0
	leaq	.LC9(%rip), %rax
	leaq	.LC30(%rip), %rcx
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, %xmm4
	leaq	.LC7(%rip), %rax
	movq	$0, -640(%rbp)
	movaps	%xmm0, -416(%rbp)
	movq	%rcx, %xmm0
	movq	%rax, %xmm5
	leaq	.LC29(%rip), %rcx
	punpcklqdq	%xmm4, %xmm0
	leaq	.LC10(%rip), %rax
	movb	$1, -296(%rbp)
	movq	%rax, %xmm6
	movaps	%xmm0, -384(%rbp)
	leaq	.LC12(%rip), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm5, %xmm0
	leaq	.LC28(%rip), %rcx
	movq	%rax, %xmm7
	movq	%r13, -432(%rbp)
	leaq	-720(%rbp), %rax
	movaps	%xmm0, -352(%rbp)
	movq	%rcx, %xmm0
	leaq	.LC27(%rip), %rcx
	movq	%rax, -368(%rbp)
	leaq	-688(%rbp), %rax
	punpcklqdq	%xmm6, %xmm0
	movq	%rax, -336(%rbp)
	leaq	-256(%rbp), %rax
	movaps	%xmm0, -320(%rbp)
	movq	%rcx, %xmm0
	movq	%r13, -400(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movq	%r13, -304(%rbp)
	movq	%r13, -272(%rbp)
	movq	%rax, -832(%rbp)
	movaps	%xmm0, -288(%rbp)
.L402:
	movzbl	24(%r12), %r14d
	movb	$0, -753(%rbp)
	movq	$0, -752(%rbp)
	testb	%r14b, %r14b
	je	.L391
	movq	(%r12), %rdx
	movq	-800(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-753(%rbp), %r8
	leaq	.LC40(%rip), %rcx
	call	_ZN2v88internal4Intl13GetBoolOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcS8_Pb@PLT
	movl	%eax, %ebx
	movl	%eax, %r13d
.L392:
	movb	%r13b, -824(%rbp)
	testb	%r13b, %r13b
	je	.L397
	movzbl	%bh, %ebx
	testb	%bl, %bl
	je	.L590
	testb	%r14b, %r14b
	je	.L576
	cmpb	$0, -753(%rbp)
	je	.L405
	movq	3464(%r15), %rax
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	-840(%rbp), %rsi
	movq	-808(%rbp), %rdi
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
.L406:
	movq	-624(%rbp), %rsi
	movq	-752(%rbp), %rdi
	movq	$0, -624(%rbp)
	movq	%rsi, -752(%rbp)
	testq	%rdi, %rdi
	je	.L404
	call	_ZdaPv@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L576
	call	_ZdaPv@PLT
.L576:
	movq	-752(%rbp), %rsi
.L404:
	movq	8(%r12), %r14
	movq	%rsi, -816(%rbp)
	movq	%r14, %rdi
	call	uloc_toLegacyKey_67@PLT
	movq	-816(%rbp), %rsi
	movq	%rax, %rdi
	call	uloc_toLegacyType_67@PLT
	testq	%rax, %rax
	je	.L464
	movq	-752(%rbp), %rsi
	movq	-808(%rbp), %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-840(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-776(%rbp), %rdi
	movq	-624(%rbp), %rcx
	movq	-616(%rbp), %r8
	movq	-736(%rbp), %rsi
	movq	-728(%rbp), %rdx
	call	_ZN6icu_6713LocaleBuilder23setUnicodeLocaleKeywordENS_11StringPieceES1_@PLT
	movq	-752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L400
.L575:
	call	_ZdaPv@PLT
.L400:
	addq	$32, %r12
	cmpq	-832(%rbp), %r12
	jne	.L402
	movb	%r13b, -865(%rbp)
.L401:
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L411
	call	_ZdlPv@PLT
.L411:
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L412
	call	_ZdlPv@PLT
.L412:
	movq	-720(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L413
	call	_ZdlPv@PLT
.L413:
	cmpb	$0, -824(%rbp)
	je	.L577
	movq	-808(%rbp), %rdx
	movq	-776(%rbp), %rsi
	movl	$0, -624(%rbp)
	movq	-784(%rbp), %rdi
	call	_ZN6icu_6713LocaleBuilder5buildER10UErrorCode@PLT
	cmpb	$0, -865(%rbp)
	je	.L414
	movl	-624(%rbp), %eax
	testl	%eax, %eax
	jle	.L415
.L414:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$73, %esi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L416:
	movq	-784(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L317:
	leaq	-5(%rcx), %rax
	cmpq	$3, %rax
	ja	.L316
	movq	(%rdx), %rax
	movzbl	(%rax), %edi
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	ja	.L316
	movzbl	1(%rax), %edi
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	ja	.L316
	movzbl	2(%rax), %edi
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	ja	.L316
	movzbl	3(%rax), %edi
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	ja	.L316
	movzbl	4(%rax), %edi
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	ja	.L316
	cmpq	$5, %rcx
	je	.L318
	movzbl	5(%rax), %edi
	xorl	%ebx, %ebx
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	ja	.L316
	cmpq	$6, %rcx
	je	.L318
	movzbl	6(%rax), %edi
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	ja	.L316
	cmpq	$8, %rcx
	jne	.L318
	movzbl	7(%rax), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L316
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L320:
	cmpq	$4, %rbx
	jne	.L459
	movq	32(%rdx), %rcx
	movzbl	(%rcx), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L459
	movzbl	1(%rcx), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L459
	movzbl	2(%rcx), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L459
	movzbl	3(%rcx), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L459
	cmpq	$64, %rdi
	je	.L321
	movl	$64, %eax
	movl	$2, %ebx
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$32, %eax
	movl	$1, %ebx
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L343:
	movzbl	1(%rdi), %edx
	movl	%edx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L345
	subl	$48, %edx
	cmpb	$9, %dl
	ja	.L344
	.p2align 4,,10
	.p2align 3
.L345:
	movzbl	2(%rdi), %edx
	movl	%edx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L346
	subl	$48, %edx
	cmpb	$9, %dl
	ja	.L344
.L346:
	cmpq	%r12, %rdi
	je	.L572
	call	_ZdlPv@PLT
.L572:
	movq	-624(%rbp), %rdx
	movq	-616(%rbp), %rsi
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L391:
	movq	16(%r12), %r13
	pxor	%xmm0, %xmm0
	movq	$0, -608(%rbp)
	movaps	%xmm0, -624(%rbp)
	movq	8(%r13), %rax
	movq	0(%r13), %rsi
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L591
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L592
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	0(%r13), %rsi
	movq	%rax, %rcx
	movq	8(%r13), %rax
	movq	%rax, %r13
	subq	%rsi, %r13
.L394:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -608(%rbp)
	movaps	%xmm0, -624(%rbp)
	cmpq	%rax, %rsi
	je	.L396
	movq	%rcx, %rdi
	movq	%r13, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L396:
	addq	%r13, %rcx
	movq	(%r12), %rdx
	movq	%r15, %rdi
	movq	-800(%rbp), %rsi
	movq	%rcx, -616(%rbp)
	movq	-808(%rbp), %rcx
	leaq	-752(%rbp), %r9
	leaq	.LC40(%rip), %r8
	call	_ZN2v88internal4Intl15GetStringOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcSt6vectorIS8_SaIS8_EES8_PSt10unique_ptrIA_cSt14default_deleteISD_EE@PLT
	movq	-624(%rbp), %rdi
	movl	%eax, %ebx
	movl	%eax, %r13d
	testq	%rdi, %rdi
	je	.L392
	call	_ZdlPv@PLT
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L590:
	movq	-752(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L575
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L405:
	movq	2520(%r15), %rax
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	-856(%rbp), %rsi
	movq	-808(%rbp), %rdi
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L591:
	movq	%rbx, %r13
	xorl	%ecx, %ecx
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L464:
	movb	%bl, -824(%rbp)
.L397:
	movq	-752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L401
	call	_ZdaPv@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L323:
	cmpq	$3, %rcx
	jne	.L324
	movq	(%rax), %rcx
	movzbl	(%rcx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L324
	movzbl	1(%rcx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L324
	movzbl	2(%rcx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L325
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L581:
	leaq	.LC19(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L371:
	cmpq	-880(%rbp), %rdi
	jne	.L593
.L363:
	movq	-808(%rbp), %rcx
	movq	-800(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	leaq	-720(%rbp), %r9
	leaq	.LC22(%rip), %r8
	movq	$0, -720(%rbp)
	leaq	.LC24(%rip), %rdx
	movaps	%xmm0, -624(%rbp)
	movq	$0, -608(%rbp)
	call	_ZN2v88internal4Intl15GetStringOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcSt6vectorIS8_SaIS8_EES8_PSt10unique_ptrIA_cSt14default_deleteISD_EE@PLT
	movq	-624(%rbp), %rdi
	movl	%eax, %r12d
	testq	%rdi, %rdi
	je	.L374
	call	_ZdlPv@PLT
.L374:
	movl	%r12d, %ebx
	testb	%r12b, %r12b
	je	.L375
	movl	%r12d, %eax
	movzbl	%ah, %ebx
	testb	%bl, %bl
	je	.L380
	movq	-720(%rbp), %rsi
	movq	-808(%rbp), %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-776(%rbp), %r14
	movq	-624(%rbp), %rsi
	movq	-616(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6713LocaleBuilder9setScriptENS_11StringPieceE@PLT
	movq	%r14, %rsi
	movq	-784(%rbp), %r14
	movq	-856(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6713LocaleBuilder5buildER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	-744(%rbp), %edx
	movb	$0, -866(%rbp)
	testl	%edx, %edx
	jle	.L594
	.p2align 4,,10
	.p2align 3
.L375:
	movq	-720(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L385
.L439:
	call	_ZdaPv@PLT
.L385:
	movq	-736(%rbp), %r12
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L589:
	leaq	.LC26(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L415:
	movq	-784(%rbp), %rdi
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	call	_Znwm@PLT
	movq	%rax, %r12
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r12)
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%r12)
	movq	32(%r15), %rax
	subq	48(%r15), %rax
	movq	%rbx, 16(%r12)
	cmpq	$33554432, %rax
	jg	.L595
.L417:
	movq	%rbx, %xmm0
	movq	%r12, %xmm1
	movl	$16, %edi
	punpcklqdq	%xmm1, %xmm0
	leaq	8(%r12), %r13
	movaps	%xmm0, -800(%rbp)
	call	_Znwm@PLT
	movdqa	-800(%rbp), %xmm0
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, %rbx
	movups	%xmm0, (%rax)
	je	.L596
	lock addl	$1, 0(%r13)
.L418:
	movl	$48, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, %r14
	movups	%xmm0, 8(%rax)
	movq	%rbx, 24(%rax)
	movq	%r14, %rsi
	movq	$0, (%rax)
	leaq	_ZN2v88internal7ManagedIN6icu_676LocaleEE10DestructorEPv(%rip), %rax
	movq	%rax, 32(%r14)
	movq	$0, 40(%r14)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r15), %rdi
	movq	(%rax), %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, 40(%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L419
	movl	$-1, %eax
	lock xaddl	%eax, 0(%r13)
.L420:
	cmpl	$1, %eax
	je	.L597
.L422:
	movq	-848(%rbp), %rsi
	movq	(%rsi), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L426
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r12
.L427:
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 23(%r14)
	leaq	23(%r14), %r15
	testb	$1, %r13b
	je	.L416
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L429
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L429:
	testb	$24, %al
	je	.L416
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L416
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L416
.L426:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r12
	jmp	.L427
.L595:
	movq	%r15, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L417
.L594:
	movq	-720(%rbp), %rdi
	cmpb	$0, (%rdi)
	je	.L439
.L380:
	movq	-808(%rbp), %rcx
	movq	-800(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	leaq	-688(%rbp), %r9
	leaq	.LC22(%rip), %r8
	movq	$0, -688(%rbp)
	leaq	.LC25(%rip), %rdx
	movaps	%xmm0, -624(%rbp)
	movq	$0, -608(%rbp)
	call	_ZN2v88internal4Intl15GetStringOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcSt6vectorIS8_SaIS8_EES8_PSt10unique_ptrIA_cSt14default_deleteISD_EE@PLT
	movq	-624(%rbp), %rdi
	movl	%eax, %r12d
	testq	%rdi, %rdi
	je	.L378
	call	_ZdlPv@PLT
.L378:
	movq	-688(%rbp), %rdi
	movl	%r12d, %ebx
	testb	%r12b, %r12b
	je	.L381
	movl	%r12d, %eax
	movb	$1, -866(%rbp)
	movzbl	%ah, %eax
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L598
.L381:
	testq	%rdi, %rdi
	je	.L375
.L383:
	call	_ZdaPv@PLT
	jmp	.L375
.L596:
	addl	$1, 8(%r12)
	jmp	.L418
.L588:
	cmpb	$0, (%r12)
	je	.L434
	movq	-880(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, -448(%rbp)
	call	strlen@PLT
	movq	%rax, -624(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L599
	cmpq	$1, %rax
	jne	.L368
	movzbl	(%r12), %eax
	movb	%al, -432(%rbp)
.L369:
	movq	-624(%rbp), %rax
	movq	-448(%rbp), %rdx
	movq	%rax, -440(%rbp)
	movb	$0, (%rdx,%rax)
	cmpq	$4, -440(%rbp)
	movq	-448(%rbp), %rdi
	jne	.L371
	movzbl	(%rdi), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L371
	movzbl	1(%rdi), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L371
	movzbl	2(%rdi), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L371
	movzbl	3(%rdi), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L371
	cmpq	-880(%rbp), %rdi
	je	.L574
	call	_ZdlPv@PLT
.L574:
	movq	-736(%rbp), %r12
	jmp	.L434
.L419:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L420
.L597:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L423
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L424:
	cmpl	$1, %eax
	jne	.L422
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L422
.L423:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L424
.L598:
	movq	%rdi, %rsi
	movq	-808(%rbp), %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-776(%rbp), %rbx
	movq	-624(%rbp), %rsi
	movq	-616(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_6713LocaleBuilder9setRegionENS_11StringPieceE@PLT
	movq	%rbx, %rsi
	movq	-784(%rbp), %rbx
	movq	-856(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_6713LocaleBuilder5buildER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	cmpl	$0, -744(%rbp)
	movq	-688(%rbp), %rdi
	jle	.L382
	movb	$0, -866(%rbp)
	movl	%r12d, %ebx
	jmp	.L381
.L599:
	movq	-784(%rbp), %rdi
	movq	-808(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -448(%rbp)
	movq	%rax, %rdi
	movq	-624(%rbp), %rax
	movq	%rax, -432(%rbp)
.L367:
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	jmp	.L369
.L368:
	testq	%rax, %rax
	je	.L369
	movq	-880(%rbp), %rdi
	jmp	.L367
.L579:
	call	__stack_chk_fail@PLT
.L382:
	cmpb	$0, (%rdi)
	movl	%r12d, %ebx
	setne	-866(%rbp)
	jmp	.L383
.L309:
	leaq	.LC13(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L592:
	call	_ZSt17__throw_bad_allocv@PLT
.L587:
	movq	-840(%rbp), %rdi
	jmp	.L303
.L593:
	call	_ZdlPv@PLT
	jmp	.L363
.L585:
	movq	-832(%rbp), %rdi
	jmp	.L298
	.cfi_endproc
.LFE18407:
	.size	_ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE, .-_ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE
	.weak	_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.data.rel.ro._ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"awG",@progbits,_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,comdat
	.align 8
	.type	_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @object
	.size	_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, 56
_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
	.quad	0
	.quad	0
	.quad	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev
	.quad	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.quad	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci
	.quad	_ZN6icu_678ByteSink15GetAppendBufferEiiPciPi
	.quad	_ZN6icu_678ByteSink5FlushEv
	.weak	_ZTVSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_676LocaleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.data.rel.ro.local,"aw"
	.align 8
.LC11:
	.quad	_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
