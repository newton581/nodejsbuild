	.file	"builtins-reflect.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internalL23Builtin_Impl_ReflectSetENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Reflect.set"
.LC1:
	.string	"(location_) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL23Builtin_Impl_ReflectSetENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL23Builtin_Impl_ReflectSetENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL23Builtin_Impl_ReflectSetENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18391:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	cmpl	$5, %edi
	jg	.L6
	leaq	88(%rdx), %r15
	movq	%r15, %r8
.L7:
	movq	%r8, %r9
.L32:
	movq	(%r15), %rax
	movq	%r15, %r14
	testb	$1, %al
	jne	.L12
.L15:
	leaq	.LC0(%rip), %rax
	xorl	%edx, %edx
	leaq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	movq	$11, -136(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L48
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$26, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L20:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L30
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L30:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$232, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L50
	leaq	-16(%rsi), %r8
	cmpl	$7, %edi
	je	.L51
	leaq	-24(%rsi), %r9
	cmpl	$8, %edi
	je	.L32
	movq	(%r15), %rax
	leaq	-32(%rsi), %r14
	testb	$1, %al
	je	.L15
.L12:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L15
	movq	(%r8), %rax
	testb	$1, %al
	jne	.L16
.L19:
	movq	%r8, %rsi
	movq	%r12, %rdi
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-248(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L47
.L18:
	movq	(%r8), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L52
.L22:
	movq	(%r8), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	je	.L53
.L26:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	%r12, -200(%rbp)
	movq	(%r8), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L54
.L27:
	movq	%r14, -176(%rbp)
	leaq	-224(%rbp), %r14
	movq	%r14, %rdi
	movq	%r9, -248(%rbp)
	movq	%r8, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -168(%rbp)
	movq	%r15, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	-248(%rbp), %r9
.L25:
	xorl	%edx, %edx
	movq	%r9, %rsi
	movq	%r14, %rdi
	movabsq	$4294967297, %rcx
	call	_ZN2v88internal6Object16SetSuperPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L47
	movzbl	%ah, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r14
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L47:
	movq	312(%r12), %r14
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L19
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L53:
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L23
	testb	$2, %al
	jne	.L22
.L23:
	leaq	-144(%rbp), %rdi
	leaq	-228(%rbp), %rsi
	movq	%r9, -256(%rbp)
	movq	%r8, -248(%rbp)
	movq	%rdi, -264(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-248(%rbp), %r8
	movq	-256(%rbp), %r9
	testb	%al, %al
	je	.L22
	movabsq	$824633720832, %rax
	movq	%r14, -96(%rbp)
	movq	-264(%rbp), %rdi
	leaq	-224(%rbp), %r14
	movq	%rax, -132(%rbp)
	movl	-228(%rbp), %eax
	movl	$3, -144(%rbp)
	movq	%r12, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movq	%r15, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movdqa	-128(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm3
	movq	-248(%rbp), %r8
	movdqa	-144(%rbp), %xmm0
	movdqa	-80(%rbp), %xmm4
	movq	-256(%rbp), %r9
	movaps	%xmm1, -208(%rbp)
	movq	%r8, -112(%rbp)
	movdqa	-112(%rbp), %xmm2
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%r8, %rsi
	movq	%r12, %rdi
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-248(%rbp), %r9
	movq	%rax, %r8
	jmp	.L27
.L49:
	call	__stack_chk_fail@PLT
.L51:
	leaq	88(%rdx), %r9
	jmp	.L32
.L50:
	leaq	88(%rdx), %r8
	jmp	.L7
	.cfi_endproc
.LFE18391:
	.size	_ZN2v88internalL23Builtin_Impl_ReflectSetENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL23Builtin_Impl_ReflectSetENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL27Builtin_Impl_ReflectOwnKeysENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"Reflect.ownKeys"
	.section	.text._ZN2v88internalL27Builtin_Impl_ReflectOwnKeysENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Builtin_Impl_ReflectOwnKeysENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL27Builtin_Impl_ReflectOwnKeysENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18388:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L56
.L59:
	leaq	.LC3(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$15, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L70
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$26, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L62:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L64
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L64:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$32, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L59
	leaq	-8(%rsi), %rdi
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L72
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L72:
	movq	312(%r12), %r14
	jmp	.L62
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18388:
	.size	_ZN2v88internalL27Builtin_Impl_ReflectOwnKeysENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL27Builtin_Impl_ReflectOwnKeysENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Builtin_Impl_ReflectGetOwnPropertyDescriptorENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"Reflect.getOwnPropertyDescriptor"
	.section	.text._ZN2v88internalL44Builtin_Impl_ReflectGetOwnPropertyDescriptorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_ReflectGetOwnPropertyDescriptorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_ReflectGetOwnPropertyDescriptorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18385:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L74
.L77:
	leaq	.LC4(%rip), %rax
	xorl	%edx, %edx
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	$32, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L92
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$26, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L82:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L86
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L86:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L77
	movq	-16(%rsi), %rax
	leaq	-8(%rsi), %r14
	leaq	-16(%rsi), %rdx
	testb	$1, %al
	jne	.L78
.L81:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L91
.L80:
	leaq	-96(%rbp), %r15
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r15, %rcx
	andb	$-64, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorE@PLT
	testb	%al, %al
	je	.L91
	shrw	$8, %ax
	jne	.L84
	movq	88(%r12), %r14
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L91:
	movq	312(%r12), %r14
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L92:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L81
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE@PLT
	movq	(%rax), %r14
	jmp	.L82
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18385:
	.size	_ZN2v88internalL44Builtin_Impl_ReflectGetOwnPropertyDescriptorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_ReflectGetOwnPropertyDescriptorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Builtin_Impl_ReflectDefinePropertyENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"Reflect.defineProperty"
	.section	.text._ZN2v88internalL34Builtin_Impl_ReflectDefinePropertyENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Builtin_Impl_ReflectDefinePropertyENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL34Builtin_Impl_ReflectDefinePropertyENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18382:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L95
.L98:
	leaq	.LC5(%rip), %rax
	xorl	%edx, %edx
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	$22, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L113
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$26, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L103:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L107
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L107:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L98
	movq	-16(%rsi), %rax
	leaq	-8(%rsi), %r15
	leaq	-24(%rsi), %r14
	leaq	-16(%rsi), %r9
	testb	$1, %al
	jne	.L99
.L102:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L112
.L101:
	leaq	-96(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rcx, %rdx
	andb	$-64, -96(%rbp)
	movq	%r9, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal18PropertyDescriptor20ToPropertyDescriptorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %r9
	testb	%al, %al
	jne	.L104
.L112:
	movq	312(%r12), %r14
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L113:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L99:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L102
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L104:
	movabsq	$4294967297, %r8
	movq	%r9, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L112
	movzbl	%ah, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r14
	jmp	.L103
.L114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18382:
	.size	_ZN2v88internalL34Builtin_Impl_ReflectDefinePropertyENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL34Builtin_Impl_ReflectDefinePropertyENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L121
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L124
.L115:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L115
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL40Builtin_Impl_Stats_ReflectDefinePropertyEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC7:
	.string	"V8.Builtin_ReflectDefineProperty"
	.section	.text._ZN2v88internalL40Builtin_Impl_Stats_ReflectDefinePropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_Stats_ReflectDefinePropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_Stats_ReflectDefinePropertyEiPmPNS0_7IsolateE:
.LFB18380:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L154
.L126:
	movq	_ZZN2v88internalL40Builtin_Impl_Stats_ReflectDefinePropertyEiPmPNS0_7IsolateEE27trace_event_unique_atomic20(%rip), %rbx
	testq	%rbx, %rbx
	je	.L155
.L128:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L156
.L130:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL34Builtin_Impl_ReflectDefinePropertyENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L157
.L134:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L158
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L159
.L129:
	movq	%rbx, _ZZN2v88internalL40Builtin_Impl_Stats_ReflectDefinePropertyEiPmPNS0_7IsolateEE27trace_event_unique_atomic20(%rip)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L156:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L160
.L131:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L132
	movq	(%rdi), %rax
	call	*8(%rax)
.L132:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L133
	movq	(%rdi), %rax
	call	*8(%rax)
.L133:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L157:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L154:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$809, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L160:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L159:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L129
.L158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18380:
	.size	_ZN2v88internalL40Builtin_Impl_Stats_ReflectDefinePropertyEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_Stats_ReflectDefinePropertyEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL50Builtin_Impl_Stats_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"V8.Builtin_ReflectGetOwnPropertyDescriptor"
	.section	.text._ZN2v88internalL50Builtin_Impl_Stats_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL50Builtin_Impl_Stats_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL50Builtin_Impl_Stats_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE:
.LFB18383:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L190
.L162:
	movq	_ZZN2v88internalL50Builtin_Impl_Stats_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateEE27trace_event_unique_atomic50(%rip), %rbx
	testq	%rbx, %rbx
	je	.L191
.L164:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L192
.L166:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL44Builtin_Impl_ReflectGetOwnPropertyDescriptorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L193
.L170:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L194
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L195
.L165:
	movq	%rbx, _ZZN2v88internalL50Builtin_Impl_Stats_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateEE27trace_event_unique_atomic50(%rip)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L192:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L196
.L167:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L168
	movq	(%rdi), %rax
	call	*8(%rax)
.L168:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L169
	movq	(%rdi), %rax
	call	*8(%rax)
.L169:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L193:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L190:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$810, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L196:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L195:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L165
.L194:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18383:
	.size	_ZN2v88internalL50Builtin_Impl_Stats_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE, .-_ZN2v88internalL50Builtin_Impl_Stats_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Builtin_Impl_Stats_ReflectOwnKeysEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"V8.Builtin_ReflectOwnKeys"
	.section	.text._ZN2v88internalL33Builtin_Impl_Stats_ReflectOwnKeysEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Builtin_Impl_Stats_ReflectOwnKeysEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Builtin_Impl_Stats_ReflectOwnKeysEiPmPNS0_7IsolateE:
.LFB18386:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L226
.L198:
	movq	_ZZN2v88internalL33Builtin_Impl_Stats_ReflectOwnKeysEiPmPNS0_7IsolateEE27trace_event_unique_atomic76(%rip), %rbx
	testq	%rbx, %rbx
	je	.L227
.L200:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L228
.L202:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL27Builtin_Impl_ReflectOwnKeysENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L229
.L206:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L231
.L201:
	movq	%rbx, _ZZN2v88internalL33Builtin_Impl_Stats_ReflectOwnKeysEiPmPNS0_7IsolateEE27trace_event_unique_atomic76(%rip)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L228:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L232
.L203:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L204
	movq	(%rdi), %rax
	call	*8(%rax)
.L204:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L205
	movq	(%rdi), %rax
	call	*8(%rax)
.L205:
	leaq	.LC9(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L226:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$811, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L232:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L231:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L201
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18386:
	.size	_ZN2v88internalL33Builtin_Impl_Stats_ReflectOwnKeysEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Builtin_Impl_Stats_ReflectOwnKeysEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Builtin_Impl_Stats_ReflectSetEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"V8.Builtin_ReflectSet"
	.section	.text._ZN2v88internalL29Builtin_Impl_Stats_ReflectSetEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Builtin_Impl_Stats_ReflectSetEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Builtin_Impl_Stats_ReflectSetEiPmPNS0_7IsolateE:
.LFB18389:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L262
.L234:
	movq	_ZZN2v88internalL29Builtin_Impl_Stats_ReflectSetEiPmPNS0_7IsolateEE27trace_event_unique_atomic98(%rip), %rbx
	testq	%rbx, %rbx
	je	.L263
.L236:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L264
.L238:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL23Builtin_Impl_ReflectSetENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L265
.L242:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L266
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L267
.L237:
	movq	%rbx, _ZZN2v88internalL29Builtin_Impl_Stats_ReflectSetEiPmPNS0_7IsolateEE27trace_event_unique_atomic98(%rip)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L264:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L268
.L239:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L240
	movq	(%rdi), %rax
	call	*8(%rax)
.L240:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L241
	movq	(%rdi), %rax
	call	*8(%rax)
.L241:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L265:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L262:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$812, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L268:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L267:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L237
.L266:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18389:
	.size	_ZN2v88internalL29Builtin_Impl_Stats_ReflectSetEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Builtin_Impl_Stats_ReflectSetEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Builtin_ReflectDefinePropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Builtin_ReflectDefinePropertyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Builtin_ReflectDefinePropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Builtin_ReflectDefinePropertyEiPmPNS0_7IsolateE:
.LFB18381:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L273
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL34Builtin_Impl_ReflectDefinePropertyENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore 6
	jmp	_ZN2v88internalL40Builtin_Impl_Stats_ReflectDefinePropertyEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18381:
	.size	_ZN2v88internal29Builtin_ReflectDefinePropertyEiPmPNS0_7IsolateE, .-_ZN2v88internal29Builtin_ReflectDefinePropertyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal39Builtin_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Builtin_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal39Builtin_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE, @function
_ZN2v88internal39Builtin_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE:
.LFB18384:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L278
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL44Builtin_Impl_ReflectGetOwnPropertyDescriptorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore 6
	jmp	_ZN2v88internalL50Builtin_Impl_Stats_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18384:
	.size	_ZN2v88internal39Builtin_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE, .-_ZN2v88internal39Builtin_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Builtin_ReflectOwnKeysEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Builtin_ReflectOwnKeysEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Builtin_ReflectOwnKeysEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Builtin_ReflectOwnKeysEiPmPNS0_7IsolateE:
.LFB18387:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L283
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL27Builtin_Impl_ReflectOwnKeysENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore 6
	jmp	_ZN2v88internalL33Builtin_Impl_Stats_ReflectOwnKeysEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18387:
	.size	_ZN2v88internal22Builtin_ReflectOwnKeysEiPmPNS0_7IsolateE, .-_ZN2v88internal22Builtin_ReflectOwnKeysEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Builtin_ReflectSetEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Builtin_ReflectSetEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Builtin_ReflectSetEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Builtin_ReflectSetEiPmPNS0_7IsolateE:
.LFB18390:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L288
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL23Builtin_Impl_ReflectSetENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore 6
	jmp	_ZN2v88internalL29Builtin_Impl_Stats_ReflectSetEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18390:
	.size	_ZN2v88internal18Builtin_ReflectSetEiPmPNS0_7IsolateE, .-_ZN2v88internal18Builtin_ReflectSetEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal29Builtin_ReflectDefinePropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal29Builtin_ReflectDefinePropertyEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal29Builtin_ReflectDefinePropertyEiPmPNS0_7IsolateE:
.LFB22114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22114:
	.size	_GLOBAL__sub_I__ZN2v88internal29Builtin_ReflectDefinePropertyEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal29Builtin_ReflectDefinePropertyEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal29Builtin_ReflectDefinePropertyEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL29Builtin_Impl_Stats_ReflectSetEiPmPNS0_7IsolateEE27trace_event_unique_atomic98,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Builtin_Impl_Stats_ReflectSetEiPmPNS0_7IsolateEE27trace_event_unique_atomic98, @object
	.size	_ZZN2v88internalL29Builtin_Impl_Stats_ReflectSetEiPmPNS0_7IsolateEE27trace_event_unique_atomic98, 8
_ZZN2v88internalL29Builtin_Impl_Stats_ReflectSetEiPmPNS0_7IsolateEE27trace_event_unique_atomic98:
	.zero	8
	.section	.bss._ZZN2v88internalL33Builtin_Impl_Stats_ReflectOwnKeysEiPmPNS0_7IsolateEE27trace_event_unique_atomic76,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Builtin_Impl_Stats_ReflectOwnKeysEiPmPNS0_7IsolateEE27trace_event_unique_atomic76, @object
	.size	_ZZN2v88internalL33Builtin_Impl_Stats_ReflectOwnKeysEiPmPNS0_7IsolateEE27trace_event_unique_atomic76, 8
_ZZN2v88internalL33Builtin_Impl_Stats_ReflectOwnKeysEiPmPNS0_7IsolateEE27trace_event_unique_atomic76:
	.zero	8
	.section	.bss._ZZN2v88internalL50Builtin_Impl_Stats_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateEE27trace_event_unique_atomic50,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL50Builtin_Impl_Stats_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateEE27trace_event_unique_atomic50, @object
	.size	_ZZN2v88internalL50Builtin_Impl_Stats_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateEE27trace_event_unique_atomic50, 8
_ZZN2v88internalL50Builtin_Impl_Stats_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateEE27trace_event_unique_atomic50:
	.zero	8
	.section	.bss._ZZN2v88internalL40Builtin_Impl_Stats_ReflectDefinePropertyEiPmPNS0_7IsolateEE27trace_event_unique_atomic20,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Builtin_Impl_Stats_ReflectDefinePropertyEiPmPNS0_7IsolateEE27trace_event_unique_atomic20, @object
	.size	_ZZN2v88internalL40Builtin_Impl_Stats_ReflectDefinePropertyEiPmPNS0_7IsolateEE27trace_event_unique_atomic20, 8
_ZZN2v88internalL40Builtin_Impl_Stats_ReflectDefinePropertyEiPmPNS0_7IsolateEE27trace_event_unique_atomic20:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
