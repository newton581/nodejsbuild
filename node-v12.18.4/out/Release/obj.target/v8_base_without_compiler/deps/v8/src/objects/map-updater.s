	.file	"map-updater.cc"
	.text
	.section	.text._ZN2v88internal10MapUpdaterC2EPNS0_7IsolateENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdaterC2EPNS0_7IsolateENS0_6HandleINS0_3MapEEE
	.type	_ZN2v88internal10MapUpdaterC2EPNS0_7IsolateENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal10MapUpdaterC2EPNS0_7IsolateENS0_6HandleINS0_3MapEEE:
.LFB17784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%rsi, (%rdi)
	movq	(%rdx), %rax
	movq	%rdx, 8(%rdi)
	movq	41112(%r12), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3:
	movq	8(%rbx), %rdx
	movq	%rax, 16(%rbx)
	pxor	%xmm0, %xmm0
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	(%rdx), %rax
	movl	15(%rax), %eax
	movups	%xmm0, 64(%rbx)
	movb	$0, 52(%rbx)
	shrl	$10, %eax
	movl	$0, 56(%rbx)
	andl	$1023, %eax
	movl	$0, 80(%rbx)
	movl	%eax, 48(%rbx)
	movq	(%rdx), %rax
	movq	$0, 96(%rbx)
	movzbl	14(%rax), %eax
	movl	$0, 104(%rbx)
	movb	$0, 108(%rbx)
	shrl	$3, %eax
	movups	%xmm0, 112(%rbx)
	cmpl	$5, %eax
	movb	%al, 84(%rbx)
	setbe	%dl
	cmpb	$3, %al
	setne	%al
	andl	%edx, %eax
	movb	%al, 85(%rbx)
	movl	$4294967295, %eax
	movq	%rax, 88(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L7
.L4:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L4
	.cfi_endproc
.LFE17784:
	.size	_ZN2v88internal10MapUpdaterC2EPNS0_7IsolateENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal10MapUpdaterC2EPNS0_7IsolateENS0_6HandleINS0_3MapEEE
	.globl	_ZN2v88internal10MapUpdaterC1EPNS0_7IsolateENS0_6HandleINS0_3MapEEE
	.set	_ZN2v88internal10MapUpdaterC1EPNS0_7IsolateENS0_6HandleINS0_3MapEEE,_ZN2v88internal10MapUpdaterC2EPNS0_7IsolateENS0_6HandleINS0_3MapEEE
	.section	.text._ZN2v88internal10MapUpdater15GeneralizeFieldENS0_6HandleINS0_3MapEEEiNS0_17PropertyConstnessENS0_14RepresentationENS2_INS0_9FieldTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdater15GeneralizeFieldENS0_6HandleINS0_3MapEEEiNS0_17PropertyConstnessENS0_14RepresentationENS2_INS0_9FieldTypeEEE
	.type	_ZN2v88internal10MapUpdater15GeneralizeFieldENS0_6HandleINS0_3MapEEEiNS0_17PropertyConstnessENS0_14RepresentationENS2_INS0_9FieldTypeEEE, @function
_ZN2v88internal10MapUpdater15GeneralizeFieldENS0_6HandleINS0_3MapEEEiNS0_17PropertyConstnessENS0_14RepresentationENS2_INS0_9FieldTypeEEE:
.LFB17795:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE@PLT
	.cfi_endproc
.LFE17795:
	.size	_ZN2v88internal10MapUpdater15GeneralizeFieldENS0_6HandleINS0_3MapEEEiNS0_17PropertyConstnessENS0_14RepresentationENS2_INS0_9FieldTypeEEE, .-_ZN2v88internal10MapUpdater15GeneralizeFieldENS0_6HandleINS0_3MapEEEiNS0_17PropertyConstnessENS0_14RepresentationENS2_INS0_9FieldTypeEEE
	.section	.text._ZN2v88internal10MapUpdater9NormalizeEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdater9NormalizeEPKc
	.type	_ZN2v88internal10MapUpdater9NormalizeEPKc, @function
_ZN2v88internal10MapUpdater9NormalizeEPKc:
.LFB17796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	84(%rdi), %edx
	movq	8(%rdi), %rsi
	movq	(%rdi), %rdi
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc@PLT
	movl	$4, 80(%rbx)
	movq	%rax, 40(%rbx)
	addq	$8, %rsp
	movl	$4, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17796:
	.size	_ZN2v88internal10MapUpdater9NormalizeEPKc, .-_ZN2v88internal10MapUpdater9NormalizeEPKc
	.section	.rodata._ZN2v88internal10MapUpdater32TryReconfigureToDataFieldInplaceEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"uninitialized field"
	.section	.text._ZN2v88internal10MapUpdater32TryReconfigureToDataFieldInplaceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdater32TryReconfigureToDataFieldInplaceEv
	.type	_ZN2v88internal10MapUpdater32TryReconfigureToDataFieldInplaceEv, @function
_ZN2v88internal10MapUpdater32TryReconfigureToDataFieldInplaceEv:
.LFB17797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$16777216, %edx
	jne	.L17
	cmpb	$0, 108(%rdi)
	jne	.L28
.L17:
	movl	80(%rbx), %eax
.L11:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L29
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	88(%rdi), %ecx
	movq	16(%rdi), %rsi
	leal	3(%rcx,%rcx,2), %edx
	movq	(%rsi), %rdi
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	7(%rdx,%rdi), %rdx
	movq	%rdx, %r14
	shrq	$38, %rdx
	sarq	$32, %r14
	andl	$7, %edx
	jne	.L15
	cmpb	$2, 108(%rbx)
	je	.L17
.L16:
	movq	(%rax), %rax
	movq	(%rbx), %r13
	movl	%ecx, %r8d
	leaq	-64(%rbp), %r15
	cmpb	$0, _ZN2v88internal25FLAG_trace_generalizationE(%rip)
	jne	.L30
.L18:
	movq	%r15, %rdi
	movl	%r8d, %edx
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi@PLT
	movq	41112(%r13), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L22
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L23:
	movl	100(%rbx), %ecx
	movl	88(%rbx), %edx
	movq	112(%rbx), %r9
	movzbl	108(%rbx), %r8d
	movq	(%rbx), %rdi
	call	_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE@PLT
	movq	8(%rbx), %rax
	movl	$4, 80(%rbx)
	movq	%rax, 40(%rbx)
	movl	$4, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L15:
	cmpb	$0, _ZN2v88internal40FLAG_modify_field_representation_inplaceE(%rip)
	je	.L17
	movl	%edx, %edi
	andl	$-3, %edi
	cmpb	$1, %dil
	jne	.L17
	cmpb	$4, 108(%rbx)
	jne	.L17
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L22:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L31
.L24:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r12, (%rsi)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rax, -64(%rbp)
	leal	3(%rcx,%rcx,2), %eax
	movq	(%rsi), %rcx
	sall	$3, %eax
	movl	%edx, -68(%rbp)
	movq	112(%rbx), %r12
	cltq
	movq	15(%rax,%rcx), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	41112(%r13), %rdi
	movl	-68(%rbp), %edx
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L19
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-68(%rbp), %edx
.L20:
	pushq	$0
	movl	100(%rbx), %ecx
	shrl	$2, %r14d
	leaq	-64(%rbp), %r15
	pushq	%r12
	movl	48(%rbx), %r9d
	andl	$1, %r14d
	movq	%r15, %rdi
	movl	88(%rbx), %r8d
	movq	(%rbx), %rsi
	pushq	$0
	pushq	%rax
	movzbl	108(%rbx), %eax
	pushq	%rcx
	leaq	.LC0(%rip), %rcx
	pushq	%r14
	pushq	%rax
	pushq	%rdx
	movq	stdout(%rip), %rdx
	pushq	$0
	pushq	%r9
	call	_ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_@PLT
	movq	8(%rbx), %rax
	movq	(%rbx), %r13
	addq	$80, %rsp
	movl	88(%rbx), %r8d
	movq	(%rax), %rax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L19:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L32
.L21:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L20
.L31:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L24
.L32:
	movq	%r13, %rdi
	movq	%rsi, -80(%rbp)
	movl	%edx, -68(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movl	-68(%rbp), %edx
	jmp	.L21
.L29:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17797:
	.size	_ZN2v88internal10MapUpdater32TryReconfigureToDataFieldInplaceEv, .-_ZN2v88internal10MapUpdater32TryReconfigureToDataFieldInplaceEv
	.section	.rodata._ZN2v88internal10MapUpdater32TrySaveIntegrityLevelTransitionsEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.rodata._ZN2v88internal10MapUpdater32TrySaveIntegrityLevelTransitionsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"old_map_->NumberOfOwnDescriptors() == integrity_source_map_->NumberOfOwnDescriptors()"
	.section	.rodata._ZN2v88internal10MapUpdater32TrySaveIntegrityLevelTransitionsEv.str1.1
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal10MapUpdater32TrySaveIntegrityLevelTransitionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdater32TrySaveIntegrityLevelTransitionsEv
	.type	_ZN2v88internal10MapUpdater32TrySaveIntegrityLevelTransitionsEv, @function
_ZN2v88internal10MapUpdater32TrySaveIntegrityLevelTransitionsEv:
.LFB17798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %sil
	je	.L34
	movq	-1(%rsi), %rdx
	cmpq	%rdx, 136(%rax)
	je	.L35
.L34:
	movq	88(%rax), %rsi
.L35:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L37:
	movq	(%r12), %rax
	movq	%rbx, -136(%rbp)
	movq	$0, -152(%rbp)
	movq	%rax, -144(%rbp)
	movq	(%rbx), %rax
	movq	$0, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -120(%rbp)
	testb	$1, %al
	je	.L39
	cmpl	$3, %eax
	je	.L39
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L75
	cmpq	$1, %rdx
	jne	.L43
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L76
	movl	$4, -112(%rbp)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$1, -112(%rbp)
.L41:
	movq	8(%r12), %rax
	leaq	56(%r12), %rcx
	leaq	-152(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal19TransitionsAccessor29HasIntegrityLevelTransitionToENS0_3MapEPNS0_6SymbolEPNS0_18PropertyAttributesE@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L33
	movq	(%r12), %r14
	movq	-152(%rbp), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L49:
	movq	%rax, %xmm0
	movq	%rbx, %xmm1
	leaq	-96(%rbp), %r14
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%r12)
	movq	(%rbx), %rax
	movl	15(%rax), %eax
	testl	$134217728, %eax
	jne	.L51
.L81:
	movq	72(%r12), %rax
	movq	(%r12), %r15
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %sil
	je	.L52
	movq	-1(%rsi), %rdx
	cmpq	%rdx, 136(%rax)
	je	.L53
.L52:
	movq	88(%rax), %rsi
.L53:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L54
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L55:
	movq	(%r12), %rax
	movq	%rbx, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L57
	cmpl	$3, %eax
	je	.L57
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L77
	cmpq	$1, %rdx
	je	.L78
.L43:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L79
.L50:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$1, -64(%rbp)
.L59:
	movq	72(%r12), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal19TransitionsAccessor29HasIntegrityLevelTransitionToENS0_3MapEPNS0_6SymbolEPNS0_18PropertyAttributesE@PLT
	testb	%al, %al
	je	.L80
	movq	%rbx, 72(%r12)
	movq	(%rbx), %rax
	movl	15(%rax), %eax
	testl	$134217728, %eax
	je	.L81
.L51:
	movq	72(%r12), %rcx
	movq	(%rcx), %rax
	movl	15(%rax), %edx
	movq	8(%r12), %rax
	movq	(%rax), %rax
	shrl	$10, %edx
	andl	$1023, %edx
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%eax, %edx
	jne	.L82
	movq	(%r12), %rbx
	movb	$1, 52(%r12)
	movq	(%rcx), %rax
	movq	41112(%rbx), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L66
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L67:
	movq	%rax, 16(%r12)
.L33:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	addq	$136, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	41088(%r15), %rbx
	cmpq	41096(%r15), %rbx
	je	.L84
.L56:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L36:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L85
.L38:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$3, -64(%rbp)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r15, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$3, -112(%rbp)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L78:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L61
	movl	$4, -64(%rbp)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L80:
	xorl	%r13d, %r13d
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%r13, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L38
.L61:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L59
.L66:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L86
.L68:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L67
.L79:
	movq	%r14, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	jmp	.L50
.L76:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -112(%rbp)
	jmp	.L41
.L82:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L86:
	movq	%rbx, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	jmp	.L68
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17798:
	.size	_ZN2v88internal10MapUpdater32TrySaveIntegrityLevelTransitionsEv, .-_ZN2v88internal10MapUpdater32TrySaveIntegrityLevelTransitionsEv
	.section	.rodata._ZN2v88internal10MapUpdater11FindRootMapEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"Normalize_NotEquivalent"
	.section	.rodata._ZN2v88internal10MapUpdater11FindRootMapEv.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"Normalize_PrivateSymbolsOnNonExtensible"
	.align 8
.LC6:
	.string	"Normalize_InvalidElementsTransition"
	.section	.rodata._ZN2v88internal10MapUpdater11FindRootMapEv.str1.1
.LC7:
	.string	"Normalize_RootModification1"
.LC8:
	.string	"Normalize_RootModification2"
.LC9:
	.string	"Normalize_RootModification4"
	.section	.text._ZN2v88internal10MapUpdater11FindRootMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdater11FindRootMapEv
	.type	_ZN2v88internal10MapUpdater11FindRootMapEv, @function
_ZN2v88internal10MapUpdater11FindRootMapEv:
.LFB17805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal3Map11FindRootMapEPNS0_7IsolateE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L88
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L89:
	movq	%rax, 24(%rbx)
	movq	(%rax), %rdx
	movzbl	84(%rbx), %r12d
	movzbl	14(%rdx), %r14d
	movl	15(%rdx), %edx
	andl	$16777216, %edx
	jne	.L136
	movq	8(%rbx), %rdx
	movq	%r13, %rdi
	movq	(%rdx), %rdx
	movq	%rdx, -48(%rbp)
	movq	(%rax), %rsi
	call	_ZNK2v88internal3Map25EquivalentToForTransitionES1_@PLT
	testb	%al, %al
	je	.L137
	movq	8(%rbx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %edx
	movq	24(%rbx), %rax
	movq	(%rax), %rax
	shrl	$27, %edx
	andl	$1, %edx
	movl	15(%rax), %eax
	shrl	$27, %eax
	andl	$1, %eax
	cmpb	%al, %dl
	je	.L99
	movq	%rbx, %rdi
	call	_ZN2v88internal10MapUpdater32TrySaveIntegrityLevelTransitionsEv
	testb	%al, %al
	je	.L138
	movq	72(%rbx), %rax
	movq	(%rax), %rax
	movzbl	14(%rax), %r12d
	shrl	$3, %r12d
.L99:
	movl	%r12d, %eax
	shrl	$3, %r14d
	andl	$-3, %eax
	cmpb	$12, %al
	je	.L101
	cmpb	%r14b, %r12b
	je	.L101
	cmpb	$16, %r12b
	je	.L101
	cmpb	$5, %r14b
	ja	.L104
	cmpl	$3, %r14d
	jne	.L102
.L104:
	movzbl	84(%rbx), %edx
	movq	8(%rbx), %rsi
	leaq	.LC6(%rip), %r8
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L137:
	movzbl	84(%rbx), %edx
	movq	8(%rbx), %rsi
	leaq	.LC4(%rip), %r8
.L135:
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc@PLT
	movl	$4, 80(%rbx)
	movq	%rax, 40(%rbx)
	movl	$4, %eax
.L87:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L139
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L140
.L90:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L102:
	movzbl	%r12b, %esi
	movl	%r14d, %edi
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	testb	%al, %al
	je	.L104
	.p2align 4,,10
	.p2align 3
.L101:
	movq	24(%rbx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	movl	88(%rbx), %edx
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%eax, %edx
	jb	.L141
.L105:
	movq	24(%rbx), %rsi
	movq	(%rbx), %rdi
	movzbl	%r12b, %edx
	call	_ZN2v88internal3Map14AsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movl	$1, 80(%rbx)
	movq	%rax, 24(%rbx)
	movl	$1, %eax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L136:
	movl	$4, 80(%rbx)
	movq	(%rbx), %r13
	movq	(%rax), %rax
.L133:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L142
.L92:
	movq	41112(%r13), %rdi
	movq	55(%rax), %r14
	testq	%rdi, %rdi
	je	.L94
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L95:
	movq	%rsi, 40(%rbx)
	movq	(%rbx), %rdi
	movzbl	%r12b, %edx
	call	_ZN2v88internal3Map14AsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	%rax, 40(%rbx)
	movl	80(%rbx), %eax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L94:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L143
.L96:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L142:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L92
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L141:
	movq	16(%rbx), %rcx
	leal	3(%rdx,%rdx,2), %eax
	sall	$3, %eax
	movq	(%rcx), %rcx
	cltq
	movq	7(%rax,%rcx), %r8
	movq	(%rbx), %rdi
	sarq	$32, %r8
	movl	%r8d, %eax
	andl	$1, %eax
	cmpl	%eax, 92(%rbx)
	jne	.L107
	movl	%r8d, %eax
	shrl	$3, %eax
	andl	$7, %eax
	cmpl	%eax, 96(%rbx)
	jne	.L107
	testb	$2, %r8b
	jne	.L144
	shrl	$6, %r8d
	movzbl	108(%rbx), %eax
	andl	$7, %r8d
	cmpb	%al, %r8b
	setg	%cl
	cmpl	$3, %r8d
	jne	.L111
	testb	%al, %al
	sete	%cl
.L111:
	testb	%cl, %cl
	jne	.L112
	cmpb	%al, %r8b
	je	.L112
	movzbl	84(%rbx), %edx
	movq	8(%rbx), %rsi
	leaq	.LC9(%rip), %r8
	.p2align 4,,10
	.p2align 3
.L134:
	xorl	%ecx, %ecx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc@PLT
	movl	$4, 80(%rbx)
	movq	%rax, 40(%rbx)
	movl	$4, %eax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L138:
	movzbl	84(%rbx), %edx
	movq	8(%rbx), %rsi
	leaq	.LC5(%rip), %r8
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L107:
	movzbl	84(%rbx), %edx
	movq	8(%rbx), %rsi
	leaq	.LC7(%rip), %r8
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L143:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L112:
	movl	100(%rbx), %ecx
	movq	8(%rbx), %rsi
	movq	112(%rbx), %r9
	call	_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE@PLT
	jmp	.L105
.L144:
	movzbl	84(%rbx), %edx
	movq	8(%rbx), %rsi
	leaq	.LC8(%rip), %r8
	jmp	.L134
.L139:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17805:
	.size	_ZN2v88internal10MapUpdater11FindRootMapEv, .-_ZN2v88internal10MapUpdater11FindRootMapEv
	.section	.rodata._ZN2v88internal10MapUpdater13FindTargetMapEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	"Normalize_Incompatible"
	.section	.text._ZN2v88internal10MapUpdater13FindTargetMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdater13FindTargetMapEv
	.type	_ZN2v88internal10MapUpdater13FindTargetMapEv, @function
_ZN2v88internal10MapUpdater13FindTargetMapEv:
.LFB17806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	24(%rdi), %r11
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, 32(%rdi)
	movq	(%r11), %rax
	movl	15(%rax), %r10d
	shrl	$10, %r10d
	andl	$1023, %r10d
	cmpl	48(%rdi), %r10d
	jge	.L147
	leaq	-192(%rbp), %rax
	leal	3(%r10,%r10,2), %r13d
	movl	%r10d, %r15d
	movq	%rax, -200(%rbp)
	sall	$3, %r13d
	movslq	%r13d, %r13
.L187:
	cmpl	%r15d, 88(%rbx)
	jne	.L148
	movl	56(%rbx), %eax
	subl	$4, %eax
	cmpl	$1, %eax
	jbe	.L149
	movl	96(%rbx), %eax
.L150:
	movl	104(%rbx), %edi
	leal	(%rdi,%rdi), %r12d
	movl	100(%rbx), %edi
	orl	92(%rbx), %r12d
	leal	0(,%rdi,4), %ecx
	orl	%ecx, %r12d
	movsbl	108(%rbx), %ecx
	sall	$6, %ecx
	andl	$16320, %ecx
	orl	%ecx, %r12d
	leal	0(,%rax,8), %ecx
	orl	%ecx, %r12d
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L148:
	movq	16(%rbx), %rdx
	leal	3(%r15,%r15,2), %eax
	sall	$3, %eax
	movq	(%rdx), %rdx
	cltq
	movq	7(%rax,%rdx), %r12
	shrq	$32, %r12
.L151:
	movq	32(%rbx), %rax
	movq	(%rbx), %rdx
	movq	%rax, -184(%rbp)
	movq	%rdx, -192(%rbp)
	movq	(%rax), %rax
	movq	$0, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -168(%rbp)
	testb	$1, %al
	je	.L152
	cmpl	$3, %eax
	je	.L152
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L252
	cmpq	$1, %rdx
	je	.L253
.L156:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L152:
	movl	$1, -160(%rbp)
.L154:
	leal	1(%r15), %eax
	movl	%r12d, %r14d
	movl	%r12d, %ecx
	movl	%eax, -208(%rbp)
	movq	16(%rbx), %rax
	andl	$1, %r14d
	shrl	$3, %ecx
	andl	$7, %ecx
	movl	%r14d, %edx
	movq	(%rax), %rax
	movq	-1(%r13,%rax), %rsi
	movq	-200(%rbp), %rdi
	call	_ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L160
	movq	(%rbx), %rdx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L161
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r11
.L162:
	movq	(%rbx), %rdx
	movq	39(%rsi), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L164
	movq	%r11, -216(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-216(%rbp), %r11
	movq	(%rax), %rsi
	movq	7(%rsi,%r13), %rdx
	testl	%r14d, %r14d
	jne	.L254
.L167:
	movq	%rdx, %r14
	shrq	$33, %rdx
	movl	%r12d, %esi
	shrl	%esi
	andl	$1, %edx
	sarq	$32, %r14
	movl	%edx, %ecx
	andl	$1, %esi
	xorl	$1, %ecx
	orl	%esi, %ecx
	je	.L160
	movl	%r12d, %ecx
	shrl	$6, %r14d
	shrl	$6, %ecx
	andl	$7, %r14d
	andl	$7, %ecx
	cmpb	%r14b, %cl
	setl	%dil
	cmpl	$3, %r14d
	je	.L255
	cmpl	%r14d, %ecx
	je	.L228
.L258:
	testb	%dil, %dil
	jne	.L228
.L160:
	movq	32(%rbx), %r11
.L147:
	movq	(%r11), %rax
	movl	15(%rax), %r8d
	movl	48(%rbx), %eax
	shrl	$10, %r8d
	andl	$1023, %r8d
	movl	%r8d, %r15d
	cmpl	%r8d, %eax
	je	.L256
.L188:
	cmpl	%eax, %r15d
	jge	.L204
	leal	3(%r15,%r15,2), %r12d
	leaq	-96(%rbp), %rax
	sall	$3, %r12d
	movq	%rax, -200(%rbp)
	movslq	%r12d, %r12
	cmpl	88(%rbx), %r15d
	jne	.L205
.L260:
	movl	56(%rbx), %eax
	subl	$4, %eax
	cmpl	$1, %eax
	jbe	.L206
	movl	96(%rbx), %edx
.L207:
	movl	104(%rbx), %eax
	leal	(%rax,%rax), %ecx
	movl	100(%rbx), %eax
	orl	92(%rbx), %ecx
	sall	$2, %eax
	orl	%eax, %ecx
	movsbl	108(%rbx), %eax
	sall	$6, %eax
	andl	$16320, %eax
	orl	%eax, %ecx
	leal	0(,%rdx,8), %eax
	orl	%eax, %ecx
.L208:
	movq	32(%rbx), %rax
	movq	(%rbx), %rdx
	movq	%rax, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movq	(%rax), %rax
	movq	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L209
	cmpl	$3, %eax
	je	.L209
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L257
	cmpq	$1, %rdx
	jne	.L156
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L213
	movl	$4, -64(%rbp)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L228:
	testl	%edx, %edx
	jne	.L174
	movl	88(%rbx), %eax
	testl	%esi, %esi
	jne	.L175
	movq	(%rbx), %rdx
	cmpl	%eax, %r15d
	jne	.L176
	movq	112(%rbx), %rax
	movq	(%rax), %rsi
.L177:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L178
	movq	%r11, -216(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-216(%rbp), %r11
	movq	%rax, %r9
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L255:
	testb	%cl, %cl
	sete	%dil
	cmpl	%r14d, %ecx
	jne	.L258
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L174:
	movq	(%rax), %rax
	movq	15(%r13,%rax), %rax
	cmpl	%r15d, 88(%rbx)
	jne	.L185
	movq	120(%rbx), %rdx
	movq	(%rdx), %rdx
.L186:
	cmpq	%rax, %rdx
	jne	.L160
	.p2align 4,,10
	.p2align 3
.L184:
	movq	%r11, 32(%rbx)
	movl	-208(%rbp), %r15d
	addq	$24, %r13
	cmpl	%r15d, 48(%rbx)
	jg	.L187
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L209:
	movl	$1, -64(%rbp)
.L211:
	movq	16(%rbx), %rax
	movl	%ecx, %r13d
	shrl	$3, %ecx
	leal	1(%r15), %r14d
	andl	$1, %r13d
	andl	$7, %ecx
	movq	(%rax), %rax
	movl	%r13d, %edx
	movq	-1(%r12,%rax), %rsi
	movq	-200(%rbp), %rdi
	call	_ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L204
	movq	(%rbx), %rcx
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L216
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L217:
	movq	(%rbx), %rcx
	movq	39(%rsi), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L219
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-208(%rbp), %rdx
	testl	%r13d, %r13d
	jne	.L259
.L222:
	movq	%rdx, 32(%rbx)
	addq	$24, %r12
	cmpl	%r14d, 48(%rbx)
	jle	.L204
	movl	%r14d, %r15d
	cmpl	88(%rbx), %r15d
	je	.L260
.L205:
	movq	16(%rbx), %rdx
	leal	3(%r15,%r15,2), %eax
	sall	$3, %eax
	movq	(%rdx), %rdx
	cltq
	movq	7(%rax,%rdx), %rcx
	shrq	$32, %rcx
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L164:
	movq	41088(%rdx), %rax
	cmpq	%rax, 41096(%rdx)
	je	.L261
.L166:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	movq	7(%rsi,%r13), %rdx
	testl	%r14d, %r14d
	je	.L167
.L254:
	movq	(%rax), %rcx
	movq	15(%r13,%rcx), %rcx
	cmpl	%r15d, 88(%rbx)
	jne	.L168
	movq	120(%rbx), %rsi
	movq	(%rsi), %rsi
.L169:
	cmpq	%rcx, %rsi
	je	.L167
.L250:
	movzbl	84(%rbx), %edx
	movq	(%rbx), %rdi
	leaq	.LC10(%rip), %r8
	xorl	%ecx, %ecx
	movq	8(%rbx), %rsi
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc@PLT
.L251:
	movq	%rax, 40(%rbx)
	movl	$4, 80(%rbx)
.L249:
	movl	$4, %eax
.L145:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L262
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movq	41088(%rdx), %r11
	cmpq	%r11, 41096(%rdx)
	je	.L263
.L163:
	leaq	8(%r11), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r11)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L219:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L264
.L221:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rcx)
	movq	%rsi, (%rax)
	testl	%r13d, %r13d
	je	.L222
.L259:
	movq	(%rax), %rax
	movq	15(%r12,%rax), %rax
	cmpl	%r15d, 88(%rbx)
	jne	.L223
	movq	120(%rbx), %rcx
	movq	(%rcx), %rcx
.L224:
	cmpq	%rax, %rcx
	jne	.L250
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L216:
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L265
.L218:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L256:
	movq	8(%rbx), %rax
	movq	(%rax), %rdx
	movq	32(%rbx), %rax
	cmpq	%rdx, (%rax)
	je	.L189
	movq	(%rbx), %rsi
	movl	15(%rdx), %eax
	testl	$33554432, %eax
	je	.L266
.L190:
	movq	32(%rbx), %rax
.L189:
	cmpb	$0, 52(%rbx)
	jne	.L191
	movq	32(%rbx), %rax
	movl	$4, 80(%rbx)
	movq	%rax, 40(%rbx)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L168:
	movq	16(%rbx), %rsi
	movq	(%rsi), %rsi
	movq	15(%r13,%rsi), %rsi
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L149:
	movq	16(%rbx), %rdx
	leal	3(%r15,%r15,2), %eax
	sall	$3, %eax
	movq	(%rdx), %rdx
	cltq
	movq	7(%rax,%rdx), %rcx
	shrq	$35, %rcx
	movl	%ecx, %eax
	andl	$7, %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L175:
	cmpl	%eax, %r15d
	jne	.L182
	movq	120(%rbx), %rax
	movq	(%rax), %rax
.L183:
	movq	(%rbx), %rsi
	leaq	-96(%rbp), %rdi
	movl	%r14d, %edx
	movq	%rax, -96(%rbp)
	movq	%r11, -216(%rbp)
	call	_ZN2v88internal6Object11OptimalTypeEPNS0_7IsolateENS0_14RepresentationE@PLT
	movq	-216(%rbp), %r11
	movq	%rax, %r9
.L181:
	movl	%r12d, %ecx
	movq	(%rbx), %rdi
	movq	%r11, %rsi
	movl	%r14d, %r8d
	shrl	$2, %ecx
	movl	%r15d, %edx
	movq	%r11, -216(%rbp)
	andl	$1, %ecx
	call	_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE@PLT
	movq	-216(%rbp), %r11
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L185:
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdx
	movq	15(%r13,%rdx), %rdx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L223:
	movq	16(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	15(%r12,%rcx), %rcx
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L257:
	movl	$3, -64(%rbp)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L252:
	movl	$3, -160(%rbp)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L206:
	movq	16(%rbx), %rdx
	leal	3(%r15,%r15,2), %eax
	sall	$3, %eax
	movq	(%rdx), %rdx
	cltq
	movq	7(%rax,%rdx), %rax
	shrq	$35, %rax
	andl	$7, %eax
	movl	%eax, %edx
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L191:
	movq	(%rbx), %rdx
	movq	%rax, -136(%rbp)
	movq	%rdx, -144(%rbp)
	movq	(%rax), %rax
	movq	$0, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -120(%rbp)
	testb	$1, %al
	je	.L193
	cmpl	$3, %eax
	je	.L193
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L267
	cmpq	$1, %rdx
	jne	.L156
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L197
	movl	$4, -112(%rbp)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L193:
	movl	$1, -112(%rbp)
.L195:
	movq	64(%rbx), %rax
	leaq	-144(%rbp), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L268
	movl	48(%rbx), %eax
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L263:
	movq	%rdx, %rdi
	movq	%rdx, -216(%rbp)
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdx
	movq	%rax, %r11
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%rdx, %rdi
	movq	%rsi, -232(%rbp)
	movq	%r11, -224(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	movq	-224(%rbp), %r11
	movq	-216(%rbp), %rdx
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L182:
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	15(%r13,%rax), %rax
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L176:
	movq	16(%rbx), %rax
	movq	%rdx, -224(%rbp)
	movq	%r11, -216(%rbp)
	movq	(%rax), %rax
	movq	15(%r13,%rax), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	-224(%rbp), %rdx
	movq	-216(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L178:
	movq	41088(%rdx), %r9
	cmpq	41096(%rdx), %r9
	je	.L269
.L180:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r9)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L253:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L270
	movl	$4, -160(%rbp)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L204:
	movl	$2, 80(%rbx)
	movl	$2, %eax
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%rcx, %rdi
	movq	%rcx, -208(%rbp)
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	-208(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L264:
	movq	%rcx, %rdi
	movq	%rsi, -224(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%rcx, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdx
	movq	-208(%rbp), %rcx
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L270:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -160(%rbp)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L268:
	movq	(%rbx), %r12
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L200
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L251
.L269:
	movq	%rdx, %rdi
	movq	%rsi, -232(%rbp)
	movq	%r11, -224(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	movq	-224(%rbp), %r11
	movq	-216(%rbp), %rdx
	movq	%rax, %r9
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L266:
	movl	15(%rdx), %eax
	leaq	-96(%rbp), %rdi
	orl	$33554432, %eax
	movl	%eax, 15(%rdx)
	movq	55(%rdx), %rax
	movl	$1, %edx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L213:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L211
.L200:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L271
.L202:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L251
.L267:
	movl	$3, -112(%rbp)
	jmp	.L195
.L197:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -112(%rbp)
	jmp	.L195
.L271:
	movq	%r12, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L202
.L262:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17806:
	.size	_ZN2v88internal10MapUpdater13FindTargetMapEv, .-_ZN2v88internal10MapUpdater13FindTargetMapEv
	.section	.rodata._ZN2v88internal10MapUpdater20BuildDescriptorArrayEv.str1.1,"aMS",@progbits,1
.LC11:
	.string	"(location_) != nullptr"
.LC12:
	.string	"unimplemented code"
	.section	.rodata._ZN2v88internal10MapUpdater20BuildDescriptorArrayEv.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"is_transitionable_fast_elements_kind_ implies Map::IsMostGeneralFieldType(next_representation, *next_field_type)"
	.section	.text._ZN2v88internal10MapUpdater20BuildDescriptorArrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdater20BuildDescriptorArrayEv
	.type	_ZN2v88internal10MapUpdater20BuildDescriptorArrayEv, @function
_ZN2v88internal10MapUpdater20BuildDescriptorArrayEv:
.LFB17807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movzwl	11(%rax), %eax
	movw	%ax, -270(%rbp)
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	movl	15(%rax), %ecx
	movq	(%rdi), %rbx
	movq	39(%rax), %rsi
	movq	41112(%rbx), %rdi
	movl	%ecx, -352(%rbp)
	testq	%rdi, %rdi
	je	.L273
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -296(%rbp)
.L274:
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movswl	9(%rax), %eax
	movl	48(%r14), %esi
	movq	(%r14), %rdi
	cmpl	%esi, %eax
	cmovl	%esi, %eax
	xorl	%ecx, %ecx
	movl	%eax, %edx
	subl	%esi, %edx
	call	_ZN2v88internal15DescriptorArray8AllocateEPNS0_7IsolateEiiNS0_14AllocationTypeE@PLT
	movq	%rax, -264(%rbp)
	movq	24(%r14), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	movl	%eax, -312(%rbp)
	je	.L524
	subl	$1, %eax
	leaq	-224(%rbp), %r12
	movl	$0, -268(%rbp)
	movl	$31, %r15d
	leaq	(%rax,%rax,2), %rax
	leaq	55(,%rax,8), %rax
	movq	%rax, -280(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	%r12, %rax
	movq	%r15, %r12
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L303:
	movq	16(%r14), %rax
	leaq	-7(%r12), %r13
	movq	(%rax), %rax
	movq	(%rax,%r12), %rbx
	sarq	$32, %rbx
	movl	%ebx, %edx
	andl	$2, %edx
	cmpl	$1, %edx
	movq	(%r14), %rdx
	adcl	$0, -268(%rbp)
	movq	8(%r12,%rax), %rsi
	movq	$0, -88(%rbp)
	movq	41112(%rdx), %rdi
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L525
.L281:
	testq	%rdi, %rdi
	je	.L286
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L287:
	movq	%rax, -88(%rbp)
	movl	$1, -96(%rbp)
.L285:
	movq	16(%r14), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movq	-8(%r12,%rax), %r10
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L289
	movq	%r10, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L290:
	movq	-288(%rbp), %rdx
	movl	%ebx, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal10DescriptorC1ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_15PropertyDetailsE@PLT
	movq	-264(%rbp), %rax
	movl	-216(%rbp), %ecx
	movq	(%rax), %rdi
	movq	-224(%rbp), %rax
	movq	(%rax), %rdx
	movq	-208(%rbp), %rax
	testl	%ecx, %ecx
	jne	.L292
	testq	%rax, %rax
	je	.L295
	movq	(%rax), %rax
	orq	$2, %rax
.L294:
	leaq	-1(%rdi), %rcx
	movl	-200(%rbp), %ebx
	leaq	(%rcx,%r13), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L410
	movq	%rdx, %r13
	andq	$-262144, %r13
	movq	8(%r13), %r10
	testl	$262144, %r10d
	je	.L297
	movq	%rcx, -344(%rbp)
	movq	%rax, -336(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdi, -304(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %r10
	movq	-344(%rbp), %rcx
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rdx
	movq	-320(%rbp), %rsi
	movq	-304(%rbp), %rdi
.L297:
	andl	$24, %r10d
	je	.L410
	movq	%rdi, %r10
	andq	$-262144, %r10
	testb	$24, 8(%r10)
	jne	.L410
	movq	%rcx, -336(%rbp)
	movq	%rax, -328(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdi, -304(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-336(%rbp), %rcx
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rsi
	movq	-304(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L410:
	addl	%ebx, %ebx
	sarl	%ebx
	salq	$32, %rbx
	movq	%rbx, 8(%rsi)
	movq	%rax, 16(%rsi)
	testb	$1, %al
	je	.L409
	cmpl	$3, %eax
	je	.L409
	movq	%rax, %rdx
	andq	$-262144, %rax
	leaq	9(%rcx,%r12), %r13
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	je	.L300
	movq	%r13, %rsi
	movq	%rdx, -320(%rbp)
	movq	%rdi, -304(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-320(%rbp), %rdx
	movq	-304(%rbp), %rdi
.L300:
	testb	$24, %al
	je	.L409
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L526
	.p2align 4,,10
	.p2align 3
.L409:
	addq	$24, %r12
	cmpq	%r12, -280(%rbp)
	jne	.L303
.L302:
	movl	-352(%rbp), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	movl	%eax, -304(%rbp)
	cmpl	-312(%rbp), %eax
	jle	.L277
	movzwl	-270(%rbp), %ecx
	movl	$1179649, %eax
	movl	$1, %edx
	movl	-312(%rbp), %r13d
	subw	$1041, %cx
	shrq	%cl, %rax
	notq	%rax
	andl	$1, %eax
	cmpw	$20, %cx
	cmovbe	%eax, %edx
	leal	3(%r13,%r13,2), %eax
	sall	$3, %eax
	movb	%dl, -328(%rbp)
	movslq	%eax, %r15
	.p2align 4,,10
	.p2align 3
.L360:
	movq	16(%r14), %rax
	movq	(%r14), %rbx
	movq	(%rax), %rax
	movq	-1(%r15,%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L305
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L306:
	cmpl	%r13d, 88(%r14)
	jne	.L308
	movl	56(%r14), %eax
	subl	$4, %eax
	cmpl	$1, %eax
	jbe	.L309
	movl	96(%r14), %edx
.L310:
	movl	104(%r14), %eax
	sall	$3, %edx
	leal	(%rax,%rax), %ebx
	movl	100(%r14), %eax
	orl	92(%r14), %ebx
	sall	$2, %eax
	orl	%eax, %ebx
	movsbl	108(%r14), %eax
	sall	$6, %eax
	andl	$16320, %eax
	orl	%eax, %ebx
	orl	%edx, %ebx
.L311:
	movq	-296(%rbp), %rax
	movl	%ebx, %esi
	movl	$0, %edi
	shrl	$3, %esi
	movq	(%rax), %rdx
	andl	$7, %esi
	movq	7(%r15,%rdx), %rax
	movl	%esi, -280(%rbp)
	movl	88(%r14), %esi
	movq	%rax, %rcx
	shrq	$34, %rax
	andl	$1, %eax
	sarq	$32, %rcx
	testb	$4, %bl
	cmove	%edi, %eax
	movl	%eax, -288(%rbp)
	movl	%ebx, %eax
	shrl	%eax
	andl	$1, %eax
	movl	%eax, %r8d
	je	.L313
	testb	$2, %cl
	je	.L313
	cmpl	%r13d, %esi
	jne	.L314
	movq	120(%r14), %rax
	movq	(%rax), %rdi
.L315:
	movq	15(%r15,%rdx), %rax
	cmpq	%rax, %rdi
	sete	%dil
.L313:
	movl	%ebx, %r12d
	movl	%ecx, %eax
	shrl	$6, %r12d
	shrl	$6, %eax
	andl	$7, %eax
	andl	$7, %r12d
	cmpb	%r12b, %al
	movl	%eax, %r9d
	setl	%dl
	cmpl	$3, %r12d
	jne	.L317
	testb	%al, %al
	sete	%dl
.L317:
	cmpl	%r12d, %eax
	je	.L415
	testb	%dl, %dl
	jne	.L415
	cmpb	%r12b, %al
	setg	%dl
	cmpb	$3, %al
	jne	.L320
	testb	%r12b, %r12b
	sete	%dl
.L320:
	testb	%dl, %dl
	movl	$4, %eax
	cmove	%eax, %r9d
	testb	%dil, %dil
	je	.L527
.L321:
	movq	(%r14), %rbx
	cmpl	%r13d, %esi
	jne	.L347
	movq	120(%r14), %rax
	movq	41112(%rbx), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L349
.L536:
	movq	%r10, -288(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-288(%rbp), %r10
	movq	%rax, %rdx
.L350:
	movl	-280(%rbp), %ecx
	leaq	-96(%rbp), %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-264(%rbp), %rax
	movl	-88(%rbp), %r9d
	movq	(%rax), %rdi
	movq	-96(%rbp), %rax
	movq	(%rax), %rdx
	movq	-80(%rbp), %rax
	testl	%r9d, %r9d
	jne	.L352
	testq	%rax, %rax
	je	.L295
	movq	(%rax), %r12
	orq	$2, %r12
.L353:
	leaq	-1(%rdi), %rax
	movl	-72(%rbp), %ebx
	leaq	(%rax,%r15), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L408
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %r8
	movq	%rcx, -280(%rbp)
	testl	$262144, %r8d
	je	.L355
	movq	%rax, -336(%rbp)
	movq	%rdx, -320(%rbp)
	movq	%rsi, -312(%rbp)
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-280(%rbp), %rcx
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rdx
	movq	-312(%rbp), %rsi
	movq	8(%rcx), %r8
	movq	-288(%rbp), %rdi
.L355:
	andl	$24, %r8d
	je	.L408
	movq	%rdi, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	je	.L528
	.p2align 4,,10
	.p2align 3
.L408:
	addl	%ebx, %ebx
	sarl	%ebx
	salq	$32, %rbx
	movq	%rbx, 8(%rsi)
	movq	%r12, 16(%rsi)
	testb	$1, %r12b
	je	.L346
	cmpl	$3, %r12d
	je	.L346
	leal	0(%r13,%r13,2), %edx
	leal	40(,%rdx,8), %esi
	movslq	%esi, %rsi
	leaq	(%rsi,%rax), %rbx
.L513:
	movq	%r12, %rdx
	andq	$-262144, %r12
	movq	8(%r12), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	je	.L358
	movq	%rbx, %rsi
	movq	%rdx, -288(%rbp)
	movq	%rdi, -280(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-288(%rbp), %rdx
	movq	-280(%rbp), %rdi
.L358:
	testb	$24, %al
	je	.L346
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L529
	.p2align 4,,10
	.p2align 3
.L346:
	addl	$1, %r13d
	addq	$24, %r15
	cmpl	%r13d, -304(%rbp)
	jne	.L360
.L277:
	movl	-304(%rbp), %eax
	cmpl	48(%r14), %eax
	jge	.L530
	movl	%eax, %r15d
	leal	3(%rax,%rax,2), %eax
	sall	$3, %eax
	movl	%r15d, -280(%rbp)
	movq	%r14, %r15
	cltq
	movq	%rax, -296(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, -304(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -312(%rbp)
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L532:
	movl	56(%r15), %ecx
	leal	-4(%rcx), %edx
	cmpl	$1, %edx
	jbe	.L362
	movl	96(%r15), %edx
.L363:
	movl	104(%r15), %ecx
	movsbl	108(%r15), %r8d
	leal	(%rcx,%rcx), %ebx
	movl	100(%r15), %ecx
	orl	92(%r15), %ebx
	sall	$6, %r8d
	andl	$16320, %r8d
	sall	$2, %ecx
	orl	%ecx, %ebx
	orl	%r8d, %ebx
	leal	0(,%rdx,8), %r8d
	orl	%r8d, %ebx
.L364:
	movq	(%r15), %r13
	movq	-296(%rbp), %rcx
	movq	-1(%rcx,%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L365
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L366:
	movl	%ebx, %r13d
	movq	-304(%rbp), %rdi
	movl	%ebx, %r14d
	shrl	$3, %r13d
	andl	$1, %r14d
	andl	$7, %r13d
	call	_ZN2v88internal10DescriptorC1Ev@PLT
	testb	$2, %bl
	je	.L531
	movq	(%r15), %rbx
	movl	-280(%rbp), %eax
	cmpl	%eax, 88(%r15)
	jne	.L384
	movq	120(%r15), %rax
	movq	41112(%rbx), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L386
.L546:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L387:
	movq	-312(%rbp), %rdi
	movl	%r13d, %ecx
	movq	%r12, %rsi
	movq	%rdi, -288(%rbp)
	testl	%r14d, %r14d
	jne	.L389
	call	_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-80(%rbp), %rax
	movdqa	-96(%rbp), %xmm1
	movq	%rax, -144(%rbp)
	movl	-72(%rbp), %eax
	movaps	%xmm1, -160(%rbp)
	movl	%eax, -136(%rbp)
.L390:
	movq	-264(%rbp), %rax
	movl	-152(%rbp), %edx
	movq	(%rax), %rdi
	movq	-160(%rbp), %rax
	movq	(%rax), %r14
	movq	-144(%rbp), %rax
	testl	%edx, %edx
	jne	.L391
	testq	%rax, %rax
	je	.L295
	movq	(%rax), %r12
	orq	$2, %r12
.L392:
	movl	-136(%rbp), %ebx
.L523:
	movq	-296(%rbp), %rcx
	leaq	-1(%rdi), %rax
	leaq	(%rcx,%rax), %r13
	movq	%r14, 0(%r13)
	testb	$1, %r14b
	je	.L404
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -320(%rbp)
	testl	$262144, %edx
	je	.L394
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, -336(%rbp)
	movq	%rdi, -328(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-320(%rbp), %rcx
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rdi
	movq	8(%rcx), %rdx
.L394:
	andl	$24, %edx
	je	.L404
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L404
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, -328(%rbp)
	movq	%rdi, -320(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L404:
	addl	%ebx, %ebx
	sarl	%ebx
	salq	$32, %rbx
	movq	%rbx, 8(%r13)
	movq	%r12, 16(%r13)
	testb	$1, %r12b
	je	.L383
	cmpl	$3, %r12d
	je	.L383
	movl	-280(%rbp), %ecx
	movq	%r12, %r14
	andq	$-262144, %r12
	andq	$-3, %r14
	leal	(%rcx,%rcx,2), %edx
	leal	40(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rax), %r13
	movq	8(%r12), %rax
	testl	$262144, %eax
	je	.L397
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rdi, -320(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-320(%rbp), %rdi
.L397:
	testb	$24, %al
	je	.L383
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L383
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L383:
	addl	$1, -280(%rbp)
	movl	-280(%rbp), %eax
	addq	$24, -296(%rbp)
	cmpl	%eax, 48(%r15)
	jle	.L399
.L400:
	movq	16(%r15), %rax
	movl	-280(%rbp), %ecx
	movq	(%rax), %rax
	cmpl	%ecx, 88(%r15)
	je	.L532
	movq	-296(%rbp), %rcx
	movq	7(%rcx,%rax), %rbx
	shrq	$32, %rbx
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L415:
	movl	%r12d, %r9d
	testb	%dil, %dil
	jne	.L321
.L527:
	testl	%r8d, %r8d
	jne	.L322
	movq	(%r14), %rdx
	cmpl	%r13d, %esi
	jne	.L323
	movq	112(%r14), %rax
	movq	41112(%rdx), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L325
.L539:
	movq	%rcx, -336(%rbp)
	movb	%r9b, -320(%rbp)
	movq	%r10, -312(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-312(%rbp), %r10
	movzbl	-320(%rbp), %r9d
	movq	-336(%rbp), %rcx
	movq	%rax, %r11
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L292:
	testq	%rax, %rax
	je	.L295
	movq	(%rax), %rax
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L289:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L533
.L291:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r10, (%rsi)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L308:
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	7(%r15,%rax), %rbx
	shrq	$32, %rbx
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L305:
	movq	41088(%rbx), %r10
	cmpq	41096(%rbx), %r10
	je	.L534
.L307:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r10)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L286:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L535
.L288:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L525:
	cmpl	$3, %esi
	je	.L281
	andq	$-3, %rsi
	testq	%rdi, %rdi
	je	.L282
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L283:
	movq	%rax, -88(%rbp)
	movl	$0, -96(%rbp)
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L352:
	testq	%rax, %rax
	je	.L295
	movq	(%rax), %r12
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L347:
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	15(%r15,%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L536
.L349:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L537
.L351:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L295:
	leaq	.LC11(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L322:
	cmpl	%r13d, %esi
	jne	.L329
	movq	120(%r14), %rax
	movq	(%rax), %rax
.L330:
	movq	(%r14), %rsi
	movl	%r9d, %edx
	leaq	-96(%rbp), %rdi
	movq	%rax, -96(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r10, -320(%rbp)
	movb	%r9b, -312(%rbp)
	call	_ZN2v88internal6Object11OptimalTypeEPNS0_7IsolateENS0_14RepresentationE@PLT
	movq	-336(%rbp), %rcx
	movq	-320(%rbp), %r10
	movzbl	-312(%rbp), %r9d
	movq	%rax, %r11
.L328:
	movq	-296(%rbp), %rax
	andl	$2, %ecx
	movq	(%rax), %rax
	leaq	15(%r15,%rax), %rax
	jne	.L331
	movq	(%r14), %rdx
	movb	%r9b, -344(%rbp)
	movq	%r10, -336(%rbp)
	movq	%r11, -320(%rbp)
	movq	%rdx, -312(%rbp)
	movq	(%rax), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	-312(%rbp), %rdx
	movq	-320(%rbp), %r11
	movq	-336(%rbp), %r10
	movzbl	-344(%rbp), %r9d
	movq	%rax, %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L332
	movb	%r9b, -336(%rbp)
	movq	%r10, -320(%rbp)
	movq	%r11, -312(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-312(%rbp), %r11
	movq	-320(%rbp), %r10
	movzbl	-336(%rbp), %r9d
	movq	%rax, %rcx
.L335:
	movq	(%r14), %r8
	movl	%r9d, %edx
	movq	%r11, %rsi
	movl	%r12d, %edi
	movq	%r10, -320(%rbp)
	movb	%r9b, -312(%rbp)
	call	_ZN2v88internal3Map19GeneralizeFieldTypeENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEES2_S5_PNS0_7IsolateE@PLT
	cmpb	$0, -328(%rbp)
	movq	(%r14), %rdi
	movzbl	-312(%rbp), %r9d
	movq	-320(%rbp), %r10
	movq	%rax, %rsi
	jne	.L336
	movq	%r10, -312(%rbp)
	call	_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE@PLT
	movq	(%r14), %rdi
	movq	-312(%rbp), %r10
	movl	$4, %r9d
	movq	%rax, %rsi
.L336:
	movb	%r9b, -320(%rbp)
	movq	%r10, -312(%rbp)
	call	_ZN2v88internal3Map13WrapFieldTypeEPNS0_7IsolateENS0_6HandleINS0_9FieldTypeEEE@PLT
	leaq	-192(%rbp), %rdi
	movl	%eax, -256(%rbp)
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal10DescriptorC1Ev@PLT
	andl	$1, %ebx
	jne	.L337
	subq	$8, %rsp
	leaq	-256(%rbp), %rax
	movl	-268(%rbp), %ebx
	movq	-312(%rbp), %r10
	pushq	%rax
	movl	-280(%rbp), %ecx
	leaq	-96(%rbp), %rdi
	movzbl	-320(%rbp), %r9d
	movl	-288(%rbp), %r8d
	movl	%ebx, %edx
	movq	%r10, %rsi
	addl	$1, %ebx
	call	_ZN2v88internal10Descriptor9DataFieldENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE@PLT
	movl	-72(%rbp), %edx
	movdqa	-96(%rbp), %xmm0
	movl	%ebx, -268(%rbp)
	movq	-80(%rbp), %rax
	movq	-264(%rbp), %rcx
	movaps	%xmm0, -192(%rbp)
	movl	-184(%rbp), %ebx
	popq	%r10
	movl	%edx, -168(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rax, -176(%rbp)
	popq	%r11
	movq	(%rcx), %rdi
	movq	(%rdx), %rdx
	testl	%ebx, %ebx
	jne	.L338
	testq	%rax, %rax
	je	.L295
	movq	(%rax), %r12
	orq	$2, %r12
.L339:
	leaq	-1(%rdi), %rax
	movl	-168(%rbp), %ebx
	leaq	(%rax,%r15), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L406
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %r8
	movq	%rcx, -280(%rbp)
	testl	$262144, %r8d
	je	.L341
	movq	%rax, -336(%rbp)
	movq	%rdx, -320(%rbp)
	movq	%rsi, -312(%rbp)
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-280(%rbp), %rcx
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rdx
	movq	-312(%rbp), %rsi
	movq	8(%rcx), %r8
	movq	-288(%rbp), %rdi
.L341:
	andl	$24, %r8d
	je	.L406
	movq	%rdi, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	je	.L538
	.p2align 4,,10
	.p2align 3
.L406:
	addl	%ebx, %ebx
	sarl	%ebx
	salq	$32, %rbx
	movq	%rbx, 8(%rsi)
	movq	%r12, 16(%rsi)
	testb	$1, %r12b
	je	.L346
	cmpl	$3, %r12d
	je	.L346
	leal	0(%r13,%r13,2), %edx
	leal	40(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rax), %rbx
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L314:
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	15(%r15,%rax), %rdi
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L526:
	movq	%r13, %rsi
	addq	$24, %r12
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	cmpq	%r12, -280(%rbp)
	jne	.L303
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%rbx, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L528:
	movq	%rax, -312(%rbp)
	movq	%rsi, -288(%rbp)
	movq	%rdi, -280(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-312(%rbp), %rax
	movq	-288(%rbp), %rsi
	movq	-280(%rbp), %rdi
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L323:
	movq	16(%r14), %rax
	movq	%rcx, -344(%rbp)
	movq	%rdx, -336(%rbp)
	movq	(%rax), %rax
	movb	%r9b, -320(%rbp)
	movq	%r10, -312(%rbp)
	movq	15(%r15,%rax), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	-336(%rbp), %rdx
	movq	-344(%rbp), %rcx
	movzbl	-320(%rbp), %r9d
	movq	-312(%rbp), %r10
	movq	%rax, %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L539
.L325:
	movq	41088(%rdx), %r11
	cmpq	41096(%rdx), %r11
	je	.L540
.L327:
	leaq	8(%r11), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r11)
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L329:
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	15(%r15,%rax), %rax
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L338:
	testq	%rax, %rax
	je	.L295
	movq	(%rax), %r12
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L331:
	movq	%r10, -336(%rbp)
	movl	%r9d, %edx
	leaq	-96(%rbp), %rdi
	movq	%r11, -320(%rbp)
	movq	(%rax), %rax
	movq	(%r14), %rsi
	movb	%r9b, -312(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6Object11OptimalTypeEPNS0_7IsolateENS0_14RepresentationE@PLT
	movq	-336(%rbp), %r10
	movq	-320(%rbp), %r11
	movzbl	-312(%rbp), %r9d
	movq	%rax, %rcx
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L282:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L541
.L284:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L273:
	movq	41088(%rbx), %rax
	movq	%rax, -296(%rbp)
	cmpq	%rax, 41096(%rbx)
	je	.L542
.L275:
	movq	-296(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L309:
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	7(%r15,%rax), %rdx
	shrq	$35, %rdx
	andl	$7, %edx
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L533:
	movq	%rdx, %rdi
	movq	%r10, -320(%rbp)
	movq	%rdx, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %r10
	movq	-304(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L534:
	movq	%rbx, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L535:
	movq	%rdx, %rdi
	movq	%rsi, -320(%rbp)
	movq	%rdx, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	movq	-304(%rbp), %rdx
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L332:
	movq	41088(%rdx), %rcx
	cmpq	41096(%rdx), %rcx
	je	.L543
.L334:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L335
.L538:
	movq	%rax, -312(%rbp)
	movq	%rsi, -288(%rbp)
	movq	%rdi, -280(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-312(%rbp), %rax
	movq	-288(%rbp), %rsi
	movq	-280(%rbp), %rdi
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L365:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L544
.L367:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L391:
	testq	%rax, %rax
	je	.L295
	movq	(%rax), %r12
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L389:
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-80(%rbp), %rax
	movdqa	-96(%rbp), %xmm2
	movq	%rax, -144(%rbp)
	movl	-72(%rbp), %eax
	movaps	%xmm2, -160(%rbp)
	movl	%eax, -136(%rbp)
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L531:
	movl	%ebx, %eax
	movq	(%r15), %rdx
	shrl	$6, %eax
	andl	$7, %eax
	movl	%eax, -320(%rbp)
	movl	-280(%rbp), %eax
	cmpl	%eax, 88(%r15)
	jne	.L369
	movq	112(%r15), %rax
	movq	41112(%rdx), %rdi
	movq	(%rax), %r8
	testq	%rdi, %rdi
	je	.L371
.L548:
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L372:
	cmpb	$0, 85(%r15)
	jne	.L545
.L374:
	movq	(%r15), %rdi
	call	_ZN2v88internal3Map13WrapFieldTypeEPNS0_7IsolateENS0_6HandleINS0_9FieldTypeEEE@PLT
	leaq	-128(%rbp), %rdi
	movl	%eax, -240(%rbp)
	movq	%rdx, -232(%rbp)
	call	_ZN2v88internal10DescriptorC1Ev@PLT
	testl	%r14d, %r14d
	jne	.L337
	subq	$8, %rsp
	shrl	$2, %ebx
	movl	%r13d, %ecx
	movq	%r12, %rsi
	leaq	-240(%rbp), %rax
	movl	%ebx, %r8d
	movl	-268(%rbp), %ebx
	movq	-312(%rbp), %rdi
	pushq	%rax
	movzbl	-320(%rbp), %r9d
	andl	$1, %r8d
	movl	%ebx, %edx
	movq	%rdi, -288(%rbp)
	addl	$1, %ebx
	call	_ZN2v88internal10Descriptor9DataFieldENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE@PLT
	movl	-72(%rbp), %edx
	movdqa	-96(%rbp), %xmm3
	movl	%ebx, -268(%rbp)
	movq	-264(%rbp), %rcx
	movq	-80(%rbp), %rax
	movaps	%xmm3, -128(%rbp)
	movl	-120(%rbp), %r8d
	movl	%edx, -104(%rbp)
	movq	-128(%rbp), %rdx
	movq	%rax, -112(%rbp)
	movq	(%rcx), %rdi
	movq	(%rdx), %r14
	popq	%rcx
	popq	%rsi
	testl	%r8d, %r8d
	jne	.L375
	testq	%rax, %rax
	je	.L295
	movq	(%rax), %r12
	orq	$2, %r12
.L376:
	movl	-104(%rbp), %ebx
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L384:
	movq	16(%r15), %rax
	movq	-296(%rbp), %rcx
	movq	(%rax), %rax
	movq	15(%rcx,%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L546
.L386:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L547
.L388:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L387
.L375:
	testq	%rax, %rax
	je	.L295
	movq	(%rax), %r12
	jmp	.L376
.L369:
	movq	16(%r15), %rax
	movq	-296(%rbp), %rcx
	movq	%rdx, -288(%rbp)
	movq	(%rax), %rax
	movq	15(%rcx,%rax), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	-288(%rbp), %rdx
	movq	%rax, %r8
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L548
.L371:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L549
.L373:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r8, (%rsi)
	jmp	.L372
.L537:
	movq	%rbx, %rdi
	movq	%r10, -312(%rbp)
	movq	%rsi, -288(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %r10
	movq	-288(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L351
.L362:
	movq	-296(%rbp), %rcx
	movq	7(%rcx,%rax), %rdx
	shrq	$35, %rdx
	andl	$7, %edx
	jmp	.L363
.L544:
	movq	%r13, %rdi
	movq	%rsi, -288(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-288(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L367
.L524:
	movl	$0, -268(%rbp)
	jmp	.L302
.L541:
	movq	%rdx, %rdi
	movq	%rsi, -320(%rbp)
	movq	%rdx, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	movq	-304(%rbp), %rdx
	jmp	.L284
.L542:
	movq	%rbx, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, -296(%rbp)
	jmp	.L275
.L547:
	movq	%rbx, %rdi
	movq	%rsi, -288(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-288(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L388
.L530:
	leaq	-96(%rbp), %rax
	movq	%rax, -288(%rbp)
.L399:
	movq	-264(%rbp), %rax
	movq	-288(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal15DescriptorArray4SortEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L550
	movq	-264(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L540:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%rcx, -352(%rbp)
	movq	%rsi, -344(%rbp)
	movb	%r9b, -336(%rbp)
	movq	%r10, -320(%rbp)
	movq	%rdx, -312(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-352(%rbp), %rcx
	movq	-344(%rbp), %rsi
	movzbl	-336(%rbp), %r9d
	movq	-320(%rbp), %r10
	movq	%rax, %r11
	movq	-312(%rbp), %rdx
	jmp	.L327
.L543:
	movq	%rdx, %rdi
	movb	%r9b, -344(%rbp)
	movq	%r10, -336(%rbp)
	movq	%r11, -320(%rbp)
	movq	%rdx, -312(%rbp)
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-352(%rbp), %rsi
	movzbl	-344(%rbp), %r9d
	movq	-336(%rbp), %r10
	movq	-320(%rbp), %r11
	movq	%rax, %rcx
	movq	-312(%rbp), %rdx
	jmp	.L334
.L545:
	cmpl	$3, -320(%rbp)
	jne	.L374
	movq	(%rsi), %rax
	movq	%rsi, -328(%rbp)
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal9FieldType3AnyEv@PLT
	cmpq	%rax, -288(%rbp)
	movq	-328(%rbp), %rsi
	je	.L374
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L549:
	movq	%rdx, %rdi
	movq	%r8, -328(%rbp)
	movq	%rdx, -288(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-328(%rbp), %r8
	movq	-288(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L373
.L337:
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L550:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17807:
	.size	_ZN2v88internal10MapUpdater20BuildDescriptorArrayEv, .-_ZN2v88internal10MapUpdater20BuildDescriptorArrayEv
	.section	.text._ZN2v88internal10MapUpdater12FindSplitMapENS0_6HandleINS0_15DescriptorArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdater12FindSplitMapENS0_6HandleINS0_15DescriptorArrayEEE
	.type	_ZN2v88internal10MapUpdater12FindSplitMapENS0_6HandleINS0_15DescriptorArrayEEE, @function
_ZN2v88internal10MapUpdater12FindSplitMapENS0_6HandleINS0_15DescriptorArrayEEE:
.LFB17808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	(%rax), %r15
	movl	15(%r15), %r14d
	shrl	$10, %r14d
	andl	$1023, %r14d
	cmpl	48(%rdi), %r14d
	jge	.L552
	addl	$1, %r14d
	leaq	-96(%rbp), %rax
	movq	%rsi, %r13
	leal	(%r14,%r14,2), %r12d
	movq	%rax, -104(%rbp)
	sall	$3, %r12d
	movslq	%r12d, %r12
.L566:
	movq	0(%r13), %rax
	movq	-1(%r12,%rax), %rsi
	movq	7(%r12,%rax), %rbx
	movq	%r15, -80(%rbp)
	movq	(%r8), %rax
	movq	$0, -72(%rbp)
	movq	$0, -88(%rbp)
	sarq	$32, %rbx
	movq	%rax, -96(%rbp)
	movq	71(%r15), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L553
	cmpl	$3, %eax
	je	.L553
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L580
	cmpq	$1, %rdx
	je	.L581
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L553:
	movl	$1, -64(%rbp)
.L555:
	movl	%ebx, %ecx
	movq	-104(%rbp), %rdi
	movl	%ebx, %edx
	movq	%r8, -112(%rbp)
	shrl	$3, %ecx
	andl	$1, %edx
	andl	$7, %ecx
	call	_ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE@PLT
	movq	-112(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L552
	movq	39(%rax), %rax
	movl	%ebx, %r9d
	shrl	$2, %r9d
	leaq	-1(%r12,%rax), %rcx
	movl	%r9d, %edi
	movq	8(%rcx), %rax
	andl	$1, %edi
	movq	%rax, %rsi
	shrq	$34, %rax
	andl	$1, %eax
	sarq	$32, %rsi
	cmpl	%edi, %eax
	jne	.L552
	movl	%ebx, %r10d
	movl	%esi, %eax
	shrl	%r10d
	shrl	%eax
	movl	%r10d, %edi
	andl	$1, %eax
	andl	$1, %edi
	cmpl	%edi, %eax
	jne	.L552
	shrl	$6, %esi
	shrl	$6, %ebx
	andl	$7, %esi
	andl	$7, %ebx
	cmpb	%bl, %sil
	jne	.L552
	testl	%eax, %eax
	jne	.L562
	movq	%r8, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	16(%rcx), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	%rax, %rbx
	movq	0(%r13), %rax
	movq	15(%r12,%rax), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	-104(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal9FieldType5NowIsES1_@PLT
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %r8
	testb	%al, %al
	je	.L552
.L564:
	addq	$24, %r12
	movq	%rdx, %r15
	cmpl	%r14d, 48(%r8)
	jle	.L552
	addl	$1, %r14d
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L562:
	movq	16(%rcx), %rcx
	movq	0(%r13), %rax
	movq	15(%r12,%rax), %rax
	cmpq	%rax, %rcx
	je	.L564
.L552:
	movq	(%r8), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L567
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L568:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L582
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L583
.L569:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r15, (%rax)
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L580:
	movl	$3, -64(%rbp)
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L581:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L584
	movl	$4, -64(%rbp)
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L583:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L584:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L555
.L582:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17808:
	.size	_ZN2v88internal10MapUpdater12FindSplitMapENS0_6HandleINS0_15DescriptorArrayEEE, .-_ZN2v88internal10MapUpdater12FindSplitMapENS0_6HandleINS0_15DescriptorArrayEEE
	.section	.rodata._ZN2v88internal10MapUpdater15ConstructNewMapEv.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"has_integrity_level_transition_"
	.align 8
.LC15:
	.string	"Normalize_CantHaveMoreTransitions"
	.section	.rodata._ZN2v88internal10MapUpdater15ConstructNewMapEv.str1.1,"aMS",@progbits,1
.LC16:
	.string	""
	.section	.text._ZN2v88internal10MapUpdater15ConstructNewMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdater15ConstructNewMapEv
	.type	_ZN2v88internal10MapUpdater15ConstructNewMapEv, @function
_ZN2v88internal10MapUpdater15ConstructNewMapEv:
.LFB17809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal10MapUpdater20BuildDescriptorArrayEv
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal10MapUpdater12FindSplitMapENS0_6HandleINS0_15DescriptorArrayEEE
	movq	%rax, %r14
	movq	(%rax), %rax
	movl	15(%rax), %r9d
	shrl	$10, %r9d
	andl	$1023, %r9d
	cmpl	%r9d, 48(%r15)
	jne	.L586
	cmpb	$0, 52(%r15)
	je	.L633
	movl	$3, 80(%r15)
	movl	$3, %eax
.L585:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L634
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore_state
	movl	%r9d, %r13d
	cmpl	88(%r15), %r9d
	jne	.L589
	movl	56(%r15), %eax
	subl	$4, %eax
	cmpl	$1, %eax
	jbe	.L590
	movl	96(%r15), %ecx
.L591:
	movl	104(%r15), %eax
	leal	(%rax,%rax), %edx
	movl	100(%r15), %eax
	orl	92(%r15), %edx
	sall	$2, %eax
	orl	%eax, %edx
	movsbl	108(%r15), %eax
	sall	$6, %eax
	andl	$16320, %eax
	orl	%eax, %edx
	leal	0(,%rcx,8), %eax
	orl	%eax, %edx
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L589:
	movq	16(%r15), %rdx
	leal	3(%r9,%r9,2), %eax
	sall	$3, %eax
	movq	(%rdx), %rdx
	cltq
	movq	7(%rax,%rdx), %rdx
	shrq	$32, %rdx
.L592:
	movq	(%r15), %rax
	movq	%r14, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	(%r14), %rax
	movq	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L593
	cmpl	$3, %eax
	je	.L593
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$3, %rcx
	je	.L635
	cmpq	$1, %rcx
	je	.L636
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L593:
	movl	$1, -64(%rbp)
.L595:
	movq	16(%r15), %rcx
	leal	3(%r13,%r13,2), %eax
	leaq	-96(%rbp), %r12
	sall	$3, %eax
	movq	%r12, %rdi
	movq	(%rcx), %rcx
	cltq
	movq	-1(%rax,%rcx), %rsi
	movl	%edx, %ecx
	andl	$1, %edx
	shrl	$3, %ecx
	andl	$7, %ecx
	call	_ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	jne	.L601
.L604:
	movq	%r12, %rdi
	call	_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv@PLT
	testb	%al, %al
	je	.L637
.L602:
	movq	8(%r15), %rax
	movq	(%r15), %rsi
	leaq	-104(%rbp), %r12
	movq	(%rax), %rax
	movl	15(%rax), %edx
	andl	$33554432, %edx
	je	.L638
.L605:
	cmpb	$0, _ZN2v88internal25FLAG_trace_generalizationE(%rip)
	je	.L606
	movl	88(%r15), %eax
	testl	%eax, %eax
	js	.L606
	movq	16(%r15), %rdx
	leal	3(%rax,%rax,2), %eax
	sall	$3, %eax
	movq	(%rdx), %rcx
	cltq
	movq	7(%rax,%rcx), %rdx
	movq	%rdx, %rsi
	shrq	$33, %rdx
	sarq	$32, %rsi
	movq	%rsi, -168(%rbp)
	movq	(%rbx), %rsi
	movq	7(%rax,%rsi), %rsi
	movq	$0, -128(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -144(%rbp)
	sarq	$32, %rsi
	andl	$1, %edx
	movl	%edx, -156(%rbp)
	movq	(%r15), %rdx
	movq	%rsi, -120(%rbp)
	movq	$0, -152(%rbp)
	je	.L639
	movq	15(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L612
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L613:
	movq	%rax, -144(%rbp)
.L611:
	movl	88(%r15), %eax
	movq	(%rbx), %rcx
	movq	(%r15), %rdx
	leal	3(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	leaq	15(%rax,%rcx), %rax
	testb	$2, -120(%rbp)
	jne	.L615
	movq	%rdx, -136(%rbp)
	movq	(%rax), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	-136(%rbp), %rdx
	movq	%rax, %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L616
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L617:
	movq	%rax, -136(%rbp)
.L619:
	movq	-120(%rbp), %rdi
	movq	8(%r15), %rax
	movq	-168(%rbp), %rsi
	movl	-156(%rbp), %r9d
	movl	%edi, %ecx
	movq	(%rax), %rax
	movl	%edi, %edx
	shrl	$6, %ecx
	shrl	$2, %edx
	movl	%ecx, %edi
	movl	%esi, %ecx
	movq	%rax, -104(%rbp)
	movl	%esi, %eax
	shrl	$6, %ecx
	shrl	$2, %eax
	andl	$1, %edx
	andl	$7, %edi
	movl	%ecx, %esi
	andl	$1, %eax
	xorl	%ecx, %ecx
	andl	$7, %esi
	testl	%r9d, %r9d
	je	.L623
	movl	104(%r15), %r8d
	xorl	%ecx, %ecx
	testl	%r8d, %r8d
	sete	%cl
.L623:
	pushq	-152(%rbp)
	movl	88(%r15), %r8d
	movl	%r13d, %r9d
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-128(%rbp)
	pushq	%rdx
	pushq	%rax
	movl	48(%r15), %eax
	pushq	%rdi
	movq	stdout(%rip), %rdx
	movq	%r12, %rdi
	pushq	%rsi
	movq	(%r15), %rsi
	pushq	%rcx
	leaq	.LC16(%rip), %rcx
	pushq	%rax
	call	_ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_@PLT
	addq	$80, %rsp
.L606:
	movl	48(%r15), %ecx
	movq	(%r15), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi@PLT
	movq	(%r15), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	movq	%rax, %r13
	call	_ZN2v88internal3Map21AddMissingTransitionsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE@PLT
	movq	(%r15), %rsi
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	movq	(%r14), %rax
	movq	%rax, -104(%rbp)
	movq	0(%r13), %rcx
	movq	(%rbx), %rdx
	call	_ZN2v88internal3Map18ReplaceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayENS0_16LayoutDescriptorE@PLT
	cmpb	$0, 52(%r15)
	movq	-120(%rbp), %r8
	jne	.L640
	movq	%r8, 40(%r15)
	movl	$4, %eax
.L625:
	movl	%eax, 80(%r15)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L601:
	movq	(%r15), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE@PLT
	cmpq	$0, -112(%rbp)
	jne	.L602
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L640:
	movq	%r8, 32(%r15)
	movl	$3, %eax
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L590:
	movq	16(%r15), %rdx
	leal	3(%r9,%r9,2), %eax
	sall	$3, %eax
	movq	(%rdx), %rdx
	cltq
	movq	7(%rax,%rdx), %rax
	shrq	$35, %rax
	andl	$7, %eax
	movl	%eax, %ecx
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	.LC14(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L638:
	movl	15(%rax), %edx
	movq	%r12, %rdi
	orl	$33554432, %edx
	movl	%edx, 15(%rax)
	movl	$1, %edx
	movq	55(%rax), %rax
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L635:
	movl	$3, -64(%rbp)
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L639:
	movq	%rdx, -128(%rbp)
	movq	15(%rax,%rcx), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	-128(%rbp), %rdx
	movq	%rax, %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L608
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L609:
	movq	%rax, -128(%rbp)
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L637:
	movq	8(%r15), %rsi
	movq	(%r15), %rdi
	leaq	.LC15(%rip), %r8
	xorl	%ecx, %ecx
	movzbl	84(%r15), %edx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc@PLT
	movl	$4, 80(%r15)
	movq	%rax, 40(%r15)
	movl	$4, %eax
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L615:
	movq	(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L620
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L621:
	movq	%rcx, -152(%rbp)
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L636:
	movq	-1(%rax), %rcx
	cmpw	$149, 11(%rcx)
	jne	.L641
	movl	$4, -64(%rbp)
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L612:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L642
.L614:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L616:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L643
.L618:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L608:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L644
.L610:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L620:
	movq	41088(%rdx), %rcx
	cmpq	41096(%rdx), %rcx
	je	.L645
.L622:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L641:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L595
.L645:
	movq	%rdx, %rdi
	movq	%rsi, -176(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	-152(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L622
.L642:
	movq	%rdx, %rdi
	movq	%rsi, -176(%rbp)
	movq	%rdx, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	-144(%rbp), %rdx
	jmp	.L614
.L643:
	movq	%rdx, %rdi
	movq	%rsi, -176(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	-136(%rbp), %rdx
	jmp	.L618
.L644:
	movq	%rdx, %rdi
	movq	%rsi, -176(%rbp)
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	-128(%rbp), %rdx
	jmp	.L610
.L634:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17809:
	.size	_ZN2v88internal10MapUpdater15ConstructNewMapEv, .-_ZN2v88internal10MapUpdater15ConstructNewMapEv
	.section	.rodata._ZN2v88internal10MapUpdater43ConstructNewMapWithIntegrityLevelTransitionEv.str1.1,"aMS",@progbits,1
.LC17:
	.string	"CopyForPreventExtensions"
	.section	.text._ZN2v88internal10MapUpdater43ConstructNewMapWithIntegrityLevelTransitionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdater43ConstructNewMapWithIntegrityLevelTransitionEv
	.type	_ZN2v88internal10MapUpdater43ConstructNewMapWithIntegrityLevelTransitionEv, @function
_ZN2v88internal10MapUpdater43ConstructNewMapWithIntegrityLevelTransitionEv:
.LFB17813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	movq	(%rax), %rax
	movq	$0, -40(%rbp)
	movq	%rax, -48(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -40(%rbp)
	testb	$1, %al
	je	.L647
	cmpl	$3, %eax
	je	.L647
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L660
	cmpq	$1, %rdx
	je	.L661
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L647:
	movl	$1, -32(%rbp)
.L649:
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv@PLT
	testb	%al, %al
	je	.L662
	movq	8(%rbx), %rax
	movq	64(%rbx), %rcx
	leaq	.LC17(%rip), %r8
	movl	56(%rbx), %edx
	movq	32(%rbx), %rsi
	movq	(%rax), %rax
	movq	(%rbx), %rdi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpb	$12, %al
	sete	%r9b
	movzbl	%r9b, %r9d
	call	_ZN2v88internal3Map24CopyForPreventExtensionsEPNS0_7IsolateENS0_6HandleIS1_EENS0_18PropertyAttributesENS4_INS0_6SymbolEEEPKcb@PLT
	movl	$4, 80(%rbx)
	movq	%rax, 40(%rbx)
.L656:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L663
	addq	$56, %rsp
	movl	$4, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L662:
	.cfi_restore_state
	movzbl	84(%rbx), %edx
	movq	(%rbx), %rdi
	leaq	.LC15(%rip), %r8
	xorl	%ecx, %ecx
	movq	8(%rbx), %rsi
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc@PLT
	movl	$4, 80(%rbx)
	movq	%rax, 40(%rbx)
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L660:
	movl	$3, -32(%rbp)
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L661:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L664
	movl	$4, -32(%rbp)
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L664:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -32(%rbp)
	jmp	.L649
.L663:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17813:
	.size	_ZN2v88internal10MapUpdater43ConstructNewMapWithIntegrityLevelTransitionEv, .-_ZN2v88internal10MapUpdater43ConstructNewMapWithIntegrityLevelTransitionEv
	.section	.text._ZN2v88internal10MapUpdater22ReconfigureToDataFieldEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdater22ReconfigureToDataFieldEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEE
	.type	_ZN2v88internal10MapUpdater22ReconfigureToDataFieldEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEE, @function
_ZN2v88internal10MapUpdater22ReconfigureToDataFieldEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEE:
.LFB17792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, 96(%rdi)
	movq	16(%rdi), %rdx
	leal	3(%rsi,%rsi,2), %eax
	movl	%esi, 88(%rdi)
	sall	$3, %eax
	movl	$0, 92(%rdi)
	cltq
	movl	$0, 104(%rdi)
	movq	(%rdx), %rdx
	movq	7(%rax,%rdx), %rax
	sarq	$32, %rax
	movl	%eax, %edx
	andl	$1, %edx
	cmpl	%edx, 92(%rdi)
	je	.L694
	movl	$0, 100(%rdi)
	movb	%r8b, 108(%rdi)
	movq	%r9, 112(%rbx)
.L678:
	movq	8(%rbx), %rax
	movq	(%rax), %rax
	movzwl	11(%rax), %eax
	subw	$1041, %ax
	cmpw	$20, %ax
	ja	.L679
	movl	$1179649, %edx
	btq	%rax, %rdx
	jnc	.L679
	movq	(%rbx), %rdi
	call	_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE@PLT
	movb	$4, 108(%rbx)
	movq	%rax, 112(%rbx)
.L679:
	movq	%rbx, %rdi
	call	_ZN2v88internal10MapUpdater32TryReconfigureToDataFieldInplaceEv
	cmpl	$4, %eax
	jne	.L680
.L681:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	40(%rbx), %rax
	jne	.L695
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L680:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal10MapUpdater11FindRootMapEv
	cmpl	$4, %eax
	je	.L681
	movq	%rbx, %rdi
	call	_ZN2v88internal10MapUpdater13FindTargetMapEv
	cmpl	$4, %eax
	je	.L681
	movq	%rbx, %rdi
	call	_ZN2v88internal10MapUpdater15ConstructNewMapEv
	cmpl	$3, %eax
	jne	.L681
	movq	%rbx, %rdi
	call	_ZN2v88internal10MapUpdater43ConstructNewMapWithIntegrityLevelTransitionEv
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L694:
	movl	%eax, %edx
	movl	%eax, %r13d
	movl	%r8d, %r9d
	shrl	$2, %edx
	andl	$1, %edx
	testl	%ecx, %ecx
	cmovne	%edx, %ecx
	shrl	$6, %r13d
	andl	$7, %r13d
	cmpb	%r8b, %r13b
	movl	%ecx, 100(%rdi)
	setl	%dl
	cmpb	$3, %r8b
	jne	.L669
	testb	%r13b, %r13b
	sete	%dl
.L669:
	cmpb	%r13b, %r8b
	je	.L670
	testb	%dl, %dl
	je	.L696
.L670:
	movl	88(%rbx), %edx
	movq	16(%rbx), %rcx
	movb	%r9b, 108(%rbx)
	leal	3(%rdx,%rdx,2), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	testb	$2, %al
	jne	.L673
	movq	(%rcx), %rax
	movq	(%rbx), %r15
	movq	15(%rdx,%rax), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L674
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L673:
	movq	(%rcx), %rax
	leaq	-64(%rbp), %rdi
	movq	15(%rdx,%rax), %rax
	movq	(%rbx), %rsi
	movl	%r9d, %edx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object11OptimalTypeEPNS0_7IsolateENS0_14RepresentationE@PLT
	movq	%rax, %rsi
.L677:
	movzbl	108(%rbx), %edx
	movq	(%rbx), %r8
	movq	%r12, %rcx
	movl	%r13d, %edi
	call	_ZN2v88internal3Map19GeneralizeFieldTypeENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEES2_S5_PNS0_7IsolateE@PLT
	movq	%rax, 112(%rbx)
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L696:
	cmpb	%r8b, %r13b
	setg	%dl
	cmpb	$3, %r13b
	jne	.L672
	testb	%r8b, %r8b
	sete	%dl
.L672:
	testb	%dl, %dl
	movl	$4, %r9d
	cmovne	%r13d, %r9d
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L674:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L697
.L676:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r14, (%rsi)
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L697:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L676
.L695:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17792:
	.size	_ZN2v88internal10MapUpdater22ReconfigureToDataFieldEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEE, .-_ZN2v88internal10MapUpdater22ReconfigureToDataFieldEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEE
	.section	.text._ZN2v88internal10MapUpdater23ReconfigureElementsKindENS0_12ElementsKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdater23ReconfigureElementsKindENS0_12ElementsKindE
	.type	_ZN2v88internal10MapUpdater23ReconfigureElementsKindENS0_12ElementsKindE, @function
_ZN2v88internal10MapUpdater23ReconfigureElementsKindENS0_12ElementsKindE:
.LFB17793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$5, %sil
	movb	%sil, 84(%rdi)
	setbe	%al
	cmpb	$3, %sil
	setne	%dl
	andl	%edx, %eax
	movb	%al, 85(%rdi)
	call	_ZN2v88internal10MapUpdater11FindRootMapEv
	cmpl	$4, %eax
	jne	.L699
.L700:
	movq	40(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L699:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal10MapUpdater13FindTargetMapEv
	cmpl	$4, %eax
	je	.L700
	movq	%rbx, %rdi
	call	_ZN2v88internal10MapUpdater15ConstructNewMapEv
	cmpl	$3, %eax
	jne	.L700
	movq	%rbx, %rdi
	call	_ZN2v88internal10MapUpdater43ConstructNewMapWithIntegrityLevelTransitionEv
	jmp	.L700
	.cfi_endproc
.LFE17793:
	.size	_ZN2v88internal10MapUpdater23ReconfigureElementsKindENS0_12ElementsKindE, .-_ZN2v88internal10MapUpdater23ReconfigureElementsKindENS0_12ElementsKindE
	.section	.text._ZN2v88internal10MapUpdater6UpdateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MapUpdater6UpdateEv
	.type	_ZN2v88internal10MapUpdater6UpdateEv, @function
_ZN2v88internal10MapUpdater6UpdateEv:
.LFB17794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal10MapUpdater11FindRootMapEv
	cmpl	$4, %eax
	jne	.L707
.L708:
	movq	40(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L707:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal10MapUpdater13FindTargetMapEv
	cmpl	$4, %eax
	je	.L708
	movq	%rbx, %rdi
	call	_ZN2v88internal10MapUpdater15ConstructNewMapEv
	cmpl	$3, %eax
	jne	.L708
	movq	%rbx, %rdi
	call	_ZN2v88internal10MapUpdater43ConstructNewMapWithIntegrityLevelTransitionEv
	jmp	.L708
	.cfi_endproc
.LFE17794:
	.size	_ZN2v88internal10MapUpdater6UpdateEv, .-_ZN2v88internal10MapUpdater6UpdateEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10MapUpdaterC2EPNS0_7IsolateENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10MapUpdaterC2EPNS0_7IsolateENS0_6HandleINS0_3MapEEE, @function
_GLOBAL__sub_I__ZN2v88internal10MapUpdaterC2EPNS0_7IsolateENS0_6HandleINS0_3MapEEE:
.LFB21497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21497:
	.size	_GLOBAL__sub_I__ZN2v88internal10MapUpdaterC2EPNS0_7IsolateENS0_6HandleINS0_3MapEEE, .-_GLOBAL__sub_I__ZN2v88internal10MapUpdaterC2EPNS0_7IsolateENS0_6HandleINS0_3MapEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10MapUpdaterC2EPNS0_7IsolateENS0_6HandleINS0_3MapEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
