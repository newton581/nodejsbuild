	.file	"wasm-debug.cc"
	.text
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB25020:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25020:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB25027:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25027:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB25032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L4
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L4:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25032:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB25025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L8
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L8:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25025:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB25031:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE25031:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB25024:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE25024:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB25030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	128(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L15
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L16
.L15:
	movq	120(%rbx), %rax
	movq	112(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	112(%rbx), %rdi
	leaq	160(%rbx), %rax
	movq	$0, 136(%rbx)
	movq	$0, 128(%rbx)
	cmpq	%rax, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	leaq	32(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm15WasmInterpreterD1Ev@PLT
	.cfi_endproc
.LFE25030:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB25023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	40(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L24
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L25
.L24:
	movq	32(%rbx), %rax
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	addq	$72, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L23
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25023:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB25029:
	.cfi_startproc
	endbr64
	movl	$168, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25029:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB25022:
	.cfi_startproc
	endbr64
	movl	$80, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25022:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_117InterpreterHandle17GetInstanceObjectEv.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_117InterpreterHandle17GetInstanceObjectEv.isra.0, @function
_ZN2v88internal4wasm12_GLOBAL__N_117InterpreterHandle17GetInstanceObjectEv.isra.0:
.LFB25251:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-1472(%rbp), %rdi
	subq	$1480, %rsp
	movq	(%rbx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	(%rbx), %rbx
	movq	-56(%rbp), %rdi
	call	_ZNK2v88internal25WasmInterpreterEntryFrame13wasm_instanceEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L35
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L36:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L40
	addq	$1480, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L41
.L37:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rbx, %rdi
	movq	%rsi, -1480(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1480(%rbp), %rsi
	jmp	.L37
.L40:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25251:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_117InterpreterHandle17GetInstanceObjectEv.isra.0, .-_ZN2v88internal4wasm12_GLOBAL__N_117InterpreterHandle17GetInstanceObjectEv.isra.0
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%ld"
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"len > 0 && len < value.length()"
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE.str1.1
.LC2:
	.string	"Check failed: %s."
.LC3:
	.string	"(location_) != nullptr"
.LC4:
	.string	"unimplemented code"
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE, @function
_ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE:
.LFB19930:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$88, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$6, 16(%rbp)
	ja	.L43
	movzbl	16(%rbp), %eax
	leaq	.L45(%rip), %rdx
	movq	%rdi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE,"a",@progbits
	.align 4
	.align 4
.L45:
	.long	.L43-.L45
	.long	.L49-.L45
	.long	.L48-.L45
	.long	.L47-.L45
	.long	.L46-.L45
	.long	.L43-.L45
	.long	.L44-.L45
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE
	.p2align 4,,10
	.p2align 3
.L44:
	movq	17(%rbp), %rax
.L53:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L67
	addq	$88, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movslq	17(%rbp), %rsi
	movq	41112(%rdi), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L63
.L55:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L48:
	movq	17(%rbp), %rcx
	movslq	%ecx, %rsi
	cmpq	%rcx, %rsi
	jne	.L54
	movq	41112(%rdi), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	jne	.L55
.L63:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L68
.L61:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L47:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	cvtss2sd	17(%rbp), %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L46:
	movsd	17(%rbp), %xmm0
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	-48(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rdx
	movl	$21, %esi
	movq	%rdi, -64(%rbp)
	movq	$21, -56(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	testl	%eax, %eax
	jle	.L57
	cmpl	-56(%rbp), %eax
	jge	.L57
	movq	-64(%rbp), %rdx
	cltq
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	movq	%rdx, -80(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	jne	.L53
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L43:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19930:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE, .-_ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE
	.section	.text._ZN2v88internal7ManagedINS0_4wasm12_GLOBAL__N_117InterpreterHandleEE10DestructorEPv,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal7ManagedINS0_4wasm12_GLOBAL__N_117InterpreterHandleEE10DestructorEPv, @function
_ZN2v88internal7ManagedINS0_4wasm12_GLOBAL__N_117InterpreterHandleEE10DestructorEPv:
.LFB24072:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L69
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L72
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L73
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L74:
	cmpl	$1, %eax
	je	.L81
.L72:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L69:
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L76
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L77:
	cmpl	$1, %eax
	jne	.L72
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L73:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L76:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L77
	.cfi_endproc
.LFE24072:
	.size	_ZN2v88internal7ManagedINS0_4wasm12_GLOBAL__N_117InterpreterHandleEE10DestructorEPv, .-_ZN2v88internal7ManagedINS0_4wasm12_GLOBAL__N_117InterpreterHandleEE10DestructorEPv
	.section	.text._ZN2v88internal7ManagedINS0_4wasm12SignatureMapEE10DestructorEPv,"axG",@progbits,_ZN2v88internal7ManagedINS0_4wasm12SignatureMapEE10DestructorEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal7ManagedINS0_4wasm12SignatureMapEE10DestructorEPv
	.type	_ZN2v88internal7ManagedINS0_4wasm12SignatureMapEE10DestructorEPv, @function
_ZN2v88internal7ManagedINS0_4wasm12SignatureMapEE10DestructorEPv:
.LFB24134:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L82
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L85
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L86
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L87:
	cmpl	$1, %eax
	je	.L94
.L85:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L89
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L90:
	cmpl	$1, %eax
	jne	.L85
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L86:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L89:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L90
	.cfi_endproc
.LFE24134:
	.size	_ZN2v88internal7ManagedINS0_4wasm12SignatureMapEE10DestructorEPv, .-_ZN2v88internal7ManagedINS0_4wasm12SignatureMapEE10DestructorEPv
	.section	.text._ZN2v88internal12_GLOBAL__N_128GetOrCreateInterpreterHandleEPNS0_7IsolateENS0_6HandleINS0_13WasmDebugInfoEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_128GetOrCreateInterpreterHandleEPNS0_7IsolateENS0_6HandleINS0_13WasmDebugInfoEEE, @function
_ZN2v88internal12_GLOBAL__N_128GetOrCreateInterpreterHandleEPNS0_7IsolateENS0_6HandleINS0_13WasmDebugInfoEEE:
.LFB20036:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L96
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	cmpq	%rsi, 88(%r12)
	je	.L133
.L99:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	7(%rsi), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	jne	.L134
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L135
.L98:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	cmpq	%rsi, 88(%r12)
	jne	.L99
.L133:
	movl	_ZN2v88internal15FLAG_stack_sizeE(%rip), %eax
	movl	$168, %edi
	sall	$11, %eax
	movl	%eax, -88(%rbp)
	movslq	%eax, %rbx
	call	_Znwm@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r13
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r13)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	32(%r13), %r15
	movq	%rax, 0(%r13)
	movq	(%r14), %rax
	movq	%r12, 16(%r13)
	movq	7(%rax), %rdx
	movq	135(%rdx), %rdx
	movq	23(%rdx), %rdx
	movq	7(%rdx), %rdx
	movq	24(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	208(%rdx), %rdx
	movq	%rdx, 24(%r13)
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L100
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L101:
	movq	(%r14), %rax
	movq	%r15, %rdi
	leaq	-80(%rbp), %rcx
	movq	%r12, %rsi
	movq	7(%rax), %rax
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	232(%rax), %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -72(%rbp)
	movq	24(%r13), %rdx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal4wasm15WasmInterpreterC1EPNS0_7IsolateEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesENS0_6HandleINS0_18WasmInstanceObjectEEE@PLT
	movq	32(%r12), %r15
	leaq	160(%r13), %rax
	movb	$-1, 104(%r13)
	movq	%rax, 112(%r13)
	addq	%rbx, %r15
	movl	$0, 108(%r13)
	movq	%r15, %rax
	subq	48(%r12), %rax
	movq	$1, 120(%r13)
	movq	$0, 128(%r13)
	movq	$0, 136(%r13)
	movl	$0x3f800000, 144(%r13)
	movq	$0, 152(%r13)
	movq	$0, 160(%r13)
	movq	%r15, 32(%r12)
	cmpq	$33554432, %rax
	jg	.L136
.L103:
	movl	-88(%rbp), %eax
	testl	%eax, %eax
	js	.L137
	je	.L105
	cmpq	40(%r12), %r15
	jg	.L138
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$16, %edi
	call	_Znwm@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, %rdx
	leaq	16(%r13), %rax
	movq	%rax, (%rdx)
	leaq	8(%r13), %rax
	movq	%r13, 8(%rdx)
	movq	%rax, -88(%rbp)
	je	.L139
	leaq	8(%r13), %rax
	lock addl	$1, (%rax)
.L106:
	movl	$48, %edi
	movq	%rdx, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, %r15
	movups	%xmm0, 8(%rax)
	movq	%rbx, (%rax)
	movq	%r15, %rsi
	movq	%rdx, 24(%rax)
	leaq	_ZN2v88internal7ManagedINS0_4wasm12_GLOBAL__N_117InterpreterHandleEE10DestructorEPv(%rip), %rax
	xorl	%edx, %edx
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r12), %rdi
	movq	(%rax), %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%rax, 40(%r15)
	movq	%rax, %rdi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L107
	leaq	8(%r13), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
.L108:
	cmpl	$1, %eax
	je	.L140
.L110:
	movq	(%r14), %r13
	movq	(%rbx), %r12
	movq	%r12, 15(%r13)
	leaq	15(%r13), %r15
	testb	$1, %r12b
	je	.L117
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L115
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
.L115:
	testb	$24, %al
	je	.L117
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L141
	.p2align 4,,10
	.p2align 3
.L117:
	movq	(%rbx), %rsi
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L100:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L142
.L102:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L137:
	movq	40(%r12), %rax
	addq	%rbx, %rax
	cmpq	$67108864, %rax
	jle	.L105
	movq	%rax, 40(%r12)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L136:
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L139:
	addl	$1, 8(%r13)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L107:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r12, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L140:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L111
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L112:
	cmpl	$1, %eax
	jne	.L110
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L111:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L142:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L102
.L134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20036:
	.size	_ZN2v88internal12_GLOBAL__N_128GetOrCreateInterpreterHandleEPNS0_7IsolateENS0_6HandleINS0_13WasmDebugInfoEEE, .-_ZN2v88internal12_GLOBAL__N_128GetOrCreateInterpreterHandleEPNS0_7IsolateENS0_6HandleINS0_13WasmDebugInfoEEE
	.section	.text._ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE
	.type	_ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE, @function
_ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE:
.LFB20039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	$105, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE@PLT
	movq	(%rbx), %r13
	movq	(%rax), %r14
	movq	%rax, %r12
	movq	%r13, 7(%r14)
	testb	$1, %r13b
	je	.L151
	movq	%r13, %r15
	leaq	7(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L165
	testb	$24, %al
	je	.L151
.L170:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L166
	.p2align 4,,10
	.p2align 3
.L151:
	movq	(%rbx), %r14
	movq	(%r12), %r13
	movq	%r13, 191(%r14)
	leaq	191(%r14), %r15
	testb	$1, %r13b
	je	.L150
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L167
	testb	$24, %al
	je	.L150
.L169:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L168
.L150:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L169
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L170
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L150
	.cfi_endproc
.LFE20039:
	.size	_ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE, .-_ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE
	.section	.text._ZN2v88internal13WasmDebugInfo15SetupForTestingENS0_6HandleINS0_18WasmInstanceObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13WasmDebugInfo15SetupForTestingENS0_6HandleINS0_18WasmInstanceObjectEEE
	.type	_ZN2v88internal13WasmDebugInfo15SetupForTestingENS0_6HandleINS0_18WasmInstanceObjectEEE, @function
_ZN2v88internal13WasmDebugInfo15SetupForTestingENS0_6HandleINS0_18WasmInstanceObjectEEE:
.LFB20040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE
	movl	$168, %edi
	movq	%rax, %rbx
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r9
	movl	_ZN2v88internal15FLAG_stack_sizeE(%rip), %eax
	sall	$11, %eax
	leaq	-37592(%r9), %r14
	movl	%eax, -88(%rbp)
	movslq	%eax, %r13
	call	_Znwm@PLT
	movq	%rax, %r12
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r12)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	32(%r12), %r15
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	%r14, 16(%r12)
	movq	7(%rax), %rdx
	movq	135(%rdx), %rdx
	movq	23(%rdx), %rdx
	movq	7(%rdx), %rdx
	movq	24(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	208(%rdx), %rdx
	movq	%rdx, 24(%r12)
	movq	41112(%r14), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L172
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L173:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	leaq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movq	7(%rax), %rax
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	232(%rax), %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -72(%rbp)
	movq	24(%r12), %rdx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal4wasm15WasmInterpreterC1EPNS0_7IsolateEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesENS0_6HandleINS0_18WasmInstanceObjectEEE@PLT
	leaq	160(%r12), %rax
	movb	$-1, 104(%r12)
	movq	%rax, 112(%r12)
	movl	$0, 108(%r12)
	movq	$1, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movl	$0x3f800000, 144(%r12)
	movq	$0, 152(%r12)
	movq	$0, 160(%r12)
	movq	32(%r14), %r15
	addq	%r13, %r15
	movq	%r15, %rax
	subq	48(%r14), %rax
	movq	%r15, 32(%r14)
	cmpq	$33554432, %rax
	jg	.L205
.L175:
	movl	-88(%rbp), %eax
	testl	%eax, %eax
	js	.L206
	je	.L177
	cmpq	40(%r14), %r15
	jg	.L207
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$16, %edi
	call	_Znwm@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, %rdx
	leaq	16(%r12), %rax
	movq	%rax, (%rdx)
	leaq	8(%r12), %rax
	movq	%r12, 8(%rdx)
	movq	%rax, -88(%rbp)
	je	.L208
	leaq	8(%r12), %rax
	lock addl	$1, (%rax)
.L178:
	movl	$48, %edi
	movq	%rdx, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	%rax, %r15
	movups	%xmm0, 8(%rax)
	movq	%r13, (%rax)
	movq	%r15, %rsi
	movq	%rdx, 24(%rax)
	leaq	_ZN2v88internal7ManagedINS0_4wasm12_GLOBAL__N_117InterpreterHandleEE10DestructorEPv(%rip), %rax
	xorl	%edx, %edx
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r14), %rdi
	movq	(%rax), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%rax, 40(%r15)
	movq	%rax, %rdi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L179
	leaq	8(%r12), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
	cmpl	$1, %eax
	je	.L209
.L182:
	movq	(%rbx), %r14
	movq	0(%r13), %r12
	movq	%r12, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %r12b
	je	.L189
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L187
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L187:
	testb	$24, %al
	je	.L189
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L210
	.p2align 4,,10
	.p2align 3
.L189:
	movq	0(%r13), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	addq	$16, %rax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L211
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	addl	$1, 8(%r12)
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L172:
	movq	41088(%r14), %r8
	cmpq	41096(%r14), %r8
	je	.L212
.L174:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r8)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L206:
	movq	40(%r14), %rax
	addq	%r13, %rax
	cmpq	$67108864, %rax
	jle	.L177
	movq	%rax, 40(%r14)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%r14, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L179:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L182
.L209:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L183
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L184:
	cmpl	$1, %eax
	jne	.L182
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%r14, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L183:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%r14, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L174
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20040:
	.size	_ZN2v88internal13WasmDebugInfo15SetupForTestingENS0_6HandleINS0_18WasmInstanceObjectEEE, .-_ZN2v88internal13WasmDebugInfo15SetupForTestingENS0_6HandleINS0_18WasmInstanceObjectEEE
	.section	.text._ZN2v88internal13WasmDebugInfo21RedirectToInterpreterENS0_6HandleIS1_EENS0_6VectorIiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13WasmDebugInfo21RedirectToInterpreterENS0_6HandleIS1_EENS0_6VectorIiEE
	.type	_ZN2v88internal13WasmDebugInfo21RedirectToInterpreterENS0_6HandleIS1_EENS0_6VectorIiEE, @function
_ZN2v88internal13WasmDebugInfo21RedirectToInterpreterENS0_6HandleIS1_EENS0_6VectorIiEE:
.LFB20042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %r14
	movq	%r14, %rdi
	movq	%r14, -328(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_128GetOrCreateInterpreterHandleEPNS0_7IsolateENS0_6HandleINS0_13WasmDebugInfoEEE
	movq	(%r12), %rax
	movq	41112(%r14), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L214
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L215:
	movq	135(%rsi), %rax
	leaq	-192(%rbp), %r14
	movq	%r14, %rdi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r12
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal18WasmInstanceObject6moduleEv@PLT
	movq	%rax, -336(%rbp)
	leaq	-304(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal4wasm29NativeModuleModificationScopeC1EPNS1_12NativeModuleE@PLT
	leaq	(%rbx,%r13,4), %rax
	leaq	-256(%rbp), %rdx
	movq	%rax, -312(%rbp)
	movq	%rdx, -320(%rbp)
	cmpq	%rax, %rbx
	je	.L227
	.p2align 4,,10
	.p2align 3
.L228:
	movl	(%rbx), %r15d
	movq	%r12, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm12NativeModule25IsRedirectedToInterpreterEj@PLT
	testb	%al, %al
	jne	.L219
	movq	-320(%rbp), %rdi
	leaq	-272(%rbp), %r13
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	-336(%rbp), %rcx
	movslq	%r15d, %rax
	movq	%r14, %rdi
	salq	$5, %rax
	leaq	192(%r12), %rdx
	addq	136(%rcx), %rax
	movq	-328(%rbp), %rcx
	movq	(%rax), %r8
	movq	45752(%rcx), %rsi
	movl	%r15d, %ecx
	call	_ZN2v88internal8compiler27CompileWasmInterpreterEntryEPNS0_4wasm10WasmEngineERKNS2_12WasmFeaturesEjPNS0_9SignatureINS2_9ValueTypeEEE@PLT
	movq	-96(%rbp), %rax
	pushq	$1
	movq	%r14, %rcx
	pushq	$4
	movl	-100(%rbp), %r9d
	leaq	-296(%rbp), %rdi
	movl	%r15d, %edx
	movq	%rax, -272(%rbp)
	movq	-88(%rbp), %rax
	movq	%r12, %rsi
	pushq	%r13
	movl	-104(%rbp), %r8d
	movq	%rax, -264(%rbp)
	movq	-80(%rbp), %rax
	movq	$0, -96(%rbp)
	movq	%rax, -288(%rbp)
	movq	-72(%rbp), %rax
	movq	$0, -88(%rbp)
	movq	%rax, -280(%rbp)
	leaq	-288(%rbp), %rax
	pushq	%rax
	movq	$0, -80(%rbp)
	call	_ZN2v88internal4wasm12NativeModule7AddCodeEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierE@PLT
	movq	-288(%rbp), %rdi
	addq	$32, %rsp
	testq	%rdi, %rdi
	je	.L220
	call	_ZdaPv@PLT
.L220:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L221
	call	_ZdaPv@PLT
.L221:
	movq	-296(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -296(%rbp)
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal4wasm12NativeModule11PublishCodeESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE@PLT
	movq	-272(%rbp), %r13
	testq	%r13, %r13
	je	.L222
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm8WasmCodeD1Ev@PLT
	movl	$144, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L222:
	movq	-296(%rbp), %r13
	testq	%r13, %r13
	je	.L223
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm8WasmCodeD1Ev@PLT
	movl	$144, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L223:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L224
	call	_ZdaPv@PLT
.L224:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L225
	call	_ZdaPv@PLT
.L225:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L226
	call	_ZdaPv@PLT
.L226:
	movq	-320(%rbp), %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
.L219:
	addq	$4, %rbx
	cmpq	%rbx, -312(%rbp)
	jne	.L228
.L227:
	movq	-344(%rbp), %rdi
	call	_ZN2v88internal4wasm29NativeModuleModificationScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L255
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movq	-328(%rbp), %rdx
	movq	41088(%rdx), %rax
	cmpq	%rax, 41096(%rdx)
	je	.L256
.L216:
	movq	-328(%rbp), %rcx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L215
.L256:
	movq	%rdx, %rdi
	movq	%rsi, -312(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	jmp	.L216
.L255:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20042:
	.size	_ZN2v88internal13WasmDebugInfo21RedirectToInterpreterENS0_6HandleIS1_EENS0_6VectorIiEE, .-_ZN2v88internal13WasmDebugInfo21RedirectToInterpreterENS0_6HandleIS1_EENS0_6VectorIiEE
	.section	.text._ZN2v88internal13WasmDebugInfo13SetBreakpointENS0_6HandleIS1_EEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13WasmDebugInfo13SetBreakpointENS0_6HandleIS1_EEii
	.type	_ZN2v88internal13WasmDebugInfo13SetBreakpointENS0_6HandleIS1_EEii, @function
_ZN2v88internal13WasmDebugInfo13SetBreakpointENS0_6HandleIS1_EEii:
.LFB20041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	movl	%esi, -36(%rbp)
	movq	%r13, %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal12_GLOBAL__N_128GetOrCreateInterpreterHandleEPNS0_7IsolateENS0_6HandleINS0_13WasmDebugInfoEEE
	leaq	-36(%rbp), %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	movq	%rax, %rbx
	call	_ZN2v88internal13WasmDebugInfo21RedirectToInterpreterENS0_6HandleIS1_EENS0_6VectorIiEE
	movq	8(%rbx), %rax
	movslq	-36(%rbp), %rsi
	movslq	%r12d, %rdx
	leaq	16(%rbx), %rdi
	movl	$1, %ecx
	salq	$5, %rsi
	addq	136(%rax), %rsi
	call	_ZN2v88internal4wasm15WasmInterpreter13SetBreakpointEPKNS1_12WasmFunctionEmb@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20041:
	.size	_ZN2v88internal13WasmDebugInfo13SetBreakpointENS0_6HandleIS1_EEii, .-_ZN2v88internal13WasmDebugInfo13SetBreakpointENS0_6HandleIS1_EEii
	.section	.text._ZN2v88internal13WasmDebugInfo11PrepareStepENS0_10StepActionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13WasmDebugInfo11PrepareStepENS0_10StepActionE
	.type	_ZN2v88internal13WasmDebugInfo11PrepareStepENS0_10StepActionE, @function
_ZN2v88internal13WasmDebugInfo11PrepareStepENS0_10StepActionE:
.LFB20059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	15(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rbx
	movb	%sil, 88(%rbx)
	leaq	16(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv@PLT
	movl	%eax, 92(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20059:
	.size	_ZN2v88internal13WasmDebugInfo11PrepareStepENS0_10StepActionE, .-_ZN2v88internal13WasmDebugInfo11PrepareStepENS0_10StepActionE
	.section	.text.unlikely._ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi,"ax",@progbits
	.align 2
.LCOLDB6:
	.section	.text._ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi,"ax",@progbits
.LHOTB6:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi
	.type	_ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi, @function
_ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi:
.LFB20062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	xorl	%esi, %esi
	movq	15(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r15
	leaq	16(%r15), %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi@PLT
	movq	104(%r15), %rsi
	xorl	%edx, %edx
	movq	%rax, %r12
	movq	%rbx, %rax
	divq	%rsi
	movq	96(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L262
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L279:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L262
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L278
.L264:
	cmpq	%rdi, %rbx
	jne	.L279
	movl	16(%rcx), %esi
	movq	120(%r15), %r15
	movq	%r12, %rdi
	movl	%esi, -68(%rbp)
	subl	$1, %r15d
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj@PLT
	movl	-68(%rbp), %esi
	movl	%eax, %ebx
	cmpl	%r15d, %esi
	je	.L280
	addl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj@PLT
.L265:
	leal	(%r14,%rbx), %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread8GetFrameEi@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L281
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv@PLT
	jmp	.L265
.L281:
	call	__stack_chk_fail@PLT
.L278:
	jmp	.L262
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi
	.cfi_startproc
	.type	_ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi.cold, @function
_ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi.cold:
.LFSB20062:
.L262:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	16, %eax
	ud2
	.cfi_endproc
.LFE20062:
	.section	.text._ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi
	.size	_ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi, .-_ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi
	.section	.text.unlikely._ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi
	.size	_ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi.cold, .-_ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi.cold
.LCOLDE6:
	.section	.text._ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi
.LHOTE6:
	.section	.text._ZN2v88internal13WasmDebugInfo19NumInterpretedCallsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13WasmDebugInfo19NumInterpretedCallsEv
	.type	_ZN2v88internal13WasmDebugInfo19NumInterpretedCallsEv, @function
_ZN2v88internal13WasmDebugInfo19NumInterpretedCallsEv:
.LFB20063:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L283
.L286:
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L287
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi@PLT
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	_ZN2v88internal4wasm15WasmInterpreter6Thread19NumInterpretedCallsEv@PLT
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	jne	.L286
.L287:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE20063:
	.size	_ZN2v88internal13WasmDebugInfo19NumInterpretedCallsEv, .-_ZN2v88internal13WasmDebugInfo19NumInterpretedCallsEv
	.section	.rodata._ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi.str1.1,"aMS",@progbits,1
.LC7:
	.string	"memory"
.LC8:
	.string	"globals"
.LC9:
	.string	"global#%d"
	.section	.text.unlikely._ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi,"ax",@progbits
	.align 2
.LCOLDB10:
	.section	.text._ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi,"ax",@progbits
.LHOTB10:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi
	.type	_ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi, @function
_ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi:
.LFB20064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	xorl	%esi, %esi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	15(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r14
	leaq	16(%r14), %rax
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi@PLT
	movq	104(%r14), %rsi
	xorl	%edx, %edx
	movq	%rax, %r13
	movq	%r15, %rax
	divq	%rsi
	movq	96(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L291
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movq	8(%rcx), %rdi
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L327:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L291
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L326
.L293:
	cmpq	%rdi, %r15
	jne	.L327
	movl	16(%rcx), %r15d
	movl	120(%r14), %eax
	movq	%r13, %rdi
	subl	$1, %eax
	movl	%r15d, %esi
	movl	%eax, -152(%rbp)
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj@PLT
	movl	%eax, -144(%rbp)
	cmpl	-152(%rbp), %r15d
	je	.L328
	leal	1(%r15), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj@PLT
.L294:
	movl	-144(%rbp), %edx
	leaq	-120(%rbp), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
	addl	%ebx, %edx
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread8GetFrameEi@PLT
	movq	(%r14), %r13
	movq	(%r12), %rax
	movq	41112(%r13), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L295
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L296:
	movq	(%r14), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-37504(%rdx), %rcx
	cmpq	%rcx, 159(%rax)
	jne	.L329
.L298:
	movq	-136(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalCountEv@PLT
	movl	%eax, -136(%rbp)
	testl	%eax, %eax
	jne	.L330
.L302:
	movq	-120(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L306
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm23InterpretedFrameDeleterclEPNS1_16InterpretedFrameE@PLT
.L306:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L331
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv@PLT
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L329:
	movq	(%r14), %rdi
	leaq	.LC7(%rip), %rax
	leaq	-112(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -112(%rbp)
	movq	$6, -104(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	(%r14), %rcx
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	41112(%rcx), %rdi
	movq	159(%rax), %rax
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L299
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L300:
	movq	(%r14), %rdi
	movq	23(%rsi), %r8
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movl	$2, %esi
	call	_ZN2v88internal7Factory15NewJSTypedArrayENS0_17ExternalArrayTypeENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L295:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L332
.L297:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L330:
	movq	(%r14), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	(%r14), %rdi
	leaq	-112(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rax, %r13
	movq	%rcx, %rsi
	movq	%rax, -144(%rbp)
	leaq	.LC8(%rip), %rax
	movq	%rcx, -152(%rbp)
	movq	%rax, -112(%rbp)
	movq	$7, -104(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%r13d, %r13d
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	leaq	-96(%rbp), %rax
	movq	%r15, -176(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-80(%rbp), %rax
	movq	%r12, -168(%rbp)
	movl	%r13d, %r12d
	movq	%rbx, %r13
	movq	%rax, %rbx
.L305:
	xorl	%eax, %eax
	movl	%r12d, %ecx
	leaq	.LC9(%rip), %rdx
	movq	%rbx, %rdi
	movl	$21, %esi
	movq	(%r14), %r15
	movq	%rbx, -96(%rbp)
	movq	$21, -88(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	testl	%eax, %eax
	jle	.L303
	cmpl	-88(%rbp), %eax
	jge	.L303
	movq	-96(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	%r15, %rdi
	cltq
	movq	%rax, -104(%rbp)
	movq	%rdx, -112(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-160(%rbp), %rdi
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%rax, %r15
	addl	$1, %r12d
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalValueEj@PLT
	movdqa	-96(%rbp), %xmm0
	movzbl	-80(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm0, (%rsp)
	movb	%al, 16(%rsp)
	movq	(%r14), %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE
	addq	$32, %rsp
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	-144(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	cmpl	%r12d, -136(%rbp)
	jne	.L305
	movq	-168(%rbp), %r12
	movq	-176(%rbp), %r15
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L299:
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L333
.L301:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L303:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L332:
	movq	%r13, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%rcx, %rdi
	movq	%rsi, -152(%rbp)
	movq	%rcx, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L301
.L331:
	call	__stack_chk_fail@PLT
.L326:
	jmp	.L291
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi
	.cfi_startproc
	.type	_ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi.cold, @function
_ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi.cold:
.LFSB20064:
.L291:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	16, %eax
	ud2
	.cfi_endproc
.LFE20064:
	.section	.text._ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi
	.size	_ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi, .-_ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi
	.section	.text.unlikely._ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi
	.size	_ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi.cold, .-_ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi.cold
.LCOLDE10:
	.section	.text._ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi
.LHOTE10:
	.section	.rodata._ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi.str1.1,"aMS",@progbits,1
.LC11:
	.string	"arg#%d"
.LC12:
	.string	"local#%d"
.LC13:
	.string	"locals"
.LC14:
	.string	"stack"
	.section	.text.unlikely._ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi,"ax",@progbits
	.align 2
.LCOLDB15:
	.section	.text._ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi,"ax",@progbits
.LHOTB15:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi
	.type	_ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi, @function
_ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi:
.LFB20065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$184, %rsp
	movq	%rdi, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	15(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r12
	leaq	16(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi@PLT
	movq	104(%r12), %rsi
	xorl	%edx, %edx
	movq	%rax, %r15
	movq	%r13, %rax
	divq	%rsi
	movq	96(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L335
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L402:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L335
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L401
.L337:
	cmpq	%rdi, %r13
	jne	.L402
	movl	16(%rcx), %r14d
	movl	120(%r12), %eax
	movq	%r15, %rdi
	subl	$1, %eax
	movl	%r14d, %esi
	movl	%eax, -144(%rbp)
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj@PLT
	movl	%eax, %r13d
	cmpl	-144(%rbp), %r14d
	je	.L403
	leal	1(%r14), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj@PLT
.L338:
	leal	(%rbx,%r13), %edx
	leaq	-120(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread8GetFrameEi@PLT
	movq	(%r12), %rbx
	movq	-120(%rbp), %r14
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, -152(%rbp)
	call	_ZNK2v88internal4wasm16InterpretedFrame17GetParameterCountEv@PLT
	movq	%r14, %rdi
	movl	%eax, -168(%rbp)
	call	_ZNK2v88internal4wasm16InterpretedFrame13GetLocalCountEv@PLT
	movl	%eax, -164(%rbp)
	testl	%eax, %eax
	jle	.L404
	movq	(%r12), %rdi
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	(%r12), %rdi
	leaq	-112(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rsi
	movq	%rax, -176(%rbp)
	movq	%rax, %r15
	leaq	.LC13(%rip), %rax
	movq	%rcx, -160(%rbp)
	movq	%rax, -112(%rbp)
	movq	$6, -104(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-152(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	leaq	-96(%rbp), %rax
	movq	%rax, -144(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -184(%rbp)
.L364:
	movq	%r14, %rdi
	call	_ZNK2v88internal4wasm16InterpretedFrame8functionEv@PLT
	movl	8(%rax), %r15d
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	movq	23(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L405
.L340:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L347
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	cmpl	%r15d, 11(%rsi)
	jg	.L406
.L351:
	cmpl	%r13d, -168(%rbp)
	movq	(%r12), %r15
	movl	%r13d, %ecx
	movl	$21, %esi
	leaq	.LC11(%rip), %rax
	leaq	.LC12(%rip), %rdx
	movq	$21, -88(%rbp)
	cmovg	%rax, %rdx
	movq	-184(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	testl	%eax, %eax
	jle	.L361
	cmpl	-88(%rbp), %eax
	jge	.L361
	movq	-96(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	%r15, %rdi
	cltq
	movq	%rax, -104(%rbp)
	movq	%rdx, -112(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-144(%rbp), %rdi
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%rax, %r15
	call	_ZNK2v88internal4wasm16InterpretedFrame13GetLocalValueEi@PLT
	movdqa	-96(%rbp), %xmm2
	movzbl	-80(%rbp), %eax
	subq	$32, %rsp
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	movq	(%r12), %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE
	addq	$32, %rsp
	movq	%rax, %rdx
	testq	%r15, %r15
	je	.L407
	.p2align 4,,10
	.p2align 3
.L363:
	movq	-176(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	addl	$1, %r13d
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	cmpl	%r13d, -164(%rbp)
	jne	.L364
.L339:
	movq	%r14, %rdi
	xorl	%r15d, %r15d
	call	_ZNK2v88internal4wasm16InterpretedFrame14GetStackHeightEv@PLT
	movq	(%r12), %rdi
	xorl	%esi, %esi
	movl	%eax, %ebx
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	(%r12), %rdi
	movq	-160(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, %r13
	leaq	.LC14(%rip), %rax
	movq	$5, -104(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-152(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	leaq	-96(%rbp), %rax
	movq	%rax, -144(%rbp)
	testl	%ebx, %ebx
	jle	.L368
	.p2align 4,,10
	.p2align 3
.L365:
	movq	-144(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r14, %rsi
	call	_ZNK2v88internal4wasm16InterpretedFrame13GetStackValueEi@PLT
	movdqa	-96(%rbp), %xmm0
	movzbl	-80(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm0, (%rsp)
	movb	%al, 16(%rsp)
	movq	(%r12), %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE
	movl	%r15d, %esi
	addq	$32, %rsp
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	movq	%r13, %rdi
	addl	$1, %r15d
	call	_ZN2v88internal8JSObject29SetOwnElementIgnoreAttributesENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	cmpl	%r15d, %ebx
	jne	.L365
.L368:
	movq	-120(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L367
	movq	-192(%rbp), %rdi
	call	_ZN2v88internal4wasm23InterpretedFrameDeleterclEPNS1_16InterpretedFrameE@PLT
.L367:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L408
	movq	-152(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L409
.L349:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	cmpl	%r15d, 11(%rsi)
	jle	.L351
.L406:
	leal	16(,%r15,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rsi,%rdx), %rcx
	cmpq	88(%rbx), %rcx
	je	.L351
	movq	(%rax), %rax
	movq	-1(%rdx,%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L353
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L354:
	cmpl	%r13d, 11(%rsi)
	jle	.L351
	leal	16(,%r13,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rsi,%rdx), %rcx
	cmpq	88(%rbx), %rcx
	je	.L351
	movq	(%rax), %rax
	movq	-1(%rdx,%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L395
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L351
.L359:
	movq	-144(%rbp), %rdi
	movl	%r13d, %edx
	movq	%r14, %rsi
	call	_ZNK2v88internal4wasm16InterpretedFrame13GetLocalValueEi@PLT
	movdqa	-96(%rbp), %xmm1
	movzbl	-80(%rbp), %eax
	subq	$32, %rsp
	movb	%al, 16(%rsp)
	movups	%xmm1, (%rsp)
	movq	(%r12), %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_122WasmValueToValueObjectEPNS0_7IsolateENS1_9WasmValueE
	addq	$32, %rsp
	movq	%rax, %rdx
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L405:
	movq	7(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	135(%rax), %r8
	testq	%rdi, %rdi
	je	.L341
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L342:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm16DecodeLocalNamesEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE@PLT
	movq	-136(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	movq	%rdx, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %dl
	je	.L369
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -200(%rbp)
	testl	$262144, %eax
	jne	.L410
	testb	$24, %al
	je	.L369
.L412:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L411
.L369:
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rsi
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%rdx, -224(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-200(%rbp), %rcx
	movq	-224(%rbp), %rdx
	movq	-216(%rbp), %rsi
	movq	-208(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L412
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L407:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L403:
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L341:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L413
.L343:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r8, (%rsi)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L404:
	leaq	-112(%rbp), %rax
	movq	%rax, -160(%rbp)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L409:
	movq	%rbx, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L353:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L414
.L355:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L411:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rsi
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L361:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%rbx, %rdi
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L414:
	movq	%rbx, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L395:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L415
.L360:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L359
.L415:
	movq	%rbx, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L360
.L408:
	call	__stack_chk_fail@PLT
.L401:
	jmp	.L335
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi
	.cfi_startproc
	.type	_ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi.cold, @function
_ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi.cold:
.LFSB20065:
.L335:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	16, %eax
	ud2
	.cfi_endproc
.LFE20065:
	.section	.text._ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi
	.size	_ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi, .-_ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi
	.section	.text.unlikely._ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi
	.size	_ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi.cold, .-_ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi.cold
.LCOLDE15:
	.section	.text._ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi
.LHOTE15:
	.section	.text._ZN2v88internal13WasmDebugInfo13GetCWasmEntryENS0_6HandleIS1_EEPNS0_9SignatureINS0_4wasm9ValueTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13WasmDebugInfo13GetCWasmEntryENS0_6HandleIS1_EEPNS0_9SignatureINS0_4wasm9ValueTypeEEE
	.type	_ZN2v88internal13WasmDebugInfo13GetCWasmEntryENS0_6HandleIS1_EEPNS0_9SignatureINS0_4wasm9ValueTypeEEE, @function
_ZN2v88internal13WasmDebugInfo13GetCWasmEntryENS0_6HandleIS1_EEPNS0_9SignatureINS0_4wasm9ValueTypeEEE:
.LFB20066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	%rdx, %rax
	movq	31(%rdx), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %rbx
	cmpq	-37504(%rax), %rsi
	je	.L482
.L417:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L433
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L434:
	movq	(%r12), %rax
	movq	%r14, %rsi
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal4wasm12SignatureMap4FindERKNS0_9SignatureINS1_9ValueTypeEEE@PLT
	cmpl	$-1, %eax
	je	.L436
	leal	16(,%rax,8), %r12d
	movslq	%r12d, %r12
.L437:
	movq	0(%r13), %rax
	movq	-1(%r12,%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L446
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L483
.L435:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L446:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L484
.L448:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm12SignatureMap12FindOrInsertERKNS0_9SignatureINS1_9ValueTypeEEE@PLT
	movl	%eax, %r15d
	movq	0(%r13), %rax
	cmpl	%r15d, 11(%rax)
	je	.L485
.L438:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17CompileCWasmEntryEPNS0_7IsolateEPNS0_9SignatureINS0_4wasm9ValueTypeEEE@PLT
	testq	%rax, %rax
	je	.L486
	movq	0(%r13), %rdi
	leal	16(,%r15,8), %r12d
	movq	(%rax), %r14
	movslq	%r12d, %r12
	leaq	-1(%rdi,%r12), %r15
	movq	%r14, (%r15)
	testb	$1, %r14b
	je	.L437
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L487
.L444:
	testb	$24, %al
	je	.L437
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L437
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L482:
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%r12), %r15
	movq	(%rax), %r13
	leaq	31(%r15), %rsi
	movq	%r13, 31(%r15)
	testb	$1, %r13b
	je	.L454
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L419
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L419:
	testb	$24, %al
	je	.L454
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L488
	.p2align 4,,10
	.p2align 3
.L454:
	movl	$80, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r15
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%r15)
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r15)
	leaq	72(%r15), %rax
	movups	%xmm0, 16(%r15)
	movups	%xmm0, 48(%r15)
	movq	%rax, 24(%r15)
	movq	$1, 32(%r15)
	movq	$0, 40(%r15)
	movl	$0x3f800000, 56(%r15)
	movq	$0, 64(%r15)
	movq	$0, 72(%r15)
	movq	32(%rbx), %rax
	subq	48(%rbx), %rax
	cmpq	$33554432, %rax
	jg	.L489
.L421:
	movl	$16, %edi
	call	_Znwm@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, %r13
	leaq	16(%r15), %rax
	movq	%rax, 0(%r13)
	leaq	8(%r15), %rax
	movq	%r15, 8(%r13)
	movq	%rax, -56(%rbp)
	je	.L490
	leaq	8(%r15), %rax
	lock addl	$1, (%rax)
.L422:
	movl	$48, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %r9
	movups	%xmm0, 8(%rax)
	movq	%r13, 24(%rax)
	movq	%r9, %rsi
	movq	$0, (%rax)
	leaq	_ZN2v88internal7ManagedINS0_4wasm12SignatureMapEE10DestructorEPv(%rip), %rax
	movq	%rax, 32(%r9)
	movq	$0, 40(%r9)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%rbx), %rdi
	movq	(%rax), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	-56(%rbp), %r9
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	%rax, 40(%r9)
	movq	%r9, %rsi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	-56(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L423
	leaq	8(%r15), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
.L424:
	cmpl	$1, %eax
	je	.L491
.L426:
	movq	(%r12), %r15
	movq	0(%r13), %r13
	movq	%r13, 39(%r15)
	leaq	39(%r15), %rsi
	testb	$1, %r13b
	je	.L451
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L431
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L431:
	testb	$24, %al
	je	.L451
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L492
	.p2align 4,,10
	.p2align 3
.L451:
	movq	(%r12), %rax
	movq	31(%rax), %rsi
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L487:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L489:
	movq	%rbx, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L484:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L485:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$1, %ecx
	movl	%r15d, %edx
	call	_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE@PLT
	movq	(%r12), %rdi
	movq	(%rax), %r12
	movq	%rax, %r13
	leaq	31(%rdi), %rsi
	movq	%r12, 31(%rdi)
	testb	$1, %r12b
	je	.L438
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L440
	movq	%r12, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
.L440:
	testb	$24, %al
	je	.L438
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L438
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L490:
	addl	$1, 8(%r15)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L423:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L492:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L491:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L427
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L428:
	cmpl	$1, %eax
	jne	.L426
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L486:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L427:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L428
	.cfi_endproc
.LFE20066:
	.size	_ZN2v88internal13WasmDebugInfo13GetCWasmEntryENS0_6HandleIS1_EEPNS0_9SignatureINS0_4wasm9ValueTypeEEE, .-_ZN2v88internal13WasmDebugInfo13GetCWasmEntryENS0_6HandleIS1_EEPNS0_9SignatureINS0_4wasm9ValueTypeEEE
	.section	.rodata._ZNSt6vectorISt4pairIjiESaIS1_EE17_M_realloc_insertIJRKjiEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_.str1.1,"aMS",@progbits,1
.LC16:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt4pairIjiESaIS1_EE17_M_realloc_insertIJRKjiEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIjiESaIS1_EE17_M_realloc_insertIJRKjiEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIjiESaIS1_EE17_M_realloc_insertIJRKjiEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorISt4pairIjiESaIS1_EE17_M_realloc_insertIJRKjiEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorISt4pairIjiESaIS1_EE17_M_realloc_insertIJRKjiEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB23376:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %r8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r15
	movq	%r13, %rax
	subq	%r15, %rax
	sarq	$3, %rax
	cmpq	%r8, %rax
	je	.L512
	movq	%rsi, %rbx
	movq	%rdi, %r12
	subq	%r15, %rsi
	testq	%rax, %rax
	je	.L503
	movabsq	$9223372036854775800, %rdi
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L513
.L495:
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rsi
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	leaq	(%rax,%rdi), %rax
	leaq	8(%r14), %r8
.L502:
	movl	(%rcx), %ecx
	movl	(%rdx), %edx
	addq	%r14, %rsi
	movl	%edx, (%rsi)
	movl	%ecx, 4(%rsi)
	cmpq	%r15, %rbx
	je	.L497
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L498:
	movl	(%rdx), %edi
	movl	4(%rdx), %esi
	addq	$8, %rdx
	addq	$8, %rcx
	movl	%edi, -8(%rcx)
	movl	%esi, -4(%rcx)
	cmpq	%rdx, %rbx
	jne	.L498
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	8(%r14,%rdx), %r8
.L497:
	cmpq	%r13, %rbx
	je	.L499
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L500:
	movl	4(%rdx), %esi
	movl	(%rdx), %edi
	addq	$8, %rdx
	addq	$8, %rcx
	movl	%edi, -8(%rcx)
	movl	%esi, -4(%rcx)
	cmpq	%r13, %rdx
	jne	.L500
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L499:
	testq	%r15, %r15
	je	.L501
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L501:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	movq	%rax, 16(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L496
	movl	$8, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L503:
	movl	$8, %edi
	jmp	.L495
.L496:
	cmpq	%r8, %r9
	movq	%r8, %rax
	cmovbe	%r9, %rax
	leaq	0(,%rax,8), %rdi
	jmp	.L495
.L512:
	leaq	.LC16(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23376:
	.size	_ZNSt6vectorISt4pairIjiESaIS1_EE17_M_realloc_insertIJRKjiEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorISt4pairIjiESaIS1_EE17_M_realloc_insertIJRKjiEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text.unlikely._ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm,"ax",@progbits
	.align 2
.LCOLDB17:
	.section	.text._ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm,"ax",@progbits
.LHOTB17:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm
	.type	_ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm, @function
_ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm:
.LFB20061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	xorl	%esi, %esi
	movq	15(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r14
	leaq	16(%r14), %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi@PLT
	movq	104(%r14), %rsi
	xorl	%edx, %edx
	movq	%rax, %r13
	movq	%rbx, %rax
	divq	%rsi
	movq	96(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L515
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L549:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L515
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L548
.L517:
	cmpq	%rdi, %rbx
	jne	.L549
	movl	16(%rcx), %ebx
	movq	120(%r14), %r14
	movq	%r13, %rdi
	movl	%ebx, %esi
	subl	$1, %r14d
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj@PLT
	movl	%eax, %r15d
	cmpl	%r14d, %ebx
	je	.L550
	leal	1(%rbx), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj@PLT
	movl	%eax, %ebx
.L518:
	movq	$0, 16(%r12)
	pxor	%xmm0, %xmm0
	movl	%ebx, %r14d
	movups	%xmm0, (%r12)
	subl	%r15d, %r14d
	jne	.L551
.L519:
	leaq	-68(%rbp), %rax
	leaq	-64(%rbp), %r14
	movq	%rax, -88(%rbp)
	cmpl	%ebx, %r15d
	jnb	.L514
	.p2align 4,,10
	.p2align 3
.L524:
	movq	%r13, %rsi
	movl	%r15d, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread8GetFrameEi@PLT
	movq	-64(%rbp), %rdi
	call	_ZNK2v88internal4wasm16InterpretedFrame2pcEv@PLT
	movq	-64(%rbp), %rdi
	movl	%eax, -68(%rbp)
	call	_ZNK2v88internal4wasm16InterpretedFrame8functionEv@PLT
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L525
	movl	8(%rax), %edx
	movl	-68(%rbp), %eax
	movl	%edx, (%rsi)
	movl	%eax, 4(%rsi)
	addq	$8, 8(%r12)
.L526:
	movq	-64(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L527
	movq	%r14, %rdi
	addl	$1, %r15d
	call	_ZN2v88internal4wasm23InterpretedFrameDeleterclEPNS1_16InterpretedFrameE@PLT
	cmpl	%r15d, %ebx
	jne	.L524
.L514:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L552
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	addl	$1, %r15d
	cmpl	%ebx, %r15d
	jne	.L524
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L525:
	movq	-88(%rbp), %rcx
	leaq	8(%rax), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorISt4pairIjiESaIS1_EE17_M_realloc_insertIJRKjiEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L550:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv@PLT
	movl	%eax, %ebx
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L551:
	movl	%r14d, %r14d
	salq	$3, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	(%r12), %r9
	movq	8(%r12), %rdi
	movq	%rax, %rsi
	movq	%rax, %rcx
	movq	%r9, %rdx
	cmpq	%r9, %rdi
	je	.L523
	.p2align 4,,10
	.p2align 3
.L520:
	movl	(%rdx), %r8d
	movl	4(%rdx), %eax
	addq	$8, %rdx
	addq	$8, %rcx
	movl	%r8d, -8(%rcx)
	movl	%eax, -4(%rcx)
	cmpq	%rdx, %rdi
	jne	.L520
.L523:
	testq	%r9, %r9
	je	.L522
	movq	%r9, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rsi
.L522:
	movq	%rsi, %xmm0
	addq	%rsi, %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 16(%r12)
	movups	%xmm0, (%r12)
	jmp	.L519
.L552:
	call	__stack_chk_fail@PLT
.L548:
	jmp	.L515
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm
	.cfi_startproc
	.type	_ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm.cold, @function
_ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm.cold:
.LFSB20061:
.L515:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	16, %eax
	ud2
	.cfi_endproc
.LFE20061:
	.section	.text._ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm
	.size	_ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm, .-_ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm
	.section	.text.unlikely._ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm
	.size	_ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm.cold, .-_ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm.cold
.LCOLDE17:
	.section	.text._ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm
.LHOTE17:
	.section	.text._ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,"axG",@progbits,_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.type	_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, @function
_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_:
.LFB24012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r9
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r9, %rax
	divq	%r8
	leaq	0(,%rdx,8), %r15
	leaq	0(%r13,%r15), %r14
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L564
	movq	%rdi, %rbx
	movq	(%r12), %rdi
	movq	%rdx, %r11
	movq	%r12, %r10
	movq	8(%rdi), %rcx
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L571:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L564
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rcx, %rax
	divq	%r8
	cmpq	%r11, %rdx
	jne	.L564
	movq	%rsi, %rdi
.L556:
	cmpq	%rcx, %r9
	jne	.L571
	movq	(%rdi), %rcx
	cmpq	%r10, %r12
	je	.L572
	testq	%rcx, %rcx
	je	.L558
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L558
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rcx
.L558:
	movq	%rcx, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L565
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L558
	movq	%r10, 0(%r13,%rdx,8)
	addq	(%rbx), %r15
	movq	(%r15), %rax
	movq	%r15, %r14
.L557:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L573
.L559:
	movq	$0, (%r14)
	movq	(%rdi), %rcx
	jmp	.L558
.L565:
	movq	%r10, %rax
	jmp	.L557
.L573:
	movq	%rcx, 16(%rbx)
	jmp	.L559
	.cfi_endproc
.LFE24012:
	.size	_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, .-_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.section	.text._ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm
	.type	_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm, @function
_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm:
.LFB24358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L575
	movq	(%rbx), %r8
.L576:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L585
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L586:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L599
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L600
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L578:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L580
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L582:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L583:
	testq	%rsi, %rsi
	je	.L580
.L581:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L582
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L588
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L581
	.p2align 4,,10
	.p2align 3
.L580:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r10
	je	.L584
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L584:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L585:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L587
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L587:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L588:
	movq	%rdx, %rdi
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L599:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L578
.L600:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE24358:
	.size	_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm, .-_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm
	.section	.rodata._ZN2v88internal13WasmDebugInfo14RunInterpreterEPNS0_7IsolateENS0_6HandleIS1_EEmiNS0_6VectorINS0_4wasm9WasmValueEEES9_.str1.1,"aMS",@progbits,1
.LC18:
	.string	"unreachable code"
	.section	.text._ZN2v88internal13WasmDebugInfo14RunInterpreterEPNS0_7IsolateENS0_6HandleIS1_EEmiNS0_6VectorINS0_4wasm9WasmValueEEES9_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13WasmDebugInfo14RunInterpreterEPNS0_7IsolateENS0_6HandleIS1_EEmiNS0_6VectorINS0_4wasm9WasmValueEEES9_
	.type	_ZN2v88internal13WasmDebugInfo14RunInterpreterEPNS0_7IsolateENS0_6HandleIS1_EEmiNS0_6VectorINS0_4wasm9WasmValueEEES9_, @function
_ZN2v88internal13WasmDebugInfo14RunInterpreterEPNS0_7IsolateENS0_6HandleIS1_EEmiNS0_6VectorINS0_4wasm9WasmValueEEES9_:
.LFB20060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rdx, -248(%rbp)
	movq	%rax, -288(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12_GLOBAL__N_128GetOrCreateInterpreterHandleEPNS0_7IsolateENS0_6HandleINS0_13WasmDebugInfoEEE
	movq	41112(%r13), %rdi
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L602
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L603:
	movq	8(%rbx), %rax
	salq	$5, %r12
	leaq	16(%rbx), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	136(%rax), %rax
	movq	(%rax,%r12), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread15StartActivationEv@PLT
	movl	$24, %edi
	movl	%eax, -268(%rbp)
	movl	%eax, %r13d
	leaq	96(%rbx), %rax
	movq	%rax, -256(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movl	%r13d, 16(%rdi)
	movq	%rax, 8(%rdi)
	movq	104(%rbx), %rsi
	divq	%rsi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L605
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L674:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L605
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L605
.L607:
	cmpq	%r8, -248(%rbp)
	jne	.L674
	call	_ZdlPv@PLT
.L640:
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi@PLT
	movq	%r15, %rdx
	leaq	-128(%rbp), %r15
	movq	%rax, %r13
	movq	8(%rbx), %rax
	movq	%r13, %rdi
	movq	136(%rax), %rsi
	addq	%r12, %rsi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread9InitFrameEPKNS1_12WasmFunctionEPNS1_9WasmValueE@PLT
.L608:
	movzbl	88(%rbx), %eax
	cmpb	$1, %al
	je	.L609
.L681:
	jg	.L610
	cmpb	$-1, %al
	jne	.L675
.L671:
	movl	$-1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread3RunEi@PLT
	cmpl	$3, %eax
	je	.L618
.L678:
	ja	.L619
	testl	%eax, %eax
	jne	.L676
.L620:
	movq	-248(%rbp), %rax
	xorl	%esi, %esi
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi@PLT
	movl	-268(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread16FinishActivationEj@PLT
	movq	-256(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	call	_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
.L639:
	movq	-264(%rbp), %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L677
	addq	$264, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L613
.L673:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread3RunEi@PLT
	cmpl	$3, %eax
	jne	.L678
.L618:
	movq	-280(%rbp), %rax
	cmpq	$0, (%rax)
	jne	.L679
.L624:
	movq	-248(%rbp), %rax
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	$1, %r12d
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi@PLT
	movl	-268(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread16FinishActivationEj@PLT
	movq	-256(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	call	_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L676:
	cmpl	$2, %eax
	jne	.L613
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	41472(%rax), %rsi
	call	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE@PLT
	movq	(%rbx), %r12
	movq	41472(%r12), %rax
	cmpb	$0, 13(%rax)
	jne	.L680
.L625:
	movzbl	88(%rbx), %eax
	cmpb	$1, %al
	je	.L632
	jg	.L633
	cmpb	$-1, %al
	je	.L631
	testb	%al, %al
	jne	.L613
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv@PLT
	cmpl	92(%rbx), %eax
	setl	%al
.L636:
	testb	%al, %al
	jne	.L635
.L631:
	movq	%r15, %rdi
	call	_ZN2v88internal10DebugScopeD1Ev@PLT
.L683:
	movzbl	88(%rbx), %eax
	cmpb	$1, %al
	jne	.L681
	.p2align 4,,10
	.p2align 3
.L609:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv@PLT
	cmpl	92(%rbx), %eax
	je	.L673
	setle	%sil
	movq	%r13, %rdi
	movzbl	%sil, %esi
	addl	$1, %esi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread13AddBreakFlagsEh@PLT
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L619:
	cmpl	$4, %eax
	jne	.L613
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetTrapReasonEv@PLT
	movl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movl	%eax, %esi
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread14RaiseExceptionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testl	%eax, %eax
	je	.L608
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L675:
	testb	%al, %al
	jne	.L613
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread13AddBreakFlagsEh@PLT
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L602:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L682
.L604:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L633:
	cmpb	$2, %al
	jne	.L613
.L635:
	movq	(%rbx), %rsi
	movb	$-1, 88(%rbx)
	movq	41472(%rsi), %rdi
	addq	$288, %rsi
	call	_ZN2v88internal5Debug12OnDebugBreakENS0_6HandleINS0_10FixedArrayEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal10DebugScopeD1Ev@PLT
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L605:
	movq	%rdi, %rcx
	movq	-248(%rbp), %rdx
	movq	-256(%rbp), %rdi
	movq	%r9, %rsi
	movl	$1, %r8d
	call	_ZNSt10_HashtableImSt4pairIKmjESaIS2_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_117InterpreterHandle17GetInstanceObjectEv.isra.0
	movq	41112(%r12), %rdi
	movq	(%rax), %rax
	movq	135(%rax), %rsi
	testq	%rdi, %rdi
	je	.L626
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L627:
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r8, -296(%rbp)
	call	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv@PLT
	leaq	-208(%rbp), %r9
	movq	%r12, %rsi
	leal	-1(%rax), %edx
	movq	%r9, %rdi
	movq	%r9, -304(%rbp)
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread8GetFrameEi@PLT
	movq	-296(%rbp), %r8
	movq	-208(%rbp), %rdi
	movq	(%r8), %rax
	movq	%rax, -200(%rbp)
	call	_ZNK2v88internal4wasm16InterpretedFrame8functionEv@PLT
	leaq	-200(%rbp), %rdi
	movl	8(%rax), %esi
	call	_ZN2v88internal16WasmModuleObject17GetFunctionOffsetEj@PLT
	movq	-208(%rbp), %rdi
	movl	%eax, %r12d
	call	_ZNK2v88internal4wasm16InterpretedFrame2pcEv@PLT
	movq	-208(%rbp), %rsi
	movq	-296(%rbp), %r8
	addl	%eax, %r12d
	testq	%rsi, %rsi
	je	.L629
	movq	-304(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal4wasm23InterpretedFrameDeleterclEPNS1_16InterpretedFrameE@PLT
	movq	-296(%rbp), %r8
.L629:
	movq	(%rbx), %rdi
	movq	%r8, %rsi
	movl	%r12d, %edx
	call	_ZN2v88internal16WasmModuleObject16CheckBreakPointsEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L625
	movq	(%rbx), %rax
	movb	$-1, 88(%rbx)
	movq	41472(%rax), %rdi
	call	_ZN2v88internal5Debug12OnDebugBreakENS0_6HandleINS0_10FixedArrayEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal10DebugScopeD1Ev@PLT
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L679:
	leaq	-240(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread14GetReturnValueEi@PLT
	movq	-288(%rbp), %rcx
	movdqu	-240(%rbp), %xmm0
	movups	%xmm0, (%rcx)
	movzbl	-224(%rbp), %eax
	movb	%al, 16(%rcx)
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L632:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv@PLT
	cmpl	%eax, 92(%rbx)
	sete	%al
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L682:
	movq	%r13, %rdi
	movq	%rsi, -256(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L626:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L684
.L628:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L684:
	movq	%r12, %rdi
	movq	%rsi, -296(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-296(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L628
.L613:
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L677:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20060:
	.size	_ZN2v88internal13WasmDebugInfo14RunInterpreterEPNS0_7IsolateENS0_6HandleIS1_EEmiNS0_6VectorINS0_4wasm9WasmValueEEES9_, .-_ZN2v88internal13WasmDebugInfo14RunInterpreterEPNS0_7IsolateENS0_6HandleIS1_EEmiNS0_6VectorINS0_4wasm9WasmValueEEES9_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE, @function
_GLOBAL__sub_I__ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE:
.LFB25047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25047:
	.size	_GLOBAL__sub_I__ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE, .-_GLOBAL__sub_I__ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE,"aw"
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_117InterpreterHandleESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12SignatureMapESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
