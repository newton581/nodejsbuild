	.file	"v8-console-message.cc"
	.text
	.section	.text._ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE
	.type	_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE, @function
_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE:
.LFB4276:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4276:
	.size	_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE, .-_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE
	.section	.text._ZN12v8_inspector17V8InspectorClient13currentTimeMSEv,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv
	.type	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv, @function
_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv:
.LFB4282:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE4282:
	.size	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv, .-_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB7707:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE7707:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB7708:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7708:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS1_16V8ConsoleMessageESt14default_deleteIS4_EEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS1_16V8ConsoleMessageESt14default_deleteIS4_EEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS1_16V8ConsoleMessageESt14default_deleteIS4_EEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation:
.LFB10819:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L7
	cmpl	$3, %edx
	je	.L8
	cmpl	$1, %edx
	je	.L12
.L8:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE10819:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS1_16V8ConsoleMessageESt14default_deleteIS4_EEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS1_16V8ConsoleMessageESt14default_deleteIS4_EEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector23V8ConsoleMessageStorage5clearEvEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector23V8ConsoleMessageStorage5clearEvEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector23V8ConsoleMessageStorage5clearEvEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation:
.LFB10832:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE10832:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector23V8ConsoleMessageStorage5clearEvEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector23V8ConsoleMessageStorage5clearEvEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.section	.text._ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Console14ConsoleMessageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev, @function
_ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev:
.LFB6146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	136(%rdi), %rdi
	leaq	152(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	88(%rbx), %rdi
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L16
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6146:
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev, .-_ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev
	.weak	_ZN12v8_inspector8protocol7Console14ConsoleMessageD1Ev
	.set	_ZN12v8_inspector8protocol7Console14ConsoleMessageD1Ev,_ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev:
.LFB5241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L22
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5241:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev:
.LFB5754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L26
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5754:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev,_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.section	.text._ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv:
.LFB6165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L31
	movq	(%rdi), %rax
	call	*24(%rax)
.L31:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L38:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6165:
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv:
.LFB6164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L39
	movq	(%rdi), %rax
	call	*24(%rax)
.L39:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L46:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6164:
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv:
.LFB5250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L47
	movq	(%rdi), %rax
	call	*24(%rax)
.L47:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L54:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5250:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv:
.LFB5249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L55
	movq	(%rdi), %rax
	call	*24(%rax)
.L55:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L62
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L62:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5249:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv:
.LFB5296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L63
	movq	(%rdi), %rax
	call	*24(%rax)
.L63:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L70:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5296:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv:
.LFB5295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L71
	movq	(%rdi), %rax
	call	*24(%rax)
.L71:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L78:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5295:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv:
.LFB5397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L79
	movq	(%rdi), %rax
	call	*24(%rax)
.L79:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L86:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5397:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv:
.LFB5396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L87
	movq	(%rdi), %rax
	call	*24(%rax)
.L87:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L94:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5396:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv:
.LFB5365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L95
	movq	(%rdi), %rax
	call	*24(%rax)
.L95:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L102:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5365:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv:
.LFB5364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L103
	movq	(%rdi), %rax
	call	*24(%rax)
.L103:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L110:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5364:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv:
.LFB5699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime16ExceptionDetails7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L111
	movq	(%rdi), %rax
	call	*24(%rax)
.L111:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L118:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5699:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv:
.LFB5698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime16ExceptionDetails7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L119
	movq	(%rdi), %rax
	call	*24(%rax)
.L119:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5698:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv:
.LFB5768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L127
	movq	(%rdi), %rax
	call	*24(%rax)
.L127:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L134
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L134:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5768:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv:
.LFB5767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L135
	movq	(%rdi), %rax
	call	*24(%rax)
.L135:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L142
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L142:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5767:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.section	.text._ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_6StringEEE, @function
_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_6StringEEE:
.LFB7916:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$64, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L143
	movl	$1, %eax
	testq	%r12, %r12
	je	.L143
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %r13
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	leaq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L145
	call	_ZdlPv@PLT
.L145:
	movl	$1, %eax
.L143:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L151
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L151:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7916:
	.size	_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_6StringEEE, .-_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_6StringEEE
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS0_16V8ConsoleMessageESt14default_deleteIS6_EEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS0_16V8ConsoleMessageESt14default_deleteIS6_EEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS0_16V8ConsoleMessageESt14default_deleteIS6_EEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB10817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	(%rsi), %r12
	movq	(%rax), %rsi
	movl	(%rsi), %eax
	testl	%eax, %eax
	je	.L155
.L153:
	movq	184(%r12), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector18V8RuntimeAgentImpl12messageAddedEPNS_16V8ConsoleMessageE@PLT
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	216(%r12), %rdi
	call	_ZN12v8_inspector18V8ConsoleAgentImpl12messageAddedEPNS_16V8ConsoleMessageE@PLT
	movq	(%rbx), %rax
	movq	(%rax), %rsi
	jmp	.L153
	.cfi_endproc
.LFE10817:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS0_16V8ConsoleMessageESt14default_deleteIS6_EEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS0_16V8ConsoleMessageESt14default_deleteIS6_EEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.rodata._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_23V8ConsoleMessageStorage5clearEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"console"
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_23V8ConsoleMessageStorage5clearEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_23V8ConsoleMessageStorage5clearEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_23V8ConsoleMessageStorage5clearEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB10831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$48, %rsp
	movq	(%rsi), %r13
	leaq	.LC1(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_8String16E@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L156
	call	_ZdlPv@PLT
.L156:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L160
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L160:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10831:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_23V8ConsoleMessageStorage5clearEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_23V8ConsoleMessageStorage5clearEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev:
.LFB5243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L162
	call	_ZdlPv@PLT
.L162:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L163
	call	_ZdlPv@PLT
.L163:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5243:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev:
.LFB5756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L166
	call	_ZdlPv@PLT
.L166:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L167
	call	_ZdlPv@PLT
.L167:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L168
	call	_ZdlPv@PLT
.L168:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5756:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev:
.LFB5874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	80(%r12), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	64(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L171
	call	_ZdlPv@PLT
.L171:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5874:
	.size	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, .-_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.section	.text._ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Console14ConsoleMessageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev, @function
_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev:
.LFB6148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	136(%rdi), %rdi
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L175
	call	_ZdlPv@PLT
.L175:
	movq	88(%r12), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L176
	call	_ZdlPv@PLT
.L176:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L177
	call	_ZdlPv@PLT
.L177:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L178
	call	_ZdlPv@PLT
.L178:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6148:
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev, .-_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev:
.LFB5275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	160(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L181
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -64(%rbp)
	cmpq	%r12, %rax
	je	.L182
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r14
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L237:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L185
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L186
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L185:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L187
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L188
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L187:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L183:
	addq	$8, %r12
	cmpq	%r12, -64(%rbp)
	je	.L236
.L189:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L183
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L237
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -64(%rbp)
	jne	.L189
	.p2align 4,,10
	.p2align 3
.L236:
	movq	0(%r13), %r12
.L182:
	testq	%r12, %r12
	je	.L190
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L190:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L181:
	movq	152(%rbx), %r13
	testq	%r13, %r13
	je	.L191
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	jne	.L201
	testq	%r12, %r12
	je	.L202
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L202:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L191:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L203
	call	_ZdlPv@PLT
.L203:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L204
	call	_ZdlPv@PLT
.L204:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L180
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L195
	call	_ZdlPv@PLT
.L195:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L196
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L197
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L196:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L198
	call	_ZdlPv@PLT
.L198:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L199
	call	_ZdlPv@PLT
.L199:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L193:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L238
.L201:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L193
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L239
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L201
	.p2align 4,,10
	.p2align 3
.L238:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L240
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L180:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	call	*%rax
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L186:
	call	*%rax
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L197:
	call	*%rax
	jmp	.L196
	.cfi_endproc
.LFE5275:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev:
.LFB5388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L242
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L243
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L242:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L241
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L245
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	popq	%rbx
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	call	*%rax
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L245:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE5388:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev:
.LFB5348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L251
	call	_ZdlPv@PLT
.L251:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L252
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L253
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L252:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L254
	call	_ZdlPv@PLT
.L254:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L255
	call	_ZdlPv@PLT
.L255:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L250
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	call	*%rax
	jmp	.L252
	.cfi_endproc
.LFE5348:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev:
.LFB5390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L262
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L263
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L262:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L264
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L265
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L264:
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	call	*%rax
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L265:
	call	*%rax
	jmp	.L264
	.cfi_endproc
.LFE5390:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev:
.LFB5350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L274
	call	_ZdlPv@PLT
.L274:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L275
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L276
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L275:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L277
	call	_ZdlPv@PLT
.L277:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L278
	call	_ZdlPv@PLT
.L278:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L279
	call	_ZdlPv@PLT
.L279:
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	call	*%rax
	jmp	.L275
	.cfi_endproc
.LFE5350:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.section	.text._ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0, @function
_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0:
.LFB14627:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %rax
	movq	%rdi, -136(%rbp)
	movq	%rbx, -168(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rbx
	je	.L285
	.p2align 4,,10
	.p2align 3
.L801:
	movq	-64(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -56(%rbp)
	testq	%rcx, %rcx
	je	.L286
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L287
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	movq	312(%rcx), %r12
	leaq	-64(%rax), %rbx
	movq	%rax, %xmm1
	movq	%rbx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rcx)
	testq	%r12, %r12
	je	.L288
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L289
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L290
	call	_ZdlPv@PLT
.L290:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L291
	call	_ZdlPv@PLT
.L291:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L288:
	movq	-56(%rbp), %rax
	movq	304(%rax), %rdx
	movq	%rdx, -80(%rbp)
	testq	%rdx, %rdx
	je	.L292
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L293
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L294
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -184(%rbp)
	movq	%rax, -96(%rbp)
	cmpq	%rax, %rsi
	je	.L295
	.p2align 4,,10
	.p2align 3
.L652:
	movq	-96(%rbp), %rax
	movq	(%rax), %rdx
	movq	%rdx, -120(%rbp)
	testq	%rdx, %rdx
	je	.L296
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L297
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	16(%rdx), %rdx
	movq	%rdx, -72(%rbp)
	testq	%rdx, %rdx
	je	.L298
	movq	(%rdx), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L299
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L300
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -104(%rbp)
	cmpq	%rax, %rcx
	je	.L301
	.p2align 4,,10
	.p2align 3
.L433:
	movq	-104(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -128(%rbp)
	testq	%rcx, %rcx
	je	.L302
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L303
	movq	16(%rcx), %rdx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	%rdx, -88(%rbp)
	testq	%rdx, %rdx
	je	.L304
	movq	(%rdx), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L305
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L306
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -200(%rbp)
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rcx
	je	.L307
	.p2align 4,,10
	.p2align 3
.L364:
	movq	-112(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L308
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L309
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L310
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L311
	movq	160(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L312
	movq	8(%r15), %rax
	movq	(%r15), %r14
	cmpq	%r14, %rax
	je	.L313
	movq	%r12, -208(%rbp)
	movq	%rax, %r12
	movq	%r13, -176(%rbp)
	movq	%r15, -216(%rbp)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	16(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L316
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L317
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L316:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L318
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L319
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L318:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L314:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1400
.L320:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L314
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1401
	addq	$8, %r14
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r14, %r12
	jne	.L320
	.p2align 4,,10
	.p2align 3
.L1400:
	movq	-216(%rbp), %r15
	movq	-176(%rbp), %r13
	movq	-208(%rbp), %r12
	movq	(%r15), %r14
.L313:
	testq	%r14, %r14
	je	.L321
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L321:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L312:
	movq	152(%r12), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L322
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L323
	movq	%r13, -208(%rbp)
	movq	%r12, -216(%rbp)
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L1403:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L327
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L328
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L327:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L329
	call	_ZdlPv@PLT
.L329:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L330
	call	_ZdlPv@PLT
.L330:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L331
	call	_ZdlPv@PLT
.L331:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L324:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1402
.L332:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L324
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1403
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L332
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	-176(%rbp), %rax
	movq	-208(%rbp), %r13
	movq	-216(%rbp), %r12
	movq	(%rax), %r14
.L323:
	testq	%r14, %r14
	je	.L333
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L333:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L322:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L334
	call	_ZdlPv@PLT
.L334:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L335
	call	_ZdlPv@PLT
.L335:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L336
	call	_ZdlPv@PLT
.L336:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L310:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L337
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L338
	movq	160(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L339
	movq	8(%r15), %rax
	movq	(%r15), %r14
	cmpq	%r14, %rax
	je	.L340
	movq	%r12, -208(%rbp)
	movq	%rax, %r12
	movq	%r13, -176(%rbp)
	movq	%r15, -216(%rbp)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	16(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L343
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L344
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L343:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L345
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L346
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L345:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L341:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1404
.L347:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L341
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1405
	addq	$8, %r14
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r14, %r12
	jne	.L347
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	-216(%rbp), %r15
	movq	-176(%rbp), %r13
	movq	-208(%rbp), %r12
	movq	(%r15), %r14
.L340:
	testq	%r14, %r14
	je	.L348
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L348:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L339:
	movq	152(%r12), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L349
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L350
	movq	%r13, -208(%rbp)
	movq	%r12, -216(%rbp)
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L1407:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L353
	call	_ZdlPv@PLT
.L353:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L354
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L355
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L354:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L356
	call	_ZdlPv@PLT
.L356:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L357
	call	_ZdlPv@PLT
.L357:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L358
	call	_ZdlPv@PLT
.L358:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L351:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1406
.L359:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L351
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1407
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L359
	.p2align 4,,10
	.p2align 3
.L1406:
	movq	-176(%rbp), %rax
	movq	-208(%rbp), %r13
	movq	-216(%rbp), %r12
	movq	(%rax), %r14
.L350:
	testq	%r14, %r14
	je	.L360
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L360:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L349:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L361
	call	_ZdlPv@PLT
.L361:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L362
	call	_ZdlPv@PLT
.L362:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L363
	call	_ZdlPv@PLT
.L363:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L337:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L308:
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rax, -200(%rbp)
	jne	.L364
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
.L307:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L365
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L365:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L306:
	movq	-88(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L366
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -200(%rbp)
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rsi
	je	.L367
	.p2align 4,,10
	.p2align 3
.L401:
	movq	-112(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L368
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L369
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L370
	call	_ZdlPv@PLT
.L370:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L371
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L372
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L373
	movq	8(%r15), %rax
	movq	(%r15), %r14
	cmpq	%r14, %rax
	je	.L374
	movq	%r12, -176(%rbp)
	movq	%rax, %r12
	movq	%r13, -208(%rbp)
	movq	%r15, -216(%rbp)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L1409:
	movq	16(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L377
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L378
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L377:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L379
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L380
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L379:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L375:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1408
.L381:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L375
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1409
	addq	$8, %r14
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r14, %r12
	jne	.L381
	.p2align 4,,10
	.p2align 3
.L1408:
	movq	-216(%rbp), %r15
	movq	-176(%rbp), %r12
	movq	-208(%rbp), %r13
	movq	(%r15), %r14
.L374:
	testq	%r14, %r14
	je	.L382
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L382:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L373:
	movq	152(%r13), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L383
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L384
	movq	%r12, -208(%rbp)
	movq	%r13, -216(%rbp)
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L1411:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L387
	call	_ZdlPv@PLT
.L387:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L388
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L389
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L388:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L390
	call	_ZdlPv@PLT
.L390:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L391
	call	_ZdlPv@PLT
.L391:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L392
	call	_ZdlPv@PLT
.L392:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L385:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1410
.L393:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L385
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1411
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L393
	.p2align 4,,10
	.p2align 3
.L1410:
	movq	-176(%rbp), %rax
	movq	-208(%rbp), %r12
	movq	-216(%rbp), %r13
	movq	(%rax), %r14
.L384:
	testq	%r14, %r14
	je	.L394
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L394:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L383:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L395
	call	_ZdlPv@PLT
.L395:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L396
	call	_ZdlPv@PLT
.L396:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L397
	call	_ZdlPv@PLT
.L397:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L371:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L398
	call	_ZdlPv@PLT
.L398:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L399
	call	_ZdlPv@PLT
.L399:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L400
	call	_ZdlPv@PLT
.L400:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L368:
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rax, -200(%rbp)
	jne	.L401
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
.L367:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L402
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L402:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L366:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L403
	call	_ZdlPv@PLT
.L403:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L404
	call	_ZdlPv@PLT
.L404:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L405
	call	_ZdlPv@PLT
.L405:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L304:
	movq	-128(%rbp), %rax
	movq	8(%rax), %r12
	testq	%r12, %r12
	je	.L406
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L407
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L408
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -88(%rbp)
	cmpq	%r13, %rax
	je	.L409
	movq	%r12, -112(%rbp)
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L1413:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L412
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L413
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L412:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L414
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L415
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L414:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L410:
	addq	$8, %r13
	cmpq	%r13, -88(%rbp)
	je	.L1412
.L416:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L410
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1413
	movq	%r15, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -88(%rbp)
	jne	.L416
	.p2align 4,,10
	.p2align 3
.L1412:
	movq	-112(%rbp), %r12
	movq	(%r14), %r13
.L409:
	testq	%r13, %r13
	je	.L417
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L417:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L408:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L418
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -88(%rbp)
	cmpq	%r13, %rax
	je	.L419
	movq	%r12, -112(%rbp)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L1415:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L422
	call	_ZdlPv@PLT
.L422:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L423
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L424
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L423:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L425
	call	_ZdlPv@PLT
.L425:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L426
	call	_ZdlPv@PLT
.L426:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L427
	call	_ZdlPv@PLT
.L427:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L420:
	addq	$8, %r13
	cmpq	%r13, -88(%rbp)
	je	.L1414
.L428:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L420
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1415
	movq	%r15, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -88(%rbp)
	jne	.L428
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	-112(%rbp), %r12
	movq	(%r14), %r13
.L419:
	testq	%r13, %r13
	je	.L429
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L429:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L418:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L430
	call	_ZdlPv@PLT
.L430:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L431
	call	_ZdlPv@PLT
.L431:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L432
	call	_ZdlPv@PLT
.L432:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L406:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L302:
	addq	$8, -104(%rbp)
	movq	-104(%rbp), %rax
	cmpq	%rax, -192(%rbp)
	jne	.L433
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
.L301:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L434
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L434:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L300:
	movq	-72(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L435
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -128(%rbp)
	movq	%rax, -88(%rbp)
	cmpq	%rax, %rsi
	je	.L436
	.p2align 4,,10
	.p2align 3
.L470:
	movq	-88(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L437
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L438
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L439
	call	_ZdlPv@PLT
.L439:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L440
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L441
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L442
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -152(%rbp)
	cmpq	%r14, %rdx
	je	.L443
	movq	%r12, -160(%rbp)
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L1417:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L446
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L447
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L446:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L448
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L449
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L448:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L444:
	addq	$8, %r14
	cmpq	%r14, -152(%rbp)
	je	.L1416
.L450:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L444
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1417
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -152(%rbp)
	jne	.L450
	.p2align 4,,10
	.p2align 3
.L1416:
	movq	-112(%rbp), %rax
	movq	-160(%rbp), %r12
	movq	(%rax), %r14
.L443:
	testq	%r14, %r14
	je	.L451
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L451:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L442:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L452
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -152(%rbp)
	cmpq	%r14, %rcx
	je	.L453
	movq	%r12, -160(%rbp)
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L1419:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L456
	call	_ZdlPv@PLT
.L456:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L457
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L458
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L457:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L459
	call	_ZdlPv@PLT
.L459:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L460
	call	_ZdlPv@PLT
.L460:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L461
	call	_ZdlPv@PLT
.L461:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L454:
	addq	$8, %r14
	cmpq	%r14, -152(%rbp)
	je	.L1418
.L462:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L454
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1419
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -152(%rbp)
	jne	.L462
	.p2align 4,,10
	.p2align 3
.L1418:
	movq	-112(%rbp), %rax
	movq	-160(%rbp), %r12
	movq	(%rax), %r14
.L453:
	testq	%r14, %r14
	je	.L463
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L463:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L452:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L464
	call	_ZdlPv@PLT
.L464:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L465
	call	_ZdlPv@PLT
.L465:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L466
	call	_ZdlPv@PLT
.L466:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L440:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L467
	call	_ZdlPv@PLT
.L467:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L468
	call	_ZdlPv@PLT
.L468:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L469
	call	_ZdlPv@PLT
.L469:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L437:
	addq	$8, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	jne	.L470
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
.L436:
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L471
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L471:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L435:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L472
	call	_ZdlPv@PLT
.L472:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L473
	call	_ZdlPv@PLT
.L473:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L474
	call	_ZdlPv@PLT
.L474:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L298:
	movq	-120(%rbp), %rax
	movq	8(%rax), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L475
	movq	(%rcx), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L476
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L477
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -192(%rbp)
	movq	%rax, -104(%rbp)
	cmpq	%rax, %rdx
	je	.L478
	.p2align 4,,10
	.p2align 3
.L610:
	movq	-104(%rbp), %rax
	movq	(%rax), %rdx
	movq	%rdx, -128(%rbp)
	testq	%rdx, %rdx
	je	.L479
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L480
	movq	16(%rdx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	testq	%r12, %r12
	je	.L481
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L482
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L483
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -88(%rbp)
	cmpq	%r13, %rax
	je	.L484
	movq	%r12, -112(%rbp)
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L1421:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L487
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L488
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L487:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L489
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L490
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L489:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L485:
	addq	$8, %r13
	cmpq	%r13, -88(%rbp)
	je	.L1420
.L491:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L485
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1421
	movq	%r15, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -88(%rbp)
	jne	.L491
	.p2align 4,,10
	.p2align 3
.L1420:
	movq	-112(%rbp), %r12
	movq	(%r14), %r13
.L484:
	testq	%r13, %r13
	je	.L492
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L492:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L483:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L493
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -88(%rbp)
	cmpq	%r13, %rax
	je	.L494
	movq	%r12, -112(%rbp)
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L1423:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L497
	call	_ZdlPv@PLT
.L497:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L498
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L499
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L498:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L500
	call	_ZdlPv@PLT
.L500:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L501
	call	_ZdlPv@PLT
.L501:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L502
	call	_ZdlPv@PLT
.L502:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L495:
	addq	$8, %r13
	cmpq	%r13, -88(%rbp)
	je	.L1422
.L503:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L495
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1423
	movq	%r15, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -88(%rbp)
	jne	.L503
	.p2align 4,,10
	.p2align 3
.L1422:
	movq	-112(%rbp), %r12
	movq	(%r14), %r13
.L494:
	testq	%r13, %r13
	je	.L504
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L504:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L493:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L505
	call	_ZdlPv@PLT
.L505:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L506
	call	_ZdlPv@PLT
.L506:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L507
	call	_ZdlPv@PLT
.L507:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L481:
	movq	-128(%rbp), %rax
	movq	8(%rax), %rcx
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L508
	movq	(%rcx), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L509
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L510
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -200(%rbp)
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rsi
	je	.L511
	.p2align 4,,10
	.p2align 3
.L568:
	movq	-112(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L512
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L513
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L514
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L515
	movq	160(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L516
	movq	8(%r15), %rax
	movq	(%r15), %r14
	cmpq	%r14, %rax
	je	.L517
	movq	%r12, -208(%rbp)
	movq	%rax, %r12
	movq	%r13, -176(%rbp)
	movq	%r15, -216(%rbp)
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L1425:
	movq	16(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L520
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L521
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L520:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L522
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L523
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L522:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L518:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1424
.L524:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L518
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1425
	addq	$8, %r14
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r14, %r12
	jne	.L524
	.p2align 4,,10
	.p2align 3
.L1424:
	movq	-216(%rbp), %r15
	movq	-176(%rbp), %r13
	movq	-208(%rbp), %r12
	movq	(%r15), %r14
.L517:
	testq	%r14, %r14
	je	.L525
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L525:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L516:
	movq	152(%r12), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L526
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L527
	movq	%r13, -208(%rbp)
	movq	%r12, -216(%rbp)
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L1427:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L530
	call	_ZdlPv@PLT
.L530:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L531
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L532
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L531:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L533
	call	_ZdlPv@PLT
.L533:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L534
	call	_ZdlPv@PLT
.L534:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L535
	call	_ZdlPv@PLT
.L535:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L528:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1426
.L536:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L528
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1427
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L536
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	-176(%rbp), %rax
	movq	-208(%rbp), %r13
	movq	-216(%rbp), %r12
	movq	(%rax), %r14
.L527:
	testq	%r14, %r14
	je	.L537
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L537:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L526:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L538
	call	_ZdlPv@PLT
.L538:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L539
	call	_ZdlPv@PLT
.L539:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L540
	call	_ZdlPv@PLT
.L540:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L514:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L541
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L542
	movq	160(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L543
	movq	8(%r15), %rax
	movq	(%r15), %r14
	cmpq	%r14, %rax
	je	.L544
	movq	%r12, -208(%rbp)
	movq	%rax, %r12
	movq	%r13, -176(%rbp)
	movq	%r15, -216(%rbp)
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L1429:
	movq	16(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L547
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L548
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L547:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L549
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L550
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L549:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L545:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1428
.L551:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L545
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1429
	addq	$8, %r14
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r14, %r12
	jne	.L551
	.p2align 4,,10
	.p2align 3
.L1428:
	movq	-216(%rbp), %r15
	movq	-176(%rbp), %r13
	movq	-208(%rbp), %r12
	movq	(%r15), %r14
.L544:
	testq	%r14, %r14
	je	.L552
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L552:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L543:
	movq	152(%r12), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L553
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L554
	movq	%r13, -208(%rbp)
	movq	%r12, -216(%rbp)
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L1431:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L557
	call	_ZdlPv@PLT
.L557:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L558
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L559
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L558:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L560
	call	_ZdlPv@PLT
.L560:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L561
	call	_ZdlPv@PLT
.L561:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L562
	call	_ZdlPv@PLT
.L562:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L555:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1430
.L563:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L555
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1431
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L563
	.p2align 4,,10
	.p2align 3
.L1430:
	movq	-176(%rbp), %rax
	movq	-208(%rbp), %r13
	movq	-216(%rbp), %r12
	movq	(%rax), %r14
.L554:
	testq	%r14, %r14
	je	.L564
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L564:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L553:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L565
	call	_ZdlPv@PLT
.L565:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L566
	call	_ZdlPv@PLT
.L566:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L567
	call	_ZdlPv@PLT
.L567:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L541:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L512:
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rax, -200(%rbp)
	jne	.L568
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
.L511:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L569
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L569:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L510:
	movq	-88(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L570
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -200(%rbp)
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rdx
	je	.L571
	.p2align 4,,10
	.p2align 3
.L605:
	movq	-112(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L572
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L573
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L574
	call	_ZdlPv@PLT
.L574:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L575
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L576
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L577
	movq	8(%r15), %rax
	movq	(%r15), %r14
	cmpq	%r14, %rax
	je	.L578
	movq	%r12, -176(%rbp)
	movq	%rax, %r12
	movq	%r13, -208(%rbp)
	movq	%r15, -216(%rbp)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L1433:
	movq	16(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L581
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L582
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L581:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L583
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L584
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L583:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L579:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1432
.L585:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L579
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1433
	addq	$8, %r14
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r14, %r12
	jne	.L585
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	-216(%rbp), %r15
	movq	-176(%rbp), %r12
	movq	-208(%rbp), %r13
	movq	(%r15), %r14
.L578:
	testq	%r14, %r14
	je	.L586
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L586:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L577:
	movq	152(%r13), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L587
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L588
	movq	%r12, -208(%rbp)
	movq	%r13, -216(%rbp)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L1435:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L591
	call	_ZdlPv@PLT
.L591:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L592
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L593
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L592:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L594
	call	_ZdlPv@PLT
.L594:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L595
	call	_ZdlPv@PLT
.L595:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L596
	call	_ZdlPv@PLT
.L596:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L589:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1434
.L597:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L589
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1435
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L597
	.p2align 4,,10
	.p2align 3
.L1434:
	movq	-176(%rbp), %rax
	movq	-208(%rbp), %r12
	movq	-216(%rbp), %r13
	movq	(%rax), %r14
.L588:
	testq	%r14, %r14
	je	.L598
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L598:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L587:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L599
	call	_ZdlPv@PLT
.L599:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L600
	call	_ZdlPv@PLT
.L600:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L601
	call	_ZdlPv@PLT
.L601:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L575:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L602
	call	_ZdlPv@PLT
.L602:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L603
	call	_ZdlPv@PLT
.L603:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L604
	call	_ZdlPv@PLT
.L604:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L572:
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rax, -200(%rbp)
	jne	.L605
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
.L571:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L606
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L606:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L570:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L607
	call	_ZdlPv@PLT
.L607:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L608
	call	_ZdlPv@PLT
.L608:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L609
	call	_ZdlPv@PLT
.L609:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L508:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L479:
	addq	$8, -104(%rbp)
	movq	-104(%rbp), %rax
	cmpq	%rax, -192(%rbp)
	jne	.L610
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
.L478:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L611
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L611:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L477:
	movq	-72(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L612
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -128(%rbp)
	movq	%rax, -88(%rbp)
	cmpq	%rax, %rcx
	je	.L613
	.p2align 4,,10
	.p2align 3
.L647:
	movq	-88(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L614
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L615
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L616
	call	_ZdlPv@PLT
.L616:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L617
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L618
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L619
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -152(%rbp)
	cmpq	%r14, %rcx
	je	.L620
	movq	%r12, -160(%rbp)
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L623
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L624
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L623:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L625
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L626
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L625:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L621:
	addq	$8, %r14
	cmpq	%r14, -152(%rbp)
	je	.L1436
.L627:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L621
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1437
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -152(%rbp)
	jne	.L627
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	-112(%rbp), %rax
	movq	-160(%rbp), %r12
	movq	(%rax), %r14
.L620:
	testq	%r14, %r14
	je	.L628
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L628:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L619:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L629
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	movq	%rsi, -152(%rbp)
	cmpq	%r14, %rsi
	je	.L630
	movq	%r12, -160(%rbp)
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L1439:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L633
	call	_ZdlPv@PLT
.L633:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L634
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L635
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L634:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L636
	call	_ZdlPv@PLT
.L636:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L637
	call	_ZdlPv@PLT
.L637:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L638
	call	_ZdlPv@PLT
.L638:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L631:
	addq	$8, %r14
	cmpq	%r14, -152(%rbp)
	je	.L1438
.L639:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L631
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1439
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -152(%rbp)
	jne	.L639
	.p2align 4,,10
	.p2align 3
.L1438:
	movq	-112(%rbp), %rax
	movq	-160(%rbp), %r12
	movq	(%rax), %r14
.L630:
	testq	%r14, %r14
	je	.L640
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L640:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L629:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L641
	call	_ZdlPv@PLT
.L641:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L642
	call	_ZdlPv@PLT
.L642:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L643
	call	_ZdlPv@PLT
.L643:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L617:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L644
	call	_ZdlPv@PLT
.L644:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L645
	call	_ZdlPv@PLT
.L645:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L646
	call	_ZdlPv@PLT
.L646:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L614:
	addq	$8, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	jne	.L647
.L1454:
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
.L613:
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L648
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L648:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L612:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L649
	call	_ZdlPv@PLT
.L649:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L650
	call	_ZdlPv@PLT
.L650:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L651
	call	_ZdlPv@PLT
.L651:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L475:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L296:
	addq	$8, -96(%rbp)
	movq	-96(%rbp), %rax
	cmpq	%rax, -184(%rbp)
	jne	.L652
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
.L295:
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L653
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L653:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L294:
	movq	-80(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L654
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -176(%rbp)
	movq	%rax, -96(%rbp)
	cmpq	%rax, %rsi
	je	.L655
	.p2align 4,,10
	.p2align 3
.L789:
	movq	-96(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L656
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L657
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rcx), %rdi
	movq	%rax, (%rcx)
	leaq	168(%rcx), %rax
	cmpq	%rax, %rdi
	je	.L658
	call	_ZdlPv@PLT
.L658:
	movq	-72(%rbp), %rax
	movq	136(%rax), %rdx
	movq	%rdx, -88(%rbp)
	testq	%rdx, %rdx
	je	.L659
	movq	(%rdx), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L660
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L661
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -184(%rbp)
	movq	%rax, -104(%rbp)
	cmpq	%rax, %rsi
	je	.L662
	.p2align 4,,10
	.p2align 3
.L744:
	movq	-104(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -120(%rbp)
	testq	%rcx, %rcx
	je	.L663
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L664
	movq	16(%rcx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%r12, %r12
	je	.L665
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L666
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L667
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -112(%rbp)
	cmpq	%r13, %rax
	je	.L668
	movq	%r12, -152(%rbp)
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L671
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L672
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L671:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L673
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L674
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L673:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L669:
	addq	$8, %r13
	cmpq	%r13, -112(%rbp)
	je	.L1440
.L675:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L669
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1441
	movq	%r15, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -112(%rbp)
	jne	.L675
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	-152(%rbp), %r12
	movq	(%r14), %r13
.L668:
	testq	%r13, %r13
	je	.L676
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L676:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L667:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L677
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -112(%rbp)
	cmpq	%r13, %rax
	je	.L678
	movq	%r12, -152(%rbp)
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L1443:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L681
	call	_ZdlPv@PLT
.L681:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L682
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L683
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L682:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L684
	call	_ZdlPv@PLT
.L684:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L685
	call	_ZdlPv@PLT
.L685:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L686
	call	_ZdlPv@PLT
.L686:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L679:
	addq	$8, %r13
	cmpq	%r13, -112(%rbp)
	je	.L1442
.L687:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L679
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1443
	movq	%r15, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -112(%rbp)
	jne	.L687
	.p2align 4,,10
	.p2align 3
.L1442:
	movq	-152(%rbp), %r12
	movq	(%r14), %r13
.L678:
	testq	%r13, %r13
	je	.L688
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L688:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L677:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L689
	call	_ZdlPv@PLT
.L689:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L690
	call	_ZdlPv@PLT
.L690:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L691
	call	_ZdlPv@PLT
.L691:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L665:
	movq	-120(%rbp), %rax
	movq	8(%rax), %r14
	testq	%r14, %r14
	je	.L692
	movq	(%r14), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L693
	movq	160(%r14), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r13, %r13
	je	.L694
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -112(%rbp)
	cmpq	%r12, %rax
	je	.L695
	movq	%r13, -152(%rbp)
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	16(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L698
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L699
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L698:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L700
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L701
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L700:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L696:
	addq	$8, %r12
	cmpq	%r12, -112(%rbp)
	je	.L1444
.L702:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L696
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1445
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -112(%rbp)
	jne	.L702
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	-152(%rbp), %r13
	movq	0(%r13), %r12
.L695:
	testq	%r12, %r12
	je	.L703
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L703:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L694:
	movq	152(%r14), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L704
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -192(%rbp)
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rdx
	je	.L705
	movq	%r14, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L739:
	movq	-112(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L706
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L707
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L708
	call	_ZdlPv@PLT
.L708:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L709
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L710
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L711
	movq	8(%r15), %rax
	movq	(%r15), %r14
	cmpq	%r14, %rax
	je	.L712
	movq	%r12, -160(%rbp)
	movq	%rax, %r12
	movq	%r13, -208(%rbp)
	movq	%r15, -216(%rbp)
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L1447:
	movq	16(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L715
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L716
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L715:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L717
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L718
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L717:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L713:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1446
.L719:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L713
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1447
	addq	$8, %r14
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r14, %r12
	jne	.L719
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	-216(%rbp), %r15
	movq	-160(%rbp), %r12
	movq	-208(%rbp), %r13
	movq	(%r15), %r14
.L712:
	testq	%r14, %r14
	je	.L720
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L720:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L711:
	movq	152(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L721
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L722
	movq	%r12, -208(%rbp)
	movq	%r13, -216(%rbp)
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L1449:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L725
	call	_ZdlPv@PLT
.L725:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L726
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L727
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L726:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L728
	call	_ZdlPv@PLT
.L728:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L729
	call	_ZdlPv@PLT
.L729:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L730
	call	_ZdlPv@PLT
.L730:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L723:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1448
.L731:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L723
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1449
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L731
	.p2align 4,,10
	.p2align 3
.L1448:
	movq	-160(%rbp), %rax
	movq	-208(%rbp), %r12
	movq	-216(%rbp), %r13
	movq	(%rax), %r14
.L722:
	testq	%r14, %r14
	je	.L732
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L732:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L721:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L733
	call	_ZdlPv@PLT
.L733:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L734
	call	_ZdlPv@PLT
.L734:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L735
	call	_ZdlPv@PLT
.L735:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L709:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L736
	call	_ZdlPv@PLT
.L736:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L737
	call	_ZdlPv@PLT
.L737:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L738
	call	_ZdlPv@PLT
.L738:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L706:
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rax, -192(%rbp)
	jne	.L739
	movq	-152(%rbp), %rax
	movq	-200(%rbp), %r14
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
.L705:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L740
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L740:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L704:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rax
	cmpq	%rax, %rdi
	je	.L741
	call	_ZdlPv@PLT
.L741:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rax
	cmpq	%rax, %rdi
	je	.L742
	call	_ZdlPv@PLT
.L742:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L743
	call	_ZdlPv@PLT
.L743:
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L692:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L663:
	addq	$8, -104(%rbp)
	movq	-104(%rbp), %rax
	cmpq	%rax, -184(%rbp)
	jne	.L744
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
.L662:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L745
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L745:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L661:
	movq	-88(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L746
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -144(%rbp)
	movq	%rax, -104(%rbp)
	cmpq	%rax, %rdx
	je	.L747
	.p2align 4,,10
	.p2align 3
.L781:
	movq	-104(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L748
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L749
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L750
	call	_ZdlPv@PLT
.L750:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L751
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L752
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L753
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -152(%rbp)
	cmpq	%r14, %rcx
	je	.L754
	movq	%r12, -160(%rbp)
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L757
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L758
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L757:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L759
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L760
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L759:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L755:
	addq	$8, %r14
	cmpq	%r14, -152(%rbp)
	je	.L1450
.L761:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L755
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1451
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -152(%rbp)
	jne	.L761
	.p2align 4,,10
	.p2align 3
.L1450:
	movq	-120(%rbp), %rax
	movq	-160(%rbp), %r12
	movq	(%rax), %r14
.L754:
	testq	%r14, %r14
	je	.L762
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L762:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L753:
	movq	152(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L763
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	movq	%rsi, -152(%rbp)
	cmpq	%r14, %rsi
	je	.L764
	movq	%r12, -160(%rbp)
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L1453:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L767
	call	_ZdlPv@PLT
.L767:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L768
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L769
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L768:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L770
	call	_ZdlPv@PLT
.L770:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L771
	call	_ZdlPv@PLT
.L771:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L772
	call	_ZdlPv@PLT
.L772:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L765:
	addq	$8, %r14
	cmpq	%r14, -152(%rbp)
	je	.L1452
.L773:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L765
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1453
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -152(%rbp)
	jne	.L773
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	-120(%rbp), %rax
	movq	-160(%rbp), %r12
	movq	(%rax), %r14
.L764:
	testq	%r14, %r14
	je	.L774
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L774:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L763:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L775
	call	_ZdlPv@PLT
.L775:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L776
	call	_ZdlPv@PLT
.L776:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L777
	call	_ZdlPv@PLT
.L777:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L751:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L778
	call	_ZdlPv@PLT
.L778:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L779
	call	_ZdlPv@PLT
.L779:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L780
	call	_ZdlPv@PLT
.L780:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L748:
	addq	$8, -104(%rbp)
	movq	-104(%rbp), %rax
	cmpq	%rax, -144(%rbp)
	jne	.L781
	movq	-112(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
.L747:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L782
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L782:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L746:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L783
	call	_ZdlPv@PLT
.L783:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L784
	call	_ZdlPv@PLT
.L784:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L785
	call	_ZdlPv@PLT
.L785:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L659:
	movq	-72(%rbp), %rax
	movq	96(%rax), %rdi
	addq	$112, %rax
	cmpq	%rax, %rdi
	je	.L786
	call	_ZdlPv@PLT
.L786:
	movq	-72(%rbp), %rax
	movq	48(%rax), %rdi
	addq	$64, %rax
	cmpq	%rax, %rdi
	je	.L787
	call	_ZdlPv@PLT
.L787:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L788
	call	_ZdlPv@PLT
.L788:
	movq	-72(%rbp), %rdi
	movl	$192, %esi
	call	_ZdlPvm@PLT
.L656:
	addq	$8, -96(%rbp)
	movq	-96(%rbp), %rax
	cmpq	%rax, -176(%rbp)
	jne	.L789
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
.L655:
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L790
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L790:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L654:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L791
	call	_ZdlPv@PLT
.L791:
	movq	-80(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L792
	call	_ZdlPv@PLT
.L792:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L793
	call	_ZdlPv@PLT
.L793:
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L292:
	movq	-56(%rbp), %rax
	movq	264(%rax), %rdi
	addq	$280, %rax
	cmpq	%rax, %rdi
	je	.L794
	call	_ZdlPv@PLT
.L794:
	movq	-56(%rbp), %rax
	movq	216(%rax), %rdi
	addq	$232, %rax
	cmpq	%rax, %rdi
	je	.L795
	call	_ZdlPv@PLT
.L795:
	movq	-56(%rbp), %rax
	movq	168(%rax), %rdi
	addq	$184, %rax
	cmpq	%rax, %rdi
	je	.L796
	call	_ZdlPv@PLT
.L796:
	movq	-56(%rbp), %rax
	movq	152(%rax), %rdi
	testq	%rdi, %rdi
	je	.L797
	movq	(%rdi), %rax
	call	*24(%rax)
.L797:
	movq	-56(%rbp), %rax
	movq	112(%rax), %rdi
	subq	$-128, %rax
	cmpq	%rax, %rdi
	je	.L798
	call	_ZdlPv@PLT
.L798:
	movq	-56(%rbp), %rax
	movq	64(%rax), %rdi
	addq	$80, %rax
	cmpq	%rax, %rdi
	je	.L799
	call	_ZdlPv@PLT
.L799:
	movq	-56(%rbp), %rax
	movq	16(%rax), %rdi
	addq	$32, %rax
	cmpq	%rax, %rdi
	je	.L800
	call	_ZdlPv@PLT
.L800:
	movq	-56(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L286:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -168(%rbp)
	jne	.L801
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L285:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L802
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L802:
	movq	-136(%rbp), %rdi
	addq	$184, %rsp
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L657:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L293:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L289:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L438:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L664:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L749:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L303:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L615:
	movq	%r12, %rdi
	call	*%rax
	addq	$8, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	jne	.L647
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L660:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L299:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L476:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L707:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L513:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L573:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L369:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L666:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L618:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L693:
	movq	%r14, %rdi
	call	*%rax
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L305:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L482:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L441:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%r12, %rdi
	call	*%rax
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L509:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L752:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L338:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L672:
	call	*%rax
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L674:
	call	*%rax
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L635:
	call	*%rax
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L626:
	call	*%rax
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L488:
	call	*%rax
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L624:
	call	*%rax
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L458:
	call	*%rax
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L415:
	call	*%rax
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L699:
	call	*%rax
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L576:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L683:
	call	*%rax
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L424:
	call	*%rax
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L449:
	call	*%rax
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L499:
	call	*%rax
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L413:
	call	*%rax
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L311:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L542:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L769:
	call	*%rax
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L710:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L447:
	call	*%rax
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L760:
	call	*%rax
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L490:
	call	*%rax
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L701:
	call	*%rax
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L515:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L758:
	call	*%rax
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L727:
	call	*%rax
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L355:
	call	*%rax
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L389:
	call	*%rax
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L559:
	call	*%rax
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L593:
	call	*%rax
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L328:
	call	*%rax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L346:
	call	*%rax
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L344:
	call	*%rax
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L532:
	call	*%rax
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L523:
	call	*%rax
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L521:
	call	*%rax
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L550:
	call	*%rax
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L548:
	call	*%rax
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L380:
	call	*%rax
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L378:
	call	*%rax
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L584:
	call	*%rax
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L582:
	call	*%rax
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L319:
	call	*%rax
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L317:
	call	*%rax
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L718:
	call	*%rax
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L716:
	call	*%rax
	jmp	.L715
	.cfi_endproc
.LFE14627:
	.size	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0, .-_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
	.section	.rodata._ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi.part.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"<message collected>"
	.section	.text._ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi.part.0, @function
_ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi.part.0:
.LFB14607:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 24(%rdi)
	movl	$0, 116(%rdi)
	je	.L1488
.L1456:
	movq	136(%rbx), %r12
	pxor	%xmm0, %xmm0
	movq	144(%rbx), %r14
	movq	$0, 152(%rbx)
	movl	$0, 132(%rbx)
	movups	%xmm0, 136(%rbx)
	movq	%r12, %rbx
	cmpq	%r14, %r12
	je	.L1469
	.p2align 4,,10
	.p2align 3
.L1464:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L1467
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1468
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1468:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1467:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L1464
.L1469:
	testq	%r12, %r12
	je	.L1455
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1455:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1489
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1488:
	.cfi_restore_state
	leaq	-80(%rbp), %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-80(%rbp), %rdx
	leaq	-64(%rbp), %r12
	movq	16(%rbx), %rdi
	movq	-72(%rbp), %rax
	cmpq	%r12, %rdx
	je	.L1490
	leaq	32(%rbx), %rcx
	movq	-64(%rbp), %rsi
	cmpq	%rcx, %rdi
	je	.L1491
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	32(%rbx), %rcx
	movq	%rdx, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	testq	%rdi, %rdi
	je	.L1462
	movq	%rdi, -80(%rbp)
	movq	%rcx, -64(%rbp)
.L1460:
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movw	%ax, (%rdi)
	movq	-48(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, 48(%rbx)
	cmpq	%r12, %rdi
	je	.L1456
	call	_ZdlPv@PLT
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1491:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%rdx, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
.L1462:
	movq	%r12, -80(%rbp)
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1490:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L1458
	cmpq	$1, %rax
	je	.L1492
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L1458
	movq	%r12, %rsi
	call	memmove@PLT
	movq	-72(%rbp), %rax
	movq	16(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L1458:
	xorl	%ecx, %ecx
	movq	%rax, 24(%rbx)
	movw	%cx, (%rdi,%rdx)
	movq	-80(%rbp), %rdi
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1492:
	movzwl	-64(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-72(%rbp), %rax
	movq	16(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L1458
.L1489:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14607:
	.size	_ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi.part.0, .-_ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi.part.0
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev:
.LFB5277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rax, (%rdi)
	movq	160(%rdi), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1494
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rdx
	je	.L1495
	movq	%rdi, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	-56(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L1496
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1497
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L1498
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1499
	movq	160(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L1500
	movq	8(%r15), %rax
	movq	(%r15), %r14
	cmpq	%r14, %rax
	je	.L1501
	movq	%r12, -96(%rbp)
	movq	%rax, %r12
	movq	%r13, -72(%rbp)
	movq	%r15, -104(%rbp)
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1712:
	movq	16(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L1504
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1505
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1504:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L1506
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1507
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1506:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1502:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1711
.L1508:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L1502
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1712
	addq	$8, %r14
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r14, %r12
	jne	.L1508
	.p2align 4,,10
	.p2align 3
.L1711:
	movq	-104(%rbp), %r15
	movq	-72(%rbp), %r13
	movq	-96(%rbp), %r12
	movq	(%r15), %r14
.L1501:
	testq	%r14, %r14
	je	.L1509
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1509:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1500:
	movq	152(%r12), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L1510
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1511
	movq	%r13, -96(%rbp)
	movq	%r12, -104(%rbp)
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L1714:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1514
	call	_ZdlPv@PLT
.L1514:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1515
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1516
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1515:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1517
	call	_ZdlPv@PLT
.L1517:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1518
	call	_ZdlPv@PLT
.L1518:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1519
	call	_ZdlPv@PLT
.L1519:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1512:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1713
.L1520:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1512
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1714
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L1520
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	-72(%rbp), %rax
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%rax), %r14
.L1511:
	testq	%r14, %r14
	je	.L1521
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1521:
	movq	-72(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1510:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1522
	call	_ZdlPv@PLT
.L1522:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1523
	call	_ZdlPv@PLT
.L1523:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1524
	call	_ZdlPv@PLT
.L1524:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1498:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1525
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1526
	movq	160(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L1527
	movq	8(%r15), %rax
	movq	(%r15), %r14
	cmpq	%r14, %rax
	je	.L1528
	movq	%r12, -96(%rbp)
	movq	%rax, %r12
	movq	%r13, -72(%rbp)
	movq	%r15, -104(%rbp)
	jmp	.L1535
	.p2align 4,,10
	.p2align 3
.L1716:
	movq	16(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L1531
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1532
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1531:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L1533
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1534
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1533:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1529:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1715
.L1535:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L1529
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1716
	addq	$8, %r14
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r14, %r12
	jne	.L1535
	.p2align 4,,10
	.p2align 3
.L1715:
	movq	-104(%rbp), %r15
	movq	-72(%rbp), %r13
	movq	-96(%rbp), %r12
	movq	(%r15), %r14
.L1528:
	testq	%r14, %r14
	je	.L1536
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1536:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1527:
	movq	152(%r12), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L1537
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1538
	movq	%r13, -96(%rbp)
	movq	%r12, -104(%rbp)
	jmp	.L1547
	.p2align 4,,10
	.p2align 3
.L1718:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1541
	call	_ZdlPv@PLT
.L1541:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1542
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1543
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1542:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1544
	call	_ZdlPv@PLT
.L1544:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1545
	call	_ZdlPv@PLT
.L1545:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1546
	call	_ZdlPv@PLT
.L1546:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1539:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1717
.L1547:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1539
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1718
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L1547
	.p2align 4,,10
	.p2align 3
.L1717:
	movq	-72(%rbp), %rax
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%rax), %r14
.L1538:
	testq	%r14, %r14
	je	.L1548
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1548:
	movq	-72(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1537:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1549
	call	_ZdlPv@PLT
.L1549:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1550
	call	_ZdlPv@PLT
.L1550:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1551
	call	_ZdlPv@PLT
.L1551:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1525:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1496:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L1552
	movq	-64(%rbp), %rax
	movq	-88(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L1495:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1553
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1553:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1494:
	movq	152(%rbx), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1554
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -80(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rcx
	je	.L1555
	movq	%rbx, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	-56(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L1556
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1557
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1558
	call	_ZdlPv@PLT
.L1558:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1559
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1560
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L1561
	movq	8(%r15), %rax
	movq	(%r15), %r14
	cmpq	%r14, %rax
	je	.L1562
	movq	%r12, -72(%rbp)
	movq	%rax, %r12
	movq	%r13, -96(%rbp)
	movq	%r15, -104(%rbp)
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1720:
	movq	16(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L1565
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1566
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1565:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L1567
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1568
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1567:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1563:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1719
.L1569:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L1563
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1720
	addq	$8, %r14
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r14, %r12
	jne	.L1569
	.p2align 4,,10
	.p2align 3
.L1719:
	movq	-104(%rbp), %r15
	movq	-72(%rbp), %r12
	movq	-96(%rbp), %r13
	movq	(%r15), %r14
.L1562:
	testq	%r14, %r14
	je	.L1570
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1570:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1561:
	movq	152(%r13), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L1571
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1572
	movq	%r12, -96(%rbp)
	movq	%r13, -104(%rbp)
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L1722:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1575
	call	_ZdlPv@PLT
.L1575:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1576
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1577
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1576:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1578
	call	_ZdlPv@PLT
.L1578:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1579
	call	_ZdlPv@PLT
.L1579:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1580
	call	_ZdlPv@PLT
.L1580:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1573:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1721
.L1581:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1573
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1722
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L1581
	.p2align 4,,10
	.p2align 3
.L1721:
	movq	-72(%rbp), %rax
	movq	-96(%rbp), %r12
	movq	-104(%rbp), %r13
	movq	(%rax), %r14
.L1572:
	testq	%r14, %r14
	je	.L1582
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1582:
	movq	-72(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1571:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1583
	call	_ZdlPv@PLT
.L1583:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1584
	call	_ZdlPv@PLT
.L1584:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1585
	call	_ZdlPv@PLT
.L1585:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1559:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1586
	call	_ZdlPv@PLT
.L1586:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1587
	call	_ZdlPv@PLT
.L1587:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1588
	call	_ZdlPv@PLT
.L1588:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1556:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L1589
	movq	-64(%rbp), %rax
	movq	-88(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L1555:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1590
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1590:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1554:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1591
	call	_ZdlPv@PLT
.L1591:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1592
	call	_ZdlPv@PLT
.L1592:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1593
	call	_ZdlPv@PLT
.L1593:
	addq	$72, %rsp
	movq	%rbx, %rdi
	movl	$168, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1557:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1556
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1499:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1526:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1566:
	call	*%rax
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1577:
	call	*%rax
	jmp	.L1576
	.p2align 4,,10
	.p2align 3
.L1568:
	call	*%rax
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1532:
	call	*%rax
	jmp	.L1531
	.p2align 4,,10
	.p2align 3
.L1534:
	call	*%rax
	jmp	.L1533
	.p2align 4,,10
	.p2align 3
.L1507:
	call	*%rax
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1516:
	call	*%rax
	jmp	.L1515
	.p2align 4,,10
	.p2align 3
.L1543:
	call	*%rax
	jmp	.L1542
	.p2align 4,,10
	.p2align 3
.L1505:
	call	*%rax
	jmp	.L1504
	.cfi_endproc
.LFE5277:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_5ValueEEEj.str1.1,"aMS",@progbits,1
.LC3:
	.string	"true"
.LC4:
	.string	"false"
.LC5:
	.string	"Symbol("
.LC6:
	.string	"[object Proxy]"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_5ValueEEEj,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_5ValueEEEj, @function
_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_5ValueEEEj:
.LFB7912:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1790
	movq	(%rsi), %rax
	movq	%rdi, %r12
	movq	%rsi, %r14
	movq	%rax, %rcx
	andl	$3, %ecx
	testb	$1, %dl
	je	.L1725
	cmpq	$1, %rcx
	jne	.L1726
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	cmpw	$67, %cx
	jne	.L1727
	cmpl	$3, 43(%rax)
	movl	$1, %r15d
	je	.L1723
	andl	$2, %edx
	jne	.L1786
	.p2align 4,,10
	.p2align 3
.L1785:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %ecx
.L1730:
	cmpw	$63, %cx
	jbe	.L1847
.L1726:
	movq	%r14, %rdi
	call	_ZNK2v85Value14IsStringObjectEv@PLT
	movq	%r14, %rdi
	movl	%eax, %r15d
	testb	%al, %al
	je	.L1731
	call	_ZNK2v812StringObject7ValueOfEv@PLT
	leaq	64(%r12), %rdi
	movq	%rax, %r13
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1741
	testq	%r13, %r13
	je	.L1723
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r14
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	leaq	16(%r12), %rdi
	movq	%r14, %rsi
.L1841:
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1723
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1723:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1848
	addq	$88, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1725:
	.cfi_restore_state
	andl	$2, %edx
	je	.L1729
	cmpq	$1, %rcx
	jne	.L1726
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %ecx
	cmpw	$67, %cx
	jne	.L1730
.L1786:
	cmpl	$5, 43(%rax)
	movl	$1, %r15d
	jne	.L1726
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1731:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L1734
	movq	112(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1723
	movq	%r12, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_6StringEEE
	leaq	64(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1723
	leaq	16(%r12), %rdi
	movl	$110, %esi
	movl	%ebx, %r15d
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1729:
	cmpq	$1, %rcx
	je	.L1785
	jmp	.L1726
	.p2align 4,,10
	.p2align 3
.L1734:
	movq	%r14, %rdi
	call	_ZNK2v85Value14IsBigIntObjectEv@PLT
	movq	%r14, %rdi
	movl	%eax, %r15d
	testb	%al, %al
	je	.L1737
	call	_ZNK2v812BigIntObject7ValueOfEv@PLT
	movq	112(%r12), %rsi
	movq	%rax, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1741
	leaq	64(%r12), %r14
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1849
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %rbx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	leaq	16(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1779
	call	_ZdlPv@PLT
.L1779:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1741
	leaq	16(%r12), %rdi
	movl	$110, %esi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1847:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_6StringEEE
	movl	%eax, %r15d
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1727:
	andl	$2, %edx
	je	.L1785
	jmp	.L1730
	.p2align 4,,10
	.p2align 3
.L1790:
	movl	$1, %r15d
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1741:
	xorl	%r15d, %r15d
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1737:
	call	_ZNK2v85Value8IsSymbolEv@PLT
	testb	%al, %al
	jne	.L1846
	movq	%r14, %rdi
	call	_ZNK2v85Value14IsSymbolObjectEv@PLT
	movq	%r14, %rdi
	testb	%al, %al
	je	.L1744
	call	_ZNK2v812SymbolObject7ValueOfEv@PLT
	movq	%rax, %r14
.L1846:
	leaq	-96(%rbp), %r15
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	leaq	16(%r12), %r13
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1745
	call	_ZdlPv@PLT
.L1745:
	movq	%r14, %rdi
	call	_ZNK2v86Symbol4NameEv@PLT
	movq	%r12, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_5ValueEEEj
	movl	$41, %esi
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1849:
	xorl	%r15d, %r15d
	jmp	.L1779
	.p2align 4,,10
	.p2align 3
.L1744:
	call	_ZNK2v85Value14IsNumberObjectEv@PLT
	movq	%r14, %rdi
	movl	%eax, %r15d
	testb	%al, %al
	jne	.L1850
	call	_ZNK2v85Value15IsBooleanObjectEv@PLT
	movl	%eax, %r15d
	testb	%al, %al
	jne	.L1851
	movq	%r14, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	movl	%eax, %ebx
	testb	%al, %al
	je	.L1751
	movq	40(%r12), %rax
	movq	48(%r12), %rcx
	cmpq	%rcx, %rax
	je	.L1752
	.p2align 4,,10
	.p2align 3
.L1755:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1753
	movq	(%r14), %rsi
	cmpq	%rsi, (%rdx)
	je	.L1795
.L1753:
	addq	$8, %rax
	cmpq	%rax, %rcx
	jne	.L1755
.L1752:
	movq	%r14, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movl	(%r12), %ecx
	movl	%eax, -100(%rbp)
	movl	%eax, %esi
	cmpl	%ecx, %eax
	ja	.L1723
	movq	48(%r12), %r8
	movq	40(%r12), %r10
	movq	%r8, %rdx
	subq	%r10, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	$263, %rdx
	ja	.L1723
	subl	%esi, %ecx
	movl	%ecx, (%r12)
	cmpq	56(%r12), %r8
	je	.L1756
	movq	%r14, (%r8)
	addq	$8, 48(%r12)
.L1757:
	xorl	%r13d, %r13d
	leaq	16(%r12), %rax
	cmpl	$0, -100(%rbp)
	movq	%rax, -112(%rbp)
	je	.L1787
.L1767:
	movq	112(%r12), %rsi
	movl	%r13d, %edx
	movq	%r14, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1769
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_5ValueEEEj
	movl	%eax, %r15d
	testb	%al, %al
	je	.L1768
.L1769:
	addl	$1, %r13d
	cmpl	%r13d, -100(%rbp)
	jne	.L1852
.L1787:
	movl	%ebx, %r15d
.L1768:
	subq	$8, 48(%r12)
	jmp	.L1723
.L1852:
	movq	-112(%rbp), %rdi
	movl	$44, %esi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	jmp	.L1767
.L1850:
	call	_ZNK2v812NumberObject7ValueOfEv@PLT
	leaq	-96(%rbp), %r13
	movl	$6, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String1610fromDoubleEdi@PLT
.L1844:
	leaq	16(%r12), %rdi
	movq	%r13, %rsi
	jmp	.L1841
.L1851:
	movq	%r14, %rdi
	addq	$16, %r12
	leaq	-96(%rbp), %r13
	call	_ZNK2v813BooleanObject7ValueOfEv@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	testb	%al, %al
	leaq	.LC4(%rip), %rax
	cmove	%rax, %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	jmp	.L1841
.L1751:
	movq	%r14, %rdi
	call	_ZNK2v85Value7IsProxyEv@PLT
	movl	%eax, %r15d
	testb	%al, %al
	jne	.L1853
	movq	%r14, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1784
	movq	%r14, %rdi
	call	_ZNK2v85Value6IsDateEv@PLT
	testb	%al, %al
	je	.L1854
.L1784:
	movq	112(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1723
.L1831:
	movq	%r12, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_6StringEEE
	movl	%eax, %r15d
	jmp	.L1723
.L1795:
	movl	%ebx, %r15d
	jmp	.L1723
.L1853:
	leaq	-96(%rbp), %r13
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L1844
.L1854:
	movq	%r14, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L1784
	movq	%r14, %rdi
	call	_ZNK2v85Value13IsNativeErrorEv@PLT
	testb	%al, %al
	jne	.L1784
	movq	%r14, %rdi
	call	_ZNK2v85Value8IsRegExpEv@PLT
	testb	%al, %al
	jne	.L1784
	movq	112(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object19ObjectProtoToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L1831
	jmp	.L1784
.L1848:
	call	__stack_chk_fail@PLT
.L1756:
	testq	%rax, %rax
	movl	$1, %r13d
	movq	%rdx, -128(%rbp)
	cmovne	%rax, %r13
	movq	%r8, -120(%rbp)
	movq	%r10, -112(%rbp)
	addq	%rax, %r13
	movabsq	$1152921504606846975, %rax
	cmovc	%rax, %r13
	salq	$3, %r13
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	-128(%rbp), %rdx
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r8
	movq	%r14, (%rax,%rdx)
	cmpq	%r8, %r10
	je	.L1799
	leaq	-8(%r8), %rsi
	leaq	15(%r10), %rcx
	subq	%r10, %rsi
	subq	%rax, %rcx
	movq	%rsi, %rdx
	shrq	$3, %rdx
	cmpq	$30, %rcx
	jbe	.L1796
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdx
	je	.L1796
	addq	$1, %rdx
	xorl	%ecx, %ecx
	movq	%rdx, %rdi
	shrq	%rdi
	salq	$4, %rdi
.L1763:
	movdqu	(%r10,%rcx), %xmm1
	movups	%xmm1, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rdi, %rcx
	jne	.L1763
	movq	%rdx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rcx
	leaq	(%r10,%rcx), %r9
	addq	%rax, %rcx
	cmpq	%rdi, %rdx
	je	.L1765
	movq	(%r9), %rdx
	movq	%rdx, (%rcx)
.L1765:
	leaq	8(%rax,%rsi), %r15
.L1782:
	addq	$8, %r15
	testq	%r10, %r10
	je	.L1766
	movq	%r10, %rdi
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L1766:
	movq	%rax, %xmm0
	movq	%r15, %xmm2
	addq	%r13, %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 56(%r12)
	movups	%xmm0, 40(%r12)
	jmp	.L1757
.L1796:
	xorl	%edx, %edx
.L1762:
	movq	(%r10,%rdx), %rcx
	movq	%rcx, (%rax,%rdx)
	addq	$8, %rdx
	leaq	(%r10,%rdx), %rcx
	cmpq	%rcx, %r8
	jne	.L1762
	jmp	.L1765
.L1799:
	movq	%rax, %r15
	jmp	.L1782
	.cfi_endproc
.LFE7912:
	.size	_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_5ValueEEEj, .-_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_5ValueEEEj
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB5094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	312(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L1856
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1857
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1858
	call	_ZdlPv@PLT
.L1858:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1859
	call	_ZdlPv@PLT
.L1859:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1856:
	movq	304(%rbx), %rcx
	movq	%rcx, -64(%rbp)
	testq	%rcx, %rcx
	je	.L1860
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1861
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L1862
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -72(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rcx
	je	.L1863
	movq	%rbx, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L1920:
	movq	-56(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L1864
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1865
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L1866
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1867
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1868
	movq	8(%r14), %rax
	movq	(%r14), %rbx
	cmpq	%rbx, %rax
	je	.L1869
	movq	%r12, -104(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
	movq	%r13, -96(%rbp)
	jmp	.L1876
	.p2align 4,,10
	.p2align 3
.L2204:
	movq	16(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L1872
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1873
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1872:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L1874
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1875
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1874:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1870:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L2203
.L1876:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L1870
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2204
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L1876
	.p2align 4,,10
	.p2align 3
.L2203:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r14), %rbx
.L1869:
	testq	%rbx, %rbx
	je	.L1877
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1877:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1868:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L1878
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L1879
	movq	%r13, -96(%rbp)
	movq	%r12, -104(%rbp)
	jmp	.L1888
	.p2align 4,,10
	.p2align 3
.L2206:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1882
	call	_ZdlPv@PLT
.L1882:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1883
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1884
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1883:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1885
	call	_ZdlPv@PLT
.L1885:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1886
	call	_ZdlPv@PLT
.L1886:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1887
	call	_ZdlPv@PLT
.L1887:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1880:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2205
.L1888:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1880
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2206
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L1888
	.p2align 4,,10
	.p2align 3
.L2205:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r15), %r14
.L1879:
	testq	%r14, %r14
	je	.L1889
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1889:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1878:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1890
	call	_ZdlPv@PLT
.L1890:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1891
	call	_ZdlPv@PLT
.L1891:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1892
	call	_ZdlPv@PLT
.L1892:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1866:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1893
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1894
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1895
	movq	8(%r14), %rax
	movq	(%r14), %rbx
	cmpq	%rbx, %rax
	je	.L1896
	movq	%r12, -104(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
	movq	%r13, -96(%rbp)
	jmp	.L1903
	.p2align 4,,10
	.p2align 3
.L2208:
	movq	16(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L1899
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1900
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1899:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L1901
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1902
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1901:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1897:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L2207
.L1903:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L1897
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2208
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L1903
	.p2align 4,,10
	.p2align 3
.L2207:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r14), %rbx
.L1896:
	testq	%rbx, %rbx
	je	.L1904
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1904:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1895:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L1905
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L1906
	movq	%r13, -96(%rbp)
	movq	%r12, -104(%rbp)
	jmp	.L1915
	.p2align 4,,10
	.p2align 3
.L2210:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1909
	call	_ZdlPv@PLT
.L1909:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1910
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1911
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1910:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1912
	call	_ZdlPv@PLT
.L1912:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1913
	call	_ZdlPv@PLT
.L1913:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1914
	call	_ZdlPv@PLT
.L1914:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1907:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2209
.L1915:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1907
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2210
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L1915
	.p2align 4,,10
	.p2align 3
.L2209:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r15), %r14
.L1906:
	testq	%r14, %r14
	je	.L1916
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1916:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1905:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1917
	call	_ZdlPv@PLT
.L1917:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1918
	call	_ZdlPv@PLT
.L1918:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1919
	call	_ZdlPv@PLT
.L1919:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1893:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1864:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	jne	.L1920
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L1863:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1921
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1921:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1862:
	movq	-64(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L1922
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rdx
	je	.L1923
	movq	%rbx, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L2007:
	movq	-56(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1924
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1925
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1926
	call	_ZdlPv@PLT
.L1926:
	movq	136(%rbx), %rsi
	movq	%rsi, -72(%rbp)
	testq	%rsi, %rsi
	je	.L1927
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1928
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1929
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L1930
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1987:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1931
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1932
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1933
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1934
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L1935
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L1936
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -136(%rbp)
	jmp	.L1943
	.p2align 4,,10
	.p2align 3
.L2212:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1939
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1940
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1939:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L1941
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1942
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1941:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1937:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2211
.L1943:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1937
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2212
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L2213:
	movq	-88(%rbp), %r13
	movq	0(%r13), %r12
.L1990:
	testq	%r12, %r12
	je	.L2000
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2000:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1989:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2001
	call	_ZdlPv@PLT
.L2001:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2002
	call	_ZdlPv@PLT
.L2002:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2003
	call	_ZdlPv@PLT
.L2003:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1927:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2004
	call	_ZdlPv@PLT
.L2004:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2005
	call	_ZdlPv@PLT
.L2005:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2006
	call	_ZdlPv@PLT
.L2006:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1924:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L2007
	movq	-96(%rbp), %rax
	movq	-120(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L1923:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L2008
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2008:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1922:
	movq	-64(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2009
	call	_ZdlPv@PLT
.L2009:
	movq	-64(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2010
	call	_ZdlPv@PLT
.L2010:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2011
	call	_ZdlPv@PLT
.L2011:
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1860:
	movq	264(%rbx), %rdi
	leaq	280(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2012
	call	_ZdlPv@PLT
.L2012:
	movq	216(%rbx), %rdi
	leaq	232(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2013
	call	_ZdlPv@PLT
.L2013:
	movq	168(%rbx), %rdi
	leaq	184(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2014
	call	_ZdlPv@PLT
.L2014:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2015
	movq	(%rdi), %rax
	call	*24(%rax)
.L2015:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2016
	call	_ZdlPv@PLT
.L2016:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2017
	call	_ZdlPv@PLT
.L2017:
	movq	16(%rbx), %rdi
	leaq	32(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2018
	call	_ZdlPv@PLT
.L2018:
	addq	$104, %rsp
	movq	%rbx, %rdi
	movl	$320, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1932:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1931:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L1987
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1930:
	testq	%rdi, %rdi
	je	.L1988
	call	_ZdlPv@PLT
.L1988:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1929:
	movq	-72(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L1989
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L1990
	movq	%r13, -88(%rbp)
	jmp	.L1999
	.p2align 4,,10
	.p2align 3
.L2214:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1993
	call	_ZdlPv@PLT
.L1993:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L1994
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1995
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1994:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1996
	call	_ZdlPv@PLT
.L1996:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1997
	call	_ZdlPv@PLT
.L1997:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1998
	call	_ZdlPv@PLT
.L1998:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1991:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2213
.L1999:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L1991
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2214
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1991
	.p2align 4,,10
	.p2align 3
.L2217:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L1946:
	testq	%r14, %r14
	je	.L1956
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1956:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1945:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1957
	call	_ZdlPv@PLT
.L1957:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1958
	call	_ZdlPv@PLT
.L1958:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1959
	call	_ZdlPv@PLT
.L1959:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1933:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1960
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1961
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L1962
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L1963
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -136(%rbp)
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L2216:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1966
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1967
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1966:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L1968
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1969
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1968:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1964:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2215
.L1970:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1964
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2216
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L2219:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L1973:
	testq	%r14, %r14
	je	.L1983
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1983:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1972:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1984
	call	_ZdlPv@PLT
.L1984:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1985
	call	_ZdlPv@PLT
.L1985:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1986
	call	_ZdlPv@PLT
.L1986:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1960:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1931
	.p2align 4,,10
	.p2align 3
.L2211:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L1936:
	testq	%r14, %r14
	je	.L1944
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1944:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1935:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L1945
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1946
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L2218:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1949
	call	_ZdlPv@PLT
.L1949:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1950
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1951
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1950:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1952
	call	_ZdlPv@PLT
.L1952:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1953
	call	_ZdlPv@PLT
.L1953:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1954
	call	_ZdlPv@PLT
.L1954:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1947:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2217
.L1955:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1947
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2218
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1947
	.p2align 4,,10
	.p2align 3
.L2215:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L1963:
	testq	%r14, %r14
	je	.L1971
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1971:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1962:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L1972
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1973
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L1982
	.p2align 4,,10
	.p2align 3
.L2220:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1976
	call	_ZdlPv@PLT
.L1976:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1977
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1978
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1977:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1979
	call	_ZdlPv@PLT
.L1979:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1980
	call	_ZdlPv@PLT
.L1980:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1981
	call	_ZdlPv@PLT
.L1981:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1974:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2219
.L1982:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1974
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2220
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1974
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L1925:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1924
	.p2align 4,,10
	.p2align 3
.L1857:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1861:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1867:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1866
	.p2align 4,,10
	.p2align 3
.L1928:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L1927
	.p2align 4,,10
	.p2align 3
.L1894:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L1961:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1960
	.p2align 4,,10
	.p2align 3
.L1900:
	call	*%rdx
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L1884:
	call	*%rax
	jmp	.L1883
	.p2align 4,,10
	.p2align 3
.L1902:
	call	*%rdx
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1911:
	call	*%rax
	jmp	.L1910
	.p2align 4,,10
	.p2align 3
.L1875:
	call	*%rdx
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1873:
	call	*%rdx
	jmp	.L1872
	.p2align 4,,10
	.p2align 3
.L1995:
	call	*%rax
	jmp	.L1994
	.p2align 4,,10
	.p2align 3
.L1934:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1933
	.p2align 4,,10
	.p2align 3
.L1942:
	call	*%rdx
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1940:
	call	*%rdx
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1967:
	call	*%rdx
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L1969:
	call	*%rdx
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L1978:
	call	*%rax
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L1951:
	call	*%rax
	jmp	.L1950
	.cfi_endproc
.LFE5094:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev:
.LFB5092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	312(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L2222
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2223
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2224
	call	_ZdlPv@PLT
.L2224:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2225
	call	_ZdlPv@PLT
.L2225:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2222:
	movq	304(%rbx), %r12
	testq	%r12, %r12
	je	.L2226
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r14
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2227
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2228
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	movq	%rax, -56(%rbp)
	cmpq	%r15, %rax
	jne	.L2236
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2300:
	movq	16(%r8), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r8)
	testq	%rdi, %rdi
	je	.L2232
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2233
	movq	%r8, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-72(%rbp), %r8
.L2232:
	movq	8(%r8), %rdi
	testq	%rdi, %rdi
	je	.L2234
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2235
	movq	%r8, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-72(%rbp), %r8
.L2234:
	movl	$24, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L2230:
	addq	$8, %r15
	cmpq	%r15, -56(%rbp)
	je	.L2299
.L2236:
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L2230
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2300
	movq	%r8, %rdi
	addq	$8, %r15
	call	*%rax
	cmpq	%r15, -56(%rbp)
	jne	.L2236
	.p2align 4,,10
	.p2align 3
.L2299:
	movq	0(%r13), %r15
.L2229:
	testq	%r15, %r15
	je	.L2237
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2237:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2228:
	movq	152(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L2238
	movq	8(%rax), %rdx
	movq	(%rax), %r13
	movq	%rdx, -56(%rbp)
	cmpq	%r13, %rdx
	jne	.L2248
	jmp	.L2239
	.p2align 4,,10
	.p2align 3
.L2302:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2242
	call	_ZdlPv@PLT
.L2242:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2243
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2244
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2243:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2245
	call	_ZdlPv@PLT
.L2245:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2246
	call	_ZdlPv@PLT
.L2246:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2247
	call	_ZdlPv@PLT
.L2247:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2240:
	addq	$8, %r13
	cmpq	%r13, -56(%rbp)
	je	.L2301
.L2248:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L2240
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2302
	movq	%r15, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -56(%rbp)
	jne	.L2248
	.p2align 4,,10
	.p2align 3
.L2301:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
.L2239:
	testq	%r13, %r13
	je	.L2249
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2249:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2238:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2250
	call	_ZdlPv@PLT
.L2250:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2251
	call	_ZdlPv@PLT
.L2251:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2252
	call	_ZdlPv@PLT
.L2252:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2226:
	movq	264(%rbx), %rdi
	leaq	280(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2253
	call	_ZdlPv@PLT
.L2253:
	movq	216(%rbx), %rdi
	leaq	232(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2254
	call	_ZdlPv@PLT
.L2254:
	movq	168(%rbx), %rdi
	leaq	184(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2255
	call	_ZdlPv@PLT
.L2255:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2256
	movq	(%rdi), %rax
	call	*24(%rax)
.L2256:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2257
	call	_ZdlPv@PLT
.L2257:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2258
	call	_ZdlPv@PLT
.L2258:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L2221
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2221:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2223:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2227:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2226
	.p2align 4,,10
	.p2align 3
.L2235:
	movq	%r8, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %r8
	jmp	.L2234
	.p2align 4,,10
	.p2align 3
.L2244:
	call	*%rax
	jmp	.L2243
	.p2align 4,,10
	.p2align 3
.L2233:
	movq	%r8, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %r8
	jmp	.L2232
	.cfi_endproc
.LFE5092:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev:
.LFB14764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rdx
	leaq	-64(%rdx), %rax
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	304(%rdi), %r12
	movups	%xmm0, -8(%rdi)
	testq	%r12, %r12
	je	.L2304
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rcx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L2379
	movq	%r12, %rdi
	call	*%rdx
.L2304:
	movq	296(%r14), %r15
	testq	%r15, %r15
	je	.L2308
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r13
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	je	.L2380
	movq	%r15, %rdi
	call	*%rdx
.L2308:
	movq	256(%r14), %rdi
	leaq	272(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L2335
	call	_ZdlPv@PLT
.L2335:
	movq	208(%r14), %rdi
	leaq	224(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L2336
	call	_ZdlPv@PLT
.L2336:
	movq	160(%r14), %rdi
	leaq	176(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L2337
	call	_ZdlPv@PLT
.L2337:
	movq	144(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2338
	movq	(%rdi), %rdx
	call	*24(%rdx)
.L2338:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L2339
	call	_ZdlPv@PLT
.L2339:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L2340
	call	_ZdlPv@PLT
.L2340:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2303
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2303:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2380:
	.cfi_restore_state
	movq	160(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2310
	movq	8(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movq	%rax, -56(%rbp)
.L2318:
	movq	-56(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L2311
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2312
	movq	(%rbx), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2313
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L2314
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L2315
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2314:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2316
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L2317
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2316:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2312:
	addq	$8, -56(%rbp)
	jmp	.L2318
.L2313:
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L2312
.L2317:
	call	*%rdx
	jmp	.L2316
.L2311:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2319
	call	_ZdlPv@PLT
.L2319:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2310:
	movq	152(%r15), %r12
	testq	%r12, %r12
	je	.L2320
	movq	8(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movq	%rax, -56(%rbp)
.L2330:
	movq	-56(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L2321
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2322
	movq	(%rbx), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2323
	movq	152(%rbx), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	leaq	168(%rbx), %rdx
	movq	%rax, (%rbx)
	cmpq	%rdx, %rdi
	je	.L2324
	call	_ZdlPv@PLT
.L2324:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2325
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L2326
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2325:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L2327
	call	_ZdlPv@PLT
.L2327:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L2328
	call	_ZdlPv@PLT
.L2328:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L2329
	call	_ZdlPv@PLT
.L2329:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2322:
	addq	$8, -56(%rbp)
	jmp	.L2330
.L2323:
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L2322
.L2326:
	call	*%rdx
	jmp	.L2325
.L2321:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2331
	call	_ZdlPv@PLT
.L2331:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2320:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L2332
	call	_ZdlPv@PLT
.L2332:
	movq	56(%r15), %rdi
	leaq	72(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L2333
	call	_ZdlPv@PLT
.L2333:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L2334
	call	_ZdlPv@PLT
.L2334:
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2308
.L2379:
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	leaq	72(%r12), %rdx
	movq	%rax, (%r12)
	cmpq	%rdx, %rdi
	je	.L2306
	call	_ZdlPv@PLT
.L2306:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L2307
	call	_ZdlPv@PLT
.L2307:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2304
.L2315:
	call	*%rdx
	jmp	.L2314
	.cfi_endproc
.LFE14764:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB14762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rdx
	leaq	-64(%rdx), %rax
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-8(%rdi), %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	304(%rdi), %r13
	movups	%xmm0, -8(%rdi)
	testq	%r13, %r13
	je	.L2382
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rcx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L2457
	movq	%r13, %rdi
	call	*%rdx
.L2382:
	movq	296(%r15), %r14
	testq	%r14, %r14
	je	.L2386
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	je	.L2458
	movq	%r14, %rdi
	call	*%rdx
.L2386:
	movq	256(%r15), %rdi
	leaq	272(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L2413
	call	_ZdlPv@PLT
.L2413:
	movq	208(%r15), %rdi
	leaq	224(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L2414
	call	_ZdlPv@PLT
.L2414:
	movq	160(%r15), %rdi
	leaq	176(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L2415
	call	_ZdlPv@PLT
.L2415:
	movq	144(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2416
	movq	(%rdi), %rdx
	call	*24(%rdx)
.L2416:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L2417
	call	_ZdlPv@PLT
.L2417:
	movq	56(%r15), %rdi
	leaq	72(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L2418
	call	_ZdlPv@PLT
.L2418:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2419
	call	_ZdlPv@PLT
.L2419:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
.L2457:
	.cfi_restore_state
	movq	56(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	leaq	72(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L2384
	call	_ZdlPv@PLT
.L2384:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L2385
	call	_ZdlPv@PLT
.L2385:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2382
.L2458:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r14)
	movq	160(%r14), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L2388
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, -64(%rbp)
.L2396:
	movq	-64(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L2389
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L2390
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2391
	movq	16(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L2392
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2393
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2392:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2394
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2395
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2394:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2390:
	addq	$8, -64(%rbp)
	jmp	.L2396
.L2389:
	movq	-56(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2397
	call	_ZdlPv@PLT
.L2397:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2388:
	movq	152(%r14), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L2398
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, -64(%rbp)
.L2408:
	movq	-64(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L2399
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L2400
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2401
	movq	152(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	leaq	168(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L2402
	call	_ZdlPv@PLT
.L2402:
	movq	136(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2403
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2404
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2403:
	movq	96(%r13), %rdi
	leaq	112(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L2405
	call	_ZdlPv@PLT
.L2405:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L2406
	call	_ZdlPv@PLT
.L2406:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L2407
	call	_ZdlPv@PLT
.L2407:
	movl	$192, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2400:
	addq	$8, -64(%rbp)
	jmp	.L2408
.L2399:
	movq	-56(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2409
	call	_ZdlPv@PLT
.L2409:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2398:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L2410
	call	_ZdlPv@PLT
.L2410:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L2411
	call	_ZdlPv@PLT
.L2411:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L2412
	call	_ZdlPv@PLT
.L2412:
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2386
.L2393:
	call	*%rdx
	jmp	.L2392
.L2401:
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L2400
.L2404:
	call	*%rdx
	jmp	.L2403
.L2391:
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L2390
.L2395:
	call	*%rdx
	jmp	.L2394
	.cfi_endproc
.LFE14762:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev, @function
_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev:
.LFB5800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime10StackTraceE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L2460
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2461
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm2
	leaq	80(%r12), %rax
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L2462
	call	_ZdlPv@PLT
.L2462:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2463
	call	_ZdlPv@PLT
.L2463:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2460:
	movq	72(%rbx), %r12
	testq	%r12, %r12
	je	.L2464
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2465
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2464:
	movq	64(%rbx), %r13
	testq	%r13, %r13
	je	.L2466
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	jne	.L2473
	testq	%r12, %r12
	je	.L2474
	.p2align 4,,10
	.p2align 3
.L2495:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2474:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2466:
	movq	24(%rbx), %rdi
	addq	$40, %rbx
	cmpq	%rbx, %rdi
	je	.L2459
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2494:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r15), %rdi
	movq	%rax, (%r15)
	leaq	104(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2470
	call	_ZdlPv@PLT
.L2470:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2471
	call	_ZdlPv@PLT
.L2471:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2472
	call	_ZdlPv@PLT
.L2472:
	movl	$136, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2468:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L2493
.L2473:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L2468
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2494
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L2473
	.p2align 4,,10
	.p2align 3
.L2493:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L2495
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L2459:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2465:
	.cfi_restore_state
	call	*%rax
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2461:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2460
	.cfi_endproc
.LFE5800:
	.size	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev, .-_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.set	.LTHUNK2,_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev:
.LFB14787:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK2
	.cfi_endproc
.LFE14787:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev,_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev, @function
_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev:
.LFB5802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5802:
	.size	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev, .-_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev:
.LFB5671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	168(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L2499
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2500
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2499:
	movq	160(%rbx), %r12
	testq	%r12, %r12
	je	.L2501
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2502
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2501:
	movq	120(%rbx), %rdi
	leaq	136(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2503
	call	_ZdlPv@PLT
.L2503:
	movq	72(%rbx), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2504
	call	_ZdlPv@PLT
.L2504:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L2498
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2498:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2502:
	.cfi_restore_state
	call	*%rax
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2500:
	call	*%rax
	jmp	.L2499
	.cfi_endproc
.LFE5671:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev,_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev:
.LFB5673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	168(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L2514
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2515
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2514:
	movq	160(%r12), %r13
	testq	%r13, %r13
	je	.L2516
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2517
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2516:
	movq	120(%r12), %rdi
	leaq	136(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2518
	call	_ZdlPv@PLT
.L2518:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2519
	call	_ZdlPv@PLT
.L2519:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2520
	call	_ZdlPv@PLT
.L2520:
	movq	%r12, %rdi
	movl	$184, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L2515:
	.cfi_restore_state
	call	*%rax
	jmp	.L2514
	.p2align 4,,10
	.p2align 3
.L2517:
	call	*%rax
	jmp	.L2516
	.cfi_endproc
.LFE5673:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev:
.LFB14763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime10StackTraceE(%rip), %rax
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm1
	movq	%rsi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-8(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r12
	movups	%xmm0, -8(%rdi)
	testq	%r12, %r12
	je	.L2529
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2562
	movq	%r12, %rdi
	call	*%rax
.L2529:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2533
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2563
	call	*%rax
.L2533:
	movq	56(%rbx), %r15
	testq	%r15, %r15
	je	.L2535
	movq	8(%r15), %r12
	movq	(%r15), %r14
	cmpq	%r14, %r12
	je	.L2536
	.p2align 4,,10
	.p2align 3
.L2542:
	movq	(%r14), %r9
	testq	%r9, %r9
	je	.L2537
	movq	(%r9), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2564
	movq	%r9, %rdi
	call	*%rax
.L2537:
	addq	$8, %r14
	cmpq	%r14, %r12
	jne	.L2542
	movq	(%r15), %r14
.L2536:
	testq	%r14, %r14
	je	.L2543
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2543:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2535:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L2544
	call	_ZdlPv@PLT
.L2544:
	addq	$24, %rsp
	movq	%r13, %rdi
	movl	$88, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
.L2562:
	.cfi_restore_state
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm2
	leaq	80(%r12), %rax
	movq	%rdx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L2531
	call	_ZdlPv@PLT
.L2531:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2532
	call	_ZdlPv@PLT
.L2532:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2529
.L2564:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r9), %rdi
	movq	%rax, (%r9)
	leaq	104(%r9), %rax
	cmpq	%rax, %rdi
	je	.L2539
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r9
.L2539:
	movq	48(%r9), %rdi
	leaq	64(%r9), %rax
	cmpq	%rax, %rdi
	je	.L2540
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r9
.L2540:
	movq	8(%r9), %rdi
	leaq	24(%r9), %rax
	cmpq	%rax, %rdi
	je	.L2541
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r9
.L2541:
	movl	$136, %esi
	movq	%r9, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2537
.L2563:
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movq	-56(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
	jmp	.L2533
	.cfi_endproc
.LFE14763:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev:
.LFB5872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	80(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	64(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L2566
	call	_ZdlPv@PLT
.L2566:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L2565
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2565:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5872:
	.size	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev, .-_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD1Ev,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev:
.LFB14761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	72(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	movq	56(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L2570
	call	_ZdlPv@PLT
.L2570:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L2569
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2569:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14761:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev:
.LFB14760:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movups	%xmm0, -8(%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2574
	call	_ZdlPv@PLT
.L2574:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L2575
	call	_ZdlPv@PLT
.L2575:
	popq	%rbx
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14760:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.section	.text._ZN12v8_inspector16V8ConsoleMessageD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8ConsoleMessageD2Ev
	.type	_ZN12v8_inspector16V8ConsoleMessageD2Ev, @function
_ZN12v8_inspector16V8ConsoleMessageD2Ev:
.LFB7931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	200(%rdi), %rdi
	leaq	216(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2578
	call	_ZdlPv@PLT
.L2578:
	movq	160(%rbx), %rdi
	leaq	176(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2579
	call	_ZdlPv@PLT
.L2579:
	movq	144(%rbx), %r14
	movq	136(%rbx), %r12
	cmpq	%r12, %r14
	je	.L2580
	.p2align 4,,10
	.p2align 3
.L2583:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2581
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2582
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2582:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2581:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L2583
	movq	136(%rbx), %r12
.L2580:
	testq	%r12, %r12
	je	.L2584
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2584:
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2585
	movq	(%rdi), %rax
	call	*64(%rax)
.L2585:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2586
	call	_ZdlPv@PLT
.L2586:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L2577
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2577:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7931:
	.size	_ZN12v8_inspector16V8ConsoleMessageD2Ev, .-_ZN12v8_inspector16V8ConsoleMessageD2Ev
	.globl	_ZN12v8_inspector16V8ConsoleMessageD1Ev
	.set	_ZN12v8_inspector16V8ConsoleMessageD1Ev,_ZN12v8_inspector16V8ConsoleMessageD2Ev
	.section	.text._ZNK12v8_inspector16V8ConsoleMessage13wrapExceptionEPNS_22V8InspectorSessionImplEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8ConsoleMessage13wrapExceptionEPNS_22V8InspectorSessionImplEb
	.type	_ZNK12v8_inspector16V8ConsoleMessage13wrapExceptionEPNS_22V8InspectorSessionImplEb, @function
_ZNK12v8_inspector16V8ConsoleMessage13wrapExceptionEPNS_22V8InspectorSessionImplEb:
.LFB7945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	144(%rsi), %rax
	cmpq	%rax, 136(%rsi)
	je	.L2604
	movq	%rdx, %r14
	movl	116(%rsi), %edx
	movq	%rsi, %rbx
	testl	%edx, %edx
	jne	.L2618
.L2604:
	movq	$0, (%r12)
.L2602:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2619
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2618:
	.cfi_restore_state
	movl	16(%r14), %esi
	movq	24(%r14), %rdi
	movl	%ecx, %r13d
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii@PLT
	testq	%rax, %rax
	je	.L2604
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	leaq	-96(%rbp), %r15
	movzbl	%r13b, %r13d
	call	_ZNK12v8_inspector16InspectedContext7isolateEv@PLT
	leaq	-128(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, -152(%rbp)
	movq	%rdi, -136(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	136(%rbx), %rax
	movq	-144(%rbp), %r8
	movq	(%rax), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2607
	movq	-152(%rbp), %r9
	movq	(%rbx), %rsi
	movq	%r9, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-144(%rbp), %r8
	movq	%rax, %rbx
.L2607:
	movq	%r8, %rdi
	call	_ZNK12v8_inspector16InspectedContext7contextEv@PLT
	movq	%r12, %rdi
	movl	%r13d, %r9d
	movq	%r15, %r8
	movq	%rax, %rdx
	movq	%rbx, %rcx
	movq	%r14, %rsi
	call	_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2608
	call	_ZdlPv@PLT
.L2608:
	movq	-136(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L2602
.L2619:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7945:
	.size	_ZNK12v8_inspector16V8ConsoleMessage13wrapExceptionEPNS_22V8InspectorSessionImplEb, .-_ZNK12v8_inspector16V8ConsoleMessage13wrapExceptionEPNS_22V8InspectorSessionImplEb
	.section	.text._ZNK12v8_inspector16V8ConsoleMessage6originEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8ConsoleMessage6originEv
	.type	_ZNK12v8_inspector16V8ConsoleMessage6originEv, @function
_ZNK12v8_inspector16V8ConsoleMessage6originEv:
.LFB7946:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE7946:
	.size	_ZNK12v8_inspector16V8ConsoleMessage6originEv, .-_ZNK12v8_inspector16V8ConsoleMessage6originEv
	.section	.text._ZNK12v8_inspector16V8ConsoleMessage4typeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8ConsoleMessage4typeEv
	.type	_ZNK12v8_inspector16V8ConsoleMessage4typeEv, @function
_ZNK12v8_inspector16V8ConsoleMessage4typeEv:
.LFB7947:
	.cfi_startproc
	endbr64
	movl	120(%rdi), %eax
	ret
	.cfi_endproc
.LFE7947:
	.size	_ZNK12v8_inspector16V8ConsoleMessage4typeEv, .-_ZNK12v8_inspector16V8ConsoleMessage4typeEv
	.section	.text._ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi
	.type	_ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi, @function
_ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi:
.LFB7954:
	.cfi_startproc
	endbr64
	cmpl	%esi, 116(%rdi)
	jne	.L2622
	jmp	_ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi.part.0
	.p2align 4,,10
	.p2align 3
.L2622:
	ret
	.cfi_endproc
.LFE7954:
	.size	_ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi, .-_ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi
	.section	.text._ZN12v8_inspector23V8ConsoleMessageStorageC2EPNS_15V8InspectorImplEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorageC2EPNS_15V8InspectorImplEi
	.type	_ZN12v8_inspector23V8ConsoleMessageStorageC2EPNS_15V8InspectorImplEi, @function
_ZN12v8_inspector23V8ConsoleMessageStorageC2EPNS_15V8InspectorImplEi:
.LFB7968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	movl	$0, 12(%rdi)
	movq	$0, 16(%rdi)
	movq	$8, 24(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	movl	$64, %edi
	call	_Znwm@PLT
	movq	24(%rbx), %rdx
	movl	$512, %edi
	movq	%rax, 16(%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	call	_Znwm@PLT
	movq	%r12, 56(%rbx)
	movq	%rax, %xmm0
	movq	%rax, (%r12)
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 64(%rbx)
	leaq	104(%rbx), %rax
	movq	%r12, 88(%rbx)
	movq	%rdx, 48(%rbx)
	movq	%rdx, 80(%rbx)
	movl	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	%rax, 120(%rbx)
	movq	%rax, 128(%rbx)
	movq	$0, 136(%rbx)
	movups	%xmm0, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7968:
	.size	_ZN12v8_inspector23V8ConsoleMessageStorageC2EPNS_15V8InspectorImplEi, .-_ZN12v8_inspector23V8ConsoleMessageStorageC2EPNS_15V8InspectorImplEi
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorageC1EPNS_15V8InspectorImplEi
	.set	_ZN12v8_inspector23V8ConsoleMessageStorageC1EPNS_15V8InspectorImplEi,_ZN12v8_inspector23V8ConsoleMessageStorageC2EPNS_15V8InspectorImplEi
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_:
.LFB9490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	(%rdi), %rdi
	cmpq	%rax, %rsi
	je	.L2642
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L2643
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	16(%r12), %rdx
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
	testq	%rdi, %rdi
	je	.L2632
	movq	%rdi, (%rbx)
	movq	%rdx, 16(%rbx)
.L2630:
	xorl	%eax, %eax
	movq	$0, 8(%rbx)
	movw	%ax, (%rdi)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2643:
	.cfi_restore_state
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
.L2632:
	movq	%rsi, (%rbx)
	movq	%rsi, %rdi
	jmp	.L2630
	.p2align 4,,10
	.p2align 3
.L2642:
	movq	8(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L2628
	cmpq	$1, %rax
	je	.L2644
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L2628
	call	memmove@PLT
	movq	8(%rbx), %rax
	movq	(%r12), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L2628:
	xorl	%ecx, %ecx
	movq	%rax, 8(%r12)
	movw	%cx, (%rdi,%rdx)
	movq	(%rbx), %rdi
	jmp	.L2630
	.p2align 4,,10
	.p2align 3
.L2644:
	movzwl	16(%rbx), %eax
	movw	%ax, (%rdi)
	movq	8(%rbx), %rax
	movq	(%r12), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L2628
	.cfi_endproc
.LFE9490:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	.section	.text._ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_,"axG",@progbits,_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	.type	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_, @function
_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_:
.LFB9822:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %r9
	movq	8(%rdi), %r8
	movq	(%rsi), %rsi
	movq	(%rdi), %rcx
	cmpq	%r9, %r8
	movq	%r9, %rdx
	cmovbe	%r8, %rdx
	testq	%rdx, %rdx
	je	.L2646
	xorl	%eax, %eax
	jmp	.L2648
	.p2align 4,,10
	.p2align 3
.L2658:
	jb	.L2651
	addq	$1, %rax
	cmpq	%rdx, %rax
	je	.L2646
.L2648:
	movzwl	(%rcx,%rax,2), %edi
	cmpw	%di, (%rsi,%rax,2)
	jbe	.L2658
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2646:
	subq	%r9, %r8
	movl	$2147483647, %eax
	cmpq	$2147483647, %r8
	jg	.L2659
	cmpq	$-2147483648, %r8
	movl	$-2147483648, %eax
	cmovge	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2659:
	ret
	.p2align 4,,10
	.p2align 3
.L2651:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE9822:
	.size	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_, .-_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB10744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	8(%rdi), %rax
	movq	%rdi, -216(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rax, -208(%rbp)
	subq	%rdx, %rax
	movq	%rdx, -144(%rbp)
	sarq	$3, %rax
	movabsq	$1152921504606846975, %rdx
	movq	%rsi, -128(%rbp)
	cmpq	%rdx, %rax
	je	.L3789
	movq	%rsi, %r13
	subq	-144(%rbp), %r13
	testq	%rax, %rax
	je	.L3187
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L3790
.L2662:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -168(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -200(%rbp)
	leaq	8(%rax), %rbx
.L3186:
	movq	(%r12), %rax
	movq	-168(%rbp), %rcx
	movq	$0, (%r12)
	movq	-144(%rbp), %r14
	movq	%rax, (%rcx,%r13)
	cmpq	%r14, -96(%rbp)
	je	.L2664
	movq	%rcx, -88(%rbp)
	movq	%r14, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L3180:
	movq	-56(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	(%rcx), %rax
	movq	$0, (%rcx)
	movq	%rax, (%rdx)
	movq	(%rcx), %rbx
	testq	%rbx, %rbx
	je	.L2665
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2666
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	movq	312(%rbx), %r12
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm2
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	testq	%r12, %r12
	je	.L2667
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2668
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2669
	call	_ZdlPv@PLT
.L2669:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2670
	call	_ZdlPv@PLT
.L2670:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2667:
	movq	304(%rbx), %rsi
	movq	%rsi, -80(%rbp)
	testq	%rsi, %rsi
	je	.L2671
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2672
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L2673
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -136(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L2674
	movq	%rbx, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L3006:
	movq	-64(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -104(%rbp)
	testq	%rcx, %rcx
	je	.L2675
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2676
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	16(%rcx), %rcx
	movq	%rcx, -112(%rbp)
	testq	%rcx, %rcx
	je	.L2677
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2678
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L2679
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -72(%rbp)
	cmpq	%rax, %rbx
	je	.L2680
	.p2align 4,,10
	.p2align 3
.L2787:
	movq	-72(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2681
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2682
	movq	16(%rbx), %rcx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rcx, -120(%rbp)
	testq	%rcx, %rcx
	je	.L2683
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2684
	movq	160(%rcx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%r12, %r12
	je	.L2685
	movq	8(%r12), %r15
	movq	(%r12), %r14
	cmpq	%r14, %r15
	je	.L2686
	movq	%r12, -176(%rbp)
	jmp	.L2693
	.p2align 4,,10
	.p2align 3
.L3792:
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L2689
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2690
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2689:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L2691
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2692
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2691:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2687:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3791
.L2693:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L2687
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3792
	addq	$8, %r14
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2693
	.p2align 4,,10
	.p2align 3
.L3791:
	movq	-176(%rbp), %r12
	movq	(%r12), %r14
.L2686:
	testq	%r14, %r14
	je	.L2694
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2694:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2685:
	movq	-120(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2695
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -176(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2696
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2730:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2697
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2698
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2699
	call	_ZdlPv@PLT
.L2699:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2700
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2701
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2702
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2703
	movq	%rbx, -248(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -224(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2710
	.p2align 4,,10
	.p2align 3
.L3794:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2706
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2707
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2706:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2708
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2709
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2708:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2704:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3793
.L2710:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2704
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3794
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2710
	.p2align 4,,10
	.p2align 3
.L3793:
	movq	-224(%rbp), %r12
	movq	-248(%rbp), %rbx
	movq	-256(%rbp), %r13
	movq	(%r14), %r15
.L2703:
	testq	%r15, %r15
	je	.L2711
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2711:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2702:
	movq	152(%r13), %rax
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L2712
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2713
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2722
	.p2align 4,,10
	.p2align 3
.L3796:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2716
	call	_ZdlPv@PLT
.L2716:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2717
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2718
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2717:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2719
	call	_ZdlPv@PLT
.L2719:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2720
	call	_ZdlPv@PLT
.L2720:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2721
	call	_ZdlPv@PLT
.L2721:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2714:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3795
.L2722:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2714
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3796
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2722
	.p2align 4,,10
	.p2align 3
.L3795:
	movq	-224(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L2713:
	testq	%r14, %r14
	je	.L2723
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2723:
	movq	-224(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2712:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2724
	call	_ZdlPv@PLT
.L2724:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2725
	call	_ZdlPv@PLT
.L2725:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2726
	call	_ZdlPv@PLT
.L2726:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2700:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2727
	call	_ZdlPv@PLT
.L2727:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2728
	call	_ZdlPv@PLT
.L2728:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2729
	call	_ZdlPv@PLT
.L2729:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2697:
	addq	$8, %rbx
	cmpq	%rbx, -176(%rbp)
	jne	.L2730
.L3847:
	movq	-192(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2696:
	testq	%rdi, %rdi
	je	.L2731
	call	_ZdlPv@PLT
.L2731:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2695:
	movq	-120(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2732
	call	_ZdlPv@PLT
.L2732:
	movq	-120(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2733
	call	_ZdlPv@PLT
.L2733:
	movq	-120(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2734
	call	_ZdlPv@PLT
.L2734:
	movq	-120(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2683:
	movq	8(%rbx), %rdx
	movq	%rdx, -120(%rbp)
	testq	%rdx, %rdx
	je	.L2735
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2736
	movq	160(%rdx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	testq	%r12, %r12
	je	.L2737
	movq	8(%r12), %r15
	movq	(%r12), %r14
	cmpq	%r14, %r15
	je	.L2738
	movq	%r12, -176(%rbp)
	jmp	.L2745
	.p2align 4,,10
	.p2align 3
.L3798:
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L2741
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2742
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2741:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L2743
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2744
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2743:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2739:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3797
.L2745:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L2739
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3798
	addq	$8, %r14
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2745
	.p2align 4,,10
	.p2align 3
.L3797:
	movq	-176(%rbp), %r12
	movq	(%r12), %r14
.L2738:
	testq	%r14, %r14
	je	.L2746
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2746:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2737:
	movq	-120(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2747
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -176(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2748
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2782:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2749
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2750
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2751
	call	_ZdlPv@PLT
.L2751:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2752
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2753
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2754
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2755
	movq	%rbx, -248(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -224(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2762
	.p2align 4,,10
	.p2align 3
.L3800:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2758
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2759
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2758:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2760
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2761
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2760:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2756:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3799
.L2762:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2756
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3800
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2762
	.p2align 4,,10
	.p2align 3
.L3799:
	movq	-224(%rbp), %r12
	movq	-248(%rbp), %rbx
	movq	-256(%rbp), %r13
	movq	(%r14), %r15
.L2755:
	testq	%r15, %r15
	je	.L2763
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2763:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2754:
	movq	152(%r13), %rax
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L2764
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2765
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2774
	.p2align 4,,10
	.p2align 3
.L3802:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2768
	call	_ZdlPv@PLT
.L2768:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2769
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2770
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2769:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2771
	call	_ZdlPv@PLT
.L2771:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2772
	call	_ZdlPv@PLT
.L2772:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2773
	call	_ZdlPv@PLT
.L2773:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2766:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3801
.L2774:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2766
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3802
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2774
	.p2align 4,,10
	.p2align 3
.L3801:
	movq	-224(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L2765:
	testq	%r14, %r14
	je	.L2775
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2775:
	movq	-224(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2764:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2776
	call	_ZdlPv@PLT
.L2776:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2777
	call	_ZdlPv@PLT
.L2777:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2778
	call	_ZdlPv@PLT
.L2778:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2752:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2779
	call	_ZdlPv@PLT
.L2779:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2780
	call	_ZdlPv@PLT
.L2780:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2781
	call	_ZdlPv@PLT
.L2781:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2749:
	addq	$8, %rbx
	cmpq	%rbx, -176(%rbp)
	jne	.L2782
.L3848:
	movq	-192(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2748:
	testq	%rdi, %rdi
	je	.L2783
	call	_ZdlPv@PLT
.L2783:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2747:
	movq	-120(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2784
	call	_ZdlPv@PLT
.L2784:
	movq	-120(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2785
	call	_ZdlPv@PLT
.L2785:
	movq	-120(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2786
	call	_ZdlPv@PLT
.L2786:
	movq	-120(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2735:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2681:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rax, -152(%rbp)
	jne	.L2787
	movq	-184(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
.L2680:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L2788
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2788:
	movq	-184(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2679:
	movq	-112(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2789
	movq	8(%rax), %rsi
	movq	(%rax), %rbx
	movq	%rsi, -72(%rbp)
	cmpq	%rbx, %rsi
	je	.L2790
	.p2align 4,,10
	.p2align 3
.L2824:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2791
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2792
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2793
	call	_ZdlPv@PLT
.L2793:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2794
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2795
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2796
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2797
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -152(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2804
	.p2align 4,,10
	.p2align 3
.L3804:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2800
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2801
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2800:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2802
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2803
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2802:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2798:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3803
.L2804:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2798
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3804
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2804
	.p2align 4,,10
	.p2align 3
.L3803:
	movq	-152(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2797:
	testq	%r15, %r15
	je	.L2805
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2805:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2796:
	movq	152(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2806
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2807
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2816
	.p2align 4,,10
	.p2align 3
.L3806:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2810
	call	_ZdlPv@PLT
.L2810:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2811
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2812
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2811:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2813
	call	_ZdlPv@PLT
.L2813:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2814
	call	_ZdlPv@PLT
.L2814:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2815
	call	_ZdlPv@PLT
.L2815:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2808:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3805
.L2816:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2808
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3806
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2816
	.p2align 4,,10
	.p2align 3
.L3805:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2807:
	testq	%r14, %r14
	je	.L2817
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2817:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2806:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2818
	call	_ZdlPv@PLT
.L2818:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2819
	call	_ZdlPv@PLT
.L2819:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2820
	call	_ZdlPv@PLT
.L2820:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2794:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2821
	call	_ZdlPv@PLT
.L2821:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2822
	call	_ZdlPv@PLT
.L2822:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2823
	call	_ZdlPv@PLT
.L2823:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2791:
	addq	$8, %rbx
	cmpq	%rbx, -72(%rbp)
	jne	.L2824
.L3845:
	movq	-120(%rbp), %rax
	movq	(%rax), %rbx
.L2790:
	testq	%rbx, %rbx
	je	.L2825
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L2825:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2789:
	movq	-112(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2826
	call	_ZdlPv@PLT
.L2826:
	movq	-112(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2827
	call	_ZdlPv@PLT
.L2827:
	movq	-112(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2828
	call	_ZdlPv@PLT
.L2828:
	movq	-112(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2677:
	movq	-104(%rbp), %rax
	movq	8(%rax), %rdx
	movq	%rdx, -112(%rbp)
	testq	%rdx, %rdx
	je	.L2829
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2830
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L2831
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -72(%rbp)
	cmpq	%rax, %rbx
	je	.L2832
	.p2align 4,,10
	.p2align 3
.L2939:
	movq	-72(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2833
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2834
	movq	16(%rbx), %rcx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rcx, -120(%rbp)
	testq	%rcx, %rcx
	je	.L2835
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2836
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2837
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -176(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L2838
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2895:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2839
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2840
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2841
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2842
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L2843
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L2844
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -248(%rbp)
	jmp	.L2851
	.p2align 4,,10
	.p2align 3
.L3808:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2847
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2848
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2847:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2849
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2850
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2849:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2845:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3807
.L2851:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2845
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3808
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L2851
	.p2align 4,,10
	.p2align 3
.L3807:
	movq	-224(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L2844:
	testq	%r14, %r14
	je	.L2852
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2852:
	movq	-224(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2843:
	movq	152(%r13), %rax
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L2853
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2854
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2863
	.p2align 4,,10
	.p2align 3
.L3810:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2857
	call	_ZdlPv@PLT
.L2857:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2858
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2859
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2858:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2860
	call	_ZdlPv@PLT
.L2860:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2861
	call	_ZdlPv@PLT
.L2861:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2862
	call	_ZdlPv@PLT
.L2862:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2855:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3809
.L2863:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2855
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3810
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2863
	.p2align 4,,10
	.p2align 3
.L3809:
	movq	-224(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L2854:
	testq	%r14, %r14
	je	.L2864
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2864:
	movq	-224(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2853:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2865
	call	_ZdlPv@PLT
.L2865:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2866
	call	_ZdlPv@PLT
.L2866:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2867
	call	_ZdlPv@PLT
.L2867:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2841:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2868
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2869
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L2870
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L2871
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -248(%rbp)
	jmp	.L2878
	.p2align 4,,10
	.p2align 3
.L3812:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2874
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2875
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2874:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2876
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2877
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2876:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2872:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3811
.L2878:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2872
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3812
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L2878
	.p2align 4,,10
	.p2align 3
.L3811:
	movq	-224(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L2871:
	testq	%r14, %r14
	je	.L2879
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2879:
	movq	-224(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2870:
	movq	152(%r13), %rax
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L2880
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2881
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2890
	.p2align 4,,10
	.p2align 3
.L3814:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2884
	call	_ZdlPv@PLT
.L2884:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2885
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2886
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2885:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2887
	call	_ZdlPv@PLT
.L2887:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2888
	call	_ZdlPv@PLT
.L2888:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2889
	call	_ZdlPv@PLT
.L2889:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2882:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3813
.L2890:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2882
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3814
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2890
	.p2align 4,,10
	.p2align 3
.L3813:
	movq	-224(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L2881:
	testq	%r14, %r14
	je	.L2891
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2891:
	movq	-224(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2880:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2892
	call	_ZdlPv@PLT
.L2892:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2893
	call	_ZdlPv@PLT
.L2893:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2894
	call	_ZdlPv@PLT
.L2894:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2868:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2839:
	addq	$8, %rbx
	cmpq	%rbx, -176(%rbp)
	jne	.L2895
.L3849:
	movq	-192(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2838:
	testq	%rdi, %rdi
	je	.L2896
	call	_ZdlPv@PLT
.L2896:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2837:
	movq	-120(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L2897
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L2898
	movq	%r13, -176(%rbp)
	jmp	.L2907
	.p2align 4,,10
	.p2align 3
.L3816:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2901
	call	_ZdlPv@PLT
.L2901:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L2902
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2903
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2902:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2904
	call	_ZdlPv@PLT
.L2904:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2905
	call	_ZdlPv@PLT
.L2905:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2906
	call	_ZdlPv@PLT
.L2906:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2899:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L3815
.L2907:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L2899
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3816
	addq	$8, %r12
	movq	%r14, %rdi
	call	*%rax
	cmpq	%r12, %r15
	jne	.L2907
	.p2align 4,,10
	.p2align 3
.L3815:
	movq	-176(%rbp), %r13
	movq	0(%r13), %r12
.L2898:
	testq	%r12, %r12
	je	.L2908
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2908:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2897:
	movq	-120(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2909
	call	_ZdlPv@PLT
.L2909:
	movq	-120(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2910
	call	_ZdlPv@PLT
.L2910:
	movq	-120(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2911
	call	_ZdlPv@PLT
.L2911:
	movq	-120(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2835:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L2912
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2913
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2914
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	cmpq	%r15, %rax
	je	.L2915
	movq	%rbx, -120(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -176(%rbp)
	jmp	.L2922
	.p2align 4,,10
	.p2align 3
.L3818:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L2918
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2919
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2918:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L2920
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2921
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2920:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2916:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3817
.L2922:
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.L2916
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3818
	addq	$8, %rbx
	movq	%r14, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2922
	.p2align 4,,10
	.p2align 3
.L3817:
	movq	-120(%rbp), %rbx
	movq	-176(%rbp), %r12
	movq	0(%r13), %r15
.L2915:
	testq	%r15, %r15
	je	.L2923
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2923:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2914:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L2924
	movq	8(%r14), %r15
	movq	(%r14), %r13
	cmpq	%r13, %r15
	je	.L2925
	movq	%r12, -120(%rbp)
	movq	%r14, -176(%rbp)
	jmp	.L2934
	.p2align 4,,10
	.p2align 3
.L3820:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2928
	call	_ZdlPv@PLT
.L2928:
	movq	136(%r12), %r14
	testq	%r14, %r14
	je	.L2929
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2930
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2929:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2931
	call	_ZdlPv@PLT
.L2931:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2932
	call	_ZdlPv@PLT
.L2932:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2933
	call	_ZdlPv@PLT
.L2933:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2926:
	addq	$8, %r13
	cmpq	%r13, %r15
	je	.L3819
.L2934:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L2926
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3820
	addq	$8, %r13
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r13, %r15
	jne	.L2934
	.p2align 4,,10
	.p2align 3
.L3819:
	movq	-176(%rbp), %r14
	movq	-120(%rbp), %r12
	movq	(%r14), %r13
.L2925:
	testq	%r13, %r13
	je	.L2935
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2935:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2924:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2936
	call	_ZdlPv@PLT
.L2936:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2937
	call	_ZdlPv@PLT
.L2937:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2938
	call	_ZdlPv@PLT
.L2938:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2912:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2833:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rax, -152(%rbp)
	jne	.L2939
	movq	-184(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
.L2832:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L2940
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2940:
	movq	-184(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2831:
	movq	-112(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L2941
	movq	8(%rax), %rdx
	movq	(%rax), %rbx
	movq	%rdx, -152(%rbp)
	cmpq	%rbx, %rdx
	je	.L2942
	movq	%rbx, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L3001:
	movq	-72(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2943
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2944
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2945
	call	_ZdlPv@PLT
.L2945:
	movq	136(%rbx), %rsi
	movq	%rsi, -120(%rbp)
	testq	%rsi, %rsi
	je	.L2946
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2947
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2948
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -176(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2949
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2981:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2950
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2951
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2952
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2953
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2952:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2954
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2955
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2956
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2957
	movq	%rbx, -248(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -224(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2964
	.p2align 4,,10
	.p2align 3
.L3822:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2960
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2961
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2960:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2962
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2963
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2962:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2958:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3821
.L2964:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2958
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3822
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2964
	.p2align 4,,10
	.p2align 3
.L3821:
	movq	-224(%rbp), %r12
	movq	-248(%rbp), %rbx
	movq	-256(%rbp), %r13
	movq	(%r14), %r15
.L2957:
	testq	%r15, %r15
	je	.L2965
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2965:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2956:
	movq	152(%r13), %rax
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L2966
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2967
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2976
	.p2align 4,,10
	.p2align 3
.L3824:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2970
	call	_ZdlPv@PLT
.L2970:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2971
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2972
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2971:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2973
	call	_ZdlPv@PLT
.L2973:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2974
	call	_ZdlPv@PLT
.L2974:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2975
	call	_ZdlPv@PLT
.L2975:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2968:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3823
.L2976:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2968
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3824
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2976
	.p2align 4,,10
	.p2align 3
.L3823:
	movq	-224(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L2967:
	testq	%r14, %r14
	je	.L2977
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2977:
	movq	-224(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2966:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2978
	call	_ZdlPv@PLT
.L2978:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2979
	call	_ZdlPv@PLT
.L2979:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2980
	call	_ZdlPv@PLT
.L2980:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2954:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2950:
	addq	$8, %rbx
	cmpq	%rbx, -176(%rbp)
	jne	.L2981
.L3851:
	movq	-192(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2949:
	testq	%rdi, %rdi
	je	.L2982
	call	_ZdlPv@PLT
.L2982:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2948:
	movq	-120(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L2983
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L2984
	movq	%r13, -176(%rbp)
	jmp	.L2993
	.p2align 4,,10
	.p2align 3
.L3826:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2987
	call	_ZdlPv@PLT
.L2987:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L2988
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2989
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2988:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2990
	call	_ZdlPv@PLT
.L2990:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2991
	call	_ZdlPv@PLT
.L2991:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2992
	call	_ZdlPv@PLT
.L2992:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2985:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L3825
.L2993:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L2985
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3826
	addq	$8, %r12
	movq	%r14, %rdi
	call	*%rax
	cmpq	%r12, %r15
	jne	.L2993
	.p2align 4,,10
	.p2align 3
.L3825:
	movq	-176(%rbp), %r13
	movq	0(%r13), %r12
.L2984:
	testq	%r12, %r12
	je	.L2994
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2994:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2983:
	movq	-120(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2995
	call	_ZdlPv@PLT
.L2995:
	movq	-120(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2996
	call	_ZdlPv@PLT
.L2996:
	movq	-120(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2997
	call	_ZdlPv@PLT
.L2997:
	movq	-120(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2946:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2998
	call	_ZdlPv@PLT
.L2998:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2999
	call	_ZdlPv@PLT
.L2999:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3000
	call	_ZdlPv@PLT
.L3000:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2943:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rax, -152(%rbp)
	jne	.L3001
	movq	-184(%rbp), %rax
	movq	(%rax), %rbx
.L2942:
	testq	%rbx, %rbx
	je	.L3002
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L3002:
	movq	-184(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2941:
	movq	-112(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L3003
	call	_ZdlPv@PLT
.L3003:
	movq	-112(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L3004
	call	_ZdlPv@PLT
.L3004:
	movq	-112(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L3005
	call	_ZdlPv@PLT
.L3005:
	movq	-112(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2829:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2675:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L3006
	movq	-160(%rbp), %rax
	movq	-232(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L2674:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L3007
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3007:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2673:
	movq	-80(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L3008
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -120(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rsi
	je	.L3009
	movq	%rbx, -224(%rbp)
	.p2align 4,,10
	.p2align 3
.L3168:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3010
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3011
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3012
	call	_ZdlPv@PLT
.L3012:
	movq	136(%rbx), %rsi
	movq	%rsi, -104(%rbp)
	testq	%rsi, %rsi
	je	.L3013
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3014
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L3015
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -136(%rbp)
	movq	%rax, -72(%rbp)
	cmpq	%rax, %rcx
	je	.L3016
	movq	%rbx, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L3123:
	movq	-72(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3017
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3018
	movq	16(%rbx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L3019
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3020
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3021
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	cmpq	%r15, %rax
	je	.L3022
	movq	%rbx, -112(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -160(%rbp)
	jmp	.L3029
	.p2align 4,,10
	.p2align 3
.L3828:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L3025
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3026
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3025:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L3027
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3028
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3027:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3023:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3827
.L3029:
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.L3023
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3828
	addq	$8, %rbx
	movq	%r14, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L3029
	.p2align 4,,10
	.p2align 3
.L3827:
	movq	-112(%rbp), %rbx
	movq	-160(%rbp), %r12
	movq	0(%r13), %r15
.L3022:
	testq	%r15, %r15
	je	.L3030
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3030:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3021:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L3031
	movq	8(%r14), %r15
	movq	(%r14), %r13
	cmpq	%r13, %r15
	je	.L3032
	movq	%r12, -112(%rbp)
	movq	%r14, -160(%rbp)
	jmp	.L3041
	.p2align 4,,10
	.p2align 3
.L3830:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3035
	call	_ZdlPv@PLT
.L3035:
	movq	136(%r12), %r14
	testq	%r14, %r14
	je	.L3036
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3037
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3036:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3038
	call	_ZdlPv@PLT
.L3038:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3039
	call	_ZdlPv@PLT
.L3039:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3040
	call	_ZdlPv@PLT
.L3040:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3033:
	addq	$8, %r13
	cmpq	%r13, %r15
	je	.L3829
.L3041:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L3033
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3830
	addq	$8, %r13
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r13, %r15
	jne	.L3041
	.p2align 4,,10
	.p2align 3
.L3829:
	movq	-160(%rbp), %r14
	movq	-112(%rbp), %r12
	movq	(%r14), %r13
.L3032:
	testq	%r13, %r13
	je	.L3042
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3042:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3031:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3043
	call	_ZdlPv@PLT
.L3043:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3044
	call	_ZdlPv@PLT
.L3044:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3045
	call	_ZdlPv@PLT
.L3045:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3019:
	movq	8(%rbx), %rdx
	movq	%rdx, -112(%rbp)
	testq	%rdx, %rdx
	je	.L3046
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3047
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L3048
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -160(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L3049
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3106:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3050
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3051
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3052
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3053
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L3054
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L3055
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -248(%rbp)
	jmp	.L3062
	.p2align 4,,10
	.p2align 3
.L3832:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3058
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3059
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3058:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3060
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3061
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3060:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3056:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3831
.L3062:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3056
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3832
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L3062
	.p2align 4,,10
	.p2align 3
.L3831:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L3055:
	testq	%r14, %r14
	je	.L3063
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3063:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3054:
	movq	152(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L3064
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3065
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3074
	.p2align 4,,10
	.p2align 3
.L3834:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3068
	call	_ZdlPv@PLT
.L3068:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3069
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3070
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3069:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3071
	call	_ZdlPv@PLT
.L3071:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3072
	call	_ZdlPv@PLT
.L3072:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3073
	call	_ZdlPv@PLT
.L3073:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3066:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3833
.L3074:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3066
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3834
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3074
	.p2align 4,,10
	.p2align 3
.L3833:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L3065:
	testq	%r14, %r14
	je	.L3075
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3075:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3064:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3076
	call	_ZdlPv@PLT
.L3076:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3077
	call	_ZdlPv@PLT
.L3077:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3078
	call	_ZdlPv@PLT
.L3078:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3052:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3079
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3080
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L3081
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L3082
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -248(%rbp)
	jmp	.L3089
	.p2align 4,,10
	.p2align 3
.L3836:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3085
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3086
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3085:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3087
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3088
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3087:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3083:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3835
.L3089:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3083
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3836
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L3089
	.p2align 4,,10
	.p2align 3
.L3835:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L3082:
	testq	%r14, %r14
	je	.L3090
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3090:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3081:
	movq	152(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L3091
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3092
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3101
	.p2align 4,,10
	.p2align 3
.L3838:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3095
	call	_ZdlPv@PLT
.L3095:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3096
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3097
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3096:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3098
	call	_ZdlPv@PLT
.L3098:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3099
	call	_ZdlPv@PLT
.L3099:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3100
	call	_ZdlPv@PLT
.L3100:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3093:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3837
.L3101:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3093
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3838
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3101
	.p2align 4,,10
	.p2align 3
.L3837:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L3092:
	testq	%r14, %r14
	je	.L3102
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3102:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3091:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3103
	call	_ZdlPv@PLT
.L3103:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3104
	call	_ZdlPv@PLT
.L3104:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3105
	call	_ZdlPv@PLT
.L3105:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3079:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3050:
	addq	$8, %rbx
	cmpq	%rbx, -160(%rbp)
	jne	.L3106
.L3850:
	movq	-184(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3049:
	testq	%rdi, %rdi
	je	.L3107
	call	_ZdlPv@PLT
.L3107:
	movq	-184(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3048:
	movq	-112(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L3108
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L3109
	movq	%r13, -160(%rbp)
	jmp	.L3118
	.p2align 4,,10
	.p2align 3
.L3840:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3112
	call	_ZdlPv@PLT
.L3112:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L3113
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3114
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3113:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3115
	call	_ZdlPv@PLT
.L3115:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3116
	call	_ZdlPv@PLT
.L3116:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3117
	call	_ZdlPv@PLT
.L3117:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3110:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L3839
.L3118:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L3110
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3840
	addq	$8, %r12
	movq	%r14, %rdi
	call	*%rax
	cmpq	%r12, %r15
	jne	.L3118
	.p2align 4,,10
	.p2align 3
.L3839:
	movq	-160(%rbp), %r13
	movq	0(%r13), %r12
.L3109:
	testq	%r12, %r12
	je	.L3119
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3119:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3108:
	movq	-112(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L3120
	call	_ZdlPv@PLT
.L3120:
	movq	-112(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L3121
	call	_ZdlPv@PLT
.L3121:
	movq	-112(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L3122
	call	_ZdlPv@PLT
.L3122:
	movq	-112(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3046:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3017:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L3123
	movq	-176(%rbp), %rax
	movq	-232(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
.L3016:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L3124
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3124:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3015:
	movq	-104(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3125
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L3126
	movq	%rbx, -160(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3160:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3127
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3128
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3129
	call	_ZdlPv@PLT
.L3129:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3130
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3131
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3132
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L3133
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -136(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L3140
	.p2align 4,,10
	.p2align 3
.L3842:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3136
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3137
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3136:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3138
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3139
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3138:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3134:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3841
.L3140:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3134
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3842
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L3140
	.p2align 4,,10
	.p2align 3
.L3841:
	movq	-136(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L3133:
	testq	%r15, %r15
	je	.L3141
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3141:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3132:
	movq	152(%r13), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L3142
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3143
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L3152
	.p2align 4,,10
	.p2align 3
.L3844:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3146
	call	_ZdlPv@PLT
.L3146:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3147
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3148
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3147:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3149
	call	_ZdlPv@PLT
.L3149:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3150
	call	_ZdlPv@PLT
.L3150:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3151
	call	_ZdlPv@PLT
.L3151:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3144:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3843
.L3152:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3144
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3844
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3152
	.p2align 4,,10
	.p2align 3
.L3843:
	movq	-136(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L3143:
	testq	%r14, %r14
	je	.L3153
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3153:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3142:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3154
	call	_ZdlPv@PLT
.L3154:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3155
	call	_ZdlPv@PLT
.L3155:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3156
	call	_ZdlPv@PLT
.L3156:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3130:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3157
	call	_ZdlPv@PLT
.L3157:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3158
	call	_ZdlPv@PLT
.L3158:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3159
	call	_ZdlPv@PLT
.L3159:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3127:
	addq	$8, %rbx
	cmpq	%rbx, -72(%rbp)
	jne	.L3160
.L3846:
	movq	-112(%rbp), %rax
	movq	-160(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3126:
	testq	%rdi, %rdi
	je	.L3161
	call	_ZdlPv@PLT
.L3161:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3125:
	movq	-104(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L3162
	call	_ZdlPv@PLT
.L3162:
	movq	-104(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L3163
	call	_ZdlPv@PLT
.L3163:
	movq	-104(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L3164
	call	_ZdlPv@PLT
.L3164:
	movq	-104(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3013:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3165
	call	_ZdlPv@PLT
.L3165:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3166
	call	_ZdlPv@PLT
.L3166:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3167
	call	_ZdlPv@PLT
.L3167:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3010:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	jne	.L3168
	movq	-152(%rbp), %rax
	movq	-224(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L3009:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L3169
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3169:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3008:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L3170
	call	_ZdlPv@PLT
.L3170:
	movq	-80(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L3171
	call	_ZdlPv@PLT
.L3171:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L3172
	call	_ZdlPv@PLT
.L3172:
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2671:
	movq	264(%rbx), %rdi
	leaq	280(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3173
	call	_ZdlPv@PLT
.L3173:
	movq	216(%rbx), %rdi
	leaq	232(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3174
	call	_ZdlPv@PLT
.L3174:
	movq	168(%rbx), %rdi
	leaq	184(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3175
	call	_ZdlPv@PLT
.L3175:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3176
	movq	(%rdi), %rax
	call	*24(%rax)
.L3176:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3177
	call	_ZdlPv@PLT
.L3177:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3178
	call	_ZdlPv@PLT
.L3178:
	movq	16(%rbx), %rdi
	leaq	32(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3179
	call	_ZdlPv@PLT
.L3179:
	movl	$320, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2665:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	addq	$8, -88(%rbp)
	cmpq	%rax, -96(%rbp)
	jne	.L3180
	movq	-168(%rbp), %rdx
	movq	-96(%rbp), %rax
	subq	-144(%rbp), %rax
	leaq	8(%rdx,%rax), %rbx
.L2664:
	movq	-96(%rbp), %rdi
	movq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3181
	subq	%rdi, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L3189
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L3183:
	movdqu	(%rdi,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L3183
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	-96(%rbp), %rdi
	movq	%rdi, -128(%rbp)
	cmpq	%rcx, %rax
	je	.L3184
.L3182:
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L3184:
	leaq	8(%rbx,%rsi), %rbx
.L3181:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L3185
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3185:
	movq	-168(%rbp), %xmm0
	movq	-216(%rbp), %rax
	movq	%rbx, %xmm3
	movq	-200(%rbp), %rdx
	punpcklqdq	%xmm3, %xmm0
	movq	%rdx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2666:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2665
	.p2align 4,,10
	.p2align 3
.L3011:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L3010
	.p2align 4,,10
	.p2align 3
.L2676:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2675
	.p2align 4,,10
	.p2align 3
.L2668:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2667
	.p2align 4,,10
	.p2align 3
.L2672:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L2671
	.p2align 4,,10
	.p2align 3
.L2834:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2833
	.p2align 4,,10
	.p2align 3
.L2682:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2681
	.p2align 4,,10
	.p2align 3
.L2792:
	movq	%r12, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%rbx, -72(%rbp)
	jne	.L2824
	jmp	.L3845
	.p2align 4,,10
	.p2align 3
.L3790:
	testq	%rcx, %rcx
	jne	.L2663
	movq	$0, -200(%rbp)
	movl	$8, %ebx
	movq	$0, -168(%rbp)
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3018:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L3017
	.p2align 4,,10
	.p2align 3
.L2944:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2943
	.p2align 4,,10
	.p2align 3
.L3128:
	movq	%r12, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%rbx, -72(%rbp)
	jne	.L3160
	jmp	.L3846
	.p2align 4,,10
	.p2align 3
.L2830:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L2829
	.p2align 4,,10
	.p2align 3
.L2678:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2677
	.p2align 4,,10
	.p2align 3
.L3014:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L3013
	.p2align 4,,10
	.p2align 3
.L2698:
	movq	%r12, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%rbx, -176(%rbp)
	jne	.L2730
	jmp	.L3847
	.p2align 4,,10
	.p2align 3
.L2750:
	movq	%r12, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%rbx, -176(%rbp)
	jne	.L2782
	jmp	.L3848
	.p2align 4,,10
	.p2align 3
.L2840:
	movq	%r12, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%rbx, -176(%rbp)
	jne	.L2895
	jmp	.L3849
	.p2align 4,,10
	.p2align 3
.L3051:
	movq	%r12, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%rbx, -160(%rbp)
	jne	.L3106
	jmp	.L3850
	.p2align 4,,10
	.p2align 3
.L2951:
	movq	%r12, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%rbx, -176(%rbp)
	jne	.L2981
	jmp	.L3851
	.p2align 4,,10
	.p2align 3
.L3187:
	movl	$8, %ebx
	jmp	.L2662
	.p2align 4,,10
	.p2align 3
.L2736:
	movq	%rdx, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2681
	.p2align 4,,10
	.p2align 3
.L2836:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2835
	.p2align 4,,10
	.p2align 3
.L2684:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2683
	.p2align 4,,10
	.p2align 3
.L3047:
	movq	%rdx, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3017
	.p2align 4,,10
	.p2align 3
.L2795:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2794
	.p2align 4,,10
	.p2align 3
.L3020:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3019
	.p2align 4,,10
	.p2align 3
.L2947:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L2946
	.p2align 4,,10
	.p2align 3
.L3131:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3130
	.p2align 4,,10
	.p2align 3
.L2913:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2833
	.p2align 4,,10
	.p2align 3
.L2742:
	call	*%rax
	jmp	.L2741
	.p2align 4,,10
	.p2align 3
.L3139:
	call	*%rdx
	jmp	.L3138
	.p2align 4,,10
	.p2align 3
.L2842:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2841
	.p2align 4,,10
	.p2align 3
.L2753:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2752
	.p2align 4,,10
	.p2align 3
.L2955:
	movq	%r13, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2950
	.p2align 4,,10
	.p2align 3
.L3148:
	call	*%rax
	jmp	.L3147
	.p2align 4,,10
	.p2align 3
.L3037:
	call	*%rax
	jmp	.L3036
	.p2align 4,,10
	.p2align 3
.L3028:
	call	*%rdx
	jmp	.L3027
	.p2align 4,,10
	.p2align 3
.L2803:
	call	*%rdx
	jmp	.L2802
	.p2align 4,,10
	.p2align 3
.L2989:
	call	*%rax
	jmp	.L2988
	.p2align 4,,10
	.p2align 3
.L2919:
	call	*%rdx
	jmp	.L2918
	.p2align 4,,10
	.p2align 3
.L2801:
	call	*%rdx
	jmp	.L2800
	.p2align 4,,10
	.p2align 3
.L2921:
	call	*%rdx
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L2744:
	call	*%rax
	jmp	.L2743
	.p2align 4,,10
	.p2align 3
.L2869:
	movq	%r13, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2839
	.p2align 4,,10
	.p2align 3
.L3026:
	call	*%rdx
	jmp	.L3025
	.p2align 4,,10
	.p2align 3
.L2903:
	call	*%rax
	jmp	.L2902
	.p2align 4,,10
	.p2align 3
.L2690:
	call	*%rax
	jmp	.L2689
	.p2align 4,,10
	.p2align 3
.L2953:
	call	*%rax
	jmp	.L2952
	.p2align 4,,10
	.p2align 3
.L3080:
	movq	%r13, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3050
	.p2align 4,,10
	.p2align 3
.L2812:
	call	*%rax
	jmp	.L2811
	.p2align 4,,10
	.p2align 3
.L3053:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3052
	.p2align 4,,10
	.p2align 3
.L2930:
	call	*%rax
	jmp	.L2929
	.p2align 4,,10
	.p2align 3
.L2692:
	call	*%rax
	jmp	.L2691
	.p2align 4,,10
	.p2align 3
.L3114:
	call	*%rax
	jmp	.L3113
	.p2align 4,,10
	.p2align 3
.L3137:
	call	*%rdx
	jmp	.L3136
	.p2align 4,,10
	.p2align 3
.L2701:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2700
	.p2align 4,,10
	.p2align 3
.L2886:
	call	*%rax
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L3097:
	call	*%rax
	jmp	.L3096
	.p2align 4,,10
	.p2align 3
.L2718:
	call	*%rax
	jmp	.L2717
	.p2align 4,,10
	.p2align 3
.L3070:
	call	*%rax
	jmp	.L3069
	.p2align 4,,10
	.p2align 3
.L2770:
	call	*%rax
	jmp	.L2769
	.p2align 4,,10
	.p2align 3
.L2859:
	call	*%rax
	jmp	.L2858
	.p2align 4,,10
	.p2align 3
.L2972:
	call	*%rax
	jmp	.L2971
	.p2align 4,,10
	.p2align 3
.L2877:
	call	*%rdx
	jmp	.L2876
	.p2align 4,,10
	.p2align 3
.L2875:
	call	*%rdx
	jmp	.L2874
	.p2align 4,,10
	.p2align 3
.L3088:
	call	*%rdx
	jmp	.L3087
	.p2align 4,,10
	.p2align 3
.L3086:
	call	*%rdx
	jmp	.L3085
	.p2align 4,,10
	.p2align 3
.L3061:
	call	*%rdx
	jmp	.L3060
	.p2align 4,,10
	.p2align 3
.L3059:
	call	*%rdx
	jmp	.L3058
	.p2align 4,,10
	.p2align 3
.L2709:
	call	*%rdx
	jmp	.L2708
	.p2align 4,,10
	.p2align 3
.L2707:
	call	*%rdx
	jmp	.L2706
	.p2align 4,,10
	.p2align 3
.L2761:
	call	*%rdx
	jmp	.L2760
	.p2align 4,,10
	.p2align 3
.L2759:
	call	*%rdx
	jmp	.L2758
	.p2align 4,,10
	.p2align 3
.L2850:
	call	*%rdx
	jmp	.L2849
	.p2align 4,,10
	.p2align 3
.L2848:
	call	*%rdx
	jmp	.L2847
	.p2align 4,,10
	.p2align 3
.L2963:
	call	*%rdx
	jmp	.L2962
	.p2align 4,,10
	.p2align 3
.L2961:
	call	*%rdx
	jmp	.L2960
.L3189:
	movq	%rbx, %rdx
	jmp	.L3182
.L2663:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L2662
.L3789:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10744:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNK12v8_inspector16V8ConsoleMessage13wrapArgumentsEPNS_22V8InspectorSessionImplEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8ConsoleMessage13wrapArgumentsEPNS_22V8InspectorSessionImplEb
	.type	_ZNK12v8_inspector16V8ConsoleMessage13wrapArgumentsEPNS_22V8InspectorSessionImplEb, @function
_ZNK12v8_inspector16V8ConsoleMessage13wrapArgumentsEPNS_22V8InspectorSessionImplEb:
.LFB7940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movl	116(%rsi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	144(%rsi), %rax
	cmpq	%rax, 136(%rsi)
	je	.L3854
	testl	%r14d, %r14d
	jne	.L3938
.L3854:
	movq	$0, (%r12)
.L3852:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3939
	addq	$232, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3938:
	.cfi_restore_state
	movl	16(%rdx), %eax
	movq	24(%rdx), %rdi
	movq	%rsi, %rbx
	movl	%r14d, %edx
	movl	%ecx, %r15d
	movl	%eax, %esi
	movq	%rdi, -192(%rbp)
	movl	%eax, -196(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3854
	movq	%rax, %rdi
	call	_ZNK12v8_inspector16InspectedContext7isolateEv@PLT
	leaq	-176(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rdi, -256(%rbp)
	movq	%rax, -248(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r13, %rdi
	call	_ZNK12v8_inspector16InspectedContext7contextEv@PLT
	movl	$24, %edi
	movq	%rax, -208(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	%rax, %r13
	movq	$0, 16(%rax)
	movq	136(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %r9
	testq	%r9, %r9
	je	.L3857
	movq	(%r9), %rsi
	movq	-248(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r9
.L3857:
	movq	%r9, %rdi
	movq	%r9, -216(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	136(%rbx), %rdx
	movq	-216(%rbp), %r9
	testb	%al, %al
	movq	144(%rbx), %rax
	je	.L3859
	cmpl	$7, 120(%rbx)
	jne	.L3859
	testb	%r15b, %r15b
	je	.L3859
	subq	%rdx, %rax
	xorl	%ebx, %ebx
	cmpq	$15, %rax
	jbe	.L3937
	movq	8(%rdx), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L3866
	movq	(%r15), %rsi
	movq	-248(%rbp), %rdi
	movq	%r9, -216(%rbp)
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-216(%rbp), %r9
	movq	%rax, %r15
.L3866:
	movq	%r15, %rdi
	movq	%r9, -216(%rbp)
	call	_ZNK2v85Value7IsArrayEv@PLT
	movq	-216(%rbp), %r9
	testb	%al, %al
	je	.L3867
	movq	%r15, %rbx
.L3937:
	leaq	-144(%rbp), %rax
	movq	%rax, -216(%rbp)
.L3865:
	movq	-208(%rbp), %rdx
	movq	-184(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r9, %rcx
	movq	-216(%rbp), %rdi
	call	_ZN12v8_inspector22V8InspectorSessionImpl9wrapTableEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS1_10MaybeLocalINS1_5ArrayEEE@PLT
	movl	-196(%rbp), %esi
	movq	-192(%rbp), %rdi
	movl	%r14d, %edx
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii@PLT
	testq	%rax, %rax
	je	.L3940
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L3873
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	je	.L3874
	movq	$0, -144(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r13)
.L3875:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3863
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L3863:
	movq	%r13, (%r12)
.L3889:
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L3852
	.p2align 4,,10
	.p2align 3
.L3859:
	cmpq	%rdx, %rax
	je	.L3863
	movzbl	%r15b, %eax
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rdx
	movl	%r14d, -232(%rbp)
	movl	%eax, -216(%rbp)
	leaq	-144(%rbp), %rax
	leaq	-96(%rbp), %r15
	movq	%r12, -264(%rbp)
	movq	%rax, %r14
	movq	%rcx, %r12
	movq	%rdx, -224(%rbp)
	jmp	.L3888
	.p2align 4,,10
	.p2align 3
.L3944:
	movq	$0, -144(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r13)
.L3885:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3886
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3887
	movq	%rdi, -240(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movq	-240(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L3886:
	movq	144(%rbx), %rax
	subq	136(%rbx), %rax
	addq	$1, %r12
	sarq	$3, %rax
	cmpq	%rax, %r12
	jnb	.L3941
.L3888:
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	136(%rbx), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.L3877
	movq	(%rcx), %rsi
	movq	-248(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rcx
.L3877:
	movl	-216(%rbp), %r9d
	movq	-208(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r15, %r8
	movq	-184(%rbp), %rsi
	call	_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	cmpq	-224(%rbp), %rdi
	je	.L3878
	call	_ZdlPv@PLT
.L3878:
	movl	-232(%rbp), %edx
	movl	-196(%rbp), %esi
	movq	-192(%rbp), %rdi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii@PLT
	testq	%rax, %rax
	je	.L3942
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L3943
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	jne	.L3944
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L3885
	.p2align 4,,10
	.p2align 3
.L3887:
	call	*%rax
	jmp	.L3886
	.p2align 4,,10
	.p2align 3
.L3942:
	movq	-264(%rbp), %r12
	movq	-144(%rbp), %r14
	movq	$0, (%r12)
	testq	%r14, %r14
	je	.L3872
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3881
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3872:
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	jne	.L3892
	jmp	.L3894
	.p2align 4,,10
	.p2align 3
.L3946:
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3890:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L3945
.L3892:
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L3890
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3946
	call	*%rax
	jmp	.L3890
	.p2align 4,,10
	.p2align 3
.L3945:
	movq	0(%r13), %r15
.L3894:
	testq	%r15, %r15
	je	.L3893
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3893:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3889
	.p2align 4,,10
	.p2align 3
.L3943:
	movq	%r13, %rdi
	movq	-264(%rbp), %r12
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3883
	movq	(%rdi), %rax
	call	*24(%rax)
.L3883:
	xorl	%r13d, %r13d
	jmp	.L3863
	.p2align 4,,10
	.p2align 3
.L3941:
	movq	-264(%rbp), %r12
	jmp	.L3863
.L3881:
	call	*%rax
	jmp	.L3872
.L3867:
	movq	(%r15), %rax
	leaq	-144(%rbp), %rcx
	movq	%rcx, -216(%rbp)
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L3865
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L3865
	movq	-248(%rbp), %rsi
	movq	%rcx, %rdi
	movq	%r9, -232(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	-248(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	-208(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rcx
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	movq	-224(%rbp), %rdi
	testb	%al, %al
	cmovne	%rdi, %rbx
	movq	-216(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-232(%rbp), %r9
	jmp	.L3865
.L3940:
	movq	-144(%rbp), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L3872
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L3872
.L3873:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
	jmp	.L3875
.L3874:
	movq	-216(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L3875
.L3939:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7940:
	.size	_ZNK12v8_inspector16V8ConsoleMessage13wrapArgumentsEPNS_22V8InspectorSessionImplEb, .-_ZNK12v8_inspector16V8ConsoleMessage13wrapArgumentsEPNS_22V8InspectorSessionImplEb
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_.str1.1,"aMS",@progbits,1
.LC8:
	.string	"basic_string::_M_create"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB11362:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L3966
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L3957
	movq	16(%rdi), %rax
.L3949:
	cmpq	%r15, %rax
	jb	.L3969
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L3953
.L3972:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L3970
	testq	%rdx, %rdx
	je	.L3953
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L3953:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3969:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L3971
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L3952
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L3952:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L3956
	call	_ZdlPv@PLT
.L3956:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L3953
	jmp	.L3972
	.p2align 4,,10
	.p2align 3
.L3966:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L3957:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L3949
	.p2align 4,,10
	.p2align 3
.L3970:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L3953
.L3971:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11362:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.text._ZN12v8_inspector16V8ConsoleMessage11setLocationERKNS_8String16EjjSt10unique_ptrINS_16V8StackTraceImplESt14default_deleteIS5_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8ConsoleMessage11setLocationERKNS_8String16EjjSt10unique_ptrINS_16V8StackTraceImplESt14default_deleteIS5_EEi
	.type	_ZN12v8_inspector16V8ConsoleMessage11setLocationERKNS_8String16EjjSt10unique_ptrINS_16V8StackTraceImplESt14default_deleteIS5_EEi, @function
_ZN12v8_inspector16V8ConsoleMessage11setLocationERKNS_8String16EjjSt10unique_ptrINS_16V8StackTraceImplESt14default_deleteIS5_EEi:
.LFB7933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r13), %rax
	movl	-52(%rbp), %edx
	movl	%r15d, 100(%rbx)
	movq	104(%rbx), %rdi
	movq	%rax, 88(%rbx)
	movq	(%r12), %rax
	movl	%edx, 96(%rbx)
	movq	$0, (%r12)
	movq	%rax, 104(%rbx)
	testq	%rdi, %rdi
	je	.L3974
	movq	(%rdi), %rax
	call	*64(%rax)
.L3974:
	movl	%r14d, 112(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7933:
	.size	_ZN12v8_inspector16V8ConsoleMessage11setLocationERKNS_8String16EjjSt10unique_ptrINS_16V8StackTraceImplESt14default_deleteIS5_EEi, .-_ZN12v8_inspector16V8ConsoleMessage11setLocationERKNS_8String16EjjSt10unique_ptrINS_16V8StackTraceImplESt14default_deleteIS5_EEi
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v86GlobalINS1_5ValueEEESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v86GlobalINS1_5ValueEEESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v86GlobalINS1_5ValueEEESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v86GlobalINS1_5ValueEEESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v86GlobalINS1_5ValueEEESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB12550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r13
	movq	%rdi, -88(%rbp)
	movabsq	$1152921504606846975, %rdi
	movq	%rax, -80(%rbp)
	subq	%r13, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L4009
	movq	%rsi, %r8
	movq	%rsi, %r12
	movq	%rsi, %rbx
	subq	%r13, %r8
	testq	%rax, %rax
	je	.L3993
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L4010
.L3981:
	movq	%rsi, %rdi
	movq	%rdx, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-96(%rbp), %r8
	movq	%rax, -64(%rbp)
	movq	-104(%rbp), %rdx
	addq	%rax, %rsi
	movq	%rsi, -72(%rbp)
	leaq	8(%rax), %rsi
.L3992:
	movq	(%rdx), %rax
	movq	-64(%rbp), %rcx
	movq	$0, (%rdx)
	movq	%rax, (%rcx,%r8)
	cmpq	%r13, %r12
	je	.L3983
	movq	%rcx, %r14
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L3986:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L3984
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L3985
	movq	%r8, -56(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-56(%rbp), %r8
.L3985:
	movl	$8, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L3984:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r12
	jne	.L3986
	movq	-64(%rbp), %rcx
	movq	%r12, %rax
	subq	%r13, %rax
	leaq	8(%rcx,%rax), %rsi
.L3983:
	movq	-80(%rbp), %rax
	cmpq	%rax, %r12
	je	.L3987
	movq	%rax, %r15
	subq	%r12, %r15
	leaq	-8(%r15), %r8
	movq	%r8, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r8, %r8
	je	.L3995
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L3989:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L3989
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rbx
	leaq	(%rsi,%rbx), %rdx
	addq	%r12, %rbx
	cmpq	%rax, %rdi
	je	.L3990
.L3988:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L3990:
	leaq	8(%rsi,%r8), %rsi
.L3987:
	testq	%r13, %r13
	je	.L3991
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rsi
.L3991:
	movq	-64(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movq	%rsi, %xmm2
	movq	-72(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4010:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L3982
	movq	$0, -72(%rbp)
	movl	$8, %esi
	movq	$0, -64(%rbp)
	jmp	.L3992
	.p2align 4,,10
	.p2align 3
.L3993:
	movl	$8, %esi
	jmp	.L3981
.L3995:
	movq	%rsi, %rdx
	jmp	.L3988
.L3982:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	movq	%rdi, %rsi
	salq	$3, %rsi
	jmp	.L3981
.L4009:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12550:
	.size	_ZNSt6vectorISt10unique_ptrIN2v86GlobalINS1_5ValueEEESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v86GlobalINS1_5ValueEEESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_,"axG",@progbits,_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	.type	_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_, @function
_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_:
.LFB12600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rsi), %rdx
	movq	24(%r14), %rax
	movq	%rsi, -56(%rbp)
	leaq	8(%rdx), %r13
	cmpq	%rax, %r13
	jnb	.L4012
	.p2align 4,,10
	.p2align 3
.L4017:
	movq	0(%r13), %rbx
	leaq	512(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L4016:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L4013
	movq	%r15, %rdi
	addq	$8, %rbx
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev
	movl	$240, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	cmpq	%rbx, %r12
	jne	.L4016
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L4017
.L4034:
	movq	-56(%rbp), %rcx
	movq	24(%rcx), %rdx
.L4012:
	movq	-56(%rbp), %rcx
	movq	(%rcx), %rbx
	cmpq	%rdx, %rax
	je	.L4018
	movq	16(%rcx), %r13
	cmpq	%rbx, %r13
	je	.L4023
	.p2align 4,,10
	.p2align 3
.L4019:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4022
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	cmpq	%rbx, %r13
	jne	.L4019
.L4023:
	movq	(%r14), %r13
	movq	8(%r14), %rbx
	cmpq	%rbx, %r13
	je	.L4011
	.p2align 4,,10
	.p2align 3
.L4021:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4025
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	cmpq	%rbx, %r13
	jne	.L4021
.L4011:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4013:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L4016
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L4017
	jmp	.L4034
	.p2align 4,,10
	.p2align 3
.L4025:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L4021
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4022:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L4019
	jmp	.L4023
.L4018:
	movq	(%r14), %r13
	cmpq	%rbx, %r13
	je	.L4011
.L4029:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4027
.L4035:
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	cmpq	%r13, %rbx
	je	.L4011
	movq	(%rbx), %r12
	testq	%r12, %r12
	jne	.L4035
.L4027:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L4029
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12600:
	.size	_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_, .-_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	.section	.rodata._ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_
	.type	_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_, @function
_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_:
.LFB12622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L4045
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L4046
.L4038:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	(%r12), %rdx
	movq	48(%rbx), %rax
	movq	$0, (%r12)
	movq	%rdx, (%rax)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4046:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L4047
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L4048
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L4043
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L4043:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L4041:
	movq	%r15, 40(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r13), %r14
	movq	(%r15), %xmm0
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L4038
	.p2align 4,,10
	.p2align 3
.L4047:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L4040
	cmpq	%r14, %rsi
	je	.L4041
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L4041
	.p2align 4,,10
	.p2align 3
.L4040:
	cmpq	%r14, %rsi
	je	.L4041
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L4041
.L4045:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4048:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE12622:
	.size	_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_, .-_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm:
.LFB12777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	subq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, %r13
	leaq	(%r9,%rax), %r15
	subq	%rsi, %r13
	cmpq	(%rdi), %r14
	je	.L4062
	movq	16(%rdi), %rax
.L4050:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %r15
	ja	.L4083
	cmpq	%rax, %r15
	jbe	.L4052
	addq	%rax, %rax
	cmpq	%rax, %r15
	jnb	.L4052
	movabsq	$4611686018427387904, %rdi
	movq	%rdx, %r15
	cmpq	%rdx, %rax
	ja	.L4053
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L4052:
	leaq	2(%r15,%r15), %rdi
.L4053:
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	testq	%r12, %r12
	movq	(%rbx), %r11
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	%rax, %r10
	je	.L4055
	cmpq	$1, %r12
	je	.L4084
	movq	%r12, %rdx
	addq	%rdx, %rdx
	je	.L4055
	movq	%r11, %rsi
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r11
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L4055:
	testq	%rcx, %rcx
	je	.L4057
	testq	%r8, %r8
	je	.L4057
	leaq	(%r10,%r12,2), %rdi
	cmpq	$1, %r8
	je	.L4085
	movq	%r8, %rdx
	addq	%rdx, %rdx
	je	.L4057
	movq	%rcx, %rsi
	movq	%r8, -80(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
.L4057:
	testq	%r13, %r13
	jne	.L4086
.L4059:
	cmpq	%r11, %r14
	je	.L4061
	movq	%r11, %rdi
	movq	%r10, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r10
.L4061:
	movq	%r15, 16(%rbx)
	movq	%r10, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4086:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	addq	%r12, %r8
	leaq	(%r10,%r8,2), %rdi
	leaq	(%r11,%rax,2), %rsi
	cmpq	$1, %r13
	je	.L4087
	addq	%r13, %r13
	je	.L4059
	movq	%r13, %rdx
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
	jmp	.L4059
	.p2align 4,,10
	.p2align 3
.L4062:
	movl	$7, %eax
	jmp	.L4050
	.p2align 4,,10
	.p2align 3
.L4087:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	jmp	.L4059
	.p2align 4,,10
	.p2align 3
.L4084:
	movzwl	(%r11), %eax
	movw	%ax, (%r10)
	jmp	.L4055
	.p2align 4,,10
	.p2align 3
.L4085:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	testq	%r13, %r13
	je	.L4059
	jmp	.L4086
.L4083:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12777:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag:
.LFB12799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	jne	.L4089
	testq	%rdx, %rdx
	jne	.L4105
.L4089:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L4090
	movq	(%r12), %rdi
.L4091:
	cmpq	$2, %rbx
	je	.L4106
	testq	%rbx, %rbx
	je	.L4094
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L4094:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4106:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L4094
	.p2align 4,,10
	.p2align 3
.L4090:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L4107
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L4091
.L4105:
	leaq	.LC10(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L4107:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12799:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.section	.text._ZN12v8_inspector16V8ConsoleMessageC2ENS_15V8MessageOriginEdRKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8ConsoleMessageC2ENS_15V8MessageOriginEdRKNS_8String16E
	.type	_ZN12v8_inspector16V8ConsoleMessageC2ENS_15V8MessageOriginEdRKNS_8String16E, @function
_ZN12v8_inspector16V8ConsoleMessageC2ENS_15V8MessageOriginEdRKNS_8String16E:
.LFB7928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	leaq	32(%rbx), %rax
	movsd	%xmm0, -8(%rdi)
	movl	%esi, -16(%rdi)
	movq	%rax, 16(%rbx)
	movq	8(%rdx), %rax
	movq	(%rdx), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%r12), %rax
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	$0, 64(%rbx)
	movq	%rax, 48(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 56(%rbx)
	xorl	%eax, %eax
	movw	%ax, 72(%rbx)
	leaq	176(%rbx), %rax
	movq	%rax, 160(%rbx)
	leaq	216(%rbx), %rax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 128(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 168(%rbx)
	movw	%dx, 176(%rbx)
	movq	$0, 192(%rbx)
	movq	%rax, 200(%rbx)
	movq	$0, 208(%rbx)
	movw	%cx, 216(%rbx)
	movq	$0, 232(%rbx)
	movups	%xmm0, 136(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7928:
	.size	_ZN12v8_inspector16V8ConsoleMessageC2ENS_15V8MessageOriginEdRKNS_8String16E, .-_ZN12v8_inspector16V8ConsoleMessageC2ENS_15V8MessageOriginEdRKNS_8String16E
	.globl	_ZN12v8_inspector16V8ConsoleMessageC1ENS_15V8MessageOriginEdRKNS_8String16E
	.set	_ZN12v8_inspector16V8ConsoleMessageC1ENS_15V8MessageOriginEdRKNS_8String16E,_ZN12v8_inspector16V8ConsoleMessageC2ENS_15V8MessageOriginEdRKNS_8String16E
	.section	.text._ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Console8FrontendE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Console8FrontendE
	.type	_ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Console8FrontendE, @function
_ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Console8FrontendE:
.LFB7935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -168(%rbp)
	movq	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum3LogE(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	120(%r13), %eax
	leal	-14(%rax), %edx
	cmpl	$1, %edx
	jbe	.L4131
	cmpl	$1, %eax
	je	.L4131
	cmpl	$3, %eax
	je	.L4132
	cmpl	$13, %eax
	je	.L4132
	cmpl	$4, %eax
	je	.L4142
	leaq	-80(%rbp), %rbx
	cmpl	$2, %eax
	je	.L4143
	.p2align 4,,10
	.p2align 3
.L4114:
	movl	$192, %edi
	call	_Znwm@PLT
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum10ConsoleApiE(%rip), %rsi
	leaq	16+_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE(%rip), %rax
	leaq	24(%r15), %rdx
	leaq	88(%r15), %r8
	movq	%rax, (%r15)
	xorl	%eax, %eax
	movq	%rdx, 8(%r15)
	leaq	64(%r15), %rdx
	leaq	48(%r15), %r9
	movq	%rdx, 48(%r15)
	xorl	%edx, %edx
	leaq	8(%r15), %r10
	movw	%dx, 64(%r15)
	leaq	104(%r15), %rdx
	movq	%rdx, 88(%r15)
	leaq	152(%r15), %rdx
	movw	%ax, 24(%r15)
	movq	%r8, -176(%rbp)
	movw	%cx, 104(%r15)
	movups	%xmm0, 152(%r15)
	movq	%r9, -184(%rbp)
	movq	%rdx, 136(%r15)
	movq	%r10, -192(%rbp)
	movq	$0, 16(%r15)
	movq	$0, 40(%r15)
	movq	$0, 56(%r15)
	movq	$0, 80(%r15)
	movq	$0, 96(%r15)
	movq	$0, 120(%r15)
	movb	$0, 128(%r15)
	movq	$0, 168(%r15)
	movq	$0, 144(%r15)
	movb	$0, 176(%r15)
	movl	$0, 180(%r15)
	movb	$0, 184(%r15)
	movl	$0, 188(%r15)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-192(%rbp), %r10
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-184(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r9, %rdi
	movq	%rdx, 40(%r15)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-176(%rbp), %r8
	movq	-112(%rbp), %rdx
	leaq	16(%r13), %rsi
	movq	%r8, %rdi
	movq	%rdx, 80(%r15)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	48(%r13), %rdx
	movq	-96(%rbp), %rdi
	movq	%rdx, 120(%r15)
	cmpq	%rbx, %rdi
	je	.L4121
	call	_ZdlPv@PLT
.L4121:
	movl	96(%r13), %edx
	movq	56(%r13), %rsi
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movb	$1, 176(%r15)
	movl	%edx, 180(%r15)
	movl	100(%r13), %edx
	movb	$1, 184(%r15)
	movl	%edx, 188(%r15)
	movq	64(%r13), %rdx
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	88(%r13), %rdx
	leaq	136(%r15), %rdi
	movq	%r12, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rdx
	movq	-96(%rbp), %rdi
	movb	$1, 128(%r15)
	movq	%rdx, 168(%r15)
	cmpq	%rbx, %rdi
	je	.L4122
	call	_ZdlPv@PLT
.L4122:
	movq	-168(%rbp), %rdi
	leaq	-152(%rbp), %rsi
	movq	%r15, -152(%rbp)
	call	_ZN12v8_inspector8protocol7Console8Frontend12messageAddedESt10unique_ptrINS1_14ConsoleMessageESt14default_deleteIS4_EE@PLT
	movq	-152(%rbp), %r12
	testq	%r12, %r12
	je	.L4123
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4124
	leaq	16+_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE(%rip), %rax
	movq	136(%r12), %rdi
	movq	%rax, (%r12)
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4125
	call	_ZdlPv@PLT
.L4125:
	movq	88(%r12), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4126
	call	_ZdlPv@PLT
.L4126:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4127
	call	_ZdlPv@PLT
.L4127:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4128
	call	_ZdlPv@PLT
.L4128:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4123:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4110
	call	_ZdlPv@PLT
.L4110:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4144
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4131:
	.cfi_restore_state
	movq	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum5DebugE(%rip), %rsi
.L4140:
	movq	%r12, %rdi
	leaq	-80(%rbp), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -112(%rbp)
	cmpq	%rbx, %rdi
	je	.L4114
.L4138:
	call	_ZdlPv@PLT
	jmp	.L4114
	.p2align 4,,10
	.p2align 3
.L4132:
	movq	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum5ErrorE(%rip), %rsi
	jmp	.L4140
	.p2align 4,,10
	.p2align 3
.L4124:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4123
	.p2align 4,,10
	.p2align 3
.L4142:
	movq	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum7WarningE(%rip), %rsi
	jmp	.L4140
	.p2align 4,,10
	.p2align 3
.L4143:
	movq	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum4InfoE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -112(%rbp)
	cmpq	%rbx, %rdi
	jne	.L4138
	jmp	.L4114
.L4144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7935:
	.size	_ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Console8FrontendE, .-_ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Console8FrontendE
	.section	.rodata._ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Runtime8FrontendEPNS_22V8InspectorSessionImplEb.str1.1,"aMS",@progbits,1
.LC11:
	.string	"unreachable code"
	.section	.text._ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Runtime8FrontendEPNS_22V8InspectorSessionImplEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Runtime8FrontendEPNS_22V8InspectorSessionImplEb
	.type	_ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Runtime8FrontendEPNS_22V8InspectorSessionImplEb, @function
_ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Runtime8FrontendEPNS_22V8InspectorSessionImplEb:
.LFB7943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movl	16(%rdx), %r13d
	movq	24(%rdx), %rbx
	movq	%rsi, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L4302
	cmpl	$2, %eax
	je	.L4303
	testl	%eax, %eax
	jne	.L4174
	leaq	-232(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	%cl, %ecx
	call	_ZNK12v8_inspector16V8ConsoleMessage13wrapArgumentsEPNS_22V8InspectorSessionImplEb
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl24hasConsoleMessageStorageEi@PLT
	testb	%al, %al
	je	.L4175
	cmpq	$0, -232(%rbp)
	je	.L4176
	leaq	-112(%rbp), %r15
	leaq	-216(%rbp), %r14
.L4177:
	leaq	-136(%rbp), %r13
	xorl	%edx, %edx
	movq	208(%r12), %rax
	movb	$0, -160(%rbp)
	movq	%r13, -152(%rbp)
	movq	$0, -144(%rbp)
	movw	%dx, -136(%rbp)
	movq	$0, -120(%rbp)
	testq	%rax, %rax
	jne	.L4304
.L4185:
	movq	104(%r12), %rsi
	movl	120(%r12), %ecx
	testq	%rsi, %rsi
	je	.L4187
	movq	24(%rbx), %rdx
	cmpl	$13, %ecx
	ja	.L4188
	movl	$1, %eax
	salq	%cl, %rax
	testl	$8472, %eax
	jne	.L4305
.L4188:
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerEi@PLT
	movq	-216(%rbp), %rsi
	movl	120(%r12), %ecx
.L4187:
	movzbl	-160(%rbp), %eax
	movb	%al, -112(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rax, -256(%rbp)
	movq	%rax, -104(%rbp)
	movq	-152(%rbp), %rax
	cmpq	%r13, %rax
	je	.L4306
	movq	%rax, -104(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, -88(%rbp)
.L4190:
	movq	-144(%rbp), %rax
	movsd	8(%r12), %xmm0
	movq	%r13, -152(%rbp)
	leaq	-208(%rbp), %rbx
	movl	116(%r12), %r10d
	movq	%rsi, -216(%rbp)
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	movw	%ax, -136(%rbp)
	movq	-120(%rbp), %rax
	movq	$0, -144(%rbp)
	movq	%rax, -72(%rbp)
	movq	-232(%rbp), %rax
	movl	%r10d, -268(%rbp)
	movq	$0, -232(%rbp)
	movq	%rax, -224(%rbp)
	movsd	%xmm0, -264(%rbp)
	cmpl	$15, %ecx
	ja	.L4191
	leaq	.L4193(%rip), %rdx
	movslq	(%rdx,%rcx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Runtime8FrontendEPNS_22V8InspectorSessionImplEb,"a",@progbits
	.align 4
	.align 4
.L4193:
	.long	.L4191-.L4193
	.long	.L4207-.L4193
	.long	.L4206-.L4193
	.long	.L4205-.L4193
	.long	.L4204-.L4193
	.long	.L4203-.L4193
	.long	.L4202-.L4193
	.long	.L4201-.L4193
	.long	.L4200-.L4193
	.long	.L4199-.L4193
	.long	.L4198-.L4193
	.long	.L4197-.L4193
	.long	.L4196-.L4193
	.long	.L4195-.L4193
	.long	.L4194-.L4193
	.long	.L4192-.L4193
	.section	.text._ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Runtime8FrontendEPNS_22V8InspectorSessionImplEb
	.p2align 4,,10
	.p2align 3
.L4313:
	movq	(%r12), %r15
.L4211:
	testq	%r15, %r15
	je	.L4215
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4215:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4210:
	movq	-216(%rbp), %r12
	testq	%r12, %r12
	je	.L4216
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4217
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4216:
	movq	-104(%rbp), %rdi
	cmpq	-256(%rbp), %rdi
	je	.L4218
	call	_ZdlPv@PLT
.L4218:
	movq	-152(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4175
	call	_ZdlPv@PLT
.L4175:
	movq	-232(%rbp), %r14
	testq	%r14, %r14
	je	.L4145
	movq	8(%r14), %rbx
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L4221
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %r15
	jmp	.L4224
	.p2align 4,,10
	.p2align 3
.L4308:
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4222:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L4307
.L4224:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L4222
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	je	.L4308
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L4224
	.p2align 4,,10
	.p2align 3
.L4307:
	movq	(%r14), %r12
.L4221:
	testq	%r12, %r12
	je	.L4225
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4225:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4145:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4309
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4302:
	.cfi_restore_state
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	%cl, %ecx
	call	_ZNK12v8_inspector16V8ConsoleMessage13wrapExceptionEPNS_22V8InspectorSessionImplEb
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl24hasConsoleMessageStorageEi@PLT
	testb	%al, %al
	jne	.L4310
.L4147:
	movq	-224(%rbp), %r12
	testq	%r12, %r12
	je	.L4145
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4171
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L4145
	.p2align 4,,10
	.p2align 3
.L4310:
	movl	$184, %edi
	leaq	160(%r12), %r14
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	cmpq	$0, -224(%rbp)
	movq	%rax, %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	32(%r13), %rax
	movq	%rax, 16(%r13)
	leaq	88(%r13), %rax
	movq	%rax, 72(%r13)
	leaq	136(%r13), %rax
	movq	%rax, 120(%r13)
	movl	124(%r12), %eax
	movups	%xmm0, 88(%r13)
	movups	%xmm0, 136(%r13)
	pxor	%xmm0, %xmm0
	movq	$0, 24(%r13)
	movw	%si, 32(%r13)
	movq	$0, 104(%r13)
	movq	$0, 80(%r13)
	movb	$0, 112(%r13)
	movq	$0, 152(%r13)
	movq	$0, 128(%r13)
	movb	$0, 176(%r13)
	movl	$0, 180(%r13)
	movq	$0, 48(%r13)
	movq	$0, 56(%r13)
	movb	$0, 64(%r13)
	movl	%eax, 8(%r13)
	movups	%xmm0, 160(%r13)
	je	.L4149
	leaq	16(%r12), %r14
.L4149:
	movq	%r14, %rsi
	leaq	16(%r13), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r14), %rax
	xorl	%ecx, %ecx
	movl	112(%r12), %esi
	movq	%rax, 48(%r13)
	movl	96(%r12), %eax
	testl	%eax, %eax
	leal	-1(%rax), %edx
	cmove	%ecx, %edx
	movl	%edx, 56(%r13)
	movl	100(%r12), %edx
	leal	-1(%rdx), %eax
	testl	%edx, %edx
	cmove	%ecx, %eax
	movl	%eax, 60(%r13)
	testl	%esi, %esi
	jne	.L4311
.L4152:
	movq	64(%r12), %rax
	testq	%rax, %rax
	jne	.L4312
.L4155:
	movq	104(%r12), %rsi
	leaq	-216(%rbp), %r14
	testq	%rsi, %rsi
	je	.L4157
	movq	24(%rbx), %rdx
	movq	%r14, %rdi
	call	_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerE@PLT
	movq	-216(%rbp), %rax
	movq	160(%r13), %r15
	movq	$0, -216(%rbp)
	movq	%rax, 160(%r13)
	testq	%r15, %r15
	je	.L4157
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rbx
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L4160
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4161:
	movq	-216(%rbp), %r15
	testq	%r15, %r15
	je	.L4157
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L4163
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4157:
	movl	116(%r12), %eax
	testl	%eax, %eax
	je	.L4164
	movl	%eax, 180(%r13)
	movb	$1, 176(%r13)
.L4164:
	movq	-224(%rbp), %rax
	testq	%rax, %rax
	je	.L4166
	movq	168(%r13), %rdi
	movq	$0, -224(%rbp)
	movq	%rax, 168(%r13)
	testq	%rdi, %rdi
	je	.L4166
	movq	(%rdi), %rax
	call	*24(%rax)
.L4166:
	movsd	8(%r12), %xmm0
	movq	-248(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r13, -216(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime8Frontend15exceptionThrownEdSt10unique_ptrINS1_16ExceptionDetailsESt14default_deleteIS4_EE@PLT
	movq	-216(%rbp), %r12
	testq	%r12, %r12
	je	.L4147
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4169
	call	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	movl	$184, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L4147
.L4305:
	movq	%r14, %rdi
	call	_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerE@PLT
	movq	-216(%rbp), %rsi
	movl	120(%r12), %ecx
	jmp	.L4187
	.p2align 4,,10
	.p2align 3
.L4304:
	movq	200(%r12), %rsi
	leaq	-96(%rbp), %rcx
	movq	%r15, %rdi
	movq	%rcx, -112(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	movq	%rcx, -256(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	232(%r12), %rax
	leaq	-152(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, -160(%rbp)
	movq	-256(%rbp), %rcx
	movq	%rax, -120(%rbp)
	cmpq	%rcx, %rdi
	je	.L4185
	call	_ZdlPv@PLT
	jmp	.L4185
	.p2align 4,,10
	.p2align 3
.L4306:
	movdqu	-136(%rbp), %xmm1
	movups	%xmm1, -88(%rbp)
	jmp	.L4190
	.p2align 4,,10
	.p2align 3
.L4303:
	movl	128(%r12), %edx
	movq	%rsi, %rdi
	leaq	16(%r12), %rsi
	call	_ZN12v8_inspector8protocol7Runtime8Frontend16exceptionRevokedERKNS_8String16Ei@PLT
	jmp	.L4145
	.p2align 4,,10
	.p2align 3
.L4176:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-232(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, -232(%rbp)
	movups	%xmm0, (%rax)
	testq	%rdi, %rdi
	je	.L4178
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L4178:
	cmpq	$0, 24(%r12)
	leaq	-112(%rbp), %r15
	leaq	-216(%rbp), %r14
	je	.L4177
	movl	$320, %edi
	call	_Znwm@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%rax, %r13
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	movq	%rax, %xmm2
	leaq	-64(%rax), %rsi
	leaq	32(%r13), %rax
	movw	%cx, 32(%r13)
	movq	%rax, 16(%r13)
	leaq	80(%r13), %rax
	leaq	16(%r13), %r14
	movq	%rsi, %xmm0
	movq	%rax, 64(%r13)
	leaq	128(%r13), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 112(%r13)
	leaq	184(%r13), %rax
	movq	%rax, 168(%r13)
	leaq	232(%r13), %rax
	movups	%xmm0, 0(%r13)
	pxor	%xmm0, %xmm0
	movq	%rax, 216(%r13)
	leaq	280(%r13), %rax
	movups	%xmm0, 80(%r13)
	movups	%xmm0, 128(%r13)
	movups	%xmm0, 184(%r13)
	movups	%xmm0, 232(%r13)
	movups	%xmm0, 280(%r13)
	pxor	%xmm0, %xmm0
	movq	%rax, 264(%r13)
	movups	%xmm0, 304(%r13)
	movq	$0, 24(%r13)
	movq	$0, 48(%r13)
	movb	$0, 56(%r13)
	movq	$0, 96(%r13)
	movq	$0, 72(%r13)
	movb	$0, 104(%r13)
	movq	$0, 144(%r13)
	movq	$0, 120(%r13)
	movq	$0, 152(%r13)
	movb	$0, 160(%r13)
	movq	$0, 200(%r13)
	movq	$0, 176(%r13)
	movb	$0, 208(%r13)
	movq	$0, 248(%r13)
	movq	$0, 224(%r13)
	movb	$0, 256(%r13)
	movq	$0, 296(%r13)
	movq	$0, 272(%r13)
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6StringE(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movq	%r13, -216(%rbp)
	movq	%rax, 48(%r13)
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4180
	call	_ZdlPv@PLT
	movq	-216(%rbp), %r13
.L4180:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	16(%r12), %rsi
	movq	%rax, %r14
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r14)
	leaq	32(%r14), %rax
	leaq	16(%r14), %rdi
	movq	%rax, 16(%r14)
	movq	24(%r12), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	48(%r12), %rax
	movq	%rax, 48(%r14)
	movq	152(%r13), %rdi
	movq	%r14, 152(%r13)
	testq	%rdi, %rdi
	je	.L4181
	movq	(%rdi), %rax
	call	*24(%rax)
.L4181:
	movq	-232(%rbp), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L4182
	movq	-216(%rbp), %rax
	leaq	-216(%rbp), %r14
	movq	$0, -216(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%rdi)
.L4183:
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4177
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4177
	.p2align 4,,10
	.p2align 3
.L4207:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum5DebugE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	.p2align 4,,10
	.p2align 3
.L4208:
	movq	-248(%rbp), %rdi
	movq	%r15, %r9
	movq	%r14, %r8
	movl	%r10d, %ecx
	leaq	-224(%rbp), %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol7Runtime8Frontend16consoleAPICalledERKNS_8String16ESt10unique_ptrISt6vectorIS6_INS1_12RemoteObjectESt14default_deleteIS8_EESaISB_EES9_ISD_EEidN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS1_10StackTraceEEENSI_10ValueMaybeIS3_EE@PLT
	movq	-208(%rbp), %rdi
	leaq	-192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4209
	call	_ZdlPv@PLT
.L4209:
	movq	-224(%rbp), %r12
	testq	%r12, %r12
	je	.L4210
	movq	8(%r12), %r14
	movq	(%r12), %r15
	cmpq	%r15, %r14
	je	.L4211
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rbx
	jmp	.L4214
	.p2align 4,,10
	.p2align 3
.L4314:
	movq	%rdi, -248(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movq	-248(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L4212:
	addq	$8, %r15
	cmpq	%r15, %r14
	je	.L4313
.L4214:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L4212
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	je	.L4314
	call	*%rax
	jmp	.L4212
	.p2align 4,,10
	.p2align 3
.L4206:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum4InfoE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4205:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum5ErrorE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4204:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum7WarningE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4203:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum3DirE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4202:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum6DirxmlE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4192:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum5CountE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	-268(%rbp), %r10d
	movsd	-264(%rbp), %xmm0
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4201:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum5TableE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4200:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum5TraceE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4199:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum10StartGroupE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4198:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum19StartGroupCollapsedE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4197:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum8EndGroupE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4196:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum5ClearE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4195:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum6AssertE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4194:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum7TimeEndE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4312:
	movq	56(%r12), %rsi
	leaq	-112(%rbp), %r15
	leaq	-96(%rbp), %r14
	movq	%r15, %rdi
	movq	%r14, -112(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	88(%r12), %rax
	leaq	120(%r13), %rdi
	movq	%r15, %rsi
	movq	%rax, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, 112(%r13)
	movq	%rax, 152(%r13)
	cmpq	%r14, %rdi
	je	.L4155
	call	_ZdlPv@PLT
	jmp	.L4155
	.p2align 4,,10
	.p2align 3
.L4311:
	leaq	-160(%rbp), %rdi
	leaq	-112(%rbp), %r15
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	-160(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-96(%rbp), %r14
	movq	-152(%rbp), %rax
	movq	%r14, -112(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-128(%rbp), %rax
	leaq	72(%r13), %rdi
	movq	%r15, %rsi
	movq	%rax, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, 64(%r13)
	movq	%rax, 104(%r13)
	cmpq	%r14, %rdi
	je	.L4153
	call	_ZdlPv@PLT
.L4153:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4152
	call	_ZdlPv@PLT
	jmp	.L4152
	.p2align 4,,10
	.p2align 3
.L4217:
	call	*%rax
	jmp	.L4216
.L4191:
	movq	_ZN12v8_inspector8protocol7Runtime16ConsoleAPICalled8TypeEnum3LogE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-264(%rbp), %xmm0
	movl	-268(%rbp), %r10d
	jmp	.L4208
	.p2align 4,,10
	.p2align 3
.L4171:
	call	*%rax
	jmp	.L4145
.L4169:
	call	*%rax
	jmp	.L4147
.L4163:
	call	*%rax
	jmp	.L4157
.L4160:
	call	*%rax
	jmp	.L4161
.L4182:
	leaq	-216(%rbp), %r14
	movq	%r14, %rdx
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L4183
.L4309:
	call	__stack_chk_fail@PLT
.L4174:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE7943:
	.size	_ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Runtime8FrontendEPNS_22V8InspectorSessionImplEb, .-_ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Runtime8FrontendEPNS_22V8InspectorSessionImplEb
	.section	.text._ZN12v8_inspector16V8ConsoleMessage25createForRevokedExceptionEdRKNS_8String16Ej,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8ConsoleMessage25createForRevokedExceptionEdRKNS_8String16Ej
	.type	_ZN12v8_inspector16V8ConsoleMessage25createForRevokedExceptionEdRKNS_8String16Ej, @function
_ZN12v8_inspector16V8ConsoleMessage25createForRevokedExceptionEdRKNS_8String16Ej:
.LFB7953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$240, %edi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movsd	%xmm0, -40(%rbp)
	call	_Znwm@PLT
	movsd	-40(%rbp), %xmm0
	movq	(%r12), %rsi
	movq	%rax, %rbx
	movl	$2, (%rax)
	leaq	16(%rax), %rdi
	leaq	32(%rax), %rax
	movsd	%xmm0, -24(%rax)
	movq	%rax, 16(%rbx)
	movq	8(%r12), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%r12), %rax
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, 0(%r13)
	movq	%rax, 48(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 56(%rbx)
	xorl	%eax, %eax
	movw	%ax, 72(%rbx)
	leaq	176(%rbx), %rax
	movq	%rax, 160(%rbx)
	leaq	216(%rbx), %rax
	movq	%rax, 200(%rbx)
	movq	%r13, %rax
	movl	%r14d, 128(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movl	$0, 132(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 168(%rbx)
	movw	%dx, 176(%rbx)
	movq	$0, 192(%rbx)
	movq	$0, 208(%rbx)
	movw	%cx, 216(%rbx)
	movq	$0, 232(%rbx)
	movups	%xmm0, 136(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7953:
	.size	_ZN12v8_inspector16V8ConsoleMessage25createForRevokedExceptionEdRKNS_8String16Ej, .-_ZN12v8_inspector16V8ConsoleMessage25createForRevokedExceptionEdRKNS_8String16Ej
	.section	.text._ZN12v8_inspector16V8ConsoleMessage18createForExceptionEdRKNS_8String16ES3_jjSt10unique_ptrINS_16V8StackTraceImplESt14default_deleteIS5_EEiPN2v87IsolateES3_iNS9_5LocalINS9_5ValueEEEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8ConsoleMessage18createForExceptionEdRKNS_8String16ES3_jjSt10unique_ptrINS_16V8StackTraceImplESt14default_deleteIS5_EEiPN2v87IsolateES3_iNS9_5LocalINS9_5ValueEEEj
	.type	_ZN12v8_inspector16V8ConsoleMessage18createForExceptionEdRKNS_8String16ES3_jjSt10unique_ptrINS_16V8StackTraceImplESt14default_deleteIS5_EEiPN2v87IsolateES3_iNS9_5LocalINS9_5ValueEEEj, @function
_ZN12v8_inspector16V8ConsoleMessage18createForExceptionEdRKNS_8String16ES3_jjSt10unique_ptrINS_16V8StackTraceImplESt14default_deleteIS5_EEiPN2v87IsolateES3_iNS9_5LocalINS9_5ValueEEEj:
.LFB7952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$240, %edi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movl	%r8d, -76(%rbp)
	movl	%ecx, -80(%rbp)
	movq	32(%rbp), %r14
	movq	%r9, -72(%rbp)
	movq	%rax, -96(%rbp)
	movsd	%xmm0, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movsd	-88(%rbp), %xmm0
	movq	(%r14), %rsi
	movq	%rax, %rbx
	movl	$1, (%rax)
	leaq	16(%rax), %rdi
	addq	$32, %rax
	movsd	%xmm0, -24(%rax)
	movq	%rax, 16(%rbx)
	movq	8(%r14), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%r14), %rax
	movq	-72(%rbp), %r9
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rbx, (%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 48(%rbx)
	leaq	72(%rbx), %rax
	movq	(%r9), %r14
	leaq	56(%rbx), %rdi
	movq	%rax, 56(%rbx)
	leaq	176(%rbx), %rax
	movq	%rax, 160(%rbx)
	leaq	216(%rbx), %rax
	movw	%dx, 72(%rbx)
	movw	%cx, 176(%rbx)
	movw	%si, 216(%rbx)
	movq	%r15, %rsi
	movq	$0, 64(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 128(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 192(%rbx)
	movq	%rax, 200(%rbx)
	movq	$0, 208(%rbx)
	movq	$0, 232(%rbx)
	movq	$0, (%r9)
	movups	%xmm0, 136(%rbx)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r15), %rdx
	movl	-80(%rbp), %ecx
	movl	-76(%rbp), %r8d
	movq	104(%rbx), %rdi
	movq	%r14, 104(%rbx)
	movq	%rdx, 88(%rbx)
	movl	%ecx, 96(%rbx)
	movl	%r8d, 100(%rbx)
	testq	%rdi, %rdi
	je	.L4318
	movq	(%rdi), %rax
	call	*64(%rax)
.L4318:
	movl	16(%rbp), %eax
	movl	56(%rbp), %edx
	movq	%r13, %rsi
	movl	%eax, 112(%rbx)
	movq	(%r12), %rax
	movl	%edx, 124(%rax)
	movq	(%r12), %rbx
	leaq	160(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r13), %rax
	movq	%rax, 192(%rbx)
	movl	40(%rbp), %eax
	testl	%eax, %eax
	je	.L4317
	cmpq	$0, 48(%rbp)
	je	.L4317
	movl	40(%rbp), %ecx
	movq	(%r12), %rax
	movl	$8, %edi
	movl	%ecx, 116(%rax)
	movq	(%r12), %r13
	call	_Znwm@PLT
	movq	48(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rbx, -64(%rbp)
	movq	%rax, (%rbx)
	movq	144(%r13), %rsi
	cmpq	152(%r13), %rsi
	je	.L4320
	movq	$0, -64(%rbp)
	movq	%rbx, (%rsi)
	addq	$8, 144(%r13)
.L4321:
	movq	-64(%rbp), %r13
	testq	%r13, %r13
	je	.L4322
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L4323
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L4323:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4322:
	movq	48(%rbp), %rsi
	movq	-96(%rbp), %rdi
	call	_ZN2v85debug18EstimatedValueSizeEPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movl	%eax, %r8d
	movq	(%r12), %rax
	addl	%r8d, 132(%rax)
.L4317:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4341
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4320:
	.cfi_restore_state
	leaq	-64(%rbp), %rdx
	leaq	136(%r13), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN2v86GlobalINS1_5ValueEEESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L4321
.L4341:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7952:
	.size	_ZN12v8_inspector16V8ConsoleMessage18createForExceptionEdRKNS_8String16ES3_jjSt10unique_ptrINS_16V8StackTraceImplESt14default_deleteIS5_EEiPN2v87IsolateES3_iNS9_5LocalINS9_5ValueEEEj, .-_ZN12v8_inspector16V8ConsoleMessage18createForExceptionEdRKNS_8String16ES3_jjSt10unique_ptrINS_16V8StackTraceImplESt14default_deleteIS5_EEiPN2v87IsolateES3_iNS9_5LocalINS9_5ValueEEEj
	.section	.rodata._ZN12v8_inspector16V8ConsoleMessage19createForConsoleAPIEN2v85LocalINS1_7ContextEEEiiPNS_15V8InspectorImplEdNS_14ConsoleAPITypeERKSt6vectorINS2_INS1_5ValueEEESaISA_EERKNS_8String16ESt10unique_ptrINS_16V8StackTraceImplESt14default_deleteISJ_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata._ZN12v8_inspector16V8ConsoleMessage19createForConsoleAPIEN2v85LocalINS1_7ContextEEEiiPNS_15V8InspectorImplEdNS_14ConsoleAPITypeERKSt6vectorINS2_INS1_5ValueEEESaISA_EERKNS_8String16ESt10unique_ptrINS_16V8StackTraceImplESt14default_deleteISJ_EE.str1.1,"aMS",@progbits,1
.LC13:
	.string	" "
	.section	.text._ZN12v8_inspector16V8ConsoleMessage19createForConsoleAPIEN2v85LocalINS1_7ContextEEEiiPNS_15V8InspectorImplEdNS_14ConsoleAPITypeERKSt6vectorINS2_INS1_5ValueEEESaISA_EERKNS_8String16ESt10unique_ptrINS_16V8StackTraceImplESt14default_deleteISJ_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8ConsoleMessage19createForConsoleAPIEN2v85LocalINS1_7ContextEEEiiPNS_15V8InspectorImplEdNS_14ConsoleAPITypeERKSt6vectorINS2_INS1_5ValueEEESaISA_EERKNS_8String16ESt10unique_ptrINS_16V8StackTraceImplESt14default_deleteISJ_EE
	.type	_ZN12v8_inspector16V8ConsoleMessage19createForConsoleAPIEN2v85LocalINS1_7ContextEEEiiPNS_15V8InspectorImplEdNS_14ConsoleAPITypeERKSt6vectorINS2_INS1_5ValueEEESaISA_EERKNS_8String16ESt10unique_ptrINS_16V8StackTraceImplESt14default_deleteISJ_EE, @function
_ZN12v8_inspector16V8ConsoleMessage19createForConsoleAPIEN2v85LocalINS1_7ContextEEEiiPNS_15V8InspectorImplEdNS_14ConsoleAPITypeERKSt6vectorINS2_INS1_5ValueEEESaISA_EERKNS_8String16ESt10unique_ptrINS_16V8StackTraceImplESt14default_deleteISJ_EE:
.LFB7950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rsi, %rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -328(%rbp)
	movq	16(%rbp), %rbx
	movl	%r9d, -316(%rbp)
	movq	24(%rbp), %r14
	movq	%rsi, -288(%rbp)
	movq	32(%rbp), %r12
	movl	%edx, -264(%rbp)
	movl	%ecx, -320(%rbp)
	movsd	%xmm0, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	xorl	%edi, %edi
	movq	$0, -88(%rbp)
	movw	%di, -80(%rbp)
	movq	%rax, %r13
	movl	$240, %edi
	leaq	-80(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movsd	-280(%rbp), %xmm0
	movq	-96(%rbp), %rsi
	leaq	32(%rax), %rdx
	movl	$0, (%rax)
	leaq	16(%rax), %rdi
	movq	%rdx, 16(%rax)
	movq	-88(%rbp), %rdx
	movsd	%xmm0, 8(%rax)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -280(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-64(%rbp), %rdx
	xorl	%r8d, %r8d
	pxor	%xmm0, %xmm0
	movq	-280(%rbp), %rax
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	movq	-96(%rbp), %rdi
	movq	%rdx, 48(%rax)
	leaq	72(%rax), %rdx
	movq	%rdx, 56(%rax)
	leaq	176(%rax), %rdx
	movq	%rdx, 160(%rax)
	leaq	216(%rax), %rdx
	movq	$0, 64(%rax)
	movw	%r8w, 72(%rax)
	movq	$0, 88(%rax)
	movq	$0, 96(%rax)
	movq	$0, 104(%rax)
	movq	$0, 112(%rax)
	movq	$0, 120(%rax)
	movq	$0, 128(%rax)
	movq	$0, 152(%rax)
	movq	$0, 168(%rax)
	movw	%r9w, 176(%rax)
	movq	$0, 192(%rax)
	movq	%rdx, 200(%rax)
	movq	$0, 208(%rax)
	movw	%r10w, 216(%rax)
	movq	$0, 232(%rax)
	movq	%rax, (%r15)
	movups	%xmm0, 136(%rax)
	cmpq	-272(%rbp), %rdi
	je	.L4343
	call	_ZdlPv@PLT
.L4343:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4344
	movq	(%rdi), %rax
	call	*8(%rax)
	testb	%al, %al
	je	.L4345
.L4433:
	movq	(%r12), %rdi
.L4344:
	movq	(%r15), %rax
	movq	$0, (%r12)
	movq	104(%rax), %r8
	movq	%rdi, 104(%rax)
	testq	%r8, %r8
	je	.L4347
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*64(%rax)
.L4347:
	movq	(%r15), %r12
	movq	%r14, %rsi
	leaq	200(%r12), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r14), %rax
	movl	-316(%rbp), %ecx
	movq	%rax, 232(%r12)
	movq	(%r15), %rax
	xorl	%r12d, %r12d
	movl	%ecx, 120(%rax)
	movl	-264(%rbp), %ecx
	movq	(%r15), %rax
	movl	%ecx, 116(%rax)
	leaq	-224(%rbp), %rcx
	movq	(%rbx), %rax
	movq	%rcx, -280(%rbp)
	cmpq	8(%rbx), %rax
	jne	.L4348
	jmp	.L4351
	.p2align 4,,10
	.p2align 3
.L4437:
	movq	-224(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 144(%rdi)
.L4355:
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%r12, %rdx
	jbe	.L4435
	leaq	0(,%r12,8), %rcx
	movq	%r13, %rdi
	movq	(%rax,%rcx), %rsi
	call	_ZN2v85debug18EstimatedValueSizeEPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movl	%eax, %r8d
	movq	(%r15), %rax
	addl	%r8d, 132(%rax)
	movq	-224(%rbp), %r14
	testq	%r14, %r14
	je	.L4357
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4358
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L4358:
	movl	$8, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4357:
	movq	(%rbx), %rax
	movq	8(%rbx), %r14
	addq	$1, %r12
	subq	%rax, %r14
	sarq	$3, %r14
	cmpq	%r14, %r12
	jnb	.L4436
.L4348:
	leaq	0(,%r12,8), %rcx
	movq	(%rax,%r12,8), %r14
	movl	$8, %edi
	movq	%rcx, -264(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdx
	testq	%r14, %r14
	je	.L4353
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-264(%rbp), %rdx
	movq	%rax, %r14
.L4353:
	movq	%r14, (%rdx)
	leaq	_ZN12v8_inspector12_GLOBAL__N_1L32kGlobalConsoleMessageHandleLabelE(%rip), %rsi
	movq	%r14, %rdi
	movq	%rdx, -224(%rbp)
	call	_ZN2v82V822AnnotateStrongRetainerEPmPKc@PLT
	movq	(%r15), %rdi
	movq	144(%rdi), %rsi
	cmpq	152(%rdi), %rsi
	jne	.L4437
	movq	-280(%rbp), %rdx
	addq	$136, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN2v86GlobalINS1_5ValueEEESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L4355
	.p2align 4,,10
	.p2align 3
.L4351:
	movl	-316(%rbp), %ecx
	leal	-14(%rcx), %eax
	cmpl	$1, %eax
	jbe	.L4381
	cmpl	$1, %ecx
	jne	.L4438
.L4381:
	movl	$2, %r13d
.L4349:
	cmpl	$12, -316(%rbp)
	jne	.L4375
.L4342:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4439
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4436:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L4351
	leaq	-224(%rbp), %rcx
	movq	%rbx, -296(%rbp)
	movq	-288(%rbp), %rbx
	xorl	%r13d, %r13d
	movq	%rcx, -280(%rbp)
	leaq	-208(%rbp), %rcx
	leaq	-160(%rbp), %r12
	movq	%rcx, -264(%rbp)
	movq	%r14, -288(%rbp)
	.p2align 4,,10
	.p2align 3
.L4352:
	movq	%rbx, %rdi
	movq	(%rax,%r13,8), %r14
	movl	$10000, -224(%rbp)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	-264(%rbp), %rdi
	movq	%rax, -216(%rbp)
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	pxor	%xmm1, %xmm1
	movq	%rbx, %rdi
	movq	$0, -168(%rbp)
	movups	%xmm1, -184(%rbp)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, -112(%rbp)
	movq	-280(%rbp), %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_120V8ValueStringBuilder6appendEN2v85LocalINS2_5ValueEEEj
	testb	%al, %al
	jne	.L4365
.L4434:
	movq	-272(%rbp), %rax
	movq	$0, -88(%rbp)
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
.L4366:
	movq	%r12, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4368
	call	_ZdlPv@PLT
.L4368:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4369
	call	_ZdlPv@PLT
.L4369:
	movq	(%r15), %r9
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rcx
	movq	24(%r9), %rsi
	movq	16(%r9), %rax
	leaq	32(%r9), %rdx
	leaq	(%r8,%rsi), %r10
	cmpq	%rdx, %rax
	je	.L4383
	movq	32(%r9), %rdx
.L4370:
	cmpq	%rdx, %r10
	ja	.L4371
	testq	%r8, %r8
	je	.L4372
	leaq	(%rax,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L4440
	addq	%r8, %r8
	je	.L4372
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r10, -312(%rbp)
	movq	%r9, -304(%rbp)
	call	memmove@PLT
	movq	-304(%rbp), %r9
	movq	-312(%rbp), %r10
	movq	16(%r9), %rax
	.p2align 4,,10
	.p2align 3
.L4372:
	xorl	%ecx, %ecx
	movq	%r10, 24(%r9)
	movw	%cx, (%rax,%r10,2)
	movq	-96(%rbp), %rdi
	cmpq	-272(%rbp), %rdi
	je	.L4374
	call	_ZdlPv@PLT
.L4374:
	addq	$1, %r13
	cmpq	%r13, -288(%rbp)
	je	.L4351
	leaq	.LC13(%rip), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	(%r15), %r9
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rcx
	movq	24(%r9), %rsi
	movq	16(%r9), %rax
	leaq	32(%r9), %rdx
	leaq	(%r8,%rsi), %r14
	cmpq	%rdx, %rax
	je	.L4382
	movq	32(%r9), %rdx
.L4360:
	cmpq	%rdx, %r14
	ja	.L4361
	testq	%r8, %r8
	je	.L4362
	leaq	(%rax,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L4441
	addq	%r8, %r8
	je	.L4362
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -304(%rbp)
	call	memmove@PLT
	movq	-304(%rbp), %r9
	movq	16(%r9), %rax
	.p2align 4,,10
	.p2align 3
.L4362:
	xorl	%esi, %esi
	movq	%r14, 24(%r9)
	movw	%si, (%rax,%r14,2)
	movq	-96(%rbp), %rdi
	cmpq	-272(%rbp), %rdi
	je	.L4364
	call	_ZdlPv@PLT
.L4364:
	movq	-296(%rbp), %rax
	movq	(%rax), %rax
	jmp	.L4352
	.p2align 4,,10
	.p2align 3
.L4371:
	leaq	16(%r9), %rdi
	xorl	%edx, %edx
	movq	%r10, -312(%rbp)
	movq	%r9, -304(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-304(%rbp), %r9
	movq	-312(%rbp), %r10
	movq	16(%r9), %rax
	jmp	.L4372
	.p2align 4,,10
	.p2align 3
.L4365:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L4434
	movq	-264(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	jmp	.L4366
	.p2align 4,,10
	.p2align 3
.L4383:
	movl	$7, %edx
	jmp	.L4370
	.p2align 4,,10
	.p2align 3
.L4361:
	leaq	16(%r9), %rdi
	xorl	%edx, %edx
	movq	%r9, -304(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-304(%rbp), %r9
	movq	16(%r9), %rax
	jmp	.L4362
	.p2align 4,,10
	.p2align 3
.L4440:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	16(%r9), %rax
	jmp	.L4372
	.p2align 4,,10
	.p2align 3
.L4382:
	movl	$7, %edx
	jmp	.L4360
	.p2align 4,,10
	.p2align 3
.L4345:
	movq	(%r12), %rsi
	leaq	-224(%rbp), %rax
	movq	%rax, %rcx
	movq	(%rsi), %rax
	movq	%rcx, %rdi
	movq	%rcx, -280(%rbp)
	call	*16(%rax)
	leaq	-96(%rbp), %r8
	movq	-280(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -296(%rbp)
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movq	(%r15), %rdx
	movq	-296(%rbp), %r8
	leaq	56(%rdx), %rdi
	movq	%r8, %rsi
	movq	%rdx, -280(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-280(%rbp), %rdx
	movq	%rax, 88(%rdx)
	movq	-96(%rbp), %rdi
	cmpq	-272(%rbp), %rdi
	je	.L4346
	call	_ZdlPv@PLT
.L4346:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	(%r15), %rdx
	movq	(%r12), %rdi
	movl	%eax, 96(%rdx)
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r15), %rdx
	movl	%eax, 100(%rdx)
	jmp	.L4433
	.p2align 4,,10
	.p2align 3
.L4438:
	cmpl	$3, %ecx
	je	.L4384
	cmpl	$13, %ecx
	je	.L4384
	movl	$4, %r13d
	cmpl	$4, %ecx
	jne	.L4349
	movl	$16, %r13d
	.p2align 4,,10
	.p2align 3
.L4375:
	movq	-328(%rbp), %rax
	movq	(%r15), %rsi
	leaq	-224(%rbp), %r8
	leaq	-256(%rbp), %rbx
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	16(%rax), %r12
	addq	$56, %rsi
	movq	(%r12), %rax
	movq	128(%rax), %r14
	movq	48(%rsi), %rax
	movq	%rax, -288(%rbp)
	movl	44(%rsi), %eax
	movl	%eax, -272(%rbp)
	movl	40(%rsi), %eax
	movl	%eax, -280(%rbp)
	call	_ZN12v8_inspector12toStringViewERKNS_8String16E@PLT
	movq	(%r15), %rax
	movq	%rbx, %rdi
	leaq	16(%rax), %rsi
	call	_ZN12v8_inspector12toStringViewERKNS_8String16E@PLT
	leaq	_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE(%rip), %rax
	movq	-264(%rbp), %r8
	cmpq	%rax, %r14
	je	.L4342
	movl	-272(%rbp), %eax
	movl	%r13d, %edx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	pushq	-288(%rbp)
	movl	-280(%rbp), %r9d
	movl	-320(%rbp), %esi
	pushq	%rax
	call	*%r14
	popq	%rax
	popq	%rdx
	jmp	.L4342
	.p2align 4,,10
	.p2align 3
.L4441:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	16(%r9), %rax
	jmp	.L4362
.L4384:
	movl	$8, %r13d
	jmp	.L4349
.L4435:
	movq	%r12, %rsi
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L4439:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7950:
	.size	_ZN12v8_inspector16V8ConsoleMessage19createForConsoleAPIEN2v85LocalINS1_7ContextEEEiiPNS_15V8InspectorImplEdNS_14ConsoleAPITypeERKSt6vectorINS2_INS1_5ValueEEESaISA_EERKNS_8String16ESt10unique_ptrINS_16V8StackTraceImplESt14default_deleteISJ_EE, .-_ZN12v8_inspector16V8ConsoleMessage19createForConsoleAPIEN2v85LocalINS1_7ContextEEEiiPNS_15V8InspectorImplEdNS_14ConsoleAPITypeERKSt6vectorINS2_INS1_5ValueEEESaISA_EERKNS_8String16ESt10unique_ptrINS_16V8StackTraceImplESt14default_deleteISJ_EE
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE24_M_get_insert_unique_posERS1_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE24_M_get_insert_unique_posERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE24_M_get_insert_unique_posERS1_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE24_M_get_insert_unique_posERS1_, @function
_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE24_M_get_insert_unique_posERS1_:
.LFB13616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L4456
	movl	(%rsi), %r8d
	jmp	.L4445
	.p2align 4,,10
	.p2align 3
.L4457:
	movq	16(%rbx), %rax
	movl	$1, %r9d
	testq	%rax, %rax
	je	.L4446
.L4458:
	movq	%rax, %rbx
.L4445:
	movl	32(%rbx), %ecx
	cmpl	%ecx, %r8d
	jl	.L4457
	movq	24(%rbx), %rax
	xorl	%r9d, %r9d
	testq	%rax, %rax
	jne	.L4458
.L4446:
	movq	%rbx, %rsi
	testb	%r9b, %r9b
	jne	.L4444
.L4449:
	xorl	%edx, %edx
	movq	%rbx, %rax
	cmpl	%ecx, %r8d
	cmovg	%rdx, %rax
	cmovg	%rsi, %rdx
.L4451:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4456:
	.cfi_restore_state
	leaq	8(%rdi), %rbx
.L4444:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	cmpq	%rbx, 24(%rdi)
	je	.L4451
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rsi
	movl	(%r12), %r8d
	movl	32(%rax), %ecx
	movq	%rax, %rbx
	jmp	.L4449
	.cfi_endproc
.LFE13616:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE24_M_get_insert_unique_posERS1_, .-_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE24_M_get_insert_unique_posERS1_
	.section	.text._ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_,"axG",@progbits,_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_
	.type	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_, @function
_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_:
.LFB13656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L4490
	movq	8(%rsi), %r10
	movq	(%rsi), %r11
	movabsq	$-2147483649, %r13
	movl	$2147483648, %esi
	.p2align 4,,10
	.p2align 3
.L4462:
	movq	40(%rbx), %r8
	movq	32(%rbx), %r9
	cmpq	%r8, %r10
	movq	%r8, %rcx
	cmovbe	%r10, %rcx
	testq	%rcx, %rcx
	je	.L4464
	xorl	%eax, %eax
	jmp	.L4467
	.p2align 4,,10
	.p2align 3
.L4491:
	ja	.L4466
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L4464
.L4467:
	movzwl	(%r9,%rax,2), %edx
	cmpw	%dx, (%r11,%rax,2)
	jnb	.L4491
.L4465:
	movq	16(%rbx), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L4463
.L4492:
	movq	%rax, %rbx
	jmp	.L4462
	.p2align 4,,10
	.p2align 3
.L4464:
	movq	%r10, %rax
	subq	%r8, %rax
	cmpq	%rsi, %rax
	jge	.L4466
	cmpq	%r13, %rax
	jle	.L4465
	testl	%eax, %eax
	js	.L4465
.L4466:
	movq	24(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L4492
.L4463:
	movq	%rbx, %rsi
	testb	%dl, %dl
	jne	.L4461
.L4469:
	testq	%rcx, %rcx
	je	.L4472
	xorl	%eax, %eax
	jmp	.L4475
	.p2align 4,,10
	.p2align 3
.L4493:
	ja	.L4474
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L4472
.L4475:
	movzwl	(%r11,%rax,2), %edi
	cmpw	%di, (%r9,%rax,2)
	jnb	.L4493
.L4473:
	addq	$8, %rsp
	xorl	%eax, %eax
	movq	%rsi, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4472:
	.cfi_restore_state
	subq	%r10, %r8
	cmpq	$2147483647, %r8
	jg	.L4474
	cmpq	$-2147483648, %r8
	jl	.L4473
	testl	%r8d, %r8d
	js	.L4473
.L4474:
	movq	%rbx, %rax
	xorl	%edx, %edx
.L4486:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4490:
	.cfi_restore_state
	leaq	8(%rdi), %rbx
.L4461:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	cmpq	%rbx, 24(%rdi)
	je	.L4486
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	8(%r12), %r10
	movq	%rbx, %rsi
	movq	(%r12), %r11
	movq	40(%rax), %r8
	movq	32(%rax), %r9
	movq	%rax, %rbx
	cmpq	%r8, %r10
	movq	%r8, %rcx
	cmovbe	%r10, %rcx
	jmp	.L4469
	.cfi_endproc
.LFE13656:
	.size	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_, .-_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_
	.section	.text._ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_,"axG",@progbits,_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_
	.type	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_, @function
_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_:
.LFB12673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	je	.L4505
	leaq	32(%rsi), %r12
	movq	%rsi, %rbx
	movq	%rdx, %rdi
	movq	%r12, %rsi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	js	.L4506
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rbx, %r13
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	xorl	%edx, %edx
	testl	%eax, %eax
	js	.L4507
.L4497:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4506:
	.cfi_restore_state
	movq	24(%r14), %r13
	movq	%r13, %rdx
	cmpq	%rbx, %r13
	je	.L4497
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r15, %rsi
	leaq	32(%rax), %rdi
	movq	%rax, %r12
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	jns	.L4496
	cmpq	$0, 24(%r12)
	movl	$0, %r13d
	cmovne	%rbx, %r13
	cmove	%r12, %rbx
	movq	%rbx, %rdx
	jmp	.L4497
	.p2align 4,,10
	.p2align 3
.L4505:
	cmpq	$0, 40(%rdi)
	je	.L4496
	movq	32(%rdi), %rbx
	movq	%rdx, %rsi
	xorl	%r13d, %r13d
	leaq	32(%rbx), %rdi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	%rbx, %rdx
	testl	%eax, %eax
	js	.L4497
.L4496:
	addq	$24, %rsp
	movq	%r15, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_
	.p2align 4,,10
	.p2align 3
.L4507:
	.cfi_restore_state
	movq	32(%r14), %rax
	cmpq	%rbx, %rax
	je	.L4508
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%r15, %rdi
	leaq	32(%rax), %rsi
	movq	%rax, %r12
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	jns	.L4496
	movq	-56(%rbp), %rdx
	cmpq	$0, 24(%rbx)
	cmovne	%r12, %rdx
	cmpq	$0, 24(%rbx)
	cmovne	%r12, %rbx
	movq	%rdx, %r13
	movq	%rbx, %rdx
	jmp	.L4497
	.p2align 4,,10
	.p2align 3
.L4508:
	xorl	%r13d, %r13d
	movq	%rax, %rdx
	jmp	.L4497
	.cfi_endproc
.LFE12673:
	.size	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_, .-_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_
	.section	.text._ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS3_EESF_IJEEEEESt17_Rb_tree_iteratorIS4_ESt23_Rb_tree_const_iteratorIS4_EDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS3_EESF_IJEEEEESt17_Rb_tree_iteratorIS4_ESt23_Rb_tree_const_iteratorIS4_EDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS3_EESF_IJEEEEESt17_Rb_tree_iteratorIS4_ESt23_Rb_tree_const_iteratorIS4_EDpOT_
	.type	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS3_EESF_IJEEEEESt17_Rb_tree_iteratorIS4_ESt23_Rb_tree_const_iteratorIS4_EDpOT_, @function
_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS3_EESF_IJEEEEESt17_Rb_tree_iteratorIS4_ESt23_Rb_tree_const_iteratorIS4_EDpOT_:
.LFB10882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$80, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	movq	(%r14), %r14
	leaq	48(%rax), %rcx
	movq	%rax, %r12
	leaq	32(%rax), %r15
	movq	%rcx, 32(%rax)
	movq	(%r14), %rsi
	movq	%r15, %rdi
	movq	8(%r14), %rax
	movq	%rcx, -56(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%r14), %rax
	movq	%r13, %rsi
	movq	%r15, %rdx
	movl	$0, 72(%r12)
	movq	%rbx, %rdi
	movq	%rax, 64(%r12)
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_
	movq	-56(%rbp), %rcx
	testq	%rdx, %rdx
	movq	%rax, %r13
	je	.L4510
	leaq	8(%rbx), %rcx
	movl	$1, %edi
	testq	%rax, %rax
	je	.L4517
.L4511:
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4517:
	.cfi_restore_state
	cmpq	%rcx, %rdx
	je	.L4511
	leaq	32(%rdx), %rsi
	movq	%r15, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdx
	shrl	$31, %eax
	movl	%eax, %edi
	jmp	.L4511
	.p2align 4,,10
	.p2align 3
.L4510:
	movq	32(%r12), %rdi
	cmpq	%rdi, %rcx
	je	.L4513
	call	_ZdlPv@PLT
.L4513:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10882:
	.size	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS3_EESF_IJEEEEESt17_Rb_tree_iteratorIS4_ESt23_Rb_tree_const_iteratorIS4_EDpOT_, .-_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS3_EESF_IJEEEEESt17_Rb_tree_iteratorIS4_ESt23_Rb_tree_const_iteratorIS4_EDpOT_
	.section	.text._ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_,"axG",@progbits,_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_
	.type	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_, @function
_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_:
.LFB13674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L4549
	movq	8(%rsi), %r10
	movq	(%rsi), %r11
	movabsq	$-2147483649, %r13
	movl	$2147483648, %esi
	.p2align 4,,10
	.p2align 3
.L4521:
	movq	40(%rbx), %r8
	movq	32(%rbx), %r9
	cmpq	%r8, %r10
	movq	%r8, %rcx
	cmovbe	%r10, %rcx
	testq	%rcx, %rcx
	je	.L4523
	xorl	%eax, %eax
	jmp	.L4526
	.p2align 4,,10
	.p2align 3
.L4550:
	ja	.L4525
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L4523
.L4526:
	movzwl	(%r9,%rax,2), %edx
	cmpw	%dx, (%r11,%rax,2)
	jnb	.L4550
.L4524:
	movq	16(%rbx), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L4522
.L4551:
	movq	%rax, %rbx
	jmp	.L4521
	.p2align 4,,10
	.p2align 3
.L4523:
	movq	%r10, %rax
	subq	%r8, %rax
	cmpq	%rsi, %rax
	jge	.L4525
	cmpq	%r13, %rax
	jle	.L4524
	testl	%eax, %eax
	js	.L4524
.L4525:
	movq	24(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L4551
.L4522:
	movq	%rbx, %rsi
	testb	%dl, %dl
	jne	.L4520
.L4528:
	testq	%rcx, %rcx
	je	.L4531
	xorl	%eax, %eax
	jmp	.L4534
	.p2align 4,,10
	.p2align 3
.L4552:
	ja	.L4533
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L4531
.L4534:
	movzwl	(%r11,%rax,2), %edi
	cmpw	%di, (%r9,%rax,2)
	jnb	.L4552
.L4532:
	addq	$8, %rsp
	xorl	%eax, %eax
	movq	%rsi, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4531:
	.cfi_restore_state
	subq	%r10, %r8
	cmpq	$2147483647, %r8
	jg	.L4533
	cmpq	$-2147483648, %r8
	jl	.L4532
	testl	%r8d, %r8d
	js	.L4532
.L4533:
	movq	%rbx, %rax
	xorl	%edx, %edx
.L4545:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4549:
	.cfi_restore_state
	leaq	8(%rdi), %rbx
.L4520:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	cmpq	%rbx, 24(%rdi)
	je	.L4545
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	8(%r12), %r10
	movq	%rbx, %rsi
	movq	(%r12), %r11
	movq	40(%rax), %r8
	movq	32(%rax), %r9
	movq	%rax, %rbx
	cmpq	%r8, %r10
	movq	%r8, %rcx
	cmovbe	%r10, %rcx
	jmp	.L4528
	.cfi_endproc
.LFE13674:
	.size	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_, .-_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_
	.section	.text._ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_,"axG",@progbits,_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_
	.type	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_, @function
_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_:
.LFB12683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	je	.L4564
	leaq	32(%rsi), %r12
	movq	%rsi, %rbx
	movq	%rdx, %rdi
	movq	%r12, %rsi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	js	.L4565
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rbx, %r13
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	xorl	%edx, %edx
	testl	%eax, %eax
	js	.L4566
.L4556:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4565:
	.cfi_restore_state
	movq	24(%r14), %r13
	movq	%r13, %rdx
	cmpq	%rbx, %r13
	je	.L4556
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r15, %rsi
	leaq	32(%rax), %rdi
	movq	%rax, %r12
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	jns	.L4555
	cmpq	$0, 24(%r12)
	movl	$0, %r13d
	cmovne	%rbx, %r13
	cmove	%r12, %rbx
	movq	%rbx, %rdx
	jmp	.L4556
	.p2align 4,,10
	.p2align 3
.L4564:
	cmpq	$0, 40(%rdi)
	je	.L4555
	movq	32(%rdi), %rbx
	movq	%rdx, %rsi
	xorl	%r13d, %r13d
	leaq	32(%rbx), %rdi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	%rbx, %rdx
	testl	%eax, %eax
	js	.L4556
.L4555:
	addq	$24, %rsp
	movq	%r15, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE24_M_get_insert_unique_posERS3_
	.p2align 4,,10
	.p2align 3
.L4566:
	.cfi_restore_state
	movq	32(%r14), %rax
	cmpq	%rbx, %rax
	je	.L4567
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%r15, %rdi
	leaq	32(%rax), %rsi
	movq	%rax, %r12
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	jns	.L4555
	movq	-56(%rbp), %rdx
	cmpq	$0, 24(%rbx)
	cmovne	%r12, %rdx
	cmpq	$0, 24(%rbx)
	cmovne	%r12, %rbx
	movq	%rdx, %r13
	movq	%rbx, %rdx
	jmp	.L4556
	.p2align 4,,10
	.p2align 3
.L4567:
	xorl	%r13d, %r13d
	movq	%rax, %rdx
	jmp	.L4556
	.cfi_endproc
.LFE12683:
	.size	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_, .-_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_
	.section	.text._ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE24_M_get_insert_unique_posERKS1_,"axG",@progbits,_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE24_M_get_insert_unique_posERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE24_M_get_insert_unique_posERKS1_
	.type	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE24_M_get_insert_unique_posERKS1_, @function
_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE24_M_get_insert_unique_posERKS1_:
.LFB13986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L4599
	movq	8(%rsi), %r10
	movq	(%rsi), %r11
	movabsq	$-2147483649, %r13
	movl	$2147483648, %esi
	.p2align 4,,10
	.p2align 3
.L4571:
	movq	40(%rbx), %r8
	movq	32(%rbx), %r9
	cmpq	%r8, %r10
	movq	%r8, %rcx
	cmovbe	%r10, %rcx
	testq	%rcx, %rcx
	je	.L4573
	xorl	%eax, %eax
	jmp	.L4576
	.p2align 4,,10
	.p2align 3
.L4600:
	ja	.L4575
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L4573
.L4576:
	movzwl	(%r9,%rax,2), %edx
	cmpw	%dx, (%r11,%rax,2)
	jnb	.L4600
.L4574:
	movq	16(%rbx), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L4572
.L4601:
	movq	%rax, %rbx
	jmp	.L4571
	.p2align 4,,10
	.p2align 3
.L4573:
	movq	%r10, %rax
	subq	%r8, %rax
	cmpq	%rsi, %rax
	jge	.L4575
	cmpq	%r13, %rax
	jle	.L4574
	testl	%eax, %eax
	js	.L4574
.L4575:
	movq	24(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L4601
.L4572:
	movq	%rbx, %rsi
	testb	%dl, %dl
	jne	.L4570
.L4578:
	testq	%rcx, %rcx
	je	.L4581
	xorl	%eax, %eax
	jmp	.L4584
	.p2align 4,,10
	.p2align 3
.L4602:
	ja	.L4583
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L4581
.L4584:
	movzwl	(%r11,%rax,2), %edi
	cmpw	%di, (%r9,%rax,2)
	jnb	.L4602
.L4582:
	addq	$8, %rsp
	xorl	%eax, %eax
	movq	%rsi, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4581:
	.cfi_restore_state
	subq	%r10, %r8
	cmpq	$2147483647, %r8
	jg	.L4583
	cmpq	$-2147483648, %r8
	jl	.L4582
	testl	%r8d, %r8d
	js	.L4582
.L4583:
	movq	%rbx, %rax
	xorl	%edx, %edx
.L4595:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4599:
	.cfi_restore_state
	leaq	8(%rdi), %rbx
.L4570:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	cmpq	%rbx, 24(%rdi)
	je	.L4595
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	8(%r12), %r10
	movq	%rbx, %rsi
	movq	(%r12), %r11
	movq	40(%rax), %r8
	movq	32(%rax), %r9
	movq	%rax, %rbx
	cmpq	%r8, %r10
	movq	%r8, %rcx
	cmovbe	%r10, %rcx
	jmp	.L4578
	.cfi_endproc
.LFE13986:
	.size	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE24_M_get_insert_unique_posERKS1_, .-_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE24_M_get_insert_unique_posERKS1_
	.section	.text._ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS1_ERKS1_,"axG",@progbits,_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS1_ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS1_ERKS1_
	.type	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS1_ERKS1_, @function
_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS1_ERKS1_:
.LFB13638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	je	.L4614
	leaq	32(%rsi), %r12
	movq	%rsi, %rbx
	movq	%rdx, %rdi
	movq	%r12, %rsi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	js	.L4615
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rbx, %r13
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	xorl	%edx, %edx
	testl	%eax, %eax
	js	.L4616
.L4606:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4615:
	.cfi_restore_state
	movq	24(%r14), %r13
	movq	%r13, %rdx
	cmpq	%rbx, %r13
	je	.L4606
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r15, %rsi
	leaq	32(%rax), %rdi
	movq	%rax, %r12
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	jns	.L4605
	cmpq	$0, 24(%r12)
	movl	$0, %r13d
	cmovne	%rbx, %r13
	cmove	%r12, %rbx
	movq	%rbx, %rdx
	jmp	.L4606
	.p2align 4,,10
	.p2align 3
.L4614:
	cmpq	$0, 40(%rdi)
	je	.L4605
	movq	32(%rdi), %rbx
	movq	%rdx, %rsi
	xorl	%r13d, %r13d
	leaq	32(%rbx), %rdi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	%rbx, %rdx
	testl	%eax, %eax
	js	.L4606
.L4605:
	addq	$24, %rsp
	movq	%r15, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE24_M_get_insert_unique_posERKS1_
	.p2align 4,,10
	.p2align 3
.L4616:
	.cfi_restore_state
	movq	32(%r14), %rax
	cmpq	%rbx, %rax
	je	.L4617
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%r15, %rdi
	leaq	32(%rax), %rsi
	movq	%rax, %r12
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	jns	.L4605
	movq	-56(%rbp), %rdx
	cmpq	$0, 24(%rbx)
	cmovne	%r12, %rdx
	cmpq	$0, 24(%rbx)
	cmovne	%r12, %rbx
	movq	%rdx, %r13
	movq	%rbx, %rdx
	jmp	.L4606
	.p2align 4,,10
	.p2align 3
.L4617:
	xorl	%r13d, %r13d
	movq	%rax, %rdx
	jmp	.L4606
	.cfi_endproc
.LFE13638:
	.size	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS1_ERKS1_, .-_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS1_ERKS1_
	.section	.text._ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE8_M_eraseEPSt13_Rb_tree_nodeIS1_E,"axG",@progbits,_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE8_M_eraseEPSt13_Rb_tree_nodeIS1_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE8_M_eraseEPSt13_Rb_tree_nodeIS1_E
	.type	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE8_M_eraseEPSt13_Rb_tree_nodeIS1_E, @function
_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE8_M_eraseEPSt13_Rb_tree_nodeIS1_E:
.LFB14309:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L4633
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L4622:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE8_M_eraseEPSt13_Rb_tree_nodeIS1_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L4620
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L4618
.L4621:
	movq	%rbx, %r12
	jmp	.L4622
	.p2align 4,,10
	.p2align 3
.L4620:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L4621
.L4618:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4633:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE14309:
	.size	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE8_M_eraseEPSt13_Rb_tree_nodeIS1_E, .-_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE8_M_eraseEPSt13_Rb_tree_nodeIS1_E
	.section	.text._ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB14313:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L4651
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L4640:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L4638
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L4636
.L4639:
	movq	%rbx, %r12
	jmp	.L4640
	.p2align 4,,10
	.p2align 3
.L4638:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L4639
.L4636:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4651:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE14313:
	.size	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB14317:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L4669
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L4658:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L4656
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L4654
.L4657:
	movq	%rbx, %r12
	jmp	.L4658
	.p2align 4,,10
	.p2align 3
.L4656:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L4657
.L4654:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4669:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE14317:
	.size	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB10812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	testq	%rsi, %rsi
	je	.L4672
	movq	%rsi, %rbx
.L4686:
	movq	24(%rbx), %rsi
	movq	-64(%rbp), %rdi
	movq	%rbx, %r12
	leaq	136(%r12), %r15
	call	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	leaq	40(%r12), %rax
	movq	16(%rbx), %rbx
	movq	152(%r12), %r14
	movq	%rax, -56(%rbp)
	testq	%r14, %r14
	je	.L4674
.L4677:
	movq	24(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	32(%r14), %rdi
	leaq	48(%r14), %rax
	movq	16(%r14), %r13
	cmpq	%rax, %rdi
	je	.L4675
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L4674
.L4676:
	movq	%r13, %r14
	jmp	.L4677
	.p2align 4,,10
	.p2align 3
.L4675:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L4676
.L4674:
	movq	104(%r12), %r14
	leaq	88(%r12), %r15
	testq	%r14, %r14
	je	.L4678
.L4681:
	movq	24(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	32(%r14), %rdi
	leaq	48(%r14), %rax
	movq	16(%r14), %r13
	cmpq	%rax, %rdi
	je	.L4679
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L4678
.L4680:
	movq	%r13, %r14
	jmp	.L4681
	.p2align 4,,10
	.p2align 3
.L4679:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L4680
.L4678:
	movq	56(%r12), %r15
	testq	%r15, %r15
	je	.L4682
.L4685:
	movq	-56(%rbp), %rdi
	movq	24(%r15), %rsi
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE8_M_eraseEPSt13_Rb_tree_nodeIS1_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r13
	cmpq	%rax, %rdi
	je	.L4683
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L4682
.L4684:
	movq	%r13, %r15
	jmp	.L4685
	.p2align 4,,10
	.p2align 3
.L4683:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L4684
.L4682:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L4686
.L4672:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10812:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZN12v8_inspector23V8ConsoleMessageStorage5clearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorage5clearEv
	.type	_ZN12v8_inspector23V8ConsoleMessageStorage5clearEv, @function
_ZN12v8_inspector23V8ConsoleMessageStorage5clearEv:
.LFB7980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-160(%rbp), %rdx
	leaq	-128(%rbp), %rsi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$16, %rdi
	subq	$136, %rsp
	movdqu	48(%rdi), %xmm1
	movq	40(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movdqu	64(%rdi), %xmm2
	movdqu	16(%rdi), %xmm3
	movdqu	32(%rdi), %xmm4
	leaq	8(%r14), %r15
	movaps	%xmm1, -160(%rbp)
	movq	%rax, -168(%rbp)
	movq	32(%rdi), %rax
	movq	16(%rdi), %r13
	movaps	%xmm2, -144(%rbp)
	movq	%rax, -176(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	88(%rbx), %rax
	leaq	8(%rax), %r12
	cmpq	%r15, %r12
	jbe	.L4720
	.p2align 4,,10
	.p2align 3
.L4721:
	movq	(%r15), %rdi
	addq	$8, %r15
	call	_ZdlPv@PLT
	cmpq	%r15, %r12
	ja	.L4721
.L4720:
	movq	%r13, %xmm0
	movq	%r14, %xmm5
	movq	(%rbx), %rdi
	movl	8(%rbx), %esi
	movhps	-168(%rbp), %xmm0
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_23V8ConsoleMessageStorage5clearEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	leaq	-96(%rbp), %r12
	movl	$0, 12(%rbx)
	movups	%xmm0, 64(%rbx)
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector23V8ConsoleMessageStorage5clearEvEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation(%rip), %rcx
	movq	%rax, %xmm6
	movq	%r12, %rdx
	movq	-176(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 80(%rbx)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L4722
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L4722:
	movq	112(%rbx), %rsi
	leaq	96(%rbx), %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	leaq	104(%rbx), %rax
	movq	$0, 112(%rbx)
	movq	%rax, 120(%rbx)
	movq	%rax, 128(%rbx)
	movq	$0, 136(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4729
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4729:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7980:
	.size	_ZN12v8_inspector23V8ConsoleMessageStorage5clearEv, .-_ZN12v8_inspector23V8ConsoleMessageStorage5clearEv
	.section	.rodata._ZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS_16V8ConsoleMessageESt14default_deleteIS2_EE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"v8.console"
.LC15:
	.string	"V8ConsoleMessage::Exception"
.LC16:
	.string	"V8ConsoleMessage::Error"
.LC17:
	.string	"V8ConsoleMessage::Assert"
	.section	.text._ZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS_16V8ConsoleMessageESt14default_deleteIS2_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS_16V8ConsoleMessageESt14default_deleteIS2_EE
	.type	_ZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS_16V8ConsoleMessageESt14default_deleteIS2_EE, @function
_ZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS_16V8ConsoleMessageESt14default_deleteIS2_EE:
.LFB7974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rsi), %rdx
	movl	8(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %r13
	movl	120(%rdx), %eax
	cmpl	$12, %eax
	je	.L4831
.L4731:
	cmpl	$1, (%rdx)
	je	.L4832
	cmpl	$3, %eax
	je	.L4833
	leaq	-96(%rbp), %r15
	cmpl	$13, %eax
	je	.L4834
.L4737:
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS0_16V8ConsoleMessageESt14default_deleteIS6_EEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS1_16V8ConsoleMessageESt14default_deleteIS4_EEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation(%rip), %rdi
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	%rax, %xmm1
	movq	%rdi, %xmm0
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L4755
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L4755:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl24hasConsoleMessageStorageEi@PLT
	testb	%al, %al
	je	.L4730
	movq	64(%rbx), %rsi
	movq	88(%rbx), %rdi
	subq	56(%rbx), %rdi
	movq	48(%rbx), %r9
	sarq	$3, %rdi
	movq	%rsi, %rcx
	subq	72(%rbx), %rcx
	movq	32(%rbx), %rax
	subq	$1, %rdi
	sarq	$3, %rcx
	movl	12(%rbx), %edx
	salq	$6, %rdi
	leaq	(%rdi,%rcx), %rdi
	movq	%r9, %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	addq	%rdi, %rcx
	cmpq	$1000, %rcx
	je	.L4835
.L4757:
	movq	(%r12), %rdi
	jmp	.L4827
	.p2align 4,,10
	.p2align 3
.L4836:
	testq	%r13, %r13
	je	.L4764
	movq	%r13, %rdi
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev
	movq	%r13, %rdi
	movl	$240, %esi
	call	_ZdlPvm@PLT
	movq	(%r12), %rdi
.L4764:
	movq	32(%rbx), %rax
	addq	$8, %rax
	movq	%rax, 32(%rbx)
.L4765:
	movl	12(%rbx), %edx
	movq	64(%rbx), %rsi
.L4827:
	movq	24(%rdi), %rcx
	leal	(%rdx,%rcx,2), %ecx
	addl	132(%rdi), %ecx
	cmpl	$10485760, %ecx
	jle	.L4774
	cmpq	%rax, %rsi
	je	.L4762
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	movl	132(%rcx), %ecx
	leal	(%rcx,%rsi,2), %ecx
	movq	48(%rbx), %rsi
	subl	%ecx, %edx
	movl	%edx, 12(%rbx)
	leaq	-8(%rsi), %rdx
	movq	(%rax), %r13
	cmpq	%rdx, %rax
	jne	.L4836
	testq	%r13, %r13
	je	.L4766
	movq	%r13, %rdi
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev
	movl	$240, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4766:
	movq	40(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	56(%rbx), %rax
	movq	(%r12), %rdi
	leaq	8(%rax), %rdx
	movq	%rdx, 56(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 40(%rbx)
	movq	%rdx, 48(%rbx)
	movq	%rax, 32(%rbx)
	jmp	.L4765
	.p2align 4,,10
	.p2align 3
.L4774:
	movq	%rsi, %rax
.L4762:
	movq	80(%rbx), %rsi
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rax
	je	.L4767
	movq	$0, (%r12)
	movq	%rdi, (%rax)
	movq	64(%rbx), %rax
	addq	$8, %rax
	movq	%rax, 64(%rbx)
.L4768:
	cmpq	%rax, 72(%rbx)
	jne	.L4769
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
.L4769:
	movq	-8(%rax), %rax
	movq	24(%rax), %rdx
	movl	132(%rax), %eax
	leal	(%rax,%rdx,2), %eax
	addl	%eax, 12(%rbx)
.L4730:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4837
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4832:
	.cfi_restore_state
	movq	_ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic517(%rip), %rdx
	testq	%rdx, %rdx
	je	.L4838
.L4734:
	movzbl	(%rdx), %eax
	leaq	-96(%rbp), %r15
	testb	$5, %al
	je	.L4737
	pxor	%xmm0, %xmm0
	movq	%rdx, -104(%rbp)
	leaq	-96(%rbp), %r15
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	-104(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4839
.L4752:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4753
	movq	(%rdi), %rax
	call	*8(%rax)
.L4753:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4737
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L4737
	.p2align 4,,10
	.p2align 3
.L4831:
	call	_ZN12v8_inspector23V8ConsoleMessageStorage5clearEv
	movq	(%r12), %rdx
	movl	120(%rdx), %eax
	jmp	.L4731
	.p2align 4,,10
	.p2align 3
.L4838:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4840
.L4735:
	movq	%rdx, _ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic517(%rip)
	jmp	.L4734
	.p2align 4,,10
	.p2align 3
.L4833:
	movq	_ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic520(%rip), %rdx
	testq	%rdx, %rdx
	je	.L4841
.L4743:
	movzbl	(%rdx), %eax
	leaq	-96(%rbp), %r15
	testb	$5, %al
	je	.L4737
	pxor	%xmm0, %xmm0
	movq	%rdx, -104(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	-104(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4752
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$73, %esi
	pushq	$16
	leaq	.LC16(%rip), %rcx
	pushq	%r15
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L4752
	.p2align 4,,10
	.p2align 3
.L4835:
	movq	(%rax), %rcx
	subq	$8, %r9
	movq	24(%rcx), %rsi
	movl	132(%rcx), %ecx
	leal	(%rcx,%rsi,2), %ecx
	subl	%ecx, %edx
	movl	%edx, 12(%rbx)
	movq	(%rax), %r13
	cmpq	%r9, %rax
	je	.L4758
	testq	%r13, %r13
	je	.L4759
	movq	%r13, %rdi
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev
	movl	$240, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4759:
	movq	32(%rbx), %rax
	movl	12(%rbx), %edx
	movq	64(%rbx), %rsi
	addq	$8, %rax
	movq	%rax, 32(%rbx)
	jmp	.L4757
	.p2align 4,,10
	.p2align 3
.L4834:
	movq	_ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic523(%rip), %rdx
	testq	%rdx, %rdx
	je	.L4842
.L4750:
	movzbl	(%rdx), %eax
	leaq	-96(%rbp), %r15
	testb	$5, %al
	je	.L4737
	pxor	%xmm0, %xmm0
	movq	%rdx, -104(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	-104(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4752
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$73, %esi
	pushq	$16
	leaq	.LC17(%rip), %rcx
	pushq	%r15
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L4752
	.p2align 4,,10
	.p2align 3
.L4767:
	leaq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_
	movq	64(%rbx), %rax
	jmp	.L4768
.L4841:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4843
.L4744:
	movq	%rdx, _ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic520(%rip)
	jmp	.L4743
.L4843:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L4744
.L4842:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4844
.L4751:
	movq	%rdx, _ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic523(%rip)
	jmp	.L4750
.L4758:
	testq	%r13, %r13
	je	.L4760
	movq	%r13, %rdi
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev
	movl	$240, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4760:
	movq	40(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	56(%rbx), %rax
	movq	64(%rbx), %rsi
	leaq	8(%rax), %rdx
	movq	%rdx, 56(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 40(%rbx)
	movq	%rdx, 48(%rbx)
	movl	12(%rbx), %edx
	movq	%rax, 32(%rbx)
	jmp	.L4757
.L4840:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L4735
.L4839:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$73, %esi
	pushq	$16
	leaq	.LC15(%rip), %rcx
	pushq	%r15
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L4752
.L4844:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L4751
.L4837:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7974:
	.size	_ZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS_16V8ConsoleMessageESt14default_deleteIS2_EE, .-_ZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS_16V8ConsoleMessageESt14default_deleteIS2_EE
	.section	.text._ZN12v8_inspector23V8ConsoleMessageStorageD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorageD2Ev
	.type	_ZN12v8_inspector23V8ConsoleMessageStorageD2Ev, @function
_ZN12v8_inspector23V8ConsoleMessageStorageD2Ev:
.LFB7971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector23V8ConsoleMessageStorage5clearEv
	movq	112(%rbx), %rsi
	leaq	96(%rbx), %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	leaq	16(%rbx), %rdi
	movdqu	32(%rbx), %xmm2
	movdqu	48(%rbx), %xmm3
	leaq	-112(%rbp), %rdx
	leaq	-80(%rbp), %rsi
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm3, -64(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN12v8_inspector16V8ConsoleMessageESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4845
	movq	88(%rbx), %rax
	movq	56(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L4847
	.p2align 4,,10
	.p2align 3
.L4848:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L4848
	movq	16(%rbx), %rdi
.L4847:
	call	_ZdlPv@PLT
.L4845:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4855
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4855:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7971:
	.size	_ZN12v8_inspector23V8ConsoleMessageStorageD2Ev, .-_ZN12v8_inspector23V8ConsoleMessageStorageD2Ev
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorageD1Ev
	.set	_ZN12v8_inspector23V8ConsoleMessageStorageD1Ev,_ZN12v8_inspector23V8ConsoleMessageStorageD2Ev
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE5eraseERS1_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE5eraseERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE5eraseERS1_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE5eraseERS1_, @function
_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE5eraseERS1_:
.LFB10902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r8
	testq	%r8, %r8
	je	.L4887
	movl	(%rsi), %ecx
	movq	%r15, %r14
	movq	%r8, %rbx
	jmp	.L4858
	.p2align 4,,10
	.p2align 3
.L4940:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L4939
.L4888:
	movq	%rax, %rbx
.L4858:
	cmpl	%ecx, 32(%rbx)
	jl	.L4940
	movq	16(%rbx), %rax
	jle	.L4941
	movq	%rbx, %r14
	testq	%rax, %rax
	jne	.L4888
.L4939:
	cmpq	%r14, %r15
	sete	%al
.L4857:
	movq	40(%r13), %rdx
	movq	%rdx, -64(%rbp)
	cmpq	%r14, 24(%r13)
	jne	.L4891
	testb	%al, %al
	je	.L4891
.L4869:
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	$0, 16(%r13)
	movq	%r15, 24(%r13)
	movq	%r15, 32(%r13)
	movq	$0, 40(%r13)
.L4856:
	movq	-64(%rbp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4941:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L4862
	jmp	.L4868
	.p2align 4,,10
	.p2align 3
.L4942:
	movq	%rdx, %r14
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L4868
.L4862:
	cmpl	32(%rdx), %ecx
	jl	.L4942
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L4862
	.p2align 4,,10
	.p2align 3
.L4868:
	testq	%rax, %rax
	je	.L4863
.L4943:
	cmpl	32(%rax), %ecx
	jg	.L4867
	movq	%rax, %rbx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L4943
.L4863:
	movq	40(%r13), %rax
	movq	%rax, -64(%rbp)
	cmpq	%rbx, 24(%r13)
	je	.L4944
.L4871:
	cmpq	%rbx, %r14
	je	.L4891
	.p2align 4,,10
	.p2align 3
.L4883:
	movq	%rbx, %r12
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rax, %rbx
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %r12
	leaq	40(%rax), %rax
	movq	152(%r12), %r8
	movq	%rax, -72(%rbp)
	leaq	136(%r12), %rax
	movq	%rax, -80(%rbp)
	testq	%r8, %r8
	je	.L4877
.L4873:
	movq	24(%r8), %rsi
	movq	-80(%rbp), %rdi
	movq	%r8, -56(%rbp)
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	-56(%rbp), %r8
	movq	16(%r8), %rax
	movq	32(%r8), %rdi
	movq	%rax, -56(%rbp)
	leaq	48(%r8), %rax
	cmpq	%rax, %rdi
	je	.L4876
	movq	%r8, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r8
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	cmpq	$0, -56(%rbp)
	je	.L4877
.L4878:
	movq	-56(%rbp), %r8
	jmp	.L4873
	.p2align 4,,10
	.p2align 3
.L4867:
	movq	24(%rax), %rax
	jmp	.L4868
	.p2align 4,,10
	.p2align 3
.L4876:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	cmpq	$0, -56(%rbp)
	jne	.L4878
.L4877:
	movq	104(%r12), %r8
	leaq	88(%r12), %rax
	movq	%rax, -80(%rbp)
	testq	%r8, %r8
	je	.L4874
.L4875:
	movq	24(%r8), %rsi
	movq	-80(%rbp), %rdi
	movq	%r8, -56(%rbp)
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	-56(%rbp), %r8
	movq	16(%r8), %rax
	movq	32(%r8), %rdi
	movq	%rax, -56(%rbp)
	leaq	48(%r8), %rax
	cmpq	%rax, %rdi
	je	.L4881
	movq	%r8, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r8
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	cmpq	$0, -56(%rbp)
	je	.L4874
.L4882:
	movq	-56(%rbp), %r8
	jmp	.L4875
	.p2align 4,,10
	.p2align 3
.L4881:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	cmpq	$0, -56(%rbp)
	jne	.L4882
.L4874:
	movq	56(%r12), %r8
	testq	%r8, %r8
	je	.L4879
.L4880:
	movq	24(%r8), %rsi
	movq	-72(%rbp), %rdi
	movq	%r8, -56(%rbp)
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE8_M_eraseEPSt13_Rb_tree_nodeIS1_E
	movq	-56(%rbp), %r8
	movq	16(%r8), %rax
	movq	32(%r8), %rdi
	movq	%rax, -56(%rbp)
	leaq	48(%r8), %rax
	cmpq	%rax, %rdi
	je	.L4885
	movq	%r8, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r8
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	cmpq	$0, -56(%rbp)
	je	.L4879
.L4886:
	movq	-56(%rbp), %r8
	jmp	.L4880
	.p2align 4,,10
	.p2align 3
.L4885:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	cmpq	$0, -56(%rbp)
	jne	.L4886
.L4879:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	40(%r13), %rax
	subq	$1, %rax
	movq	%rax, 40(%r13)
	cmpq	%rbx, %r14
	jne	.L4883
	subq	%rax, -64(%rbp)
	jmp	.L4856
	.p2align 4,,10
	.p2align 3
.L4944:
	cmpq	%r15, %r14
	jne	.L4871
	jmp	.L4869
	.p2align 4,,10
	.p2align 3
.L4891:
	movq	$0, -64(%rbp)
	jmp	.L4856
	.p2align 4,,10
	.p2align 3
.L4887:
	movq	%r15, %r14
	movl	$1, %eax
	jmp	.L4857
	.cfi_endproc
.LFE10902:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE5eraseERS1_, .-_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE5eraseERS1_
	.section	.text._ZN12v8_inspector23V8ConsoleMessageStorage16contextDestroyedEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorage16contextDestroyedEi
	.type	_ZN12v8_inspector23V8ConsoleMessageStorage16contextDestroyedEi, @function
_ZN12v8_inspector23V8ConsoleMessageStorage16contextDestroyedEi:
.LFB7991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	$0, 12(%rdi)
	movq	88(%rbx), %r11
	movq	56(%rdi), %rdi
	movq	64(%rbx), %r10
	movl	%esi, -20(%rbp)
	movq	%r11, %rax
	movq	48(%rbx), %r9
	movq	32(%rbx), %rsi
	subq	%rdi, %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$6, %rax
	movq	%rax, %rdx
	movq	%r10, %rax
	subq	72(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	movq	%r9, %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
	addq	%rdx, %rax
	je	.L4959
	movq	40(%rbx), %r8
	xorl	%r12d, %r12d
	jmp	.L4958
	.p2align 4,,10
	.p2align 3
.L4970:
	cmpq	$63, %rax
	jg	.L4950
	leaq	(%rsi,%r12,8), %rax
.L4951:
	movq	(%rax), %rdi
	movl	116(%rdi), %eax
	cmpl	%eax, -20(%rbp)
	jne	.L4953
	call	_ZN12v8_inspector16V8ConsoleMessage16contextDestroyedEi.part.0
	movq	40(%rbx), %r8
	movq	88(%rbx), %r11
	movq	64(%rbx), %r10
	movq	48(%rbx), %r9
.L4953:
	movq	32(%rbx), %rsi
	movq	56(%rbx), %rdi
	movq	%rsi, %rax
	subq	%r8, %rax
	sarq	$3, %rax
	addq	%r12, %rax
	js	.L4954
	cmpq	$63, %rax
	jg	.L4955
	leaq	(%rsi,%r12,8), %rax
.L4956:
	movq	(%rax), %rax
	addq	$1, %r12
	movq	24(%rax), %rdx
	movl	132(%rax), %eax
	leal	(%rax,%rdx,2), %eax
	movq	%r11, %rdx
	addl	%eax, 12(%rbx)
	movq	%r10, %rax
	subq	%rdi, %rdx
	subq	72(%rbx), %rax
	sarq	$3, %rdx
	sarq	$3, %rax
	subq	$1, %rdx
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	%r9, %rax
	subq	%rsi, %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rax, %r12
	jnb	.L4959
.L4958:
	movq	%rsi, %rax
	subq	%r8, %rax
	sarq	$3, %rax
	addq	%r12, %rax
	jns	.L4970
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
.L4952:
	movq	%rdx, %rcx
	movq	(%rdi,%rdx,8), %rdx
	salq	$6, %rcx
	subq	%rcx, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L4951
	.p2align 4,,10
	.p2align 3
.L4950:
	movq	%rax, %rdx
	sarq	$6, %rdx
	jmp	.L4952
	.p2align 4,,10
	.p2align 3
.L4955:
	movq	%rax, %rdx
	sarq	$6, %rdx
.L4957:
	movq	%rdx, %rcx
	movq	(%rdi,%rdx,8), %rdx
	salq	$6, %rcx
	subq	%rcx, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L4956
	.p2align 4,,10
	.p2align 3
.L4954:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L4957
	.p2align 4,,10
	.p2align 3
.L4959:
	movq	112(%rbx), %rax
	leaq	104(%rbx), %rsi
	movl	-20(%rbp), %edx
	movq	%rsi, %rcx
	testq	%rax, %rax
	jne	.L4960
	jmp	.L4945
	.p2align 4,,10
	.p2align 3
.L4971:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L4961
.L4960:
	cmpl	%edx, 32(%rax)
	jge	.L4971
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L4960
.L4961:
	cmpq	%rcx, %rsi
	je	.L4945
	cmpl	32(%rcx), %edx
	jl	.L4945
	leaq	-20(%rbp), %rsi
	leaq	96(%rbx), %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE5eraseERS1_
.L4945:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7991:
	.size	_ZN12v8_inspector23V8ConsoleMessageStorage16contextDestroyedEi, .-_ZN12v8_inspector23V8ConsoleMessageStorage16contextDestroyedEi
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_, @function
_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_:
.LFB10851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$184, %edi
	subq	$40, %rsp
	call	_Znwm@PLT
	movl	$18, %ecx
	leaq	8(%rbx), %r8
	movq	%rax, %r12
	movq	(%r14), %rax
	leaq	40(%r12), %r15
	movl	(%rax), %edx
	movq	%r15, %rdi
	xorl	%eax, %eax
	movl	%edx, 32(%r12)
	rep stosq
	leaq	48(%r12), %rax
	movq	%rax, 64(%r12)
	movq	%rax, 72(%r12)
	leaq	96(%r12), %rax
	movq	%rax, 112(%r12)
	movq	%rax, 120(%r12)
	leaq	144(%r12), %rax
	movq	%rax, 160(%r12)
	movq	%rax, 168(%r12)
	cmpq	%r13, %r8
	je	.L5044
	movq	%r13, %r14
	movl	32(%r13), %r13d
	leaq	32(%r12), %rsi
	movl	%edx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	cmpl	%edx, %r13d
	jle	.L4985
	cmpq	%r14, 24(%rbx)
	je	.L5011
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	-64(%rbp), %edx
	movq	-72(%rbp), %r8
	cmpl	%edx, 32(%rax)
	movq	-56(%rbp), %rsi
	jge	.L4989
	cmpq	$0, 24(%rax)
	je	.L4975
.L5011:
	movq	%r14, %rcx
.L4992:
	testq	%rcx, %rcx
	setne	%al
.L5006:
	cmpq	%r14, %r8
	je	.L5013
	testb	%al, %al
	je	.L5045
.L5013:
	movl	$1, %edi
.L4991:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rcx
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4985:
	.cfi_restore_state
	jge	.L4993
	cmpq	%r14, 32(%rbx)
	je	.L5012
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movl	-64(%rbp), %edx
	movq	-72(%rbp), %r8
	cmpl	%edx, 32(%rax)
	movq	-56(%rbp), %rsi
	jle	.L4989
	cmpq	$0, 24(%r14)
	je	.L4990
	movq	%rax, %r14
	movl	$1, %edi
	jmp	.L4991
	.p2align 4,,10
	.p2align 3
.L4998:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L4999
.L4993:
	movq	104(%r12), %r13
	leaq	88(%r12), %rax
	movq	%rax, -56(%rbp)
	testq	%r13, %r13
	je	.L4996
.L4997:
	movq	-56(%rbp), %rdi
	movq	24(%r13), %rsi
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rdx
	movq	16(%r13), %rbx
	cmpq	%rdx, %rdi
	je	.L5002
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L4996
.L5003:
	movq	%rbx, %r13
	jmp	.L4997
	.p2align 4,,10
	.p2align 3
.L5002:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L5003
.L4996:
	movq	56(%r12), %r13
	testq	%r13, %r13
	je	.L5000
.L5001:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE8_M_eraseEPSt13_Rb_tree_nodeIS1_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L5004
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L5000
.L5005:
	movq	%rbx, %r13
	jmp	.L5001
	.p2align 4,,10
	.p2align 3
.L5004:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L5005
.L5000:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5044:
	.cfi_restore_state
	cmpq	$0, 40(%rbx)
	je	.L4974
	movq	32(%rbx), %rax
	cmpl	%edx, 32(%rax)
	jl	.L4975
.L4974:
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	jne	.L4977
	jmp	.L5046
	.p2align 4,,10
	.p2align 3
.L5047:
	movq	16(%rcx), %rax
	movl	$1, %edi
	testq	%rax, %rax
	je	.L4978
.L5048:
	movq	%rax, %rcx
.L4977:
	movl	32(%rcx), %esi
	cmpl	%esi, %edx
	jl	.L5047
	movq	24(%rcx), %rax
	xorl	%edi, %edi
	testq	%rax, %rax
	jne	.L5048
.L4978:
	movq	%rcx, %r14
	testb	%dil, %dil
	jne	.L4976
.L4981:
	cmpl	%esi, %edx
	jg	.L4982
.L5010:
	movq	152(%r12), %r13
	leaq	136(%r12), %rax
	movq	%rcx, %r14
	movq	%rax, -56(%rbp)
	testq	%r13, %r13
	je	.L4993
	.p2align 4,,10
	.p2align 3
.L4994:
	movq	-56(%rbp), %rdi
	movq	24(%r13), %rsi
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rdx
	movq	16(%r13), %rbx
	cmpq	%rdx, %rdi
	je	.L4998
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L4993
.L4999:
	movq	%rbx, %r13
	jmp	.L4994
	.p2align 4,,10
	.p2align 3
.L4975:
	movq	%rax, %r14
	xorl	%eax, %eax
	jmp	.L5006
	.p2align 4,,10
	.p2align 3
.L4989:
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE24_M_get_insert_unique_posERS1_
	movq	-56(%rbp), %r8
	movq	%rax, %rcx
	movq	%rdx, %r14
.L4984:
	testq	%r14, %r14
	jne	.L4992
	jmp	.L5010
	.p2align 4,,10
	.p2align 3
.L5009:
	movq	%rcx, %r14
.L4982:
	xorl	%ecx, %ecx
	jmp	.L4984
	.p2align 4,,10
	.p2align 3
.L5045:
	movl	32(%r12), %edx
	movl	32(%r14), %r13d
.L4990:
	xorl	%edi, %edi
	cmpl	%r13d, %edx
	setl	%dil
	jmp	.L4991
	.p2align 4,,10
	.p2align 3
.L5046:
	movq	%r13, %rcx
.L4976:
	movl	%edx, -56(%rbp)
	cmpq	%rcx, 24(%rbx)
	je	.L5009
	movq	%rcx, %rdi
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %rcx
	movl	-56(%rbp), %edx
	movl	32(%rax), %esi
	movq	-72(%rbp), %r8
	movq	%rcx, %r14
	movq	%rax, %rcx
	jmp	.L4981
	.p2align 4,,10
	.p2align 3
.L5012:
	xorl	%ecx, %ecx
	jmp	.L4992
	.cfi_endproc
.LFE10851:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_, .-_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_
	.section	.text._ZN12v8_inspector23V8ConsoleMessageStorage7timeEndEiRKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorage7timeEndEiRKNS_8String16E
	.type	_ZN12v8_inspector23V8ConsoleMessageStorage7timeEndEiRKNS_8String16E, @function
_ZN12v8_inspector23V8ConsoleMessageStorage7timeEndEiRKNS_8String16E:
.LFB7989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	leaq	104(%rdi), %rdx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	%esi, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	112(%rdi), %rax
	testq	%rax, %rax
	je	.L5073
	movl	%esi, %ecx
	movq	%rdx, %r12
	jmp	.L5051
	.p2align 4,,10
	.p2align 3
.L5090:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5052
.L5051:
	cmpl	%ecx, 32(%rax)
	jge	.L5090
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L5051
.L5052:
	cmpq	%r12, %rdx
	je	.L5050
	cmpl	32(%r12), %ecx
	jl	.L5050
.L5055:
	movq	152(%r12), %rax
	leaq	144(%r12), %r14
	testq	%rax, %rax
	je	.L5074
	movq	8(%r13), %rcx
	movq	0(%r13), %r8
	movl	$2147483648, %r11d
	movq	%r14, %r13
	movabsq	$-2147483649, %r10
	.p2align 4,,10
	.p2align 3
.L5057:
	movq	40(%rax), %rdx
	movq	%rcx, %rdi
	movq	32(%rax), %r9
	cmpq	%rcx, %rdx
	cmovbe	%rdx, %rdi
	testq	%rdi, %rdi
	je	.L5059
	xorl	%esi, %esi
	jmp	.L5062
	.p2align 4,,10
	.p2align 3
.L5091:
	ja	.L5061
	addq	$1, %rsi
	cmpq	%rsi, %rdi
	je	.L5059
.L5062:
	movzwl	(%r8,%rsi,2), %r15d
	cmpw	%r15w, (%r9,%rsi,2)
	jnb	.L5091
.L5060:
	movq	24(%rax), %rax
.L5063:
	testq	%rax, %rax
	jne	.L5057
	cmpq	%r13, %r14
	je	.L5075
	movq	40(%r13), %rdx
	movq	32(%r13), %rdi
	cmpq	%rdx, %rcx
	movq	%rdx, %rsi
	cmovbe	%rcx, %rsi
	testq	%rsi, %rsi
	je	.L5064
	xorl	%eax, %eax
	jmp	.L5066
	.p2align 4,,10
	.p2align 3
.L5092:
	ja	.L5065
	addq	$1, %rax
	cmpq	%rax, %rsi
	je	.L5064
.L5066:
	movzwl	(%rdi,%rax,2), %r10d
	cmpw	%r10w, (%r8,%rax,2)
	jnb	.L5092
	pxor	%xmm2, %xmm2
	movsd	%xmm2, -96(%rbp)
	jmp	.L5049
	.p2align 4,,10
	.p2align 3
.L5073:
	movq	%rdx, %r12
	.p2align 4,,10
	.p2align 3
.L5050:
	leaq	-84(%rbp), %rax
	movq	%r12, %rsi
	leaq	-64(%rbp), %rcx
	leaq	96(%rbx), %rdi
	leaq	-65(%rbp), %r8
	movq	%rax, -64(%rbp)
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	call	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_
	movq	%rax, %r12
	jmp	.L5055
	.p2align 4,,10
	.p2align 3
.L5059:
	subq	%rcx, %rdx
	cmpq	%r11, %rdx
	jge	.L5061
	cmpq	%r10, %rdx
	jle	.L5060
	testl	%edx, %edx
	js	.L5060
.L5061:
	movq	%rax, %r13
	movq	16(%rax), %rax
	jmp	.L5063
	.p2align 4,,10
	.p2align 3
.L5064:
	subq	%rdx, %rcx
	cmpq	$2147483647, %rcx
	jg	.L5065
	cmpq	$-2147483648, %rcx
	jl	.L5077
	pxor	%xmm1, %xmm1
	movsd	%xmm1, -96(%rbp)
	testl	%ecx, %ecx
	js	.L5049
.L5065:
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	176(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5093
.L5070:
	subsd	72(%r13), %xmm0
	movq	%r13, %rdi
	movq	%r14, %rsi
	movsd	%xmm0, -96(%rbp)
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	32(%rax), %rdi
	movq	%rax, %r13
	leaq	48(%rax), %rax
	cmpq	%rax, %rdi
	je	.L5067
	call	_ZdlPv@PLT
.L5067:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	subq	$1, 176(%r12)
.L5049:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5094
	movsd	-96(%rbp), %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5075:
	.cfi_restore_state
	pxor	%xmm3, %xmm3
	movsd	%xmm3, -96(%rbp)
	jmp	.L5049
	.p2align 4,,10
	.p2align 3
.L5093:
	call	*%rax
	jmp	.L5070
	.p2align 4,,10
	.p2align 3
.L5074:
	pxor	%xmm4, %xmm4
	movsd	%xmm4, -96(%rbp)
	jmp	.L5049
.L5077:
	pxor	%xmm5, %xmm5
	movsd	%xmm5, -96(%rbp)
	jmp	.L5049
.L5094:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7989:
	.size	_ZN12v8_inspector23V8ConsoleMessageStorage7timeEndEiRKNS_8String16E, .-_ZN12v8_inspector23V8ConsoleMessageStorage7timeEndEiRKNS_8String16E
	.section	.text._ZN12v8_inspector23V8ConsoleMessageStorage8hasTimerEiRKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorage8hasTimerEiRKNS_8String16E
	.type	_ZN12v8_inspector23V8ConsoleMessageStorage8hasTimerEiRKNS_8String16E, @function
_ZN12v8_inspector23V8ConsoleMessageStorage8hasTimerEiRKNS_8String16E:
.LFB7990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$56, %rsp
	movl	%esi, -68(%rbp)
	movq	112(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	104(%rdi), %rax
	testq	%rdx, %rdx
	je	.L5115
	movl	%esi, %ecx
	movq	%rax, %rsi
	jmp	.L5097
	.p2align 4,,10
	.p2align 3
.L5131:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L5098
.L5097:
	cmpl	%ecx, 32(%rdx)
	jge	.L5131
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L5097
.L5098:
	cmpq	%rsi, %rax
	je	.L5096
	cmpl	32(%rsi), %ecx
	jl	.L5096
.L5101:
	movq	152(%rsi), %rdx
	leaq	144(%rsi), %r10
	testq	%rdx, %rdx
	je	.L5120
	movq	8(%rbx), %rax
	movq	(%rbx), %r9
	movq	%r10, %rdi
	movl	$2147483648, %r12d
	movabsq	$-2147483649, %rbx
	.p2align 4,,10
	.p2align 3
.L5103:
	movq	40(%rdx), %rcx
	movq	%rax, %r8
	movq	32(%rdx), %r11
	cmpq	%rax, %rcx
	cmovbe	%rcx, %r8
	testq	%r8, %r8
	je	.L5105
	xorl	%esi, %esi
	jmp	.L5108
	.p2align 4,,10
	.p2align 3
.L5132:
	ja	.L5107
	addq	$1, %rsi
	cmpq	%rsi, %r8
	je	.L5105
.L5108:
	movzwl	(%r9,%rsi,2), %r14d
	cmpw	%r14w, (%r11,%rsi,2)
	jnb	.L5132
.L5106:
	movq	24(%rdx), %rdx
.L5109:
	testq	%rdx, %rdx
	jne	.L5103
	xorl	%r8d, %r8d
	cmpq	%rdi, %r10
	je	.L5095
	movq	40(%rdi), %rcx
	movq	32(%rdi), %rdi
	cmpq	%rcx, %rax
	movq	%rcx, %rsi
	cmovbe	%rax, %rsi
	testq	%rsi, %rsi
	je	.L5110
	xorl	%edx, %edx
	jmp	.L5111
	.p2align 4,,10
	.p2align 3
.L5133:
	ja	.L5119
	addq	$1, %rdx
	cmpq	%rdx, %rsi
	je	.L5110
.L5111:
	movzwl	(%rdi,%rdx,2), %ebx
	cmpw	%bx, (%r9,%rdx,2)
	jnb	.L5133
.L5120:
	xorl	%r8d, %r8d
.L5095:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5134
	addq	$56, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5115:
	.cfi_restore_state
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L5096:
	leaq	-68(%rbp), %rax
	leaq	-48(%rbp), %rcx
	addq	$96, %rdi
	leaq	-49(%rbp), %r8
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%rax, -48(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_
	movq	%rax, %rsi
	jmp	.L5101
	.p2align 4,,10
	.p2align 3
.L5105:
	subq	%rax, %rcx
	cmpq	%r12, %rcx
	jge	.L5107
	cmpq	%rbx, %rcx
	jle	.L5106
	testl	%ecx, %ecx
	js	.L5106
.L5107:
	movq	%rdx, %rdi
	movq	16(%rdx), %rdx
	jmp	.L5109
	.p2align 4,,10
	.p2align 3
.L5110:
	subq	%rcx, %rax
	movl	$1, %r8d
	cmpq	$2147483647, %rax
	jg	.L5095
	cmpq	$-2147483648, %rax
	jl	.L5120
	notl	%eax
	shrl	$31, %eax
	movl	%eax, %r8d
	jmp	.L5095
	.p2align 4,,10
	.p2align 3
.L5119:
	movl	$1, %r8d
	jmp	.L5095
.L5134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7990:
	.size	_ZN12v8_inspector23V8ConsoleMessageStorage8hasTimerEiRKNS_8String16E, .-_ZN12v8_inspector23V8ConsoleMessageStorage8hasTimerEiRKNS_8String16E
	.section	.text._ZN12v8_inspector23V8ConsoleMessageStorage5countEiRKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorage5countEiRKNS_8String16E
	.type	_ZN12v8_inspector23V8ConsoleMessageStorage5countEiRKNS_8String16E, @function
_ZN12v8_inspector23V8ConsoleMessageStorage5countEiRKNS_8String16E:
.LFB7985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	104(%rdi), %rdx
	subq	$56, %rsp
	movl	%esi, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	112(%rdi), %rax
	testq	%rax, %rax
	je	.L5156
	movl	%esi, %ecx
	movq	%rdx, %rsi
	jmp	.L5137
	.p2align 4,,10
	.p2align 3
.L5168:
	movq	%rax, %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5138
.L5137:
	cmpl	%ecx, 32(%rax)
	jge	.L5168
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L5137
.L5138:
	cmpq	%rsi, %rdx
	je	.L5136
	cmpl	32(%rsi), %ecx
	jl	.L5136
	movq	104(%rsi), %rdx
	leaq	96(%rsi), %r10
	testq	%rdx, %rdx
	je	.L5157
.L5172:
	movq	8(%rbx), %rcx
	movq	(%rbx), %r11
	movq	%r10, %r9
	movl	$2147483648, %r14d
	movabsq	$-2147483649, %r13
	.p2align 4,,10
	.p2align 3
.L5143:
	movq	40(%rdx), %rax
	movq	%rcx, %r8
	movq	32(%rdx), %r12
	cmpq	%rcx, %rax
	cmovbe	%rax, %r8
	testq	%r8, %r8
	je	.L5145
	xorl	%edi, %edi
	jmp	.L5148
	.p2align 4,,10
	.p2align 3
.L5169:
	ja	.L5147
	addq	$1, %rdi
	cmpq	%rdi, %r8
	je	.L5145
.L5148:
	movzwl	(%r11,%rdi,2), %r15d
	cmpw	%r15w, (%r12,%rdi,2)
	jnb	.L5169
.L5146:
	movq	24(%rdx), %rdx
.L5149:
	testq	%rdx, %rdx
	jne	.L5143
	cmpq	%r9, %r10
	je	.L5142
	movq	40(%r9), %rdx
	movq	32(%r9), %r8
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovbe	%rcx, %rdi
	testq	%rdi, %rdi
	je	.L5150
	xorl	%eax, %eax
	jmp	.L5152
	.p2align 4,,10
	.p2align 3
.L5170:
	ja	.L5151
	addq	$1, %rax
	cmpq	%rax, %rdi
	je	.L5150
.L5152:
	movzwl	(%r8,%rax,2), %r14d
	cmpw	%r14w, (%r11,%rax,2)
	jnb	.L5170
.L5142:
	leaq	88(%rsi), %rdi
	leaq	-64(%rbp), %rcx
	movq	%r9, %rsi
	movq	%rbx, -64(%rbp)
	leaq	-65(%rbp), %r8
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS3_EESF_IJEEEEESt17_Rb_tree_iteratorIS4_ESt23_Rb_tree_const_iteratorIS4_EDpOT_
	movq	%rax, %r9
.L5151:
	movl	72(%r9), %eax
	addl	$1, %eax
	movl	%eax, 72(%r9)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L5171
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5156:
	.cfi_restore_state
	movq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L5136:
	leaq	-84(%rbp), %rax
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	addq	$96, %rdi
	leaq	-64(%rbp), %rcx
	leaq	-65(%rbp), %r8
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_
	movq	%rax, %rsi
	movq	104(%rsi), %rdx
	leaq	96(%rsi), %r10
	testq	%rdx, %rdx
	jne	.L5172
.L5157:
	movq	%r10, %r9
	jmp	.L5142
	.p2align 4,,10
	.p2align 3
.L5145:
	subq	%rcx, %rax
	cmpq	%r14, %rax
	jge	.L5147
	cmpq	%r13, %rax
	jle	.L5146
	testl	%eax, %eax
	js	.L5146
.L5147:
	movq	%rdx, %r9
	movq	16(%rdx), %rdx
	jmp	.L5149
	.p2align 4,,10
	.p2align 3
.L5150:
	subq	%rdx, %rcx
	cmpq	$2147483647, %rcx
	jg	.L5151
	cmpq	$-2147483648, %rcx
	jl	.L5142
	testl	%ecx, %ecx
	jns	.L5151
	jmp	.L5142
.L5171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7985:
	.size	_ZN12v8_inspector23V8ConsoleMessageStorage5countEiRKNS_8String16E, .-_ZN12v8_inspector23V8ConsoleMessageStorage5countEiRKNS_8String16E
	.section	.text._ZN12v8_inspector23V8ConsoleMessageStorage10countResetEiRKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorage10countResetEiRKNS_8String16E
	.type	_ZN12v8_inspector23V8ConsoleMessageStorage10countResetEiRKNS_8String16E, @function
_ZN12v8_inspector23V8ConsoleMessageStorage10countResetEiRKNS_8String16E:
.LFB7987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	104(%rdi), %rdx
	subq	$56, %rsp
	movl	%esi, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	112(%rdi), %rax
	testq	%rax, %rax
	je	.L5207
	movl	%esi, %ecx
	movq	%rdx, %rsi
	jmp	.L5175
	.p2align 4,,10
	.p2align 3
.L5232:
	movq	%rax, %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5176
.L5175:
	cmpl	%ecx, 32(%rax)
	jge	.L5232
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L5175
.L5176:
	cmpq	%rdx, %rsi
	je	.L5174
	cmpl	32(%rsi), %ecx
	jl	.L5174
.L5179:
	movq	104(%rsi), %rcx
	leaq	96(%rsi), %r10
	testq	%rcx, %rcx
	je	.L5210
	movq	8(%rbx), %rdx
	movq	(%rbx), %r9
	movq	%r10, %r11
	movq	%rcx, %rax
	movl	$2147483648, %r14d
	.p2align 4,,10
	.p2align 3
.L5181:
	movq	40(%rax), %rdi
	movq	%rdx, %r12
	movq	32(%rax), %r15
	cmpq	%rdx, %rdi
	cmovbe	%rdi, %r12
	testq	%r12, %r12
	je	.L5183
	xorl	%r8d, %r8d
	jmp	.L5186
	.p2align 4,,10
	.p2align 3
.L5233:
	ja	.L5185
	addq	$1, %r8
	cmpq	%r12, %r8
	je	.L5183
.L5186:
	movzwl	(%r9,%r8,2), %r13d
	cmpw	%r13w, (%r15,%r8,2)
	jnb	.L5233
.L5184:
	movq	24(%rax), %rax
.L5187:
	testq	%rax, %rax
	jne	.L5181
	xorl	%eax, %eax
	cmpq	%r11, %r10
	je	.L5173
	movq	40(%r11), %rdi
	movq	32(%r11), %r11
	cmpq	%rdi, %rdx
	movq	%rdi, %r8
	cmovbe	%rdx, %r8
	testq	%r8, %r8
	je	.L5188
	xorl	%eax, %eax
	jmp	.L5190
	.p2align 4,,10
	.p2align 3
.L5234:
	ja	.L5214
	addq	$1, %rax
	cmpq	%r8, %rax
	je	.L5188
.L5190:
	movzwl	(%r11,%rax,2), %r15d
	cmpw	%r15w, (%r9,%rax,2)
	jnb	.L5234
.L5210:
	xorl	%eax, %eax
.L5173:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L5235
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5207:
	.cfi_restore_state
	movq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L5174:
	leaq	-84(%rbp), %rax
	leaq	-64(%rbp), %rcx
	addq	$96, %rdi
	leaq	-65(%rbp), %r8
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_
	movq	%rax, %rsi
	jmp	.L5179
	.p2align 4,,10
	.p2align 3
.L5183:
	subq	%rdx, %rdi
	cmpq	%r14, %rdi
	jge	.L5185
	movabsq	$-2147483649, %r15
	cmpq	%r15, %rdi
	jle	.L5184
	testl	%edi, %edi
	js	.L5184
.L5185:
	movq	%rax, %r11
	movq	16(%rax), %rax
	jmp	.L5187
	.p2align 4,,10
	.p2align 3
.L5188:
	movq	%rdx, %rax
	subq	%rdi, %rax
	movq	%rax, %rdi
	cmpq	$2147483647, %rax
	jg	.L5214
	xorl	%eax, %eax
	cmpq	$-2147483648, %rdi
	jl	.L5173
	testl	%edi, %edi
	js	.L5173
.L5214:
	movq	%r10, %r11
	movl	$2147483648, %r14d
	movabsq	$-2147483649, %r13
	.p2align 4,,10
	.p2align 3
.L5192:
	movq	40(%rcx), %rdi
	movq	32(%rcx), %r12
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovbe	%rdx, %rax
	testq	%rax, %rax
	je	.L5193
	xorl	%r8d, %r8d
	jmp	.L5196
	.p2align 4,,10
	.p2align 3
.L5236:
	ja	.L5195
	addq	$1, %r8
	cmpq	%r8, %rax
	je	.L5193
.L5196:
	movzwl	(%r9,%r8,2), %r15d
	cmpw	%r15w, (%r12,%r8,2)
	jnb	.L5236
.L5194:
	movq	24(%rcx), %rcx
.L5197:
	testq	%rcx, %rcx
	jne	.L5192
	cmpq	%r11, %r10
	je	.L5198
	movq	40(%r11), %rdi
	movq	32(%r11), %r8
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovbe	%rdx, %rax
	testq	%rax, %rax
	je	.L5199
	xorl	%ecx, %ecx
	jmp	.L5201
	.p2align 4,,10
	.p2align 3
.L5237:
	ja	.L5200
	addq	$1, %rcx
	cmpq	%rcx, %rax
	je	.L5199
.L5201:
	movzwl	(%r8,%rcx,2), %r14d
	cmpw	%r14w, (%r9,%rcx,2)
	jnb	.L5237
.L5198:
	leaq	88(%rsi), %rdi
	leaq	-64(%rbp), %rcx
	movq	%r11, %rsi
	movq	%rbx, -64(%rbp)
	leaq	-65(%rbp), %r8
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS3_EESF_IJEEEEESt17_Rb_tree_iteratorIS4_ESt23_Rb_tree_const_iteratorIS4_EDpOT_
	movq	%rax, %r11
.L5200:
	movl	$0, 72(%r11)
	movl	$1, %eax
	jmp	.L5173
	.p2align 4,,10
	.p2align 3
.L5193:
	subq	%rdx, %rdi
	cmpq	%r14, %rdi
	jge	.L5195
	cmpq	%r13, %rdi
	jle	.L5194
	testl	%edi, %edi
	js	.L5194
.L5195:
	movq	%rcx, %r11
	movq	16(%rcx), %rcx
	jmp	.L5197
	.p2align 4,,10
	.p2align 3
.L5199:
	subq	%rdi, %rdx
	cmpq	$2147483647, %rdx
	jg	.L5200
	cmpq	$-2147483648, %rdx
	jl	.L5198
	testl	%edx, %edx
	jns	.L5200
	jmp	.L5198
.L5235:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7987:
	.size	_ZN12v8_inspector23V8ConsoleMessageStorage10countResetEiRKNS_8String16E, .-_ZN12v8_inspector23V8ConsoleMessageStorage10countResetEiRKNS_8String16E
	.section	.text._ZN12v8_inspector23V8ConsoleMessageStorage30shouldReportDeprecationMessageEiRKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorage30shouldReportDeprecationMessageEiRKNS_8String16E
	.type	_ZN12v8_inspector23V8ConsoleMessageStorage30shouldReportDeprecationMessageEiRKNS_8String16E, @function
_ZN12v8_inspector23V8ConsoleMessageStorage30shouldReportDeprecationMessageEiRKNS_8String16E:
.LFB7984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	104(%rdi), %rdx
	subq	$56, %rsp
	movl	%esi, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	112(%rdi), %rax
	testq	%rax, %rax
	je	.L5262
	movl	%esi, %ecx
	movq	%rdx, %r12
	jmp	.L5240
	.p2align 4,,10
	.p2align 3
.L5279:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5241
.L5240:
	cmpl	%ecx, 32(%rax)
	jge	.L5279
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L5240
.L5241:
	cmpq	%r12, %rdx
	je	.L5239
	cmpl	32(%r12), %ecx
	jl	.L5239
.L5244:
	movq	56(%r12), %rax
	leaq	48(%r12), %r13
	testq	%rax, %rax
	je	.L5267
	movq	8(%rbx), %rcx
	movq	(%rbx), %r9
	movq	%r13, %r8
	movl	$2147483648, %r14d
	movabsq	$-2147483649, %r11
	.p2align 4,,10
	.p2align 3
.L5246:
	movq	40(%rax), %rdx
	movq	%rcx, %rdi
	movq	32(%rax), %r10
	cmpq	%rcx, %rdx
	cmovbe	%rdx, %rdi
	testq	%rdi, %rdi
	je	.L5248
	xorl	%esi, %esi
	jmp	.L5251
	.p2align 4,,10
	.p2align 3
.L5280:
	ja	.L5250
	addq	$1, %rsi
	cmpq	%rsi, %rdi
	je	.L5248
.L5251:
	movzwl	(%r9,%rsi,2), %r15d
	cmpw	%r15w, (%r10,%rsi,2)
	jnb	.L5280
.L5249:
	movq	24(%rax), %rax
.L5252:
	testq	%rax, %rax
	jne	.L5246
	cmpq	%r8, %r13
	je	.L5245
	movq	40(%r8), %rdx
	movq	32(%r8), %rdi
	cmpq	%rdx, %rcx
	movq	%rdx, %rsi
	cmovbe	%rcx, %rsi
	testq	%rsi, %rsi
	je	.L5253
	xorl	%eax, %eax
	jmp	.L5255
	.p2align 4,,10
	.p2align 3
.L5281:
	ja	.L5265
	addq	$1, %rax
	cmpq	%rax, %rsi
	je	.L5253
.L5255:
	movzwl	(%rdi,%rax,2), %r11d
	cmpw	%r11w, (%r9,%rax,2)
	jnb	.L5281
.L5267:
	movq	%r13, %r8
.L5245:
	leaq	40(%r12), %rdi
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ES1_St9_IdentityIS1_ESt4lessIS1_ESaIS1_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS1_ERKS1_
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L5260
	testq	%rax, %rax
	jne	.L5268
	cmpq	%r14, %r13
	jne	.L5282
.L5268:
	movl	$1, %r8d
.L5256:
	movl	$72, %edi
	movl	%r8d, -88(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rsi
	movq	%rax, %r15
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	%rax, 32(%r15)
	movq	8(%rbx), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%rbx), %rax
	movl	-88(%rbp), %r8d
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rax, 64(%r15)
	movl	%r8d, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 80(%r12)
.L5260:
	movl	$1, %eax
.L5238:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L5283
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5262:
	.cfi_restore_state
	movq	%rdx, %r12
	.p2align 4,,10
	.p2align 3
.L5239:
	leaq	-84(%rbp), %rax
	movq	%r12, %rsi
	leaq	-64(%rbp), %rcx
	addq	$96, %rdi
	leaq	-65(%rbp), %r8
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_
	movq	%rax, %r12
	jmp	.L5244
	.p2align 4,,10
	.p2align 3
.L5248:
	subq	%rcx, %rdx
	cmpq	%r14, %rdx
	jge	.L5250
	cmpq	%r11, %rdx
	jle	.L5249
	testl	%edx, %edx
	js	.L5249
.L5250:
	movq	%rax, %r8
	movq	16(%rax), %rax
	jmp	.L5252
	.p2align 4,,10
	.p2align 3
.L5253:
	subq	%rdx, %rcx
	xorl	%eax, %eax
	cmpq	$2147483647, %rcx
	jg	.L5238
	cmpq	$-2147483648, %rcx
	jl	.L5267
	testl	%ecx, %ecx
	jns	.L5238
	jmp	.L5267
	.p2align 4,,10
	.p2align 3
.L5265:
	xorl	%eax, %eax
	jmp	.L5238
	.p2align 4,,10
	.p2align 3
.L5282:
	leaq	32(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	shrl	$31, %eax
	movl	%eax, %r8d
	jmp	.L5256
.L5283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7984:
	.size	_ZN12v8_inspector23V8ConsoleMessageStorage30shouldReportDeprecationMessageEiRKNS_8String16E, .-_ZN12v8_inspector23V8ConsoleMessageStorage30shouldReportDeprecationMessageEiRKNS_8String16E
	.section	.text._ZN12v8_inspector23V8ConsoleMessageStorage4timeEiRKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorage4timeEiRKNS_8String16E
	.type	_ZN12v8_inspector23V8ConsoleMessageStorage4timeEiRKNS_8String16E, @function
_ZN12v8_inspector23V8ConsoleMessageStorage4timeEiRKNS_8String16E:
.LFB7986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	leaq	104(%rdi), %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %r14
	movq	(%r14), %rax
	movq	176(%rax), %rbx
	movq	112(%rdi), %rax
	testq	%rax, %rax
	je	.L5310
	movl	%esi, %ecx
	movq	%rdx, %r13
	jmp	.L5286
	.p2align 4,,10
	.p2align 3
.L5329:
	movq	%rax, %r13
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5287
.L5286:
	cmpl	%ecx, 32(%rax)
	jge	.L5329
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L5286
.L5287:
	cmpq	%r13, %rdx
	je	.L5285
	cmpl	32(%r13), %ecx
	jl	.L5285
.L5290:
	movq	152(%r13), %rax
	leaq	144(%r13), %r8
	testq	%rax, %rax
	je	.L5311
	movq	8(%r15), %rcx
	movq	(%r15), %r9
	movq	%r8, %r12
	.p2align 4,,10
	.p2align 3
.L5292:
	movq	40(%rax), %rdx
	movq	%rcx, %rdi
	movq	32(%rax), %r10
	cmpq	%rcx, %rdx
	cmovbe	%rdx, %rdi
	testq	%rdi, %rdi
	je	.L5294
	xorl	%esi, %esi
	jmp	.L5297
	.p2align 4,,10
	.p2align 3
.L5330:
	ja	.L5296
	addq	$1, %rsi
	cmpq	%rsi, %rdi
	je	.L5294
.L5297:
	movzwl	(%r9,%rsi,2), %r11d
	cmpw	%r11w, (%r10,%rsi,2)
	jnb	.L5330
.L5295:
	movq	24(%rax), %rax
.L5298:
	testq	%rax, %rax
	jne	.L5292
	cmpq	%r12, %r8
	je	.L5291
	movq	40(%r12), %rdx
	movq	32(%r12), %rdi
	cmpq	%rdx, %rcx
	movq	%rdx, %rsi
	cmovbe	%rcx, %rsi
	testq	%rsi, %rsi
	je	.L5299
	xorl	%eax, %eax
	jmp	.L5301
	.p2align 4,,10
	.p2align 3
.L5331:
	ja	.L5300
	addq	$1, %rax
	cmpq	%rax, %rsi
	je	.L5299
.L5301:
	movzwl	(%rdi,%rax,2), %r11d
	cmpw	%r11w, (%r9,%rax,2)
	jnb	.L5331
.L5291:
	movl	$80, %edi
	movq	%r8, -120(%rbp)
	movq	%r12, -112(%rbp)
	call	_Znwm@PLT
	movq	(%r15), %rsi
	leaq	48(%rax), %rcx
	leaq	32(%rax), %r9
	movq	%rax, %r12
	movq	%rcx, 32(%rax)
	movq	8(%r15), %rax
	movq	%r9, %rdi
	movq	%rcx, -104(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	movq	%r9, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%r15), %rax
	movq	-96(%rbp), %r9
	movq	$0x000000000, 72(%r12)
	movq	-112(%rbp), %r10
	leaq	136(%r13), %rdi
	movq	%rax, 64(%r12)
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	_ZNSt8_Rb_treeIN12v8_inspector8String16ESt4pairIKS1_dESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERS3_
	movq	-104(%rbp), %rcx
	testq	%rdx, %rdx
	movq	%rax, %r15
	je	.L5302
	testq	%rax, %rax
	movq	-120(%rbp), %r8
	jne	.L5312
	cmpq	%rdx, %r8
	movq	-96(%rbp), %r9
	jne	.L5332
.L5312:
	movl	$1, %edi
.L5303:
	movq	%r8, %rcx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 176(%r13)
.L5300:
	leaq	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv(%rip), %rax
	pxor	%xmm0, %xmm0
	cmpq	%rax, %rbx
	jne	.L5333
.L5306:
	movsd	%xmm0, 72(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5334
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5310:
	.cfi_restore_state
	movq	%rdx, %r13
	.p2align 4,,10
	.p2align 3
.L5285:
	leaq	-84(%rbp), %rax
	movq	%r13, %rsi
	leaq	-64(%rbp), %rcx
	addq	$96, %rdi
	leaq	-65(%rbp), %r8
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_
	movq	%rax, %r13
	jmp	.L5290
	.p2align 4,,10
	.p2align 3
.L5294:
	subq	%rcx, %rdx
	movl	$2147483648, %esi
	cmpq	%rsi, %rdx
	jge	.L5296
	movabsq	$-2147483649, %rsi
	cmpq	%rsi, %rdx
	jle	.L5295
	testl	%edx, %edx
	js	.L5295
.L5296:
	movq	%rax, %r12
	movq	16(%rax), %rax
	jmp	.L5298
	.p2align 4,,10
	.p2align 3
.L5299:
	subq	%rdx, %rcx
	cmpq	$2147483647, %rcx
	jg	.L5300
	cmpq	$-2147483648, %rcx
	jl	.L5291
	testl	%ecx, %ecx
	js	.L5291
	leaq	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv(%rip), %rax
	pxor	%xmm0, %xmm0
	cmpq	%rax, %rbx
	je	.L5306
.L5333:
	movq	%r14, %rdi
	call	*%rbx
	jmp	.L5306
	.p2align 4,,10
	.p2align 3
.L5302:
	movq	32(%r12), %rdi
	cmpq	%rdi, %rcx
	je	.L5305
	call	_ZdlPv@PLT
.L5305:
	movq	%r12, %rdi
	movq	%r15, %r12
	call	_ZdlPv@PLT
	jmp	.L5300
	.p2align 4,,10
	.p2align 3
.L5332:
	leaq	32(%rdx), %rsi
	movq	%r9, %rdi
	movq	%r8, -104(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %r8
	shrl	$31, %eax
	movl	%eax, %edi
	jmp	.L5303
	.p2align 4,,10
	.p2align 3
.L5311:
	movq	%r8, %r12
	jmp	.L5291
.L5334:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7986:
	.size	_ZN12v8_inspector23V8ConsoleMessageStorage4timeEiRKNS_8String16E, .-_ZN12v8_inspector23V8ConsoleMessageStorage4timeEiRKNS_8String16E
	.section	.text._ZN12v8_inspector23V8ConsoleMessageStorage7timeLogEiRKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8ConsoleMessageStorage7timeLogEiRKNS_8String16E
	.type	_ZN12v8_inspector23V8ConsoleMessageStorage7timeLogEiRKNS_8String16E, @function
_ZN12v8_inspector23V8ConsoleMessageStorage7timeLogEiRKNS_8String16E:
.LFB7988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movl	%esi, -68(%rbp)
	movq	112(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	104(%rdi), %rax
	testq	%rdx, %rdx
	je	.L5358
	movl	%esi, %ecx
	movq	%rax, %rsi
	jmp	.L5337
	.p2align 4,,10
	.p2align 3
.L5375:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L5338
.L5337:
	cmpl	%ecx, 32(%rdx)
	jge	.L5375
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L5337
.L5338:
	cmpq	%rsi, %rax
	je	.L5336
	cmpl	32(%rsi), %ecx
	jl	.L5336
.L5341:
	movq	152(%rsi), %rax
	leaq	144(%rsi), %r9
	testq	%rax, %rax
	je	.L5361
	movq	8(%r12), %rcx
	movq	(%r12), %r8
	movl	$2147483648, %r13d
	movq	%r9, %r12
	movabsq	$-2147483649, %r11
	.p2align 4,,10
	.p2align 3
.L5343:
	movq	40(%rax), %rdx
	movq	%rcx, %rdi
	movq	32(%rax), %r10
	cmpq	%rcx, %rdx
	cmovbe	%rdx, %rdi
	testq	%rdi, %rdi
	je	.L5345
	xorl	%esi, %esi
	jmp	.L5348
	.p2align 4,,10
	.p2align 3
.L5376:
	ja	.L5347
	addq	$1, %rsi
	cmpq	%rsi, %rdi
	je	.L5345
.L5348:
	movzwl	(%r8,%rsi,2), %r14d
	cmpw	%r14w, (%r10,%rsi,2)
	jnb	.L5376
.L5346:
	movq	24(%rax), %rax
.L5349:
	testq	%rax, %rax
	jne	.L5343
	cmpq	%r12, %r9
	je	.L5361
	movq	40(%r12), %rdx
	movq	32(%r12), %rdi
	cmpq	%rdx, %rcx
	movq	%rdx, %rsi
	cmovbe	%rcx, %rsi
	testq	%rsi, %rsi
	je	.L5350
	xorl	%eax, %eax
	jmp	.L5352
	.p2align 4,,10
	.p2align 3
.L5377:
	ja	.L5351
	addq	$1, %rax
	cmpq	%rax, %rsi
	je	.L5350
.L5352:
	movzwl	(%rdi,%rax,2), %r11d
	cmpw	%r11w, (%r8,%rax,2)
	jnb	.L5377
.L5361:
	pxor	%xmm0, %xmm0
.L5335:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5378
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5358:
	.cfi_restore_state
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L5336:
	leaq	-68(%rbp), %rax
	leaq	-48(%rbp), %rcx
	leaq	96(%rbx), %rdi
	leaq	-49(%rbp), %r8
	movq	%rax, -48(%rbp)
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	call	_ZNSt8_Rb_treeIiSt4pairIKiN12v8_inspector23V8ConsoleMessageStorage14PerContextDataEESt10_Select1stIS5_ESt4lessIiESaIS5_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESG_IJEEEEESt17_Rb_tree_iteratorIS5_ESt23_Rb_tree_const_iteratorIS5_EDpOT_
	movq	%rax, %rsi
	jmp	.L5341
	.p2align 4,,10
	.p2align 3
.L5345:
	subq	%rcx, %rdx
	cmpq	%r13, %rdx
	jge	.L5347
	cmpq	%r11, %rdx
	jle	.L5346
	testl	%edx, %edx
	js	.L5346
.L5347:
	movq	%rax, %r12
	movq	16(%rax), %rax
	jmp	.L5349
	.p2align 4,,10
	.p2align 3
.L5350:
	subq	%rdx, %rcx
	cmpq	$2147483647, %rcx
	jg	.L5351
	pxor	%xmm0, %xmm0
	cmpq	$-2147483648, %rcx
	jl	.L5335
	testl	%ecx, %ecx
	js	.L5335
.L5351:
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	176(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5379
.L5355:
	subsd	72(%r12), %xmm0
	jmp	.L5335
	.p2align 4,,10
	.p2align 3
.L5379:
	call	*%rax
	jmp	.L5355
.L5378:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7988:
	.size	_ZN12v8_inspector23V8ConsoleMessageStorage7timeLogEiRKNS_8String16E, .-_ZN12v8_inspector23V8ConsoleMessageStorage7timeLogEiRKNS_8String16E
	.weak	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE, 48
_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime9CallFrameE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime9CallFrameE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, 48
_ZTVN12v8_inspector8protocol7Runtime9CallFrameE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Console14ConsoleMessageE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE, @object
	.size	_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE, 48
_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Console14ConsoleMessageD1Ev
	.quad	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev
	.section	.bss._ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic523,"aw",@nobits
	.align 8
	.type	_ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic523, @object
	.size	_ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic523, 8
_ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic523:
	.zero	8
	.section	.bss._ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic520,"aw",@nobits
	.align 8
	.type	_ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic520, @object
	.size	_ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic520, 8
_ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic520:
	.zero	8
	.section	.bss._ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic517,"aw",@nobits
	.align 8
	.type	_ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic517, @object
	.size	_ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic517, 8
_ZZN12v8_inspector12_GLOBAL__N_126TraceV8ConsoleMessageEventENS_15V8MessageOriginENS_14ConsoleAPITypeEE28trace_event_unique_atomic517:
	.zero	8
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_1L32kGlobalConsoleMessageHandleLabelE,"a"
	.align 16
	.type	_ZN12v8_inspector12_GLOBAL__N_1L32kGlobalConsoleMessageHandleLabelE, @object
	.size	_ZN12v8_inspector12_GLOBAL__N_1L32kGlobalConsoleMessageHandleLabelE, 17
_ZN12v8_inspector12_GLOBAL__N_1L32kGlobalConsoleMessageHandleLabelE:
	.string	"DevTools console"
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata._ZStL19piecewise_construct,"a"
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
