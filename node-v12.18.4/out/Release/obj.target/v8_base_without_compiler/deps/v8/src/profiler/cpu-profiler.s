	.file	"cpu-profiler.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4973:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4973:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4974:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4974:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4976:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4976:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal23ProfilerEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE,"axG",@progbits,_ZN2v88internal23ProfilerEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23ProfilerEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE
	.type	_ZN2v88internal23ProfilerEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE, @function
_ZN2v88internal23ProfilerEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE:
.LFB8840:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8840:
	.size	_ZN2v88internal23ProfilerEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE, .-_ZN2v88internal23ProfilerEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE
	.section	.text._ZN2v88internal10CpuSamplerD2Ev,"axG",@progbits,_ZN2v88internal10CpuSamplerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10CpuSamplerD2Ev
	.type	_ZN2v88internal10CpuSamplerD2Ev, @function
_ZN2v88internal10CpuSamplerD2Ev:
.LFB24326:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal10CpuSamplerE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v87sampler7SamplerD2Ev@PLT
	.cfi_endproc
.LFE24326:
	.size	_ZN2v88internal10CpuSamplerD2Ev, .-_ZN2v88internal10CpuSamplerD2Ev
	.weak	_ZN2v88internal10CpuSamplerD1Ev
	.set	_ZN2v88internal10CpuSamplerD1Ev,_ZN2v88internal10CpuSamplerD2Ev
	.section	.text._ZN2v88internal10CpuSamplerD0Ev,"axG",@progbits,_ZN2v88internal10CpuSamplerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10CpuSamplerD0Ev
	.type	_ZN2v88internal10CpuSamplerD0Ev, @function
_ZN2v88internal10CpuSamplerD0Ev:
.LFB24328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10CpuSamplerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v87sampler7SamplerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24328:
	.size	_ZN2v88internal10CpuSamplerD0Ev, .-_ZN2v88internal10CpuSamplerD0Ev
	.section	.text._ZN2v88internal20ProfilerCodeObserverD2Ev,"axG",@progbits,_ZN2v88internal20ProfilerCodeObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20ProfilerCodeObserverD2Ev
	.type	_ZN2v88internal20ProfilerCodeObserverD2Ev, @function
_ZN2v88internal20ProfilerCodeObserverD2Ev:
.LFB24322:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal20ProfilerCodeObserverE(%rip), %rax
	addq	$16, %rdi
	movq	%rax, -16(%rdi)
	jmp	_ZN2v88internal7CodeMapD1Ev@PLT
	.cfi_endproc
.LFE24322:
	.size	_ZN2v88internal20ProfilerCodeObserverD2Ev, .-_ZN2v88internal20ProfilerCodeObserverD2Ev
	.weak	_ZN2v88internal20ProfilerCodeObserverD1Ev
	.set	_ZN2v88internal20ProfilerCodeObserverD1Ev,_ZN2v88internal20ProfilerCodeObserverD2Ev
	.section	.text._ZN2v88internal20ProfilerCodeObserverD0Ev,"axG",@progbits,_ZN2v88internal20ProfilerCodeObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20ProfilerCodeObserverD0Ev
	.type	_ZN2v88internal20ProfilerCodeObserverD0Ev, @function
_ZN2v88internal20ProfilerCodeObserverD0Ev:
.LFB24324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal20ProfilerCodeObserverE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN2v88internal7CodeMapD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$160, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24324:
	.size	_ZN2v88internal20ProfilerCodeObserverD0Ev, .-_ZN2v88internal20ProfilerCodeObserverD0Ev
	.section	.text._ZN2v88internal10CpuSampler11SampleStackERKNS_13RegisterStateE,"axG",@progbits,_ZN2v88internal10CpuSampler11SampleStackERKNS_13RegisterStateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10CpuSampler11SampleStackERKNS_13RegisterStateE
	.type	_ZN2v88internal10CpuSampler11SampleStackERKNS_13RegisterStateE, @function
_ZN2v88internal10CpuSampler11SampleStackERKNS_13RegisterStateE:
.LFB19053:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	mfence
	movq	524544(%rax), %rcx
	movl	4144(%rcx), %ecx
	testl	%ecx, %ecx
	jne	.L23
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	524544(%rax), %rbx
	testq	%rbx, %rbx
	je	.L12
	pxor	%xmm0, %xmm0
	subq	$8, %rsp
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movl	$1, %r9d
	movl	$1, %r8d
	movl	360(%rax), %eax
	movl	%eax, (%rbx)
	movl	$5, 8(%rbx)
	movzwl	4120(%rbx), %eax
	movups	%xmm0, 16(%rbx)
	movq	$0, 4112(%rbx)
	andw	$-1024, %ax
	movups	%xmm0, 4128(%rbx)
	orb	$2, %ah
	movw	%ax, 4120(%rbx)
	movq	48(%rdi), %rax
	movq	24(%rdi), %rsi
	leaq	8(%rbx), %rdi
	pushq	524680(%rax)
	call	_ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE@PLT
	cmpb	$0, 8(%r12)
	popq	%rax
	popq	%rdx
	jne	.L26
.L15:
	movq	48(%r12), %rdx
	movq	524544(%rdx), %rax
	leaq	524544(%rdx), %rsi
	leaq	384(%rdx), %rcx
	movl	$1, 4144(%rax)
	movq	524544(%rdx), %rax
	addq	$4160, %rax
	cmpq	%rsi, %rax
	cmove	%rcx, %rax
	movq	%rax, 524544(%rdx)
.L12:
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	cmpq	$0, 4128(%rbx)
	je	.L15
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jne	.L17
	addl	$1, 12(%r12)
	movl	8(%rbx), %eax
.L17:
	cmpl	$6, %eax
	jne	.L15
	addl	$1, 16(%r12)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE19053:
	.size	_ZN2v88internal10CpuSampler11SampleStackERKNS_13RegisterStateE, .-_ZN2v88internal10CpuSampler11SampleStackERKNS_13RegisterStateE
	.section	.text._ZN2v88internal23SamplingEventsProcessor16ProcessOneSampleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23SamplingEventsProcessor16ProcessOneSampleEv
	.type	_ZN2v88internal23SamplingEventsProcessor16ProcessOneSampleEv, @function
_ZN2v88internal23SamplingEventsProcessor16ProcessOneSampleEv:
.LFB19088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	264(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	344(%rbx), %rax
	movq	4144(%rax), %rax
	testq	%rax, %rax
	je	.L44
	movq	%r12, %rdi
	movl	(%rax), %r13d
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpl	%r13d, 364(%rbx)
	je	.L45
.L29:
	mfence
	movq	524608(%rbx), %rax
	movl	4144(%rax), %eax
	cmpl	$1, %eax
	je	.L46
.L33:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	344(%rbx), %rax
	movq	%r12, %rdi
	movq	4144(%rax), %rbx
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$2, %eax
	testq	%rbx, %rbx
	je	.L27
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$1, %eax
.L27:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L47
	addq	$4168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	524608(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L33
	movl	364(%rbx), %eax
	cmpl	%eax, (%rsi)
	jne	.L35
	movq	56(%rbx), %rdi
	addq	$8, %rsi
	call	_ZN2v88internal16ProfileGenerator16RecordTickSampleERKNS0_10TickSampleE@PLT
	movq	524608(%rbx), %rax
	leaq	524544(%rbx), %rcx
	leaq	384(%rbx), %rdx
	movl	$0, 4144(%rax)
	movq	524608(%rbx), %rax
	addq	$4160, %rax
	cmpq	%rcx, %rax
	cmove	%rdx, %rax
	movq	%rax, 524608(%rbx)
	xorl	%eax, %eax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L45:
	movzwl	-72(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movl	$5, -4184(%rbp)
	movq	$0, -80(%rbp)
	andw	$-1024, %ax
	movaps	%xmm0, -4176(%rbp)
	orb	$2, %ah
	movaps	%xmm0, -64(%rbp)
	movw	%ax, -72(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	344(%rbx), %r13
	movq	4144(%r13), %rax
	testq	%rax, %rax
	je	.L48
	leaq	-4192(%rbp), %rdi
	movl	$518, %ecx
	movq	%rax, %rsi
	rep movsq
	movq	%rax, 344(%rbx)
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r13, %r13
	je	.L31
	movq	%r13, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L31:
	movq	56(%rbx), %rdi
	leaq	-4184(%rbp), %rsi
	call	_ZN2v88internal16ProfileGenerator16RecordTickSampleERKNS0_10TickSampleE@PLT
	xorl	%eax, %eax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L31
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19088:
	.size	_ZN2v88internal23SamplingEventsProcessor16ProcessOneSampleEv, .-_ZN2v88internal23SamplingEventsProcessor16ProcessOneSampleEv
	.section	.text._ZN2v88internal23SamplingEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23SamplingEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE
	.type	_ZN2v88internal23SamplingEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE, @function
_ZN2v88internal23SamplingEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE:
.LFB19093:
	.cfi_startproc
	endbr64
	cmpq	%rsi, 524680(%rdi)
	je	.L65
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	72(%rdi), %rdx
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	xchgl	(%rdx), %eax
	testl	%eax, %eax
	jne	.L68
.L52:
	movq	%r12, 524680(%rbx)
	movl	$32, %edi
	movl	$1, 72(%rbx)
	call	_Znwm@PLT
	xorl	%esi, %esi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v84base9SemaphoreC1Ei@PLT
	movq	%r12, 40(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v84base6Thread5StartEv@PLT
	testb	%al, %al
	jne	.L69
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movq	40(%rbx), %rdi
	call	_ZN2v84base9Semaphore4WaitEv@PLT
	movq	40(%rbx), %r12
	testq	%r12, %r12
	je	.L54
	movq	%r12, %rdi
	call	_ZN2v84base9SemaphoreD1Ev@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L54:
	movq	$0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	leaq	128(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	80(%rbx), %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v84base6Thread4JoinEv@PLT
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE19093:
	.size	_ZN2v88internal23SamplingEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE, .-_ZN2v88internal23SamplingEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE
	.section	.text._ZN2v88internal11CpuProfiler13EnableLoggingEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11CpuProfiler13EnableLoggingEv.part.0, @function
_ZN2v88internal11CpuProfiler13EnableLoggingEv.part.0:
.LFB24481:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	56(%rdi), %r13
	testq	%r13, %r13
	je	.L79
.L71:
	movl	$16, %edi
	call	_Znwm@PLT
	movq	(%r12), %rsi
	movq	%r13, 8(%rax)
	movq	%rax, %rbx
	addq	$1, 41816(%rsi)
	movq	45752(%rsi), %rdi
	movq	%rsi, (%rax)
	movb	$1, 41812(%rsi)
	call	_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE@PLT
	movq	(%rbx), %rax
	movq	8(%rbx), %rsi
	movq	41016(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE@PLT
	cmpb	$0, _ZN2v88internal22FLAG_prof_browser_modeE(%rip)
	je	.L80
.L72:
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger20LogCompiledFunctionsEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger20LogAccessorCallbacksEv@PLT
	movq	64(%r12), %r13
	movq	%rbx, 64(%r12)
	testq	%r13, %r13
	je	.L70
	movq	0(%r13), %rax
	movq	8(%r13), %rsi
	movq	41016(%rax), %rdi
	call	_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE@PLT
	movq	0(%r13), %rdx
	subq	$1, 41816(%rdx)
	jne	.L74
	movq	0(%r13), %rax
	movb	$0, 41812(%rax)
.L74:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$16, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger14LogCodeObjectsEv@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$56, %edi
	call	_Znwm@PLT
	movl	8(%r12), %ecx
	movq	(%r12), %rsi
	leaq	72(%r12), %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal16ProfilerListenerC1EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE@PLT
	movq	56(%r12), %rdi
	movq	%r13, 56(%r12)
	testq	%rdi, %rdi
	je	.L71
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	56(%r12), %r13
	jmp	.L71
	.cfi_endproc
.LFE24481:
	.size	_ZN2v88internal11CpuProfiler13EnableLoggingEv.part.0, .-_ZN2v88internal11CpuProfiler13EnableLoggingEv.part.0
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB18558:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L87
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L90
.L81:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L81
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE18558:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal14ProfilingScopeC2EPNS0_7IsolateEPNS0_16ProfilerListenerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14ProfilingScopeC2EPNS0_7IsolateEPNS0_16ProfilerListenerE
	.type	_ZN2v88internal14ProfilingScopeC2EPNS0_7IsolateEPNS0_16ProfilerListenerE, @function
_ZN2v88internal14ProfilingScopeC2EPNS0_7IsolateEPNS0_16ProfilerListenerE:
.LFB19055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$1, 41816(%rsi)
	movb	$1, 41812(%rsi)
	movups	%xmm0, (%rdi)
	movq	45752(%rsi), %rdi
	call	_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE@PLT
	movq	(%rbx), %rax
	movq	8(%rbx), %rsi
	movq	41016(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE@PLT
	cmpb	$0, _ZN2v88internal22FLAG_prof_browser_modeE(%rip)
	je	.L94
.L92:
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger20LogCompiledFunctionsEv@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Logger20LogAccessorCallbacksEv@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger14LogCodeObjectsEv@PLT
	jmp	.L92
	.cfi_endproc
.LFE19055:
	.size	_ZN2v88internal14ProfilingScopeC2EPNS0_7IsolateEPNS0_16ProfilerListenerE, .-_ZN2v88internal14ProfilingScopeC2EPNS0_7IsolateEPNS0_16ProfilerListenerE
	.globl	_ZN2v88internal14ProfilingScopeC1EPNS0_7IsolateEPNS0_16ProfilerListenerE
	.set	_ZN2v88internal14ProfilingScopeC1EPNS0_7IsolateEPNS0_16ProfilerListenerE,_ZN2v88internal14ProfilingScopeC2EPNS0_7IsolateEPNS0_16ProfilerListenerE
	.section	.text._ZN2v88internal14ProfilingScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14ProfilingScopeD2Ev
	.type	_ZN2v88internal14ProfilingScopeD2Ev, @function
_ZN2v88internal14ProfilingScopeD2Ev:
.LFB19058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rdi), %rsi
	movq	41016(%rax), %rdi
	call	_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE@PLT
	movq	(%rbx), %rdx
	subq	$1, 41816(%rdx)
	jne	.L95
	movq	(%rbx), %rax
	movb	$0, 41812(%rax)
.L95:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19058:
	.size	_ZN2v88internal14ProfilingScopeD2Ev, .-_ZN2v88internal14ProfilingScopeD2Ev
	.globl	_ZN2v88internal14ProfilingScopeD1Ev
	.set	_ZN2v88internal14ProfilingScopeD1Ev,_ZN2v88internal14ProfilingScopeD2Ev
	.section	.rodata._ZN2v88internal23ProfilerEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"v8:ProfEvntProc"
	.section	.text._ZN2v88internal23ProfilerEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ProfilerEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverE
	.type	_ZN2v88internal23ProfilerEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverE, @function
_ZN2v88internal23ProfilerEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverE:
.LFB19068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	leaq	-64(%rbp), %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rax
	movl	$65536, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v84base6ThreadC2ERKNS1_7OptionsE@PLT
	leaq	16+_ZTVN2v88internal23ProfilerEventsProcessorE(%rip), %rax
	movq	%r14, 56(%rbx)
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	addq	$64, %rax
	movq	%rax, 48(%rbx)
	movq	%r13, 64(%rbx)
	movl	$1, 72(%rbx)
	call	_ZN2v84base17ConditionVariableC1Ev@PLT
	leaq	128(%rbx), %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	168(%rbx), %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	208(%rbx), %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	movl	$72, %edi
	call	_ZN2v88internal8MallocednwEm@PLT
	leaq	264(%rbx), %rdi
	movl	$0, (%rax)
	movq	%rax, %xmm0
	movq	$0, 64(%rax)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 248(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	304(%rbx), %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	movl	$4152, %edi
	call	_ZN2v88internal8MallocednwEm@PLT
	pxor	%xmm0, %xmm0
	movzwl	4120(%rax), %edx
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 4128(%rax)
	movq	%rax, %xmm0
	andw	$-1024, %dx
	punpcklqdq	%xmm0, %xmm0
	movl	$5, 8(%rax)
	movq	$0, 4112(%rax)
	orb	$2, %dh
	movw	%dx, 4120(%rax)
	movq	$0, 4144(%rax)
	movq	64(%rbx), %rax
	movq	$0, 360(%rbx)
	movq	%r12, 368(%rbx)
	movups	%xmm0, 344(%rbx)
	movq	%rbx, 152(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L101:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19068:
	.size	_ZN2v88internal23ProfilerEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverE, .-_ZN2v88internal23ProfilerEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverE
	.globl	_ZN2v88internal23ProfilerEventsProcessorC1EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverE
	.set	_ZN2v88internal23ProfilerEventsProcessorC1EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverE,_ZN2v88internal23ProfilerEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverE
	.section	.text._ZN2v88internal23SamplingEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverENS_4base9TimeDeltaEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23SamplingEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverENS_4base9TimeDeltaEb
	.type	_ZN2v88internal23SamplingEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverENS_4base9TimeDeltaEb, @function
_ZN2v88internal23SamplingEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverENS_4base9TimeDeltaEb:
.LFB19071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal23ProfilerEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverE
	leaq	16+_ZTVN2v88internal23SamplingEventsProcessorE(%rip), %rax
	leaq	384(%r13), %rsi
	pxor	%xmm0, %xmm0
	movq	%rax, 0(%r13)
	addq	$64, %rax
	leaq	524544(%r13), %rcx
	movq	%rax, 48(%r13)
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L103:
	movzwl	4120(%rax), %edx
	movups	%xmm0, 16(%rax)
	addq	$4160, %rax
	movl	$5, -4152(%rax)
	andw	$-1024, %dx
	movq	$0, -48(%rax)
	orb	$2, %dh
	movups	%xmm0, -32(%rax)
	movw	%dx, -40(%rax)
	movl	$0, -16(%rax)
	cmpq	%rcx, %rax
	jne	.L103
	movq	%rsi, 524544(%r13)
	movl	$56, %edi
	movq	%rsi, 524608(%r13)
	call	_Znwm@PLT
	movq	%r14, %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87sampler7SamplerC2EPNS_7IsolateE@PLT
	movq	%r15, 524672(%r13)
	movq	%r15, %rdi
	leaq	16+_ZTVN2v88internal10CpuSamplerE(%rip), %rax
	movq	%r12, 524680(%r13)
	movb	%bl, 524688(%r13)
	movq	%r13, 48(%r15)
	movq	%rax, (%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87sampler7Sampler5StartEv@PLT
	.cfi_endproc
.LFE19071:
	.size	_ZN2v88internal23SamplingEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverENS_4base9TimeDeltaEb, .-_ZN2v88internal23SamplingEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverENS_4base9TimeDeltaEb
	.globl	_ZN2v88internal23SamplingEventsProcessorC1EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverENS_4base9TimeDeltaEb
	.set	_ZN2v88internal23SamplingEventsProcessorC1EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverENS_4base9TimeDeltaEb,_ZN2v88internal23SamplingEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverENS_4base9TimeDeltaEb
	.section	.text._ZN2v88internal23ProfilerEventsProcessorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ProfilerEventsProcessorD2Ev
	.type	_ZN2v88internal23ProfilerEventsProcessorD2Ev, @function
_ZN2v88internal23ProfilerEventsProcessorD2Ev:
.LFB19078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23ProfilerEventsProcessorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	264(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	addq	$64, %rax
	movq	%rax, 48(%rdi)
	movq	64(%rdi), %rax
	movq	$0, 152(%rax)
	movq	344(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L107
	.p2align 4,,10
	.p2align 3
.L108:
	movq	4144(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8MalloceddlEPv@PLT
	testq	%rbx, %rbx
	jne	.L108
.L107:
	leaq	304(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	%r13, %rdi
	leaq	168(%r12), %r13
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	248(%r12), %rbx
	testq	%rbx, %rbx
	je	.L109
	.p2align 4,,10
	.p2align 3
.L110:
	movq	64(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8MalloceddlEPv@PLT
	testq	%rbx, %rbx
	jne	.L110
.L109:
	leaq	208(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	leaq	128(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	leaq	80(%r12), %rdi
	call	_ZN2v84base17ConditionVariableD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base6ThreadD2Ev@PLT
	.cfi_endproc
.LFE19078:
	.size	_ZN2v88internal23ProfilerEventsProcessorD2Ev, .-_ZN2v88internal23ProfilerEventsProcessorD2Ev
	.set	.LTHUNK4,_ZN2v88internal23ProfilerEventsProcessorD2Ev
	.section	.text._ZThn48_N2v88internal23ProfilerEventsProcessorD1Ev,"ax",@progbits
	.p2align 4
	.globl	_ZThn48_N2v88internal23ProfilerEventsProcessorD1Ev
	.type	_ZThn48_N2v88internal23ProfilerEventsProcessorD1Ev, @function
_ZThn48_N2v88internal23ProfilerEventsProcessorD1Ev:
.LFB24570:
	.cfi_startproc
	endbr64
	subq	$48, %rdi
	jmp	.LTHUNK4
	.cfi_endproc
.LFE24570:
	.size	_ZThn48_N2v88internal23ProfilerEventsProcessorD1Ev, .-_ZThn48_N2v88internal23ProfilerEventsProcessorD1Ev
	.globl	_ZN2v88internal23ProfilerEventsProcessorD1Ev
	.set	_ZN2v88internal23ProfilerEventsProcessorD1Ev,_ZN2v88internal23ProfilerEventsProcessorD2Ev
	.section	.text._ZN2v88internal23SamplingEventsProcessorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23SamplingEventsProcessorD2Ev
	.type	_ZN2v88internal23SamplingEventsProcessorD2Ev, @function
_ZN2v88internal23SamplingEventsProcessorD2Ev:
.LFB19074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23SamplingEventsProcessorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$64, %rax
	movq	%rax, 48(%rdi)
	movq	524672(%rdi), %rdi
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	movq	524672(%r12), %rdi
	testq	%rdi, %rdi
	je	.L121
	movq	(%rdi), %rax
	call	*8(%rax)
.L121:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23ProfilerEventsProcessorD2Ev
	.cfi_endproc
.LFE19074:
	.size	_ZN2v88internal23SamplingEventsProcessorD2Ev, .-_ZN2v88internal23SamplingEventsProcessorD2Ev
	.globl	_ZN2v88internal23SamplingEventsProcessorD1Ev
	.set	_ZN2v88internal23SamplingEventsProcessorD1Ev,_ZN2v88internal23SamplingEventsProcessorD2Ev
	.section	.text._ZThn48_N2v88internal23SamplingEventsProcessorD0Ev,"ax",@progbits
	.p2align 4
	.globl	_ZThn48_N2v88internal23SamplingEventsProcessorD0Ev
	.type	_ZThn48_N2v88internal23SamplingEventsProcessorD0Ev, @function
_ZThn48_N2v88internal23SamplingEventsProcessorD0Ev:
.LFB24560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23SamplingEventsProcessorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, -48(%rdi)
	addq	$64, %rax
	movq	%rax, (%rdi)
	movq	524624(%rdi), %rdi
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	movq	524624(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L127
	movq	(%rdi), %rax
	call	*8(%rax)
.L127:
	movq	%r12, %rdi
	call	_ZN2v88internal23ProfilerEventsProcessorD2Ev
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11AlignedFreeEPv@PLT
	.cfi_endproc
.LFE24560:
	.size	_ZThn48_N2v88internal23SamplingEventsProcessorD0Ev, .-_ZThn48_N2v88internal23SamplingEventsProcessorD0Ev
	.section	.text._ZN2v88internal23SamplingEventsProcessorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23SamplingEventsProcessorD0Ev
	.type	_ZN2v88internal23SamplingEventsProcessorD0Ev, @function
_ZN2v88internal23SamplingEventsProcessorD0Ev:
.LFB19076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23SamplingEventsProcessorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$64, %rax
	movq	%rax, 48(%rdi)
	movq	524672(%rdi), %rdi
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	movq	524672(%r12), %rdi
	testq	%rdi, %rdi
	je	.L133
	movq	(%rdi), %rax
	call	*8(%rax)
.L133:
	movq	%r12, %rdi
	call	_ZN2v88internal23ProfilerEventsProcessorD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11AlignedFreeEPv@PLT
	.cfi_endproc
.LFE19076:
	.size	_ZN2v88internal23SamplingEventsProcessorD0Ev, .-_ZN2v88internal23SamplingEventsProcessorD0Ev
	.section	.text._ZThn48_N2v88internal23SamplingEventsProcessorD1Ev,"ax",@progbits
	.p2align 4
	.globl	_ZThn48_N2v88internal23SamplingEventsProcessorD1Ev
	.type	_ZThn48_N2v88internal23SamplingEventsProcessorD1Ev, @function
_ZThn48_N2v88internal23SamplingEventsProcessorD1Ev:
.LFB24561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23SamplingEventsProcessorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -48(%rdi)
	addq	$64, %rax
	movq	%rax, (%rdi)
	movq	524624(%rdi), %rdi
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	movq	524624(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L139
	movq	(%rdi), %rax
	call	*8(%rax)
.L139:
	addq	$8, %rsp
	leaq	-48(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23ProfilerEventsProcessorD2Ev
	.cfi_endproc
.LFE24561:
	.size	_ZThn48_N2v88internal23SamplingEventsProcessorD1Ev, .-_ZThn48_N2v88internal23SamplingEventsProcessorD1Ev
	.section	.text._ZN2v88internal23ProfilerEventsProcessorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ProfilerEventsProcessorD0Ev
	.type	_ZN2v88internal23ProfilerEventsProcessorD0Ev, @function
_ZN2v88internal23ProfilerEventsProcessorD0Ev:
.LFB19080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal23ProfilerEventsProcessorD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$376, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19080:
	.size	_ZN2v88internal23ProfilerEventsProcessorD0Ev, .-_ZN2v88internal23ProfilerEventsProcessorD0Ev
	.section	.text._ZThn48_N2v88internal23ProfilerEventsProcessorD0Ev,"ax",@progbits
	.p2align 4
	.globl	_ZThn48_N2v88internal23ProfilerEventsProcessorD0Ev
	.type	_ZThn48_N2v88internal23ProfilerEventsProcessorD0Ev, @function
_ZThn48_N2v88internal23ProfilerEventsProcessorD0Ev:
.LFB24559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal23ProfilerEventsProcessorD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$376, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24559:
	.size	_ZThn48_N2v88internal23ProfilerEventsProcessorD0Ev, .-_ZThn48_N2v88internal23ProfilerEventsProcessorD0Ev
	.section	.text._ZN2v88internal23ProfilerEventsProcessor7EnqueueERKNS0_19CodeEventsContainerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ProfilerEventsProcessor7EnqueueERKNS0_19CodeEventsContainerE
	.type	_ZN2v88internal23ProfilerEventsProcessor7EnqueueERKNS0_19CodeEventsContainerE, @function
_ZN2v88internal23ProfilerEventsProcessor7EnqueueERKNS0_19CodeEventsContainerE:
.LFB19081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	lock xaddl	%eax, 360(%rdi)
	movl	$72, %edi
	addl	$1, %eax
	movl	%eax, 4(%rsi)
	call	_ZN2v88internal8MallocednwEm@PLT
	movl	$0, (%rax)
	movq	%rax, %rbx
	movq	$0, 64(%rax)
	movdqu	0(%r13), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r13), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%r13), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%r13), %xmm3
	leaq	208(%r12), %r13
	movq	%r13, %rdi
	movups	%xmm3, 48(%rax)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	256(%r12), %rax
	movq	%r13, %rdi
	movq	%rbx, 64(%rax)
	movq	%rbx, 256(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE19081:
	.size	_ZN2v88internal23ProfilerEventsProcessor7EnqueueERKNS0_19CodeEventsContainerE, .-_ZN2v88internal23ProfilerEventsProcessor7EnqueueERKNS0_19CodeEventsContainerE
	.section	.text._ZN2v88internal23ProfilerEventsProcessor13AddDeoptStackEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ProfilerEventsProcessor13AddDeoptStackEmi
	.type	_ZN2v88internal23ProfilerEventsProcessor13AddDeoptStackEmi, @function
_ZN2v88internal23ProfilerEventsProcessor13AddDeoptStackEmi:
.LFB19082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$112, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	pxor	%xmm0, %xmm0
	xorl	%r9d, %r9d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %r8
	movslq	%edx, %rdx
	movq	%rdi, %r12
	leaq	-4192(%rbp), %r13
	movl	360(%rdi), %eax
	movl	%eax, -4192(%rbp)
	movq	%r8, -4224(%rbp)
	xorl	%r8d, %r8d
	movzwl	-72(%rbp), %eax
	movq	368(%rdi), %rsi
	movaps	%xmm0, -64(%rbp)
	leaq	-4184(%rbp), %rdi
	movaps	%xmm0, -4176(%rbp)
	andw	$-1024, %ax
	movl	$5, -4184(%rbp)
	orb	$2, %ah
	movq	$0, -80(%rbp)
	movw	%ax, -72(%rbp)
	movq	12560(%rsi), %rax
	pushq	$0
	movq	%rax, %rcx
	movq	%rax, -4208(%rbp)
	movq	$0, -4200(%rbp)
	subq	%rdx, %rcx
	leaq	-4224(%rbp), %rdx
	movq	%rcx, -4216(%rbp)
	movl	$1, %ecx
	call	_ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE@PLT
	movl	$4152, %edi
	call	_ZN2v88internal8MallocednwEm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movl	$518, %ecx
	movq	%rax, %rbx
	movups	%xmm0, 16(%rax)
	leaq	304(%r12), %r13
	movl	$5, 8(%rax)
	movq	%rbx, %rdi
	movq	$0, 4112(%rax)
	movzwl	4120(%rax), %eax
	movups	%xmm0, 4128(%rbx)
	movq	$0, 4144(%rbx)
	andw	$-1024, %ax
	orb	$2, %ah
	movw	%ax, 4120(%rbx)
	rep movsq
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	352(%r12), %rax
	movq	%r13, %rdi
	movq	%rbx, 4144(%rax)
	movq	%rbx, 352(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L153:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19082:
	.size	_ZN2v88internal23ProfilerEventsProcessor13AddDeoptStackEmi, .-_ZN2v88internal23ProfilerEventsProcessor13AddDeoptStackEmi
	.section	.rodata._ZN2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.type	_ZN2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE, @function
_ZN2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE:
.LFB19087:
	.cfi_startproc
	endbr64
	cmpl	$6, (%rsi)
	ja	.L154
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L157(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	(%rsi), %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE,"a",@progbits
	.align 4
	.align 4
.L157:
	.long	.L158-.L157
	.long	.L156-.L157
	.long	.L156-.L157
	.long	.L156-.L157
	.long	.L159-.L157
	.long	.L158-.L157
	.long	.L156-.L157
	.section	.text._ZN2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.p2align 4,,10
	.p2align 3
.L156:
	movl	$1, %eax
	lock xaddl	%eax, 360(%rdi)
	movl	$72, %edi
	addl	$1, %eax
	leaq	208(%r12), %r14
	movl	%eax, 4(%rsi)
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	%r14, %rdi
	movl	$0, (%rax)
	movq	%rax, %r13
	movq	$0, 64(%rax)
	movdqu	(%rbx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%rax)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	256(%r12), %rax
	movq	%r14, %rdi
	movq	%r13, 64(%rax)
	movq	%r13, 256(%r12)
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movl	40(%rsi), %edx
	movq	32(%rsi), %r15
	movl	$1, %eax
	movl	%edx, -52(%rbp)
	lock xaddl	%eax, 360(%rdi)
	movl	$72, %edi
	addl	$1, %eax
	leaq	208(%r12), %r14
	movl	%eax, 4(%rsi)
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	%r14, %rdi
	movl	$0, (%rax)
	movq	%rax, %r13
	movq	$0, 64(%rax)
	movdqu	(%rbx), %xmm4
	movups	%xmm4, (%rax)
	movdqu	16(%rbx), %xmm5
	movups	%xmm5, 16(%rax)
	movdqu	32(%rbx), %xmm6
	movups	%xmm6, 32(%rax)
	movdqu	48(%rbx), %xmm7
	movups	%xmm7, 48(%rax)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	256(%r12), %rax
	movq	%r14, %rdi
	movq	%r13, 64(%rax)
	movq	%r13, 256(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	-52(%rbp), %edx
	addq	$24, %rsp
	movq	%r15, %rsi
	popq	%rbx
	.cfi_restore 3
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23ProfilerEventsProcessor13AddDeoptStackEmi
.L154:
	ret
.L158:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19087:
	.size	_ZN2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE, .-_ZN2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.section	.text._ZThn48_N2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE,"ax",@progbits
	.p2align 4
	.globl	_ZThn48_N2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.type	_ZThn48_N2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE, @function
_ZThn48_N2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE:
.LFB24562:
	.cfi_startproc
	endbr64
	cmpl	$6, (%rsi)
	ja	.L163
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L166(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	(%rsi), %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZThn48_N2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE,"a",@progbits
	.align 4
	.align 4
.L166:
	.long	.L167-.L166
	.long	.L165-.L166
	.long	.L165-.L166
	.long	.L165-.L166
	.long	.L168-.L166
	.long	.L167-.L166
	.long	.L165-.L166
	.section	.text._ZThn48_N2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.p2align 4,,10
	.p2align 3
.L165:
	movl	$1, %eax
	lock xaddl	%eax, 312(%rdi)
	movl	$72, %edi
	addl	$1, %eax
	leaq	160(%r13), %r14
	movl	%eax, 4(%rsi)
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	%r14, %rdi
	movl	$0, (%rax)
	movq	%rax, %r12
	movq	$0, 64(%rax)
	movdqu	(%rbx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%rax)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	208(%r13), %rax
	movq	%r14, %rdi
	movq	%r12, 64(%rax)
	movq	%r12, 208(%r13)
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movl	40(%rsi), %edx
	movq	32(%rsi), %r15
	movl	$1, %eax
	movl	%edx, -52(%rbp)
	lock xaddl	%eax, 312(%rdi)
	movl	$72, %edi
	addl	$1, %eax
	leaq	160(%r13), %r14
	movl	%eax, 4(%rsi)
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	%r14, %rdi
	movl	$0, (%rax)
	movq	%rax, %r12
	movq	$0, 64(%rax)
	movdqu	(%rbx), %xmm4
	movups	%xmm4, (%rax)
	movdqu	16(%rbx), %xmm5
	movups	%xmm5, 16(%rax)
	movdqu	32(%rbx), %xmm6
	movups	%xmm6, 32(%rax)
	movdqu	48(%rbx), %xmm7
	movups	%xmm7, 48(%rax)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	208(%r13), %rax
	movq	%r14, %rdi
	movq	%r12, 64(%rax)
	movq	%r12, 208(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	-52(%rbp), %edx
	addq	$24, %rsp
	leaq	-48(%r13), %rdi
	popq	%rbx
	.cfi_restore 3
	movq	%r15, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23ProfilerEventsProcessor13AddDeoptStackEmi
.L163:
	ret
.L167:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24562:
	.size	_ZThn48_N2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE, .-_ZThn48_N2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.section	.text._ZN2v88internal23ProfilerEventsProcessor15AddCurrentStackEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ProfilerEventsProcessor15AddCurrentStackEb
	.type	_ZN2v88internal23ProfilerEventsProcessor15AddCurrentStackEb, @function
_ZN2v88internal23ProfilerEventsProcessor15AddCurrentStackEb:
.LFB19083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1544, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	pxor	%xmm0, %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%esi, %ebx
	movq	%rdi, %r12
	movl	360(%rdi), %eax
	movl	%eax, -4192(%rbp)
	movl	$5, -4184(%rbp)
	movzwl	-72(%rbp), %eax
	movq	368(%rdi), %rsi
	movaps	%xmm0, -64(%rbp)
	leaq	-5632(%rbp), %rdi
	movq	$0, -80(%rbp)
	andw	$-1024, %ax
	movaps	%xmm0, -4176(%rbp)
	orb	$2, %ah
	movaps	%xmm0, -5664(%rbp)
	movw	%ax, -72(%rbp)
	movaps	%xmm0, -5648(%rbp)
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-4216(%rbp), %rax
	testq	%rax, %rax
	je	.L173
	movq	24(%rax), %rdx
	movq	%rdx, -5656(%rbp)
	movq	32(%rax), %rdx
	movq	40(%rax), %rax
	movq	%rdx, -5648(%rbp)
	movq	(%rax), %rax
	movq	%rax, -5664(%rbp)
.L173:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movzbl	%bl, %r8d
	movl	$1, %ecx
	pushq	$0
	movq	368(%r12), %rsi
	leaq	-5664(%rbp), %rdx
	leaq	-4184(%rbp), %rdi
	leaq	-4192(%rbp), %r13
	call	_ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE@PLT
	movl	$4152, %edi
	call	_ZN2v88internal8MallocednwEm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movl	$518, %ecx
	movq	%rax, %rbx
	movups	%xmm0, 16(%rax)
	leaq	304(%r12), %r13
	movl	$5, 8(%rax)
	movq	%rbx, %rdi
	movq	$0, 4112(%rax)
	movzwl	4120(%rax), %eax
	movups	%xmm0, 4128(%rbx)
	movq	$0, 4144(%rbx)
	andw	$-1024, %ax
	orb	$2, %ah
	movw	%ax, 4120(%rbx)
	rep movsq
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	352(%r12), %rax
	movq	%r13, %rdi
	movq	%rbx, 4144(%rax)
	movq	%rbx, 352(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L179:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19083:
	.size	_ZN2v88internal23ProfilerEventsProcessor15AddCurrentStackEb, .-_ZN2v88internal23ProfilerEventsProcessor15AddCurrentStackEb
	.section	.text._ZN2v88internal23ProfilerEventsProcessor9AddSampleENS0_10TickSampleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ProfilerEventsProcessor9AddSampleENS0_10TickSampleE
	.type	_ZN2v88internal23ProfilerEventsProcessor9AddSampleENS0_10TickSampleE, @function
_ZN2v88internal23ProfilerEventsProcessor9AddSampleENS0_10TickSampleE:
.LFB19084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	$517, %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r12
	leaq	16(%rbp), %rsi
	movl	360(%rdi), %r13d
	leaq	-4184(%rbp), %rdi
	rep movsq
	movl	$4152, %edi
	call	_ZN2v88internal8MallocednwEm@PLT
	movl	%r13d, -4192(%rbp)
	movl	$518, %ecx
	leaq	-4192(%rbp), %rsi
	movq	%rax, %rdi
	leaq	304(%r12), %r13
	movq	%rax, %rbx
	movq	$0, 4144(%rax)
	rep movsq
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	352(%r12), %rax
	movq	%rbx, 4144(%rax)
	movq	%rbx, 352(%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L184
	addq	$4168, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
.L184:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19084:
	.size	_ZN2v88internal23ProfilerEventsProcessor9AddSampleENS0_10TickSampleE, .-_ZN2v88internal23ProfilerEventsProcessor9AddSampleENS0_10TickSampleE
	.section	.text._ZN2v88internal23ProfilerEventsProcessor17StopSynchronouslyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ProfilerEventsProcessor17StopSynchronouslyEv
	.type	_ZN2v88internal23ProfilerEventsProcessor17StopSynchronouslyEv, @function
_ZN2v88internal23ProfilerEventsProcessor17StopSynchronouslyEv:
.LFB19085:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	xchgl	72(%rdi), %eax
	testl	%eax, %eax
	jne	.L190
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	128(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	80(%r12), %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base6Thread4JoinEv@PLT
	.cfi_endproc
.LFE19085:
	.size	_ZN2v88internal23ProfilerEventsProcessor17StopSynchronouslyEv, .-_ZN2v88internal23ProfilerEventsProcessor17StopSynchronouslyEv
	.section	.text._ZN2v88internal23SamplingEventsProcessornwEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23SamplingEventsProcessornwEm
	.type	_ZN2v88internal23SamplingEventsProcessornwEm, @function
_ZN2v88internal23SamplingEventsProcessornwEm:
.LFB19094:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZN2v88internal12AlignedAllocEmm@PLT
	.cfi_endproc
.LFE19094:
	.size	_ZN2v88internal23SamplingEventsProcessornwEm, .-_ZN2v88internal23SamplingEventsProcessornwEm
	.section	.text._ZN2v88internal23SamplingEventsProcessordlEPv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23SamplingEventsProcessordlEPv
	.type	_ZN2v88internal23SamplingEventsProcessordlEPv, @function
_ZN2v88internal23SamplingEventsProcessordlEPv:
.LFB19095:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal11AlignedFreeEPv@PLT
	.cfi_endproc
.LFE19095:
	.size	_ZN2v88internal23SamplingEventsProcessordlEPv, .-_ZN2v88internal23SamplingEventsProcessordlEPv
	.section	.rodata._ZN2v88internal20ProfilerCodeObserverC2EPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"native V8Runtime"
	.section	.text._ZN2v88internal20ProfilerCodeObserverC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ProfilerCodeObserverC2EPNS0_7IsolateE
	.type	_ZN2v88internal20ProfilerCodeObserverC2EPNS0_7IsolateE, @function
_ZN2v88internal20ProfilerCodeObserverC2EPNS0_7IsolateE:
.LFB19097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	.LC2(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	16(%rdi), %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal20ProfilerCodeObserverE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal7CodeMapC1Ev@PLT
	movq	8(%r14), %rax
	movq	$0, 152(%r14)
	movq	40960(%rax), %rbx
	leaq	50888(%rbx), %rax
	leaq	23264(%rbx), %r15
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L194:
	movq	(%r15), %rbx
	movl	$64, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rbx, 8(%rax)
	movq	%rax, %rdx
	movl	$1, %ecx
	addq	$24, %r15
	movl	$397579, (%rax)
	movq	%r13, 16(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, 24(%rax)
	call	_ZN2v88internal7CodeMap7AddCodeEmPNS0_9CodeEntryEj@PLT
	cmpq	%r15, -72(%rbp)
	jne	.L194
	movq	8(%r14), %r13
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %r14
	addq	$41184, %r13
	.p2align 4,,10
	.p2align 3
.L200:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, -64(%rbp)
	leaq	63(%rax), %rsi
	movl	43(%rax), %eax
	testl	%eax, %eax
	js	.L210
.L196:
	movq	%r12, %rdi
	call	_ZN2v88internal7CodeMap9FindEntryEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L197
	movl	%ebx, %esi
	addl	$1, %ebx
	call	_ZN2v88internal9CodeEntry12SetBuiltinIdENS0_8Builtins4NameE@PLT
	cmpl	$1553, %ebx
	jne	.L200
	.p2align 4,,10
	.p2align 3
.L193:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L211
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	addl	$1, %ebx
	cmpl	$1553, %ebx
	jne	.L200
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%r14, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rsi
	jmp	.L196
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19097:
	.size	_ZN2v88internal20ProfilerCodeObserverC2EPNS0_7IsolateE, .-_ZN2v88internal20ProfilerCodeObserverC2EPNS0_7IsolateE
	.globl	_ZN2v88internal20ProfilerCodeObserverC1EPNS0_7IsolateE
	.set	_ZN2v88internal20ProfilerCodeObserverC1EPNS0_7IsolateE,_ZN2v88internal20ProfilerCodeObserverC2EPNS0_7IsolateE
	.section	.rodata._ZN2v88internal20ProfilerCodeObserver24CodeEventHandlerInternalERKNS0_19CodeEventsContainerE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal20ProfilerCodeObserver24CodeEventHandlerInternalERKNS0_19CodeEventsContainerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ProfilerCodeObserver24CodeEventHandlerInternalERKNS0_19CodeEventsContainerE
	.type	_ZN2v88internal20ProfilerCodeObserver24CodeEventHandlerInternalERKNS0_19CodeEventsContainerE, @function
_ZN2v88internal20ProfilerCodeObserver24CodeEventHandlerInternalERKNS0_19CodeEventsContainerE:
.LFB19100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movdqu	16(%rsi), %xmm1
	movq	8(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$5, (%rsi)
	movl	24(%rsi), %r13d
	movaps	%xmm1, -112(%rbp)
	ja	.L212
	movl	(%rsi), %eax
	leaq	.L215(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal20ProfilerCodeObserver24CodeEventHandlerInternalERKNS0_19CodeEventsContainerE,"a",@progbits
	.align 4
	.align 4
.L215:
	.long	.L212-.L215
	.long	.L219-.L215
	.long	.L218-.L215
	.long	.L217-.L215
	.long	.L216-.L215
	.long	.L214-.L215
	.section	.text._ZN2v88internal20ProfilerCodeObserver24CodeEventHandlerInternalERKNS0_19CodeEventsContainerE
	.p2align 4,,10
	.p2align 3
.L214:
	addq	$16, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal7CodeMap9FindEntryEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L212
	movl	-112(%rbp), %esi
	call	_ZN2v88internal9CodeEntry12SetBuiltinIdENS0_8Builtins4NameE@PLT
	.p2align 4,,10
	.p2align 3
.L212:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L244
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	-112(%rbp), %rdx
	addq	$16, %rdi
	movl	%r13d, %ecx
	movq	%r8, %rsi
	call	_ZN2v88internal7CodeMap7AddCodeEmPNS0_9CodeEntryEj@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L218:
	movq	-112(%rbp), %rdx
	addq	$16, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal7CodeMap8MoveCodeEmm@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L217:
	addq	$16, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal7CodeMap9FindEntryEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L212
	movq	-112(%rbp), %rbx
	call	_ZN2v88internal9CodeEntry14EnsureRareDataEv@PLT
	movq	%rbx, 8(%rax)
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L216:
	movq	48(%rsi), %r12
	movslq	56(%rsi), %rbx
	addq	$16, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal7CodeMap9FindEntryEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L212
	movabsq	$576460752303423487, %rax
	movq	%rbx, %r15
	salq	$4, %r15
	cmpq	%rax, %rbx
	ja	.L245
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.L223
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%rax, %rcx
.L223:
	movq	-112(%rbp), %r8
	leaq	(%rcx,%r15), %rbx
	testq	%r15, %r15
	je	.L224
	movq	%rcx, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, -168(%rbp)
	call	memcpy@PLT
	movq	%rbx, %xmm2
	movq	%r14, %rdi
	movl	%r13d, %edx
	movq	-168(%rbp), %r8
	movq	%rax, %xmm0
	leaq	-160(%rbp), %rcx
	movq	%rbx, -144(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movq	%r8, %rsi
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal9CodeEntry14set_deopt_infoEPKciSt6vectorINS_20CpuProfileDeoptFrameESaIS5_EE@PLT
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L230
.L225:
	call	_ZdlPv@PLT
.L227:
	testq	%r12, %r12
	je	.L212
.L230:
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L224:
	movq	%rcx, %xmm0
	movq	%r14, %rdi
	movl	%r13d, %edx
	movq	%r8, %rsi
	movq	%rcx, -144(%rbp)
	punpcklqdq	%xmm0, %xmm0
	leaq	-160(%rbp), %rcx
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal9CodeEntry14set_deopt_infoEPKciSt6vectorINS_20CpuProfileDeoptFrameESaIS5_EE@PLT
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L225
	jmp	.L227
.L244:
	call	__stack_chk_fail@PLT
.L245:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE19100:
	.size	_ZN2v88internal20ProfilerCodeObserver24CodeEventHandlerInternalERKNS0_19CodeEventsContainerE, .-_ZN2v88internal20ProfilerCodeObserver24CodeEventHandlerInternalERKNS0_19CodeEventsContainerE
	.section	.text._ZN2v88internal20ProfilerCodeObserver16CodeEventHandlerERKNS0_19CodeEventsContainerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ProfilerCodeObserver16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.type	_ZN2v88internal20ProfilerCodeObserver16CodeEventHandlerERKNS0_19CodeEventsContainerE, @function
_ZN2v88internal20ProfilerCodeObserver16CodeEventHandlerERKNS0_19CodeEventsContainerE:
.LFB19099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	152(%rdi), %r13
	testq	%r13, %r13
	je	.L247
	movq	0(%r13), %rax
	leaq	_ZN2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L248
	cmpl	$6, (%rsi)
	ja	.L246
	movl	(%rsi), %eax
	leaq	.L251(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal20ProfilerCodeObserver16CodeEventHandlerERKNS0_19CodeEventsContainerE,"a",@progbits
	.align 4
	.align 4
.L251:
	.long	.L252-.L251
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L253-.L251
	.long	.L252-.L251
	.long	.L250-.L251
	.section	.text._ZN2v88internal20ProfilerCodeObserver16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.p2align 4,,10
	.p2align 3
.L247:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal20ProfilerCodeObserver24CodeEventHandlerInternalERKNS0_19CodeEventsContainerE
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movl	$1, %eax
	lock xaddl	%eax, 360(%r13)
	movl	$72, %edi
	addl	$1, %eax
	movl	%eax, 4(%rsi)
	call	_ZN2v88internal8MallocednwEm@PLT
	movl	$0, (%rax)
	movq	%rax, %rbx
	movq	$0, 64(%rax)
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%r12), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%r12), %xmm3
	leaq	208(%r13), %r12
	movq	%r12, %rdi
	movups	%xmm3, 48(%rax)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	256(%r13), %rax
	movq	%r12, %rdi
	movq	%rbx, 64(%rax)
	movq	%rbx, 256(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movq	32(%rsi), %r14
	movl	40(%rsi), %r15d
	movl	$1, %eax
	lock xaddl	%eax, 360(%r13)
	movl	$72, %edi
	addl	$1, %eax
	movl	%eax, 4(%rsi)
	call	_ZN2v88internal8MallocednwEm@PLT
	movl	$0, (%rax)
	movq	%rax, %rbx
	movq	$0, 64(%rax)
	movdqu	(%r12), %xmm4
	movups	%xmm4, (%rax)
	movdqu	16(%r12), %xmm5
	movups	%xmm5, 16(%rax)
	movdqu	32(%r12), %xmm6
	movups	%xmm6, 32(%rax)
	movdqu	48(%r12), %xmm7
	leaq	208(%r13), %r12
	movq	%r12, %rdi
	movups	%xmm7, 48(%rax)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	256(%r13), %rax
	movq	%r12, %rdi
	movq	%rbx, 64(%rax)
	movq	%rbx, 256(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movl	%r15d, %edx
	movq	%r14, %rsi
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23ProfilerEventsProcessor13AddDeoptStackEmi
.L246:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L252:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19099:
	.size	_ZN2v88internal20ProfilerCodeObserver16CodeEventHandlerERKNS0_19CodeEventsContainerE, .-_ZN2v88internal20ProfilerCodeObserver16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.section	.text._ZN2v88internal23ProfilerEventsProcessor16ProcessCodeEventEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ProfilerEventsProcessor16ProcessCodeEventEv
	.type	_ZN2v88internal23ProfilerEventsProcessor16ProcessCodeEventEv, @function
_ZN2v88internal23ProfilerEventsProcessor16ProcessCodeEventEv:
.LFB19086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	168(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -112(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	248(%rbx), %r12
	movq	64(%r12), %rax
	testq	%rax, %rax
	je	.L266
	movdqu	(%rax), %xmm0
	movq	%r13, %rdi
	movaps	%xmm0, -112(%rbp)
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -96(%rbp)
	movdqu	32(%rax), %xmm2
	movaps	%xmm2, -80(%rbp)
	movdqu	48(%rax), %xmm3
	movq	%rax, 248(%rbx)
	movaps	%xmm3, -64(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r12, %r12
	je	.L258
	movq	%r12, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L258:
	cmpl	$6, -112(%rbp)
	je	.L267
	movq	64(%rbx), %rdi
	leaq	-112(%rbp), %rsi
	call	_ZN2v88internal20ProfilerCodeObserver24CodeEventHandlerInternalERKNS0_19CodeEventsContainerE
.L260:
	movl	-108(%rbp), %eax
	movl	%eax, 364(%rbx)
	movl	$1, %eax
.L255:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L268
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	xorl	%eax, %eax
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L267:
	movq	56(%rbx), %rdi
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %rsi
	call	_ZN2v88internal16ProfileGenerator26UpdateNativeContextAddressEmm@PLT
	jmp	.L260
.L268:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19086:
	.size	_ZN2v88internal23ProfilerEventsProcessor16ProcessCodeEventEv, .-_ZN2v88internal23ProfilerEventsProcessor16ProcessCodeEventEv
	.section	.text._ZN2v88internal23SamplingEventsProcessor3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23SamplingEventsProcessor3RunEv
	.type	_ZN2v88internal23SamplingEventsProcessor3RunEv, @function
_ZN2v88internal23SamplingEventsProcessor3RunEv:
.LFB19092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	128(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	leaq	80(%r12), %r15
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	72(%r12), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L281
	.p2align 4,,10
	.p2align 3
.L294:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	524680(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v84base4bits20SignedSaturatedAdd64Ell@PLT
	movq	%rax, %rbx
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L271:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	cmpl	$2, %r14d
	je	.L273
	cmpq	%rax, %rbx
	jle	.L277
.L274:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	movl	%eax, %r14d
	cmpl	$1, %eax
	jne	.L271
	movq	%r12, %rdi
	call	_ZN2v88internal23ProfilerEventsProcessor16ProcessCodeEventEv
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	cmpq	%rax, %rbx
	jg	.L274
	.p2align 4,,10
	.p2align 3
.L277:
	movq	524672(%r12), %rdi
	call	_ZN2v87sampler7Sampler8DoSampleEv@PLT
.L296:
	movq	-72(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L294
	.p2align 4,,10
	.p2align 3
.L281:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testl	%eax, %eax
	je	.L281
	movq	%r12, %rdi
	call	_ZN2v88internal23ProfilerEventsProcessor16ProcessCodeEventEv
	testb	%al, %al
	jne	.L281
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L295
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	cmpq	%rax, %rbx
	jle	.L277
	leaq	-64(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	subq	%rax, %rcx
	movq	%rcx, -64(%rbp)
	call	_ZN2v84base17ConditionVariable7WaitForEPNS0_5MutexERKNS0_9TimeDeltaE@PLT
	testb	%al, %al
	je	.L277
	movq	-72(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L277
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	cmpq	%rax, %rbx
	jg	.L278
	movq	524672(%r12), %rdi
	call	_ZN2v87sampler7Sampler8DoSampleEv@PLT
	jmp	.L296
.L295:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19092:
	.size	_ZN2v88internal23SamplingEventsProcessor3RunEv, .-_ZN2v88internal23SamplingEventsProcessor3RunEv
	.section	.text._ZN2v88internal20ProfilerCodeObserver32CreateEntriesForRuntimeCallStatsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ProfilerCodeObserver32CreateEntriesForRuntimeCallStatsEv
	.type	_ZN2v88internal20ProfilerCodeObserver32CreateEntriesForRuntimeCallStatsEv, @function
_ZN2v88internal20ProfilerCodeObserver32CreateEntriesForRuntimeCallStatsEv:
.LFB19101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	.LC2(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	16(%rdi), %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	40960(%rax), %r12
	leaq	23264(%r12), %r14
	addq	$50888, %r12
	.p2align 4,,10
	.p2align 3
.L298:
	movq	(%r14), %r15
	movl	$64, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r15, 8(%rax)
	movq	%rax, %rdx
	movl	$1, %ecx
	addq	$24, %r14
	movl	$397579, (%rax)
	movq	%r13, 16(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, 24(%rax)
	call	_ZN2v88internal7CodeMap7AddCodeEmPNS0_9CodeEntryEj@PLT
	cmpq	%r12, %r14
	jne	.L298
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19101:
	.size	_ZN2v88internal20ProfilerCodeObserver32CreateEntriesForRuntimeCallStatsEv, .-_ZN2v88internal20ProfilerCodeObserver32CreateEntriesForRuntimeCallStatsEv
	.section	.text._ZN2v88internal20ProfilerCodeObserver11LogBuiltinsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ProfilerCodeObserver11LogBuiltinsEv
	.type	_ZN2v88internal20ProfilerCodeObserver11LogBuiltinsEv, @function
_ZN2v88internal20ProfilerCodeObserver11LogBuiltinsEv:
.LFB19102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	leaq	41184(%rax), %r13
	.p2align 4,,10
	.p2align 3
.L307:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, -48(%rbp)
	leaq	63(%rax), %rsi
	movl	43(%rax), %eax
	testl	%eax, %eax
	js	.L316
.L303:
	movq	%r12, %rdi
	call	_ZN2v88internal7CodeMap9FindEntryEm@PLT
	testq	%rax, %rax
	je	.L304
	movl	%ebx, %esi
	movq	%rax, %rdi
	addl	$1, %ebx
	call	_ZN2v88internal9CodeEntry12SetBuiltinIdENS0_8Builtins4NameE@PLT
	cmpl	$1553, %ebx
	jne	.L307
	.p2align 4,,10
	.p2align 3
.L301:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L317
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	addl	$1, %ebx
	cmpl	$1553, %ebx
	jne	.L307
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%r14, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rsi
	jmp	.L303
.L317:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19102:
	.size	_ZN2v88internal20ProfilerCodeObserver11LogBuiltinsEv, .-_ZN2v88internal20ProfilerCodeObserver11LogBuiltinsEv
	.section	.text._ZN2v88internal11CpuProfiler16GetProfilesCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler16GetProfilesCountEv
	.type	_ZN2v88internal11CpuProfiler16GetProfilesCountEv, @function
_ZN2v88internal11CpuProfiler16GetProfilesCountEv:
.LFB19103:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	movq	32(%rdx), %rax
	subq	24(%rdx), %rax
	sarq	$3, %rax
	ret
	.cfi_endproc
.LFE19103:
	.size	_ZN2v88internal11CpuProfiler16GetProfilesCountEv, .-_ZN2v88internal11CpuProfiler16GetProfilesCountEv
	.section	.rodata._ZN2v88internal11CpuProfiler10GetProfileEi.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal11CpuProfiler10GetProfileEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler10GetProfileEi
	.type	_ZN2v88internal11CpuProfiler10GetProfileEi, @function
_ZN2v88internal11CpuProfiler10GetProfileEi:
.LFB19104:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movslq	%esi, %rsi
	movq	24(%rax), %rcx
	movq	32(%rax), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L324
	movq	(%rcx,%rsi,8), %rax
	ret
.L324:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE19104:
	.size	_ZN2v88internal11CpuProfiler10GetProfileEi, .-_ZN2v88internal11CpuProfiler10GetProfileEi
	.section	.text._ZN2v88internal11CpuProfiler21set_sampling_intervalENS_4base9TimeDeltaE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler21set_sampling_intervalENS_4base9TimeDeltaE
	.type	_ZN2v88internal11CpuProfiler21set_sampling_intervalENS_4base9TimeDeltaE, @function
_ZN2v88internal11CpuProfiler21set_sampling_intervalENS_4base9TimeDeltaE:
.LFB19124:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE19124:
	.size	_ZN2v88internal11CpuProfiler21set_sampling_intervalENS_4base9TimeDeltaE, .-_ZN2v88internal11CpuProfiler21set_sampling_intervalENS_4base9TimeDeltaE
	.section	.text._ZN2v88internal11CpuProfiler24set_use_precise_samplingEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler24set_use_precise_samplingEb
	.type	_ZN2v88internal11CpuProfiler24set_use_precise_samplingEb, @function
_ZN2v88internal11CpuProfiler24set_use_precise_samplingEb:
.LFB19125:
	.cfi_startproc
	endbr64
	movb	%sil, 16(%rdi)
	ret
	.cfi_endproc
.LFE19125:
	.size	_ZN2v88internal11CpuProfiler24set_use_precise_samplingEb, .-_ZN2v88internal11CpuProfiler24set_use_precise_samplingEb
	.section	.text._ZN2v88internal11CpuProfiler13ResetProfilesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler13ResetProfilesEv
	.type	_ZN2v88internal11CpuProfiler13ResetProfilesEv, @function
_ZN2v88internal11CpuProfiler13ResetProfilesEv:
.LFB19126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$112, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	movq	(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal21CpuProfilesCollectionC1EPNS0_7IsolateE@PLT
	movq	32(%rbx), %r13
	movq	%r12, 32(%rbx)
	testq	%r13, %r13
	je	.L328
	leaq	80(%r13), %rdi
	call	_ZN2v84base9SemaphoreD1Ev@PLT
	movq	64(%r13), %rax
	movq	56(%r13), %r14
	movq	%rax, -64(%rbp)
	cmpq	%r14, %rax
	je	.L329
	.p2align 4,,10
	.p2align 3
.L335:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L330
	leaq	128(%r12), %rdi
	call	_ZN2v88internal11ProfileTreeD1Ev@PLT
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L331
	movq	120(%r12), %rax
	movq	88(%r12), %r15
	leaq	8(%rax), %rdx
	cmpq	%r15, %rdx
	jbe	.L332
	.p2align 4,,10
	.p2align 3
.L333:
	movq	(%r15), %rdi
	movq	%rdx, -56(%rbp)
	addq	$8, %r15
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rdx
	cmpq	%r15, %rdx
	ja	.L333
	movq	48(%r12), %rdi
.L332:
	call	_ZdlPv@PLT
.L331:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L334
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L334:
	movl	$272, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L330:
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	jne	.L335
	movq	56(%r13), %r14
.L329:
	testq	%r14, %r14
	je	.L336
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L336:
	movq	32(%r13), %rax
	movq	24(%r13), %r14
	movq	%rax, -64(%rbp)
	cmpq	%r14, %rax
	je	.L337
	.p2align 4,,10
	.p2align 3
.L343:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L338
	leaq	128(%r12), %rdi
	call	_ZN2v88internal11ProfileTreeD1Ev@PLT
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L339
	movq	120(%r12), %rax
	movq	88(%r12), %r15
	leaq	8(%rax), %rdx
	cmpq	%r15, %rdx
	jbe	.L340
	.p2align 4,,10
	.p2align 3
.L341:
	movq	(%r15), %rdi
	movq	%rdx, -56(%rbp)
	addq	$8, %r15
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rdx
	cmpq	%r15, %rdx
	ja	.L341
	movq	48(%r12), %rdi
.L340:
	call	_ZdlPv@PLT
.L339:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L342
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L342:
	movl	$272, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L338:
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	jne	.L343
	movq	24(%r13), %r14
.L337:
	testq	%r14, %r14
	je	.L344
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L344:
	movq	%r13, %rdi
	call	_ZN2v88internal14StringsStorageD1Ev@PLT
	movl	$112, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	32(%rbx), %r12
.L328:
	movq	%rbx, 48(%r12)
	movq	40(%rbx), %rdi
	movq	$0, 40(%rbx)
	testq	%rdi, %rdi
	je	.L345
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L345:
	cmpq	$0, 64(%rbx)
	je	.L387
.L327:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	.cfi_restore_state
	movq	56(%rbx), %rdi
	movq	$0, 56(%rbx)
	testq	%rdi, %rdi
	je	.L327
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19126:
	.size	_ZN2v88internal11CpuProfiler13ResetProfilesEv, .-_ZN2v88internal11CpuProfiler13ResetProfilesEv
	.section	.text._ZN2v88internal11CpuProfiler13DeleteProfileEPNS0_10CpuProfileE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler13DeleteProfileEPNS0_10CpuProfileE
	.type	_ZN2v88internal11CpuProfiler13DeleteProfileEPNS0_10CpuProfileE, @function
_ZN2v88internal11CpuProfiler13DeleteProfileEPNS0_10CpuProfileE:
.LFB19106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	call	_ZN2v88internal21CpuProfilesCollection13RemoveProfileEPNS0_10CpuProfileE@PLT
	movq	32(%r12), %rax
	movq	32(%rax), %rdx
	cmpq	%rdx, 24(%rax)
	je	.L391
.L388:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	cmpb	$0, 232(%r12)
	jne	.L388
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CpuProfiler13ResetProfilesEv
	.cfi_endproc
.LFE19106:
	.size	_ZN2v88internal11CpuProfiler13DeleteProfileEPNS0_10CpuProfileE, .-_ZN2v88internal11CpuProfiler13DeleteProfileEPNS0_10CpuProfileE
	.section	.text._ZN2v88internal11CpuProfiler13EnableLoggingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler13EnableLoggingEv
	.type	_ZN2v88internal11CpuProfiler13EnableLoggingEv, @function
_ZN2v88internal11CpuProfiler13EnableLoggingEv:
.LFB19127:
	.cfi_startproc
	endbr64
	cmpq	$0, 64(%rdi)
	je	.L394
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	jmp	_ZN2v88internal11CpuProfiler13EnableLoggingEv.part.0
	.cfi_endproc
.LFE19127:
	.size	_ZN2v88internal11CpuProfiler13EnableLoggingEv, .-_ZN2v88internal11CpuProfiler13EnableLoggingEv
	.section	.text._ZN2v88internal11CpuProfiler14DisableLoggingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler14DisableLoggingEv
	.type	_ZN2v88internal11CpuProfiler14DisableLoggingEv, @function
_ZN2v88internal11CpuProfiler14DisableLoggingEv:
.LFB19128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	64(%rdi), %r12
	testq	%r12, %r12
	je	.L395
	movq	$0, 64(%rdi)
	movq	(%r12), %rax
	movq	8(%r12), %rsi
	movq	41016(%rax), %rdi
	call	_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE@PLT
	movq	(%r12), %rdx
	subq	$1, 41816(%rdx)
	jne	.L397
	movq	(%r12), %rax
	movb	$0, 41812(%rax)
.L397:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L395:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19128:
	.size	_ZN2v88internal11CpuProfiler14DisableLoggingEv, .-_ZN2v88internal11CpuProfiler14DisableLoggingEv
	.section	.text._ZNK2v88internal11CpuProfiler23ComputeSamplingIntervalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11CpuProfiler23ComputeSamplingIntervalEv
	.type	_ZNK2v88internal11CpuProfiler23ComputeSamplingIntervalEv, @function
_ZNK2v88internal11CpuProfiler23ComputeSamplingIntervalEv:
.LFB19129:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	_ZNK2v88internal21CpuProfilesCollection25GetCommonSamplingIntervalEv@PLT
	.cfi_endproc
.LFE19129:
	.size	_ZNK2v88internal11CpuProfiler23ComputeSamplingIntervalEv, .-_ZNK2v88internal11CpuProfiler23ComputeSamplingIntervalEv
	.section	.text._ZN2v88internal11CpuProfiler22AdjustSamplingIntervalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler22AdjustSamplingIntervalEv
	.type	_ZN2v88internal11CpuProfiler22AdjustSamplingIntervalEv, @function
_ZN2v88internal11CpuProfiler22AdjustSamplingIntervalEv:
.LFB19130:
	.cfi_startproc
	endbr64
	cmpq	$0, 48(%rdi)
	je	.L400
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	call	_ZNK2v88internal21CpuProfilesCollection25GetCommonSamplingIntervalEv@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %rsi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L400:
	ret
	.cfi_endproc
.LFE19130:
	.size	_ZN2v88internal11CpuProfiler22AdjustSamplingIntervalEv, .-_ZN2v88internal11CpuProfiler22AdjustSamplingIntervalEv
	.section	.text._ZN2v88internal11CpuProfiler13CollectSampleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler13CollectSampleEv
	.type	_ZN2v88internal11CpuProfiler13CollectSampleEv, @function
_ZN2v88internal11CpuProfiler13CollectSampleEv:
.LFB19132:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L405
	xorl	%esi, %esi
	jmp	_ZN2v88internal23ProfilerEventsProcessor15AddCurrentStackEb
	.p2align 4,,10
	.p2align 3
.L405:
	ret
	.cfi_endproc
.LFE19132:
	.size	_ZN2v88internal11CpuProfiler13CollectSampleEv, .-_ZN2v88internal11CpuProfiler13CollectSampleEv
	.section	.text._ZN2v88internal11CpuProfiler26StartProcessorIfNotStartedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler26StartProcessorIfNotStartedEv
	.type	_ZN2v88internal11CpuProfiler26StartProcessorIfNotStartedEv, @function
_ZN2v88internal11CpuProfiler26StartProcessorIfNotStartedEv:
.LFB19135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L433
	cmpq	$0, 64(%rbx)
	je	.L434
	cmpq	$0, 40(%rbx)
	movq	32(%rbx), %r12
	je	.L435
.L410:
	movq	%r12, %rdi
	call	_ZNK2v88internal21CpuProfilesCollection25GetCommonSamplingIntervalEv@PLT
	movl	$64, %esi
	movl	$524736, %edi
	movq	40(%rbx), %r15
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal12AlignedAllocEmm@PLT
	movq	(%rbx), %r13
	leaq	72(%rbx), %rcx
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, %r12
	movzbl	16(%rbx), %r14d
	movq	%r13, %rsi
	call	_ZN2v88internal23ProfilerEventsProcessorC2EPNS0_7IsolateEPNS0_16ProfileGeneratorEPNS0_20ProfilerCodeObserverE
	leaq	16+_ZTVN2v88internal23SamplingEventsProcessorE(%rip), %r8
	pxor	%xmm0, %xmm0
	leaq	384(%r12), %rdi
	leaq	64(%r8), %rcx
	movq	%r8, (%r12)
	movq	%rdi, %rax
	leaq	524544(%r12), %r9
	movq	%rcx, 48(%r12)
	.p2align 4,,10
	.p2align 3
.L412:
	movzwl	4120(%rax), %edx
	movups	%xmm0, 16(%rax)
	addq	$4160, %rax
	movl	$5, -4152(%rax)
	andw	$-1024, %dx
	movq	$0, -48(%rax)
	orb	$2, %dh
	movups	%xmm0, -32(%rax)
	movw	%dx, -40(%rax)
	movl	$0, -16(%rax)
	cmpq	%rax, %r9
	jne	.L412
	movq	%rdi, 524544(%r12)
	movq	%rdi, 524608(%r12)
	movl	$56, %edi
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v87sampler7SamplerC2EPNS_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal10CpuSamplerE(%rip), %rax
	movq	%r12, 48(%r15)
	movq	%r15, %rdi
	movq	%rax, (%r15)
	movq	-56(%rbp), %rax
	movq	%r15, 524672(%r12)
	movq	%rax, 524680(%r12)
	movb	%r14b, 524688(%r12)
	call	_ZN2v87sampler7Sampler5StartEv@PLT
	movq	48(%rbx), %r13
	movq	%r12, 48(%rbx)
	testq	%r13, %r13
	je	.L413
	movq	0(%r13), %rax
	leaq	_ZN2v88internal23SamplingEventsProcessorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L414
	leaq	16+_ZTVN2v88internal23SamplingEventsProcessorE(%rip), %r8
	movq	524672(%r13), %rdi
	leaq	64(%r8), %rcx
	movq	%r8, 0(%r13)
	movq	%rcx, 48(%r13)
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	movq	524672(%r13), %rdi
	testq	%rdi, %rdi
	je	.L415
	movq	(%rdi), %rax
	call	*8(%rax)
.L415:
	movq	%r13, %rdi
	call	_ZN2v88internal23ProfilerEventsProcessorD2Ev
	movq	%r13, %rdi
	call	_ZN2v88internal11AlignedFreeEPv@PLT
	movq	48(%rbx), %r12
.L413:
	movb	$1, 232(%rbx)
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal23ProfilerEventsProcessor15AddCurrentStackEb
	movl	$32, %edi
	movq	48(%rbx), %r12
	call	_Znwm@PLT
	xorl	%esi, %esi
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN2v84base9SemaphoreC1Ei@PLT
	movq	%rbx, 40(%r12)
	movq	%r12, %rdi
	call	_ZN2v84base6Thread5StartEv@PLT
	testb	%al, %al
	jne	.L436
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	movq	40(%r12), %rdi
	call	_ZN2v84base9Semaphore4WaitEv@PLT
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L417
	movq	%r13, %rdi
	call	_ZN2v84base9SemaphoreD1Ev@PLT
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L417:
	movq	$0, 40(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23ProfilerEventsProcessor15AddCurrentStackEb
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal11CpuProfiler13EnableLoggingEv.part.0
	cmpq	$0, 40(%rbx)
	movq	32(%rbx), %r12
	jne	.L410
	.p2align 4,,10
	.p2align 3
.L435:
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	88(%rbx), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal16ProfileGeneratorC1EPNS0_21CpuProfilesCollectionEPNS0_7CodeMapE@PLT
	movq	40(%rbx), %rdi
	movq	%r13, 40(%rbx)
	testq	%rdi, %rdi
	je	.L432
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L432:
	movq	32(%rbx), %r12
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L414:
	movq	%r13, %rdi
	call	*%rax
	movq	48(%rbx), %r12
	jmp	.L413
	.cfi_endproc
.LFE19135:
	.size	_ZN2v88internal11CpuProfiler26StartProcessorIfNotStartedEv, .-_ZN2v88internal11CpuProfiler26StartProcessorIfNotStartedEv
	.section	.rodata._ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE.part.0.str1.1,"aMS",@progbits,1
.LC5:
	.string	"v8"
.LC6:
	.string	"CpuProfiler::StartProfiling"
	.section	.text._ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE.part.0, @function
_ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE.part.0:
.LFB24558:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsEE28trace_event_unique_atomic512(%rip), %rbx
	testq	%rbx, %rbx
	je	.L459
	movq	$0, -96(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L460
.L441:
	cmpq	$0, 48(%r12)
	je	.L445
	movq	32(%r12), %rdi
	call	_ZNK2v88internal21CpuProfilesCollection25GetCommonSamplingIntervalEv@PLT
	movq	48(%r12), %rdi
	movq	%rax, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L445:
	movq	%r12, %rdi
	call	_ZN2v88internal11CpuProfiler26StartProcessorIfNotStartedEv
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L461
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L462
.L440:
	movq	%rbx, _ZZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsEE28trace_event_unique_atomic512(%rip)
	movq	$0, -96(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	je	.L441
.L460:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L463
.L442:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L443
	movq	(%rdi), %rax
	call	*8(%rax)
.L443:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L444
	movq	(%rdi), %rax
	call	*8(%rax)
.L444:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r13, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L462:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L463:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L442
.L461:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24558:
	.size	_ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE.part.0, .-_ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE.part.0
	.section	.text._ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE
	.type	_ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE, @function
_ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE:
.LFB19133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	32(%rdi), %rdi
	call	_ZN2v88internal21CpuProfilesCollection14StartProfilingEPKcNS_19CpuProfilingOptionsE@PLT
	testb	%al, %al
	jne	.L467
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE.part.0
	.cfi_endproc
.LFE19133:
	.size	_ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE, .-_ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE
	.section	.text._ZN2v88internal11CpuProfiler14StartProfilingENS0_6StringENS_19CpuProfilingOptionsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler14StartProfilingENS0_6StringENS_19CpuProfilingOptionsE
	.type	_ZN2v88internal11CpuProfiler14StartProfilingENS0_6StringENS_19CpuProfilingOptionsE, @function
_ZN2v88internal11CpuProfiler14StartProfilingENS0_6StringENS_19CpuProfilingOptionsE:
.LFB19134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	32(%rdi), %rdi
	movq	%rdx, -48(%rbp)
	movl	%ecx, -40(%rbp)
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movl	-40(%rbp), %ecx
	movq	32(%r12), %rdi
	movq	-48(%rbp), %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal21CpuProfilesCollection14StartProfilingEPKcNS_19CpuProfilingOptionsE@PLT
	testb	%al, %al
	jne	.L471
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	.cfi_restore_state
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE.part.0
	.cfi_endproc
.LFE19134:
	.size	_ZN2v88internal11CpuProfiler14StartProfilingENS0_6StringENS_19CpuProfilingOptionsE, .-_ZN2v88internal11CpuProfiler14StartProfilingENS0_6StringENS_19CpuProfilingOptionsE
	.section	.text._ZN2v88internal11CpuProfiler13StopProcessorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler13StopProcessorEv
	.type	_ZN2v88internal11CpuProfiler13StopProcessorEv, @function
_ZN2v88internal11CpuProfiler13StopProcessorEv:
.LFB19139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %r12
	movb	$0, 232(%rdi)
	xchgl	72(%r12), %eax
	testl	%eax, %eax
	je	.L473
	leaq	128(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	80(%r12), %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base6Thread4JoinEv@PLT
.L473:
	movq	48(%rbx), %r12
	movq	$0, 48(%rbx)
	testq	%r12, %r12
	je	.L474
	movq	(%r12), %rax
	leaq	_ZN2v88internal23SamplingEventsProcessorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L475
	leaq	16+_ZTVN2v88internal23SamplingEventsProcessorE(%rip), %rax
	movq	524672(%r12), %rdi
	movq	%rax, (%r12)
	addq	$64, %rax
	movq	%rax, 48(%r12)
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	movq	524672(%r12), %rdi
	testq	%rdi, %rdi
	je	.L476
	movq	(%rdi), %rax
	call	*8(%rax)
.L476:
	movq	%r12, %rdi
	call	_ZN2v88internal23ProfilerEventsProcessorD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal11AlignedFreeEPv@PLT
.L474:
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jne	.L472
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L472
	movq	$0, 64(%rbx)
	movq	(%r12), %rax
	movq	8(%r12), %rsi
	movq	41016(%rax), %rdi
	call	_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE@PLT
	movq	(%r12), %rdx
	subq	$1, 41816(%rdx)
	jne	.L478
	movq	(%r12), %rax
	movb	$0, 41812(%rax)
.L478:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L474
	.cfi_endproc
.LFE19139:
	.size	_ZN2v88internal11CpuProfiler13StopProcessorEv, .-_ZN2v88internal11CpuProfiler13StopProcessorEv
	.section	.text._ZN2v88internal11CpuProfiler26StopProcessorIfLastProfileEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler26StopProcessorIfLastProfileEPKc
	.type	_ZN2v88internal11CpuProfiler26StopProcessorIfLastProfileEPKc, @function
_ZN2v88internal11CpuProfiler26StopProcessorIfLastProfileEPKc:
.LFB19138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	call	_ZN2v88internal21CpuProfilesCollection13IsLastProfileEPKc@PLT
	testb	%al, %al
	jne	.L495
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CpuProfiler13StopProcessorEv
	.cfi_endproc
.LFE19138:
	.size	_ZN2v88internal11CpuProfiler26StopProcessorIfLastProfileEPKc, .-_ZN2v88internal11CpuProfiler26StopProcessorIfLastProfileEPKc
	.section	.text._ZN2v88internal11CpuProfiler17DeleteAllProfilesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler17DeleteAllProfilesEv
	.type	_ZN2v88internal11CpuProfiler17DeleteAllProfilesEv, @function
_ZN2v88internal11CpuProfiler17DeleteAllProfilesEv:
.LFB19105:
	.cfi_startproc
	endbr64
	cmpb	$0, 232(%rdi)
	jne	.L502
	jmp	_ZN2v88internal11CpuProfiler13ResetProfilesEv
	.p2align 4,,10
	.p2align 3
.L502:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	_ZN2v88internal11CpuProfiler13StopProcessorEv
	movq	-8(%rbp), %rdi
	leave
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CpuProfiler13ResetProfilesEv
	.cfi_endproc
.LFE19105:
	.size	_ZN2v88internal11CpuProfiler17DeleteAllProfilesEv, .-_ZN2v88internal11CpuProfiler17DeleteAllProfilesEv
	.section	.text._ZN2v88internal11CpuProfiler13StopProfilingEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler13StopProfilingEPKc
	.type	_ZN2v88internal11CpuProfiler13StopProfilingEPKc, @function
_ZN2v88internal11CpuProfiler13StopProfilingEPKc:
.LFB19136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpb	$0, 232(%rdi)
	je	.L503
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal21CpuProfilesCollection13IsLastProfileEPKc@PLT
	testb	%al, %al
	jne	.L511
.L505:
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal21CpuProfilesCollection13StopProfilingEPKc@PLT
	cmpq	$0, 48(%rbx)
	movq	%rax, %r13
	je	.L503
	movq	32(%rbx), %rdi
	call	_ZNK2v88internal21CpuProfilesCollection25GetCommonSamplingIntervalEv@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L503:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L511:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal11CpuProfiler13StopProcessorEv
	jmp	.L505
	.cfi_endproc
.LFE19136:
	.size	_ZN2v88internal11CpuProfiler13StopProfilingEPKc, .-_ZN2v88internal11CpuProfiler13StopProfilingEPKc
	.section	.text._ZN2v88internal11CpuProfiler13StopProfilingENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler13StopProfilingENS0_6StringE
	.type	_ZN2v88internal11CpuProfiler13StopProfilingENS0_6StringE, @function
_ZN2v88internal11CpuProfiler13StopProfilingENS0_6StringE:
.LFB19137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	cmpb	$0, 232(%rbx)
	je	.L512
	movq	32(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal21CpuProfilesCollection13IsLastProfileEPKc@PLT
	testb	%al, %al
	jne	.L520
.L514:
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal21CpuProfilesCollection13StopProfilingEPKc@PLT
	cmpq	$0, 48(%rbx)
	movq	%rax, %r13
	je	.L512
	movq	32(%rbx), %rdi
	call	_ZNK2v88internal21CpuProfilesCollection25GetCommonSamplingIntervalEv@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L512:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal11CpuProfiler13StopProcessorEv
	jmp	.L514
	.cfi_endproc
.LFE19137:
	.size	_ZN2v88internal11CpuProfiler13StopProfilingENS0_6StringE, .-_ZN2v88internal11CpuProfiler13StopProfilingENS0_6StringE
	.section	.text._ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS5_,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS5_
	.type	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS5_, @function
_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS5_:
.LFB22560:
	.cfi_startproc
	endbr64
	movq	(%rsi), %r9
	movq	8(%rdi), %rsi
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%rsi
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L522
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L543:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L522
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L522
.L524:
	cmpq	%rdi, %r9
	jne	.L543
	movq	(%rcx), %rdi
	testq	%rdi, %rdi
	jne	.L526
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L544:
	cmpq	%r9, %r8
	jne	.L525
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L525
.L526:
	movq	8(%rdi), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rsi
	cmpq	%r10, %rdx
	je	.L544
.L525:
	movq	%rcx, %rax
	movq	%rdi, %rdx
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	xorl	%eax, %eax
	xorl	%edx, %edx
	ret
	.cfi_endproc
.LFE22560:
	.size	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS5_, .-_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS5_
	.section	.text._ZN2v88internal11CpuProfilerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfilerD2Ev
	.type	_ZN2v88internal11CpuProfilerD2Ev, @function
_ZN2v88internal11CpuProfilerD2Ev:
.LFB19122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %eax
	testb	%al, %al
	je	.L645
.L547:
	movq	(%rbx), %rax
	leaq	56+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	-64(%rbp), %rsi
	leaq	_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS5_
	movq	%rax, %rcx
	cmpq	%rdx, %rax
	je	.L549
.L551:
	movq	16(%rcx), %rax
	movq	%rcx, %rdi
	movq	(%rcx), %rcx
	cmpq	%rax, %rbx
	je	.L550
	cmpq	%rcx, %rdx
	jne	.L551
.L549:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L550:
	movq	8+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rsi
	movq	8(%rdi), %rax
	xorl	%edx, %edx
	movq	_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %r10
	divq	%rsi
	leaq	0(,%rdx,8), %r12
	movq	%rdx, %r9
	leaq	(%r10,%r12), %r11
	movq	(%r11), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%rdx, %r8
	movq	(%rdx), %rdx
	cmpq	%rdi, %rdx
	jne	.L552
	cmpq	%r8, %rax
	je	.L646
	testq	%rcx, %rcx
	je	.L555
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r9
	je	.L555
	movq	%r8, (%r10,%rdx,8)
	movq	(%rdi), %rcx
.L555:
	movq	%rcx, (%r8)
	call	_ZdlPv@PLT
	leaq	56+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	subq	$1, 24+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L557
	movq	$0, 64(%rbx)
	movq	(%r12), %rax
	movq	8(%r12), %rsi
	movq	41016(%rax), %rdi
	call	_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE@PLT
	movq	(%r12), %rdx
	subq	$1, 41816(%rdx)
	je	.L647
.L559:
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L557:
	leaq	16+_ZTVN2v88internal20ProfilerCodeObserverE(%rip), %rax
	leaq	88(%rbx), %rdi
	movq	%rax, 72(%rbx)
	call	_ZN2v88internal7CodeMapD1Ev@PLT
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L560
	movq	(%r12), %rax
	movq	8(%r12), %rsi
	movq	41016(%rax), %rdi
	call	_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE@PLT
	movq	(%r12), %rdx
	subq	$1, 41816(%rdx)
	je	.L648
.L561:
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L560:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L562
	movq	(%rdi), %rax
	call	*8(%rax)
.L562:
	movq	48(%rbx), %r12
	testq	%r12, %r12
	je	.L563
	movq	(%r12), %rax
	leaq	_ZN2v88internal23SamplingEventsProcessorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L564
	leaq	16+_ZTVN2v88internal23SamplingEventsProcessorE(%rip), %rax
	movq	524672(%r12), %rdi
	movq	%rax, (%r12)
	addq	$64, %rax
	movq	%rax, 48(%r12)
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	movq	524672(%r12), %rdi
	testq	%rdi, %rdi
	je	.L565
	movq	(%rdi), %rax
	call	*8(%rax)
.L565:
	movq	%r12, %rdi
	call	_ZN2v88internal23ProfilerEventsProcessorD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal11AlignedFreeEPv@PLT
.L563:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L566
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L566:
	movq	32(%rbx), %r13
	testq	%r13, %r13
	je	.L545
	leaq	80(%r13), %rdi
	call	_ZN2v84base9SemaphoreD1Ev@PLT
	movq	64(%r13), %rax
	movq	56(%r13), %r14
	movq	%rax, -72(%rbp)
	cmpq	%r14, %rax
	je	.L568
	.p2align 4,,10
	.p2align 3
.L574:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L569
	leaq	128(%r12), %rdi
	call	_ZN2v88internal11ProfileTreeD1Ev@PLT
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L570
	movq	120(%r12), %rax
	movq	88(%r12), %rbx
	leaq	8(%rax), %r15
	cmpq	%rbx, %r15
	jbe	.L571
	.p2align 4,,10
	.p2align 3
.L572:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r15
	ja	.L572
	movq	48(%r12), %rdi
.L571:
	call	_ZdlPv@PLT
.L570:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L573
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L573:
	movl	$272, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L569:
	addq	$8, %r14
	cmpq	%r14, -72(%rbp)
	jne	.L574
	movq	56(%r13), %r14
.L568:
	testq	%r14, %r14
	je	.L575
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L575:
	movq	32(%r13), %rax
	movq	24(%r13), %r14
	movq	%rax, -72(%rbp)
	cmpq	%r14, %rax
	je	.L576
	.p2align 4,,10
	.p2align 3
.L582:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L577
	leaq	128(%r12), %rdi
	call	_ZN2v88internal11ProfileTreeD1Ev@PLT
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L578
	movq	120(%r12), %rax
	movq	88(%r12), %rbx
	leaq	8(%rax), %r15
	cmpq	%rbx, %r15
	jbe	.L579
	.p2align 4,,10
	.p2align 3
.L580:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r15
	ja	.L580
	movq	48(%r12), %rdi
.L579:
	call	_ZdlPv@PLT
.L578:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L581
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L581:
	movl	$272, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L577:
	addq	$8, %r14
	cmpq	%r14, -72(%rbp)
	jne	.L582
	movq	24(%r13), %r14
.L576:
	testq	%r14, %r14
	je	.L583
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L583:
	movq	%r13, %rdi
	call	_ZN2v88internal14StringsStorageD1Ev@PLT
	movl	$112, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L545:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L649
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_restore_state
	movq	(%r12), %rax
	movb	$0, 41812(%rax)
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L648:
	movq	(%r12), %rax
	movb	$0, 41812(%rax)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L645:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L547
	leaq	48+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 88+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	leaq	8(%rax), %rdi
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movups	%xmm0, 56+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movups	%xmm0, 72+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$1, 8+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$0, 16+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$0, 24+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movl	$0x3f800000, 32+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$0, 40+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$0, 48+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L646:
	testq	%rcx, %rcx
	je	.L585
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r9
	je	.L555
	movq	%r8, (%r10,%rdx,8)
	movq	_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %r11
	addq	%r12, %r11
	movq	(%r11), %rax
.L554:
	leaq	16+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L650
.L556:
	movq	$0, (%r11)
	movq	(%rdi), %rcx
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L564:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L585:
	movq	%r8, %rax
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L650:
	movq	%rcx, 16+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	jmp	.L556
.L649:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19122:
	.size	_ZN2v88internal11CpuProfilerD2Ev, .-_ZN2v88internal11CpuProfilerD2Ev
	.globl	_ZN2v88internal11CpuProfilerD1Ev
	.set	_ZN2v88internal11CpuProfilerD1Ev,_ZN2v88internal11CpuProfilerD2Ev
	.section	.text._ZN2v88internal11CpuProfiler13CollectSampleEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfiler13CollectSampleEPNS0_7IsolateE
	.type	_ZN2v88internal11CpuProfiler13CollectSampleEPNS0_7IsolateE, @function
_ZN2v88internal11CpuProfiler13CollectSampleEPNS0_7IsolateE:
.LFB19131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %eax
	testb	%al, %al
	je	.L664
.L653:
	leaq	56+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	movq	%rbx, -32(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	-32(%rbp), %rsi
	leaq	_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS5_
	movq	%rdx, %r12
	movq	%rax, %rbx
	cmpq	%rdx, %rax
	je	.L655
	.p2align 4,,10
	.p2align 3
.L658:
	movq	16(%rbx), %rax
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L656
	xorl	%esi, %esi
	call	_ZN2v88internal23ProfilerEventsProcessor15AddCurrentStackEb
	movq	(%rbx), %rbx
	cmpq	%rbx, %r12
	jne	.L658
.L655:
	leaq	56+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L665
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	movq	(%rbx), %rbx
	cmpq	%rbx, %r12
	jne	.L658
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L664:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L653
	leaq	48+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 88+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	leaq	8(%rax), %rdi
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movups	%xmm0, 56+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movups	%xmm0, 72+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$1, 8+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$0, 16+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$0, 24+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movl	$0x3f800000, 32+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$0, 40+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$0, 48+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L653
.L665:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19131:
	.size	_ZN2v88internal11CpuProfiler13CollectSampleEPNS0_7IsolateE, .-_ZN2v88internal11CpuProfiler13CollectSampleEPNS0_7IsolateE
	.section	.text._ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE
	.type	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE, @function
_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE:
.LFB24120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	cmpq	$1, %rsi
	je	.L694
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rsi
	ja	.L695
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%r12), %rax
	movq	%rax, -64(%rbp)
.L668:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L671
	leaq	16(%r12), %rax
	xorl	%r14d, %r14d
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, -56(%rbp)
	xorl	%r15d, %r15d
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L696:
	movq	(%r8), %rax
	movl	%edi, %r14d
	movq	%rax, (%rsi)
	movq	%rsi, (%r8)
.L674:
	movq	%rsi, %r8
	testq	%r10, %r10
	je	.L672
.L698:
	movq	%r10, %rsi
.L670:
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	movq	%r9, %r11
	movq	(%rsi), %r10
	divq	%rbx
	testq	%r8, %r8
	setne	%dil
	cmpq	%r11, %rdx
	movq	%rdx, %rcx
	movq	%rdx, %r9
	sete	%al
	andb	%al, %dil
	jne	.L696
	testb	%r14b, %r14b
	je	.L675
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L675
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	%rbx
	cmpq	%r11, %rdx
	je	.L675
	movq	%r8, 0(%r13,%rdx,8)
.L675:
	leaq	0(%r13,%rcx,8), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L697
	movq	(%rdx), %rdx
	xorl	%r14d, %r14d
	movq	%rsi, %r8
	movq	%rdx, (%rsi)
	movq	(%rax), %rax
	movq	%rsi, (%rax)
	testq	%r10, %r10
	jne	.L698
.L672:
	testb	%dil, %dil
	je	.L671
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L671
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	%rbx
	cmpq	%rdx, %rcx
	je	.L671
	movq	%rsi, 0(%r13,%rdx,8)
.L671:
	movq	(%r12), %rdi
	cmpq	-64(%rbp), %rdi
	je	.L678
	call	_ZdlPv@PLT
.L678:
	movq	%rbx, 8(%r12)
	movq	%r13, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L697:
	.cfi_restore_state
	movq	16(%r12), %rdx
	movq	%rdx, (%rsi)
	movq	-56(%rbp), %rdx
	movq	%rsi, 16(%r12)
	movq	%rdx, (%rax)
	cmpq	$0, (%rsi)
	je	.L680
	movq	%rsi, 0(%r13,%r15,8)
	xorl	%r14d, %r14d
	movq	%rcx, %r15
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L680:
	xorl	%r14d, %r14d
	movq	%rcx, %r15
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L694:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, -64(%rbp)
	jmp	.L668
.L695:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE24120:
	.size	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE, .-_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE
	.section	.text._ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeEPNS0_21CpuProfilesCollectionEPNS0_16ProfileGeneratorEPNS0_23ProfilerEventsProcessorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeEPNS0_21CpuProfilesCollectionEPNS0_16ProfileGeneratorEPNS0_23ProfilerEventsProcessorE
	.type	_ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeEPNS0_21CpuProfilesCollectionEPNS0_16ProfileGeneratorEPNS0_23ProfilerEventsProcessorE, @function
_ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeEPNS0_21CpuProfilesCollectionEPNS0_16ProfileGeneratorEPNS0_23ProfilerEventsProcessorE:
.LFB19119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%r8, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	88(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%r9, -72(%rbp)
	movl	%ecx, -76(%rbp)
	movhps	-72(%rbp), %xmm0
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movl	%eax, 12(%rdi)
	movslq	_ZN2v88internal35FLAG_cpu_profiler_sampling_intervalE(%rip), %rax
	movq	%rsi, (%rdi)
	movq	%rax, 24(%rdi)
	movq	16(%rbp), %rax
	movl	%edx, 8(%rdi)
	movq	%rax, 48(%rdi)
	leaq	16+_ZTVN2v88internal20ProfilerCodeObserverE(%rip), %rax
	movq	%rax, 72(%rdi)
	movb	$1, 16(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	%rsi, 80(%rdi)
	movups	%xmm0, 32(%rdi)
	movq	%r14, %rdi
	call	_ZN2v88internal7CodeMapC1Ev@PLT
	movq	80(%r12), %rax
	movq	$0, 224(%r12)
	movq	40960(%rax), %r13
	leaq	50888(%r13), %rax
	leaq	23264(%r13), %r15
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L700:
	movq	(%r15), %r13
	movl	$64, %edi
	call	_Znwm@PLT
	pxor	%xmm1, %xmm1
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r13, 8(%rax)
	movq	%rax, %rdx
	movl	$1, %ecx
	addq	$24, %r15
	movl	$397579, (%rax)
	leaq	.LC2(%rip), %rax
	movq	%rax, 16(%rdx)
	movq	$0, 40(%rdx)
	movq	$0, 48(%rdx)
	movq	$0, 56(%rdx)
	movups	%xmm1, 24(%rdx)
	call	_ZN2v88internal7CodeMap7AddCodeEmPNS0_9CodeEntryEj@PLT
	cmpq	%r15, -72(%rbp)
	jne	.L700
	movq	80(%r12), %rax
	xorl	%r13d, %r13d
	leaq	-64(%rbp), %r15
	addq	$41184, %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L706:
	movq	-72(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, -64(%rbp)
	leaq	63(%rax), %rsi
	movl	43(%rax), %eax
	testl	%eax, %eax
	js	.L734
.L702:
	movq	%r14, %rdi
	call	_ZN2v88internal7CodeMap9FindEntryEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L703
	movl	%r13d, %esi
	addl	$1, %r13d
	call	_ZN2v88internal9CodeEntry12SetBuiltinIdENS0_8Builtins4NameE@PLT
	cmpl	$1553, %r13d
	jne	.L706
	.p2align 4,,10
	.p2align 3
.L704:
	movb	$0, 232(%r12)
	movq	32(%r12), %rax
	movq	%r12, 48(%rax)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %eax
	testb	%al, %al
	je	.L735
.L708:
	leaq	56+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	movq	24+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdx
	movq	8+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rsi
	movl	$1, %ecx
	movq	%rbx, 8(%rax)
	leaq	32+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	movq	%rax, %r13
	movq	$0, (%rax)
	movq	%r12, 16(%rax)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L736
.L710:
	movq	8+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rcx
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rsi
	divq	%rcx
	leaq	0(,%rdx,8), %rdi
	movq	%rdx, %rbx
	leaq	(%rsi,%rdi), %r11
	movq	(%r11), %r8
	testq	%r8, %r8
	je	.L711
	movq	(%r8), %rsi
	movq	8(%r13), %r14
	movq	8(%rsi), %rdi
	movq	%rsi, %rax
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L737:
	movq	(%rax), %r9
	testq	%r9, %r9
	je	.L713
	movq	8(%r9), %rdi
	movq	%rax, %r8
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %rbx
	jne	.L713
	movq	%r9, %rax
.L714:
	cmpq	%rdi, %r14
	jne	.L737
	movq	%rax, 0(%r13)
	movq	%r13, (%r8)
.L716:
	leaq	56+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	addq	$1, 24+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpl	$1, -76(%rbp)
	je	.L738
.L699:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L739
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L703:
	.cfi_restore_state
	addl	$1, %r13d
	cmpl	$1553, %r13d
	jne	.L706
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L734:
	movq	%r15, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rsi
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L738:
	cmpq	$0, 64(%r12)
	jne	.L699
	movq	%r12, %rdi
	call	_ZN2v88internal11CpuProfiler13EnableLoggingEv.part.0
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L736:
	movq	%rdx, %rsi
	leaq	_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_11CpuProfilerEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L735:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L708
	leaq	48+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 88+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	leaq	8(%rax), %rdi
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movups	%xmm0, 56+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movups	%xmm0, 72+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$1, 8+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$0, 16+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$0, 24+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movl	$0x3f800000, 32+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$0, 40+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	$0, 48+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L713:
	movq	%rsi, 0(%r13)
	movq	(%r11), %rax
	movq	%r13, (%rax)
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L711:
	movq	16+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rax
	movq	%r13, 16+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip)
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L718
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%r13, (%rsi,%rdx,8)
	addq	_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rdi
	movq	%rdi, %r11
.L718:
	leaq	16+_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object(%rip), %rax
	movq	%rax, (%r11)
	jmp	.L716
.L739:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19119:
	.size	_ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeEPNS0_21CpuProfilesCollectionEPNS0_16ProfileGeneratorEPNS0_23ProfilerEventsProcessorE, .-_ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeEPNS0_21CpuProfilesCollectionEPNS0_16ProfileGeneratorEPNS0_23ProfilerEventsProcessorE
	.globl	_ZN2v88internal11CpuProfilerC1EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeEPNS0_21CpuProfilesCollectionEPNS0_16ProfileGeneratorEPNS0_23ProfilerEventsProcessorE
	.set	_ZN2v88internal11CpuProfilerC1EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeEPNS0_21CpuProfilesCollectionEPNS0_16ProfileGeneratorEPNS0_23ProfilerEventsProcessorE,_ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeEPNS0_21CpuProfilesCollectionEPNS0_16ProfileGeneratorEPNS0_23ProfilerEventsProcessorE
	.section	.text._ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeE
	.type	_ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeE, @function
_ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeE:
.LFB19116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$112, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal21CpuProfilesCollectionC1EPNS0_7IsolateE@PLT
	subq	$8, %rsp
	movl	%r14d, %edx
	movq	%rbx, %r8
	pushq	$0
	movl	%r15d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	call	_ZN2v88internal11CpuProfilerC1EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeEPNS0_21CpuProfilesCollectionEPNS0_16ProfileGeneratorEPNS0_23ProfilerEventsProcessorE
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19116:
	.size	_ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeE, .-_ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeE
	.globl	_ZN2v88internal11CpuProfilerC1EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeE
	.set	_ZN2v88internal11CpuProfilerC1EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeE,_ZN2v88internal11CpuProfilerC2EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal14ProfilingScopeC2EPNS0_7IsolateEPNS0_16ProfilerListenerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal14ProfilingScopeC2EPNS0_7IsolateEPNS0_16ProfilerListenerE, @function
_GLOBAL__sub_I__ZN2v88internal14ProfilingScopeC2EPNS0_7IsolateEPNS0_16ProfilerListenerE:
.LFB24418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24418:
	.size	_GLOBAL__sub_I__ZN2v88internal14ProfilingScopeC2EPNS0_7IsolateEPNS0_16ProfilerListenerE, .-_GLOBAL__sub_I__ZN2v88internal14ProfilingScopeC2EPNS0_7IsolateEPNS0_16ProfilerListenerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal14ProfilingScopeC2EPNS0_7IsolateEPNS0_16ProfilerListenerE
	.weak	_ZTVN2v88internal10CpuSamplerE
	.section	.data.rel.ro.local._ZTVN2v88internal10CpuSamplerE,"awG",@progbits,_ZTVN2v88internal10CpuSamplerE,comdat
	.align 8
	.type	_ZTVN2v88internal10CpuSamplerE, @object
	.size	_ZTVN2v88internal10CpuSamplerE, 40
_ZTVN2v88internal10CpuSamplerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal10CpuSamplerD1Ev
	.quad	_ZN2v88internal10CpuSamplerD0Ev
	.quad	_ZN2v88internal10CpuSampler11SampleStackERKNS_13RegisterStateE
	.weak	_ZTVN2v88internal23SamplingEventsProcessorE
	.section	.data.rel.ro.local._ZTVN2v88internal23SamplingEventsProcessorE,"awG",@progbits,_ZTVN2v88internal23SamplingEventsProcessorE,comdat
	.align 8
	.type	_ZTVN2v88internal23SamplingEventsProcessorE, @object
	.size	_ZTVN2v88internal23SamplingEventsProcessorE, 104
_ZTVN2v88internal23SamplingEventsProcessorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23SamplingEventsProcessorD1Ev
	.quad	_ZN2v88internal23SamplingEventsProcessorD0Ev
	.quad	_ZN2v88internal23SamplingEventsProcessor3RunEv
	.quad	_ZN2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.quad	_ZN2v88internal23SamplingEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE
	.quad	_ZN2v88internal23SamplingEventsProcessor16ProcessOneSampleEv
	.quad	-48
	.quad	0
	.quad	_ZThn48_N2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.quad	_ZThn48_N2v88internal23SamplingEventsProcessorD1Ev
	.quad	_ZThn48_N2v88internal23SamplingEventsProcessorD0Ev
	.weak	_ZTVN2v88internal23ProfilerEventsProcessorE
	.section	.data.rel.ro._ZTVN2v88internal23ProfilerEventsProcessorE,"awG",@progbits,_ZTVN2v88internal23ProfilerEventsProcessorE,comdat
	.align 8
	.type	_ZTVN2v88internal23ProfilerEventsProcessorE, @object
	.size	_ZTVN2v88internal23ProfilerEventsProcessorE, 104
_ZTVN2v88internal23ProfilerEventsProcessorE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.quad	_ZN2v88internal23ProfilerEventsProcessor19SetSamplingIntervalENS_4base9TimeDeltaE
	.quad	__cxa_pure_virtual
	.quad	-48
	.quad	0
	.quad	_ZThn48_N2v88internal23ProfilerEventsProcessor16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.quad	0
	.quad	0
	.weak	_ZTVN2v88internal20ProfilerCodeObserverE
	.section	.data.rel.ro.local._ZTVN2v88internal20ProfilerCodeObserverE,"awG",@progbits,_ZTVN2v88internal20ProfilerCodeObserverE,comdat
	.align 8
	.type	_ZTVN2v88internal20ProfilerCodeObserverE, @object
	.size	_ZTVN2v88internal20ProfilerCodeObserverE, 40
_ZTVN2v88internal20ProfilerCodeObserverE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal20ProfilerCodeObserver16CodeEventHandlerERKNS0_19CodeEventsContainerE
	.quad	_ZN2v88internal20ProfilerCodeObserverD1Ev
	.quad	_ZN2v88internal20ProfilerCodeObserverD0Ev
	.section	.bss._ZZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsEE28trace_event_unique_atomic512,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsEE28trace_event_unique_atomic512, @object
	.size	_ZZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsEE28trace_event_unique_atomic512, 8
_ZZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsEE28trace_event_unique_atomic512:
	.zero	8
	.section	.bss._ZGVZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object, @object
	.size	_ZGVZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object, 8
_ZGVZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object,"aw",@nobits
	.align 32
	.type	_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object, @object
	.size	_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object, 96
_ZZN2v88internal12_GLOBAL__N_119GetProfilersManagerEvE6object:
	.zero	96
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
