	.file	"futex-emulation.cc"
	.text
	.section	.text._ZN2v84base16LazyInstanceImplINS_8internal13FutexWaitListENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS_8internal13FutexWaitListENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS_8internal13FutexWaitListENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS_8internal13FutexWaitListENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS_8internal13FutexWaitListENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv:
.LFB21171:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE21171:
	.size	_ZN2v84base16LazyInstanceImplINS_8internal13FutexWaitListENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS_8internal13FutexWaitListENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB21447:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE21447:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB21448:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L5
	cmpl	$3, %edx
	je	.L6
	cmpl	$1, %edx
	je	.L10
.L6:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE21448:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv:
.LFB21169:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base5MutexC1Ev@PLT
	.cfi_endproc
.LFE21169:
	.size	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.section	.text._ZN2v88internal17FutexWaitListNode10NotifyWakeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17FutexWaitListNode10NotifyWakeEv
	.type	_ZN2v88internal17FutexWaitListNode10NotifyWakeEv, @function
_ZN2v88internal17FutexWaitListNode10NotifyWakeEv:
.LFB17782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L23
.L13:
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	movb	$1, 81(%rbx)
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rax
	leaq	-64(%rbp), %r12
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L13
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L13
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17782:
	.size	_ZN2v88internal17FutexWaitListNode10NotifyWakeEv, .-_ZN2v88internal17FutexWaitListNode10NotifyWakeEv
	.section	.text._ZN2v88internal13FutexWaitListC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FutexWaitListC2Ev
	.type	_ZN2v88internal13FutexWaitListC2Ev, @function
_ZN2v88internal13FutexWaitListC2Ev:
.LFB17784:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE17784:
	.size	_ZN2v88internal13FutexWaitListC2Ev, .-_ZN2v88internal13FutexWaitListC2Ev
	.globl	_ZN2v88internal13FutexWaitListC1Ev
	.set	_ZN2v88internal13FutexWaitListC1Ev,_ZN2v88internal13FutexWaitListC2Ev
	.section	.text._ZN2v88internal13FutexWaitList7AddNodeEPNS0_17FutexWaitListNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FutexWaitList7AddNodeEPNS0_17FutexWaitListNodeE
	.type	_ZN2v88internal13FutexWaitList7AddNodeEPNS0_17FutexWaitListNodeE, @function
_ZN2v88internal13FutexWaitList7AddNodeEPNS0_17FutexWaitListNodeE:
.LFB17786:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L27
	movq	%rsi, 56(%rax)
	movq	8(%rdi), %rax
.L28:
	movq	%rax, 48(%rsi)
	movq	$0, 56(%rsi)
	movq	%rsi, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rsi, (%rdi)
	jmp	.L28
	.cfi_endproc
.LFE17786:
	.size	_ZN2v88internal13FutexWaitList7AddNodeEPNS0_17FutexWaitListNodeE, .-_ZN2v88internal13FutexWaitList7AddNodeEPNS0_17FutexWaitListNodeE
	.section	.text._ZN2v88internal13FutexWaitList10RemoveNodeEPNS0_17FutexWaitListNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FutexWaitList10RemoveNodeEPNS0_17FutexWaitListNodeE
	.type	_ZN2v88internal13FutexWaitList10RemoveNodeEPNS0_17FutexWaitListNodeE, @function
_ZN2v88internal13FutexWaitList10RemoveNodeEPNS0_17FutexWaitListNodeE:
.LFB17787:
	.cfi_startproc
	endbr64
	movq	48(%rsi), %rax
	movq	56(%rsi), %rdx
	testq	%rax, %rax
	je	.L30
	movq	%rdx, 56(%rax)
	movq	56(%rsi), %rax
	movq	48(%rsi), %rdx
	testq	%rax, %rax
	je	.L32
.L34:
	pxor	%xmm0, %xmm0
	movq	%rdx, 48(%rax)
	movups	%xmm0, 48(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rdx, (%rdi)
	movq	56(%rsi), %rax
	movq	48(%rsi), %rdx
	testq	%rax, %rax
	jne	.L34
.L32:
	pxor	%xmm0, %xmm0
	movq	%rdx, 8(%rdi)
	movups	%xmm0, 48(%rsi)
	ret
	.cfi_endproc
.LFE17787:
	.size	_ZN2v88internal13FutexWaitList10RemoveNodeEPNS0_17FutexWaitListNodeE, .-_ZN2v88internal13FutexWaitList10RemoveNodeEPNS0_17FutexWaitListNodeE
	.section	.text._ZN2v88internal21AtomicsWaitWakeHandle4WakeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21AtomicsWaitWakeHandle4WakeEv
	.type	_ZN2v88internal21AtomicsWaitWakeHandle4WakeEv, @function
_ZN2v88internal21AtomicsWaitWakeHandle4WakeEv:
.LFB17788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L54
.L36:
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movb	$1, 8(%rbx)
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%rbx), %rbx
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	leaq	45552(%rbx), %r12
	cmpb	$2, %al
	jne	.L55
.L38:
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	movb	$1, 45633(%rbx)
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rax
	leaq	-80(%rbp), %r12
	movq	%rax, -72(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L36
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rax
	leaq	-80(%rbp), %r13
	movq	%rax, -72(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L38
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L38
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17788:
	.size	_ZN2v88internal21AtomicsWaitWakeHandle4WakeEv, .-_ZN2v88internal21AtomicsWaitWakeHandle4WakeEv
	.section	.text._ZN2v88internal14FutexEmulation4WakeENS0_6HandleINS0_13JSArrayBufferEEEmj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FutexEmulation4WakeENS0_6HandleINS0_13JSArrayBufferEEEmj
	.type	_ZN2v88internal14FutexEmulation4WakeENS0_6HandleINS0_13JSArrayBufferEEEmj, @function
_ZN2v88internal14FutexEmulation4WakeENS0_6HandleINS0_13JSArrayBufferEEEmj:
.LFB17795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	31(%rax), %r13
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L91
.L58:
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movzbl	_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %eax
	cmpb	$2, %al
	jne	.L92
.L60:
	movq	8+_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rbx
	testq	%rbx, %rbx
	sete	%dl
	testl	%r12d, %r12d
	sete	%al
	orb	%al, %dl
	jne	.L71
	xorl	%r15d, %r15d
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L90:
	movq	56(%rbx), %rbx
	testl	%r12d, %r12d
	sete	%dl
	testq	%rbx, %rbx
	je	.L73
.L93:
	testb	%dl, %dl
	jne	.L73
.L68:
	cmpq	%r13, 64(%rbx)
	jne	.L90
	cmpq	%r14, 72(%rbx)
	jne	.L90
	cmpb	$0, 80(%rbx)
	je	.L90
	movb	$0, 80(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	xorl	%edx, %edx
	cmpl	$-1, %r12d
	je	.L67
	subl	$1, %r12d
	sete	%dl
.L67:
	movq	56(%rbx), %rbx
	addl	$1, %r15d
	testq	%rbx, %rbx
	jne	.L93
	.p2align 4,,10
	.p2align 3
.L73:
	salq	$32, %r15
.L62:
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal13FutexWaitListENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rax
	leaq	-96(%rbp), %r15
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r15, %rsi
	leaq	_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L60
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rax
	leaq	-96(%rbp), %r15
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r15, %rsi
	leaq	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L58
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L71:
	xorl	%r15d, %r15d
	jmp	.L62
.L94:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17795:
	.size	_ZN2v88internal14FutexEmulation4WakeENS0_6HandleINS0_13JSArrayBufferEEEmj, .-_ZN2v88internal14FutexEmulation4WakeENS0_6HandleINS0_13JSArrayBufferEEEmj
	.section	.text._ZN2v88internal14FutexEmulation20NumWaitersForTestingENS0_6HandleINS0_13JSArrayBufferEEEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FutexEmulation20NumWaitersForTestingENS0_6HandleINS0_13JSArrayBufferEEEm
	.type	_ZN2v88internal14FutexEmulation20NumWaitersForTestingENS0_6HandleINS0_13JSArrayBufferEEEm, @function
_ZN2v88internal14FutexEmulation20NumWaitersForTestingENS0_6HandleINS0_13JSArrayBufferEEEm:
.LFB17796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	31(%rax), %rbx
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L119
.L96:
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movzbl	_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %eax
	cmpb	$2, %al
	jne	.L120
.L98:
	movq	8+_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rdx
	testq	%rdx, %rdx
	je	.L104
.L123:
	xorl	%eax, %eax
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L101:
	movq	56(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L121
.L102:
	cmpq	64(%rdx), %rbx
	jne	.L101
	cmpq	72(%rdx), %r12
	jne	.L101
	cmpb	$1, 80(%rdx)
	movq	56(%rdx), %rdx
	sbbl	$-1, %eax
	testq	%rdx, %rdx
	jne	.L102
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%rax, %r12
	salq	$32, %r12
.L100:
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rax
	leaq	-80(%rbp), %r13
	movq	%rax, -72(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L96
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L120:
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal13FutexWaitListENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rax
	leaq	-80(%rbp), %r13
	movq	%rax, -72(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L98
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	movq	8+_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rdx
	testq	%rdx, %rdx
	jne	.L123
.L104:
	xorl	%r12d, %r12d
	jmp	.L100
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17796:
	.size	_ZN2v88internal14FutexEmulation20NumWaitersForTestingENS0_6HandleINS0_13JSArrayBufferEEEm, .-_ZN2v88internal14FutexEmulation20NumWaitersForTestingENS0_6HandleINS0_13JSArrayBufferEEEm
	.section	.rodata._ZN2v88internal14FutexEmulation4WaitIiEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"callback_result != AtomicsWaitEvent::kTerminatedExecution"
	.section	.rodata._ZN2v88internal14FutexEmulation4WaitIiEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d.str1.1,"aMS",@progbits,1
.LC4:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal14FutexEmulation4WaitIiEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d,"axG",@progbits,_ZN2v88internal14FutexEmulation4WaitIiEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d,comdat
	.p2align 4
	.weak	_ZN2v88internal14FutexEmulation4WaitIiEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d
	.type	_ZN2v88internal14FutexEmulation4WaitIiEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d, @function
_ZN2v88internal14FutexEmulation4WaitIiEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d:
.LFB19650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$152, %rsp
	movsd	%xmm0, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	ucomisd	.LC0(%rip), %xmm0
	ja	.L166
	movsd	.LC1(%rip), %xmm1
	movapd	%xmm0, %xmm2
	mulsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	comisd	.LC2(%rip), %xmm0
	jbe	.L225
.L166:
	movb	$0, -145(%rbp)
	xorl	%r13d, %r13d
.L125:
	movslq	%ebx, %rax
	movsd	-176(%rbp), %xmm0
	movq	%r14, %rcx
	xorl	%esi, %esi
	movq	%rax, %r8
	leaq	-112(%rbp), %r9
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	movq	%r12, -112(%rbp)
	movb	$0, -104(%rbp)
	call	_ZN2v88internal7Isolate22RunAtomicsWaitCallbackENS_7Isolate16AtomicsWaitEventENS0_6HandleINS0_13JSArrayBufferEEEmldPNS0_21AtomicsWaitWakeHandleE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L127
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
.L128:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L226
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	cvttsd2siq	%xmm0, %rcx
	movb	$1, -145(%rbp)
	movabsq	$2361183241434822607, %rdx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	sarq	$7, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %r13
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L127:
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L227
.L129:
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	(%r15), %rax
	movq	31(%rax), %rax
	movq	%r14, 45624(%r12)
	movb	$1, 45632(%r12)
	movq	%rax, 45616(%r12)
	cmpl	%ebx, (%rax,%r14)
	jne	.L228
	cmpb	$0, -145(%rbp)
	movq	$0, -184(%rbp)
	jne	.L229
.L134:
	movzbl	_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %eax
	cmpb	$2, %al
	je	.L135
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal13FutexWaitListENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rax
	leaq	-96(%rbp), %r13
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rdi
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L135
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L135:
	movq	16+_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rax
	leaq	45552(%r12), %rcx
	movq	%rcx, -168(%rbp)
	testq	%rax, %rax
	je	.L137
	movq	%rcx, 56(%rax)
.L138:
	movq	%rax, 45600(%r12)
	movq	-168(%rbp), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rdx
	movq	%rdx, %xmm7
	movq	$0, 45608(%r12)
	movq	%rax, 16+_ZN2v88internal14FutexEmulation10wait_list_E(%rip)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm7
	movaps	%xmm7, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L139:
	movzbl	45633(%r12), %ebx
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %r13
.L149:
	movb	$0, 45633(%r12)
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	je	.L140
	leaq	-96(%rbp), %r8
	movdqa	-144(%rbp), %xmm3
	movq	%r13, -88(%rbp)
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	movq	%r8, %rsi
	leaq	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movq	%rax, -96(%rbp)
	movq	%r8, -160(%rbp)
	movaps	%xmm3, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L140
	movq	-160(%rbp), %r8
	movl	$3, %edx
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	*%rax
.L140:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testb	%bl, %bl
	je	.L142
	leaq	37512(%r12), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	%rax, %rbx
	cmpq	%rax, 312(%r12)
	je	.L230
.L142:
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	je	.L147
	movdqa	-144(%rbp), %xmm4
	leaq	-96(%rbp), %rbx
	movq	%r13, -88(%rbp)
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	movq	%rbx, %rsi
	leaq	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movq	%rax, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L147
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L147:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movzbl	45633(%r12), %ebx
	testb	%bl, %bl
	jne	.L149
	cmpb	$0, -104(%rbp)
	je	.L150
	movb	$0, 45632(%r12)
	xorl	%ebx, %ebx
	movl	$4, %r13d
.L146:
	movzbl	_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %eax
	cmpb	$2, %al
	je	.L158
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal13FutexWaitListENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %r8
	leaq	8+_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r8, %rsi
	leaq	_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rdi
	movq	%rax, %xmm5
	movq	%r8, -144(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L158
	movq	-144(%rbp), %r8
	movl	$3, %edx
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	*%rax
.L158:
	movq	45600(%r12), %rax
	movq	45608(%r12), %rdx
	testq	%rax, %rax
	je	.L160
	movq	%rdx, 56(%rax)
	movq	45608(%r12), %rdx
	movq	45600(%r12), %rax
.L161:
	testq	%rdx, %rdx
	je	.L162
	movq	%rax, 48(%rdx)
.L163:
	movb	$0, 45632(%r12)
	pxor	%xmm0, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movups	%xmm0, 45600(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	movq	%r15, %rdx
	movsd	-176(%rbp), %xmm0
	movq	-192(%rbp), %r8
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate22RunAtomicsWaitCallbackENS_7Isolate16AtomicsWaitEventENS0_6HandleINS0_13JSArrayBufferEEEmldPNS0_21AtomicsWaitWakeHandleE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L133
	cmpl	$3, %r13d
	je	.L231
.L132:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %rbx
.L133:
	movq	%rbx, %rax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L228:
	movb	$0, 45632(%r12)
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movabsq	$4294967296, %rbx
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	movq	%r15, %rdx
	movsd	-176(%rbp), %xmm0
	movq	-192(%rbp), %r8
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate22RunAtomicsWaitCallbackENS_7Isolate16AtomicsWaitEventENS0_6HandleINS0_13JSArrayBufferEEEmldPNS0_21AtomicsWaitWakeHandleE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L132
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L227:
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %r8
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r8, %rsi
	leaq	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movq	%rax, %xmm7
	movq	%r8, -144(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L129
	movq	-144(%rbp), %r8
	movl	$3, %edx
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	*%rax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L150:
	cmpb	$0, 45632(%r12)
	je	.L168
	cmpb	$0, -145(%rbp)
	je	.L151
	call	_ZN2v84base9TimeTicks3NowEv@PLT
	cmpq	-184(%rbp), %rax
	jge	.L232
	movq	-184(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -120(%rbp)
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	je	.L153
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	movdqa	-144(%rbp), %xmm6
	leaq	-96(%rbp), %r13
	movq	%rax, -96(%rbp)
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rax
	movq	%r13, %rsi
	leaq	-8(%rax), %rdi
	movq	%rax, -88(%rbp)
	movaps	%xmm6, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L153
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L153:
	movq	-168(%rbp), %rdi
	leaq	-120(%rbp), %rdx
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rsi
	call	_ZN2v84base17ConditionVariable7WaitForEPNS0_5MutexERKNS0_9TimeDeltaE@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L151:
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	je	.L156
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	movdqa	-144(%rbp), %xmm5
	leaq	-96(%rbp), %r13
	movq	%rax, -96(%rbp)
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rax
	movq	%r13, %rsi
	leaq	-8(%rax), %rdi
	movq	%rax, -88(%rbp)
	movaps	%xmm5, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L156
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L156:
	movq	-168(%rbp), %rdi
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rsi
	call	_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L230:
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L233
.L144:
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movl	$3, %r13d
	call	_ZN2v84base5Mutex4LockEv@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L229:
	call	_ZN2v84base9TimeTicks3NowEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v84base4bits20SignedSaturatedAdd64Ell@PLT
	movq	%rax, -184(%rbp)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%rcx, 8+_ZN2v88internal14FutexEmulation10wait_list_E(%rip)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L162:
	movq	%rax, 16+_ZN2v88internal14FutexEmulation10wait_list_E(%rip)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%rdx, 8+_ZN2v88internal14FutexEmulation10wait_list_E(%rip)
	jmp	.L161
.L233:
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rax
	leaq	-96(%rbp), %r13
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L144
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L232:
	movabsq	$8589934592, %rbx
	movl	$2, %r13d
	jmp	.L146
.L231:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L226:
	call	__stack_chk_fail@PLT
.L168:
	xorl	%ebx, %ebx
	movl	$1, %r13d
	jmp	.L146
	.cfi_endproc
.LFE19650:
	.size	_ZN2v88internal14FutexEmulation4WaitIiEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d, .-_ZN2v88internal14FutexEmulation4WaitIiEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d
	.section	.text._ZN2v88internal14FutexEmulation6Wait32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FutexEmulation6Wait32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid
	.type	_ZN2v88internal14FutexEmulation6Wait32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid, @function
_ZN2v88internal14FutexEmulation6Wait32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid:
.LFB17792:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal14FutexEmulation4WaitIiEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d
	.cfi_endproc
.LFE17792:
	.size	_ZN2v88internal14FutexEmulation6Wait32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid, .-_ZN2v88internal14FutexEmulation6Wait32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid
	.section	.rodata._ZN2v88internal14FutexEmulation8WaitJs32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid.str1.1,"aMS",@progbits,1
.LC5:
	.string	"unreachable code"
	.section	.text._ZN2v88internal14FutexEmulation8WaitJs32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FutexEmulation8WaitJs32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid
	.type	_ZN2v88internal14FutexEmulation8WaitJs32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid, @function
_ZN2v88internal14FutexEmulation8WaitJs32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid:
.LFB17790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal14FutexEmulation4WaitIiEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d
	testb	$1, %al
	jne	.L236
	sarq	$32, %rax
	cmpq	$1, %rax
	je	.L237
	cmpl	$2, %eax
	je	.L238
	testl	%eax, %eax
	je	.L244
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L244:
	movq	3032(%rbx), %rax
.L236:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	movq	3440(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movq	2952(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17790:
	.size	_ZN2v88internal14FutexEmulation8WaitJs32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid, .-_ZN2v88internal14FutexEmulation8WaitJs32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid
	.section	.text._ZN2v88internal14FutexEmulation4WaitIlEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d,"axG",@progbits,_ZN2v88internal14FutexEmulation4WaitIlEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d,comdat
	.p2align 4
	.weak	_ZN2v88internal14FutexEmulation4WaitIlEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d
	.type	_ZN2v88internal14FutexEmulation4WaitIlEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d, @function
_ZN2v88internal14FutexEmulation4WaitIlEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d:
.LFB19651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -168(%rbp)
	movsd	%xmm0, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	ucomisd	.LC0(%rip), %xmm0
	ja	.L287
	movsd	.LC1(%rip), %xmm1
	movapd	%xmm0, %xmm2
	mulsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	comisd	.LC2(%rip), %xmm0
	jbe	.L346
.L287:
	movb	$0, -145(%rbp)
	xorl	%r13d, %r13d
	movq	%rcx, %r8
.L246:
	movsd	-184(%rbp), %xmm0
	movq	%r14, %rcx
	movq	%r15, %rdx
	xorl	%esi, %esi
	leaq	-112(%rbp), %r9
	movq	%r12, %rdi
	movq	%r12, -112(%rbp)
	movb	$0, -104(%rbp)
	call	_ZN2v88internal7Isolate22RunAtomicsWaitCallbackENS_7Isolate16AtomicsWaitEventENS0_6HandleINS0_13JSArrayBufferEEEmldPNS0_21AtomicsWaitWakeHandleE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L248
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
.L249:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L347
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	cvttsd2siq	%xmm0, %rcx
	movb	$1, -145(%rbp)
	movabsq	$2361183241434822607, %rdx
	movq	-168(%rbp), %r8
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	sarq	$7, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %r13
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L248:
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L348
.L250:
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	(%r15), %rax
	movq	-168(%rbp), %rbx
	movq	31(%rax), %rax
	movq	%r14, 45624(%r12)
	movb	$1, 45632(%r12)
	movq	%rax, 45616(%r12)
	cmpq	%rbx, (%rax,%r14)
	jne	.L349
	cmpb	$0, -145(%rbp)
	movq	$0, -192(%rbp)
	jne	.L350
.L255:
	movzbl	_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %eax
	cmpb	$2, %al
	je	.L256
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal13FutexWaitListENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rax
	leaq	-96(%rbp), %r13
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rdi
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L256
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L256:
	movq	16+_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rax
	leaq	45552(%r12), %rcx
	movq	%rcx, -176(%rbp)
	testq	%rax, %rax
	je	.L258
	movq	%rcx, 56(%rax)
.L259:
	movq	%rax, 45600(%r12)
	movq	-176(%rbp), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rdx
	movq	%rdx, %xmm7
	movq	$0, 45608(%r12)
	movq	%rax, 16+_ZN2v88internal14FutexEmulation10wait_list_E(%rip)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm7
	movaps	%xmm7, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L260:
	movzbl	45633(%r12), %ebx
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %r13
.L270:
	movb	$0, 45633(%r12)
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	je	.L261
	leaq	-96(%rbp), %r8
	movdqa	-144(%rbp), %xmm3
	movq	%r13, -88(%rbp)
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	movq	%r8, %rsi
	leaq	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movq	%rax, -96(%rbp)
	movq	%r8, -160(%rbp)
	movaps	%xmm3, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L261
	movq	-160(%rbp), %r8
	movl	$3, %edx
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	*%rax
.L261:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testb	%bl, %bl
	je	.L263
	leaq	37512(%r12), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	%rax, %rbx
	cmpq	%rax, 312(%r12)
	je	.L351
.L263:
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	je	.L268
	movdqa	-144(%rbp), %xmm4
	leaq	-96(%rbp), %rbx
	movq	%r13, -88(%rbp)
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	movq	%rbx, %rsi
	leaq	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movq	%rax, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L268
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L268:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movzbl	45633(%r12), %ebx
	testb	%bl, %bl
	jne	.L270
	cmpb	$0, -104(%rbp)
	je	.L271
	movb	$0, 45632(%r12)
	xorl	%ebx, %ebx
	movl	$4, %r13d
.L267:
	movzbl	_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %eax
	cmpb	$2, %al
	je	.L279
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal13FutexWaitListENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %r8
	leaq	8+_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r8, %rsi
	leaq	_ZN2v88internal14FutexEmulation10wait_list_E(%rip), %rdi
	movq	%rax, %xmm5
	movq	%r8, -144(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L279
	movq	-144(%rbp), %r8
	movl	$3, %edx
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	*%rax
.L279:
	movq	45600(%r12), %rax
	movq	45608(%r12), %rdx
	testq	%rax, %rax
	je	.L281
	movq	%rdx, 56(%rax)
	movq	45608(%r12), %rdx
	movq	45600(%r12), %rax
.L282:
	testq	%rdx, %rdx
	je	.L283
	movq	%rax, 48(%rdx)
.L284:
	movb	$0, 45632(%r12)
	pxor	%xmm0, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movups	%xmm0, 45600(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	movq	%r15, %rdx
	movsd	-184(%rbp), %xmm0
	movq	-168(%rbp), %r8
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate22RunAtomicsWaitCallbackENS_7Isolate16AtomicsWaitEventENS0_6HandleINS0_13JSArrayBufferEEEmldPNS0_21AtomicsWaitWakeHandleE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L254
	cmpl	$3, %r13d
	je	.L352
.L253:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %rbx
.L254:
	movq	%rbx, %rax
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L349:
	movb	$0, 45632(%r12)
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%rbx, %r8
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	movsd	-184(%rbp), %xmm0
	movq	%r15, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movabsq	$4294967296, %rbx
	call	_ZN2v88internal7Isolate22RunAtomicsWaitCallbackENS_7Isolate16AtomicsWaitEventENS0_6HandleINS0_13JSArrayBufferEEEmldPNS0_21AtomicsWaitWakeHandleE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L253
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rax
	leaq	-96(%rbp), %rbx
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rbx, %rsi
	leaq	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L250
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L271:
	cmpb	$0, 45632(%r12)
	je	.L289
	cmpb	$0, -145(%rbp)
	je	.L272
	call	_ZN2v84base9TimeTicks3NowEv@PLT
	cmpq	-192(%rbp), %rax
	jge	.L353
	movq	-192(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -120(%rbp)
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	je	.L274
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	movdqa	-144(%rbp), %xmm6
	leaq	-96(%rbp), %r13
	movq	%rax, -96(%rbp)
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rax
	movq	%r13, %rsi
	leaq	-8(%rax), %rdi
	movq	%rax, -88(%rbp)
	movaps	%xmm6, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L274
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L274:
	movq	-176(%rbp), %rdi
	leaq	-120(%rbp), %rdx
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rsi
	call	_ZN2v84base17ConditionVariable7WaitForEPNS0_5MutexERKNS0_9TimeDeltaE@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L272:
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	je	.L277
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	movdqa	-144(%rbp), %xmm5
	leaq	-96(%rbp), %r13
	movq	%rax, -96(%rbp)
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rax
	movq	%r13, %rsi
	leaq	-8(%rax), %rdi
	movq	%rax, -88(%rbp)
	movaps	%xmm5, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L277
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L277:
	movq	-176(%rbp), %rdi
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rsi
	call	_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L351:
	movzbl	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L354
.L265:
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movl	$3, %r13d
	call	_ZN2v84base5Mutex4LockEv@PLT
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L350:
	call	_ZN2v84base9TimeTicks3NowEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v84base4bits20SignedSaturatedAdd64Ell@PLT
	movq	%rax, -192(%rbp)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L258:
	movq	%rcx, 8+_ZN2v88internal14FutexEmulation10wait_list_E(%rip)
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%rax, 16+_ZN2v88internal14FutexEmulation10wait_list_E(%rip)
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%rdx, 8+_ZN2v88internal14FutexEmulation10wait_list_E(%rip)
	jmp	.L282
.L354:
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rax
	leaq	-96(%rbp), %r13
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v88internal14FutexEmulation6mutex_E(%rip), %rdi
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L265
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L353:
	movabsq	$8589934592, %rbx
	movl	$2, %r13d
	jmp	.L267
.L352:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L347:
	call	__stack_chk_fail@PLT
.L289:
	xorl	%ebx, %ebx
	movl	$1, %r13d
	jmp	.L267
	.cfi_endproc
.LFE19651:
	.size	_ZN2v88internal14FutexEmulation4WaitIlEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d, .-_ZN2v88internal14FutexEmulation4WaitIlEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d
	.section	.text._ZN2v88internal14FutexEmulation6Wait64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FutexEmulation6Wait64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld
	.type	_ZN2v88internal14FutexEmulation6Wait64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld, @function
_ZN2v88internal14FutexEmulation6Wait64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld:
.LFB17793:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal14FutexEmulation4WaitIlEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d
	.cfi_endproc
.LFE17793:
	.size	_ZN2v88internal14FutexEmulation6Wait64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld, .-_ZN2v88internal14FutexEmulation6Wait64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld
	.section	.text._ZN2v88internal14FutexEmulation8WaitJs64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FutexEmulation8WaitJs64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld
	.type	_ZN2v88internal14FutexEmulation8WaitJs64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld, @function
_ZN2v88internal14FutexEmulation8WaitJs64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld:
.LFB17791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal14FutexEmulation4WaitIlEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmT_d
	testb	$1, %al
	jne	.L357
	sarq	$32, %rax
	cmpq	$1, %rax
	je	.L358
	cmpl	$2, %eax
	je	.L359
	testl	%eax, %eax
	je	.L365
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L365:
	movq	3032(%rbx), %rax
.L357:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	movq	3440(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	movq	2952(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17791:
	.size	_ZN2v88internal14FutexEmulation8WaitJs64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld, .-_ZN2v88internal14FutexEmulation8WaitJs64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal14FutexEmulation6mutex_E,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal14FutexEmulation6mutex_E, @function
_GLOBAL__sub_I__ZN2v88internal14FutexEmulation6mutex_E:
.LFB21513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21513:
	.size	_GLOBAL__sub_I__ZN2v88internal14FutexEmulation6mutex_E, .-_GLOBAL__sub_I__ZN2v88internal14FutexEmulation6mutex_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal14FutexEmulation6mutex_E
	.globl	_ZN2v88internal14FutexEmulation10wait_list_E
	.section	.bss._ZN2v88internal14FutexEmulation10wait_list_E,"aw",@nobits
	.align 16
	.type	_ZN2v88internal14FutexEmulation10wait_list_E, @object
	.size	_ZN2v88internal14FutexEmulation10wait_list_E, 24
_ZN2v88internal14FutexEmulation10wait_list_E:
	.zero	24
	.globl	_ZN2v88internal14FutexEmulation6mutex_E
	.section	.bss._ZN2v88internal14FutexEmulation6mutex_E,"aw",@nobits
	.align 32
	.type	_ZN2v88internal14FutexEmulation6mutex_E, @object
	.size	_ZN2v88internal14FutexEmulation6mutex_E, 48
_ZN2v88internal14FutexEmulation6mutex_E:
	.zero	48
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	4294967295
	.long	2146435071
	.align 8
.LC1:
	.long	0
	.long	1083129856
	.align 8
.LC2:
	.long	0
	.long	1138753536
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
