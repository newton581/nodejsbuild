	.file	"compilation-statistics.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1338:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1338:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.rodata._ZN2v88internalL9WriteLineERSobPKcRKNS0_21CompilationStatistics10BasicStatsES7_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"\"%s_time\"=%.3f\n\"%s_space\"=%zu"
	.section	.rodata._ZN2v88internalL9WriteLineERSobPKcRKNS0_21CompilationStatistics10BasicStatsES7_.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"%34s %10.3f (%5.1f%%)  %10zu (%5.1f%%) %10zu %10zu"
	.section	.rodata._ZN2v88internalL9WriteLineERSobPKcRKNS0_21CompilationStatistics10BasicStatsES7_.str1.1
.LC3:
	.string	"   "
	.section	.text._ZN2v88internalL9WriteLineERSobPKcRKNS0_21CompilationStatistics10BasicStatsES7_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL9WriteLineERSobPKcRKNS0_21CompilationStatistics10BasicStatsES7_, @function
_ZN2v88internalL9WriteLineERSobPKcRKNS0_21CompilationStatistics10BasicStatsES7_:
.LFB4136:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movq	8(%rbx), %r8
	testb	%r15b, %r15b
	je	.L4
	movq	%r8, %r9
	leaq	-192(%rbp), %r13
	movq	%r14, %r8
	movq	%r14, %rcx
	leaq	.LC0(%rip), %rdx
	movl	$128, %esi
	movq	%r13, %rdi
	movl	$1, %eax
	call	_ZN2v84base2OS8SNPrintFEPciPKcz@PLT
	movq	%r13, %rdx
.L5:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L5
	movl	%eax, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ebx
	addb	%al, %bl
	sbbq	$3, %rdx
	subq	%r13, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L3:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	leaq	(%r8,%r8,4), %rax
	movq	16(%rbx), %r9
	leaq	(%rax,%rax,4), %rax
	salq	$2, %rax
	js	.L8
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	8(%r13), %rax
	testq	%rax, %rax
	js	.L10
.L29:
	pxor	%xmm3, %xmm3
	cvtsi2sdq	%rax, %xmm3
.L11:
	pxor	%xmm1, %xmm1
	pxor	%xmm4, %xmm4
	subq	$8, %rsp
	movq	%r14, %rcx
	cvtsi2sdq	0(%r13), %xmm4
	divsd	%xmm3, %xmm2
	leaq	-192(%rbp), %r13
	movl	$128, %esi
	cvtsi2sdq	(%rbx), %xmm1
	pushq	24(%rbx)
	movq	%r13, %rdi
	movl	$3, %eax
	leaq	.LC2(%rip), %rdx
	divsd	%xmm4, %xmm1
	mulsd	.LC1(%rip), %xmm1
	call	_ZN2v84base2OS8SNPrintFEPciPKcz@PLT
	movq	%r13, %rdx
.L12:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L12
	movl	%eax, %ecx
	movq	%r12, %rdi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %esi
	addb	%al, %sil
	movq	%r13, %rsi
	sbbq	$3, %rdx
	subq	%r13, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	$0, 40(%rbx)
	popq	%rax
	popq	%rdx
	jne	.L27
.L14:
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L28
	cmpb	$0, 56(%r13)
	je	.L17
	movsbl	67(%r13), %esi
.L18:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	shrq	%rax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	8(%r13), %rax
	addsd	%xmm2, %xmm2
	testq	%rax, %rax
	jns	.L29
.L10:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm3, %xmm3
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm3
	addsd	%xmm3, %xmm3
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L18
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%rbx), %r13
	testq	%r13, %r13
	je	.L30
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L30:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L14
.L26:
	call	__stack_chk_fail@PLT
.L28:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE4136:
	.size	_ZN2v88internalL9WriteLineERSobPKcRKNS0_21CompilationStatistics10BasicStatsES7_, .-_ZN2v88internalL9WriteLineERSobPKcRKNS0_21CompilationStatistics10BasicStatsES7_
	.section	.text._ZN2v88internal21CompilationStatistics16RecordTotalStatsEmRKNS1_10BasicStatsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21CompilationStatistics16RecordTotalStatsEmRKNS1_10BasicStatsE
	.type	_ZN2v88internal21CompilationStatistics16RecordTotalStatsEmRKNS1_10BasicStatsE, @function
_ZN2v88internal21CompilationStatistics16RecordTotalStatsEmRKNS1_10BasicStatsE:
.LFB4134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	168(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	(%r12), %rax
	addq	%rax, (%rbx)
	movq	8(%r12), %rax
	addq	%rax, 8(%rbx)
	movq	24(%r12), %rax
	cmpq	24(%rbx), %rax
	ja	.L34
.L32:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	%rax, 24(%rbx)
	movq	16(%r12), %rax
	leaq	32(%r12), %rsi
	leaq	32(%rbx), %rdi
	movq	%rax, 16(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L32
	.cfi_endproc
.LFE4134:
	.size	_ZN2v88internal21CompilationStatistics16RecordTotalStatsEmRKNS1_10BasicStatsE, .-_ZN2v88internal21CompilationStatistics16RecordTotalStatsEmRKNS1_10BasicStatsE
	.section	.text._ZN2v88internal21CompilationStatistics10BasicStats10AccumulateERKS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21CompilationStatistics10BasicStats10AccumulateERKS2_
	.type	_ZN2v88internal21CompilationStatistics10BasicStats10AccumulateERKS2_, @function
_ZN2v88internal21CompilationStatistics10BasicStats10AccumulateERKS2_:
.LFB4135:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	addq	%rax, (%rdi)
	movq	8(%rsi), %rax
	addq	%rax, 8(%rdi)
	movq	24(%rsi), %rax
	cmpq	24(%rdi), %rax
	ja	.L37
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rax, 24(%rdi)
	movq	16(%rsi), %rax
	addq	$32, %rdi
	addq	$32, %rsi
	movq	%rax, -16(%rdi)
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	.cfi_endproc
.LFE4135:
	.size	_ZN2v88internal21CompilationStatistics10BasicStats10AccumulateERKS2_, .-_ZN2v88internal21CompilationStatistics10BasicStats10AccumulateERKS2_
	.section	.rodata._ZN2v88internallsERSoRKNS0_21AsPrintableStatisticsE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"cannot create std::vector larger than max_size()"
	.align 8
.LC5:
	.string	"----------------------------------------------------------------------------------------------------------------------\n"
	.align 8
.LC6:
	.string	"                Turbofan phase            Time (ms)    "
	.align 8
.LC7:
	.string	"                   Space (bytes)             Function\n"
	.align 8
.LC8:
	.string	"                                                       "
	.align 8
.LC9:
	.string	"          Total          Max.     Abs. max.\n"
	.align 8
.LC10:
	.string	"                                   -----------------------------------------------------------------------------------\n"
	.section	.rodata._ZN2v88internallsERSoRKNS0_21AsPrintableStatisticsE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"totals"
	.section	.text._ZN2v88internallsERSoRKNS0_21AsPrintableStatisticsE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKNS0_21AsPrintableStatisticsE
	.type	_ZN2v88internallsERSoRKNS0_21AsPrintableStatisticsE, @function
_ZN2v88internallsERSoRKNS0_21AsPrintableStatisticsE:
.LFB4140:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r13
	movq	%rsi, -56(%rbp)
	movq	112(%r13), %rbx
	cmpq	%rax, %rbx
	ja	.L45
	movq	%rdi, %r12
	testq	%rbx, %rbx
	je	.L72
	leaq	0(,%rbx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%rax, -88(%rbp)
	cmpq	$1, %rbx
	je	.L41
	movq	%rbx, %rdx
	pxor	%xmm0, %xmm0
	shrq	%rdx
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L42:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L42
	movq	-88(%rbp), %rax
	movq	%rbx, %rdx
	andq	$-2, %rdx
	leaq	(%rax,%rdx,8), %rax
	cmpq	%rdx, %rbx
	je	.L43
.L41:
	movq	$0, (%rax)
.L43:
	addq	-88(%rbp), %r15
	movq	%r15, -64(%rbp)
.L40:
	movq	96(%r13), %rdi
	leaq	80(%r13), %rbx
	movq	-88(%rbp), %r14
	cmpq	%rbx, %rdi
	je	.L47
	.p2align 4,,10
	.p2align 3
.L44:
	movq	128(%rdi), %rax
	movq	%rdi, (%r14,%rax,8)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdi
	cmpq	%rbx, %rax
	jne	.L44
.L47:
	movq	160(%r13), %rbx
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rbx
	ja	.L45
	testq	%rbx, %rbx
	je	.L74
	leaq	0(,%rbx,8), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	cmpq	$1, %rbx
	movq	-72(%rbp), %rcx
	movq	%rax, -80(%rbp)
	je	.L49
	movq	%rbx, %rdx
	pxor	%xmm0, %xmm0
	shrq	%rdx
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L50:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L50
	movq	-80(%rbp), %rax
	movq	%rbx, %rdx
	andq	$-2, %rdx
	leaq	(%rax,%rdx,8), %rax
	cmpq	%rdx, %rbx
	je	.L51
.L49:
	movq	$0, (%rax)
.L51:
	addq	-80(%rbp), %rcx
	movq	%rcx, -96(%rbp)
.L48:
	movq	144(%r13), %rdi
	leaq	128(%r13), %rbx
	movq	-80(%rbp), %r14
	cmpq	%rbx, %rdi
	je	.L55
	.p2align 4,,10
	.p2align 3
.L52:
	movq	128(%rdi), %rax
	movq	%rdi, (%r14,%rax,8)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdi
	cmpq	%rbx, %rax
	jne	.L52
.L55:
	movq	-56(%rbp), %rax
	movzbl	8(%rax), %esi
	testb	%sil, %sil
	je	.L102
	movq	-88(%rbp), %rbx
	cmpq	%rbx, -64(%rbp)
	je	.L68
.L71:
	movq	-88(%rbp), %rbx
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L105:
	movsbl	67(%rdi), %esi
.L65:
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-56(%rbp), %rax
	movzbl	8(%rax), %esi
	cmpq	%rbx, -64(%rbp)
	je	.L56
.L67:
	movq	(%rbx), %r14
	movq	%r14, %rcx
	testb	%sil, %sil
	je	.L103
.L57:
	movq	32(%r14), %rdx
	movq	%r12, %rdi
	addq	$64, %rcx
	movq	%r13, %r8
	call	_ZN2v88internalL9WriteLineERSobPKcRKNS0_21CompilationStatistics10BasicStatsES7_
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %rdi
	testq	%rdi, %rdi
	je	.L104
	cmpb	$0, 56(%rdi)
	jne	.L105
	movq	%rdi, -72(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-72(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L65
	call	*%rax
	movsbl	%al, %esi
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L103:
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rdx
	cmpq	%rdx, %rax
	je	.L62
	movq	%rbx, -104(%rbp)
	movq	-96(%rbp), %r15
	movq	%r14, %rbx
	movq	%rax, %r14
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L59:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L106
.L61:
	movq	(%r14), %rcx
	movq	144(%rcx), %rdx
	cmpq	40(%rbx), %rdx
	jne	.L59
	testq	%rdx, %rdx
	je	.L60
	movq	136(%rcx), %rdi
	movq	32(%rbx), %rsi
	movq	%rcx, -72(%rbp)
	call	memcmp@PLT
	movq	-72(%rbp), %rcx
	testl	%eax, %eax
	jne	.L59
.L60:
	movq	-56(%rbp), %rax
	movq	32(%rcx), %rdx
	leaq	64(%rcx), %r11
	movq	%r13, %r8
	movq	%r11, %rcx
	movq	%r12, %rdi
	addq	$8, %r14
	movzbl	8(%rax), %esi
	call	_ZN2v88internalL9WriteLineERSobPKcRKNS0_21CompilationStatistics10BasicStatsES7_
	cmpq	%r14, %r15
	jne	.L61
.L106:
	movq	%rbx, %r14
	movq	-104(%rbp), %rbx
.L62:
	leaq	.LC10(%rip), %rsi
	movl	$119, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	movq	(%rbx), %rcx
	movzbl	8(%rax), %esi
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L102:
	movl	$119, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$55, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$54, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$55, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$44, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC5(%rip), %rsi
	movl	$119, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	movq	-88(%rbp), %rbx
	movzbl	8(%rax), %esi
	cmpq	%rbx, -64(%rbp)
	jne	.L71
	.p2align 4,,10
	.p2align 3
.L56:
	testb	%sil, %sil
	jne	.L68
	leaq	.LC5(%rip), %rsi
	movl	$119, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	movzbl	8(%rax), %esi
.L68:
	movq	%r13, %r8
	movq	%r13, %rcx
	leaq	.LC11(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internalL9WriteLineERSobPKcRKNS0_21CompilationStatistics10BasicStatsES7_
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L69
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L69:
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L81
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L81:
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	$0, -80(%rbp)
	movq	$0, -96(%rbp)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L72:
	movq	$0, -88(%rbp)
	movq	$0, -64(%rbp)
	jmp	.L40
.L104:
	call	_ZSt16__throw_bad_castv@PLT
.L45:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4140:
	.size	_ZN2v88internallsERSoRKNS0_21AsPrintableStatisticsE, .-_ZN2v88internallsERSoRKNS0_21AsPrintableStatisticsE
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_:
.LFB4708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$168, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	%rax, %r12
	addq	$48, %rax
	movq	%rax, -88(%rbp)
	movq	%rax, 32(%r12)
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdx
	je	.L155
	movq	%rdx, 32(%r12)
	movq	16(%rbx), %rdx
	movq	%rdx, 48(%r12)
.L109:
	movq	8(%rbx), %r14
	movdqu	40(%rbx), %xmm0
	movq	%rax, (%rbx)
	movq	32(%rbx), %rax
	movq	64(%rbx), %rdx
	movq	$0, 8(%rbx)
	movq	%r14, 40(%r12)
	movq	%rax, 64(%r12)
	movq	56(%rbx), %rax
	movb	$0, 16(%rbx)
	movq	%rax, 88(%r12)
	leaq	112(%r12), %rax
	movq	%rax, -96(%rbp)
	movq	%rax, 96(%r12)
	leaq	80(%rbx), %rax
	movups	%xmm0, 72(%r12)
	cmpq	%rax, %rdx
	je	.L156
	movq	%rdx, 96(%r12)
	movq	80(%rbx), %rdx
	movq	%rdx, 112(%r12)
.L111:
	movq	72(%rbx), %rdx
	movq	%rax, 64(%rbx)
	movq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	%rdx, 104(%r12)
	movq	104(%rbx), %rdx
	movq	%rax, 128(%r12)
	leaq	152(%r12), %rax
	movq	%rax, -104(%rbp)
	movq	%rax, 136(%r12)
	leaq	120(%rbx), %rax
	movb	$0, 80(%rbx)
	cmpq	%rax, %rdx
	je	.L157
	movq	%rdx, 136(%r12)
	movq	120(%rbx), %rdx
	movq	%rdx, 152(%r12)
.L113:
	movq	112(%rbx), %rdx
	movq	%rax, 104(%rbx)
	leaq	8(%r13), %rax
	movq	$0, 112(%rbx)
	movb	$0, 120(%rbx)
	movq	16(%r13), %rbx
	movq	%rdx, 144(%r12)
	movq	%rax, -56(%rbp)
	testq	%rbx, %rbx
	je	.L114
	movq	32(%r12), %r15
	movq	%r12, -72(%rbp)
	movq	%r13, -80(%rbp)
	movq	%r15, %r12
	movq	%r14, %r15
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L120:
	movq	16(%rbx), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L116
.L158:
	movq	%rax, %rbx
.L115:
	movq	40(%rbx), %r14
	movq	32(%rbx), %r13
	cmpq	%r14, %r15
	movq	%r14, %rdx
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L117
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, -64(%rbp)
	call	memcmp@PLT
	movq	-64(%rbp), %rdx
	testl	%eax, %eax
	jne	.L118
.L117:
	movq	%r15, %rax
	movl	$2147483648, %ecx
	subq	%r14, %rax
	cmpq	%rcx, %rax
	jge	.L119
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L120
.L118:
	testl	%eax, %eax
	js	.L120
.L119:
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L158
.L116:
	movq	%r14, %rcx
	movq	%r13, %r11
	movq	%r15, %r14
	movq	-80(%rbp), %r13
	movq	%r12, %r15
	movq	%rbx, %r9
	movq	-72(%rbp), %r12
	testb	%sil, %sil
	jne	.L159
.L123:
	testq	%rdx, %rdx
	je	.L124
	movq	%r15, %rsi
	movq	%r11, %rdi
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	memcmp@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	testl	%eax, %eax
	jne	.L125
.L124:
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L126
	cmpq	$-2147483648, %rcx
	jl	.L127
	movl	%ecx, %eax
.L125:
	testl	%eax, %eax
	js	.L127
.L126:
	movq	136(%r12), %rdi
	cmpq	%rdi, -104(%rbp)
	je	.L132
	call	_ZdlPv@PLT
.L132:
	movq	96(%r12), %rdi
	cmpq	%rdi, -96(%rbp)
	je	.L133
	call	_ZdlPv@PLT
.L133:
	movq	32(%r12), %rdi
	cmpq	%rdi, -88(%rbp)
	je	.L134
	call	_ZdlPv@PLT
.L134:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$72, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	testq	%r9, %r9
	je	.L160
.L135:
	movl	$1, %edi
	cmpq	%r9, -56(%rbp)
	jne	.L161
.L128:
	movq	-56(%rbp), %rcx
	movq	%r9, %rdx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r13)
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	cmpq	%rbx, 24(%r13)
	je	.L162
.L136:
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %r9
	movq	40(%rax), %rcx
	movq	32(%rax), %r11
	movq	%rax, %rbx
	cmpq	%rcx, %r14
	movq	%rcx, %rdx
	cmovbe	%r14, %rdx
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L157:
	movdqu	120(%rbx), %xmm3
	movups	%xmm3, 152(%r12)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L156:
	movdqu	80(%rbx), %xmm2
	movups	%xmm2, 112(%r12)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L155:
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 48(%r12)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L161:
	movq	40(%r9), %rbx
	cmpq	%rbx, %r14
	movq	%rbx, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L129
	movq	32(%r9), %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcmp@PLT
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %edi
	jne	.L130
.L129:
	movq	%r14, %r8
	xorl	%edi, %edi
	subq	%rbx, %r8
	cmpq	$2147483647, %r8
	jg	.L128
	cmpq	$-2147483648, %r8
	jl	.L154
	movl	%r8d, %edi
.L130:
	shrl	$31, %edi
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L114:
	leaq	8(%r13), %rax
	cmpq	24(%r13), %rax
	jne	.L163
	leaq	8(%r13), %r9
.L154:
	movl	$1, %edi
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L162:
	movq	%rbx, %r9
	jmp	.L135
.L163:
	movq	32(%r12), %r15
	movq	%rax, %rbx
	jmp	.L136
.L160:
	xorl	%ebx, %ebx
	jmp	.L126
	.cfi_endproc
.LFE4708:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	.section	.rodata._ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE
	.type	_ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE, @function
_ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE:
.LFB4112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$360, %rsp
	movq	%rsi, -376(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	168(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	-320(%rbp), %rax
	movq	%rax, -360(%rbp)
	movq	%rax, -336(%rbp)
	testq	%r15, %r15
	je	.L185
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -344(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L238
	cmpq	$1, %rax
	jne	.L168
	movzbl	(%r15), %edx
	movb	%dl, -320(%rbp)
	movq	-360(%rbp), %rdx
.L169:
	movq	%rax, -328(%rbp)
	leaq	128(%r12), %rcx
	movb	$0, (%rdx,%rax)
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L170
	movq	-328(%rbp), %r14
	movq	-336(%rbp), %r9
	movq	%r12, -392(%rbp)
	movq	%rbx, -400(%rbp)
	movq	%rcx, %rbx
	movq	%rcx, -384(%rbp)
	movq	%r9, %r15
	movq	%r14, %r12
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L176:
	movq	24(%r13), %r13
	testq	%r13, %r13
	je	.L172
.L171:
	movq	40(%r13), %r14
	movq	%r12, %rdx
	cmpq	%r12, %r14
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L173
	movq	32(%r13), %rdi
	movq	%r15, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L174
.L173:
	movq	%r14, %rax
	movl	$2147483648, %ecx
	subq	%r12, %rax
	cmpq	%rcx, %rax
	jge	.L175
	movabsq	$-2147483649, %rsi
	cmpq	%rsi, %rax
	jle	.L176
.L174:
	testl	%eax, %eax
	js	.L176
.L175:
	movq	%r13, %rbx
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L171
.L172:
	movq	-384(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r12, %r14
	movq	-400(%rbp), %rbx
	movq	-392(%rbp), %r12
	cmpq	%r8, %rcx
	je	.L170
	movq	40(%r8), %r13
	cmpq	%r13, %r14
	movq	%r13, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L178
	movq	32(%r8), %rsi
	movq	%r15, %rdi
	movq	%r8, -384(%rbp)
	call	memcmp@PLT
	movq	-384(%rbp), %r8
	testl	%eax, %eax
	jne	.L179
.L178:
	subq	%r13, %r14
	cmpq	$2147483647, %r14
	jg	.L180
	cmpq	$-2147483648, %r14
	jl	.L170
	movl	%r14d, %eax
.L179:
	testl	%eax, %eax
	jns	.L180
.L170:
	movq	160(%r12), %rax
	pxor	%xmm0, %xmm0
	leaq	-256(%rbp), %r14
	leaq	-216(%rbp), %r13
	cmpq	$0, -376(%rbp)
	movq	%r14, -272(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -280(%rbp)
	movq	$0, -264(%rbp)
	movb	$0, -256(%rbp)
	movq	%rax, -240(%rbp)
	movq	%r13, -232(%rbp)
	movups	%xmm0, -296(%rbp)
	je	.L185
	movq	-376(%rbp), %rdi
	call	strlen@PLT
	movq	%rax, -344(%rbp)
	movq	%rax, %r15
	cmpq	$15, %rax
	ja	.L239
	cmpq	$1, %rax
	jne	.L183
	movq	-376(%rbp), %rcx
	movzbl	(%rcx), %edx
	movb	%dl, -216(%rbp)
	movq	%r13, %rdx
.L184:
	movq	%rax, -224(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-336(%rbp), %r8
	leaq	-176(%rbp), %rax
	movq	-328(%rbp), %r15
	movq	%rax, -376(%rbp)
	movq	%rax, -192(%rbp)
	movq	%r8, %rax
	addq	%r15, %rax
	je	.L215
	testq	%r8, %r8
	je	.L185
.L215:
	movq	%r15, -344(%rbp)
	cmpq	$15, %r15
	ja	.L240
	cmpq	$1, %r15
	jne	.L189
	movzbl	(%r8), %eax
	movb	%al, -176(%rbp)
	movq	-376(%rbp), %rax
.L190:
	movq	%r15, -184(%rbp)
	movb	$0, (%rax,%r15)
	movq	-304(%rbp), %rax
	leaq	-112(%rbp), %r15
	movq	-272(%rbp), %r9
	movq	-264(%rbp), %r8
	movq	%r15, -128(%rbp)
	movq	%rax, -160(%rbp)
	movq	-280(%rbp), %rax
	movdqu	-296(%rbp), %xmm1
	movq	%rax, -136(%rbp)
	movq	%r9, %rax
	addq	%r8, %rax
	movups	%xmm1, -152(%rbp)
	je	.L216
	testq	%r9, %r9
	je	.L185
.L216:
	movq	%r8, -344(%rbp)
	cmpq	$15, %r8
	ja	.L241
	cmpq	$1, %r8
	jne	.L194
	movzbl	(%r9), %eax
	movb	%al, -112(%rbp)
	movq	%r15, %rax
.L195:
	movq	%r8, -120(%rbp)
	leaq	-72(%rbp), %rcx
	movb	$0, (%rax,%r8)
	movq	-240(%rbp), %rax
	movq	-232(%rbp), %r9
	movq	-224(%rbp), %r8
	movq	%rcx, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%r9, %rax
	addq	%r8, %rax
	je	.L217
	testq	%r9, %r9
	je	.L185
.L217:
	movq	%r8, -344(%rbp)
	cmpq	$15, %r8
	ja	.L242
	cmpq	$1, %r8
	jne	.L199
	movzbl	(%r9), %eax
	movb	%al, -72(%rbp)
	movq	%rcx, %rax
.L200:
	movq	%r8, -80(%rbp)
	leaq	120(%r12), %rdi
	leaq	-192(%rbp), %rsi
	movb	$0, (%rax,%r8)
	movq	%rcx, -384(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	movq	-88(%rbp), %rdi
	movq	-384(%rbp), %rcx
	movq	%rax, %r8
	cmpq	%rcx, %rdi
	je	.L201
	movq	%rax, -384(%rbp)
	call	_ZdlPv@PLT
	movq	-384(%rbp), %r8
.L201:
	movq	-128(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L202
	movq	%r8, -384(%rbp)
	call	_ZdlPv@PLT
	movq	-384(%rbp), %r8
.L202:
	movq	-192(%rbp), %rdi
	cmpq	-376(%rbp), %rdi
	je	.L203
	movq	%r8, -376(%rbp)
	call	_ZdlPv@PLT
	movq	-376(%rbp), %r8
.L203:
	movq	-232(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L204
	movq	%r8, -376(%rbp)
	call	_ZdlPv@PLT
	movq	-376(%rbp), %r8
.L204:
	movq	-272(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L180
	movq	%r8, -376(%rbp)
	call	_ZdlPv@PLT
	movq	-376(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L180:
	movq	(%rbx), %rax
	addq	%rax, 64(%r8)
	movq	8(%rbx), %rax
	addq	%rax, 72(%r8)
	movq	24(%rbx), %rax
	cmpq	88(%r8), %rax
	ja	.L243
.L206:
	movq	-336(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L207
	call	_ZdlPv@PLT
.L207:
	movq	-368(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L244
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L245
	movq	-360(%rbp), %rdx
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	.LC12(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L238:
	leaq	-336(%rbp), %r13
	leaq	-344(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -336(%rbp)
	movq	%rax, %rdi
	movq	-344(%rbp), %rax
	movq	%rax, -320(%rbp)
.L167:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-344(%rbp), %rax
	movq	-336(%rbp), %rdx
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%rax, 88(%r8)
	movq	16(%rbx), %rax
	leaq	32(%rbx), %rsi
	leaq	96(%r8), %rdi
	movq	%rax, 80(%r8)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L239:
	leaq	-232(%rbp), %rdi
	leaq	-344(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -232(%rbp)
	movq	%rax, %rdi
	movq	-344(%rbp), %rax
	movq	%rax, -216(%rbp)
.L182:
	movq	-376(%rbp), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	-344(%rbp), %rax
	movq	-232(%rbp), %rdx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L183:
	testq	%rax, %rax
	jne	.L246
	movq	%r13, %rdx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L199:
	testq	%r8, %r8
	jne	.L247
	movq	%rcx, %rax
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L194:
	testq	%r8, %r8
	jne	.L248
	movq	%r15, %rax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L189:
	testq	%r15, %r15
	jne	.L249
	movq	-376(%rbp), %rax
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L240:
	leaq	-192(%rbp), %rdi
	leaq	-344(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -384(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-384(%rbp), %r8
	movq	%rax, -192(%rbp)
	movq	%rax, %rdi
	movq	-344(%rbp), %rax
	movq	%rax, -176(%rbp)
.L188:
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-344(%rbp), %r15
	movq	-192(%rbp), %rax
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L242:
	leaq	-88(%rbp), %rdi
	leaq	-344(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -400(%rbp)
	movq	%r8, -392(%rbp)
	movq	%r9, -384(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-384(%rbp), %r9
	movq	-392(%rbp), %r8
	movq	%rax, -88(%rbp)
	movq	%rax, %rdi
	movq	-344(%rbp), %rax
	movq	-400(%rbp), %rcx
	movq	%rax, -72(%rbp)
.L198:
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%rcx, -384(%rbp)
	call	memcpy@PLT
	movq	-344(%rbp), %r8
	movq	-88(%rbp), %rax
	movq	-384(%rbp), %rcx
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L241:
	leaq	-128(%rbp), %rdi
	leaq	-344(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -392(%rbp)
	movq	%r9, -384(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-384(%rbp), %r9
	movq	-392(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-344(%rbp), %rax
	movq	%rax, -112(%rbp)
.L193:
	movq	%r8, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-344(%rbp), %r8
	movq	-128(%rbp), %rax
	jmp	.L195
.L244:
	call	__stack_chk_fail@PLT
.L249:
	movq	-376(%rbp), %rdi
	jmp	.L188
.L248:
	movq	%r15, %rdi
	jmp	.L193
.L247:
	movq	%rcx, %rdi
	jmp	.L198
.L246:
	movq	%r13, %rdi
	jmp	.L182
.L245:
	movq	-360(%rbp), %rdi
	jmp	.L167
	.cfi_endproc
.LFE4112:
	.size	_ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE, .-_ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_:
.LFB4728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$136, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	%rax, %r12
	addq	$48, %rax
	movq	%rax, -88(%rbp)
	movq	%rax, 32(%r12)
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdx
	je	.L295
	movq	%rdx, 32(%r12)
	movq	16(%rbx), %rdx
	movq	%rdx, 48(%r12)
.L252:
	movq	8(%rbx), %r15
	movdqu	40(%rbx), %xmm0
	movq	%rax, (%rbx)
	movq	32(%rbx), %rax
	movq	64(%rbx), %rdx
	movq	$0, 8(%rbx)
	movq	%r15, 40(%r12)
	movq	%rax, 64(%r12)
	movq	56(%rbx), %rax
	movb	$0, 16(%rbx)
	movq	%rax, 88(%r12)
	leaq	112(%r12), %rax
	movq	%rax, -96(%rbp)
	movq	%rax, 96(%r12)
	leaq	80(%rbx), %rax
	movups	%xmm0, 72(%r12)
	cmpq	%rax, %rdx
	je	.L296
	movq	%rdx, 96(%r12)
	movq	80(%rbx), %rdx
	movq	%rdx, 112(%r12)
.L254:
	movq	%rax, 64(%rbx)
	movq	96(%rbx), %rax
	movq	72(%rbx), %rdx
	movb	$0, 80(%rbx)
	movq	$0, 72(%rbx)
	movq	16(%r13), %rbx
	movq	%rax, 128(%r12)
	leaq	8(%r13), %rax
	movq	%rdx, 104(%r12)
	movq	%rax, -56(%rbp)
	testq	%rbx, %rbx
	je	.L255
	movq	32(%r12), %r14
	movq	%r12, -72(%rbp)
	movq	%r13, -80(%rbp)
	movq	%r14, %r12
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L261:
	movq	16(%rbx), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L257
.L297:
	movq	%rax, %rbx
.L256:
	movq	40(%rbx), %r14
	movq	32(%rbx), %r13
	cmpq	%r14, %r15
	movq	%r14, %rdx
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L258
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, -64(%rbp)
	call	memcmp@PLT
	movq	-64(%rbp), %rdx
	testl	%eax, %eax
	jne	.L259
.L258:
	movq	%r15, %rax
	movl	$2147483648, %ecx
	subq	%r14, %rax
	cmpq	%rcx, %rax
	jge	.L260
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L261
.L259:
	testl	%eax, %eax
	js	.L261
.L260:
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L297
.L257:
	movq	%r14, %rcx
	movq	%r13, %r11
	movq	%r12, %r14
	movq	-80(%rbp), %r13
	movq	-72(%rbp), %r12
	movq	%rbx, %r10
	testb	%sil, %sil
	jne	.L298
.L264:
	testq	%rdx, %rdx
	je	.L265
	movq	%r14, %rsi
	movq	%r11, %rdi
	movq	%r10, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	memcmp@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	jne	.L266
.L265:
	subq	%r15, %rcx
	cmpq	$2147483647, %rcx
	jg	.L267
	cmpq	$-2147483648, %rcx
	jl	.L268
	movl	%ecx, %eax
.L266:
	testl	%eax, %eax
	js	.L268
.L267:
	movq	96(%r12), %rdi
	cmpq	%rdi, -96(%rbp)
	je	.L273
	call	_ZdlPv@PLT
	movq	32(%r12), %r14
.L273:
	cmpq	%r14, -88(%rbp)
	je	.L274
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L274:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$56, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	testq	%r10, %r10
	je	.L299
	movl	$1, %edi
	cmpq	%r10, -56(%rbp)
	jne	.L300
.L269:
	movq	-56(%rbp), %rcx
	movq	%r10, %rdx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r13)
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	cmpq	24(%r13), %rbx
	je	.L301
.L275:
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %r10
	movq	40(%rax), %rcx
	movq	32(%rax), %r11
	movq	%rax, %rbx
	cmpq	%rcx, %r15
	movq	%rcx, %rdx
	cmovbe	%r15, %rdx
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L296:
	movdqu	80(%rbx), %xmm2
	movups	%xmm2, 112(%r12)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L295:
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 48(%r12)
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L301:
	movq	%rbx, %r10
	movl	$1, %edi
	cmpq	%r10, -56(%rbp)
	je	.L269
.L300:
	movq	40(%r10), %rbx
	cmpq	%rbx, %r15
	movq	%rbx, %rdx
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L270
	movq	32(%r10), %rsi
	movq	%r14, %rdi
	movq	%r10, -64(%rbp)
	call	memcmp@PLT
	movq	-64(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %edi
	jne	.L271
.L270:
	movq	%r15, %r8
	xorl	%edi, %edi
	subq	%rbx, %r8
	cmpq	$2147483647, %r8
	jg	.L269
	cmpq	$-2147483648, %r8
	jl	.L294
	movl	%r8d, %edi
.L271:
	shrl	$31, %edi
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	8(%r13), %rax
	cmpq	24(%r13), %rax
	je	.L281
	movq	32(%r12), %r14
	movq	%rax, %rbx
	jmp	.L275
.L281:
	leaq	8(%r13), %r10
.L294:
	movl	$1, %edi
	jmp	.L269
.L299:
	xorl	%ebx, %ebx
	jmp	.L267
	.cfi_endproc
.LFE4728:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	.section	.text._ZN2v88internal21CompilationStatistics20RecordPhaseKindStatsEPKcRKNS1_10BasicStatsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21CompilationStatistics20RecordPhaseKindStatsEPKcRKNS1_10BasicStatsE
	.type	_ZN2v88internal21CompilationStatistics20RecordPhaseKindStatsEPKcRKNS1_10BasicStatsE, @function
_ZN2v88internal21CompilationStatistics20RecordPhaseKindStatsEPKcRKNS1_10BasicStatsE:
.LFB4126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	168(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	-256(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	%rax, -272(%rbp)
	testq	%r13, %r13
	je	.L324
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, -280(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L356
	cmpq	$1, %rax
	jne	.L306
	movzbl	0(%r13), %edx
	movb	%dl, -256(%rbp)
	movq	-296(%rbp), %rdx
.L307:
	movq	%rax, -264(%rbp)
	leaq	80(%r12), %r11
	movb	$0, (%rdx,%rax)
	movq	88(%r12), %r15
	movq	-264(%rbp), %r8
	movq	-272(%rbp), %r10
	testq	%r15, %r15
	je	.L316
	movq	%r14, -304(%rbp)
	movq	%r15, %r13
	movq	%r10, %r14
	movq	%r12, -320(%rbp)
	movq	%r8, %r12
	movq	%rbx, -328(%rbp)
	movq	%r11, %rbx
	movq	%r11, -312(%rbp)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L314:
	movq	24(%r13), %r13
	testq	%r13, %r13
	je	.L310
.L309:
	movq	40(%r13), %r15
	movq	%r12, %rdx
	cmpq	%r12, %r15
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L311
	movq	32(%r13), %rdi
	movq	%r14, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L312
.L311:
	movq	%r15, %rax
	movl	$2147483648, %ecx
	subq	%r12, %rax
	cmpq	%rcx, %rax
	jge	.L313
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L314
.L312:
	testl	%eax, %eax
	js	.L314
.L313:
	movq	%r13, %rbx
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L309
.L310:
	movq	-312(%rbp), %r11
	movq	%rbx, %r13
	movq	%r12, %r8
	movq	%r14, %r10
	movq	-320(%rbp), %r12
	movq	-304(%rbp), %r14
	movq	-328(%rbp), %rbx
	cmpq	%r13, %r11
	je	.L316
	movq	40(%r13), %r15
	cmpq	%r15, %r8
	movq	%r15, %rdx
	cmovbe	%r8, %rdx
	testq	%rdx, %rdx
	je	.L317
	movq	32(%r13), %rsi
	movq	%r10, %rdi
	movq	%r8, -312(%rbp)
	movq	%r10, -304(%rbp)
	call	memcmp@PLT
	movq	-304(%rbp), %r10
	movq	-312(%rbp), %r8
	testl	%eax, %eax
	jne	.L318
.L317:
	movq	%r8, %rax
	subq	%r15, %rax
	cmpq	$2147483647, %rax
	jg	.L319
	cmpq	$-2147483648, %rax
	jl	.L316
.L318:
	testl	%eax, %eax
	jns	.L319
.L316:
	movq	112(%r12), %rax
	leaq	-192(%rbp), %rcx
	pxor	%xmm0, %xmm0
	leaq	-144(%rbp), %r15
	movq	$0, -240(%rbp)
	movq	%rax, -176(%rbp)
	movq	%r10, %rax
	addq	%r8, %rax
	movq	%rcx, -304(%rbp)
	movq	$0, -216(%rbp)
	movq	%rcx, -208(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -192(%rbp)
	movq	%r15, -160(%rbp)
	movups	%xmm0, -232(%rbp)
	je	.L336
	testq	%r10, %r10
	je	.L324
.L336:
	movq	%r8, -280(%rbp)
	cmpq	$15, %r8
	ja	.L357
	cmpq	$1, %r8
	jne	.L322
	movzbl	(%r10), %eax
	movb	%al, -144(%rbp)
	movq	%r15, %rax
.L323:
	movq	%r8, -152(%rbp)
	leaq	-80(%rbp), %r9
	movb	$0, (%rax,%r8)
	movq	-240(%rbp), %rax
	movq	-208(%rbp), %r10
	movq	-200(%rbp), %r13
	movq	%r9, -96(%rbp)
	movq	%rax, -128(%rbp)
	movq	-216(%rbp), %rax
	movdqu	-232(%rbp), %xmm1
	movq	%rax, -104(%rbp)
	movq	%r10, %rax
	addq	%r13, %rax
	movups	%xmm1, -120(%rbp)
	je	.L341
	testq	%r10, %r10
	je	.L324
.L341:
	movq	%r13, -280(%rbp)
	cmpq	$15, %r13
	ja	.L358
	cmpq	$1, %r13
	jne	.L328
	movzbl	(%r10), %eax
	movb	%al, -80(%rbp)
	movq	%r9, %rax
.L329:
	movq	%r13, -88(%rbp)
	leaq	72(%r12), %rdi
	leaq	-160(%rbp), %rsi
	movb	$0, (%rax,%r13)
	movq	-176(%rbp), %rax
	movq	%r9, -312(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJS6_IS5_SB_EEEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	movq	-96(%rbp), %rdi
	movq	-312(%rbp), %r9
	movq	%rax, %r13
	cmpq	%r9, %rdi
	je	.L330
	call	_ZdlPv@PLT
.L330:
	movq	-160(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L331
	call	_ZdlPv@PLT
.L331:
	movq	-208(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L319
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L319:
	movq	(%rbx), %rax
	addq	%rax, 64(%r13)
	movq	8(%rbx), %rax
	addq	%rax, 72(%r13)
	movq	24(%rbx), %rax
	cmpq	88(%r13), %rax
	ja	.L359
.L333:
	movq	-272(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L334
	call	_ZdlPv@PLT
.L334:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L360
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L361
	movq	-296(%rbp), %rdx
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L356:
	leaq	-272(%rbp), %r15
	leaq	-280(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -304(%rbp)
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-304(%rbp), %r8
	movq	%rax, -272(%rbp)
	movq	%rax, %rdi
	movq	-280(%rbp), %rax
	movq	%rax, -256(%rbp)
.L305:
	movq	%r8, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-280(%rbp), %rax
	movq	-272(%rbp), %rdx
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L359:
	movq	%rax, 88(%r13)
	movq	16(%rbx), %rax
	leaq	32(%rbx), %rsi
	leaq	96(%r13), %rdi
	movq	%rax, 80(%r13)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L324:
	leaq	.LC12(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L322:
	testq	%r8, %r8
	jne	.L362
	movq	%r15, %rax
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L328:
	testq	%r13, %r13
	jne	.L363
	movq	%r9, %rax
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L358:
	leaq	-96(%rbp), %rdi
	leaq	-280(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -320(%rbp)
	movq	%r10, -312(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-312(%rbp), %r10
	movq	-320(%rbp), %r9
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-280(%rbp), %rax
	movq	%rax, -80(%rbp)
.L327:
	movq	%r13, %rdx
	movq	%r10, %rsi
	movq	%r9, -312(%rbp)
	call	memcpy@PLT
	movq	-280(%rbp), %r13
	movq	-96(%rbp), %rax
	movq	-312(%rbp), %r9
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L357:
	leaq	-160(%rbp), %rdi
	leaq	-280(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r10, -320(%rbp)
	movq	%r8, -312(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-312(%rbp), %r8
	movq	-320(%rbp), %r10
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	movq	-280(%rbp), %rax
	movq	%rax, -144(%rbp)
.L321:
	movq	%r8, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-280(%rbp), %r8
	movq	-160(%rbp), %rax
	jmp	.L323
.L360:
	call	__stack_chk_fail@PLT
.L363:
	movq	%r9, %rdi
	jmp	.L327
.L362:
	movq	%r15, %rdi
	jmp	.L321
.L361:
	movq	-296(%rbp), %rdi
	jmp	.L305
	.cfi_endproc
.LFE4126:
	.size	_ZN2v88internal21CompilationStatistics20RecordPhaseKindStatsEPKcRKNS1_10BasicStatsE, .-_ZN2v88internal21CompilationStatistics20RecordPhaseKindStatsEPKcRKNS1_10BasicStatsE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE, @function
_GLOBAL__sub_I__ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE:
.LFB5165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5165:
	.size	_GLOBAL__sub_I__ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE, .-_GLOBAL__sub_I__ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1079574528
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
