	.file	"wasm-code-manager.cc"
	.text
	.section	.text._ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,"axG",@progbits,_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.type	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, @function
_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv:
.LFB8081:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8081:
	.size	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, .-_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.section	.text._ZNK2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorage7GetCodeENS1_12WireBytesRefE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorage7GetCodeENS1_12WireBytesRefE, @function
_ZNK2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorage7GetCodeENS1_12WireBytesRefE:
.LFB20847:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	8(%rdi), %rcx
	movl	%esi, %eax
	shrq	$32, %rdx
	addl	%esi, %edx
	subq	%rax, %rdx
	addq	(%rcx), %rax
	ret
	.cfi_endproc
.LFE20847:
	.size	_ZNK2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorage7GetCodeENS1_12WireBytesRefE, .-_ZNK2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorage7GetCodeENS1_12WireBytesRefE
	.section	.text._ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB27032:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27032:
	.size	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB27081:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27081:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB27088:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27088:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB27096:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27096:
	.size	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB27085:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE27085:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB27092:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE27092:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN2v88internal6Logger27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal6Logger27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.type	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, @function
_ZN2v88internal6Logger27is_listening_to_code_eventsEv:
.LFB20251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L10
	cmpq	$0, 80(%rbx)
	setne	%al
.L10:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20251:
	.size	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, .-_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB27034:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27034:
	.size	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB27095:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27095:
	.size	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB27083:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27083:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB27090:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27090:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB27091:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L17
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	ret
	.cfi_endproc
.LFE27091:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB27049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE27049:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB27086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L21
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L21:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27086:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB27093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L25
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L25:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27093:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB27639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27639:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB27050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27050:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB27640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE27640:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB27084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %r12
	movq	%rax, 16(%rdi)
	testq	%r12, %r12
	je	.L35
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L38
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L44
.L35:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L35
.L44:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L41
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L42:
	cmpl	$1, %eax
	jne	.L35
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L42
	.cfi_endproc
.LFE27084:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD2Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD2Ev:
.LFB27036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L45
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L48
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L54
.L45:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L45
.L54:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L51
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L52:
	cmpl	$1, %eax
	jne	.L45
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L52
	.cfi_endproc
.LFE27036:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD2Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD2Ev
	.set	_ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD1Ev,_ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD2Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD0Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD0Ev:
.LFB27038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L57
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L58
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L64
.L57:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L57
.L64:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L61
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L62:
	cmpl	$1, %eax
	jne	.L57
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L61:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L62
	.cfi_endproc
.LFE27038:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD0Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD0Ev
	.section	.text._ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE
	.type	_ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE, @function
_ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE:
.LFB20561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %r12
	cmpq	%r12, %rdi
	jne	.L68
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L78:
	movq	(%r12), %r12
	cmpq	%r13, %r12
	je	.L66
.L68:
	movq	16(%r12), %rcx
	movq	24(%r12), %rax
	leaq	(%rcx,%rax), %rsi
	cmpq	%rbx, %rsi
	jb	.L78
	leaq	(%r14,%rbx), %rdx
	cmpq	%rcx, %rdx
	je	.L79
	jb	.L80
	addq	%r14, %rax
	movq	(%r12), %r14
	movq	%rax, 24(%r12)
	cmpq	%r14, %r13
	je	.L71
	addq	%rax, %rcx
	cmpq	%rcx, 16(%r14)
	je	.L81
.L71:
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	addq	%rax, %r14
	movq	%rbx, 16(%r12)
	movq	%rbx, %rax
	movq	%r14, 24(%r12)
	addq	$16, %rsp
	movq	%r14, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	%r14, %xmm1
	movq	%rbx, %xmm0
	movl	$32, %edi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_Znwm@PLT
	movdqa	-48(%rbp), %xmm0
	movq	%r12, %rsi
	movq	%rax, %rdi
	movups	%xmm0, 16(%rax)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 16(%r13)
	movq	%rbx, %rax
	movq	%r14, %rdx
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	%r14, %xmm2
	movq	%rbx, %xmm0
	movl	$32, %edi
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_Znwm@PLT
	movdqa	-48(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%rax, %rdi
	movups	%xmm0, 16(%rax)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 16(%r13)
	movq	%rbx, %rax
	movq	%r14, %rdx
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	addq	24(%r14), %rax
	movq	%r14, %rdi
	movq	%rax, 24(%r12)
	subq	$1, 16(%r13)
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L71
	.cfi_endproc
.LFE20561:
	.size	_ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE, .-_ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE
	.section	.text._ZN2v88internal4wasm22DisjointAllocationPool8AllocateEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm22DisjointAllocationPool8AllocateEm
	.type	_ZN2v88internal4wasm22DisjointAllocationPool8AllocateEm, @function
_ZN2v88internal4wasm22DisjointAllocationPool8AllocateEm:
.LFB20562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	cmpq	%rdi, %r12
	je	.L83
	movq	%rsi, %rdx
.L85:
	movq	16(%r12), %rax
	movq	24(%r12), %rsi
	movq	%rax, %rcx
	addq	%rsi, %rcx
	cmovc	%rax, %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rdx
	ja	.L91
	cmpq	%rsi, %rdx
	je	.L92
	leaq	(%rdx,%rax), %rcx
	subq	%rdx, %rsi
	movq	%rcx, 16(%r12)
	movq	%rsi, 24(%r12)
.L88:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	(%r12), %r12
	cmpq	%r12, %rdi
	jne	.L85
.L83:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L92:
	subq	$1, 16(%rdi)
	movq	%r12, %rdi
	movq	%rdx, -32(%rbp)
	movq	%rax, -24(%rbp)
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rdx
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20562:
	.size	_ZN2v88internal4wasm22DisjointAllocationPool8AllocateEm, .-_ZN2v88internal4wasm22DisjointAllocationPool8AllocateEm
	.section	.text._ZN2v88internal4wasm22DisjointAllocationPool16AllocateInRegionEmNS_4base13AddressRegionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm22DisjointAllocationPool16AllocateInRegionEmNS_4base13AddressRegionE
	.type	_ZN2v88internal4wasm22DisjointAllocationPool16AllocateInRegionEmNS_4base13AddressRegionE, @function
_ZN2v88internal4wasm22DisjointAllocationPool16AllocateInRegionEmNS_4base13AddressRegionE:
.LFB20563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	cmpq	%r13, %rdi
	je	.L94
	movq	%rdi, %r15
	movq	%rsi, %rbx
	addq	%rdx, %rcx
.L96:
	movq	16(%r13), %r14
	movq	24(%r13), %rsi
	movq	%rdx, %r12
	cmpq	%rdx, %r14
	leaq	(%r14,%rsi), %rdi
	cmovnb	%r14, %r12
	movq	%rdi, %rax
	cmpq	%rdi, %rcx
	cmovbe	%rcx, %rax
	cmpq	%r12, %rax
	cmovb	%r12, %rax
	subq	%r12, %rax
	cmpq	%rax, %rbx
	ja	.L104
	cmpq	%rsi, %rbx
	je	.L105
	cmpq	%rdx, %r14
	jnb	.L106
	leaq	(%rbx,%r12), %rdx
	cmpq	%rdi, %rdx
	jne	.L100
	subq	%rbx, %rsi
	movq	%rsi, 24(%r13)
.L98:
	movq	%r12, %rax
	movq	%rbx, %rdx
.L101:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	addq	%rbx, %r14
	subq	%rbx, %rsi
	movq	%r14, 16(%r13)
	movq	%rsi, 24(%r13)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L104:
	movq	0(%r13), %r13
	cmpq	%r15, %r13
	jne	.L96
.L94:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%r12, %rax
	movl	$32, %edi
	movq	%rdx, -64(%rbp)
	subq	%r14, %rax
	movq	%rax, -56(%rbp)
	call	_Znwm@PLT
	movq	%r14, %xmm0
	movq	%r13, %rsi
	movhps	-56(%rbp), %xmm0
	movq	%rax, %rdi
	movups	%xmm0, 16(%rax)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	movq	-64(%rbp), %rdx
	addq	$1, 16(%r15)
	movq	24(%r13), %rax
	addq	16(%r13), %rax
	subq	%rdx, %rax
	movq	%rdx, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L105:
	subq	$1, 16(%r15)
	movq	%r13, %rdi
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L98
	.cfi_endproc
.LFE20563:
	.size	_ZN2v88internal4wasm22DisjointAllocationPool16AllocateInRegionEmNS_4base13AddressRegionE, .-_ZN2v88internal4wasm22DisjointAllocationPool16AllocateInRegionEmNS_4base13AddressRegionE
	.section	.text._ZNK2v88internal4wasm8WasmCode13constant_poolEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm8WasmCode13constant_poolEv
	.type	_ZNK2v88internal4wasm8WasmCode13constant_poolEv, @function
_ZNK2v88internal4wasm8WasmCode13constant_poolEv:
.LFB20564:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE20564:
	.size	_ZNK2v88internal4wasm8WasmCode13constant_poolEv, .-_ZNK2v88internal4wasm8WasmCode13constant_poolEv
	.section	.text._ZNK2v88internal4wasm8WasmCode13handler_tableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm8WasmCode13handler_tableEv
	.type	_ZNK2v88internal4wasm8WasmCode13handler_tableEv, @function
_ZNK2v88internal4wasm8WasmCode13handler_tableEv:
.LFB20565:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	88(%rdi), %rax
	ret
	.cfi_endproc
.LFE20565:
	.size	_ZNK2v88internal4wasm8WasmCode13handler_tableEv, .-_ZNK2v88internal4wasm8WasmCode13handler_tableEv
	.section	.text._ZNK2v88internal4wasm8WasmCode18handler_table_sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm8WasmCode18handler_table_sizeEv
	.type	_ZNK2v88internal4wasm8WasmCode18handler_table_sizeEv, @function
_ZNK2v88internal4wasm8WasmCode18handler_table_sizeEv:
.LFB20566:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	subl	88(%rdi), %eax
	ret
	.cfi_endproc
.LFE20566:
	.size	_ZNK2v88internal4wasm8WasmCode18handler_table_sizeEv, .-_ZNK2v88internal4wasm8WasmCode18handler_table_sizeEv
	.section	.text._ZNK2v88internal4wasm8WasmCode13code_commentsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm8WasmCode13code_commentsEv
	.type	_ZNK2v88internal4wasm8WasmCode13code_commentsEv, @function
_ZNK2v88internal4wasm8WasmCode13code_commentsEv:
.LFB20567:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	96(%rdi), %rax
	ret
	.cfi_endproc
.LFE20567:
	.size	_ZNK2v88internal4wasm8WasmCode13code_commentsEv, .-_ZNK2v88internal4wasm8WasmCode13code_commentsEv
	.section	.text._ZNK2v88internal4wasm8WasmCode18code_comments_sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm8WasmCode18code_comments_sizeEv
	.type	_ZNK2v88internal4wasm8WasmCode18code_comments_sizeEv, @function
_ZNK2v88internal4wasm8WasmCode18code_comments_sizeEv:
.LFB20568:
	.cfi_startproc
	endbr64
	movl	104(%rdi), %eax
	subl	96(%rdi), %eax
	ret
	.cfi_endproc
.LFE20568:
	.size	_ZNK2v88internal4wasm8WasmCode18code_comments_sizeEv, .-_ZNK2v88internal4wasm8WasmCode18code_comments_sizeEv
	.section	.rodata._ZN2v88internal4wasm8WasmCode23RegisterTrapHandlerDataEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"0 <= index"
.LC1:
	.string	"Check failed: %s."
.LC2:
	.string	"!has_trap_handler_index()"
	.section	.text._ZN2v88internal4wasm8WasmCode23RegisterTrapHandlerDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm8WasmCode23RegisterTrapHandlerDataEv
	.type	_ZN2v88internal4wasm8WasmCode23RegisterTrapHandlerDataEv, @function
_ZN2v88internal4wasm8WasmCode23RegisterTrapHandlerDataEv:
.LFB20569:
	.cfi_startproc
	endbr64
	movl	60(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L120
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	128(%rdi), %rdx
	testq	%rdx, %rdx
	jne	.L123
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movq	120(%rdi), %rcx
	movq	8(%rdi), %rsi
	movq	(%rdi), %rdi
	call	_ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE@PLT
	testl	%eax, %eax
	js	.L124
	movl	112(%rbx), %edx
	testl	%edx, %edx
	jns	.L125
	movl	%eax, 112(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L125:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20569:
	.size	_ZN2v88internal4wasm8WasmCode23RegisterTrapHandlerDataEv, .-_ZN2v88internal4wasm8WasmCode23RegisterTrapHandlerDataEv
	.section	.text._ZN2v88internal4wasm8WasmCode14ShouldBeLoggedEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm8WasmCode14ShouldBeLoggedEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm8WasmCode14ShouldBeLoggedEPNS0_7IsolateE, @function
_ZN2v88internal4wasm8WasmCode14ShouldBeLoggedEPNS0_7IsolateE:
.LFB20570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	41016(%rdi), %r13
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L127
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L136
.L128:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	cmpq	$0, 80(%r13)
	jne	.L128
.L131:
	movq	41488(%r12), %rax
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L129
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L137:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L130
.L129:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*136(%rax)
	testb	%al, %al
	je	.L137
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L130:
	movzbl	41812(%r12), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	call	*%rax
	testb	%al, %al
	je	.L131
	jmp	.L128
	.cfi_endproc
.LFE20570:
	.size	_ZN2v88internal4wasm8WasmCode14ShouldBeLoggedEPNS0_7IsolateE, .-_ZN2v88internal4wasm8WasmCode14ShouldBeLoggedEPNS0_7IsolateE
	.section	.rodata._ZNK2v88internal4wasm8WasmCode7LogCodeEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"<name too long>"
.LC4:
	.string	"(location_) != nullptr"
.LC5:
	.string	"wasm-function[%d]"
	.section	.text._ZNK2v88internal4wasm8WasmCode7LogCodeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm8WasmCode7LogCodeEPNS0_7IsolateE
	.type	_ZNK2v88internal4wasm8WasmCode7LogCodeEPNS0_7IsolateE, @function
_ZNK2v88internal4wasm8WasmCode7LogCodeEPNS0_7IsolateE:
.LFB20571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	56(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %edx
	je	.L162
	movq	48(%rdi), %rax
	movq	%rsi, %r12
	movq	%rdi, %rbx
	leaq	-144(%rbp), %r13
	movq	232(%rax), %rcx
	movq	208(%rax), %rdi
	movq	8(%rcx), %rsi
	movq	(%rcx), %rcx
	movq	%rsi, -136(%rbp)
	movq	%r13, %rsi
	movq	%rcx, -144(%rbp)
	call	_ZNK2v88internal4wasm10WasmModule18LookupFunctionNameERKNS1_15ModuleWireBytesEj@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullENS1_12WireBytesRefE@PLT
	movq	%rax, %r14
	movq	48(%rbx), %rax
	movq	%rdx, %r15
	cmpq	$0, 224(%rax)
	je	.L199
.L140:
	testq	%r15, %r15
	je	.L151
	movq	41088(%r12), %rax
	movslq	%r15d, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	addl	$1, 41104(%r12)
	leaq	-128(%rbp), %r13
	movq	%rax, -184(%rbp)
	movq	41096(%r12), %rax
	movq	%r13, %rsi
	movq	%r14, -128(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L200
.L152:
	movq	(%rax), %rax
	movq	%r13, %rsi
	leaq	-152(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-156(%rbp), %r8
	movl	$1, %edx
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	41488(%r12), %r15
	movslq	-156(%rbp), %r14
	movq	-152(%rbp), %r13
	leaq	56(%r15), %rax
	movq	%rax, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.L157
	.p2align 4,,10
	.p2align 3
.L154:
	movq	8(%r15), %rdi
	movq	%r13, %rcx
	movq	%r14, %r8
	movq	%rbx, %rdx
	movl	$11, %esi
	movq	(%rdi), %r9
	call	*48(%r9)
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L154
.L157:
	movq	-176(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L156
	call	_ZdaPv@PLT
.L156:
	subl	$1, 41104(%r12)
	movq	-184(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-168(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L158
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	cmpq	$0, 40(%rbx)
	jne	.L201
	.p2align 4,,10
	.p2align 3
.L162:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L202
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movl	56(%rbx), %ecx
	leaq	-96(%rbp), %rdi
	leaq	.LC5(%rip), %rdx
	xorl	%eax, %eax
	movl	$32, %esi
	movq	%rdi, -112(%rbp)
	movq	$32, -104(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	41488(%r12), %r13
	movq	-112(%rbp), %r14
	movslq	%eax, %r15
	leaq	56(%r13), %rax
	movq	%r15, -104(%rbp)
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%r13), %r13
	testq	%r13, %r13
	je	.L160
	.p2align 4,,10
	.p2align 3
.L159:
	movq	8(%r13), %rdi
	movq	%r14, %rcx
	movq	%r15, %r8
	movq	%rbx, %rdx
	movl	$11, %esi
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L159
.L160:
	movq	-168(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L158:
	cmpq	$0, 40(%rbx)
	je	.L162
.L201:
	movq	41016(%r12), %r12
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L163
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L164
.L165:
	movq	40(%rbx), %rcx
	movq	32(%rbx), %rdx
	movq	%r12, %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_6VectorIKhEE@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L199:
	movq	208(%rax), %rax
	movq	41696(%r12), %rdx
	cmpq	$0, 416(%rax)
	je	.L140
	testq	%rdx, %rdx
	je	.L140
	movq	41088(%r12), %rcx
	movq	%r12, %rdi
	addl	$1, 41104(%r12)
	movq	408(%rax), %rsi
	movq	%rcx, -192(%rbp)
	movq	41096(%r12), %rcx
	movq	%rcx, -184(%rbp)
	call	*%rdx
	movq	48(%rbx), %r8
	movl	$104, %edi
	movq	%rax, %r13
	movq	%r8, -176(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal4wasm19WasmModuleSourceMapC1EPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	-176(%rbp), %r8
	movq	-168(%rbp), %rax
	movq	224(%r8), %r13
	movq	%rax, 224(%r8)
	testq	%r13, %r13
	je	.L141
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	je	.L142
	call	_ZdlPv@PLT
.L142:
	movq	48(%r13), %rdi
	testq	%rdi, %rdi
	je	.L143
	call	_ZdlPv@PLT
.L143:
	movq	32(%r13), %rax
	movq	24(%r13), %r8
	movq	%rax, -168(%rbp)
	cmpq	%r8, %rax
	je	.L144
	.p2align 4,,10
	.p2align 3
.L148:
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L145
	movq	%r8, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-176(%rbp), %r8
	addq	$32, %r8
	cmpq	%r8, -168(%rbp)
	jne	.L148
.L146:
	movq	24(%r13), %r8
.L144:
	testq	%r8, %r8
	je	.L149
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L149:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L150
	call	_ZdlPv@PLT
.L150:
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L141:
	subl	$1, 41104(%r12)
	movq	-192(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-184(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L140
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L164:
	cmpq	$0, 80(%r12)
	jne	.L165
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L163:
	call	*%rax
	testb	%al, %al
	je	.L162
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L200:
	leaq	.LC3(%rip), %rax
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	$15, -120(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	jne	.L152
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	addq	$32, %r8
	cmpq	%r8, -168(%rbp)
	jne	.L148
	jmp	.L146
.L202:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20571:
	.size	_ZNK2v88internal4wasm8WasmCode7LogCodeEPNS0_7IsolateE, .-_ZNK2v88internal4wasm8WasmCode7LogCodeEPNS0_7IsolateE
	.section	.text._ZNK2v88internal4wasm8WasmCode8ValidateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm8WasmCode8ValidateEv
	.type	_ZNK2v88internal4wasm8WasmCode8ValidateEv, @function
_ZNK2v88internal4wasm8WasmCode8ValidateEv:
.LFB20573:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE20573:
	.size	_ZNK2v88internal4wasm8WasmCode8ValidateEv, .-_ZNK2v88internal4wasm8WasmCode8ValidateEv
	.section	.rodata._ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom.str1.1,"aMS",@progbits,1
.LC6:
	.string	"runtime-stub"
.LC7:
	.string	"jump table"
.LC8:
	.string	"unknown kind"
.LC9:
	.string	"wasm-to-capi"
.LC10:
	.string	"wasm function"
.LC11:
	.string	"wasm-to-js"
.LC12:
	.string	"interpreter entry"
.LC13:
	.string	"Liftoff"
.LC14:
	.string	"TurboFan"
.LC15:
	.string	"name: "
.LC16:
	.string	"\n"
.LC17:
	.string	"index: "
.LC18:
	.string	"compiler: "
.LC19:
	.string	"Instructions (size = "
.LC20:
	.string	")\n"
	.section	.rodata._ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"Exception Handler Table (size = "
	.section	.rodata._ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom.str1.1
.LC22:
	.string	"):\n"
	.section	.rodata._ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom.str1.8
	.align 8
.LC23:
	.string	"Protected instructions:\n pc offset  land pad\n"
	.align 8
.LC24:
	.string	"Source positions:\n pc offset  position\n"
	.section	.rodata._ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom.str1.1
.LC25:
	.string	"Safepoints (size = "
.LC26:
	.string	"  "
.LC27:
	.string	" (sp -> fp)"
.LC28:
	.string	" trampoline: "
.LC29:
	.string	" deopt: "
.LC30:
	.string	"RelocInfo (size = "
.LC31:
	.string	"  statement"
.LC32:
	.string	""
.LC33:
	.string	"Body (size = "
.LC34:
	.string	" = "
.LC35:
	.string	" + "
.LC36:
	.string	" padding)\n"
.LC37:
	.string	"kind: "
	.section	.text._ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom
	.type	_ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom, @function
_ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom:
.LFB20576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L205
	movq	%rsi, %r13
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L205:
	cmpl	$-1, 56(%rbx)
	je	.L206
	movl	$7, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	56(%rbx), %esi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L206:
	movl	$6, %edx
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	$5, 60(%rbx)
	ja	.L207
	movl	60(%rbx), %eax
	leaq	.L234(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom,"a",@progbits
	.align 4
	.align 4
.L234:
	.long	.L238-.L234
	.long	.L233-.L234
	.long	.L237-.L234
	.long	.L236-.L234
	.long	.L235-.L234
	.long	.L242-.L234
	.section	.text._ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$17, %edx
	leaq	.LC12(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$10, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%edx, %edx
	cmpb	$2, 136(%rbx)
	movq	%r12, %rdi
	setne	%dl
	leaq	.LC14(%rip), %rax
	leaq	.LC13(%rip), %rsi
	addq	$7, %rdx
	cmpb	$2, 136(%rbx)
	cmovne	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$13, %edx
	movq	%r12, %rdi
	movq	8(%rbx), %r15
	leaq	.LC33(%rip), %rsi
	subq	104(%rbx), %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$3, %edx
	leaq	.LC34(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	104(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$3, %edx
	leaq	.LC35(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$10, %edx
	leaq	.LC36(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	80(%rbx), %rax
	movq	104(%rbx), %rcx
	cmpq	%rcx, 64(%rbx)
	cmovbe	64(%rbx), %rcx
	testq	%rax, %rax
	je	.L232
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
.L232:
	cmpq	%rcx, 88(%rbx)
	cmovbe	88(%rbx), %rcx
	movl	$21, %edx
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	movq	%rcx, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC20(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	subq	$8, %rsp
	movq	(%rbx), %rdx
	movq	%rbx, %r9
	pushq	%r14
	movl	$2, %r8d
	movq	%r12, %rsi
	xorl	%edi, %edi
	leaq	(%rdx,%r13), %rcx
	leaq	-128(%rbp), %r13
	call	_ZN2v88internal12Disassembler6DecodeEPNS0_7IsolateEPSoPhS5_NS0_13CodeReferenceEm@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	64(%rbx), %rdx
	movq	88(%rbx), %rsi
	popq	%rdi
	popq	%r8
	cmpl	%edx, %esi
	je	.L210
	movl	$1, %ecx
	subl	%esi, %edx
	movq	%r13, %rdi
	addq	(%rbx), %rsi
	call	_ZN2v88internal12HandlerTableC1EmiNS1_12EncodingModeE@PLT
	movl	$32, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal12HandlerTable21NumberOfReturnEntriesEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
	movl	$3, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12HandlerTable23HandlerTableReturnPrintERSo@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L210:
	cmpq	$0, 128(%rbx)
	jne	.L260
.L211:
	cmpq	$0, 40(%rbx)
	jne	.L261
.L215:
	movq	80(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L262
.L219:
	movl	$18, %edx
	leaq	.LC30(%rip), %rsi
	leaq	-112(%rbp), %r14
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC20(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	subq	$8, %rsp
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	pushq	$-1
	movq	16(%rbx), %rcx
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	movq	24(%rbx), %r8
	call	_ZN2v88internal13RelocIteratorC1ENS0_6VectorIhEENS2_IKhEEmi@PLT
	cmpb	$0, -72(%rbp)
	popq	%rax
	popq	%rdx
	jne	.L228
	.p2align 4,,10
	.p2align 3
.L225:
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal9RelocInfo5PrintEPNS0_7IsolateERSo@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	je	.L225
.L228:
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	104(%rbx), %rdx
	movq	96(%rbx), %rsi
	cmpl	%esi, %edx
	je	.L204
	subl	%esi, %edx
	movq	%r12, %rdi
	addq	(%rbx), %rsi
	call	_ZN2v88internal24PrintCodeCommentsSectionERSomj@PLT
.L204:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movl	$10, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L233:
	movl	$12, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L237:
	movl	$10, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L236:
	movl	$12, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L262:
	movl	72(%rbx), %ecx
	movq	(%rbx), %rsi
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal14SafepointTableC1Emmjb@PLT
	movl	$19, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-112(%rbp), %eax
	movq	%r12, %rdi
	leal	12(%rax), %esi
	imull	-116(%rbp), %esi
	addl	$8, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC20(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-116(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L224
	.p2align 4,,10
	.p2align 3
.L220:
	leal	(%r14,%r14,2), %ecx
	movq	-104(%rbp), %rax
	movq	%r12, %rdi
	leal	0(,%rcx,4), %r15d
	movl	(%rax,%r15), %r8d
	movq	%r8, %rsi
	addq	(%rbx), %rsi
	movq	%r8, -136(%rbp)
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movq	(%r12), %rax
	movq	-136(%rbp), %r8
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	%r8, %rsi
	movq	$6, 16(%r12,%rax)
	movq	(%r12), %rax
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC26(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-136(%rbp), %rdi
	movq	%r12, %rdx
	movl	%r14d, %esi
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	movl	24(%rdi), %eax
	andl	$-75, %eax
	orl	$2, %eax
	movl	%eax, 24(%rdi)
	movq	%r13, %rdi
	call	_ZNK2v88internal14SafepointTable10PrintEntryEjRSo@PLT
	movl	$11, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-104(%rbp), %rcx
	addq	%r15, %rcx
	cmpb	$0, -88(%rbp)
	movl	4(%rcx), %r15d
	je	.L221
	movl	8(%rcx), %r9d
	cmpl	$-1, %r9d
	movl	%r9d, -136(%rbp)
	je	.L221
	movl	$13, %edx
	leaq	.LC28(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movl	-136(%rbp), %r9d
	movq	%r12, %rdi
	movq	-24(%rax), %rdx
	movl	%r9d, %esi
	addq	%r12, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	call	_ZNSolsEi@PLT
	movq	(%rax), %rdx
	addq	-24(%rdx), %rax
	movl	24(%rax), %edx
	andl	$-75, %edx
	orl	$2, %edx
	movl	%edx, 24(%rax)
.L221:
	cmpl	$-1, %r15d
	je	.L222
	movl	$8, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	addl	$1, %r14d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	$6, 16(%r12,%rax)
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, -116(%rbp)
	ja	.L220
.L224:
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L222:
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	addl	$1, %r14d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	-116(%rbp), %r14d
	jb	.L220
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L261:
	movl	$39, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%rbx), %rdx
	movq	32(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE@PLT
	cmpl	$-1, -104(%rbp)
	je	.L216
	leaq	.LC32(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L230:
	movq	(%r12), %rax
	movl	-96(%rbp), %esi
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	$10, 16(%r12,%rax)
	movq	(%r12), %rax
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	call	_ZNSolsEi@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	-24(%rax), %rdx
	addq	%rdi, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$2, %eax
	movl	%eax, 24(%rdx)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	movq	$10, 16(%rdi,%rax)
	movq	-88(%rbp), %rsi
	shrq	%rsi
	andl	$1073741823, %esi
	subl	$1, %esi
	call	_ZNSolsEi@PLT
	cmpb	$0, -80(%rbp)
	movl	$11, %edx
	leaq	.LC31(%rip), %rsi
	movq	%rax, %r15
	jne	.L259
	xorl	%edx, %edx
	movq	%r14, %rsi
.L259:
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -104(%rbp)
	jne	.L230
.L216:
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$45, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	120(%rbx), %rcx
	movq	128(%rbx), %rax
	leaq	.LC16(%rip), %r15
	leaq	(%rcx,%rax,8), %rax
	movq	%rcx, %r14
	movq	%rax, -136(%rbp)
	cmpq	%rcx, %rax
	je	.L213
	.p2align 4,,10
	.p2align 3
.L214:
	movq	(%r12), %rax
	movq	%r12, %rdi
	addq	$8, %r14
	movq	-24(%rax), %rax
	movq	$10, 16(%r12,%rax)
	movq	(%r12), %rax
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	movl	-8(%r14), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	$10, 16(%rdi,%rax)
	movq	(%rdi), %rax
	movq	-24(%rax), %rdx
	addq	%rdi, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	movl	-4(%r14), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r14, -136(%rbp)
	jne	.L214
.L213:
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L211
.L207:
	movl	$12, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L208
.L238:
	movl	$13, %edx
	leaq	.LC10(%rip), %rsi
	jmp	.L208
.L263:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20576:
	.size	_ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom, .-_ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom
	.section	.rodata._ZNK2v88internal4wasm8WasmCode5PrintEPKc.str1.1,"aMS",@progbits,1
.LC38:
	.string	"--- WebAssembly code ---\n"
.LC39:
	.string	"--- End code ---\n"
	.section	.text._ZNK2v88internal4wasm8WasmCode5PrintEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm8WasmCode5PrintEPKc
	.type	_ZNK2v88internal4wasm8WasmCode5PrintEPKc, @function
_ZNK2v88internal4wasm8WasmCode5PrintEPKc:
.LFB20575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-400(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	subq	$360, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	%rbx, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$25, %edx
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	leaq	.LC38(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal4wasm8WasmCode11DisassembleEPKcRSom
	movl	$17, %edx
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r15, %rdi
	movq	%rbx, -320(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L267
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L267:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20575:
	.size	_ZNK2v88internal4wasm8WasmCode5PrintEPKc, .-_ZNK2v88internal4wasm8WasmCode5PrintEPKc
	.section	.text._ZNK2v88internal4wasm8WasmCode10MaybePrintEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm8WasmCode10MaybePrintEPKc
	.type	_ZNK2v88internal4wasm8WasmCode10MaybePrintEPKc, @function
_ZNK2v88internal4wasm8WasmCode10MaybePrintEPKc:
.LFB20574:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal20FLAG_print_wasm_codeE(%rip)
	je	.L269
	movl	60(%rdi), %edx
	testl	%edx, %edx
	jne	.L276
.L270:
	jmp	_ZNK2v88internal4wasm8WasmCode5PrintEPKc
	.p2align 4,,10
	.p2align 3
.L269:
	cmpb	$0, _ZN2v88internal25FLAG_print_wasm_stub_codeE(%rip)
	je	.L272
	movl	60(%rdi), %eax
	testl	%eax, %eax
	jne	.L270
.L272:
	cmpb	$0, _ZN2v88internal15FLAG_print_codeE(%rip)
	jne	.L270
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	cmpb	$0, _ZN2v88internal25FLAG_print_wasm_stub_codeE(%rip)
	je	.L272
	jmp	_ZNK2v88internal4wasm8WasmCode5PrintEPKc
	.cfi_endproc
.LFE20574:
	.size	_ZNK2v88internal4wasm8WasmCode10MaybePrintEPKc, .-_ZNK2v88internal4wasm8WasmCode10MaybePrintEPKc
	.section	.text._ZN2v88internal4wasm23GetWasmCodeKindAsStringENS1_8WasmCode4KindE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm23GetWasmCodeKindAsStringENS1_8WasmCode4KindE
	.type	_ZN2v88internal4wasm23GetWasmCodeKindAsStringENS1_8WasmCode4KindE, @function
_ZN2v88internal4wasm23GetWasmCodeKindAsStringENS1_8WasmCode4KindE:
.LFB20579:
	.cfi_startproc
	endbr64
	cmpl	$5, %edi
	ja	.L278
	leaq	.L280(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm23GetWasmCodeKindAsStringENS1_8WasmCode4KindE,"a",@progbits
	.align 4
	.align 4
.L280:
	.long	.L286-.L280
	.long	.L284-.L280
	.long	.L283-.L280
	.long	.L282-.L280
	.long	.L281-.L280
	.long	.L279-.L280
	.section	.text._ZN2v88internal4wasm23GetWasmCodeKindAsStringENS1_8WasmCode4KindE
	.p2align 4,,10
	.p2align 3
.L284:
	leaq	.LC9(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	.LC7(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	leaq	.LC6(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	leaq	.LC12(%rip), %rax
	ret
.L278:
	leaq	.LC8(%rip), %rax
	ret
	.cfi_endproc
.LFE20579:
	.size	_ZN2v88internal4wasm23GetWasmCodeKindAsStringENS1_8WasmCode4KindE, .-_ZN2v88internal4wasm23GetWasmCodeKindAsStringENS1_8WasmCode4KindE
	.section	.text._ZN2v88internal4wasm8WasmCodeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm8WasmCodeD2Ev
	.type	_ZN2v88internal4wasm8WasmCodeD2Ev, @function
_ZN2v88internal4wasm8WasmCodeD2Ev:
.LFB20581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	112(%rdi), %edi
	testl	%edi, %edi
	js	.L288
	call	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi@PLT
.L288:
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L289
	call	_ZdaPv@PLT
.L289:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L290
	call	_ZdaPv@PLT
.L290:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L287
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20581:
	.size	_ZN2v88internal4wasm8WasmCodeD2Ev, .-_ZN2v88internal4wasm8WasmCodeD2Ev
	.globl	_ZN2v88internal4wasm8WasmCodeD1Ev
	.set	_ZN2v88internal4wasm8WasmCodeD1Ev,_ZN2v88internal4wasm8WasmCodeD2Ev
	.section	.text._ZN2v88internal4wasm8WasmCode27DecRefOnPotentiallyDeadCodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm8WasmCode27DecRefOnPotentiallyDeadCodeEv
	.type	_ZN2v88internal4wasm8WasmCode27DecRefOnPotentiallyDeadCodeEv, @function
_ZN2v88internal4wasm8WasmCode27DecRefOnPotentiallyDeadCodeEv:
.LFB20583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rbx, %rsi
	subq	$8, %rsp
	movq	48(%rdi), %rax
	movq	664(%rax), %rdi
	call	_ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L299
	lock subl	$1, 140(%rbx)
	sete	%al
.L299:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20583:
	.size	_ZN2v88internal4wasm8WasmCode27DecRefOnPotentiallyDeadCodeEv, .-_ZN2v88internal4wasm8WasmCode27DecRefOnPotentiallyDeadCodeEv
	.section	.rodata._ZN2v88internal4wasm8WasmCode17DecrementRefCountENS0_6VectorIKPS2_EE.str1.1,"aMS",@progbits,1
.LC41:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal4wasm8WasmCode17DecrementRefCountENS0_6VectorIKPS2_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm8WasmCode17DecrementRefCountENS0_6VectorIKPS2_EE
	.type	_ZN2v88internal4wasm8WasmCode17DecrementRefCountENS0_6VectorIKPS2_EE, @function
_ZN2v88internal4wasm8WasmCode17DecrementRefCountENS0_6VectorIKPS2_EE:
.LFB20584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	(%rdi,%rsi,8), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-64(%rbp), %rax
	movq	$1, -104(%rbp)
	movq	%rax, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movl	$0x3f800000, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	cmpq	%rdi, %r14
	je	.L345
	leaq	-96(%rbp), %rax
	movq	%rdi, %r12
	xorl	%r15d, %r15d
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L335:
	movq	(%r12), %rbx
	movl	140(%rbx), %eax
	leaq	140(%rbx), %rdx
.L309:
	cmpl	$1, %eax
	je	.L388
	leal	-1(%rax), %ecx
	lock cmpxchgl	%ecx, (%rdx)
	jne	.L309
.L308:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L335
	testq	%r15, %r15
	je	.L336
	leaq	-112(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm10WasmEngine12FreeDeadCodeERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE@PLT
.L336:
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	jne	.L337
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L389:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L339
.L340:
	movq	%rbx, %r12
.L337:
	movq	16(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	jne	.L389
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L340
.L339:
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	leaq	0(,%rax,8), %rdx
.L305:
	xorl	%esi, %esi
	call	memset@PLT
	movq	$0, -88(%rbp)
	movq	-112(%rbp), %rdi
	movq	$0, -96(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L304
	call	_ZdlPv@PLT
.L304:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L390
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm8WasmCode27DecRefOnPotentiallyDeadCodeEv
	testb	%al, %al
	je	.L308
	movq	48(%rbx), %r8
	movq	-104(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	movq	-112(%rbp), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	leaq	0(,%rdx,8), %r9
	testq	%rax, %rax
	je	.L310
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L391:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L310
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L310
.L312:
	cmpq	%rsi, %r8
	jne	.L391
	leaq	16(%rcx), %r13
.L343:
	movq	8(%r13), %rdx
	cmpq	16(%r13), %rdx
	je	.L326
	movq	%rbx, (%rdx)
	addq	$8, 8(%r13)
.L327:
	testq	%r15, %r15
	jne	.L308
	movq	48(%rbx), %rax
	movq	664(%rax), %r15
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L326:
	movabsq	$1152921504606846975, %rsi
	movq	0(%r13), %r9
	subq	%r9, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L392
	testq	%rax, %rax
	je	.L393
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	ja	.L394
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
	testq	%rsi, %rsi
	je	.L331
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rsi
	cmova	%rax, %rsi
	leaq	0(,%rsi,8), %rcx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L310:
	movl	$40, %edi
	movq	%r8, -136(%rbp)
	movq	%r9, -144(%rbp)
	call	_Znwm@PLT
	movq	-136(%rbp), %r8
	movq	-88(%rbp), %rdx
	leaq	-80(%rbp), %rdi
	movq	$0, (%rax)
	movq	-104(%rbp), %rsi
	movl	$1, %ecx
	movq	%rax, %r13
	movq	%r8, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-136(%rbp), %r8
	testb	%al, %al
	movq	%rdx, %r11
	jne	.L313
	movq	-112(%rbp), %r10
	movq	-144(%rbp), %r9
	leaq	(%r10,%r9), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L323
.L398:
	movq	(%rdx), %rdx
	movq	%rdx, 0(%r13)
	movq	(%rax), %rax
	movq	%r13, (%rax)
.L324:
	addq	$1, -88(%rbp)
	addq	$16, %r13
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L393:
	movl	$8, %ecx
.L329:
	movq	%rcx, %rdi
	movq	%rdx, -152(%rbp)
	movq	%r9, -144(%rbp)
	movq	%rcx, -136(%rbp)
	call	_Znwm@PLT
	movq	-136(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %r9
	movq	%rax, %r8
	leaq	(%rax,%rcx), %r10
.L331:
	leaq	8(%r8,%rdx), %rax
	movq	%rbx, (%r8,%rdx)
	movq	%rax, -136(%rbp)
	testq	%rdx, %rdx
	jg	.L395
	testq	%r9, %r9
	jne	.L333
.L334:
	movq	%r8, %xmm0
	movq	%r10, 16(%r13)
	movhps	-136(%rbp), %xmm0
	movups	%xmm0, 0(%r13)
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L313:
	cmpq	$1, %rdx
	je	.L396
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L397
	leaq	0(,%rdx,8), %rdx
	movq	%r8, -152(%rbp)
	movq	%rdx, %rdi
	movq	%r11, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_Znwm@PLT
	movq	-136(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %r11
	movq	%rax, %r10
.L316:
	movq	-96(%rbp), %rsi
	movq	$0, -96(%rbp)
	testq	%rsi, %rsi
	je	.L318
	xorl	%edi, %edi
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L320:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%r9), %rax
	movq	%rcx, (%rax)
.L321:
	testq	%rsi, %rsi
	je	.L318
.L319:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r11
	leaq	(%r10,%rdx,8), %r9
	movq	(%r9), %rax
	testq	%rax, %rax
	jne	.L320
	movq	-96(%rbp), %rax
	movq	%rax, (%rcx)
	movq	-128(%rbp), %rax
	movq	%rcx, -96(%rbp)
	movq	%rax, (%r9)
	cmpq	$0, (%rcx)
	je	.L346
	movq	%rcx, (%r10,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L319
	.p2align 4,,10
	.p2align 3
.L318:
	movq	-112(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L322
	movq	%r11, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r8, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %r11
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %r8
.L322:
	movq	%r8, %rax
	xorl	%edx, %edx
	movq	%r11, -104(%rbp)
	divq	%r11
	movq	%r10, -112(%rbp)
	leaq	0(,%rdx,8), %r9
	leaq	(%r10,%r9), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L398
.L323:
	movq	-96(%rbp), %rdx
	movq	%r13, -96(%rbp)
	movq	%rdx, 0(%r13)
	testq	%rdx, %rdx
	je	.L325
	movq	8(%rdx), %rax
	xorl	%edx, %edx
	divq	-104(%rbp)
	movq	%r13, (%r10,%rdx,8)
	movq	-112(%rbp), %rax
	addq	%r9, %rax
.L325:
	movq	-128(%rbp), %rsi
	movq	%rsi, (%rax)
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%r9, %rsi
	movq	%r8, %rdi
	movq	%r10, -152(%rbp)
	movq	%r9, -144(%rbp)
	call	memmove@PLT
	movq	-144(%rbp), %r9
	movq	-152(%rbp), %r10
	movq	%rax, %r8
.L333:
	movq	%r9, %rdi
	movq	%r8, -152(%rbp)
	movq	%r10, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %r10
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L394:
	movabsq	$9223372036854775800, %rcx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L346:
	movq	%rdx, %rdi
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L345:
	movq	%rax, %rdi
	movl	$8, %edx
	jmp	.L305
.L396:
	movq	$0, -64(%rbp)
	movq	-120(%rbp), %r10
	jmp	.L316
.L392:
	leaq	.LC41(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L397:
	call	_ZSt17__throw_bad_allocv@PLT
.L390:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20584:
	.size	_ZN2v88internal4wasm8WasmCode17DecrementRefCountENS0_6VectorIKPS2_EE, .-_ZN2v88internal4wasm8WasmCode17DecrementRefCountENS0_6VectorIKPS2_EE
	.section	.text._ZN2v88internal4wasm17WasmCodeAllocator13SetExecutableEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmCodeAllocator13SetExecutableEb
	.type	_ZN2v88internal4wasm17WasmCodeAllocator13SetExecutableEb, @function
_ZN2v88internal4wasm17WasmCodeAllocator13SetExecutableEb:
.LFB20644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$40, %rsp
	movl	%esi, -76(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	$1, %eax
	cmpb	%r14b, 168(%rbx)
	je	.L400
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	cmpb	$0, _ZN2v88internal35FLAG_wasm_write_protect_code_memoryE(%rip)
	movq	%rax, %r14
	je	.L403
	movq	(%rax), %rax
	cmpb	$1, -76(%rbp)
	movq	%r14, %rdi
	sbbl	%r13d, %r13d
	call	*24(%rax)
	movq	72(%rbx), %r12
	andl	$-2, %r13d
	leaq	72(%rbx), %rcx
	movq	%rcx, -72(%rbp)
	addl	$4, %r13d
	cmpq	%r12, %rcx
	je	.L403
	movq	%rax, %rcx
	subq	$1, %rax
	negq	%rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, -56(%rbp)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L412:
	movq	(%r12), %r12
	cmpq	%r12, -72(%rbp)
	je	.L403
.L404:
	movq	16(%r12), %rsi
	movq	-64(%rbp), %rdx
	movl	%r13d, %ecx
	movq	%r14, %rdi
	addq	24(%r12), %rdx
	andq	-56(%rbp), %rdx
	call	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE@PLT
	testb	%al, %al
	jne	.L412
.L400:
	movq	%r15, %rdi
	movb	%al, -56(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movzbl	-56(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movzbl	-76(%rbp), %eax
	movb	%al, 168(%rbx)
	movl	$1, %eax
	jmp	.L400
	.cfi_endproc
.LFE20644:
	.size	_ZN2v88internal4wasm17WasmCodeAllocator13SetExecutableEb, .-_ZN2v88internal4wasm17WasmCodeAllocator13SetExecutableEb
	.section	.text._ZNK2v88internal4wasm17WasmCodeAllocator19GetSingleCodeRegionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm17WasmCodeAllocator19GetSingleCodeRegionEv
	.type	_ZNK2v88internal4wasm17WasmCodeAllocator19GetSingleCodeRegionEv, @function
_ZNK2v88internal4wasm17WasmCodeAllocator19GetSingleCodeRegionEv:
.LFB20646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	120(%rbx), %rax
	movq	%r12, %rdi
	movq	16(%rax), %r13
	movq	8(%rax), %r14
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	movq	%r13, %rdx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20646:
	.size	_ZNK2v88internal4wasm17WasmCodeAllocator19GetSingleCodeRegionEv, .-_ZNK2v88internal4wasm17WasmCodeAllocator19GetSingleCodeRegionEv
	.section	.text._ZNK2v88internal4wasm12NativeModule20CreateCompilationEnvEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm12NativeModule20CreateCompilationEnvEv
	.type	_ZNK2v88internal4wasm12NativeModule20CreateCompilationEnvEv, @function
_ZNK2v88internal4wasm12NativeModule20CreateCompilationEnvEv:
.LFB20710:
	.cfi_startproc
	endbr64
	movq	208(%rsi), %rdx
	movzbl	676(%rsi), %ecx
	movb	$1, 9(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movb	%cl, 8(%rdi)
	testq	%rdx, %rdx
	je	.L416
	movl	8(%rdx), %ecx
	salq	$16, %rcx
	cmpb	$0, 17(%rdx)
	movq	%rcx, 16(%rdi)
	movl	$2147418112, %ecx
	je	.L417
	movl	12(%rdx), %ecx
	salq	$16, %rcx
.L417:
	movq	192(%rsi), %rdx
	movq	%rcx, 24(%rax)
	movb	$0, 45(%rax)
	movq	%rdx, 32(%rax)
	movl	200(%rsi), %edx
	movl	%edx, 40(%rax)
	movzbl	204(%rsi), %edx
	movb	%dl, 44(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	movq	$0, 16(%rdi)
	movl	$2147418112, %ecx
	jmp	.L417
	.cfi_endproc
.LFE20710:
	.size	_ZNK2v88internal4wasm12NativeModule20CreateCompilationEnvEv, .-_ZNK2v88internal4wasm12NativeModule20CreateCompilationEnvEv
	.section	.text._ZN2v88internal4wasm12NativeModule20AddCodeWithCodeSpaceEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierENS0_6VectorIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule20AddCodeWithCodeSpaceEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierENS0_6VectorIhEE
	.type	_ZN2v88internal4wasm12NativeModule20AddCodeWithCodeSpaceEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierENS0_6VectorIhEE, @function
_ZN2v88internal4wasm12NativeModule20AddCodeWithCodeSpaceEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierENS0_6VectorIhEE:
.LFB20786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$216, %rsp
	movq	16(%rbp), %rax
	movq	%rdi, -144(%rbp)
	movq	48(%rbp), %r13
	movq	56(%rbp), %r15
	movl	%edx, -204(%rbp)
	movq	%rax, -224(%rbp)
	movq	24(%rbp), %rax
	movl	%r8d, -208(%rbp)
	movq	%rax, -232(%rbp)
	movl	40(%rbp), %eax
	movl	%r9d, -212(%rbp)
	movl	%eax, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	52(%rcx), %rax
	testl	%eax, %eax
	jg	.L421
	movq	$0, -152(%rbp)
	movq	(%rcx), %r14
	movq	$0, -160(%rbp)
.L422:
	movq	$0, -168(%rbp)
	movl	20(%rbx), %edi
	testl	%edi, %edi
	je	.L423
	movslq	16(%rbx), %rax
	movq	%rax, -168(%rbp)
.L423:
	movslq	40(%rbx), %rcx
	movslq	24(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r13, %r14
	movq	%rcx, -192(%rbp)
	movslq	12(%rbx), %rcx
	movq	%rax, -176(%rbp)
	movslq	32(%rbx), %rax
	movq	%rcx, %rdx
	movq	%rcx, -200(%rbp)
	movq	%rax, -136(%rbp)
	call	memcpy@PLT
	subq	$8, %rsp
	subq	(%rbx), %r14
	leaq	-128(%rbp), %rbx
	movl	_ZN2v88internal9RelocInfo10kApplyMaskE(%rip), %esi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	-136(%rbp), %rax
	movq	-160(%rbp), %rcx
	movq	-152(%rbp), %r8
	orl	$48, %esi
	leaq	0(%r13,%rax), %r9
	pushq	%rsi
	movq	%r13, %rsi
	call	_ZN2v88internal13RelocIteratorC1ENS0_6VectorIhEENS2_IKhEEmi@PLT
	cmpb	$0, -72(%rbp)
	popq	%rcx
	popq	%rsi
	jne	.L432
	movq	%r13, -256(%rbp)
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L458:
	cmpb	$6, %al
	je	.L440
	cmpb	$8, %al
	je	.L454
.L428:
	movq	%rbx, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	jne	.L455
.L424:
	movzbl	-104(%rbp), %eax
	cmpb	$4, %al
	je	.L456
	cmpb	$5, %al
	je	.L457
	testb	%al, %al
	jne	.L458
.L440:
	movq	-112(%rbp), %rax
	movq	%rbx, %rdi
	subl	%r14d, (%rax)
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	je	.L424
.L455:
	movq	-256(%rbp), %r13
.L432:
	movq	-192(%rbp), %xmm0
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	-168(%rbp), %xmm1
	movhps	-200(%rbp), %xmm0
	movhps	-176(%rbp), %xmm1
	movaps	%xmm1, -256(%rbp)
	movaps	%xmm0, -192(%rbp)
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	movq	-224(%rbp), %rax
	movl	$144, %edi
	movq	(%rax), %r14
	movq	8(%rax), %rbx
	movq	$0, (%rax)
	movq	-232(%rbp), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	$0, (%rax)
	movq	%rcx, -176(%rbp)
	movq	%rdx, -168(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	%r13, (%rax)
	movq	%rax, %rdi
	movdqa	-256(%rbp), %xmm1
	movq	%r15, 8(%rax)
	movq	-160(%rbp), %rax
	movdqa	-192(%rbp), %xmm0
	movq	%rcx, 32(%rdi)
	movq	%rax, 16(%rdi)
	movq	-152(%rbp), %rax
	movq	%rdx, 40(%rdi)
	movq	%rax, 24(%rdi)
	movl	-204(%rbp), %eax
	movq	%r12, 48(%rdi)
	movl	%eax, 56(%rdi)
	movl	32(%rbp), %eax
	movl	$-1, 112(%rdi)
	movl	%eax, 60(%rdi)
	movq	-136(%rbp), %rax
	movq	%r14, 120(%rdi)
	movq	%rax, 64(%rdi)
	movl	-208(%rbp), %eax
	movq	%rbx, 128(%rdi)
	movl	%eax, 72(%rdi)
	movl	-212(%rbp), %eax
	movl	$1, 140(%rdi)
	movl	%eax, 76(%rdi)
	movzbl	-216(%rbp), %eax
	movups	%xmm1, 80(%rdi)
	movb	%al, 136(%rdi)
	movq	-144(%rbp), %rax
	movups	%xmm0, 96(%rdi)
	movq	%rdi, (%rax)
	cmpb	$0, _ZN2v88internal20FLAG_print_wasm_codeE(%rip)
	je	.L459
	movl	32(%rbp), %edx
	testl	%edx, %edx
	jne	.L453
.L433:
	xorl	%esi, %esi
	call	_ZNK2v88internal4wasm8WasmCode5PrintEPKc
.L420:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L460
	movq	-144(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	leaq	-112(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal9RelocInfo13wasm_call_tagEv@PLT
	movq	208(%r12), %rdx
	movl	$2863311531, %ecx
	movq	%r13, %rdi
	subl	60(%rdx), %eax
	movl	%eax, %edx
	imulq	%rcx, %rdx
	shrq	$35, %rdx
	leal	(%rdx,%rdx,2), %ecx
	sall	$6, %edx
	sall	$2, %ecx
	subl	%ecx, %eax
	leal	(%rax,%rax,4), %eax
	leal	(%rax,%rdx), %esi
	movq	504(%r12), %rax
	movl	$1, %edx
	addq	(%rax), %rsi
	call	_ZN2v88internal9RelocInfo21set_wasm_call_addressEmNS0_15ICacheFlushModeE@PLT
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L457:
	leaq	-112(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal9RelocInfo13wasm_call_tagEv@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	cltq
	movq	248(%r12,%rax,8), %rsi
	call	_ZN2v88internal9RelocInfo26set_wasm_stub_call_addressEmNS0_15ICacheFlushModeE@PLT
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L459:
	movl	32(%rbp), %eax
	testl	%eax, %eax
	je	.L437
.L453:
	cmpb	$0, _ZN2v88internal25FLAG_print_wasm_stub_codeE(%rip)
	jne	.L433
.L437:
	cmpb	$0, _ZN2v88internal15FLAG_print_codeE(%rip)
	je	.L420
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L454:
	movq	-112(%rbp), %rax
	addq	%r14, (%rax)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	_Znam@PLT
	movslq	52(%rbx), %rdx
	movslq	8(%rbx), %rsi
	movq	(%rbx), %r14
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	subq	%rdx, %rsi
	addq	%r14, %rsi
	call	memcpy@PLT
	jmp	.L422
.L460:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20786:
	.size	_ZN2v88internal4wasm12NativeModule20AddCodeWithCodeSpaceEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierENS0_6VectorIhEE, .-_ZN2v88internal4wasm12NativeModule20AddCodeWithCodeSpaceEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierENS0_6VectorIhEE
	.section	.rodata._ZN2v88internal4wasm11GetCodeKindERKNS1_21WasmCompilationResultE.str1.1,"aMS",@progbits,1
.LC42:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4wasm11GetCodeKindERKNS1_21WasmCompilationResultE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm11GetCodeKindERKNS1_21WasmCompilationResultE
	.type	_ZN2v88internal4wasm11GetCodeKindERKNS1_21WasmCompilationResultE, @function
_ZN2v88internal4wasm11GetCodeKindERKNS1_21WasmCompilationResultE:
.LFB20788:
	.cfi_startproc
	endbr64
	movsbl	134(%rdi), %eax
	cmpb	$2, %al
	ja	.L462
	addl	%eax, %eax
	ret
.L462:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC42(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20788:
	.size	_ZN2v88internal4wasm11GetCodeKindERKNS1_21WasmCompilationResultE, .-_ZN2v88internal4wasm11GetCodeKindERKNS1_21WasmCompilationResultE
	.section	.text._ZNK2v88internal4wasm12NativeModule17SnapshotCodeTableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm12NativeModule17SnapshotCodeTableEv
	.type	_ZNK2v88internal4wasm12NativeModule17SnapshotCodeTableEv, @function
_ZNK2v88internal4wasm12NativeModule17SnapshotCodeTableEv:
.LFB20791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	536(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	208(%r13), %rax
	movq	624(%r13), %r15
	pxor	%xmm0, %xmm0
	movl	68(%rax), %r13d
	movups	%xmm0, (%r12)
	movq	$0, 16(%r12)
	salq	$3, %r13
	je	.L467
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	leaq	(%rax,%r13), %rbx
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	%rbx, 16(%r12)
	call	memcpy@PLT
.L468:
	movq	%rbx, 8(%r12)
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	movq	$0, (%r12)
	xorl	%ebx, %ebx
	jmp	.L468
	.cfi_endproc
.LFE20791:
	.size	_ZNK2v88internal4wasm12NativeModule17SnapshotCodeTableEv, .-_ZNK2v88internal4wasm12NativeModule17SnapshotCodeTableEv
	.section	.text._ZNK2v88internal4wasm12NativeModule7HasCodeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm12NativeModule7HasCodeEj
	.type	_ZNK2v88internal4wasm12NativeModule7HasCodeEj, @function
_ZNK2v88internal4wasm12NativeModule7HasCodeEj:
.LFB20802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	536(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	208(%r12), %rax
	movq	%r13, %rdi
	subl	60(%rax), %ebx
	movq	624(%r12), %rax
	cmpq	$0, (%rax,%rbx,8)
	setne	%r12b
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20802:
	.size	_ZNK2v88internal4wasm12NativeModule7HasCodeEj, .-_ZNK2v88internal4wasm12NativeModule7HasCodeEj
	.section	.text._ZN2v88internal4wasm12NativeModule16SetWasmSourceMapESt10unique_ptrINS1_19WasmModuleSourceMapESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule16SetWasmSourceMapESt10unique_ptrINS1_19WasmModuleSourceMapESt14default_deleteIS4_EE
	.type	_ZN2v88internal4wasm12NativeModule16SetWasmSourceMapESt10unique_ptrINS1_19WasmModuleSourceMapESt14default_deleteIS4_EE, @function
_ZN2v88internal4wasm12NativeModule16SetWasmSourceMapESt10unique_ptrINS1_19WasmModuleSourceMapESt14default_deleteIS4_EE:
.LFB20803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rsi), %rax
	movq	224(%rdi), %r13
	movq	$0, (%rsi)
	movq	%rax, 224(%rdi)
	testq	%r13, %r13
	je	.L473
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	je	.L475
	call	_ZdlPv@PLT
.L475:
	movq	48(%r13), %rdi
	testq	%rdi, %rdi
	je	.L476
	call	_ZdlPv@PLT
.L476:
	movq	32(%r13), %rbx
	movq	24(%r13), %r12
	cmpq	%r12, %rbx
	je	.L477
	.p2align 4,,10
	.p2align 3
.L481:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L478
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L481
	movq	24(%r13), %r12
.L477:
	testq	%r12, %r12
	je	.L482
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L482:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L483
	call	_ZdlPv@PLT
.L483:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$104, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L481
	movq	24(%r13), %r12
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L473:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20803:
	.size	_ZN2v88internal4wasm12NativeModule16SetWasmSourceMapESt10unique_ptrINS1_19WasmModuleSourceMapESt14default_deleteIS4_EE, .-_ZN2v88internal4wasm12NativeModule16SetWasmSourceMapESt10unique_ptrINS1_19WasmModuleSourceMapESt14default_deleteIS4_EE
	.section	.text._ZNK2v88internal4wasm12NativeModule16GetWasmSourceMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm12NativeModule16GetWasmSourceMapEv
	.type	_ZNK2v88internal4wasm12NativeModule16GetWasmSourceMapEv, @function
_ZNK2v88internal4wasm12NativeModule16GetWasmSourceMapEv:
.LFB20805:
	.cfi_startproc
	endbr64
	movq	224(%rdi), %rax
	ret
	.cfi_endproc
.LFE20805:
	.size	_ZNK2v88internal4wasm12NativeModule16GetWasmSourceMapEv, .-_ZNK2v88internal4wasm12NativeModule16GetWasmSourceMapEv
	.section	.text._ZN2v88internal4wasm12NativeModule12SetWireBytesENS0_11OwnedVectorIKhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule12SetWireBytesENS0_11OwnedVectorIKhEE
	.type	_ZN2v88internal4wasm12NativeModule12SetWireBytesENS0_11OwnedVectorIKhEE, @function
_ZN2v88internal4wasm12NativeModule12SetWireBytesENS0_11OwnedVectorIKhEE:
.LFB20848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r12
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r12)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	16(%r12), %r15
	movq	%rax, (%r12)
	movq	0(%r13), %rax
	movq	$0, 0(%r13)
	movq	%rax, 16(%r12)
	movq	8(%r13), %rax
	movq	240(%rbx), %r13
	movq	%r15, 232(%rbx)
	movq	%rax, 24(%r12)
	cmpq	%r12, %r13
	je	.L499
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L500
	lock addl	$1, 8(%r12)
	movq	240(%rbx), %r13
.L501:
	testq	%r13, %r13
	je	.L503
	testq	%r14, %r14
	je	.L504
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L535
	.p2align 4,,10
	.p2align 3
.L503:
	movq	%r12, 240(%rbx)
	movq	24(%r12), %rax
.L499:
	testq	%rax, %rax
	jne	.L536
.L534:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L526
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L537
.L498:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L538
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L498
.L537:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L520
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L521:
	cmpl	$1, %eax
	jne	.L498
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L500:
	movl	$2, 8(%r12)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L536:
	movl	$40, %edi
	movq	520(%rbx), %r13
	call	_Znwm@PLT
	leaq	-80(%rbp), %rsi
	movabsq	$4294967297, %rcx
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	leaq	16(%rax), %rdx
	movq	%r13, %rdi
	movq	%rcx, (%rax)
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageE(%rip), %rcx
	movq	%r12, 32(%rax)
	movq	%rcx, 16(%rax)
	movq	%r15, 24(%rax)
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal4wasm16CompilationState19SetWireBytesStorageESt10shared_ptrINS1_16WireBytesStorageEE@PLT
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	jne	.L534
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L520:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L504:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L503
.L535:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L507
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L508:
	cmpl	$1, %eax
	jne	.L503
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L507:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L508
.L538:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20848:
	.size	_ZN2v88internal4wasm12NativeModule12SetWireBytesENS0_11OwnedVectorIKhEE, .-_ZN2v88internal4wasm12NativeModule12SetWireBytesENS0_11OwnedVectorIKhEE
	.section	.text._ZNK2v88internal4wasm12NativeModule18GetJumpTableOffsetEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm12NativeModule18GetJumpTableOffsetEj
	.type	_ZNK2v88internal4wasm12NativeModule18GetJumpTableOffsetEj, @function
_ZNK2v88internal4wasm12NativeModule18GetJumpTableOffsetEj:
.LFB20864:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rax
	movl	$2863311531, %edx
	subl	60(%rax), %esi
	movl	%esi, %eax
	imulq	%rdx, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,2), %edx
	sall	$6, %eax
	sall	$2, %edx
	subl	%edx, %esi
	leal	(%rsi,%rsi,4), %edx
	addl	%edx, %eax
	ret
	.cfi_endproc
.LFE20864:
	.size	_ZNK2v88internal4wasm12NativeModule18GetJumpTableOffsetEj, .-_ZNK2v88internal4wasm12NativeModule18GetJumpTableOffsetEj
	.section	.text._ZNK2v88internal4wasm12NativeModule24GetCallTargetForFunctionEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm12NativeModule24GetCallTargetForFunctionEj
	.type	_ZNK2v88internal4wasm12NativeModule24GetCallTargetForFunctionEj, @function
_ZNK2v88internal4wasm12NativeModule24GetCallTargetForFunctionEj:
.LFB20865:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rax
	movl	$2863311531, %edx
	subl	60(%rax), %esi
	movl	%esi, %eax
	imulq	%rdx, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,2), %edx
	sall	$6, %eax
	sall	$2, %edx
	subl	%edx, %esi
	leal	(%rsi,%rsi,4), %edx
	addl	%edx, %eax
	movq	504(%rdi), %rdx
	addq	(%rdx), %rax
	ret
	.cfi_endproc
.LFE20865:
	.size	_ZNK2v88internal4wasm12NativeModule24GetCallTargetForFunctionEj, .-_ZNK2v88internal4wasm12NativeModule24GetCallTargetForFunctionEj
	.section	.text._ZNK2v88internal4wasm12NativeModule33GetFunctionIndexFromJumpTableSlotEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm12NativeModule33GetFunctionIndexFromJumpTableSlotEm
	.type	_ZNK2v88internal4wasm12NativeModule33GetFunctionIndexFromJumpTableSlotEm, @function
_ZNK2v88internal4wasm12NativeModule33GetFunctionIndexFromJumpTableSlotEm:
.LFB20866:
	.cfi_startproc
	endbr64
	movq	504(%rdi), %rax
	movl	$3435973837, %edx
	subl	(%rax), %esi
	movl	%esi, %eax
	andl	$63, %esi
	imulq	%rdx, %rsi
	shrl	$6, %eax
	movq	208(%rdi), %rdx
	leal	(%rax,%rax,2), %eax
	shrq	$34, %rsi
	leal	(%rsi,%rax,4), %eax
	addl	60(%rdx), %eax
	ret
	.cfi_endproc
.LFE20866:
	.size	_ZNK2v88internal4wasm12NativeModule33GetFunctionIndexFromJumpTableSlotEm, .-_ZNK2v88internal4wasm12NativeModule33GetFunctionIndexFromJumpTableSlotEm
	.section	.rodata._ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm.str1.1,"aMS",@progbits,1
.LC43:
	.string	"WasmI32PairToBigInt"
.LC44:
	.string	"<unknown>"
.LC45:
	.string	"ThrowWasmTrapUnreachable"
.LC46:
	.string	"ThrowWasmTrapMemOutOfBounds"
.LC47:
	.string	"ThrowWasmTrapUnalignedAccess"
.LC48:
	.string	"ThrowWasmTrapDivByZero"
	.section	.rodata._ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm.str1.8,"aMS",@progbits,1
	.align 8
.LC49:
	.string	"ThrowWasmTrapDivUnrepresentable"
	.section	.rodata._ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm.str1.1
.LC50:
	.string	"ThrowWasmTrapRemByZero"
	.section	.rodata._ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm.str1.8
	.align 8
.LC51:
	.string	"ThrowWasmTrapFloatUnrepresentable"
	.section	.rodata._ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm.str1.1
.LC52:
	.string	"ThrowWasmTrapFuncInvalid"
.LC53:
	.string	"ThrowWasmTrapFuncSigMismatch"
	.section	.rodata._ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm.str1.8
	.align 8
.LC54:
	.string	"ThrowWasmTrapDataSegmentDropped"
	.align 8
.LC55:
	.string	"ThrowWasmTrapElemSegmentDropped"
	.section	.rodata._ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm.str1.1
.LC56:
	.string	"ThrowWasmTrapTableOutOfBounds"
.LC57:
	.string	"WasmCompileLazy"
.LC58:
	.string	"WasmAllocateHeapNumber"
.LC59:
	.string	"WasmAtomicNotify"
.LC60:
	.string	"WasmI32AtomicWait"
.LC61:
	.string	"WasmI64AtomicWait"
.LC62:
	.string	"WasmMemoryGrow"
.LC63:
	.string	"WasmTableGet"
.LC64:
	.string	"WasmTableSet"
.LC65:
	.string	"WasmRecordWrite"
.LC66:
	.string	"WasmStackGuard"
.LC67:
	.string	"WasmStackOverflow"
.LC68:
	.string	"WasmToNumber"
.LC69:
	.string	"WasmThrow"
.LC70:
	.string	"WasmRethrow"
.LC71:
	.string	"DoubleToI"
.LC72:
	.string	"WasmI64ToBigInt"
.LC73:
	.string	"WasmBigIntToI64"
.LC74:
	.string	"WasmBigIntToI32Pair"
	.section	.text._ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm
	.type	_ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm, @function
_ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm:
.LFB20867:
	.cfi_startproc
	endbr64
	leaq	.LC45(%rip), %rax
	cmpq	%rsi, 248(%rdi)
	je	.L542
	leaq	.LC46(%rip), %rax
	cmpq	%rsi, 256(%rdi)
	je	.L542
	leaq	.LC47(%rip), %rax
	cmpq	%rsi, 264(%rdi)
	je	.L542
	leaq	.LC48(%rip), %rax
	cmpq	%rsi, 272(%rdi)
	je	.L542
	leaq	.LC49(%rip), %rax
	cmpq	%rsi, 280(%rdi)
	je	.L542
	leaq	.LC50(%rip), %rax
	cmpq	%rsi, 288(%rdi)
	je	.L542
	leaq	.LC51(%rip), %rax
	cmpq	%rsi, 296(%rdi)
	je	.L542
	leaq	.LC52(%rip), %rax
	cmpq	%rsi, 304(%rdi)
	je	.L542
	leaq	.LC53(%rip), %rax
	cmpq	%rsi, 312(%rdi)
	je	.L542
	leaq	.LC54(%rip), %rax
	cmpq	%rsi, 320(%rdi)
	je	.L542
	leaq	.LC55(%rip), %rax
	cmpq	%rsi, 328(%rdi)
	je	.L542
	leaq	.LC56(%rip), %rax
	cmpq	%rsi, 336(%rdi)
	je	.L542
	leaq	.LC57(%rip), %rax
	cmpq	%rsi, 344(%rdi)
	je	.L542
	leaq	.LC58(%rip), %rax
	cmpq	%rsi, 352(%rdi)
	je	.L542
	leaq	.LC59(%rip), %rax
	cmpq	%rsi, 360(%rdi)
	je	.L542
	leaq	.LC60(%rip), %rax
	cmpq	%rsi, 368(%rdi)
	je	.L542
	leaq	.LC61(%rip), %rax
	cmpq	%rsi, 376(%rdi)
	je	.L542
	leaq	.LC62(%rip), %rax
	cmpq	%rsi, 384(%rdi)
	je	.L542
	leaq	.LC63(%rip), %rax
	cmpq	%rsi, 392(%rdi)
	je	.L542
	leaq	.LC64(%rip), %rax
	cmpq	%rsi, 400(%rdi)
	je	.L542
	leaq	.LC65(%rip), %rax
	cmpq	%rsi, 408(%rdi)
	je	.L542
	leaq	.LC66(%rip), %rax
	cmpq	%rsi, 416(%rdi)
	je	.L542
	leaq	.LC67(%rip), %rax
	cmpq	%rsi, 424(%rdi)
	je	.L542
	leaq	.LC68(%rip), %rax
	cmpq	%rsi, 432(%rdi)
	je	.L542
	leaq	.LC69(%rip), %rax
	cmpq	%rsi, 440(%rdi)
	je	.L542
	leaq	.LC70(%rip), %rax
	cmpq	%rsi, 448(%rdi)
	je	.L542
	leaq	.LC71(%rip), %rax
	cmpq	%rsi, 456(%rdi)
	je	.L542
	leaq	.LC72(%rip), %rax
	cmpq	%rsi, 464(%rdi)
	je	.L542
	leaq	.LC43(%rip), %rax
	cmpq	%rsi, 472(%rdi)
	je	.L542
	leaq	.LC73(%rip), %rax
	cmpq	%rsi, 480(%rdi)
	je	.L542
	cmpq	%rsi, 488(%rdi)
	leaq	.LC44(%rip), %rax
	leaq	.LC74(%rip), %rdx
	cmove	%rdx, %rax
.L542:
	ret
	.cfi_endproc
.LFE20867:
	.size	_ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm, .-_ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm
	.section	.text._ZN2v88internal4wasm15WasmCodeManagerC2EPNS1_17WasmMemoryTrackerEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmCodeManagerC2EPNS1_17WasmMemoryTrackerEm
	.type	_ZN2v88internal4wasm15WasmCodeManagerC2EPNS1_17WasmMemoryTrackerEm, @function
_ZN2v88internal4wasm15WasmCodeManagerC2EPNS1_17WasmMemoryTrackerEm:
.LFB20884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	40(%rdi), %rdi
	subq	$8, %rsp
	movq	%rdx, -32(%rdi)
	shrq	%rdx
	movq	%rsi, -40(%rdi)
	movb	$0, -24(%rdi)
	movq	$0, -16(%rdi)
	movq	%rdx, -8(%rdi)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	88(%rbx), %rax
	movl	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	%rax, 104(%rbx)
	movq	%rax, 112(%rbx)
	movq	$0, 120(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20884:
	.size	_ZN2v88internal4wasm15WasmCodeManagerC2EPNS1_17WasmMemoryTrackerEm, .-_ZN2v88internal4wasm15WasmCodeManagerC2EPNS1_17WasmMemoryTrackerEm
	.globl	_ZN2v88internal4wasm15WasmCodeManagerC1EPNS1_17WasmMemoryTrackerEm
	.set	_ZN2v88internal4wasm15WasmCodeManagerC1EPNS1_17WasmMemoryTrackerEm,_ZN2v88internal4wasm15WasmCodeManagerC2EPNS1_17WasmMemoryTrackerEm
	.section	.text._ZN2v88internal4wasm15WasmCodeManager6CommitENS_4base13AddressRegionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmCodeManager6CommitENS_4base13AddressRegionE
	.type	_ZN2v88internal4wasm15WasmCodeManager6CommitENS_4base13AddressRegionE, @function
_ZN2v88internal4wasm15WasmCodeManager6CommitENS_4base13AddressRegionE:
.LFB20886:
	.cfi_startproc
	endbr64
	movzbl	_ZN2v88internal14FLAG_perf_profE(%rip), %r8d
	testb	%r8b, %r8b
	je	.L578
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L578:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	movq	24(%rdi), %rax
.L580:
	movq	8(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rbx, %rdx
	jb	.L577
	leaq	(%rbx,%rax), %rdx
	lock cmpxchgq	%rdx, 0(%r13)
	jne	.L580
	xorl	%r14d, %r14d
	cmpb	$0, _ZN2v88internal35FLAG_wasm_write_protect_code_memoryE(%rip)
	sete	%r14b
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	addl	$2, %r14d
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movl	%r14d, %ecx
	call	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE@PLT
	movl	%eax, %r8d
	testb	%al, %al
	je	.L589
	movl	$1, %r8d
.L577:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L589:
	.cfi_restore_state
	lock subq	%rbx, 0(%r13)
	jmp	.L577
	.cfi_endproc
.LFE20886:
	.size	_ZN2v88internal4wasm15WasmCodeManager6CommitENS_4base13AddressRegionE, .-_ZN2v88internal4wasm15WasmCodeManager6CommitENS_4base13AddressRegionE
	.section	.rodata._ZN2v88internal4wasm15WasmCodeManager8DecommitENS_4base13AddressRegionE.str1.8,"aMS",@progbits,1
	.align 8
.LC75:
	.string	"allocator->SetPermissions(reinterpret_cast<void*>(region.begin()), region.size(), PageAllocator::kNoAccess)"
	.section	.text._ZN2v88internal4wasm15WasmCodeManager8DecommitENS_4base13AddressRegionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmCodeManager8DecommitENS_4base13AddressRegionE
	.type	_ZN2v88internal4wasm15WasmCodeManager8DecommitENS_4base13AddressRegionE, @function
_ZN2v88internal4wasm15WasmCodeManager8DecommitENS_4base13AddressRegionE:
.LFB20887:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal14FLAG_perf_profE(%rip)
	je	.L596
	ret
	.p2align 4,,10
	.p2align 3
.L596:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movq	%rax, %rdi
	lock subq	%r12, 24(%rbx)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	*72(%rax)
	testb	%al, %al
	je	.L597
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	.cfi_restore_state
	leaq	.LC75(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20887:
	.size	_ZN2v88internal4wasm15WasmCodeManager8DecommitENS_4base13AddressRegionE, .-_ZN2v88internal4wasm15WasmCodeManager8DecommitENS_4base13AddressRegionE
	.section	.text._ZN2v88internal4wasm15WasmCodeManager11TryAllocateEmPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmCodeManager11TryAllocateEmPv
	.type	_ZN2v88internal4wasm15WasmCodeManager11TryAllocateEmPv, @function
_ZN2v88internal4wasm15WasmCodeManager11TryAllocateEmPv:
.LFB20897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movq	(%r14), %rdi
	movq	%rax, %rdx
	leaq	-1(%rax,%r15), %rsi
	movq	%rax, %rbx
	negq	%rdx
	andq	%rdx, %rsi
	movq	%rsi, %r15
	call	_ZN2v88internal4wasm17WasmMemoryTracker19ReserveAddressSpaceEm@PLT
	movq	-88(%rbp), %rcx
	testb	%al, %al
	jne	.L599
	movq	$0, (%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
.L598:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L607
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L599:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L608
.L601:
	leaq	-80(%rbp), %r9
	movq	%r13, %rsi
	movq	%rbx, %r8
	movq	%r15, %rdx
	movq	%r9, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal13VirtualMemoryC1EPNS_13PageAllocatorEmPvm@PLT
	movq	-72(%rbp), %r13
	movq	-88(%rbp), %r9
	testq	%r13, %r13
	je	.L609
	cmpb	$0, _ZN2v88internal14FLAG_perf_profE(%rip)
	jne	.L610
.L604:
	movq	-80(%rbp), %rax
	movdqu	-72(%rbp), %xmm1
	movq	%r9, %rdi
	movq	%r9, -88(%rbp)
	movq	%rax, (%r12)
	movups	%xmm1, 8(%r12)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	movq	-88(%rbp), %r9
.L603:
	movq	%r9, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L609:
	movq	(%r14), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm17WasmMemoryTracker18ReleaseReservationEm@PLT
	pxor	%xmm0, %xmm0
	movq	-88(%rbp), %r9
	movq	$0, (%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L608:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	movq	%rax, %rcx
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L610:
	movq	-64(%rbp), %r14
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movl	$3, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE@PLT
	movq	-88(%rbp), %r9
	jmp	.L604
.L607:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20897:
	.size	_ZN2v88internal4wasm15WasmCodeManager11TryAllocateEmPv, .-_ZN2v88internal4wasm15WasmCodeManager11TryAllocateEmPv
	.section	.text._ZN2v88internal4wasm15WasmCodeManager31SetMaxCommittedMemoryForTestingEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmCodeManager31SetMaxCommittedMemoryForTestingEm
	.type	_ZN2v88internal4wasm15WasmCodeManager31SetMaxCommittedMemoryForTestingEm, @function
_ZN2v88internal4wasm15WasmCodeManager31SetMaxCommittedMemoryForTestingEm:
.LFB20901:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	shrq	%rsi
	movq	%rsi, 32(%rdi)
	mfence
	ret
	.cfi_endproc
.LFE20901:
	.size	_ZN2v88internal4wasm15WasmCodeManager31SetMaxCommittedMemoryForTestingEm, .-_ZN2v88internal4wasm15WasmCodeManager31SetMaxCommittedMemoryForTestingEm
	.section	.text._ZN2v88internal4wasm15WasmCodeManager28EstimateNativeModuleCodeSizeEPKNS1_10WasmModuleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmCodeManager28EstimateNativeModuleCodeSizeEPKNS1_10WasmModuleE
	.type	_ZN2v88internal4wasm15WasmCodeManager28EstimateNativeModuleCodeSizeEPKNS1_10WasmModuleE, @function
_ZN2v88internal4wasm15WasmCodeManager28EstimateNativeModuleCodeSizeEPKNS1_10WasmModuleE:
.LFB20902:
	.cfi_startproc
	endbr64
	movq	136(%rdi), %rdx
	movq	144(%rdi), %rsi
	movl	$512, %eax
	cmpq	%rsi, %rdx
	je	.L613
	.p2align 4,,10
	.p2align 3
.L614:
	movl	20(%rdx), %ecx
	addq	$32, %rdx
	leaq	32(%rax,%rcx,4), %rax
	cmpq	%rdx, %rsi
	jne	.L614
.L613:
	movl	68(%rdi), %edx
	movl	$2863311531, %ecx
	addl	$11, %edx
	imulq	%rcx, %rdx
	shrq	$29, %rdx
	movq	%rdx, %rcx
	movl	60(%rdi), %edx
	andl	$4294967232, %ecx
	salq	$9, %rdx
	addq	%rcx, %rdx
	addq	%rdx, %rax
	ret
	.cfi_endproc
.LFE20902:
	.size	_ZN2v88internal4wasm15WasmCodeManager28EstimateNativeModuleCodeSizeEPKNS1_10WasmModuleE, .-_ZN2v88internal4wasm15WasmCodeManager28EstimateNativeModuleCodeSizeEPKNS1_10WasmModuleE
	.section	.text._ZN2v88internal4wasm15WasmCodeManager31EstimateNativeModuleNonCodeSizeEPKNS1_10WasmModuleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmCodeManager31EstimateNativeModuleNonCodeSizeEPKNS1_10WasmModuleE
	.type	_ZN2v88internal4wasm15WasmCodeManager31EstimateNativeModuleNonCodeSizeEPKNS1_10WasmModuleE, @function
_ZN2v88internal4wasm15WasmCodeManager31EstimateNativeModuleNonCodeSizeEPKNS1_10WasmModuleE:
.LFB20903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal4wasm18EstimateStoredSizeEPKNS1_10WasmModuleE@PLT
	movq	%rax, %r8
	movl	68(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	(%rax,%rax,8), %rdx
	leaq	(%rax,%rdx,2), %rax
	leaq	680(%r8,%rax,8), %rax
	ret
	.cfi_endproc
.LFE20903:
	.size	_ZN2v88internal4wasm15WasmCodeManager31EstimateNativeModuleNonCodeSizeEPKNS1_10WasmModuleE, .-_ZN2v88internal4wasm15WasmCodeManager31EstimateNativeModuleNonCodeSizeEPKNS1_10WasmModuleE
	.section	.text._ZNK2v88internal4wasm12NativeModule14SampleCodeSizeEPNS0_8CountersENS2_16CodeSamplingTimeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm12NativeModule14SampleCodeSizeEPNS0_8CountersENS2_16CodeSamplingTimeE
	.type	_ZNK2v88internal4wasm12NativeModule14SampleCodeSizeEPNS0_8CountersENS2_16CodeSamplingTimeE, @function
_ZNK2v88internal4wasm12NativeModule14SampleCodeSizeEPNS0_8CountersENS2_16CodeSamplingTimeE:
.LFB20914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$2, %dl
	je	.L626
	movq	152(%rdi), %r12
	leaq	1928(%rsi), %r13
	shrq	$20, %r12
	cmpb	$1, %dl
	je	.L622
	leaq	1888(%rsi), %r13
	testb	%dl, %dl
	movl	$0, %eax
	cmovne	%rax, %r13
.L622:
	addq	$8, %rsp
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Histogram9AddSampleEi@PLT
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_restore_state
	movq	144(%rdi), %r12
	leaq	1848(%rsi), %r13
	shrq	$20, %r12
	movq	152(%rdi), %r14
	cmpq	$2097151, %r14
	jbe	.L622
	movq	208(%rdi), %rax
	cmpb	$0, 392(%rax)
	jne	.L622
	movq	160(%rdi), %r15
	leaq	1968(%rbx), %rdi
	movq	%r15, %rsi
	shrq	$20, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	leaq	(%r15,%r15,4), %rax
	xorl	%edx, %edx
	leaq	2008(%rbx), %rdi
	leaq	(%rax,%rax,4), %rax
	salq	$2, %rax
	divq	%r14
	movl	%eax, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L622
	.cfi_endproc
.LFE20914:
	.size	_ZNK2v88internal4wasm12NativeModule14SampleCodeSizeEPNS0_8CountersENS2_16CodeSamplingTimeE, .-_ZNK2v88internal4wasm12NativeModule14SampleCodeSizeEPNS0_8CountersENS2_16CodeSamplingTimeE
	.section	.text._ZN2v88internal4wasm12NativeModule25IsRedirectedToInterpreterEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule25IsRedirectedToInterpreterEj
	.type	_ZN2v88internal4wasm12NativeModule25IsRedirectedToInterpreterEj, @function
_ZN2v88internal4wasm12NativeModule25IsRedirectedToInterpreterEj:
.LFB20933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	536(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	632(%r12), %rdx
	testq	%rdx, %rdx
	je	.L629
	movq	208(%r12), %rax
	subl	60(%rax), %ebx
	movl	%ebx, %eax
	movl	%ebx, %ecx
	shrl	$3, %eax
	andl	$7, %ecx
	movzbl	(%rdx,%rax), %r12d
	sarl	%cl, %r12d
	andl	$1, %r12d
.L628:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L629:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L628
	.cfi_endproc
.LFE20933:
	.size	_ZN2v88internal4wasm12NativeModule25IsRedirectedToInterpreterEj, .-_ZN2v88internal4wasm12NativeModule25IsRedirectedToInterpreterEj
	.section	.text._ZNK2v88internal4wasm15WasmCodeManager18LookupNativeModuleEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm15WasmCodeManager18LookupNativeModuleEm
	.type	_ZNK2v88internal4wasm15WasmCodeManager18LookupNativeModuleEm, @function
_ZNK2v88internal4wasm15WasmCodeManager18LookupNativeModuleEm:
.LFB20936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	40(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpq	$0, 120(%r12)
	je	.L639
	movq	96(%r12), %rax
	leaq	88(%r12), %rdi
	testq	%rax, %rax
	jne	.L634
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L653:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L633
.L634:
	cmpq	%rbx, 32(%rax)
	ja	.L653
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L634
.L633:
	cmpq	%rdi, 104(%r12)
	je	.L639
	call	_ZSt18_Rb_tree_decrementPKSt18_Rb_tree_node_base@PLT
	movq	48(%rax), %r12
	cmpq	%rbx, 40(%rax)
	jbe	.L639
	cmpq	%rbx, 32(%rax)
	ja	.L639
.L632:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L632
	.cfi_endproc
.LFE20936:
	.size	_ZNK2v88internal4wasm15WasmCodeManager18LookupNativeModuleEm, .-_ZNK2v88internal4wasm15WasmCodeManager18LookupNativeModuleEm
	.section	.rodata._ZN2v88internal4wasm29NativeModuleModificationScopeC2EPNS1_12NativeModuleE.str1.1,"aMS",@progbits,1
.LC76:
	.string	"success"
	.section	.text._ZN2v88internal4wasm29NativeModuleModificationScopeC2EPNS1_12NativeModuleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm29NativeModuleModificationScopeC2EPNS1_12NativeModuleE
	.type	_ZN2v88internal4wasm29NativeModuleModificationScopeC2EPNS1_12NativeModuleE, @function
_ZN2v88internal4wasm29NativeModuleModificationScopeC2EPNS1_12NativeModuleE:
.LFB20939:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal35FLAG_wasm_write_protect_code_memoryE(%rip)
	movq	%rsi, (%rdi)
	je	.L660
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L660
	movl	672(%rsi), %eax
	leal	1(%rax), %edx
	movl	%edx, 672(%rsi)
	testl	%eax, %eax
	je	.L663
.L660:
	ret
	.p2align 4,,10
	.p2align 3
.L663:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4wasm17WasmCodeAllocator13SetExecutableEb
	testb	%al, %al
	je	.L664
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L664:
	.cfi_restore_state
	leaq	.LC76(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20939:
	.size	_ZN2v88internal4wasm29NativeModuleModificationScopeC2EPNS1_12NativeModuleE, .-_ZN2v88internal4wasm29NativeModuleModificationScopeC2EPNS1_12NativeModuleE
	.globl	_ZN2v88internal4wasm29NativeModuleModificationScopeC1EPNS1_12NativeModuleE
	.set	_ZN2v88internal4wasm29NativeModuleModificationScopeC1EPNS1_12NativeModuleE,_ZN2v88internal4wasm29NativeModuleModificationScopeC2EPNS1_12NativeModuleE
	.section	.text._ZN2v88internal4wasm29NativeModuleModificationScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm29NativeModuleModificationScopeD2Ev
	.type	_ZN2v88internal4wasm29NativeModuleModificationScopeD2Ev, @function
_ZN2v88internal4wasm29NativeModuleModificationScopeD2Ev:
.LFB20942:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal35FLAG_wasm_write_protect_code_memoryE(%rip)
	je	.L671
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L671
	movl	672(%rax), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 672(%rax)
	cmpl	$1, %edx
	je	.L674
.L671:
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4wasm17WasmCodeAllocator13SetExecutableEb
	testb	%al, %al
	je	.L675
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L675:
	.cfi_restore_state
	leaq	.LC76(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20942:
	.size	_ZN2v88internal4wasm29NativeModuleModificationScopeD2Ev, .-_ZN2v88internal4wasm29NativeModuleModificationScopeD2Ev
	.globl	_ZN2v88internal4wasm29NativeModuleModificationScopeD1Ev
	.set	_ZN2v88internal4wasm29NativeModuleModificationScopeD1Ev,_ZN2v88internal4wasm29NativeModuleModificationScopeD2Ev
	.section	.text._ZN2v88internal4wasm16WasmCodeRefScopeC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16WasmCodeRefScopeC2Ev
	.type	_ZN2v88internal4wasm16WasmCodeRefScopeC2Ev, @function
_ZN2v88internal4wasm16WasmCodeRefScopeC2Ev:
.LFB20966:
	.cfi_startproc
	endbr64
	movq	%fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff, %xmm0
	leaq	56(%rdi), %rax
	movq	$1, 16(%rdi)
	movq	%rax, %xmm1
	movq	$0, 24(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 32(%rdi)
	movl	$0x3f800000, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movups	%xmm0, (%rdi)
	movq	%rdi, %fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff
	ret
	.cfi_endproc
.LFE20966:
	.size	_ZN2v88internal4wasm16WasmCodeRefScopeC2Ev, .-_ZN2v88internal4wasm16WasmCodeRefScopeC2Ev
	.globl	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev
	.set	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev,_ZN2v88internal4wasm16WasmCodeRefScopeC2Ev
	.section	.rodata._ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE7reserveEm.str1.1,"aMS",@progbits,1
.LC77:
	.string	"vector::reserve"
	.section	.text._ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE7reserveEm,"axG",@progbits,_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE7reserveEm
	.type	_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE7reserveEm, @function
_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE7reserveEm:
.LFB23024:
	.cfi_startproc
	endbr64
	movabsq	$384307168202282325, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	ja	.L696
	movq	(%rdi), %r15
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movabsq	$-6148914691236517205, %rcx
	subq	%r15, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L697
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L697:
	.cfi_restore_state
	movq	8(%rdi), %r12
	leaq	(%rsi,%rsi,2), %r14
	xorl	%r13d, %r13d
	movq	%r12, %rax
	subq	%r15, %rax
	movq	%rax, -56(%rbp)
	leaq	0(,%r14,8), %rax
	movq	%rax, -64(%rbp)
	testq	%rsi, %rsi
	je	.L680
	movq	%rax, %rdi
	call	_Znwm@PLT
	movq	%rax, %r13
.L680:
	movq	%r13, %r14
	cmpq	%r15, %r12
	je	.L685
	.p2align 4,,10
	.p2align 3
.L684:
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	addq	$24, %r15
	addq	$24, %r14
	movups	%xmm0, -16(%r14)
	movq	-24(%r15), %rcx
	movq	%rcx, -24(%r14)
	movdqu	-16(%r15), %xmm1
	movups	%xmm1, -16(%r14)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	cmpq	%r15, %r12
	jne	.L684
.L685:
	movq	8(%rbx), %r12
	movq	(%rbx), %r15
	cmpq	%r15, %r12
	je	.L682
	.p2align 4,,10
	.p2align 3
.L683:
	movq	%r15, %rdi
	addq	$24, %r15
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	cmpq	%r15, %r12
	jne	.L683
	movq	(%rbx), %r15
.L682:
	testq	%r15, %r15
	je	.L686
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L686:
	movq	-56(%rbp), %r15
	movq	%r13, (%rbx)
	addq	%r13, %r15
	addq	-64(%rbp), %r13
	movq	%r15, 8(%rbx)
	movq	%r13, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L696:
	.cfi_restore_state
	leaq	.LC77(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23024:
	.size	_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE7reserveEm, .-_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE7reserveEm
	.section	.text._ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE7reserveEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE7reserveEm
	.type	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE7reserveEm, @function
_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE7reserveEm:
.LFB23396:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	ja	.L710
	movq	(%rdi), %r12
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L711
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L711:
	.cfi_restore_state
	movq	8(%rdi), %r13
	leaq	0(,%rsi,8), %r14
	subq	%r12, %r13
	testq	%rsi, %rsi
	je	.L705
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r12
	movq	8(%rbx), %rdx
	movq	%rax, %r15
	subq	%r12, %rdx
.L701:
	testq	%rdx, %rdx
	jg	.L712
	testq	%r12, %r12
	jne	.L703
.L704:
	addq	%r15, %r13
	addq	%r15, %r14
	movq	%r15, (%rbx)
	movq	%r13, 8(%rbx)
	movq	%r14, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	memmove@PLT
.L703:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L705:
	movq	%r13, %rdx
	xorl	%r15d, %r15d
	jmp	.L701
.L710:
	leaq	.LC77(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23396:
	.size	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE7reserveEm, .-_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE7reserveEm
	.section	.rodata._ZN2v88internal4wasm16WasmCodeRefScopeD2Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC78:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal4wasm16WasmCodeRefScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16WasmCodeRefScopeD2Ev
	.type	_ZN2v88internal4wasm16WasmCodeRefScopeD2Ev, @function
_ZN2v88internal4wasm16WasmCodeRefScopeD2Ev:
.LFB20970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff
	movq	32(%rdi), %rsi
	leaq	-64(%rbp), %rdi
	movq	$0, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE7reserveEm
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L714
	movq	%rbx, %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L715:
	movq	(%rdx), %rdx
	addq	$1, %rax
	testq	%rdx, %rdx
	jne	.L715
	movq	-64(%rbp), %rsi
	movq	-48(%rbp), %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jbe	.L716
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L764
	leaq	0(,%rax,8), %r13
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L718:
	movq	8(%rbx), %rdx
	addq	$8, %rax
	movq	%rdx, -8(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L718
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L719
	call	_ZdlPv@PLT
.L719:
	leaq	(%r14,%r13), %rsi
	movq	%r14, -64(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rsi, -48(%rbp)
.L720:
	movabsq	$2305843009213693951, %rax
	subq	%r14, %rsi
	movq	%r14, %rdi
	sarq	$3, %rsi
	movslq	%esi, %rsi
	andq	%rax, %rsi
	call	_ZN2v88internal4wasm8WasmCode17DecrementRefCountENS0_6VectorIKPS2_EE
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L728
	call	_ZdlPv@PLT
.L728:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L732
	.p2align 4,,10
	.p2align 3
.L729:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L729
.L732:
	movq	16(%r12), %rax
	movq	8(%r12), %rdi
	xorl	%esi, %esi
	addq	$56, %r12
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%r12), %rdi
	movq	$0, -24(%r12)
	movq	$0, -32(%r12)
	cmpq	%r12, %rdi
	je	.L713
	call	_ZdlPv@PLT
.L713:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L765
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L716:
	.cfi_restore_state
	movq	-56(%rbp), %rcx
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jbe	.L721
	subq	$1, %rdx
	movq	%rbx, %rax
	cmpq	%rcx, %rsi
	je	.L723
	.p2align 4,,10
	.p2align 3
.L722:
	subq	$1, %rdx
	movq	(%rax), %rax
	cmpq	$-1, %rdx
	jne	.L722
	cmpq	%rax, %rbx
	je	.L766
	.p2align 4,,10
	.p2align 3
.L724:
	movq	8(%rbx), %rdx
	addq	$8, %rsi
	movq	%rdx, -8(%rsi)
	movq	(%rbx), %rbx
	cmpq	%rbx, %rax
	jne	.L724
	movq	-56(%rbp), %rsi
	testq	%rax, %rax
	je	.L726
	.p2align 4,,10
	.p2align 3
.L727:
	movq	8(%rax), %rdx
	addq	$8, %rsi
	movq	%rdx, -8(%rsi)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L727
.L726:
	movq	%rsi, -56(%rbp)
	movq	-64(%rbp), %r14
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L721:
	movq	8(%rbx), %rax
	addq	$8, %rsi
	movq	%rax, -8(%rsi)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L721
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %r14
.L733:
	cmpq	%rax, %rsi
	je	.L720
	movq	%rsi, -56(%rbp)
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L714:
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rax
	movq	%rsi, %r14
	jmp	.L733
.L764:
	leaq	.LC78(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L766:
	movq	%rcx, %rsi
	.p2align 4,,10
	.p2align 3
.L723:
	movq	%rbx, %rax
	jmp	.L727
.L765:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20970:
	.size	_ZN2v88internal4wasm16WasmCodeRefScopeD2Ev, .-_ZN2v88internal4wasm16WasmCodeRefScopeD2Ev
	.globl	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev
	.set	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev,_ZN2v88internal4wasm16WasmCodeRefScopeD2Ev
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB23661:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L775
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L769:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L769
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L775:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE23661:
	.size	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.text._ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.type	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, @function
_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E:
.LFB24508:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L793
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L782:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L780
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L778
.L781:
	movq	%rbx, %r12
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L780:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L781
.L778:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L793:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE24508:
	.size	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, .-_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.section	.text._ZN2v88internal9AssemblerD2Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD2Ev
	.type	_ZN2v88internal9AssemblerD2Ev, @function
_ZN2v88internal9AssemblerD2Ev:
.LFB20511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L800
.L797:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L797
.L800:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L798
.L799:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L803
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L798
.L804:
	movq	%rbx, %r13
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L803:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L804
.L798:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L802
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L805
	.p2align 4,,10
	.p2align 3
.L806:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L806
	movq	328(%r12), %rdi
.L805:
	call	_ZdlPv@PLT
.L802:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L807
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L808
	.p2align 4,,10
	.p2align 3
.L809:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L809
	movq	240(%r12), %rdi
.L808:
	call	_ZdlPv@PLT
.L807:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.cfi_endproc
.LFE20511:
	.size	_ZN2v88internal9AssemblerD2Ev, .-_ZN2v88internal9AssemblerD2Ev
	.weak	_ZN2v88internal9AssemblerD1Ev
	.set	_ZN2v88internal9AssemblerD1Ev,_ZN2v88internal9AssemblerD2Ev
	.section	.text._ZN2v88internal4wasm18JumpTableAssemblerD2Ev,"axG",@progbits,_ZN2v88internal4wasm18JumpTableAssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm18JumpTableAssemblerD2Ev
	.type	_ZN2v88internal4wasm18JumpTableAssemblerD2Ev, @function
_ZN2v88internal4wasm18JumpTableAssemblerD2Ev:
.LFB27040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L834
.L831:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L831
.L834:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L832
.L833:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L837
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L832
.L838:
	movq	%rbx, %r13
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L837:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L838
.L832:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L836
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L839
	.p2align 4,,10
	.p2align 3
.L840:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L840
	movq	328(%r12), %rdi
.L839:
	call	_ZdlPv@PLT
.L836:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L841
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L842
	.p2align 4,,10
	.p2align 3
.L843:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L843
	movq	240(%r12), %rdi
.L842:
	call	_ZdlPv@PLT
.L841:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.cfi_endproc
.LFE27040:
	.size	_ZN2v88internal4wasm18JumpTableAssemblerD2Ev, .-_ZN2v88internal4wasm18JumpTableAssemblerD2Ev
	.weak	_ZN2v88internal4wasm18JumpTableAssemblerD1Ev
	.set	_ZN2v88internal4wasm18JumpTableAssemblerD1Ev,_ZN2v88internal4wasm18JumpTableAssemblerD2Ev
	.section	.text._ZN2v88internal14MacroAssemblerD2Ev,"axG",@progbits,_ZN2v88internal14MacroAssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MacroAssemblerD2Ev
	.type	_ZN2v88internal14MacroAssemblerD2Ev, @function
_ZN2v88internal14MacroAssemblerD2Ev:
.LFB20526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L868
.L865:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L865
.L868:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L866
.L867:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L871
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L866
.L872:
	movq	%rbx, %r13
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L871:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L872
.L866:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L870
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L873
	.p2align 4,,10
	.p2align 3
.L874:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L874
	movq	328(%r12), %rdi
.L873:
	call	_ZdlPv@PLT
.L870:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L875
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L876
	.p2align 4,,10
	.p2align 3
.L877:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L877
	movq	240(%r12), %rdi
.L876:
	call	_ZdlPv@PLT
.L875:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.cfi_endproc
.LFE20526:
	.size	_ZN2v88internal14MacroAssemblerD2Ev, .-_ZN2v88internal14MacroAssemblerD2Ev
	.weak	_ZN2v88internal14MacroAssemblerD1Ev
	.set	_ZN2v88internal14MacroAssemblerD1Ev,_ZN2v88internal14MacroAssemblerD2Ev
	.section	.text._ZN2v88internal9AssemblerD0Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD0Ev
	.type	_ZN2v88internal9AssemblerD0Ev, @function
_ZN2v88internal9AssemblerD0Ev:
.LFB20513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L902
.L899:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L899
.L902:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L900
.L901:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L905
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L900
.L906:
	movq	%rbx, %r13
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L905:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L906
.L900:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L904
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L907
	.p2align 4,,10
	.p2align 3
.L908:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L908
	movq	328(%r12), %rdi
.L907:
	call	_ZdlPv@PLT
.L904:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L909
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L910
	.p2align 4,,10
	.p2align 3
.L911:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L911
	movq	240(%r12), %rdi
.L910:
	call	_ZdlPv@PLT
.L909:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE20513:
	.size	_ZN2v88internal9AssemblerD0Ev, .-_ZN2v88internal9AssemblerD0Ev
	.section	.text._ZN2v88internal14MacroAssemblerD0Ev,"axG",@progbits,_ZN2v88internal14MacroAssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MacroAssemblerD0Ev
	.type	_ZN2v88internal14MacroAssemblerD0Ev, @function
_ZN2v88internal14MacroAssemblerD0Ev:
.LFB20528:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L936
.L933:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L933
.L936:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L934
.L935:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L939
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L934
.L940:
	movq	%rbx, %r13
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L939:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L940
.L934:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L938
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L941
	.p2align 4,,10
	.p2align 3
.L942:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L942
	movq	328(%r12), %rdi
.L941:
	call	_ZdlPv@PLT
.L938:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L943
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L944
	.p2align 4,,10
	.p2align 3
.L945:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L945
	movq	240(%r12), %rdi
.L944:
	call	_ZdlPv@PLT
.L943:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE20528:
	.size	_ZN2v88internal14MacroAssemblerD0Ev, .-_ZN2v88internal14MacroAssemblerD0Ev
	.section	.text._ZN2v88internal4wasm18JumpTableAssemblerD0Ev,"axG",@progbits,_ZN2v88internal4wasm18JumpTableAssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm18JumpTableAssemblerD0Ev
	.type	_ZN2v88internal4wasm18JumpTableAssemblerD0Ev, @function
_ZN2v88internal4wasm18JumpTableAssemblerD0Ev:
.LFB27042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L970
.L967:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L967
.L970:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L968
.L969:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L973
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L968
.L974:
	movq	%rbx, %r13
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L973:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L974
.L968:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L972
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L975
	.p2align 4,,10
	.p2align 3
.L976:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L976
	movq	328(%r12), %rdi
.L975:
	call	_ZdlPv@PLT
.L972:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L977
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L978
	.p2align 4,,10
	.p2align 3
.L979:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L979
	movq	240(%r12), %rdi
.L978:
	call	_ZdlPv@PLT
.L977:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE27042:
	.size	_ZN2v88internal4wasm18JumpTableAssemblerD0Ev, .-_ZN2v88internal4wasm18JumpTableAssemblerD0Ev
	.section	.text._ZN2v88internal4wasm18JumpTableAssembler18PatchJumpTableSlotEmjmNS1_8WasmCode11FlushICacheE,"axG",@progbits,_ZN2v88internal4wasm18JumpTableAssembler18PatchJumpTableSlotEmjmNS1_8WasmCode11FlushICacheE,comdat
	.p2align 4
	.weak	_ZN2v88internal4wasm18JumpTableAssembler18PatchJumpTableSlotEmjmNS1_8WasmCode11FlushICacheE
	.type	_ZN2v88internal4wasm18JumpTableAssembler18PatchJumpTableSlotEmjmNS1_8WasmCode11FlushICacheE, @function
_ZN2v88internal4wasm18JumpTableAssembler18PatchJumpTableSlotEmjmNS1_8WasmCode11FlushICacheE:
.LFB20499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-648(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-608(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$2863311531, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$616, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, %eax
	imulq	%rdx, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,2), %edx
	sall	$6, %eax
	sall	$2, %edx
	subl	%edx, %esi
	leal	(%rsi,%rsi,4), %edx
	leal	(%rdx,%rax), %r14d
	movl	$256, %edx
	addq	%rdi, %r14
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal23ExternalAssemblerBufferEPvi@PLT
	movl	$256, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movw	%dx, -636(%rbp)
	movq	%r15, %r8
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-640(%rbp), %rdx
	movw	%ax, -624(%rbp)
	movq	$0, -632(%rbp)
	movl	$257, -640(%rbp)
	call	_ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE@PLT
	movq	-648(%rbp), %rdi
	leaq	16+_ZTVN2v88internal14MacroAssemblerE(%rip), %rax
	movq	%rax, -608(%rbp)
	testq	%rdi, %rdi
	je	.L1001
	movq	(%rdi), %rax
	call	*8(%rax)
.L1001:
	leaq	16+_ZTVN2v88internal4wasm18JumpTableAssemblerE(%rip), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal4wasm18JumpTableAssembler12EmitJumpSlotEm@PLT
	movq	-576(%rbp), %rax
	movl	$5, %esi
	movq	%r13, %rdi
	subq	-592(%rbp), %rax
	subl	%eax, %esi
	call	_ZN2v88internal4wasm18JumpTableAssembler8NopBytesEi@PLT
	testb	%bl, %bl
	jne	.L1040
.L1002:
	movq	-128(%rbp), %rbx
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	leaq	-144(%rbp), %r12
	movq	%rax, -608(%rbp)
	testq	%rbx, %rbx
	je	.L1006
.L1003:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1003
.L1006:
	movq	-184(%rbp), %r12
	leaq	-200(%rbp), %r14
	testq	%r12, %r12
	je	.L1004
.L1005:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L1009
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1004
.L1010:
	movq	%rbx, %r12
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1010
.L1004:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1008
	movq	-208(%rbp), %rax
	movq	-240(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L1011
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L1012
	movq	-280(%rbp), %rdi
.L1011:
	call	_ZdlPv@PLT
.L1008:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1013
	movq	-296(%rbp), %rax
	movq	-328(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L1014
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L1015
	movq	-368(%rbp), %rdi
.L1014:
	call	_ZdlPv@PLT
.L1013:
	movq	%r13, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1041
	addq	$616, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1040:
	.cfi_restore_state
	movl	$5, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	jmp	.L1002
.L1041:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20499:
	.size	_ZN2v88internal4wasm18JumpTableAssembler18PatchJumpTableSlotEmjmNS1_8WasmCode11FlushICacheE, .-_ZN2v88internal4wasm18JumpTableAssembler18PatchJumpTableSlotEmjmNS1_8WasmCode11FlushICacheE
	.section	.text._ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB24591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movabsq	$-6148914691236517205, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movabsq	$384307168202282325, %rsi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r14
	movq	%rdi, -80(%rbp)
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	je	.L1065
	movq	%r12, %rdx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L1056
	movabsq	$9223372036854775800, %rcx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1066
.L1044:
	movq	%rcx, %rdi
	movq	%r8, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rax, -64(%rbp)
	movq	-96(%rbp), %r8
	addq	%rax, %rcx
	addq	$24, %rax
	movq	%rcx, -72(%rbp)
	movq	%rax, -56(%rbp)
.L1055:
	movq	-64(%rbp), %r13
	movdqu	8(%r8), %xmm4
	movq	%r8, %rdi
	leaq	0(%r13,%rdx), %rax
	movq	(%r8), %rdx
	movups	%xmm4, 8(%rax)
	movq	%rdx, (%rax)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	cmpq	%r14, %r12
	je	.L1046
	movq	%r14, %r15
	.p2align 4,,10
	.p2align 3
.L1047:
	pxor	%xmm1, %xmm1
	movq	%r15, %rdi
	addq	$24, %r15
	addq	$24, %r13
	movups	%xmm1, -16(%r13)
	movq	-24(%r15), %rcx
	movq	%rcx, -24(%r13)
	movdqu	-16(%r15), %xmm2
	movups	%xmm2, -16(%r13)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	cmpq	%r15, %r12
	jne	.L1047
	leaq	-24(%r12), %rax
	movq	-64(%rbp), %rsi
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	48(%rsi,%rax,8), %rax
	movq	%rax, -56(%rbp)
.L1046:
	movq	-56(%rbp), %r13
	movq	%r12, %r15
	cmpq	%rbx, %r12
	je	.L1053
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	(%r15), %rcx
	movdqu	8(%r15), %xmm3
	movq	%r15, %rdi
	addq	$24, %r15
	addq	$24, %r13
	movq	%rcx, -24(%r13)
	movups	%xmm3, -16(%r13)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	cmpq	%r15, %rbx
	jne	.L1052
	movq	%rbx, %rax
	movq	-56(%rbp), %rsi
	subq	%r12, %rax
	subq	$24, %rax
	shrq	$3, %rax
	leaq	24(%rsi,%rax,8), %rax
	movq	%rax, -56(%rbp)
.L1053:
	movq	%r14, %r12
	cmpq	%rbx, %r14
	je	.L1054
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	%r12, %rdi
	addq	$24, %r12
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	cmpq	%rbx, %r12
	jne	.L1049
.L1054:
	testq	%r14, %r14
	je	.L1051
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1051:
	movq	-64(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rsi
	movhps	-56(%rbp), %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1066:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L1045
	movq	$24, -56(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1056:
	movl	$24, %ecx
	jmp	.L1044
.L1045:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	imulq	$24, %rsi, %rcx
	jmp	.L1044
.L1065:
	leaq	.LC41(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24591:
	.size	_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZN2v88internal4wasm17WasmCodeAllocatorC2EPNS1_15WasmCodeManagerENS0_13VirtualMemoryEbSt10shared_ptrINS0_8CountersEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmCodeAllocatorC2EPNS1_15WasmCodeManagerENS0_13VirtualMemoryEbSt10shared_ptrINS0_8CountersEE
	.type	_ZN2v88internal4wasm17WasmCodeAllocatorC2EPNS1_15WasmCodeManagerENS0_13VirtualMemoryEbSt10shared_ptrINS0_8CountersEE, @function
_ZN2v88internal4wasm17WasmCodeAllocatorC2EPNS1_15WasmCodeManagerENS0_13VirtualMemoryEbSt10shared_ptrINS0_8CountersEE:
.LFB20631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$8, %rdi
	leaq	48(%rbx), %r14
	subq	$24, %rsp
	movq	%rsi, -8(%rdi)
	call	_ZN2v84base5MutexC1Ev@PLT
	movq	%r14, %xmm0
	movq	16(%r12), %rcx
	movq	$0, 64(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movq	8(%r12), %rax
	movl	$32, %edi
	movups	%xmm0, 48(%rbx)
	movq	%rcx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %xmm0
	movq	%r14, %rsi
	leaq	120(%rbx), %r14
	movq	%rax, %rdi
	movhps	-64(%rbp), %xmm0
	movups	%xmm0, 16(%rax)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	leaq	72(%rbx), %rax
	movq	%r14, %rdi
	addq	$1, 64(%rbx)
	movq	%rax, %xmm0
	leaq	96(%rbx), %rax
	cmpb	$1, %r13b
	movdqu	(%r15), %xmm1
	punpcklqdq	%xmm0, %xmm0
	sbbq	%rsi, %rsi
	movq	$0, 88(%rbx)
	movups	%xmm0, 72(%rbx)
	movq	%rax, %xmm0
	andq	$-3, %rsi
	punpcklqdq	%xmm0, %xmm0
	addq	$4, %rsi
	movq	$0, 112(%rbx)
	movq	$0, 136(%rbx)
	movq	$0, 160(%rbx)
	movb	$0, 168(%rbx)
	movb	%r13b, 169(%rbx)
	movups	%xmm0, 96(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 120(%rbx)
	movups	%xmm0, 144(%rbx)
	movups	%xmm1, 176(%rbx)
	movups	%xmm0, (%r15)
	call	_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE7reserveEm
	movq	128(%rbx), %rsi
	cmpq	136(%rbx), %rsi
	je	.L1069
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 8(%rsi)
	movq	(%r12), %rax
	movdqu	8(%r12), %xmm2
	movq	%rax, (%rsi)
	movups	%xmm2, 8(%rsi)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	addq	$24, 128(%rbx)
.L1070:
	movq	176(%rbx), %rdi
	addq	$24, %rsp
	movl	$1, %esi
	popq	%rbx
	popq	%r12
	addq	$2088, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Histogram9AddSampleEi@PLT
	.p2align 4,,10
	.p2align 3
.L1069:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L1070
	.cfi_endproc
.LFE20631:
	.size	_ZN2v88internal4wasm17WasmCodeAllocatorC2EPNS1_15WasmCodeManagerENS0_13VirtualMemoryEbSt10shared_ptrINS0_8CountersEE, .-_ZN2v88internal4wasm17WasmCodeAllocatorC2EPNS1_15WasmCodeManagerENS0_13VirtualMemoryEbSt10shared_ptrINS0_8CountersEE
	.globl	_ZN2v88internal4wasm17WasmCodeAllocatorC1EPNS1_15WasmCodeManagerENS0_13VirtualMemoryEbSt10shared_ptrINS0_8CountersEE
	.set	_ZN2v88internal4wasm17WasmCodeAllocatorC1EPNS1_15WasmCodeManagerENS0_13VirtualMemoryEbSt10shared_ptrINS0_8CountersEE,_ZN2v88internal4wasm17WasmCodeAllocatorC2EPNS1_15WasmCodeManagerENS0_13VirtualMemoryEbSt10shared_ptrINS0_8CountersEE
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E, @function
_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E:
.LFB24655:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1098
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
.L1080:
	movq	24(%rbx), %rsi
	movq	%rbx, %r13
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	40(%r13), %r12
	movq	16(%rbx), %rbx
	testq	%r12, %r12
	je	.L1075
	movl	112(%r12), %edi
	testl	%edi, %edi
	js	.L1076
	call	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi@PLT
.L1076:
	movq	120(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1077
	call	_ZdaPv@PLT
.L1077:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1078
	call	_ZdaPv@PLT
.L1078:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1079
	call	_ZdaPv@PLT
.L1079:
	movl	$144, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1075:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1080
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1098:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE24655:
	.size	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E, .-_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	.type	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_, @function
_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_:
.LFB24720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	movq	(%r14), %r8
	movq	(%r15), %r15
	movq	$0, (%r14)
	movq	16(%rbx), %r12
	movq	%rax, %r13
	leaq	8(%rbx), %r14
	movq	%r15, 32(%rax)
	movq	%r8, 40(%rax)
	testq	%r12, %r12
	jne	.L1103
	jmp	.L1140
	.p2align 4,,10
	.p2align 3
.L1141:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L1104
.L1142:
	movq	%rax, %r12
.L1103:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r15
	jb	.L1141
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L1142
.L1104:
	testb	%dl, %dl
	jne	.L1143
	cmpq	%rcx, %r15
	jbe	.L1109
.L1117:
	movl	$1, %edi
	cmpq	%r12, %r14
	jne	.L1144
.L1110:
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1143:
	.cfi_restore_state
	cmpq	%r12, 24(%rbx)
	je	.L1117
.L1118:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %r8
	cmpq	32(%rax), %r15
	ja	.L1145
	movq	%rax, %r12
.L1109:
	testq	%r8, %r8
	je	.L1112
	movl	112(%r8), %edi
	testl	%edi, %edi
	js	.L1113
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi@PLT
	movq	-56(%rbp), %r8
.L1113:
	movq	120(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1114
	movq	%r8, -56(%rbp)
	call	_ZdaPv@PLT
	movq	-56(%rbp), %r8
.L1114:
	movq	32(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1115
	movq	%r8, -56(%rbp)
	call	_ZdaPv@PLT
	movq	-56(%rbp), %r8
.L1115:
	movq	16(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1116
	movq	%r8, -56(%rbp)
	call	_ZdaPv@PLT
	movq	-56(%rbp), %r8
.L1116:
	movl	$144, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1112:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1145:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1109
	movl	$1, %edi
	cmpq	%r12, %r14
	je	.L1110
.L1144:
	xorl	%edi, %edi
	cmpq	32(%r12), %r15
	setb	%dil
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1140:
	movq	%r14, %r12
	cmpq	24(%rbx), %r14
	jne	.L1118
	movl	$1, %edi
	jmp	.L1110
	.cfi_endproc
.LFE24720:
	.size	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_, .-_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB24795:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1154
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1148:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1148
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1154:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE24795:
	.size	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZN2v88internal4wasm15WasmCodeManager16FreeNativeModuleENS0_6VectorINS0_13VirtualMemoryEEEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmCodeManager16FreeNativeModuleENS0_6VectorINS0_13VirtualMemoryEEEm
	.type	_ZN2v88internal4wasm15WasmCodeManager16FreeNativeModuleENS0_6VectorINS0_13VirtualMemoryEEEm, @function
_ZN2v88internal4wasm15WasmCodeManager16FreeNativeModuleENS0_6VectorINS0_13VirtualMemoryEEEm:
.LFB20935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	40(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rax, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%rax, -80(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	(%rbx,%rbx,2), %rax
	leaq	80(%r13), %rdx
	leaq	(%r15,%rax,8), %rax
	movq	%rdx, -72(%rbp)
	leaq	88(%r13), %rbx
	movq	%rax, -64(%rbp)
	cmpq	%rax, %r15
	je	.L1180
	.p2align 4,,10
	.p2align 3
.L1181:
	movq	96(%r13), %r11
	movq	8(%r15), %rsi
	testq	%r11, %r11
	je	.L1159
	movq	%rbx, %r14
	movq	%r11, %r12
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1213:
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L1212
.L1182:
	movq	%rax, %r12
.L1160:
	cmpq	32(%r12), %rsi
	ja	.L1213
	movq	16(%r12), %rax
	jnb	.L1214
	movq	%r12, %r14
	testq	%rax, %rax
	jne	.L1182
.L1212:
	cmpq	%r14, 104(%r13)
	jne	.L1176
	cmpq	%r14, %rbx
	je	.L1175
	.p2align 4,,10
	.p2align 3
.L1176:
	movq	16(%r15), %rsi
	movq	0(%r13), %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker18ReleaseReservationEm@PLT
	movq	%r15, %rdi
	addq	$24, %r15
	call	_ZN2v88internal13VirtualMemory4FreeEv@PLT
	cmpq	-64(%rbp), %r15
	jne	.L1181
.L1180:
	movq	-88(%rbp), %rax
	lock subq	%rax, 24(%r13)
	movq	-80(%rbp), %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1175:
	.cfi_restore_state
	movq	24(%r11), %rsi
	movq	-72(%rbp), %rdi
	movq	%r11, -56(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	-56(%rbp), %r11
	movq	%r11, %rdi
	movq	16(%r11), %r11
	movq	%r11, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r11
	testq	%r11, %r11
	jne	.L1175
.L1177:
	movq	$0, 96(%r13)
	movq	%rbx, 104(%r13)
	movq	%rbx, 112(%r13)
	movq	$0, 120(%r13)
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1214:
	movq	24(%r12), %rcx
	testq	%rcx, %rcx
	jne	.L1167
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1215:
	movq	%rcx, %r14
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1173
.L1167:
	cmpq	32(%rcx), %rsi
	jb	.L1215
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L1167
	.p2align 4,,10
	.p2align 3
.L1173:
	testq	%rax, %rax
	je	.L1168
.L1216:
	cmpq	32(%rax), %rsi
	ja	.L1172
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1216
.L1168:
	cmpq	%r12, 104(%r13)
	jne	.L1211
	cmpq	%r14, %rbx
	jne	.L1211
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1178:
	movq	%r12, %rdi
	movq	%r12, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, %r12
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 120(%r13)
.L1211:
	cmpq	%r14, %r12
	jne	.L1178
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1172:
	movq	24(%rax), %rax
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1159:
	cmpq	104(%r13), %rbx
	jne	.L1176
	jmp	.L1177
	.cfi_endproc
.LFE20935:
	.size	_ZN2v88internal4wasm15WasmCodeManager16FreeNativeModuleENS0_6VectorINS0_13VirtualMemoryEEEm, .-_ZN2v88internal4wasm15WasmCodeManager16FreeNativeModuleENS0_6VectorINS0_13VirtualMemoryEEEm
	.section	.text._ZN2v88internal4wasm17WasmCodeAllocatorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmCodeAllocatorD2Ev
	.type	_ZN2v88internal4wasm17WasmCodeAllocatorD2Ev, @function
_ZN2v88internal4wasm17WasmCodeAllocatorD2Ev:
.LFB20634:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	144(%rbx), %rcx
	movq	120(%rbx), %rsi
	movq	128(%rbx), %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
	imulq	%rax, %rdx
	call	_ZN2v88internal4wasm15WasmCodeManager16FreeNativeModuleENS0_6VectorINS0_13VirtualMemoryEEEm
	movq	184(%rbx), %r12
	testq	%r12, %r12
	je	.L1219
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L1220
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1240
	.p2align 4,,10
	.p2align 3
.L1219:
	movq	128(%rbx), %r13
	movq	120(%rbx), %r12
	cmpq	%r12, %r13
	je	.L1225
	.p2align 4,,10
	.p2align 3
.L1226:
	movq	%r12, %rdi
	addq	$24, %r12
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	cmpq	%r12, %r13
	jne	.L1226
	movq	120(%rbx), %r12
.L1225:
	testq	%r12, %r12
	je	.L1227
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1227:
	movq	96(%rbx), %r12
	leaq	96(%rbx), %r13
	cmpq	%r12, %r13
	je	.L1231
	.p2align 4,,10
	.p2align 3
.L1228:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	jne	.L1228
.L1231:
	movq	72(%rbx), %r12
	leaq	72(%rbx), %r13
	cmpq	%r13, %r12
	je	.L1229
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	cmpq	%r13, %r12
	jne	.L1230
.L1229:
	movq	48(%rbx), %r12
	leaq	48(%rbx), %r13
	cmpq	%r13, %r12
	je	.L1232
	.p2align 4,,10
	.p2align 3
.L1233:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	cmpq	%r13, %r12
	jne	.L1233
.L1232:
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1220:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1219
.L1240:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L1223
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1224:
	cmpl	$1, %eax
	jne	.L1219
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1219
.L1223:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1224
	.cfi_endproc
.LFE20634:
	.size	_ZN2v88internal4wasm17WasmCodeAllocatorD2Ev, .-_ZN2v88internal4wasm17WasmCodeAllocatorD2Ev
	.globl	_ZN2v88internal4wasm17WasmCodeAllocatorD1Ev
	.set	_ZN2v88internal4wasm17WasmCodeAllocatorD1Ev,_ZN2v88internal4wasm17WasmCodeAllocatorD2Ev
	.section	.text._ZN2v88internal4wasm12NativeModuleD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModuleD2Ev
	.type	_ZN2v88internal4wasm12NativeModuleD2Ev, @function
_ZN2v88internal4wasm12NativeModuleD2Ev:
.LFB20869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	520(%rdi), %rdi
	call	_ZN2v88internal4wasm16CompilationState16AbortCompilationEv@PLT
	movq	664(%r12), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE@PLT
	movq	528(%r12), %r13
	movq	$0, 528(%r12)
	testq	%r13, %r13
	je	.L1242
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm22WasmImportWrapperCacheD1Ev@PLT
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1242:
	movq	640(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1243
	call	_ZdlPv@PLT
.L1243:
	movq	632(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1244
	call	_ZdaPv@PLT
.L1244:
	movq	624(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1245
	call	_ZdaPv@PLT
.L1245:
	movq	592(%r12), %rsi
	leaq	576(%r12), %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	leaq	536(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	528(%r12), %r13
	testq	%r13, %r13
	je	.L1246
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm22WasmImportWrapperCacheD1Ev@PLT
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1246:
	movq	520(%r12), %r13
	testq	%r13, %r13
	je	.L1247
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm16CompilationStateD1Ev@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1247:
	movq	240(%r12), %r13
	testq	%r13, %r13
	je	.L1249
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1250
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1306
	.p2align 4,,10
	.p2align 3
.L1249:
	movq	224(%r12), %r14
	testq	%r14, %r14
	je	.L1255
	movq	72(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1256
	call	_ZdlPv@PLT
.L1256:
	movq	48(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1257
	call	_ZdlPv@PLT
.L1257:
	movq	32(%r14), %rbx
	movq	24(%r14), %r13
	cmpq	%r13, %rbx
	je	.L1258
	.p2align 4,,10
	.p2align 3
.L1262:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1259
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1262
	movq	24(%r14), %r13
.L1258:
	testq	%r13, %r13
	je	.L1263
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1263:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1264
	call	_ZdlPv@PLT
.L1264:
	movl	$104, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1255:
	movq	216(%r12), %r13
	testq	%r13, %r13
	je	.L1266
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1267
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1307
.L1266:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm17WasmCodeAllocatorD1Ev
	.p2align 4,,10
	.p2align 3
.L1259:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1262
	movq	24(%r14), %r13
	jmp	.L1258
	.p2align 4,,10
	.p2align 3
.L1267:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1266
.L1307:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1270
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1271:
	cmpl	$1, %eax
	jne	.L1266
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1250:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1249
.L1306:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1253
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1254:
	cmpl	$1, %eax
	jne	.L1249
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1253:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1270:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1271
	.cfi_endproc
.LFE20869:
	.size	_ZN2v88internal4wasm12NativeModuleD2Ev, .-_ZN2v88internal4wasm12NativeModuleD2Ev
	.globl	_ZN2v88internal4wasm12NativeModuleD1Ev
	.set	_ZN2v88internal4wasm12NativeModuleD1Ev,_ZN2v88internal4wasm12NativeModuleD2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB27094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L1308
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm12NativeModuleD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$680, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1308:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27094:
	.size	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE17_M_emplace_uniqueIJS0_ImS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE17_M_emplace_uniqueIJS0_ImS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE17_M_emplace_uniqueIJS0_ImS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_
	.type	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE17_M_emplace_uniqueIJS0_ImS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_, @function
_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE17_M_emplace_uniqueIJS0_ImS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_:
.LFB24804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$56, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	(%r14), %r15
	movdqu	8(%r14), %xmm0
	leaq	8(%rbx), %r14
	movq	16(%rbx), %r12
	movq	%rax, %r13
	movq	%r15, 32(%rax)
	movups	%xmm0, 40(%rax)
	testq	%r12, %r12
	jne	.L1313
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1334:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L1314
.L1335:
	movq	%rax, %r12
.L1313:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r15
	jb	.L1334
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L1335
.L1314:
	testb	%dl, %dl
	jne	.L1336
	cmpq	%rcx, %r15
	jbe	.L1319
.L1322:
	movl	$1, %edi
	cmpq	%r12, %r14
	jne	.L1337
.L1320:
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1336:
	.cfi_restore_state
	cmpq	24(%rbx), %r12
	je	.L1322
.L1323:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	32(%rax), %r15
	ja	.L1338
	movq	%rax, %r12
.L1319:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1338:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1319
	movl	$1, %edi
	cmpq	%r12, %r14
	je	.L1320
.L1337:
	xorl	%edi, %edi
	cmpq	32(%r12), %r15
	setb	%dil
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1333:
	movq	%r14, %r12
	cmpq	24(%rbx), %r14
	jne	.L1323
	movl	$1, %edi
	jmp	.L1320
	.cfi_endproc
.LFE24804:
	.size	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE17_M_emplace_uniqueIJS0_ImS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_, .-_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE17_M_emplace_uniqueIJS0_ImS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_
	.section	.text._ZN2v88internal4wasm15WasmCodeManager11AssignRangeENS_4base13AddressRegionEPNS1_12NativeModuleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmCodeManager11AssignRangeENS_4base13AddressRegionEPNS1_12NativeModuleE
	.type	_ZN2v88internal4wasm15WasmCodeManager11AssignRangeENS_4base13AddressRegionEPNS1_12NativeModuleE, @function
_ZN2v88internal4wasm15WasmCodeManager11AssignRangeENS_4base13AddressRegionEPNS1_12NativeModuleE:
.LFB20888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	40(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	addq	%r13, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	-80(%rbp), %rsi
	leaq	80(%rbx), %rdi
	movq	%r13, -80(%rbp)
	movq	%r12, -72(%rbp)
	movq	%r14, -64(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE17_M_emplace_uniqueIJS0_ImS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1342
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1342:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20888:
	.size	_ZN2v88internal4wasm15WasmCodeManager11AssignRangeENS_4base13AddressRegionEPNS1_12NativeModuleE, .-_ZN2v88internal4wasm15WasmCodeManager11AssignRangeENS_4base13AddressRegionEPNS1_12NativeModuleE
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB24843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rdi, -88(%rbp)
	movabsq	$1152921504606846975, %rdi
	movq	%rax, -80(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rcx, -56(%rbp)
	cmpq	%rdi, %rax
	je	.L1382
	movq	%rsi, %r8
	movq	%rsi, %r12
	movq	%rsi, %rbx
	subq	-56(%rbp), %r8
	testq	%rax, %rax
	je	.L1360
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L1383
.L1345:
	movq	%rsi, %rdi
	movq	%rdx, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rsi, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rsi
	movq	-96(%rbp), %r8
	movq	%rax, -64(%rbp)
	movq	-104(%rbp), %rdx
	addq	%rax, %rsi
	movq	%rsi, -72(%rbp)
	leaq	8(%rax), %rsi
.L1359:
	movq	(%rdx), %rax
	movq	-64(%rbp), %r14
	movq	$0, (%rdx)
	movq	-56(%rbp), %r15
	movq	%rax, (%r14,%r8)
	cmpq	%r15, %r12
	je	.L1347
	.p2align 4,,10
	.p2align 3
.L1353:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L1348
	movl	112(%r13), %edi
	testl	%edi, %edi
	js	.L1349
	call	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi@PLT
.L1349:
	movq	120(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1350
	call	_ZdaPv@PLT
.L1350:
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1351
	call	_ZdaPv@PLT
.L1351:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1352
	call	_ZdaPv@PLT
.L1352:
	movl	$144, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1348:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r12
	jne	.L1353
	movq	-64(%rbp), %rcx
	movq	%r12, %rax
	subq	-56(%rbp), %rax
	leaq	8(%rcx,%rax), %rsi
.L1347:
	movq	-80(%rbp), %rax
	cmpq	%rax, %r12
	je	.L1354
	movq	%rax, %r15
	subq	%r12, %r15
	leaq	-8(%r15), %r8
	movq	%r8, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r8, %r8
	je	.L1362
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1356:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1356
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rbx
	leaq	(%rsi,%rbx), %rdx
	addq	%r12, %rbx
	cmpq	%rax, %rdi
	je	.L1357
.L1355:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L1357:
	leaq	8(%rsi,%r8), %rsi
.L1354:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1358
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rsi
.L1358:
	movq	-64(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movq	%rsi, %xmm2
	movq	-72(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1383:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L1346
	movq	$0, -72(%rbp)
	movl	$8, %esi
	movq	$0, -64(%rbp)
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1360:
	movl	$8, %esi
	jmp	.L1345
.L1362:
	movq	%rsi, %rdx
	jmp	.L1355
.L1346:
	cmpq	%rdi, %r9
	movq	%rdi, %rsi
	cmovbe	%r9, %rsi
	salq	$3, %rsi
	jmp	.L1345
.L1382:
	leaq	.LC41(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24843:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZN2v84base11SmallVectorINS0_13AddressRegionELm1EE4GrowEm,"axG",@progbits,_ZN2v84base11SmallVectorINS0_13AddressRegionELm1EE4GrowEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorINS0_13AddressRegionELm1EE4GrowEm
	.type	_ZN2v84base11SmallVectorINS0_13AddressRegionELm1EE4GrowEm, @function
_ZN2v84base11SmallVectorINS0_13AddressRegionELm1EE4GrowEm:
.LFB25628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rdi), %r12
	movq	16(%rdi), %rdi
	subq	%rax, %r12
	subq	%rax, %rdi
	sarq	$4, %rdi
	addq	%rdi, %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	salq	$4, %rax
	movq	%rax, %rdi
	movq	%rax, %r14
	call	malloc@PLT
	movq	(%rbx), %r15
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%r15, %rsi
	call	memcpy@PLT
	leaq	24(%rbx), %rax
	cmpq	%rax, %r15
	je	.L1385
	movq	%r15, %rdi
	call	free@PLT
.L1385:
	leaq	0(%r13,%r12), %rax
	movq	%r13, (%rbx)
	addq	%r14, %r13
	movq	%r13, 16(%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25628:
	.size	_ZN2v84base11SmallVectorINS0_13AddressRegionELm1EE4GrowEm, .-_ZN2v84base11SmallVectorINS0_13AddressRegionELm1EE4GrowEm
	.section	.text._ZN2v88internal4wasm17WasmCodeAllocator8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmCodeAllocator8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE
	.type	_ZN2v88internal4wasm17WasmCodeAllocator8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE, @function
_ZN2v88internal4wasm17WasmCodeAllocator8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE:
.LFB20645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	(%rsi,%rdx,8), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-128(%rbp), %rbx
	movq	%rbx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	subq	$168, %rsp
	movq	%rdi, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	cmpq	%rsi, %r14
	je	.L1401
	movq	%rsi, %r15
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	(%r15), %r12
	movl	$204, %esi
	addq	$8, %r15
	movq	8(%r12), %rdx
	movq	(%r12), %rdi
	call	memset@PLT
	movq	8(%r12), %rsi
	movq	(%r12), %rdi
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	movq	8(%r12), %rdx
	movq	(%r12), %rsi
	movq	%rbx, %rdi
	addq	%rdx, %r13
	call	_ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE
	cmpq	%r15, %r14
	jne	.L1389
.L1388:
	movq	-176(%rbp), %r15
	lock addq	%r13, 160(%r15)
	leaq	8(%r15), %rax
	addq	$96, %r15
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	-128(%rbp), %r14
	movq	%rax, %r12
	movq	%rax, -152(%rbp)
	subq	$1, %rax
	movq	%rax, -144(%rbp)
	leaq	-56(%rbp), %rax
	negq	%r12
	movq	%rax, -184(%rbp)
	leaq	-72(%rbp), %rax
	movq	%rax, %xmm1
	movq	%rax, -136(%rbp)
	movhps	-184(%rbp), %xmm1
	movaps	%xmm1, -208(%rbp)
	cmpq	%rbx, %r14
	je	.L1399
	movq	%rbx, -160(%rbp)
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	(%r14), %r14
	cmpq	-160(%rbp), %r14
	je	.L1408
.L1398:
	movq	24(%r14), %r13
	movq	16(%r14), %rsi
	movq	%r15, %rdi
	movq	16(%r14), %rbx
	movq	%r13, %rdx
	call	_ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE
	movq	-152(%rbp), %rcx
	movq	%rbx, %rdi
	andq	%r12, %rdi
	leaq	-1(%rcx,%rax), %rsi
	andq	%r12, %rsi
	cmpq	%rdi, %rsi
	cmovb	%rdi, %rsi
	addq	%rdx, %rax
	leaq	(%rbx,%r13), %rdx
	addq	-144(%rbp), %rdx
	andq	%r12, %rax
	andq	%r12, %rdx
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1394
	subq	%rsi, %rdx
	movq	-176(%rbp), %rax
	movq	%rdx, %r13
	lock subq	%rdx, 144(%rax)
	cmpb	$0, _ZN2v88internal14FLAG_perf_profE(%rip)
	movq	-184(%rbp), %rcx
	movdqa	-208(%rbp), %xmm2
	movq	(%rax), %rbx
	movq	%rcx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rdx, -64(%rbp)
	movaps	%xmm2, -96(%rbp)
	jne	.L1394
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movq	%rax, %rdi
	lock subq	%r13, 24(%rbx)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	-168(%rbp), %rsi
	movq	%r13, %rdx
	call	*72(%rax)
	testb	%al, %al
	je	.L1396
	movq	-96(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1394
	call	free@PLT
	movq	(%r14), %r14
	cmpq	-160(%rbp), %r14
	jne	.L1398
	.p2align 4,,10
	.p2align 3
.L1408:
	movq	-160(%rbp), %rbx
.L1399:
	movq	-192(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-128(%rbp), %r12
	cmpq	%rbx, %r12
	je	.L1387
	.p2align 4,,10
	.p2align 3
.L1392:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	jne	.L1392
.L1387:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1409
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1396:
	.cfi_restore_state
	leaq	.LC75(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1401:
	xorl	%r13d, %r13d
	jmp	.L1388
.L1409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20645:
	.size	_ZN2v88internal4wasm17WasmCodeAllocator8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE, .-_ZN2v88internal4wasm17WasmCodeAllocator8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE
	.section	.text._ZN2v88internal4wasm12NativeModule8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE
	.type	_ZN2v88internal4wasm12NativeModule8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE, @function
_ZN2v88internal4wasm12NativeModule8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE:
.LFB20934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	call	_ZN2v88internal4wasm17WasmCodeAllocator8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE
	leaq	536(%r14), %rax
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	576(%r14), %rcx
	leaq	(%r15,%rbx,8), %rax
	movq	%rcx, -88(%rbp)
	leaq	584(%r14), %rcx
	movq	%rax, -72(%rbp)
	movq	%rcx, -56(%rbp)
	cmpq	%rax, %r15
	je	.L1435
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	(%r15), %rax
	movq	592(%r14), %rsi
	movq	-56(%rbp), %r12
	movq	(%rax), %rcx
	testq	%rsi, %rsi
	je	.L1437
	movq	%rsi, %rbx
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L1476
.L1438:
	movq	%rax, %rbx
.L1413:
	cmpq	32(%rbx), %rcx
	ja	.L1477
	movq	16(%rbx), %rax
	jnb	.L1478
	movq	%rbx, %r12
	testq	%rax, %rax
	jne	.L1438
.L1476:
	cmpq	-56(%rbp), %r12
	sete	%al
.L1412:
	cmpq	%r12, 600(%r14)
	jne	.L1428
	testb	%al, %al
	je	.L1428
.L1424:
	movq	-88(%rbp), %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	-56(%rbp), %rax
	movq	$0, 592(%r14)
	movq	$0, 616(%r14)
	movq	%rax, 600(%r14)
	movq	%rax, 608(%r14)
.L1428:
	addq	$8, %r15
	cmpq	%r15, -72(%rbp)
	jne	.L1436
.L1435:
	movq	-80(%rbp), %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1478:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L1417
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1479:
	movq	%rdx, %r12
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L1423
.L1417:
	cmpq	32(%rdx), %rcx
	jb	.L1479
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1417
	.p2align 4,,10
	.p2align 3
.L1423:
	testq	%rax, %rax
	je	.L1418
.L1480:
	cmpq	32(%rax), %rcx
	ja	.L1422
	movq	%rax, %rbx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1480
.L1418:
	cmpq	%rbx, 600(%r14)
	jne	.L1475
	cmpq	-56(%rbp), %r12
	jne	.L1475
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1429:
	movq	%rbx, %rdi
	movq	%rbx, %r13
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	40(%rax), %r13
	movq	%rax, -64(%rbp)
	testq	%r13, %r13
	je	.L1430
	movl	112(%r13), %edi
	testl	%edi, %edi
	js	.L1431
	call	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi@PLT
.L1431:
	movq	120(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1432
	call	_ZdaPv@PLT
.L1432:
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1433
	call	_ZdaPv@PLT
.L1433:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1434
	call	_ZdaPv@PLT
.L1434:
	movl	$144, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1430:
	movq	-64(%rbp), %rdi
	call	_ZdlPv@PLT
	subq	$1, 616(%r14)
.L1475:
	cmpq	%r12, %rbx
	jne	.L1429
	addq	$8, %r15
	cmpq	%r15, -72(%rbp)
	jne	.L1436
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1422:
	movq	24(%rax), %rax
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1437:
	movl	$1, %eax
	jmp	.L1412
	.cfi_endproc
.LFE20934:
	.size	_ZN2v88internal4wasm12NativeModule8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE, .-_ZN2v88internal4wasm12NativeModule8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE
	.section	.text._ZNSt6vectorIN2v88internal4wasm12NativeModule13CodeSpaceDataESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm12NativeModule13CodeSpaceDataESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm12NativeModule13CodeSpaceDataESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm12NativeModule13CodeSpaceDataESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm12NativeModule13CodeSpaceDataESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB25821:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$384307168202282325, %rcx
	cmpq	%rcx, %rax
	je	.L1498
	movq	%r12, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L1490
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1499
.L1483:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	24(%r13), %rbx
.L1489:
	movdqu	(%rdx), %xmm2
	movq	16(%rdx), %rax
	movups	%xmm2, 0(%r13,%r8)
	movq	%rax, 16(%r13,%r8)
	cmpq	%r14, %r12
	je	.L1485
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L1486:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L1486
	leaq	-24(%r12), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	48(%r13,%rax,8), %rbx
.L1485:
	cmpq	%rsi, %r12
	je	.L1487
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-24(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	24(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L1487:
	testq	%r14, %r14
	je	.L1488
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1488:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1499:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L1484
	movq	$0, -56(%rbp)
	movl	$24, %ebx
	xorl	%r13d, %r13d
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1490:
	movl	$24, %ebx
	jmp	.L1483
.L1484:
	cmpq	%rcx, %rdi
	movq	%rcx, %rbx
	cmovbe	%rdi, %rbx
	imulq	$24, %rbx, %rbx
	jmp	.L1483
.L1498:
	leaq	.LC41(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE25821:
	.size	_ZNSt6vectorIN2v88internal4wasm12NativeModule13CodeSpaceDataESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm12NativeModule13CodeSpaceDataESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm:
.LFB26369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1501
	movq	(%rbx), %r8
.L1502:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L1511
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L1512:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1501:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1525
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1526
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L1504:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1506
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1508:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1509:
	testq	%rsi, %rsi
	je	.L1506
.L1507:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1508
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1514
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1507
	.p2align 4,,10
	.p2align 3
.L1506:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L1510
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1510:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1513
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L1513:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1514:
	movq	%rdx, %rdi
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1525:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L1504
.L1526:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE26369:
	.size	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.section	.text._ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE
	.type	_ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE, @function
_ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE:
.LFB20972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff, %rbx
	movq	16(%rbx), %rsi
	divq	%rsi
	movq	8(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L1528
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L1530
	.p2align 4,,10
	.p2align 3
.L1540:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1528
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r13
	jne	.L1528
.L1530:
	cmpq	%r12, %rdi
	jne	.L1540
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1528:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	8(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r12, 8(%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	$0, (%rax)
	call	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	lock addl	$1, 140(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20972:
	.size	_ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE, .-_ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE
	.section	.rodata._ZN2v88internal4wasm12NativeModule17PublishCodeLockedESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE.str1.1,"aMS",@progbits,1
.LC79:
	.string	"!prior_code->DecRef()"
	.section	.text._ZN2v88internal4wasm12NativeModule17PublishCodeLockedESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule17PublishCodeLockedESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE
	.type	_ZN2v88internal4wasm12NativeModule17PublishCodeLockedESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE, @function
_ZN2v88internal4wasm12NativeModule17PublishCodeLockedESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE:
.LFB20789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	56(%r12), %r13d
	cmpl	$-1, %r13d
	je	.L1542
	movq	208(%rdi), %rdx
	movl	60(%rdx), %eax
	cmpl	%r13d, %eax
	jbe	.L1583
.L1542:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE
	movq	(%r14), %r12
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdx
	leaq	576(%rbx), %rdi
	movq	(%r12), %rax
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1584
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1583:
	.cfi_restore_state
	movl	60(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L1543
	movq	128(%r12), %r8
	testq	%r8, %r8
	je	.L1543
	movq	120(%r12), %rcx
	movq	8(%r12), %rsi
	movq	%r8, %rdx
	movq	(%r12), %rdi
	call	_ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE@PLT
	testl	%eax, %eax
	js	.L1585
	movl	112(%r12), %edx
	testl	%edx, %edx
	jns	.L1586
	movl	%eax, 112(%r12)
	movq	(%r14), %r12
	movq	208(%rbx), %rdx
	movl	56(%r12), %r13d
	movl	60(%rdx), %eax
.L1543:
	movl	%r13d, %r8d
	subl	%eax, %r8d
	movq	624(%rbx), %rax
	movl	%r8d, %ecx
	leaq	(%rax,%rcx,8), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L1546
	movzbl	136(%r12), %ecx
	cmpb	%cl, 136(%r15)
	jge	.L1587
	movq	%r12, (%rax)
	movq	%r15, %rdi
	movl	%r8d, -68(%rbp)
	call	_ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE
	movl	140(%r15), %eax
	leaq	140(%r15), %rdx
	movl	-68(%rbp), %r8d
.L1552:
	cmpl	$1, %eax
	je	.L1588
	leal	-1(%rax), %ecx
	lock cmpxchgl	%ecx, (%rdx)
	jne	.L1552
.L1564:
	movq	(%r14), %r12
	movq	632(%rbx), %rsi
	movl	56(%r12), %r13d
	movl	60(%r12), %edi
	testq	%rsi, %rsi
	je	.L1554
	movq	208(%rbx), %rax
	movl	%r13d, %ecx
	subl	60(%rax), %ecx
	movl	%ecx, %eax
	andl	$7, %ecx
	shrl	$3, %eax
	addq	%rsi, %rax
	movzbl	(%rax), %r9d
	btl	%ecx, %r9d
	movl	%r9d, %edx
	jnc	.L1554
	cmpl	$4, %edi
	jne	.L1542
.L1557:
	movl	$1, %esi
	sall	%cl, %esi
	orl	%esi, %edx
	movb	%dl, (%rax)
	movq	504(%rbx), %rax
	movq	(%r14), %r12
	jmp	.L1558
	.p2align 4,,10
	.p2align 3
.L1546:
	movq	%r12, (%rax)
	jmp	.L1564
	.p2align 4,,10
	.p2align 3
.L1554:
	movq	504(%rbx), %rax
	testq	%rax, %rax
	je	.L1589
	cmpl	$4, %edi
	je	.L1582
.L1558:
	movq	(%r12), %rdx
	movq	(%rax), %rdi
	movl	$1, %ecx
	movl	%r8d, %esi
	call	_ZN2v88internal4wasm18JumpTableAssembler18PatchJumpTableSlotEmjmNS1_8WasmCode11FlushICacheE
	movq	(%r14), %r12
	jmp	.L1542
	.p2align 4,,10
	.p2align 3
.L1587:
	cmpl	$4, 60(%r12)
	jne	.L1542
	movq	632(%rbx), %rsi
	jmp	.L1553
	.p2align 4,,10
	.p2align 3
.L1589:
	cmpl	$4, %edi
	jne	.L1542
.L1582:
	movq	208(%rbx), %rdx
.L1553:
	testq	%rsi, %rsi
	je	.L1559
	subl	60(%rdx), %r13d
	movl	%r13d, %eax
	movl	%r13d, %ecx
	shrl	$3, %eax
	andl	$7, %ecx
	addq	%rsi, %rax
	movzbl	(%rax), %edx
	jmp	.L1557
.L1588:
	movq	%r15, %rdi
	movl	%r8d, -68(%rbp)
	call	_ZN2v88internal4wasm8WasmCode27DecRefOnPotentiallyDeadCodeEv
	movl	-68(%rbp), %r8d
	testb	%al, %al
	je	.L1564
	leaq	.LC79(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1585:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1559:
	movl	68(%rdx), %r12d
	movl	%r8d, -68(%rbp)
	addl	$7, %r12d
	shrl	$3, %r12d
	movq	%r12, %rdi
	call	_Znam@PLT
	testq	%r12, %r12
	movl	-68(%rbp), %r8d
	movq	%rax, %rcx
	je	.L1563
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movl	%r8d, -68(%rbp)
	call	memset@PLT
	movl	-68(%rbp), %r8d
	movq	%rax, %rcx
.L1563:
	movq	632(%rbx), %rdi
	movq	%rcx, 632(%rbx)
	testq	%rdi, %rdi
	je	.L1590
	movl	%r8d, -68(%rbp)
	call	_ZdaPv@PLT
	movq	208(%rbx), %rax
	movl	-68(%rbp), %r8d
	subl	60(%rax), %r13d
	movl	%r13d, %eax
	movl	%r13d, %ecx
	shrl	$3, %eax
	addq	632(%rbx), %rax
	andl	$7, %ecx
	movzbl	(%rax), %edx
	jmp	.L1557
.L1586:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1590:
	movq	208(%rbx), %rax
	subl	60(%rax), %r13d
	movl	%r13d, %eax
	shrl	$3, %eax
	addq	%rcx, %rax
	movl	%r13d, %ecx
	movzbl	(%rax), %edx
	andl	$7, %ecx
	jmp	.L1557
.L1584:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20789:
	.size	_ZN2v88internal4wasm12NativeModule17PublishCodeLockedESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE, .-_ZN2v88internal4wasm12NativeModule17PublishCodeLockedESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE
	.section	.text._ZNK2v88internal4wasm12NativeModule7GetCodeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm12NativeModule7GetCodeEj
	.type	_ZNK2v88internal4wasm12NativeModule7GetCodeEj, @function
_ZNK2v88internal4wasm12NativeModule7GetCodeEj:
.LFB20801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	536(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	208(%r12), %rax
	subl	60(%rax), %ebx
	movq	624(%r12), %rax
	movq	(%rax,%rbx,8), %r12
	testq	%r12, %r12
	je	.L1592
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE
.L1592:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20801:
	.size	_ZNK2v88internal4wasm12NativeModule7GetCodeEj, .-_ZNK2v88internal4wasm12NativeModule7GetCodeEj
	.section	.text._ZNK2v88internal4wasm12NativeModule6LookupEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm12NativeModule6LookupEm
	.type	_ZNK2v88internal4wasm12NativeModule6LookupEm, @function
_ZNK2v88internal4wasm12NativeModule6LookupEm:
.LFB20863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	536(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	592(%r12), %rax
	leaq	584(%r12), %rdi
	testq	%rax, %rax
	jne	.L1599
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1613:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1598
.L1599:
	cmpq	%rbx, 32(%rax)
	ja	.L1613
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1599
.L1598:
	cmpq	%rdi, 600(%r12)
	je	.L1605
	call	_ZSt18_Rb_tree_decrementPKSt18_Rb_tree_node_base@PLT
	movq	40(%rax), %r12
	movq	(%r12), %rax
	cmpq	%rbx, %rax
	ja	.L1605
	addq	8(%r12), %rax
	cmpq	%rbx, %rax
	jbe	.L1605
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L1605:
	xorl	%r12d, %r12d
.L1602:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20863:
	.size	_ZNK2v88internal4wasm12NativeModule6LookupEm, .-_ZNK2v88internal4wasm12NativeModule6LookupEm
	.section	.text._ZN2v88internal4wasm12NativeModule12LogWasmCodesEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule12LogWasmCodesEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm12NativeModule12LogWasmCodesEPNS0_7IsolateE, @function
_ZN2v88internal4wasm12NativeModule12LogWasmCodesEPNS0_7IsolateE:
.LFB20709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rsi, %rdi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm8WasmCode14ShouldBeLoggedEPNS0_7IsolateE
	testb	%al, %al
	je	.L1614
	movq	208(%r15), %rax
	leaq	536(%r15), %r12
	movl	60(%rax), %ebx
	movl	68(%rax), %edx
	addl	%ebx, %edx
	movl	%edx, -132(%rbp)
	movq	%fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff, %rax
	movq	$1, -112(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	%rax, %rsi
	leaq	-72(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movl	$0x3f800000, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rsi, %fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff
	cmpl	%edx, %ebx
	jl	.L1620
	jmp	.L1619
	.p2align 4,,10
	.p2align 3
.L1625:
	movq	%r14, %rdi
	addl	$1, %ebx
	call	_ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal4wasm8WasmCode7LogCodeEPNS0_7IsolateE
	cmpl	%ebx, -132(%rbp)
	je	.L1619
.L1620:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	208(%r15), %rax
	movl	%ebx, %ecx
	subl	60(%rax), %ecx
	movq	624(%r15), %rax
	movq	(%rax,%rcx,8), %r14
	testq	%r14, %r14
	jne	.L1625
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpl	%ebx, -132(%rbp)
	jne	.L1620
.L1619:
	movq	-144(%rbp), %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev
.L1614:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1626
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1626:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20709:
	.size	_ZN2v88internal4wasm12NativeModule12LogWasmCodesEPNS0_7IsolateE, .-_ZN2v88internal4wasm12NativeModule12LogWasmCodesEPNS0_7IsolateE
	.section	.text._ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm
	.type	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm, @function
_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm:
.LFB20937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	40(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpq	$0, 120(%r12)
	je	.L1634
	movq	96(%r12), %rax
	leaq	88(%r12), %rdi
	testq	%rax, %rax
	jne	.L1631
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1665:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1630
.L1631:
	cmpq	32(%rax), %rbx
	jb	.L1665
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1631
.L1630:
	cmpq	%rdi, 104(%r12)
	je	.L1634
	call	_ZSt18_Rb_tree_decrementPKSt18_Rb_tree_node_base@PLT
	cmpq	40(%rax), %rbx
	jnb	.L1634
	cmpq	32(%rax), %rbx
	jb	.L1634
	movq	48(%rax), %r12
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r12, %r12
	je	.L1627
	leaq	536(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	592(%r12), %rax
	leaq	584(%r12), %rdi
	testq	%rax, %rax
	jne	.L1638
	jmp	.L1637
	.p2align 4,,10
	.p2align 3
.L1666:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1637
.L1638:
	cmpq	32(%rax), %rbx
	jb	.L1666
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1638
.L1637:
	cmpq	%rdi, 600(%r12)
	je	.L1644
	call	_ZSt18_Rb_tree_decrementPKSt18_Rb_tree_node_base@PLT
	movq	40(%rax), %r12
	movq	(%r12), %rax
	cmpq	%rbx, %rax
	ja	.L1644
	addq	8(%r12), %rax
	cmpq	%rax, %rbx
	jb	.L1667
.L1644:
	xorl	%r12d, %r12d
.L1641:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1634:
	.cfi_restore_state
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1627:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1667:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE
	jmp	.L1641
	.cfi_endproc
.LFE20937:
	.size	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm, .-_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm
	.section	.text._ZN2v88internal4wasm12NativeModule11PublishCodeESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule11PublishCodeESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE
	.type	_ZN2v88internal4wasm12NativeModule11PublishCodeESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE, @function
_ZN2v88internal4wasm12NativeModule11PublishCodeESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE:
.LFB20787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	536(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	0(%r13), %r12
	movq	$0, 0(%r13)
	movl	56(%r12), %r13d
	movq	%r12, -72(%rbp)
	cmpl	$-1, %r13d
	je	.L1669
	movq	208(%rbx), %rax
	movl	60(%rax), %edx
	cmpl	%edx, %r13d
	jnb	.L1726
.L1669:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE
	movq	-72(%rbp), %r13
	leaq	-72(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	leaq	576(%rbx), %rdi
	movq	0(%r13), %rax
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L1690
	movl	112(%r12), %edi
	testl	%edi, %edi
	jns	.L1727
	movq	120(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1692
.L1733:
	call	_ZdaPv@PLT
.L1692:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1693
	call	_ZdaPv@PLT
.L1693:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1694
	call	_ZdaPv@PLT
.L1694:
	movl	$144, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1690:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1728
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1726:
	.cfi_restore_state
	movl	60(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L1670
	movq	128(%r12), %r8
	testq	%r8, %r8
	je	.L1670
	movq	120(%r12), %rcx
	movq	8(%r12), %rsi
	movq	%r8, %rdx
	movq	(%r12), %rdi
	call	_ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE@PLT
	testl	%eax, %eax
	js	.L1729
	movl	112(%r12), %edx
	testl	%edx, %edx
	jns	.L1730
	movl	%eax, 112(%r12)
	movq	-72(%rbp), %r12
	movq	208(%rbx), %rax
	movl	56(%r12), %r13d
	movl	60(%rax), %edx
.L1670:
	movl	%r13d, %r8d
	subl	%edx, %r8d
	movq	624(%rbx), %rdx
	movl	%r8d, %ecx
	leaq	(%rdx,%rcx,8), %rdx
	movq	(%rdx), %r15
	testq	%r15, %r15
	je	.L1673
	movzbl	136(%r12), %ecx
	cmpb	%cl, 136(%r15)
	jge	.L1731
	movq	%r12, (%rdx)
	movq	%r15, %rdi
	movl	%r8d, -84(%rbp)
	call	_ZN2v88internal4wasm16WasmCodeRefScope6AddRefEPNS1_8WasmCodeE
	movl	140(%r15), %eax
	leaq	140(%r15), %rdx
	movl	-84(%rbp), %r8d
.L1679:
	cmpl	$1, %eax
	je	.L1732
	leal	-1(%rax), %ecx
	lock cmpxchgl	%ecx, (%rdx)
	jne	.L1679
.L1695:
	movq	-72(%rbp), %r12
	movq	632(%rbx), %rdi
	movl	56(%r12), %r13d
	movl	60(%r12), %esi
	testq	%rdi, %rdi
	je	.L1681
	movq	208(%rbx), %rax
	movl	%r13d, %edx
	subl	60(%rax), %edx
	movl	%edx, %ecx
	andl	$7, %edx
	shrl	$3, %ecx
	movzbl	(%rdi,%rcx), %ecx
	btl	%edx, %ecx
	jnc	.L1681
	cmpl	$4, %esi
	jne	.L1669
.L1685:
	subl	60(%rax), %r13d
	movl	$1, %edx
	movl	%r13d, %eax
	andl	$7, %r13d
	shrl	$3, %eax
	movl	%r13d, %ecx
	sall	%cl, %edx
	orb	%dl, (%rdi,%rax)
	movq	-72(%rbp), %r12
	movq	504(%rbx), %rax
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1727:
	call	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi@PLT
	movq	120(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L1733
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1673:
	movq	%r12, (%rdx)
	jmp	.L1695
	.p2align 4,,10
	.p2align 3
.L1681:
	movq	504(%rbx), %rax
	testq	%rax, %rax
	je	.L1734
	cmpl	$4, %esi
	je	.L1725
.L1684:
	movq	(%r12), %rdx
	movq	(%rax), %rdi
	movl	$1, %ecx
	movl	%r8d, %esi
	call	_ZN2v88internal4wasm18JumpTableAssembler18PatchJumpTableSlotEmjmNS1_8WasmCode11FlushICacheE
	movq	-72(%rbp), %r12
	jmp	.L1669
	.p2align 4,,10
	.p2align 3
.L1731:
	cmpl	$4, 60(%r12)
	jne	.L1669
	movq	632(%rbx), %rdi
	jmp	.L1680
	.p2align 4,,10
	.p2align 3
.L1734:
	cmpl	$4, %esi
	jne	.L1669
.L1725:
	movq	208(%rbx), %rax
.L1680:
	testq	%rdi, %rdi
	jne	.L1685
	movl	68(%rax), %r12d
	movl	%r8d, -84(%rbp)
	addl	$7, %r12d
	shrl	$3, %r12d
	movq	%r12, %rdi
	call	_Znam@PLT
	testq	%r12, %r12
	movl	-84(%rbp), %r8d
	movq	%rax, %rdi
	je	.L1689
	movq	%r12, %rdx
	xorl	%esi, %esi
	movl	%r8d, -84(%rbp)
	call	memset@PLT
	movl	-84(%rbp), %r8d
	movq	%rax, %rdi
.L1689:
	movq	632(%rbx), %r9
	movq	%rdi, 632(%rbx)
	testq	%r9, %r9
	je	.L1735
	movq	%r9, %rdi
	movl	%r8d, -84(%rbp)
	call	_ZdaPv@PLT
	movq	208(%rbx), %rax
	movq	632(%rbx), %rdi
	movl	-84(%rbp), %r8d
	jmp	.L1685
.L1732:
	movq	%r15, %rdi
	movl	%r8d, -84(%rbp)
	call	_ZN2v88internal4wasm8WasmCode27DecRefOnPotentiallyDeadCodeEv
	movl	-84(%rbp), %r8d
	testb	%al, %al
	je	.L1695
	leaq	.LC79(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1729:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1730:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1735:
	movq	208(%rbx), %rax
	jmp	.L1685
.L1728:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20787:
	.size	_ZN2v88internal4wasm12NativeModule11PublishCodeESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE, .-_ZN2v88internal4wasm12NativeModule11PublishCodeESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE
	.section	.text._ZN2v88internal4wasm12NativeModule28CreateEmptyJumpTableInRegionEjNS_4base13AddressRegionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule28CreateEmptyJumpTableInRegionEjNS_4base13AddressRegionE
	.type	_ZN2v88internal4wasm12NativeModule28CreateEmptyJumpTableInRegionEjNS_4base13AddressRegionE, @function
_ZN2v88internal4wasm12NativeModule28CreateEmptyJumpTableInRegionEjNS_4base13AddressRegionE:
.LFB20806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rcx, %rdx
	movq	%rdx, %r8
	movq	%rax, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	movq	%rdi, %rsi
	movq	%rbx, %rdx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE
	movl	$204, %esi
	movq	%rax, %rdi
	movq	%rdx, %r13
	movq	%rax, %r14
	call	memset@PLT
	movl	$144, %edi
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	movabsq	$25769803775, %rcx
	movq	%r13, 8(%rax)
	movq	%r12, 48(%rax)
	movq	%r14, (%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	%rcx, 56(%rax)
	movq	%rbx, 64(%rax)
	movq	$0, 72(%rax)
	movq	$0, 80(%rax)
	movq	%rbx, 88(%rax)
	movq	%rbx, 96(%rax)
	movq	%rbx, 104(%rax)
	movl	$-1, 112(%rax)
	movq	$0, 120(%rax)
	movq	$0, 128(%rax)
	movb	$0, 136(%rax)
	movl	$1, 140(%rax)
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal4wasm12NativeModule11PublishCodeESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE
	movq	-48(%rbp), %r13
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L1736
	movl	112(%r13), %edi
	testl	%edi, %edi
	jns	.L1756
	movq	120(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1739
.L1758:
	call	_ZdaPv@PLT
.L1739:
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1740
	call	_ZdaPv@PLT
.L1740:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1741
	call	_ZdaPv@PLT
.L1741:
	movl	$144, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1736:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1757
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1756:
	.cfi_restore_state
	call	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi@PLT
	movq	120(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L1758
	jmp	.L1739
.L1757:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20806:
	.size	_ZN2v88internal4wasm12NativeModule28CreateEmptyJumpTableInRegionEjNS_4base13AddressRegionE, .-_ZN2v88internal4wasm12NativeModule28CreateEmptyJumpTableInRegionEjNS_4base13AddressRegionE
	.section	.rodata._ZN2v88internal4wasm12NativeModule26ReserveCodeTableForTestingEj.str1.1,"aMS",@progbits,1
.LC80:
	.string	"1 == code_space_data_.size()"
	.section	.text._ZN2v88internal4wasm12NativeModule26ReserveCodeTableForTestingEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule26ReserveCodeTableForTestingEj
	.type	_ZN2v88internal4wasm12NativeModule26ReserveCodeTableForTestingEj, @function
_ZN2v88internal4wasm12NativeModule26ReserveCodeTableForTestingEj:
.LFB20708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	leaq	0(,%r14,8), %r15
	.cfi_offset 13, -40
	movq	%r14, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff, %rax
	movl	$0x3f800000, -88(%rbp)
	movq	$1, -112(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-72(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%r12, %fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff
	call	_Znam@PLT
	movq	%rax, %rcx
	testq	%r14, %r14
	je	.L1763
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	%rax, %rcx
.L1763:
	movq	208(%rbx), %rax
	movq	624(%rbx), %r14
	movl	68(%rax), %eax
	testl	%eax, %eax
	jne	.L1774
	movq	%rcx, 624(%rbx)
	testq	%r14, %r14
	jne	.L1764
.L1765:
	movq	640(%rbx), %r14
	movq	648(%rbx), %rax
	subq	%r14, %rax
	cmpq	$24, %rax
	je	.L1775
	leaq	.LC80(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1774:
	leaq	0(,%rax,8), %rdx
	movq	%r14, %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movq	%rax, 624(%rbx)
.L1764:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	jmp	.L1765
	.p2align 4,,10
	.p2align 3
.L1775:
	movl	$2863311531, %eax
	leal	11(%r13), %esi
	movq	(%r14), %rdx
	movq	8(%r14), %rcx
	imulq	%rax, %rsi
	movq	%rbx, %rdi
	shrq	$35, %rsi
	sall	$6, %esi
	call	_ZN2v88internal4wasm12NativeModule28CreateEmptyJumpTableInRegionEjNS_4base13AddressRegionE
	movq	%r12, %rdi
	movq	%rax, 16(%r14)
	movq	640(%rbx), %rax
	movq	16(%rax), %rax
	movq	%rax, 504(%rbx)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1776
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1776:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20708:
	.size	_ZN2v88internal4wasm12NativeModule26ReserveCodeTableForTestingEj, .-_ZN2v88internal4wasm12NativeModule26ReserveCodeTableForTestingEj
	.section	.text._ZN2v88internal4wasm12NativeModule11UseLazyStubEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule11UseLazyStubEj
	.type	_ZN2v88internal4wasm12NativeModule11UseLazyStubEj, @function
_ZN2v88internal4wasm12NativeModule11UseLazyStubEj:
.LFB20712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$744, %rsp
	movl	%esi, -732(%rbp)
	movq	208(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	512(%rdi), %rax
	testq	%rax, %rax
	je	.L1858
	leaq	-672(%rbp), %rcx
	leaq	-704(%rbp), %r14
	movq	%rcx, -728(%rbp)
	leaq	16+_ZTVN2v88internal14MacroAssemblerE(%rip), %rcx
	leaq	-608(%rbp), %r13
	movq	%rcx, -744(%rbp)
	leaq	16+_ZTVN2v88internal4wasm18JumpTableAssemblerE(%rip), %rcx
	movq	%rcx, -752(%rbp)
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rcx
	movq	%rcx, -760(%rbp)
.L1778:
	subl	60(%rdx), %esi
	movl	$2863311531, %edx
	movq	%r14, %rdi
	leal	(%rsi,%rsi,4), %r12d
	addl	%r12d, %r12d
	addq	(%rax), %r12
	movl	%esi, %eax
	imulq	%rdx, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,2), %edx
	sall	$6, %eax
	sall	$2, %edx
	subl	%edx, %esi
	leal	(%rsi,%rsi,4), %edx
	addl	%edx, %eax
	movq	504(%rbx), %rdx
	addq	(%rdx), %rax
	movl	$256, %edx
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal23ExternalAssemblerBufferEPvi@PLT
	movl	$256, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movw	%dx, -668(%rbp)
	movq	%r14, %r8
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	-728(%rbp), %rdx
	movw	%ax, -656(%rbp)
	movq	$0, -664(%rbp)
	movl	$257, -672(%rbp)
	call	_ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE@PLT
	movq	-744(%rbp), %rax
	movq	-704(%rbp), %rdi
	movq	%rax, -608(%rbp)
	testq	%rdi, %rdi
	je	.L1796
	movq	(%rdi), %rax
	call	*8(%rax)
.L1796:
	movq	-752(%rbp), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal4wasm18JumpTableAssembler12EmitJumpSlotEm@PLT
	movq	-576(%rbp), %rax
	movl	$5, %esi
	movq	%r13, %rdi
	subq	-592(%rbp), %rax
	subl	%eax, %esi
	call	_ZN2v88internal4wasm18JumpTableAssembler8NopBytesEi@PLT
	movq	%r15, %rdi
	movl	$5, %esi
	leaq	-144(%rbp), %r15
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	movq	-760(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	%rax, -608(%rbp)
	testq	%rbx, %rbx
	je	.L1800
.L1797:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1797
.L1800:
	movq	-184(%rbp), %r14
	leaq	-200(%rbp), %r12
	testq	%r14, %r14
	je	.L1798
.L1799:
	movq	24(%r14), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r14), %rdi
	movq	16(%r14), %rbx
	testq	%rdi, %rdi
	je	.L1803
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1798
.L1804:
	movq	%rbx, %r14
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1803:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1804
.L1798:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1802
	movq	-208(%rbp), %rax
	movq	-240(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L1805
	.p2align 4,,10
	.p2align 3
.L1806:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L1806
	movq	-280(%rbp), %rdi
.L1805:
	call	_ZdlPv@PLT
.L1802:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1807
	movq	-296(%rbp), %rax
	movq	-328(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L1808
	.p2align 4,,10
	.p2align 3
.L1809:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L1809
	movq	-368(%rbp), %rdi
.L1808:
	call	_ZdlPv@PLT
.L1807:
	movq	%r13, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1864
	addq	$744, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1858:
	.cfi_restore_state
	movq	%fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff, %rax
	movl	68(%rdx), %r10d
	movq	$1, -656(%rbp)
	leaq	-608(%rbp), %r13
	movq	$0, -648(%rbp)
	movq	%rax, -672(%rbp)
	leal	(%r10,%r10,4), %esi
	leaq	-672(%rbp), %rax
	movq	%rax, -728(%rbp)
	movq	%rax, %rcx
	leaq	-616(%rbp), %rax
	leal	(%rsi,%rsi), %r14d
	movq	%rax, -664(%rbp)
	movl	%r14d, %esi
	movq	$0, -640(%rbp)
	movl	$0x3f800000, -632(%rbp)
	movq	$0, -624(%rbp)
	movq	$0, -616(%rbp)
	movq	%rcx, %fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff
	movq	640(%rdi), %rax
	movl	%r10d, -752(%rbp)
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movl	%r14d, -768(%rbp)
	movq	%rdx, %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal4wasm12NativeModule28CreateEmptyJumpTableInRegionEjNS_4base13AddressRegionE
	movq	208(%rbx), %rdx
	leaq	-712(%rbp), %r8
	movq	344(%rbx), %r12
	movq	%rax, 512(%rbx)
	movq	(%rax), %rsi
	movq	%r8, %rdi
	movl	60(%rdx), %r15d
	leal	256(%r14), %edx
	movq	%r8, -744(%rbp)
	leaq	-704(%rbp), %r14
	movq	%rsi, -760(%rbp)
	call	_ZN2v88internal23ExternalAssemblerBufferEPvi@PLT
	xorl	%ecx, %ecx
	movl	$256, %esi
	movq	%r13, %rdi
	movq	-744(%rbp), %r8
	movw	%cx, -688(%rbp)
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movw	%si, -700(%rbp)
	xorl	%esi, %esi
	movq	$0, -696(%rbp)
	movl	$257, -704(%rbp)
	call	_ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE@PLT
	movq	-712(%rbp), %rdi
	leaq	16+_ZTVN2v88internal14MacroAssemblerE(%rip), %rax
	movl	-752(%rbp), %r10d
	movq	%rax, -744(%rbp)
	testq	%rdi, %rdi
	movq	%rax, -608(%rbp)
	je	.L1779
	movq	(%rdi), %rax
	call	*8(%rax)
	movl	-752(%rbp), %r10d
.L1779:
	leaq	16+_ZTVN2v88internal4wasm18JumpTableAssemblerE(%rip), %rax
	movq	%rax, -752(%rbp)
	movq	%rax, -608(%rbp)
	leal	(%r15,%r10), %eax
	testl	%r10d, %r10d
	je	.L1783
	movq	%rbx, -776(%rbp)
	movl	%r15d, %ebx
	movl	%eax, %r15d
	.p2align 4,,10
	.p2align 3
.L1784:
	movl	%ebx, %esi
	movq	%r12, %rdx
	movq	%r13, %rdi
	addl	$1, %ebx
	call	_ZN2v88internal4wasm18JumpTableAssembler23EmitLazyCompileJumpSlotEjm@PLT
	cmpl	%ebx, %r15d
	jne	.L1784
	movq	-776(%rbp), %rbx
.L1783:
	movl	-768(%rbp), %esi
	movq	-760(%rbp), %rdi
	leaq	-144(%rbp), %r15
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	movq	-128(%rbp), %r12
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rax, -760(%rbp)
	movq	%rax, -608(%rbp)
	testq	%r12, %r12
	je	.L1781
.L1782:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1782
.L1781:
	movq	-184(%rbp), %r15
	testq	%r15, %r15
	je	.L1785
	movq	%rbx, -768(%rbp)
	leaq	-200(%rbp), %r12
.L1786:
	movq	24(%r15), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r15), %rdi
	movq	16(%r15), %rbx
	testq	%rdi, %rdi
	je	.L1789
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1860
.L1790:
	movq	%rbx, %r15
	jmp	.L1786
	.p2align 4,,10
	.p2align 3
.L1789:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1790
.L1860:
	movq	-768(%rbp), %rbx
.L1785:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1788
	movq	-208(%rbp), %rax
	movq	-240(%rbp), %r15
	leaq	8(%rax), %r12
	cmpq	%r15, %r12
	jbe	.L1791
	.p2align 4,,10
	.p2align 3
.L1792:
	movq	(%r15), %rdi
	addq	$8, %r15
	call	_ZdlPv@PLT
	cmpq	%r15, %r12
	ja	.L1792
	movq	-280(%rbp), %rdi
.L1791:
	call	_ZdlPv@PLT
.L1788:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1793
	movq	-296(%rbp), %rax
	movq	-328(%rbp), %r15
	leaq	8(%rax), %r12
	cmpq	%r15, %r12
	jbe	.L1794
	.p2align 4,,10
	.p2align 3
.L1795:
	movq	(%r15), %rdi
	addq	$8, %r15
	call	_ZdlPv@PLT
	cmpq	%r15, %r12
	ja	.L1795
	movq	-368(%rbp), %rdi
.L1794:
	call	_ZdlPv@PLT
.L1793:
	movq	%r13, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	movq	-728(%rbp), %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev
	movq	512(%rbx), %rax
	movq	208(%rbx), %rdx
	movl	-732(%rbp), %esi
	jmp	.L1778
.L1864:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20712:
	.size	_ZN2v88internal4wasm12NativeModule11UseLazyStubEj, .-_ZN2v88internal4wasm12NativeModule11UseLazyStubEj
	.section	.rodata._ZN2v88internal4wasm12NativeModule15SetRuntimeStubsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC88:
	.string	"embedded_data.ContainsBuiltin(builtin)"
	.section	.text._ZN2v88internal4wasm12NativeModule15SetRuntimeStubsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule15SetRuntimeStubsEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm12NativeModule15SetRuntimeStubsEPNS0_7IsolateE, @function
_ZN2v88internal4wasm12NativeModule15SetRuntimeStubsEPNS0_7IsolateE:
.LFB20713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$558, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$1096, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff, %rax
	movq	$1, -1024(%rbp)
	movq	$0, -1016(%rbp)
	movq	%rax, -1040(%rbp)
	leaq	-1040(%rbp), %rax
	movq	%rax, -1136(%rbp)
	movq	%rax, %rcx
	leaq	-984(%rbp), %rax
	movq	%rax, -1032(%rbp)
	movq	$0, -1008(%rbp)
	movl	$0x3f800000, -1000(%rbp)
	movq	$0, -992(%rbp)
	movq	$0, -984(%rbp)
	movq	%rcx, %fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff
	movq	640(%rdi), %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal4wasm12NativeModule28CreateEmptyJumpTableInRegionEjNS_4base13AddressRegionE
	movq	%rax, -1128(%rbp)
	movq	(%rax), %rax
	movq	%rax, -1120(%rbp)
	movq	%rax, %rbx
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r12d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movdqa	.LC81(%rip), %xmm0
	movl	%r12d, -1080(%rbp)
	leaq	-1088(%rbp), %r12
	movq	%rax, -1088(%rbp)
	movabsq	$2834678416019, %rax
	movaps	%xmm0, -432(%rbp)
	movdqa	.LC82(%rip), %xmm0
	movq	%rax, -320(%rbp)
	leaq	-432(%rbp), %rax
	movaps	%xmm0, -416(%rbp)
	movdqa	.LC83(%rip), %xmm0
	movl	$661, -312(%rbp)
	movaps	%xmm0, -400(%rbp)
	movdqa	.LC84(%rip), %xmm0
	movq	%rax, -1112(%rbp)
	movaps	%xmm0, -384(%rbp)
	movdqa	.LC85(%rip), %xmm0
	movaps	%xmm0, -368(%rbp)
	movdqa	.LC86(%rip), %xmm0
	movaps	%xmm0, -352(%rbp)
	movdqa	.LC87(%rip), %xmm0
	movaps	%xmm0, -336(%rbp)
	jmp	.L1867
	.p2align 4,,10
	.p2align 3
.L1866:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movq	%rbx, 248(%r13,%r15,8)
	addq	$18, %rbx
	movq	%rax, %r8
	leaq	-304(%rbp), %rax
	movq	%r8, (%rax,%r15,8)
	addq	$1, %r15
	cmpq	$31, %r15
	je	.L1909
.L1867:
	movq	-1112(%rbp), %rax
	movq	%r12, %rdi
	movl	(%rax,%r15,4), %r14d
	movl	%r14d, %esi
	call	_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi@PLT
	testl	%eax, %eax
	jne	.L1866
	leaq	.LC88(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1909:
	movq	-1120(%rbp), %rsi
	leaq	-1096(%rbp), %rbx
	movl	$814, %edx
	movq	%rax, -1112(%rbp)
	movq	%rbx, %rdi
	leaq	-976(%rbp), %r12
	call	_ZN2v88internal23ExternalAssemblerBufferEPvi@PLT
	movl	$256, %edx
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movw	%dx, -1068(%rbp)
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rbx, %r8
	leaq	-1072(%rbp), %rdx
	movw	%ax, -1056(%rbp)
	movq	$0, -1064(%rbp)
	movl	$257, -1072(%rbp)
	call	_ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE@PLT
	movq	-1096(%rbp), %rdi
	leaq	16+_ZTVN2v88internal14MacroAssemblerE(%rip), %rax
	movq	%rax, -976(%rbp)
	movq	-1112(%rbp), %rax
	testq	%rdi, %rdi
	je	.L1868
	movq	(%rdi), %rdx
	call	*8(%rdx)
	movq	-1112(%rbp), %rax
.L1868:
	leaq	16+_ZTVN2v88internal4wasm18JumpTableAssemblerE(%rip), %rdx
	movq	%rax, %r14
	xorl	%ebx, %ebx
	movq	%rdx, -976(%rbp)
	.p2align 4,,10
	.p2align 3
.L1869:
	movq	(%r14), %rsi
	movq	%r12, %rdi
	addl	$18, %ebx
	addq	$8, %r14
	call	_ZN2v88internal4wasm18JumpTableAssembler19EmitRuntimeStubSlotEm@PLT
	movq	-944(%rbp), %rax
	movl	%ebx, %esi
	movq	%r12, %rdi
	subq	-960(%rbp), %rax
	subl	%eax, %esi
	call	_ZN2v88internal4wasm18JumpTableAssembler8NopBytesEi@PLT
	cmpl	$558, %ebx
	jne	.L1869
	movq	-1120(%rbp), %rdi
	movl	$558, %esi
	leaq	-512(%rbp), %rbx
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	movq	-496(%rbp), %r14
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rax, -976(%rbp)
	testq	%r14, %r14
	je	.L1873
.L1870:
	movq	24(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r14, %rdi
	movq	16(%r14), %r14
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L1870
.L1873:
	movq	-552(%rbp), %r14
	leaq	-568(%rbp), %rbx
	testq	%r14, %r14
	je	.L1871
.L1872:
	movq	24(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r14), %rdi
	movq	16(%r14), %r15
	testq	%rdi, %rdi
	je	.L1876
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	je	.L1871
.L1877:
	movq	%r15, %r14
	jmp	.L1872
	.p2align 4,,10
	.p2align 3
.L1876:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L1877
.L1871:
	movq	-648(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1875
	movq	-576(%rbp), %rax
	movq	-608(%rbp), %r14
	leaq	8(%rax), %rbx
	cmpq	%r14, %rbx
	jbe	.L1878
	.p2align 4,,10
	.p2align 3
.L1879:
	movq	(%r14), %rdi
	addq	$8, %r14
	call	_ZdlPv@PLT
	cmpq	%r14, %rbx
	ja	.L1879
	movq	-648(%rbp), %rdi
.L1878:
	call	_ZdlPv@PLT
.L1875:
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1880
	movq	-664(%rbp), %rax
	movq	-696(%rbp), %r14
	leaq	8(%rax), %rbx
	cmpq	%r14, %rbx
	jbe	.L1881
	.p2align 4,,10
	.p2align 3
.L1882:
	movq	(%r14), %rdi
	addq	$8, %r14
	call	_ZdlPv@PLT
	cmpq	%r14, %rbx
	ja	.L1882
	movq	-736(%rbp), %rdi
.L1881:
	call	_ZdlPv@PLT
.L1880:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	movq	-1128(%rbp), %rax
	movq	-1136(%rbp), %rdi
	movq	%rax, 496(%r13)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1910
	addq	$1096, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1910:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20713:
	.size	_ZN2v88internal4wasm12NativeModule15SetRuntimeStubsEPNS0_7IsolateE, .-_ZN2v88internal4wasm12NativeModule15SetRuntimeStubsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal4wasm12NativeModule12AddCodeSpaceENS_4base13AddressRegionE.str1.8,"aMS",@progbits,1
	.align 8
.LC89:
	.string	"region.contains(jump_table->instruction_start())"
	.section	.text._ZN2v88internal4wasm12NativeModule12AddCodeSpaceENS_4base13AddressRegionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule12AddCodeSpaceENS_4base13AddressRegionE
	.type	_ZN2v88internal4wasm12NativeModule12AddCodeSpaceENS_4base13AddressRegionE, @function
_ZN2v88internal4wasm12NativeModule12AddCodeSpaceENS_4base13AddressRegionE:
.LFB20835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$136, %rsp
	movq	664(%rdi), %rdx
	movq	648(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	640(%rdi), %rax
	movzbl	296(%rdx), %ecx
	movq	%fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff, %rdx
	movl	$0x3f800000, -72(%rbp)
	movq	$1, -96(%rbp)
	movq	%rdx, -112(%rbp)
	leaq	-56(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	%r14, %fs:_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE@tpoff
	movq	208(%rdi), %rdx
	movl	68(%rdx), %edx
	testl	%edx, %edx
	je	.L1912
	cmpq	%rsi, %rax
	jne	.L1920
	xorl	%eax, %eax
	testb	%cl, %cl
	je	.L1928
.L1913:
	movq	%rax, 504(%rdi)
	jmp	.L1915
	.p2align 4,,10
	.p2align 3
.L1912:
	cmpq	%rsi, %rax
	je	.L1929
.L1920:
	xorl	%eax, %eax
.L1915:
	movq	%r13, -144(%rbp)
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	cmpq	%rsi, 656(%rdi)
	je	.L1916
	movdqa	-144(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movq	-128(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 648(%rdi)
.L1917:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1930
	addq	$136, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1928:
	.cfi_restore_state
	leal	11(%rdx), %esi
	movl	$2863311531, %eax
	movq	%r13, %rdx
	movq	%r12, %rcx
	imulq	%rax, %rsi
	movq	%rdi, -152(%rbp)
	shrq	$35, %rsi
	sall	$6, %esi
	call	_ZN2v88internal4wasm12NativeModule28CreateEmptyJumpTableInRegionEjNS_4base13AddressRegionE
	movq	(%rax), %rdx
	subq	%r13, %rdx
	cmpq	%r12, %rdx
	jnb	.L1914
	movq	-152(%rbp), %rdi
	movq	%rax, 504(%rdi)
	movq	648(%rdi), %rsi
	jmp	.L1915
	.p2align 4,,10
	.p2align 3
.L1916:
	leaq	-144(%rbp), %rdx
	addq	$640, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm12NativeModule13CodeSpaceDataESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L1917
	.p2align 4,,10
	.p2align 3
.L1914:
	leaq	.LC89(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1930:
	call	__stack_chk_fail@PLT
.L1929:
	xorl	%eax, %eax
	jmp	.L1913
	.cfi_endproc
.LFE20835:
	.size	_ZN2v88internal4wasm12NativeModule12AddCodeSpaceENS_4base13AddressRegionE, .-_ZN2v88internal4wasm12NativeModule12AddCodeSpaceENS_4base13AddressRegionE
	.section	.rodata._ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE.str1.8,"aMS",@progbits,1
	.align 8
.LC90:
	.string	"wasm code reservation in region"
	.section	.rodata._ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE.str1.1,"aMS",@progbits,1
.LC91:
	.string	"wasm code reservation"
.LC92:
	.string	"wasm code commit"
	.section	.text._ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE
	.type	_ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE, @function
_ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE:
.LFB20641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	addq	$31, %r12
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	andq	$-32, %r12
	leaq	48(%rbx), %r14
	subq	$168, %rsp
	movq	%rsi, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	8(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal4wasm22DisjointAllocationPool16AllocateInRegionEmNS_4base13AddressRegionE
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L1932
	movq	%rax, -184(%rbp)
.L1933:
	movq	-168(%rbp), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-184(%rbp), %rdi
	movq	%rax, %rcx
	leaq	-1(%r13,%rdi), %rdx
	negq	%rcx
	leaq	-1(%rdi,%rax), %rsi
	leaq	(%rdx,%rax), %r15
	andq	%rcx, %rsi
	andq	%rcx, %r15
	cmpq	%rsi, %r15
	jbe	.L1942
	subq	%rsi, %r15
	lock addq	%r15, 144(%rbx)
	cmpb	$0, _ZN2v88internal14FLAG_perf_profE(%rip)
	movq	(%rbx), %rcx
	leaq	-56(%rbp), %rax
	leaq	-72(%rbp), %r14
	movq	%rsi, -72(%rbp)
	movq	%r14, %xmm0
	movq	%rax, %xmm1
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%r15, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	jne	.L1942
	leaq	24(%rcx), %r12
	movq	24(%rcx), %rax
.L1945:
	movq	8(%rcx), %rdx
	subq	%rax, %rdx
	cmpq	%r15, %rdx
	jb	.L1944
	leaq	(%rax,%r15), %rdx
	lock cmpxchgq	%rdx, (%r12)
	jne	.L1945
	xorl	%ecx, %ecx
	cmpb	$0, _ZN2v88internal35FLAG_wasm_write_protect_code_memoryE(%rip)
	movq	%rsi, -176(%rbp)
	sete	%cl
	addl	$2, %ecx
	movl	%ecx, -168(%rbp)
	movl	%ecx, -192(%rbp)
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movl	-168(%rbp), %ecx
	movq	-176(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE@PLT
	testb	%al, %al
	je	.L1947
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1942
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L1942:
	movq	-184(%rbp), %rsi
	leaq	72(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE
	lock addq	%r13, 152(%rbx)
	movq	-200(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1959
	movq	-184(%rbp), %rax
	addq	$168, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1932:
	.cfi_restore_state
	cmpb	$0, 169(%rbx)
	je	.L1934
	cmpq	$-1, %r15
	jne	.L1960
	movq	128(%rbx), %rdx
	movq	120(%rbx), %rax
	cmpq	%rdx, %rax
	je	.L1952
	movq	-8(%rdx), %r15
	addq	-16(%rdx), %r15
	.p2align 4,,10
	.p2align 3
.L1938:
	addq	16(%rax), %r13
	addq	$24, %rax
	cmpq	%rax, %rdx
	jne	.L1938
	movabsq	$-3689348814741910323, %rdx
	movq	%r13, %rax
	mulq	%rdx
	shrq	$2, %rdx
	cmpq	%r12, %rdx
	cmovb	%r12, %rdx
	movq	%rdx, %rdi
.L1937:
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	movq	(%rbx), %rsi
	movq	%r15, %rcx
	movq	%rax, %rdx
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal4wasm15WasmCodeManager11TryAllocateEmPv
	movq	-152(%rbp), %r15
	testq	%r15, %r15
	je	.L1961
	movq	(%rbx), %rax
	movq	-144(%rbp), %r13
	leaq	40(%rax), %r8
	movq	%rax, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -184(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-176(%rbp), %rax
	leaq	-128(%rbp), %rsi
	leaq	(%r15,%r13), %rdx
	movq	%r15, -128(%rbp)
	movq	%rax, -112(%rbp)
	movq	-208(%rbp), %rax
	movq	%rdx, -120(%rbp)
	leaq	80(%rax), %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE17_M_emplace_uniqueIJS0_ImS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_
	movq	-184(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE
	movq	128(%rbx), %rsi
	cmpq	136(%rbx), %rsi
	je	.L1940
	movq	-160(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-192(%rbp), %rdi
	movups	%xmm0, 8(%rsi)
	movq	%rax, (%rsi)
	movdqu	-152(%rbp), %xmm2
	movups	%xmm2, 8(%rsi)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	addq	$24, 128(%rbx)
.L1941:
	movq	-176(%rbp), %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal4wasm12NativeModule12AddCodeSpaceENS_4base13AddressRegionE
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	$-1, %rcx
	call	_ZN2v88internal4wasm22DisjointAllocationPool16AllocateInRegionEmNS_4base13AddressRegionE
	movq	128(%rbx), %rsi
	subq	120(%rbx), %rsi
	movq	%rax, -184(%rbp)
	sarq	$3, %rsi
	movq	%rdx, %r13
	movabsq	$-6148914691236517205, %rax
	imulq	%rax, %rsi
	movq	176(%rbx), %rax
	leaq	2088(%rax), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	-192(%rbp), %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	jmp	.L1933
	.p2align 4,,10
	.p2align 3
.L1947:
	lock subq	%r15, (%r12)
.L1944:
	xorl	%edx, %edx
	leaq	.LC92(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.p2align 4,,10
	.p2align 3
.L1952:
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L1940:
	movq	-192(%rbp), %rdx
	leaq	120(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal13VirtualMemoryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L1941
.L1960:
	leaq	.LC90(%rip), %rsi
.L1935:
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
.L1934:
	addq	$1, %r15
	leaq	.LC90(%rip), %rsi
	leaq	.LC91(%rip), %rax
	cmove	%rax, %rsi
	jmp	.L1935
.L1959:
	call	__stack_chk_fail@PLT
.L1961:
	xorl	%edx, %edx
	leaq	.LC91(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.cfi_endproc
.LFE20641:
	.size	_ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE, .-_ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE
	.section	.text._ZN2v88internal4wasm17WasmCodeAllocator15AllocateForCodeEPNS1_12NativeModuleEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmCodeAllocator15AllocateForCodeEPNS1_12NativeModuleEm
	.type	_ZN2v88internal4wasm17WasmCodeAllocator15AllocateForCodeEPNS1_12NativeModuleEm, @function
_ZN2v88internal4wasm17WasmCodeAllocator15AllocateForCodeEPNS1_12NativeModuleEm:
.LFB20640:
	.cfi_startproc
	endbr64
	xorl	%ecx, %ecx
	movq	$-1, %r8
	jmp	_ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE
	.cfi_endproc
.LFE20640:
	.size	_ZN2v88internal4wasm17WasmCodeAllocator15AllocateForCodeEPNS1_12NativeModuleEm, .-_ZN2v88internal4wasm17WasmCodeAllocator15AllocateForCodeEPNS1_12NativeModuleEm
	.section	.text._ZN2v88internal4wasm12NativeModule7AddCodeEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule7AddCodeEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierE
	.type	_ZN2v88internal4wasm12NativeModule7AddCodeEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierE, @function
_ZN2v88internal4wasm12NativeModule7AddCodeEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierE:
.LFB20785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r9d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	movq	$-1, %r8
	subq	$88, %rsp
	movq	16(%rbp), %r9
	movq	24(%rbp), %r10
	movl	40(%rbp), %r11d
	movslq	12(%rcx), %rdx
	xorl	%ecx, %ecx
	movq	%r9, -128(%rbp)
	movq	%r10, -120(%rbp)
	movl	%r11d, -108(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE
	movq	-120(%rbp), %r10
	movq	-128(%rbp), %r9
	movq	%r13, %rdi
	movl	-108(%rbp), %r11d
	movq	-104(%rbp), %rsi
	movl	%ebx, %r8d
	movq	(%r10), %rcx
	movq	$0, (%r10)
	movq	%rcx, -80(%rbp)
	movq	8(%r10), %rcx
	movq	%rcx, -72(%rbp)
	movq	(%r9), %rcx
	movq	$0, (%r9)
	movq	%rcx, -96(%rbp)
	movq	8(%r9), %rcx
	movl	%r15d, %r9d
	pushq	%rdx
	movl	%r14d, %edx
	pushq	%rax
	movl	32(%rbp), %eax
	pushq	%r11
	pushq	%rax
	leaq	-80(%rbp), %rax
	pushq	%rax
	leaq	-96(%rbp), %rax
	pushq	%rax
	movq	%rcx, -88(%rbp)
	movq	%r12, %rcx
	call	_ZN2v88internal4wasm12NativeModule20AddCodeWithCodeSpaceEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierENS0_6VectorIhEE
	movq	-96(%rbp), %rdi
	addq	$48, %rsp
	testq	%rdi, %rdi
	je	.L1964
	call	_ZdaPv@PLT
.L1964:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1963
	call	_ZdaPv@PLT
.L1963:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1974
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1974:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20785:
	.size	_ZN2v88internal4wasm12NativeModule7AddCodeEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierE, .-_ZN2v88internal4wasm12NativeModule7AddCodeEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierE
	.section	.text._ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS0_6VectorINS1_21WasmCompilationResultEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS0_6VectorINS1_21WasmCompilationResultEEE
	.type	_ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS0_6VectorINS1_21WasmCompilationResultEEE, @function
_ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS0_6VectorINS1_21WasmCompilationResultEEE:
.LFB20916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %rax
	salq	$4, %rax
	addq	%rcx, %rax
	leaq	(%rdx,%rax,8), %rcx
	movq	%rcx, -136(%rbp)
	cmpq	%rcx, %rdx
	je	.L2029
	subq	%rdx, %rcx
	movq	%rdx, %rax
	movq	%rcx, %rdx
	movabsq	$1220740416642543857, %rcx
	subq	$136, %rdx
	shrq	$3, %rdx
	imulq	%rcx, %rdx
	movabsq	$2305843009213693951, %rcx
	andq	%rcx, %rdx
	leaq	1(%rdx), %rcx
	cmpq	$5, %rdx
	jbe	.L2030
	movq	%rcx, %rsi
	movdqa	.LC93(%rip), %xmm6
	pxor	%xmm2, %xmm2
	pxor	%xmm4, %xmm4
	shrq	$2, %rsi
	movdqa	.LC94(%rip), %xmm5
	movq	%rsi, %rdx
	salq	$4, %rdx
	addq	%rsi, %rdx
	salq	$5, %rdx
	addq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L1978:
	movd	420(%rax), %xmm7
	movd	284(%rax), %xmm1
	addq	$544, %rax
	movd	-532(%rax), %xmm0
	punpckldq	%xmm7, %xmm1
	movd	-396(%rax), %xmm7
	punpckldq	%xmm7, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movdqa	%xmm4, %xmm1
	paddd	%xmm6, %xmm0
	pand	%xmm5, %xmm0
	pcmpgtd	%xmm0, %xmm1
	movdqa	%xmm0, %xmm3
	punpckldq	%xmm1, %xmm3
	punpckhdq	%xmm1, %xmm0
	paddq	%xmm3, %xmm0
	paddq	%xmm0, %xmm2
	cmpq	%rdx, %rax
	jne	.L1978
	movq	%rcx, %rsi
	movdqa	%xmm2, %xmm0
	movq	-152(%rbp), %rbx
	andq	$-4, %rsi
	psrldq	$8, %xmm0
	movq	%rsi, %rax
	paddq	%xmm0, %xmm2
	salq	$4, %rax
	movq	%xmm2, %rdx
	addq	%rsi, %rax
	leaq	(%rbx,%rax,8), %rax
	cmpq	%rcx, %rsi
	je	.L1976
.L1977:
	movl	12(%rax), %ebx
	leal	31(%rbx), %ecx
	movq	-136(%rbp), %rbx
	andl	$-32, %ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rdx
	leaq	136(%rax), %rcx
	cmpq	%rcx, %rbx
	je	.L1976
	movl	148(%rax), %ecx
	addl	$31, %ecx
	andl	$-32, %ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rdx
	leaq	272(%rax), %rcx
	cmpq	%rcx, %rbx
	je	.L1976
	movl	284(%rax), %ecx
	addl	$31, %ecx
	andl	$-32, %ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rdx
	leaq	408(%rax), %rcx
	cmpq	%rcx, %rbx
	je	.L1976
	movl	420(%rax), %ecx
	addl	$31, %ecx
	andl	$-32, %ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rdx
	leaq	544(%rax), %rcx
	cmpq	%rcx, %rbx
	je	.L1976
	movl	556(%rax), %ecx
	addl	$31, %ecx
	andl	$-32, %ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rdx
	leaq	680(%rax), %rcx
	cmpq	%rcx, %rbx
	je	.L1976
	movl	692(%rax), %eax
	addl	$31, %eax
	andl	$-32, %eax
	cltq
	addq	%rax, %rdx
.L1976:
	xorl	%ecx, %ecx
	movq	$-1, %r8
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -80(%rbp)
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, -144(%rbp)
	ja	.L2105
	cmpq	$0, -144(%rbp)
	jne	.L2106
.L1981:
	movq	-136(%rbp), %rcx
	leaq	-120(%rbp), %r15
	cmpq	%rcx, -152(%rbp)
	je	.L2004
	movq	%r12, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L1990:
	movl	12(%r14), %eax
	movq	%rbx, %rsi
	movzbl	133(%r14), %ecx
	addl	$31, %eax
	andl	$-32, %eax
	cltq
	movq	%rax, %rdi
	addq	%rax, %rbx
	movsbl	134(%r14), %eax
	cmpb	$2, %al
	ja	.L1993
	movq	96(%r14), %rdx
	addl	%eax, %eax
	movq	$0, 96(%r14)
	movq	%rdx, -96(%rbp)
	movq	104(%r14), %rdx
	movq	$0, 104(%r14)
	movq	%rdx, -88(%rbp)
	movq	112(%r14), %rdx
	movq	$0, 112(%r14)
	movq	%rdx, -112(%rbp)
	movq	120(%r14), %rdx
	movq	%rdx, -104(%rbp)
	movl	128(%r14), %edx
	pushq	%rdi
	movq	%r15, %rdi
	pushq	%rsi
	movq	%r13, %rsi
	pushq	%rcx
	movq	%r14, %rcx
	pushq	%rax
	leaq	-96(%rbp), %rax
	pushq	%rax
	leaq	-112(%rbp), %rax
	pushq	%rax
	movl	92(%r14), %r9d
	movl	88(%r14), %r8d
	call	_ZN2v88internal4wasm12NativeModule20AddCodeWithCodeSpaceEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierENS0_6VectorIhEE
	movq	-72(%rbp), %rsi
	addq	$48, %rsp
	cmpq	-64(%rbp), %rsi
	je	.L1994
	movq	-120(%rbp), %rax
	movq	$0, -120(%rbp)
	movq	%rax, (%rsi)
	addq	$8, -72(%rbp)
.L1995:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L1997
	movl	112(%r12), %edi
	testl	%edi, %edi
	js	.L1998
	call	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi@PLT
.L1998:
	movq	120(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1999
	call	_ZdaPv@PLT
.L1999:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2000
	call	_ZdaPv@PLT
.L2000:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2001
	call	_ZdaPv@PLT
.L2001:
	movl	$144, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1997:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2002
	call	_ZdaPv@PLT
.L2002:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2003
	call	_ZdaPv@PLT
	addq	$136, %r14
	cmpq	-136(%rbp), %r14
	jne	.L1990
.L2104:
	movq	-152(%rbp), %r12
.L2004:
	movq	$0, 16(%r12)
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	leaq	-96(%rbp), %r14
	movq	-144(%rbp), %rsi
	movups	%xmm0, (%r12)
	call	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE7reserveEm
	leaq	536(%r13), %rax
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -136(%rbp)
	cmpq	%rbx, %rax
	jne	.L2021
	jmp	.L1992
	.p2align 4,,10
	.p2align 3
.L2107:
	movq	%rax, (%rdx)
	addq	$8, 8(%r12)
.L2009:
	movq	-96(%rbp), %r15
	testq	%r15, %r15
	je	.L2016
	movl	112(%r15), %edi
	testl	%edi, %edi
	js	.L2017
	call	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi@PLT
.L2017:
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2018
	call	_ZdaPv@PLT
.L2018:
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2019
	call	_ZdaPv@PLT
.L2019:
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2020
	call	_ZdaPv@PLT
.L2020:
	movl	$144, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2016:
	addq	$8, %rbx
	cmpq	%rbx, -136(%rbp)
	je	.L1992
.L2021:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal4wasm12NativeModule17PublishCodeLockedESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE
	movq	8(%r12), %rdx
	movq	%rax, %r15
	cmpq	16(%r12), %rdx
	jne	.L2107
	movabsq	$1152921504606846975, %rcx
	movq	(%r12), %r8
	subq	%r8, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L2108
	testq	%rax, %rax
	je	.L2031
	movabsq	$9223372036854775800, %r9
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L2109
.L2011:
	movq	%r9, %rdi
	movq	%rdx, -168(%rbp)
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	call	_Znwm@PLT
	movq	-168(%rbp), %rdx
	movq	-152(%rbp), %r9
	movq	%rax, %rcx
	movq	-160(%rbp), %r8
	movq	%r15, (%rcx,%rdx)
	addq	%rax, %r9
	leaq	8(%rcx,%rdx), %r15
	testq	%rdx, %rdx
	jg	.L2110
.L2013:
	testq	%r8, %r8
	jne	.L2014
.L2015:
	movq	%rcx, %xmm0
	movq	%r15, %xmm4
	movq	%r9, 16(%r12)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r12)
	jmp	.L2009
	.p2align 4,,10
	.p2align 3
.L2109:
	testq	%rcx, %rcx
	jne	.L2111
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	%r15, (%rcx,%rdx)
	leaq	8(%rcx,%rdx), %r15
	testq	%rdx, %rdx
	jle	.L2013
.L2110:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r9, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	memmove@PLT
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r9
	movq	%rax, %rcx
.L2014:
	movq	%r8, %rdi
	movq	%rcx, -160(%rbp)
	movq	%r9, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %rcx
	movq	-152(%rbp), %r9
	jmp	.L2015
	.p2align 4,,10
	.p2align 3
.L2003:
	addq	$136, %r14
	cmpq	-136(%rbp), %r14
	jne	.L1990
	jmp	.L2104
	.p2align 4,,10
	.p2align 3
.L1994:
	leaq	-80(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm8WasmCodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L1992:
	movq	-144(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-72(%rbp), %rbx
	movq	-80(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2006
	.p2align 4,,10
	.p2align 3
.L2007:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L2022
	movl	112(%r14), %edi
	testl	%edi, %edi
	js	.L2023
	call	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi@PLT
.L2023:
	movq	120(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2024
	call	_ZdaPv@PLT
.L2024:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2025
	call	_ZdaPv@PLT
.L2025:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2026
	call	_ZdaPv@PLT
.L2026:
	movl	$144, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2022:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L2007
	movq	-80(%rbp), %r13
.L2006:
	testq	%r13, %r13
	je	.L1975
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1975:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2112
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2031:
	.cfi_restore_state
	movl	$8, %r9d
	jmp	.L2011
	.p2align 4,,10
	.p2align 3
.L2106:
	movq	-144(%rbp), %rax
	salq	$3, %rax
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r15
	movq	%rax, -168(%rbp)
	cmpq	%r15, %rcx
	je	.L1982
	movq	%r13, -192(%rbp)
	movq	%rbx, -176(%rbp)
	movq	%rax, %rbx
	movq	%r12, -184(%rbp)
	movq	%r15, %r12
	movq	%rcx, %r15
	.p2align 4,,10
	.p2align 3
.L1988:
	movq	(%r12), %rcx
	movq	$0, (%r12)
	movq	%rcx, (%rbx)
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1983
	movl	112(%r13), %edi
	testl	%edi, %edi
	js	.L1984
	call	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi@PLT
.L1984:
	movq	120(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1985
	call	_ZdaPv@PLT
.L1985:
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1986
	call	_ZdaPv@PLT
.L1986:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1987
	call	_ZdaPv@PLT
.L1987:
	movl	$144, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1983:
	addq	$8, %r12
	addq	$8, %rbx
	cmpq	%r12, %r15
	jne	.L1988
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r12
	movq	-192(%rbp), %r13
	movq	-80(%rbp), %r15
.L1982:
	testq	%r15, %r15
	je	.L1989
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1989:
	movq	-168(%rbp), %rax
	movq	%rax, %xmm0
	addq	-160(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	jmp	.L1981
.L2029:
	xorl	%edx, %edx
	jmp	.L1976
.L2030:
	xorl	%edx, %edx
	jmp	.L1977
.L1993:
	leaq	.LC42(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2108:
	leaq	.LC41(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2105:
	leaq	.LC77(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2111:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	leaq	0(,%rcx,8), %r9
	jmp	.L2011
.L2112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20916:
	.size	_ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS0_6VectorINS1_21WasmCompilationResultEEE, .-_ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS0_6VectorINS1_21WasmCompilationResultEEE
	.section	.text._ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS1_21WasmCompilationResultE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS1_21WasmCompilationResultE
	.type	_ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS1_21WasmCompilationResultE, @function
_ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS1_21WasmCompilationResultE:
.LFB20915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdx
	movl	$1, %ecx
	movq	%r8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	leaq	-48(%rbp), %rdi
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS0_6VectorINS1_21WasmCompilationResultEEE
	movq	-48(%rbp), %rdi
	movq	(%rdi), %r12
	call	_ZdlPv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2116
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2116:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20915:
	.size	_ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS1_21WasmCompilationResultE, .-_ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS1_21WasmCompilationResultE
	.section	.text._ZN2v88internal4wasm12NativeModuleC2EPNS1_10WasmEngineERKNS1_12WasmFeaturesEbNS0_13VirtualMemoryESt10shared_ptrIKNS1_10WasmModuleEES9_INS0_8CountersEEPS9_IS2_E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModuleC2EPNS1_10WasmEngineERKNS1_12WasmFeaturesEbNS0_13VirtualMemoryESt10shared_ptrIKNS1_10WasmModuleEES9_INS0_8CountersEEPS9_IS2_E
	.type	_ZN2v88internal4wasm12NativeModuleC2EPNS1_10WasmEngineERKNS1_12WasmFeaturesEbNS0_13VirtualMemoryESt10shared_ptrIKNS1_10WasmModuleEES9_INS0_8CountersEEPS9_IS2_E, @function
_ZN2v88internal4wasm12NativeModuleC2EPNS1_10WasmEngineERKNS1_12WasmFeaturesEbNS0_13VirtualMemoryESt10shared_ptrIKNS1_10WasmModuleEES9_INS0_8CountersEEPS9_IS2_E:
.LFB20706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r8, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %r15
	movq	24(%rbp), %r13
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movdqu	(%r15), %xmm1
	movq	8(%r15), %rdx
	movaps	%xmm1, -96(%rbp)
	testq	%rdx, %rdx
	je	.L2118
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2119
	lock addl	$1, 8(%rdx)
.L2118:
	movdqu	8(%rdi), %xmm2
	movq	(%rdi), %rdx
	movq	%r9, -120(%rbp)
	movq	%rax, -112(%rbp)
	movups	%xmm2, -72(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	movzbl	%r14b, %ecx
	leaq	-80(%rbp), %r14
	movq	%r12, %rdi
	leaq	-96(%rbp), %r8
	leaq	280(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal4wasm17WasmCodeAllocatorC1EPNS1_15WasmCodeManagerENS0_13VirtualMemoryEbSt10shared_ptrINS0_8CountersEE
	movq	%r14, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	-88(%rbp), %rdi
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %r9
	testq	%rdi, %rdi
	je	.L2121
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L2122
	movl	$-1, %ecx
	lock xaddl	%ecx, 8(%rdi)
	cmpl	$1, %ecx
	je	.L2158
	.p2align 4,,10
	.p2align 3
.L2121:
	movq	(%rax), %rdx
	movdqu	(%r9), %xmm3
	pxor	%xmm0, %xmm0
	leaq	248(%r12), %rdi
	movl	$31, %ecx
	movq	%rdx, 192(%r12)
	movl	8(%rax), %edx
	movzbl	12(%rax), %eax
	movups	%xmm0, (%r9)
	movl	%edx, 200(%r12)
	xorl	%edx, %edx
	movb	%al, 204(%r12)
	movq	%rdx, %rax
	movups	%xmm3, 208(%r12)
	movups	%xmm0, 224(%r12)
	movq	$0, 240(%r12)
	rep stosq
	movl	$96, %edi
	movups	%xmm0, 496(%r12)
	movups	%xmm0, 512(%r12)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movl	$12, %ecx
	movq	%rax, %r8
	movq	%rax, %rdi
	movq	%rdx, %rax
	rep stosq
	movq	%r8, %rdi
	movq	%r8, -112(%rbp)
	call	_ZN2v84base5MutexC1Ev@PLT
	movq	-112(%rbp), %r8
	leaq	536(%r12), %rdi
	leaq	88(%r8), %rax
	movq	$1, 48(%r8)
	movq	%rax, 40(%r8)
	movq	$0, 56(%r8)
	movq	$0, 64(%r8)
	movl	$0x3f800000, 72(%r8)
	movq	$0, 80(%r8)
	movq	$0, 88(%r8)
	movq	%r8, 528(%r12)
	call	_ZN2v84base5MutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	leaq	584(%r12), %rax
	movq	%rax, 600(%r12)
	movq	%rax, 608(%r12)
	movzbl	_ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip), %eax
	movq	%rbx, 664(%r12)
	movabsq	$4294967297, %rbx
	movl	$0, 584(%r12)
	movq	$0, 592(%r12)
	movq	$0, 616(%r12)
	movq	$0, 656(%r12)
	movl	$0, 672(%r12)
	movb	%al, 676(%r12)
	movb	$0, 677(%r12)
	movups	%xmm0, 624(%r12)
	movups	%xmm0, 640(%r12)
	call	_Znwm@PLT
	movq	%r12, %xmm0
	movq	%rbx, 8(%rax)
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rbx
	movq	%rax, %xmm4
	movq	%rbx, (%rax)
	movq	8(%r13), %rbx
	punpcklqdq	%xmm4, %xmm0
	movq	%r12, 16(%rax)
	movups	%xmm0, 0(%r13)
	testq	%rbx, %rbx
	je	.L2128
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L2129
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rbx)
	cmpl	$1, %eax
	je	.L2159
	.p2align 4,,10
	.p2align 3
.L2128:
	movdqu	(%r15), %xmm5
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %rdi
	movq	%r13, %rsi
	movups	%xmm0, (%r15)
	movq	%r14, %rdx
	movaps	%xmm5, -80(%rbp)
	call	_ZN2v88internal4wasm16CompilationState3NewERKSt10shared_ptrINS1_12NativeModuleEES3_INS0_8CountersEE@PLT
	movq	-96(%rbp), %rax
	movq	520(%r12), %r13
	movq	$0, -96(%rbp)
	movq	%rax, 520(%r12)
	testq	%r13, %r13
	je	.L2135
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm16CompilationStateD1Ev@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	movq	-96(%rbp), %r13
	testq	%r13, %r13
	je	.L2135
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm16CompilationStateD1Ev@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2135:
	movq	-72(%rbp), %r13
	testq	%r13, %r13
	je	.L2138
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L2139
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L2160
	.p2align 4,,10
	.p2align 3
.L2138:
	movq	208(%r12), %rax
	movl	68(%rax), %edx
	testl	%edx, %edx
	jne	.L2161
.L2145:
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	120(%r12), %rax
	movq	%r13, %rdi
	movq	8(%rax), %r15
	movq	16(%rax), %r14
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal4wasm12NativeModule12AddCodeSpaceENS_4base13AddressRegionE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2162
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2119:
	.cfi_restore_state
	addl	$1, 8(%rdx)
	jmp	.L2118
	.p2align 4,,10
	.p2align 3
.L2129:
	movl	8(%rbx), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%rbx)
	cmpl	$1, %eax
	jne	.L2128
.L2159:
	movq	(%rbx), %rax
	movq	%rdx, -112(%rbp)
	movq	%rbx, %rdi
	call	*16(%rax)
	movq	-112(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2132
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rbx)
.L2133:
	cmpl	$1, %eax
	jne	.L2128
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	jmp	.L2128
	.p2align 4,,10
	.p2align 3
.L2122:
	movl	8(%rdi), %ecx
	leal	-1(%rcx), %esi
	movl	%esi, 8(%rdi)
	cmpl	$1, %ecx
	jne	.L2121
.L2158:
	movq	(%rdi), %rcx
	movq	%rdx, -136(%rbp)
	movq	%r9, -128(%rbp)
	movq	%rax, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	*16(%rcx)
	movq	-136(%rbp), %rdx
	movq	-112(%rbp), %rdi
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %r9
	testq	%rdx, %rdx
	je	.L2125
	movl	$-1, %edx
	lock xaddl	%edx, 12(%rdi)
.L2126:
	cmpl	$1, %edx
	jne	.L2121
	movq	(%rdi), %rdx
	movq	%r9, -120(%rbp)
	movq	%rax, -112(%rbp)
	call	*24(%rdx)
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %rax
	jmp	.L2121
	.p2align 4,,10
	.p2align 3
.L2139:
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %eax
	jne	.L2138
.L2160:
	movq	0(%r13), %rax
	movq	%rdx, -104(%rbp)
	movq	%r13, %rdi
	call	*16(%rax)
	movq	-104(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2142
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L2143:
	cmpl	$1, %eax
	jne	.L2138
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L2138
	.p2align 4,,10
	.p2align 3
.L2161:
	leaq	0(,%rdx,8), %r13
	movq	%r13, %rdi
	call	_Znam@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	624(%r12), %rdi
	movq	%rax, 624(%r12)
	testq	%rdi, %rdi
	je	.L2145
	call	_ZdaPv@PLT
	jmp	.L2145
	.p2align 4,,10
	.p2align 3
.L2142:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L2132:
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L2133
	.p2align 4,,10
	.p2align 3
.L2125:
	movl	12(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%rdi)
	jmp	.L2126
.L2162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20706:
	.size	_ZN2v88internal4wasm12NativeModuleC2EPNS1_10WasmEngineERKNS1_12WasmFeaturesEbNS0_13VirtualMemoryESt10shared_ptrIKNS1_10WasmModuleEES9_INS0_8CountersEEPS9_IS2_E, .-_ZN2v88internal4wasm12NativeModuleC2EPNS1_10WasmEngineERKNS1_12WasmFeaturesEbNS0_13VirtualMemoryESt10shared_ptrIKNS1_10WasmModuleEES9_INS0_8CountersEEPS9_IS2_E
	.globl	_ZN2v88internal4wasm12NativeModuleC1EPNS1_10WasmEngineERKNS1_12WasmFeaturesEbNS0_13VirtualMemoryESt10shared_ptrIKNS1_10WasmModuleEES9_INS0_8CountersEEPS9_IS2_E
	.set	_ZN2v88internal4wasm12NativeModuleC1EPNS1_10WasmEngineERKNS1_12WasmFeaturesEbNS0_13VirtualMemoryESt10shared_ptrIKNS1_10WasmModuleEES9_INS0_8CountersEEPS9_IS2_E,_ZN2v88internal4wasm12NativeModuleC2EPNS1_10WasmEngineERKNS1_12WasmFeaturesEbNS0_13VirtualMemoryESt10shared_ptrIKNS1_10WasmModuleEES9_INS0_8CountersEEPS9_IS2_E
	.section	.rodata._ZN2v88internal4wasm15WasmCodeManager15NewNativeModuleEPNS1_10WasmEngineEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE.str1.1,"aMS",@progbits,1
.LC95:
	.string	"NewNativeModule"
	.section	.text._ZN2v88internal4wasm15WasmCodeManager15NewNativeModuleEPNS1_10WasmEngineEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmCodeManager15NewNativeModuleEPNS1_10WasmEngineEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE
	.type	_ZN2v88internal4wasm15WasmCodeManager15NewNativeModuleEPNS1_10WasmEngineEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE, @function
_ZN2v88internal4wasm15WasmCodeManager15NewNativeModuleEPNS1_10WasmEngineEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE:
.LFB20904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$184, %rsp
	movl	16(%rbp), %eax
	movq	%rdi, -152(%rbp)
	movq	%rdx, -176(%rbp)
	movl	%eax, -180(%rbp)
	movq	24(%rbp), %rax
	movq	%r8, -160(%rbp)
	movq	%rax, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rdx
	movq	32(%rsi), %rax
	cmpq	%rax, %rdx
	ja	.L2189
.L2164:
	movq	$0, -112(%rbp)
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r12
	movl	$3, %r13d
	movups	%xmm0, -104(%rbp)
	leaq	37592(%r14), %r15
.L2167:
	xorl	%ecx, %ecx
	movl	$1073741824, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmCodeManager11TryAllocateEmPv
	movq	-80(%rbp), %rax
	movdqu	-72(%rbp), %xmm1
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	movups	%xmm1, -104(%rbp)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	-104(%rbp), %r10
	testq	%r10, %r10
	jne	.L2165
	subl	$1, %r13d
	je	.L2190
	movl	$1, %edx
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Heap26MemoryPressureNotificationENS_19MemoryPressureLevelEb@PLT
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2190:
	xorl	%edx, %edx
	leaq	.LC95(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.p2align 4,,10
	.p2align 3
.L2189:
	movl	$2, %esi
	movq	%rcx, %rdi
	call	_ZN2v87Isolate26MemoryPressureNotificationENS_19MemoryPressureLevelE@PLT
	movq	24(%rbx), %rdx
	movq	8(%rbx), %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	movq	%rax, 32(%rbx)
	mfence
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2165:
	movq	%r10, %rax
	addq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqu	-104(%rbp), %xmm2
	movq	%rax, -192(%rbp)
	movq	-152(%rbp), %rax
	leaq	-112(%rbp), %r13
	movq	%r13, %rdi
	movq	%r10, -216(%rbp)
	movups	%xmm0, (%rax)
	movq	-112(%rbp), %rax
	movups	%xmm2, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	movq	-208(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-216(%rbp), %r10
	movdqu	(%rax), %xmm3
	movups	%xmm0, (%rax)
	movq	40968(%r14), %rax
	movdqu	40960(%r14), %xmm5
	testq	%rax, %rax
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	je	.L2168
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2169
	lock addl	$1, 8(%rax)
.L2168:
	movl	$680, %edi
	movq	%r10, -208(%rbp)
	call	_Znwm@PLT
	pushq	-152(%rbp)
	movq	%r12, %r8
	movq	-160(%rbp), %rdx
	movq	%rax, %rdi
	leaq	-128(%rbp), %rax
	movzbl	-180(%rbp), %ecx
	leaq	-144(%rbp), %r9
	pushq	%rax
	movq	-176(%rbp), %rsi
	call	_ZN2v88internal4wasm12NativeModuleC1EPNS1_10WasmEngineERKNS1_12WasmFeaturesEbNS0_13VirtualMemoryESt10shared_ptrIKNS1_10WasmModuleEES9_INS0_8CountersEEPS9_IS2_E
	movq	-120(%rbp), %r14
	popq	%rax
	movq	-208(%rbp), %r10
	popq	%rdx
	testq	%r14, %r14
	je	.L2171
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L2172
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r14)
.L2173:
	cmpl	$1, %edx
	jne	.L2171
	movq	(%r14), %rdx
	movq	%r10, -176(%rbp)
	movq	%r14, %rdi
	call	*16(%rdx)
	testq	%r15, %r15
	movq	-176(%rbp), %r10
	je	.L2175
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L2176:
	cmpl	$1, %eax
	jne	.L2171
	movq	(%r14), %rax
	movq	%r10, -176(%rbp)
	movq	%r14, %rdi
	call	*24(%rax)
	movq	-176(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L2171:
	movq	-136(%rbp), %r14
	testq	%r14, %r14
	je	.L2178
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L2179
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r14)
.L2180:
	cmpl	$1, %edx
	jne	.L2178
	movq	(%r14), %rdx
	movq	%r10, -176(%rbp)
	movq	%r14, %rdi
	call	*16(%rdx)
	testq	%r15, %r15
	movq	-176(%rbp), %r10
	je	.L2182
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L2183:
	cmpl	$1, %eax
	jne	.L2178
	movq	(%r14), %rax
	movq	%r10, -176(%rbp)
	movq	%r14, %rdi
	call	*24(%rax)
	movq	-176(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L2178:
	movq	%r10, %xmm0
	movq	%r12, %rdi
	leaq	40(%rbx), %r14
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-152(%rbp), %rax
	movq	%r12, %rsi
	leaq	80(%rbx), %rdi
	movdqa	-176(%rbp), %xmm0
	movq	(%rax), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE17_M_emplace_uniqueIJS0_ImS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2191
	movq	-152(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2172:
	.cfi_restore_state
	movl	8(%r14), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r14)
	jmp	.L2173
	.p2align 4,,10
	.p2align 3
.L2179:
	movl	8(%r14), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r14)
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L2169:
	addl	$1, 8(%rax)
	jmp	.L2168
.L2182:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L2183
.L2175:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L2176
.L2191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20904:
	.size	_ZN2v88internal4wasm15WasmCodeManager15NewNativeModuleEPNS1_10WasmEngineEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE, .-_ZN2v88internal4wasm15WasmCodeManager15NewNativeModuleEPNS1_10WasmEngineEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE
	.section	.text._ZN2v88internal4wasm12NativeModule19AddDeserializedCodeEjNS0_6VectorIKhEEjjmmmmmNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IS4_EESA_NS1_8WasmCode4KindENS1_13ExecutionTierE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule19AddDeserializedCodeEjNS0_6VectorIKhEEjjmmmmmNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IS4_EESA_NS1_8WasmCode4KindENS1_13ExecutionTierE
	.type	_ZN2v88internal4wasm12NativeModule19AddDeserializedCodeEjNS0_6VectorIKhEEjjmmmmmNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IS4_EESA_NS1_8WasmCode4KindENS1_13ExecutionTierE, @function
_ZN2v88internal4wasm12NativeModule19AddDeserializedCodeEjNS0_6VectorIKhEEjjmmmmmNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IS4_EESA_NS1_8WasmCode4KindENS1_13ExecutionTierE:
.LFB20790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	xorl	%ecx, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	movq	$-1, %r8
	subq	$120, %rsp
	movq	56(%rbp), %r11
	movl	88(%rbp), %eax
	movl	%r9d, -68(%rbp)
	movq	64(%rbp), %r9
	movq	16(%rbp), %xmm1
	movq	%rdx, -96(%rbp)
	movq	%r13, %rdx
	movq	40(%rbp), %xmm0
	movq	72(%rbp), %r15
	movq	%r11, -88(%rbp)
	movq	%r9, -80(%rbp)
	movhps	24(%rbp), %xmm1
	movl	%eax, -72(%rbp)
	movhps	48(%rbp), %xmm0
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%r12, %rdi
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE
	movq	-96(%rbp), %r10
	movq	%rax, %rcx
	movq	%rdx, -120(%rbp)
	movq	%r13, %rdx
	movq	%rcx, %rdi
	movq	%r10, %rsi
	movq	%rcx, -112(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %r11
	movq	-80(%rbp), %r9
	movl	$144, %edi
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	(%r11), %rdx
	movq	(%r9), %r10
	movq	$0, (%r11)
	movq	8(%r11), %r13
	movq	8(%r15), %r15
	movq	$0, (%r9)
	movq	8(%r9), %r9
	movq	%rdx, -104(%rbp)
	movq	%r10, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	-120(%rbp), %r8
	movq	-96(%rbp), %r10
	movl	%ebx, 72(%rax)
	movl	-68(%rbp), %ebx
	movq	-88(%rbp), %r9
	movq	%rcx, (%rax)
	movl	80(%rbp), %ecx
	movq	-104(%rbp), %rdx
	movq	%rsi, 32(%rax)
	leaq	-64(%rbp), %rsi
	movl	%ebx, 76(%rax)
	movzbl	-72(%rbp), %ebx
	movl	%ecx, 60(%rax)
	movq	32(%rbp), %rcx
	movq	%r12, 48(%rax)
	movdqa	-160(%rbp), %xmm1
	movq	%r13, 128(%rax)
	movdqa	-144(%rbp), %xmm0
	movq	%r8, 8(%rax)
	movq	%r10, 16(%rax)
	movq	%r9, 24(%rax)
	movq	%r15, 40(%rax)
	movl	%r14d, 56(%rax)
	movq	%rcx, 64(%rax)
	movl	$-1, 112(%rax)
	movq	%rdx, 120(%rax)
	movb	%bl, 136(%rax)
	movl	$1, 140(%rax)
	movups	%xmm1, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal4wasm12NativeModule11PublishCodeESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE
	movq	-64(%rbp), %r13
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L2192
	movl	112(%r13), %edi
	testl	%edi, %edi
	jns	.L2212
	movq	120(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2195
.L2214:
	call	_ZdaPv@PLT
.L2195:
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2196
	call	_ZdaPv@PLT
.L2196:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2197
	call	_ZdaPv@PLT
.L2197:
	movl	$144, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2192:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2213
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2212:
	.cfi_restore_state
	call	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi@PLT
	movq	120(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L2214
	jmp	.L2195
.L2213:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20790:
	.size	_ZN2v88internal4wasm12NativeModule19AddDeserializedCodeEjNS0_6VectorIKhEEjjmmmmmNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IS4_EESA_NS1_8WasmCode4KindENS1_13ExecutionTierE, .-_ZN2v88internal4wasm12NativeModule19AddDeserializedCodeEjNS0_6VectorIKhEEjjmmmmmNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IS4_EESA_NS1_8WasmCode4KindENS1_13ExecutionTierE
	.section	.text._ZN2v88internal4wasm12NativeModule26AddAndPublishAnonymousCodeENS0_6HandleINS0_4CodeEEENS1_8WasmCode4KindEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule26AddAndPublishAnonymousCodeENS0_6HandleINS0_4CodeEEENS1_8WasmCode4KindEPKc
	.type	_ZN2v88internal4wasm12NativeModule26AddAndPublishAnonymousCodeENS0_6HandleINS0_4CodeEEENS1_8WasmCode4KindEPKc, @function
_ZN2v88internal4wasm12NativeModule26AddAndPublishAnonymousCodeENS0_6HandleINS0_4CodeEEENS1_8WasmCode4KindEPKc:
.LFB20714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r12
	movq	%rdi, -200(%rbp)
	movl	%edx, -236(%rbp)
	movq	%rcx, -320(%rbp)
	movl	43(%r12), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	js	.L2253
	movq	$0, -208(%rbp)
	movq	7(%r12), %rax
	movslq	11(%rax), %rbx
	movq	%rbx, -304(%rbp)
	movq	%rbx, -312(%rbp)
	testq	%rbx, %rbx
	je	.L2216
	movq	%rbx, %rdi
	call	_Znam@PLT
	movq	(%r14), %r12
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%rax, -208(%rbp)
	movq	7(%r12), %rcx
	leaq	15(%rcx), %rsi
	call	memcpy@PLT
.L2216:
	movq	%r12, %rax
	movq	23(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	subq	$37592, %rbx
	testb	$1, %sil
	jne	.L2217
.L2219:
	movq	7(%rsi), %rsi
.L2218:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2220
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L2221:
	movq	$0, -264(%rbp)
	movslq	11(%rsi), %rax
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	jne	.L2284
.L2223:
	movq	(%r14), %rdx
	movq	%rdx, -128(%rbp)
	movl	43(%rdx), %r9d
	leaq	43(%rdx), %rcx
	testl	%r9d, %r9d
	js	.L2285
	movl	39(%rdx), %eax
	leaq	-128(%rbp), %r12
.L2225:
	movq	%rdx, -192(%rbp)
	movl	(%rcx), %r8d
	cltq
	leaq	63(%rdx), %r15
	movq	%rax, -224(%rbp)
	leaq	-192(%rbp), %rbx
	testl	%r8d, %r8d
	js	.L2286
.L2227:
	movl	(%rcx), %eax
	movl	%eax, %ecx
	andl	$64, %ecx
	movl	%ecx, -240(%rbp)
	jne	.L2228
	movl	%eax, %ecx
	shrl	%ecx
	andl	$31, %ecx
	cmpl	$5, %ecx
	je	.L2228
.L2229:
	movq	%r12, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZNK2v88internal4Code19has_safepoint_tableEv@PLT
	testb	%al, %al
	movq	(%r14), %rax
	jne	.L2230
	movq	$0, -288(%rbp)
.L2231:
	movslq	51(%rax), %rcx
	movq	-200(%rbp), %rsi
	movq	$-1, %r8
	movslq	55(%rax), %rax
	movq	-224(%rbp), %rdx
	movq	%rsi, %rdi
	movq	%rcx, -272(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal4wasm17WasmCodeAllocator23AllocateForCodeInRegionEPNS1_12NativeModuleEmNS_4base13AddressRegionE
	movq	%r15, %rsi
	movq	%rdx, -248(%rbp)
	movq	%rax, %r13
	movq	-224(%rbp), %rdx
	movq	%r13, %rdi
	call	memcpy@PLT
	movq	(%r14), %rsi
	movq	%rsi, -128(%rbp)
	movl	43(%rsi), %edi
	leaq	63(%rsi), %rax
	testl	%edi, %edi
	js	.L2287
.L2233:
	movl	_ZN2v88internal9RelocInfo10kApplyMaskE(%rip), %r15d
	movq	%r13, %r14
	movq	%rbx, %rdi
	subq	%rax, %r14
	movq	-232(%rbp), %rax
	orl	$32, %r15d
	leaq	0(%r13,%rax), %r9
	movl	%r15d, %edx
	movq	%r9, -296(%rbp)
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	-208(%rbp), %rcx
	pushq	%r15
	movq	-296(%rbp), %r9
	movq	%r12, %rdi
	leaq	-176(%rbp), %r15
	movq	-312(%rbp), %r8
	movq	-248(%rbp), %rdx
	call	_ZN2v88internal13RelocIteratorC1ENS0_6VectorIhEENS2_IKhEEmi@PLT
	cmpb	$0, -72(%rbp)
	popq	%rcx
	popq	%rsi
	je	.L2234
	jmp	.L2241
	.p2align 4,,10
	.p2align 3
.L2290:
	cmpb	$6, %al
	je	.L2256
	cmpb	$8, %al
	je	.L2288
	.p2align 4,,10
	.p2align 3
.L2238:
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	jne	.L2241
.L2234:
	movzbl	-104(%rbp), %eax
	cmpb	$5, %al
	je	.L2289
	testb	%al, %al
	jne	.L2290
.L2256:
	movq	-112(%rbp), %rax
	subl	%r14d, (%rax)
	jmp	.L2238
	.p2align 4,,10
	.p2align 3
.L2228:
	shrl	$7, %eax
	andl	$16777215, %eax
	movl	%eax, -240(%rbp)
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2289:
	movq	%r15, %rdi
	call	_ZNK2v88internal9RelocInfo13wasm_call_tagEv@PLT
	movq	-200(%rbp), %rcx
	leaq	-112(%rbp), %rdi
	movl	$1, %edx
	cltq
	movq	248(%rcx,%rax,8), %rsi
	call	_ZN2v88internal9RelocInfo26set_wasm_stub_call_addressEmNS0_15ICacheFlushModeE@PLT
	jmp	.L2238
	.p2align 4,,10
	.p2align 3
.L2241:
	movq	-232(%rbp), %r14
	movq	-248(%rbp), %r15
	movq	%r13, %rdi
	movq	-288(%rbp), %xmm1
	movq	%r14, %xmm0
	movq	%r15, %rsi
	movhps	-272(%rbp), %xmm1
	movhps	-224(%rbp), %xmm0
	movaps	%xmm1, -288(%rbp)
	movaps	%xmm0, -224(%rbp)
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	movl	$144, %edi
	call	_Znwm@PLT
	movdqa	-288(%rbp), %xmm1
	movdqa	-224(%rbp), %xmm0
	movq	%r13, (%rax)
	movq	%rax, %rbx
	cmpb	$0, _ZN2v88internal20FLAG_print_wasm_codeE(%rip)
	movq	%r15, 8(%rax)
	movq	-208(%rbp), %rax
	movl	$-1, 56(%rbx)
	movq	%rax, 16(%rbx)
	movq	-304(%rbp), %rax
	movq	%r14, 64(%rbx)
	movq	%rax, 24(%rbx)
	movq	-264(%rbp), %rax
	movl	$0, 76(%rbx)
	movq	%rax, 32(%rbx)
	movq	-256(%rbp), %rax
	movl	$-1, 112(%rbx)
	movq	%rax, 40(%rbx)
	movq	-200(%rbp), %rax
	movq	$0, 120(%rbx)
	movq	%rax, 48(%rbx)
	movl	-236(%rbp), %eax
	movq	$0, 128(%rbx)
	movl	%eax, 60(%rbx)
	movl	-240(%rbp), %eax
	movb	$0, 136(%rbx)
	movl	%eax, 72(%rbx)
	movl	$1, 140(%rbx)
	movups	%xmm1, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	je	.L2291
	movl	-236(%rbp), %edx
	testl	%edx, %edx
	jne	.L2283
.L2242:
	movq	-320(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal4wasm8WasmCode5PrintEPKc
.L2245:
	movq	-200(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rbx, -128(%rbp)
	call	_ZN2v88internal4wasm12NativeModule11PublishCodeESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE
	movq	-128(%rbp), %r13
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L2215
	movl	112(%r13), %edi
	testl	%edi, %edi
	jns	.L2292
	movq	120(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2248
.L2295:
	call	_ZdaPv@PLT
.L2248:
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2249
	call	_ZdaPv@PLT
.L2249:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2250
	call	_ZdaPv@PLT
.L2250:
	movl	$144, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2215:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2293
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2291:
	.cfi_restore_state
	movl	-236(%rbp), %eax
	testl	%eax, %eax
	je	.L2251
.L2283:
	cmpb	$0, _ZN2v88internal25FLAG_print_wasm_stub_codeE(%rip)
	jne	.L2242
.L2251:
	cmpb	$0, _ZN2v88internal15FLAG_print_codeE(%rip)
	je	.L2245
	jmp	.L2242
	.p2align 4,,10
	.p2align 3
.L2288:
	movq	-112(%rbp), %rax
	addq	%r14, (%rax)
	jmp	.L2238
	.p2align 4,,10
	.p2align 3
.L2230:
	movslq	47(%rax), %rcx
	movq	%rcx, -288(%rbp)
	jmp	.L2231
	.p2align 4,,10
	.p2align 3
.L2220:
	movq	41088(%rbx), %r12
	cmpq	%r12, 41096(%rbx)
	je	.L2294
.L2222:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L2221
	.p2align 4,,10
	.p2align 3
.L2284:
	movq	%rax, %rdi
	call	_Znam@PLT
	movq	(%r12), %rsi
	movq	%rax, -264(%rbp)
	movslq	11(%rsi), %rdx
	testq	%rdx, %rdx
	jle	.L2223
	addq	$15, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	jmp	.L2223
	.p2align 4,,10
	.p2align 3
.L2253:
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -208(%rbp)
	jmp	.L2216
	.p2align 4,,10
	.p2align 3
.L2217:
	movq	-1(%rsi), %rax
	cmpw	$70, 11(%rax)
	jne	.L2219
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2285:
	leaq	-128(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	movq	(%r14), %rdx
	leaq	43(%rdx), %rcx
	jmp	.L2225
	.p2align 4,,10
	.p2align 3
.L2287:
	movq	%r12, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	(%r14), %rsi
	jmp	.L2233
	.p2align 4,,10
	.p2align 3
.L2286:
	movq	%rbx, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	(%r14), %rdx
	movq	%rax, %r15
	leaq	43(%rdx), %rcx
	jmp	.L2227
	.p2align 4,,10
	.p2align 3
.L2292:
	call	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi@PLT
	movq	120(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L2295
	jmp	.L2248
.L2294:
	movq	%rbx, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2222
.L2293:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20714:
	.size	_ZN2v88internal4wasm12NativeModule26AddAndPublishAnonymousCodeENS0_6HandleINS0_4CodeEEENS1_8WasmCode4KindEPKc, .-_ZN2v88internal4wasm12NativeModule26AddAndPublishAnonymousCodeENS0_6HandleINS0_4CodeEEENS1_8WasmCode4KindEPKc
	.section	.text._ZN2v88internal4wasm12NativeModule17AddCodeForTestingENS0_6HandleINS0_4CodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12NativeModule17AddCodeForTestingENS0_6HandleINS0_4CodeEEE
	.type	_ZN2v88internal4wasm12NativeModule17AddCodeForTestingENS0_6HandleINS0_4CodeEEE, @function
_ZN2v88internal4wasm12NativeModule17AddCodeForTestingENS0_6HandleINS0_4CodeEEE:
.LFB20711:
	.cfi_startproc
	endbr64
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	_ZN2v88internal4wasm12NativeModule26AddAndPublishAnonymousCodeENS0_6HandleINS0_4CodeEEENS1_8WasmCode4KindEPKc
	.cfi_endproc
.LFE20711:
	.size	_ZN2v88internal4wasm12NativeModule17AddCodeForTestingENS0_6HandleINS0_4CodeEEE, .-_ZN2v88internal4wasm12NativeModule17AddCodeForTestingENS0_6HandleINS0_4CodeEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE, @function
_GLOBAL__sub_I__ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE:
.LFB27109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27109:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE, .-_GLOBAL__sub_I__ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm22DisjointAllocationPool5MergeENS_4base13AddressRegionE
	.weak	_ZTVN2v88internal9AssemblerE
	.section	.data.rel.ro.local._ZTVN2v88internal9AssemblerE,"awG",@progbits,_ZTVN2v88internal9AssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal9AssemblerE, @object
	.size	_ZTVN2v88internal9AssemblerE, 40
_ZTVN2v88internal9AssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal9AssemblerD1Ev
	.quad	_ZN2v88internal9AssemblerD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.weak	_ZTVN2v88internal14MacroAssemblerE
	.section	.data.rel.ro._ZTVN2v88internal14MacroAssemblerE,"awG",@progbits,_ZTVN2v88internal14MacroAssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal14MacroAssemblerE, @object
	.size	_ZTVN2v88internal14MacroAssemblerE, 112
_ZTVN2v88internal14MacroAssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14MacroAssemblerD1Ev
	.quad	_ZN2v88internal14MacroAssemblerD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.quad	_ZN2v88internal14TurboAssembler4JumpERKNS0_17ExternalReferenceE
	.quad	_ZN2v88internal14TurboAssembler18CallBuiltinByIndexENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler14JumpCodeObjectENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_
	.quad	_ZN2v88internal14TurboAssembler22LoadFromConstantsTableENS0_8RegisterEi
	.quad	_ZN2v88internal14TurboAssembler22LoadRootRegisterOffsetENS0_8RegisterEl
	.quad	_ZN2v88internal14TurboAssembler16LoadRootRelativeENS0_8RegisterEi
	.quad	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.weak	_ZTVN2v88internal4wasm18JumpTableAssemblerE
	.section	.data.rel.ro._ZTVN2v88internal4wasm18JumpTableAssemblerE,"awG",@progbits,_ZTVN2v88internal4wasm18JumpTableAssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm18JumpTableAssemblerE, @object
	.size	_ZTVN2v88internal4wasm18JumpTableAssemblerE, 112
_ZTVN2v88internal4wasm18JumpTableAssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm18JumpTableAssemblerD1Ev
	.quad	_ZN2v88internal4wasm18JumpTableAssemblerD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.quad	_ZN2v88internal14TurboAssembler4JumpERKNS0_17ExternalReferenceE
	.quad	_ZN2v88internal14TurboAssembler18CallBuiltinByIndexENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler14JumpCodeObjectENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_
	.quad	_ZN2v88internal14TurboAssembler22LoadFromConstantsTableENS0_8RegisterEi
	.quad	_ZN2v88internal14TurboAssembler22LoadRootRegisterOffsetENS0_8RegisterEl
	.quad	_ZN2v88internal14TurboAssembler16LoadRootRelativeENS0_8RegisterEi
	.quad	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageE,"aw"
	.align 8
	.type	_ZTVN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageE, @object
	.size	_ZTVN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageE, 40
_ZTVN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD1Ev
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageD0Ev
	.quad	_ZNK2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorage7GetCodeENS1_12WireBytesRefE
	.weak	_ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm12NativeModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal11OwnedVectorIKhEESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE,"aw"
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm12_GLOBAL__N_128NativeModuleWireBytesStorageESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.tbss._ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE,"awT",@nobits
	.align 8
	.type	_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE, 8
_ZN2v88internal4wasm12_GLOBAL__N_123current_code_refs_scopeE:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC81:
	.long	646
	.long	647
	.long	648
	.long	649
	.align 16
.LC82:
	.long	650
	.long	651
	.long	652
	.long	653
	.align 16
.LC83:
	.long	654
	.long	655
	.long	656
	.long	657
	.align 16
.LC84:
	.long	632
	.long	633
	.long	634
	.long	635
	.align 16
.LC85:
	.long	636
	.long	637
	.long	638
	.long	639
	.align 16
.LC86:
	.long	640
	.long	641
	.long	642
	.long	643
	.align 16
.LC87:
	.long	644
	.long	645
	.long	709
	.long	658
	.align 16
.LC93:
	.long	31
	.long	31
	.long	31
	.long	31
	.align 16
.LC94:
	.long	-32
	.long	-32
	.long	-32
	.long	-32
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
