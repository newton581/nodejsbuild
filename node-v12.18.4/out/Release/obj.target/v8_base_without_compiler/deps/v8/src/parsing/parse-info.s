	.file	"parse-info.cc"
	.text
	.section	.rodata._ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/v8/src/parsing/parse-info.cc:24"
	.section	.text._ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE
	.type	_ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE, @function
_ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE:
.LFB21773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$64, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	pxor	%xmm0, %xmm0
	movl	$512, %eax
	xorl	%edx, %edx
	movdqa	.LC1(%rip), %xmm1
	movq	%r12, (%rbx)
	movl	$0, 8(%rbx)
	movw	%ax, 48(%rbx)
	movq	$-1, 68(%rbx)
	movq	$0, 168(%rbx)
	movw	%dx, 176(%rbx)
	movb	$0, 178(%rbx)
	movq	$-1, 184(%rbx)
	movl	$0, 192(%rbx)
	movq	$0, 216(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm1, 52(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 152(%rbx)
	movups	%xmm0, 200(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21773:
	.size	_ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE, .-_ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE
	.globl	_ZN2v88internal9ParseInfoC1EPNS0_19AccountingAllocatorE
	.set	_ZN2v88internal9ParseInfoC1EPNS0_19AccountingAllocatorE,_ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE
	.section	.text._ZN2v88internal9ParseInfoC2EPNS0_7IsolateEPNS0_19AccountingAllocatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfoC2EPNS0_7IsolateEPNS0_19AccountingAllocatorE
	.type	_ZN2v88internal9ParseInfoC2EPNS0_7IsolateEPNS0_19AccountingAllocatorE, @function
_ZN2v88internal9ParseInfoC2EPNS0_7IsolateEPNS0_19AccountingAllocatorE:
.LFB21776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$64, %edi
	call	_Znwm@PLT
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movl	$512, %eax
	xorl	%edx, %edx
	movq	%r13, (%rbx)
	movdqa	.LC1(%rip), %xmm1
	movl	$0, 8(%rbx)
	pxor	%xmm0, %xmm0
	movq	$0, 136(%rbx)
	movw	%ax, 48(%rbx)
	movq	$-1, 68(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 128(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 160(%rbx)
	movq	$0, 168(%rbx)
	movw	%dx, 176(%rbx)
	movb	$0, 178(%rbx)
	movq	$-1, 184(%rbx)
	movl	$0, 192(%rbx)
	movq	$0, 216(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm1, 52(%rbx)
	movups	%xmm0, 200(%rbx)
	movq	1176(%r12), %rax
	movq	37528(%r12), %xmm0
	movhps	15(%rax), %xmm0
	movq	40960(%r12), %rax
	movups	%xmm0, 32(%rbx)
	addq	$23240, %rax
	movq	%rax, 136(%rbx)
	movq	41016(%r12), %rax
	movq	%rax, 144(%rbx)
	movq	41496(%r12), %rax
	cmpb	$0, _ZN2v88internal33FLAG_enable_lazy_source_positionsE(%rip)
	movq	%rax, 120(%rbx)
	movl	$268435456, %eax
	jne	.L44
.L7:
	movl	41832(%r12), %edx
	movl	%eax, 8(%rbx)
	testl	%edx, %edx
	je	.L8
	movl	%eax, %ecx
	subl	$3, %edx
	orb	$4, %ch
	cmpl	$1, %edx
	ja	.L45
	orb	$12, %ah
	movl	%eax, 8(%rbx)
.L8:
	cmpl	$1, 41836(%r12)
	jne	.L10
	orb	$2, %ah
	movl	%eax, 8(%rbx)
.L10:
	movq	41528(%r12), %rdi
	call	_ZNK2v88internal18CompilerDispatcher9IsEnabledEv@PLT
	testb	%al, %al
	jne	.L46
.L12:
	cmpb	$0, _ZN2v88internal15FLAG_always_optE(%rip)
	movl	8(%rbx), %eax
	jne	.L16
	cmpb	$0, _ZN2v88internal23FLAG_prepare_always_optE(%rip)
	je	.L47
.L16:
	orl	$131072, %eax
.L17:
	movl	%eax, %edx
	andl	$-262145, %edx
	cmpb	$0, _ZN2v88internal9FLAG_lazyE(%rip)
	je	.L19
	orl	$262144, %eax
	movl	%eax, %edx
.L19:
	movl	%edx, %eax
	andl	$-524289, %eax
	cmpb	$0, _ZN2v88internal25FLAG_allow_natives_syntaxE(%rip)
	je	.L21
	movl	%edx, %eax
	orl	$524288, %eax
.L21:
	movl	%eax, %edx
	andl	$-4194305, %edx
	cmpb	$0, _ZN2v88internal27FLAG_harmony_dynamic_importE(%rip)
	je	.L23
	orl	$4194304, %eax
	movl	%eax, %edx
.L23:
	movl	%edx, %eax
	andl	$-8388609, %eax
	cmpb	$0, _ZN2v88internal24FLAG_harmony_import_metaE(%rip)
	je	.L25
	movl	%edx, %eax
	orl	$8388608, %eax
.L25:
	movl	%eax, %edx
	andl	$-16777217, %edx
	cmpb	$0, _ZN2v88internal30FLAG_harmony_optional_chainingE(%rip)
	je	.L27
	orl	$16777216, %eax
	movl	%eax, %edx
.L27:
	movl	%edx, %eax
	andl	$-536870913, %eax
	cmpb	$0, _ZN2v88internal20FLAG_harmony_nullishE(%rip)
	je	.L29
	movl	%edx, %eax
	orl	$536870912, %eax
.L29:
	movl	%eax, %edx
	andl	$-67108865, %edx
	cmpb	$0, _ZN2v88internal28FLAG_harmony_private_methodsE(%rip)
	je	.L31
	orl	$67108864, %eax
	movl	%eax, %edx
.L31:
	movl	%edx, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	andl	$-131073, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$16, %edi
	movq	41528(%r12), %r12
	call	_Znwm@PLT
	movq	160(%rbx), %r13
	movq	%r12, (%rax)
	movq	$0, 8(%rax)
	movq	%rax, 160(%rbx)
	testq	%r13, %r13
	je	.L12
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L15
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L14
.L15:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r12, %rdi
	call	_ZNK2v88internal7Isolate34NeedsDetailedOptimizedCodeLineInfoEv@PLT
	testb	%al, %al
	movl	8(%rbx), %eax
	je	.L6
	orl	$268435456, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L6:
	andl	$-268435457, %eax
	jmp	.L7
.L45:
	movl	%ecx, 8(%rbx)
	movl	%ecx, %eax
	jmp	.L8
	.cfi_endproc
.LFE21776:
	.size	_ZN2v88internal9ParseInfoC2EPNS0_7IsolateEPNS0_19AccountingAllocatorE, .-_ZN2v88internal9ParseInfoC2EPNS0_7IsolateEPNS0_19AccountingAllocatorE
	.globl	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateEPNS0_19AccountingAllocatorE
	.set	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateEPNS0_19AccountingAllocatorE,_ZN2v88internal9ParseInfoC2EPNS0_7IsolateEPNS0_19AccountingAllocatorE
	.section	.text._ZN2v88internal9ParseInfoC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfoC2EPNS0_7IsolateE
	.type	_ZN2v88internal9ParseInfoC2EPNS0_7IsolateE, @function
_ZN2v88internal9ParseInfoC2EPNS0_7IsolateE:
.LFB21779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	41136(%rsi), %rdx
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateEPNS0_19AccountingAllocatorE
	movslq	4804(%rbx), %rcx
	movl	$1, %eax
	movabsq	$4294967296, %rdx
	cmpq	$2147483647, %rcx
	je	.L49
	leal	1(%rcx), %eax
	movq	%rax, %rdx
	salq	$32, %rdx
.L49:
	movq	41016(%rbx), %r13
	movq	%rdx, 4800(%rbx)
	movl	%eax, 52(%r12)
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L55
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movl	52(%r12), %edx
	addq	$8, %rsp
	movq	%r13, %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi@PLT
	.cfi_endproc
.LFE21779:
	.size	_ZN2v88internal9ParseInfoC2EPNS0_7IsolateE, .-_ZN2v88internal9ParseInfoC2EPNS0_7IsolateE
	.globl	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateE
	.set	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateE,_ZN2v88internal9ParseInfoC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.type	_ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, @function
_ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE:
.LFB21786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	41136(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateEPNS0_19AccountingAllocatorE
	movq	0(%r13), %rdx
	movq	%r13, 80(%rbx)
	movslq	67(%rdx), %rax
	leaq	95(%rdx), %rsi
	movl	%eax, 52(%rbx)
	testb	$1, 99(%rdx)
	movl	8(%rbx), %eax
	je	.L57
	orl	$4, %eax
.L58:
	movl	%eax, %ecx
	movl	%eax, 8(%rbx)
	andl	$-65, %eax
	orl	$64, %ecx
	testb	$32, 4(%rsi)
	cmovne	%ecx, %eax
	movl	%eax, 8(%rbx)
	testb	$8, %ah
	jne	.L83
.L63:
	orb	$-127, %al
	cmpl	$1, 41836(%r12)
	movl	%eax, 8(%rbx)
	je	.L84
.L66:
	andb	$-3, %ah
	movl	%eax, 8(%rbx)
	movq	71(%rdx), %rax
	testb	$1, %al
	jne	.L85
.L70:
	cmpl	$1, 41836(%r12)
	je	.L86
.L72:
	movl	8(%rbx), %eax
	andb	$-3, %ah
.L73:
	movl	%eax, 8(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	andl	$-5, %eax
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	-48(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	movl	8(%rbx), %eax
	je	.L88
	movq	0(%r13), %rdx
	orb	$2, %ah
	movl	%eax, 8(%rbx)
	movq	71(%rdx), %rax
	testb	$1, %al
	je	.L70
	.p2align 4,,10
	.p2align 3
.L85:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L70
	cmpl	$1, 41836(%r12)
	movb	$4, 49(%rbx)
	jne	.L72
.L86:
	leaq	-48(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L72
	movl	8(%rbx), %eax
	orb	$2, %ah
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L83:
	leaq	-48(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L62
.L82:
	movl	8(%rbx), %eax
	movq	0(%r13), %rdx
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L88:
	movq	0(%r13), %rdx
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L62:
	movq	(%rbx), %r14
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	subq	%rax, %rdx
	cmpq	$55, %rdx
	jbe	.L89
	leaq	56(%rax), %rdx
	movq	%rdx, 16(%r14)
.L65:
	leaq	16(%rax), %rdx
	movq	%r14, (%rax)
	movl	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	%rdx, 40(%rax)
	movq	$0, 48(%rax)
	movq	%rax, 152(%rbx)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$56, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L65
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21786:
	.size	_ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, .-_ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.globl	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.set	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE,_ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.section	.text._ZN2v88internal9ParseInfoD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfoD2Ev
	.type	_ZN2v88internal9ParseInfoD2Ev, @function
_ZN2v88internal9ParseInfoD2Ev:
.LFB21792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	216(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L91
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L92
.L91:
	movq	160(%r12), %r13
	testq	%r13, %r13
	je	.L93
	movq	8(%r13), %rbx
	testq	%rbx, %rbx
	je	.L94
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L95
.L94:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L93:
	movq	112(%r12), %r13
	testq	%r13, %r13
	je	.L96
	movq	0(%r13), %rdi
	call	free@PLT
	movl	$1112, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L96:
	movq	104(%r12), %rdi
	testq	%rdi, %rdi
	je	.L97
	movq	(%rdi), %rax
	call	*8(%rax)
.L97:
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L98
	movq	(%rdi), %rax
	call	*8(%rax)
.L98:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L90
	movq	%r12, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21792:
	.size	_ZN2v88internal9ParseInfoD2Ev, .-_ZN2v88internal9ParseInfoD2Ev
	.globl	_ZN2v88internal9ParseInfoD1Ev
	.set	_ZN2v88internal9ParseInfoD1Ev,_ZN2v88internal9ParseInfoD2Ev
	.section	.text._ZNK2v88internal9ParseInfo5scopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ParseInfo5scopeEv
	.type	_ZNK2v88internal9ParseInfo5scopeEv, @function
_ZNK2v88internal9ParseInfo5scopeEv:
.LFB21794:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	movq	40(%rax), %rax
	ret
	.cfi_endproc
.LFE21794:
	.size	_ZNK2v88internal9ParseInfo5scopeEv, .-_ZNK2v88internal9ParseInfo5scopeEv
	.section	.text._ZN2v88internal9ParseInfo26GetOrCreateAstValueFactoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfo26GetOrCreateAstValueFactoryEv
	.type	_ZN2v88internal9ParseInfo26GetOrCreateAstValueFactoryEv, @function
_ZN2v88internal9ParseInfo26GetOrCreateAstValueFactoryEv:
.LFB21796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	112(%rdi), %r12
	testq	%r12, %r12
	je	.L128
.L122:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	(%rdi), %rcx
	movq	120(%rdi), %r13
	movq	%rdi, %rbx
	movq	40(%rdi), %r15
	movl	$1112, %edi
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	72(%r13), %rax
	movq	%rax, 8(%r12)
	movq	80(%r13), %rax
	movq	%rax, 16(%r12)
	movl	72(%r13), %eax
	leaq	(%rax,%rax,2), %r14
	salq	$3, %r14
	movq	%r14, %rdi
	call	malloc@PLT
	movq	64(%r13), %rsi
	movq	%r14, %rdx
	movq	%rax, (%r12)
	movq	%rax, %rdi
	call	memcpy@PLT
	leaq	24(%r12), %rax
	leaq	80(%r12), %rdi
	movq	-56(%rbp), %rcx
	movq	%rax, 32(%r12)
	leaq	40(%r12), %rax
	andq	$-8, %rdi
	movq	%rax, 48(%r12)
	movl	%r12d, %eax
	subl	%edi, %eax
	movq	%r13, 56(%r12)
	movq	%rcx, 1096(%r12)
	leal	1096(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	movq	$0, 24(%r12)
	movq	$0, 40(%r12)
	movq	$0, 64(%r12)
	movq	%r15, 1104(%r12)
	movq	$0, 72(%r12)
	movq	$0, 1088(%r12)
	rep stosq
	movq	%r12, %rdi
	call	_ZN2v88internal15AstValueFactory13NewConsStringEv@PLT
	movq	112(%rbx), %r13
	movq	%r12, 112(%rbx)
	movq	%rax, 64(%r12)
	testq	%r13, %r13
	je	.L122
	movq	0(%r13), %rdi
	call	free@PLT
	movl	$1112, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	112(%rbx), %r12
	jmp	.L122
	.cfi_endproc
.LFE21796:
	.size	_ZN2v88internal9ParseInfo26GetOrCreateAstValueFactoryEv, .-_ZN2v88internal9ParseInfo26GetOrCreateAstValueFactoryEv
	.section	.text._ZN2v88internal9ParseInfo10FromParentEPKS1_PNS0_19AccountingAllocatorEPKNS0_15FunctionLiteralEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfo10FromParentEPKS1_PNS0_19AccountingAllocatorEPKNS0_15FunctionLiteralEPKNS0_12AstRawStringE
	.type	_ZN2v88internal9ParseInfo10FromParentEPKS1_PNS0_19AccountingAllocatorEPKNS0_15FunctionLiteralEPKNS0_12AstRawStringE, @function
_ZN2v88internal9ParseInfo10FromParentEPKS1_PNS0_19AccountingAllocatorEPKNS0_15FunctionLiteralEPKNS0_12AstRawStringE:
.LFB21790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$224, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	%r8, -56(%rbp)
	movq	%rdx, -64(%rbp)
	call	_Znwm@PLT
	movl	$64, %edi
	movq	%rax, %r12
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	%r12, 0(%r13)
	movl	8(%r14), %eax
	xorl	%edi, %edi
	movl	$512, %esi
	movq	%r15, (%r12)
	pxor	%xmm0, %xmm0
	movl	%eax, 8(%r12)
	movl	52(%r14), %eax
	movw	%si, 48(%r12)
	movl	%eax, 52(%r12)
	movq	144(%r14), %rax
	movw	%di, 176(%r12)
	movq	%r12, %rdi
	movq	%rax, 144(%r12)
	movq	120(%r14), %rax
	movq	$0, 32(%r12)
	movq	%rax, 120(%r12)
	movq	40(%r14), %rax
	movq	$0, 56(%r12)
	movq	$-1, 64(%r12)
	movl	$-1, 72(%r12)
	movq	$0, 112(%r12)
	movq	$0, 152(%r12)
	movq	$0, 160(%r12)
	movq	$0, 168(%r12)
	movb	$0, 178(%r12)
	movq	$-1, 184(%r12)
	movl	$0, 192(%r12)
	movq	$0, 216(%r12)
	movq	%rax, 40(%r12)
	movups	%xmm0, 16(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movups	%xmm0, 128(%r12)
	movups	%xmm0, 200(%r12)
	call	_ZN2v88internal9ParseInfo26GetOrCreateAstValueFactoryEv
	movq	-56(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal15AstValueFactory21CloneFromOtherFactoryEPKNS0_12AstRawStringE@PLT
	movq	0(%r13), %rdx
	movq	%rbx, %rdi
	movq	%rax, 128(%rdx)
	movq	0(%r13), %r12
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	%rbx, %rdi
	movl	%eax, 56(%r12)
	movq	0(%r13), %r12
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	%rbx, %rdi
	movl	%eax, 60(%r12)
	movq	0(%r13), %rax
	movl	28(%rbx), %edx
	movl	%edx, 68(%rax)
	movq	0(%r13), %r12
	call	_ZNK2v88internal15FunctionLiteral13language_modeEv@PLT
	testb	%al, %al
	movl	8(%r12), %eax
	je	.L130
	orl	$8, %eax
.L131:
	movl	%eax, 8(%r12)
	movq	%rbx, %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	movl	8(%r12), %edx
	movb	%al, 48(%r12)
	movl	4(%rbx), %eax
	shrl	$7, %eax
	andl	$7, %eax
	movb	%al, 49(%r12)
	testb	$16, 6(%rbx)
	je	.L132
	orb	$-128, %dh
.L133:
	movl	%edx, 8(%r12)
	movl	28(%rbx), %ecx
	movl	%edx, %eax
	andl	$-2, %eax
	testl	%ecx, %ecx
	jne	.L135
	movl	%edx, %eax
	orl	$1, %eax
.L135:
	movl	%eax, %edx
	movl	%eax, 8(%r12)
	andl	$-134217729, %eax
	orl	$134217728, %edx
	testb	$64, 6(%rbx)
	cmovne	%edx, %eax
	movl	%eax, 8(%r12)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	andb	$127, %dh
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L130:
	andl	$-9, %eax
	jmp	.L131
	.cfi_endproc
.LFE21790:
	.size	_ZN2v88internal9ParseInfo10FromParentEPKS1_PNS0_19AccountingAllocatorEPKNS0_15FunctionLiteralEPKNS0_12AstRawStringE, .-_ZN2v88internal9ParseInfo10FromParentEPKS1_PNS0_19AccountingAllocatorEPKNS0_15FunctionLiteralEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal9ParseInfo22AllocateSourceRangeMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfo22AllocateSourceRangeMapEv
	.type	_ZN2v88internal9ParseInfo22AllocateSourceRangeMapEv, @function
_ZN2v88internal9ParseInfo22AllocateSourceRangeMapEv:
.LFB21797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$55, %rdx
	jbe	.L143
	leaq	56(%rax), %rdx
	movq	%rdx, 16(%r12)
.L141:
	leaq	16(%rax), %rdx
	movq	%r12, (%rax)
	movl	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	%rdx, 40(%rax)
	movq	$0, 48(%rax)
	movq	%rax, 152(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L141
	.cfi_endproc
.LFE21797:
	.size	_ZN2v88internal9ParseInfo22AllocateSourceRangeMapEv, .-_ZN2v88internal9ParseInfo22AllocateSourceRangeMapEv
	.section	.text._ZN2v88internal9ParseInfo20ResetCharacterStreamEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfo20ResetCharacterStreamEv
	.type	_ZN2v88internal9ParseInfo20ResetCharacterStreamEv, @function
_ZN2v88internal9ParseInfo20ResetCharacterStreamEv:
.LFB21798:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %r8
	movq	$0, 96(%rdi)
	testq	%r8, %r8
	je	.L144
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	8(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L144:
	ret
	.cfi_endproc
.LFE21798:
	.size	_ZN2v88internal9ParseInfo20ResetCharacterStreamEv, .-_ZN2v88internal9ParseInfo20ResetCharacterStreamEv
	.section	.text._ZN2v88internal9ParseInfo20set_character_streamESt10unique_ptrINS0_20Utf16CharacterStreamESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfo20set_character_streamESt10unique_ptrINS0_20Utf16CharacterStreamESt14default_deleteIS3_EE
	.type	_ZN2v88internal9ParseInfo20set_character_streamESt10unique_ptrINS0_20Utf16CharacterStreamESt14default_deleteIS3_EE, @function
_ZN2v88internal9ParseInfo20set_character_streamESt10unique_ptrINS0_20Utf16CharacterStreamESt14default_deleteIS3_EE:
.LFB21799:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	movq	(%rsi), %rdx
	movq	%rax, (%rsi)
	movq	%rdx, 96(%rdi)
	ret
	.cfi_endproc
.LFE21799:
	.size	_ZN2v88internal9ParseInfo20set_character_streamESt10unique_ptrINS0_20Utf16CharacterStreamESt14default_deleteIS3_EE, .-_ZN2v88internal9ParseInfo20set_character_streamESt10unique_ptrINS0_20Utf16CharacterStreamESt14default_deleteIS3_EE
	.section	.text._ZN2v88internal9ParseInfo27SetScriptForToplevelCompileEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfo27SetScriptForToplevelCompileEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.type	_ZN2v88internal9ParseInfo27SetScriptForToplevelCompileEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, @function
_ZN2v88internal9ParseInfo27SetScriptForToplevelCompileEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE:
.LFB21800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdx, 80(%rdi)
	movq	(%rdx), %rax
	movslq	67(%rax), %rax
	movl	%eax, 52(%rdi)
	movl	8(%rdi), %eax
	movq	(%rdx), %rcx
	movl	%eax, %edx
	andl	$-5, %eax
	orl	$4, %edx
	testb	$1, 99(%rcx)
	cmovne	%edx, %eax
	movl	%eax, 8(%rdi)
	movq	(%r12), %rcx
	movl	%eax, %edx
	andl	$-65, %eax
	orl	$64, %edx
	testb	$32, 99(%rcx)
	cmovne	%edx, %eax
	movl	%eax, 8(%rdi)
	testb	$8, %ah
	jne	.L169
.L154:
	orb	$-127, %al
	movl	%eax, 8(%rbx)
	cmpl	$1, 41836(%r13)
	je	.L170
.L157:
	andb	$-3, %ah
	movl	%eax, 8(%rbx)
	movq	(%r12), %rax
	movq	71(%rax), %rax
	testb	$1, %al
	jne	.L171
.L147:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movq	(%r12), %rax
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	movl	8(%rbx), %eax
	je	.L157
	orb	$2, %ah
	movl	%eax, 8(%rbx)
	movq	(%r12), %rax
	movq	71(%rax), %rax
	testb	$1, %al
	je	.L147
	.p2align 4,,10
	.p2align 3
.L171:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L147
	movb	$4, 49(%rbx)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L169:
	movq	(%r12), %rax
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L153
.L168:
	movl	8(%rbx), %eax
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L153:
	movq	(%rbx), %r14
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	subq	%rax, %rdx
	cmpq	$55, %rdx
	jbe	.L173
	leaq	56(%rax), %rdx
	movq	%rdx, 16(%r14)
.L156:
	leaq	16(%rax), %rdx
	movq	%r14, (%rax)
	movl	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	%rdx, 40(%rax)
	movq	$0, 48(%rax)
	movq	%rax, 152(%rbx)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$56, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L156
.L172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21800:
	.size	_ZN2v88internal9ParseInfo27SetScriptForToplevelCompileEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, .-_ZN2v88internal9ParseInfo27SetScriptForToplevelCompileEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.section	.text._ZN2v88internal9ParseInfo12CreateScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsENS0_11NativesFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfo12CreateScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsENS0_11NativesFlagE
	.type	_ZN2v88internal9ParseInfo12CreateScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsENS0_11NativesFlagE, @function
_ZN2v88internal9ParseInfo12CreateScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsENS0_11NativesFlagE:
.LFB21795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$8, %rsp
	movl	52(%rdi), %edx
	cmpl	$-1, %edx
	je	.L185
	movq	%r14, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal7Isolate32NeedsSourcePositionsForProfilingEv@PLT
	testb	%al, %al
	jne	.L186
.L177:
	movq	0(%r13), %rax
	cmpl	$1, %r15d
	je	.L178
.L187:
	cmpl	$2, %r15d
	jne	.L180
	movabsq	$17179869184, %rdx
	movq	%rdx, 47(%rax)
	movq	0(%r13), %rax
.L180:
	movslq	99(%rax), %rcx
	sall	$2, %ebx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	andl	$-61, %ecx
	orl	%ebx, %ecx
	salq	$32, %rcx
	movq	%rcx, 95(%rax)
	call	_ZN2v88internal9ParseInfo27SetScriptForToplevelCompileEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movq	%r14, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory9NewScriptENS0_6HandleINS0_6StringEEENS0_14AllocationTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal7Isolate32NeedsSourcePositionsForProfilingEv@PLT
	testb	%al, %al
	je	.L177
.L186:
	movq	%r13, %rdi
	call	_ZN2v88internal6Script12InitLineEndsENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %rax
	cmpl	$1, %r15d
	jne	.L187
.L178:
	movabsq	$4294967296, %rdx
	movq	%rdx, 47(%rax)
	movq	0(%r13), %rax
	jmp	.L180
	.cfi_endproc
.LFE21795:
	.size	_ZN2v88internal9ParseInfo12CreateScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsENS0_11NativesFlagE, .-_ZN2v88internal9ParseInfo12CreateScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsENS0_11NativesFlagE
	.section	.text._ZN2v88internal9ParseInfo10set_scriptENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfo10set_scriptENS0_6HandleINS0_6ScriptEEE
	.type	_ZN2v88internal9ParseInfo10set_scriptENS0_6HandleINS0_6ScriptEEE, @function
_ZN2v88internal9ParseInfo10set_scriptENS0_6HandleINS0_6ScriptEEE:
.LFB21801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, 80(%rdi)
	movq	(%rsi), %rax
	movslq	67(%rax), %rax
	movl	%eax, 52(%rdi)
	movl	8(%rdi), %eax
	movq	(%rsi), %rcx
	movl	%eax, %edx
	andl	$-5, %eax
	orl	$4, %edx
	testb	$1, 99(%rcx)
	cmovne	%edx, %eax
	movl	%eax, 8(%rdi)
	movq	(%rsi), %rcx
	movl	%eax, %edx
	andl	$-65, %eax
	orl	$64, %edx
	testb	$32, 99(%rcx)
	cmovne	%edx, %eax
	movl	%eax, 8(%rdi)
	testb	$8, %ah
	jne	.L203
.L188:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L188
	movq	(%rbx), %r12
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$55, %rdx
	jbe	.L205
	leaq	56(%rax), %rdx
	movq	%rdx, 16(%r12)
.L197:
	leaq	16(%rax), %rdx
	movq	%r12, (%rax)
	movl	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	%rdx, 40(%rax)
	movq	$0, 48(%rax)
	movq	%rax, 152(%rbx)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L205:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L197
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21801:
	.size	_ZN2v88internal9ParseInfo10set_scriptENS0_6HandleINS0_6ScriptEEE, .-_ZN2v88internal9ParseInfo10set_scriptENS0_6HandleINS0_6ScriptEEE
	.section	.text._ZN2v88internal9ParseInfo13ParallelTasks7EnqueueEPS1_PKNS0_12AstRawStringEPNS0_15FunctionLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfo13ParallelTasks7EnqueueEPS1_PKNS0_12AstRawStringEPNS0_15FunctionLiteralE
	.type	_ZN2v88internal9ParseInfo13ParallelTasks7EnqueueEPS1_PKNS0_12AstRawStringEPNS0_15FunctionLiteralE, @function
_ZN2v88internal9ParseInfo13ParallelTasks7EnqueueEPS1_PKNS0_12AstRawStringEPNS0_15FunctionLiteralE:
.LFB21802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	_ZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralE@PLT
	testb	%al, %al
	jne	.L209
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movl	$24, %edi
	movq	%rdx, %r13
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	movq	%r12, 8(%rax)
	movq	%r13, 16(%rax)
	movq	%rdx, (%rax)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21802:
	.size	_ZN2v88internal9ParseInfo13ParallelTasks7EnqueueEPS1_PKNS0_12AstRawStringEPNS0_15FunctionLiteralE, .-_ZN2v88internal9ParseInfo13ParallelTasks7EnqueueEPS1_PKNS0_12AstRawStringEPNS0_15FunctionLiteralE
	.section	.text._ZN2v88internal9ParseInfo15SetFunctionInfoINS0_6HandleINS0_18SharedFunctionInfoEEEEEvT_,"axG",@progbits,_ZN2v88internal9ParseInfo15SetFunctionInfoINS0_6HandleINS0_18SharedFunctionInfoEEEEEvT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9ParseInfo15SetFunctionInfoINS0_6HandleINS0_18SharedFunctionInfoEEEEEvT_
	.type	_ZN2v88internal9ParseInfo15SetFunctionInfoINS0_6HandleINS0_18SharedFunctionInfoEEEEEvT_, @function
_ZN2v88internal9ParseInfo15SetFunctionInfoINS0_6HandleINS0_18SharedFunctionInfoEEEEEvT_:
.LFB24313:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	47(%rax), %eax
	testb	$64, %al
	movl	8(%rdi), %eax
	je	.L211
	orl	$8, %eax
.L212:
	movl	%eax, 8(%rdi)
	movq	(%rsi), %rdx
	movl	47(%rdx), %edx
	andl	$31, %edx
	movb	%dl, 48(%rdi)
	movq	(%rsi), %rdx
	movl	47(%rdx), %edx
	shrl	$7, %edx
	andl	$7, %edx
	movb	%dl, 49(%rdi)
	movq	(%rsi), %rdx
	movl	47(%rdx), %ecx
	movl	%eax, %edx
	andb	$127, %ah
	orb	$-128, %dh
	andl	$16777216, %ecx
	cmovne	%edx, %eax
	movl	%eax, 8(%rdi)
	movq	(%rsi), %rdx
	movl	47(%rdx), %ecx
	movl	%eax, %edx
	andl	$-2, %eax
	orl	$1, %edx
	andl	$268435456, %ecx
	cmovne	%edx, %eax
	movl	%eax, 8(%rdi)
	movq	(%rsi), %rdx
	movl	47(%rdx), %ecx
	andl	$536870912, %ecx
	jne	.L222
.L217:
	movl	8(%rdi), %eax
	andl	$-134217729, %eax
	movl	%eax, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	andl	$-9, %eax
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L222:
	movl	47(%rdx), %edx
	andb	$4, %dh
	jne	.L217
	orl	$134217728, %eax
	movl	%eax, 8(%rdi)
	ret
	.cfi_endproc
.LFE24313:
	.size	_ZN2v88internal9ParseInfo15SetFunctionInfoINS0_6HandleINS0_18SharedFunctionInfoEEEEEvT_, .-_ZN2v88internal9ParseInfo15SetFunctionInfoINS0_6HandleINS0_18SharedFunctionInfoEEEEEvT_
	.section	.text._ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB21783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	41136(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateEPNS0_19AccountingAllocatorE
	movq	(%r12), %rdx
	orl	$128, 8(%rbx)
	movl	47(%rdx), %eax
	testb	$64, %ah
	movl	8(%rbx), %eax
	je	.L224
	orb	$16, %ah
.L225:
	movl	%eax, 8(%rbx)
	leaq	-64(%rbp), %r15
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r15, %rdi
	movl	%eax, 56(%rbx)
	movq	(%r12), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%eax, 60(%rbx)
	movq	(%r12), %rax
	movl	51(%rax), %eax
	movl	%eax, 68(%rbx)
	call	_ZN2v88internal9ParseInfo15SetFunctionInfoINS0_6HandleINS0_18SharedFunctionInfoEEEEEvT_
	movq	(%r12), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L264
.L226:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L227
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L228:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9ParseInfo10set_scriptENS0_6HandleINS0_6ScriptEEE
	movq	(%r12), %rdx
	movabsq	$287762808832, %rcx
	movq	7(%rdx), %rax
	cmpq	%rcx, %rax
	je	.L234
	testb	$1, %al
	jne	.L265
.L233:
	movq	15(%rdx), %rax
	testb	$1, %al
	jne	.L266
.L237:
	movq	%rdx, %rax
	movq	%rdx, -88(%rbp)
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rdx
.L238:
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo17HasOuterScopeInfoEv@PLT
	testb	%al, %al
	je	.L235
	movq	-88(%rbp), %rdx
	movq	15(%rdx), %rax
	testb	$1, %al
	jne	.L267
.L239:
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
.L240:
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo14OuterScopeInfoEv@PLT
.L236:
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L235
	movabsq	$287762808832, %rcx
	movq	(%r12), %rdx
	movq	7(%rdx), %rax
	cmpq	%rcx, %rax
	je	.L244
	testb	$1, %al
	jne	.L268
.L243:
	movq	15(%rdx), %rax
	testb	$1, %al
	jne	.L269
.L245:
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
.L246:
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo14OuterScopeInfoEv@PLT
	movq	41112(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L247
.L275:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L248:
	movq	%rax, 88(%rbx)
.L235:
	cmpl	$1, 41836(%r13)
	je	.L270
.L250:
	movl	8(%rbx), %eax
	andb	$-3, %ah
.L252:
	movl	%eax, 8(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L271
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	jne	.L272
.L234:
	movq	23(%rdx), %rax
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	je	.L236
	cmpl	$1, 41836(%r13)
	jne	.L250
.L270:
	movq	(%r12), %rax
	movq	23(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$75, 11(%rdx)
	jne	.L251
	leaq	-72(%rbp), %rdi
	movq	%rax, -72(%rbp)
	call	_ZNK2v88internal16FeedbackMetadata18HasTypeProfileSlotEv@PLT
	testb	%al, %al
	je	.L250
.L253:
	movl	8(%rbx), %eax
	orb	$2, %ah
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L224:
	andb	$-17, %ah
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L227:
	movq	41088(%r13), %r14
	cmpq	%r14, 41096(%r13)
	je	.L273
.L229:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L264:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L226
	movq	23(%rsi), %rsi
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L268:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	jne	.L274
.L244:
	movq	41112(%r13), %rdi
	movq	23(%rdx), %rsi
	testq	%rdi, %rdi
	jne	.L275
.L247:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L276
.L249:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L266:
	movq	-1(%rax), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L237
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L272:
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L233
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L251:
	movq	(%r14), %rax
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L253
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L267:
	movq	-1(%rax), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L239
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L273:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L274:
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L243
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L269:
	movq	-1(%rax), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L245
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L276:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L249
.L271:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21783:
	.size	_ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.globl	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.set	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE,_ZN2v88internal9ParseInfoC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE, @function
_GLOBAL__sub_I__ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE:
.LFB28375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28375:
	.size	_GLOBAL__sub_I__ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE, .-_GLOBAL__sub_I__ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal9ParseInfoC2EPNS0_19AccountingAllocatorE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	-1
	.long	0
	.long	0
	.long	-1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
