	.file	"v8-profiler-agent-impl.cc"
	.text
	.section	.text._ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB2642:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2642:
	.size	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE
	.type	_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE, @function
_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE:
.LFB3651:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE3651:
	.size	_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE, .-_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE
	.section	.text._ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev, @function
_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev:
.LFB6187:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6187:
	.size	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev, .-_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD1Ev,_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler13CoverageRangeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev, @function
_ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev:
.LFB6215:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6215:
	.size	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev, .-_ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD1Ev,_ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB11435:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE11435:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN12v8_inspector8protocol8Debugger8LocationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8LocationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8LocationD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger8LocationD2Ev, @function
_ZN12v8_inspector8protocol8Debugger8LocationD2Ev:
.LFB5511:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L7
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	ret
	.cfi_endproc
.LFE5511:
	.size	_ZN12v8_inspector8protocol8Debugger8LocationD2Ev, .-_ZN12v8_inspector8protocol8Debugger8LocationD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger8LocationD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger8LocationD1Ev,_ZN12v8_inspector8protocol8Debugger8LocationD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev:
.LFB5119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L9
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5119:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev,_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler10TypeObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev, @function
_ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev:
.LFB6314:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L14
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	ret
	.cfi_endproc
.LFE6314:
	.size	_ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev, .-_ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler10TypeObjectD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler10TypeObjectD1Ev,_ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev, @function
_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev:
.LFB6189:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6189:
	.size	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev, .-_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler13CoverageRangeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev, @function
_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev:
.LFB6217:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6217:
	.size	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev, .-_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger8LocationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8LocationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev, @function
_ZN12v8_inspector8protocol8Debugger8LocationD0Ev:
.LFB5513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5513:
	.size	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev, .-_ZN12v8_inspector8protocol8Debugger8LocationD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler10TypeObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev, @function
_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev:
.LFB6316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6316:
	.size	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev, .-_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv:
.LFB5522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L24
	movq	(%rdi), %rax
	call	*24(%rax)
.L24:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L31:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5522:
	.size	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv:
.LFB5521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L32
	movq	(%rdi), %rax
	call	*24(%rax)
.L32:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L39:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5521:
	.size	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv:
.LFB6164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler7Profile7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L40
	movq	(%rdi), %rax
	call	*24(%rax)
.L40:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L47:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6164:
	.size	_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv:
.LFB6163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler7Profile7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L48
	movq	(%rdi), %rax
	call	*24(%rax)
.L48:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6163:
	.size	_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv:
.LFB6065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler11ProfileNode7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L56
	movq	(%rdi), %rax
	call	*24(%rax)
.L56:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L63:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6065:
	.size	_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv:
.LFB6064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler11ProfileNode7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L64
	movq	(%rdi), %rax
	call	*24(%rax)
.L64:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L71:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6064:
	.size	_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv:
.LFB6195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L72
	movq	(%rdi), %rax
	call	*24(%rax)
.L72:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L79:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6195:
	.size	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv:
.LFB6194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L80
	movq	(%rdi), %rax
	call	*24(%rax)
.L80:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6194:
	.size	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv:
.LFB5133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L88
	movq	(%rdi), %rax
	call	*24(%rax)
.L88:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L95:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5133:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv:
.LFB5132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L96
	movq	(%rdi), %rax
	call	*24(%rax)
.L96:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L103:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5132:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl6enableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl6enableEv
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl6enableEv, @function
_ZN12v8_inspector19V8ProfilerAgentImpl6enableEv:
.LFB7809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 48(%rsi)
	jne	.L107
	movb	$1, 48(%rsi)
	movq	32(%rsi), %r14
	leaq	-80(%rbp), %r13
	leaq	_ZN12v8_inspector18ProfilerAgentStateL15profilerEnabledE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L107
	call	_ZdlPv@PLT
.L107:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L110:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7809:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl6enableEv, .-_ZN12v8_inspector19V8ProfilerAgentImpl6enableEv
	.section	.rodata._ZN12v8_inspector19V8ProfilerAgentImpl19setSamplingIntervalEi.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Cannot change sampling interval when profiling."
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl19setSamplingIntervalEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl19setSamplingIntervalEi
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl19setSamplingIntervalEi, @function
_ZN12v8_inspector19V8ProfilerAgentImpl19setSamplingIntervalEi:
.LFB7811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 24(%rsi)
	je	.L112
	leaq	-80(%rbp), %r13
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L111
	call	_ZdlPv@PLT
.L111:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	32(%rsi), %r14
	leaq	-80(%rbp), %r13
	movl	%edx, %r15d
	leaq	_ZN12v8_inspector18ProfilerAgentStateL16samplingIntervalE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movl	%r15d, %edx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L115
	call	_ZdlPv@PLT
.L115:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L111
.L118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7811:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl19setSamplingIntervalEi, .-_ZN12v8_inspector19V8ProfilerAgentImpl19setSamplingIntervalEi
	.section	.rodata._ZN12v8_inspector19V8ProfilerAgentImpl20startPreciseCoverageEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES5_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Profiler is not enabled"
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl20startPreciseCoverageEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl20startPreciseCoverageEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES5_
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl20startPreciseCoverageEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES5_, @function
_ZN12v8_inspector19V8ProfilerAgentImpl20startPreciseCoverageEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES5_:
.LFB7819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 48(%rsi)
	je	.L138
	movzbl	(%rdx), %r15d
	movl	$0, -120(%rbp)
	movq	%rsi, %rbx
	testb	%r15b, %r15b
	je	.L123
	movzbl	1(%rdx), %eax
	movl	%eax, -120(%rbp)
	movl	%eax, %r15d
.L123:
	movzbl	(%rcx), %r14d
	movl	$0, -108(%rbp)
	testb	%r14b, %r14b
	je	.L124
	movzbl	1(%rcx), %eax
	movl	%eax, -108(%rbp)
	movl	%eax, %r14d
.L124:
	movq	32(%rbx), %r8
	leaq	-96(%rbp), %r13
	leaq	_ZN12v8_inspector18ProfilerAgentStateL22preciseCoverageStartedE(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-104(%rbp), %r8
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rax, %rdi
	je	.L125
	call	_ZdlPv@PLT
.L125:
	movq	32(%rbx), %r8
	leaq	_ZN12v8_inspector18ProfilerAgentStateL24preciseCoverageCallCountE(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movl	-120(%rbp), %edx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L126
	call	_ZdlPv@PLT
.L126:
	movq	32(%rbx), %r8
	leaq	_ZN12v8_inspector18ProfilerAgentStateL23preciseCoverageDetailedE(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	movl	-108(%rbp), %edx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L127
	call	_ZdlPv@PLT
.L127:
	cmpb	$1, %r14b
	sbbl	%esi, %esi
	andl	$-2, %esi
	testb	%r15b, %r15b
	jne	.L139
	addl	$4, %esi
.L129:
	movq	16(%rbx), %rdi
	call	_ZN2v85debug8Coverage10SelectModeEPNS_7IsolateENS0_12CoverageModeE@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L119:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L140
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	addl	$3, %esi
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L138:
	leaq	-96(%rbp), %r13
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L119
	call	_ZdlPv@PLT
	jmp	.L119
.L140:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7819:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl20startPreciseCoverageEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES5_, .-_ZN12v8_inspector19V8ProfilerAgentImpl20startPreciseCoverageEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES5_
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl19stopPreciseCoverageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl19stopPreciseCoverageEv
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl19stopPreciseCoverageEv, @function
_ZN12v8_inspector19V8ProfilerAgentImpl19stopPreciseCoverageEv:
.LFB7820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 48(%rsi)
	jne	.L142
	leaq	-96(%rbp), %r12
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L141
	call	_ZdlPv@PLT
.L141:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L150
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movq	32(%rsi), %r13
	leaq	-96(%rbp), %r12
	movq	%rsi, %rbx
	leaq	_ZN12v8_inspector18ProfilerAgentStateL22preciseCoverageStartedE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r13
	cmpq	%r13, %rdi
	je	.L145
	call	_ZdlPv@PLT
.L145:
	movq	32(%rbx), %r15
	leaq	_ZN12v8_inspector18ProfilerAgentStateL24preciseCoverageCallCountE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L146
	call	_ZdlPv@PLT
.L146:
	movq	32(%rbx), %r15
	leaq	_ZN12v8_inspector18ProfilerAgentStateL23preciseCoverageDetailedE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v85debug8Coverage10SelectModeEPNS_7IsolateENS0_12CoverageModeE@PLT
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L141
.L150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7820:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl19stopPreciseCoverageEv, .-_ZN12v8_inspector19V8ProfilerAgentImpl19stopPreciseCoverageEv
	.section	.text._ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv:
.LFB6225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler13CoverageRange7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L151
	movq	(%rdi), %rax
	call	*24(%rax)
.L151:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L158
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L158:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6225:
	.size	_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv:
.LFB6224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler13CoverageRange7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L159
	movq	(%rdi), %rax
	call	*24(%rax)
.L159:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L166
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L166:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6224:
	.size	_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv:
.LFB6259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L167
	movq	(%rdi), %rax
	call	*24(%rax)
.L167:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L174:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6259:
	.size	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv:
.LFB6258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L175
	movq	(%rdi), %rax
	call	*24(%rax)
.L175:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L182
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L182:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6258:
	.size	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv:
.LFB6293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L183
	movq	(%rdi), %rax
	call	*24(%rax)
.L183:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L190
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L190:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6293:
	.size	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv:
.LFB6292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L191
	movq	(%rdi), %rax
	call	*24(%rax)
.L191:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L198
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L198:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6292:
	.size	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl16startTypeProfileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl16startTypeProfileEv
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl16startTypeProfileEv, @function
_ZN12v8_inspector19V8ProfilerAgentImpl16startTypeProfileEv:
.LFB7861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	32(%rsi), %r14
	leaq	_ZN12v8_inspector18ProfilerAgentStateL18typeProfileStartedE(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movq	16(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v85debug11TypeProfile10SelectModeEPNS_7IsolateENS0_15TypeProfileModeE@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L203
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L203:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7861:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl16startTypeProfileEv, .-_ZN12v8_inspector19V8ProfilerAgentImpl16startTypeProfileEv
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl15stopTypeProfileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl15stopTypeProfileEv
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl15stopTypeProfileEv, @function
_ZN12v8_inspector19V8ProfilerAgentImpl15stopTypeProfileEv:
.LFB7862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	32(%rsi), %r14
	leaq	_ZN12v8_inspector18ProfilerAgentStateL18typeProfileStartedE(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v85debug11TypeProfile10SelectModeEPNS_7IsolateENS0_15TypeProfileModeE@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L208:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7862:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl15stopTypeProfileEv, .-_ZN12v8_inspector19V8ProfilerAgentImpl15stopTypeProfileEv
	.section	.text._ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv:
.LFB6320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler10TypeObject7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L209
	movq	(%rdi), %rax
	call	*24(%rax)
.L209:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L216:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6320:
	.size	_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv:
.LFB6319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler10TypeObject7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L217
	movq	(%rdi), %rax
	call	*24(%rax)
.L217:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L224
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L224:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6319:
	.size	_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv:
.LFB6350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L225
	movq	(%rdi), %rax
	call	*24(%rax)
.L225:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L232
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L232:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6350:
	.size	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv:
.LFB6349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L233
	movq	(%rdi), %rax
	call	*24(%rax)
.L233:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L240:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6349:
	.size	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv:
.LFB6383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L241
	movq	(%rdi), %rax
	call	*24(%rax)
.L241:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L248
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L248:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6383:
	.size	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv:
.LFB6382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L249
	movq	(%rdi), %rax
	call	*24(%rax)
.L249:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L256:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6382:
	.size	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev, @function
_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev:
.LFB6248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L258
	movq	8(%r15), %r13
	movq	(%r15), %r12
	cmpq	%r12, %r13
	je	.L259
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %r14
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L277:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L260:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L276
.L262:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L260
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L277
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L262
	.p2align 4,,10
	.p2align 3
.L276:
	movq	(%r15), %r12
.L259:
	testq	%r12, %r12
	je	.L263
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L263:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L258:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L257
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6248:
	.size	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev, .-_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD1Ev,_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev, @function
_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev:
.LFB6341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L278
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	je	.L280
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %r14
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L295:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r14, (%r12)
	cmpq	%rax, %rdi
	je	.L283
	call	_ZdlPv@PLT
.L283:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L281:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L294
.L284:
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L281
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L295
	addq	$8, %r15
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r15, %rbx
	jne	.L284
	.p2align 4,,10
	.p2align 3
.L294:
	movq	0(%r13), %r15
.L280:
	testq	%r15, %r15
	je	.L285
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L285:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6341:
	.size	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev, .-_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD1Ev,_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev, @function
_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev:
.LFB6343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L297
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	jne	.L302
	testq	%r15, %r15
	je	.L303
	.p2align 4,,10
	.p2align 3
.L317:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L303:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L297:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r14), %rdi
	movq	%rax, (%r14)
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L301
	call	_ZdlPv@PLT
.L301:
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L299:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L315
.L302:
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L299
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L316
	addq	$8, %r15
	movq	%r14, %rdi
	call	*%rax
	cmpq	%r15, %rbx
	jne	.L302
	.p2align 4,,10
	.p2align 3
.L315:
	movq	0(%r13), %r15
	testq	%r15, %r15
	jne	.L317
	jmp	.L303
	.cfi_endproc
.LFE6343:
	.size	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev, .-_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImplD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImplD2Ev
	.type	_ZN12v8_inspector19V8ProfilerAgentImplD2Ev, @function
_ZN12v8_inspector19V8ProfilerAgentImplD2Ev:
.LFB7801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector19V8ProfilerAgentImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L319
	call	_ZN2v811CpuProfiler7DisposeEv@PLT
.L319:
	movq	80(%rbx), %rdi
	leaq	96(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L320
	call	_ZdlPv@PLT
.L320:
	movq	64(%rbx), %r13
	movq	56(%rbx), %r12
	cmpq	%r12, %r13
	je	.L321
	.p2align 4,,10
	.p2align 3
.L326:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L322
	call	_ZdlPv@PLT
.L322:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L323
	call	_ZdlPv@PLT
	addq	$80, %r12
	cmpq	%r13, %r12
	jne	.L326
.L324:
	movq	56(%rbx), %r12
.L321:
	testq	%r12, %r12
	je	.L318
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	addq	$80, %r12
	cmpq	%r12, %r13
	jne	.L326
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L318:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7801:
	.size	_ZN12v8_inspector19V8ProfilerAgentImplD2Ev, .-_ZN12v8_inspector19V8ProfilerAgentImplD2Ev
	.globl	_ZN12v8_inspector19V8ProfilerAgentImplD1Ev
	.set	_ZN12v8_inspector19V8ProfilerAgentImplD1Ev,_ZN12v8_inspector19V8ProfilerAgentImplD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev:
.LFB5121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L333
	call	_ZdlPv@PLT
.L333:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L334
	call	_ZdlPv@PLT
.L334:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L335
	call	_ZdlPv@PLT
.L335:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5121:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev, @function
_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev:
.LFB6282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	88(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L338
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -72(%rbp)
	cmpq	%r12, %rax
	je	.L339
	.p2align 4,,10
	.p2align 3
.L349:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L340
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L341
	movq	48(%r15), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r14, %r14
	je	.L342
	movq	8(%r14), %rax
	movq	(%r14), %r8
	movq	%rax, -56(%rbp)
	cmpq	%r8, %rax
	jne	.L346
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L375:
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L344:
	addq	$8, %r8
	cmpq	%r8, -56(%rbp)
	je	.L374
.L346:
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L344
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	%r8, -64(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L375
	call	*%rax
	movq	-64(%rbp), %r8
	addq	$8, %r8
	cmpq	%r8, -56(%rbp)
	jne	.L346
	.p2align 4,,10
	.p2align 3
.L374:
	movq	(%r14), %r8
.L343:
	testq	%r8, %r8
	je	.L347
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L347:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L342:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L348
	call	_ZdlPv@PLT
.L348:
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L340:
	addq	$8, %r12
	cmpq	%r12, -72(%rbp)
	jne	.L349
.L376:
	movq	0(%r13), %r12
.L339:
	testq	%r12, %r12
	je	.L350
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L350:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L338:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L351
	call	_ZdlPv@PLT
.L351:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L337
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore_state
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -72(%rbp)
	jne	.L349
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L337:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6282:
	.size	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev, .-_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD1Ev,_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev, @function
_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev:
.LFB6250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	48(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L378
	movq	8(%r15), %rbx
	movq	(%r15), %r13
	cmpq	%r13, %rbx
	je	.L379
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %r14
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L397:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L380:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L396
.L382:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L380
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L397
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L382
	.p2align 4,,10
	.p2align 3
.L396:
	movq	(%r15), %r13
.L379:
	testq	%r13, %r13
	je	.L383
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L383:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L378:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L384
	call	_ZdlPv@PLT
.L384:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6250:
	.size	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev, .-_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev, @function
_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev:
.LFB6374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	88(%rdi), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L399
	movq	8(%rax), %rdx
	movq	(%rax), %rbx
	movq	%rdx, -72(%rbp)
	cmpq	%rbx, %rdx
	je	.L400
	movq	%rdi, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L410:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L401
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L402
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L403
	movq	8(%r13), %r14
	movq	0(%r13), %r15
	cmpq	%r15, %r14
	je	.L404
	movq	%r14, -64(%rbp)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L436:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r14), %rdi
	movq	%rax, (%r14)
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L407
	call	_ZdlPv@PLT
.L407:
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L405:
	addq	$8, %r15
	cmpq	%r15, -64(%rbp)
	je	.L435
.L408:
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L405
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L436
	movq	%r14, %rdi
	addq	$8, %r15
	call	*%rax
	cmpq	%r15, -64(%rbp)
	jne	.L408
	.p2align 4,,10
	.p2align 3
.L435:
	movq	0(%r13), %r15
.L404:
	testq	%r15, %r15
	je	.L409
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L409:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L403:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L401:
	addq	$8, %rbx
	cmpq	%rbx, -72(%rbp)
	jne	.L410
.L437:
	movq	-56(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	(%rax), %rbx
.L400:
	testq	%rbx, %rbx
	je	.L411
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L411:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L399:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L412
	call	_ZdlPv@PLT
.L412:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L413
	call	_ZdlPv@PLT
.L413:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	movq	%r12, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%rbx, -72(%rbp)
	jne	.L410
	jmp	.L437
	.cfi_endproc
.LFE6374:
	.size	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev, .-_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev, @function
_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev:
.LFB6372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L439
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -72(%rbp)
	cmpq	%r14, %rdx
	je	.L440
	movq	%rdi, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L450:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L441
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L442
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L443
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	jne	.L448
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L476:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L447
	movq	%r8, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
.L447:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L445:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L475
.L448:
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L445
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L476
	addq	$8, %r15
	movq	%r8, %rdi
	call	*%rax
	cmpq	%r15, %rbx
	jne	.L448
	.p2align 4,,10
	.p2align 3
.L475:
	movq	0(%r13), %r15
.L444:
	testq	%r15, %r15
	je	.L449
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L449:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L443:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L441:
	addq	$8, %r14
	cmpq	%r14, -72(%rbp)
	jne	.L450
	movq	-56(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	(%rax), %r14
.L440:
	testq	%r14, %r14
	je	.L451
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L451:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L439:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L452
	call	_ZdlPv@PLT
.L452:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L438
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L438:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6372:
	.size	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev, .-_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD1Ev,_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImplD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImplD0Ev
	.type	_ZN12v8_inspector19V8ProfilerAgentImplD0Ev, @function
_ZN12v8_inspector19V8ProfilerAgentImplD0Ev:
.LFB7803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector19V8ProfilerAgentImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L478
	call	_ZN2v811CpuProfiler7DisposeEv@PLT
.L478:
	movq	80(%r13), %rdi
	leaq	96(%r13), %rax
	cmpq	%rax, %rdi
	je	.L479
	call	_ZdlPv@PLT
.L479:
	movq	64(%r13), %rbx
	movq	56(%r13), %r12
	cmpq	%r12, %rbx
	je	.L480
	.p2align 4,,10
	.p2align 3
.L485:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L481
	call	_ZdlPv@PLT
.L481:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L482
	call	_ZdlPv@PLT
	addq	$80, %r12
	cmpq	%r12, %rbx
	jne	.L485
.L483:
	movq	56(%r13), %r12
.L480:
	testq	%r12, %r12
	je	.L486
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L486:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$128, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_restore_state
	addq	$80, %r12
	cmpq	%r12, %rbx
	jne	.L485
	jmp	.L483
	.cfi_endproc
.LFE7803:
	.size	_ZN12v8_inspector19V8ProfilerAgentImplD0Ev, .-_ZN12v8_inspector19V8ProfilerAgentImplD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev, @function
_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev:
.LFB6284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %r14
	movq	%rax, (%rdi)
	testq	%r14, %r14
	je	.L495
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -72(%rbp)
	cmpq	%r13, %rax
	je	.L496
	.p2align 4,,10
	.p2align 3
.L506:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L497
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L498
	movq	48(%r15), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rbx, %rbx
	je	.L499
	movq	8(%rbx), %rax
	movq	(%rbx), %r8
	movq	%rax, -56(%rbp)
	cmpq	%r8, %rax
	jne	.L503
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L532:
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L501:
	addq	$8, %r8
	cmpq	%r8, -56(%rbp)
	je	.L531
.L503:
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L501
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	%r8, -64(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L532
	call	*%rax
	movq	-64(%rbp), %r8
	addq	$8, %r8
	cmpq	%r8, -56(%rbp)
	jne	.L503
	.p2align 4,,10
	.p2align 3
.L531:
	movq	(%rbx), %r8
.L500:
	testq	%r8, %r8
	je	.L504
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L504:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L499:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L505
	call	_ZdlPv@PLT
.L505:
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L497:
	addq	$8, %r13
	cmpq	%r13, -72(%rbp)
	jne	.L506
.L533:
	movq	(%r14), %r13
.L496:
	testq	%r13, %r13
	je	.L507
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L507:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L495:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L508
	call	_ZdlPv@PLT
.L508:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L509
	call	_ZdlPv@PLT
.L509:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	movq	%r15, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -72(%rbp)
	jne	.L506
	jmp	.L533
	.cfi_endproc
.LFE6284:
	.size	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev, .-_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler7ProfileD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler7ProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev, @function
_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev:
.LFB6149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L535
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L536
	call	_ZdlPv@PLT
.L536:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L535:
	movq	32(%r13), %r12
	testq	%r12, %r12
	je	.L537
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L538
	call	_ZdlPv@PLT
.L538:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L537:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L539
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -72(%rbp)
	cmpq	%r14, %rax
	je	.L540
	.p2align 4,,10
	.p2align 3
.L557:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L541
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L542
	movq	88(%r12), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rbx, %rbx
	je	.L543
	movq	8(%rbx), %rax
	movq	(%rbx), %r8
	movq	%rax, -56(%rbp)
	cmpq	%r8, %rax
	jne	.L547
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L602:
	movl	$16, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L545:
	addq	$8, %r8
	cmpq	%r8, -56(%rbp)
	je	.L601
.L547:
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L545
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rdx
	movq	%r8, -64(%rbp)
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L602
	call	*%rax
	movq	-64(%rbp), %r8
	addq	$8, %r8
	cmpq	%r8, -56(%rbp)
	jne	.L547
	.p2align 4,,10
	.p2align 3
.L601:
	movq	(%rbx), %r8
.L544:
	testq	%r8, %r8
	je	.L548
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L548:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L543:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L549
	call	_ZdlPv@PLT
.L549:
	movq	32(%r12), %rbx
	testq	%rbx, %rbx
	je	.L550
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L551
	call	_ZdlPv@PLT
.L551:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L550:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	je	.L552
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L553
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L554
	call	_ZdlPv@PLT
.L554:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L555
	call	_ZdlPv@PLT
.L555:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L556
	call	_ZdlPv@PLT
.L556:
	movl	$136, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L552:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L541:
	addq	$8, %r14
	cmpq	%r14, -72(%rbp)
	jne	.L557
.L603:
	movq	(%r15), %r14
.L540:
	testq	%r14, %r14
	je	.L558
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L558:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L539:
	addq	$40, %rsp
	movq	%r13, %rdi
	movl	$48, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L542:
	.cfi_restore_state
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -72(%rbp)
	jne	.L557
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L553:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L552
	.cfi_endproc
.LFE6149:
	.size	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev, .-_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler7ProfileD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler7ProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler7ProfileD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler7ProfileD2Ev, @function
_ZN12v8_inspector8protocol8Profiler7ProfileD2Ev:
.LFB6147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L605
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L606
	call	_ZdlPv@PLT
.L606:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L605:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L607
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L608
	call	_ZdlPv@PLT
.L608:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L607:
	movq	8(%rbx), %r14
	testq	%r14, %r14
	je	.L604
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -64(%rbp)
	cmpq	%r13, %rax
	je	.L610
	.p2align 4,,10
	.p2align 3
.L627:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L611
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L612
	movq	88(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L613
	movq	8(%r15), %rax
	movq	(%r15), %rbx
	movq	%rax, -56(%rbp)
	cmpq	%rbx, %rax
	jne	.L617
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L669:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L615:
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	je	.L668
.L617:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L615
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L669
	call	*%rax
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L617
	.p2align 4,,10
	.p2align 3
.L668:
	movq	(%r15), %rbx
.L614:
	testq	%rbx, %rbx
	je	.L618
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L618:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L613:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L619
	call	_ZdlPv@PLT
.L619:
	movq	32(%r12), %rbx
	testq	%rbx, %rbx
	je	.L620
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L621
	call	_ZdlPv@PLT
.L621:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L620:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	je	.L622
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L623
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L624
	call	_ZdlPv@PLT
.L624:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L625
	call	_ZdlPv@PLT
.L625:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L626
	call	_ZdlPv@PLT
.L626:
	movl	$136, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L622:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L611:
	addq	$8, %r13
	cmpq	%r13, -64(%rbp)
	jne	.L627
	movq	(%r14), %r13
.L610:
	testq	%r13, %r13
	je	.L628
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L628:
	addq	$24, %rsp
	movq	%r14, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L604:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L622
	.cfi_endproc
.LFE6147:
	.size	_ZN12v8_inspector8protocol8Profiler7ProfileD2Ev, .-_ZN12v8_inspector8protocol8Profiler7ProfileD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler7ProfileD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler7ProfileD1Ev,_ZN12v8_inspector8protocol8Profiler7ProfileD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler11ProfileNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev, @function
_ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev:
.LFB6042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	88(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L671
	movq	8(%r15), %r13
	movq	(%r15), %r12
	cmpq	%r12, %r13
	je	.L672
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %r14
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L703:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L673:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L702
.L675:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L673
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L703
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L675
	.p2align 4,,10
	.p2align 3
.L702:
	movq	(%r15), %r12
.L672:
	testq	%r12, %r12
	je	.L676
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L676:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L671:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L677
	call	_ZdlPv@PLT
.L677:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L678
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L679
	call	_ZdlPv@PLT
.L679:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L678:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L670
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L681
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L682
	call	_ZdlPv@PLT
.L682:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L683
	call	_ZdlPv@PLT
.L683:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L684
	call	_ZdlPv@PLT
.L684:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L670:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L681:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE6042:
	.size	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev, .-_ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD1Ev,_ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler11ProfileNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev, @function
_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev:
.LFB6044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L705
	movq	8(%r15), %rbx
	movq	(%r15), %r13
	cmpq	%r13, %rbx
	je	.L706
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %r14
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L740:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L707:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L739
.L709:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L707
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L740
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L709
	.p2align 4,,10
	.p2align 3
.L739:
	movq	(%r15), %r13
.L706:
	testq	%r13, %r13
	je	.L710
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L710:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L705:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L711
	call	_ZdlPv@PLT
.L711:
	movq	32(%r12), %r13
	testq	%r13, %r13
	je	.L712
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L713
	call	_ZdlPv@PLT
.L713:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L712:
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L714
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L715
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L716
	call	_ZdlPv@PLT
.L716:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L717
	call	_ZdlPv@PLT
.L717:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L718
	call	_ZdlPv@PLT
.L718:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L714:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L714
	.cfi_endproc
.LFE6044:
	.size	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev, .-_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.type	_ZN12v8_inspector19V8ProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE, @function
_ZN12v8_inspector19V8ProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE:
.LFB7798:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector19V8ProfilerAgentImplE(%rip), %rax
	movq	%rsi, 8(%rdi)
	pxor	%xmm0, %xmm0
	movq	%rax, (%rdi)
	movq	24(%rsi), %rax
	movq	8(%rax), %rax
	movq	%rdx, 40(%rdi)
	xorl	%edx, %edx
	movq	$0, 24(%rdi)
	movq	%rax, 16(%rdi)
	xorl	%eax, %eax
	movw	%ax, 48(%rdi)
	leaq	96(%rdi), %rax
	movq	%rcx, 32(%rdi)
	movq	$0, 72(%rdi)
	movq	%rax, 80(%rdi)
	movq	$0, 88(%rdi)
	movw	%dx, 96(%rdi)
	movq	$0, 112(%rdi)
	movl	$0, 120(%rdi)
	movups	%xmm0, 56(%rdi)
	ret
	.cfi_endproc
.LFE7798:
	.size	_ZN12v8_inspector19V8ProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE, .-_ZN12v8_inspector19V8ProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.globl	_ZN12v8_inspector19V8ProfilerAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.set	_ZN12v8_inspector19V8ProfilerAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE,_ZN12v8_inspector19V8ProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl7restoreEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl7restoreEv
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl7restoreEv, @function
_ZN12v8_inspector19V8ProfilerAgentImpl7restoreEv:
.LFB7812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN12v8_inspector18ProfilerAgentStateL15profilerEnabledE(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	32(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %r14
	movl	%eax, %ebx
	cmpq	%r14, %rdi
	je	.L743
	call	_ZdlPv@PLT
.L743:
	testb	%bl, %bl
	jne	.L762
.L742:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L763
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L762:
	.cfi_restore_state
	movb	$1, 48(%r12)
	movq	32(%r12), %r15
	leaq	_ZN12v8_inspector18ProfilerAgentStateL22userInitiatedProfilingE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %ebx
	cmpq	%r14, %rdi
	je	.L746
	call	_ZdlPv@PLT
.L746:
	testb	%bl, %bl
	je	.L747
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	*48(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L747
	call	_ZdlPv@PLT
.L747:
	movq	32(%r12), %r15
	leaq	_ZN12v8_inspector18ProfilerAgentStateL22preciseCoverageStartedE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %ebx
	cmpq	%r14, %rdi
	je	.L749
	call	_ZdlPv@PLT
.L749:
	testb	%bl, %bl
	je	.L742
	movq	32(%r12), %r15
	leaq	_ZN12v8_inspector18ProfilerAgentStateL24preciseCoverageCallCountE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %ebx
	cmpq	%r14, %rdi
	je	.L751
	call	_ZdlPv@PLT
.L751:
	movq	32(%r12), %r15
	leaq	_ZN12v8_inspector18ProfilerAgentStateL23preciseCoverageDetailedE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %r15d
	cmpq	%r14, %rdi
	je	.L752
	call	_ZdlPv@PLT
.L752:
	movq	(%r12), %rax
	movq	%r13, %rdi
	movb	$1, -114(%rbp)
	leaq	-114(%rbp), %rcx
	movb	%r15b, -113(%rbp)
	leaq	-116(%rbp), %rdx
	movq	%r12, %rsi
	movb	$1, -116(%rbp)
	movb	%bl, -115(%rbp)
	call	*56(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L742
	call	_ZdlPv@PLT
	jmp	.L742
.L763:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7812:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl7restoreEv, .-_ZN12v8_inspector19V8ProfilerAgentImpl7restoreEv
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl13nextProfileIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl13nextProfileIdEv
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl13nextProfileIdEv, @function
_ZN12v8_inspector19V8ProfilerAgentImpl13nextProfileIdEv:
.LFB7870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	lock xaddl	%esi, _ZN12v8_inspector12_GLOBAL__N_115s_lastProfileIdE(%rip)
	addl	$1, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L767
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L767:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7870:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl13nextProfileIdEv, .-_ZN12v8_inspector19V8ProfilerAgentImpl13nextProfileIdEv
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl14startProfilingERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl14startProfilingERKNS_8String16E
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl14startProfilingERKNS_8String16E, @function
_ZN12v8_inspector19V8ProfilerAgentImpl14startProfilingERKNS_8String16E:
.LFB7871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	16(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.L769
.L775:
	movq	24(%rbx), %r14
.L770:
	addl	$1, %eax
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	movl	%eax, 120(%rbx)
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v811CpuProfiler14StartProfilingENS_5LocalINS_6StringEEEb@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L776
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L769:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	leaq	-96(%rbp), %r14
	call	_ZN2v811CpuProfiler3NewEPNS_7IsolateE@PLT
	movq	32(%rbx), %r15
	leaq	_ZN12v8_inspector18ProfilerAgentStateL16samplingIntervalE(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, 24(%rbx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15integerPropertyERKNS_8String16Ei@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %r15d
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L771
	call	_ZdlPv@PLT
.L771:
	movq	24(%rbx), %r14
	testl	%r15d, %r15d
	jne	.L772
	movl	120(%rbx), %eax
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L772:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v811CpuProfiler19SetSamplingIntervalEi@PLT
	movl	120(%rbx), %eax
	jmp	.L775
.L776:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7871:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl14startProfilingERKNS_8String16E, .-_ZN12v8_inspector19V8ProfilerAgentImpl14startProfilingERKNS_8String16E
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl5startEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl5startEv
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl5startEv, @function
_ZN12v8_inspector19V8ProfilerAgentImpl5startEv:
.LFB7813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 49(%rsi)
	jne	.L789
	cmpb	$0, 48(%rsi)
	movq	%rsi, %rbx
	je	.L800
	movb	$1, 49(%rsi)
	movl	$1, %esi
	lock xaddl	%esi, _ZN12v8_inspector12_GLOBAL__N_115s_lastProfileIdE(%rip)
	leaq	-96(%rbp), %r14
	addl	$1, %esi
	leaq	-80(%rbp), %r13
	movq	%r14, %rdi
	leaq	80(%rbx), %r15
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	-96(%rbp), %rdx
	movq	80(%rbx), %rdi
	movq	-88(%rbp), %rax
	cmpq	%r13, %rdx
	je	.L801
	leaq	96(%rbx), %rsi
	movq	-80(%rbp), %rcx
	cmpq	%rsi, %rdi
	je	.L802
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	96(%rbx), %rsi
	movq	%rdx, 80(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 88(%rbx)
	testq	%rdi, %rdi
	je	.L787
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L785:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 112(%rbx)
	cmpq	%r13, %rdi
	je	.L788
	call	_ZdlPv@PLT
.L788:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector19V8ProfilerAgentImpl14startProfilingERKNS_8String16E
	movq	32(%rbx), %r15
	leaq	_ZN12v8_inspector18ProfilerAgentStateL22userInitiatedProfilingE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L789
	call	_ZdlPv@PLT
.L789:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L777:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L803
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L800:
	.cfi_restore_state
	leaq	-96(%rbp), %r13
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L777
	call	_ZdlPv@PLT
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L802:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, 80(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 88(%rbx)
.L787:
	movq	%r13, -96(%rbp)
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L801:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L783
	cmpq	$1, %rax
	je	.L804
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L783
	movq	%r13, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	80(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L783:
	xorl	%ecx, %ecx
	movq	%rax, 88(%rbx)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L804:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	80(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L783
.L803:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7813:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl5startEv, .-_ZN12v8_inspector19V8ProfilerAgentImpl5startEv
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movabsq	$1152921504606846975, %rdi
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L832
	movq	%rsi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L819
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L833
.L807:
	movq	%r14, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %r14
	movq	%r14, -64(%rbp)
	leaq	8(%rax), %r14
.L818:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rcx
	movq	$0, (%rdx)
	movq	%rax, (%rcx,%rsi)
	cmpq	%r12, %r13
	je	.L809
	movq	%rcx, %r14
	movq	%r12, %r15
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L835:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L810:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	je	.L834
.L812:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L810
	movq	(%rdi), %rsi
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L835
	call	*%rsi
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	jne	.L812
	.p2align 4,,10
	.p2align 3
.L834:
	movq	-56(%rbp), %rcx
	movq	%r13, %rax
	subq	%r12, %rax
	leaq	8(%rcx,%rax), %r14
.L809:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L813
	movq	%rax, %r15
	subq	%r13, %r15
	leaq	-8(%r15), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L821
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L815:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L815
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%r14,%r8), %rdx
	leaq	0(%r13,%r8), %rbx
	cmpq	%rax, %rsi
	je	.L816
.L814:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L816:
	leaq	8(%r14,%rdi), %r14
.L813:
	testq	%r12, %r12
	je	.L817
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L817:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r14, %xmm2
	movq	-64(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L833:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L808
	movq	$0, -64(%rbp)
	movl	$8, %r14d
	movq	$0, -56(%rbp)
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L819:
	movl	$8, %r14d
	jmp	.L807
.L821:
	movq	%r14, %rdx
	jmp	.L814
.L808:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	leaq	0(,%rdi,8), %r14
	jmp	.L807
.L832:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11059:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB11085:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L850
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L846
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L851
.L838:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L845:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L852
	testq	%r13, %r13
	jg	.L841
	testq	%r9, %r9
	jne	.L844
.L842:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L852:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L841
.L844:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L851:
	testq	%rsi, %rsi
	jne	.L839
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L841:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L842
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L846:
	movl	$4, %r14d
	jmp	.L838
.L850:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L839:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L838
	.cfi_endproc
.LFE11085:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rdi, -112(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -104(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rcx, -72(%rbp)
	cmpq	%rdx, %rax
	je	.L913
	movq	%rsi, %r14
	movq	%rsi, %r13
	subq	-72(%rbp), %r14
	testq	%rax, %rax
	je	.L881
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L914
.L855:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -88(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -96(%rbp)
	leaq	8(%rax), %rbx
.L880:
	movq	(%r12), %rax
	movq	$0, (%r12)
	movq	-88(%rbp), %r12
	movq	%rax, (%r12,%r14)
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L857
	movq	%r13, -56(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L874:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rax, (%r12)
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L858
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L859
	movq	88(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L860
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L861
	movq	%rbx, -80(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L916:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L862:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L915
.L864:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L862
	movq	(%rdi), %rdx
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L916
	call	*%rdx
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L864
	.p2align 4,,10
	.p2align 3
.L915:
	movq	-80(%rbp), %rbx
	movq	(%r14), %r15
.L861:
	testq	%r15, %r15
	je	.L865
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L865:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L860:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L866
	call	_ZdlPv@PLT
.L866:
	movq	32(%r13), %r14
	testq	%r14, %r14
	je	.L867
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L868
	call	_ZdlPv@PLT
.L868:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L867:
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L869
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L870
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	leaq	104(%r14), %rax
	cmpq	%rax, %rdi
	je	.L871
	call	_ZdlPv@PLT
.L871:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L872
	call	_ZdlPv@PLT
.L872:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L873
	call	_ZdlPv@PLT
.L873:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L869:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L858:
	addq	$8, %rbx
	addq	$8, %r12
	cmpq	%rbx, -56(%rbp)
	jne	.L874
.L917:
	movq	-56(%rbp), %r13
	movq	-88(%rbp), %rcx
	movq	%r13, %rax
	subq	-72(%rbp), %rax
	leaq	8(%rcx,%rax), %rbx
.L857:
	movq	-104(%rbp), %rax
	cmpq	%rax, %r13
	je	.L875
	subq	%r13, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L883
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L877:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L877
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	%r13, %rdi
	movq	%rdi, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L878
.L876:
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L878:
	leaq	8(%rbx,%rsi), %rbx
.L875:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L879
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L879:
	movq	-88(%rbp), %xmm0
	movq	-112(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-96(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L859:
	.cfi_restore_state
	movq	%r13, %rdi
	addq	$8, %rbx
	addq	$8, %r12
	call	*%rax
	cmpq	%rbx, -56(%rbp)
	jne	.L874
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L870:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L914:
	testq	%rcx, %rcx
	jne	.L856
	movq	$0, -96(%rbp)
	movl	$8, %ebx
	movq	$0, -88(%rbp)
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L881:
	movl	$8, %ebx
	jmp	.L855
.L883:
	movq	%rbx, %rdx
	jmp	.L876
.L856:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L855
.L913:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11093:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movabsq	$1152921504606846975, %rdi
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L945
	movq	%rsi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L932
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L946
.L920:
	movq	%r14, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %r14
	movq	%r14, -64(%rbp)
	leaq	8(%rax), %r14
.L931:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rcx
	movq	$0, (%rdx)
	movq	%rax, (%rcx,%rsi)
	cmpq	%r12, %r13
	je	.L922
	movq	%rcx, %r14
	movq	%r12, %r15
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L948:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L923:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	je	.L947
.L925:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L923
	movq	(%rdi), %rsi
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L948
	call	*%rsi
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	jne	.L925
	.p2align 4,,10
	.p2align 3
.L947:
	movq	-56(%rbp), %rcx
	movq	%r13, %rax
	subq	%r12, %rax
	leaq	8(%rcx,%rax), %r14
.L922:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L926
	movq	%rax, %r15
	subq	%r13, %r15
	leaq	-8(%r15), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L934
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L928:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L928
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%r14,%r8), %rdx
	leaq	0(%r13,%r8), %rbx
	cmpq	%rax, %rsi
	je	.L929
.L927:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L929:
	leaq	8(%r14,%rdi), %r14
.L926:
	testq	%r12, %r12
	je	.L930
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L930:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r14, %xmm2
	movq	-64(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L946:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L921
	movq	$0, -64(%rbp)
	movl	$8, %r14d
	movq	$0, -56(%rbp)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L932:
	movl	$8, %r14d
	jmp	.L920
.L934:
	movq	%r14, %rdx
	jmp	.L927
.L921:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	leaq	0(,%rdi,8), %r14
	jmp	.L920
.L945:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11226:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rdi, -112(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -104(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rcx, -72(%rbp)
	cmpq	%rdx, %rax
	je	.L993
	movq	%rsi, %r14
	movq	%rsi, %r13
	subq	-72(%rbp), %r14
	testq	%rax, %rax
	je	.L970
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L994
.L951:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -88(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -96(%rbp)
	leaq	8(%rax), %rbx
.L969:
	movq	(%r12), %rax
	movq	$0, (%r12)
	movq	-88(%rbp), %r12
	movq	%rax, (%r12,%r14)
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L953
	movq	%r13, -56(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L963:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rax, (%r12)
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L954
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L955
	movq	48(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L956
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L957
	movq	%rbx, -80(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L996:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L958:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L995
.L960:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L958
	movq	(%rdi), %rdx
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L996
	call	*%rdx
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L960
	.p2align 4,,10
	.p2align 3
.L995:
	movq	-80(%rbp), %rbx
	movq	(%r14), %r15
.L957:
	testq	%r15, %r15
	je	.L961
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L961:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L956:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L962
	call	_ZdlPv@PLT
.L962:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L954:
	addq	$8, %rbx
	addq	$8, %r12
	cmpq	%rbx, -56(%rbp)
	jne	.L963
.L997:
	movq	-56(%rbp), %r13
	movq	-88(%rbp), %rcx
	movq	%r13, %rax
	subq	-72(%rbp), %rax
	leaq	8(%rcx,%rax), %rbx
.L953:
	movq	-104(%rbp), %rax
	cmpq	%rax, %r13
	je	.L964
	subq	%r13, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L972
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L966:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L966
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	%r13, %rdi
	movq	%rdi, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L967
.L965:
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L967:
	leaq	8(%rbx,%rsi), %rbx
.L964:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L968
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L968:
	movq	-88(%rbp), %xmm0
	movq	-112(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-96(%rbp), %rsi
	punpcklqdq	%xmm2, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L955:
	.cfi_restore_state
	movq	%r13, %rdi
	addq	$8, %rbx
	addq	$8, %r12
	call	*%rax
	cmpq	%rbx, -56(%rbp)
	jne	.L963
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L994:
	testq	%rcx, %rcx
	jne	.L952
	movq	$0, -96(%rbp)
	movl	$8, %ebx
	movq	$0, -88(%rbp)
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L970:
	movl	$8, %ebx
	jmp	.L951
.L972:
	movq	%rbx, %rdx
	jmp	.L965
.L952:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L951
.L993:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11237:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler14ScriptCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler14ScriptCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler14ScriptCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler14ScriptCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler14ScriptCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rsi, -64(%rbp)
	movq	%rdi, -144(%rbp)
	movq	%rax, -136(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rsi, -88(%rbp)
	movq	%rcx, -96(%rbp)
	cmpq	%rdx, %rax
	je	.L1060
	movq	%rsi, %r13
	subq	-96(%rbp), %r13
	testq	%rax, %rax
	je	.L1027
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1061
.L1000:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -120(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -128(%rbp)
	leaq	8(%rax), %rbx
.L1026:
	movq	(%r12), %rax
	movq	-120(%rbp), %rdx
	movq	$0, (%r12)
	movq	%rax, (%rdx,%r13)
	movq	-96(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L1002
	movq	%rdx, -56(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	(%rbx), %rax
	movq	-56(%rbp), %rcx
	movq	$0, (%rbx)
	movq	%rax, (%rcx)
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1003
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1004
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE(%rip), %rax
	movq	%rax, (%r12)
	movq	88(%r12), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L1005
	movq	8(%rax), %rsi
	movq	(%rax), %r13
	movq	%rsi, -72(%rbp)
	cmpq	%r13, %rsi
	je	.L1006
	movq	%rbx, -104(%rbp)
	movq	%r12, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L1007
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1008
	movq	48(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L1009
	movq	8(%r15), %r14
	movq	(%r15), %rbx
	cmpq	%rbx, %r14
	jne	.L1013
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1063:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1011:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L1062
.L1013:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1011
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1063
	call	*%rax
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L1013
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	(%r15), %rbx
.L1010:
	testq	%rbx, %rbx
	je	.L1014
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1014:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1009:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1015
	call	_ZdlPv@PLT
.L1015:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1007:
	addq	$8, %r13
	cmpq	%r13, -72(%rbp)
	jne	.L1016
	movq	-80(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	-112(%rbp), %r12
	movq	(%rax), %r13
.L1006:
	testq	%r13, %r13
	je	.L1017
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1017:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1005:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1018
	call	_ZdlPv@PLT
.L1018:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1019
	call	_ZdlPv@PLT
.L1019:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1003:
	addq	$8, -56(%rbp)
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L1020
	movq	-120(%rbp), %rcx
	movq	-64(%rbp), %rax
	subq	-96(%rbp), %rax
	leaq	8(%rcx,%rax), %rbx
.L1002:
	movq	-64(%rbp), %rdi
	movq	-136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1021
	subq	%rdi, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L1029
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1023:
	movdqu	(%rdi,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1023
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	-64(%rbp), %rdi
	movq	%rdi, -88(%rbp)
	cmpq	%rax, %rcx
	je	.L1024
.L1022:
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1024:
	leaq	8(%rbx,%rsi), %rbx
.L1021:
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L1025
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1025:
	movq	-120(%rbp), %xmm0
	movq	-128(%rbp), %rcx
	movq	%rbx, %xmm2
	movq	-144(%rbp), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1008:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1061:
	testq	%rcx, %rcx
	jne	.L1001
	movq	$0, -128(%rbp)
	movl	$8, %ebx
	movq	$0, -120(%rbp)
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1027:
	movl	$8, %ebx
	jmp	.L1000
.L1029:
	movq	%rbx, %rdx
	jmp	.L1022
.L1001:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L1000
.L1060:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11248:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler14ScriptCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler14ScriptCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$1152921504606846975, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -88(%rbp)
	movq	%rax, -80(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L1092
	movq	%rsi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L1079
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1093
.L1066:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, -64(%rbp)
	addq	%rax, %r14
	movq	%r14, -72(%rbp)
	leaq	8(%rax), %r14
.L1078:
	movq	(%r15), %rax
	movq	-64(%rbp), %rcx
	movq	$0, (%r15)
	movq	%rax, (%rcx,%rsi)
	cmpq	%r12, %r13
	je	.L1068
	movq	%rcx, %r14
	movq	%r12, %r15
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	8(%r8), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	leaq	24(%r8), %rsi
	movq	%rax, (%r8)
	cmpq	%rsi, %rdi
	je	.L1071
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1071:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1069:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	je	.L1094
.L1072:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L1069
	movq	(%r8), %rsi
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L1095
	addq	$8, %r15
	movq	%r8, %rdi
	addq	$8, %r14
	call	*%rsi
	cmpq	%r15, %r13
	jne	.L1072
	.p2align 4,,10
	.p2align 3
.L1094:
	movq	-64(%rbp), %rcx
	movq	%r13, %rax
	subq	%r12, %rax
	leaq	8(%rcx,%rax), %r14
.L1068:
	movq	-80(%rbp), %rax
	cmpq	%rax, %r13
	je	.L1073
	subq	%r13, %rax
	leaq	-8(%rax), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L1081
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1075:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1075
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rbx
	leaq	(%r14,%rbx), %rdx
	addq	%r13, %rbx
	cmpq	%rax, %rsi
	je	.L1076
.L1074:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L1076:
	leaq	8(%r14,%rdi), %r14
.L1073:
	testq	%r12, %r12
	je	.L1077
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1077:
	movq	-64(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movq	%r14, %xmm2
	movq	-72(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1093:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L1067
	movq	$0, -72(%rbp)
	movl	$8, %r14d
	movq	$0, -64(%rbp)
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1079:
	movl	$8, %r14d
	jmp	.L1066
.L1081:
	movq	%r14, %rdx
	jmp	.L1074
.L1067:
	cmpq	%rdx, %rdi
	cmovbe	%rdi, %rdx
	leaq	0(,%rdx,8), %r14
	jmp	.L1066
.L1092:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11337:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movabsq	$1152921504606846975, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	movq	8(%rdi), %rax
	movq	(%rdi), %rsi
	movq	%rdi, -120(%rbp)
	movq	%rax, -112(%rbp)
	subq	%rsi, %rax
	movq	%rsi, -72(%rbp)
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L1140
	movq	%r12, %r14
	subq	-72(%rbp), %r14
	testq	%rax, %rax
	je	.L1117
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1141
.L1098:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -96(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -104(%rbp)
	leaq	8(%rax), %rbx
.L1116:
	movq	0(%r13), %rax
	movq	-96(%rbp), %rcx
	movq	$0, 0(%r13)
	movq	%rax, (%rcx,%r14)
	movq	-72(%rbp), %rax
	cmpq	%rax, %r12
	je	.L1100
	movq	%r12, -56(%rbp)
	movq	%rcx, %r14
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rax, (%r14)
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1101
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1102
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1103
	movq	8(%r12), %rax
	movq	(%r12), %r13
	cmpq	%r13, %rax
	je	.L1104
	movq	%rbx, -80(%rbp)
	movq	%rax, %rbx
	movq	%r12, -88(%rbp)
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1143:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1107
	call	_ZdlPv@PLT
.L1107:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1105:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L1142
.L1108:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L1105
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1143
	addq	$8, %r13
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r13, %rbx
	jne	.L1108
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	-88(%rbp), %r12
	movq	-80(%rbp), %rbx
	movq	(%r12), %r13
.L1104:
	testq	%r13, %r13
	je	.L1109
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1109:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1103:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1101:
	addq	$8, %rbx
	addq	$8, %r14
	cmpq	%rbx, -56(%rbp)
	jne	.L1110
.L1144:
	movq	-56(%rbp), %r12
	movq	-96(%rbp), %rcx
	movq	%r12, %rax
	subq	-72(%rbp), %rax
	leaq	8(%rcx,%rax), %rbx
.L1100:
	movq	-112(%rbp), %rax
	cmpq	%rax, %r12
	je	.L1111
	subq	%r12, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L1119
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1113:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1113
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	%r12, %rdi
	movq	%rdi, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L1114
.L1112:
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1114:
	leaq	8(%rbx,%rsi), %rbx
.L1111:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L1115
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1115:
	movq	-96(%rbp), %xmm0
	movq	-120(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-104(%rbp), %rdx
	punpcklqdq	%xmm2, %xmm0
	movq	%rdx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1102:
	.cfi_restore_state
	movq	%r15, %rdi
	addq	$8, %rbx
	addq	$8, %r14
	call	*%rax
	cmpq	%rbx, -56(%rbp)
	jne	.L1110
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1141:
	testq	%rcx, %rcx
	jne	.L1099
	movq	$0, -104(%rbp)
	movl	$8, %ebx
	movq	$0, -96(%rbp)
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1117:
	movl	$8, %ebx
	jmp	.L1098
.L1119:
	movq	%rbx, %rdx
	jmp	.L1112
.L1099:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L1098
.L1140:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11346:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rsi, -64(%rbp)
	movq	%rdi, -152(%rbp)
	movq	%rax, -144(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rsi, -96(%rbp)
	movq	%rcx, -104(%rbp)
	cmpq	%rdx, %rax
	je	.L1207
	movq	%rsi, %r13
	subq	-104(%rbp), %r13
	testq	%rax, %rax
	je	.L1174
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1208
.L1147:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -128(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -136(%rbp)
	leaq	8(%rax), %rbx
.L1173:
	movq	(%r12), %rax
	movq	-128(%rbp), %rdx
	movq	$0, (%r12)
	movq	%rax, (%rdx,%r13)
	movq	-104(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L1149
	movq	%rdx, -56(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1167:
	movq	(%rbx), %rax
	movq	-56(%rbp), %rcx
	movq	$0, (%rbx)
	movq	%rax, (%rcx)
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1150
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1151
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE(%rip), %rax
	movq	%rax, (%r12)
	movq	88(%r12), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L1152
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L1153
	movq	%r12, -120(%rbp)
	movq	%rbx, -112(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1154
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1155
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1156
	movq	8(%r13), %r14
	movq	0(%r13), %r15
	cmpq	%r15, %r14
	je	.L1157
	movq	%r12, -80(%rbp)
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1210:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1160
	call	_ZdlPv@PLT
.L1160:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1158:
	addq	$8, %r15
	cmpq	%r15, %r14
	je	.L1209
.L1161:
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L1158
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1210
	addq	$8, %r15
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r15, %r14
	jne	.L1161
	.p2align 4,,10
	.p2align 3
.L1209:
	movq	-80(%rbp), %r12
	movq	0(%r13), %r15
.L1157:
	testq	%r15, %r15
	je	.L1162
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1162:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1156:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1154:
	addq	$8, %rbx
	cmpq	%rbx, -72(%rbp)
	jne	.L1163
	movq	-88(%rbp), %rax
	movq	-112(%rbp), %rbx
	movq	-120(%rbp), %r12
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1153:
	testq	%rdi, %rdi
	je	.L1164
	call	_ZdlPv@PLT
.L1164:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1152:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1165
	call	_ZdlPv@PLT
.L1165:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1166
	call	_ZdlPv@PLT
.L1166:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1150:
	addq	$8, -56(%rbp)
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L1167
	movq	-128(%rbp), %rcx
	movq	-64(%rbp), %rax
	subq	-104(%rbp), %rax
	leaq	8(%rcx,%rax), %rbx
.L1149:
	movq	-64(%rbp), %rdi
	movq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1168
	subq	%rdi, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L1176
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1170:
	movdqu	(%rdi,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1170
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	-64(%rbp), %rdi
	movq	%rdi, -96(%rbp)
	cmpq	%rax, %rcx
	je	.L1171
.L1169:
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1171:
	leaq	8(%rbx,%rsi), %rbx
.L1168:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L1172
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1172:
	movq	-128(%rbp), %xmm0
	movq	-152(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-136(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1155:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1151:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1208:
	testq	%rcx, %rcx
	jne	.L1148
	movq	$0, -136(%rbp)
	movl	$8, %ebx
	movq	$0, -128(%rbp)
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1174:
	movl	$8, %ebx
	jmp	.L1147
.L1176:
	movq	%rbx, %rdx
	jmp	.L1169
.L1148:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L1147
.L1207:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11357:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"basic_string::_M_create"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB11815:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L1230
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L1221
	movq	16(%rdi), %rax
.L1213:
	cmpq	%r15, %rax
	jb	.L1233
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L1217
.L1236:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L1234
	testq	%rdx, %rdx
	je	.L1217
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L1217:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1233:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L1235
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L1216
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L1216:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L1220
	call	_ZdlPv@PLT
.L1220:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L1217
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1230:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1221:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1234:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L1217
.L1235:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11815:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.text._ZN12v8_inspector12_GLOBAL__N_120currentDebugLocationEPNS_15V8InspectorImplE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_120currentDebugLocationEPNS_15V8InspectorImplE, @function
_ZN12v8_inspector12_GLOBAL__N_120currentDebugLocationEPNS_15V8InspectorImplE:
.LFB7781:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-136(%rbp), %rdi
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	24(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector10V8Debugger17captureStackTraceEb@PLT
	movl	$64, %edi
	call	_Znwm@PLT
	movq	-136(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %r15
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	movq	$0, 16(%rbx)
	movw	%ax, 24(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 52(%rbx)
	movl	$0, 56(%rbx)
	movl	$0, 48(%rbx)
	movq	(%rsi), %rax
	call	*40(%rax)
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-136(%rbp), %rdi
	movq	%rax, 40(%rbx)
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-96(%rbp), %rdi
	movq	%rbx, (%r12)
	movl	%eax, 48(%rbx)
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1238
	call	_ZdlPv@PLT
	movq	(%r12), %rbx
.L1238:
	movq	-136(%rbp), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-136(%rbp), %rdi
	movb	$1, 52(%rbx)
	movl	%eax, 56(%rbx)
	testq	%rdi, %rdi
	je	.L1237
	movq	(%rdi), %rax
	call	*64(%rax)
.L1237:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1245
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1245:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7781:
	.size	_ZN12v8_inspector12_GLOBAL__N_120currentDebugLocationEPNS_15V8InspectorImplE, .-_ZN12v8_inspector12_GLOBAL__N_120currentDebugLocationEPNS_15V8InspectorImplE
	.section	.text._ZNSt6vectorIN12v8_inspector19V8ProfilerAgentImpl17ProfileDescriptorESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector19V8ProfilerAgentImpl17ProfileDescriptorESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector19V8ProfilerAgentImpl17ProfileDescriptorESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN12v8_inspector19V8ProfilerAgentImpl17ProfileDescriptorESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN12v8_inspector19V8ProfilerAgentImpl17ProfileDescriptorESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB13344:
	.cfi_startproc
	endbr64
	movabsq	$115292150460684697, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movabsq	$-3689348814741910323, %rdx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r13
	movq	%rdi, -88(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rbx, %rax
	subq	%r13, %rax
	sarq	$4, %rax
	imulq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L1281
	subq	%r13, %rsi
	testq	%rax, %rax
	je	.L1273
	movabsq	$9223372036854775760, %rdx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1282
.L1248:
	movq	%rdx, %rdi
	movq	%rsi, -96(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%rax, -72(%rbp)
	leaq	80(%rax), %r8
	addq	%rax, %rdx
	movq	%rdx, -80(%rbp)
.L1272:
	movq	-72(%rbp), %rax
	movq	(%r14), %rcx
	addq	%rsi, %rax
	leaq	16(%rax), %rdx
	movq	%rdx, (%rax)
	leaq	16(%r14), %rdx
	cmpq	%rdx, %rcx
	je	.L1283
	movq	%rcx, (%rax)
	movq	16(%r14), %rcx
	movq	%rcx, 16(%rax)
.L1251:
	movq	8(%r14), %rcx
	movq	%rdx, (%r14)
	xorl	%r10d, %r10d
	movq	32(%r14), %rdx
	movq	$0, 8(%r14)
	movq	%rcx, 8(%rax)
	movq	40(%r14), %rcx
	movq	%rdx, 32(%rax)
	leaq	56(%rax), %rdx
	movq	%rdx, 40(%rax)
	leaq	56(%r14), %rdx
	movw	%r10w, 16(%r14)
	cmpq	%rdx, %rcx
	je	.L1284
	movq	%rcx, 40(%rax)
	movq	56(%r14), %rcx
	movq	%rcx, 56(%rax)
.L1253:
	movq	48(%r14), %rcx
	movq	%rdx, 40(%r14)
	xorl	%r9d, %r9d
	movq	72(%r14), %rdx
	movq	$0, 48(%r14)
	movq	%rcx, 48(%rax)
	movq	%rdx, 72(%rax)
	movq	-64(%rbp), %rax
	movw	%r9w, 56(%r14)
	cmpq	%r13, %rax
	je	.L1254
	leaq	-80(%rax), %rdx
	movq	-72(%rbp), %r15
	leaq	16(%r13), %r14
	movabsq	$922337203685477581, %rsi
	subq	%r13, %rdx
	leaq	56(%r13), %r12
	shrq	$4, %rdx
	imulq	%rsi, %rdx
	movabsq	$1152921504606846975, %rsi
	andq	%rsi, %rdx
	movq	%rdx, -96(%rbp)
	leaq	(%rdx,%rdx,4), %rdx
	salq	$4, %rdx
	leaq	96(%r13,%rdx), %rax
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L1263:
	leaq	16(%r15), %rsi
	movq	%rsi, (%r15)
	movq	-16(%r14), %rsi
	cmpq	%r14, %rsi
	je	.L1285
.L1255:
	movq	%rsi, (%r15)
	movq	(%r14), %rsi
	movq	%rsi, 16(%r15)
.L1256:
	movq	-8(%r14), %rsi
	xorl	%r8d, %r8d
	movq	%rsi, 8(%r15)
	movq	16(%r14), %rsi
	movq	%r14, -16(%r14)
	movq	$0, -8(%r14)
	movw	%r8w, (%r14)
	movq	%rsi, 32(%r15)
	leaq	56(%r15), %rsi
	movq	%rsi, 40(%r15)
	movq	24(%r14), %rsi
	cmpq	%r12, %rsi
	je	.L1286
	movq	%rsi, 40(%r15)
	movq	40(%r14), %rsi
	movq	%rsi, 56(%r15)
.L1258:
	movq	32(%r14), %rsi
	xorl	%edi, %edi
	movq	%rsi, 48(%r15)
	movq	56(%r14), %rsi
	movq	%r12, 24(%r14)
	movw	%di, 40(%r14)
	movq	$0, 32(%r14)
	movq	%rsi, 72(%r15)
	movq	24(%r14), %rdi
	cmpq	%r12, %rdi
	je	.L1259
	call	_ZdlPv@PLT
.L1259:
	movq	-16(%r14), %rdi
	cmpq	%r14, %rdi
	je	.L1260
	call	_ZdlPv@PLT
	addq	$80, %r15
	addq	$80, %r14
	addq	$80, %r12
	cmpq	%r14, -56(%rbp)
	jne	.L1263
.L1261:
	movq	-96(%rbp), %rax
	leaq	10(%rax,%rax,4), %r8
	salq	$4, %r8
	addq	-72(%rbp), %r8
.L1254:
	movq	-64(%rbp), %rcx
	cmpq	%rbx, %rcx
	je	.L1264
	leaq	16(%rcx), %rax
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L1270:
	leaq	16(%rdx), %rsi
	movq	%rsi, (%rdx)
	movq	-16(%rax), %rsi
	cmpq	%rax, %rsi
	je	.L1287
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	movq	%rsi, 16(%rdx)
.L1266:
	movq	-8(%rax), %rsi
	leaq	56(%rcx), %rdi
	movq	%rax, -16(%rax)
	movq	$0, -8(%rax)
	movq	%rsi, 8(%rdx)
	xorl	%esi, %esi
	movw	%si, (%rax)
	movq	16(%rax), %rsi
	movq	%rsi, 32(%rdx)
	leaq	56(%rdx), %rsi
	movq	%rsi, 40(%rdx)
	movq	24(%rax), %rsi
	cmpq	%rdi, %rsi
	je	.L1288
	movq	%rsi, 40(%rdx)
	movq	40(%rax), %rsi
	addq	$80, %rcx
	addq	$80, %rdx
	addq	$80, %rax
	movq	%rsi, -24(%rdx)
	movq	-48(%rax), %rsi
	movq	%rsi, -32(%rdx)
	movq	-24(%rax), %rsi
	movq	%rsi, -8(%rdx)
	cmpq	%rbx, %rcx
	jne	.L1270
.L1268:
	movabsq	$922337203685477581, %rdx
	subq	-64(%rbp), %rbx
	leaq	-80(%rbx), %rax
	shrq	$4, %rax
	imulq	%rdx, %rax
	movabsq	$1152921504606846975, %rdx
	andq	%rdx, %rax
	leaq	5(%rax,%rax,4), %rax
	salq	$4, %rax
	addq	%rax, %r8
.L1264:
	testq	%r13, %r13
	je	.L1271
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1271:
	movq	-72(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movq	%r8, %xmm5
	movq	-80(%rbp), %rbx
	punpcklqdq	%xmm5, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1260:
	.cfi_restore_state
	addq	$80, %r15
	addq	$80, %r14
	addq	$80, %r12
	cmpq	-56(%rbp), %r14
	je	.L1261
	leaq	16(%r15), %rsi
	movq	%rsi, (%r15)
	movq	-16(%r14), %rsi
	cmpq	%r14, %rsi
	jne	.L1255
.L1285:
	movdqu	(%r14), %xmm1
	movups	%xmm1, 16(%r15)
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1287:
	movdqu	(%rax), %xmm3
	movups	%xmm3, 16(%rdx)
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1288:
	movq	32(%rax), %rsi
	addq	$80, %rcx
	addq	$80, %rdx
	addq	$80, %rax
	movdqu	-40(%rax), %xmm4
	movq	%rsi, -32(%rdx)
	movq	-24(%rax), %rsi
	movups	%xmm4, -24(%rdx)
	movq	%rsi, -8(%rdx)
	cmpq	%rcx, %rbx
	jne	.L1270
	jmp	.L1268
	.p2align 4,,10
	.p2align 3
.L1286:
	movdqu	40(%r14), %xmm2
	movups	%xmm2, 56(%r15)
	jmp	.L1258
	.p2align 4,,10
	.p2align 3
.L1282:
	testq	%rdi, %rdi
	jne	.L1249
	movq	$0, -80(%rbp)
	movl	$80, %r8d
	movq	$0, -72(%rbp)
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1273:
	movl	$80, %edx
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1284:
	movdqu	56(%r14), %xmm7
	movups	%xmm7, 56(%rax)
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1283:
	movdqu	16(%r14), %xmm6
	movups	%xmm6, 16(%rax)
	jmp	.L1251
.L1249:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	imulq	$80, %rcx, %rdx
	jmp	.L1248
.L1281:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13344:
	.size	_ZNSt6vectorIN12v8_inspector19V8ProfilerAgentImpl17ProfileDescriptorESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN12v8_inspector19V8ProfilerAgentImpl17ProfileDescriptorESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag:
.LFB13718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	jne	.L1290
	testq	%rdx, %rdx
	jne	.L1306
.L1290:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L1291
	movq	(%r12), %rdi
.L1292:
	cmpq	$2, %rbx
	je	.L1307
	testq	%rbx, %rbx
	je	.L1295
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L1295:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1307:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1291:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L1308
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L1292
.L1306:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1308:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13718:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.section	.text._ZN12v8_inspector12_GLOBAL__N_117resourceNameToUrlEPNS_15V8InspectorImplEN2v85LocalINS3_6StringEEE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_117resourceNameToUrlEPNS_15V8InspectorImplEN2v85LocalINS3_6StringEEE, @function
_ZN12v8_inspector12_GLOBAL__N_117resourceNameToUrlEPNS_15V8InspectorImplEN2v85LocalINS3_6StringEEE:
.LFB7773:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	16(%rbx), %r14
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	(%r14), %rax
	movq	216(%rax), %rbx
	call	_ZN12v8_inspector12toStringViewERKNS_8String16E@PLT
	leaq	_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE(%rip), %rax
	cmpq	%rax, %rbx
	jne	.L1310
	movq	$0, -136(%rbp)
.L1311:
	leaq	16(%r12), %rax
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	-88(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-64(%rbp), %rax
	movq	%rax, 32(%r12)
	.p2align 4,,10
	.p2align 3
.L1312:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1313
	movq	(%rdi), %rax
	call	*8(%rax)
.L1313:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1309
	call	_ZdlPv@PLT
.L1309:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1323
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1310:
	.cfi_restore_state
	leaq	-136(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	*%rbx
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1311
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	jmp	.L1312
.L1323:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7773:
	.size	_ZN12v8_inspector12_GLOBAL__N_117resourceNameToUrlEPNS_15V8InspectorImplEN2v85LocalINS3_6StringEEE, .-_ZN12v8_inspector12_GLOBAL__N_117resourceNameToUrlEPNS_15V8InspectorImplEN2v85LocalINS3_6StringEEE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_118coverageToProtocolEPNS_15V8InspectorImplERKN2v85debug8CoverageEPSt10unique_ptrISt6vectorIS8_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteISC_EESaISF_EESD_ISH_EE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_118coverageToProtocolEPNS_15V8InspectorImplERKN2v85debug8CoverageEPSt10unique_ptrISt6vectorIS8_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteISC_EESaISF_EESD_ISH_EE, @function
_ZN12v8_inspector12_GLOBAL__N_118coverageToProtocolEPNS_15V8InspectorImplERKN2v85debug8CoverageEPSt10unique_ptrISt6vectorIS8_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteISC_EESaISF_EESD_ISH_EE:
.LFB7822:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$344, %rsp
	movq	%rdi, -360(%rbp)
	movl	$24, %edi
	movq	%rsi, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rcx, -376(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, -336(%rbp)
	movq	%rax, -344(%rbp)
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	8(%rbx), %rax
	movq	%rax, -320(%rbp)
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	-352(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZNK2v85debug8Coverage11ScriptCountEv@PLT
	movq	-336(%rbp), %rdx
	cmpq	%rdx, %rax
	jbe	.L1325
	leaq	-240(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%rax, -312(%rbp)
	call	_ZNK2v85debug8Coverage13GetScriptDataEm@PLT
	movq	%rbx, %rdi
	call	_ZNK2v85debug8Coverage10ScriptData9GetScriptEv@PLT
	movl	$24, %edi
	movq	%rax, -328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, -280(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	leaq	-96(%rbp), %rax
	movq	%rax, -304(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -296(%rbp)
	.p2align 4,,10
	.p2align 3
.L1373:
	movq	-312(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZNK2v85debug8Coverage10ScriptData13FunctionCountEv@PLT
	movq	-280(%rbp), %rdx
	cmpq	%rdx, %rax
	jbe	.L1326
	leaq	-208(%rbp), %r14
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZNK2v85debug8Coverage10ScriptData15GetFunctionDataEm@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	$0, 16(%rax)
	movq	%rax, %rbx
	movups	%xmm0, (%rax)
	call	_ZNK2v85debug8Coverage12FunctionData5CountEv@PLT
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	_ZNK2v85debug8Coverage12FunctionData9EndOffsetEv@PLT
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	_ZNK2v85debug8Coverage12FunctionData11StartOffsetEv@PLT
	movl	$24, %edi
	movl	%eax, %r15d
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE(%rip), %rsi
	movq	%rsi, (%rax)
	movq	8(%rbx), %rsi
	movl	%r15d, 8(%rax)
	movl	%r13d, 12(%rax)
	movl	%r12d, 16(%rax)
	movq	%rax, -176(%rbp)
	cmpq	16(%rbx), %rsi
	je	.L1327
	movq	$0, -176(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%rbx)
.L1328:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1329
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1330
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1329:
	xorl	%r12d, %r12d
	leaq	-176(%rbp), %r13
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1595:
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
	cmpl	$1, %eax
	je	.L1594
	.p2align 4,,10
	.p2align 3
.L1337:
	addq	$1, %r12
.L1345:
	movq	%r14, %rdi
	call	_ZNK2v85debug8Coverage12FunctionData10BlockCountEv@PLT
	cmpq	%r12, %rax
	jbe	.L1331
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v85debug8Coverage12FunctionData12GetBlockDataEm@PLT
	movq	%r13, %rdi
	call	_ZNK2v85debug8Coverage9BlockData5CountEv@PLT
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	_ZNK2v85debug8Coverage9BlockData9EndOffsetEv@PLT
	movq	%r13, %rdi
	movl	%eax, -272(%rbp)
	call	_ZNK2v85debug8Coverage9BlockData11StartOffsetEv@PLT
	movl	$24, %edi
	movl	%eax, -264(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE(%rip), %rcx
	movl	-272(%rbp), %edx
	movq	8(%rbx), %rsi
	movq	%rcx, (%rax)
	movl	-264(%rbp), %ecx
	movl	%edx, 12(%rax)
	movl	%ecx, 8(%rax)
	movl	%r15d, 16(%rax)
	movq	%rax, -248(%rbp)
	cmpq	16(%rbx), %rsi
	je	.L1332
	movq	$0, -248(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%rbx)
.L1333:
	movq	-248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1334
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1335
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1334:
	movq	-160(%rbp), %r15
	testq	%r15, %r15
	je	.L1337
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	jne	.L1595
	movl	8(%r15), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r15)
	cmpl	$1, %eax
	jne	.L1337
.L1594:
	movq	(%r15), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rcx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1596
.L1341:
	testq	%rdx, %rdx
	je	.L1342
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L1343:
	cmpl	$1, %eax
	jne	.L1337
	movq	(%r15), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r15, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1344
	call	*8(%rax)
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1332:
	leaq	-248(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1335:
	call	*%rax
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1331:
	movl	$64, %edi
	call	_Znwm@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	leaq	8(%r13), %r12
	movw	%r8w, 24(%r13)
	movq	%rax, 8(%r13)
	movq	$0, 16(%r13)
	movq	$0, 40(%r13)
	movq	$0, 48(%r13)
	movb	$0, 56(%r13)
	call	_ZNK2v85debug8Coverage12FunctionData4NameEv@PLT
	movq	-304(%rbp), %r15
	movq	-320(%rbp), %rsi
	movq	%rax, %rdx
	movq	%r15, %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	48(%r13), %r12
	movq	%rbx, 48(%r13)
	movq	%rax, 40(%r13)
	testq	%r12, %r12
	je	.L1346
	movq	8(%r12), %rbx
	movq	(%r12), %r15
	cmpq	%r15, %rbx
	jne	.L1350
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1598:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1348:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L1597
.L1350:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1348
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1598
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L1350
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	(%r12), %r15
.L1347:
	testq	%r15, %r15
	je	.L1351
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1351:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1346:
	movq	%r14, %rdi
	call	_ZNK2v85debug8Coverage12FunctionData16HasBlockCoverageEv@PLT
	movq	%r13, -176(%rbp)
	movb	%al, 56(%r13)
	movq	-288(%rbp), %rax
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L1352
	movq	$0, -176(%rbp)
	movq	%r13, (%rsi)
	addq	$8, 8(%rax)
.L1353:
	movq	-176(%rbp), %r14
	testq	%r14, %r14
	je	.L1354
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1355
	movq	48(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L1356
	movq	8(%r12), %rbx
	movq	(%r12), %r13
	cmpq	%r13, %rbx
	jne	.L1360
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1600:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1358:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L1599
.L1360:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1358
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1600
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1360
	.p2align 4,,10
	.p2align 3
.L1599:
	movq	(%r12), %r13
.L1357:
	testq	%r13, %r13
	je	.L1361
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1361:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1356:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1362
	call	_ZdlPv@PLT
.L1362:
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1354:
	movq	-96(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L1363
	call	_ZdlPv@PLT
.L1363:
	movq	-192(%rbp), %r12
	testq	%r12, %r12
	je	.L1365
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1366
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1601
	.p2align 4,,10
	.p2align 3
.L1365:
	addq	$1, -280(%rbp)
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1342:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1596:
	movq	%rdx, -264(%rbp)
	movq	%r15, %rdi
	call	*%rax
	movq	-264(%rbp), %rdx
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1366:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1365
.L1601:
	movq	(%r12), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rcx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1602
.L1369:
	testq	%rbx, %rbx
	je	.L1370
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1371:
	cmpl	$1, %eax
	jne	.L1365
	movq	(%r12), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1372
	call	*8(%rax)
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1352:
	movq	%rax, %rdi
	leaq	-176(%rbp), %rdx
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1327:
	leaq	-176(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1330:
	call	*%rax
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1355:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1326:
	xorl	%edi, %edi
	leaq	-128(%rbp), %rax
	movq	$0, -136(%rbp)
	movw	%di, -128(%rbp)
	movq	-328(%rbp), %rdi
	movq	%rax, -272(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -112(%rbp)
	call	_ZNK2v85debug6Script9SourceURLEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1374
	movq	%rax, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	testl	%eax, %eax
	jne	.L1603
.L1374:
	movq	-328(%rbp), %rdi
	call	_ZNK2v85debug6Script4NameEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1385
	movq	%rax, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	testl	%eax, %eax
	jne	.L1604
.L1385:
	movl	$96, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	-328(%rbp), %rdi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %r13
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	leaq	48(%rbx), %r12
	movw	%ax, 24(%rbx)
	leaq	64(%rbx), %rax
	movw	%dx, 64(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movq	%rax, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	call	_ZNK2v85debug6Script2IdEv@PLT
	movq	-304(%rbp), %r14
	movl	%eax, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	leaq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, 40(%rbx)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rax
	movq	-288(%rbp), %rcx
	movq	%rax, 80(%rbx)
	movq	88(%rbx), %rax
	movq	%rcx, 88(%rbx)
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	je	.L1393
	movq	8(%rax), %rsi
	movq	(%rax), %r12
	movq	%rsi, -264(%rbp)
	cmpq	%r12, %rsi
	je	.L1394
	movq	%rbx, -288(%rbp)
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1395
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1396
	movq	48(%rbx), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r14, %r14
	je	.L1397
	movq	8(%r14), %r13
	movq	(%r14), %r15
	cmpq	%r15, %r13
	jne	.L1401
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1606:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1399:
	addq	$8, %r15
	cmpq	%r15, %r13
	je	.L1605
.L1401:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1399
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1606
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %r13
	jne	.L1401
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	(%r14), %r15
.L1398:
	testq	%r15, %r15
	je	.L1402
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1402:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1397:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1403
	call	_ZdlPv@PLT
.L1403:
	movl	$64, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1395:
	addq	$8, %r12
	cmpq	%r12, -264(%rbp)
	jne	.L1404
.L1610:
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %rbx
	movq	(%rax), %r12
.L1394:
	testq	%r12, %r12
	je	.L1405
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1405:
	movq	-280(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1393:
	movq	-344(%rbp), %rax
	movq	%rbx, -176(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L1406
	movq	$0, -176(%rbp)
	movq	%rbx, (%rsi)
	addq	$8, 8(%rax)
.L1407:
	movq	-176(%rbp), %r13
	testq	%r13, %r13
	je	.L1408
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1409
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	88(%r13), %rax
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	je	.L1410
	movq	8(%rax), %rbx
	movq	(%rax), %r12
	movq	%rbx, -264(%rbp)
	cmpq	%r12, %rbx
	je	.L1411
	movq	%r13, -288(%rbp)
	.p2align 4,,10
	.p2align 3
.L1421:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1412
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1413
	movq	48(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1414
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	jne	.L1418
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1608:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1416:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L1607
.L1418:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1416
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1608
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L1418
.L1607:
	movq	(%r14), %r15
.L1415:
	testq	%r15, %r15
	je	.L1419
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1419:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1414:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1420
	call	_ZdlPv@PLT
.L1420:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1412:
	addq	$8, %r12
	cmpq	%r12, -264(%rbp)
	jne	.L1421
.L1611:
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %r13
	movq	(%rax), %r12
.L1411:
	testq	%r12, %r12
	je	.L1422
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1422:
	movq	-280(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1410:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1423
	call	_ZdlPv@PLT
.L1423:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1424
	call	_ZdlPv@PLT
.L1424:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1408:
	movq	-96(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L1425
	call	_ZdlPv@PLT
.L1425:
	movq	-144(%rbp), %rdi
	cmpq	-272(%rbp), %rdi
	je	.L1426
	call	_ZdlPv@PLT
.L1426:
	movq	-224(%rbp), %r12
	testq	%r12, %r12
	je	.L1428
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1429
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1609
	.p2align 4,,10
	.p2align 3
.L1428:
	addq	$1, -336(%rbp)
	jmp	.L1436
	.p2align 4,,10
	.p2align 3
.L1344:
	call	*%rdx
	jmp	.L1337
.L1370:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1371
.L1396:
	movq	%rbx, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -264(%rbp)
	jne	.L1404
	jmp	.L1610
.L1413:
	movq	%r13, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -264(%rbp)
	jne	.L1421
	jmp	.L1611
.L1602:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1369
.L1429:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1428
.L1609:
	movq	(%r12), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rcx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1612
.L1432:
	testq	%rbx, %rbx
	je	.L1433
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1434:
	cmpl	$1, %eax
	jne	.L1428
	movq	(%r12), %rdx
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L1435
	call	*8(%rdx)
	jmp	.L1428
.L1603:
	movq	-304(%rbp), %rdi
	movq	-320(%rbp), %rsi
	movq	%r12, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-144(%rbp), %rdi
	movq	-96(%rbp), %rax
	cmpq	-296(%rbp), %rax
	je	.L1593
	movq	-88(%rbp), %xmm0
	movq	-80(%rbp), %rdx
	cmpq	-272(%rbp), %rdi
	je	.L1613
	movq	%rdx, %xmm3
	movq	-128(%rbp), %rcx
	movq	%rax, -144(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -136(%rbp)
	testq	%rdi, %rdi
	je	.L1391
	movq	%rdi, -96(%rbp)
	movq	%rcx, -80(%rbp)
.L1389:
	xorl	%ecx, %ecx
	movq	$0, -88(%rbp)
	movw	%cx, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -112(%rbp)
	cmpq	-296(%rbp), %rdi
	je	.L1385
	call	_ZdlPv@PLT
	jmp	.L1385
.L1406:
	movq	%rax, %rdi
	leaq	-176(%rbp), %rdx
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler14ScriptCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1407
.L1409:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1408
.L1604:
	movq	-304(%rbp), %rdi
	movq	-368(%rbp), %rsi
	movq	%r12, %rdx
	call	_ZN12v8_inspector12_GLOBAL__N_117resourceNameToUrlEPNS_15V8InspectorImplEN2v85LocalINS3_6StringEEE
	movq	-144(%rbp), %rdi
	movq	-96(%rbp), %rax
	cmpq	-296(%rbp), %rax
	je	.L1593
	movq	-88(%rbp), %xmm0
	movq	-80(%rbp), %xmm1
	cmpq	-272(%rbp), %rdi
	je	.L1614
	punpcklqdq	%xmm1, %xmm0
	movq	-128(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movups	%xmm0, -136(%rbp)
	testq	%rdi, %rdi
	je	.L1391
	movq	%rdi, -96(%rbp)
	movq	%rdx, -80(%rbp)
	jmp	.L1389
.L1614:
	movq	%rax, -144(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -136(%rbp)
.L1391:
	movq	-296(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	%rax, %rdi
	jmp	.L1389
.L1325:
	movq	-376(%rbp), %rbx
	movq	-344(%rbp), %rcx
	movq	(%rbx), %rax
	movq	%rcx, (%rbx)
	movq	%rax, -296(%rbp)
	testq	%rax, %rax
	je	.L1437
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -280(%rbp)
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rbx
	je	.L1438
.L1456:
	movq	-264(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L1439
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1440
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	88(%r13), %rax
	movq	%rax, -288(%rbp)
	testq	%rax, %rax
	je	.L1441
	movq	8(%rax), %rbx
	movq	(%rax), %r12
	movq	%rbx, -272(%rbp)
	cmpq	%r12, %rbx
	je	.L1442
	movq	%r13, -304(%rbp)
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1443
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1444
	movq	48(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1445
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	jne	.L1449
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1616:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1447:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L1615
.L1449:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1447
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1616
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L1449
.L1615:
	movq	(%r14), %r15
.L1446:
	testq	%r15, %r15
	je	.L1450
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1450:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1445:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1451
	call	_ZdlPv@PLT
.L1451:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1443:
	addq	$8, %r12
	cmpq	%r12, -272(%rbp)
	jne	.L1452
.L1618:
	movq	-288(%rbp), %rax
	movq	-304(%rbp), %r13
	movq	(%rax), %r12
.L1442:
	testq	%r12, %r12
	je	.L1453
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1453:
	movq	-288(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1441:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1454
	call	_ZdlPv@PLT
.L1454:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1455
	call	_ZdlPv@PLT
.L1455:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1439:
	addq	$8, -264(%rbp)
	movq	-264(%rbp), %rax
	cmpq	%rax, -280(%rbp)
	jne	.L1456
	movq	-296(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -264(%rbp)
.L1438:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L1457
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1457:
	movq	-296(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1437:
	movq	-360(%rbp), %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1617
	movq	-360(%rbp), %rax
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1444:
	.cfi_restore_state
	movq	%r13, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -272(%rbp)
	jne	.L1452
	jmp	.L1618
.L1593:
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L1387
	cmpq	$1, %rax
	je	.L1619
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L1387
	movq	-296(%rbp), %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
.L1387:
	xorl	%esi, %esi
	movq	%rax, -136(%rbp)
	movw	%si, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L1389
.L1372:
	call	*%rdx
	jmp	.L1365
.L1433:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1434
.L1440:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1439
.L1613:
	movq	%rdx, %xmm2
	movq	%rax, -144(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -136(%rbp)
	jmp	.L1391
.L1612:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1432
.L1435:
	call	*%rax
	jmp	.L1428
.L1619:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L1387
.L1617:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7822:
	.size	_ZN12v8_inspector12_GLOBAL__N_118coverageToProtocolEPNS_15V8InspectorImplERKN2v85debug8CoverageEPSt10unique_ptrISt6vectorIS8_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteISC_EESaISF_EESD_ISH_EE, .-_ZN12v8_inspector12_GLOBAL__N_118coverageToProtocolEPNS_15V8InspectorImplERKN2v85debug8CoverageEPSt10unique_ptrISt6vectorIS8_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteISC_EESaISF_EESD_ISH_EE
	.section	.rodata._ZN12v8_inspector19V8ProfilerAgentImpl19takePreciseCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"Precise coverage has not been started."
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl19takePreciseCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl19takePreciseCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl19takePreciseCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE, @function
_ZN12v8_inspector19V8ProfilerAgentImpl19takePreciseCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE:
.LFB7844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%rdx, -152(%rbp)
	movq	32(%rsi), %r14
	leaq	_ZN12v8_inspector18ProfilerAgentStateL22preciseCoverageStartedE(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %r14d
	cmpq	%r15, %rdi
	je	.L1621
	call	_ZdlPv@PLT
.L1621:
	testb	%r14b, %r14b
	jne	.L1622
	leaq	.LC5(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1620
	call	_ZdlPv@PLT
.L1620:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1636
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1622:
	.cfi_restore_state
	movq	16(%rbx), %rsi
	leaq	-128(%rbp), %r14
	leaq	-144(%rbp), %r13
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v85debug8Coverage14CollectPreciseEPNS_7IsolateE@PLT
	movq	8(%rbx), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	-152(%rbp), %rcx
	movq	24(%rax), %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_118coverageToProtocolEPNS_15V8InspectorImplERKN2v85debug8CoverageEPSt10unique_ptrISt6vectorIS8_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteISC_EESaISF_EESD_ISH_EE
	movq	-136(%rbp), %r13
	testq	%r13, %r13
	je	.L1626
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1627
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1628:
	cmpl	$1, %eax
	jne	.L1626
	movq	0(%r13), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1637
.L1630:
	testq	%rbx, %rbx
	je	.L1631
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1632:
	cmpl	$1, %eax
	jne	.L1626
	movq	0(%r13), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1633
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1626:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1627:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1631:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1630
.L1633:
	call	*%rdx
	jmp	.L1626
.L1636:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7844:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl19takePreciseCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE, .-_ZN12v8_inspector19V8ProfilerAgentImpl19takePreciseCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl21getBestEffortCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl21getBestEffortCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl21getBestEffortCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE, @function
_ZN12v8_inspector19V8ProfilerAgentImpl21getBestEffortCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE:
.LFB7851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	16(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v85debug8Coverage17CollectBestEffortEPNS_7IsolateE@PLT
	movq	8(%rbx), %rax
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_118coverageToProtocolEPNS_15V8InspectorImplERKN2v85debug8CoverageEPSt10unique_ptrISt6vectorIS8_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteISC_EESaISF_EESD_ISH_EE
	movq	-88(%rbp), %r13
	testq	%r13, %r13
	je	.L1640
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1641
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1650
	.p2align 4,,10
	.p2align 3
.L1640:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1651
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1641:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1640
.L1650:
	movq	0(%r13), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1652
.L1644:
	testq	%rbx, %rbx
	je	.L1645
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1646:
	cmpl	$1, %eax
	jne	.L1640
	movq	0(%r13), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1647
	call	*8(%rax)
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1645:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1647:
	call	*%rdx
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1652:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1644
.L1651:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7851:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl21getBestEffortCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE, .-_ZN12v8_inspector19V8ProfilerAgentImpl21getBestEffortCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.section	.rodata._ZN12v8_inspector19V8ProfilerAgentImpl15takeTypeProfileEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS5_EESaIS8_EES6_ISA_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"Type profile has not been started."
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl15takeTypeProfileEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS5_EESaIS8_EES6_ISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl15takeTypeProfileEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl15takeTypeProfileEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS5_EESaIS8_EES6_ISA_EE, @function
_ZN12v8_inspector19V8ProfilerAgentImpl15takeTypeProfileEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS5_EESaIS8_EES6_ISA_EE:
.LFB7863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$408, %rsp
	movq	%rdi, -344(%rbp)
	movq	32(%rsi), %r12
	leaq	_ZN12v8_inspector18ProfilerAgentStateL18typeProfileStartedE(%rip), %rsi
	movq	%rdx, -416(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %r12d
	leaq	-80(%rbp), %rax
	movq	%rax, -312(%rbp)
	cmpq	%rax, %rdi
	je	.L1654
	call	_ZdlPv@PLT
.L1654:
	testb	%r12b, %r12b
	jne	.L1655
	movq	-320(%rbp), %rbx
	leaq	.LC6(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-344(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-312(%rbp), %rdi
	je	.L1653
	call	_ZdlPv@PLT
.L1653:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1934
	movq	-344(%rbp), %rax
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1655:
	.cfi_restore_state
	leaq	-272(%rbp), %rax
	movq	16(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	-288(%rbp), %rax
	movq	16(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v85debug11TypeProfile7CollectEPNS_7IsolateE@PLT
	movq	8(%rbx), %rax
	movl	$24, %edi
	movq	24(%rax), %rbx
	movq	%rbx, -432(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	8(%rbx), %rax
	movq	%rax, -368(%rbp)
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	-424(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZNK2v85debug11TypeProfile11ScriptCountEv@PLT
	movq	-360(%rbp), %rdx
	cmpq	%rdx, %rax
	jbe	.L1658
	leaq	-240(%rbp), %r12
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK2v85debug11TypeProfile13GetScriptDataEm@PLT
	movq	%r12, %rdi
	call	_ZNK2v85debug11TypeProfile10ScriptData9GetScriptEv@PLT
	movl	$24, %edi
	movq	%rax, -328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, 16(%rax)
	movq	%rax, %rbx
	movq	%rax, -376(%rbp)
	movups	%xmm0, (%rax)
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -408(%rbp)
	call	_ZNK2v85debug11TypeProfile10ScriptData7EntriesEv@PLT
	movq	-176(%rbp), %r14
	movq	-168(%rbp), %r12
	cmpq	%r12, %r14
	je	.L1659
	leaq	-208(%rbp), %rax
	movq	%r12, -392(%rbp)
	movq	%rbx, %r12
	movq	%rax, -336(%rbp)
	.p2align 4,,10
	.p2align 3
.L1689:
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	-336(%rbp), %rdi
	movq	%r14, %rsi
	movq	$0, 16(%rax)
	movq	%rax, %r13
	movups	%xmm0, (%rax)
	call	_ZNK2v85debug11TypeProfile5Entry5TypesEv@PLT
	movq	-208(%rbp), %rbx
	movq	-200(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L1660
	.p2align 4,,10
	.p2align 3
.L1669:
	movl	$48, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rcx
	movq	-368(%rbp), %rsi
	movq	-320(%rbp), %rdi
	leaq	8(%rax), %r8
	leaq	24(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%r8, -400(%rbp)
	xorl	%r8d, %r8d
	movw	%r8w, 24(%rax)
	movq	%rdx, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 40(%rax)
	movq	(%rbx), %rdx
	movq	%rax, -384(%rbp)
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-400(%rbp), %r8
	movq	-320(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-384(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	8(%r13), %rsi
	movq	%rdx, 40(%rax)
	movq	%rax, -296(%rbp)
	cmpq	16(%r13), %rsi
	je	.L1661
	movq	$0, -296(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r13)
.L1662:
	movq	-296(%rbp), %r8
	testq	%r8, %r8
	je	.L1663
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1935
	movq	%r8, %rdi
	call	*%rax
.L1663:
	movq	-96(%rbp), %rdi
	cmpq	-312(%rbp), %rdi
	je	.L1666
	call	_ZdlPv@PLT
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L1669
.L1668:
	movq	-208(%rbp), %r15
.L1660:
	testq	%r15, %r15
	je	.L1670
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1670:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, (%rbx)
	movq	$0, 16(%rbx)
	movl	$0, 8(%rbx)
	call	_ZNK2v85debug11TypeProfile5Entry14SourcePositionEv@PLT
	movq	16(%rbx), %r15
	movq	%r13, 16(%rbx)
	movl	%eax, 8(%rbx)
	testq	%r15, %r15
	je	.L1671
	movq	8(%r15), %rax
	movq	(%r15), %r13
	movq	%rax, -384(%rbp)
	cmpq	%r13, %rax
	jne	.L1676
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1937:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1675
	movq	%r8, -400(%rbp)
	call	_ZdlPv@PLT
	movq	-400(%rbp), %r8
.L1675:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1673:
	addq	$8, %r13
	cmpq	%r13, -384(%rbp)
	je	.L1936
.L1676:
	movq	0(%r13), %r8
	testq	%r8, %r8
	je	.L1673
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1937
	movq	%r8, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -384(%rbp)
	jne	.L1676
	.p2align 4,,10
	.p2align 3
.L1936:
	movq	(%r15), %r13
.L1672:
	testq	%r13, %r13
	je	.L1677
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1677:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1671:
	movq	%rbx, -208(%rbp)
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L1678
	movq	$0, -208(%rbp)
	movq	%rbx, (%rsi)
	addq	$8, 8(%r12)
.L1679:
	movq	-208(%rbp), %r13
	testq	%r13, %r13
	je	.L1680
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1681
	movq	16(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L1682
	movq	8(%r15), %rax
	movq	(%r15), %rbx
	movq	%rax, -384(%rbp)
	cmpq	%rbx, %rax
	jne	.L1687
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L1939:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1686
	movq	%r8, -400(%rbp)
	call	_ZdlPv@PLT
	movq	-400(%rbp), %r8
.L1686:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1684:
	addq	$8, %rbx
	cmpq	%rbx, -384(%rbp)
	je	.L1938
.L1687:
	movq	(%rbx), %r8
	testq	%r8, %r8
	je	.L1684
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1939
	movq	%r8, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%rbx, -384(%rbp)
	jne	.L1687
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	(%r15), %rbx
.L1683:
	testq	%rbx, %rbx
	je	.L1688
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1688:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1682:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1680:
	addq	$24, %r14
	cmpq	%r14, -392(%rbp)
	jne	.L1689
	movq	-168(%rbp), %rbx
	movq	-176(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1659
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %r14
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1942:
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1940
	.p2align 4,,10
	.p2align 3
.L1691:
	addq	$24, %r12
	cmpq	%r12, %rbx
	je	.L1941
.L1699:
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L1691
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	jne	.L1942
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1691
.L1940:
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1943
.L1695:
	testq	%r15, %r15
	je	.L1696
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1697:
	cmpl	$1, %eax
	jne	.L1691
	movq	0(%r13), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1698
	call	*8(%rax)
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1699
	.p2align 4,,10
	.p2align 3
.L1941:
	movq	-176(%rbp), %r12
.L1659:
	testq	%r12, %r12
	je	.L1700
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1700:
	xorl	%edi, %edi
	leaq	-128(%rbp), %rax
	movq	$0, -136(%rbp)
	movw	%di, -128(%rbp)
	movq	-328(%rbp), %rdi
	movq	%rax, -336(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -112(%rbp)
	call	_ZNK2v85debug6Script9SourceURLEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1701
	movq	%rax, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	testl	%eax, %eax
	jne	.L1944
.L1701:
	movq	-328(%rbp), %rdi
	call	_ZNK2v85debug6Script4NameEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1712
	movq	%rax, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	testl	%eax, %eax
	jne	.L1945
.L1712:
	movl	$96, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	-328(%rbp), %rdi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %r13
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	leaq	48(%rbx), %r12
	movw	%ax, 24(%rbx)
	leaq	64(%rbx), %rax
	movw	%dx, 64(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movq	%rax, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	call	_ZNK2v85debug6Script2IdEv@PLT
	movq	-320(%rbp), %r14
	movl	%eax, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	leaq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, 40(%rbx)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rax
	movq	88(%rbx), %r14
	movq	%rax, 80(%rbx)
	movq	-376(%rbp), %rax
	movq	%rax, 88(%rbx)
	testq	%r14, %r14
	je	.L1720
	movq	8(%r14), %rax
	movq	(%r14), %r15
	movq	%rax, -328(%rbp)
	cmpq	%r15, %rax
	je	.L1721
	movq	%r14, -376(%rbp)
	movq	%rbx, -384(%rbp)
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L1722
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1723
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1724
	movq	8(%r13), %rbx
	movq	0(%r13), %r14
	cmpq	%r14, %rbx
	jne	.L1729
	jmp	.L1725
	.p2align 4,,10
	.p2align 3
.L1947:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1728
	movq	%r8, -392(%rbp)
	call	_ZdlPv@PLT
	movq	-392(%rbp), %r8
.L1728:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1726:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1946
.L1729:
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L1726
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1947
	addq	$8, %r14
	movq	%r8, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L1729
	.p2align 4,,10
	.p2align 3
.L1946:
	movq	0(%r13), %r14
.L1725:
	testq	%r14, %r14
	je	.L1730
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1730:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1724:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1722:
	addq	$8, %r15
	cmpq	%r15, -328(%rbp)
	jne	.L1731
.L1952:
	movq	-376(%rbp), %r14
	movq	-384(%rbp), %rbx
	movq	(%r14), %r15
.L1721:
	testq	%r15, %r15
	je	.L1732
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1732:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1720:
	movq	-352(%rbp), %rax
	movq	%rbx, -176(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L1733
	movq	$0, -176(%rbp)
	movq	%rbx, (%rsi)
	addq	$8, 8(%rax)
.L1734:
	movq	-176(%rbp), %r12
	testq	%r12, %r12
	je	.L1735
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1736
	movq	88(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L1737
	movq	8(%r15), %rax
	movq	(%r15), %rbx
	movq	%rax, -328(%rbp)
	cmpq	%rbx, %rax
	je	.L1738
	movq	%r12, -376(%rbp)
	movq	%r15, -384(%rbp)
	.p2align 4,,10
	.p2align 3
.L1748:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1739
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1740
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1741
	movq	8(%r13), %r14
	movq	0(%r13), %r15
	cmpq	%r15, %r14
	je	.L1742
	movq	%r14, -392(%rbp)
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L1949:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r14), %rdi
	movq	%rax, (%r14)
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1745
	call	_ZdlPv@PLT
.L1745:
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1743:
	addq	$8, %r15
	cmpq	%r15, -392(%rbp)
	je	.L1948
.L1746:
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L1743
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1949
	movq	%r14, %rdi
	addq	$8, %r15
	call	*%rax
	cmpq	%r15, -392(%rbp)
	jne	.L1746
	.p2align 4,,10
	.p2align 3
.L1948:
	movq	0(%r13), %r15
.L1742:
	testq	%r15, %r15
	je	.L1747
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1747:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1741:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1739:
	addq	$8, %rbx
	cmpq	%rbx, -328(%rbp)
	jne	.L1748
	movq	-384(%rbp), %r15
	movq	-376(%rbp), %r12
	movq	(%r15), %rbx
.L1738:
	testq	%rbx, %rbx
	je	.L1749
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1749:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1737:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1750
	call	_ZdlPv@PLT
.L1750:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1751
	call	_ZdlPv@PLT
.L1751:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1735:
	movq	-96(%rbp), %rdi
	cmpq	-312(%rbp), %rdi
	je	.L1752
	call	_ZdlPv@PLT
.L1752:
	movq	-144(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L1753
	call	_ZdlPv@PLT
.L1753:
	movq	-224(%rbp), %r12
	testq	%r12, %r12
	je	.L1755
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1756
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1950
	.p2align 4,,10
	.p2align 3
.L1755:
	addq	$1, -360(%rbp)
	jmp	.L1763
	.p2align 4,,10
	.p2align 3
.L1666:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L1669
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L1661:
	leaq	-296(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1662
	.p2align 4,,10
	.p2align 3
.L1935:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1665
	movq	%r8, -384(%rbp)
	call	_ZdlPv@PLT
	movq	-384(%rbp), %r8
.L1665:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L1696:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1697
	.p2align 4,,10
	.p2align 3
.L1943:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1695
	.p2align 4,,10
	.p2align 3
.L1678:
	movq	-336(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1679
	.p2align 4,,10
	.p2align 3
.L1681:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1680
	.p2align 4,,10
	.p2align 3
.L1740:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1739
	.p2align 4,,10
	.p2align 3
.L1756:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1755
.L1950:
	movq	(%r12), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1951
.L1759:
	testq	%rbx, %rbx
	je	.L1760
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1761:
	cmpl	$1, %eax
	jne	.L1755
	movq	(%r12), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1762
	call	*8(%rax)
	jmp	.L1755
	.p2align 4,,10
	.p2align 3
.L1723:
	movq	%r12, %rdi
	addq	$8, %r15
	call	*%rax
	cmpq	%r15, -328(%rbp)
	jne	.L1731
	jmp	.L1952
	.p2align 4,,10
	.p2align 3
.L1944:
	movq	-320(%rbp), %rdi
	movq	-368(%rbp), %rsi
	movq	%r12, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-144(%rbp), %rdi
	movq	-96(%rbp), %rax
	cmpq	-312(%rbp), %rax
	je	.L1933
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rcx
	cmpq	-336(%rbp), %rdi
	je	.L1953
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	-128(%rbp), %rsi
	movq	%rax, -144(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -136(%rbp)
	testq	%rdi, %rdi
	je	.L1718
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L1716:
	xorl	%ecx, %ecx
	movq	$0, -88(%rbp)
	movw	%cx, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -112(%rbp)
	cmpq	-312(%rbp), %rdi
	je	.L1712
	call	_ZdlPv@PLT
	jmp	.L1712
	.p2align 4,,10
	.p2align 3
.L1733:
	movq	-408(%rbp), %rdx
	movq	%rax, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1734
	.p2align 4,,10
	.p2align 3
.L1736:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1945:
	movq	-320(%rbp), %rdi
	movq	-432(%rbp), %rsi
	movq	%r12, %rdx
	call	_ZN12v8_inspector12_GLOBAL__N_117resourceNameToUrlEPNS_15V8InspectorImplEN2v85LocalINS3_6StringEEE
	movq	-144(%rbp), %rdi
	movq	-96(%rbp), %rax
	cmpq	-312(%rbp), %rax
	je	.L1933
	movq	-88(%rbp), %xmm0
	movq	-80(%rbp), %rdx
	cmpq	-336(%rbp), %rdi
	je	.L1954
	movq	%rdx, %xmm2
	movq	-128(%rbp), %rcx
	movq	%rax, -144(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -136(%rbp)
	testq	%rdi, %rdi
	je	.L1718
	movq	%rdi, -96(%rbp)
	movq	%rcx, -80(%rbp)
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1658:
	movq	-416(%rbp), %rcx
	movq	-352(%rbp), %rdx
	movq	(%rcx), %rax
	movq	%rdx, (%rcx)
	movq	%rax, -368(%rbp)
	testq	%rax, %rax
	je	.L1764
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -352(%rbp)
	movq	%rax, -312(%rbp)
	cmpq	%rax, %rcx
	je	.L1765
	.p2align 4,,10
	.p2align 3
.L1783:
	movq	-312(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L1766
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1767
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE(%rip), %rax
	movq	%rax, (%r12)
	movq	88(%r12), %rax
	movq	%rax, -320(%rbp)
	testq	%rax, %rax
	je	.L1768
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	%rcx, -336(%rbp)
	cmpq	%r15, %rcx
	je	.L1769
	movq	%r12, -360(%rbp)
	.p2align 4,,10
	.p2align 3
.L1779:
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L1770
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1771
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1772
	movq	8(%r13), %rbx
	movq	0(%r13), %r14
	cmpq	%r14, %rbx
	jne	.L1777
	jmp	.L1773
	.p2align 4,,10
	.p2align 3
.L1956:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1776
	movq	%r8, -328(%rbp)
	call	_ZdlPv@PLT
	movq	-328(%rbp), %r8
.L1776:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1774:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1955
.L1777:
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L1774
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1956
	addq	$8, %r14
	movq	%r8, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L1777
	.p2align 4,,10
	.p2align 3
.L1955:
	movq	0(%r13), %r14
.L1773:
	testq	%r14, %r14
	je	.L1778
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1778:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1772:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1770:
	addq	$8, %r15
	cmpq	%r15, -336(%rbp)
	jne	.L1779
	movq	-320(%rbp), %rax
	movq	-360(%rbp), %r12
	movq	(%rax), %r15
.L1769:
	testq	%r15, %r15
	je	.L1780
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1780:
	movq	-320(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1768:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1781
	call	_ZdlPv@PLT
.L1781:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1782
	call	_ZdlPv@PLT
.L1782:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1766:
	addq	$8, -312(%rbp)
	movq	-312(%rbp), %rax
	cmpq	%rax, -352(%rbp)
	jne	.L1783
	movq	-368(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -312(%rbp)
.L1765:
	movq	-312(%rbp), %rax
	testq	%rax, %rax
	je	.L1784
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1784:
	movq	-368(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1764:
	movq	-344(%rbp), %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-280(%rbp), %r12
	testq	%r12, %r12
	je	.L1786
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1787
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L1788:
	cmpl	$1, %eax
	jne	.L1786
	movq	(%r12), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1957
.L1790:
	testq	%rbx, %rbx
	je	.L1791
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1792:
	cmpl	$1, %eax
	jne	.L1786
	movq	(%r12), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1793
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1786:
	movq	-440(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1760:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1933:
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L1714
	cmpq	$1, %rax
	je	.L1958
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L1714
	movq	-312(%rbp), %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L1714:
	xorl	%esi, %esi
	movq	%rax, -136(%rbp)
	movw	%si, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1771:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1770
	.p2align 4,,10
	.p2align 3
.L1698:
	call	*%rdx
	jmp	.L1691
	.p2align 4,,10
	.p2align 3
.L1954:
	movq	%rdx, %xmm4
	movq	%rax, -144(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -136(%rbp)
.L1718:
	movq	-312(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -312(%rbp)
	movq	%rax, %rdi
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1767:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1766
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, -144(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -136(%rbp)
	jmp	.L1718
	.p2align 4,,10
	.p2align 3
.L1787:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L1788
	.p2align 4,,10
	.p2align 3
.L1958:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1951:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1762:
	call	*%rdx
	jmp	.L1755
	.p2align 4,,10
	.p2align 3
.L1791:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1792
	.p2align 4,,10
	.p2align 3
.L1957:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1790
.L1793:
	call	*%rdx
	jmp	.L1786
.L1934:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7863:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl15takeTypeProfileEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS5_EESaIS8_EES6_ISA_EE, .-_ZN12v8_inspector19V8ProfilerAgentImpl15takeTypeProfileEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_123buildInspectorObjectForEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"no reason"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123buildInspectorObjectForEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123buildInspectorObjectForEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeE, @function
_ZN12v8_inspector12_GLOBAL__N_123buildInspectorObjectForEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeE:
.LFB7776:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-224(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r8, -272(%rbp)
	movq	%rsi, -248(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$136, %edi
	call	_Znwm@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	88(%rbx), %r9
	movq	%rax, 8(%rbx)
	leaq	64(%rbx), %rax
	leaq	8(%rbx), %r15
	movq	%rax, 48(%rbx)
	leaq	104(%rbx), %rax
	leaq	48(%rbx), %r14
	movw	%cx, 64(%rbx)
	movw	%dx, 24(%rbx)
	movw	%si, 104(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 80(%rbx)
	movq	%rax, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 128(%rbx)
	movq	%r9, -264(%rbp)
	call	_ZNK2v814CpuProfileNode15GetFunctionNameEv@PLT
	leaq	-192(%rbp), %r10
	movq	-248(%rbp), %rsi
	movq	%rax, %rdx
	movq	%r10, %rdi
	movq	%r10, -248(%rbp)
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-248(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-160(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, 40(%rbx)
	call	_ZNK2v814CpuProfileNode11GetScriptIdEv@PLT
	movl	%eax, %esi
	leaq	-144(%rbp), %rax
	movq	%rax, %r15
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	leaq	-96(%rbp), %r14
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, 80(%rbx)
	call	_ZNK2v814CpuProfileNode21GetScriptResourceNameEv@PLT
	movq	-272(%rbp), %r8
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_117resourceNameToUrlEPNS_15V8InspectorImplEN2v85LocalINS3_6StringEEE
	movq	-264(%rbp), %r9
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, 120(%rbx)
	call	_ZNK2v814CpuProfileNode13GetLineNumberEv@PLT
	movq	%r12, %rdi
	subl	$1, %eax
	movl	%eax, 128(%rbx)
	call	_ZNK2v814CpuProfileNode15GetColumnNumberEv@PLT
	movq	-96(%rbp), %rdi
	subl	$1, %eax
	movl	%eax, 132(%rbx)
	leaq	-80(%rbp), %rax
	movq	%rax, -272(%rbp)
	cmpq	%rax, %rdi
	je	.L1960
	call	_ZdlPv@PLT
.L1960:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rdi
	je	.L1961
	call	_ZdlPv@PLT
.L1961:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1962
	call	_ZdlPv@PLT
.L1962:
	movl	$96, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	leaq	64(%r15), %rax
	movb	$0, 24(%r15)
	movl	$0, 28(%r15)
	movq	$0, 32(%r15)
	movb	$0, 40(%r15)
	movq	$0, 80(%r15)
	movq	%rax, 48(%r15)
	movq	$0, 56(%r15)
	movq	$0, 88(%r15)
	movl	$0, 8(%r15)
	movq	%rbx, 16(%r15)
	movups	%xmm0, 64(%r15)
	call	_ZNK2v814CpuProfileNode11GetHitCountEv@PLT
	movb	$1, 24(%r15)
	movq	%r12, %rdi
	movl	%eax, 28(%r15)
	call	_ZNK2v814CpuProfileNode9GetNodeIdEv@PLT
	movq	%r15, 0(%r13)
	movq	%r12, %rdi
	movl	%eax, 8(%r15)
	call	_ZNK2v814CpuProfileNode16GetChildrenCountEv@PLT
	movl	%eax, -248(%rbp)
	testl	%eax, %eax
	jne	.L2018
.L1964:
	movq	%r12, %rdi
	call	_ZNK2v814CpuProfileNode16GetBailoutReasonEv@PLT
	testq	%rax, %rax
	je	.L1972
	cmpb	$0, (%rax)
	jne	.L2019
.L1972:
	movq	%r12, %rdi
	call	_ZNK2v814CpuProfileNode15GetHitLineCountEv@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L2020
.L1976:
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2021
	addq	$248, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2019:
	.cfi_restore_state
	movl	$10, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1972
	movq	-280(%rbp), %rdi
	movq	%rax, %rsi
	movq	0(%r13), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rax
	movq	%r14, %rdi
	movq	-272(%rbp), %r15
	leaq	(%rsi,%rax,2), %rdx
	movq	%r15, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-112(%rbp), %rax
	leaq	48(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 40(%rbx)
	movq	%rax, 80(%rbx)
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1973
	call	_ZdlPv@PLT
.L1973:
	movq	-144(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L1972
	call	_ZdlPv@PLT
	jmp	.L1972
	.p2align 4,,10
	.p2align 3
.L2018:
	movl	$24, %edi
	xorl	%ebx, %ebx
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %r15
	movups	%xmm0, (%rax)
	leaq	-232(%rbp), %rax
	movq	%rax, -288(%rbp)
	movl	-248(%rbp), %eax
	testl	%eax, %eax
	jle	.L1969
	.p2align 4,,10
	.p2align 3
.L1965:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNK2v814CpuProfileNode8GetChildEi@PLT
	movq	%rax, %rdi
	call	_ZNK2v814CpuProfileNode9GetNodeIdEv@PLT
	movq	8(%r15), %rsi
	movl	%eax, -232(%rbp)
	cmpq	16(%r15), %rsi
	je	.L1968
	movl	%eax, (%rsi)
	addl	$1, %ebx
	addq	$4, 8(%r15)
	cmpl	%ebx, -248(%rbp)
	jne	.L1965
.L1969:
	movq	0(%r13), %rax
	movq	32(%rax), %rbx
	movq	%r15, 32(%rax)
	testq	%rbx, %rbx
	je	.L1964
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1971
	call	_ZdlPv@PLT
.L1971:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L1968:
	movq	-288(%rbp), %rdx
	movq	%r15, %rdi
	addl	$1, %ebx
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	cmpl	%ebx, -248(%rbp)
	jne	.L1965
	jmp	.L1969
	.p2align 4,,10
	.p2align 3
.L2020:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	%r15d, %edx
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	salq	$3, %rdx
	movq	%rax, %rbx
	movq	%rdx, %rdi
	movups	%xmm0, (%rax)
	movq	%rdx, -248(%rbp)
	call	_Znwm@PLT
	movq	-248(%rbp), %rdx
	movq	%rax, %r14
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1977:
	movl	$0, (%rax)
	addq	$8, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L1977
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v814CpuProfileNode12GetLineTicksEPNS0_8LineTickEj@PLT
	testb	%al, %al
	je	.L1978
	leal	-1(%r15), %eax
	movq	%r14, %r12
	leaq	8(%r14,%rax,8), %r15
	jmp	.L1983
	.p2align 4,,10
	.p2align 3
.L2022:
	movq	$0, -232(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%rbx)
.L1980:
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1981
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1982
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1981:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1978
.L1983:
	movl	$16, %edi
	call	_Znwm@PLT
	movl	(%r12), %edx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE(%rip), %rcx
	movq	8(%rbx), %rsi
	movq	%rcx, (%rax)
	movl	%edx, 8(%rax)
	movl	4(%r12), %edx
	movq	%rax, -232(%rbp)
	movl	%edx, 12(%rax)
	cmpq	16(%rbx), %rsi
	jne	.L2022
	leaq	-232(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1980
	.p2align 4,,10
	.p2align 3
.L1978:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	0(%r13), %rax
	movq	88(%rax), %r12
	movq	%rbx, 88(%rax)
	testq	%r12, %r12
	je	.L1976
	movq	8(%r12), %rbx
	movq	(%r12), %r15
	cmpq	%r15, %rbx
	je	.L1984
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %r14
	jmp	.L1987
	.p2align 4,,10
	.p2align 3
.L2024:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1985:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L2023
.L1987:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1985
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L2024
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L1987
	.p2align 4,,10
	.p2align 3
.L2023:
	movq	(%r12), %r15
.L1984:
	testq	%r15, %r15
	je	.L1988
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1988:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1976
	.p2align 4,,10
	.p2align 3
.L1982:
	call	*%rax
	jmp	.L1981
.L2021:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7776:
	.size	_ZN12v8_inspector12_GLOBAL__N_123buildInspectorObjectForEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeE, .-_ZN12v8_inspector12_GLOBAL__N_123buildInspectorObjectForEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_116flattenNodesTreeEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeEPSt6vectorISt10unique_ptrINS_8protocol8Profiler11ProfileNodeESt14default_deleteISB_EESaISE_EE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_116flattenNodesTreeEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeEPSt6vectorISt10unique_ptrINS_8protocol8Profiler11ProfileNodeESt14default_deleteISB_EESaISE_EE, @function
_ZN12v8_inspector12_GLOBAL__N_116flattenNodesTreeEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeEPSt6vectorISt10unique_ptrINS_8protocol8Profiler11ProfileNodeESt14default_deleteISB_EESaISE_EE:
.LFB7779:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector12_GLOBAL__N_123buildInspectorObjectForEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeE
	movq	8(%rbx), %rsi
	cmpq	16(%rbx), %rsi
	je	.L2026
	movq	-64(%rbp), %rax
	movq	$0, -64(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%rbx)
.L2027:
	movq	-64(%rbp), %r15
	testq	%r15, %r15
	je	.L2028
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2029
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	movq	88(%r15), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L2030
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -72(%rbp)
	cmpq	%r14, %rcx
	jne	.L2034
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2073:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L2032:
	addq	$8, %r14
	cmpq	%r14, -72(%rbp)
	je	.L2072
.L2034:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2032
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2073
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, -72(%rbp)
	jne	.L2034
	.p2align 4,,10
	.p2align 3
.L2072:
	movq	-80(%rbp), %rax
	movq	(%rax), %r14
.L2031:
	testq	%r14, %r14
	je	.L2035
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2035:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2030:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2036
	call	_ZdlPv@PLT
.L2036:
	movq	32(%r15), %r14
	testq	%r14, %r14
	je	.L2037
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2038
	call	_ZdlPv@PLT
.L2038:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2037:
	movq	16(%r15), %r14
	testq	%r14, %r14
	je	.L2039
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2040
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	leaq	104(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2041
	call	_ZdlPv@PLT
.L2041:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2042
	call	_ZdlPv@PLT
.L2042:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2043
	call	_ZdlPv@PLT
.L2043:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2039:
	movl	$96, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2028:
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZNK2v814CpuProfileNode16GetChildrenCountEv@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jle	.L2025
.L2044:
	movl	%r14d, %esi
	movq	%r12, %rdi
	addl	$1, %r14d
	call	_ZNK2v814CpuProfileNode8GetChildEi@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_116flattenNodesTreeEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeEPSt6vectorISt10unique_ptrINS_8protocol8Profiler11ProfileNodeESt14default_deleteISB_EESaISE_EE
	cmpl	%r14d, %r15d
	jne	.L2044
.L2025:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2074
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2026:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L2027
	.p2align 4,,10
	.p2align 3
.L2029:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2040:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2039
.L2074:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7779:
	.size	_ZN12v8_inspector12_GLOBAL__N_116flattenNodesTreeEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeEPSt6vectorISt10unique_ptrINS_8protocol8Profiler11ProfileNodeESt14default_deleteISB_EESaISE_EE, .-_ZN12v8_inspector12_GLOBAL__N_116flattenNodesTreeEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeEPSt6vectorISt10unique_ptrINS_8protocol8Profiler11ProfileNodeESt14default_deleteISB_EESaISE_EE
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl13stopProfilingERKNS_8String16Eb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl13stopProfilingERKNS_8String16Eb
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl13stopProfilingERKNS_8String16Eb, @function
_ZN12v8_inspector19V8ProfilerAgentImpl13stopProfilingERKNS_8String16Eb:
.LFB7872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%rdi, -104(%rbp)
	movq	16(%rsi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	movq	24(%rbx), %r14
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v811CpuProfiler13StopProfilingENS_5LocalINS_6StringEEE@PLT
	movq	%rax, %r12
	movq	-104(%rbp), %rax
	movq	$0, (%rax)
	testq	%r12, %r12
	je	.L2076
	testb	%r13b, %r13b
	jne	.L2193
.L2077:
	movq	%r12, %rdi
	call	_ZN2v810CpuProfile6DeleteEv@PLT
.L2076:
	subl	$1, 120(%rbx)
	je	.L2194
.L2124:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2195
	movq	-104(%rbp), %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2193:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movl	$24, %edi
	movq	24(%rax), %r14
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, 16(%rax)
	movq	%rax, %r13
	movups	%xmm0, (%rax)
	call	_ZNK2v810CpuProfile14GetTopDownRootEv@PLT
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_116flattenNodesTreeEPNS_15V8InspectorImplEPKN2v814CpuProfileNodeEPSt6vectorISt10unique_ptrINS_8protocol8Profiler11ProfileNodeESt14default_deleteISB_EESaISE_EE
	movl	$48, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, -120(%rbp)
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%r13, 8(%r14)
	xorl	%r13d, %r13d
	movq	%rax, (%r14)
	movups	%xmm0, 32(%r14)
	movups	%xmm1, 16(%r14)
	call	_ZNK2v810CpuProfile12GetStartTimeEv@PLT
	pxor	%xmm1, %xmm1
	movq	%r12, %rdi
	cvtsi2sdq	%rax, %xmm1
	movsd	%xmm1, 16(%r14)
	call	_ZNK2v810CpuProfile10GetEndTimeEv@PLT
	pxor	%xmm1, %xmm1
	movl	$24, %edi
	cvtsi2sdq	%rax, %xmm1
	movsd	%xmm1, 24(%r14)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, 16(%rax)
	movq	%rax, %r14
	movups	%xmm0, (%rax)
	call	_ZNK2v810CpuProfile15GetSamplesCountEv@PLT
	leaq	-84(%rbp), %rdx
	movl	%eax, -112(%rbp)
	movq	%rdx, -128(%rbp)
	testl	%eax, %eax
	jle	.L2082
	.p2align 4,,10
	.p2align 3
.L2078:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNK2v810CpuProfile9GetSampleEi@PLT
	movq	%rax, %rdi
	call	_ZNK2v814CpuProfileNode9GetNodeIdEv@PLT
	movq	8(%r14), %rsi
	movl	%eax, -84(%rbp)
	cmpq	16(%r14), %rsi
	je	.L2081
	movl	%eax, (%rsi)
	addl	$1, %r13d
	addq	$4, 8(%r14)
	cmpl	%r13d, -112(%rbp)
	jne	.L2078
.L2082:
	movq	-120(%rbp), %rax
	movq	32(%rax), %r13
	movq	%r14, 32(%rax)
	testq	%r13, %r13
	je	.L2080
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2084
	call	_ZdlPv@PLT
.L2084:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2080:
	movl	$24, %edi
	xorl	%r13d, %r13d
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, 16(%rax)
	movq	%rax, %r14
	movups	%xmm0, (%rax)
	call	_ZNK2v810CpuProfile15GetSamplesCountEv@PLT
	movq	%r12, %rdi
	movl	%eax, -112(%rbp)
	call	_ZNK2v810CpuProfile12GetStartTimeEv@PLT
	movq	%rax, %rcx
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jle	.L2096
	movq	%rbx, -152(%rbp)
	movq	%r14, %rbx
	movq	%r15, -160(%rbp)
	movl	%r13d, %r15d
	movq	%rcx, %r13
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2197:
	movl	%esi, (%rdx)
	addl	$1, %r15d
	addq	$4, 8(%rbx)
	cmpl	%r15d, -112(%rbp)
	je	.L2196
.L2085:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNK2v810CpuProfile18GetSampleTimestampEi@PLT
	movq	8(%rbx), %rdx
	movq	%rax, %r9
	movq	%r13, %rax
	movl	%r9d, %esi
	movq	%r9, %r13
	subl	%eax, %esi
	cmpq	16(%rbx), %rdx
	jne	.L2197
	movabsq	$2305843009213693951, %rcx
	movq	(%rbx), %r14
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L2198
	testq	%rax, %rax
	je	.L2128
	movabsq	$9223372036854775804, %r10
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L2199
.L2091:
	movq	%r10, %rdi
	movq	%rdx, -144(%rbp)
	movl	%esi, -136(%rbp)
	movq	%r10, -128(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %r10
	movl	-136(%rbp), %esi
	movq	-144(%rbp), %rdx
	movq	%rax, %r9
	addq	%rax, %r10
.L2092:
	leaq	4(%r9,%rdx), %rax
	movl	%esi, (%r9,%rdx)
	movq	%rax, -128(%rbp)
	testq	%rdx, %rdx
	jg	.L2200
	testq	%r14, %r14
	jne	.L2094
.L2095:
	movq	%r9, %xmm0
	movq	%r10, 16(%rbx)
	addl	$1, %r15d
	movhps	-128(%rbp), %xmm0
	movups	%xmm0, (%rbx)
	cmpl	%r15d, -112(%rbp)
	jne	.L2085
	.p2align 4,,10
	.p2align 3
.L2196:
	movq	%rbx, %r14
	movq	-160(%rbp), %r15
	movq	-152(%rbp), %rbx
.L2096:
	movq	-120(%rbp), %rax
	movq	40(%rax), %r13
	movq	%r14, 40(%rax)
	testq	%r13, %r13
	je	.L2087
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2097
	call	_ZdlPv@PLT
.L2097:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2087:
	movq	-104(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	(%rax), %rcx
	movq	%rdx, (%rax)
	movq	%rcx, -112(%rbp)
	testq	%rcx, %rcx
	je	.L2077
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2099
	movq	40(%rcx), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%r13, %r13
	je	.L2100
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2101
	call	_ZdlPv@PLT
.L2101:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2100:
	movq	-112(%rbp), %rax
	movq	32(%rax), %r13
	testq	%r13, %r13
	je	.L2102
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2103
	call	_ZdlPv@PLT
.L2103:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2102:
	movq	-112(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L2104
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2105
	movq	%r12, -136(%rbp)
	movq	%r15, -152(%rbp)
	movq	%rbx, -144(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2122:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2106
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2107
	movq	88(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L2108
	movq	8(%r13), %r12
	movq	0(%r13), %r14
	cmpq	%r14, %r12
	jne	.L2112
	jmp	.L2109
	.p2align 4,,10
	.p2align 3
.L2202:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L2110:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L2201
.L2112:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2110
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2202
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, %r12
	jne	.L2112
	.p2align 4,,10
	.p2align 3
.L2201:
	movq	0(%r13), %r14
.L2109:
	testq	%r14, %r14
	je	.L2113
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2113:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2108:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2114
	call	_ZdlPv@PLT
.L2114:
	movq	32(%r15), %r12
	testq	%r12, %r12
	je	.L2115
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2116
	call	_ZdlPv@PLT
.L2116:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2115:
	movq	16(%r15), %r12
	testq	%r12, %r12
	je	.L2117
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2118
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2119
	call	_ZdlPv@PLT
.L2119:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2120
	call	_ZdlPv@PLT
.L2120:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2121
	call	_ZdlPv@PLT
.L2121:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2117:
	movl	$96, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2106:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L2122
	movq	-128(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r15
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2105:
	testq	%rdi, %rdi
	je	.L2123
	call	_ZdlPv@PLT
.L2123:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2104:
	movq	-112(%rbp), %rdi
	movl	$48, %esi
	call	_ZdlPvm@PLT
	jmp	.L2077
	.p2align 4,,10
	.p2align 3
.L2194:
	movq	24(%rbx), %rdi
	call	_ZN2v811CpuProfiler7DisposeEv@PLT
	movq	$0, 24(%rbx)
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2081:
	movq	-128(%rbp), %rdx
	movq	%r14, %rdi
	addl	$1, %r13d
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	cmpl	%r13d, -112(%rbp)
	jne	.L2078
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2200:
	movq	%r9, %rdi
	movq	%r14, %rsi
	movq	%r10, -136(%rbp)
	call	memmove@PLT
	movq	-136(%rbp), %r10
	movq	%rax, %r9
.L2094:
	movq	%r14, %rdi
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %r9
	jmp	.L2095
	.p2align 4,,10
	.p2align 3
.L2199:
	testq	%rdi, %rdi
	jne	.L2203
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2128:
	movl	$4, %r10d
	jmp	.L2091
	.p2align 4,,10
	.p2align 3
.L2107:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2106
	.p2align 4,,10
	.p2align 3
.L2118:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L2099:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2077
.L2195:
	call	__stack_chk_fail@PLT
.L2198:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2203:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rdi
	movq	%rax, %r10
	cmovbe	%rdi, %r10
	salq	$2, %r10
	jmp	.L2091
	.cfi_endproc
.LFE7872:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl13stopProfilingERKNS_8String16Eb, .-_ZN12v8_inspector19V8ProfilerAgentImpl13stopProfilingERKNS_8String16Eb
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl7disableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl7disableEv
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl7disableEv, @function
_ZN12v8_inspector19V8ProfilerAgentImpl7disableEv:
.LFB7810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -168(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpb	$0, 48(%rsi)
	je	.L2294
	movq	64(%rsi), %r12
	movq	56(%rsi), %rbx
	movabsq	$-3689348814741910323, %rdx
	movq	%r12, %rax
	subq	%rbx, %rax
	sarq	$4, %rax
	imulq	%rdx, %rax
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2207
	leaq	-120(%rbp), %rax
	movq	%rax, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L2234:
	subq	$1, %r15
	movq	-136(%rbp), %rsi
	movq	-160(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	(%r15,%r15,4), %rdx
	salq	$4, %rdx
	addq	%rbx, %rdx
	call	_ZN12v8_inspector19V8ProfilerAgentImpl13stopProfilingERKNS_8String16Eb
	movq	-120(%rbp), %r14
	testq	%r14, %r14
	je	.L2208
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2209
	movq	40(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L2210
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2211
	call	_ZdlPv@PLT
.L2211:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2210:
	movq	32(%r14), %r12
	testq	%r12, %r12
	je	.L2212
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2213
	call	_ZdlPv@PLT
.L2213:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2212:
	movq	8(%r14), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2214
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	%rcx, -144(%rbp)
	cmpq	%r13, %rcx
	je	.L2215
	movq	%r15, -176(%rbp)
	movq	%r14, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L2232:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L2216
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2217
	movq	88(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L2218
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	jne	.L2222
	jmp	.L2219
	.p2align 4,,10
	.p2align 3
.L2296:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L2220:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2295
.L2222:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2220
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2296
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, %rbx
	jne	.L2222
	.p2align 4,,10
	.p2align 3
.L2295:
	movq	(%r15), %r14
.L2219:
	testq	%r14, %r14
	je	.L2223
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2223:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2218:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2224
	call	_ZdlPv@PLT
.L2224:
	movq	32(%r12), %rbx
	testq	%rbx, %rbx
	je	.L2225
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2226
	call	_ZdlPv@PLT
.L2226:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2225:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	je	.L2227
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2228
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2229
	call	_ZdlPv@PLT
.L2229:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2230
	call	_ZdlPv@PLT
.L2230:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2231
	call	_ZdlPv@PLT
.L2231:
	movl	$136, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2227:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2216:
	addq	$8, %r13
	cmpq	%r13, -144(%rbp)
	jne	.L2232
.L2298:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r15
	movq	-184(%rbp), %r14
	movq	(%rax), %r13
.L2215:
	testq	%r13, %r13
	je	.L2233
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2233:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2214:
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2208:
	movq	-136(%rbp), %rax
	movq	56(%rax), %rbx
	testq	%r15, %r15
	jne	.L2234
	movq	64(%rax), %r12
.L2207:
	cmpq	%rbx, %r12
	je	.L2235
	movq	%rbx, %r13
	.p2align 4,,10
	.p2align 3
.L2240:
	movq	40(%r13), %rdi
	leaq	56(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2236
	call	_ZdlPv@PLT
.L2236:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2237
	call	_ZdlPv@PLT
	addq	$80, %r13
	cmpq	%r12, %r13
	jne	.L2240
	movq	-136(%rbp), %rax
	movq	%rbx, 64(%rax)
.L2235:
	movq	-136(%rbp), %rsi
	leaq	-112(%rbp), %r12
	xorl	%edx, %edx
	leaq	-88(%rbp), %rbx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*72(%rax)
	movq	-104(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2241
	call	_ZdlPv@PLT
.L2241:
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*80(%rax)
	movq	-104(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2242
	call	_ZdlPv@PLT
.L2242:
	movq	-136(%rbp), %rax
	leaq	_ZN12v8_inspector18ProfilerAgentStateL15profilerEnabledE(%rip), %rsi
	movq	%r12, %rdi
	movb	$0, 48(%rax)
	movq	32(%rax), %r13
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2243
	call	_ZdlPv@PLT
.L2243:
	movq	-168(%rbp), %rdi
.L2294:
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2297
	movq	-168(%rbp), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2217:
	.cfi_restore_state
	movq	%r12, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -144(%rbp)
	jne	.L2232
	jmp	.L2298
	.p2align 4,,10
	.p2align 3
.L2228:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2227
	.p2align 4,,10
	.p2align 3
.L2209:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2208
	.p2align 4,,10
	.p2align 3
.L2237:
	addq	$80, %r13
	cmpq	%r12, %r13
	jne	.L2240
	movq	-136(%rbp), %rax
	movq	%rbx, 64(%rax)
	jmp	.L2235
.L2297:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7810:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl7disableEv, .-_ZN12v8_inspector19V8ProfilerAgentImpl7disableEv
	.section	.rodata._ZN12v8_inspector19V8ProfilerAgentImpl4stopEPSt10unique_ptrINS_8protocol8Profiler7ProfileESt14default_deleteIS4_EE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"No recording profiles found"
.LC9:
	.string	"Profile is not found"
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl4stopEPSt10unique_ptrINS_8protocol8Profiler7ProfileESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl4stopEPSt10unique_ptrINS_8protocol8Profiler7ProfileESt14default_deleteIS4_EE
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl4stopEPSt10unique_ptrINS_8protocol8Profiler7ProfileESt14default_deleteIS4_EE, @function
_ZN12v8_inspector19V8ProfilerAgentImpl4stopEPSt10unique_ptrINS_8protocol8Profiler7ProfileESt14default_deleteIS4_EE:
.LFB7818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 49(%rsi)
	jne	.L2300
	leaq	-96(%rbp), %r12
	movq	%rdi, %r14
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2299
	call	_ZdlPv@PLT
.L2299:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2452
	movq	-120(%rbp), %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2300:
	.cfi_restore_state
	movb	$0, 49(%rsi)
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	movq	%rdx, %r15
	setne	%cl
	leaq	-104(%rbp), %rdi
	leaq	80(%rsi), %rdx
	movq	%rsi, %rbx
	call	_ZN12v8_inspector19V8ProfilerAgentImpl13stopProfilingERKNS_8String16Eb
	testq	%r15, %r15
	je	.L2303
	movq	-104(%rbp), %rax
	movq	(%r15), %rcx
	movq	$0, -104(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rax, (%r15)
	testq	%rcx, %rcx
	je	.L2304
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2305
	movq	40(%rcx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%r12, %r12
	je	.L2306
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2307
	call	_ZdlPv@PLT
.L2307:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2306:
	movq	-128(%rbp), %rax
	movq	32(%rax), %r12
	testq	%r12, %r12
	je	.L2308
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2309
	call	_ZdlPv@PLT
.L2309:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2308:
	movq	-128(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L2310
	movq	8(%rax), %rdx
	movq	(%rax), %r12
	movq	%rdx, -136(%rbp)
	cmpq	%r12, %rdx
	je	.L2311
	movq	%rbx, -152(%rbp)
	movq	%r15, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L2328:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L2312
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L2313
	movq	88(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L2314
	movq	8(%r13), %rbx
	movq	0(%r13), %r14
	cmpq	%r14, %rbx
	jne	.L2318
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2454:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L2316:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2453
.L2318:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2316
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2454
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, %rbx
	jne	.L2318
	.p2align 4,,10
	.p2align 3
.L2453:
	movq	0(%r13), %r14
.L2315:
	testq	%r14, %r14
	je	.L2319
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2319:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2314:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2320
	call	_ZdlPv@PLT
.L2320:
	movq	32(%r15), %r13
	testq	%r13, %r13
	je	.L2321
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2322
	call	_ZdlPv@PLT
.L2322:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2321:
	movq	16(%r15), %r13
	testq	%r13, %r13
	je	.L2323
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2324
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2325
	call	_ZdlPv@PLT
.L2325:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2326
	call	_ZdlPv@PLT
.L2326:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2327
	call	_ZdlPv@PLT
.L2327:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2323:
	movl	$96, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2312:
	addq	$8, %r12
	cmpq	%r12, -136(%rbp)
	jne	.L2328
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rbx
	movq	-160(%rbp), %r15
	movq	(%rax), %r12
.L2311:
	testq	%r12, %r12
	je	.L2329
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2329:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2310:
	movq	-128(%rbp), %rdi
	movl	$48, %esi
	call	_ZdlPvm@PLT
	movq	(%r15), %rax
.L2304:
	testq	%rax, %rax
	je	.L2455
.L2303:
	xorl	%eax, %eax
	leaq	-80(%rbp), %r12
	xorl	%edx, %edx
	movq	$0, -64(%rbp)
	movw	%ax, -80(%rbp)
	movq	80(%rbx), %rax
	xorl	%ecx, %ecx
	movq	%r12, -96(%rbp)
	movq	$0, 88(%rbx)
	movw	%dx, (%rax)
	movq	-96(%rbp), %rax
	movq	$0, -88(%rbp)
	movw	%cx, (%rax)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 112(%rbx)
	cmpq	%r12, %rdi
	je	.L2332
	call	_ZdlPv@PLT
.L2332:
	movq	32(%rbx), %r14
	leaq	-96(%rbp), %r13
	leaq	_ZN12v8_inspector18ProfilerAgentStateL22userInitiatedProfilingE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L2333
	call	_ZdlPv@PLT
.L2333:
	movq	-120(%rbp), %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L2331:
	movq	-104(%rbp), %r13
	testq	%r13, %r13
	je	.L2299
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2334
	movq	40(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L2335
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2336
	call	_ZdlPv@PLT
.L2336:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2335:
	movq	32(%r13), %r12
	testq	%r12, %r12
	je	.L2337
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2338
	call	_ZdlPv@PLT
.L2338:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2337:
	movq	8(%r13), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L2339
	movq	8(%rax), %rdx
	movq	(%rax), %r12
	movq	%rdx, -128(%rbp)
	cmpq	%r12, %rdx
	je	.L2340
	movq	%r13, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L2357:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2341
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2342
	movq	88(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2343
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	jne	.L2347
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2457:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L2345:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L2456
.L2347:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2345
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2457
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L2347
	.p2align 4,,10
	.p2align 3
.L2456:
	movq	(%r14), %r15
.L2344:
	testq	%r15, %r15
	je	.L2348
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2348:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2343:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2349
	call	_ZdlPv@PLT
.L2349:
	movq	32(%r13), %r14
	testq	%r14, %r14
	je	.L2350
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2351
	call	_ZdlPv@PLT
.L2351:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2350:
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L2352
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2353
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	leaq	104(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2354
	call	_ZdlPv@PLT
.L2354:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2355
	call	_ZdlPv@PLT
.L2355:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2356
	call	_ZdlPv@PLT
.L2356:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2352:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2341:
	addq	$8, %r12
	cmpq	%r12, -128(%rbp)
	jne	.L2357
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %r13
	movq	(%rax), %r12
.L2340:
	testq	%r12, %r12
	je	.L2358
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2358:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2339:
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2299
	.p2align 4,,10
	.p2align 3
.L2342:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2341
	.p2align 4,,10
	.p2align 3
.L2313:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2312
	.p2align 4,,10
	.p2align 3
.L2353:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2352
	.p2align 4,,10
	.p2align 3
.L2324:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2323
	.p2align 4,,10
	.p2align 3
.L2455:
	leaq	-96(%rbp), %r12
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2331
	call	_ZdlPv@PLT
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2334:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2299
	.p2align 4,,10
	.p2align 3
.L2305:
	movq	%rcx, %rdi
	call	*%rax
	movq	(%r15), %rax
	jmp	.L2304
.L2452:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7818:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl4stopEPSt10unique_ptrINS_8protocol8Profiler7ProfileESt14default_deleteIS4_EE, .-_ZN12v8_inspector19V8ProfilerAgentImpl4stopEPSt10unique_ptrINS_8protocol8Profiler7ProfileESt14default_deleteIS4_EE
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl17consoleProfileEndERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl17consoleProfileEndERKNS_8String16E
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl17consoleProfileEndERKNS_8String16E, @function
_ZN12v8_inspector19V8ProfilerAgentImpl17consoleProfileEndERKNS_8String16E:
.LFB7808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 48(%rdi)
	je	.L2458
	movq	%rdi, %r13
	leaq	-240(%rbp), %rax
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	movq	8(%rsi), %rdi
	movq	%rax, -296(%rbp)
	leaq	-208(%rbp), %r15
	movq	%rsi, %r14
	movq	%rax, -256(%rbp)
	movq	64(%r13), %rbx
	leaq	-192(%rbp), %rax
	leaq	-256(%rbp), %r12
	movq	$0, -248(%rbp)
	movq	56(%r13), %r9
	movw	%r10w, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	%r15, -312(%rbp)
	movq	%rax, -304(%rbp)
	movq	%rax, -208(%rbp)
	movq	$0, -200(%rbp)
	movw	%r11w, -192(%rbp)
	movq	$0, -176(%rbp)
	testq	%rdi, %rdi
	je	.L2683
	movabsq	$-3689348814741910323, %rax
	subq	%r9, %rbx
	movq	%rbx, %r10
	sarq	$4, %r10
	imulq	%rax, %r10
	testq	%r10, %r10
	je	.L2679
	movq	(%rsi), %rsi
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L2487:
	movq	48(%r9,%rbx), %rdx
	movq	40(%r9,%rbx), %r11
	cmpq	%rdx, %rdi
	movq	%rdx, %rcx
	cmovbe	%rdi, %rcx
	testq	%rcx, %rcx
	je	.L2467
	xorl	%eax, %eax
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L2684:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L2467
.L2469:
	movzwl	(%rsi,%rax,2), %r15d
	cmpw	%r15w, (%r11,%rax,2)
	je	.L2684
.L2468:
	addq	$1, %r8
	addq	$80, %rbx
	cmpq	%r10, %r8
	jne	.L2487
.L2679:
	movq	-256(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L2458
	call	_ZdlPv@PLT
.L2458:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2685
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2683:
	.cfi_restore_state
	cmpq	%rbx, %r9
	je	.L2458
	leaq	-80(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-48(%rbx), %rax
	movq	64(%r13), %rbx
	movq	%r15, %rdi
	leaq	-40(%rbx), %rsi
	movq	%rax, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-8(%rbx), %rax
	movq	64(%r13), %rbx
	movq	%rax, -176(%rbp)
	leaq	-80(%rbx), %rax
	movq	%rax, 64(%r13)
	movq	-40(%rbx), %rdi
	leaq	-24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2463
	call	_ZdlPv@PLT
.L2463:
	movq	-80(%rbx), %rdi
	subq	$64, %rbx
	cmpq	%rbx, %rdi
	je	.L2465
	call	_ZdlPv@PLT
.L2465:
	leaq	-288(%rbp), %rdi
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector19V8ProfilerAgentImpl13stopProfilingERKNS_8String16Eb
	cmpq	$0, -288(%rbp)
	je	.L2486
	movq	8(%r13), %rax
	leaq	-280(%rbp), %rdi
	leaq	-144(%rbp), %rbx
	addq	$40, %r13
	movq	24(%rax), %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_120currentDebugLocationEPNS_15V8InspectorImplE
	movq	-208(%rbp), %rsi
	movq	-200(%rbp), %rax
	leaq	-160(%rbp), %rdi
	movq	%rbx, -312(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	movq	%rbx, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-176(%rbp), %rax
	movq	-160(%rbp), %rdx
	leaq	-88(%rbp), %rsi
	movb	$1, -112(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rsi, -328(%rbp)
	movq	%rsi, -104(%rbp)
	cmpq	%rbx, %rdx
	je	.L2686
	movq	%rdx, -104(%rbp)
	movq	-144(%rbp), %rdx
	movq	%rdx, -88(%rbp)
.L2491:
	movq	%rax, -72(%rbp)
	movq	%r12, %rsi
	leaq	-112(%rbp), %r8
	movq	%r13, %rdi
	movq	-152(%rbp), %rdx
	movq	-288(%rbp), %rax
	movq	$0, -152(%rbp)
	movq	-312(%rbp), %rcx
	movq	$0, -288(%rbp)
	movq	%rdx, -96(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movq	-280(%rbp), %rax
	movq	%rcx, -160(%rbp)
	leaq	-264(%rbp), %rcx
	movw	%dx, -144(%rbp)
	leaq	-272(%rbp), %rdx
	movq	$0, -280(%rbp)
	movq	%rax, -272(%rbp)
	call	_ZN12v8_inspector8protocol8Profiler8Frontend22consoleProfileFinishedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EES6_INS1_7ProfileES9_ISC_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE@PLT
	movq	-272(%rbp), %r12
	testq	%r12, %r12
	je	.L2492
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2493
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2494
	call	_ZdlPv@PLT
.L2494:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2492:
	movq	-264(%rbp), %r13
	testq	%r13, %r13
	je	.L2495
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2496
	movq	40(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L2497
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2498
	call	_ZdlPv@PLT
.L2498:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2497:
	movq	32(%r13), %r12
	testq	%r12, %r12
	je	.L2499
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2500
	call	_ZdlPv@PLT
.L2500:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2499:
	movq	8(%r13), %rax
	movq	%rax, -336(%rbp)
	testq	%rax, %rax
	je	.L2501
	movq	8(%rax), %rcx
	movq	(%rax), %r12
	movq	%rcx, -320(%rbp)
	cmpq	%r12, %rcx
	je	.L2502
	movq	%r13, -344(%rbp)
	.p2align 4,,10
	.p2align 3
.L2519:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2503
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2504
	movq	88(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2505
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	jne	.L2509
	jmp	.L2506
	.p2align 4,,10
	.p2align 3
.L2688:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L2507:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L2687
.L2509:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2507
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2688
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L2509
	.p2align 4,,10
	.p2align 3
.L2687:
	movq	(%r14), %r15
.L2506:
	testq	%r15, %r15
	je	.L2510
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2510:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2505:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2511
	call	_ZdlPv@PLT
.L2511:
	movq	32(%r13), %r14
	testq	%r14, %r14
	je	.L2512
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2513
	call	_ZdlPv@PLT
.L2513:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2512:
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L2514
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2515
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	leaq	104(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2516
	call	_ZdlPv@PLT
.L2516:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2517
	call	_ZdlPv@PLT
.L2517:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2518
	call	_ZdlPv@PLT
.L2518:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2514:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2503:
	addq	$8, %r12
	cmpq	%r12, -320(%rbp)
	jne	.L2519
	movq	-336(%rbp), %rax
	movq	-344(%rbp), %r13
	movq	(%rax), %r12
.L2502:
	testq	%r12, %r12
	je	.L2520
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2520:
	movq	-336(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2501:
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2495:
	movq	-104(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L2521
	call	_ZdlPv@PLT
.L2521:
	movq	-160(%rbp), %rdi
	cmpq	-312(%rbp), %rdi
	je	.L2522
	call	_ZdlPv@PLT
.L2522:
	movq	-280(%rbp), %r12
	testq	%r12, %r12
	je	.L2523
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2524
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2525
	call	_ZdlPv@PLT
.L2525:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2523:
	movq	-288(%rbp), %r13
	testq	%r13, %r13
	je	.L2486
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2527
	movq	40(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L2528
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2529
	call	_ZdlPv@PLT
.L2529:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2528:
	movq	32(%r13), %r12
	testq	%r12, %r12
	je	.L2530
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2531
	call	_ZdlPv@PLT
.L2531:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2530:
	movq	8(%r13), %rax
	movq	%rax, -320(%rbp)
	testq	%rax, %rax
	je	.L2532
	movq	8(%rax), %rcx
	movq	(%rax), %r12
	movq	%rcx, -312(%rbp)
	cmpq	%r12, %rcx
	je	.L2533
	movq	%r13, -328(%rbp)
	.p2align 4,,10
	.p2align 3
.L2550:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2534
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2535
	movq	88(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2536
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	jne	.L2540
	jmp	.L2537
	.p2align 4,,10
	.p2align 3
.L2690:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L2538:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L2689
.L2540:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2538
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2690
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L2540
	.p2align 4,,10
	.p2align 3
.L2689:
	movq	(%r14), %r15
.L2537:
	testq	%r15, %r15
	je	.L2541
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2541:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2536:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2542
	call	_ZdlPv@PLT
.L2542:
	movq	32(%r13), %r14
	testq	%r14, %r14
	je	.L2543
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2544
	call	_ZdlPv@PLT
.L2544:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2543:
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L2545
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2546
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	leaq	104(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2547
	call	_ZdlPv@PLT
.L2547:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2548
	call	_ZdlPv@PLT
.L2548:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2549
	call	_ZdlPv@PLT
.L2549:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2545:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2534:
	addq	$8, %r12
	cmpq	%r12, -312(%rbp)
	jne	.L2550
	movq	-320(%rbp), %rax
	movq	-328(%rbp), %r13
	movq	(%rax), %r12
.L2533:
	testq	%r12, %r12
	je	.L2551
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2551:
	movq	-320(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2532:
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2486:
	movq	-208(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L2679
	call	_ZdlPv@PLT
	jmp	.L2679
	.p2align 4,,10
	.p2align 3
.L2467:
	subq	%rdi, %rdx
	movl	$2147483648, %eax
	cmpq	%rax, %rdx
	jge	.L2468
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rdx
	jle	.L2468
	testl	%edx, %edx
	jne	.L2468
	movq	-312(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r14), %rax
	movq	56(%r13), %r14
	movq	%r12, %rdi
	addq	%rbx, %r14
	movq	%rax, -176(%rbp)
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r14), %rax
	addq	56(%r13), %rbx
	leaq	80(%rbx), %r14
	movq	%rax, -224(%rbp)
	movq	64(%r13), %rax
	cmpq	%r14, %rax
	je	.L2470
	movabsq	$-3689348814741910323, %rcx
	movq	%rax, %rdx
	subq	%r14, %rdx
	movq	%rdx, %r14
	sarq	$4, %r14
	imulq	%rcx, %r14
	testq	%rdx, %rdx
	jle	.L2557
	addq	$96, %rbx
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L2471:
	leaq	-80(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L2691
	movq	%rax, -96(%rbx)
	movq	-8(%rbx), %rax
	movq	-80(%rbx), %rdx
	movq	%rax, -88(%rbx)
	movq	(%rbx), %rax
	movq	%rax, -80(%rbx)
	testq	%rdi, %rdi
	je	.L2476
	movq	%rdi, -16(%rbx)
	movq	%rdx, (%rbx)
.L2474:
	xorl	%r8d, %r8d
	movq	$0, -8(%rbx)
	movw	%r8w, (%rdi)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rsi
	movq	-56(%rbx), %rdi
	movq	%rax, -64(%rbx)
	leaq	40(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L2692
	leaq	-40(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L2693
	movq	32(%rbx), %rcx
	movq	-40(%rbx), %rdx
	movq	%rsi, -56(%rbx)
	movq	%rcx, -48(%rbx)
	movq	40(%rbx), %rcx
	movq	%rcx, -40(%rbx)
	testq	%rdi, %rdi
	je	.L2482
	movq	%rdi, 24(%rbx)
	movq	%rdx, 40(%rbx)
.L2480:
	xorl	%ecx, %ecx
	movq	$0, 32(%rbx)
	addq	$80, %rbx
	movw	%cx, (%rdi)
	movq	-24(%rbx), %rax
	movq	%rax, -104(%rbx)
	subq	$1, %r14
	je	.L2694
.L2483:
	movq	-16(%rbx), %rax
	movq	-96(%rbx), %rdi
	cmpq	%rbx, %rax
	jne	.L2471
	movq	-8(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L2472
	cmpq	$1, %rax
	je	.L2695
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L2472
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-8(%rbx), %rax
	movq	-96(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L2472:
	xorl	%r9d, %r9d
	movq	%rax, -88(%rbx)
	movw	%r9w, (%rdi,%rdx)
	movq	-16(%rbx), %rdi
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L2692:
	movq	32(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L2478
	cmpq	$1, %rax
	je	.L2696
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L2478
	call	memmove@PLT
	movq	32(%rbx), %rax
	movq	-56(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L2478:
	xorl	%esi, %esi
	movq	%rax, -48(%rbx)
	movw	%si, (%rdi,%rdx)
	movq	24(%rbx), %rdi
	jmp	.L2480
	.p2align 4,,10
	.p2align 3
.L2693:
	movq	32(%rbx), %rdx
	movq	%rsi, -56(%rbx)
	movq	%rdx, -48(%rbx)
	movq	40(%rbx), %rdx
	movq	%rdx, -40(%rbx)
.L2482:
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
	jmp	.L2480
	.p2align 4,,10
	.p2align 3
.L2691:
	movq	%rax, -96(%rbx)
	movq	-8(%rbx), %rax
	movq	%rax, -88(%rbx)
	movq	(%rbx), %rax
	movq	%rax, -80(%rbx)
.L2476:
	movq	%rbx, -16(%rbx)
	movq	%rbx, %rdi
	jmp	.L2474
.L2557:
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L2470:
	leaq	-80(%r14), %rax
	movq	%rax, 64(%r13)
	movq	-40(%r14), %rdi
	leaq	-24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2484
	call	_ZdlPv@PLT
.L2484:
	movq	-80(%r14), %rdi
	subq	$64, %r14
	cmpq	%r14, %rdi
	je	.L2485
	call	_ZdlPv@PLT
.L2485:
	cmpq	$0, -248(%rbp)
	jne	.L2465
	jmp	.L2486
	.p2align 4,,10
	.p2align 3
.L2504:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2535:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2534
	.p2align 4,,10
	.p2align 3
.L2694:
	movq	64(%r13), %r14
	jmp	.L2470
	.p2align 4,,10
	.p2align 3
.L2686:
	movdqa	-144(%rbp), %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L2491
	.p2align 4,,10
	.p2align 3
.L2546:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2545
	.p2align 4,,10
	.p2align 3
.L2515:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2514
	.p2align 4,,10
	.p2align 3
.L2496:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2524:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2523
	.p2align 4,,10
	.p2align 3
.L2493:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L2527:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2486
	.p2align 4,,10
	.p2align 3
.L2695:
	movzwl	(%rbx), %eax
	movw	%ax, (%rdi)
	movq	-8(%rbx), %rax
	movq	-96(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2696:
	movzwl	40(%rbx), %eax
	movw	%ax, (%rdi)
	movq	32(%rbx), %rax
	movq	-56(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L2478
.L2685:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7808:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl17consoleProfileEndERKNS_8String16E, .-_ZN12v8_inspector19V8ProfilerAgentImpl17consoleProfileEndERKNS_8String16E
	.section	.text._ZN12v8_inspector19V8ProfilerAgentImpl14consoleProfileERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8ProfilerAgentImpl14consoleProfileERKNS_8String16E
	.type	_ZN12v8_inspector19V8ProfilerAgentImpl14consoleProfileERKNS_8String16E, @function
_ZN12v8_inspector19V8ProfilerAgentImpl14consoleProfileERKNS_8String16E:
.LFB7804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 48(%rdi)
	jne	.L2721
.L2697:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2722
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2721:
	.cfi_restore_state
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$1, %esi
	lock xaddl	%esi, _ZN12v8_inspector12_GLOBAL__N_115s_lastProfileIdE(%rip)
	leaq	-240(%rbp), %r12
	addl	$1, %esi
	leaq	-144(%rbp), %r13
	movq	%r12, %rdi
	leaq	-128(%rbp), %rbx
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	-240(%rbp), %rsi
	movq	%r13, %rdi
	movq	-232(%rbp), %rdx
	movq	%rbx, -144(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	(%r14), %rsi
	leaq	-88(%rbp), %rax
	leaq	-104(%rbp), %rdi
	movq	-208(%rbp), %rdx
	movq	%rax, -264(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rdx, -112(%rbp)
	movq	8(%r14), %rdx
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%r14), %rdx
	movq	64(%r15), %rsi
	movq	%rdx, -72(%rbp)
	cmpq	72(%r15), %rsi
	je	.L2699
	leaq	16(%rsi), %rdx
	movq	%rdx, (%rsi)
	movq	-144(%rbp), %rdx
	cmpq	%rbx, %rdx
	je	.L2723
	movq	%rdx, (%rsi)
	movq	-128(%rbp), %rdx
	movq	%rdx, 16(%rsi)
.L2701:
	movq	-136(%rbp), %rdx
	movq	%rdx, 8(%rsi)
	xorl	%edx, %edx
	movw	%dx, -128(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rbx, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	%rdx, 32(%rsi)
	leaq	56(%rsi), %rdx
	movq	%rdx, 40(%rsi)
	movq	-104(%rbp), %rdx
	cmpq	-264(%rbp), %rdx
	je	.L2724
	movq	%rdx, 40(%rsi)
	movq	-88(%rbp), %rdx
	movq	%rdx, 56(%rsi)
.L2703:
	movq	-96(%rbp), %rdx
	movq	%rdx, 48(%rsi)
	movq	-72(%rbp), %rdx
	movq	%rdx, 72(%rsi)
	addq	$80, 64(%r15)
.L2704:
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %rbx
	call	_ZN12v8_inspector19V8ProfilerAgentImpl14startProfilingERKNS_8String16E
	movq	(%r14), %rsi
	movq	8(%r14), %rdx
	leaq	40(%r15), %rax
	leaq	-192(%rbp), %rdi
	movq	%rax, -264(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rbx, -192(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%r14), %rdx
	leaq	-120(%rbp), %r14
	movq	-192(%rbp), %rcx
	movb	$1, -144(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%r14, -136(%rbp)
	cmpq	%rbx, %rcx
	je	.L2725
	movq	%rcx, -136(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -120(%rbp)
.L2708:
	xorl	%eax, %eax
	movq	-184(%rbp), %rcx
	movq	%rdx, -104(%rbp)
	leaq	-248(%rbp), %rdx
	movw	%ax, -176(%rbp)
	movq	8(%r15), %rax
	movq	%rdx, %rdi
	movq	%rcx, -128(%rbp)
	movq	24(%rax), %rsi
	movq	%rdx, -272(%rbp)
	movq	%rbx, -192(%rbp)
	movq	$0, -184(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_120currentDebugLocationEPNS_15V8InspectorImplE
	movq	-272(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	-264(%rbp), %rdi
	call	_ZN12v8_inspector8protocol8Profiler8Frontend21consoleProfileStartedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE@PLT
	movq	-248(%rbp), %r12
	testq	%r12, %r12
	je	.L2709
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2710
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2711
	call	_ZdlPv@PLT
.L2711:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2709:
	movq	-136(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2712
	call	_ZdlPv@PLT
.L2712:
	movq	-192(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2713
	call	_ZdlPv@PLT
.L2713:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2697
	call	_ZdlPv@PLT
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2699:
	leaq	56(%r15), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN12v8_inspector19V8ProfilerAgentImpl17ProfileDescriptorESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	-104(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L2720
	call	_ZdlPv@PLT
.L2720:
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2704
	call	_ZdlPv@PLT
	jmp	.L2704
	.p2align 4,,10
	.p2align 3
.L2725:
	movdqa	-176(%rbp), %xmm0
	movups	%xmm0, -120(%rbp)
	jmp	.L2708
	.p2align 4,,10
	.p2align 3
.L2724:
	movdqu	-88(%rbp), %xmm2
	movups	%xmm2, 56(%rsi)
	jmp	.L2703
	.p2align 4,,10
	.p2align 3
.L2723:
	movdqa	-128(%rbp), %xmm1
	movups	%xmm1, 16(%rsi)
	jmp	.L2701
	.p2align 4,,10
	.p2align 3
.L2710:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2709
.L2722:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7804:
	.size	_ZN12v8_inspector19V8ProfilerAgentImpl14consoleProfileERKNS_8String16E, .-_ZN12v8_inspector19V8ProfilerAgentImpl14consoleProfileERKNS_8String16E
	.weak	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime9CallFrameE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime9CallFrameE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, 48
_ZTVN12v8_inspector8protocol7Runtime9CallFrameE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger8LocationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger8LocationE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger8LocationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger8LocationE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger8LocationE, 48
_ZTVN12v8_inspector8protocol8Debugger8LocationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger8LocationD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE, 48
_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler7ProfileE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler7ProfileE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler7ProfileE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler7ProfileE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler7ProfileE, 48
_ZTVN12v8_inspector8protocol8Profiler7ProfileE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler7ProfileD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE, 48
_ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE, 48
_ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE, 48
_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE, 48
_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler10TypeObjectE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE, 48
_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler10TypeObjectD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE, 48
_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE, 48
_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev
	.weak	_ZTVN12v8_inspector19V8ProfilerAgentImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector19V8ProfilerAgentImplE,"awG",@progbits,_ZTVN12v8_inspector19V8ProfilerAgentImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector19V8ProfilerAgentImplE, @object
	.size	_ZTVN12v8_inspector19V8ProfilerAgentImplE, 128
_ZTVN12v8_inspector19V8ProfilerAgentImplE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector19V8ProfilerAgentImplD1Ev
	.quad	_ZN12v8_inspector19V8ProfilerAgentImplD0Ev
	.quad	_ZN12v8_inspector19V8ProfilerAgentImpl7disableEv
	.quad	_ZN12v8_inspector19V8ProfilerAgentImpl6enableEv
	.quad	_ZN12v8_inspector19V8ProfilerAgentImpl21getBestEffortCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.quad	_ZN12v8_inspector19V8ProfilerAgentImpl19setSamplingIntervalEi
	.quad	_ZN12v8_inspector19V8ProfilerAgentImpl5startEv
	.quad	_ZN12v8_inspector19V8ProfilerAgentImpl20startPreciseCoverageEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES5_
	.quad	_ZN12v8_inspector19V8ProfilerAgentImpl16startTypeProfileEv
	.quad	_ZN12v8_inspector19V8ProfilerAgentImpl4stopEPSt10unique_ptrINS_8protocol8Profiler7ProfileESt14default_deleteIS4_EE
	.quad	_ZN12v8_inspector19V8ProfilerAgentImpl19stopPreciseCoverageEv
	.quad	_ZN12v8_inspector19V8ProfilerAgentImpl15stopTypeProfileEv
	.quad	_ZN12v8_inspector19V8ProfilerAgentImpl19takePreciseCoverageEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler14ScriptCoverageESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.quad	_ZN12v8_inspector19V8ProfilerAgentImpl15takeTypeProfileEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Profiler17ScriptTypeProfileESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.section	.bss._ZN12v8_inspector12_GLOBAL__N_115s_lastProfileIdE,"aw",@nobits
	.align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_115s_lastProfileIdE, @object
	.size	_ZN12v8_inspector12_GLOBAL__N_115s_lastProfileIdE, 4
_ZN12v8_inspector12_GLOBAL__N_115s_lastProfileIdE:
	.zero	4
	.section	.rodata._ZN12v8_inspector18ProfilerAgentStateL18typeProfileStartedE,"a"
	.align 16
	.type	_ZN12v8_inspector18ProfilerAgentStateL18typeProfileStartedE, @object
	.size	_ZN12v8_inspector18ProfilerAgentStateL18typeProfileStartedE, 19
_ZN12v8_inspector18ProfilerAgentStateL18typeProfileStartedE:
	.string	"typeProfileStarted"
	.section	.rodata._ZN12v8_inspector18ProfilerAgentStateL23preciseCoverageDetailedE,"a"
	.align 16
	.type	_ZN12v8_inspector18ProfilerAgentStateL23preciseCoverageDetailedE, @object
	.size	_ZN12v8_inspector18ProfilerAgentStateL23preciseCoverageDetailedE, 24
_ZN12v8_inspector18ProfilerAgentStateL23preciseCoverageDetailedE:
	.string	"preciseCoverageDetailed"
	.section	.rodata._ZN12v8_inspector18ProfilerAgentStateL24preciseCoverageCallCountE,"a"
	.align 16
	.type	_ZN12v8_inspector18ProfilerAgentStateL24preciseCoverageCallCountE, @object
	.size	_ZN12v8_inspector18ProfilerAgentStateL24preciseCoverageCallCountE, 25
_ZN12v8_inspector18ProfilerAgentStateL24preciseCoverageCallCountE:
	.string	"preciseCoverageCallCount"
	.section	.rodata._ZN12v8_inspector18ProfilerAgentStateL22preciseCoverageStartedE,"a"
	.align 16
	.type	_ZN12v8_inspector18ProfilerAgentStateL22preciseCoverageStartedE, @object
	.size	_ZN12v8_inspector18ProfilerAgentStateL22preciseCoverageStartedE, 23
_ZN12v8_inspector18ProfilerAgentStateL22preciseCoverageStartedE:
	.string	"preciseCoverageStarted"
	.section	.rodata._ZN12v8_inspector18ProfilerAgentStateL15profilerEnabledE,"a"
	.align 16
	.type	_ZN12v8_inspector18ProfilerAgentStateL15profilerEnabledE, @object
	.size	_ZN12v8_inspector18ProfilerAgentStateL15profilerEnabledE, 16
_ZN12v8_inspector18ProfilerAgentStateL15profilerEnabledE:
	.string	"profilerEnabled"
	.section	.rodata._ZN12v8_inspector18ProfilerAgentStateL22userInitiatedProfilingE,"a"
	.align 16
	.type	_ZN12v8_inspector18ProfilerAgentStateL22userInitiatedProfilingE, @object
	.size	_ZN12v8_inspector18ProfilerAgentStateL22userInitiatedProfilingE, 23
_ZN12v8_inspector18ProfilerAgentStateL22userInitiatedProfilingE:
	.string	"userInitiatedProfiling"
	.section	.rodata._ZN12v8_inspector18ProfilerAgentStateL16samplingIntervalE,"a"
	.align 16
	.type	_ZN12v8_inspector18ProfilerAgentStateL16samplingIntervalE, @object
	.size	_ZN12v8_inspector18ProfilerAgentStateL16samplingIntervalE, 17
_ZN12v8_inspector18ProfilerAgentStateL16samplingIntervalE:
	.string	"samplingInterval"
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
