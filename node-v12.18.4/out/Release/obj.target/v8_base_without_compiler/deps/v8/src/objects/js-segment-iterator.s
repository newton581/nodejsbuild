	.file	"js-segment-iterator.cc"
	.text
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB22120:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22120:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB22148:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L3:
	ret
	.cfi_endproc
.LFE22148:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB22150:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22150:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB22122:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22122:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB22149:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22149:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv,"axG",@progbits,_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv
	.type	_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv, @function
_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv:
.LFB21777:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L11
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L12
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L13:
	cmpl	$1, %eax
	je	.L20
.L11:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L15
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L16:
	cmpl	$1, %eax
	jne	.L11
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L15:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L16
	.cfi_endproc
.LFE21777:
	.size	_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv, .-_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv
	.section	.text._ZNK2v88internal17JSSegmentIterator10GetSegmentEPNS0_7IsolateEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17JSSegmentIterator10GetSegmentEPNS0_7IsolateEii
	.type	_ZNK2v88internal17JSSegmentIterator10GetSegmentEPNS0_7IsolateEii, @function
_ZNK2v88internal17JSSegmentIterator10GetSegmentEPNS0_7IsolateEii:
.LFB18169:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	(%r8), %rax
	movq	31(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	jmp	_ZN2v88internal4Intl8ToStringEPNS0_7IsolateERKN6icu_6713UnicodeStringEii@PLT
	.cfi_endproc
.LFE18169:
	.size	_ZNK2v88internal17JSSegmentIterator10GetSegmentEPNS0_7IsolateEii, .-_ZNK2v88internal17JSSegmentIterator10GetSegmentEPNS0_7IsolateEii
	.section	.rodata._ZNK2v88internal17JSSegmentIterator19GranularityAsStringEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal17JSSegmentIterator19GranularityAsStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17JSSegmentIterator19GranularityAsStringEv
	.type	_ZNK2v88internal17JSSegmentIterator19GranularityAsStringEv, @function
_ZNK2v88internal17JSSegmentIterator19GranularityAsStringEv:
.LFB18170:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	43(%rdx), %rax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L23
	cmpl	$2, %eax
	je	.L24
	testl	%eax, %eax
	je	.L29
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$34000, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$36144, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$34336, %rax
	ret
	.cfi_endproc
.LFE18170:
	.size	_ZNK2v88internal17JSSegmentIterator19GranularityAsStringEv, .-_ZNK2v88internal17JSSegmentIterator19GranularityAsStringEv
	.section	.rodata._ZN2v88internal17JSSegmentIterator6CreateEPNS0_7IsolateEPN6icu_6713BreakIteratorENS0_11JSSegmenter11GranularityENS0_6HandleINS0_6StringEEE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"(break_iterator) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal17JSSegmentIterator6CreateEPNS0_7IsolateEPN6icu_6713BreakIteratorENS0_11JSSegmenter11GranularityENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSSegmentIterator6CreateEPNS0_7IsolateEPN6icu_6713BreakIteratorENS0_11JSSegmenter11GranularityENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal17JSSegmentIterator6CreateEPNS0_7IsolateEPN6icu_6713BreakIteratorENS0_11JSSegmenter11GranularityENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal17JSSegmentIterator6CreateEPNS0_7IsolateEPN6icu_6713BreakIteratorENS0_11JSSegmenter11GranularityENS0_6HandleINS0_6StringEEE:
.LFB18171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	testq	%rsi, %rsi
	je	.L72
	movq	12464(%rdi), %rax
	movq	%rdi, %r15
	movq	%rsi, %r12
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L32
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L33:
	movq	647(%rsi), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L36:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%rax, %r14
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r14)
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%r14)
	movq	32(%r15), %rax
	subq	48(%r15), %rax
	movq	%r12, 16(%r14)
	cmpq	$33554432, %rax
	jg	.L73
.L38:
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%r12, %xmm0
	movq	%r14, %xmm1
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %rbx
	movups	%xmm0, (%rax)
	leaq	8(%r14), %rax
	movq	%rax, -72(%rbp)
	je	.L74
	leaq	8(%r14), %rax
	lock addl	$1, (%rax)
.L39:
	movl	$48, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, %r9
	movups	%xmm0, 8(%rax)
	movq	%rbx, 24(%rax)
	movq	%r9, %rsi
	movq	$0, (%rax)
	leaq	_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv(%rip), %rax
	movq	%rax, 32(%r9)
	movq	$0, 40(%r9)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r15), %rdi
	movq	(%rax), %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	-72(%rbp), %r9
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	%rax, 40(%r9)
	movq	%r9, %rsi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	-72(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L40
	leaq	8(%r14), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
	cmpl	$1, %eax
	je	.L75
.L43:
	movq	-64(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal4Intl22SetTextToBreakIteratorEPNS0_7IsolateENS0_6HandleINS0_6StringEEEPN6icu_6713BreakIteratorE@PLT
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %r12
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	$0, 39(%rax)
	movq	0(%r13), %rax
	movl	-56(%rbp), %r14d
	movslq	43(%rax), %rdx
	andl	$-4, %edx
	orl	%edx, %r14d
	salq	$32, %r14
	movq	%r14, 39(%rax)
	movq	0(%r13), %r15
	movq	(%rbx), %r14
	movq	%r14, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r14b
	je	.L54
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L48
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
.L48:
	testb	$24, %al
	je	.L54
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L76
	.p2align 4,,10
	.p2align 3
.L54:
	movq	0(%r13), %r14
	movq	(%r12), %r12
	movq	%r12, 31(%r14)
	leaq	31(%r14), %r15
	testb	$1, %r12b
	je	.L53
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L51
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L51:
	testb	$24, %al
	je	.L53
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L77
.L53:
	movq	0(%r13), %rdx
	movslq	43(%rdx), %rax
	andl	$-5, %eax
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L78
.L34:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L35:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L79
.L37:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r15, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L74:
	addl	$1, 8(%r14)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L40:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L43
.L75:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L44
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L45:
	cmpl	$1, %eax
	jne	.L43
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L72:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L44:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L37
	.cfi_endproc
.LFE18171:
	.size	_ZN2v88internal17JSSegmentIterator6CreateEPNS0_7IsolateEPN6icu_6713BreakIteratorENS0_11JSSegmenter11GranularityENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal17JSSegmentIterator6CreateEPNS0_7IsolateEPN6icu_6713BreakIteratorENS0_11JSSegmenter11GranularityENS0_6HandleINS0_6StringEEE
	.section	.text._ZNK2v88internal17JSSegmentIterator9BreakTypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17JSSegmentIterator9BreakTypeEv
	.type	_ZNK2v88internal17JSSegmentIterator9BreakTypeEv, @function
_ZNK2v88internal17JSSegmentIterator9BreakTypeEv:
.LFB18172:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testb	$4, 43(%rax)
	je	.L93
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	(%rbx), %rcx
	movslq	43(%rcx), %rdx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L83
	cmpl	$2, %edx
	je	.L84
	testl	%edx, %edx
	je	.L94
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	andq	$-262144, %rcx
	movq	24(%rcx), %rax
	subq	$37504, %rax
.L82:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	andq	$-262144, %rcx
	movq	24(%rcx), %rdx
	cmpl	$99, %eax
	jbe	.L95
	leal	-100(%rax), %ecx
	leaq	-37504(%rdx), %rsi
	cmpl	$100, %ecx
	leaq	-35768(%rdx), %rax
	cmovnb	%rsi, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	andq	$-262144, %rcx
	movq	24(%rcx), %rdx
	cmpl	$99, %eax
	jbe	.L96
	leal	-100(%rax), %ecx
	leaq	-37504(%rdx), %rsi
	cmpl	$400, %ecx
	leaq	-34000(%rdx), %rax
	cmovnb	%rsi, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore 3
	.cfi_restore 6
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37504, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	-35712(%rdx), %rax
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L96:
	leaq	-35888(%rdx), %rax
	jmp	.L82
	.cfi_endproc
.LFE18172:
	.size	_ZNK2v88internal17JSSegmentIterator9BreakTypeEv, .-_ZNK2v88internal17JSSegmentIterator9BreakTypeEv
	.section	.rodata._ZN2v88internal17JSSegmentIterator5IndexEPNS0_7IsolateENS0_6HandleIS1_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"(icu_break_iterator) != nullptr"
	.section	.text._ZN2v88internal17JSSegmentIterator5IndexEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSSegmentIterator5IndexEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal17JSSegmentIterator5IndexEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal17JSSegmentIterator5IndexEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L100
	movq	(%rdi), %rax
	call	*112(%rax)
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	movl	%eax, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18173:
	.size	_ZN2v88internal17JSSegmentIterator5IndexEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal17JSSegmentIterator5IndexEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal17JSSegmentIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"JSReceiver::CreateDataProperty(isolate, result, factory->segment_string(), segment, Just(kDontThrow)) .FromJust()"
	.align 8
.LC5:
	.string	"JSReceiver::CreateDataProperty(isolate, result, factory->breakType_string(), break_type, Just(kDontThrow)) .FromJust()"
	.align 8
.LC6:
	.string	"JSReceiver::CreateDataProperty(isolate, result, factory->index_string(), new_index, Just(kDontThrow)) .FromJust()"
	.section	.text._ZN2v88internal17JSSegmentIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSSegmentIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal17JSSegmentIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal17JSSegmentIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*112(%rax)
	movq	%r14, %rdi
	movl	%eax, %r13d
	movq	(%r14), %rax
	call	*104(%rax)
	movq	(%rbx), %rdx
	movl	%eax, %r14d
	movslq	43(%rdx), %rax
	orl	$4, %eax
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	cmpl	$-1, %r14d
	je	.L116
	movl	%r14d, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	31(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal4Intl8ToStringEPNS0_7IsolateERKN6icu_6713UnicodeStringEii@PLT
	movq	%rax, %r14
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L103
	movq	(%rbx), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal17JSSegmentIterator9BreakTypeEv
	movq	%rax, %rbx
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L105
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L106:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	leaq	1800(%r12), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movabsq	$4294967297, %r8
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L117
.L108:
	shrw	$8, %ax
	je	.L118
	leaq	1208(%r12), %rdx
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$4294967297, %r8
	call	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L119
.L110:
	shrw	$8, %ax
	je	.L120
	leaq	2688(%r12), %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$4294967297, %r8
	call	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L121
.L112:
	shrw	$8, %ax
	je	.L122
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewJSIteratorResultENS0_6HandleINS0_6ObjectEEEb@PLT
.L103:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L123
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L124
.L107:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L116:
	leaq	88(%r12), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewJSIteratorResultENS0_6HandleINS0_6ObjectEEEb@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L117:
	movl	%eax, -68(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-68(%rbp), %eax
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L118:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L119:
	movl	%eax, -68(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-68(%rbp), %eax
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L120:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L121:
	movl	%eax, -68(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-68(%rbp), %eax
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L122:
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L123:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18174:
	.size	_ZN2v88internal17JSSegmentIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal17JSSegmentIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal17JSSegmentIterator9FollowingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"following"
.LC9:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal17JSSegmentIterator9FollowingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSSegmentIterator9FollowingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal17JSSegmentIterator9FollowingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, @function
_ZN2v88internal17JSSegmentIterator9FollowingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE:
.LFB18178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r15
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L126
	movq	%rbx, %r14
	testq	%rax, %rax
	js	.L128
.L131:
	sarq	$32, %rax
	movl	%eax, %esi
	js	.L135
.L136:
	movq	(%r15), %rax
	movl	%esi, -100(%rbp)
	movq	%r15, %rdi
	call	*40(%rax)
	movl	-100(%rbp), %esi
	cmpl	%esi, 8(%rax)
	ja	.L143
	leaq	.LC8(%rip), %rax
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$9, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L144
	leaq	2552(%r12), %rdx
	movq	%rbx, %r8
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	%rax, -37504(%rcx)
	je	.L155
.L128:
	movl	$197, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object14ConvertToIndexEPNS0_7IsolateENS0_6HandleIS1_EENS0_15MessageTemplateE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L153
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jne	.L131
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	je	.L156
.L135:
	leaq	.LC8(%rip), %rax
	xorl	%edx, %edx
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	$9, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	leaq	2552(%r12), %rdx
	movq	%r14, %r8
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L144
.L154:
	movl	$210, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L153:
	xorl	%eax, %eax
	movb	$0, %ah
.L130:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L157
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movslq	43(%rdx), %rax
	movq	%r15, %rdi
	orl	$4, %eax
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	movq	(%r15), %rax
	call	*104(%rax)
	movl	%eax, %r8d
	movl	$1, %eax
	cmpl	$-1, %r8d
	sete	%dl
	movb	%dl, %ah
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L143:
	movq	0(%r13), %rdx
	movq	%r15, %rdi
	movslq	43(%rdx), %rax
	orl	$4, %eax
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	movq	(%r15), %rax
	call	*120(%rax)
	movl	$1, %eax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L156:
	movsd	7(%rax), %xmm0
	movsd	.LC7(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rax
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L135
	movl	%eax, %ecx
	pxor	%xmm1, %xmm1
	movd	%xmm2, %esi
	cvtsi2sdq	%rcx, %xmm1
	ucomisd	%xmm0, %xmm1
	setnp	%cl
	cmove	%ecx, %edx
	testb	%dl, %dl
	je	.L135
	cmpl	$-1, %eax
	je	.L135
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L144:
	leaq	.LC9(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18178:
	.size	_ZN2v88internal17JSSegmentIterator9FollowingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, .-_ZN2v88internal17JSSegmentIterator9FollowingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal17JSSegmentIterator9PrecedingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"preceding"
	.section	.text._ZN2v88internal17JSSegmentIterator9PrecedingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSSegmentIterator9PrecedingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal17JSSegmentIterator9PrecedingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, @function
_ZN2v88internal17JSSegmentIterator9PrecedingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE:
.LFB18179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r15
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L159
	movq	%rbx, %r14
	testq	%rax, %rax
	js	.L161
.L164:
	sarq	$32, %rax
	movl	%eax, %esi
	js	.L168
.L169:
	movq	(%r15), %rax
	movl	%esi, -100(%rbp)
	movq	%r15, %rdi
	call	*40(%rax)
	movl	-100(%rbp), %esi
	cmpl	%esi, 8(%rax)
	jb	.L180
	testl	%esi, %esi
	jne	.L176
.L180:
	leaq	.LC10(%rip), %rax
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$9, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L178
	leaq	2552(%r12), %rdx
	movq	%rbx, %r8
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	%rax, -37504(%rcx)
	je	.L190
.L161:
	movl	$197, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object14ConvertToIndexEPNS0_7IsolateENS0_6HandleIS1_EENS0_15MessageTemplateE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L188
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jne	.L164
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	je	.L191
.L168:
	leaq	.LC10(%rip), %rax
	xorl	%edx, %edx
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	$9, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	leaq	2552(%r12), %rdx
	movq	%r14, %r8
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L178
.L189:
	movl	$210, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L188:
	xorl	%eax, %eax
	movb	$0, %ah
.L163:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L192
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movslq	43(%rdx), %rax
	movq	%r15, %rdi
	orl	$4, %eax
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	movq	(%r15), %rax
	call	*96(%rax)
	movl	%eax, %r8d
	movl	$1, %eax
	cmpl	$-1, %r8d
	sete	%dl
	movb	%dl, %ah
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L191:
	movsd	7(%rax), %xmm0
	movsd	.LC7(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rax
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L168
	movl	%eax, %ecx
	pxor	%xmm1, %xmm1
	movd	%xmm2, %esi
	cvtsi2sdq	%rcx, %xmm1
	ucomisd	%xmm0, %xmm1
	setnp	%cl
	cmove	%ecx, %edx
	testb	%dl, %dl
	je	.L168
	cmpl	$-1, %eax
	je	.L168
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L178:
	leaq	.LC9(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L176:
	movq	0(%r13), %rdx
	movq	%r15, %rdi
	movslq	43(%rdx), %rax
	orl	$4, %eax
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	movq	(%r15), %rax
	call	*128(%rax)
	movl	$1, %eax
	jmp	.L163
.L192:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18179:
	.size	_ZN2v88internal17JSSegmentIterator9PrecedingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, .-_ZN2v88internal17JSSegmentIterator9PrecedingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal17JSSegmentIterator10GetSegmentEPNS0_7IsolateEii,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal17JSSegmentIterator10GetSegmentEPNS0_7IsolateEii, @function
_GLOBAL__sub_I__ZNK2v88internal17JSSegmentIterator10GetSegmentEPNS0_7IsolateEii:
.LFB22151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22151:
	.size	_GLOBAL__sub_I__ZNK2v88internal17JSSegmentIterator10GetSegmentEPNS0_7IsolateEii, .-_GLOBAL__sub_I__ZNK2v88internal17JSSegmentIterator10GetSegmentEPNS0_7IsolateEii
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal17JSSegmentIterator10GetSegmentEPNS0_7IsolateEii
	.weak	_ZTVSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713BreakIteratorELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
