	.file	"memory-tracing.cc"
	.text
	.section	.rodata._ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh.str1.1,"aMS",@progbits,1
.LC0:
	.string	"turbofan"
.LC1:
	.string	"liftoff"
.LC2:
	.string	"interpreter"
.LC3:
	.string	"none"
.LC4:
	.string	" store to"
.LC5:
	.string	"load from"
.LC6:
	.string	" i8:%d / %02x"
.LC7:
	.string	"i16:%d / %04x"
.LC8:
	.string	"i32:%d / %08x"
.LC9:
	.string	"i64:%ld / %016lx"
.LC10:
	.string	"f32:%f / %08x"
.LC11:
	.string	"f64:%f / %016lx"
.LC12:
	.string	"???"
	.section	.rodata._ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"%-11s func:%6d+0x%-6x%s %08x val: %s\n"
	.section	.text._ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh
	.type	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh, @function
_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh:
.LFB5091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-112(%rbp), %rax
	cmpb	$13, 5(%rsi)
	movq	$64, -120(%rbp)
	movq	%rax, -128(%rbp)
	ja	.L2
	movzbl	5(%rsi), %eax
	leaq	.L4(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh,"a",@progbits
	.align 4
	.align 4
.L4:
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.section	.text._ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh
	.p2align 4,,10
	.p2align 3
.L2:
	movq	-128(%rbp), %rdi
	movq	-120(%rbp), %rsi
	leaq	.LC12(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	.LC1(%rip), %rdx
	cmpb	$2, %r12b
	je	.L11
	leaq	.LC0(%rip), %rdx
	jg	.L11
	testb	%r12b, %r12b
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rax
	cmove	%rax, %rdx
.L11:
	movl	(%rbx), %eax
	pushq	-128(%rbp)
	leaq	.LC4(%rip), %r9
	movl	%r14d, %r8d
	cmpb	$0, 4(%rbx)
	movl	%r13d, %ecx
	movl	$1, %edi
	pushq	%rax
	leaq	.LC5(%rip), %rax
	leaq	.LC13(%rip), %rsi
	cmove	%rax, %r9
	xorl	%eax, %eax
	call	__printf_chk@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	(%rsi), %eax
	movq	-128(%rbp), %rdi
	leaq	.LC6(%rip), %rdx
	movq	-120(%rbp), %rsi
	movzbl	(%r8,%rax), %ecx
	xorl	%eax, %eax
	movl	%ecx, %r8d
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L8:
	movl	(%rsi), %eax
	movq	-128(%rbp), %rdi
	leaq	.LC7(%rip), %rdx
	movq	-120(%rbp), %rsi
	movzwl	(%r8,%rax), %ecx
	xorl	%eax, %eax
	movl	%ecx, %r8d
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L7:
	movl	(%rsi), %eax
	movq	-128(%rbp), %rdi
	leaq	.LC8(%rip), %rdx
	movq	-120(%rbp), %rsi
	movl	(%r8,%rax), %ecx
	xorl	%eax, %eax
	movl	%ecx, %r8d
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L6:
	movl	(%rsi), %eax
	movq	-128(%rbp), %rdi
	leaq	.LC9(%rip), %rdx
	movq	-120(%rbp), %rsi
	movq	(%r8,%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %r8
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L5:
	movl	(%rsi), %eax
	movq	-128(%rbp), %rdi
	pxor	%xmm0, %xmm0
	leaq	.LC10(%rip), %rdx
	movq	-120(%rbp), %rsi
	movl	(%r8,%rax), %ecx
	movl	$1, %eax
	movd	%ecx, %xmm1
	cvtss2sd	%xmm1, %xmm0
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L3:
	movl	(%rsi), %eax
	movq	-128(%rbp), %rdi
	leaq	.LC11(%rip), %rdx
	movq	-120(%rbp), %rsi
	movq	(%r8,%rax), %rax
	movq	%rax, %rcx
	movq	%rax, %xmm0
	movl	$1, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L10
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5091:
	.size	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh, .-_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh, @function
_GLOBAL__sub_I__ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh:
.LFB5873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5873:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh, .-_GLOBAL__sub_I__ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
