	.file	"value-serializer.cc"
	.text
	.section	.text._ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv,"axG",@progbits,_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv
	.type	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv, @function
_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv:
.LFB24056:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24056:
	.size	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv, .-_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv
	.section	.text._ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm,"axG",@progbits,_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm
	.type	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm, @function
_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm:
.LFB24055:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	%rax, %rsi
	ja	.L5
	addq	%r8, %rsi
	movq	%r8, %rax
	movq	%rsi, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	jmp	_ZN2v88internal4Zone9NewExpandEm@PLT
	.cfi_endproc
.LFE24055:
	.size	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm, .-_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm
	.section	.text._ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED2Ev,"axG",@progbits,_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED2Ev
	.type	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED2Ev, @function
_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED2Ev:
.LFB21049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	.cfi_endproc
.LFE21049:
	.size	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED2Ev, .-_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED2Ev
	.weak	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED1Ev
	.set	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED1Ev,_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED2Ev
	.section	.text._ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED0Ev,"axG",@progbits,_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED0Ev
	.type	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED0Ev, @function
_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED0Ev:
.LFB21051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21051:
	.size	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED0Ev, .-_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED0Ev
	.section	.text._ZN2v88internalL16IsValidObjectKeyENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL16IsValidObjectKeyENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internalL16IsValidObjectKeyENS0_6HandleINS0_6ObjectEEE:
.LFB18833:
	.cfi_startproc
	movq	(%rdi), %rax
	movl	$1, %r8d
	testb	$1, %al
	jne	.L17
.L10:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	-1(%rax), %rdx
	movl	$1, %r8d
	cmpw	$64, 11(%rdx)
	jbe	.L10
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	sete	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE18833:
	.size	_ZN2v88internalL16IsValidObjectKeyENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internalL16IsValidObjectKeyENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internalL30SetPropertiesFromKeyValuePairsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPNS3_INS0_6ObjectEEEj,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30SetPropertiesFromKeyValuePairsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPNS3_INS0_6ObjectEEEj, @function
_ZN2v88internalL30SetPropertiesFromKeyValuePairsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPNS3_INS0_6ObjectEEEj:
.LFB18850:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	%ecx, %ecx
	movl	%ecx, -172(%rbp)
	je	.L19
	movq	%rsi, %r15
	movq	%rdx, %r12
	leaq	-144(%rbp), %r13
	xorl	%ebx, %ebx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L20:
	movq	-168(%rbp), %rsi
	leal	1(%rbx), %eax
	movq	%r15, %rdx
	movq	%r13, %rdi
	movl	$1, %r9d
	leaq	-145(%rbp), %r8
	movq	(%r12,%rax,8), %r14
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -145(%rbp)
	je	.L23
	cmpl	$4, -140(%rbp)
	jne	.L23
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE@PLT
	testq	%rax, %rax
	je	.L23
	addl	$2, %ebx
	cmpl	-172(%rbp), %ebx
	jnb	.L19
.L24:
	movl	%ebx, %eax
	movq	(%r12,%rax,8), %rcx
	movq	%rcx, %rdi
	call	_ZN2v88internalL16IsValidObjectKeyENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	jne	.L20
.L23:
	xorl	%eax, %eax
	movb	$0, %ah
.L25:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L34
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	$257, %eax
	jmp	.L25
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18850:
	.size	_ZN2v88internalL30SetPropertiesFromKeyValuePairsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPNS3_INS0_6ObjectEEEj, .-_ZN2v88internalL30SetPropertiesFromKeyValuePairsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPNS3_INS0_6ObjectEEEj
	.section	.rodata._ZN2v88internalL16CommitPropertiesENS0_6HandleINS0_8JSObjectEEENS1_INS0_3MapEEERKSt6vectorINS1_INS0_6ObjectEEESaIS8_EE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.rodata._ZN2v88internalL16CommitPropertiesENS0_6HandleINS0_8JSObjectEEENS1_INS0_3MapEEERKSt6vectorINS1_INS0_6ObjectEEESaIS8_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"(!IsSmi() && (*layout_word_index < length())) || (IsSmi() && (*layout_word_index < 1))"
	.section	.rodata._ZN2v88internalL16CommitPropertiesENS0_6HandleINS0_8JSObjectEEENS1_INS0_3MapEEERKSt6vectorINS1_INS0_6ObjectEEESaIS8_EE.str1.1
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL16CommitPropertiesENS0_6HandleINS0_8JSObjectEEENS1_INS0_3MapEEERKSt6vectorINS1_INS0_6ObjectEEESaIS8_EE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL16CommitPropertiesENS0_6HandleINS0_8JSObjectEEENS1_INS0_3MapEEERKSt6vectorINS1_INS0_6ObjectEEESaIS8_EE, @function
_ZN2v88internalL16CommitPropertiesENS0_6HandleINS0_8JSObjectEEENS1_INS0_3MapEEERKSt6vectorINS1_INS0_6ObjectEEESaIS8_EE:
.LFB18832:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8JSObject21AllocateStorageForMapENS0_6HandleIS1_EENS2_INS0_3MapEEE@PLT
	movq	0(%r13), %r15
	movq	-1(%r15), %rax
	movq	(%r12), %rdx
	movq	39(%rax), %r14
	addq	$7, %r14
	cmpq	8(%r12), %rdx
	je	.L35
	movl	$1, %ebx
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L71:
	movq	(%rdx,%rcx,8), %rax
	leaq	-1(%r15), %rdi
	movq	(%rax), %rdx
	leal	(%rbx,%rbx,2), %eax
	sall	$3, %eax
	cltq
	movq	(%rax,%r14), %r8
	movq	-1(%r15), %r11
	movq	39(%r11), %rcx
	sarq	$32, %r8
	movq	7(%rax,%rcx), %rsi
	movq	%rsi, %rax
	shrq	$38, %rsi
	shrq	$51, %rax
	andl	$7, %esi
	movq	%rax, %r10
	movzbl	7(%r11), %eax
	movzbl	8(%r11), %ecx
	andl	$1023, %r10d
	subl	%ecx, %eax
	cmpl	%eax, %r10d
	setl	%cl
	jl	.L100
	subl	%eax, %r10d
	movl	$16, %r9d
	leal	16(,%r10,8), %r10d
.L38:
	cmpl	$2, %esi
	jne	.L101
	movl	$32768, %esi
.L39:
	movzbl	%cl, %ecx
	cltq
	movslq	%r10d, %r10
	movslq	%r9d, %r9
	salq	$14, %rcx
	salq	$17, %rax
	salq	$27, %r9
	orq	%rcx, %rax
	shrl	$6, %r8d
	movq	%rdx, %rcx
	orq	%r10, %rax
	notq	%rcx
	andl	$7, %r8d
	orq	%r9, %rax
	andl	$1, %ecx
	orq	%rax, %rsi
	andl	$16384, %eax
	cmpl	$2, %r8d
	jne	.L42
	testb	%cl, %cl
	je	.L43
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movq	%xmm0, %r9
.L44:
	movq	%rsi, %r8
	movl	%esi, %ecx
	movq	(%rdi), %rdx
	shrq	$30, %r8
	sarl	$3, %ecx
	andl	$15, %r8d
	andl	$2047, %ecx
	subl	%r8d, %ecx
	testq	%rax, %rax
	jne	.L45
	movq	7(%r15), %rdx
	andq	$-262144, %r15
	movq	24(%r15), %rax
	subq	$37592, %rax
	testb	$1, %dl
	je	.L59
	cmpq	288(%rax), %rdx
	je	.L59
	.p2align 4,,10
	.p2align 3
.L47:
	leal	16(,%rcx,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
.L73:
	movq	%r9, 7(%rax)
.L58:
	movq	(%r12), %rdx
	movq	8(%r12), %rax
	movl	%ebx, %ecx
	addl	$1, %ebx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jnb	.L35
	movq	0(%r13), %r15
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L59:
	movq	968(%rax), %rdx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L101:
	cmpb	$2, %sil
	jg	.L40
	je	.L41
.L77:
	xorl	%esi, %esi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L40:
	subl	$3, %esi
	cmpb	$1, %sil
	jbe	.L77
.L41:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	testq	%rax, %rax
	je	.L60
	andl	$16376, %esi
	addq	%rdi, %rsi
	movq	%rdx, (%rsi)
	testb	%cl, %cl
	jne	.L58
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L62
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L62:
	testb	$24, %al
	je	.L58
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L58
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L100:
	movzbl	8(%r11), %r9d
	movzbl	8(%r11), %r11d
	addl	%r11d, %r10d
	sall	$3, %r9d
	sall	$3, %r10d
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L45:
	movq	47(%rdx), %rax
	testq	%rax, %rax
	je	.L57
	movq	%rax, %r8
	movl	$32, %edx
	notq	%r8
	movl	%r8d, %r10d
	andl	$1, %r10d
	jne	.L49
	movslq	11(%rax), %rdx
	sall	$3, %edx
.L49:
	cmpl	%edx, %ecx
	jnb	.L57
	testl	%ecx, %ecx
	leal	31(%rcx), %edx
	cmovns	%ecx, %edx
	sarl	$5, %edx
	andl	$1, %r8d
	jne	.L50
	cmpl	%edx, 11(%rax)
	jle	.L50
	movl	%ecx, %r8d
	sarl	$31, %r8d
	shrl	$27, %r8d
	addl	%r8d, %ecx
	andl	$31, %ecx
	subl	%r8d, %ecx
	movl	$1, %r8d
	sall	%cl, %r8d
	movl	%r8d, %ecx
	testb	%r10b, %r10b
	je	.L102
.L54:
	sarq	$32, %rax
	testl	%eax, %ecx
	sete	%dl
.L56:
	movq	%rsi, %rax
	andl	$16376, %eax
	addq	%rdi, %rax
	testb	%dl, %dl
	jne	.L57
	movq	%r9, (%rax)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L60:
	movq	7(%r15), %rdi
	andq	$-262144, %r15
	movq	24(%r15), %rax
	subq	$37592, %rax
	testb	$1, %dil
	je	.L67
	cmpq	288(%rax), %rdi
	je	.L67
.L66:
	movq	%rsi, %rax
	sarl	$3, %esi
	shrq	$30, %rax
	andl	$2047, %esi
	andl	$15, %eax
	subl	%eax, %esi
	leal	16(,%rsi,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %r15
	movq	%rdx, (%r15)
	testb	%cl, %cl
	jne	.L58
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L69
	movq	%r15, %rsi
	movq	%rdx, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
.L69:
	testb	$24, %al
	je	.L58
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L58
	movq	%r15, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L57:
	andl	$16376, %esi
	movq	(%rsi,%rdi), %rax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L43:
	movabsq	$-2251799814209537, %r9
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37512(%rcx), %rdx
	je	.L44
	movq	7(%rdx), %r9
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L67:
	movq	968(%rax), %rdi
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L35:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	sall	$2, %edx
	movslq	%edx, %rdx
	movl	15(%rax,%rdx), %eax
	testl	%eax, %r8d
	sete	%dl
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L50:
	cmpl	$31, %ecx
	jg	.L52
	testb	%r10b, %r10b
	je	.L52
	movl	$1, %edx
	sall	%cl, %edx
	movl	%edx, %ecx
	jmp	.L54
.L52:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18832:
	.size	_ZN2v88internalL16CommitPropertiesENS0_6HandleINS0_8JSObjectEEENS1_INS0_3MapEEERKSt6vectorINS1_INS0_6ObjectEEESaIS8_EE, .-_ZN2v88internalL16CommitPropertiesENS0_6HandleINS0_8JSObjectEEENS1_INS0_3MapEEERKSt6vectorINS1_INS0_6ObjectEEESaIS8_EE
	.section	.rodata._ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../deps/v8/src/objects/value-serializer.cc:247"
	.section	.text._ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE
	.type	_ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE, @function
_ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE:
.LFB18692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm2
	xorl	%eax, %eax
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	leaq	.LC3(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	48(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$37592, %r12
	subq	$8, %rsp
	movw	%ax, 40(%rdi)
	movq	41136(%rsi), %rsi
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%rdi)
	movq	%r13, %rdi
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	pxor	%xmm0, %xmm0
	movdqa	.LC4(%rip), %xmm1
	leaq	16+_ZTVN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEEE(%rip), %rax
	movq	%r12, 128(%rbx)
	movq	%r13, 176(%rbx)
	movq	%r12, 208(%rbx)
	movq	%r13, 256(%rbx)
	movb	$0, 168(%rbx)
	movq	%rax, 112(%rbx)
	movl	$0, 184(%rbx)
	movb	$0, 248(%rbx)
	movq	%rax, 192(%rbx)
	movups	%xmm1, 136(%rbx)
	movups	%xmm0, 152(%rbx)
	movups	%xmm1, 216(%rbx)
	movups	%xmm0, 232(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18692:
	.size	_ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE, .-_ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE
	.globl	_ZN2v88internal15ValueSerializerC1EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE
	.set	_ZN2v88internal15ValueSerializerC1EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE,_ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE
	.section	.text._ZN2v88internal15ValueSerializerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializerD2Ev
	.type	_ZN2v88internal15ValueSerializerD2Ev, @function
_ZN2v88internal15ValueSerializerD2Ev:
.LFB18695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L106
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L107
	movq	(%rdi), %rax
	call	*56(%rax)
.L106:
	leaq	16+_ZTVN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEEE(%rip), %r12
	leaq	192(%rbx), %r13
	movq	%r12, 192(%rbx)
	movq	%r13, %rdi
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	movq	%r12, 112(%rbx)
	leaq	112(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	addq	$8, %rsp
	leaq	48(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4ZoneD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	free@PLT
	jmp	.L106
	.cfi_endproc
.LFE18695:
	.size	_ZN2v88internal15ValueSerializerD2Ev, .-_ZN2v88internal15ValueSerializerD2Ev
	.globl	_ZN2v88internal15ValueSerializerD1Ev
	.set	_ZN2v88internal15ValueSerializerD1Ev,_ZN2v88internal15ValueSerializerD2Ev
	.section	.text._ZN2v88internal15ValueSerializer37SetTreatArrayBufferViewsAsHostObjectsEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer37SetTreatArrayBufferViewsAsHostObjectsEb
	.type	_ZN2v88internal15ValueSerializer37SetTreatArrayBufferViewsAsHostObjectsEb, @function
_ZN2v88internal15ValueSerializer37SetTreatArrayBufferViewsAsHostObjectsEb:
.LFB18698:
	.cfi_startproc
	endbr64
	movb	%sil, 40(%rdi)
	ret
	.cfi_endproc
.LFE18698:
	.size	_ZN2v88internal15ValueSerializer37SetTreatArrayBufferViewsAsHostObjectsEb, .-_ZN2v88internal15ValueSerializer37SetTreatArrayBufferViewsAsHostObjectsEb
	.section	.text._ZN2v88internal15ValueSerializer12ExpandBufferEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	.type	_ZN2v88internal15ValueSerializer12ExpandBufferEm, @function
_ZN2v88internal15ValueSerializer12ExpandBufferEm:
.LFB18709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%rbx), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	$0, -32(%rbp)
	leaq	(%rax,%rax), %rdx
	cmpq	%rsi, %rdx
	cmovb	%rsi, %rdx
	addq	$64, %rdx
	testq	%rdi, %rdi
	je	.L114
	movq	(%rdi), %rax
	leaq	-32(%rbp), %rcx
	movq	%r8, %rsi
	call	*48(%rax)
.L115:
	testq	%rax, %rax
	je	.L116
	movq	%rax, 16(%rbx)
	movq	-32(%rbp), %rax
	movl	$1, %edx
	movq	%rax, 32(%rbx)
.L117:
	xorl	%eax, %eax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	movb	$1, %ah
	movb	%dl, %al
	jne	.L120
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movb	$1, 41(%rbx)
	xorl	%edx, %edx
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L114:
	movq	%rdx, %rsi
	movq	%r8, %rdi
	movq	%rdx, -40(%rbp)
	call	realloc@PLT
	movq	-40(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	jmp	.L115
.L120:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18709:
	.size	_ZN2v88internal15ValueSerializer12ExpandBufferEm, .-_ZN2v88internal15ValueSerializer12ExpandBufferEm
	.section	.text._ZN2v88internal15ValueSerializer15ReserveRawBytesEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer15ReserveRawBytesEm
	.type	_ZN2v88internal15ValueSerializer15ReserveRawBytesEm, @function
_ZN2v88internal15ValueSerializer15ReserveRawBytesEm:
.LFB18708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	24(%rdi), %r13
	leaq	0(%r13,%rsi), %r12
	cmpq	%r12, 32(%rdi)
	jb	.L122
.L124:
	movq	16(%rdi), %r14
	movq	%r12, 24(%rdi)
	movl	$1, %edx
	addq	%r13, %r14
.L123:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r12
	movb	%dl, %al
	popq	%r13
	movq	%r14, %rdx
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-40(%rbp), %rdi
	testb	%al, %al
	jne	.L124
	xorl	%edx, %edx
	jmp	.L123
	.cfi_endproc
.LFE18708:
	.size	_ZN2v88internal15ValueSerializer15ReserveRawBytesEm, .-_ZN2v88internal15ValueSerializer15ReserveRawBytesEm
	.section	.text._ZN2v88internal15ValueSerializer13WriteRawBytesEPKvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer13WriteRawBytesEPKvm
	.type	_ZN2v88internal15ValueSerializer13WriteRawBytesEPKvm, @function
_ZN2v88internal15ValueSerializer13WriteRawBytesEPKvm:
.LFB18707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	subq	$24, %rsp
	movq	24(%rdi), %r15
	leaq	(%rdx,%r15), %r14
	cmpq	32(%rdi), %r14
	ja	.L127
.L130:
	movq	%r14, 24(%rdi)
	testq	%rdx, %rdx
	jne	.L132
.L126:
	addq	$24, %rsp
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	addq	16(%rdi), %r15
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r15, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%rdx, -48(%rbp)
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-40(%rbp), %rdi
	movq	-48(%rbp), %rdx
	testb	%al, %al
	jne	.L130
	jmp	.L126
	.cfi_endproc
.LFE18707:
	.size	_ZN2v88internal15ValueSerializer13WriteRawBytesEPKvm, .-_ZN2v88internal15ValueSerializer13WriteRawBytesEPKvm
	.section	.text._ZN2v88internal15ValueSerializer11WriteDoubleEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer11WriteDoubleEd
	.type	_ZN2v88internal15ValueSerializer11WriteDoubleEd, @function
_ZN2v88internal15ValueSerializer11WriteDoubleEd:
.LFB18702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	24(%rdi), %r13
	leaq	8(%r13), %r12
	cmpq	32(%rdi), %r12
	ja	.L134
.L136:
	movq	16(%rdi), %rax
	movq	%r12, 24(%rdi)
	movq	%xmm0, (%rax,%r13)
.L133:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%rdi, -24(%rbp)
	movsd	%xmm0, -32(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-24(%rbp), %rdi
	movsd	-32(%rbp), %xmm0
	testb	%al, %al
	jne	.L136
	jmp	.L133
	.cfi_endproc
.LFE18702:
	.size	_ZN2v88internal15ValueSerializer11WriteDoubleEd, .-_ZN2v88internal15ValueSerializer11WriteDoubleEd
	.section	.text._ZN2v88internal15ValueSerializer18WriteOneByteStringENS0_6VectorIKhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer18WriteOneByteStringENS0_6VectorIKhEE
	.type	_ZN2v88internal15ValueSerializer18WriteOneByteStringENS0_6VectorIKhEE, @function
_ZN2v88internal15ValueSerializer18WriteOneByteStringENS0_6VectorIKhEE:
.LFB18703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-61(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, %eax
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L140:
	movl	%eax, %esi
	movl	%eax, %ecx
	orl	$-128, %esi
	movb	%sil, (%rdx)
	movq	%rdx, %rsi
	addq	$1, %rdx
	shrl	$7, %eax
	jne	.L140
	movl	%ecx, %eax
	subq	%r15, %rdx
	andl	$127, %eax
	movb	%al, (%rsi)
	movq	24(%rbx), %rcx
	leaq	(%rdx,%rcx), %r13
	cmpq	32(%rbx), %r13
	ja	.L141
.L144:
	movq	%r13, 24(%rbx)
	testq	%rdx, %rdx
	jne	.L156
.L146:
	movslq	%r12d, %r12
	leaq	(%r12,%r13), %r15
	cmpq	32(%rbx), %r15
	ja	.L147
.L150:
	movq	%r15, 24(%rbx)
	testq	%r12, %r12
	jne	.L157
.L139:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L158
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	movslq	%r12d, %r12
	addq	%rcx, %rdi
	call	memcpy@PLT
	movq	24(%rbx), %r13
	leaq	(%r12,%r13), %r15
	cmpq	32(%rbx), %r15
	jbe	.L150
	.p2align 4,,10
	.p2align 3
.L147:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L150
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L157:
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	addq	%r13, %rdi
	call	memcpy@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	testb	%al, %al
	jne	.L144
	movq	24(%rbx), %r13
	jmp	.L146
.L158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18703:
	.size	_ZN2v88internal15ValueSerializer18WriteOneByteStringENS0_6VectorIKhEE, .-_ZN2v88internal15ValueSerializer18WriteOneByteStringENS0_6VectorIKhEE
	.section	.text._ZN2v88internal15ValueSerializer18WriteTwoByteStringENS0_6VectorIKtEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer18WriteTwoByteStringENS0_6VectorIKtEE
	.type	_ZN2v88internal15ValueSerializer18WriteTwoByteStringENS0_6VectorIKtEE, @function
_ZN2v88internal15ValueSerializer18WriteTwoByteStringENS0_6VectorIKtEE:
.LFB18704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-61(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movslq	%edx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	(%rdx,%rdx), %eax
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L160:
	movl	%eax, %esi
	movl	%eax, %ecx
	orl	$-128, %esi
	movb	%sil, (%rdx)
	movq	%rdx, %rsi
	addq	$1, %rdx
	shrl	$7, %eax
	jne	.L160
	movl	%ecx, %eax
	subq	%r15, %rdx
	andl	$127, %eax
	movb	%al, (%rsi)
	movq	24(%rbx), %rcx
	leaq	(%rdx,%rcx), %r13
	cmpq	32(%rbx), %r13
	ja	.L161
.L164:
	movq	%r13, 24(%rbx)
	testq	%rdx, %rdx
	jne	.L176
.L166:
	addq	%r12, %r12
	leaq	(%r12,%r13), %r15
	cmpq	32(%rbx), %r15
	ja	.L167
.L170:
	movq	%r15, 24(%rbx)
	testq	%r12, %r12
	jne	.L177
.L159:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L178
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	addq	%r12, %r12
	addq	%rcx, %rdi
	call	memcpy@PLT
	movq	24(%rbx), %r13
	leaq	(%r12,%r13), %r15
	cmpq	32(%rbx), %r15
	jbe	.L170
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L170
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L177:
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	addq	%r13, %rdi
	call	memcpy@PLT
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	testb	%al, %al
	jne	.L164
	movq	24(%rbx), %r13
	jmp	.L166
.L178:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18704:
	.size	_ZN2v88internal15ValueSerializer18WriteTwoByteStringENS0_6VectorIKtEE, .-_ZN2v88internal15ValueSerializer18WriteTwoByteStringENS0_6VectorIKtEE
	.section	.text._ZN2v88internal15ValueSerializer8WriteTagENS0_16SerializationTagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer8WriteTagENS0_16SerializationTagE
	.type	_ZN2v88internal15ValueSerializer8WriteTagENS0_16SerializationTagE, @function
_ZN2v88internal15ValueSerializer8WriteTagENS0_16SerializationTagE:
.LFB18699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	subq	$24, %rsp
	movq	24(%rdi), %r14
	leaq	1(%r14), %r13
	cmpq	32(%rdi), %r13
	ja	.L180
.L182:
	movq	16(%rdi), %rax
	movq	%r13, 24(%rdi)
	movb	%r12b, (%rax,%r14)
.L179:
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-40(%rbp), %rdi
	testb	%al, %al
	jne	.L182
	jmp	.L179
	.cfi_endproc
.LFE18699:
	.size	_ZN2v88internal15ValueSerializer8WriteTagENS0_16SerializationTagE, .-_ZN2v88internal15ValueSerializer8WriteTagENS0_16SerializationTagE
	.section	.text._ZN2v88internal15ValueSerializer11WriteHeaderEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer11WriteHeaderEv
	.type	_ZN2v88internal15ValueSerializer11WriteHeaderEv, @function
_ZN2v88internal15ValueSerializer11WriteHeaderEv:
.LFB18697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %r13
	leaq	1(%r13), %r12
	cmpq	32(%rdi), %r12
	ja	.L186
.L188:
	movq	16(%rbx), %rax
	movq	%r12, 24(%rbx)
	movb	$-1, (%rax,%r13)
.L187:
	movq	24(%rbx), %r13
	leaq	1(%r13), %r12
	cmpq	32(%rbx), %r12
	ja	.L190
.L192:
	movq	16(%rbx), %rax
	movq	%r12, 24(%rbx)
	movb	$13, (%rax,%r13)
.L185:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	%r12, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L188
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L192
	jmp	.L185
	.cfi_endproc
.LFE18697:
	.size	_ZN2v88internal15ValueSerializer11WriteHeaderEv, .-_ZN2v88internal15ValueSerializer11WriteHeaderEv
	.section	.text._ZN2v88internal15ValueSerializer11WriteUint32Ej,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer11WriteUint32Ej
	.type	_ZN2v88internal15ValueSerializer11WriteUint32Ej, @function
_ZN2v88internal15ValueSerializer11WriteUint32Ej:
.LFB18710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-45(%rbp), %r12
	movq	%r12, %rdx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L196:
	movl	%esi, %ecx
	movl	%esi, %eax
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	movq	%rdx, %rcx
	addq	$1, %rdx
	shrl	$7, %esi
	jne	.L196
	andl	$127, %eax
	subq	%r12, %rdx
	movb	%al, (%rcx)
	movq	24(%rdi), %r14
	leaq	(%rdx,%r14), %r13
	cmpq	32(%rdi), %r13
	ja	.L197
.L200:
	movq	%r13, 24(%rdi)
	testq	%rdx, %rdx
	jne	.L205
.L195:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	addq	16(%rdi), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%r13, %rsi
	movq	%rdx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rdx
	testb	%al, %al
	jne	.L200
	jmp	.L195
.L206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18710:
	.size	_ZN2v88internal15ValueSerializer11WriteUint32Ej, .-_ZN2v88internal15ValueSerializer11WriteUint32Ej
	.section	.text._ZN2v88internal15ValueSerializer11WriteUint64Em,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer11WriteUint64Em
	.type	_ZN2v88internal15ValueSerializer11WriteUint64Em, @function
_ZN2v88internal15ValueSerializer11WriteUint64Em:
.LFB18711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-50(%rbp), %r12
	movq	%r12, %rdx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L208:
	movl	%esi, %ecx
	movl	%esi, %eax
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	movq	%rdx, %rcx
	addq	$1, %rdx
	shrq	$7, %rsi
	jne	.L208
	andl	$127, %eax
	subq	%r12, %rdx
	movb	%al, (%rcx)
	movq	24(%rdi), %r14
	leaq	(%rdx,%r14), %r13
	cmpq	32(%rdi), %r13
	ja	.L209
.L212:
	movq	%r13, 24(%rdi)
	testq	%rdx, %rdx
	jne	.L217
.L207:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	addq	16(%rdi), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L209:
	movq	%r13, %rsi
	movq	%rdx, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %rdx
	testb	%al, %al
	jne	.L212
	jmp	.L207
.L218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18711:
	.size	_ZN2v88internal15ValueSerializer11WriteUint64Em, .-_ZN2v88internal15ValueSerializer11WriteUint64Em
	.section	.text._ZN2v88internal15ValueSerializer7ReleaseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer7ReleaseEv
	.type	_ZN2v88internal15ValueSerializer7ReleaseEv, @function
_ZN2v88internal15ValueSerializer7ReleaseEv:
.LFB18712:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	movq	$0, 16(%rdi)
	movups	%xmm0, 24(%rdi)
	ret
	.cfi_endproc
.LFE18712:
	.size	_ZN2v88internal15ValueSerializer7ReleaseEv, .-_ZN2v88internal15ValueSerializer7ReleaseEv
	.section	.text._ZN2v88internal15ValueSerializer19TransferArrayBufferEjNS0_6HandleINS0_13JSArrayBufferEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer19TransferArrayBufferEjNS0_6HandleINS0_13JSArrayBufferEEE
	.type	_ZN2v88internal15ValueSerializer19TransferArrayBufferEjNS0_6HandleINS0_13JSArrayBufferEEE, @function
_ZN2v88internal15ValueSerializer19TransferArrayBufferEjNS0_6HandleINS0_13JSArrayBufferEEE:
.LFB18717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$192, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	(%rdx), %rsi
	call	_ZN2v88internal15IdentityMapBase8GetEntryEm@PLT
	movl	%ebx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18717:
	.size	_ZN2v88internal15ValueSerializer19TransferArrayBufferEjNS0_6HandleINS0_13JSArrayBufferEEE, .-_ZN2v88internal15ValueSerializer19TransferArrayBufferEjNS0_6HandleINS0_13JSArrayBufferEEE
	.section	.text._ZN2v88internal15ValueSerializer12WriteOddballENS0_7OddballE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer12WriteOddballENS0_7OddballE
	.type	_ZN2v88internal15ValueSerializer12WriteOddballENS0_7OddballE, @function
_ZN2v88internal15ValueSerializer12WriteOddballENS0_7OddballE:
.LFB18719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movslq	43(%rsi), %rax
	cmpb	$3, %al
	je	.L230
	ja	.L224
	testb	%al, %al
	je	.L231
	cmpb	$1, %al
	jne	.L225
	movl	$84, %r12d
.L223:
	movq	24(%rdi), %r14
	leaq	1(%r14), %r13
	cmpq	32(%rdi), %r13
	ja	.L226
.L228:
	movq	16(%rdi), %rax
	movq	%r13, 24(%rdi)
	movb	%r12b, (%rax,%r14)
.L222:
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	cmpb	$5, %al
	jne	.L225
	movl	$95, %r12d
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$70, %r12d
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L230:
	movl	$48, %r12d
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L226:
	movq	%r13, %rsi
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-40(%rbp), %rdi
	testb	%al, %al
	jne	.L228
	jmp	.L222
.L225:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18719:
	.size	_ZN2v88internal15ValueSerializer12WriteOddballENS0_7OddballE, .-_ZN2v88internal15ValueSerializer12WriteOddballENS0_7OddballE
	.section	.text._ZN2v88internal15ValueSerializer8WriteSmiENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer8WriteSmiENS0_3SmiE
	.type	_ZN2v88internal15ValueSerializer8WriteSmiENS0_3SmiE, @function
_ZN2v88internal15ValueSerializer8WriteSmiENS0_3SmiE:
.LFB18720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	24(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	1(%r14), %r13
	cmpq	32(%rdi), %r13
	ja	.L236
.L238:
	movq	16(%rbx), %rax
	movq	%r13, 24(%rbx)
	movb	$73, (%rax,%r14)
.L237:
	movq	%r12, %rsi
	leaq	-45(%rbp), %r12
	sarq	$32, %rsi
	movq	%r12, %rdx
	leal	(%rsi,%rsi), %eax
	sarl	$31, %esi
	xorl	%eax, %esi
	.p2align 4,,10
	.p2align 3
.L240:
	movl	%esi, %ecx
	movl	%esi, %eax
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	movq	%rdx, %rcx
	addq	$1, %rdx
	shrl	$7, %esi
	jne	.L240
	andl	$127, %eax
	subq	%r12, %rdx
	movb	%al, (%rcx)
	movq	24(%rbx), %r14
	leaq	(%rdx,%r14), %r13
	cmpq	32(%rbx), %r13
	ja	.L241
.L244:
	movq	%r13, 24(%rbx)
	testq	%rdx, %rdx
	jne	.L249
.L235:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L250
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	addq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%r13, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L238
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L241:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-56(%rbp), %rdx
	testb	%al, %al
	jne	.L244
	jmp	.L235
.L250:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18720:
	.size	_ZN2v88internal15ValueSerializer8WriteSmiENS0_3SmiE, .-_ZN2v88internal15ValueSerializer8WriteSmiENS0_3SmiE
	.section	.text._ZN2v88internal15ValueSerializer15WriteHeapNumberENS0_10HeapNumberE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer15WriteHeapNumberENS0_10HeapNumberE
	.type	_ZN2v88internal15ValueSerializer15WriteHeapNumberENS0_10HeapNumberE, @function
_ZN2v88internal15ValueSerializer15WriteHeapNumberENS0_10HeapNumberE:
.LFB18721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	24(%rdi), %r14
	movq	%rdi, %rbx
	leaq	1(%r14), %r13
	cmpq	32(%rdi), %r13
	ja	.L252
.L254:
	movq	16(%rbx), %rax
	movq	%r13, 24(%rbx)
	movb	$78, (%rax,%r14)
.L253:
	movq	24(%rbx), %r13
	movq	7(%r12), %r14
	leaq	8(%r13), %r12
	cmpq	32(%rbx), %r12
	ja	.L256
.L258:
	movq	16(%rbx), %rax
	movq	%r12, 24(%rbx)
	movq	%r14, (%rax,%r13)
.L251:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movq	%r13, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L254
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L258
	jmp	.L251
	.cfi_endproc
.LFE18721:
	.size	_ZN2v88internal15ValueSerializer15WriteHeapNumberENS0_10HeapNumberE, .-_ZN2v88internal15ValueSerializer15WriteHeapNumberENS0_10HeapNumberE
	.section	.text._ZN2v88internal15ValueSerializer11WriteBigIntENS0_6BigIntE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer11WriteBigIntENS0_6BigIntE
	.type	_ZN2v88internal15ValueSerializer11WriteBigIntENS0_6BigIntE, @function
_ZN2v88internal15ValueSerializer11WriteBigIntENS0_6BigIntE:
.LFB18722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	24(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	1(%r14), %r13
	cmpq	32(%rdi), %r13
	ja	.L262
.L264:
	movq	16(%rbx), %rax
	movq	%r13, 24(%rbx)
	movb	$90, (%rax,%r14)
.L263:
	leaq	-72(%rbp), %r14
	movq	%r12, -72(%rbp)
	leaq	-61(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNK2v88internal6BigInt27GetBitfieldForSerializationEv@PLT
	movl	%eax, %edi
	movl	%eax, %r12d
	call	_ZN2v88internal6BigInt27DigitsByteLengthForBitfieldEj@PLT
	movq	%r15, %rdx
	movslq	%eax, %r13
	.p2align 4,,10
	.p2align 3
.L266:
	movl	%r12d, %ecx
	movl	%r12d, %eax
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	movq	%rdx, %rcx
	addq	$1, %rdx
	shrl	$7, %r12d
	jne	.L266
	andl	$127, %eax
	subq	%r15, %rdx
	movb	%al, (%rcx)
	movq	24(%rbx), %rcx
	leaq	(%rdx,%rcx), %r12
	cmpq	32(%rbx), %r12
	ja	.L267
.L270:
	movq	%r12, 24(%rbx)
	testq	%rdx, %rdx
	jne	.L281
.L272:
	addq	%r12, %r13
	cmpq	32(%rbx), %r13
	ja	.L273
.L275:
	movq	16(%rbx), %rsi
	movq	%r13, 24(%rbx)
	movq	%r14, %rdi
	addq	%r12, %rsi
	call	_ZN2v88internal6BigInt15SerializeDigitsEPh@PLT
.L261:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L282
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	addq	%rcx, %rdi
	call	memcpy@PLT
	movq	24(%rbx), %r12
	addq	%r12, %r13
	cmpq	32(%rbx), %r13
	jbe	.L275
	.p2align 4,,10
	.p2align 3
.L273:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L275
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%r13, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L264
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rcx
	testb	%al, %al
	jne	.L270
	movq	24(%rbx), %r12
	jmp	.L272
.L282:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18722:
	.size	_ZN2v88internal15ValueSerializer11WriteBigIntENS0_6BigIntE, .-_ZN2v88internal15ValueSerializer11WriteBigIntENS0_6BigIntE
	.section	.text._ZN2v88internal15ValueSerializer11WriteJSDateENS0_6JSDateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer11WriteJSDateENS0_6JSDateE
	.type	_ZN2v88internal15ValueSerializer11WriteJSDateENS0_6JSDateE, @function
_ZN2v88internal15ValueSerializer11WriteJSDateENS0_6JSDateE:
.LFB18728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	24(%rdi), %r14
	leaq	1(%r14), %r13
	cmpq	32(%rdi), %r13
	ja	.L284
.L286:
	movq	16(%rbx), %rax
	movq	%r13, 24(%rbx)
	movb	$68, (%rax,%r14)
.L285:
	movq	23(%r12), %rax
	testb	$1, %al
	jne	.L288
	movq	24(%rbx), %r13
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	leaq	8(%r13), %r12
	cmpq	32(%rbx), %r12
	ja	.L290
.L292:
	movq	16(%rbx), %rax
	movq	%r12, 24(%rbx)
	movq	%xmm0, (%rax,%r13)
.L283:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	movq	24(%rbx), %r13
	movsd	7(%rax), %xmm0
	leaq	8(%r13), %r12
	cmpq	32(%rbx), %r12
	jbe	.L292
.L290:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movsd	%xmm0, -40(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movsd	-40(%rbp), %xmm0
	testb	%al, %al
	jne	.L292
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L284:
	movq	%r13, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L286
	jmp	.L285
	.cfi_endproc
.LFE18728:
	.size	_ZN2v88internal15ValueSerializer11WriteJSDateENS0_6JSDateE, .-_ZN2v88internal15ValueSerializer11WriteJSDateENS0_6JSDateE
	.section	.text._ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE:
.LFB18746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L296
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*16(%rax)
.L297:
	movq	(%rbx), %rdi
	movq	12552(%rdi), %rax
	cmpq	%rax, 96(%rdi)
	jne	.L303
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	movq	(%rbx), %r13
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	1607(%rax), %r14
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L298
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L299:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6StringEEE@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L298:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L304
.L300:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L300
	.cfi_endproc
.LFE18746:
	.size	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateE
	.type	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateE, @function
_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateE:
.LFB18744:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	128(%rax), %rdx
	jmp	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	.cfi_endproc
.LFE18744:
	.size	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateE, .-_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateE
	.section	.text._ZN2v88internal15ValueSerializer18ThrowIfOutOfMemoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer18ThrowIfOutOfMemoryEv
	.type	_ZN2v88internal15ValueSerializer18ThrowIfOutOfMemoryEv, @function
_ZN2v88internal15ValueSerializer18ThrowIfOutOfMemoryEv:
.LFB18745:
	.cfi_startproc
	endbr64
	cmpb	$0, 41(%rdi)
	movl	$257, %eax
	jne	.L313
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$359, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%eax, -4(%rbp)
	movq	(%rdi), %rax
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	xorb	%al, %al
	movb	$0, %ah
	ret
	.cfi_endproc
.LFE18745:
	.size	_ZN2v88internal15ValueSerializer18ThrowIfOutOfMemoryEv, .-_ZN2v88internal15ValueSerializer18ThrowIfOutOfMemoryEv
	.section	.text._ZN2v88internal15ValueSerializer15WriteHostObjectENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer15WriteHostObjectENS0_6HandleINS0_8JSObjectEEE
	.type	_ZN2v88internal15ValueSerializer15WriteHostObjectENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal15ValueSerializer15WriteHostObjectENS0_6HandleINS0_8JSObjectEEE:
.LFB18742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	24(%rdi), %r13
	leaq	1(%r13), %rbx
	cmpq	32(%rdi), %rbx
	ja	.L315
.L317:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$92, (%rax,%r13)
.L316:
	movq	8(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L328
	movq	(%rdi), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	*24(%rax)
	movq	(%r12), %rdi
	movq	12552(%rdi), %rax
	cmpq	%rax, 96(%rdi)
	je	.L324
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	movb	$0, %ah
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	jne	.L329
.L325:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	movb	%dl, %al
	popq	%r13
	popq	%r14
	movb	$1, %ah
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	leaq	128(%rdi), %rdx
	movl	$359, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%edx, %edx
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L328:
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	1607(%rax), %r12
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L320
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L321:
	movq	%r14, %rcx
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$358, %edx
	call	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_@PLT
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	movb	$0, %ah
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movq	%rbx, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L317
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L320:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L330
.L322:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r12, (%rsi)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L322
	.cfi_endproc
.LFE18742:
	.size	_ZN2v88internal15ValueSerializer15WriteHostObjectENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal15ValueSerializer15WriteHostObjectENS0_6HandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal17ValueDeserializerC2EPNS0_7IsolateENS0_6VectorIKhEEPNS_17ValueDeserializer8DelegateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializerC2EPNS0_7IsolateENS0_6VectorIKhEEPNS_17ValueDeserializer8DelegateE
	.type	_ZN2v88internal17ValueDeserializerC2EPNS0_7IsolateENS0_6VectorIKhEEPNS_17ValueDeserializer8DelegateE, @function
_ZN2v88internal17ValueDeserializerC2EPNS0_7IsolateENS0_6VectorIKhEEPNS_17ValueDeserializer8DelegateE:
.LFB18751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movslq	%ecx, %rax
	addq	%rdx, %rax
	movq	%rax, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%r8, -24(%rbp)
	cmpl	$102400, %ecx
	movq	$0, 36(%rdi)
	movb	$0, 44(%rdi)
	movhps	-24(%rbp), %xmm0
	setg	32(%rdi)
	movups	%xmm0, (%rdi)
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%rdi)
	movq	41152(%rsi), %rdi
	movq	288(%rsi), %rsi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	$0, 56(%rbx)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18751:
	.size	_ZN2v88internal17ValueDeserializerC2EPNS0_7IsolateENS0_6VectorIKhEEPNS_17ValueDeserializer8DelegateE, .-_ZN2v88internal17ValueDeserializerC2EPNS0_7IsolateENS0_6VectorIKhEEPNS_17ValueDeserializer8DelegateE
	.globl	_ZN2v88internal17ValueDeserializerC1EPNS0_7IsolateENS0_6VectorIKhEEPNS_17ValueDeserializer8DelegateE
	.set	_ZN2v88internal17ValueDeserializerC1EPNS0_7IsolateENS0_6VectorIKhEEPNS_17ValueDeserializer8DelegateE,_ZN2v88internal17ValueDeserializerC2EPNS0_7IsolateENS0_6VectorIKhEEPNS_17ValueDeserializer8DelegateE
	.section	.text._ZN2v88internal17ValueDeserializerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializerD2Ev
	.type	_ZN2v88internal17ValueDeserializerD2Ev, @function
_ZN2v88internal17ValueDeserializerD2Ev:
.LFB18754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %rdi
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L333
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18754:
	.size	_ZN2v88internal17ValueDeserializerD2Ev, .-_ZN2v88internal17ValueDeserializerD2Ev
	.globl	_ZN2v88internal17ValueDeserializerD1Ev
	.set	_ZN2v88internal17ValueDeserializerD1Ev,_ZN2v88internal17ValueDeserializerD2Ev
	.section	.text._ZN2v88internal17ValueDeserializer10ReadHeaderEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer10ReadHeaderEv
	.type	_ZN2v88internal17ValueDeserializer10ReadHeaderEv, @function
_ZN2v88internal17ValueDeserializer10ReadHeaderEv:
.LFB18756:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	cmpq	%r8, %rax
	jnb	.L337
	cmpb	$-1, (%rax)
	je	.L349
.L337:
	movl	$257, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	addq	$1, %rax
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, 16(%rdi)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L339:
	movzbl	(%rax), %esi
	cmpl	$31, %ecx
	ja	.L341
	movl	%esi, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r9d
.L341:
	addq	$1, %rax
	movq	%rax, 16(%rdi)
	testb	%sil, %sil
	jns	.L350
.L342:
	cmpq	%rax, %r8
	ja	.L339
.L343:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$363, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory8NewErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	movb	$0, %ah
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore 6
	.cfi_restore 12
	movl	%r9d, 36(%rdi)
	cmpl	$13, %r9d
	ja	.L343
	jmp	.L337
	.cfi_endproc
.LFE18756:
	.size	_ZN2v88internal17ValueDeserializer10ReadHeaderEv, .-_ZN2v88internal17ValueDeserializer10ReadHeaderEv
	.section	.text._ZNK2v88internal17ValueDeserializer7PeekTagEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17ValueDeserializer7PeekTagEv
	.type	_ZNK2v88internal17ValueDeserializer7PeekTagEv, @function
_ZNK2v88internal17ValueDeserializer7PeekTagEv:
.LFB18757:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L352:
	movzbl	(%rax), %edx
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L357
.L354:
	cmpq	%rax, %rcx
	ja	.L352
	xorl	%eax, %eax
	movb	$0, %ah
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	movl	$1, %eax
	movb	%dl, %ah
	ret
	.cfi_endproc
.LFE18757:
	.size	_ZNK2v88internal17ValueDeserializer7PeekTagEv, .-_ZNK2v88internal17ValueDeserializer7PeekTagEv
	.section	.text._ZN2v88internal17ValueDeserializer10ConsumeTagENS0_16SerializationTagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer10ConsumeTagENS0_16SerializationTagE
	.type	_ZN2v88internal17ValueDeserializer10ConsumeTagENS0_16SerializationTagE, @function
_ZN2v88internal17ValueDeserializer10ConsumeTagENS0_16SerializationTagE:
.LFB18758:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rcx
	movq	16(%rdi), %rax
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L365:
	movzbl	(%rax), %edx
	addq	$1, %rax
	movq	%rax, 16(%rdi)
	testb	%dl, %dl
	jne	.L364
.L360:
	cmpq	%rax, %rcx
	ja	.L365
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.p2align 4,,10
	.p2align 3
.L364:
	ret
	.cfi_endproc
.LFE18758:
	.size	_ZN2v88internal17ValueDeserializer10ConsumeTagENS0_16SerializationTagE, .-_ZN2v88internal17ValueDeserializer10ConsumeTagENS0_16SerializationTagE
	.section	.text._ZN2v88internal17ValueDeserializer7ReadTagEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer7ReadTagEv
	.type	_ZN2v88internal17ValueDeserializer7ReadTagEv, @function
_ZN2v88internal17ValueDeserializer7ReadTagEv:
.LFB18759:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rcx
	movq	16(%rdi), %rax
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L367:
	movzbl	(%rax), %edx
	addq	$1, %rax
	movq	%rax, 16(%rdi)
	testb	%dl, %dl
	jne	.L372
.L369:
	cmpq	%rax, %rcx
	ja	.L367
	xorl	%eax, %eax
	movb	$0, %ah
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	movl	$1, %eax
	movb	%dl, %ah
	ret
	.cfi_endproc
.LFE18759:
	.size	_ZN2v88internal17ValueDeserializer7ReadTagEv, .-_ZN2v88internal17ValueDeserializer7ReadTagEv
	.section	.text._ZN2v88internal17ValueDeserializer10ReadDoubleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer10ReadDoubleEv
	.type	_ZN2v88internal17ValueDeserializer10ReadDoubleEv, @function
_ZN2v88internal17ValueDeserializer10ReadDoubleEv:
.LFB18762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	24(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	jbe	.L374
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
.L375:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L379
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	movq	(%rax), %rdx
	addq	$8, %rax
	movq	%rax, 16(%rdi)
	movq	%rdx, %xmm0
	movq	%rdx, -16(%rbp)
	ucomisd	%xmm0, %xmm0
	jp	.L380
.L376:
	movsd	-16(%rbp), %xmm0
	movl	$1, %eax
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L380:
	movq	.LC5(%rip), %rax
	movq	%rax, -16(%rbp)
	jmp	.L376
.L379:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18762:
	.size	_ZN2v88internal17ValueDeserializer10ReadDoubleEv, .-_ZN2v88internal17ValueDeserializer10ReadDoubleEv
	.section	.text._ZN2v88internal17ValueDeserializer12ReadRawBytesEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer12ReadRawBytesEi
	.type	_ZN2v88internal17ValueDeserializer12ReadRawBytesEi, @function
_ZN2v88internal17ValueDeserializer12ReadRawBytesEi:
.LFB18763:
	.cfi_startproc
	endbr64
	movq	24(%rsi), %rcx
	movq	%rdi, %rax
	movq	16(%rsi), %rdi
	movslq	%edx, %rdx
	subq	%rdi, %rcx
	cmpq	%rcx, %rdx
	jle	.L382
	movb	$0, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	leaq	(%rdi,%rdx), %rcx
	movb	$1, (%rax)
	movq	%rcx, 16(%rsi)
	movq	%rdi, 8(%rax)
	movq	%rdx, 16(%rax)
	ret
	.cfi_endproc
.LFE18763:
	.size	_ZN2v88internal17ValueDeserializer12ReadRawBytesEi, .-_ZN2v88internal17ValueDeserializer12ReadRawBytesEi
	.section	.text._ZN2v88internal17ValueDeserializer10ReadUint32EPj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer10ReadUint32EPj
	.type	_ZN2v88internal17ValueDeserializer10ReadUint32EPj, @function
_ZN2v88internal17ValueDeserializer10ReadUint32EPj:
.LFB18764:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %r10
	movq	16(%rdi), %rax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L391:
	movzbl	(%rax), %r9d
	cmpl	$31, %ecx
	ja	.L386
	movl	%r9d, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r8d
.L386:
	addq	$1, %rax
	movq	%rax, 16(%rdi)
	testb	%r9b, %r9b
	jns	.L390
.L387:
	cmpq	%rax, %r10
	ja	.L391
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	movl	%r8d, (%rsi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE18764:
	.size	_ZN2v88internal17ValueDeserializer10ReadUint32EPj, .-_ZN2v88internal17ValueDeserializer10ReadUint32EPj
	.section	.text._ZN2v88internal17ValueDeserializer10ReadUint64EPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer10ReadUint64EPm
	.type	_ZN2v88internal17ValueDeserializer10ReadUint64EPm, @function
_ZN2v88internal17ValueDeserializer10ReadUint64EPm:
.LFB18765:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %r10
	movq	16(%rdi), %rax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L399:
	movzbl	(%rax), %r9d
	cmpl	$63, %ecx
	ja	.L394
	movq	%r9, %rdx
	andl	$127, %edx
	salq	%cl, %rdx
	addl	$7, %ecx
	orq	%rdx, %r8
.L394:
	addq	$1, %rax
	movq	%rax, 16(%rdi)
	testb	%r9b, %r9b
	jns	.L398
.L395:
	cmpq	%rax, %r10
	ja	.L399
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%r8, (%rsi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE18765:
	.size	_ZN2v88internal17ValueDeserializer10ReadUint64EPm, .-_ZN2v88internal17ValueDeserializer10ReadUint64EPm
	.section	.text._ZN2v88internal17ValueDeserializer10ReadDoubleEPd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer10ReadDoubleEPd
	.type	_ZN2v88internal17ValueDeserializer10ReadDoubleEPd, @function
_ZN2v88internal17ValueDeserializer10ReadDoubleEPd:
.LFB18766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	24(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	ja	.L400
	movq	(%rax), %rdx
	addq	$8, %rax
	movq	%rax, 16(%rdi)
	movq	%rdx, %xmm0
	movq	%rdx, -16(%rbp)
	ucomisd	%xmm0, %xmm0
	jp	.L407
.L402:
	movsd	-16(%rbp), %xmm0
	movl	$1, %r8d
	movsd	%xmm0, (%rsi)
.L400:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L408
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	movq	.LC5(%rip), %rax
	movq	%rax, -16(%rbp)
	jmp	.L402
.L408:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18766:
	.size	_ZN2v88internal17ValueDeserializer10ReadDoubleEPd, .-_ZN2v88internal17ValueDeserializer10ReadDoubleEPd
	.section	.text._ZN2v88internal17ValueDeserializer12ReadRawBytesEmPPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer12ReadRawBytesEmPPKv
	.type	_ZN2v88internal17ValueDeserializer12ReadRawBytesEmPPKv, @function
_ZN2v88internal17ValueDeserializer12ReadRawBytesEmPPKv:
.LFB18767:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	xorl	%r8d, %r8d
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L409
	addq	%rax, %rsi
	movq	%rax, (%rdx)
	movl	$1, %r8d
	movq	%rsi, 16(%rdi)
.L409:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE18767:
	.size	_ZN2v88internal17ValueDeserializer12ReadRawBytesEmPPKv, .-_ZN2v88internal17ValueDeserializer12ReadRawBytesEmPPKv
	.section	.rodata._ZN2v88internal17ValueDeserializer19TransferArrayBufferEjNS0_6HandleINS0_13JSArrayBufferEEE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal17ValueDeserializer19TransferArrayBufferEjNS0_6HandleINS0_13JSArrayBufferEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer19TransferArrayBufferEjNS0_6HandleINS0_13JSArrayBufferEEE
	.type	_ZN2v88internal17ValueDeserializer19TransferArrayBufferEjNS0_6HandleINS0_13JSArrayBufferEEE, @function
_ZN2v88internal17ValueDeserializer19TransferArrayBufferEjNS0_6HandleINS0_13JSArrayBufferEEE:
.LFB18768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	56(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L420
.L413:
	movq	(%rbx), %rdi
	movq	%r13, %rcx
	movl	%r14d, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal22SimpleNumberDictionary3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE@PLT
	movq	%rax, %r13
	cmpq	%r12, %rax
	je	.L412
	testq	%rax, %rax
	je	.L415
	movq	(%r12), %rax
	cmpq	%rax, 0(%r13)
	je	.L412
.L415:
	movq	%r12, %rdi
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	0(%r13), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 56(%rbx)
.L412:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	movq	(%rdi), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	41152(%rdi), %r12
	call	_ZN2v88internal9HashTableINS0_22SimpleNumberDictionaryENS0_27SimpleNumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	(%rax), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 56(%rbx)
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L413
	leaq	.LC7(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18768:
	.size	_ZN2v88internal17ValueDeserializer19TransferArrayBufferEjNS0_6HandleINS0_13JSArrayBufferEEE, .-_ZN2v88internal17ValueDeserializer19TransferArrayBufferEjNS0_6HandleINS0_13JSArrayBufferEEE
	.section	.text._ZN2v88internal17ValueDeserializer10ReadBigIntEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer10ReadBigIntEv
	.type	_ZN2v88internal17ValueDeserializer10ReadBigIntEv, @function
_ZN2v88internal17ValueDeserializer10ReadBigIntEv:
.LFB18775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	movq	16(%rbx), %rax
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L430:
	movzbl	(%rax), %esi
	cmpl	$31, %ecx
	ja	.L424
	movl	%esi, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r12d
.L424:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%sil, %sil
	jns	.L429
.L425:
	cmpq	%rax, %rdi
	ja	.L430
.L423:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	movl	%r12d, %edi
	call	_ZN2v88internal6BigInt27DigitsByteLengthForBitfieldEj@PLT
	movq	16(%rbx), %rdx
	movslq	%eax, %rcx
	movq	24(%rbx), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rcx
	jg	.L423
	leaq	(%rdx,%rcx), %rax
	movzbl	32(%rbx), %r8d
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	movq	%rax, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6BigInt20FromSerializedDigitsEPNS0_7IsolateEjNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	.cfi_endproc
.LFE18775:
	.size	_ZN2v88internal17ValueDeserializer10ReadBigIntEv, .-_ZN2v88internal17ValueDeserializer10ReadBigIntEv
	.section	.text._ZN2v88internal17ValueDeserializer14ReadUtf8StringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer14ReadUtf8StringEv
	.type	_ZN2v88internal17ValueDeserializer14ReadUtf8StringEv, @function
_ZN2v88internal17ValueDeserializer14ReadUtf8StringEv:
.LFB18782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	24(%rdi), %r9
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L440:
	movzbl	(%rax), %r8d
	cmpl	$31, %ecx
	ja	.L433
	movl	%r8d, %esi
	andl	$127, %esi
	sall	%cl, %esi
	addl	$7, %ecx
	orl	%esi, %edx
.L433:
	addq	$1, %rax
	movq	%rax, 16(%rdi)
	testb	%r8b, %r8b
	jns	.L439
.L434:
	cmpq	%rax, %r9
	ja	.L440
.L432:
	xorl	%eax, %eax
.L435:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L441
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	testl	%edx, %edx
	js	.L432
	movslq	%edx, %rdx
	subq	%rax, %r9
	cmpq	%r9, %rdx
	jg	.L432
	leaq	(%rax,%rdx), %rcx
	movzbl	32(%rdi), %r9d
	movq	(%rdi), %r8
	movq	%rdx, -24(%rbp)
	movq	%rcx, 16(%rdi)
	leaq	-32(%rbp), %rsi
	movl	%r9d, %edx
	movq	%r8, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	jmp	.L435
.L441:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18782:
	.size	_ZN2v88internal17ValueDeserializer14ReadUtf8StringEv, .-_ZN2v88internal17ValueDeserializer14ReadUtf8StringEv
	.section	.text._ZN2v88internal17ValueDeserializer17ReadOneByteStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer17ReadOneByteStringEv
	.type	_ZN2v88internal17ValueDeserializer17ReadOneByteStringEv, @function
_ZN2v88internal17ValueDeserializer17ReadOneByteStringEv:
.LFB18783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	24(%rdi), %r9
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	movq	16(%rdi), %rax
	movq	$0, -24(%rbp)
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L451:
	movzbl	(%rax), %r8d
	cmpl	$31, %ecx
	ja	.L444
	movl	%r8d, %esi
	andl	$127, %esi
	sall	%cl, %esi
	addl	$7, %ecx
	orl	%esi, %edx
.L444:
	addq	$1, %rax
	movq	%rax, 16(%rdi)
	testb	%r8b, %r8b
	jns	.L450
.L445:
	cmpq	%rax, %r9
	ja	.L451
.L443:
	xorl	%eax, %eax
.L446:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L452
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	testl	%edx, %edx
	js	.L443
	movslq	%edx, %rdx
	subq	%rax, %r9
	cmpq	%r9, %rdx
	jg	.L443
	leaq	(%rax,%rdx), %rcx
	movq	%rdx, -24(%rbp)
	movzbl	32(%rdi), %edx
	leaq	-32(%rbp), %rsi
	movq	%rcx, 16(%rdi)
	movq	(%rdi), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	jmp	.L446
.L452:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18783:
	.size	_ZN2v88internal17ValueDeserializer17ReadOneByteStringEv, .-_ZN2v88internal17ValueDeserializer17ReadOneByteStringEv
	.section	.text._ZN2v88internal17ValueDeserializer17ReadTwoByteStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer17ReadTwoByteStringEv
	.type	_ZN2v88internal17ValueDeserializer17ReadTwoByteStringEv, @function
_ZN2v88internal17ValueDeserializer17ReadTwoByteStringEv:
.LFB18784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %r8
	movq	16(%rdi), %r12
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L467:
	movzbl	(%r12), %edx
	cmpl	$31, %ecx
	ja	.L455
	movl	%edx, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %esi
.L455:
	addq	$1, %r12
	movq	%r12, 16(%rdi)
	testb	%dl, %dl
	jns	.L466
.L456:
	cmpq	%r12, %r8
	ja	.L467
.L465:
	xorl	%eax, %eax
.L459:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	testl	%esi, %esi
	js	.L465
	testb	$1, %sil
	jne	.L465
	movslq	%esi, %r13
	subq	%r12, %r8
	cmpq	%r8, %r13
	jg	.L465
	movq	(%rdi), %r8
	leaq	(%r12,%r13), %rax
	movq	%rax, 16(%rdi)
	leaq	128(%r8), %rax
	testl	%esi, %esi
	je	.L459
	movzbl	32(%rdi), %edx
	shrl	%esi
	movq	%r8, %rdi
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L465
	movq	(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	leaq	15(%rax), %rdi
	call	memcpy@PLT
	movq	%rbx, %rax
	jmp	.L459
	.cfi_endproc
.LFE18784:
	.size	_ZN2v88internal17ValueDeserializer17ReadTwoByteStringEv, .-_ZN2v88internal17ValueDeserializer17ReadTwoByteStringEv
	.section	.text._ZN2v88internal17ValueDeserializer18ReadExpectedStringENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer18ReadExpectedStringENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal17ValueDeserializer18ReadExpectedStringENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal17ValueDeserializer18ReadExpectedStringENS0_6HandleINS0_6StringEEE:
.LFB18785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%r15, %r12
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L516:
	movzbl	(%r12), %r13d
	addq	$1, %r12
	movq	%r12, 16(%rbx)
	testb	%r13b, %r13b
	jne	.L515
.L470:
	cmpq	%r12, %rax
	ja	.L516
.L477:
	movq	%r15, 16(%rbx)
	xorl	%r13d, %r13d
.L468:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L517
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L519:
	movzbl	(%r12), %edi
	cmpl	$31, %ecx
	ja	.L471
	movl	%edi, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r14d
.L471:
	addq	$1, %r12
	movq	%r12, 16(%rbx)
	testb	%dil, %dil
	jns	.L518
.L472:
	cmpq	%r12, %rax
	ja	.L519
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L518:
	testl	%r14d, %r14d
	js	.L477
	movslq	%r14d, %rdx
	subq	%r12, %rax
	cmpq	%rax, %rdx
	jg	.L477
	addq	%r12, %rdx
	leaq	-64(%rbp), %rdi
	movq	%rdx, 16(%rbx)
	movq	(%rsi), %rax
	leaq	-65(%rbp), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%rax, %rsi
	movq	%rdx, %rax
	movq	%rdx, %rcx
	shrq	$32, %rax
	cmpb	$34, %r13b
	je	.L520
	cmpb	$99, %r13b
	je	.L521
	cmpl	$1, %eax
	sete	%dl
	cmpb	$83, %r13b
	sete	%al
	andb	%al, %dl
	movl	%edx, %r13d
	je	.L477
	movslq	%ecx, %rax
	movl	%r14d, %edx
	cmpq	%rax, %rdx
	jne	.L477
	leaq	(%rsi,%rdx), %r8
	cmpq	$7, %rdx
	jbe	.L493
	movq	%rsi, %rdi
	testb	$7, %sil
	jne	.L484
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L482:
	addq	$1, %rdi
	testb	$7, %dil
	je	.L481
.L484:
	cmpb	$0, (%rdi)
	jns	.L482
	movl	%edi, %eax
	subl	%esi, %eax
.L483:
	cmpl	%eax, %ecx
	jg	.L477
	movq	%r12, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L477
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L521:
	cmpl	$2, %eax
	jne	.L477
	movl	%ecx, %eax
	movl	%r14d, %edx
	addq	%rax, %rax
	cmpq	%rax, %rdx
	jne	.L477
.L514:
	movq	%r12, %rdi
	movl	$1, %r13d
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L477
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L520:
	cmpl	$1, %eax
	jne	.L477
	movl	%r14d, %edx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rdx
	jne	.L477
	jmp	.L514
.L481:
	movabsq	$-9187201950435737472, %r9
	jmp	.L488
.L485:
	testq	%r9, -8(%rdi)
	jne	.L486
.L488:
	movq	%rdi, %rax
	addq	$8, %rdi
	cmpq	%rdi, %r8
	jnb	.L485
.L480:
	cmpq	%rax, %r8
	ja	.L487
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L489:
	addq	$1, %rax
	cmpq	%rax, %r8
	je	.L486
.L487:
	cmpb	$0, (%rax)
	jns	.L489
.L486:
	subl	%esi, %eax
	jmp	.L483
.L493:
	movq	%rsi, %rax
	jmp	.L480
.L517:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18785:
	.size	_ZN2v88internal17ValueDeserializer18ReadExpectedStringENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal17ValueDeserializer18ReadExpectedStringENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal17ValueDeserializer10ReadJSDateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer10ReadJSDateEv
	.type	_ZN2v88internal17ValueDeserializer10ReadJSDateEv, @function
_ZN2v88internal17ValueDeserializer10ReadJSDateEv:
.LFB18795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	leaq	-8(%rax), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	ja	.L532
	movq	(%rdx), %rax
	addq	$8, %rdx
	movq	%rdi, %rbx
	movq	%rdx, 16(%rdi)
	movq	%rax, %xmm0
	movq	%rax, -48(%rbp)
	ucomisd	%xmm0, %xmm0
	jp	.L537
.L524:
	movl	40(%rbx), %r13d
	movq	(%rbx), %r12
	movsd	-48(%rbp), %xmm0
	leal	1(%r13), %eax
	movl	%eax, 40(%rbx)
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	327(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L525
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movsd	-56(%rbp), %xmm0
	movq	%rax, %r14
.L526:
	movq	(%rbx), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	327(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L528
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movsd	-56(%rbp), %xmm0
	movq	%rax, %rdi
.L529:
	movq	%r14, %rsi
	call	_ZN2v88internal6JSDate3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEEd@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L532
	movq	(%rbx), %rdi
	movq	48(%rbx), %rsi
	movl	%r13d, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %r13
	cmpq	%rdi, %rax
	je	.L533
	testq	%rax, %rax
	je	.L534
	testq	%rdi, %rdi
	je	.L534
	movq	(%rdi), %rax
	cmpq	%rax, 0(%r13)
	je	.L533
.L534:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	0(%r13), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
.L533:
	movq	%r12, %rax
.L532:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L538
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L539
.L527:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L537:
	movq	.LC5(%rip), %rax
	movq	%rax, -48(%rbp)
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L528:
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L540
.L530:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L539:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movsd	-56(%rbp), %xmm0
	movq	%rax, %r14
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L540:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movsd	-56(%rbp), %xmm0
	movq	%rax, %rdi
	jmp	.L530
.L538:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18795:
	.size	_ZN2v88internal17ValueDeserializer10ReadJSDateEv, .-_ZN2v88internal17ValueDeserializer10ReadJSDateEv
	.section	.text._ZN2v88internal17ValueDeserializer17ReadJSArrayBufferEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer17ReadJSArrayBufferEb
	.type	_ZN2v88internal17ValueDeserializer17ReadJSArrayBufferEb, @function
_ZN2v88internal17ValueDeserializer17ReadJSArrayBufferEb:
.LFB18815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	40(%rdi), %r12d
	movq	24(%rdi), %rdx
	leal	1(%r12), %eax
	movl	%eax, 40(%rdi)
	movq	16(%rdi), %rax
	testb	%sil, %sil
	je	.L560
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L543:
	movzbl	(%rax), %edi
	cmpl	$31, %ecx
	ja	.L545
	movl	%edi, %esi
	andl	$127, %esi
	sall	%cl, %esi
	addl	$7, %ecx
	orl	%esi, %r8d
.L545:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%dil, %dil
	jns	.L576
.L546:
	cmpq	%rdx, %rax
	jb	.L543
.L575:
	movq	(%rbx), %rdi
.L544:
	movq	12552(%rdi), %rax
	cmpq	%rax, 96(%rdi)
	jne	.L577
.L549:
	xorl	%eax, %eax
.L572:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L579:
	movzbl	(%rax), %edi
	cmpl	$31, %ecx
	ja	.L554
	movl	%edi, %esi
	andl	$127, %esi
	sall	%cl, %esi
	addl	$7, %ecx
	orl	%esi, %r13d
.L554:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%dil, %dil
	jns	.L578
.L542:
	cmpq	%rax, %rdx
	ja	.L579
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L578:
	movl	%r13d, %r15d
	subq	%rax, %rdx
	cmpq	%rdx, %r15
	ja	.L549
	movzbl	32(%rbx), %edx
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal13JSArrayBuffer19SetupAllocatingDataENS0_6HandleIS1_EEPNS0_7IsolateEmbNS0_10SharedFlagE@PLT
	testb	%al, %al
	je	.L549
	movq	16(%rbx), %rsi
	testl	%r13d, %r13d
	jne	.L580
.L556:
	addq	%r15, %rsi
	movq	(%rbx), %rdi
	movl	%r12d, %edx
	xorl	%r8d, %r8d
	movq	%rsi, 16(%rbx)
	movq	48(%rbx), %rsi
	movq	%r14, %rcx
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %r12
	cmpq	%rdi, %rax
	je	.L557
	testq	%rax, %rax
	je	.L558
	testq	%rdi, %rdi
	je	.L558
	movq	(%rdi), %rax
	cmpq	%rax, (%r12)
	je	.L557
.L558:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	(%r12), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
.L557:
	movq	%r14, %rax
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L576:
	movq	8(%rbx), %r9
	movq	(%rbx), %rdi
	testq	%r9, %r9
	je	.L544
	movq	(%r9), %rax
	movq	%rdi, %rsi
	movl	%r8d, %edx
	movq	%r9, %rdi
	call	*32(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L575
	movq	(%rbx), %rdi
	movq	48(%rbx), %rsi
	movl	%r12d, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %r12
	cmpq	%rdi, %rax
	je	.L551
	testq	%r12, %r12
	je	.L550
	testq	%rdi, %rdi
	je	.L550
	movq	(%rdi), %rax
	cmpq	%rax, (%r12)
	je	.L551
.L550:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	(%r12), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
.L551:
	movq	%r13, %rax
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L577:
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	%r15, %rdx
	movq	31(%rax), %rdi
	call	memcpy@PLT
	movq	16(%rbx), %rsi
	jmp	.L556
	.cfi_endproc
.LFE18815:
	.size	_ZN2v88internal17ValueDeserializer17ReadJSArrayBufferEb, .-_ZN2v88internal17ValueDeserializer17ReadJSArrayBufferEb
	.section	.text._ZN2v88internal17ValueDeserializer28ReadTransferredJSArrayBufferEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer28ReadTransferredJSArrayBufferEv
	.type	_ZN2v88internal17ValueDeserializer28ReadTransferredJSArrayBufferEv, @function
_ZN2v88internal17ValueDeserializer28ReadTransferredJSArrayBufferEv:
.LFB18819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	40(%rdi), %r13d
	leal	1(%r13), %eax
	movl	%eax, 40(%rdi)
	movq	24(%rdi), %rdi
	movq	16(%rbx), %rax
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L609:
	movzbl	(%rax), %esi
	cmpl	$31, %ecx
	ja	.L583
	movl	%esi, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r12d
.L583:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%sil, %sil
	jns	.L608
.L584:
	cmpq	%rax, %rdi
	ja	.L609
.L591:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L608:
	.cfi_restore_state
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.L591
	movq	(%rbx), %r15
	movq	(%r14), %rdx
	movl	%r12d, %edi
	movq	1176(%r15), %rax
	movq	%rdx, -56(%rbp)
	movq	15(%rax), %rsi
	call	_Z11halfsiphashjm@PLT
	movq	-56(%rbp), %rdx
	movq	88(%r15), %r10
	movl	$1, %ecx
	movq	96(%r15), %r9
	movslq	35(%rdx), %rdi
	leaq	-1(%rdx), %rsi
	subl	$1, %edi
	andl	%edi, %eax
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L610:
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	cvttsd2siq	%xmm0, %rdx
	cmpl	%edx, %r12d
	je	.L589
.L586:
	addl	%ecx, %eax
	addl	$1, %ecx
	andl	%edi, %eax
.L590:
	leal	(%rax,%rax), %r8d
	leal	40(,%r8,8), %edx
	movslq	%edx, %rdx
	movq	(%rdx,%rsi), %rdx
	cmpq	%rdx, %r10
	je	.L591
	cmpq	%rdx, %r9
	je	.L586
	testb	$1, %dl
	je	.L610
	movsd	7(%rdx), %xmm0
	cvttsd2siq	%xmm0, %rdx
	cmpl	%edx, %r12d
	jne	.L586
.L589:
	cmpl	$-1, %eax
	je	.L591
	movq	(%rbx), %r15
	movq	(%r14), %rdx
	leal	48(,%r8,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L594
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L595:
	movq	(%rbx), %rdi
	movq	48(%rbx), %rsi
	movl	%r13d, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %r13
	cmpq	%rdi, %rax
	je	.L597
	testq	%rdi, %rdi
	je	.L598
	testq	%rax, %rax
	je	.L598
	movq	(%rdi), %rax
	cmpq	%rax, 0(%r13)
	je	.L597
.L598:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	0(%r13), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
.L597:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L594:
	.cfi_restore_state
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L611
.L596:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L611:
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L596
	.cfi_endproc
.LFE18819:
	.size	_ZN2v88internal17ValueDeserializer28ReadTransferredJSArrayBufferEv, .-_ZN2v88internal17ValueDeserializer28ReadTransferredJSArrayBufferEv
	.section	.text._ZN2v88internal17ValueDeserializer21ReadJSArrayBufferViewENS0_6HandleINS0_13JSArrayBufferEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer21ReadJSArrayBufferViewENS0_6HandleINS0_13JSArrayBufferEEE
	.type	_ZN2v88internal17ValueDeserializer21ReadJSArrayBufferViewENS0_6HandleINS0_13JSArrayBufferEEE, @function
_ZN2v88internal17ValueDeserializer21ReadJSArrayBufferViewENS0_6HandleINS0_13JSArrayBufferEEE:
.LFB18820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	24(%rdi), %rdi
	movq	23(%rax), %r10
	movq	16(%rbx), %rax
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L613:
	movzbl	(%rax), %r9d
	cmpl	$7, %ecx
	ja	.L615
	movl	%r9d, %r8d
	andl	$127, %r8d
	sall	%cl, %r8d
	addl	$7, %ecx
	orl	%r8d, %edx
.L615:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%r9b, %r9b
	jns	.L654
.L616:
	cmpq	%rax, %rdi
	ja	.L613
.L622:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L654:
	.cfi_restore_state
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L656:
	movzbl	(%rax), %r11d
	cmpl	$31, %ecx
	ja	.L618
	movl	%r11d, %r8d
	andl	$127, %r8d
	sall	%cl, %r8d
	addl	$7, %ecx
	orl	%r8d, %r9d
.L618:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%r11b, %r11b
	jns	.L655
.L619:
	cmpq	%rax, %rdi
	ja	.L656
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L655:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L658:
	movzbl	(%rax), %r12d
	cmpl	$31, %ecx
	ja	.L620
	movl	%r12d, %r11d
	andl	$127, %r11d
	sall	%cl, %r11d
	addl	$7, %ecx
	orl	%r11d, %r8d
.L620:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%r12b, %r12b
	jns	.L657
.L621:
	cmpq	%rax, %rdi
	ja	.L658
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L657:
	cmpl	%r9d, %r10d
	jb	.L622
	subl	%r9d, %r10d
	cmpl	%r8d, %r10d
	jb	.L622
	movl	40(%rbx), %r12d
	subl	$63, %edx
	leal	1(%r12), %eax
	movl	%eax, 40(%rbx)
	cmpb	$56, %dl
	ja	.L622
	leaq	.L624(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal17ValueDeserializer21ReadJSArrayBufferViewENS0_6HandleINS0_13JSArrayBufferEEE,"a",@progbits
	.align 4
	.align 4
.L624:
	.long	.L635-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L634-.L624
	.long	.L641-.L624
	.long	.L632-.L624
	.long	.L622-.L624
	.long	.L642-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L630-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L629-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L643-.L624
	.long	.L622-.L624
	.long	.L627-.L624
	.long	.L622-.L624
	.long	.L626-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L625-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L622-.L624
	.long	.L623-.L624
	.section	.text._ZN2v88internal17ValueDeserializer21ReadJSArrayBufferViewENS0_6HandleINS0_13JSArrayBufferEEE
.L623:
	movl	$2, %ecx
	movl	$3, %r10d
	.p2align 4,,10
	.p2align 3
.L631:
	movl	%r9d, %eax
	xorl	%edx, %edx
	divl	%ecx
	testl	%edx, %edx
	jne	.L622
	movl	%r8d, %eax
	xorl	%edx, %edx
	divl	%ecx
	testl	%edx, %edx
	jne	.L622
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L625:
	movl	$8, %ecx
	movl	$10, %r10d
	jmp	.L631
.L626:
	movl	$4, %ecx
	movl	$7, %r10d
	jmp	.L631
.L627:
	movl	$4, %ecx
	movl	$5, %r10d
	jmp	.L631
.L629:
	movl	$2, %ecx
	movl	$4, %r10d
	jmp	.L631
.L632:
	movl	$4, %ecx
	movl	$6, %r10d
	jmp	.L631
.L635:
	movq	(%rbx), %rdi
	movl	%r8d, %ecx
	movl	%r9d, %edx
	xorl	%r8d, %r8d
	call	_ZN2v88internal7Factory13NewJSDataViewENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	48(%rbx), %rsi
	movl	%r12d, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	movq	%rax, %r13
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %r12
	cmpq	%rdi, %rax
	je	.L638
	testq	%rax, %rax
	je	.L639
	testq	%rdi, %rdi
	je	.L639
.L653:
	movq	(%rdi), %rax
	cmpq	%rax, (%r12)
	je	.L638
.L639:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	(%r12), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
.L638:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L641:
	.cfi_restore_state
	movl	$1, %ecx
	movl	$9, %r10d
.L633:
	movl	%r8d, %eax
	xorl	%edx, %edx
	divl	%ecx
	movl	%eax, %r8d
	.p2align 4,,10
	.p2align 3
.L640:
	movl	%r9d, %ecx
	movq	(%rbx), %rdi
	movzbl	32(%rbx), %r9d
	movq	%rsi, %rdx
	movl	%r10d, %esi
	call	_ZN2v88internal7Factory15NewJSTypedArrayENS0_17ExternalArrayTypeENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	48(%rbx), %rsi
	movl	%r12d, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	movq	%rax, %r13
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %r12
	cmpq	%rdi, %rax
	je	.L638
	testq	%rdi, %rdi
	je	.L639
	testq	%rax, %rax
	jne	.L653
	jmp	.L639
.L643:
	movl	$1, %r10d
	jmp	.L640
.L642:
	movl	$8, %ecx
	movl	$8, %r10d
	jmp	.L631
.L634:
	movl	$2, %r10d
	jmp	.L640
.L630:
	movl	$8, %ecx
	movl	$11, %r10d
	jmp	.L631
	.cfi_endproc
.LFE18820:
	.size	_ZN2v88internal17ValueDeserializer21ReadJSArrayBufferViewENS0_6HandleINS0_13JSArrayBufferEEE, .-_ZN2v88internal17ValueDeserializer21ReadJSArrayBufferViewENS0_6HandleINS0_13JSArrayBufferEEE
	.section	.text._ZN2v88internal17ValueDeserializer22ReadWasmModuleTransferEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer22ReadWasmModuleTransferEv
	.type	_ZN2v88internal17ValueDeserializer22ReadWasmModuleTransferEv, @function
_ZN2v88internal17ValueDeserializer22ReadWasmModuleTransferEv:
.LFB18825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	call	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE@PLT
	movq	%rax, -45(%rbp)
	movzbl	%dh, %eax
	movb	%al, -36(%rbp)
	movq	%rdx, %rax
	shrq	$16, %rax
	movb	%dl, -37(%rbp)
	movb	%al, -35(%rbp)
	movq	%rdx, %rax
	shrq	$32, %rdx
	shrq	$24, %rax
	cmpb	$0, _ZN2v88internal36FLAG_wasm_disable_structured_cloningE(%rip)
	movb	%dl, -33(%rbp)
	movb	%al, -34(%rbp)
	je	.L660
	cmpb	$0, -43(%rbp)
	jne	.L660
.L661:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	.cfi_restore_state
	cmpb	$0, 44(%rbx)
	jne	.L661
	movq	24(%rbx), %rdi
	movq	16(%rbx), %rax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L682:
	movzbl	(%rax), %esi
	cmpl	$31, %ecx
	ja	.L665
	movl	%esi, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r8d
.L665:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%sil, %sil
	jns	.L681
.L666:
	cmpq	%rdi, %rax
	jb	.L682
.L664:
	movq	(%rbx), %rdi
.L667:
	movq	12552(%rdi), %rax
	xorl	%r12d, %r12d
	cmpq	%rax, 96(%rdi)
	jne	.L683
.L671:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L681:
	.cfi_restore_state
	movq	8(%rbx), %r9
	movq	(%rbx), %rdi
	testq	%r9, %r9
	je	.L667
	movq	(%r9), %rax
	movq	%rdi, %rsi
	movl	%r8d, %edx
	movq	%r9, %rdi
	call	*24(%rax)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L664
	movl	40(%rbx), %edx
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	48(%rbx), %rsi
	leal	1(%rdx), %eax
	movl	%eax, 40(%rbx)
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %r13
	cmpq	%rdi, %rax
	je	.L671
	testq	%rdi, %rdi
	je	.L669
	testq	%r13, %r13
	je	.L669
	movq	(%rdi), %rax
	cmpq	%rax, 0(%r13)
	je	.L671
.L669:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	0(%r13), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L683:
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L671
	.cfi_endproc
.LFE18825:
	.size	_ZN2v88internal17ValueDeserializer22ReadWasmModuleTransferEv, .-_ZN2v88internal17ValueDeserializer22ReadWasmModuleTransferEv
	.section	.text._ZN2v88internal17ValueDeserializer14ReadWasmMemoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer14ReadWasmMemoryEv
	.type	_ZN2v88internal17ValueDeserializer14ReadWasmMemoryEv, @function
_ZN2v88internal17ValueDeserializer14ReadWasmMemoryEv:
.LFB18827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	40(%rdi), %r12d
	leal	1(%r12), %eax
	movl	%eax, 40(%rdi)
	movq	(%rdi), %rdi
	call	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE@PLT
	movzbl	%dh, %ecx
	movb	%dl, -37(%rbp)
	movb	%cl, -36(%rbp)
	movq	%rdx, %rcx
	shrq	$16, %rcx
	movq	%rax, -45(%rbp)
	movb	%cl, -35(%rbp)
	movq	%rdx, %rcx
	shrq	$32, %rdx
	shrq	$24, %rcx
	movb	%dl, -33(%rbp)
	movb	%cl, -34(%rbp)
	testl	$16711680, %eax
	jne	.L685
.L704:
	xorl	%eax, %eax
.L700:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L685:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	movq	16(%rbx), %rax
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L687:
	movzbl	(%rax), %esi
	cmpl	$31, %ecx
	ja	.L688
	movl	%esi, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r13d
.L688:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%sil, %sil
	jns	.L692
.L689:
	cmpq	%rax, %rdi
	ja	.L687
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L706:
	movzbl	(%rax), %edx
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%dl, %dl
	jne	.L705
.L692:
	cmpq	%rdi, %rax
	jb	.L706
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L705:
	cmpb	$117, %dl
	jne	.L704
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal17ValueDeserializer17ReadJSArrayBufferEb
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L704
	movl	%r13d, %edx
	movq	(%rbx), %rdi
	shrl	%r13d
	andl	$1, %edx
	negl	%edx
	xorl	%r13d, %edx
	call	_ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEEj@PLT
	movq	(%rbx), %rdx
	movq	%rax, %rsi
	movq	%rax, %r13
	movq	45752(%rdx), %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker26RegisterWasmMemoryAsSharedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE@PLT
	movq	(%rbx), %rdi
	movq	48(%rbx), %rsi
	movl	%r12d, %edx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %r12
	cmpq	%rdi, %rax
	je	.L696
	testq	%rax, %rax
	je	.L697
	testq	%rdi, %rdi
	je	.L697
	movq	(%rdi), %rax
	cmpq	%rax, (%r12)
	je	.L696
.L697:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	(%r12), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
.L696:
	movq	%r13, %rax
	jmp	.L700
	.cfi_endproc
.LFE18827:
	.size	_ZN2v88internal17ValueDeserializer14ReadWasmMemoryEv, .-_ZN2v88internal17ValueDeserializer14ReadWasmMemoryEv
	.section	.text._ZN2v88internal17ValueDeserializer14ReadHostObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer14ReadHostObjectEv
	.type	_ZN2v88internal17ValueDeserializer14ReadHostObjectEv, @function
_ZN2v88internal17ValueDeserializer14ReadHostObjectEv:
.LFB18831:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rdi)
	je	.L724
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r12), %rax
	jb	.L725
	movl	40(%rbx), %r13d
	movq	8(%rbx), %rdi
	movq	(%rbx), %rsi
	leal	1(%r13), %eax
	movl	%eax, 40(%rbx)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L726
	movq	(%rbx), %rdi
	movq	48(%rbx), %rsi
	movl	%r13d, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %r13
	cmpq	%rdi, %rax
	je	.L713
	testq	%r13, %r13
	je	.L714
	testq	%rdi, %rdi
	je	.L714
	movq	(%rdi), %rax
	cmpq	%rax, 0(%r13)
	je	.L713
.L714:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	0(%r13), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
.L713:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L724:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L725:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L726:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	12552(%rdi), %rax
	cmpq	%rax, 96(%rdi)
	je	.L713
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L713
	.cfi_endproc
.LFE18831:
	.size	_ZN2v88internal17ValueDeserializer14ReadHostObjectEv, .-_ZN2v88internal17ValueDeserializer14ReadHostObjectEv
	.section	.text._ZN2v88internal17ValueDeserializer15HasObjectWithIDEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer15HasObjectWithIDEj
	.type	_ZN2v88internal17ValueDeserializer15HasObjectWithIDEj, @function
_ZN2v88internal17ValueDeserializer15HasObjectWithIDEj:
.LFB18844:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	(%rax), %rdx
	xorl	%eax, %eax
	cmpl	%esi, 11(%rdx)
	ja	.L730
	ret
	.p2align 4,,10
	.p2align 3
.L730:
	leal	16(,%rsi,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	movq	(%rdi), %rdx
	cmpq	%rax, 96(%rdx)
	setne	%al
	ret
	.cfi_endproc
.LFE18844:
	.size	_ZN2v88internal17ValueDeserializer15HasObjectWithIDEj, .-_ZN2v88internal17ValueDeserializer15HasObjectWithIDEj
	.section	.text._ZN2v88internal17ValueDeserializer15GetObjectWithIDEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer15GetObjectWithIDEj
	.type	_ZN2v88internal17ValueDeserializer15GetObjectWithIDEj, @function
_ZN2v88internal17ValueDeserializer15GetObjectWithIDEj:
.LFB18845:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	(%rax), %rax
	cmpl	%esi, 11(%rax)
	ja	.L732
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L732:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	16(,%rsi,8), %edx
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	-1(%rax,%rdx), %rsi
	movq	(%rdi), %rbx
	xorl	%eax, %eax
	cmpq	%rsi, 96(%rbx)
	je	.L733
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L735
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L733:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L735:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L741
.L737:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L741:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L737
	.cfi_endproc
.LFE18845:
	.size	_ZN2v88internal17ValueDeserializer15GetObjectWithIDEj, .-_ZN2v88internal17ValueDeserializer15GetObjectWithIDEj
	.section	.text._ZN2v88internal17ValueDeserializer15AddObjectWithIDEjNS0_6HandleINS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer15AddObjectWithIDEjNS0_6HandleINS0_10JSReceiverEEE
	.type	_ZN2v88internal17ValueDeserializer15AddObjectWithIDEjNS0_6HandleINS0_10JSReceiverEEE, @function
_ZN2v88internal17ValueDeserializer15AddObjectWithIDEjNS0_6HandleINS0_10JSReceiverEEE:
.LFB18849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r9d
	movq	%rdx, %rcx
	xorl	%r8d, %r8d
	movl	%r9d, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	48(%rdi), %rsi
	movq	(%rdi), %rdi
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L742
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L744
	testq	%rax, %rax
	je	.L744
	movq	(%rdi), %rax
	cmpq	%rax, (%r12)
	je	.L742
.L744:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	(%r12), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
.L742:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18849:
	.size	_ZN2v88internal17ValueDeserializer15AddObjectWithIDEjNS0_6HandleINS0_10JSReceiverEEE, .-_ZN2v88internal17ValueDeserializer15AddObjectWithIDEjNS0_6HandleINS0_10JSReceiverEEE
	.section	.rodata._ZN2v88internal17ValueDeserializer14ReadWasmModuleEv.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"ValueDeserializer::ReadWasmModule"
	.section	.text._ZN2v88internal17ValueDeserializer14ReadWasmModuleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer14ReadWasmModuleEv
	.type	_ZN2v88internal17ValueDeserializer14ReadWasmModuleEv, @function
_ZN2v88internal17ValueDeserializer14ReadWasmModuleEv:
.LFB18826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE@PLT
	movw	%dx, -130(%rbp)
	movq	%rax, -138(%rbp)
	movq	%rdx, %rax
	shrq	$32, %rdx
	shrq	$16, %rax
	cmpb	$0, _ZN2v88internal36FLAG_wasm_disable_structured_cloningE(%rip)
	movb	%dl, -126(%rbp)
	movw	%ax, -128(%rbp)
	je	.L747
	cmpb	$0, -136(%rbp)
	jne	.L747
.L752:
	xorl	%eax, %eax
.L750:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L773
	subq	$-128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L747:
	.cfi_restore_state
	cmpb	$0, 44(%r12)
	je	.L752
	movq	24(%r12), %rdx
	movq	16(%r12), %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	testq	%rcx, %rcx
	jle	.L752
	leaq	1(%rax), %rbx
	movq	%rbx, 16(%r12)
	cmpb	$121, (%rax)
	jne	.L752
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L775:
	movzbl	(%rbx), %esi
	cmpl	$31, %ecx
	ja	.L753
	movl	%esi, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %edi
.L753:
	addq	$1, %rbx
	movq	%rbx, 16(%r12)
	testb	%sil, %sil
	jns	.L774
.L754:
	cmpq	%rbx, %rdx
	ja	.L775
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L774:
	testl	%edi, %edi
	js	.L752
	movq	%rdx, %rax
	movslq	%edi, %r13
	subq	%rbx, %rax
	cmpq	%rax, %r13
	jg	.L752
	leaq	(%rbx,%r13), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsi, 16(%r12)
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L777:
	movzbl	(%rsi), %edi
	cmpl	$31, %ecx
	ja	.L757
	movl	%edi, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r8d
.L757:
	addq	$1, %rsi
	movq	%rsi, 16(%r12)
	testb	%dil, %dil
	jns	.L776
.L758:
	cmpq	%rsi, %rdx
	ja	.L777
	jmp	.L752
.L776:
	testl	%r8d, %r8d
	js	.L752
	movslq	%r8d, %r9
	subq	%rsi, %rdx
	cmpq	%rdx, %r9
	jg	.L752
	leaq	(%rsi,%r9), %rax
	movq	(%r12), %rdi
	movq	%rbx, %rcx
	movq	%r13, %r8
	movq	%rax, 16(%r12)
	movq	%r9, %rdx
	call	_ZN2v88internal4wasm23DeserializeNativeModuleEPNS0_7IsolateENS0_6VectorIKhEES6_@PLT
	testq	%rax, %rax
	je	.L778
	movl	40(%r12), %esi
	leal	1(%rsi), %edx
	movl	%edx, 40(%r12)
.L762:
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal17ValueDeserializer15AddObjectWithIDEjNS0_6HandleINS0_10JSReceiverEEE
	movq	-152(%rbp), %rax
	jmp	.L750
.L773:
	call	__stack_chk_fail@PLT
.L778:
	movq	(%r12), %rdi
	movb	$0, -56(%rbp)
	leaq	-96(%rbp), %r14
	leaq	.LC8(%rip), %rax
	movq	%rax, -88(%rbp)
	leaq	-56(%rbp), %rax
	movq	%rdi, -96(%rbp)
	movq	%rax, -72(%rbp)
	movl	$0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE@PLT
	movq	(%r12), %rsi
	movq	%r14, %rcx
	movq	%rbx, -112(%rbp)
	movq	%rax, %r8
	movb	%dl, -117(%rbp)
	movq	%rdx, %rax
	movzbl	%dh, %edx
	movb	%dl, -116(%rbp)
	movq	%rax, %rdx
	movq	45752(%rsi), %rdi
	shrq	$16, %rdx
	movq	%r8, -125(%rbp)
	leaq	-112(%rbp), %r8
	movb	%dl, -115(%rbp)
	movq	%rax, %rdx
	shrq	$32, %rax
	shrq	$24, %rdx
	movb	%al, -113(%rbp)
	movb	%dl, -114(%rbp)
	leaq	-125(%rbp), %rdx
	movq	%r13, -104(%rbp)
	call	_ZN2v88internal4wasm10WasmEngine11SyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesE@PLT
	movq	%r14, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal4wasm12ErrorThrowerD1Ev@PLT
	movl	40(%r12), %esi
	movq	-152(%rbp), %rax
	leal	1(%rsi), %edx
	movl	%edx, 40(%r12)
	testq	%rax, %rax
	je	.L750
	jmp	.L762
	.cfi_endproc
.LFE18826:
	.size	_ZN2v88internal17ValueDeserializer14ReadWasmModuleEv, .-_ZN2v88internal17ValueDeserializer14ReadWasmModuleEv
	.section	.text._ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_,"axG",@progbits,_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	.type	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_, @function
_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_:
.LFB21052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-45(%rbp), %r12
	movq	%r12, %rdx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L780:
	movl	%esi, %eax
	movl	%esi, %ecx
	orl	$-128, %eax
	movb	%al, (%rdx)
	movq	%rdx, %rax
	addq	$1, %rdx
	shrl	$7, %esi
	jne	.L780
	andl	$127, %ecx
	subq	%r12, %rdx
	movb	%cl, (%rax)
	movq	24(%rdi), %r14
	leaq	(%rdx,%r14), %r13
	cmpq	32(%rdi), %r13
	ja	.L781
.L784:
	movq	%r13, 24(%rdi)
	testq	%rdx, %rdx
	jne	.L789
.L779:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L790
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L789:
	.cfi_restore_state
	addq	16(%rdi), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L781:
	movq	%r13, %rsi
	movq	%rdx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rdx
	testb	%al, %al
	jne	.L784
	jmp	.L779
.L790:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21052:
	.size	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_, .-_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	.section	.text._ZN2v88internal15ValueSerializer19WriteBigIntContentsENS0_6BigIntE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer19WriteBigIntContentsENS0_6BigIntE
	.type	_ZN2v88internal15ValueSerializer19WriteBigIntContentsENS0_6BigIntE, @function
_ZN2v88internal15ValueSerializer19WriteBigIntContentsENS0_6BigIntE:
.LFB18706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-40(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%rsi, -40(%rbp)
	call	_ZNK2v88internal6BigInt27GetBitfieldForSerializationEv@PLT
	movl	%eax, %r13d
	movl	%eax, %edi
	call	_ZN2v88internal6BigInt27DigitsByteLengthForBitfieldEj@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movslq	%eax, %rbx
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	movq	24(%r12), %r13
	addq	%r13, %rbx
	cmpq	32(%r12), %rbx
	ja	.L792
.L794:
	movq	16(%r12), %rsi
	movq	%rbx, 24(%r12)
	movq	%r14, %rdi
	addq	%r13, %rsi
	call	_ZN2v88internal6BigInt15SerializeDigitsEPh@PLT
.L791:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L792:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L794
	jmp	.L791
	.cfi_endproc
.LFE18706:
	.size	_ZN2v88internal15ValueSerializer19WriteBigIntContentsENS0_6BigIntE, .-_ZN2v88internal15ValueSerializer19WriteBigIntContentsENS0_6BigIntE
	.section	.text._ZN2v88internal15ValueSerializer18WriteJSArrayBufferENS0_6HandleINS0_13JSArrayBufferEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer18WriteJSArrayBufferENS0_6HandleINS0_13JSArrayBufferEEE
	.type	_ZN2v88internal15ValueSerializer18WriteJSArrayBufferENS0_6HandleINS0_13JSArrayBufferEEE, @function
_ZN2v88internal15ValueSerializer18WriteJSArrayBufferENS0_6HandleINS0_13JSArrayBufferEEE:
.LFB18733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rsi
	movl	39(%rsi), %eax
	testb	$8, %al
	je	.L798
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L838
	movq	(%rdi), %rax
	movq	%r13, %rdx
	movq	(%r12), %rsi
	call	*32(%rax)
	movq	(%r12), %rdi
	movq	%rax, %r13
	movq	12552(%rdi), %rax
	cmpq	%rax, 96(%rdi)
	je	.L801
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	movb	$0, %ah
.L808:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L798:
	.cfi_restore_state
	leaq	192(%rdi), %rdi
	call	_ZNK2v88internal15IdentityMapBase9FindEntryEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L809
	movq	24(%r12), %r14
	leaq	1(%r14), %r13
	cmpq	32(%r12), %r13
	ja	.L810
.L812:
	movq	16(%r12), %rax
	movq	%r13, 24(%r12)
	movb	$116, (%rax,%r14)
.L811:
	movl	(%rbx), %esi
.L841:
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	jne	.L839
.L831:
	xorl	%eax, %eax
	addq	$24, %rsp
	movb	%dl, %al
	popq	%rbx
	popq	%r12
	movb	$1, %ah
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	movq	24(%r12), %r14
	leaq	1(%r14), %rbx
	cmpq	32(%r12), %rbx
	ja	.L802
.L804:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$117, (%rax,%r14)
.L803:
	testb	%r13b, %r13b
	je	.L842
.L806:
	movq	%r13, %rsi
	shrq	$32, %rsi
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L809:
	movq	0(%r13), %rax
	movl	39(%rax), %edx
	andl	$4, %edx
	jne	.L843
	movq	23(%rax), %rax
	testq	%rax, %rax
	js	.L816
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L817:
	comisd	.LC9(%rip), %xmm0
	ja	.L838
	movq	24(%r12), %r14
	leaq	1(%r14), %rbx
	cmpq	32(%r12), %rbx
	ja	.L820
.L822:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$66, (%rax,%r14)
.L821:
	cvttsd2siq	%xmm0, %r14
	movq	%r12, %rdi
	movsd	%xmm0, -56(%rbp)
	movl	%r14d, %esi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	movsd	.LC10(%rip), %xmm1
	movsd	-56(%rbp), %xmm0
	comisd	%xmm1, %xmm0
	jb	.L825
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %r14
	btcq	$63, %r14
.L825:
	movq	0(%r13), %rax
	movq	24(%r12), %r13
	movq	31(%rax), %r15
	leaq	(%r14,%r13), %rbx
	cmpq	32(%r12), %rbx
	ja	.L826
.L829:
	movq	%rbx, 24(%r12)
	testq	%r14, %r14
	jne	.L844
.L830:
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	je	.L831
	.p2align 4,,10
	.p2align 3
.L839:
	movq	(%r12), %rax
	movl	$359, %esi
	movq	%r12, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%edx, %edx
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L838:
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$358, %esi
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	movb	$0, %ah
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L843:
	.cfi_restore_state
	movq	(%r12), %rax
	movl	$360, %esi
	movq	%r12, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L810:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L812
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L816:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L842:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L802:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L804
	jmp	.L803
.L844:
	movq	16(%r12), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	addq	%r13, %rdi
	call	memcpy@PLT
	jmp	.L830
.L820:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movsd	-56(%rbp), %xmm0
	testb	%al, %al
	jne	.L822
	jmp	.L821
.L826:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L829
	jmp	.L830
	.cfi_endproc
.LFE18733:
	.size	_ZN2v88internal15ValueSerializer18WriteJSArrayBufferENS0_6HandleINS0_13JSArrayBufferEEE, .-_ZN2v88internal15ValueSerializer18WriteJSArrayBufferENS0_6HandleINS0_13JSArrayBufferEEE
	.section	.text._ZN2v88internal15ValueSerializer22WriteJSArrayBufferViewENS0_17JSArrayBufferViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer22WriteJSArrayBufferViewENS0_17JSArrayBufferViewE
	.type	_ZN2v88internal15ValueSerializer22WriteJSArrayBufferViewENS0_17JSArrayBufferViewE, @function
_ZN2v88internal15ValueSerializer22WriteJSArrayBufferViewENS0_17JSArrayBufferViewE:
.LFB18734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 40(%rdi)
	je	.L846
	movq	(%rdi), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L847
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L848:
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer15WriteHostObjectENS0_6HandleINS0_8JSObjectEEE
.L850:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L866
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L847:
	.cfi_restore_state
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L867
.L849:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L846:
	movq	24(%rdi), %r14
	leaq	1(%r14), %rbx
	cmpq	32(%rdi), %rbx
	ja	.L851
.L853:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$86, (%rax,%r14)
.L852:
	movq	-1(%r13), %rax
	movl	$63, %r15d
	cmpw	$1086, 11(%rax)
	je	.L868
.L855:
	movq	24(%r12), %r14
	leaq	1(%r14), %rbx
	cmpq	32(%r12), %rbx
	ja	.L856
.L858:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	%r15b, (%rax,%r14)
.L857:
	movl	31(%r13), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	movq	39(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	jne	.L869
.L860:
	xorl	%eax, %eax
	movb	%dl, %al
	movb	$1, %ah
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L869:
	movq	(%r12), %rax
	movl	$359, %esi
	movq	%r12, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%edx, %edx
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L868:
	leaq	-64(%rbp), %rdi
	movq	%r13, -64(%rbp)
	movl	$98, %r15d
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	subl	$2, %eax
	cmpl	$9, %eax
	ja	.L855
	leaq	CSWTCH.2527(%rip), %rdx
	movzbl	(%rdx,%rax), %r15d
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L867:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L851:
	movq	%rbx, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L853
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L856:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L858
	jmp	.L857
.L866:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18734:
	.size	_ZN2v88internal15ValueSerializer22WriteJSArrayBufferViewENS0_17JSArrayBufferViewE, .-_ZN2v88internal15ValueSerializer22WriteJSArrayBufferViewENS0_17JSArrayBufferViewE
	.section	.rodata._ZN2v88internal15ValueSerializer15WriteWasmModuleENS0_6HandleINS0_16WasmModuleObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"std::numeric_limits<uint32_t>::max() >= module_size"
	.section	.text._ZN2v88internal15ValueSerializer15WriteWasmModuleENS0_6HandleINS0_16WasmModuleObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer15WriteWasmModuleENS0_6HandleINS0_16WasmModuleObjectEEE
	.type	_ZN2v88internal15ValueSerializer15WriteWasmModuleENS0_6HandleINS0_16WasmModuleObjectEEE, @function
_ZN2v88internal15ValueSerializer15WriteWasmModuleENS0_6HandleINS0_16WasmModuleObjectEEE:
.LFB18736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L871
	movq	(%rdi), %rax
	movq	%rsi, %rdx
	movq	(%r12), %rsi
	call	*40(%rax)
	movq	(%r12), %rdi
	movq	12552(%rdi), %rcx
	cmpq	%rcx, 96(%rdi)
	jne	.L908
	movq	24(%r12), %r14
	movq	32(%r12), %rdx
	leaq	1(%r14), %r13
	testb	%al, %al
	je	.L900
	shrq	$32, %rax
	movq	%rax, %rbx
	cmpq	%r13, %rdx
	jb	.L875
.L877:
	movq	16(%r12), %rax
	movq	%r13, 24(%r12)
	movb	$119, (%rax,%r14)
.L876:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	movl	$257, %eax
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L908:
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	movb	$0, %ah
.L879:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L909
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L871:
	.cfi_restore_state
	movq	24(%r12), %r14
	movq	32(%r12), %rdx
	leaq	1(%r14), %r13
.L900:
	cmpq	%rdx, %r13
	ja	.L880
.L882:
	movq	16(%r12), %rax
	movq	%r13, 24(%r12)
	movb	$87, (%rax,%r14)
.L881:
	movq	24(%r12), %r14
	leaq	1(%r14), %r13
	cmpq	32(%r12), %r13
	ja	.L884
.L886:
	movq	16(%r12), %rax
	movq	%r13, 24(%r12)
	movb	$121, (%rax,%r14)
.L885:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r15
	movq	232(%r15), %rax
	movq	8(%rax), %r13
	movq	(%rax), %r8
	movl	%r13d, %esi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	movq	24(%r12), %rbx
	movq	-104(%rbp), %r8
	leaq	0(%r13,%rbx), %r14
	cmpq	32(%r12), %r14
	ja	.L888
.L890:
	movq	%r14, 24(%r12)
	addq	16(%r12), %rbx
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
.L889:
	leaq	-96(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm14WasmSerializerC1EPNS1_12NativeModuleE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal4wasm14WasmSerializer29GetSerializedNativeModuleSizeEv@PLT
	movq	%rax, %r13
	movl	$4294967295, %eax
	cmpq	%rax, %r13
	ja	.L910
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	movq	24(%r12), %rbx
	leaq	0(%r13,%rbx), %r15
	cmpq	32(%r12), %r15
	ja	.L893
.L896:
	movq	%r15, 24(%r12)
	addq	16(%r12), %rbx
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZNK2v88internal4wasm14WasmSerializer21SerializeNativeModuleENS0_6VectorIhEE@PLT
	testb	%al, %al
	je	.L911
.L894:
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	jne	.L912
.L899:
	xorl	%eax, %eax
	movb	%dl, %al
	movb	$1, %ah
.L898:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L879
	movl	%eax, -104(%rbp)
	call	_ZdlPv@PLT
	movl	-104(%rbp), %eax
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L911:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L912:
	movq	(%r12), %rax
	movl	$359, %esi
	movq	%r12, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%edx, %edx
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L910:
	leaq	.LC11(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L880:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L882
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L884:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L886
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L888:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-104(%rbp), %r8
	testb	%al, %al
	jne	.L890
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L893:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L896
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L875:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L877
	jmp	.L876
.L909:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18736:
	.size	_ZN2v88internal15ValueSerializer15WriteWasmModuleENS0_6HandleINS0_16WasmModuleObjectEEE, .-_ZN2v88internal15ValueSerializer15WriteWasmModuleENS0_6HandleINS0_16WasmModuleObjectEEE
	.section	.text._ZN2v88internal15ValueSerializer11WriteStringENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer11WriteStringENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal15ValueSerializer11WriteStringENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal15ValueSerializer11WriteStringENS0_6HandleINS0_6StringEEE:
.LFB18723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rsi), %rdx
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	%rdx, %r12
	movq	-1(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L965
.L915:
	movq	-1(%r12), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L924
.L962:
	movq	(%rax), %rsi
.L923:
	movq	%rsi, -64(%rbp)
	leaq	-64(%rbp), %rdi
	leaq	-65(%rbp), %rsi
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%rax, %r13
	movq	%rdx, %rax
	shrq	$32, %rax
	cmpl	$1, %eax
	je	.L966
	cmpl	$2, %eax
	jne	.L940
	leal	(%rdx,%rdx), %r14d
	movq	24(%rbx), %r8
	movslq	%edx, %r12
	xorl	%eax, %eax
	movl	%r14d, %edx
	.p2align 4,,10
	.p2align 3
.L941:
	movq	%rax, %rcx
	addq	$1, %rax
	shrl	$7, %edx
	jne	.L941
	leaq	2(%r8,%rcx), %rdx
	movq	32(%rbx), %rax
	leaq	1(%r8), %r15
	andl	$1, %edx
	jne	.L967
.L942:
	cmpq	%rax, %r15
	ja	.L947
.L949:
	movq	16(%rbx), %rax
	movq	%r15, 24(%rbx)
	movb	$99, (%rax,%r8)
.L948:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	addq	%r12, %r12
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	movq	24(%rbx), %r15
	leaq	(%r12,%r15), %r14
	cmpq	32(%rbx), %r14
	ja	.L951
.L954:
	movq	%r14, 24(%rbx)
	testq	%r12, %r12
	jne	.L968
.L913:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L969
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L924:
	.cfi_restore_state
	movq	-1(%r12), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L962
	movq	(%rax), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L927
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L965:
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpw	$1, %cx
	jne	.L915
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L918
	movq	23(%rdx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L918
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L967:
	cmpq	%r15, %rax
	jb	.L943
.L945:
	movq	16(%rbx), %rax
	movq	%r15, 24(%rbx)
	movb	$0, (%rax,%r8)
.L944:
	movq	24(%rbx), %r8
	movq	32(%rbx), %rax
	leaq	1(%r8), %r15
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L966:
	movq	24(%rbx), %r15
	movslq	%edx, %r12
	leaq	1(%r15), %r14
	cmpq	32(%rbx), %r14
	ja	.L931
.L933:
	movq	16(%rbx), %rax
	movq	%r14, 24(%rbx)
	movb	$34, (%rax,%r15)
.L932:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	movq	24(%rbx), %r15
	leaq	(%r12,%r15), %r14
	cmpq	32(%rbx), %r14
	jbe	.L954
.L951:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L954
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L927:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L970
.L929:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L968:
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	addq	%r15, %rdi
	call	memcpy@PLT
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L918:
	movq	41112(%r13), %rdi
	movq	15(%rdx), %r12
	testq	%rdi, %rdi
	je	.L920
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L947:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-88(%rbp), %r8
	testb	%al, %al
	jne	.L949
	jmp	.L948
.L920:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L971
.L922:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r12, (%rax)
	jmp	.L915
.L931:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L933
	jmp	.L932
.L971:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L922
.L943:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-88(%rbp), %r8
	testb	%al, %al
	jne	.L945
	jmp	.L944
.L970:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L929
.L969:
	call	__stack_chk_fail@PLT
.L940:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18723:
	.size	_ZN2v88internal15ValueSerializer11WriteStringENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal15ValueSerializer11WriteStringENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal15ValueSerializer13WriteJSRegExpENS0_8JSRegExpE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer13WriteJSRegExpENS0_8JSRegExpE
	.type	_ZN2v88internal15ValueSerializer13WriteJSRegExpENS0_8JSRegExpE, @function
_ZN2v88internal15ValueSerializer13WriteJSRegExpENS0_8JSRegExpE:
.LFB18730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	24(%rdi), %r14
	movq	%rsi, %rbx
	leaq	1(%r14), %r13
	cmpq	32(%rdi), %r13
	ja	.L973
.L975:
	movq	16(%r12), %rax
	movq	%r13, 24(%r12)
	movb	$82, (%rax,%r14)
.L974:
	movq	(%r12), %r13
	movq	23(%rbx), %rax
	movq	23(%rax), %r14
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L977
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L978:
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteStringENS0_6HandleINS0_6StringEEE
	movq	23(%rbx), %rax
	movq	%r12, %rdi
	movq	31(%rax), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	sarq	$32, %rsi
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	.p2align 4,,10
	.p2align 3
.L977:
	.cfi_restore_state
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L981
.L979:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L973:
	movq	%r13, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L975
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L981:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L979
	.cfi_endproc
.LFE18730:
	.size	_ZN2v88internal15ValueSerializer13WriteJSRegExpENS0_8JSRegExpE, .-_ZN2v88internal15ValueSerializer13WriteJSRegExpENS0_8JSRegExpE
	.section	.text._ZN2v88internal15ValueSerializer23WriteJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer23WriteJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEE
	.type	_ZN2v88internal15ValueSerializer23WriteJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEE, @function
_ZN2v88internal15ValueSerializer23WriteJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEE:
.LFB18729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	23(%rax), %r13
	movq	(%rdi), %rax
	cmpq	%r13, 112(%rax)
	je	.L1025
	cmpq	%r13, 120(%rax)
	je	.L1026
	movq	%r13, %rax
	notq	%rax
	movl	%eax, %ebx
	andl	$1, %ebx
	je	.L993
.L996:
	movq	24(%r12), %r15
	leaq	1(%r15), %r14
	cmpq	32(%r12), %r14
	ja	.L1027
.L995:
	movq	16(%r12), %rax
	movq	%r14, 24(%r12)
	movb	$110, (%rax,%r15)
.L998:
	testb	%bl, %bl
	jne	.L1028
	movsd	7(%r13), %xmm0
.L1001:
	movq	24(%r12), %r13
	leaq	8(%r13), %rbx
	cmpq	32(%r12), %rbx
	ja	.L1002
.L1003:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movq	%xmm0, (%rax,%r13)
.L988:
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	jne	.L1029
.L1020:
	xorl	%eax, %eax
	movb	%dl, %al
	movb	$1, %ah
.L1012:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L993:
	.cfi_restore_state
	movq	-1(%r13), %rax
	cmpw	$65, 11(%rax)
	je	.L996
	movq	-1(%r13), %rax
	cmpw	$66, 11(%rax)
	je	.L1005
	movq	-1(%r13), %rax
	cmpw	$63, 11(%rax)
	ja	.L1030
	movq	24(%rdi), %r14
	leaq	1(%r14), %rbx
	cmpq	32(%rdi), %rbx
	ja	.L1013
.L1015:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$115, (%rax,%r14)
.L1014:
	movq	(%r12), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1017
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1018:
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteStringENS0_6HandleINS0_6StringEEE
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1028:
	sarq	$32, %r13
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1029:
	movq	(%r12), %rax
	movl	$359, %esi
	movq	%r12, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%edx, %edx
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1025:
	movq	24(%rdi), %r13
	leaq	1(%r13), %rbx
	cmpq	32(%rdi), %rbx
	ja	.L984
.L986:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$121, (%rax,%r13)
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1027:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L995
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1026:
	movq	24(%rdi), %r13
	leaq	1(%r13), %rbx
	cmpq	32(%rdi), %rbx
	ja	.L990
.L991:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$120, (%rax,%r13)
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movsd	-56(%rbp), %xmm0
	testb	%al, %al
	jne	.L1003
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1030:
	movq	%rsi, %rdx
	movl	$358, %esi
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L984:
	movq	%rbx, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L986
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	24(%rdi), %r14
	leaq	1(%r14), %rbx
	cmpq	32(%rdi), %rbx
	ja	.L1008
.L1010:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$122, (%rax,%r14)
.L1009:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer19WriteBigIntContentsENS0_6BigIntE
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L990:
	movq	%rbx, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L991
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L1031
.L1019:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	jmp	.L1018
.L1008:
	movq	%rbx, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1010
	jmp	.L1009
.L1013:
	movq	%rbx, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1015
	jmp	.L1014
.L1031:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1019
	.cfi_endproc
.LFE18729:
	.size	_ZN2v88internal15ValueSerializer23WriteJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEE, .-_ZN2v88internal15ValueSerializer23WriteJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEE
	.section	.text._ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_,"axG",@progbits,_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_
	.type	_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_, @function
_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_:
.LFB21152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-42(%rbp), %r12
	movq	%r12, %rdx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1040:
	movl	$1, %esi
.L1033:
	movl	%esi, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movq	%rdx, %rax
	addq	$1, %rdx
	testb	$-128, %sil
	jne	.L1040
	andl	$127, %esi
	subq	%r12, %rdx
	movb	%sil, (%rax)
	movq	24(%rdi), %r14
	leaq	(%rdx,%r14), %r13
	cmpq	32(%rdi), %r13
	ja	.L1034
.L1037:
	movq	%r13, 24(%rdi)
	testq	%rdx, %rdx
	jne	.L1042
.L1032:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1043
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1042:
	.cfi_restore_state
	addq	16(%rdi), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	%r13, %rsi
	movq	%rdx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rdx
	testb	%al, %al
	jne	.L1037
	jmp	.L1032
.L1043:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21152:
	.size	_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_, .-_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_
	.section	.rodata._ZN2v88internal15ValueSerializer12WriteJSErrorENS0_6HandleINS0_8JSObjectEEE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"!handle_.is_null()"
	.section	.text._ZN2v88internal15ValueSerializer12WriteJSErrorENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer12WriteJSErrorENS0_6HandleINS0_8JSObjectEEE
	.type	_ZN2v88internal15ValueSerializer12WriteJSErrorENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal15ValueSerializer12WriteJSErrorENS0_6HandleINS0_8JSObjectEEE:
.LFB18735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-192(%rbp), %rcx
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$184, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andb	$-64, -192(%rbp)
	leaq	2832(%rdi), %rdx
	movups	%xmm0, -184(%rbp)
	movups	%xmm0, -168(%rbp)
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorE@PLT
	movw	%ax, -218(%rbp)
	testb	%al, %al
	jne	.L1045
.L1195:
	xorl	%eax, %eax
	movb	$0, %ah
.L1046:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1196
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1045:
	.cfi_restore_state
	movq	24(%r12), %r13
	movl	%eax, %r14d
	leaq	1(%r13), %r15
	cmpq	32(%r12), %r15
	ja	.L1047
.L1049:
	movq	16(%r12), %rax
	movq	%r15, 24(%r12)
	movb	$114, (%rax,%r13)
.L1048:
	leaq	-144(%rbp), %rax
	movq	(%r12), %r13
	movq	%rbx, %r15
	movq	%rax, -216(%rbp)
	testq	%rbx, %rbx
	je	.L1197
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	(%r15), %rdx
	movq	-1(%rdx), %rax
	cmpw	$1026, 11(%rax)
	je	.L1198
	movq	-1(%rdx), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L1055:
	testb	%al, %al
	je	.L1056
	movq	41112(%r13), %rdi
	movq	12464(%r13), %r9
	testq	%rdi, %rdi
	je	.L1057
	movq	%r9, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1058:
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L1056
	leaq	104(%r13), %r15
.L1067:
	movq	(%r12), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1623(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1068
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	-1(%rsi), %rax
	movzbl	13(%rax), %eax
	testb	$1, %al
	je	.L1071
.L1215:
	movq	-1(%rsi), %rax
.L1184:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1199
.L1074:
	cmpq	%rax, (%r15)
	je	.L1200
	movq	(%r12), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1719(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1079
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1080:
	movq	-1(%rsi), %rax
	movzbl	13(%rax), %eax
	testb	$1, %al
	je	.L1082
	movq	-1(%rsi), %rax
.L1186:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1201
.L1085:
	cmpq	%rax, (%r15)
	je	.L1202
	movq	(%r12), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1727(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1089
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1090:
	movq	-1(%rsi), %rax
	movzbl	13(%rax), %eax
	testb	$1, %al
	je	.L1092
	movq	-1(%rsi), %rax
.L1188:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1203
.L1095:
	cmpq	%rax, (%r15)
	je	.L1204
	movq	(%r12), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1759(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1099
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1100:
	movq	-1(%rsi), %rax
	movzbl	13(%rax), %eax
	testb	$1, %al
	je	.L1102
	movq	-1(%rsi), %rax
.L1190:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1205
.L1105:
	cmpq	%rax, (%r15)
	je	.L1206
	movq	(%r12), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1767(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1109
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1110:
	movq	-1(%rsi), %rax
	movzbl	13(%rax), %eax
	testb	$1, %al
	je	.L1112
	movq	-1(%rsi), %rax
.L1192:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1207
.L1115:
	cmpq	%rax, (%r15)
	je	.L1208
	movq	(%r12), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1775(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1119
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1120:
	movq	-1(%rsi), %rax
	movzbl	13(%rax), %eax
	testb	$1, %al
	je	.L1122
	movq	-1(%rsi), %rax
.L1194:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1209
.L1125:
	cmpq	%rax, (%r15)
	jne	.L1078
	movl	$85, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	(%r15), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jne	.L1148
	movq	%r15, %rdi
	call	_ZN2v88internal7JSProxy12GetPrototypeENS0_6HandleIS1_EE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L1067
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1198:
	movq	%rdx, %rsi
	movq	-216(%rbp), %rdi
	movq	%rdx, -208(%rbp)
	andq	$-262144, %rsi
	movq	24(%rsi), %rax
	movq	%rsi, -200(%rbp)
	movq	-25128(%rax), %rax
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-200(%rbp), %rsi
	movq	-208(%rbp), %rdx
	movq	24(%rsi), %rsi
	testb	$1, %dl
	jne	.L1210
.L1053:
	movq	-1(%rdx), %rdx
	movq	23(%rdx), %rdx
.L1054:
	cmpq	%rax, %rdx
	setne	%al
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1148:
	movq	-1(%rax), %rax
	movl	%r14d, %edx
	movq	23(%rax), %rsi
	cmpq	104(%r13), %rsi
	je	.L1061
	cmpw	$1026, 11(%rax)
	setne	%dl
.L1061:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1211
	movb	%dl, -200(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-200(%rbp), %edx
	movq	%rax, %r15
.L1062:
	testb	%dl, %dl
	je	.L1051
	testq	%r15, %r15
	jne	.L1067
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L1212
.L1059:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r9, (%rsi)
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1211:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L1213
.L1063:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1214
.L1070:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	movq	-1(%rsi), %rax
	movzbl	13(%rax), %eax
	testb	$1, %al
	jne	.L1215
.L1071:
	movq	55(%rsi), %rax
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1074
	movq	23(%rax), %rax
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1212:
	movq	%r13, %rdi
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1210:
	movq	-1(%rdx), %rdi
	cmpw	$1024, 11(%rdi)
	jne	.L1053
	movq	-37488(%rsi), %rdx
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1200:
	movl	$69, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_
.L1078:
	movzwl	-218(%rbp), %eax
	shrw	$8, %ax
	je	.L1128
	movq	-184(%rbp), %r13
	testq	%r13, %r13
	je	.L1216
.L1129:
	movq	0(%r13), %rax
	movq	(%r12), %rdi
	testb	$1, %al
	jne	.L1130
.L1133:
	movq	%r13, %rsi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1195
.L1132:
	movl	$109, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteStringENS0_6HandleINS0_6StringEEE
.L1128:
	movq	(%r12), %r13
	movq	(%rbx), %rax
	leaq	3320(%r13), %r15
	testb	$1, %al
	jne	.L1134
.L1136:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r14
.L1135:
	movq	3320(%r13), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L1137
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L1137:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%r13, -120(%rbp)
	movq	3320(%r13), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1217
.L1138:
	leaq	-144(%rbp), %r13
	movq	%r15, -112(%rbp)
	movq	%r13, %rdi
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r14, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L1139
	movq	-120(%rbp), %rax
	leaq	88(%rax), %r13
.L1140:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1218
.L1143:
	movl	$46, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	jne	.L1219
.L1144:
	xorl	%eax, %eax
	movb	%dl, %al
	movb	$1, %ah
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1139:
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L1140
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	55(%rsi), %rax
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1085
	movq	23(%rax), %rax
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1079:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1220
.L1081:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1134:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1136
	movq	%rbx, %r14
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1219:
	movq	(%r12), %rax
	movl	$359, %esi
	movq	%r12, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%edx, %edx
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1218:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1143
	movq	%r12, %rdi
	movl	$115, %esi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteStringENS0_6HandleINS0_6StringEEE
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1202:
	movl	$82, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1092:
	movq	55(%rsi), %rax
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1095
	movq	23(%rax), %rax
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1221
.L1101:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1203:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1095
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1089:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1222
.L1091:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1216:
	testb	$32, -192(%rbp)
	je	.L1128
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1130:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1133
	jmp	.L1132
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1074
	jmp	.L1184
	.p2align 4,,10
	.p2align 3
.L1213:
	movq	%r13, %rdi
	movq	%rsi, -208(%rbp)
	movb	%dl, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rsi
	movzbl	-200(%rbp), %edx
	movq	%rax, %r15
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1217:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r15
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1204:
	movl	$70, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1102:
	movq	55(%rsi), %rax
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1105
	movq	23(%rax), %rax
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1197:
	leaq	.LC12(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1047:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1049
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1201:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1085
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L1206:
	movl	$83, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1112:
	movq	55(%rsi), %rax
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1115
	movq	23(%rax), %rax
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1109:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1223
.L1111:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1214:
	movq	%r14, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L1070
	.p2align 4,,10
	.p2align 3
.L1208:
	movl	$84, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIhEEvT_
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	55(%rsi), %rax
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1125
	movq	23(%rax), %rax
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1119:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1224
.L1121:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1220:
	movq	%r14, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1205:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1105
	jmp	.L1190
.L1222:
	movq	%r14, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L1091
.L1207:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1115
	jmp	.L1192
.L1209:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1125
	jmp	.L1194
.L1221:
	movq	%r14, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L1101
.L1223:
	movq	%r14, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L1111
.L1224:
	movq	%r14, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L1121
.L1196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18735:
	.size	_ZN2v88internal15ValueSerializer12WriteJSErrorENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal15ValueSerializer12WriteJSErrorENS0_6HandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal15ValueSerializer15WriteJSReceiverENS0_6HandleINS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer15WriteJSReceiverENS0_6HandleINS0_10JSReceiverEEE
	.type	_ZN2v88internal15ValueSerializer15WriteJSReceiverENS0_6HandleINS0_10JSReceiverEEE, @function
_ZN2v88internal15ValueSerializer15WriteJSReceiverENS0_6HandleINS0_10JSReceiverEEE:
.LFB18724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$112, %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rsi
	call	_ZN2v88internal15IdentityMapBase8GetEntryEm@PLT
	movl	(%rax), %ebx
	testl	%ebx, %ebx
	je	.L1226
	movq	24(%r12), %r14
	leaq	1(%r14), %r13
	cmpq	32(%r12), %r13
	ja	.L1227
.L1229:
	movq	16(%r12), %rax
	movq	%r13, 24(%r12)
	movb	$94, (%rax,%r14)
.L1228:
	leal	-1(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	jne	.L1274
.L1231:
	xorl	%eax, %eax
	movb	%dl, %al
	movb	$1, %ah
.L1269:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1226:
	.cfi_restore_state
	movl	184(%r12), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 184(%r12)
	movl	%edx, (%rax)
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	movq	-1(%rax), %rax
	movzbl	13(%rax), %eax
	andl	$2, %eax
	movl	%eax, %r14d
	jne	.L1233
	cmpw	$1040, %dx
	jbe	.L1275
	movq	(%r12), %rbx
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movl	-72(%rbp), %edx
	cmpq	37528(%rbx), %rax
	jb	.L1237
	movq	(%r12), %r15
	subw	$1041, %dx
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	41096(%r15), %rbx
	movq	%rax, -72(%rbp)
	cmpw	$61, %dx
	ja	.L1239
	leaq	.L1241(%rip), %rsi
	movzwl	%dx, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal15ValueSerializer15WriteJSReceiverENS0_6HandleINS0_10JSReceiverEEE,"a",@progbits
	.align 4
	.align 4
.L1241:
	.long	.L1252-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1251-.L1241
	.long	.L1251-.L1241
	.long	.L1239-.L1241
	.long	.L1250-.L1241
	.long	.L1239-.L1241
	.long	.L1249-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1248-.L1241
	.long	.L1247-.L1241
	.long	.L1239-.L1241
	.long	.L1246-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1245-.L1241
	.long	.L1239-.L1241
	.long	.L1244-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1243-.L1241
	.long	.L1243-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1239-.L1241
	.long	.L1242-.L1241
	.long	.L1240-.L1241
	.section	.text._ZN2v88internal15ValueSerializer15WriteJSReceiverENS0_6HandleINS0_10JSReceiverEEE
	.p2align 4,,10
	.p2align 3
.L1233:
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$358, %esi
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	movb	$0, %ah
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1275:
	.cfi_restore_state
	jne	.L1233
	movq	(%r12), %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%rbx), %rax
	jnb	.L1276
.L1237:
	movq	(%r12), %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1227:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1229
	jmp	.L1228
	.p2align 4,,10
	.p2align 3
.L1274:
	movq	(%r12), %rax
	movl	$359, %esi
	movq	%r12, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%edx, %edx
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1239:
	movq	%r13, %rdx
	movl	$358, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%eax, %eax
	movb	$0, %ah
	.p2align 4,,10
	.p2align 3
.L1253:
	movq	-72(%rbp), %rcx
	subl	$1, 41104(%r15)
	movq	%rcx, 41088(%r15)
	cmpq	%rbx, 41096(%r15)
	je	.L1269
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	movl	%eax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-72(%rbp), %eax
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1276:
	movq	(%r12), %r15
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	41096(%r15), %rbx
	movq	%rax, -72(%rbp)
.L1272:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer15WriteHostObjectENS0_6HandleINS0_8JSObjectEEE
	jmp	.L1253
.L1243:
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer22WriteJSArrayBufferViewENS0_17JSArrayBufferViewE
	jmp	.L1253
.L1251:
	movq	0(%r13), %rax
	movq	-1(%rax), %rcx
	movzbl	7(%rcx), %eax
	sall	$3, %eax
	movl	%eax, %r14d
	je	.L1255
	movzwl	11(%rcx), %esi
	movl	$24, %eax
	cmpw	$1057, %si
	je	.L1256
	movsbl	13(%rcx), %r8d
	movzwl	%si, %edi
	movq	%rcx, -80(%rbp)
	shrl	$31, %r8d
	movl	%r8d, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movq	-80(%rbp), %rcx
.L1256:
	movzbl	7(%rcx), %edx
	subl	%eax, %r14d
	movzbl	8(%rcx), %ecx
	movl	%r14d, %eax
	sarl	$3, %eax
	subl	%ecx, %edx
	cmpl	%edx, %eax
	jne	.L1272
.L1255:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer13WriteJSObjectENS0_6HandleINS0_8JSObjectEEE
	jmp	.L1253
.L1244:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer10WriteJSSetENS0_6HandleINS0_5JSSetEEE
	jmp	.L1253
.L1245:
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer13WriteJSRegExpENS0_8JSRegExpE
	cmpb	$0, 41(%r12)
	jne	.L1273
.L1267:
	movl	$1, %r14d
.L1258:
	xorl	%eax, %eax
	movb	%r14b, %al
	movb	$1, %ah
	jmp	.L1253
.L1242:
	movq	(%r12), %rdi
	call	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE@PLT
	movzbl	%dh, %ecx
	movb	%dl, -53(%rbp)
	movb	%cl, -52(%rbp)
	movq	%rdx, %rcx
	shrq	$16, %rcx
	movq	%rax, -61(%rbp)
	movb	%cl, -51(%rbp)
	movq	%rdx, %rcx
	shrq	$32, %rdx
	shrq	$24, %rcx
	movb	%dl, -49(%rbp)
	movb	%cl, -50(%rbp)
	testl	$16711680, %eax
	je	.L1239
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer15WriteWasmMemoryENS0_6HandleINS0_16WasmMemoryObjectEEE
	jmp	.L1253
.L1246:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer10WriteJSMapENS0_6HandleINS0_5JSMapEEE
	jmp	.L1253
.L1247:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12WriteJSErrorENS0_6HandleINS0_8JSObjectEEE
	jmp	.L1253
.L1248:
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteJSDateENS0_6JSDateE
	cmpb	$0, 41(%r12)
	je	.L1267
.L1273:
	movq	(%r12), %rax
	movl	$359, %esi
	movq	%r12, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	jmp	.L1258
.L1249:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12WriteJSArrayENS0_6HandleINS0_7JSArrayEEE
	jmp	.L1253
.L1252:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer23WriteJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEE
	jmp	.L1253
.L1250:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer18WriteJSArrayBufferENS0_6HandleINS0_13JSArrayBufferEEE
	jmp	.L1253
.L1240:
	movq	(%r12), %rdi
	call	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE@PLT
	movq	%rax, -61(%rbp)
	movzbl	%dh, %eax
	movb	%al, -52(%rbp)
	movq	%rdx, %rax
	shrq	$16, %rax
	movb	%dl, -53(%rbp)
	movb	%al, -51(%rbp)
	movq	%rdx, %rax
	shrq	$32, %rdx
	shrq	$24, %rax
	cmpb	$0, _ZN2v88internal36FLAG_wasm_disable_structured_cloningE(%rip)
	movb	%dl, -49(%rbp)
	movb	%al, -50(%rbp)
	je	.L1259
	cmpb	$0, -59(%rbp)
	je	.L1239
.L1259:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer15WriteWasmModuleENS0_6HandleINS0_16WasmModuleObjectEEE
	jmp	.L1253
	.cfi_endproc
.LFE18724:
	.size	_ZN2v88internal15ValueSerializer15WriteJSReceiverENS0_6HandleINS0_10JSReceiverEEE, .-_ZN2v88internal15ValueSerializer15WriteJSReceiverENS0_6HandleINS0_10JSReceiverEEE
	.section	.text._ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE:
.LFB18718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 41(%rdi)
	jne	.L1344
	movq	(%rsi), %r13
	movq	%rsi, %r14
	testb	$1, %r13b
	jne	.L1280
	movq	24(%rdi), %rbx
	leaq	1(%rbx), %r14
	cmpq	32(%rdi), %r14
	ja	.L1281
.L1283:
	movq	16(%r12), %rax
	movq	%r14, 24(%r12)
	movb	$73, (%rax,%rbx)
.L1282:
	sarq	$32, %r13
	movq	%r12, %rdi
	leal	(%r13,%r13), %esi
	sarl	$31, %r13d
	xorl	%r13d, %esi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	jne	.L1340
.L1325:
	xorl	%eax, %eax
	movb	%dl, %al
	movb	$1, %ah
.L1279:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1345
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1280:
	.cfi_restore_state
	movq	-1(%r13), %rax
	movzwl	11(%rax), %eax
	cmpw	$67, %ax
	je	.L1286
	ja	.L1287
	cmpw	$65, %ax
	je	.L1288
	cmpw	$66, %ax
	jne	.L1290
	movq	24(%rdi), %r14
	leaq	1(%r14), %rbx
	cmpq	32(%rdi), %rbx
	ja	.L1309
.L1311:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$90, (%rax,%r14)
.L1310:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer19WriteBigIntContentsENS0_6BigIntE
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	je	.L1325
	.p2align 4,,10
	.p2align 3
.L1340:
	movq	(%r12), %rax
	movl	$359, %esi
	movq	%r12, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%edx, %edx
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1287:
	subw	$1086, %ax
	cmpw	$1, %ax
	ja	.L1290
	leaq	112(%rdi), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal15IdentityMapBase9FindEntryEm@PLT
	testq	%rax, %rax
	je	.L1346
.L1326:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer15WriteJSReceiverENS0_6HandleINS0_10JSReceiverEEE
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1344:
	movq	(%rdi), %rax
	movl	$359, %esi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1286:
	movslq	43(%r13), %rax
	cmpb	$3, %al
	je	.L1329
	ja	.L1293
	testb	%al, %al
	je	.L1330
	cmpb	$1, %al
	jne	.L1294
	movl	$84, %ebx
.L1292:
	movq	24(%r12), %r14
	leaq	1(%r14), %r13
	cmpq	32(%r12), %r13
	ja	.L1295
.L1297:
	movq	16(%r12), %rax
	movq	%r13, 24(%r12)
	movb	%bl, (%rax,%r14)
.L1342:
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	je	.L1325
	jmp	.L1340
	.p2align 4,,10
	.p2align 3
.L1293:
	cmpb	$5, %al
	jne	.L1294
	movl	$95, %ebx
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1290:
	testb	$1, %r13b
	jne	.L1347
.L1323:
	movq	%r14, %rdx
	movl	$358, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
.L1339:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1288:
	movq	24(%rdi), %r14
	leaq	1(%r14), %rbx
	cmpq	32(%rdi), %rbx
	ja	.L1300
.L1302:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$78, (%rax,%r14)
.L1301:
	movq	7(%r13), %r14
	movq	24(%r12), %r13
	leaq	8(%r13), %rbx
	cmpq	32(%r12), %rbx
	ja	.L1304
.L1306:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movq	%r14, (%rax,%r13)
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1330:
	movl	$70, %ebx
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1346:
	cmpb	$0, 40(%r12)
	jne	.L1326
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$1086, 11(%rdx)
	je	.L1348
	movq	(%r12), %rbx
	movq	23(%rax), %r13
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1317
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1316:
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer15WriteJSReceiverENS0_6HandleINS0_10JSReceiverEEE
	testb	%al, %al
	je	.L1339
	shrw	$8, %ax
	jne	.L1326
	jmp	.L1339
.L1309:
	movq	%rbx, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1311
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	%r14, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1283
	jmp	.L1282
	.p2align 4,,10
	.p2align 3
.L1347:
	movq	-1(%r13), %rax
	cmpw	$63, 11(%rax)
	ja	.L1349
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteStringENS0_6HandleINS0_6StringEEE
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1349:
	movq	-1(%r13), %rax
	cmpw	$1023, 11(%rax)
	ja	.L1326
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1329:
	movl	$48, %ebx
	jmp	.L1292
.L1304:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1306
	jmp	.L1342
.L1300:
	movq	%rbx, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1302
	jmp	.L1301
.L1295:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1297
	jmp	.L1342
.L1317:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L1350
.L1318:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	jmp	.L1316
.L1348:
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal12JSTypedArray9GetBufferEv@PLT
	movq	%rax, %rsi
	jmp	.L1316
.L1350:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1318
.L1345:
	call	__stack_chk_fail@PLT
.L1294:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18718:
	.size	_ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal15ValueSerializer10WriteJSMapENS0_6HandleINS0_5JSMapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer10WriteJSMapENS0_6HandleINS0_5JSMapEEE
	.type	_ZN2v88internal15ValueSerializer10WriteJSMapENS0_6HandleINS0_5JSMapEEE, @function
_ZN2v88internal15ValueSerializer10WriteJSMapENS0_6HandleINS0_5JSMapEEE:
.LFB18731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	(%rsi), %rax
	movq	41112(%rbx), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1352
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L1353:
	movq	15(%rsi), %r15
	movq	(%r12), %rdi
	xorl	%edx, %edx
	sarq	$32, %r15
	leal	(%r15,%r15), %eax
	movl	%eax, %esi
	movl	%eax, -64(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	0(%r13), %rdx
	movq	%rax, %rbx
	movq	(%r12), %rax
	movq	96(%rax), %rax
	movq	%rax, -56(%rbp)
	movq	15(%rdx), %rax
	movq	23(%rdx), %rsi
	sarq	$32, %rax
	movq	%rax, %rcx
	movq	%rsi, %rax
	sarq	$32, %rax
	addl	%ecx, %eax
	testl	%eax, %eax
	jle	.L1366
	leal	(%rax,%rax,2), %eax
	xorl	%r15d, %r15d
	movq	%r12, -104(%rbp)
	movq	%rbx, %r14
	movl	%eax, -60(%rbp)
	movl	%r15d, %r12d
	jmp	.L1367
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	0(%r13), %rdx
.L1367:
	movq	31(%rdx), %rax
	sarq	$32, %rax
	leal	5(%r15,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rdx
	cmpq	%rdx, -56(%rbp)
	je	.L1359
	addl	$2, %r12d
	movq	(%r14), %rdi
	leal	0(,%r12,8), %ebx
	movslq	%ebx, %rax
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L1386
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r10
	movq	%rax, -72(%rbp)
	testl	$262144, %r10d
	je	.L1361
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rax), %r10
.L1361:
	andl	$24, %r10d
	je	.L1386
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1386
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1386:
	movq	0(%r13), %rdx
	movq	(%r14), %rdi
	addl	$8, %ebx
	movslq	%ebx, %rbx
	movq	31(%rdx), %rax
	leaq	-1(%rdi,%rbx), %rbx
	sarq	$32, %rax
	leal	6(%r15,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rdx
	movq	%rdx, (%rbx)
	testb	$1, %dl
	je	.L1359
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rsi
	movq	%rax, -72(%rbp)
	testl	$262144, %esi
	je	.L1364
	movq	%rbx, %rsi
	movq	%rdx, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rdi
	movq	8(%rax), %rsi
.L1364:
	andl	$24, %esi
	je	.L1359
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1359
	movq	%rbx, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1359:
	addl	$3, %r15d
	cmpl	%r15d, -60(%rbp)
	jne	.L1402
	movq	-104(%rbp), %r12
	movq	%r14, %rbx
.L1366:
	movq	24(%r12), %r14
	leaq	1(%r14), %r13
	cmpq	32(%r12), %r13
	ja	.L1403
.L1357:
	movq	16(%r12), %rax
	movq	%r13, 24(%r12)
	movb	$59, (%rax,%r14)
.L1368:
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	jle	.L1380
	movl	-64(%rbp), %eax
	movl	$16, %r13d
	subl	$1, %eax
	leaq	24(,%rax,8), %r14
.L1381:
	movq	(%r12), %r15
	movq	(%rbx), %rax
	movq	-1(%r13,%rax), %r8
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1373
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1374:
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1377
	shrw	$8, %ax
	je	.L1377
	addq	$8, %r13
	cmpq	%r14, %r13
	jne	.L1381
.L1380:
	movq	24(%r12), %r13
	leaq	1(%r13), %rbx
	cmpq	32(%r12), %rbx
	ja	.L1404
.L1372:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$58, (%rax,%r13)
.L1382:
	movl	-64(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	jne	.L1405
.L1384:
	xorl	%eax, %eax
	movb	%dl, %al
	movb	$1, %ah
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1377:
	xorl	%eax, %eax
	movb	$0, %ah
.L1379:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1373:
	.cfi_restore_state
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1406
.L1375:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r8, (%rsi)
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1352:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L1407
.L1354:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1406:
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1403:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1357
	jmp	.L1368
.L1407:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1354
.L1405:
	movq	(%r12), %rax
	movl	$359, %esi
	movq	%r12, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%edx, %edx
	jmp	.L1384
.L1404:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1372
	jmp	.L1382
	.cfi_endproc
.LFE18731:
	.size	_ZN2v88internal15ValueSerializer10WriteJSMapENS0_6HandleINS0_5JSMapEEE, .-_ZN2v88internal15ValueSerializer10WriteJSMapENS0_6HandleINS0_5JSMapEEE
	.section	.text._ZN2v88internal15ValueSerializer10WriteJSSetENS0_6HandleINS0_5JSSetEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer10WriteJSSetENS0_6HandleINS0_5JSSetEEE
	.type	_ZN2v88internal15ValueSerializer10WriteJSSetENS0_6HandleINS0_5JSSetEEE, @function
_ZN2v88internal15ValueSerializer10WriteJSSetENS0_6HandleINS0_5JSSetEEE:
.LFB18732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	(%rsi), %rax
	movq	41112(%rbx), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1409
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L1410:
	movq	%rcx, -56(%rbp)
	movq	15(%rsi), %r12
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	movq	(%r15), %rdi
	sarq	$32, %r12
	movl	%r12d, %esi
	movq	%r12, -96(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rbx
	movq	(%r15), %rax
	movq	(%rcx), %rsi
	movq	96(%rax), %r9
	movq	15(%rsi), %rax
	movq	23(%rsi), %rdi
	sarq	$32, %rax
	movq	%rax, %rdx
	movq	%rdi, %rax
	sarq	$32, %rax
	addl	%edx, %eax
	xorl	%edx, %edx
	leal	(%rax,%rax), %r12d
	testl	%eax, %eax
	jle	.L1420
	movq	%r15, -104(%rbp)
	movq	%rbx, %r8
	movq	%rcx, %r15
	movl	%r12d, %ebx
	movq	%r9, %r12
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	(%r15), %rsi
.L1421:
	movq	31(%rsi), %rax
	sarq	$32, %rax
	leal	5(%r13,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rsi,%rax), %r14
	cmpq	%r14, %r12
	je	.L1416
	movq	(%r8), %rdi
	leal	16(,%rdx,8), %eax
	leal	1(%rdx), %r9d
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L1439
	movq	%r14, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -56(%rbp)
	testl	$262144, %edx
	je	.L1418
	movq	%r14, %rdx
	movq	%r8, -88(%rbp)
	movl	%r9d, -76(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	movq	-88(%rbp), %r8
	movl	-76(%rbp), %r9d
	movq	-72(%rbp), %rsi
	movq	8(%rax), %rdx
	movq	-64(%rbp), %rdi
.L1418:
	andl	$24, %edx
	je	.L1439
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1439
	movq	%r14, %rdx
	movq	%r8, -64(%rbp)
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %r8
	movl	-56(%rbp), %r9d
	.p2align 4,,10
	.p2align 3
.L1439:
	movl	%r9d, %edx
.L1416:
	addl	$2, %r13d
	cmpl	%r13d, %ebx
	jne	.L1451
	movq	-104(%rbp), %r15
	movq	%r8, %rbx
.L1420:
	movq	24(%r15), %r14
	leaq	1(%r14), %r13
	cmpq	32(%r15), %r13
	ja	.L1452
.L1414:
	movq	16(%r15), %rax
	movq	%r13, 24(%r15)
	movb	$39, (%rax,%r14)
.L1422:
	cmpq	$0, -96(%rbp)
	jle	.L1434
	movq	-96(%rbp), %rax
	movl	$16, %r13d
	subl	$1, %eax
	leaq	24(,%rax,8), %r14
.L1435:
	movq	(%r15), %r12
	movq	(%rbx), %rax
	movq	-1(%r13,%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1427
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1428:
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1431
	shrw	$8, %ax
	je	.L1431
	addq	$8, %r13
	cmpq	%r14, %r13
	jne	.L1435
.L1434:
	movq	24(%r15), %r13
	leaq	1(%r13), %rbx
	cmpq	32(%r15), %rbx
	ja	.L1453
.L1426:
	movq	16(%r15), %rax
	movq	%rbx, 24(%r15)
	movb	$44, (%rax,%r13)
.L1436:
	movl	-96(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	cmpb	$0, 41(%r15)
	movl	$1, %edx
	jne	.L1454
.L1438:
	xorl	%eax, %eax
	movb	%dl, %al
	movb	$1, %ah
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1431:
	xorl	%eax, %eax
	movb	$0, %ah
.L1433:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1427:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1455
.L1429:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1409:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L1456
.L1411:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1414
	jmp	.L1422
.L1456:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1411
.L1454:
	movq	(%r15), %rax
	movl	$359, %esi
	movq	%r15, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%edx, %edx
	jmp	.L1438
.L1453:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1426
	jmp	.L1436
	.cfi_endproc
.LFE18732:
	.size	_ZN2v88internal15ValueSerializer10WriteJSSetENS0_6HandleINS0_5JSSetEEE, .-_ZN2v88internal15ValueSerializer10WriteJSSetENS0_6HandleINS0_5JSSetEEE
	.section	.text._ZN2v88internal15ValueSerializer27WriteJSObjectPropertiesSlowENS0_6HandleINS0_8JSObjectEEENS2_INS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer27WriteJSObjectPropertiesSlowENS0_6HandleINS0_8JSObjectEEENS2_INS0_10FixedArrayEEE
	.type	_ZN2v88internal15ValueSerializer27WriteJSObjectPropertiesSlowENS0_6HandleINS0_8JSObjectEEENS2_INS0_10FixedArrayEEE, @function
_ZN2v88internal15ValueSerializer27WriteJSObjectPropertiesSlowENS0_6HandleINS0_8JSObjectEEENS2_INS0_10FixedArrayEEE:
.LFB18743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdx), %rax
	movq	%rdx, -176(%rbp)
	movslq	11(%rax), %rdx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	jle	.L1470
	subl	$1, %edx
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movl	$16, %r12d
	leaq	24(,%rdx,8), %rcx
	leaq	-144(%rbp), %r14
	movl	$0, -192(%rbp)
	movq	%rcx, -184(%rbp)
	leaq	-145(%rbp), %r15
.L1468:
	movq	(%rbx), %rdx
	movq	-1(%rax,%r12), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1459
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L1460:
	movq	(%rbx), %rsi
	movq	%r10, %rcx
	movl	$1, %r9d
	movq	%r15, %r8
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r10, -168(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	-168(%rbp), %r10
	testq	%rax, %rax
	je	.L1466
	cmpl	$4, -140(%rbp)
	movq	%rax, -168(%rbp)
	je	.L1465
	movq	%r10, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1466
	shrw	$8, %ax
	je	.L1466
	movq	-168(%rbp), %r8
	movq	%rbx, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1466
	shrw	$8, %ax
	je	.L1466
	addl	$1, -192(%rbp)
.L1465:
	addq	$8, %r12
	cmpq	%r12, -184(%rbp)
	je	.L1458
	movq	-176(%rbp), %rax
	movq	(%rax), %rax
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1466:
	xorl	%eax, %eax
.L1467:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1484
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1459:
	.cfi_restore_state
	movq	41088(%rdx), %r10
	cmpq	41096(%rdx), %r10
	je	.L1485
.L1461:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r10)
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	%rdx, %rdi
	movq	%rsi, -200(%rbp)
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	-168(%rbp), %rdx
	movq	%rax, %r10
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1470:
	movl	$0, -192(%rbp)
.L1458:
	movq	-192(%rbp), %rax
	salq	$32, %rax
	orq	$1, %rax
	jmp	.L1467
.L1484:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18743:
	.size	_ZN2v88internal15ValueSerializer27WriteJSObjectPropertiesSlowENS0_6HandleINS0_8JSObjectEEENS2_INS0_10FixedArrayEEE, .-_ZN2v88internal15ValueSerializer27WriteJSObjectPropertiesSlowENS0_6HandleINS0_8JSObjectEEENS2_INS0_10FixedArrayEEE
	.section	.text._ZN2v88internal15ValueSerializer17WriteJSObjectSlowENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer17WriteJSObjectSlowENS0_6HandleINS0_8JSObjectEEE
	.type	_ZN2v88internal15ValueSerializer17WriteJSObjectSlowENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal15ValueSerializer17WriteJSObjectSlowENS0_6HandleINS0_8JSObjectEEE:
.LFB18726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	24(%rdi), %r14
	leaq	1(%r14), %rbx
	cmpq	32(%rdi), %rbx
	ja	.L1487
.L1489:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$111, (%rax,%r14)
.L1488:
	movl	$18, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1491
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer27WriteJSObjectPropertiesSlowENS0_6HandleINS0_8JSObjectEEENS2_INS0_10FixedArrayEEE
	testb	%al, %al
	je	.L1491
	movq	24(%r12), %r14
	shrq	$32, %rax
	movq	%rax, %rbx
	leaq	1(%r14), %r13
	cmpq	32(%r12), %r13
	ja	.L1492
.L1494:
	movq	16(%r12), %rax
	movq	%r13, 24(%r12)
	movb	$123, (%rax,%r14)
.L1493:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	jne	.L1503
.L1496:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	movb	%dl, %al
	popq	%r13
	popq	%r14
	movb	$1, %ah
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1491:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	movb	$0, %ah
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1503:
	.cfi_restore_state
	movq	(%r12), %rax
	movl	$359, %esi
	movq	%r12, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%edx, %edx
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1487:
	movq	%rbx, %rsi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1489
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1492:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1494
	jmp	.L1493
	.cfi_endproc
.LFE18726:
	.size	_ZN2v88internal15ValueSerializer17WriteJSObjectSlowENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal15ValueSerializer17WriteJSObjectSlowENS0_6HandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal15ValueSerializer13WriteJSObjectENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer13WriteJSObjectENS0_6HandleINS0_8JSObjectEEE
	.type	_ZN2v88internal15ValueSerializer13WriteJSObjectENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal15ValueSerializer13WriteJSObjectENS0_6HandleINS0_8JSObjectEEE:
.LFB18725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %r13d
	andl	$2097152, %r13d
	movl	%r13d, -148(%rbp)
	jne	.L1505
	movq	15(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L1571
.L1505:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer17WriteJSObjectSlowENS0_6HandleINS0_8JSObjectEEE
.L1508:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1572
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1571:
	.cfi_restore_state
	movq	(%rdi), %r14
	movq	-1(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1573
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L1509:
	movq	24(%r12), %r13
	leaq	1(%r13), %r14
	cmpq	32(%r12), %r14
	ja	.L1511
.L1513:
	movq	16(%r12), %rax
	movq	%r14, 24(%r12)
	movb	$111, (%rax,%r13)
.L1512:
	movq	(%rbx), %rax
	movb	$0, -160(%rbp)
	xorl	%ecx, %ecx
	movl	15(%rax), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	cmpl	%ecx, %edx
	jle	.L1516
.L1577:
	leaq	1(%rcx), %r14
	movq	(%r12), %rdx
	movq	39(%rax), %rax
	leaq	(%r14,%r14,2), %r13
	salq	$3, %r13
	movq	-1(%r13,%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1517
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L1518:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L1570
	movq	(%rbx), %rax
	movq	39(%rax), %rdi
	movq	7(%r13,%rdi), %rdx
	movq	%rdx, %rsi
	sarq	$32, %rsi
	btq	$36, %rdx
	jc	.L1540
	cmpb	$0, -160(%rbp)
	movq	(%r15), %rdx
	jne	.L1521
	movq	-1(%rdx), %r9
	cmpq	%r9, %rax
	je	.L1574
	testb	$2, %sil
	jne	.L1521
	movq	7(%r13,%rdi), %r11
	movzbl	7(%rax), %r9d
	movzbl	8(%rax), %edi
	movq	%r11, %rcx
	shrq	$38, %r11
	shrq	$51, %rcx
	subl	%edi, %r9d
	andl	$7, %r11d
	movq	%rcx, %r10
	andl	$1023, %r10d
	cmpl	%r9d, %r10d
	setl	%dl
	jl	.L1575
	subl	%r9d, %r10d
	movl	$16, %edi
	leal	16(,%r10,8), %r10d
.L1532:
	cmpl	$2, %r11d
	jne	.L1576
	movl	$32768, %eax
.L1533:
	movzbl	%dl, %edx
	movslq	%r9d, %r9
	movslq	%r10d, %r10
	movslq	%edi, %rdi
	salq	$17, %r9
	salq	$14, %rdx
	movq	%r8, -168(%rbp)
	salq	$27, %rdi
	orq	%r9, %rdx
	shrl	$6, %esi
	orq	%r10, %rdx
	andl	$7, %esi
	orq	%rdi, %rdx
	movq	%r15, %rdi
	orq	%rax, %rdx
	call	_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %r13
.L1536:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1539
	shrw	$8, %ax
	je	.L1539
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1539
	shrw	$8, %ax
	je	.L1539
	addl	$1, -148(%rbp)
	.p2align 4,,10
	.p2align 3
.L1570:
	movq	(%rbx), %rax
.L1540:
	movl	15(%rax), %edx
	movq	%r14, %rcx
	shrl	$10, %edx
	andl	$1023, %edx
	cmpl	%ecx, %edx
	jg	.L1577
.L1516:
	movq	24(%r12), %r14
	leaq	1(%r14), %rbx
	cmpq	32(%r12), %rbx
	ja	.L1541
.L1543:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$123, (%rax,%r14)
.L1542:
	movl	-148(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	cmpb	$0, 41(%r12)
	movl	$1, %edx
	jne	.L1578
.L1545:
	xorl	%eax, %eax
	movb	%dl, %al
	movb	$1, %ah
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	41088(%r14), %rbx
	cmpq	%rbx, 41096(%r14)
	je	.L1579
.L1510:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rbx)
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1517:
	movq	41088(%rdx), %r8
	cmpq	41096(%rdx), %r8
	je	.L1580
.L1519:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r8)
	jmp	.L1518
	.p2align 4,,10
	.p2align 3
.L1576:
	cmpb	$2, %r11b
	jg	.L1534
	je	.L1535
.L1550:
	xorl	%eax, %eax
	jmp	.L1533
	.p2align 4,,10
	.p2align 3
.L1534:
	subl	$3, %r11d
	cmpb	$1, %r11b
	jbe	.L1550
.L1535:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1574:
	movb	$1, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L1521:
	movq	(%r12), %r13
	testb	$1, %dl
	jne	.L1524
.L1526:
	movl	$-1, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %r9
.L1525:
	movq	(%r8), %rdx
	movl	$1, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L1527
	movl	11(%rdx), %eax
	notl	%eax
	andl	$1, %eax
.L1527:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%r13, -120(%rbp)
	movq	(%r8), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r8, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L1581
.L1528:
	leaq	-144(%rbp), %r13
	movq	%r8, -168(%rbp)
	movq	%r13, %rdi
	movq	%rax, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movq	-168(%rbp), %r8
	je	.L1570
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.L1536
	.p2align 4,,10
	.p2align 3
.L1539:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1575:
	movzbl	8(%rax), %edi
	movzbl	8(%rax), %eax
	addl	%eax, %r10d
	sall	$3, %edi
	sall	$3, %r10d
	jmp	.L1532
	.p2align 4,,10
	.p2align 3
.L1580:
	movq	%rdx, %rdi
	movq	%rsi, -176(%rbp)
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	-168(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1524:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1526
	movq	%r15, %r9
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1513
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1579:
	movq	%r14, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1510
	.p2align 4,,10
	.p2align 3
.L1581:
	movq	%r8, %rsi
	movq	%r13, %rdi
	movq	%r9, -176(%rbp)
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-176(%rbp), %r9
	movq	-168(%rbp), %r8
	jmp	.L1528
.L1578:
	movq	(%r12), %rax
	movl	$359, %esi
	movq	%r12, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%edx, %edx
	jmp	.L1545
.L1541:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1543
	jmp	.L1542
.L1572:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18725:
	.size	_ZN2v88internal15ValueSerializer13WriteJSObjectENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal15ValueSerializer13WriteJSObjectENS0_6HandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal15ValueSerializer12WriteJSArrayENS0_6HandleINS0_7JSArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer12WriteJSArrayENS0_6HandleINS0_7JSArrayEEE
	.type	_ZN2v88internal15ValueSerializer12WriteJSArrayENS0_6HandleINS0_7JSArrayEEE, @function
_ZN2v88internal15ValueSerializer12WriteJSArrayENS0_6HandleINS0_7JSArrayEEE:
.LFB18727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %r12
	testb	$1, %r12b
	jne	.L1583
	sarq	$32, %r12
	movl	$0, %edx
	cmovs	%edx, %r12d
.L1584:
	movq	-1(%rax), %rdx
	cmpb	$47, 14(%rdx)
	ja	.L1586
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpb	$5, %al
	ja	.L1666
	testb	$1, %al
	jne	.L1586
.L1666:
	movq	24(%r15), %r14
	leaq	1(%r14), %rbx
	cmpq	32(%r15), %rbx
	ja	.L1588
.L1590:
	movq	16(%r15), %rax
	movq	%rbx, 24(%r15)
	movb	$65, (%rax,%r14)
.L1589:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	movq	0(%r13), %rcx
	movq	-1(%rcx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$2, %eax
	je	.L1592
	cmpl	$4, %eax
	je	.L1593
	xorl	%ebx, %ebx
	testl	%eax, %eax
	je	.L1697
.L1595:
	movq	(%r15), %rdx
	leaq	-144(%rbp), %r14
	cmpl	%r12d, %ebx
	jnb	.L1631
.L1632:
	movabsq	$824633720832, %rax
	movq	%r14, %rdi
	movq	%rdx, -120(%rbp)
	movl	$1, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movl	%ebx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	je	.L1698
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1649
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1649
	shrw	$8, %ax
	je	.L1649
.L1644:
	addl	$1, %ebx
	movq	(%r15), %rdx
	cmpl	%r12d, %ebx
	jne	.L1632
	.p2align 4,,10
	.p2align 3
.L1631:
	movabsq	$77309411328, %rax
	movq	%rdx, -144(%rbp)
	movq	%r13, %rsi
	movq	%r13, %rdx
	movq	%rax, -112(%rbp)
	movq	%r14, %rdi
	xorl	%eax, %eax
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movw	%ax, -104(%rbp)
	movb	$1, -102(%rbp)
	call	_ZN2v88internal14KeyAccumulator23CollectOwnPropertyNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	je	.L1649
	shrw	$8, %ax
	je	.L1649
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_17GetKeysConversionE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal15ValueSerializer27WriteJSObjectPropertiesSlowENS0_6HandleINS0_8JSObjectEEENS2_INS0_10FixedArrayEEE
	testb	%al, %al
	je	.L1649
	movq	24(%r15), %r14
	shrq	$32, %rax
	movq	%rax, %rbx
	leaq	1(%r14), %r13
	cmpq	32(%r15), %r13
	ja	.L1650
.L1652:
	movq	16(%r15), %rax
	movq	%r13, 24(%r15)
	movb	$36, (%rax,%r14)
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1586:
	movq	24(%r15), %r14
	leaq	1(%r14), %rbx
	cmpq	32(%r15), %rbx
	ja	.L1596
.L1598:
	movq	16(%r15), %rax
	movq	%rbx, 24(%r15)
	movb	$97, (%rax,%r14)
.L1597:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	movl	$18, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1649
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer27WriteJSObjectPropertiesSlowENS0_6HandleINS0_8JSObjectEEENS2_INS0_10FixedArrayEEE
	testb	%al, %al
	je	.L1649
	movq	24(%r15), %r14
	shrq	$32, %rax
	movq	%rax, %rbx
	leaq	1(%r14), %r13
	cmpq	32(%r15), %r13
	ja	.L1656
.L1658:
	movq	16(%r15), %rax
	movq	%r13, 24(%r15)
	movb	$64, (%rax,%r14)
.L1657:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	cmpb	$0, 41(%r15)
	movl	$1, %edx
	jne	.L1699
.L1660:
	xorl	%eax, %eax
	movb	%dl, %al
	movb	$1, %ah
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1649:
	xorl	%eax, %eax
	movb	$0, %ah
.L1655:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1700
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1583:
	.cfi_restore_state
	movq	-1(%r12), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1585
	xorl	%r12d, %r12d
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1699:
	movq	(%r15), %rax
	movl	$359, %esi
	movq	%r15, %rdi
	leaq	128(%rax), %rdx
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%edx, %edx
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1585:
	movsd	.LC13(%rip), %xmm0
	addsd	7(%r12), %xmm0
	movl	$0, %r12d
	movq	%xmm0, %rcx
	movq	%xmm0, %rdx
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	cmove	%edx, %r12d
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1596:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1598
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1698:
	movq	24(%r15), %rcx
	leaq	1(%rcx), %rdx
	cmpq	32(%r15), %rdx
	ja	.L1640
.L1642:
	movq	16(%r15), %rax
	movq	%rdx, 24(%r15)
	movb	$45, (%rax,%rcx)
	jmp	.L1644
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	(%r15), %rbx
	movq	23(%rcx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1624
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L1625:
	xorl	%ebx, %ebx
	testl	%r12d, %r12d
	je	.L1605
.L1627:
	movq	0(%r13), %rax
	movq	23(%rax), %rcx
	cmpq	%rcx, (%r14)
	jne	.L1595
	movq	-1(%rax), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	cmpl	$2, %edx
	jne	.L1595
	movq	(%r15), %rcx
	movq	15(%rax), %rax
	leal	16(,%rbx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rdx,%rax), %r8
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1701
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1633:
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer11WriteObjectENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1649
	shrw	$8, %ax
	je	.L1649
	addl	$1, %ebx
	cmpl	%r12d, %ebx
	jne	.L1627
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	(%r15), %rdx
	leaq	-144(%rbp), %r14
	jmp	.L1631
.L1640:
	movq	%rdx, %rsi
	movq	%r15, %rdi
	movq	%rcx, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %rcx
	testb	%al, %al
	jne	.L1642
	jmp	.L1644
	.p2align 4,,10
	.p2align 3
.L1593:
	movq	(%r15), %rdx
	leaq	-144(%rbp), %r14
	testl	%r12d, %r12d
	je	.L1631
	movq	41112(%rdx), %rdi
	movq	15(%rcx), %rsi
	testq	%rdi, %rdi
	je	.L1612
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1613:
	leal	2(%r12), %edx
	movl	$2, %ebx
	.p2align 4,,10
	.p2align 3
.L1623:
	movq	24(%r15), %r8
	leaq	1(%r8), %r14
	cmpq	32(%r15), %r14
	ja	.L1615
.L1617:
	movq	16(%r15), %rax
	movq	%r14, 24(%r15)
	movb	$78, (%rax,%r8)
.L1616:
	movq	(%rcx), %rsi
	movq	24(%r15), %r9
	leal	0(,%rbx,8), %eax
	cltq
	movq	-1(%rax,%rsi), %r8
	leaq	8(%r9), %r14
	cmpq	32(%r15), %r14
	ja	.L1619
.L1621:
	movq	16(%r15), %rax
	movq	%r14, 24(%r15)
	movq	%r8, (%rax,%r9)
.L1620:
	addl	$1, %ebx
	cmpl	%ebx, %edx
	jne	.L1623
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1697:
	movq	(%r15), %rbx
	movq	15(%rcx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1602
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L1603:
	testl	%r12d, %r12d
	je	.L1605
	leal	2(%r12), %eax
	movl	$2, %ebx
	movl	%eax, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L1610:
	movq	(%r14), %rcx
	leal	0(,%rbx,8), %eax
	cltq
	movq	-1(%rax,%rcx), %rdx
	movq	24(%r15), %r8
	leaq	1(%r8), %rcx
	cmpq	32(%r15), %rcx
	ja	.L1606
.L1608:
	movq	16(%r15), %rax
	movq	%rcx, 24(%r15)
	movb	$73, (%rax,%r8)
.L1607:
	movq	%rdx, %rsi
	movq	%r15, %rdi
	addl	$1, %ebx
	sarq	$32, %rsi
	leal	(%rsi,%rsi), %eax
	sarl	$31, %esi
	xorl	%eax, %esi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	cmpl	%ebx, -152(%rbp)
	jne	.L1610
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1606:
	movq	%rcx, %rsi
	movq	%r15, %rdi
	movq	%r8, -176(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%rcx, -160(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %rdx
	testb	%al, %al
	movq	-176(%rbp), %r8
	jne	.L1608
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1701:
	movq	41088(%rcx), %rsi
	cmpq	41096(%rcx), %rsi
	je	.L1702
.L1634:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rcx)
	movq	%r8, (%rsi)
	jmp	.L1633
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1658
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1602:
	movq	41088(%rbx), %r14
	cmpq	%r14, 41096(%rbx)
	je	.L1703
.L1604:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1624:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L1704
.L1626:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1615:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r8, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movl	%edx, -152(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movl	-152(%rbp), %edx
	movq	-160(%rbp), %rcx
	testb	%al, %al
	movq	-168(%rbp), %r8
	jne	.L1617
	jmp	.L1616
	.p2align 4,,10
	.p2align 3
.L1619:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r9, -176(%rbp)
	movq	%r8, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movl	%edx, -152(%rbp)
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	movl	-152(%rbp), %edx
	movq	-160(%rbp), %rcx
	testb	%al, %al
	movq	-168(%rbp), %r8
	movq	-176(%rbp), %r9
	jne	.L1621
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1588:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1590
	jmp	.L1589
.L1612:
	movq	41088(%rdx), %rcx
	cmpq	41096(%rdx), %rcx
	je	.L1705
.L1614:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L1613
.L1702:
	movq	%rcx, %rdi
	movq	%r8, -160(%rbp)
	movq	%rcx, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L1634
.L1704:
	movq	%rbx, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1626
.L1703:
	movq	%rbx, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1604
.L1705:
	movq	%rdx, %rdi
	movq	%rsi, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	-152(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1614
.L1700:
	call	__stack_chk_fail@PLT
.L1650:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1652
	jmp	.L1657
	.cfi_endproc
.LFE18727:
	.size	_ZN2v88internal15ValueSerializer12WriteJSArrayENS0_6HandleINS0_7JSArrayEEE, .-_ZN2v88internal15ValueSerializer12WriteJSArrayENS0_6HandleINS0_7JSArrayEEE
	.section	.text._ZN2v88internal15ValueSerializer15WriteWasmMemoryENS0_6HandleINS0_16WasmMemoryObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ValueSerializer15WriteWasmMemoryENS0_6HandleINS0_16WasmMemoryObjectEEE
	.type	_ZN2v88internal15ValueSerializer15WriteWasmMemoryENS0_6HandleINS0_16WasmMemoryObjectEEE, @function
_ZN2v88internal15ValueSerializer15WriteWasmMemoryENS0_6HandleINS0_16WasmMemoryObjectEEE:
.LFB18741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movl	39(%rax), %eax
	testb	$8, %al
	jne	.L1707
	movq	%rsi, %rdx
	movl	$358, %esi
	call	_ZN2v88internal15ValueSerializer19ThrowDataCloneErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	movb	$0, %ah
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1707:
	.cfi_restore_state
	movq	(%rdi), %rdx
	movq	45752(%rdx), %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker26RegisterWasmMemoryAsSharedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE@PLT
	movq	24(%r12), %r14
	leaq	1(%r14), %rbx
	cmpq	32(%r12), %rbx
	ja	.L1709
.L1711:
	movq	16(%r12), %rax
	movq	%rbx, 24(%r12)
	movb	$109, (%rax,%r14)
.L1710:
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movslq	35(%rax), %rsi
	leal	(%rsi,%rsi), %eax
	sarl	$31, %esi
	xorl	%eax, %esi
	call	_ZN2v88internal15ValueSerializer11WriteVarintIjEEvT_
	movq	(%r12), %rbx
	movq	0(%r13), %rax
	movq	41112(%rbx), %rdi
	movq	23(%rax), %r13
	testq	%rdi, %rdi
	je	.L1713
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15ValueSerializer15WriteJSReceiverENS0_6HandleINS0_10JSReceiverEEE
	.p2align 4,,10
	.p2align 3
.L1713:
	.cfi_restore_state
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L1718
.L1715:
	leaq	8(%rsi), %rax
	movq	%r12, %rdi
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15ValueSerializer15WriteJSReceiverENS0_6HandleINS0_10JSReceiverEEE
	.p2align 4,,10
	.p2align 3
.L1709:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15ValueSerializer12ExpandBufferEm
	testb	%al, %al
	jne	.L1711
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1718:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1715
	.cfi_endproc
.LFE18741:
	.size	_ZN2v88internal15ValueSerializer15WriteWasmMemoryENS0_6HandleINS0_16WasmMemoryObjectEEE, .-_ZN2v88internal15ValueSerializer15WriteWasmMemoryENS0_6HandleINS0_16WasmMemoryObjectEEE
	.section	.text._ZN2v88internal11HandleScope14CloseAndEscapeINS0_7JSArrayEEENS0_6HandleIT_EES6_,"axG",@progbits,_ZN2v88internal11HandleScope14CloseAndEscapeINS0_7JSArrayEEENS0_6HandleIT_EES6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_7JSArrayEEENS0_6HandleIT_EES6_
	.type	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_7JSArrayEEENS0_6HandleIT_EES6_, @function
_ZN2v88internal11HandleScope14CloseAndEscapeINS0_7JSArrayEEENS0_6HandleIT_EES6_:
.LFB21266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdx
	movq	(%rsi), %r14
	movq	16(%rdi), %rax
	subl	$1, 41104(%rbx)
	movq	%rdx, 41088(%rbx)
	cmpq	41096(%rbx), %rax
	je	.L1720
	movq	%rax, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1720:
	movq	(%r12), %r13
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1721
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1722:
	movq	41088(%rbx), %rdx
	movq	%rdx, 8(%r12)
	movq	41096(%rbx), %rdx
	movq	%rdx, 16(%r12)
	addl	$1, 41104(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1721:
	.cfi_restore_state
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1725
.L1723:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r14, (%rax)
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1725:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1723
	.cfi_endproc
.LFE21266:
	.size	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_7JSArrayEEENS0_6HandleIT_EES6_, .-_ZN2v88internal11HandleScope14CloseAndEscapeINS0_7JSArrayEEENS0_6HandleIT_EES6_
	.section	.rodata._ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC14:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB22828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movabsq	$1152921504606846975, %rsi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r12
	movq	%rcx, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L1753
	movq	%r13, %r9
	movq	%rdi, %r15
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L1738
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1754
.L1728:
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L1737:
	movq	(%rdx), %rax
	movq	%rax, (%rbx,%r9)
	cmpq	%r12, %r13
	je	.L1730
	leaq	-8(%r13), %rsi
	leaq	15(%rbx), %rax
	subq	%r12, %rsi
	subq	%r12, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L1740
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L1740
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1732:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1732
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%r8, %rdi
	je	.L1734
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1734:
	leaq	16(%rbx,%rsi), %r14
.L1730:
	cmpq	%rcx, %r13
	je	.L1735
	subq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r14
.L1735:
	testq	%r12, %r12
	je	.L1736
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1736:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1754:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L1729
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L1737
	.p2align 4,,10
	.p2align 3
.L1738:
	movl	$8, %r14d
	jmp	.L1728
	.p2align 4,,10
	.p2align 3
.L1740:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %r13
	jne	.L1731
	jmp	.L1734
.L1729:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r14
	salq	$3, %r14
	jmp	.L1728
.L1753:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22828:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.rodata._ZN2v88internal17ValueDeserializer22ReadJSObjectPropertiesENS0_6HandleINS0_8JSObjectEEENS0_16SerializationTagEb.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"properties.size() < std::numeric_limits<uint32_t>::max()"
	.section	.text._ZN2v88internal17ValueDeserializer22ReadJSObjectPropertiesENS0_6HandleINS0_8JSObjectEEENS0_16SerializationTagEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer22ReadJSObjectPropertiesENS0_6HandleINS0_8JSObjectEEENS0_16SerializationTagEb
	.type	_ZN2v88internal17ValueDeserializer22ReadJSObjectPropertiesENS0_6HandleINS0_8JSObjectEEENS0_16SerializationTagEb, @function
_ZN2v88internal17ValueDeserializer22ReadJSObjectPropertiesENS0_6HandleINS0_8JSObjectEEENS0_16SerializationTagEb:
.LFB18834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -208(%rbp)
	testb	%cl, %cl
	jne	.L1922
.L1756:
	movb	%r13b, -200(%rbp)
	leaq	-144(%rbp), %r15
.L1838:
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rcx
	movq	%rsi, %rax
	jmp	.L1830
	.p2align 4,,10
	.p2align 3
.L1924:
	movzbl	(%rax), %edx
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L1923
.L1830:
	cmpq	%rax, %rcx
	ja	.L1924
.L1837:
	xorl	%eax, %eax
.L1827:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1925
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1923:
	.cfi_restore_state
	cmpb	%dl, -200(%rbp)
	je	.L1832
	movq	%rbx, %rdi
	call	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1837
	movq	%rax, %rdi
	call	_ZN2v88internalL16IsValidObjectKeyENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1837
	movq	%rbx, %rdi
	call	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1837
	movq	(%rbx), %rsi
	movl	$1, %r9d
	movq	%r13, %rcx
	movq	%r14, %rdx
	leaq	-176(%rbp), %r8
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -176(%rbp)
	je	.L1837
	cmpl	$4, -140(%rbp)
	jne	.L1837
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE@PLT
	testq	%rax, %rax
	je	.L1837
	addl	$1, -208(%rbp)
	jmp	.L1838
	.p2align 4,,10
	.p2align 3
.L1926:
	movzbl	(%rsi), %eax
	addq	$1, %rsi
	movq	%rsi, 16(%rbx)
	testb	%al, %al
	jne	.L1842
.L1832:
	cmpq	%rsi, %rcx
	ja	.L1926
	call	_ZN2v82V817FromJustIsNothingEv@PLT
.L1842:
	movq	-208(%rbp), %rax
	salq	$32, %rax
	orq	$1, %rax
	jmp	.L1827
	.p2align 4,,10
	.p2align 3
.L1922:
	movq	(%rdi), %r12
	movq	(%rsi), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1757
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1758:
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	$0, -160(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_Znwm@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%rdi, %rcx
	je	.L1767
	leaq	-8(%rcx), %rax
	leaq	15(%rdi), %rdx
	subq	%rdi, %rax
	subq	%r12, %rdx
	shrq	$3, %rax
	cmpq	$30, %rdx
	jbe	.L1847
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rax
	je	.L1847
	addq	$1, %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	shrq	%rcx
	salq	$4, %rcx
.L1765:
	movdqu	(%rdi,%rdx), %xmm1
	movups	%xmm1, (%r12,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L1765
	movq	%rax, %rsi
	andq	$-2, %rsi
	leaq	0(,%rsi,8), %rdx
	leaq	(%rdi,%rdx), %rcx
	addq	%r12, %rdx
	cmpq	%rax, %rsi
	je	.L1761
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
.L1761:
	call	_ZdlPv@PLT
.L1762:
	movq	%r12, %xmm0
	leaq	-144(%rbp), %rax
	addq	$64, %r12
	movq	%r14, -200(%rbp)
	movq	%r12, -160(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -216(%rbp)
	movaps	%xmm0, -176(%rbp)
.L1821:
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rcx
	movq	%rsi, %rax
	jmp	.L1770
	.p2align 4,,10
	.p2align 3
.L1928:
	movzbl	(%rax), %edx
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L1927
.L1770:
	cmpq	%rax, %rcx
	ja	.L1928
.L1823:
	movq	-176(%rbp), %rdi
	xorl	%eax, %eax
.L1774:
	testq	%rdi, %rdi
	je	.L1827
	movq	%rax, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %rax
	jmp	.L1827
.L1757:
	movq	41088(%r12), %r15
	cmpq	%r15, 41096(%r12)
	je	.L1929
.L1759:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1927:
	cmpb	%dl, %r13b
	je	.L1930
	movq	(%r15), %rax
	movq	(%rbx), %rdx
	movq	71(%rax), %r12
	testb	$1, %r12b
	je	.L1778
	cmpl	$3, %r12d
	je	.L1778
	movq	%r12, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L1931
	cmpq	$1, %rax
	je	.L1932
.L1779:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1932:
	movq	-1(%r12), %rax
	cmpw	$149, 11(%rax)
	je	.L1778
	movq	-1(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L1778:
	movq	%rbx, %rdi
	call	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1823
	movq	%r14, %rdi
	call	_ZN2v88internalL16IsValidObjectKeyENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1823
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L1933
.L1791:
	movq	%rbx, %rdi
	movq	%r14, %r12
	movq	$0, -184(%rbp)
	movq	-200(%rbp), %r14
	call	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	testq	%rax, %rax
	je	.L1823
	movq	%rax, -184(%rbp)
	movq	-168(%rbp), %rax
.L1803:
	subq	-176(%rbp), %rax
	movabsq	$34359738359, %rdx
	cmpq	%rdx, %rax
	ja	.L1822
	movq	%r15, %rsi
	leaq	-176(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internalL16CommitPropertiesENS0_6HandleINS0_8JSObjectEEENS1_INS0_3MapEEERKSt6vectorINS1_INS0_6ObjectEEESaIS8_EE
	movq	-176(%rbp), %rax
	movq	(%rbx), %rsi
	leaq	-144(%rbp), %r15
	movl	$1, %r9d
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rax, -200(%rbp)
	movq	-168(%rbp), %rax
	leaq	-185(%rbp), %r8
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -185(%rbp)
	je	.L1823
	cmpl	$4, -140(%rbp)
	jne	.L1823
	movq	-184(%rbp), %rsi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE@PLT
	testq	%rax, %rax
	je	.L1823
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1825
	call	_ZdlPv@PLT
.L1825:
	movq	-208(%rbp), %r12
	subq	-200(%rbp), %r12
	sarq	$3, %r12
	leal	1(%r12), %eax
	movl	%eax, -208(%rbp)
	jmp	.L1756
	.p2align 4,,10
	.p2align 3
.L1931:
	andq	$-3, %r12
	movq	39(%r12), %rcx
	movl	15(%r12), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	7(%rcx,%rax), %rax
	movq	%rax, %rsi
	shrq	$35, %rax
	sarq	$32, %rsi
	andl	$7, %eax
	andl	$2, %esi
	orl	%esi, %eax
	jne	.L1778
	movl	15(%r12), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rcx), %rsi
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L1778
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1782
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-208(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L1778
.L1783:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal17ValueDeserializer18ReadExpectedStringENS0_6HandleINS0_6StringEEE
	movq	-208(%rbp), %rdx
	testb	%al, %al
	je	.L1778
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1785
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L1786:
	movq	%rbx, %rdi
	movq	%r10, -208(%rbp)
	movq	$0, -184(%rbp)
	call	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	movq	-208(%rbp), %r10
	testq	%rax, %rax
	je	.L1823
	movq	%rax, -184(%rbp)
	jmp	.L1840
.L1933:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1791
	movq	(%rbx), %rdi
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L1934
.L1795:
	movq	%rdi, -144(%rbp)
	movq	%r15, -136(%rbp)
	movq	(%r15), %rax
	movq	$0, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -120(%rbp)
	testb	$1, %al
	je	.L1796
	cmpl	$3, %eax
	je	.L1796
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L1935
	cmpq	$1, %rdx
	jne	.L1779
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L1800
	movl	$4, -112(%rbp)
.L1798:
	movq	-216(%rbp), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal19TransitionsAccessor28FindTransitionToDataPropertyENS0_6HandleINS0_4NameEEENS1_17RequestedLocationE@PLT
	movq	%rbx, %rdi
	movq	$0, -184(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	movq	-208(%rbp), %r10
	testq	%rax, %rax
	je	.L1823
	movq	%rax, -184(%rbp)
	testq	%r10, %r10
	je	.L1936
.L1840:
	movq	-168(%rbp), %rax
	movq	(%r10), %rsi
	movq	%rax, %r12
	subq	-176(%rbp), %r12
	movq	39(%rsi), %rdi
	sarq	$3, %r12
	leal	3(%r12,%r12,2), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	7(%rdx,%rdi), %r8
	movq	-184(%rbp), %rdi
	movzbl	_ZN2v88internal17FLAG_track_fieldsE(%rip), %r9d
	movq	%r8, %rcx
	shrq	$38, %r8
	movq	(%rdi), %rdi
	andl	$7, %r8d
	sarq	$32, %rcx
	cmpl	$1, %r8d
	jne	.L1804
	testb	%r9b, %r9b
	jne	.L1937
.L1804:
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	je	.L1848
	cmpl	$2, %r8d
	je	.L1806
.L1848:
	cmpl	$3, %r8d
	jne	.L1810
	cmpb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	jne	.L1938
.L1810:
	testb	%r9b, %r9b
	je	.L1814
	testl	%r8d, %r8d
	je	.L1920
.L1814:
	cmpl	$3, %r8d
	je	.L1817
.L1818:
	movq	-168(%rbp), %rsi
	cmpq	-160(%rbp), %rsi
	je	.L1819
	movq	-184(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -168(%rbp)
.L1820:
	movq	%r10, %r15
	jmp	.L1821
.L1847:
	movq	%r12, %rdx
	movq	%rdi, %rax
.L1763:
	movq	(%rax), %rsi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %rcx
	jne	.L1763
.L1767:
	testq	%rdi, %rdi
	je	.L1762
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1785:
	movq	41088(%rdx), %r10
	cmpq	41096(%rdx), %r10
	je	.L1939
.L1787:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%rdx)
	movq	%r12, (%r10)
	jmp	.L1786
.L1937:
	notq	%rdi
	movq	%rdi, %rdx
	andl	$1, %edx
.L1805:
	testb	%dl, %dl
	jne	.L1818
.L1920:
	movq	%r14, %r12
	movq	-200(%rbp), %r14
	jmp	.L1803
.L1796:
	movl	$1, -112(%rbp)
	jmp	.L1798
.L1930:
	movq	-200(%rbp), %r14
	jmp	.L1772
.L1940:
	movzbl	(%rsi), %eax
	addq	$1, %rsi
	movq	%rsi, 16(%rbx)
	testb	%al, %al
	jne	.L1839
.L1772:
	cmpq	%rcx, %rsi
	jb	.L1940
	call	_ZN2v82V817FromJustIsNothingEv@PLT
.L1839:
	leaq	-176(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internalL16CommitPropertiesENS0_6HandleINS0_8JSObjectEEENS1_INS0_3MapEEERKSt6vectorINS1_INS0_6ObjectEEESaIS8_EE
	movq	-176(%rbp), %rdi
	movl	$4294967294, %ecx
	movq	-168(%rbp), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	movq	%rdx, %rax
	salq	$32, %rax
	orq	$1, %rax
	cmpq	%rcx, %rdx
	jbe	.L1774
.L1822:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1929:
	movq	%r12, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1759
.L1934:
	movq	%r14, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r14
	jmp	.L1795
.L1936:
	movq	%r14, %r12
	movq	-168(%rbp), %rax
	movq	-200(%rbp), %r14
	jmp	.L1803
.L1806:
	testb	$1, %dil
	je	.L1818
	movq	-1(%rdi), %rdx
	cmpw	$65, 11(%rdx)
	sete	%dl
	jmp	.L1805
.L1938:
	andl	$1, %edi
	je	.L1920
.L1817:
	movq	39(%rsi), %rax
	movq	%rcx, -232(%rbp)
	movq	%r10, -224(%rbp)
	movl	%r8d, -208(%rbp)
	movq	15(%rdx,%rax), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	-216(%rbp), %rdi
	movq	%rax, -144(%rbp)
	movq	-184(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZNK2v88internal9FieldType11NowContainsENS0_6ObjectE@PLT
	movl	-208(%rbp), %r8d
	movq	-224(%rbp), %r10
	testb	%al, %al
	movq	-232(%rbp), %rcx
	jne	.L1818
	movq	-184(%rbp), %rax
	movq	(%rbx), %rsi
	movl	%r8d, %edx
	movq	%rcx, -232(%rbp)
	movq	-216(%rbp), %rdi
	movq	%r10, -224(%rbp)
	movq	(%rax), %rax
	movl	%r8d, -208(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal6Object11OptimalTypeEPNS0_7IsolateENS0_14RepresentationE@PLT
	movq	-232(%rbp), %rcx
	movq	(%rbx), %rdi
	movl	%r12d, %edx
	movq	-224(%rbp), %r10
	movl	-208(%rbp), %r8d
	movq	%rax, %r9
	shrl	$2, %ecx
	movq	%r10, %rsi
	andl	$1, %ecx
	movq	%r10, -208(%rbp)
	call	_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE@PLT
	movq	-208(%rbp), %r10
	jmp	.L1818
.L1939:
	movq	%rdx, %rdi
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rdx
	movq	%rax, %r10
	jmp	.L1787
.L1782:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L1941
.L1784:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L1783
.L1819:
	leaq	-184(%rbp), %rdx
	leaq	-176(%rbp), %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-208(%rbp), %r10
	jmp	.L1820
.L1935:
	movl	$3, -112(%rbp)
	jmp	.L1798
.L1800:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -112(%rbp)
	jmp	.L1798
.L1941:
	movq	%rdx, %rdi
	movq	%rsi, -224(%rbp)
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	-208(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L1784
.L1925:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18834:
	.size	_ZN2v88internal17ValueDeserializer22ReadJSObjectPropertiesENS0_6HandleINS0_8JSObjectEEENS0_16SerializationTagEb, .-_ZN2v88internal17ValueDeserializer22ReadJSObjectPropertiesENS0_6HandleINS0_8JSObjectEEENS0_16SerializationTagEb
	.section	.text._ZN2v88internal17ValueDeserializer12ReadJSObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer12ReadJSObjectEv
	.type	_ZN2v88internal17ValueDeserializer12ReadJSObjectEv, @function
_ZN2v88internal17ValueDeserializer12ReadJSObjectEv:
.LFB18786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r12), %rax
	jnb	.L1943
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	xorl	%eax, %eax
.L1944:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1943:
	.cfi_restore_state
	movl	40(%rbx), %r14d
	movq	(%rbx), %r15
	leal	1(%r14), %eax
	movl	%eax, 40(%rbx)
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	(%rbx), %r12
	movq	41096(%r15), %r13
	movzbl	32(%rbx), %edx
	movq	%rax, -56(%rbp)
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1945
	movq	%r8, %rsi
	movb	%dl, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-64(%rbp), %edx
	movq	%rax, %rsi
.L1946:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	48(%rbx), %rsi
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	movl	%r14d, %edx
	movq	%rax, %r12
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L1948
	testq	%rax, %rax
	je	.L1949
	testq	%rdi, %rdi
	je	.L1949
	movq	(%rdi), %rcx
	cmpq	%rcx, (%rax)
	jne	.L1949
.L1948:
	movl	$1, %ecx
	movl	$123, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17ValueDeserializer22ReadJSObjectPropertiesENS0_6HandleINS0_8JSObjectEEENS0_16SerializationTagEb
	testb	%al, %al
	je	.L1964
	movq	24(%rbx), %r9
	movq	16(%rbx), %rdx
	shrq	$32, %rax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L1954
	.p2align 4,,10
	.p2align 3
.L1952:
	movzbl	(%rdx), %edi
	cmpl	$31, %ecx
	ja	.L1953
	movl	%edi, %esi
	andl	$127, %esi
	sall	%cl, %esi
	addl	$7, %ecx
	orl	%esi, %r8d
.L1953:
	addq	$1, %rdx
	movq	%rdx, 16(%rbx)
	testb	%dil, %dil
	jns	.L1965
.L1954:
	cmpq	%rdx, %r9
	ja	.L1952
.L1964:
	movl	41104(%r15), %eax
	movq	41096(%r15), %rcx
	leal	-1(%rax), %edx
.L1951:
	movq	-56(%rbp), %rbx
	movl	%edx, 41104(%r15)
	xorl	%eax, %eax
	movq	%rbx, 41088(%r15)
	cmpq	%rcx, %r13
	je	.L1944
	movq	%r13, 41096(%r15)
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1945:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1966
.L1947:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1946
	.p2align 4,,10
	.p2align 3
.L1949:
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rdx
	movq	-64(%rbp), %rax
	movq	41152(%rdx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
	jmp	.L1948
	.p2align 4,,10
	.p2align 3
.L1965:
	movl	41104(%r15), %ecx
	leal	-1(%rcx), %edx
	movq	41096(%r15), %rcx
	cmpl	%r8d, %eax
	jne	.L1951
	movq	-56(%rbp), %rax
	movq	(%r12), %r12
	movl	%edx, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	%rcx, %r13
	je	.L1958
	movq	%r13, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1958:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1959
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1944
	.p2align 4,,10
	.p2align 3
.L1966:
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	movb	%dl, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movzbl	-64(%rbp), %edx
	movq	%rax, %rsi
	jmp	.L1947
.L1959:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L1967
.L1961:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%r12, (%rax)
	jmp	.L1944
.L1967:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1961
	.cfi_endproc
.LFE18786:
	.size	_ZN2v88internal17ValueDeserializer12ReadJSObjectEv, .-_ZN2v88internal17ValueDeserializer12ReadJSObjectEv
	.section	.text._ZN2v88internal17ValueDeserializer17ReadSparseJSArrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer17ReadSparseJSArrayEv
	.type	_ZN2v88internal17ValueDeserializer17ReadSparseJSArrayEv, @function
_ZN2v88internal17ValueDeserializer17ReadSparseJSArrayEv:
.LFB18790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r12), %rax
	jnb	.L1969
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	xorl	%eax, %eax
.L1970:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1992
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1969:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	movq	16(%rbx), %rax
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1971:
	movzbl	(%rax), %esi
	cmpl	$31, %ecx
	ja	.L1972
	movl	%esi, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r12d
.L1972:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%sil, %sil
	jns	.L1993
.L1973:
	cmpq	%rax, %rdi
	ja	.L1971
	xorl	%eax, %eax
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L1993:
	movl	40(%rbx), %r14d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movl	$3, %esi
	leal	1(%r14), %eax
	movl	%eax, 40(%rbx)
	movq	(%rbx), %rax
	movq	41088(%rax), %rdx
	addl	$1, 41104(%rax)
	movzbl	32(%rbx), %r9d
	movq	(%rbx), %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	movq	41096(%rax), %rdx
	movq	%rdx, -48(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal7JSArray9SetLengthENS0_6HandleIS1_EEj@PLT
	movq	(%rbx), %rdi
	movq	48(%rbx), %rsi
	movl	%r14d, %edx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %r14
	cmpq	%rdi, %rax
	je	.L1974
	testq	%rax, %rax
	je	.L1975
	testq	%rdi, %rdi
	je	.L1975
	movq	(%rdi), %rax
	cmpq	%rax, (%r14)
	jne	.L1975
.L1974:
	xorl	%ecx, %ecx
	movl	$64, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17ValueDeserializer22ReadJSObjectPropertiesENS0_6HandleINS0_8JSObjectEEENS0_16SerializationTagEb
	testb	%al, %al
	je	.L1978
	movq	24(%rbx), %r9
	movq	16(%rbx), %rdx
	shrq	$32, %rax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L1980
	.p2align 4,,10
	.p2align 3
.L1995:
	movzbl	(%rdx), %edi
	cmpl	$31, %ecx
	ja	.L1979
	movl	%edi, %esi
	andl	$127, %esi
	sall	%cl, %esi
	addl	$7, %ecx
	orl	%esi, %r8d
.L1979:
	addq	$1, %rdx
	movq	%rdx, 16(%rbx)
	testb	%dil, %dil
	jns	.L1994
.L1980:
	cmpq	%rdx, %r9
	ja	.L1995
.L1978:
	xorl	%eax, %eax
.L1977:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1970
	movq	-56(%rbp), %rcx
	subl	$1, 41104(%rdi)
	movq	-48(%rbp), %rdx
	movq	%rcx, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L1970
	movq	%rdx, 41096(%rdi)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L1975:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	(%r14), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
	jmp	.L1974
	.p2align 4,,10
	.p2align 3
.L1994:
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	jmp	.L1982
	.p2align 4,,10
	.p2align 3
.L1997:
	movzbl	(%rdx), %edi
	cmpl	$31, %ecx
	ja	.L1981
	movl	%edi, %esi
	andl	$127, %esi
	sall	%cl, %esi
	addl	$7, %ecx
	orl	%esi, %r10d
.L1981:
	addq	$1, %rdx
	movq	%rdx, 16(%rbx)
	testb	%dil, %dil
	jns	.L1996
.L1982:
	cmpq	%rdx, %r9
	ja	.L1997
	jmp	.L1978
.L1996:
	cmpl	%eax, %r8d
	jne	.L1978
	cmpl	%r10d, %r12d
	jne	.L1978
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_7JSArrayEEENS0_6HandleIT_EES6_
	jmp	.L1977
.L1992:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18790:
	.size	_ZN2v88internal17ValueDeserializer17ReadSparseJSArrayEv, .-_ZN2v88internal17ValueDeserializer17ReadSparseJSArrayEv
	.section	.text._ZN2v88internal17ValueDeserializer16ReadDenseJSArrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer16ReadDenseJSArrayEv
	.type	_ZN2v88internal17ValueDeserializer16ReadDenseJSArrayEv, @function
_ZN2v88internal17ValueDeserializer16ReadDenseJSArrayEv:
.LFB18794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r12), %rax
	jnb	.L1999
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	xorl	%eax, %eax
.L2000:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2057
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1999:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	movq	16(%rbx), %rax
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	jmp	.L2003
	.p2align 4,,10
	.p2align 3
.L2001:
	movzbl	(%rax), %esi
	cmpl	$31, %ecx
	ja	.L2002
	movl	%esi, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r12d
.L2002:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%sil, %sil
	jns	.L2058
.L2003:
	cmpq	%rax, %rdi
	ja	.L2001
.L2004:
	xorl	%eax, %eax
	jmp	.L2000
	.p2align 4,,10
	.p2align 3
.L2058:
	cmpl	$134217726, %r12d
	ja	.L2004
	movl	%r12d, %edx
	subq	%rax, %rdi
	cmpq	%rdi, %rdx
	ja	.L2004
	movl	40(%rbx), %r13d
	movl	$1, %r8d
	movl	%r12d, %ecx
	movl	$3, %esi
	leal	1(%r13), %eax
	movl	%eax, 40(%rbx)
	movq	(%rbx), %rax
	movq	41088(%rax), %rdx
	addl	$1, 41104(%rax)
	movzbl	32(%rbx), %r9d
	movq	(%rbx), %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	41096(%rax), %rdx
	movq	%rdx, -64(%rbp)
	movl	%r12d, %edx
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	48(%rbx), %rsi
	movl	%r13d, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	movq	%rax, %r14
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %r13
	cmpq	%rdi, %rax
	je	.L2005
	testq	%rdi, %rdi
	je	.L2006
	testq	%rax, %rax
	je	.L2006
	movq	(%rdi), %rax
	cmpq	%rax, 0(%r13)
	je	.L2005
.L2006:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	0(%r13), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
.L2005:
	movq	(%rbx), %r15
	movq	(%r14), %rax
	movq	41112(%r15), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2007
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L2008:
	leal	-1(%r12), %eax
	xorl	%r15d, %r15d
	movq	%rax, -88(%rbp)
	testl	%r12d, %r12d
	je	.L2028
.L2029:
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rcx
	movq	%rsi, %rax
	jmp	.L2015
	.p2align 4,,10
	.p2align 3
.L2060:
	movzbl	(%rax), %edx
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L2059
.L2015:
	cmpq	%rax, %rcx
	ja	.L2060
.L2013:
	movq	%rbx, %rdi
	call	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	testq	%rax, %rax
	je	.L2012
	cmpl	$10, 36(%rbx)
	ja	.L2022
	movq	(%rbx), %rdx
	movq	(%rax), %rsi
	cmpq	%rsi, 88(%rdx)
	je	.L2020
.L2022:
	movq	0(%r13), %rdi
	cmpl	%r15d, 11(%rdi)
	jbe	.L2012
	movq	(%rax), %rdx
	leaq	15(%rdi,%r15,8), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L2020
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -96(%rbp)
	testl	$262144, %eax
	je	.L2026
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%rcx), %rax
.L2026:
	testb	$24, %al
	je	.L2020
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2020
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2020:
	leaq	1(%r15), %rax
	cmpq	%r15, -88(%rbp)
	je	.L2028
	movq	%rax, %r15
	jmp	.L2029
	.p2align 4,,10
	.p2align 3
.L2059:
	cmpb	$45, %dl
	jne	.L2013
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2061:
	movzbl	(%rsi), %eax
	addq	$1, %rsi
	movq	%rsi, 16(%rbx)
	testb	%al, %al
	jne	.L2020
.L2019:
	cmpq	%rsi, %rcx
	ja	.L2061
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2020
.L2028:
	xorl	%ecx, %ecx
	movl	$36, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17ValueDeserializer22ReadJSObjectPropertiesENS0_6HandleINS0_8JSObjectEEENS0_16SerializationTagEb
	testb	%al, %al
	je	.L2012
	shrq	$32, %rax
	movq	24(%rbx), %r9
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movq	%rax, %r8
	movq	16(%rbx), %rax
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2063:
	movzbl	(%rax), %esi
	cmpl	$31, %ecx
	ja	.L2030
	movl	%esi, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %edi
.L2030:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%sil, %sil
	jns	.L2062
.L2031:
	cmpq	%rax, %r9
	ja	.L2063
	.p2align 4,,10
	.p2align 3
.L2012:
	xorl	%eax, %eax
.L2021:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2000
	movq	-72(%rbp), %rcx
	subl	$1, 41104(%rdi)
	movq	-64(%rbp), %rdx
	movq	%rcx, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L2000
	movq	%rdx, 41096(%rdi)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rax
	jmp	.L2000
	.p2align 4,,10
	.p2align 3
.L2007:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L2064
.L2009:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L2008
.L2064:
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2009
.L2062:
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	jmp	.L2033
.L2066:
	movzbl	(%rax), %esi
	cmpl	$31, %ecx
	ja	.L2032
	movl	%esi, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r10d
.L2032:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%sil, %sil
	jns	.L2065
.L2033:
	cmpq	%rax, %r9
	ja	.L2066
	jmp	.L2012
.L2065:
	cmpl	%r10d, %r12d
	jne	.L2012
	cmpl	%r8d, %edi
	jne	.L2012
	leaq	-80(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_7JSArrayEEENS0_6HandleIT_EES6_
	jmp	.L2021
.L2057:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18794:
	.size	_ZN2v88internal17ValueDeserializer16ReadDenseJSArrayEv, .-_ZN2v88internal17ValueDeserializer16ReadDenseJSArrayEv
	.section	.text._ZN2v88internal17ValueDeserializer18ReadObjectInternalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer18ReadObjectInternalEv
	.type	_ZN2v88internal17ValueDeserializer18ReadObjectInternalEv, @function
_ZN2v88internal17ValueDeserializer18ReadObjectInternalEv:
.LFB18770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	24(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	jmp	.L2070
	.p2align 4,,10
	.p2align 3
.L2129:
	movzbl	(%rax), %esi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rdi)
	testb	%sil, %sil
	jne	.L2069
	movq	%rdx, %rax
.L2070:
	cmpq	%rax, %r8
	ja	.L2129
.L2068:
	xorl	%r9d, %r9d
.L2073:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2125
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%r9, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2069:
	.cfi_restore_state
	leal	-34(%rsi), %ecx
	cmpb	$88, %cl
	ja	.L2114
	leaq	.L2115(%rip), %r9
	movzbl	%cl, %ecx
	movslq	(%r9,%rcx,4), %rcx
	addq	%r9, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal17ValueDeserializer18ReadObjectInternalEv,"a",@progbits
	.align 4
	.align 4
.L2115:
	.long	.L2092-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2104-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2076-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2103-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2071-.L2115
	.long	.L2114-.L2115
	.long	.L2099-.L2115
	.long	.L2105-.L2115
	.long	.L2114-.L2115
	.long	.L2100-.L2115
	.long	.L2114-.L2115
	.long	.L2078-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2117-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2087-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2102-.L2115
	.long	.L2091-.L2115
	.long	.L2077-.L2115
	.long	.L2118-.L2115
	.long	.L2114-.L2115
	.long	.L2110-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2090-.L2115
	.long	.L2114-.L2115
	.long	.L2127-.L2115
	.long	.L2114-.L2115
	.long	.L2119-.L2115
	.long	.L2075-.L2115
	.long	.L2114-.L2115
	.long	.L2098-.L2115
	.long	.L2114-.L2115
	.long	.L2093-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2112-.L2115
	.long	.L2101-.L2115
	.long	.L2097-.L2115
	.long	.L2114-.L2115
	.long	.L2114-.L2115
	.long	.L2108-.L2115
	.long	.L2101-.L2115
	.long	.L2106-.L2115
	.long	.L2107-.L2115
	.long	.L2114-.L2115
	.long	.L2111-.L2115
	.long	.L2101-.L2115
	.long	.L2101-.L2115
	.long	.L2101-.L2115
	.section	.text._ZN2v88internal17ValueDeserializer18ReadObjectInternalEv
	.p2align 4,,10
	.p2align 3
.L2072:
	movzbl	(%rdx), %eax
	addq	$1, %rdx
	movq	%rdx, 16(%rdi)
	testb	%al, %al
	jns	.L2130
.L2071:
	cmpq	%r8, %rdx
	jb	.L2072
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L2114:
	cmpl	$12, 36(%rdi)
	ja	.L2068
	movq	%rax, 16(%rdi)
.L2127:
	call	_ZN2v88internal17ValueDeserializer14ReadHostObjectEv
	movq	%rax, %r9
	jmp	.L2073
.L2101:
	call	_ZN2v88internal17ValueDeserializer22ReadJSPrimitiveWrapperENS0_16SerializationTagE
	movq	%rax, %r9
	jmp	.L2073
.L2100:
	call	_ZN2v88internal17ValueDeserializer10ReadJSDateEv
	movq	%rax, %r9
	jmp	.L2073
.L2078:
	movq	(%rdi), %r9
	addq	$120, %r9
	jmp	.L2073
.L2090:
	call	_ZN2v88internal17ValueDeserializer10ReadBigIntEv
	movq	%rax, %r9
	jmp	.L2073
.L2106:
	call	_ZN2v88internal17ValueDeserializer28ReadTransferredJSArrayBufferEv
	movq	%rax, %r9
	jmp	.L2073
.L2097:
	call	_ZN2v88internal17ValueDeserializer12ReadJSObjectEv
	movq	%rax, %r9
	jmp	.L2073
.L2103:
	call	_ZN2v88internal17ValueDeserializer9ReadJSMapEv
	movq	%rax, %r9
	jmp	.L2073
.L2110:
	call	_ZN2v88internal17ValueDeserializer14ReadWasmModuleEv
	movq	%rax, %r9
	jmp	.L2073
.L2087:
	subq	$8, %r8
	xorl	%r9d, %r9d
	cmpq	%r8, %rdx
	ja	.L2073
	movq	1(%rax), %rdx
	addq	$9, %rax
	movq	%rax, 16(%rdi)
	movq	%rdx, %xmm0
	movq	%rdx, -16(%rbp)
	ucomisd	%xmm0, %xmm0
	jp	.L2131
.L2089:
	movzbl	32(%rdi), %esi
	movsd	-16(%rbp), %xmm0
	movq	(%rdi), %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	%rax, %r9
	jmp	.L2073
.L2102:
	call	_ZN2v88internal17ValueDeserializer12ReadJSRegExpEv
	movq	%rax, %r9
	jmp	.L2073
.L2105:
	xorl	%esi, %esi
	call	_ZN2v88internal17ValueDeserializer17ReadJSArrayBufferEb
	movq	%rax, %r9
	jmp	.L2073
.L2107:
	movl	$1, %esi
	call	_ZN2v88internal17ValueDeserializer17ReadJSArrayBufferEb
	movq	%rax, %r9
	jmp	.L2073
.L2111:
	call	_ZN2v88internal17ValueDeserializer22ReadWasmModuleTransferEv
	movq	%rax, %r9
	jmp	.L2073
.L2075:
	movq	(%rdi), %r9
	addq	$88, %r9
	jmp	.L2073
.L2098:
	call	_ZN2v88internal17ValueDeserializer17ReadSparseJSArrayEv
	movq	%rax, %r9
	jmp	.L2073
.L2093:
	call	_ZN2v88internal17ValueDeserializer17ReadTwoByteStringEv
	movq	%rax, %r9
	jmp	.L2073
.L2112:
	call	_ZN2v88internal17ValueDeserializer14ReadWasmMemoryEv
	movq	%rax, %r9
	jmp	.L2073
.L2108:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2125
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17ValueDeserializer11ReadJSErrorEv
.L2092:
	.cfi_restore_state
	call	_ZN2v88internal17ValueDeserializer17ReadOneByteStringEv
	movq	%rax, %r9
	jmp	.L2073
.L2104:
	call	_ZN2v88internal17ValueDeserializer9ReadJSSetEv
	movq	%rax, %r9
	jmp	.L2073
.L2076:
	movq	(%rdi), %r9
	addq	$104, %r9
	jmp	.L2073
.L2099:
	call	_ZN2v88internal17ValueDeserializer16ReadDenseJSArrayEv
	movq	%rax, %r9
	jmp	.L2073
.L2091:
	call	_ZN2v88internal17ValueDeserializer14ReadUtf8StringEv
	movq	%rax, %r9
	jmp	.L2073
.L2077:
	movq	(%rdi), %r9
	addq	$112, %r9
	jmp	.L2073
.L2118:
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2084:
	movzbl	(%rdx), %esi
	cmpl	$31, %ecx
	ja	.L2086
	movl	%esi, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r9d
.L2086:
	addq	$1, %rdx
	movq	%rdx, 16(%rdi)
	testb	%sil, %sil
	jns	.L2132
.L2083:
	cmpq	%rdx, %r8
	ja	.L2084
	jmp	.L2068
.L2119:
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	jmp	.L2094
	.p2align 4,,10
	.p2align 3
.L2095:
	movzbl	(%rdx), %esi
	cmpl	$31, %ecx
	ja	.L2096
	movl	%esi, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r9d
.L2096:
	addq	$1, %rdx
	movq	%rdx, 16(%rdi)
	testb	%sil, %sil
	jns	.L2133
.L2094:
	cmpq	%rdx, %r8
	ja	.L2095
	jmp	.L2068
.L2117:
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	jmp	.L2079
	.p2align 4,,10
	.p2align 3
.L2080:
	movzbl	(%rdx), %esi
	cmpl	$31, %ecx
	ja	.L2082
	movl	%esi, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r9d
.L2082:
	addq	$1, %rdx
	movq	%rdx, 16(%rdi)
	testb	%sil, %sil
	jns	.L2134
.L2079:
	cmpq	%rdx, %r8
	ja	.L2080
	jmp	.L2068
.L2133:
	movl	%r9d, %esi
	call	_ZN2v88internal17ValueDeserializer15GetObjectWithIDEj
	movq	%rax, %r9
	jmp	.L2073
.L2134:
	movl	%r9d, %eax
	shrl	%r9d
	movzbl	32(%rdi), %edx
	movq	(%rdi), %rdi
	andl	$1, %eax
	negl	%eax
	xorl	%r9d, %eax
	movl	%eax, %esi
	call	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r9
	jmp	.L2073
.L2132:
	movzbl	32(%rdi), %edx
	movq	(%rdi), %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	%rax, %r9
	jmp	.L2073
.L2130:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2125
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17ValueDeserializer10ReadObjectEv
.L2131:
	.cfi_restore_state
	movq	.LC5(%rip), %rax
	movq	%rax, -16(%rbp)
	jmp	.L2089
.L2125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18770:
	.size	_ZN2v88internal17ValueDeserializer18ReadObjectInternalEv, .-_ZN2v88internal17ValueDeserializer18ReadObjectInternalEv
	.section	.text._ZN2v88internal17ValueDeserializer10ReadObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	.type	_ZN2v88internal17ValueDeserializer10ReadObjectEv, @function
_ZN2v88internal17ValueDeserializer10ReadObjectEv:
.LFB18769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	movq	(%rbx), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r12), %rax
	jnb	.L2136
	movq	(%rbx), %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
.L2137:
	movq	%r13, %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2157
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2136:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal17ValueDeserializer18ReadObjectInternalEv
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2148
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L2158
.L2141:
	testq	%r12, %r12
	jne	.L2137
.L2148:
	movq	(%rbx), %r14
	movq	12480(%r14), %rax
	cmpq	%rax, 96(%r14)
	jne	.L2137
	xorl	%edx, %edx
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$362, %esi
	call	_ZN2v88internal7Factory8NewErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L2158:
	movq	-1(%rax), %rax
	cmpw	$1059, 11(%rax)
	jne	.L2141
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rcx
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L2140:
	cmpq	%rax, %rcx
	jbe	.L2141
	movzbl	(%rax), %edx
	addq	$1, %rax
	testb	%dl, %dl
	je	.L2140
	cmpb	$86, %dl
	jne	.L2141
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L2159:
	movzbl	(%rsi), %eax
	addq	$1, %rsi
	movq	%rsi, 16(%rbx)
	testb	%al, %al
	jne	.L2145
.L2143:
	cmpq	%rsi, %rcx
	ja	.L2159
	call	_ZN2v82V817FromJustIsNothingEv@PLT
.L2145:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17ValueDeserializer21ReadJSArrayBufferViewENS0_6HandleINS0_13JSArrayBufferEEE
	movq	%rax, %r12
	jmp	.L2141
.L2157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18769:
	.size	_ZN2v88internal17ValueDeserializer10ReadObjectEv, .-_ZN2v88internal17ValueDeserializer10ReadObjectEv
	.section	.text._ZN2v88internal17ValueDeserializer10ReadStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer10ReadStringEv
	.type	_ZN2v88internal17ValueDeserializer10ReadStringEv, @function
_ZN2v88internal17ValueDeserializer10ReadStringEv:
.LFB18771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$11, 36(%rdi)
	ja	.L2161
	movq	24(%rdi), %r9
	movq	16(%rdi), %rax
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2176:
	movzbl	(%rax), %r8d
	cmpl	$31, %ecx
	ja	.L2163
	movl	%r8d, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %esi
.L2163:
	addq	$1, %rax
	movq	%rax, 16(%rdi)
	testb	%r8b, %r8b
	jns	.L2175
.L2164:
	cmpq	%rax, %r9
	ja	.L2176
.L2167:
	xorl	%eax, %eax
.L2166:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L2177
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2161:
	.cfi_restore_state
	call	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	testq	%rax, %rax
	je	.L2167
	movq	(%rax), %rdx
	testb	$1, %dl
	je	.L2167
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L2166
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2175:
	testl	%esi, %esi
	js	.L2167
	movslq	%esi, %rsi
	subq	%rax, %r9
	cmpq	%r9, %rsi
	jg	.L2167
	leaq	(%rax,%rsi), %rdx
	movq	(%rdi), %r8
	movq	%rsi, -24(%rbp)
	leaq	-32(%rbp), %rsi
	movq	%rdx, 16(%rdi)
	movzbl	32(%rdi), %edx
	movq	%r8, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	jmp	.L2166
.L2177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18771:
	.size	_ZN2v88internal17ValueDeserializer10ReadStringEv, .-_ZN2v88internal17ValueDeserializer10ReadStringEv
	.section	.text._ZN2v88internal17ValueDeserializer22ReadJSPrimitiveWrapperENS0_16SerializationTagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer22ReadJSPrimitiveWrapperENS0_16SerializationTagE
	.type	_ZN2v88internal17ValueDeserializer22ReadJSPrimitiveWrapperENS0_16SerializationTagE, @function
_ZN2v88internal17ValueDeserializer22ReadJSPrimitiveWrapperENS0_16SerializationTagE:
.LFB18799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$110, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	40(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	1(%r13), %eax
	movl	%eax, 40(%rdi)
	cmpb	$12, %sil
	ja	.L2179
	leaq	.L2181(%rip), %rdx
	movzbl	%sil, %esi
	movq	%rdi, %rbx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal17ValueDeserializer22ReadJSPrimitiveWrapperENS0_16SerializationTagE,"a",@progbits
	.align 4
	.align 4
.L2181:
	.long	.L2185-.L2181
	.long	.L2179-.L2181
	.long	.L2179-.L2181
	.long	.L2179-.L2181
	.long	.L2179-.L2181
	.long	.L2184-.L2181
	.long	.L2179-.L2181
	.long	.L2179-.L2181
	.long	.L2179-.L2181
	.long	.L2179-.L2181
	.long	.L2183-.L2181
	.long	.L2182-.L2181
	.long	.L2180-.L2181
	.section	.text._ZN2v88internal17ValueDeserializer22ReadJSPrimitiveWrapperENS0_16SerializationTagE
	.p2align 4,,10
	.p2align 3
.L2185:
	movq	24(%rdi), %rax
	movq	16(%rdi), %rdx
	leaq	-8(%rax), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	ja	.L2208
	movq	(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, 16(%rdi)
	movq	%rax, %xmm0
	movq	%rax, -64(%rbp)
	ucomisd	%xmm0, %xmm0
	jp	.L2274
.L2200:
	movq	(%rbx), %r12
	movsd	-64(%rbp), %xmm0
	movzbl	32(%rbx), %r14d
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	871(%rax), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2201
	movq	%r15, %rsi
	movsd	%xmm0, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movsd	-72(%rbp), %xmm0
	movq	%rax, %rsi
.L2202:
	movl	%r14d, %edx
	movq	%r12, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movzbl	32(%rbx), %esi
	movsd	-72(%rbp), %xmm0
	movq	(%rbx), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%r12), %r15
	movq	(%rax), %r14
	leaq	23(%r15), %rsi
	movq	%r14, 23(%r15)
	testb	$1, %r14b
	jne	.L2266
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2184:
	call	_ZN2v88internal17ValueDeserializer10ReadStringEv
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2264
	movq	(%rbx), %r12
	movzbl	32(%rbx), %r15d
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1439(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2216
.L2271:
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2217:
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%r14), %r14
	movq	(%rax), %r15
	movq	%rax, %r12
	movq	%r14, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r14b
	je	.L2192
.L2266:
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	je	.L2220
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
.L2220:
	testb	$24, %al
	je	.L2192
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2192
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2192:
	movq	(%rbx), %rdi
	movq	48(%rbx), %rsi
	movl	%r13d, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %r13
	cmpq	%rdi, %rax
	je	.L2222
	testq	%rdi, %rdi
	je	.L2223
	testq	%rax, %rax
	je	.L2223
	movq	(%rdi), %rax
	cmpq	%rax, 0(%r13)
	je	.L2222
.L2223:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	0(%r13), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
.L2222:
	movq	%r12, %rax
.L2208:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2275
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2183:
	.cfi_restore_state
	movq	(%rdi), %r12
	movzbl	32(%rdi), %r14d
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	255(%rax), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2193
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2194:
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %r15
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	120(%rax), %r14
	leaq	23(%r15), %rsi
	movq	%r14, 23(%r15)
	testb	$1, %r14b
	jne	.L2266
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2182:
	movq	(%rdi), %r12
	movzbl	32(%rdi), %r14d
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	255(%rax), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2186
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2187:
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %r15
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	112(%rax), %r14
	leaq	23(%r15), %rsi
	movq	%r14, 23(%r15)
	testb	$1, %r14b
	jne	.L2266
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2180:
	call	_ZN2v88internal17ValueDeserializer10ReadBigIntEv
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2264
	movq	(%rbx), %r12
	movzbl	32(%rbx), %r15d
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	231(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L2271
.L2216:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2276
.L2218:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2217
	.p2align 4,,10
	.p2align 3
.L2193:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2277
.L2195:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L2194
	.p2align 4,,10
	.p2align 3
.L2186:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2278
.L2188:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2264:
	xorl	%eax, %eax
	jmp	.L2208
.L2179:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2201:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2279
.L2203:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L2202
	.p2align 4,,10
	.p2align 3
.L2276:
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2278:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2277:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2195
.L2274:
	movq	.LC5(%rip), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2200
.L2279:
	movq	%r12, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movsd	-72(%rbp), %xmm0
	movq	%rax, %rsi
	jmp	.L2203
.L2275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18799:
	.size	_ZN2v88internal17ValueDeserializer22ReadJSPrimitiveWrapperENS0_16SerializationTagE, .-_ZN2v88internal17ValueDeserializer22ReadJSPrimitiveWrapperENS0_16SerializationTagE
	.section	.text._ZN2v88internal17ValueDeserializer11ReadJSErrorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer11ReadJSErrorEv
	.type	_ZN2v88internal17ValueDeserializer11ReadJSErrorEv, @function
_ZN2v88internal17ValueDeserializer11ReadJSErrorEv:
.LFB18824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r15
	movq	12464(%r15), %rax
	leaq	88(%r15), %r12
	movq	%r12, %r13
	movq	39(%rax), %rax
	movq	1607(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2281
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2282:
	leaq	.L2291(%rip), %r15
.L2284:
	movq	24(%rbx), %r8
	movq	16(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	jmp	.L2288
	.p2align 4,,10
	.p2align 3
.L2285:
	movzbl	(%rdi), %esi
	cmpl	$7, %ecx
	ja	.L2287
	movl	%esi, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %edx
.L2287:
	addq	$1, %rdi
	movq	%rdi, 16(%rbx)
	testb	%sil, %sil
	jns	.L2338
.L2288:
	cmpq	%rdi, %r8
	ja	.L2285
.L2331:
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2338:
	.cfi_restore_state
	subl	$46, %edx
	cmpb	$69, %dl
	ja	.L2331
	movzbl	%dl, %edx
	movslq	(%r15,%rdx,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal17ValueDeserializer11ReadJSErrorEv,"a",@progbits
	.align 4
	.align 4
.L2291:
	.long	.L2299-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2298-.L2291
	.long	.L2297-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2296-.L2291
	.long	.L2295-.L2291
	.long	.L2294-.L2291
	.long	.L2293-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2292-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2331-.L2291
	.long	.L2290-.L2291
	.section	.text._ZN2v88internal17ValueDeserializer11ReadJSErrorEv
	.p2align 4,,10
	.p2align 3
.L2290:
	movq	%rbx, %rdi
	call	_ZN2v88internal17ValueDeserializer10ReadStringEv
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L2284
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2292:
	movq	%rbx, %rdi
	call	_ZN2v88internal17ValueDeserializer10ReadStringEv
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L2284
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2293:
	movq	(%rbx), %rdx
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	1775(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2316
	.p2align 4,,10
	.p2align 3
.L2329:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2294:
	movq	(%rbx), %rdx
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	1767(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L2329
.L2316:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L2339
.L2318:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2295:
	movq	(%rbx), %rdx
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	1759(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L2329
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	jne	.L2318
	.p2align 4,,10
	.p2align 3
.L2339:
	movq	%rdx, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2318
	.p2align 4,,10
	.p2align 3
.L2296:
	movq	(%rbx), %rdx
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	1719(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L2329
	jmp	.L2316
	.p2align 4,,10
	.p2align 3
.L2297:
	movq	(%rbx), %rdx
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	1727(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L2329
	jmp	.L2316
	.p2align 4,,10
	.p2align 3
.L2298:
	movq	(%rbx), %rdx
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	1623(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L2329
	jmp	.L2316
	.p2align 4,,10
	.p2align 3
.L2299:
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	pushq	$2
	xorl	%r9d, %r9d
	movl	$2, %r8d
	movq	%r14, %rsi
	call	_ZN2v88internal10ErrorUtils9ConstructEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEES8_NS0_13FrameSkipModeES8_NS1_20StackTraceCollectionE@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L2331
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	3808(%rdi), %rdx
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L2331
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2281:
	.cfi_restore_state
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L2340
.L2283:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L2282
	.p2align 4,,10
	.p2align 3
.L2340:
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2283
	.cfi_endproc
.LFE18824:
	.size	_ZN2v88internal17ValueDeserializer11ReadJSErrorEv, .-_ZN2v88internal17ValueDeserializer11ReadJSErrorEv
	.section	.text._ZN2v88internal17ValueDeserializer9ReadJSMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer9ReadJSMapEv
	.type	_ZN2v88internal17ValueDeserializer9ReadJSMapEv, @function
_ZN2v88internal17ValueDeserializer9ReadJSMapEv:
.LFB18807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%rbx), %rax
	jnb	.L2342
	movq	(%r15), %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	xorl	%eax, %eax
.L2343:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2393
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2342:
	.cfi_restore_state
	movq	(%r15), %r12
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movl	40(%r15), %r13d
	movq	(%r15), %rdi
	movq	%rax, -120(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -112(%rbp)
	leal	1(%r13), %eax
	movl	%eax, 40(%r15)
	call	_ZN2v88internal7Factory8NewJSMapEv@PLT
	movq	(%r15), %rdi
	movq	48(%r15), %rsi
	movl	%r13d, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	movq	%rax, %rbx
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%r15), %rdi
	movq	%rax, %r13
	cmpq	%rdi, %rax
	je	.L2344
	testq	%rdi, %rdi
	je	.L2345
	testq	%rax, %rax
	je	.L2345
	movq	(%rdi), %rax
	cmpq	%rax, 0(%r13)
	jne	.L2345
.L2344:
	movq	(%r15), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1671(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2346
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L2347:
	xorl	%r14d, %r14d
.L2365:
	movq	16(%r15), %rdi
	movq	24(%r15), %rsi
	movq	%rdi, %rax
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2395:
	movzbl	(%rax), %edx
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L2394
.L2351:
	cmpq	%rax, %rsi
	ja	.L2395
.L2349:
	xorl	%eax, %eax
.L2364:
	movl	41104(%r12), %ebx
	movq	41096(%r12), %rcx
	leal	-1(%rbx), %edx
.L2366:
	movq	-120(%rbp), %rbx
	movl	%edx, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	%rcx, -112(%rbp)
	je	.L2343
	movq	%rax, -104(%rbp)
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, 41096(%r12)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rax
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2345:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%r15), %rax
	movq	0(%r13), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%r15)
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2394:
	cmpb	$58, %dl
	je	.L2355
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	testq	%rax, %rax
	je	.L2349
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	testq	%rax, %rax
	je	.L2349
	leaq	-96(%rbp), %r10
	movq	(%r15), %rsi
	movq	%rax, -72(%rbp)
	movq	%r10, %rdi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC1EPNS0_7IsolateE@PLT
	movq	(%r15), %rdi
	leaq	-80(%rbp), %r8
	movq	%rbx, %rdx
	movl	$2, %ecx
	movq	%r13, %rsi
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	-104(%rbp), %r10
	testq	%rax, %rax
	je	.L2396
	movq	%r10, %rdi
	addl	$2, %r14d
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED1Ev@PLT
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2397:
	movzbl	(%rdi), %eax
	addq	$1, %rdi
	movq	%rdi, 16(%r15)
	testb	%al, %al
	jne	.L2373
.L2355:
	cmpq	%rdi, %rsi
	ja	.L2397
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	24(%r15), %rsi
	movq	16(%r15), %rdi
.L2373:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L2359
	.p2align 4,,10
	.p2align 3
.L2356:
	movzbl	(%rdi), %edx
	cmpl	$31, %ecx
	ja	.L2358
	movl	%edx, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r8d
.L2358:
	addq	$1, %rdi
	movq	%rdi, 16(%r15)
	testb	%dl, %dl
	jns	.L2398
.L2359:
	cmpq	%rsi, %rdi
	jb	.L2356
	movl	41104(%r12), %eax
	movq	41096(%r12), %rcx
	leal	-1(%rax), %edx
.L2357:
	xorl	%eax, %eax
	jmp	.L2366
	.p2align 4,,10
	.p2align 3
.L2346:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L2399
.L2348:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	jmp	.L2347
.L2399:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2348
.L2398:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rcx
	leal	-1(%rax), %edx
	cmpl	%r8d, %r14d
	jne	.L2357
	movq	-120(%rbp), %rax
	movq	(%rbx), %r13
	movl	%edx, 41104(%r12)
	movq	%rax, 41088(%r12)
	movq	-112(%rbp), %rax
	cmpq	%rcx, %rax
	je	.L2367
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2367:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2368
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2396:
	xorl	%eax, %eax
	movq	%r10, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED1Ev@PLT
	movq	-104(%rbp), %rax
	jmp	.L2364
.L2368:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2400
.L2370:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r13, (%rax)
	jmp	.L2343
.L2393:
	call	__stack_chk_fail@PLT
.L2400:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L2370
	.cfi_endproc
.LFE18807:
	.size	_ZN2v88internal17ValueDeserializer9ReadJSMapEv, .-_ZN2v88internal17ValueDeserializer9ReadJSMapEv
	.section	.text._ZN2v88internal17ValueDeserializer9ReadJSSetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer9ReadJSSetEv
	.type	_ZN2v88internal17ValueDeserializer9ReadJSSetEv, @function
_ZN2v88internal17ValueDeserializer9ReadJSSetEv:
.LFB18811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%rbx), %rax
	jnb	.L2402
	movq	(%r15), %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
.L2403:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2443
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2402:
	.cfi_restore_state
	movq	(%r15), %r12
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movl	40(%r15), %r13d
	movq	(%r15), %rdi
	movq	%rax, -104(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -96(%rbp)
	leal	1(%r13), %eax
	movl	%eax, 40(%r15)
	call	_ZN2v88internal7Factory8NewJSSetEv@PLT
	movq	(%r15), %rdi
	movq	48(%r15), %rsi
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	movl	%r13d, %edx
	movq	%rax, %r14
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%r15), %rdi
	movq	%rax, %rbx
	cmpq	%rdi, %rax
	je	.L2404
	testq	%rdi, %rdi
	je	.L2405
	testq	%rax, %rax
	je	.L2405
	movq	(%rdi), %rax
	cmpq	%rax, (%rbx)
	je	.L2404
.L2405:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%r15)
.L2404:
	movq	(%r15), %rbx
	movq	12464(%rbx), %rax
	movq	39(%rax), %rax
	movq	1735(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2406
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -88(%rbp)
.L2407:
	xorl	%ebx, %ebx
	leaq	-80(%rbp), %r13
.L2423:
	movq	16(%r15), %rdi
	movq	24(%r15), %rsi
	movq	%rdi, %rax
	jmp	.L2411
	.p2align 4,,10
	.p2align 3
.L2445:
	movzbl	(%rax), %edx
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L2444
.L2411:
	cmpq	%rax, %rsi
	ja	.L2445
.L2409:
	xorl	%r14d, %r14d
.L2421:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rcx
	subl	$1, %eax
.L2424:
	movq	-104(%rbp), %rbx
	movl	%eax, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	%rcx, -96(%rbp)
	je	.L2403
	movq	-96(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, 41096(%r12)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2403
	.p2align 4,,10
	.p2align 3
.L2406:
	movq	41088(%rbx), %rax
	movq	%rax, -88(%rbp)
	cmpq	41096(%rbx), %rax
	je	.L2446
.L2408:
	movq	-88(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L2407
	.p2align 4,,10
	.p2align 3
.L2444:
	cmpb	$44, %dl
	je	.L2413
	movq	%r15, %rdi
	movq	$0, -64(%rbp)
	call	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	testq	%rax, %rax
	je	.L2409
	movq	(%r15), %rsi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	(%r15), %rdi
	leaq	-64(%rbp), %r8
	movl	$1, %ecx
	movq	%r14, %rdx
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	testq	%rax, %rax
	je	.L2447
	movq	%r13, %rdi
	addl	$1, %ebx
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED1Ev@PLT
	jmp	.L2423
	.p2align 4,,10
	.p2align 3
.L2448:
	movzbl	(%rdi), %eax
	addq	$1, %rdi
	movq	%rdi, 16(%r15)
	testb	%al, %al
	jne	.L2431
.L2413:
	cmpq	%rdi, %rsi
	ja	.L2448
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	24(%r15), %rsi
	movq	16(%r15), %rdi
.L2431:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	jmp	.L2417
	.p2align 4,,10
	.p2align 3
.L2414:
	movzbl	(%rdi), %r8d
	cmpl	$31, %ecx
	ja	.L2416
	movl	%r8d, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %edx
.L2416:
	addq	$1, %rdi
	movq	%rdi, 16(%r15)
	testb	%r8b, %r8b
	jns	.L2449
.L2417:
	cmpq	%rsi, %rdi
	jb	.L2414
	movl	41104(%r12), %eax
	movq	41096(%r12), %rcx
	subl	$1, %eax
.L2415:
	xorl	%r14d, %r14d
	jmp	.L2424
.L2449:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rcx
	subl	$1, %eax
	cmpl	%edx, %ebx
	jne	.L2415
	movq	(%r14), %r13
	movq	-104(%rbp), %rbx
	movl	%eax, 41104(%r12)
	movq	-96(%rbp), %rax
	movq	%rbx, 41088(%r12)
	cmpq	%rax, %rcx
	je	.L2425
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2425:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2426
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
	jmp	.L2403
	.p2align 4,,10
	.p2align 3
.L2446:
	movq	%rbx, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, -88(%rbp)
	jmp	.L2408
.L2447:
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED1Ev@PLT
	jmp	.L2421
.L2426:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L2450
.L2428:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%r14)
	jmp	.L2403
.L2443:
	call	__stack_chk_fail@PLT
.L2450:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L2428
	.cfi_endproc
.LFE18811:
	.size	_ZN2v88internal17ValueDeserializer9ReadJSSetEv, .-_ZN2v88internal17ValueDeserializer9ReadJSSetEv
	.section	.text._ZN2v88internal17ValueDeserializer12ReadJSRegExpEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer12ReadJSRegExpEv
	.type	_ZN2v88internal17ValueDeserializer12ReadJSRegExpEv, @function
_ZN2v88internal17ValueDeserializer12ReadJSRegExpEv:
.LFB18803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	40(%rdi), %r12d
	cmpl	$11, 36(%rdi)
	leal	1(%r12), %eax
	movl	%eax, 40(%rdi)
	ja	.L2452
	call	_ZN2v88internal17ValueDeserializer14ReadUtf8StringEv
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2458
.L2454:
	movq	24(%rbx), %r9
	movq	16(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L2457
	.p2align 4,,10
	.p2align 3
.L2476:
	movzbl	(%rdx), %edi
	cmpl	$31, %ecx
	ja	.L2456
	movl	%edi, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r8d
.L2456:
	addq	$1, %rdx
	movq	%rdx, 16(%rbx)
	testb	%dil, %dil
	jns	.L2475
.L2457:
	cmpq	%rdx, %r9
	ja	.L2476
.L2458:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2475:
	.cfi_restore_state
	testl	$-64, %r8d
	jne	.L2458
	movq	(%rbx), %rdi
	movl	%r8d, %edx
	call	_ZN2v88internal8JSRegExp3NewEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS_4base5FlagsINS1_4FlagEiEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2458
	movq	(%rbx), %rdi
	movq	48(%rbx), %rsi
	movl	%r12d, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	48(%rbx), %rdi
	movq	%rax, %r12
	cmpq	%rdi, %rax
	je	.L2461
	testq	%rdi, %rdi
	je	.L2462
	testq	%rax, %rax
	je	.L2462
	movq	(%rdi), %rax
	cmpq	%rax, (%r12)
	je	.L2461
.L2462:
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	(%rbx), %rax
	movq	(%r12), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, 48(%rbx)
.L2461:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2452:
	.cfi_restore_state
	call	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2458
	movq	(%rax), %rax
	testb	$1, %al
	je	.L2458
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2458
	jmp	.L2454
	.cfi_endproc
.LFE18803:
	.size	_ZN2v88internal17ValueDeserializer12ReadJSRegExpEv, .-_ZN2v88internal17ValueDeserializer12ReadJSRegExpEv
	.section	.rodata._ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC16:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm, @function
_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm:
.LFB22829:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L2516
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	%rcx, %r13
	subq	(%rdi), %r13
	subq	%rcx, %rax
	movq	%r13, %r15
	sarq	$3, %rax
	sarq	$3, %r15
	subq	%r15, %rsi
	cmpq	%rbx, %rax
	jb	.L2479
	cmpq	$1, %rbx
	je	.L2498
	leaq	-2(%rbx), %rdx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	addq	$1, %rdx
	.p2align 4,,10
	.p2align 3
.L2481:
	movq	%rax, %rsi
	addq	$1, %rax
	salq	$4, %rsi
	movups	%xmm0, (%rcx,%rsi)
	cmpq	%rax, %rdx
	ja	.L2481
	leaq	(%rdx,%rdx), %rax
	salq	$4, %rdx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L2482
.L2480:
	movq	$0, (%rdx)
.L2482:
	leaq	(%rcx,%rbx,8), %rax
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2516:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L2479:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rsi
	jb	.L2519
	cmpq	%rbx, %r15
	movq	%rbx, %rsi
	cmovnb	%r15, %rsi
	addq	%r15, %rsi
	cmpq	%rdx, %rsi
	cmova	%rdx, %rsi
	salq	$3, %rsi
	movq	%rsi, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	cmpq	$1, %rbx
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	leaq	(%rax,%r13), %rcx
	je	.L2497
	leaq	-2(%rbx), %rax
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L2488:
	movq	%rdx, %rdi
	addq	$1, %rdx
	salq	$4, %rdi
	movups	%xmm0, (%rcx,%rdi)
	cmpq	%rdx, %rax
	ja	.L2488
	leaq	(%rax,%rax), %rdx
	salq	$4, %rax
	addq	%rax, %rcx
	cmpq	%rdx, %rbx
	je	.L2486
.L2497:
	movq	$0, (%rcx)
.L2486:
	movq	8(%r12), %rcx
	movq	(%r12), %rdi
	cmpq	%rdi, %rcx
	je	.L2496
	leaq	-8(%rcx), %rax
	leaq	15(%rdi), %rdx
	subq	%rdi, %rax
	subq	%r14, %rdx
	shrq	$3, %rax
	cmpq	$30, %rdx
	jbe	.L2500
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rax
	je	.L2500
	addq	$1, %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2494:
	movdqu	(%rdi,%rdx), %xmm1
	movups	%xmm1, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L2494
	movq	%rax, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%rdi,%rdx), %rcx
	addq	%r14, %rdx
	cmpq	%r8, %rax
	je	.L2490
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
.L2490:
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rsi
.L2491:
	addq	%r15, %rbx
	movq	%r14, (%r12)
	leaq	(%r14,%rbx,8), %rax
	addq	%rsi, %r14
	movq	%rax, 8(%r12)
	movq	%r14, 16(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2500:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L2492:
	movq	(%rax), %r8
	addq	$8, %rax
	addq	$8, %rdx
	movq	%r8, -8(%rdx)
	cmpq	%rax, %rcx
	jne	.L2492
.L2496:
	testq	%rdi, %rdi
	je	.L2491
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L2498:
	movq	%rcx, %rdx
	jmp	.L2480
.L2519:
	leaq	.LC16(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22829:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm, .-_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm
	.section	.text._ZN2v88internal17ValueDeserializer42ReadObjectUsingEntireBufferForLegacyFormatEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ValueDeserializer42ReadObjectUsingEntireBufferForLegacyFormatEv
	.type	_ZN2v88internal17ValueDeserializer42ReadObjectUsingEntireBufferForLegacyFormatEv, @function
_ZN2v88internal17ValueDeserializer42ReadObjectUsingEntireBufferForLegacyFormatEv:
.LFB18852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	movaps	%xmm0, -80(%rbp)
	addl	$1, 41104(%r12)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rsi
	cmpq	%rax, %rsi
	jbe	.L2521
	movq	%rax, %rdx
	leaq	-80(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L2522:
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	testb	%cl, %cl
	jne	.L2612
	cmpq	%rdx, %rsi
	ja	.L2522
.L2523:
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	%rsi, 16(%rbx)
	subq	%rdx, %rax
	cmpq	$8, %rax
	je	.L2569
	.p2align 4,,10
	.p2align 3
.L2568:
	movq	(%rbx), %r15
.L2610:
	xorl	%edx, %edx
	movl	$362, %esi
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory8NewErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L2609:
	xorl	%r15d, %r15d
.L2567:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2574
	call	_ZdlPv@PLT
.L2574:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L2575
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2575:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2613
	addq	$104, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2612:
	.cfi_restore_state
	movq	$0, -88(%rbp)
	cmpb	$64, %cl
	je	.L2547
	cmpb	$123, %cl
	je	.L2527
	cmpb	$36, %cl
	je	.L2568
	movq	%rbx, %rdi
	call	_ZN2v88internal17ValueDeserializer10ReadObjectEv
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L2609
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rsi
.L2543:
	cmpq	%rsi, -64(%rbp)
	je	.L2562
.L2620:
	movq	%r8, (%rsi)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rsi
	addq	$8, -72(%rbp)
	movq	%rax, %rdx
	cmpq	%rax, %rsi
	ja	.L2522
	jmp	.L2523
	.p2align 4,,10
	.p2align 3
.L2614:
	movzbl	(%rax), %edx
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%dl, %dl
	jne	.L2576
.L2547:
	cmpq	%rax, %rsi
	ja	.L2614
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	16(%rbx), %rax
	movq	24(%rbx), %rsi
	.p2align 4,,10
	.p2align 3
.L2576:
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	jmp	.L2550
	.p2align 4,,10
	.p2align 3
.L2616:
	movzbl	(%rax), %edi
	cmpl	$31, %ecx
	ja	.L2549
	movl	%edi, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r10d
.L2549:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%dil, %dil
	jns	.L2615
.L2550:
	cmpq	%rax, %rsi
	ja	.L2616
.L2548:
	movq	(%rbx), %rdi
.L2552:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$362, %esi
	movq	%rdi, -104(%rbp)
	xorl	%r15d, %r15d
	call	_ZN2v88internal7Factory8NewErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	-104(%rbp), %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L2567
	.p2align 4,,10
	.p2align 3
.L2617:
	movzbl	(%rax), %edx
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%dl, %dl
	jne	.L2577
.L2527:
	cmpq	%rax, %rsi
	ja	.L2617
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	16(%rbx), %rax
	movq	24(%rbx), %rsi
	.p2align 4,,10
	.p2align 3
.L2577:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L2531
	.p2align 4,,10
	.p2align 3
.L2528:
	movzbl	(%rax), %edi
	cmpl	$31, %ecx
	ja	.L2530
	movl	%edi, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r8d
.L2530:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%dil, %dil
	jns	.L2618
.L2531:
	cmpq	%rax, %rsi
	ja	.L2528
	movq	(%rbx), %r9
.L2529:
	movq	%r9, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$362, %esi
	movq	%r9, -104(%rbp)
	xorl	%r15d, %r15d
	call	_ZN2v88internal7Factory8NewErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	-104(%rbp), %r9
	xorl	%edx, %edx
	movq	(%rax), %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L2567
	.p2align 4,,10
	.p2align 3
.L2618:
	movq	-72(%rbp), %rsi
	subq	-80(%rbp), %rsi
	movl	%r8d, %eax
	sarq	$3, %rsi
	movq	(%rbx), %r9
	movq	%rsi, %rdx
	shrq	%rdx
	cmpq	%rax, %rdx
	jb	.L2529
	addq	%rax, %rax
	movzbl	32(%rbx), %edx
	subq	%rax, %rsi
	movq	12464(%r9), %rax
	movq	%rsi, -104(%rbp)
	movq	39(%rax), %rax
	movq	879(%rax), %r10
	movq	41112(%r9), %rdi
	testq	%rdi, %rdi
	je	.L2534
	movq	%r10, %rsi
	movl	%r8d, -124(%rbp)
	movq	%r9, -120(%rbp)
	movb	%dl, -112(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-112(%rbp), %edx
	movq	-120(%rbp), %r9
	movl	-124(%rbp), %r8d
	movq	%rax, %rsi
.L2535:
	movq	%r9, %rdi
	movl	%r8d, -112(%rbp)
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movl	-112(%rbp), %r8d
	movq	%rax, %r9
	testl	%r8d, %r8d
	je	.L2541
	movq	-104(%rbp), %rdi
	movq	-80(%rbp), %rax
	movl	%r8d, %ecx
	movq	%r9, %rsi
	movq	%r9, -112(%rbp)
	leaq	(%rax,%rdi,8), %rdx
	movq	(%rbx), %rdi
	call	_ZN2v88internalL30SetPropertiesFromKeyValuePairsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPNS3_INS0_6ObjectEEEj
	testb	%al, %al
	je	.L2557
	shrw	$8, %ax
	movq	-112(%rbp), %r9
	je	.L2557
.L2541:
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdx
	movq	%rsi, %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, -104(%rbp)
	ja	.L2619
	jnb	.L2542
	movq	-104(%rbp), %rax
	leaq	(%rdx,%rax,8), %rax
	cmpq	%rax, %rsi
	je	.L2542
	movq	%rax, -72(%rbp)
	movq	%rax, %rsi
.L2542:
	movq	%r9, -88(%rbp)
	movq	%r9, %r8
.L2625:
	cmpq	%rsi, -64(%rbp)
	jne	.L2620
.L2562:
	leaq	-88(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	16(%rbx), %rax
	movq	24(%rbx), %rsi
	movq	%rax, %rdx
	cmpq	%rsi, %rax
	jb	.L2522
	jmp	.L2523
	.p2align 4,,10
	.p2align 3
.L2622:
	movq	-72(%rbp), %rax
	subq	-80(%rbp), %rax
	movl	%r10d, %esi
	movl	%r11d, -120(%rbp)
	sarq	$3, %rax
	movl	%r10d, -112(%rbp)
	movq	(%rbx), %rdi
	shrq	%rax
	movq	%rsi, -104(%rbp)
	cmpq	%rsi, %rax
	jb	.L2552
	movzbl	32(%rbx), %r9d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movl	$3, %esi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movl	-120(%rbp), %r11d
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	movl	%r11d, %esi
	call	_ZN2v88internal7JSArray9SetLengthENS0_6HandleIS1_EEj@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rcx
	movl	-112(%rbp), %r10d
	movq	%rsi, %rax
	movq	-120(%rbp), %r8
	subq	%rdx, %rax
	addq	%rcx, %rcx
	sarq	$3, %rax
	movq	%rax, %r9
	subq	%rcx, %r9
	testl	%r10d, %r10d
	je	.L2556
	movq	(%rbx), %rdi
	leaq	(%rdx,%r9,8), %rdx
	movl	%r10d, %ecx
	movq	%r8, %rsi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internalL30SetPropertiesFromKeyValuePairsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPNS3_INS0_6ObjectEEEj
	testb	%al, %al
	jne	.L2621
.L2557:
	movq	(%rbx), %r15
	movq	12480(%r15), %rax
	cmpq	%rax, 96(%r15)
	jne	.L2609
	jmp	.L2610
	.p2align 4,,10
	.p2align 3
.L2615:
	xorl	%r11d, %r11d
	xorl	%ecx, %ecx
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2551:
	movzbl	(%rax), %edi
	cmpl	$31, %ecx
	ja	.L2553
	movl	%edi, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r11d
.L2553:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	testb	%dil, %dil
	jns	.L2622
.L2554:
	cmpq	%rax, %rsi
	ja	.L2551
	jmp	.L2548
.L2621:
	shrw	$8, %ax
	je	.L2557
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdx
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r8
	movq	%rsi, %rax
	subq	%rdx, %rax
	sarq	$3, %rax
.L2556:
	cmpq	%rax, %r9
	ja	.L2623
	jnb	.L2559
	leaq	(%rdx,%r9,8), %rax
	cmpq	%rsi, %rax
	je	.L2559
	movq	%rax, -72(%rbp)
	movq	%rax, %rsi
.L2559:
	movq	%r8, -88(%rbp)
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2534:
	movq	41088(%r9), %rsi
	cmpq	41096(%r9), %rsi
	je	.L2624
.L2536:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r9)
	movq	%r10, (%rsi)
	jmp	.L2535
.L2619:
	movq	-104(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r9, -112(%rbp)
	subq	%rax, %rsi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm
	movq	-112(%rbp), %r9
	movq	-72(%rbp), %rsi
	movq	%r9, -88(%rbp)
	movq	%r9, %r8
	jmp	.L2625
.L2623:
	movq	%r9, %rsi
	movq	%r15, %rdi
	movq	%r8, -104(%rbp)
	subq	%rax, %rsi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm
	movq	-104(%rbp), %r8
	movq	-72(%rbp), %rsi
	movq	%r8, -88(%rbp)
	jmp	.L2543
.L2569:
	movq	(%rdx), %rax
	movq	(%rax), %rbx
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L2570
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2570:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2571
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2572:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	jmp	.L2567
.L2624:
	movq	%r9, %rdi
	movq	%r10, -136(%rbp)
	movl	%r8d, -124(%rbp)
	movb	%dl, -120(%rbp)
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %r10
	movl	-124(%rbp), %r8d
	movzbl	-120(%rbp), %edx
	movq	-112(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L2536
.L2521:
	movq	%rsi, 16(%rdi)
	jmp	.L2568
.L2571:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L2626
.L2573:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rbx, (%r15)
	jmp	.L2572
.L2626:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r15
	jmp	.L2573
.L2613:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18852:
	.size	_ZN2v88internal17ValueDeserializer42ReadObjectUsingEntireBufferForLegacyFormatEv, .-_ZN2v88internal17ValueDeserializer42ReadObjectUsingEntireBufferForLegacyFormatEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE, @function
_GLOBAL__sub_I__ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE:
.LFB24057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24057:
	.size	_GLOBAL__sub_I__ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE, .-_GLOBAL__sub_I__ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15ValueSerializerC2EPNS0_7IsolateEPNS_15ValueSerializer8DelegateE
	.section	.rodata.CSWTCH.2527,"a"
	.align 8
	.type	CSWTCH.2527, @object
	.size	CSWTCH.2527, 10
CSWTCH.2527:
	.byte	66
	.byte	119
	.byte	87
	.byte	100
	.byte	68
	.byte	102
	.byte	70
	.byte	67
	.byte	113
	.byte	81
	.weak	_ZTVN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEEE
	.section	.data.rel.ro.local._ZTVN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEEE,"awG",@progbits,_ZTVN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEEE,comdat
	.align 8
	.type	_ZTVN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEEE, @object
	.size	_ZTVN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEEE, 48
_ZTVN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED1Ev
	.quad	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEED0Ev
	.quad	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm
	.quad	_ZN2v88internal11IdentityMapIjNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	-1
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC5:
	.long	0
	.long	2146959360
	.align 8
.LC9:
	.long	4292870144
	.long	1106247679
	.align 8
.LC10:
	.long	0
	.long	1138753536
	.align 8
.LC13:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
