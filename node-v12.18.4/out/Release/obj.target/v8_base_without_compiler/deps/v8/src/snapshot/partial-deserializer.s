	.file	"partial-deserializer.cc"
	.text
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB7245:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7245:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal19PartialDeserializerD2Ev,"axG",@progbits,_ZN2v88internal19PartialDeserializerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19PartialDeserializerD2Ev
	.type	_ZN2v88internal19PartialDeserializerD2Ev, @function
_ZN2v88internal19PartialDeserializerD2Ev:
.LFB26332:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal19PartialDeserializerE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal12DeserializerD2Ev@PLT
	.cfi_endproc
.LFE26332:
	.size	_ZN2v88internal19PartialDeserializerD2Ev, .-_ZN2v88internal19PartialDeserializerD2Ev
	.weak	_ZN2v88internal19PartialDeserializerD1Ev
	.set	_ZN2v88internal19PartialDeserializerD1Ev,_ZN2v88internal19PartialDeserializerD2Ev
	.section	.text._ZN2v88internal19PartialDeserializerD0Ev,"axG",@progbits,_ZN2v88internal19PartialDeserializerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19PartialDeserializerD0Ev
	.type	_ZN2v88internal19PartialDeserializerD0Ev, @function
_ZN2v88internal19PartialDeserializerD0Ev:
.LFB26334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal19PartialDeserializerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal12DeserializerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$624, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26334:
	.size	_ZN2v88internal19PartialDeserializerD0Ev, .-_ZN2v88internal19PartialDeserializerD0Ev
	.section	.text._ZN2v88internal19PartialDeserializer25DeserializeEmbedderFieldsENS_33DeserializeInternalFieldsCallbackE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PartialDeserializer25DeserializeEmbedderFieldsENS_33DeserializeInternalFieldsCallbackE
	.type	_ZN2v88internal19PartialDeserializer25DeserializeEmbedderFieldsENS_33DeserializeInternalFieldsCallbackE, @function
_ZN2v88internal19PartialDeserializer25DeserializeEmbedderFieldsENS_33DeserializeInternalFieldsCallbackE:
.LFB20220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	124(%rdi), %rax
	cmpl	%eax, 120(%rdi)
	jle	.L6
	movq	112(%rdi), %rdx
	leal	1(%rax), %ecx
	movq	%rdi, %rbx
	movl	%ecx, 124(%rdi)
	cmpb	$29, (%rdx,%rax)
	je	.L31
.L6:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L32
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	leaq	-80(%rbp), %rax
	movq	80(%rdi), %rsi
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	movslq	124(%rbx), %rax
	movq	112(%rbx), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 124(%rbx)
	movzbl	(%rdx,%rax), %eax
	cmpl	$26, %eax
	je	.L33
	movq	80(%rbx), %r15
	andl	$7, %eax
	movl	%eax, %esi
	movq	41088(%r15), %rdi
	addl	$1, 41104(%r15)
	movq	80(%rbx), %r12
	movq	%rdi, -88(%rbp)
	movq	41096(%r15), %rdi
	movq	%rdi, -96(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal12Deserializer23GetBackReferencedObjectENS0_13SnapshotSpaceE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L11
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L12:
	movq	112(%rbx), %r8
	movslq	124(%rbx), %rcx
	movl	$32, %r10d
	movq	%r9, -120(%rbp)
	movzbl	1(%r8,%rcx), %edx
	movzbl	2(%r8,%rcx), %edi
	movq	%rcx, %rax
	movzbl	3(%r8,%rcx), %r12d
	movzbl	(%r8,%rcx), %esi
	movl	%r10d, %ecx
	sall	$16, %edi
	sall	$8, %edx
	orl	%edi, %edx
	sall	$24, %r12d
	movl	$-1, %edi
	orl	%esi, %edx
	movl	%r12d, %esi
	movl	%edi, %r12d
	orl	%edx, %esi
	andl	$3, %edx
	addl	$1, %edx
	addl	%edx, %eax
	sall	$3, %edx
	subl	%edx, %ecx
	movl	%eax, 124(%rbx)
	shrl	%cl, %r12d
	movslq	%eax, %rcx
	movzbl	1(%r8,%rcx), %edx
	andl	%esi, %r12d
	movzbl	2(%r8,%rcx), %esi
	movzbl	(%r8,%rcx), %r11d
	movzbl	3(%r8,%rcx), %r8d
	movl	%r10d, %ecx
	movl	%r12d, -104(%rbp)
	sall	$16, %esi
	sall	$8, %edx
	orl	%esi, %edx
	sall	$24, %r8d
	orl	%r11d, %edx
	orl	%edx, %r8d
	andl	$3, %edx
	addl	$1, %edx
	addl	%edx, %eax
	sall	$3, %edx
	subl	%edx, %ecx
	movl	%eax, 124(%rbx)
	shrl	%cl, %edi
	movl	%edi, %ecx
	andl	%r8d, %ecx
	shrl	$2, %ecx
	movl	%ecx, %r13d
	movl	%ecx, -112(%rbp)
	movq	%r13, %rdi
	call	_Znam@PLT
	movslq	124(%rbx), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%rax, %r12
	movl	%esi, -108(%rbp)
	addq	112(%rbx), %rsi
	call	memcpy@PLT
	movl	-108(%rbp), %r8d
	movl	-112(%rbp), %ecx
	movq	%r12, %rdx
	movabsq	$-4294967296, %rax
	movq	-120(%rbp), %r9
	movl	-104(%rbp), %esi
	andq	%rax, %r14
	addl	%r8d, %ecx
	movq	-128(%rbp), %rax
	movq	-136(%rbp), %r8
	orq	%r13, %r14
	movq	%r9, %rdi
	movl	%ecx, 124(%rbx)
	shrl	$2, %esi
	movq	%r14, %rcx
	call	*%rax
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	movq	-88(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-96(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L29
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L11:
	movq	41088(%r12), %r9
	cmpq	%r9, 41096(%r12)
	je	.L34
.L13:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r9)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L33:
	movq	-144(%rbp), %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L13
.L32:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20220:
	.size	_ZN2v88internal19PartialDeserializer25DeserializeEmbedderFieldsENS_33DeserializeInternalFieldsCallbackE, .-_ZN2v88internal19PartialDeserializer25DeserializeEmbedderFieldsENS_33DeserializeInternalFieldsCallbackE
	.section	.rodata._ZN2v88internal19PartialDeserializer11DeserializeEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"PartialDeserializer"
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.rodata._ZN2v88internal19PartialDeserializer11DeserializeEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"start_address == code_space->top()"
	.section	.rodata._ZN2v88internal19PartialDeserializer11DeserializeEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE.str1.1
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal19PartialDeserializer11DeserializeEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PartialDeserializer11DeserializeEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE
	.type	_ZN2v88internal19PartialDeserializer11DeserializeEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE, @function
_ZN2v88internal19PartialDeserializer11DeserializeEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE:
.LFB20219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	leaq	328(%r13), %r15
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal21DeserializerAllocator12ReserveSpaceEv@PLT
	testb	%al, %al
	je	.L70
	movq	96(%r13), %rdx
	cmpq	104(%r13), %rdx
	je	.L37
	movq	%rbx, (%rdx)
	addq	$8, 96(%r13)
.L38:
	leaq	-64(%rbp), %rcx
	leaq	-56(%rbp), %r8
	xorl	%edx, %edx
	movl	$19, %esi
	movq	37856(%r12), %rbx
	movq	%r13, %rdi
	movq	$0, -64(%rbp)
	movq	104(%rbx), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal12Deserializer26DeserializeDeferredObjectsEv@PLT
	movq	-72(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal19PartialDeserializer25DeserializeEmbedderFieldsENS_33DeserializeInternalFieldsCallbackE
	movq	%r15, %rdi
	call	_ZN2v88internal21DeserializerAllocator45RegisterDeserializedObjectsForBlackAllocationEv@PLT
	movq	-80(%rbp), %rax
	cmpq	%rax, 104(%rbx)
	jne	.L71
	cmpb	$0, _ZN2v88internal20FLAG_rehash_snapshotE(%rip)
	je	.L49
	cmpb	$0, 593(%r13)
	jne	.L72
.L49:
	movq	%r13, %rdi
	call	_ZN2v88internal12Deserializer15LogNewMapEventsEv@PLT
	movq	41112(%r12), %rdi
	movq	-64(%rbp), %rsi
	testq	%rdi, %rdi
	je	.L50
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L51:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L73
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	je	.L74
.L52:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%r13, %rdi
	call	_ZN2v88internal12Deserializer6RehashEv@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L37:
	movq	88(%r13), %r8
	movq	%rdx, %rcx
	movabsq	$1152921504606846975, %rdi
	subq	%r8, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L75
	testq	%rax, %rax
	je	.L54
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %r10
	cmpq	%r10, %rax
	jbe	.L76
.L40:
	movq	%rsi, %rdi
	movq	%rcx, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %rcx
	leaq	8(%rax), %r10
	addq	%rax, %rsi
.L41:
	movq	%rbx, (%rax,%rcx)
	cmpq	%r8, %rdx
	je	.L42
	leaq	-8(%rdx), %rdi
	leaq	15(%rax), %rcx
	subq	%r8, %rdi
	subq	%r8, %rcx
	movq	%rdi, %r10
	shrq	$3, %r10
	cmpq	$30, %rcx
	jbe	.L57
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %r10
	je	.L57
	addq	$1, %r10
	xorl	%edx, %edx
	movq	%r10, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L44:
	movdqu	(%r8,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L44
	movq	%r10, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %rcx
	leaq	(%r8,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%r11, %r10
	je	.L46
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L46:
	leaq	16(%rax,%rdi), %r10
.L42:
	testq	%r8, %r8
	je	.L47
	movq	%r8, %rdi
	movq	%rsi, -96(%rbp)
	movq	%r10, -88(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %rax
.L47:
	movq	%rax, %xmm0
	movq	%r10, %xmm2
	movq	%rsi, 104(%r13)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 88(%r13)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L76:
	testq	%r10, %r10
	jne	.L77
	movl	$8, %r10d
	xorl	%esi, %esi
	xorl	%eax, %eax
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$8, %esi
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%rax, %r10
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rcx), %r11
	addq	$8, %rcx
	addq	$8, %r10
	movq	%r11, -8(%r10)
	cmpq	%rcx, %rdx
	jne	.L43
	jmp	.L46
.L73:
	call	__stack_chk_fail@PLT
.L70:
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
.L75:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L77:
	cmpq	%rdi, %r10
	movq	%rdi, %rsi
	cmovbe	%r10, %rsi
	salq	$3, %rsi
	jmp	.L40
	.cfi_endproc
.LFE20219:
	.size	_ZN2v88internal19PartialDeserializer11DeserializeEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE, .-_ZN2v88internal19PartialDeserializer11DeserializeEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE
	.section	.text._ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.type	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, @function
_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_:
.LFB25498:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L92
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L88
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L93
.L80:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L87:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L94
	testq	%r13, %r13
	jg	.L83
	testq	%r9, %r9
	jne	.L86
.L84:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L83
.L86:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L93:
	testq	%rsi, %rsi
	jne	.L81
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L84
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$8, %r14d
	jmp	.L80
.L92:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L81:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L80
	.cfi_endproc
.LFE25498:
	.size	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, .-_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.section	.text._ZN2v88internal19PartialDeserializer18DeserializeContextEPNS0_7IsolateEPKNS0_12SnapshotDataEbNS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PartialDeserializer18DeserializeContextEPNS0_7IsolateEPKNS0_12SnapshotDataEbNS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE
	.type	_ZN2v88internal19PartialDeserializer18DeserializeContextEPNS0_7IsolateEPKNS0_12SnapshotDataEbNS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE, @function
_ZN2v88internal19PartialDeserializer18DeserializeContextEPNS0_7IsolateEPKNS0_12SnapshotDataEbNS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE:
.LFB20215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-720(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$696, %rsp
	movq	%r9, -728(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal12DeserializerE(%rip), %rax
	movups	%xmm0, -680(%rbp)
	movq	%rax, -688(%rbp)
	movq	(%rsi), %rax
	movups	%xmm0, -664(%rbp)
	movups	%xmm0, -648(%rbp)
	movups	%xmm0, -632(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rsi, -736(%rbp)
	movaps	%xmm0, -608(%rbp)
	movaps	%xmm0, -592(%rbp)
	movl	$0, -616(%rbp)
	call	*16(%rax)
	movq	-736(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	%rax, -576(%rbp)
	movq	8(%rsi), %rax
	movl	%edx, -568(%rbp)
	movl	$0, -564(%rbp)
	movl	(%rax), %eax
	movups	%xmm0, -552(%rbp)
	movups	%xmm0, -536(%rbp)
	movl	%eax, -560(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -520(%rbp)
	movups	%xmm0, -504(%rbp)
	movups	%xmm0, -488(%rbp)
	movups	%xmm0, -472(%rbp)
	movups	%xmm0, -456(%rbp)
	movups	%xmm0, -440(%rbp)
	movups	%xmm0, -424(%rbp)
	movups	%xmm0, -408(%rbp)
	movups	%xmm0, -392(%rbp)
	movups	%xmm0, -376(%rbp)
	movups	%xmm0, -360(%rbp)
	movups	%xmm0, -344(%rbp)
	movups	%xmm0, -328(%rbp)
	movups	%xmm0, -312(%rbp)
	movups	%xmm0, -296(%rbp)
	movups	%xmm0, -280(%rbp)
	movups	%xmm0, -264(%rbp)
	movups	%xmm0, -248(%rbp)
	movups	%xmm0, -232(%rbp)
	movups	%xmm0, -152(%rbp)
	movl	$0, -168(%rbp)
	movb	$0, -164(%rbp)
	movl	$0, -160(%rbp)
	movups	%xmm0, -136(%rbp)
	movw	%ax, -96(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -88(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNK2v88internal12SnapshotData12ReservationsEv@PLT
	leaq	-360(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal21DeserializerAllocator17DecodeReservationERKSt6vectorINS0_14SerializedData11ReservationESaIS4_EE@PLT
	movq	-720(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L96
	call	_ZdlPv@PLT
.L96:
	movq	$0, -720(%rbp)
	movq	-376(%rbp), %rsi
	cmpq	-368(%rbp), %rsi
	je	.L97
	movq	$0, (%rsi)
	addq	$8, -376(%rbp)
.L98:
	movb	%bl, -95(%rbp)
	movq	-728(%rbp), %r8
	movq	%r12, %rsi
	movq	%r15, %rcx
	leaq	-688(%rbp), %rbx
	leaq	16+_ZTVN2v88internal19PartialDeserializerE(%rip), %r14
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r14, -688(%rbp)
	call	_ZN2v88internal19PartialDeserializer11DeserializeEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE
	movq	%rbx, %rdi
	movq	%r14, -688(%rbp)
	movq	%rax, %r12
	call	_ZN2v88internal12DeserializerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$696, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	leaq	-384(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	jmp	.L98
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20215:
	.size	_ZN2v88internal19PartialDeserializer18DeserializeContextEPNS0_7IsolateEPKNS0_12SnapshotDataEbNS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE, .-_ZN2v88internal19PartialDeserializer18DeserializeContextEPNS0_7IsolateEPKNS0_12SnapshotDataEbNS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19PartialDeserializer18DeserializeContextEPNS0_7IsolateEPKNS0_12SnapshotDataEbNS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19PartialDeserializer18DeserializeContextEPNS0_7IsolateEPKNS0_12SnapshotDataEbNS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE, @function
_GLOBAL__sub_I__ZN2v88internal19PartialDeserializer18DeserializeContextEPNS0_7IsolateEPKNS0_12SnapshotDataEbNS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE:
.LFB26368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26368:
	.size	_GLOBAL__sub_I__ZN2v88internal19PartialDeserializer18DeserializeContextEPNS0_7IsolateEPKNS0_12SnapshotDataEbNS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE, .-_GLOBAL__sub_I__ZN2v88internal19PartialDeserializer18DeserializeContextEPNS0_7IsolateEPKNS0_12SnapshotDataEbNS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19PartialDeserializer18DeserializeContextEPNS0_7IsolateEPKNS0_12SnapshotDataEbNS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE
	.weak	_ZTVN2v88internal19PartialDeserializerE
	.section	.data.rel.ro._ZTVN2v88internal19PartialDeserializerE,"awG",@progbits,_ZTVN2v88internal19PartialDeserializerE,comdat
	.align 8
	.type	_ZTVN2v88internal19PartialDeserializerE, @object
	.size	_ZTVN2v88internal19PartialDeserializerE, 56
_ZTVN2v88internal19PartialDeserializerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19PartialDeserializerD1Ev
	.quad	_ZN2v88internal19PartialDeserializerD0Ev
	.quad	_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal12Deserializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
