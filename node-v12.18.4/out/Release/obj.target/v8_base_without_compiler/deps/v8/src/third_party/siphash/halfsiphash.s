	.file	"halfsiphash.cc"
	.text
	.section	.text._Z11halfsiphashjm,"ax",@progbits
	.p2align 4
	.globl	_Z11halfsiphashjm
	.type	_Z11halfsiphashjm, @function
_Z11halfsiphashjm:
.LFB2758:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	movl	%edi, %r8d
	movl	%esi, %ecx
	shrq	$32, %rax
	xorl	$1819895653, %ecx
	xorl	%eax, %r8d
	addl	%eax, %esi
	movl	%eax, %edx
	xorl	$1952801890, %r8d
	roll	$5, %edx
	movl	%r8d, %eax
	addl	%r8d, %ecx
	xorl	%esi, %edx
	roll	$16, %esi
	roll	$8, %eax
	xorl	%ecx, %eax
	addl	%edx, %ecx
	roll	$13, %edx
	addl	%eax, %esi
	roll	$7, %eax
	xorl	%ecx, %edx
	xorl	%esi, %eax
	roll	$16, %ecx
	addl	%edx, %esi
	addl	%eax, %ecx
	roll	$5, %edx
	roll	$8, %eax
	xorl	%esi, %edx
	roll	$16, %esi
	xorl	%ecx, %eax
	addl	%edx, %ecx
	roll	$13, %edx
	addl	%eax, %esi
	roll	$7, %eax
	xorl	%ecx, %edx
	xorl	%esi, %eax
	roll	$16, %ecx
	xorl	%esi, %edi
	leal	(%rdx,%rdi), %esi
	xorl	$67108864, %eax
	roll	$5, %edx
	xorl	%esi, %edx
	addl	%eax, %ecx
	roll	$8, %eax
	xorl	%ecx, %eax
	roll	$16, %esi
	addl	%edx, %ecx
	roll	$13, %edx
	addl	%eax, %esi
	roll	$7, %eax
	xorl	%ecx, %edx
	xorl	%esi, %eax
	roll	$16, %ecx
	addl	%edx, %esi
	roll	$5, %edx
	addl	%eax, %ecx
	xorl	%esi, %edx
	roll	$8, %eax
	xorl	%ecx, %eax
	roll	$16, %esi
	addl	%edx, %ecx
	roll	$13, %edx
	addl	%eax, %esi
	roll	$7, %eax
	xorl	%ecx, %edx
	roll	$16, %ecx
	xorl	%esi, %eax
	xorl	$67108864, %esi
	xorb	$-1, %cl
	addl	%edx, %esi
	roll	$5, %edx
	addl	%eax, %ecx
	roll	$8, %eax
	xorl	%esi, %edx
	xorl	%ecx, %eax
	roll	$16, %esi
	addl	%edx, %ecx
	addl	%eax, %esi
	roll	$7, %eax
	xorl	%esi, %eax
	roll	$13, %edx
	xorl	%ecx, %edx
	movl	%eax, %edi
	roll	$16, %ecx
	addl	%edi, %ecx
	roll	$8, %edi
	leal	(%rsi,%rdx), %eax
	roll	$5, %edx
	movl	%edi, %esi
	xorl	%eax, %edx
	xorl	%ecx, %esi
	roll	$16, %eax
	addl	%esi, %eax
	addl	%edx, %ecx
	roll	$7, %esi
	roll	$13, %edx
	xorl	%eax, %esi
	xorl	%ecx, %edx
	roll	$16, %ecx
	addl	%edx, %eax
	addl	%esi, %ecx
	roll	$5, %edx
	roll	$8, %esi
	xorl	%eax, %edx
	roll	$16, %eax
	xorl	%ecx, %esi
	addl	%edx, %ecx
	roll	$13, %edx
	addl	%esi, %eax
	roll	$7, %esi
	xorl	%ecx, %edx
	xorl	%eax, %esi
	roll	$16, %ecx
	addl	%edx, %eax
	addl	%esi, %ecx
	roll	$5, %edx
	roll	$8, %esi
	xorl	%eax, %edx
	roll	$16, %eax
	xorl	%ecx, %esi
	addl	%edx, %ecx
	roll	$13, %edx
	addl	%esi, %eax
	roll	$7, %esi
	xorl	%esi, %eax
	xorl	%eax, %ecx
	movl	%ecx, %eax
	xorl	%edx, %eax
	ret
	.cfi_endproc
.LFE2758:
	.size	_Z11halfsiphashjm, .-_Z11halfsiphashjm
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
