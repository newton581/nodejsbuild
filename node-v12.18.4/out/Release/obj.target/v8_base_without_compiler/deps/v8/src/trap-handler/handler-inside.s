	.file	"handler-inside.cc"
	.text
	.section	.text._ZN2v88internal12trap_handler17TryFindLandingPadEmPm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12trap_handler17TryFindLandingPadEmPm
	.type	_ZN2v88internal12trap_handler17TryFindLandingPadEmPm, @function
_ZN2v88internal12trap_handler17TryFindLandingPadEmPm:
.LFB3447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-41(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12trap_handler12MetadataLockC1Ev@PLT
	movq	_ZN2v88internal12trap_handler15gNumCodeObjectsE(%rip), %r8
	testq	%r8, %r8
	je	.L8
	movq	_ZN2v88internal12trap_handler12gCodeObjectsE(%rip), %r10
	xorl	%edx, %edx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$1, %rdx
	cmpq	%rdx, %r8
	je	.L8
.L6:
	movq	%rdx, %rax
	salq	$4, %rax
	movq	(%r10,%rax), %rax
	testq	%rax, %rax
	je	.L3
	movq	(%rax), %rdi
	cmpq	%r12, %rdi
	ja	.L3
	movq	8(%rax), %rcx
	addq	%rdi, %rcx
	cmpq	%r12, %rcx
	jbe	.L3
	movq	16(%rax), %r9
	movq	%r12, %r11
	subq	%rdi, %r11
	testq	%r9, %r9
	je	.L3
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L4:
	leal	1(%rsi), %ecx
	movq	%rcx, %rsi
	cmpq	%r9, %rcx
	jnb	.L3
.L5:
	leaq	2(%rcx), %r14
	movl	24(%rax,%rcx,8), %ecx
	cmpq	%r11, %rcx
	jne	.L4
	movl	12(%rax,%r14,8), %eax
	movl	$1, %r12d
	addq	%rax, %rdi
	movq	%rdi, (%rbx)
	movq	_ZN2v88internal12trap_handler19gRecoveredTrapCountE(%rip), %rax
	addq	$1, %rax
	movq	%rax, _ZN2v88internal12trap_handler19gRecoveredTrapCountE(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%r12d, %r12d
.L2:
	movq	%r13, %rdi
	call	_ZN2v88internal12trap_handler12MetadataLockD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L19
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3447:
	.size	_ZN2v88internal12trap_handler17TryFindLandingPadEmPm, .-_ZN2v88internal12trap_handler17TryFindLandingPadEmPm
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
