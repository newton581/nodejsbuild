	.file	"regexp-compiler.cc"
	.text
	.section	.text._ZN2v88internal10RegExpTree13IsTextElementEv,"axG",@progbits,_ZN2v88internal10RegExpTree13IsTextElementEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpTree13IsTextElementEv
	.type	_ZN2v88internal10RegExpTree13IsTextElementEv, @function
_ZN2v88internal10RegExpTree13IsTextElementEv:
.LFB8049:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8049:
	.size	_ZN2v88internal10RegExpTree13IsTextElementEv, .-_ZN2v88internal10RegExpTree13IsTextElementEv
	.section	.text._ZN2v88internal10RegExpTree17IsAnchoredAtStartEv,"axG",@progbits,_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv
	.type	_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv, @function
_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv:
.LFB8050:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8050:
	.size	_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv, .-_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv
	.section	.text._ZN2v88internal10RegExpTree15IsAnchoredAtEndEv,"axG",@progbits,_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv
	.type	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv, @function
_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv:
.LFB8051:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8051:
	.size	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv, .-_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv
	.section	.text._ZN2v88internal10RegExpTree16CaptureRegistersEv,"axG",@progbits,_ZN2v88internal10RegExpTree16CaptureRegistersEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpTree16CaptureRegistersEv
	.type	_ZN2v88internal10RegExpTree16CaptureRegistersEv, @function
_ZN2v88internal10RegExpTree16CaptureRegistersEv:
.LFB8052:
	.cfi_startproc
	endbr64
	movabsq	$-4294967297, %rax
	ret
	.cfi_endproc
.LFE8052:
	.size	_ZN2v88internal10RegExpTree16CaptureRegistersEv, .-_ZN2v88internal10RegExpTree16CaptureRegistersEv
	.section	.text._ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv,"axG",@progbits,_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv
	.type	_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv, @function
_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv:
.LFB8199:
	.cfi_startproc
	endbr64
	movl	$-2147483648, %eax
	ret
	.cfi_endproc
.LFE8199:
	.size	_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv, .-_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv
	.section	.text._ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE,"axG",@progbits,_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.type	_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE, @function
_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE:
.LFB8200:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8200:
	.size	_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE, .-_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.section	.text._ZN2v88internal10RegExpNode13FilterOneByteEi,"axG",@progbits,_ZN2v88internal10RegExpNode13FilterOneByteEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpNode13FilterOneByteEi
	.type	_ZN2v88internal10RegExpNode13FilterOneByteEi, @function
_ZN2v88internal10RegExpNode13FilterOneByteEi:
.LFB8202:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8202:
	.size	_ZN2v88internal10RegExpNode13FilterOneByteEi, .-_ZN2v88internal10RegExpNode13FilterOneByteEi
	.section	.text._ZN2v88internal13SeqRegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,"axG",@progbits,_ZN2v88internal13SeqRegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13SeqRegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.type	_ZN2v88internal13SeqRegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, @function
_ZN2v88internal13SeqRegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb:
.LFB8220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	56(%rdi), %rdi
	movzbl	%r9b, %ebx
	movl	%ebx, %r9d
	movq	(%rdi), %rax
	call	*72(%rax)
	testl	%r12d, %r12d
	jne	.L9
	movq	%r14, 32(%r13,%rbx,8)
.L9:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8220:
	.size	_ZN2v88internal13SeqRegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, .-_ZN2v88internal13SeqRegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.section	.text._ZN2v88internal10ActionNode20GreedyLoopTextLengthEv,"axG",@progbits,_ZN2v88internal10ActionNode20GreedyLoopTextLengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ActionNode20GreedyLoopTextLengthEv
	.type	_ZN2v88internal10ActionNode20GreedyLoopTextLengthEv, @function
_ZN2v88internal10ActionNode20GreedyLoopTextLengthEv:
.LFB8222:
	.cfi_startproc
	endbr64
	movl	$-2147483648, %eax
	ret
	.cfi_endproc
.LFE8222:
	.size	_ZN2v88internal10ActionNode20GreedyLoopTextLengthEv, .-_ZN2v88internal10ActionNode20GreedyLoopTextLengthEv
	.section	.text._ZN2v88internal17BackReferenceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib,"axG",@progbits,_ZN2v88internal17BackReferenceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17BackReferenceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.type	_ZN2v88internal17BackReferenceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, @function
_ZN2v88internal17BackReferenceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib:
.LFB8253:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8253:
	.size	_ZN2v88internal17BackReferenceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, .-_ZN2v88internal17BackReferenceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.section	.text._ZN2v88internal7EndNodeD2Ev,"axG",@progbits,_ZN2v88internal7EndNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7EndNodeD2Ev
	.type	_ZN2v88internal7EndNodeD2Ev, @function
_ZN2v88internal7EndNodeD2Ev:
.LFB8261:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8261:
	.size	_ZN2v88internal7EndNodeD2Ev, .-_ZN2v88internal7EndNodeD2Ev
	.weak	_ZN2v88internal7EndNodeD1Ev
	.set	_ZN2v88internal7EndNodeD1Ev,_ZN2v88internal7EndNodeD2Ev
	.section	.text._ZN2v88internal10ChoiceNode39try_to_emit_quick_check_for_alternativeEb,"axG",@progbits,_ZN2v88internal10ChoiceNode39try_to_emit_quick_check_for_alternativeEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ChoiceNode39try_to_emit_quick_check_for_alternativeEb
	.type	_ZN2v88internal10ChoiceNode39try_to_emit_quick_check_for_alternativeEb, @function
_ZN2v88internal10ChoiceNode39try_to_emit_quick_check_for_alternativeEb:
.LFB8287:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE8287:
	.size	_ZN2v88internal10ChoiceNode39try_to_emit_quick_check_for_alternativeEb, .-_ZN2v88internal10ChoiceNode39try_to_emit_quick_check_for_alternativeEb
	.section	.text._ZN2v88internal10ChoiceNode13read_backwardEv,"axG",@progbits,_ZN2v88internal10ChoiceNode13read_backwardEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ChoiceNode13read_backwardEv
	.type	_ZN2v88internal10ChoiceNode13read_backwardEv, @function
_ZN2v88internal10ChoiceNode13read_backwardEv:
.LFB8288:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8288:
	.size	_ZN2v88internal10ChoiceNode13read_backwardEv, .-_ZN2v88internal10ChoiceNode13read_backwardEv
	.section	.text._ZN2v88internal10ChoiceNodeD2Ev,"axG",@progbits,_ZN2v88internal10ChoiceNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ChoiceNodeD2Ev
	.type	_ZN2v88internal10ChoiceNodeD2Ev, @function
_ZN2v88internal10ChoiceNodeD2Ev:
.LFB8291:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8291:
	.size	_ZN2v88internal10ChoiceNodeD2Ev, .-_ZN2v88internal10ChoiceNodeD2Ev
	.weak	_ZN2v88internal10ChoiceNodeD1Ev
	.set	_ZN2v88internal10ChoiceNodeD1Ev,_ZN2v88internal10ChoiceNodeD2Ev
	.section	.text._ZN2v88internal28NegativeLookaroundChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,"axG",@progbits,_ZN2v88internal28NegativeLookaroundChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal28NegativeLookaroundChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.type	_ZN2v88internal28NegativeLookaroundChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, @function
_ZN2v88internal28NegativeLookaroundChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb:
.LFB8296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	56(%rdi), %rax
	movzbl	%r9b, %ebx
	movl	%ebx, %r9d
	movq	(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	call	*72(%rax)
	testl	%r12d, %r12d
	jne	.L18
	movq	%r14, 32(%r13,%rbx,8)
.L18:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8296:
	.size	_ZN2v88internal28NegativeLookaroundChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, .-_ZN2v88internal28NegativeLookaroundChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.section	.text._ZN2v88internal28NegativeLookaroundChoiceNode39try_to_emit_quick_check_for_alternativeEb,"axG",@progbits,_ZN2v88internal28NegativeLookaroundChoiceNode39try_to_emit_quick_check_for_alternativeEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal28NegativeLookaroundChoiceNode39try_to_emit_quick_check_for_alternativeEb
	.type	_ZN2v88internal28NegativeLookaroundChoiceNode39try_to_emit_quick_check_for_alternativeEb, @function
_ZN2v88internal28NegativeLookaroundChoiceNode39try_to_emit_quick_check_for_alternativeEb:
.LFB8299:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE8299:
	.size	_ZN2v88internal28NegativeLookaroundChoiceNode39try_to_emit_quick_check_for_alternativeEb, .-_ZN2v88internal28NegativeLookaroundChoiceNode39try_to_emit_quick_check_for_alternativeEb
	.section	.text._ZN2v88internal14LoopChoiceNode13read_backwardEv,"axG",@progbits,_ZN2v88internal14LoopChoiceNode13read_backwardEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LoopChoiceNode13read_backwardEv
	.type	_ZN2v88internal14LoopChoiceNode13read_backwardEv, @function
_ZN2v88internal14LoopChoiceNode13read_backwardEv:
.LFB8307:
	.cfi_startproc
	endbr64
	movzbl	89(%rdi), %eax
	ret
	.cfi_endproc
.LFE8307:
	.size	_ZN2v88internal14LoopChoiceNode13read_backwardEv, .-_ZN2v88internal14LoopChoiceNode13read_backwardEv
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv,"axG",@progbits,_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv, @function
_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv:
.LFB19961:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE19961:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv, .-_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv,"axG",@progbits,_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv, @function
_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv:
.LFB19962:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.cfi_endproc
.LFE19962:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv, .-_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv
	.section	.text._ZN2v88internal23NegativeSubmatchSuccess4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23NegativeSubmatchSuccess4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.type	_ZN2v88internal23NegativeSubmatchSuccess4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, @function
_ZN2v88internal23NegativeSubmatchSuccess4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE:
.LFB20362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	40(%rsi), %r12
	movq	%rdi, %rbx
	movl	16(%rdi), %edx
	movq	(%r12), %rax
	testl	%edx, %edx
	js	.L26
	leaq	16(%rdi), %rsi
	movq	%r12, %rdi
	call	*64(%rax)
	movq	(%r12), %rax
.L26:
	movl	64(%rbx), %esi
	movq	%r12, %rdi
	call	*312(%rax)
	movq	(%r12), %rax
	movl	60(%rbx), %esi
	movq	%r12, %rdi
	call	*320(%rax)
	movl	68(%rbx), %eax
	testl	%eax, %eax
	jle	.L27
	movl	72(%rbx), %esi
	movq	%r12, %rdi
	leal	-1(%rax,%rsi), %edx
	movq	(%r12), %rax
	call	*360(%rax)
.L27:
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	56(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE20362:
	.size	_ZN2v88internal23NegativeSubmatchSuccess4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, .-_ZN2v88internal23NegativeSubmatchSuccess4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.section	.text._ZN2v88internal13AssertionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13AssertionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.type	_ZN2v88internal13AssertionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, @function
_ZN2v88internal13AssertionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb:
.LFB20401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpl	$1, 64(%rdi)
	je	.L37
	movq	56(%rdi), %rdi
	movzbl	%r9b, %ebx
	subl	$1, %ecx
	movl	%ebx, %r9d
	movq	(%rdi), %rax
	call	*72(%rax)
	testl	%r13d, %r13d
	jne	.L29
	movslq	%ebx, %r9
	movq	%r14, 32(%r12,%r9,8)
.L29:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	testb	%r9b, %r9b
	jne	.L29
	movq	56(%rdi), %rdi
	subl	$1, %ecx
	xorl	%r9d, %r9d
	movq	(%rdi), %rax
	call	*72(%rax)
	testl	%r13d, %r13d
	jne	.L29
	xorl	%ebx, %ebx
	movslq	%ebx, %r9
	movq	%r14, 32(%r12,%r9,8)
	jmp	.L29
	.cfi_endproc
.LFE20401:
	.size	_ZN2v88internal13AssertionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, .-_ZN2v88internal13AssertionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.section	.text._ZN2v88internal28NegativeLookaroundChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28NegativeLookaroundChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.type	_ZN2v88internal28NegativeLookaroundChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, @function
_ZN2v88internal28NegativeLookaroundChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib:
.LFB20402:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	movzbl	%r8b, %r8d
	movq	(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE20402:
	.size	_ZN2v88internal28NegativeLookaroundChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, .-_ZN2v88internal28NegativeLookaroundChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.section	.text._ZN2v88internal13AssertionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13AssertionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.type	_ZN2v88internal13AssertionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, @function
_ZN2v88internal13AssertionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib:
.LFB20449:
	.cfi_startproc
	endbr64
	cmpl	$1, 64(%rdi)
	jne	.L40
	testb	%r8b, %r8b
	jne	.L46
.L40:
	movq	56(%rdi), %rdi
	movzbl	%r8b, %r8d
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L46:
	movb	$1, 36(%rsi)
	ret
	.cfi_endproc
.LFE20449:
	.size	_ZN2v88internal13AssertionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, .-_ZN2v88internal13AssertionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.section	.text._ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED2Ev, @function
_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED2Ev:
.LFB24718:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24718:
	.size	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED2Ev, .-_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED2Ev
	.set	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED1Ev,_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED2Ev
	.section	.text._ZN2v88internal14LoopChoiceNodeD2Ev,"axG",@progbits,_ZN2v88internal14LoopChoiceNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LoopChoiceNodeD2Ev
	.type	_ZN2v88internal14LoopChoiceNodeD2Ev, @function
_ZN2v88internal14LoopChoiceNodeD2Ev:
.LFB24722:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24722:
	.size	_ZN2v88internal14LoopChoiceNodeD2Ev, .-_ZN2v88internal14LoopChoiceNodeD2Ev
	.weak	_ZN2v88internal14LoopChoiceNodeD1Ev
	.set	_ZN2v88internal14LoopChoiceNodeD1Ev,_ZN2v88internal14LoopChoiceNodeD2Ev
	.section	.text._ZN2v88internal28NegativeLookaroundChoiceNodeD2Ev,"axG",@progbits,_ZN2v88internal28NegativeLookaroundChoiceNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal28NegativeLookaroundChoiceNodeD2Ev
	.type	_ZN2v88internal28NegativeLookaroundChoiceNodeD2Ev, @function
_ZN2v88internal28NegativeLookaroundChoiceNodeD2Ev:
.LFB24726:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24726:
	.size	_ZN2v88internal28NegativeLookaroundChoiceNodeD2Ev, .-_ZN2v88internal28NegativeLookaroundChoiceNodeD2Ev
	.weak	_ZN2v88internal28NegativeLookaroundChoiceNodeD1Ev
	.set	_ZN2v88internal28NegativeLookaroundChoiceNodeD1Ev,_ZN2v88internal28NegativeLookaroundChoiceNodeD2Ev
	.section	.text._ZN2v88internal8TextNodeD2Ev,"axG",@progbits,_ZN2v88internal8TextNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8TextNodeD2Ev
	.type	_ZN2v88internal8TextNodeD2Ev, @function
_ZN2v88internal8TextNodeD2Ev:
.LFB24730:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24730:
	.size	_ZN2v88internal8TextNodeD2Ev, .-_ZN2v88internal8TextNodeD2Ev
	.weak	_ZN2v88internal8TextNodeD1Ev
	.set	_ZN2v88internal8TextNodeD1Ev,_ZN2v88internal8TextNodeD2Ev
	.section	.text._ZN2v88internal13AssertionNodeD2Ev,"axG",@progbits,_ZN2v88internal13AssertionNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13AssertionNodeD2Ev
	.type	_ZN2v88internal13AssertionNodeD2Ev, @function
_ZN2v88internal13AssertionNodeD2Ev:
.LFB24734:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24734:
	.size	_ZN2v88internal13AssertionNodeD2Ev, .-_ZN2v88internal13AssertionNodeD2Ev
	.weak	_ZN2v88internal13AssertionNodeD1Ev
	.set	_ZN2v88internal13AssertionNodeD1Ev,_ZN2v88internal13AssertionNodeD2Ev
	.section	.text._ZN2v88internal17BackReferenceNodeD2Ev,"axG",@progbits,_ZN2v88internal17BackReferenceNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17BackReferenceNodeD2Ev
	.type	_ZN2v88internal17BackReferenceNodeD2Ev, @function
_ZN2v88internal17BackReferenceNodeD2Ev:
.LFB24738:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24738:
	.size	_ZN2v88internal17BackReferenceNodeD2Ev, .-_ZN2v88internal17BackReferenceNodeD2Ev
	.weak	_ZN2v88internal17BackReferenceNodeD1Ev
	.set	_ZN2v88internal17BackReferenceNodeD1Ev,_ZN2v88internal17BackReferenceNodeD2Ev
	.section	.text._ZN2v88internal10ActionNodeD2Ev,"axG",@progbits,_ZN2v88internal10ActionNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ActionNodeD2Ev
	.type	_ZN2v88internal10ActionNodeD2Ev, @function
_ZN2v88internal10ActionNodeD2Ev:
.LFB24742:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24742:
	.size	_ZN2v88internal10ActionNodeD2Ev, .-_ZN2v88internal10ActionNodeD2Ev
	.weak	_ZN2v88internal10ActionNodeD1Ev
	.set	_ZN2v88internal10ActionNodeD1Ev,_ZN2v88internal10ActionNodeD2Ev
	.section	.text._ZN2v88internal23NegativeSubmatchSuccessD2Ev,"axG",@progbits,_ZN2v88internal23NegativeSubmatchSuccessD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23NegativeSubmatchSuccessD2Ev
	.type	_ZN2v88internal23NegativeSubmatchSuccessD2Ev, @function
_ZN2v88internal23NegativeSubmatchSuccessD2Ev:
.LFB24746:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24746:
	.size	_ZN2v88internal23NegativeSubmatchSuccessD2Ev, .-_ZN2v88internal23NegativeSubmatchSuccessD2Ev
	.weak	_ZN2v88internal23NegativeSubmatchSuccessD1Ev
	.set	_ZN2v88internal23NegativeSubmatchSuccessD1Ev,_ZN2v88internal23NegativeSubmatchSuccessD2Ev
	.section	.text._ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE8VisitEndEPNS0_7EndNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE8VisitEndEPNS0_7EndNodeE, @function
_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE8VisitEndEPNS0_7EndNodeE:
.LFB24788:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24788:
	.size	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE8VisitEndEPNS0_7EndNodeE, .-_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE8VisitEndEPNS0_7EndNodeE
	.section	.rodata._ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.type	_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE, @function
_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE:
.LFB20324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20324:
	.size	_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE, .-_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.section	.text._ZN2v88internal7EndNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,"axG",@progbits,_ZN2v88internal7EndNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7EndNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.type	_ZN2v88internal7EndNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, @function
_ZN2v88internal7EndNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb:
.LFB8258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8258:
	.size	_ZN2v88internal7EndNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, .-_ZN2v88internal7EndNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.section	.text._ZN2v88internal10RegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,"axG",@progbits,_ZN2v88internal10RegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.type	_ZN2v88internal10RegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, @function
_ZN2v88internal10RegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb:
.LFB8201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8201:
	.size	_ZN2v88internal10RegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, .-_ZN2v88internal10RegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.section	.text._ZN2v88internal7EndNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib,"axG",@progbits,_ZN2v88internal7EndNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7EndNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.type	_ZN2v88internal7EndNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, @function
_ZN2v88internal7EndNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib:
.LFB8257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8257:
	.size	_ZN2v88internal7EndNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, .-_ZN2v88internal7EndNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.section	.text._ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv
	.type	_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv, @function
_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv:
.LFB20406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20406:
	.size	_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv, .-_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv
	.section	.text._ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.type	_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, @function
_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib:
.LFB20407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20407:
	.size	_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, .-_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.section	.text._ZN2v88internalL19EmitSimpleCharacterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL19EmitSimpleCharacterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb, @function
_ZN2v88internalL19EmitSimpleCharacterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb:
.LFB20382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	cmpb	$0, 16(%rbp)
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	movq	40(%rsi), %r13
	je	.L73
.L69:
	movq	0(%r13), %rax
	movzwl	%bx, %esi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	*144(%rax)
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movl	%r8d, %esi
	movzbl	%r9b, %ecx
	movl	$1, %r8d
	movq	%r12, %rdx
	movl	$-1, %r9d
	movq	%r13, %rdi
	movl	$1, %r14d
	call	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii@PLT
	jmp	.L69
	.cfi_endproc
.LFE20382:
	.size	_ZN2v88internalL19EmitSimpleCharacterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb, .-_ZN2v88internalL19EmitSimpleCharacterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb
	.section	.text._ZN2v88internal8TextNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.type	_ZN2v88internal8TextNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE, @function
_ZN2v88internal8TextNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE:
.LFB20463:
	.cfi_startproc
	endbr64
	cmpb	$0, 72(%rdi)
	jne	.L85
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	64(%rdi), %rax
	movq	%rdi, %rbx
	cmpl	$1, 12(%rax)
	jne	.L77
	movq	(%rax), %rax
	cmpl	$1, 4(%rax)
	jne	.L77
	movq	8(%rax), %r14
	movq	%rsi, %r12
	movq	48(%rdi), %rsi
	leaq	8(%r14), %rdi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE@PLT
	testb	$1, 28(%r14)
	jne	.L86
	cmpl	$1, 12(%r13)
	jne	.L77
	movq	0(%r13), %rdx
	cmpb	$1, 48(%r12)
	sbbl	%eax, %eax
	movl	(%rdx), %ecx
	andl	$65280, %eax
	addl	$255, %eax
	testl	%ecx, %ecx
	jne	.L77
	cmpl	%eax, 4(%rdx)
	jge	.L80
.L77:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movl	12(%r13), %esi
	testl	%esi, %esi
	jne	.L77
.L80:
	movq	56(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE20463:
	.size	_ZN2v88internal8TextNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE, .-_ZN2v88internal8TextNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.section	.text._ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED0Ev, @function
_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED0Ev:
.LFB24720:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24720:
	.size	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED0Ev, .-_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED0Ev
	.section	.text._ZN2v88internalL22EmitDoubleBoundaryTestEPNS0_20RegExpMacroAssemblerEiiPNS0_5LabelES4_S4_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22EmitDoubleBoundaryTestEPNS0_20RegExpMacroAssemblerEiiPNS0_5LabelES4_S4_, @function
_ZN2v88internalL22EmitDoubleBoundaryTestEPNS0_20RegExpMacroAssemblerEiiPNS0_5LabelES4_S4_:
.LFB20387:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	cmpq	%rcx, %r8
	je	.L95
	movq	%rcx, %rbx
	cmpl	%edx, %esi
	je	.L96
	movzwl	%dx, %edx
	movzwl	%si, %esi
	movq	%r8, %rcx
	call	*168(%rax)
	cmpq	%r13, %rbx
	je	.L88
.L97:
	movq	(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	224(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	%r8, %rdx
	call	*72(%rax)
	cmpq	%r13, %rbx
	jne	.L97
.L88:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	cmpl	%edx, %esi
	je	.L98
	movq	176(%rax), %rax
	movzwl	%dx, %edx
	movzwl	%si, %esi
	movq	%r9, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	144(%rax), %rax
	addq	$8, %rsp
	movq	%r9, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE20387:
	.size	_ZN2v88internalL22EmitDoubleBoundaryTestEPNS0_20RegExpMacroAssemblerEiiPNS0_5LabelES4_S4_, .-_ZN2v88internalL22EmitDoubleBoundaryTestEPNS0_20RegExpMacroAssemblerEiiPNS0_5LabelES4_S4_
	.section	.text._ZN2v88internal12_GLOBAL__N_113EmitWordCheckEPNS0_20RegExpMacroAssemblerEPNS0_5LabelES5_b.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_113EmitWordCheckEPNS0_20RegExpMacroAssemblerEPNS0_5LabelES5_b.part.0, @function
_ZN2v88internal12_GLOBAL__N_113EmitWordCheckEPNS0_20RegExpMacroAssemblerEPNS0_5LabelES5_b.part.0:
.LFB24896:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$122, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movl	%ecx, %ebx
	call	*88(%rax)
	movq	(%r12), %rax
	movq	%r14, %rdx
	movl	$48, %esi
	movq	%r12, %rdi
	call	*96(%rax)
	movq	(%r12), %rax
	movq	%r13, %rdx
	movl	$96, %esi
	movq	%r12, %rdi
	call	*88(%rax)
	movq	(%r12), %rax
	movq	%r13, %rdx
	movl	$58, %esi
	movq	%r12, %rdi
	call	*96(%rax)
	movq	(%r12), %rax
	movq	%r14, %rdx
	movl	$65, %esi
	movq	%r12, %rdi
	call	*96(%rax)
	movq	(%r12), %rax
	movq	%r13, %rdx
	movl	$91, %esi
	movq	%r12, %rdi
	call	*96(%rax)
	movq	(%r12), %rax
	testb	%bl, %bl
	je	.L100
	movq	144(%rax), %rax
	movq	%r14, %rdx
.L102:
	popq	%rbx
	movq	%r12, %rdi
	movl	$95, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	72(%rax), %rax
	movq	%r13, %rdx
	jmp	.L102
	.cfi_endproc
.LFE24896:
	.size	_ZN2v88internal12_GLOBAL__N_113EmitWordCheckEPNS0_20RegExpMacroAssemblerEPNS0_5LabelES5_b.part.0, .-_ZN2v88internal12_GLOBAL__N_113EmitWordCheckEPNS0_20RegExpMacroAssemblerEPNS0_5LabelES5_b.part.0
	.section	.text._ZN2v88internal10ZoneObjectdlEPvm.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal10ZoneObjectdlEPvm.isra.0, @function
_ZN2v88internal10ZoneObjectdlEPvm.isra.0:
.LFB24909:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24909:
	.size	_ZN2v88internal10ZoneObjectdlEPvm.isra.0, .-_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.section	.text._ZN2v88internal10ChoiceNodeD0Ev,"axG",@progbits,_ZN2v88internal10ChoiceNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ChoiceNodeD0Ev
	.type	_ZN2v88internal10ChoiceNodeD0Ev, @function
_ZN2v88internal10ChoiceNodeD0Ev:
.LFB8293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE8293:
	.size	_ZN2v88internal10ChoiceNodeD0Ev, .-_ZN2v88internal10ChoiceNodeD0Ev
	.section	.text._ZN2v88internal14LoopChoiceNodeD0Ev,"axG",@progbits,_ZN2v88internal14LoopChoiceNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LoopChoiceNodeD0Ev
	.type	_ZN2v88internal14LoopChoiceNodeD0Ev, @function
_ZN2v88internal14LoopChoiceNodeD0Ev:
.LFB24724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE24724:
	.size	_ZN2v88internal14LoopChoiceNodeD0Ev, .-_ZN2v88internal14LoopChoiceNodeD0Ev
	.section	.text._ZN2v88internal28NegativeLookaroundChoiceNodeD0Ev,"axG",@progbits,_ZN2v88internal28NegativeLookaroundChoiceNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal28NegativeLookaroundChoiceNodeD0Ev
	.type	_ZN2v88internal28NegativeLookaroundChoiceNodeD0Ev, @function
_ZN2v88internal28NegativeLookaroundChoiceNodeD0Ev:
.LFB24728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE24728:
	.size	_ZN2v88internal28NegativeLookaroundChoiceNodeD0Ev, .-_ZN2v88internal28NegativeLookaroundChoiceNodeD0Ev
	.section	.text._ZN2v88internal8TextNodeD0Ev,"axG",@progbits,_ZN2v88internal8TextNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8TextNodeD0Ev
	.type	_ZN2v88internal8TextNodeD0Ev, @function
_ZN2v88internal8TextNodeD0Ev:
.LFB24732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE24732:
	.size	_ZN2v88internal8TextNodeD0Ev, .-_ZN2v88internal8TextNodeD0Ev
	.section	.text._ZN2v88internal13AssertionNodeD0Ev,"axG",@progbits,_ZN2v88internal13AssertionNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13AssertionNodeD0Ev
	.type	_ZN2v88internal13AssertionNodeD0Ev, @function
_ZN2v88internal13AssertionNodeD0Ev:
.LFB24736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE24736:
	.size	_ZN2v88internal13AssertionNodeD0Ev, .-_ZN2v88internal13AssertionNodeD0Ev
	.section	.text._ZN2v88internal17BackReferenceNodeD0Ev,"axG",@progbits,_ZN2v88internal17BackReferenceNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17BackReferenceNodeD0Ev
	.type	_ZN2v88internal17BackReferenceNodeD0Ev, @function
_ZN2v88internal17BackReferenceNodeD0Ev:
.LFB24740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE24740:
	.size	_ZN2v88internal17BackReferenceNodeD0Ev, .-_ZN2v88internal17BackReferenceNodeD0Ev
	.section	.text._ZN2v88internal10ActionNodeD0Ev,"axG",@progbits,_ZN2v88internal10ActionNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ActionNodeD0Ev
	.type	_ZN2v88internal10ActionNodeD0Ev, @function
_ZN2v88internal10ActionNodeD0Ev:
.LFB24744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE24744:
	.size	_ZN2v88internal10ActionNodeD0Ev, .-_ZN2v88internal10ActionNodeD0Ev
	.section	.text._ZN2v88internal7EndNodeD0Ev,"axG",@progbits,_ZN2v88internal7EndNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7EndNodeD0Ev
	.type	_ZN2v88internal7EndNodeD0Ev, @function
_ZN2v88internal7EndNodeD0Ev:
.LFB8263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE8263:
	.size	_ZN2v88internal7EndNodeD0Ev, .-_ZN2v88internal7EndNodeD0Ev
	.section	.text._ZN2v88internal23NegativeSubmatchSuccessD0Ev,"axG",@progbits,_ZN2v88internal23NegativeSubmatchSuccessD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23NegativeSubmatchSuccessD0Ev
	.type	_ZN2v88internal23NegativeSubmatchSuccessD0Ev, @function
_ZN2v88internal23NegativeSubmatchSuccessD0Ev:
.LFB24748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE24748:
	.size	_ZN2v88internal23NegativeSubmatchSuccessD0Ev, .-_ZN2v88internal23NegativeSubmatchSuccessD0Ev
	.section	.text._ZN2v88internal13SeqRegExpNode13FilterOneByteEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13SeqRegExpNode13FilterOneByteEi
	.type	_ZN2v88internal13SeqRegExpNode13FilterOneByteEi, @function
_ZN2v88internal13SeqRegExpNode13FilterOneByteEi:
.LFB20432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	25(%rdi), %eax
	testb	%al, %al
	jns	.L124
	movq	8(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	testl	%esi, %esi
	js	.L132
	orl	$64, %eax
	subl	$2, %esi
	movb	%al, 25(%rdi)
	movq	56(%rdi), %rdi
	movq	(%rdi), %rax
	call	*80(%rax)
	testq	%rax, %rax
	je	.L127
	movq	%rax, 56(%rbx)
	movq	%rbx, %rax
.L127:
	movzbl	25(%rbx), %edx
	movq	%rax, 8(%rbx)
	andl	$63, %edx
	orl	$-128, %edx
	movb	%dl, 25(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rdi, %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20432:
	.size	_ZN2v88internal13SeqRegExpNode13FilterOneByteEi, .-_ZN2v88internal13SeqRegExpNode13FilterOneByteEi
	.section	.text._ZN2v88internal28NegativeLookaroundChoiceNode13FilterOneByteEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28NegativeLookaroundChoiceNode13FilterOneByteEi
	.type	_ZN2v88internal28NegativeLookaroundChoiceNode13FilterOneByteEi, @function
_ZN2v88internal28NegativeLookaroundChoiceNode13FilterOneByteEi:
.LFB20440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	25(%rdi), %eax
	testb	%al, %al
	jns	.L134
	movq	8(%rdi), %r13
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	testl	%esi, %esi
	js	.L137
	testb	$64, %al
	jne	.L137
	orl	$64, %eax
	leal	-1(%rsi), %r12d
	movb	%al, 25(%rdi)
	movq	56(%rdi), %rax
	movl	%r12d, %esi
	movq	(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L142
	movq	56(%rbx), %rax
	movl	%r12d, %esi
	movq	(%rax), %rax
	movq	%r13, 16(%rax)
	movq	56(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*80(%rax)
	testq	%rax, %rax
	je	.L143
	movq	56(%rbx), %rdx
	movq	%rbx, %r13
	movq	(%rdx), %rdx
	movq	%rax, (%rdx)
	orb	$-128, 25(%rbx)
	movq	%rbx, 8(%rbx)
.L139:
	andb	$-65, 25(%rbx)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rbx, %r13
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	orb	$-128, 25(%rbx)
	movq	$0, 8(%rbx)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L143:
	orb	$-128, 25(%rbx)
	movq	%r13, 8(%rbx)
	jmp	.L139
	.cfi_endproc
.LFE20440:
	.size	_ZN2v88internal28NegativeLookaroundChoiceNode13FilterOneByteEi, .-_ZN2v88internal28NegativeLookaroundChoiceNode13FilterOneByteEi
	.section	.text._ZN2v88internal12_GLOBAL__N_117BitsetFirstSetBitESt6bitsetILm128EE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117BitsetFirstSetBitESt6bitsetILm128EE, @function
_ZN2v88internal12_GLOBAL__N_117BitsetFirstSetBitESt6bitsetILm128EE:
.LFB20482:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L145
	xorl	%eax, %eax
	rep bsfq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	testq	%rsi, %rsi
	je	.L147
	rep bsfq	%rsi, %rsi
	leal	64(%rsi), %eax
	ret
.L147:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE20482:
	.size	_ZN2v88internal12_GLOBAL__N_117BitsetFirstSetBitESt6bitsetILm128EE, .-_ZN2v88internal12_GLOBAL__N_117BitsetFirstSetBitESt6bitsetILm128EE
	.section	.text._ZN2v88internalL16GenerateBranchesEPNS0_20RegExpMacroAssemblerEPNS0_8ZoneListIiEEiiiiPNS0_5LabelES7_S7_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL16GenerateBranchesEPNS0_20RegExpMacroAssemblerEPNS0_8ZoneListIiEEiiiiPNS0_5LabelES7_S7_, @function
_ZN2v88internalL16GenerateBranchesEPNS0_20RegExpMacroAssemblerEPNS0_8ZoneListIiEEiiiiPNS0_5LabelES7_S7_:
.LFB20391:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movq	16(%rbp), %r15
	movl	%r8d, -240(%rbp)
	movl	%r9d, -232(%rbp)
	movq	(%rsi), %r8
	leaq	0(,%rdx,4), %r9
	movq	%rax, -216(%rbp)
	movq	32(%rbp), %rax
	leaq	(%r8,%r9), %rsi
	movq	%rax, -224(%rbp)
	movl	(%rsi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	%ecx, %rax
	movl	(%r8,%rax,4), %eax
	leal	-1(%rax), %edi
	movl	%edi, -248(%rbp)
	cmpl	%ecx, %edx
	je	.L227
	leal	1(%rdx), %r11d
	movq	%rdx, %rbx
	movl	%ecx, %r12d
	cmpl	%ecx, %r11d
	je	.L228
	subl	%edx, %ecx
	cmpl	$6, %ecx
	jle	.L229
	movl	-240(%rbp), %edi
	movl	-232(%rbp), %r9d
	sarl	$7, %edi
	sarl	$7, %r9d
	cmpl	%edi, %r9d
	je	.L230
	movl	%r14d, %esi
	movl	%r11d, -256(%rbp)
	movq	(%r10), %r9
	sarl	$7, %esi
	cmpl	%esi, %edi
	je	.L181
	movq	-224(%rbp), %rbx
	movq	%r10, %rdi
	movq	%r10, -224(%rbp)
	movzwl	%r14w, %esi
	movq	%rbx, %rdx
	call	*96(%r9)
	subq	$8, %rsp
	movl	-256(%rbp), %r11d
	pushq	-216(%rbp)
	movq	-224(%rbp), %r10
	pushq	%rbx
	movl	%r14d, %r8d
	movl	%r12d, %ecx
	pushq	%r15
	movl	-232(%rbp), %r9d
	movl	%r11d, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internalL16GenerateBranchesEPNS0_20RegExpMacroAssemblerEPNS0_8ZoneListIiEEiiiiPNS0_5LabelES7_S7_
	addq	$32, %rsp
.L148:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L231
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movq	%rsi, %rcx
	movl	%edx, %r14d
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%rcx, %rdi
	leal	1(%r14), %eax
	addq	$4, %rcx
	movl	4(%rdi), %edx
	subl	$1, %edx
	cmpl	%edx, (%rdi)
	je	.L232
	movl	%eax, %r14d
.L156:
	cmpl	%r12d, %r14d
	jl	.L233
.L155:
	movq	$0, -200(%rbp)
	movl	4(%r8,%r9), %edx
	movq	%r10, %rdi
	leaq	-200(%rbp), %rcx
	movq	-216(%rbp), %r8
	movl	(%rsi), %esi
	movq	%rcx, %r9
	movl	%r11d, -256(%rbp)
	subl	$1, %edx
	movq	%r10, -248(%rbp)
	call	_ZN2v88internalL22EmitDoubleBoundaryTestEPNS0_20RegExpMacroAssemblerEiiPNS0_5LabelES4_S4_
	movl	-256(%rbp), %r11d
	movq	-248(%rbp), %r10
	movl	%r11d, %eax
	.p2align 4,,10
	.p2align 3
.L160:
	cmpl	%eax, %r12d
	jle	.L234
	leal	-1(%r12), %ecx
	movslq	%eax, %rsi
	movl	%ecx, %ebx
	leaq	4(,%rsi,4), %rdx
	subl	%eax, %ebx
	leaq	2(%rsi,%rbx), %rdi
	salq	$2, %rdi
	.p2align 4,,10
	.p2align 3
.L163:
	movq	0(%r13), %rax
	movl	(%rax,%rdx), %esi
	movl	%esi, -4(%rax,%rdx)
	addq	$4, %rdx
	cmpq	%rdx, %rdi
	jne	.L163
.L162:
	subq	$8, %rsp
	pushq	-224(%rbp)
	movl	%r11d, %edx
	movq	%r13, %rsi
	movl	-232(%rbp), %r9d
	pushq	-216(%rbp)
	movq	%r10, %rdi
	movl	-240(%rbp), %r8d
	pushq	%r15
	call	_ZN2v88internalL16GenerateBranchesEPNS0_20RegExpMacroAssemblerEPNS0_8ZoneListIiEEiiiiPNS0_5LabelES7_S7_
	addq	$32, %rsp
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L230:
	cmpq	%r15, -216(%rbp)
	je	.L195
	xorl	%eax, %eax
	movl	$1, %r13d
.L165:
	andl	$127, %r14d
	je	.L235
	leal	-1(%r14), %ecx
	leaq	-192(%rbp), %rdi
	movsbl	%al, %r9d
	leaq	1(%rcx), %r14
	movq	%rdi, -232(%rbp)
	movq	%rdi, %r11
	cmpl	$8, %r14d
	jb	.L169
	movabsq	$72340172838076673, %rcx
	movzbl	%al, %eax
	imulq	%rcx, %rax
	movl	%r14d, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rdi, %r11
.L169:
	movl	%r14d, %ecx
	andl	$7, %ecx
	je	.L172
	xorl	%eax, %eax
.L170:
	movl	%eax, %edi
	addl	$1, %eax
	movb	%r9b, (%r11,%rdi)
	cmpl	%ecx, %eax
	jb	.L170
.L172:
	cmpl	%r12d, %ebx
	jge	.L236
	subl	$1, %r12d
	movl	%r13d, %r14d
	movq	%r10, -240(%rbp)
	movq	%rsi, %r13
	subl	%ebx, %r12d
	movq	%r15, -248(%rbp)
	addq	%r12, %rdx
	movq	-232(%rbp), %r12
	leaq	4(%r8,%rdx,4), %rbx
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L238:
	leal	-1(%r15), %edx
	movl	%r14d, %esi
	subl	%edi, %edx
	movslq	%edi, %rdi
	addq	$1, %rdx
	addq	%r12, %rdi
	call	memset@PLT
.L173:
	addq	$4, %r13
	xorl	$1, %r14d
	cmpq	%r13, %rbx
	je	.L237
.L174:
	movl	0(%r13), %edi
	movl	4(%r13), %r15d
	andl	$127, %edi
	andl	$127, %r15d
	cmpl	%edi, %r15d
	jg	.L238
	movl	%edi, %r15d
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L232:
	movl	%eax, -260(%rbp)
	cmpl	$-1, %r14d
	je	.L155
	movl	%r14d, %edx
	movq	-216(%rbp), %r8
	movq	$0, -200(%rbp)
	leaq	-200(%rbp), %r9
	subl	%ebx, %edx
	movl	(%rdi), %esi
	movq	%r10, %rdi
	movl	%r11d, -256(%rbp)
	andl	$1, %edx
	movl	(%rcx), %edx
	movq	%r9, %rcx
	cmovne	-224(%rbp), %r8
	movq	%r10, -248(%rbp)
	subl	$1, %edx
	call	_ZN2v88internalL22EmitDoubleBoundaryTestEPNS0_20RegExpMacroAssemblerEiiPNS0_5LabelES4_S4_
	cmpl	%ebx, %r14d
	movq	-248(%rbp), %r10
	movl	-256(%rbp), %r11d
	movl	-260(%rbp), %eax
	jle	.L160
	notl	%ebx
	movslq	%r14d, %rsi
	leal	(%rbx,%r14), %edx
	leaq	-4(,%rsi,4), %rcx
	subq	%rdx, %rsi
	leaq	-8(,%rsi,4), %rdi
	.p2align 4,,10
	.p2align 3
.L161:
	movq	0(%r13), %rdx
	movl	(%rdx,%rcx), %esi
	movl	%esi, 4(%rdx,%rcx)
	subq	$4, %rcx
	cmpq	%rcx, %rdi
	jne	.L161
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L181:
	movl	%r14d, %esi
	andl	$-128, %esi
	subl	$-128, %esi
	cmpl	%r12d, %edx
	jl	.L184
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L239:
	leal	1(%rdx), %r15d
	addq	$1, %rdx
	cmpl	%edx, %r12d
	jle	.L183
.L184:
	movl	%edx, %r15d
	cmpl	(%r8,%rdx,4), %esi
	jge	.L239
.L183:
	cmpl	$256, %esi
	jle	.L185
	movl	%r15d, %edx
	subl	%ebx, %edx
	addl	%edx, %edx
	cmpl	%edx, %ecx
	jg	.L191
.L185:
	leal	-1(%r15), %ecx
	movslq	%ecx, %rdx
	cmpl	%esi, (%r8,%rdx,4)
	jne	.L187
	leal	-2(%r15), %ecx
.L187:
	cmpl	%esi, %eax
	jg	.L188
	movl	%ebx, %edx
	leal	-1(%r12), %ecx
	movl	%r12d, %r15d
	movq	$0, -208(%rbp)
	xorl	%r12d, %edx
	andl	$1, %edx
	je	.L240
	movq	-224(%rbp), %rdx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L227:
	movq	-224(%rbp), %rdx
	movq	(%r10), %rax
	cmpq	%r15, %rdx
	je	.L150
	movzwl	%r14w, %esi
	movq	%r10, -224(%rbp)
	movq	%r10, %rdi
	call	*96(%rax)
	movq	-216(%rbp), %rsi
	cmpq	%r15, %rsi
	je	.L148
	movq	-224(%rbp), %r10
	movq	(%r10), %rax
	movq	%r10, %rdi
	call	*224(%rax)
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L228:
	movq	-224(%rbp), %r9
	movl	%edi, %edx
	movq	%r15, %rcx
	movl	%r14d, %esi
	movq	-216(%rbp), %r8
	movq	%r10, %rdi
	call	_ZN2v88internalL22EmitDoubleBoundaryTestEPNS0_20RegExpMacroAssemblerEiiPNS0_5LabelES4_S4_
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L188:
	movq	$0, -208(%rbp)
	leal	-1(%rsi), %eax
	leaq	-208(%rbp), %rdx
	movl	%eax, -248(%rbp)
	movl	%esi, %eax
.L189:
	leal	-1(%rax), %esi
	movl	%eax, -264(%rbp)
	movq	%r10, %rdi
	leaq	-200(%rbp), %r14
	movl	%ecx, -260(%rbp)
	movzwl	%si, %esi
	movq	%r10, -256(%rbp)
	call	*88(%r9)
	subq	$8, %rsp
	pushq	-224(%rbp)
	movl	%ebx, %edx
	movq	-256(%rbp), %r10
	pushq	-216(%rbp)
	movq	%r13, %rsi
	movl	-240(%rbp), %r8d
	pushq	%r14
	movl	-248(%rbp), %r9d
	movq	%r10, %rdi
	movl	-260(%rbp), %ecx
	movq	$0, -200(%rbp)
	movq	%r10, -240(%rbp)
	call	_ZN2v88internalL16GenerateBranchesEPNS0_20RegExpMacroAssemblerEPNS0_8ZoneListIiEEiiiiPNS0_5LabelES7_S7_
	movl	-208(%rbp), %eax
	addq	$32, %rsp
	testl	%eax, %eax
	jle	.L148
	movq	-240(%rbp), %r10
	xorl	%r15d, %ebx
	leaq	-208(%rbp), %rsi
	movq	(%r10), %rdx
	movq	%r10, %rdi
	call	*64(%rdx)
	andl	$1, %ebx
	movq	-240(%rbp), %r10
	movl	-264(%rbp), %eax
	je	.L190
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rbx
	movq	%rdx, -224(%rbp)
	movq	%rbx, -216(%rbp)
.L190:
	subq	$8, %rsp
	pushq	-224(%rbp)
	movl	%eax, %r8d
	movl	%r12d, %ecx
	pushq	-216(%rbp)
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movl	-232(%rbp), %r9d
	pushq	%r14
	call	_ZN2v88internalL16GenerateBranchesEPNS0_20RegExpMacroAssemblerEPNS0_8ZoneListIiEEiiiiPNS0_5LabelES7_S7_
	addq	$32, %rsp
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L240:
	movq	-216(%rbp), %rdx
	jmp	.L189
.L182:
	movl	%edx, %r15d
	cmpl	$256, %esi
	jle	.L185
	.p2align 4,,10
	.p2align 3
.L191:
	leal	(%rbx,%r12), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	movl	-248(%rbp), %ecx
	sarl	%edx
	subl	%r14d, %ecx
	cmpl	$256, %ecx
	jle	.L185
	cmpl	%edx, %r15d
	jge	.L185
	movslq	%edx, %rcx
	addl	$255, %r14d
	movl	(%r8,%rcx,4), %edi
	cmpl	%edi, %r14d
	jge	.L185
	orl	$127, %edi
	addl	$1, %edi
	cmpl	%edx, %r12d
	jg	.L186
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L241:
	addq	$1, %rcx
	cmpl	%ecx, %r12d
	jle	.L185
.L186:
	cmpl	(%r8,%rcx,4), %edi
	jge	.L241
	movl	%edi, %esi
	movl	%ecx, %r15d
	jmp	.L185
.L195:
	movq	-224(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%r15, -224(%rbp)
	movq	%rax, -216(%rbp)
	movl	$1, %eax
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L237:
	movslq	%r15d, %r12
	movq	-240(%rbp), %r10
	movq	-248(%rbp), %r15
	movl	%r14d, %r13d
.L167:
	movl	$127, %edx
	movzbl	%r13b, %eax
	movabsq	$72340172838076673, %r9
	subl	%r12d, %edx
	imulq	%r9, %rax
	addq	-232(%rbp), %r12
	addq	$1, %rdx
	cmpl	$8, %edx
	jnb	.L175
	testb	$4, %dl
	jne	.L242
	testl	%edx, %edx
	je	.L176
	movb	%al, (%r12)
	testb	$2, %dl
	jne	.L243
.L176:
	movq	16(%r10), %rdi
	movl	$1, %edx
	movl	$128, %esi
	movq	%r10, -232(%rbp)
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	-232(%rbp), %r10
	movl	$16, %edx
	movq	%rax, %rsi
	leaq	-208(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L179:
	movzbl	(%rax,%rdx), %edi
	movq	(%rsi), %rcx
	addq	$1, %rdx
	movb	%dil, -2(%rdx,%rcx)
	cmpq	$144, %rdx
	jne	.L179
	movq	(%r10), %rax
	movq	-216(%rbp), %rdx
	movq	%r10, %rdi
	movq	%r10, -216(%rbp)
	call	*184(%rax)
	movq	-224(%rbp), %rsi
	cmpq	%rsi, %r15
	je	.L148
	movq	-216(%rbp), %r10
	movq	(%r10), %rax
	movq	%r10, %rdi
	call	*224(%rax)
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L150:
	leal	-1(%r14), %esi
	movq	-216(%rbp), %rdx
	movq	%r10, %rdi
	movzwl	%si, %esi
	call	*88(%rax)
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	8(%r12), %rdi
	movl	%edx, %ecx
	movq	%rax, (%r12)
	andq	$-8, %rdi
	movq	%rax, -8(%r12,%rcx)
	subq	%rdi, %r12
	addl	%edx, %r12d
	shrl	$3, %r12d
	movl	%r12d, %ecx
	rep stosq
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L234:
	leal	-1(%r12), %ecx
	jmp	.L162
.L236:
	xorl	%r12d, %r12d
	jmp	.L167
.L235:
	leaq	-192(%rbp), %rax
	movq	%rax, -232(%rbp)
	jmp	.L172
.L242:
	movl	%edx, %edx
	movl	%eax, (%r12)
	movl	%eax, -4(%r12,%rdx)
	jmp	.L176
.L243:
	movl	%edx, %edx
	movw	%ax, -2(%r12,%rdx)
	jmp	.L176
.L231:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20391:
	.size	_ZN2v88internalL16GenerateBranchesEPNS0_20RegExpMacroAssemblerEPNS0_8ZoneListIiEEiiiiPNS0_5LabelES7_S7_, .-_ZN2v88internalL16GenerateBranchesEPNS0_20RegExpMacroAssemblerEPNS0_8ZoneListIiEEiiiiPNS0_5LabelES7_S7_
	.section	.text._ZN2v88internal7EndNode6AcceptEPNS0_11NodeVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7EndNode6AcceptEPNS0_11NodeVisitorE
	.type	_ZN2v88internal7EndNode6AcceptEPNS0_11NodeVisitorE, @function
_ZN2v88internal7EndNode6AcceptEPNS0_11NodeVisitorE:
.LFB20372:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	leaq	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE8VisitEndEPNS0_7EndNodeE(%rip), %rdx
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L246
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	movq	%r8, %rsi
	jmp	*%rax
	.cfi_endproc
.LFE20372:
	.size	_ZN2v88internal7EndNode6AcceptEPNS0_11NodeVisitorE, .-_ZN2v88internal7EndNode6AcceptEPNS0_11NodeVisitorE
	.section	.text._ZN2v88internal10ActionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ActionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.type	_ZN2v88internal10ActionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, @function
_ZN2v88internal10ActionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb:
.LFB20399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpl	$4, 80(%rdi)
	movq	%r8, %rbx
	je	.L258
	movq	56(%rdi), %rdi
	subl	$1, %ecx
	movzbl	%r9b, %r9d
	movq	(%rdi), %rax
	call	*72(%rax)
.L250:
	testl	%r12d, %r12d
	jne	.L247
	movzbl	%r14b, %r14d
	movq	%rbx, 32(%r13,%r14,8)
.L247:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movslq	%edx, %rcx
	pcmpeqd	%xmm0, %xmm0
	salq	$3, %rcx
	cmpl	(%r8), %edx
	jge	.L250
	.p2align 4,,10
	.p2align 3
.L254:
	movq	24(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax,%rcx), %rax
	cmpl	$128, 16(%rax)
	movl	$3, 20(%rax)
	je	.L251
	movl	$128, 16(%rax)
	addl	$1, %edx
	addq	$8, %rcx
	movups	%xmm0, (%rax)
	cmpl	%edx, (%rbx)
	jg	.L254
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L251:
	addl	$1, %edx
	addq	$8, %rcx
	cmpl	%edx, (%rbx)
	jg	.L254
	jmp	.L250
	.cfi_endproc
.LFE20399:
	.size	_ZN2v88internal10ActionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, .-_ZN2v88internal10ActionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.section	.text._ZN2v88internal17BackReferenceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17BackReferenceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.type	_ZN2v88internal17BackReferenceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, @function
_ZN2v88internal17BackReferenceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb:
.LFB20554:
	.cfi_startproc
	endbr64
	cmpl	(%r8), %edx
	jge	.L260
	movslq	%edx, %rax
	movl	%edx, %ecx
	pcmpeqd	%xmm0, %xmm0
	salq	$3, %rax
	.p2align 4,,10
	.p2align 3
.L263:
	movq	24(%r8), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%rax), %rsi
	cmpl	$128, 16(%rsi)
	movl	$3, 20(%rsi)
	je	.L261
	movl	$128, 16(%rsi)
	addl	$1, %ecx
	addq	$8, %rax
	movups	%xmm0, (%rsi)
	cmpl	%ecx, (%r8)
	jg	.L263
.L260:
	testl	%edx, %edx
	jne	.L259
.L265:
	movzbl	%r9b, %r9d
	movq	%r8, 32(%rdi,%r9,8)
.L259:
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	addl	$1, %ecx
	addq	$8, %rax
	cmpl	%ecx, (%r8)
	jg	.L263
	testl	%edx, %edx
	jne	.L259
	jmp	.L265
	.cfi_endproc
.LFE20554:
	.size	_ZN2v88internal17BackReferenceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, .-_ZN2v88internal17BackReferenceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.section	.rodata._ZN2v88internal10ChoiceNode6AcceptEPNS0_11NodeVisitorE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Stack overflow"
	.section	.text._ZN2v88internal10ChoiceNode6AcceptEPNS0_11NodeVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode6AcceptEPNS0_11NodeVisitorE
	.type	_ZN2v88internal10ChoiceNode6AcceptEPNS0_11NodeVisitorE, @function
_ZN2v88internal10ChoiceNode6AcceptEPNS0_11NodeVisitorE:
.LFB20374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitChoiceEPNS0_10ChoiceNodeE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L267
	movq	56(%rdi), %rsi
	xorl	%ebx, %ebx
	movl	12(%rsi), %eax
	testl	%eax, %eax
	jle	.L266
	.p2align 4,,10
	.p2align 3
.L268:
	movq	8(%r13), %rdx
	movq	(%rsi), %rax
	movq	%rbx, %r14
	salq	$4, %r14
	movq	%rdx, -56(%rbp)
	movq	(%rax,%r14), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-56(%rbp), %rdx
	cmpq	37528(%rdx), %rax
	jb	.L278
	movzbl	25(%r15), %eax
	testb	$2, %al
	jne	.L271
	testb	$1, %al
	jne	.L271
	orl	$1, %eax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movb	%al, 25(%r15)
	movq	(%r15), %rax
	call	*16(%rax)
	movzbl	25(%r15), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%r15)
.L271:
	cmpq	$0, 24(%r13)
	jne	.L266
	movq	56(%r12), %rsi
	movzbl	25(%r12), %ecx
	movq	(%rsi), %rax
	movq	(%rax,%r14), %rdi
	movzbl	25(%rdi), %edx
	orl	%ecx, %edx
	andl	$-5, %ecx
	andl	$4, %edx
	orl	%ecx, %edx
	movb	%dl, 25(%r12)
	movzbl	25(%rdi), %eax
	orl	%edx, %eax
	andl	$-9, %edx
	andl	$8, %eax
	orl	%edx, %eax
	movb	%al, 25(%r12)
	movzbl	25(%rdi), %edx
	orl	%eax, %edx
	andl	$-17, %eax
	andl	$16, %edx
	orl	%edx, %eax
	movb	%al, 25(%r12)
	testq	%rbx, %rbx
	je	.L273
	movq	(%rsi), %rax
	movzbl	27(%r12), %ecx
	movzbl	26(%r12), %edi
	movq	(%rax,%r14), %rax
	movzbl	27(%rax), %edx
	cmpb	%cl, %dl
	cmova	%ecx, %edx
	movzbl	26(%rax), %ecx
	movl	%edi, %eax
	cmpb	%dil, %cl
	movb	%dl, 27(%r12)
	cmovbe	%ecx, %eax
	addq	$1, %rbx
	movb	%al, 26(%r12)
	cmpl	%ebx, 12(%rsi)
	jg	.L268
.L266:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movq	(%rsi), %rax
	movl	$1, %ebx
	movq	(%rax,%r14), %rax
	movzwl	26(%rax), %eax
	movw	%ax, 26(%r12)
	cmpl	$1, 12(%rsi)
	jg	.L268
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L278:
	leaq	.LC1(%rip), %rax
	movq	%rax, 24(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%rdi, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE20374:
	.size	_ZN2v88internal10ChoiceNode6AcceptEPNS0_11NodeVisitorE, .-_ZN2v88internal10ChoiceNode6AcceptEPNS0_11NodeVisitorE
	.section	.text._ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitChoiceEPNS0_10ChoiceNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitChoiceEPNS0_10ChoiceNodeE, @function
_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitChoiceEPNS0_10ChoiceNodeE:
.LFB24790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	56(%rsi), %rsi
	movl	12(%rsi), %eax
	testl	%eax, %eax
	jle	.L279
	movq	%rdi, %r14
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L286:
	movq	8(%r14), %rdx
	movq	(%rsi), %rax
	movq	%r12, %r13
	salq	$4, %r13
	movq	%rdx, -56(%rbp)
	movq	(%rax,%r13), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-56(%rbp), %rdx
	cmpq	37528(%rdx), %rax
	jb	.L288
	movzbl	25(%r15), %eax
	testb	$2, %al
	jne	.L283
	testb	$1, %al
	jne	.L283
	orl	$1, %eax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movb	%al, 25(%r15)
	movq	(%r15), %rax
	call	*16(%rax)
	movzbl	25(%r15), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%r15)
.L283:
	cmpq	$0, 24(%r14)
	jne	.L279
	movq	56(%rbx), %rsi
	movzbl	25(%rbx), %ecx
	movq	(%rsi), %rax
	movq	(%rax,%r13), %rdi
	movzbl	25(%rdi), %edx
	orl	%ecx, %edx
	andl	$-5, %ecx
	andl	$4, %edx
	orl	%ecx, %edx
	movb	%dl, 25(%rbx)
	movzbl	25(%rdi), %eax
	orl	%edx, %eax
	andl	$-9, %edx
	andl	$8, %eax
	orl	%edx, %eax
	movb	%al, 25(%rbx)
	movzbl	25(%rdi), %edx
	orl	%eax, %edx
	andl	$-17, %eax
	andl	$16, %edx
	orl	%edx, %eax
	movb	%al, 25(%rbx)
	testq	%r12, %r12
	je	.L284
	movq	(%rsi), %rax
	movzbl	27(%rbx), %ecx
	movzbl	26(%rbx), %edi
	movq	(%rax,%r13), %rax
	movzbl	27(%rax), %edx
	cmpb	%cl, %dl
	cmova	%ecx, %edx
	movzbl	26(%rax), %ecx
	movl	%edi, %eax
	cmpb	%dil, %cl
	movb	%dl, 27(%rbx)
	cmovbe	%ecx, %eax
	addq	$1, %r12
	movb	%al, 26(%rbx)
	cmpl	%r12d, 12(%rsi)
	jg	.L286
.L279:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movq	(%rsi), %rax
	movl	$1, %r12d
	movq	(%rax,%r13), %rax
	movzwl	26(%rax), %eax
	movw	%ax, 26(%rbx)
	cmpl	$1, 12(%rsi)
	jg	.L286
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L288:
	leaq	.LC1(%rip), %rax
	movq	%rax, 24(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24790:
	.size	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitChoiceEPNS0_10ChoiceNodeE, .-_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitChoiceEPNS0_10ChoiceNodeE
	.section	.text._ZN2v88internal10ChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.type	_ZN2v88internal10ChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, @function
_ZN2v88internal10ChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb:
.LFB20555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-1(%rcx), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	cltd
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	56(%rdi), %r14
	movq	%rdi, -72(%rbp)
	movq	%rsi, -56(%rbp)
	movl	12(%r14), %ecx
	movl	%r9d, -64(%rbp)
	idivl	%ecx
	testl	%ecx, %ecx
	jle	.L290
	movl	%eax, -60(%rbp)
	xorl	%ebx, %ebx
	movzbl	%r9b, %r9d
	movq	%rbx, %r13
	movq	%r14, %rbx
	movl	%r9d, %r14d
	.p2align 4,,10
	.p2align 3
.L298:
	movq	%r13, %rax
	salq	$4, %rax
	addq	(%rbx), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L291
	movl	12(%rdx), %edx
	testl	%edx, %edx
	jne	.L306
.L291:
	movq	(%rax), %rdi
	movl	-60(%rbp), %ecx
	movl	%r14d, %r9d
	movq	%r15, %r8
	movq	-56(%rbp), %rsi
	movl	%r12d, %edx
	addq	$1, %r13
	movq	(%rdi), %rax
	call	*72(%rax)
	cmpl	%r13d, 12(%rbx)
	jg	.L298
.L290:
	testl	%r12d, %r12d
	jne	.L289
.L307:
	movzbl	-64(%rbp), %r14d
	movq	-72(%rbp), %rax
	movq	%r15, 32(%rax,%r14,8)
.L289:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	cmpl	(%r15), %r12d
	jge	.L290
	movslq	%r12d, %rdx
	movl	%r12d, %ecx
	pcmpeqd	%xmm0, %xmm0
	salq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L295:
	movq	24(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax,%rdx), %rax
	cmpl	$128, 16(%rax)
	movl	$3, 20(%rax)
	je	.L293
	movl	$128, 16(%rax)
	addl	$1, %ecx
	addq	$8, %rdx
	movups	%xmm0, (%rax)
	cmpl	(%r15), %ecx
	jl	.L295
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L293:
	addl	$1, %ecx
	addq	$8, %rdx
	cmpl	%ecx, (%r15)
	jg	.L295
	testl	%r12d, %r12d
	jne	.L289
	jmp	.L307
	.cfi_endproc
.LFE20555:
	.size	_ZN2v88internal10ChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, .-_ZN2v88internal10ChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.section	.text._ZN2v88internal14LoopChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LoopChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.type	_ZN2v88internal14LoopChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, @function
_ZN2v88internal14LoopChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb:
.LFB20443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, 88(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r8, %rbx
	jne	.L318
	testl	%ecx, %ecx
	jle	.L318
	movzbl	%r9b, %r13d
	subl	$1, %ecx
	movl	%r13d, %r9d
	call	_ZN2v88internal10ChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	testl	%r12d, %r12d
	jne	.L308
	movslq	%r13d, %r9
	movq	%rbx, 32(%r14,%r9,8)
.L308:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	cmpl	(%rbx), %r12d
	jge	.L311
	movslq	%r12d, %rcx
	movl	%r12d, %eax
	pcmpeqd	%xmm0, %xmm0
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L314:
	movq	24(%rbx), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%rcx), %rdx
	cmpl	$128, 16(%rdx)
	movl	$3, 20(%rdx)
	je	.L312
	movl	$128, 16(%rdx)
	addl	$1, %eax
	addq	$8, %rcx
	movups	%xmm0, (%rdx)
	cmpl	%eax, (%rbx)
	jg	.L314
.L311:
	testl	%r12d, %r12d
	jne	.L308
.L320:
	movzbl	%r9b, %r9d
	movq	%rbx, 32(%r14,%r9,8)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	addl	$1, %eax
	addq	$8, %rcx
	cmpl	%eax, (%rbx)
	jg	.L314
	testl	%r12d, %r12d
	jne	.L308
	jmp	.L320
	.cfi_endproc
.LFE20443:
	.size	_ZN2v88internal14LoopChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, .-_ZN2v88internal14LoopChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.section	.text._ZN2v88internal8TextNode13FilterOneByteEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode13FilterOneByteEi
	.type	_ZN2v88internal8TextNode13FilterOneByteEi, @function
_ZN2v88internal8TextNode13FilterOneByteEi:
.LFB20436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzbl	25(%rdi), %eax
	testb	%al, %al
	jns	.L322
	movq	8(%rdi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	movl	%esi, %r14d
	testl	%esi, %esi
	js	.L379
	orl	$64, %eax
	movb	%al, 25(%rdi)
	movq	64(%rdi), %rax
	movl	12(%rax), %edx
	testl	%edx, %edx
	jle	.L325
	leal	-1(%rdx), %r12d
	xorl	%r15d, %r15d
	salq	$4, %r12
.L346:
	movq	(%rax), %rdi
	addq	%r15, %rdi
	movl	4(%rdi), %esi
	movq	8(%rdi), %r13
	testl	%esi, %esi
	je	.L380
	movq	48(%rbx), %rsi
	leaq	8(%r13), %rdi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE@PLT
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE@PLT
	movq	-56(%rbp), %rax
	movl	12(%rax), %edx
	testb	$1, 28(%r13)
	je	.L332
	testl	%edx, %edx
	je	.L327
	movq	(%rax), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L327
	cmpl	$254, 4(%rax)
	jle	.L327
	testb	$2, 24(%r13)
	je	.L333
	testl	%edx, %edx
	jle	.L333
	subl	$1, %edx
	leaq	8(%rax,%rdx,8), %rsi
.L339:
	movl	(%rax), %edx
	movl	4(%rax), %ecx
	cmpl	$924, %edx
	jg	.L334
	cmpl	$923, %ecx
	jg	.L327
	cmpl	$375, %ecx
	jle	.L337
	cmpl	$376, %edx
	jle	.L327
.L337:
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L339
	.p2align 4,,10
	.p2align 3
.L333:
	orb	$-128, 25(%rbx)
	xorl	%eax, %eax
	movq	$0, 8(%rbx)
.L330:
	andb	$-65, 25(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L333
	movq	(%rax), %rax
	cmpl	$255, (%rax)
	jle	.L327
	testb	$2, 24(%r13)
	je	.L333
	testl	%edx, %edx
	jle	.L333
	subl	$1, %edx
	leaq	8(%rax,%rdx,8), %rsi
.L345:
	movl	(%rax), %edx
	movl	4(%rax), %ecx
	cmpl	$924, %edx
	jg	.L340
	cmpl	$923, %ecx
	jg	.L327
	cmpl	$376, %edx
	jg	.L343
	cmpl	$375, %ecx
	jle	.L343
	.p2align 4,,10
	.p2align 3
.L327:
	cmpq	%r12, %r15
	je	.L325
	movq	64(%rbx), %rax
	addq	$16, %r15
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L380:
	movq	16(%r13), %rdx
	movq	8(%r13), %rax
	testl	%edx, %edx
	jle	.L327
	subl	$1, %edx
	leaq	2(%rax,%rdx,2), %rcx
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L328:
	cmpw	$255, %dx
	ja	.L333
.L329:
	movw	%dx, (%rax)
	addq	$2, %rax
	cmpq	%rcx, %rax
	je	.L327
.L331:
	movzwl	(%rax), %edx
	testb	$2, 24(%r13)
	je	.L328
	movl	%edx, %esi
	andl	$-33, %esi
	cmpw	$924, %si
	je	.L347
	cmpw	$376, %dx
	jne	.L328
	movl	$255, %edx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L347:
	movl	$181, %edx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L379:
	addq	$24, %rsp
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	cmpl	$956, %edx
	jle	.L381
.L343:
	addq	$8, %rax
	cmpq	%rsi, %rax
	jne	.L345
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L381:
	cmpl	$955, %ecx
	jle	.L343
	jmp	.L327
.L334:
	cmpl	$956, %edx
	jg	.L337
	cmpl	$955, %ecx
	jle	.L337
	jmp	.L327
.L325:
	movq	56(%rbx), %rdi
	leal	-2(%r14), %esi
	movq	(%rdi), %rax
	call	*80(%rax)
	testq	%rax, %rax
	je	.L333
	movq	%rax, 56(%rbx)
	movq	%rbx, %rax
	orb	$-128, 25(%rbx)
	movq	%rbx, 8(%rbx)
	jmp	.L330
	.cfi_endproc
.LFE20436:
	.size	_ZN2v88internal8TextNode13FilterOneByteEi, .-_ZN2v88internal8TextNode13FilterOneByteEi
	.section	.text._ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE18VisitBackReferenceEPNS0_17BackReferenceNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE18VisitBackReferenceEPNS0_17BackReferenceNodeE, @function
_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE18VisitBackReferenceEPNS0_17BackReferenceNodeE:
.LFB24793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %r14
	movq	%rdi, %rbx
	movq	56(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L387
	movzbl	25(%r13), %eax
	testb	$2, %al
	jne	.L385
	testb	$1, %al
	jne	.L385
	orl	$1, %eax
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movb	%al, 25(%r13)
	movq	0(%r13), %rax
	call	*16(%rax)
	movzbl	25(%r13), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%r13)
.L385:
	cmpq	$0, 24(%rbx)
	jne	.L382
	cmpb	$0, 76(%r12)
	jne	.L382
	movq	56(%r12), %rax
	movzwl	26(%rax), %eax
	movw	%ax, 26(%r12)
.L382:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	.cfi_restore_state
	leaq	.LC1(%rip), %rax
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24793:
	.size	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE18VisitBackReferenceEPNS0_17BackReferenceNodeE, .-_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE18VisitBackReferenceEPNS0_17BackReferenceNodeE
	.section	.text._ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitActionEPNS0_10ActionNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitActionEPNS0_10ActionNodeE, @function
_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitActionEPNS0_10ActionNodeE:
.LFB24789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %r14
	movq	%rsi, %rbx
	movq	56(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L399
	movzbl	25(%r13), %eax
	testb	$2, %al
	jne	.L391
	testb	$1, %al
	jne	.L391
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	%al, 25(%r13)
	movq	0(%r13), %rax
	call	*16(%rax)
	movzbl	25(%r13), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%r13)
.L391:
	cmpq	$0, 24(%r12)
	je	.L400
.L388:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movq	56(%rbx), %rdi
	movzbl	25(%rbx), %ecx
	movzbl	25(%rdi), %edx
	orl	%ecx, %edx
	andl	$-5, %ecx
	andl	$4, %edx
	orl	%ecx, %edx
	movb	%dl, 25(%rbx)
	movzbl	25(%rdi), %eax
	orl	%edx, %eax
	andl	$-9, %edx
	andl	$8, %eax
	orl	%edx, %eax
	movb	%al, 25(%rbx)
	movzbl	25(%rdi), %edx
	orl	%eax, %edx
	andl	$-17, %eax
	andl	$16, %edx
	orl	%edx, %eax
	movb	%al, 25(%rbx)
	movl	80(%rbx), %eax
	testl	%eax, %eax
	je	.L392
	cmpl	$4, %eax
	je	.L388
	movzwl	26(%rdi), %eax
	movw	%ax, 26(%rbx)
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L399:
	leaq	.LC1(%rip), %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	movq	(%rdi), %rax
	call	*32(%rax)
	movw	%ax, 26(%rbx)
	jmp	.L388
	.cfi_endproc
.LFE24789:
	.size	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitActionEPNS0_10ActionNodeE, .-_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitActionEPNS0_10ActionNodeE
	.section	.text._ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE14VisitAssertionEPNS0_13AssertionNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE14VisitAssertionEPNS0_13AssertionNodeE, @function
_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE14VisitAssertionEPNS0_13AssertionNodeE:
.LFB24794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %r14
	movq	%rsi, %rbx
	movq	56(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L407
	movzbl	25(%r13), %eax
	testb	$2, %al
	jne	.L404
	testb	$1, %al
	jne	.L404
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	%al, 25(%r13)
	movq	0(%r13), %rax
	call	*16(%rax)
	movzbl	25(%r13), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%r13)
.L404:
	cmpq	$0, 24(%r12)
	jne	.L401
	movq	56(%rbx), %rax
	cmpl	$1, 64(%rbx)
	movl	$-1, %ecx
	movzbl	26(%rax), %edx
	movzbl	27(%rax), %eax
	cmove	%ecx, %eax
	movb	%dl, 26(%rbx)
	movb	%al, 27(%rbx)
.L401:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	leaq	.LC1(%rip), %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24794:
	.size	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE14VisitAssertionEPNS0_13AssertionNodeE, .-_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE14VisitAssertionEPNS0_13AssertionNodeE
	.section	.text._ZN2v88internal17BackReferenceNode6AcceptEPNS0_11NodeVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17BackReferenceNode6AcceptEPNS0_11NodeVisitorE
	.type	_ZN2v88internal17BackReferenceNode6AcceptEPNS0_11NodeVisitorE, @function
_ZN2v88internal17BackReferenceNode6AcceptEPNS0_11NodeVisitorE:
.LFB20377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE18VisitBackReferenceEPNS0_17BackReferenceNodeE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L409
	movq	8(%rsi), %r14
	movq	56(%rdi), %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L414
	movzbl	25(%rbx), %eax
	testb	$2, %al
	jne	.L412
	testb	$1, %al
	jne	.L412
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movb	%al, 25(%rbx)
	movq	(%rbx), %rax
	call	*16(%rax)
	movzbl	25(%rbx), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%rbx)
.L412:
	cmpq	$0, 24(%r12)
	jne	.L408
	cmpb	$0, 76(%r13)
	jne	.L408
	movq	56(%r13), %rax
	movzwl	26(%rax), %eax
	movw	%ax, 26(%r13)
.L408:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	.cfi_restore_state
	leaq	.LC1(%rip), %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	popq	%rbx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE20377:
	.size	_ZN2v88internal17BackReferenceNode6AcceptEPNS0_11NodeVisitorE, .-_ZN2v88internal17BackReferenceNode6AcceptEPNS0_11NodeVisitorE
	.section	.text._ZN2v88internal13AssertionNode6AcceptEPNS0_11NodeVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13AssertionNode6AcceptEPNS0_11NodeVisitorE
	.type	_ZN2v88internal13AssertionNode6AcceptEPNS0_11NodeVisitorE, @function
_ZN2v88internal13AssertionNode6AcceptEPNS0_11NodeVisitorE:
.LFB20378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE14VisitAssertionEPNS0_13AssertionNodeE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L416
	movq	8(%rsi), %r14
	movq	56(%rdi), %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L422
	movzbl	25(%rbx), %eax
	testb	$2, %al
	jne	.L419
	testb	$1, %al
	jne	.L419
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movb	%al, 25(%rbx)
	movq	(%rbx), %rax
	call	*16(%rax)
	movzbl	25(%rbx), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%rbx)
.L419:
	cmpq	$0, 24(%r12)
	jne	.L415
	movq	56(%r13), %rax
	cmpl	$1, 64(%r13)
	movl	$-1, %ecx
	movzbl	26(%rax), %edx
	movzbl	27(%rax), %eax
	cmove	%ecx, %eax
	movb	%dl, 26(%r13)
	movb	%al, 27(%r13)
.L415:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	leaq	.LC1(%rip), %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	popq	%rbx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE20378:
	.size	_ZN2v88internal13AssertionNode6AcceptEPNS0_11NodeVisitorE, .-_ZN2v88internal13AssertionNode6AcceptEPNS0_11NodeVisitorE
	.section	.text._ZN2v88internal10ActionNode6AcceptEPNS0_11NodeVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ActionNode6AcceptEPNS0_11NodeVisitorE
	.type	_ZN2v88internal10ActionNode6AcceptEPNS0_11NodeVisitorE, @function
_ZN2v88internal10ActionNode6AcceptEPNS0_11NodeVisitorE:
.LFB20373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitActionEPNS0_10ActionNodeE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L424
	movq	8(%rsi), %r14
	movq	56(%rdi), %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L435
	movzbl	25(%rbx), %eax
	testb	$2, %al
	jne	.L427
	testb	$1, %al
	jne	.L427
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movb	%al, 25(%rbx)
	movq	(%rbx), %rax
	call	*16(%rax)
	movzbl	25(%rbx), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%rbx)
.L427:
	cmpq	$0, 24(%r12)
	je	.L436
.L423:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	movq	56(%r13), %rdi
	movzbl	25(%r13), %ecx
	movzbl	25(%rdi), %edx
	orl	%ecx, %edx
	andl	$-5, %ecx
	andl	$4, %edx
	orl	%ecx, %edx
	movb	%dl, 25(%r13)
	movzbl	25(%rdi), %eax
	orl	%edx, %eax
	andl	$-9, %edx
	andl	$8, %eax
	orl	%edx, %eax
	movb	%al, 25(%r13)
	movzbl	25(%rdi), %edx
	orl	%eax, %edx
	andl	$-17, %eax
	andl	$16, %edx
	orl	%edx, %eax
	movb	%al, 25(%r13)
	movl	80(%r13), %eax
	testl	%eax, %eax
	je	.L428
	cmpl	$4, %eax
	je	.L423
	movzwl	26(%rdi), %eax
	movw	%ax, 26(%r13)
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L435:
	leaq	.LC1(%rip), %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	popq	%rbx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	movq	(%rdi), %rax
	call	*32(%rax)
	movw	%ax, 26(%r13)
	jmp	.L423
	.cfi_endproc
.LFE20373:
	.size	_ZN2v88internal10ActionNode6AcceptEPNS0_11NodeVisitorE, .-_ZN2v88internal10ActionNode6AcceptEPNS0_11NodeVisitorE
	.section	.text._ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE29VisitNegativeLookaroundChoiceEPNS0_28NegativeLookaroundChoiceNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE29VisitNegativeLookaroundChoiceEPNS0_28NegativeLookaroundChoiceNodeE, @function
_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE29VisitNegativeLookaroundChoiceEPNS0_28NegativeLookaroundChoiceNodeE:
.LFB24792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	56(%rsi), %rax
	movq	%rsi, %rbx
	movq	8(%rdi), %r14
	movq	(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L442
	movzbl	25(%r13), %eax
	testb	$2, %al
	jne	.L440
	testb	$1, %al
	jne	.L440
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	%al, 25(%r13)
	movq	0(%r13), %rax
	call	*16(%rax)
	movzbl	25(%r13), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%r13)
.L440:
	cmpq	$0, 24(%r12)
	je	.L446
.L437:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L446:
	.cfi_restore_state
	movq	56(%rbx), %rdi
	movzbl	25(%rbx), %ecx
	movq	(%rdi), %rax
	movq	(%rax), %rsi
	movzbl	25(%rsi), %edx
	orl	%ecx, %edx
	andl	$-5, %ecx
	andl	$4, %edx
	orl	%ecx, %edx
	movb	%dl, 25(%rbx)
	movzbl	25(%rsi), %eax
	orl	%edx, %eax
	andl	$-9, %edx
	andl	$8, %eax
	orl	%edx, %eax
	movb	%al, 25(%rbx)
	movzbl	25(%rsi), %edx
	orl	%eax, %edx
	andl	$-17, %eax
	andl	$16, %edx
	orl	%edx, %eax
	movb	%al, 25(%rbx)
	movq	(%rdi), %rax
	movq	8(%r12), %r14
	movq	16(%rax), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L442
	movzbl	25(%r13), %eax
	testb	$2, %al
	jne	.L443
	testb	$1, %al
	jne	.L443
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	%al, 25(%r13)
	movq	0(%r13), %rax
	call	*16(%rax)
	movzbl	25(%r13), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%r13)
.L443:
	cmpq	$0, 24(%r12)
	jne	.L437
	movq	56(%rbx), %rdi
	movzbl	25(%rbx), %ecx
	movq	(%rdi), %rax
	movq	16(%rax), %rsi
	movzbl	25(%rsi), %edx
	orl	%ecx, %edx
	andl	$-5, %ecx
	andl	$4, %edx
	orl	%ecx, %edx
	movb	%dl, 25(%rbx)
	movzbl	25(%rsi), %eax
	orl	%edx, %eax
	andl	$-9, %edx
	andl	$8, %eax
	orl	%edx, %eax
	movb	%al, 25(%rbx)
	movzbl	25(%rsi), %edx
	orl	%eax, %edx
	andl	$-17, %eax
	andl	$16, %edx
	orl	%edx, %eax
	movb	%al, 25(%rbx)
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movzwl	26(%rax), %eax
	movw	%ax, 26(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	leaq	.LC1(%rip), %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24792:
	.size	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE29VisitNegativeLookaroundChoiceEPNS0_28NegativeLookaroundChoiceNodeE, .-_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE29VisitNegativeLookaroundChoiceEPNS0_28NegativeLookaroundChoiceNodeE
	.section	.text._ZN2v88internal14LoopChoiceNode24EatsAtLeastFromLoopEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LoopChoiceNode24EatsAtLeastFromLoopEntryEv
	.type	_ZN2v88internal14LoopChoiceNode24EatsAtLeastFromLoopEntryEv, @function
_ZN2v88internal14LoopChoiceNode24EatsAtLeastFromLoopEntryEv:
.LFB20408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal14LoopChoiceNode13read_backwardEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L448
	movzbl	89(%rdi), %eax
.L449:
	testb	%al, %al
	je	.L472
	movzwl	26(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	movq	72(%rbx), %rsi
	movq	80(%rbx), %r9
	movzbl	27(%rsi), %edx
	movzbl	27(%r9), %edi
	movzbl	26(%rsi), %r10d
	movzbl	%dl, %esi
	subl	%edi, %edx
	movl	%edi, %ecx
	cmpl	%edi, %esi
	movl	%r10d, %r8d
	movl	%edx, %esi
	movl	92(%rbx), %edx
	cmovs	%eax, %esi
	subl	%edi, %r8d
	cmpl	%edi, %r10d
	cmovns	%r8d, %eax
	cmpl	$255, %edx
	jle	.L473
	movl	$1, %r8d
	movl	$255, %edx
.L452:
	movzbl	%sil, %esi
	movl	%edx, %ecx
	movl	$255, %r10d
	imull	%esi, %ecx
	addl	%edi, %ecx
	cmpl	$255, %ecx
	cmovg	%r10d, %ecx
	testb	%al, %al
	je	.L453
	testb	%r8b, %r8b
	je	.L453
	subl	$1, %edx
	movzbl	%al, %eax
	imull	%esi, %edx
	addl	%edx, %eax
	movl	$-1, %edx
	addl	%edi, %eax
	cmpl	$256, %eax
	cmovl	%eax, %edx
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L473:
	testl	%edx, %edx
	js	.L453
	andl	$255, %edx
	setne	%r8b
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L453:
	movzbl	26(%r9), %edx
.L455:
	xorl	%eax, %eax
	addq	$8, %rsp
	movb	%dl, %al
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movb	%cl, %ah
	ret
	.p2align 4,,10
	.p2align 3
.L448:
	.cfi_restore_state
	call	*%rax
	jmp	.L449
	.cfi_endproc
.LFE20408:
	.size	_ZN2v88internal14LoopChoiceNode24EatsAtLeastFromLoopEntryEv, .-_ZN2v88internal14LoopChoiceNode24EatsAtLeastFromLoopEntryEv
	.section	.text._ZN2v88internal28NegativeLookaroundChoiceNode6AcceptEPNS0_11NodeVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28NegativeLookaroundChoiceNode6AcceptEPNS0_11NodeVisitorE
	.type	_ZN2v88internal28NegativeLookaroundChoiceNode6AcceptEPNS0_11NodeVisitorE, @function
_ZN2v88internal28NegativeLookaroundChoiceNode6AcceptEPNS0_11NodeVisitorE:
.LFB20376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE29VisitNegativeLookaroundChoiceEPNS0_28NegativeLookaroundChoiceNodeE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L475
	movq	56(%rdi), %rax
	movq	8(%rsi), %r14
	movq	(%rax), %rax
	movq	(%rax), %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L480
	movzbl	25(%rbx), %eax
	testb	$2, %al
	jne	.L478
	testb	$1, %al
	jne	.L478
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movb	%al, 25(%rbx)
	movq	(%rbx), %rax
	call	*16(%rax)
	movzbl	25(%rbx), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%rbx)
.L478:
	cmpq	$0, 24(%r12)
	je	.L484
.L474:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	movq	56(%r13), %rdi
	movzbl	25(%r13), %ecx
	movq	(%rdi), %rax
	movq	(%rax), %rsi
	movzbl	25(%rsi), %edx
	orl	%ecx, %edx
	andl	$-5, %ecx
	andl	$4, %edx
	orl	%ecx, %edx
	movb	%dl, 25(%r13)
	movzbl	25(%rsi), %eax
	orl	%edx, %eax
	andl	$-9, %edx
	andl	$8, %eax
	orl	%edx, %eax
	movb	%al, 25(%r13)
	movzbl	25(%rsi), %edx
	orl	%eax, %edx
	andl	$-17, %eax
	andl	$16, %edx
	orl	%edx, %eax
	movb	%al, 25(%r13)
	movq	(%rdi), %rax
	movq	8(%r12), %r14
	movq	16(%rax), %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L480
	movzbl	25(%rbx), %eax
	testb	$2, %al
	jne	.L481
	testb	$1, %al
	jne	.L481
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movb	%al, 25(%rbx)
	movq	(%rbx), %rax
	call	*16(%rax)
	movzbl	25(%rbx), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%rbx)
.L481:
	cmpq	$0, 24(%r12)
	jne	.L474
	movq	56(%r13), %rdi
	movzbl	25(%r13), %ecx
	popq	%rbx
	popq	%r12
	movq	(%rdi), %rax
	movq	16(%rax), %rsi
	movzbl	25(%rsi), %edx
	orl	%ecx, %edx
	andl	$-5, %ecx
	andl	$4, %edx
	orl	%ecx, %edx
	movb	%dl, 25(%r13)
	movzbl	25(%rsi), %eax
	orl	%edx, %eax
	andl	$-9, %edx
	andl	$8, %eax
	orl	%edx, %eax
	movb	%al, 25(%r13)
	movzbl	25(%rsi), %edx
	orl	%eax, %edx
	andl	$-17, %eax
	andl	$16, %edx
	orl	%edx, %eax
	movb	%al, 25(%r13)
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movzwl	26(%rax), %eax
	movw	%ax, 26(%r13)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore_state
	leaq	.LC1(%rip), %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	.cfi_restore_state
	popq	%rbx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE20376:
	.size	_ZN2v88internal28NegativeLookaroundChoiceNode6AcceptEPNS0_11NodeVisitorE, .-_ZN2v88internal28NegativeLookaroundChoiceNode6AcceptEPNS0_11NodeVisitorE
	.section	.text._ZN2v88internal14LoopChoiceNode6AcceptEPNS0_11NodeVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LoopChoiceNode6AcceptEPNS0_11NodeVisitorE
	.type	_ZN2v88internal14LoopChoiceNode6AcceptEPNS0_11NodeVisitorE, @function
_ZN2v88internal14LoopChoiceNode6AcceptEPNS0_11NodeVisitorE:
.LFB20375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE15VisitLoopChoiceEPNS0_14LoopChoiceNodeE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L486
	movq	8(%rsi), %r14
	movq	80(%rdi), %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L491
	movzbl	25(%rbx), %eax
	testb	$2, %al
	jne	.L489
	testb	$1, %al
	jne	.L489
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movb	%al, 25(%rbx)
	movq	(%rbx), %rax
	call	*16(%rax)
	movzbl	25(%rbx), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%rbx)
.L489:
	cmpq	$0, 24(%r12)
	je	.L495
.L485:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	movq	80(%r13), %rsi
	movzbl	25(%r13), %ecx
	movq	72(%r13), %rbx
	movzbl	25(%rsi), %edx
	orl	%ecx, %edx
	andl	$-5, %ecx
	andl	$4, %edx
	orl	%ecx, %edx
	movb	%dl, 25(%r13)
	movzbl	25(%rsi), %eax
	orl	%edx, %eax
	andl	$-9, %edx
	andl	$8, %eax
	orl	%edx, %eax
	movb	%al, 25(%r13)
	movzbl	25(%rsi), %edx
	orl	%eax, %edx
	andl	$-17, %eax
	andl	$16, %edx
	orl	%edx, %eax
	movb	%al, 25(%r13)
	movzwl	26(%rsi), %eax
	movw	%ax, 26(%r13)
	movq	8(%r12), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L491
	movzbl	25(%rbx), %eax
	testb	$2, %al
	jne	.L492
	testb	$1, %al
	jne	.L492
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movb	%al, 25(%rbx)
	movq	(%rbx), %rax
	call	*16(%rax)
	movzbl	25(%rbx), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%rbx)
.L492:
	cmpq	$0, 24(%r12)
	jne	.L485
	movq	72(%r13), %rsi
	movzbl	25(%r13), %ecx
	popq	%rbx
	popq	%r12
	movzbl	25(%rsi), %edx
	orl	%ecx, %edx
	andl	$-5, %ecx
	andl	$4, %edx
	orl	%ecx, %edx
	movb	%dl, 25(%r13)
	movzbl	25(%rsi), %eax
	orl	%edx, %eax
	andl	$-9, %edx
	andl	$8, %eax
	orl	%edx, %eax
	movb	%al, 25(%r13)
	movzbl	25(%rsi), %edx
	orl	%eax, %edx
	andl	$-17, %eax
	andl	$16, %edx
	orl	%edx, %eax
	movb	%al, 25(%r13)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L491:
	.cfi_restore_state
	leaq	.LC1(%rip), %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	.cfi_restore_state
	popq	%rbx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE20375:
	.size	_ZN2v88internal14LoopChoiceNode6AcceptEPNS0_11NodeVisitorE, .-_ZN2v88internal14LoopChoiceNode6AcceptEPNS0_11NodeVisitorE
	.section	.text._ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE15VisitLoopChoiceEPNS0_14LoopChoiceNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE15VisitLoopChoiceEPNS0_14LoopChoiceNodeE, @function
_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE15VisitLoopChoiceEPNS0_14LoopChoiceNodeE:
.LFB24791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %r14
	movq	%rsi, %rbx
	movq	80(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L501
	movzbl	25(%r13), %eax
	testb	$2, %al
	jne	.L499
	testb	$1, %al
	jne	.L499
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	%al, 25(%r13)
	movq	0(%r13), %rax
	call	*16(%rax)
	movzbl	25(%r13), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%r13)
.L499:
	cmpq	$0, 24(%r12)
	je	.L505
.L496:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L505:
	.cfi_restore_state
	movq	80(%rbx), %rsi
	movzbl	25(%rbx), %ecx
	movq	72(%rbx), %r13
	movzbl	25(%rsi), %edx
	orl	%ecx, %edx
	andl	$-5, %ecx
	andl	$4, %edx
	orl	%ecx, %edx
	movb	%dl, 25(%rbx)
	movzbl	25(%rsi), %eax
	orl	%edx, %eax
	andl	$-9, %edx
	andl	$8, %eax
	orl	%edx, %eax
	movb	%al, 25(%rbx)
	movzbl	25(%rsi), %edx
	orl	%eax, %edx
	andl	$-17, %eax
	andl	$16, %edx
	orl	%edx, %eax
	movb	%al, 25(%rbx)
	movzwl	26(%rsi), %eax
	movw	%ax, 26(%rbx)
	movq	8(%r12), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L501
	movzbl	25(%r13), %eax
	testb	$2, %al
	jne	.L502
	testb	$1, %al
	jne	.L502
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	%al, 25(%r13)
	movq	0(%r13), %rax
	call	*16(%rax)
	movzbl	25(%r13), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%r13)
.L502:
	cmpq	$0, 24(%r12)
	jne	.L496
	movq	72(%rbx), %rsi
	movzbl	25(%rbx), %ecx
	movzbl	25(%rsi), %edx
	orl	%ecx, %edx
	andl	$-5, %ecx
	andl	$4, %edx
	orl	%ecx, %edx
	movb	%dl, 25(%rbx)
	movzbl	25(%rsi), %eax
	orl	%edx, %eax
	andl	$-9, %edx
	andl	$8, %eax
	orl	%edx, %eax
	movb	%al, 25(%rbx)
	movzbl	25(%rsi), %edx
	orl	%eax, %edx
	andl	$-17, %eax
	andl	$16, %edx
	orl	%edx, %eax
	movb	%al, 25(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_restore_state
	leaq	.LC1(%rip), %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24791:
	.size	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE15VisitLoopChoiceEPNS0_14LoopChoiceNodeE, .-_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE15VisitLoopChoiceEPNS0_14LoopChoiceNodeE
	.section	.text._ZN2v88internal8TextNode20GreedyLoopTextLengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode20GreedyLoopTextLengthEv
	.type	_ZN2v88internal8TextNode20GreedyLoopTextLengthEv, @function
_ZN2v88internal8TextNode20GreedyLoopTextLengthEv:
.LFB20462:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	movl	12(%rax), %esi
	leal	-1(%rsi), %edx
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	(%rax), %rdx
	movl	4(%rdx), %ecx
	movl	(%rdx), %eax
	testl	%ecx, %ecx
	je	.L507
	cmpl	$1, %ecx
	jne	.L515
	movl	$1, %edx
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	movq	8(%rdx), %rdx
	movl	16(%rdx), %edx
	addl	%edx, %eax
	ret
.L515:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20462:
	.size	_ZN2v88internal8TextNode20GreedyLoopTextLengthEv, .-_ZN2v88internal8TextNode20GreedyLoopTextLengthEv
	.section	.text._ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE,"axG",@progbits,_ZN2v88internal20RegExpCharacterClassC5EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	.type	_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE, @function
_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE:
.LFB8074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal20RegExpCharacterClassE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movq	%rdx, 8(%rdi)
	movw	%ax, 16(%rdi)
	movl	%ecx, 24(%rdi)
	movl	12(%rdx), %ecx
	movl	%r8d, 28(%rdi)
	testl	%ecx, %ecx
	je	.L524
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	movl	8(%rdx), %eax
	movq	%rdx, %r12
	testl	%eax, %eax
	jle	.L518
	movq	(%rdx), %rax
	movl	$1, 12(%rdx)
	movabsq	$4785070309113856, %rcx
	movq	%rcx, (%rax)
.L519:
	xorl	$1, 28(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	movq	%rsi, %rdi
	leal	1(%rax,%rax), %r13d
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	movslq	%r13d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L525
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L521:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L522
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
.L522:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%eax, 12(%r12)
	movabsq	$4785070309113856, %rax
	movl	%r13d, 8(%r12)
	movq	%rax, (%rcx,%rdx)
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L525:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L521
	.cfi_endproc
.LFE8074:
	.size	_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE, .-_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	.weak	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	.set	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE,_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	.section	.text._ZN2v88internal10RegExpAtom12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpAtom12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.type	_ZN2v88internal10RegExpAtom12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE, @function
_ZN2v88internal10RegExpAtom12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE:
.LFB20325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movslq	20(%rsi), %rax
	movl	16(%rsi), %ecx
	cmpl	%ecx, %eax
	jge	.L527
	leal	1(%rax), %edx
	movl	$4294967295, %edi
	salq	$4, %rax
	addq	8(%rsi), %rax
	movl	%edx, 20(%rsi)
	movq	%rdi, (%rax)
	movq	%r12, 8(%rax)
.L528:
	movq	16(%r12), %rax
	addl	%eax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	leal	1(%rcx,%rcx), %r13d
	movq	24(%rdx), %rax
	movq	16(%rdx), %rcx
	movslq	%r13d, %rsi
	salq	$4, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L533
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdx)
.L530:
	movslq	20(%rbx), %rdx
	movq	%rdx, %rsi
	salq	$4, %rdx
	testl	%esi, %esi
	jg	.L534
.L531:
	movq	%rcx, 8(%rbx)
	addl	$1, %esi
	addq	%rdx, %rcx
	movl	$4294967295, %eax
	movl	%r13d, 16(%rbx)
	movl	%esi, 20(%rbx)
	movq	%rax, (%rcx)
	movq	%r12, 8(%rcx)
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L534:
	movq	8(%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	20(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rsi
	salq	$4, %rdx
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L533:
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L530
	.cfi_endproc
.LFE20325:
	.size	_ZN2v88internal10RegExpAtom12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE, .-_ZN2v88internal10RegExpAtom12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.section	.text._ZN2v88internal20RegExpCharacterClass12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RegExpCharacterClass12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.type	_ZN2v88internal20RegExpCharacterClass12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE, @function
_ZN2v88internal20RegExpCharacterClass12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE:
.LFB20326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movslq	20(%rsi), %rax
	movl	16(%rsi), %ecx
	cmpl	%ecx, %eax
	jge	.L536
	movabsq	$8589934591, %rdi
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	8(%rsi), %rax
	movl	%edx, 20(%rsi)
	movq	%rdi, (%rax)
	movq	%r12, 8(%rax)
	addl	$1, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	.cfi_restore_state
	leal	1(%rcx,%rcx), %r13d
	movq	24(%rdx), %rax
	movq	16(%rdx), %rcx
	movslq	%r13d, %rsi
	salq	$4, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L542
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdx)
.L539:
	movslq	20(%rbx), %rdx
	movq	%rdx, %rsi
	salq	$4, %rdx
	testl	%esi, %esi
	jg	.L543
.L540:
	movq	%rcx, 8(%rbx)
	addl	$1, %esi
	addq	%rdx, %rcx
	movabsq	$8589934591, %rax
	movl	%r13d, 16(%rbx)
	movl	%esi, 20(%rbx)
	movq	%rax, (%rcx)
	movq	%r12, 8(%rcx)
	addl	$1, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	20(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rsi
	salq	$4, %rdx
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L542:
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L539
	.cfi_endproc
.LFE20326:
	.size	_ZN2v88internal20RegExpCharacterClass12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE, .-_ZN2v88internal20RegExpCharacterClass12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.section	.text._ZN2v88internal10RegExpText12AppendToTextEPS1_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpText12AppendToTextEPS1_PNS0_4ZoneE
	.type	_ZN2v88internal10RegExpText12AppendToTextEPS1_PNS0_4ZoneE, @function
_ZN2v88internal10RegExpText12AppendToTextEPS1_PNS0_4ZoneE:
.LFB20327:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	testl	%eax, %eax
	jle	.L561
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L551:
	movl	16(%r14), %eax
	addq	$1, %r12
	addl	%eax, 24(%rbx)
	cmpl	%r12d, 20(%r15)
	jle	.L564
.L545:
	movq	%r12, %rax
	movl	16(%rbx), %edx
	salq	$4, %rax
	addq	8(%r15), %rax
	movl	(%rax), %ecx
	movl	4(%rax), %r13d
	movq	8(%rax), %r14
	movslq	20(%rbx), %rax
	cmpl	%edx, %eax
	jge	.L546
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	8(%rbx), %rax
	movl	%edx, 20(%rbx)
	movl	%ecx, (%rax)
	movl	%r13d, 4(%rax)
	movq	%r14, 8(%rax)
.L547:
	testl	%r13d, %r13d
	je	.L551
	cmpl	$1, %r13d
	jne	.L565
	movl	$1, %eax
	addq	$1, %r12
	addl	%eax, 24(%rbx)
	cmpl	%r12d, 20(%r15)
	jg	.L545
.L564:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	leal	1(%rdx,%rdx), %r9d
	movslq	%r9d, %rsi
	salq	$4, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L566
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L549:
	movslq	20(%rbx), %rdx
	movq	%rdx, %rsi
	salq	$4, %rdx
	testl	%esi, %esi
	jle	.L550
	movq	8(%rbx), %rsi
	movq	%r8, -72(%rbp)
	movl	%r9d, -60(%rbp)
	movl	%ecx, -56(%rbp)
	call	memcpy@PLT
	movslq	20(%rbx), %rdx
	movq	-72(%rbp), %r8
	movl	-60(%rbp), %r9d
	movl	-56(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rsi
	salq	$4, %rdx
.L550:
	movq	%rdi, 8(%rbx)
	addl	$1, %esi
	addq	%rdx, %rdi
	movl	%r9d, 16(%rbx)
	movl	%esi, 20(%rbx)
	movl	%ecx, (%rdi)
	movl	%r13d, 4(%rdi)
	movq	%r14, 8(%rdi)
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L566:
	movq	%r8, %rdi
	movl	%r9d, -72(%rbp)
	movl	%ecx, -60(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movl	-60(%rbp), %ecx
	movl	-72(%rbp), %r9d
	movq	%rax, %rdi
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L561:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L565:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20327:
	.size	_ZN2v88internal10RegExpText12AppendToTextEPS1_PNS0_4ZoneE, .-_ZN2v88internal10RegExpText12AppendToTextEPS1_PNS0_4ZoneE
	.section	.text._ZN2v88internal11TextElement4AtomEPNS0_10RegExpAtomE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11TextElement4AtomEPNS0_10RegExpAtomE
	.type	_ZN2v88internal11TextElement4AtomEPNS0_10RegExpAtomE, @function
_ZN2v88internal11TextElement4AtomEPNS0_10RegExpAtomE:
.LFB20328:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movl	$4294967295, %eax
	ret
	.cfi_endproc
.LFE20328:
	.size	_ZN2v88internal11TextElement4AtomEPNS0_10RegExpAtomE, .-_ZN2v88internal11TextElement4AtomEPNS0_10RegExpAtomE
	.section	.text._ZN2v88internal11TextElement9CharClassEPNS0_20RegExpCharacterClassE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11TextElement9CharClassEPNS0_20RegExpCharacterClassE
	.type	_ZN2v88internal11TextElement9CharClassEPNS0_20RegExpCharacterClassE, @function
_ZN2v88internal11TextElement9CharClassEPNS0_20RegExpCharacterClassE:
.LFB20329:
	.cfi_startproc
	endbr64
	movabsq	$8589934591, %rax
	movq	%rdi, %rdx
	ret
	.cfi_endproc
.LFE20329:
	.size	_ZN2v88internal11TextElement9CharClassEPNS0_20RegExpCharacterClassE, .-_ZN2v88internal11TextElement9CharClassEPNS0_20RegExpCharacterClassE
	.section	.text._ZNK2v88internal11TextElement6lengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11TextElement6lengthEv
	.type	_ZNK2v88internal11TextElement6lengthEv, @function
_ZNK2v88internal11TextElement6lengthEv:
.LFB20330:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	testl	%eax, %eax
	je	.L570
	cmpl	$1, %eax
	jne	.L578
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	movq	8(%rdi), %rax
	movl	16(%rax), %eax
	ret
.L578:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20330:
	.size	_ZNK2v88internal11TextElement6lengthEv, .-_ZNK2v88internal11TextElement6lengthEv
	.section	.text._ZN2v88internal14RegExpCompilerC2EPNS0_7IsolateEPNS0_4ZoneEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpCompilerC2EPNS0_7IsolateEPNS0_4ZoneEib
	.type	_ZN2v88internal14RegExpCompilerC2EPNS0_7IsolateEPNS0_4ZoneEib, @function
_ZN2v88internal14RegExpCompilerC2EPNS0_7IsolateEPNS0_4ZoneEib:
.LFB20338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	2(%rcx,%rcx), %eax
	leaq	60(%rdi), %rcx
	movq	%rsi, %xmm5
	leaq	1084(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	%eax, 8(%rdi)
	xorl	%eax, %eax
	movdqa	.LC3(%rip), %xmm0
	movw	%ax, 49(%rdi)
	movzbl	_ZN2v88internal24FLAG_regexp_optimizationE(%rip), %eax
	movq	$-1, 12(%rdi)
	movq	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	movb	%r8b, 48(%rdi)
	movb	$0, 52(%rdi)
	movl	$1, 56(%rdi)
	movb	%al, 51(%rdi)
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L580:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L580
	movl	$0, 1084(%rbx)
	movdqa	.LC2(%rip), %xmm0
	pxor	%xmm2, %xmm2
	movdqa	.LC4(%rip), %xmm4
	.p2align 4,,10
	.p2align 3
.L581:
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm3
	movdqa	%xmm2, %xmm6
	addq	$32, %rcx
	punpckldq	%xmm1, %xmm3
	punpckhdq	%xmm1, %xmm6
	paddd	%xmm4, %xmm0
	movups	%xmm3, -32(%rcx)
	movups	%xmm6, -16(%rcx)
	cmpq	%rcx, %rsi
	jne	.L581
	movq	16(%rdx), %rax
	movq	24(%rdx), %rcx
	movdqa	%xmm5, %xmm0
	movq	%rdx, %xmm7
	punpcklqdq	%xmm7, %xmm0
	subq	%rax, %rcx
	movups	%xmm0, 1088(%rbx)
	cmpq	$63, %rcx
	jbe	.L587
	leaq	64(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L583:
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal7EndNodeE(%rip), %rdi
	movq	%rdx, 48(%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rdi, (%rax)
	movl	$0, 56(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L587:
	.cfi_restore_state
	movq	%rdx, %rdi
	movl	$64, %esi
	movq	%rdx, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-24(%rbp), %rdx
	jmp	.L583
	.cfi_endproc
.LFE20338:
	.size	_ZN2v88internal14RegExpCompilerC2EPNS0_7IsolateEPNS0_4ZoneEib, .-_ZN2v88internal14RegExpCompilerC2EPNS0_7IsolateEPNS0_4ZoneEib
	.globl	_ZN2v88internal14RegExpCompilerC1EPNS0_7IsolateEPNS0_4ZoneEib
	.set	_ZN2v88internal14RegExpCompilerC1EPNS0_7IsolateEPNS0_4ZoneEib,_ZN2v88internal14RegExpCompilerC2EPNS0_7IsolateEPNS0_4ZoneEib
	.section	.rodata._ZN2v88internal14RegExpCompiler8AssembleEPNS0_7IsolateEPNS0_20RegExpMacroAssemblerEPNS0_10RegExpNodeEiNS0_6HandleINS0_6StringEEE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"RegExp too big"
	.section	.text._ZN2v88internal14RegExpCompiler8AssembleEPNS0_7IsolateEPNS0_20RegExpMacroAssemblerEPNS0_10RegExpNodeEiNS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpCompiler8AssembleEPNS0_7IsolateEPNS0_20RegExpMacroAssemblerEPNS0_10RegExpNodeEiNS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal14RegExpCompiler8AssembleEPNS0_7IsolateEPNS0_20RegExpMacroAssemblerEPNS0_10RegExpNodeEiNS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal14RegExpCompiler8AssembleEPNS0_7IsolateEPNS0_20RegExpMacroAssemblerEPNS0_10RegExpNodeEiNS0_6HandleINS0_6StringEEE:
.LFB20340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-200(%rbp), %r15
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-160(%rbp), %rbx
	subq	$184, %rsp
	movq	%rdx, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-192(%rbp), %rax
	movq	%rcx, 40(%rsi)
	movq	%rax, 24(%rsi)
	movq	(%rcx), %rax
	movq	%r15, %rsi
	movaps	%xmm0, -192(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -200(%rbp)
	call	*288(%rax)
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movabsq	$-4294967196, %rax
	movups	%xmm0, -152(%rbp)
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	movq	(%r14), %rax
	movups	%xmm0, -136(%rbp)
	movl	$0, -160(%rbp)
	movq	$0, -120(%rbp)
	movl	$0, -112(%rbp)
	movl	$0, -108(%rbp)
	movb	$0, -104(%rbp)
	movl	$0, -102(%rbp)
	movb	$0, -98(%rbp)
	movl	$0, -96(%rbp)
	movb	$0, -92(%rbp)
	movl	$0, -90(%rbp)
	movb	$0, -86(%rbp)
	movq	$0, -84(%rbp)
	movb	$0, -76(%rbp)
	call	*24(%rax)
	movq	40(%r13), %rdi
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*64(%rax)
	movq	40(%r13), %rdi
	movq	(%rdi), %rax
	call	*208(%rax)
.L591:
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %rax
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L602:
	movq	-8(%rax), %rdi
	subq	$8, %rax
	movq	%rax, -184(%rbp)
	movl	16(%rdi), %ecx
	movb	$0, 24(%rdi)
	testl	%ecx, %ecx
	jns	.L601
.L590:
	cmpq	%rdx, %rax
	jne	.L602
	movq	40(%r13), %rdi
	cmpb	$0, 49(%r13)
	movq	(%rdi), %rax
	jne	.L603
	movq	16(%rbp), %rsi
	call	*216(%rax)
	leaq	-208(%rbp), %rdi
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	%rax, -208(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-216(%rbp), %rbx
	movq	$0, (%r12)
	cltq
	addq	%rax, 45736(%rbx)
	movl	8(%r13), %eax
	movq	$0, 24(%r13)
	movq	(%r14), %rdx
	movl	%eax, 16(%r12)
	movq	%rdx, 8(%r12)
.L593:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L588
	call	_ZdlPv@PLT
.L588:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L604
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	*24(%rax)
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L603:
	call	*16(%rax)
	leaq	.LC5(%rip), %rax
	movq	$0, 8(%r12)
	movq	%rax, (%r12)
	movl	$0, 16(%r12)
	jmp	.L593
.L604:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20340:
	.size	_ZN2v88internal14RegExpCompiler8AssembleEPNS0_7IsolateEPNS0_20RegExpMacroAssemblerEPNS0_10RegExpNodeEiNS0_6HandleINS0_6StringEEE, .-_ZN2v88internal14RegExpCompiler8AssembleEPNS0_7IsolateEPNS0_20RegExpMacroAssemblerEPNS0_10RegExpNodeEiNS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal5Trace14DeferredAction8MentionsEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Trace14DeferredAction8MentionsEi
	.type	_ZN2v88internal5Trace14DeferredAction8MentionsEi, @function
_ZN2v88internal5Trace14DeferredAction8MentionsEi:
.LFB20350:
	.cfi_startproc
	endbr64
	cmpl	$6, (%rdi)
	je	.L608
	cmpl	%esi, 4(%rdi)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L608:
	cmpl	16(%rdi), %esi
	setge	%al
	cmpl	20(%rdi), %esi
	setle	%dl
	andl	%edx, %eax
	ret
	.cfi_endproc
.LFE20350:
	.size	_ZN2v88internal5Trace14DeferredAction8MentionsEi, .-_ZN2v88internal5Trace14DeferredAction8MentionsEi
	.section	.text._ZN2v88internal5Trace12mentions_regEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Trace12mentions_regEi
	.type	_ZN2v88internal5Trace12mentions_regEi, @function
_ZN2v88internal5Trace12mentions_regEi:
.LFB20351:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	testq	%rdx, %rdx
	jne	.L613
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L611:
	cmpl	4(%rdx), %esi
	je	.L615
.L612:
	movq	8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L614
.L613:
	cmpl	$6, (%rdx)
	jne	.L611
	cmpl	20(%rdx), %esi
	setle	%al
	cmpl	16(%rdx), %esi
	setge	%cl
	andb	%cl, %al
	je	.L612
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L615:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE20351:
	.size	_ZN2v88internal5Trace12mentions_regEi, .-_ZN2v88internal5Trace12mentions_regEi
	.section	.text._ZN2v88internal5Trace17GetStoredPositionEiPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Trace17GetStoredPositionEiPi
	.type	_ZN2v88internal5Trace17GetStoredPositionEiPi, @function
_ZN2v88internal5Trace17GetStoredPositionEiPi:
.LFB20352:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	jne	.L622
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L619:
	cmpl	4(%rax), %esi
	je	.L630
.L621:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L631
.L622:
	movl	(%rax), %ecx
	cmpl	$6, %ecx
	jne	.L619
	cmpl	16(%rax), %esi
	jl	.L621
	cmpl	20(%rax), %esi
	jg	.L621
.L625:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L631:
	ret
	.p2align 4,,10
	.p2align 3
.L630:
	cmpl	$2, %ecx
	jne	.L625
	movl	16(%rax), %eax
	movl	%eax, (%rdx)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE20352:
	.size	_ZN2v88internal5Trace17GetStoredPositionEiPi, .-_ZN2v88internal5Trace17GetStoredPositionEiPi
	.section	.text._ZN2v88internal5Trace21FindAffectedRegistersEPNS0_13DynamicBitSetEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Trace21FindAffectedRegistersEPNS0_13DynamicBitSetEPNS0_4ZoneE
	.type	_ZN2v88internal5Trace21FindAffectedRegistersEPNS0_13DynamicBitSetEPNS0_4ZoneE, @function
_ZN2v88internal5Trace21FindAffectedRegistersEPNS0_13DynamicBitSetEPNS0_4ZoneE:
.LFB20355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L666
	movq	%rsi, %r12
	movq	%rdx, %r14
	movl	$-1, %r13d
	movl	$1, %r8d
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L680:
	movl	%r8d, %eax
	sall	%cl, %eax
	orl	%eax, (%r12)
.L653:
	movl	4(%rbx), %eax
	movq	8(%rbx), %rbx
	cmpl	%eax, %r13d
	cmovl	%eax, %r13d
	testq	%rbx, %rbx
	je	.L632
.L665:
	cmpl	$6, (%rbx)
	je	.L679
	movl	4(%rbx), %ecx
	cmpl	$31, %ecx
	jbe	.L680
	movq	8(%r12), %r15
	testq	%r15, %r15
	je	.L681
.L654:
	movslq	12(%r15), %rsi
	testl	%esi, %esi
	je	.L659
	movq	(%r15), %rax
	jle	.L659
	leal	-1(%rsi), %edx
	leaq	4(%rax,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L660:
	cmpl	(%rax), %ecx
	je	.L653
	addq	$4, %rax
	cmpq	%rax, %rdx
	jne	.L660
.L659:
	movl	8(%r15), %eax
	cmpl	%eax, %esi
	jge	.L661
	movq	(%r15), %rax
	leal	1(%rsi), %edx
	movl	%edx, 12(%r15)
	movl	%ecx, (%rax,%rsi,4)
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L679:
	movl	20(%rbx), %r9d
	movl	16(%rbx), %ecx
	leal	1(%r9), %r15d
	cmpl	%ecx, %r9d
	jge	.L650
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L682:
	movl	%r8d, %eax
	sall	%cl, %eax
	orl	%eax, (%r12)
.L638:
	addl	$1, %ecx
	cmpl	%r15d, %ecx
	je	.L651
.L650:
	cmpl	$31, %ecx
	jbe	.L682
	movq	8(%r12), %r10
	testq	%r10, %r10
	je	.L683
.L639:
	movslq	12(%r10), %rdx
	testl	%edx, %edx
	je	.L644
	movq	(%r10), %rax
	jle	.L644
	leal	-1(%rdx), %esi
	leaq	4(%rax,%rsi,4), %rsi
	.p2align 4,,10
	.p2align 3
.L645:
	cmpl	%ecx, (%rax)
	je	.L638
	addq	$4, %rax
	cmpq	%rax, %rsi
	jne	.L645
.L644:
	movl	8(%r10), %eax
	cmpl	%eax, %edx
	jge	.L646
	movq	(%r10), %rax
	leal	1(%rdx), %esi
	movl	%esi, 12(%r10)
	movl	%ecx, (%rax,%rdx,4)
	addl	$1, %ecx
	cmpl	%r15d, %ecx
	jne	.L650
	.p2align 4,,10
	.p2align 3
.L651:
	movq	8(%rbx), %rbx
	cmpl	%r9d, %r13d
	cmovl	%r9d, %r13d
	testq	%rbx, %rbx
	jne	.L665
.L632:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L646:
	.cfi_restore_state
	leal	1(%rax,%rax), %r11d
	movq	16(%r14), %rdi
	movslq	%r11d, %rax
	leaq	7(,%rax,4), %rsi
	movq	24(%r14), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L684
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L648:
	movslq	12(%r10), %rdx
	movq	%rdx, %rax
	salq	$2, %rdx
	testl	%eax, %eax
	jle	.L649
	movq	(%r10), %rsi
	movl	%r11d, -80(%rbp)
	movl	%ecx, -72(%rbp)
	movl	%r9d, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %r10
	movl	-80(%rbp), %r11d
	movl	$1, %r8d
	movl	-72(%rbp), %ecx
	movl	-64(%rbp), %r9d
	movq	%rax, %rdi
	movslq	12(%r10), %rdx
	movq	%rdx, %rax
	salq	$2, %rdx
.L649:
	addl	$1, %eax
	movq	%rdi, (%r10)
	movl	%r11d, 8(%r10)
	movl	%eax, 12(%r10)
	movl	%ecx, (%rdi,%rdx)
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L683:
	movq	24(%r14), %rax
	movq	16(%r14), %rdx
	movq	%rax, %rsi
	subq	%rdx, %rsi
	cmpq	$15, %rsi
	jbe	.L685
	leaq	16(%rdx), %rsi
	movq	%rdx, %r10
	subq	%rsi, %rax
	movq	%rsi, 16(%r14)
	cmpq	$7, %rax
	jbe	.L686
.L642:
	leaq	8(%rsi), %rax
	movq	%rax, 16(%r14)
.L643:
	movq	%rsi, (%rdx)
	movq	$1, 8(%rdx)
	movq	%rdx, 8(%r12)
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L661:
	leal	1(%rax,%rax), %r9d
	movq	16(%r14), %rdi
	movslq	%r9d, %rax
	leaq	7(,%rax,4), %rsi
	movq	24(%r14), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L687
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L663:
	movslq	12(%r15), %rdx
	movq	%rdx, %rax
	salq	$2, %rdx
	testl	%eax, %eax
	jg	.L688
.L664:
	addl	$1, %eax
	movq	%rdi, (%r15)
	movl	%r9d, 8(%r15)
	movl	%eax, 12(%r15)
	movl	%ecx, (%rdi,%rdx)
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L681:
	movq	24(%r14), %rax
	movq	16(%r14), %rdx
	movq	%rax, %rsi
	subq	%rdx, %rsi
	cmpq	$15, %rsi
	jbe	.L689
	leaq	16(%rdx), %rsi
	movq	%rsi, 16(%r14)
.L656:
	subq	%rsi, %rax
	movq	%rdx, %r15
	cmpq	$7, %rax
	jbe	.L690
	leaq	8(%rsi), %rax
	movq	%rax, 16(%r14)
.L658:
	movq	%rsi, (%rdx)
	movq	$1, 8(%rdx)
	movq	%rdx, 8(%r12)
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L688:
	movq	(%r15), %rsi
	movl	%r9d, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%r15), %rdx
	movl	-64(%rbp), %r9d
	movl	$1, %r8d
	movq	%rax, %rdi
	movl	-56(%rbp), %ecx
	movq	%rdx, %rax
	salq	$2, %rdx
	jmp	.L664
.L684:
	movq	%r14, %rdi
	movl	%r11d, -80(%rbp)
	movq	%r10, -72(%rbp)
	movl	%ecx, -64(%rbp)
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %r9d
	movl	-64(%rbp), %ecx
	movl	$1, %r8d
	movq	-72(%rbp), %r10
	movl	-80(%rbp), %r11d
	movq	%rax, %rdi
	jmp	.L648
.L689:
	movl	$16, %esi
	movq	%r14, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %ecx
	movq	16(%r14), %rsi
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	24(%r14), %rax
	jmp	.L656
.L685:
	movl	$16, %esi
	movq	%r14, %rdi
	movl	%ecx, -64(%rbp)
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r14), %rsi
	movl	-56(%rbp), %r9d
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	24(%r14), %rax
	movl	-64(%rbp), %ecx
	movq	%rdx, %r10
	subq	%rsi, %rax
	cmpq	$7, %rax
	ja	.L642
.L686:
	movl	$8, %esi
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movl	%ecx, -64(%rbp)
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %r9d
	movl	-64(%rbp), %ecx
	movl	$1, %r8d
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L643
.L666:
	movl	$-1, %r13d
	jmp	.L632
.L687:
	movq	%r14, %rdi
	movl	%r9d, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %ecx
	movl	-64(%rbp), %r9d
	movl	$1, %r8d
	movq	%rax, %rdi
	jmp	.L663
.L690:
	movl	$8, %esi
	movq	%r14, %rdi
	movq	%rdx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movl	$1, %r8d
	movq	%rax, %rsi
	jmp	.L658
	.cfi_endproc
.LFE20355:
	.size	_ZN2v88internal5Trace21FindAffectedRegistersEPNS0_13DynamicBitSetEPNS0_4ZoneE, .-_ZN2v88internal5Trace21FindAffectedRegistersEPNS0_13DynamicBitSetEPNS0_4ZoneE
	.section	.text._ZN2v88internal5Trace24RestoreAffectedRegistersEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Trace24RestoreAffectedRegistersEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetES6_
	.type	_ZN2v88internal5Trace24RestoreAffectedRegistersEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetES6_, @function
_ZN2v88internal5Trace24RestoreAffectedRegistersEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetES6_:
.LFB20356:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	js	.L728
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$1, %ebx
	subq	$24, %rsp
	movl	(%rcx), %esi
.L708:
	cmpl	$31, %r12d
	jle	.L732
.L693:
	movq	8(%r14), %rdx
	testq	%rdx, %rdx
	je	.L696
	movq	(%rdx), %rax
	movl	12(%rdx), %edx
	testl	%edx, %edx
	jle	.L696
	subl	$1, %edx
	leaq	4(%rax,%rdx,4), %rdx
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L733:
	addq	$4, %rax
	cmpq	%rax, %rdx
	je	.L696
.L697:
	cmpl	(%rax), %r12d
	jne	.L733
.L694:
	movq	(%r15), %rax
	movl	%r12d, %esi
	movq	%r15, %rdi
	subl	$1, %r12d
	call	*280(%rax)
.L698:
	cmpl	$-1, %r12d
	je	.L691
.L734:
	movl	(%r14), %esi
	cmpl	$31, %r12d
	jg	.L693
.L732:
	movl	%ebx, %eax
	movl	%r12d, %ecx
	sall	%cl, %eax
	testl	%esi, %eax
	jne	.L694
	movl	0(%r13), %edi
	testl	%edi, %eax
	jne	.L710
	subl	$1, %r12d
	cmpl	$-1, %r12d
	jne	.L734
.L691:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L696:
	.cfi_restore_state
	movq	8(%r13), %rdx
	testq	%rdx, %rdx
	je	.L699
	movq	(%rdx), %rax
	movl	12(%rdx), %edx
	testl	%edx, %edx
	jle	.L699
	subl	$1, %edx
	movl	0(%r13), %edi
	leaq	4(%rax,%rdx,4), %rdx
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L735:
	addq	$4, %rax
	cmpq	%rax, %rdx
	je	.L699
.L701:
	cmpl	(%rax), %r12d
	jne	.L735
.L700:
	leal	-1(%r12), %edx
	leal	1(%rdx), %r8d
	movl	%edx, %ecx
	cmpl	$31, %edx
	jle	.L736
	.p2align 4,,10
	.p2align 3
.L705:
	movq	8(%r13), %rsi
	testq	%rsi, %rsi
	je	.L702
	movq	(%rsi), %rax
	movl	12(%rsi), %esi
	testl	%esi, %esi
	jle	.L702
	subl	$1, %esi
	leaq	4(%rax,%rsi,4), %rsi
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L737:
	addq	$4, %rax
	cmpq	%rsi, %rax
	je	.L702
.L707:
	cmpl	%edx, (%rax)
	jne	.L737
.L703:
	subl	$1, %edx
	leal	1(%rdx), %r8d
	movl	%edx, %ecx
	cmpl	$31, %edx
	jg	.L705
.L736:
	movl	%ebx, %eax
	sall	%cl, %eax
	testl	%edi, %eax
	je	.L702
	testl	%ecx, %ecx
	jne	.L703
.L731:
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	.p2align 4,,10
	.p2align 3
.L702:
	movq	(%r15), %rax
	movl	%ecx, -52(%rbp)
	movl	%r12d, %edx
	movl	%r8d, %esi
	movq	%r15, %rdi
	call	*360(%rax)
	movl	-52(%rbp), %ecx
	movl	%ecx, %r12d
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L699:
	subl	$1, %r12d
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L728:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L710:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	testl	%r12d, %r12d
	je	.L731
	jmp	.L700
	.cfi_endproc
.LFE20356:
	.size	_ZN2v88internal5Trace24RestoreAffectedRegistersEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetES6_, .-_ZN2v88internal5Trace24RestoreAffectedRegistersEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetES6_
	.section	.text._ZN2v88internal5Trace22PerformDeferredActionsEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetEPS4_S7_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Trace22PerformDeferredActionsEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetEPS4_S7_PNS0_4ZoneE
	.type	_ZN2v88internal5Trace22PerformDeferredActionsEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetEPS4_S7_PNS0_4ZoneE, @function
_ZN2v88internal5Trace22PerformDeferredActionsEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetEPS4_S7_PNS0_4ZoneE:
.LFB20357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	leaq	_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv(%rip), %rdx
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	%rdi, -64(%rbp)
	movq	%r8, -88(%rbp)
	movq	24(%rax), %rax
	movq	%r9, -104(%rbp)
	cmpq	%rdx, %rax
	jne	.L739
	movq	32(%rsi), %rdi
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L740
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L851
	movq	32(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L740
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
.L743:
	addl	$1, %eax
	movl	$0, -52(%rbp)
	xorl	%r12d, %r12d
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, -92(%rbp)
	testl	%r13d, %r13d
	js	.L738
	.p2align 4,,10
	.p2align 3
.L744:
	cmpl	$31, %r12d
	jle	.L853
	movq	8(%r14), %rdx
	testq	%rdx, %rdx
	je	.L747
	movq	(%rdx), %rax
	movl	12(%rdx), %edx
	testl	%edx, %edx
	jle	.L747
	subl	$1, %edx
	leaq	4(%rax,%rdx,4), %rdx
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L854:
	addq	$4, %rax
	cmpq	%rax, %rdx
	je	.L747
.L749:
	cmpl	%r12d, (%rax)
	jne	.L854
	movq	-64(%rbp), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L747
.L802:
	movl	$-2147483648, %r8d
	xorl	%r10d, %r10d
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L847:
	cmpl	4(%rax), %r12d
	jne	.L753
	cmpl	$1, %edx
	je	.L754
	jbe	.L755
	cmpl	$2, %edx
	jne	.L756
	cmpl	$-2147483648, %r8d
	jne	.L759
	cmpb	$1, %r10b
	je	.L759
	movl	16(%rax), %r8d
.L759:
	xorl	%ecx, %ecx
	cmpb	$0, 20(%rax)
	setne	%cl
	addl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L753:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L855
.L762:
	movl	(%rax), %edx
	cmpl	$6, %edx
	jne	.L847
	cmpl	16(%rax), %r12d
	setge	%dl
	cmpl	20(%rax), %r12d
	setle	%sil
	andb	%sil, %dl
	je	.L753
	movq	8(%rax), %rax
	cmpl	$-2147483648, %r8d
	movl	$1, %ecx
	cmove	%edx, %r10d
	testq	%rax, %rax
	jne	.L762
	.p2align 4,,10
	.p2align 3
.L855:
	cmpl	$1, %ecx
	je	.L803
	cmpl	$2, %ecx
	jne	.L774
	cmpl	$31, %r12d
	jg	.L786
	movq	-104(%rbp), %rdi
	movl	$1, %eax
	movl	%r12d, %ecx
	sall	%cl, %eax
	orl	%eax, (%rdi)
	cmpl	$-2147483648, %r8d
	jne	.L856
	.p2align 4,,10
	.p2align 3
.L798:
	testb	%r10b, %r10b
	je	.L799
	movq	(%r15), %rax
	movl	%r12d, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	*360(%rax)
	.p2align 4,,10
	.p2align 3
.L747:
	addl	$1, %r12d
	cmpl	%r12d, %r13d
	jge	.L744
.L738:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L740:
	.cfi_restore_state
	call	*%rdx
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L755:
	movl	$1, %ecx
	testb	%bl, %bl
	jne	.L753
	addl	16(%rax), %r9d
	movl	$1, %ebx
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L754:
	cmpb	$1, %bl
	movl	$1, %ecx
	adcl	$0, %r9d
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L853:
	movl	$1, %eax
	movl	%r12d, %ecx
	sall	%cl, %eax
	testl	%eax, (%r14)
	je	.L747
	movq	-64(%rbp), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L747
	cmpl	$1, %r12d
	jg	.L802
	movl	$-2147483648, %r8d
	xorl	%r10d, %r10d
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L750:
	movl	(%rax), %edx
	cmpl	$6, %edx
	je	.L857
	cmpl	%r12d, 4(%rax)
	jne	.L765
	cmpl	$1, %edx
	je	.L766
	jbe	.L767
	cmpl	$2, %edx
	jne	.L756
	cmpb	$1, %r10b
	je	.L769
	cmpl	$-2147483648, %r8d
	jne	.L769
	movl	16(%rax), %r8d
.L769:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L765:
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L750
	cmpl	$1, %ecx
	je	.L803
	.p2align 4,,10
	.p2align 3
.L774:
	cmpl	$-2147483648, %r8d
	je	.L798
.L856:
	movq	(%r15), %rax
	movl	%r12d, %esi
	addl	$1, %r12d
	movl	%r8d, %edx
	movq	%r15, %rdi
	call	*352(%rax)
	cmpl	%r12d, %r13d
	jge	.L744
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L857:
	cmpl	%r12d, 20(%rax)
	setge	%dl
	cmpl	%r12d, 16(%rax)
	setle	%sil
	andb	%sil, %dl
	je	.L765
	cmpl	$-2147483648, %r8d
	movl	$1, %ecx
	cmove	%edx, %r10d
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L767:
	movl	$1, %ecx
	testb	%bl, %bl
	jne	.L765
	addl	16(%rax), %r9d
	movl	$1, %ebx
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L766:
	cmpb	$1, %bl
	movl	$1, %ecx
	adcl	$0, %r9d
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L803:
	addl	$1, -52(%rbp)
	xorl	%edx, %edx
	movl	-52(%rbp), %eax
	cmpl	%eax, -92(%rbp)
	jne	.L772
	movl	$0, -52(%rbp)
	movl	$1, %edx
.L772:
	movq	(%r15), %rax
	movl	%r9d, -56(%rbp)
	movl	%r12d, %esi
	movq	%r15, %rdi
	movb	%r10b, -80(%rbp)
	movl	%r8d, -72(%rbp)
	call	*304(%rax)
	cmpl	$31, %r12d
	movl	-72(%rbp), %r8d
	movzbl	-80(%rbp), %r10d
	movl	-56(%rbp), %r9d
	jg	.L773
	movq	-88(%rbp), %rdi
	movl	$1, %eax
	movl	%r12d, %ecx
	sall	%cl, %eax
	orl	%eax, (%rdi)
	jmp	.L774
.L773:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	je	.L858
.L775:
	movslq	12(%rcx), %rsi
	testl	%esi, %esi
	je	.L780
	movq	(%rcx), %rax
	jle	.L780
	leal	-1(%rsi), %edx
	leaq	4(%rax,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L781:
	cmpl	%r12d, (%rax)
	je	.L774
	addq	$4, %rax
	cmpq	%rax, %rdx
	jne	.L781
.L780:
	movl	8(%rcx), %eax
	cmpl	%eax, %esi
	jge	.L782
.L852:
	movq	(%rcx), %rax
	leal	1(%rsi), %edx
	movl	%edx, 12(%rcx)
	movl	%r12d, (%rax,%rsi,4)
	jmp	.L774
.L799:
	testb	%bl, %bl
	je	.L800
	movq	(%r15), %rax
	movl	%r12d, %esi
	addl	$1, %r12d
	movl	%r9d, %edx
	movq	%r15, %rdi
	call	*336(%rax)
	cmpl	%r12d, %r13d
	jge	.L744
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L786:
	movq	-104(%rbp), %rax
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	je	.L859
.L787:
	movslq	12(%rcx), %rsi
	testl	%esi, %esi
	je	.L792
	movq	(%rcx), %rax
	jle	.L792
	leal	-1(%rsi), %edx
	leaq	4(%rax,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L793:
	cmpl	(%rax), %r12d
	je	.L774
	addq	$4, %rax
	cmpq	%rax, %rdx
	jne	.L793
.L792:
	movl	8(%rcx), %eax
	cmpl	%eax, %esi
	jl	.L852
	leal	1(%rax,%rax), %eax
	movl	%eax, -72(%rbp)
	cltq
	leaq	7(,%rax,4), %rsi
	movq	16(%rbp), %rax
	andq	$-8, %rsi
	movq	16(%rax), %rdi
	movq	24(%rax), %rax
	movq	%rax, -80(%rbp)
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L860
	movq	16(%rbp), %rax
	addq	%rdi, %rsi
	movq	%rsi, 16(%rax)
.L796:
	movslq	12(%rcx), %rdx
	movq	%rdx, %rax
	salq	$2, %rdx
	testl	%eax, %eax
	jle	.L797
	movq	(%rcx), %rsi
	movl	%r9d, -108(%rbp)
	movb	%r10b, -93(%rbp)
	movl	%r8d, -56(%rbp)
	movq	%rcx, -80(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %rcx
	movl	-108(%rbp), %r9d
	movzbl	-93(%rbp), %r10d
	movl	-56(%rbp), %r8d
	movq	%rax, %rdi
	movslq	12(%rcx), %rdx
	movq	%rdx, %rax
	salq	$2, %rdx
.L797:
	movl	-72(%rbp), %esi
	addl	$1, %eax
	movq	%rdi, (%rcx)
	movl	%eax, 12(%rcx)
	movl	%esi, 8(%rcx)
	movl	%r12d, (%rdi,%rdx)
	jmp	.L774
.L739:
	movq	%rsi, %rdi
.L851:
	call	*%rax
	jmp	.L743
.L800:
	testl	%r9d, %r9d
	je	.L747
	movq	(%r15), %rax
	movl	%r12d, %esi
	addl	$1, %r12d
	movl	%r9d, %edx
	movq	%r15, %rdi
	call	*48(%rax)
	cmpl	%r12d, %r13d
	jge	.L744
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L782:
	leal	1(%rax,%rax), %r11d
	movslq	%r11d, %rax
	leaq	7(,%rax,4), %rsi
	movq	16(%rbp), %rax
	andq	$-8, %rsi
	movq	16(%rax), %rdi
	movq	24(%rax), %rax
	movq	%rax, -72(%rbp)
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L861
	movq	16(%rbp), %rax
	addq	%rdi, %rsi
	movq	%rsi, 16(%rax)
.L784:
	movslq	12(%rcx), %rdx
	movq	%rdx, %rax
	salq	$2, %rdx
	testl	%eax, %eax
	jg	.L862
.L785:
	addl	$1, %eax
	movq	%rdi, (%rcx)
	movl	%r11d, 8(%rcx)
	movl	%eax, 12(%rcx)
	movl	%r12d, (%rdi,%rdx)
	jmp	.L774
.L858:
	movq	16(%rbp), %rax
	movq	16(%rax), %rdx
	movq	24(%rax), %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$15, %rcx
	jbe	.L863
	movq	16(%rbp), %rdi
	leaq	16(%rdx), %rcx
	movq	%rcx, 16(%rdi)
.L777:
	movq	16(%rdi), %rsi
	movq	%rdx, %rcx
	subq	%rsi, %rax
	cmpq	$7, %rax
	jbe	.L864
	leaq	8(%rsi), %rax
	movq	%rax, 16(%rdi)
.L779:
	movq	-88(%rbp), %rax
	movq	%rsi, (%rdx)
	movq	$1, 8(%rdx)
	movq	%rdx, 8(%rax)
	jmp	.L775
.L862:
	movq	(%rcx), %rsi
	movl	%r9d, -108(%rbp)
	movb	%r10b, -93(%rbp)
	movl	%r8d, -56(%rbp)
	movl	%r11d, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %rcx
	movl	-108(%rbp), %r9d
	movq	%rax, %rdi
	movzbl	-93(%rbp), %r10d
	movl	-56(%rbp), %r8d
	movslq	12(%rcx), %rdx
	movl	-80(%rbp), %r11d
	movq	%rdx, %rax
	salq	$2, %rdx
	jmp	.L785
.L859:
	movq	16(%rbp), %rax
	movq	16(%rax), %rdx
	movq	24(%rax), %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$15, %rcx
	jbe	.L865
	movq	16(%rbp), %rdi
	leaq	16(%rdx), %rcx
	movq	%rcx, 16(%rdi)
.L789:
	movq	16(%rdi), %rsi
	movq	%rdx, %rcx
	subq	%rsi, %rax
	cmpq	$7, %rax
	jbe	.L866
	leaq	8(%rsi), %rax
	movq	%rax, 16(%rdi)
.L791:
	movq	-104(%rbp), %rax
	movq	%rsi, (%rdx)
	movq	$1, 8(%rdx)
	movq	%rdx, 8(%rax)
	jmp	.L787
.L863:
	movq	16(%rbp), %rdi
	movl	$16, %esi
	movl	%r9d, -56(%rbp)
	movb	%r10b, -80(%rbp)
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-72(%rbp), %r8d
	movzbl	-80(%rbp), %r10d
	movq	%rax, %rdx
	movq	16(%rbp), %rax
	movl	-56(%rbp), %r9d
	movq	16(%rbp), %rdi
	movq	24(%rax), %rax
	jmp	.L777
.L864:
	movl	$8, %esi
	movl	%r9d, -108(%rbp)
	movb	%r10b, -93(%rbp)
	movl	%r8d, -56(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movl	-56(%rbp), %r8d
	movzbl	-93(%rbp), %r10d
	movq	%rax, %rsi
	movl	-108(%rbp), %r9d
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L756:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L861:
	movq	16(%rbp), %rdi
	movl	%r9d, -108(%rbp)
	movb	%r10b, -93(%rbp)
	movl	%r8d, -56(%rbp)
	movl	%r11d, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rcx
	movl	-80(%rbp), %r11d
	movl	-56(%rbp), %r8d
	movzbl	-93(%rbp), %r10d
	movq	%rax, %rdi
	movl	-108(%rbp), %r9d
	jmp	.L784
.L860:
	movq	16(%rbp), %rdi
	movl	%r9d, -108(%rbp)
	movb	%r10b, -93(%rbp)
	movl	%r8d, -56(%rbp)
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %rcx
	movl	-56(%rbp), %r8d
	movzbl	-93(%rbp), %r10d
	movl	-108(%rbp), %r9d
	movq	%rax, %rdi
	jmp	.L796
.L866:
	movl	$8, %esi
	movl	%r9d, -108(%rbp)
	movb	%r10b, -93(%rbp)
	movl	%r8d, -56(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movl	-56(%rbp), %r8d
	movzbl	-93(%rbp), %r10d
	movq	%rax, %rsi
	movl	-108(%rbp), %r9d
	jmp	.L791
.L865:
	movq	16(%rbp), %rdi
	movl	$16, %esi
	movl	%r9d, -56(%rbp)
	movb	%r10b, -80(%rbp)
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-72(%rbp), %r8d
	movzbl	-80(%rbp), %r10d
	movq	%rax, %rdx
	movq	16(%rbp), %rax
	movl	-56(%rbp), %r9d
	movq	16(%rbp), %rdi
	movq	24(%rax), %rax
	jmp	.L789
	.cfi_endproc
.LFE20357:
	.size	_ZN2v88internal5Trace22PerformDeferredActionsEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetEPS4_S7_PNS0_4ZoneE, .-_ZN2v88internal5Trace22PerformDeferredActionsEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetEPS4_S7_PNS0_4ZoneE
	.section	.text._ZN2v88internal18GuardedAlternative8AddGuardEPNS0_5GuardEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18GuardedAlternative8AddGuardEPNS0_5GuardEPNS0_4ZoneE
	.type	_ZN2v88internal18GuardedAlternative8AddGuardEPNS0_5GuardEPNS0_4ZoneE, @function
_ZN2v88internal18GuardedAlternative8AddGuardEPNS0_5GuardEPNS0_4ZoneE:
.LFB20364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L879
.L868:
	movslq	12(%rbx), %rax
	movl	8(%rbx), %edx
	cmpl	%edx, %eax
	jge	.L873
	movq	(%rbx), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 12(%rbx)
	movq	%r14, (%rdx,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L873:
	.cfi_restore_state
	movq	16(%r12), %rcx
	movq	24(%r12), %rax
	leal	1(%rdx,%rdx), %r13d
	movslq	%r13d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L880
	addq	%rcx, %rsi
	movq	%rsi, 16(%r12)
.L876:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L881
.L877:
	addl	$1, %eax
	movq	%rcx, (%rbx)
	movl	%r13d, 8(%rbx)
	movl	%eax, 12(%rbx)
	movq	%r14, (%rcx,%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L881:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L879:
	movq	16(%rdx), %r15
	movq	24(%rdx), %rdx
	movq	%rdi, %r13
	movq	%rdx, %rax
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L882
	leaq	16(%r15), %rax
	movq	%rax, 16(%r12)
.L870:
	subq	%rax, %rdx
	movq	%r15, %rbx
	cmpq	$7, %rdx
	jbe	.L883
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r12)
.L872:
	movq	%rax, (%r15)
	movq	$1, 8(%r15)
	movq	%r15, 8(%r13)
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L880:
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L883:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L882:
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r12), %rdx
	movq	%rax, %r15
	movq	16(%r12), %rax
	jmp	.L870
	.cfi_endproc
.LFE20364:
	.size	_ZN2v88internal18GuardedAlternative8AddGuardEPNS0_5GuardEPNS0_4ZoneE, .-_ZN2v88internal18GuardedAlternative8AddGuardEPNS0_5GuardEPNS0_4ZoneE
	.section	.text._ZN2v88internal10ActionNode18SetRegisterForLoopEiiPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ActionNode18SetRegisterForLoopEiiPNS0_10RegExpNodeE
	.type	_ZN2v88internal10ActionNode18SetRegisterForLoopEiiPNS0_10RegExpNodeE, @function
_ZN2v88internal10ActionNode18SetRegisterForLoopEiiPNS0_10RegExpNodeE:
.LFB20365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	48(%rdx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$87, %rdx
	jbe	.L888
	leaq	88(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L886:
	movq	48(%rbx), %xmm0
	movq	%rbx, %xmm2
	pxor	%xmm1, %xmm1
	leaq	16+_ZTVN2v88internal10ActionNodeE(%rip), %rcx
	movq	$0, 8(%rax)
	punpcklqdq	%xmm2, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rcx, (%rax)
	movl	$0, 80(%rax)
	movl	%r13d, 64(%rax)
	movl	%r12d, 68(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L888:
	.cfi_restore_state
	movl	$88, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L886
	.cfi_endproc
.LFE20365:
	.size	_ZN2v88internal10ActionNode18SetRegisterForLoopEiiPNS0_10RegExpNodeE, .-_ZN2v88internal10ActionNode18SetRegisterForLoopEiiPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal10ActionNode17IncrementRegisterEiPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ActionNode17IncrementRegisterEiPNS0_10RegExpNodeE
	.type	_ZN2v88internal10ActionNode17IncrementRegisterEiPNS0_10RegExpNodeE, @function
_ZN2v88internal10ActionNode17IncrementRegisterEiPNS0_10RegExpNodeE:
.LFB20366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	48(%rsi), %rdi
	movq	%rsi, %rbx
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$87, %rdx
	jbe	.L893
	leaq	88(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L891:
	movq	48(%rbx), %xmm0
	movq	%rbx, %xmm2
	pxor	%xmm1, %xmm1
	leaq	16+_ZTVN2v88internal10ActionNodeE(%rip), %rcx
	movq	$0, 8(%rax)
	punpcklqdq	%xmm2, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rcx, (%rax)
	movl	$1, 80(%rax)
	movl	%r12d, 64(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L893:
	.cfi_restore_state
	movl	$88, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L891
	.cfi_endproc
.LFE20366:
	.size	_ZN2v88internal10ActionNode17IncrementRegisterEiPNS0_10RegExpNodeE, .-_ZN2v88internal10ActionNode17IncrementRegisterEiPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal10ActionNode13StorePositionEibPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ActionNode13StorePositionEibPNS0_10RegExpNodeE
	.type	_ZN2v88internal10ActionNode13StorePositionEibPNS0_10RegExpNodeE, @function
_ZN2v88internal10ActionNode13StorePositionEibPNS0_10RegExpNodeE:
.LFB20367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	48(%rdx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$87, %rdx
	jbe	.L898
	leaq	88(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L896:
	movq	48(%rbx), %xmm0
	movq	%rbx, %xmm2
	pxor	%xmm1, %xmm1
	leaq	16+_ZTVN2v88internal10ActionNodeE(%rip), %rcx
	movq	$0, 8(%rax)
	punpcklqdq	%xmm2, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rcx, (%rax)
	movl	$2, 80(%rax)
	movl	%r13d, 64(%rax)
	movb	%r12b, 68(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L898:
	.cfi_restore_state
	movl	$88, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L896
	.cfi_endproc
.LFE20367:
	.size	_ZN2v88internal10ActionNode13StorePositionEibPNS0_10RegExpNodeE, .-_ZN2v88internal10ActionNode13StorePositionEibPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal10ActionNode13ClearCapturesENS0_8IntervalEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ActionNode13ClearCapturesENS0_8IntervalEPNS0_10RegExpNodeE
	.type	_ZN2v88internal10ActionNode13ClearCapturesENS0_8IntervalEPNS0_10RegExpNodeE, @function
_ZN2v88internal10ActionNode13ClearCapturesENS0_8IntervalEPNS0_10RegExpNodeE:
.LFB20368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	48(%rsi), %rdi
	movq	%rsi, %rbx
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$87, %rdx
	jbe	.L903
	leaq	88(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L901:
	movq	48(%rbx), %xmm0
	movq	%rbx, %xmm2
	pxor	%xmm1, %xmm1
	leaq	16+_ZTVN2v88internal10ActionNodeE(%rip), %rcx
	movq	$0, 8(%rax)
	punpcklqdq	%xmm2, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rcx, (%rax)
	movl	$6, 80(%rax)
	movq	%r12, 64(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L903:
	.cfi_restore_state
	movl	$88, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L901
	.cfi_endproc
.LFE20368:
	.size	_ZN2v88internal10ActionNode13ClearCapturesENS0_8IntervalEPNS0_10RegExpNodeE, .-_ZN2v88internal10ActionNode13ClearCapturesENS0_8IntervalEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE
	.type	_ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE, @function
_ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE:
.LFB20369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	48(%rdx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$87, %rdx
	jbe	.L908
	leaq	88(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L906:
	movq	48(%rbx), %xmm0
	movq	%rbx, %xmm2
	pxor	%xmm1, %xmm1
	leaq	16+_ZTVN2v88internal10ActionNodeE(%rip), %rcx
	movq	$0, 8(%rax)
	punpcklqdq	%xmm2, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rcx, (%rax)
	movl	$3, 80(%rax)
	movl	%r13d, 64(%rax)
	movl	%r12d, 68(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L908:
	.cfi_restore_state
	movl	$88, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L906
	.cfi_endproc
.LFE20369:
	.size	_ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE, .-_ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal10ActionNode23PositiveSubmatchSuccessEiiiiPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ActionNode23PositiveSubmatchSuccessEiiiiPNS0_10RegExpNodeE
	.type	_ZN2v88internal10ActionNode23PositiveSubmatchSuccessEiiiiPNS0_10RegExpNodeE, @function
_ZN2v88internal10ActionNode23PositiveSubmatchSuccessEiiiiPNS0_10RegExpNodeE:
.LFB20370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -4(%rbp)
	movq	48(%r8), %rdi
	movl	%edx, -12(%rbp)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	movl	%esi, -8(%rbp)
	movl	%ecx, -16(%rbp)
	subq	%rax, %rdx
	cmpq	$87, %rdx
	jbe	.L913
	leaq	88(%rax), %rdx
	movd	%ecx, %xmm3
	movd	%esi, %xmm4
	movq	%rdx, 16(%rdi)
.L911:
	movq	48(%r8), %xmm0
	movq	%r8, %xmm2
	pxor	%xmm1, %xmm1
	leaq	16+_ZTVN2v88internal10ActionNodeE(%rip), %rcx
	movups	%xmm1, 32(%rax)
	movd	-12(%rbp), %xmm1
	punpcklqdq	%xmm2, %xmm0
	movq	$0, 8(%rax)
	movups	%xmm0, 48(%rax)
	movd	-4(%rbp), %xmm0
	punpckldq	%xmm3, %xmm1
	movq	$0, 16(%rax)
	punpckldq	%xmm4, %xmm0
	movq	$0, 24(%rax)
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, (%rax)
	movl	$4, 80(%rax)
	movups	%xmm0, 64(%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L913:
	.cfi_restore_state
	movl	$88, %esi
	movq	%r8, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-24(%rbp), %r8
	movd	-16(%rbp), %xmm3
	movd	-8(%rbp), %xmm4
	jmp	.L911
	.cfi_endproc
.LFE20370:
	.size	_ZN2v88internal10ActionNode23PositiveSubmatchSuccessEiiiiPNS0_10RegExpNodeE, .-_ZN2v88internal10ActionNode23PositiveSubmatchSuccessEiiiiPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal10ActionNode15EmptyMatchCheckEiiiPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ActionNode15EmptyMatchCheckEiiiPNS0_10RegExpNodeE
	.type	_ZN2v88internal10ActionNode15EmptyMatchCheckEiiiPNS0_10RegExpNodeE, @function
_ZN2v88internal10ActionNode15EmptyMatchCheckEiiiPNS0_10RegExpNodeE:
.LFB20371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	subq	$24, %rsp
	movq	48(%rcx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$87, %rdx
	jbe	.L918
	leaq	88(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L916:
	movq	48(%rcx), %xmm0
	movq	%rcx, %xmm2
	pxor	%xmm1, %xmm1
	leaq	16+_ZTVN2v88internal10ActionNodeE(%rip), %rsi
	movq	$0, 8(%rax)
	punpcklqdq	%xmm2, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rsi, (%rax)
	movl	$5, 80(%rax)
	movl	%r14d, 64(%rax)
	movl	%r13d, 68(%rax)
	movl	%r12d, 72(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L918:
	.cfi_restore_state
	movl	$88, %esi
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rcx
	jmp	.L916
	.cfi_endproc
.LFE20371:
	.size	_ZN2v88internal10ActionNode15EmptyMatchCheckEiiiPNS0_10RegExpNodeE, .-_ZN2v88internal10ActionNode15EmptyMatchCheckEiiiPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal10ChoiceNode13GenerateGuardEPNS0_20RegExpMacroAssemblerEPNS0_5GuardEPNS0_5TraceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode13GenerateGuardEPNS0_20RegExpMacroAssemblerEPNS0_5GuardEPNS0_5TraceE
	.type	_ZN2v88internal10ChoiceNode13GenerateGuardEPNS0_20RegExpMacroAssemblerEPNS0_5GuardEPNS0_5TraceE, @function
_ZN2v88internal10ChoiceNode13GenerateGuardEPNS0_20RegExpMacroAssemblerEPNS0_5GuardEPNS0_5TraceE:
.LFB20380:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	movl	4(%rdx), %edx
	movq	%rsi, %rdi
	testl	%edx, %edx
	je	.L920
	cmpl	$1, %edx
	jne	.L924
	movq	(%rsi), %rsi
	movq	16(%rcx), %rcx
	movl	8(%rax), %edx
	movq	240(%rsi), %r8
	movl	(%rax), %esi
	jmp	*%r8
	.p2align 4,,10
	.p2align 3
.L924:
	ret
	.p2align 4,,10
	.p2align 3
.L920:
	movq	(%rsi), %rsi
	movq	16(%rcx), %rcx
	movl	8(%rax), %edx
	movq	232(%rsi), %r8
	movl	(%rax), %esi
	jmp	*%r8
	.cfi_endproc
.LFE20380:
	.size	_ZN2v88internal10ChoiceNode13GenerateGuardEPNS0_20RegExpMacroAssemblerEPNS0_5GuardEPNS0_5TraceE, .-_ZN2v88internal10ChoiceNode13GenerateGuardEPNS0_20RegExpMacroAssemblerEPNS0_5GuardEPNS0_5TraceE
	.section	.text._ZN2v88internal10RegExpNodeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpNodeD2Ev
	.type	_ZN2v88internal10RegExpNodeD2Ev, @function
_ZN2v88internal10RegExpNodeD2Ev:
.LFB20394:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE20394:
	.size	_ZN2v88internal10RegExpNodeD2Ev, .-_ZN2v88internal10RegExpNodeD2Ev
	.globl	_ZN2v88internal10RegExpNodeD1Ev
	.set	_ZN2v88internal10RegExpNodeD1Ev,_ZN2v88internal10RegExpNodeD2Ev
	.section	.text._ZN2v88internal10RegExpNodeD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpNodeD0Ev
	.type	_ZN2v88internal10RegExpNodeD0Ev, @function
_ZN2v88internal10RegExpNodeD0Ev:
.LFB20396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE20396:
	.size	_ZN2v88internal10RegExpNodeD0Ev, .-_ZN2v88internal10RegExpNodeD0Ev
	.section	.text._ZN2v88internal10RegExpNode13KeepRecursingEPNS0_14RegExpCompilerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpNode13KeepRecursingEPNS0_14RegExpCompilerE
	.type	_ZN2v88internal10RegExpNode13KeepRecursingEPNS0_14RegExpCompilerE, @function
_ZN2v88internal10RegExpNode13KeepRecursingEPNS0_14RegExpCompilerE:
.LFB20398:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$0, 50(%rsi)
	jne	.L928
	cmpl	$100, 32(%rsi)
	setle	%al
.L928:
	ret
	.cfi_endproc
.LFE20398:
	.size	_ZN2v88internal10RegExpNode13KeepRecursingEPNS0_14RegExpCompilerE, .-_ZN2v88internal10RegExpNode13KeepRecursingEPNS0_14RegExpCompilerE
	.section	.text._ZN2v88internal17QuickCheckDetails11RationalizeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17QuickCheckDetails11RationalizeEb
	.type	_ZN2v88internal17QuickCheckDetails11RationalizeEb, @function
_ZN2v88internal17QuickCheckDetails11RationalizeEb:
.LFB20404:
	.cfi_startproc
	endbr64
	cmpb	$1, %sil
	movl	(%rdi), %r11d
	movq	$0, 28(%rdi)
	sbbl	%eax, %eax
	andl	$65280, %eax
	addl	$255, %eax
	testl	%r11d, %r11d
	jle	.L939
	cmpb	$1, %sil
	movzwl	4(%rdi), %edx
	movzwl	6(%rdi), %r8d
	sbbl	%esi, %esi
	andl	$8, %esi
	addl	$8, %esi
	testb	%dl, %dl
	setne	%r10b
	andl	%eax, %edx
	andl	%eax, %r8d
	cmpl	$1, %r11d
	je	.L953
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	10(%rdi), %r9d
	movl	%esi, %ecx
	testb	%r9b, %r9b
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	$1, %ebx
	cmovne	%ebx, %r10d
	andl	%eax, %r9d
	sall	%cl, %r9d
	orl	%r9d, %edx
	movzwl	12(%rdi), %r9d
	andl	%eax, %r9d
	sall	%cl, %r9d
	leal	(%rsi,%rsi), %ecx
	orl	%r9d, %r8d
	cmpl	$2, %r11d
	je	.L934
	movzwl	16(%rdi), %r9d
	testb	%r9b, %r9b
	cmovne	%ebx, %r10d
	andl	%eax, %r9d
	sall	%cl, %r9d
	orl	%r9d, %edx
	movzwl	18(%rdi), %r9d
	andl	%eax, %r9d
	sall	%cl, %r9d
	addl	%esi, %ecx
	orl	%r9d, %r8d
	cmpl	$3, %r11d
	je	.L934
	movzwl	22(%rdi), %esi
	testb	%sil, %sil
	cmovne	%ebx, %r10d
	andl	%eax, %esi
	sall	%cl, %esi
	orl	%esi, %edx
	movzwl	24(%rdi), %esi
	andl	%esi, %eax
	sall	%cl, %eax
	orl	%eax, %r8d
.L934:
	movl	%r10d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%edx, 28(%rdi)
	movl	%r8d, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L939:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%r10d, %r10d
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L953:
	movl	%edx, 28(%rdi)
	movl	%r10d, %eax
	movl	%r8d, 32(%rdi)
	ret
	.cfi_endproc
.LFE20404:
	.size	_ZN2v88internal17QuickCheckDetails11RationalizeEb, .-_ZN2v88internal17QuickCheckDetails11RationalizeEb
	.section	.text._ZN2v88internal10RegExpNode11EatsAtLeastEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpNode11EatsAtLeastEb
	.type	_ZN2v88internal10RegExpNode11EatsAtLeastEb, @function
_ZN2v88internal10RegExpNode11EatsAtLeastEb:
.LFB20405:
	.cfi_startproc
	endbr64
	testb	%sil, %sil
	je	.L958
	movzbl	27(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L958:
	movzbl	26(%rdi), %eax
	ret
	.cfi_endproc
.LFE20405:
	.size	_ZN2v88internal10RegExpNode11EatsAtLeastEb, .-_ZN2v88internal10RegExpNode11EatsAtLeastEb
	.section	.text._ZN2v88internal10RegExpNode14EmitQuickCheckEPNS0_14RegExpCompilerEPNS0_5TraceES5_bPNS0_5LabelEPNS0_17QuickCheckDetailsEbPNS0_10ChoiceNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpNode14EmitQuickCheckEPNS0_14RegExpCompilerEPNS0_5TraceES5_bPNS0_5LabelEPNS0_17QuickCheckDetailsEbPNS0_10ChoiceNodeE
	.type	_ZN2v88internal10RegExpNode14EmitQuickCheckEPNS0_14RegExpCompilerEPNS0_5TraceES5_bPNS0_5LabelEPNS0_17QuickCheckDetailsEbPNS0_10ChoiceNodeE, @function
_ZN2v88internal10RegExpNode14EmitQuickCheckEPNS0_14RegExpCompilerEPNS0_5TraceES5_bPNS0_5LabelEPNS0_17QuickCheckDetailsEbPNS0_10ChoiceNodeE:
.LFB20409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movl	24(%rbp), %r15d
	movl	%r8d, -56(%rbp)
	movq	%r9, -64(%rbp)
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	je	.L963
	movq	%rcx, %r13
	movl	92(%rcx), %ecx
	movq	(%rdi), %rax
	movq	%rsi, %r12
	movq	%rdx, %r14
	movq	%rsi, %rdx
	movq	%rbx, %rsi
	testl	%ecx, %ecx
	sete	%r8b
	xorl	%ecx, %ecx
	movzbl	%r8b, %r8d
	call	*40(%rax)
	cmpb	$0, 36(%rbx)
	je	.L1005
.L963:
	xorl	%eax, %eax
.L960:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1005:
	.cfi_restore_state
	movzbl	48(%r12), %eax
	movl	(%rbx), %r8d
	movq	$0, 28(%rbx)
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$65280, %edx
	addl	$255, %edx
	testl	%r8d, %r8d
	jle	.L963
	cmpb	$1, %al
	movzwl	4(%rbx), %r10d
	movzwl	6(%rbx), %r11d
	sbbl	%esi, %esi
	andl	$8, %esi
	addl	$8, %esi
	testb	%r10b, %r10b
	setne	%al
	andl	%edx, %r10d
	andl	%edx, %r11d
	cmpl	$1, %r8d
	je	.L965
	movzwl	10(%rbx), %r9d
	movl	$1, %edi
	movl	%esi, %ecx
	testb	%r9b, %r9b
	cmovne	%edi, %eax
	andl	%edx, %r9d
	sall	%cl, %r9d
	orl	%r9d, %r10d
	movzwl	12(%rbx), %r9d
	andl	%edx, %r9d
	sall	%cl, %r9d
	leal	(%rsi,%rsi), %ecx
	orl	%r9d, %r11d
	cmpl	$2, %r8d
	je	.L965
	movzwl	16(%rbx), %r9d
	testb	%r9b, %r9b
	cmovne	%edi, %eax
	andl	%edx, %r9d
	sall	%cl, %r9d
	orl	%r9d, %r10d
	movzwl	18(%rbx), %r9d
	andl	%edx, %r9d
	sall	%cl, %r9d
	addl	%esi, %ecx
	orl	%r9d, %r11d
	cmpl	$3, %r8d
	je	.L965
	movzwl	22(%rbx), %r9d
	movzwl	24(%rbx), %esi
	testb	%r9b, %r9b
	cmovne	%edi, %eax
	andl	%edx, %r9d
	andl	%esi, %edx
	sall	%cl, %r9d
	sall	%cl, %edx
	orl	%r9d, %r10d
	orl	%edx, %r11d
.L965:
	movl	%r10d, 28(%rbx)
	movl	%r11d, 32(%rbx)
	testb	%al, %al
	je	.L963
	movq	40(%r12), %rdi
	cmpl	%r8d, 40(%r13)
	je	.L969
	movl	92(%r14), %edx
	movq	32(%rbp), %rsi
	testl	%edx, %edx
	jne	.L970
	movzbl	27(%rsi), %r9d
.L971:
	movzbl	-56(%rbp), %ecx
	movq	16(%r14), %rdx
	movb	%al, -73(%rbp)
	movl	0(%r13), %esi
	movl	%r10d, -72(%rbp)
	xorl	$1, %ecx
	movl	%r11d, -68(%rbp)
	movzbl	%cl, %ecx
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii@PLT
	movl	(%rbx), %r8d
	movzbl	-73(%rbp), %eax
	movl	-72(%rbp), %r10d
	movl	-68(%rbp), %r11d
	movq	-56(%rbp), %rdi
.L969:
	cmpl	$1, %r8d
	je	.L1006
	cmpl	$2, %r8d
	je	.L1007
.L977:
	cmpl	$-1, %r10d
	je	.L974
.L1002:
	movb	%al, -56(%rbp)
	testb	%r15b, %r15b
	je	.L976
	movq	(%rdi), %r8
	movq	-64(%rbp), %rcx
	movl	%r10d, %edx
	movl	%r11d, %esi
	call	*80(%r8)
	movzbl	-56(%rbp), %eax
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L1006:
	cmpb	$1, 48(%r12)
	sbbl	%edx, %edx
	andl	$65280, %edx
	addl	$255, %edx
	andl	%edx, %r10d
	cmpl	%r10d, %edx
	jne	.L1002
.L974:
	testb	%r15b, %r15b
	je	.L1008
	movq	(%rdi), %rax
	movq	-64(%rbp), %rdx
	movl	%r11d, %esi
	call	*72(%rax)
	movl	%r15d, %eax
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	(%rdi), %rcx
	movq	16(%r13), %rdx
	movb	%al, -56(%rbp)
	movl	%r11d, %esi
	call	*144(%rcx)
	movzbl	-56(%rbp), %eax
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L970:
	movzbl	26(%rsi), %r9d
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L976:
	movq	(%rdi), %r8
	movq	16(%r13), %rcx
	movl	%r10d, %edx
	movl	%r11d, %esi
	call	*152(%r8)
	movzbl	-56(%rbp), %eax
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L1007:
	cmpb	$0, 48(%r12)
	je	.L977
	movzwl	%r10w, %edx
	cmpl	$65535, %edx
	jne	.L1002
	jmp	.L974
	.cfi_endproc
.LFE20409:
	.size	_ZN2v88internal10RegExpNode14EmitQuickCheckEPNS0_14RegExpCompilerEPNS0_5TraceES5_bPNS0_5LabelEPNS0_17QuickCheckDetailsEbPNS0_10ChoiceNodeE, .-_ZN2v88internal10RegExpNode14EmitQuickCheckEPNS0_14RegExpCompilerEPNS0_5TraceES5_bPNS0_5LabelEPNS0_17QuickCheckDetailsEbPNS0_10ChoiceNodeE
	.section	.text._ZN2v88internal17QuickCheckDetails5ClearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17QuickCheckDetails5ClearEv
	.type	_ZN2v88internal17QuickCheckDetails5ClearEv, @function
_ZN2v88internal17QuickCheckDetails5ClearEv:
.LFB20411:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L1010
	movl	$0, 4(%rdi)
	movb	$0, 8(%rdi)
	cmpl	$1, %eax
	je	.L1010
	movl	$0, 10(%rdi)
	movb	$0, 14(%rdi)
	cmpl	$2, %eax
	je	.L1010
	movl	$0, 16(%rdi)
	movb	$0, 20(%rdi)
	cmpl	$3, %eax
	je	.L1010
	movl	$0, 22(%rdi)
	movb	$0, 26(%rdi)
.L1010:
	movl	$0, (%rdi)
	ret
	.cfi_endproc
.LFE20411:
	.size	_ZN2v88internal17QuickCheckDetails5ClearEv, .-_ZN2v88internal17QuickCheckDetails5ClearEv
	.section	.text._ZN2v88internal17QuickCheckDetails7AdvanceEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17QuickCheckDetails7AdvanceEib
	.type	_ZN2v88internal17QuickCheckDetails7AdvanceEib, @function
_ZN2v88internal17QuickCheckDetails7AdvanceEib:
.LFB20412:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%esi, %esi
	js	.L1029
	cmpl	%esi, %eax
	jle	.L1029
	movslq	%esi, %rdx
	leaq	4(%rdi), %rax
	leaq	(%rdx,%rdx,2), %r8
	xorl	%edx, %edx
	addq	%r8, %r8
	.p2align 4,,10
	.p2align 3
.L1025:
	movl	(%rax,%r8), %ecx
	addl	$1, %edx
	addq	$6, %rax
	movl	%ecx, -6(%rax)
	movzbl	-2(%rax,%r8), %ecx
	movb	%cl, -2(%rax)
	movl	(%rdi), %r9d
	movl	%r9d, %ecx
	subl	%esi, %ecx
	cmpl	%edx, %ecx
	jg	.L1025
	cmpl	%ecx, %r9d
	jle	.L1026
	movslq	%ecx, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,2), %rax
	movl	$0, 4(%rax)
	movb	$0, 8(%rax)
	leal	1(%rcx), %eax
	cmpl	%eax, %r9d
	jle	.L1026
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,2), %rax
	movl	$0, 4(%rax)
	movb	$0, 8(%rax)
	leal	2(%rcx), %eax
	cmpl	%eax, %r9d
	jle	.L1026
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,2), %rax
	movl	$0, 4(%rax)
	movb	$0, 8(%rax)
	leal	3(%rcx), %eax
	cmpl	%eax, %r9d
	jle	.L1026
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,2), %rax
	movl	$0, 4(%rax)
	movb	$0, 8(%rax)
.L1026:
	movl	%ecx, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1029:
	testl	%eax, %eax
	jle	.L1023
	movl	$0, 4(%rdi)
	movb	$0, 8(%rdi)
	cmpl	$1, %eax
	je	.L1023
	movl	$0, 10(%rdi)
	movb	$0, 14(%rdi)
	cmpl	$2, %eax
	je	.L1023
	movl	$0, 16(%rdi)
	movb	$0, 20(%rdi)
	cmpl	$3, %eax
	je	.L1023
	movl	$0, 22(%rdi)
	movb	$0, 26(%rdi)
.L1023:
	movl	$0, (%rdi)
	ret
	.cfi_endproc
.LFE20412:
	.size	_ZN2v88internal17QuickCheckDetails7AdvanceEib, .-_ZN2v88internal17QuickCheckDetails7AdvanceEib
	.section	.text._ZN2v88internal17QuickCheckDetails5MergeEPS1_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17QuickCheckDetails5MergeEPS1_i
	.type	_ZN2v88internal17QuickCheckDetails5MergeEPS1_i, @function
_ZN2v88internal17QuickCheckDetails5MergeEPS1_i:
.LFB20413:
	.cfi_startproc
	endbr64
	cmpb	$0, 36(%rsi)
	jne	.L1040
	cmpb	$0, 36(%rdi)
	jne	.L1048
	cmpl	(%rdi), %edx
	jge	.L1040
	movslq	%edx, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	4(%rax,%rax), %rax
	leaq	(%rdi,%rax), %rcx
	addq	%rax, %rsi
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1045:
	movb	$0, 4(%rcx)
	movzwl	(%rsi), %r9d
.L1046:
	andl	%r9d, %eax
	addl	$1, %edx
	addq	$6, %rcx
	addq	$6, %rsi
	andl	%eax, %r8d
	movw	%ax, -6(%rcx)
	movw	%r8w, -4(%rcx)
	andw	-4(%rsi), %ax
	movw	%ax, -4(%rsi)
	movzwl	-4(%rcx), %r8d
	xorl	%r8d, %eax
	notl	%eax
	andw	-6(%rcx), %ax
	movw	%ax, -6(%rcx)
	andl	%r8d, %eax
	movw	%ax, -4(%rcx)
	cmpl	(%rdi), %edx
	jge	.L1040
.L1047:
	movzwl	(%rcx), %eax
	movzwl	(%rsi), %r9d
	movzwl	2(%rcx), %r8d
	cmpw	%r9w, %ax
	jne	.L1045
	cmpw	2(%rsi), %r8w
	jne	.L1045
	cmpb	$0, 4(%rsi)
	jne	.L1046
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1040:
	ret
	.p2align 4,,10
	.p2align 3
.L1048:
	movdqu	(%rsi), %xmm0
	movups	%xmm0, (%rdi)
	movdqu	16(%rsi), %xmm1
	movups	%xmm1, 16(%rdi)
	movl	32(%rsi), %eax
	movl	%eax, 32(%rdi)
	movzbl	36(%rsi), %eax
	movb	%al, 36(%rdi)
	ret
	.cfi_endproc
.LFE20413:
	.size	_ZN2v88internal17QuickCheckDetails5MergeEPS1_i, .-_ZN2v88internal17QuickCheckDetails5MergeEPS1_i
	.section	.text._ZN2v88internal10ChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.type	_ZN2v88internal10ChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, @function
_ZN2v88internal10ChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib:
.LFB20444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r8d, %eax
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%rdx, -112(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testb	%al, %al
	jne	.L1050
	movzbl	64(%rdi), %r8d
.L1050:
	movq	56(%r15), %rax
	movl	%r8d, -100(%rbp)
	movl	%r12d, %ecx
	movq	%rbx, %rsi
	movq	-112(%rbp), %rdx
	movl	12(%rax), %r13d
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	cmpl	$1, %r13d
	jle	.L1049
	leal	-2(%r13), %r14d
	movl	-100(%rbp), %r8d
	leaq	-96(%rbp), %r13
	addq	$2, %r14
	salq	$4, %r14
	movq	%r14, -120(%rbp)
	movl	$16, %r14d
	.p2align 4,,10
	.p2align 3
.L1053:
	movl	(%rbx), %edx
	movl	$0, -92(%rbp)
	movl	%r12d, %ecx
	movq	%r13, %rsi
	movb	$0, -88(%rbp)
	movl	%edx, -96(%rbp)
	movq	56(%r15), %rdx
	movl	$0, -86(%rbp)
	movb	$0, -82(%rbp)
	movl	$0, -80(%rbp)
	movb	$0, -76(%rbp)
	movl	$0, -74(%rbp)
	movb	$0, -70(%rbp)
	movq	$0, -68(%rbp)
	movb	$0, -60(%rbp)
	movq	(%rdx), %rdx
	movl	%r8d, -100(%rbp)
	movq	(%rdx,%r14), %rdi
	movq	-112(%rbp), %rdx
	addq	$16, %r14
	movq	(%rdi), %r9
	call	*40(%r9)
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17QuickCheckDetails5MergeEPS1_i
	cmpq	-120(%rbp), %r14
	movl	-100(%rbp), %r8d
	jne	.L1053
.L1049:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1058
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1058:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20444:
	.size	_ZN2v88internal10ChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, .-_ZN2v88internal10ChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.section	.text._ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib.part.0, @function
_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib.part.0:
.LFB25029:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	90(%rdi), %eax
	testb	%r8b, %r8b
	jne	.L1078
	cmpb	$0, 64(%rdi)
	je	.L1063
.L1078:
	testb	%al, %al
	je	.L1070
	movl	92(%rbx), %eax
	testl	%eax, %eax
	jle	.L1070
	movq	72(%rbx), %rdi
	movl	$1, %r8d
	movzbl	27(%rdi), %r10d
.L1064:
	movq	80(%rbx), %r9
	movzbl	27(%r9), %r9d
	cmpl	%r10d, %r9d
	jge	.L1061
	subl	$1, %eax
	movl	%eax, 92(%rbx)
	movq	(%rdi), %rax
	call	*40(%rax)
	addl	$1, 92(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1063:
	.cfi_restore_state
	testb	%al, %al
	je	.L1072
	movl	92(%rdi), %eax
	testl	%eax, %eax
	jle	.L1072
	movq	72(%rdi), %rdi
	xorl	%r8d, %r8d
	movzbl	26(%rdi), %r10d
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1070:
	movl	$1, %r8d
.L1061:
	orb	$64, 25(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal10ChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	andb	$-65, 25(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1072:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L1061
	.cfi_endproc
.LFE25029:
	.size	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib.part.0, .-_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib.part.0
	.section	.text._ZN2v88internal14LoopChoiceNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LoopChoiceNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.type	_ZN2v88internal14LoopChoiceNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, @function
_ZN2v88internal14LoopChoiceNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib:
.LFB20442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%r8b, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 90(%rdi)
	movq	(%rdi), %rax
	je	.L1080
	movq	40(%rax), %rax
	leaq	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib(%rip), %rdi
	cmpq	%rdi, %rax
	jne	.L1081
	cmpb	$0, 88(%r12)
	jne	.L1079
	testb	$64, 25(%r12)
	jne	.L1079
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib.part.0
	.p2align 4,,10
	.p2align 3
.L1080:
	.cfi_restore_state
	movb	$1, 90(%rdi)
	movq	40(%rax), %rax
	leaq	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib(%rip), %rdi
	cmpq	%rdi, %rax
	jne	.L1083
	cmpb	$0, 88(%r12)
	jne	.L1084
	testb	$64, 25(%r12)
	jne	.L1084
	movq	%r12, %rdi
	call	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib.part.0
.L1084:
	movb	$0, 90(%r12)
.L1079:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1083:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	movb	$0, 90(%r12)
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1081:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE20442:
	.size	_ZN2v88internal14LoopChoiceNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, .-_ZN2v88internal14LoopChoiceNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.section	.text._ZN2v88internal10ActionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ActionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.type	_ZN2v88internal10ActionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, @function
_ZN2v88internal10ActionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib:
.LFB20400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%r8b, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	56(%rdi), %r12
	movl	80(%rdi), %edi
	movq	(%r12), %rax
	testl	%edi, %edi
	jne	.L1087
	movq	48(%rax), %r9
	leaq	_ZN2v88internal14LoopChoiceNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib(%rip), %rdi
	cmpq	%rdi, %r9
	jne	.L1088
	cmpb	$0, 90(%r12)
	je	.L1089
	movq	40(%rax), %rax
	leaq	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib(%rip), %rdi
	cmpq	%rdi, %rax
	jne	.L1095
	cmpb	$0, 88(%r12)
	jne	.L1086
	testb	$64, 25(%r12)
	jne	.L1086
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib.part.0
	.p2align 4,,10
	.p2align 3
.L1087:
	.cfi_restore_state
	movq	40(%rax), %rax
.L1095:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1089:
	.cfi_restore_state
	movb	$1, 90(%r12)
	movq	40(%rax), %rax
	leaq	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib(%rip), %rdi
	cmpq	%rdi, %rax
	jne	.L1092
	cmpb	$0, 88(%r12)
	jne	.L1093
	testb	$64, 25(%r12)
	jne	.L1093
	movq	%r12, %rdi
	call	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib.part.0
.L1093:
	movb	$0, 90(%r12)
.L1086:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1088:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L1092:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1093
	.cfi_endproc
.LFE20400:
	.size	_ZN2v88internal10ActionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, .-_ZN2v88internal10ActionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.section	.text._ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.type	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, @function
_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib:
.LFB20441:
	.cfi_startproc
	endbr64
	cmpb	$0, 88(%rdi)
	jne	.L1111
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testb	$64, 25(%rdi)
	jne	.L1096
	movzbl	90(%rdi), %eax
	testb	%r8b, %r8b
	jne	.L1118
	cmpb	$0, 64(%rdi)
	je	.L1101
.L1118:
	testb	%al, %al
	je	.L1107
	movl	92(%rbx), %eax
	testl	%eax, %eax
	jle	.L1107
	movq	72(%rbx), %rdi
	movl	$1, %r8d
	movzbl	27(%rdi), %r9d
.L1102:
	movq	80(%rbx), %r10
	movzbl	27(%r10), %r10d
	cmpl	%r9d, %r10d
	jge	.L1099
	subl	$1, %eax
	movl	%eax, 92(%rbx)
	movq	(%rdi), %rax
	call	*40(%rax)
	addl	$1, 92(%rbx)
.L1096:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1111:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L1101:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	testb	%al, %al
	je	.L1109
	movl	92(%rdi), %eax
	testl	%eax, %eax
	jle	.L1109
	movq	72(%rdi), %rdi
	xorl	%r8d, %r8d
	movzbl	26(%rdi), %r9d
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1107:
	movl	$1, %r8d
.L1099:
	orb	$64, 25(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal10ChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	andb	$-65, 25(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1109:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L1099
	.cfi_endproc
.LFE20441:
	.size	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, .-_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.section	.text._ZN2v88internal13SeqRegExpNode15FilterSuccessorEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13SeqRegExpNode15FilterSuccessorEi
	.type	_ZN2v88internal13SeqRegExpNode15FilterSuccessorEi, @function
_ZN2v88internal13SeqRegExpNode15FilterSuccessorEi:
.LFB20433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	56(%rdi), %rdi
	movq	(%rdi), %rax
	call	*80(%rax)
	testq	%rax, %rax
	je	.L1120
	movq	%rax, 56(%rbx)
	movq	%rbx, %rax
.L1120:
	orb	$-128, 25(%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20433:
	.size	_ZN2v88internal13SeqRegExpNode15FilterSuccessorEi, .-_ZN2v88internal13SeqRegExpNode15FilterSuccessorEi
	.section	.text._ZN2v88internal30RangeContainsLatin1EquivalentsENS0_14CharacterRangeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30RangeContainsLatin1EquivalentsENS0_14CharacterRangeE
	.type	_ZN2v88internal30RangeContainsLatin1EquivalentsENS0_14CharacterRangeE, @function
_ZN2v88internal30RangeContainsLatin1EquivalentsENS0_14CharacterRangeE:
.LFB20434:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	sarq	$32, %rdx
	cmpl	$924, %edi
	jg	.L1126
	movl	$1, %eax
	cmpl	$923, %edx
	jle	.L1128
.L1125:
	ret
	.p2align 4,,10
	.p2align 3
.L1126:
	xorl	%eax, %eax
	cmpl	$956, %edi
	jg	.L1125
	cmpl	$955, %edx
	jle	.L1128
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1128:
	cmpl	$375, %edx
	setg	%al
	cmpl	$376, %edi
	setle	%dl
	andl	%edx, %eax
	ret
	.cfi_endproc
.LFE20434:
	.size	_ZN2v88internal30RangeContainsLatin1EquivalentsENS0_14CharacterRangeE, .-_ZN2v88internal30RangeContainsLatin1EquivalentsENS0_14CharacterRangeE
	.section	.text._ZN2v88internal13AssertionNode19BacktrackIfPreviousEPNS0_14RegExpCompilerEPNS0_5TraceENS1_10IfPreviousE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13AssertionNode19BacktrackIfPreviousEPNS0_14RegExpCompilerEPNS0_5TraceENS1_10IfPreviousE
	.type	_ZN2v88internal13AssertionNode19BacktrackIfPreviousEPNS0_14RegExpCompilerEPNS0_5TraceENS1_10IfPreviousE, @function
_ZN2v88internal13AssertionNode19BacktrackIfPreviousEPNS0_14RegExpCompilerEPNS0_5TraceENS1_10IfPreviousE:
.LFB20448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movdqu	(%rdx), %xmm0
	movq	40(%rsi), %r12
	movdqu	16(%rdx), %xmm1
	movdqu	32(%rdx), %xmm2
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movdqu	48(%rdx), %xmm3
	movdqu	64(%rdx), %xmm4
	movaps	%xmm0, -160(%rbp)
	movdqu	80(%rdx), %xmm5
	movaps	%xmm2, -128(%rbp)
	movl	-160(%rbp), %esi
	movaps	%xmm1, -144(%rbp)
	movq	-144(%rbp), %r10
	movl	$0, -120(%rbp)
	movq	$0, -168(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	testl	%ecx, %ecx
	jne	.L1133
	testl	%esi, %esi
	jle	.L1145
	movq	%r10, -184(%rbp)
	subl	$1, %esi
	movl	$1, %ecx
	movl	$-1, %r9d
	movl	$1, %r8d
	movq	%r10, %rdx
.L1143:
	movq	%r12, %rdi
	leaq	-168(%rbp), %r15
	call	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii@PLT
	movq	(%r12), %rax
	movq	%r15, %r8
	movq	-184(%rbp), %r10
	movl	$119, %esi
	movq	200(%rax), %rax
	movq	%r10, %r9
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	%r10, -184(%rbp)
	leaq	-168(%rbp), %r15
	testl	%esi, %esi
	jle	.L1137
	subl	$1, %esi
	movl	$-1, %r9d
	movl	$1, %r8d
	movl	$1, %ecx
.L1144:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii@PLT
	movq	(%r12), %rax
	movq	%r15, %r9
	movq	-184(%rbp), %r10
	movl	$87, %esi
	movq	200(%rax), %rax
	movq	%r10, %r8
.L1138:
	movq	%r9, -192(%rbp)
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r8, -184(%rbp)
	call	*%rax
	testb	%al, %al
	jne	.L1136
	movq	-192(%rbp), %r9
	movq	-184(%rbp), %r8
	xorl	%ecx, %ecx
	testl	%r14d, %r14d
	sete	%cl
	movq	%r12, %rdi
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal12_GLOBAL__N_113EmitWordCheckEPNS0_20RegExpMacroAssemblerEPNS0_5LabelES5_b.part.0
.L1136:
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*64(%rax)
	movq	56(%rbx), %rdi
	leaq	-160(%rbp), %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1146
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1137:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	*112(%rax)
	movl	-160(%rbp), %eax
	movl	$-1, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	leal	-1(%rax), %esi
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1145:
	movq	(%r12), %rax
	movq	%r10, %rdx
	movq	%r10, -184(%rbp)
	movq	%r12, %rdi
	call	*112(%rax)
	movl	-160(%rbp), %eax
	movq	-184(%rbp), %r10
	xorl	%ecx, %ecx
	movl	$-1, %r9d
	movl	$1, %r8d
	leal	-1(%rax), %esi
	movq	%r10, %rdx
	jmp	.L1143
.L1146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20448:
	.size	_ZN2v88internal13AssertionNode19BacktrackIfPreviousEPNS0_14RegExpCompilerEPNS0_5TraceENS1_10IfPreviousE, .-_ZN2v88internal13AssertionNode19BacktrackIfPreviousEPNS0_14RegExpCompilerEPNS0_5TraceENS1_10IfPreviousE
	.section	.text._ZN2v88internal8TextNode12TextEmitPassEPNS0_14RegExpCompilerENS1_16TextEmitPassTypeEbPNS0_5TraceEbPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode12TextEmitPassEPNS0_14RegExpCompilerENS1_16TextEmitPassTypeEbPNS0_5TraceEbPi
	.type	_ZN2v88internal8TextNode12TextEmitPassEPNS0_14RegExpCompilerENS1_16TextEmitPassTypeEbPNS0_5TraceEbPi, @function
_ZN2v88internal8TextNode12TextEmitPassEPNS0_14RegExpCompilerENS1_16TextEmitPassTypeEbPNS0_5TraceEbPi:
.LFB20453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsi, %rax
	movl	%edx, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$152, %rsp
	movq	%rdi, -128(%rbp)
	movq	16(%rbp), %rdi
	movq	%rsi, -152(%rbp)
	movq	%rdi, -104(%rbp)
	movq	40(%rax), %rdi
	movb	%cl, -78(%rbp)
	movb	%r9b, -77(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	16(%rdi), %rsi
	movq	%rdi, -168(%rbp)
	movzbl	48(%rax), %edi
	movq	16(%r8), %rax
	movl	$0, -156(%rbp)
	cmpb	$0, 72(%r10)
	movq	%rsi, -144(%rbp)
	movq	%rax, -136(%rbp)
	movq	64(%r10), %rax
	movl	12(%rax), %esi
	jne	.L1265
.L1148:
	testb	%cl, %cl
	jne	.L1220
	subl	$1, %esi
	movl	%esi, %r14d
	js	.L1147
.L1152:
	cmpb	$1, %dil
	movslq	%r14d, %rdx
	movl	%r11d, %r13d
	movq	%rbx, %r15
	sbbl	%esi, %esi
	salq	$4, %rdx
	andl	$65280, %esi
	movq	%rdx, -112(%rbp)
	addl	$255, %esi
	movl	%esi, -160(%rbp)
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1267:
	testb	$2, 24(%r11)
	je	.L1155
	cmpl	$1, %r13d
	sete	%al
	testb	%al, %al
	je	.L1266
	.p2align 4,,10
	.p2align 3
.L1158:
	subl	$1, %r14d
	subq	$16, -112(%rbp)
	cmpl	$-1, %r14d
	je	.L1147
	movq	-128(%rbp), %rax
	movq	64(%rax), %rax
.L1213:
	movq	-112(%rbp), %rdi
	addq	(%rax), %rdi
	movslq	(%rdi), %r10
	movl	(%r15), %edx
	movq	8(%rdi), %r11
	movl	4(%rdi), %edi
	addl	%r10d, %edx
	addl	-156(%rbp), %edx
	movl	%edx, -76(%rbp)
	testl	%edi, %edi
	je	.L1267
	cmpl	$4, %r13d
	jne	.L1158
	testl	%r14d, %r14d
	jne	.L1232
	cmpb	$0, -77(%rbp)
	je	.L1232
	.p2align 4,,10
	.p2align 3
.L1147:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1268
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1220:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1155:
	leal	-2(%r13), %eax
	cmpl	$1, %eax
	setbe	%al
	testb	%al, %al
	jne	.L1158
.L1266:
	movq	8(%r11), %rax
	cmpb	$0, -78(%rbp)
	movq	%rax, -88(%rbp)
	jne	.L1221
	movl	16(%r11), %ebx
	subl	$1, %ebx
	js	.L1158
.L1159:
	movslq	%ebx, %rbx
	movslq	%r10d, %rax
	movq	%r11, -96(%rbp)
	movq	%r15, %r11
	addq	%rbx, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r15,%rax,2), %r12
	movl	%r10d, %r15d
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1230:
	leaq	_ZN2v88internalL19EmitSimpleCharacterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb(%rip), %rax
.L1171:
	movq	-104(%rbp), %rsi
	addl	-76(%rbp), %r8d
	movl	$1, %r9d
	cmpl	%r8d, (%rsi)
	jl	.L1174
	movq	-128(%rbp), %rsi
	movzbl	72(%rsi), %r9d
.L1174:
	movzbl	-78(%rbp), %ecx
	subq	$8, %rsp
	movq	%r11, -72(%rbp)
	movq	-152(%rbp), %rsi
	movl	%r8d, -120(%rbp)
	pushq	%rcx
	movq	-144(%rbp), %rdi
	movq	-136(%rbp), %rcx
	call	*%rax
	popq	%rcx
	movq	-72(%rbp), %r11
	testb	%al, %al
	popq	%rsi
	je	.L1163
	movq	-104(%rbp), %rax
	movl	-120(%rbp), %r8d
	cmpl	%r8d, (%rax)
	jge	.L1163
	movl	%r8d, (%rax)
	.p2align 4,,10
	.p2align 3
.L1163:
	subq	$1, %rbx
	subq	$6, %r12
	testl	%ebx, %ebx
	js	.L1269
.L1175:
	movl	%r14d, %eax
	movl	%ebx, %r8d
	orl	%ebx, %eax
	jne	.L1231
	cmpb	$0, -77(%rbp)
	jne	.L1147
.L1231:
	leal	(%r15,%r8), %eax
	cmpl	%eax, 48(%r11)
	jle	.L1162
	cmpb	$0, 56(%r12)
	jne	.L1163
.L1162:
	movq	-88(%rbp), %rax
	movzwl	(%rax,%rbx,2), %edx
	movq	-96(%rbp), %rax
	testb	$2, 24(%rax)
	je	.L1164
	movl	%edx, %eax
	andl	$-33, %eax
	cmpw	$924, %ax
	je	.L1222
	cmpw	$376, %dx
	je	.L1223
.L1164:
	cmpl	$2, %r13d
	je	.L1228
	ja	.L1167
	testl	%r13d, %r13d
	jne	.L1230
	cmpw	$255, %dx
	jbe	.L1163
	movq	-168(%rbp), %rdi
	movq	-136(%rbp), %rsi
	movq	(%rdi), %rax
	call	*224(%rax)
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L1167:
	leaq	_ZN2v88internalL14EmitAtomLetterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb(%rip), %rax
	cmpl	$3, %r13d
	je	.L1171
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1228:
	leaq	_ZN2v88internalL17EmitAtomNonLetterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb(%rip), %rax
	jmp	.L1171
	.p2align 4,,10
	.p2align 3
.L1223:
	movl	$255, %edx
.L1165:
	cmpl	$2, %r13d
	je	.L1228
	cmpl	$3, %r13d
	je	.L1229
	cmpl	$1, %r13d
	jne	.L1163
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1222:
	movl	$181, %edx
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1229:
	leaq	_ZN2v88internalL14EmitAtomLetterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb(%rip), %rax
	jmp	.L1171
	.p2align 4,,10
	.p2align 3
.L1232:
	cmpl	%r10d, 48(%r15)
	jle	.L1177
	leaq	(%r10,%r10,2), %rax
	cmpb	$0, 56(%r15,%rax,2)
	jne	.L1158
.L1177:
	movq	-104(%rbp), %rax
	movl	-76(%rbp), %edi
	cmpl	%edi, (%rax)
	jl	.L1225
	movq	-128(%rbp), %rax
	movzbl	72(%rax), %eax
	movl	%eax, -172(%rbp)
	movl	%eax, %r12d
.L1178:
	movq	-128(%rbp), %rax
	leaq	8(%r11), %rdi
	movq	%r11, -96(%rbp)
	movq	48(%rax), %rsi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE@PLT
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE@PLT
	movl	12(%rbx), %edi
	movq	%rbx, %rax
	movq	-96(%rbp), %r11
	movl	%edi, %ebx
	movl	%edi, -88(%rbp)
	subl	$1, %ebx
	js	.L1179
	movq	(%rax), %rax
	movl	-160(%rbp), %edx
	movslq	%ebx, %rbx
	jmp	.L1181
	.p2align 4,,10
	.p2align 3
.L1270:
	subq	$1, %rbx
	testl	%ebx, %ebx
	js	.L1179
.L1181:
	movl	%ebx, %r10d
	cmpl	%edx, (%rax,%rbx,8)
	jg	.L1270
	testl	%ebx, %ebx
	jne	.L1184
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L1184
	movl	-160(%rbp), %ecx
	cmpl	4(%rax), %ecx
	jle	.L1271
	.p2align 4,,10
	.p2align 3
.L1184:
	cmpb	$0, -78(%rbp)
	je	.L1272
.L1187:
	movq	-72(%rbp), %rsi
	movq	%r11, %rdi
	movl	%r10d, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal20RegExpCharacterClass11is_standardEPNS0_4ZoneE@PLT
	movq	-88(%rbp), %r11
	movl	-96(%rbp), %r10d
	testb	%al, %al
	je	.L1192
	movq	-168(%rbp), %rdi
	movzwl	16(%r11), %esi
	movl	%r10d, -96(%rbp)
	movq	%r11, -88(%rbp)
	movq	-136(%rbp), %rdx
	movq	(%rdi), %rax
	call	*200(%rax)
	movq	-88(%rbp), %r11
	movl	-96(%rbp), %r10d
	testb	%al, %al
	jne	.L1212
.L1192:
	movq	-72(%rbp), %rax
	movq	16(%rax), %r12
	movq	24(%rax), %rax
	movq	%rax, -88(%rbp)
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L1273
	movq	-72(%rbp), %rcx
	leaq	16(%r12), %rax
	movq	%rax, 16(%rcx)
.L1193:
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jne	.L1274
.L1194:
	movq	%rax, (%r12)
	movq	-120(%rbp), %r8
	movl	%r10d, %edx
	xorl	%ebx, %ebx
	movl	%r10d, 8(%r12)
	xorl	%eax, %eax
	movl	$0, 12(%r12)
	movl	28(%r11), %r11d
	movq	%r15, -184(%rbp)
	movq	-72(%rbp), %r15
	andl	$1, %r11d
	movl	%r14d, -172(%rbp)
	movl	%r13d, -120(%rbp)
	xorl	$1, %r11d
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1275:
	xorl	$1, %r11d
.L1198:
	movl	4(%r13), %r13d
	addl	$1, %r13d
	cmpl	%edx, %eax
	jge	.L1203
.L1276:
	movq	(%r12), %rdx
	leal	1(%rax), %esi
	addq	$1, %rbx
	movl	%esi, 12(%r12)
	movl	%r13d, (%rdx,%rax,4)
	movslq	12(%r12), %rax
	cmpl	%ebx, %r10d
	jl	.L1208
.L1278:
	movl	8(%r12), %edx
.L1209:
	movq	(%r8), %rsi
	leaq	(%rsi,%rbx,8), %r13
	movl	0(%r13), %r14d
	testl	%r14d, %r14d
	je	.L1275
	cmpl	%edx, %eax
	jge	.L1199
	movq	(%r12), %rdx
	leal	1(%rax), %esi
	movl	%esi, 12(%r12)
	movl	%r14d, (%rdx,%rax,4)
	movl	4(%r13), %r13d
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	addl	$1, %r13d
	cmpl	%edx, %eax
	jl	.L1276
.L1203:
	leal	1(%rdx,%rdx), %r14d
	movq	16(%r15), %rdi
	movslq	%r14d, %rax
	leaq	7(,%rax,4), %rsi
	movq	24(%r15), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1277
	addq	%rdi, %rsi
	movq	%rsi, 16(%r15)
.L1206:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$2, %rdx
	testl	%eax, %eax
	jle	.L1207
	movq	(%r12), %rsi
	movl	%r10d, -96(%rbp)
	movb	%r11b, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-96(%rbp), %r10d
	movzbl	-88(%rbp), %r11d
	movq	-72(%rbp), %r8
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$2, %rdx
.L1207:
	addl	$1, %eax
	addq	$1, %rbx
	movq	%rdi, (%r12)
	movl	%eax, 12(%r12)
	movl	%r14d, 8(%r12)
	movl	%r13d, (%rdi,%rdx)
	movslq	12(%r12), %rax
	cmpl	%ebx, %r10d
	jge	.L1278
.L1208:
	leal	-1(%rax), %ecx
	movq	(%r12), %rdx
	movl	-120(%rbp), %r13d
	movslq	%ecx, %rsi
	movl	-172(%rbp), %r14d
	movq	-184(%rbp), %r15
	movl	-160(%rbp), %edi
	cmpl	(%rdx,%rsi,4), %edi
	jge	.L1210
	leal	-2(%rax), %ecx
.L1210:
	movq	$0, -64(%rbp)
	testb	%r11b, %r11b
	je	.L1227
	leaq	-64(%rbp), %rbx
	movq	-136(%rbp), %rdx
	movq	%rbx, %rax
.L1211:
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	-168(%rbp), %r12
	xorl	%r8d, %r8d
	pushq	%rdx
	movl	-160(%rbp), %r9d
	xorl	%edx, %edx
	pushq	%rax
	movq	%r12, %rdi
	pushq	%rbx
	call	_ZN2v88internalL16GenerateBranchesEPNS0_20RegExpMacroAssemblerEPNS0_8ZoneListIiEEiiiiPNS0_5LabelES7_S7_
	movq	(%r12), %rax
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	*64(%rax)
.L1212:
	movq	-104(%rbp), %rax
	movl	-76(%rbp), %ecx
	cmpl	(%rax), %ecx
	jle	.L1158
	movl	%ecx, (%rax)
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1221:
	xorl	%ebx, %ebx
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1225:
	movl	$1, -172(%rbp)
	movl	$1, %r12d
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1199:
	leal	1(%rdx,%rdx), %r9d
	movq	16(%r15), %rdi
	movslq	%r9d, %rax
	leaq	7(,%rax,4), %rsi
	movq	24(%r15), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1279
	addq	%rdi, %rsi
	movq	%rsi, 16(%r15)
.L1201:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$2, %rdx
	testl	%eax, %eax
	jle	.L1202
	movq	(%r12), %rsi
	movl	%r10d, -176(%rbp)
	movb	%r11b, -96(%rbp)
	movq	%r8, -88(%rbp)
	movl	%r9d, -72(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-176(%rbp), %r10d
	movzbl	-96(%rbp), %r11d
	movq	-88(%rbp), %r8
	movq	%rax, %rdi
	movl	-72(%rbp), %r9d
	movq	%rdx, %rax
	salq	$2, %rdx
.L1202:
	addl	$1, %eax
	movl	%r9d, 8(%r12)
	movl	%eax, 12(%r12)
	movq	%rdi, (%r12)
	movl	%r14d, (%rdi,%rdx)
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	jmp	.L1198
.L1265:
	leal	-1(%rsi), %edx
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	(%rax), %rdx
	movl	4(%rdx), %r9d
	movl	(%rdx), %r8d
	testl	%r9d, %r9d
	je	.L1149
	cmpl	$1, %r9d
	jne	.L1280
	movl	$1, %edx
.L1150:
	addl	%r8d, %edx
	negl	%edx
	movl	%edx, -156(%rbp)
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1179:
	testb	$1, 28(%r11)
	jne	.L1258
	movq	-168(%rbp), %rdi
	movq	-136(%rbp), %rsi
	movq	(%rdi), %rax
	call	*224(%rax)
.L1258:
	testb	%r12b, %r12b
	je	.L1212
	movq	-168(%rbp), %rdi
	movq	-136(%rbp), %rdx
	movl	-76(%rbp), %esi
	movq	(%rdi), %rax
	call	*192(%rax)
	jmp	.L1212
.L1227:
	leaq	-64(%rbp), %rbx
	movq	-136(%rbp), %rax
	movq	%rbx, %rdx
	jmp	.L1211
.L1272:
	movl	-172(%rbp), %ecx
	movl	-76(%rbp), %esi
	movl	$-1, %r9d
	movl	$1, %r8d
	movq	-136(%rbp), %rdx
	movq	-168(%rbp), %rdi
	movl	%r10d, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii@PLT
	movl	-96(%rbp), %r10d
	movq	-88(%rbp), %r11
	jmp	.L1187
.L1274:
	movq	-72(%rbp), %rcx
	leaq	7(,%rbx,4), %rsi
	andq	$-8, %rsi
	movq	16(%rcx), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, %rdx
	movq	%rcx, -88(%rbp)
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1281
	movq	-72(%rbp), %rcx
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx)
	jmp	.L1194
.L1149:
	movq	8(%rdx), %rdx
	movl	16(%rdx), %edx
	jmp	.L1150
.L1271:
	testb	$1, 28(%r11)
	je	.L1258
	movq	-168(%rbp), %rdi
	movq	-136(%rbp), %rsi
	movq	(%rdi), %rax
	call	*224(%rax)
	jmp	.L1212
.L1277:
	movq	%r15, %rdi
	movl	%r10d, -96(%rbp)
	movb	%r11b, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r8
	movzbl	-88(%rbp), %r11d
	movl	-96(%rbp), %r10d
	movq	%rax, %rdi
	jmp	.L1206
.L1273:
	movq	-72(%rbp), %rdi
	movl	$16, %esi
	movl	%r10d, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r11
	movl	-96(%rbp), %r10d
	movq	%rax, %r12
	jmp	.L1193
.L1279:
	movq	%r15, %rdi
	movl	%r10d, -176(%rbp)
	movb	%r11b, -96(%rbp)
	movq	%r8, -88(%rbp)
	movl	%r9d, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-72(%rbp), %r9d
	movq	-88(%rbp), %r8
	movzbl	-96(%rbp), %r11d
	movl	-176(%rbp), %r10d
	movq	%rax, %rdi
	jmp	.L1201
.L1281:
	movq	-72(%rbp), %rdi
	movl	%r10d, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r11
	movl	-96(%rbp), %r10d
	jmp	.L1194
.L1280:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1268:
	call	__stack_chk_fail@PLT
.L1269:
	movq	%r11, %r15
	jmp	.L1158
	.cfi_endproc
.LFE20453:
	.size	_ZN2v88internal8TextNode12TextEmitPassEPNS0_14RegExpCompilerENS1_16TextEmitPassTypeEbPNS0_5TraceEbPi, .-_ZN2v88internal8TextNode12TextEmitPassEPNS0_14RegExpCompilerENS1_16TextEmitPassTypeEbPNS0_5TraceEbPi
	.section	.text._ZN2v88internal8TextNode6LengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode6LengthEv
	.type	_ZN2v88internal8TextNode6LengthEv, @function
_ZN2v88internal8TextNode6LengthEv:
.LFB20454:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	movl	12(%rax), %esi
	leal	-1(%rsi), %edx
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	(%rax), %rdx
	movl	4(%rdx), %ecx
	movl	(%rdx), %esi
	testl	%ecx, %ecx
	je	.L1283
	cmpl	$1, %ecx
	jne	.L1291
	movl	$1, %eax
	addl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1283:
	movq	8(%rdx), %rax
	movl	16(%rax), %eax
	addl	%esi, %eax
	ret
.L1291:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20454:
	.size	_ZN2v88internal8TextNode6LengthEv, .-_ZN2v88internal8TextNode6LengthEv
	.section	.text._ZN2v88internal8TextNode8SkipPassENS1_16TextEmitPassTypeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode8SkipPassENS1_16TextEmitPassTypeEb
	.type	_ZN2v88internal8TextNode8SkipPassENS1_16TextEmitPassTypeEb, @function
_ZN2v88internal8TextNode8SkipPassENS1_16TextEmitPassTypeEb:
.LFB20455:
	.cfi_startproc
	endbr64
	testb	%sil, %sil
	je	.L1293
	cmpl	$1, %edi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1293:
	subl	$2, %edi
	cmpl	$1, %edi
	setbe	%al
	ret
	.cfi_endproc
.LFE20455:
	.size	_ZN2v88internal8TextNode8SkipPassENS1_16TextEmitPassTypeEb, .-_ZN2v88internal8TextNode8SkipPassENS1_16TextEmitPassTypeEb
	.section	.text._ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.type	_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, @function
_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE:
.LFB20456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	movl	%r8d, %ecx
	subq	$24, %rsp
	movq	24(%rdi), %rdx
	movq	16(%rdi), %r14
	movq	%rdx, %rax
	subq	%r14, %rax
	cmpq	$15, %rax
	jbe	.L1310
	leaq	16(%r14), %rax
	subq	%rax, %rdx
	movq	%rax, 16(%rdi)
	cmpq	$15, %rdx
	jbe	.L1311
.L1298:
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
.L1299:
	movq	%rax, (%r14)
	movq	$1, 8(%r14)
	movq	16(%r12), %r15
	movq	24(%r12), %rax
	subq	%r15, %rax
	cmpq	$31, %rax
	jbe	.L1312
	leaq	32(%r15), %rax
	movq	%rax, 16(%r12)
.L1301:
	movq	%r9, %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movslq	12(%r14), %rax
	movl	8(%r14), %edx
	cmpl	%edx, %eax
	jge	.L1302
	movabsq	$8589934591, %rdi
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%r14), %rax
	movl	%edx, 12(%r14)
	movq	%rdi, (%rax)
	movq	%r15, 8(%rax)
.L1303:
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$79, %rdx
	jbe	.L1313
	leaq	80(%rax), %rdx
	movq	%rdx, 16(%r12)
.L1308:
	movq	48(%rbx), %xmm0
	movq	%rbx, %xmm2
	pxor	%xmm1, %xmm1
	leaq	16+_ZTVN2v88internal8TextNodeE(%rip), %rbx
	movq	$0, 8(%rax)
	punpcklqdq	%xmm2, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rbx, (%rax)
	movq	%r14, 64(%rax)
	movb	%r13b, 72(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1302:
	.cfi_restore_state
	movq	16(%r12), %rcx
	movq	24(%r12), %rax
	leal	1(%rdx,%rdx), %r8d
	movslq	%r8d, %rsi
	salq	$4, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1314
	addq	%rcx, %rsi
	movq	%rsi, 16(%r12)
.L1305:
	movslq	12(%r14), %rdx
	movq	%rdx, %rsi
	salq	$4, %rdx
	testl	%esi, %esi
	jg	.L1315
.L1306:
	movabsq	$8589934591, %rax
	movq	%rcx, (%r14)
	addl	$1, %esi
	addq	%rdx, %rcx
	movl	%r8d, 8(%r14)
	movl	%esi, 12(%r14)
	movq	%rax, (%rcx)
	movq	%r15, 8(%rcx)
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1315:
	movq	(%r14), %rsi
	movq	%rcx, %rdi
	movl	%r8d, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%r14), %rdx
	movl	-56(%rbp), %r8d
	movq	%rax, %rcx
	movq	%rdx, %rsi
	salq	$4, %rdx
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1313:
	movl	$80, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	%rsi, -56(%rbp)
	movl	$16, %esi
	movl	%r8d, -60(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r12), %rdx
	movq	-56(%rbp), %r9
	movq	%rax, %r14
	movq	16(%r12), %rax
	movl	-60(%rbp), %ecx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	ja	.L1298
	.p2align 4,,10
	.p2align 3
.L1311:
	movl	$16, %esi
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movl	-60(%rbp), %ecx
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1312:
	movl	$32, %esi
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movl	-60(%rbp), %ecx
	movq	%rax, %r15
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1314:
	movq	%r12, %rdi
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %r8d
	movq	%rax, %rcx
	jmp	.L1305
	.cfi_endproc
.LFE20456:
	.size	_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, .-_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.section	.text._ZN2v88internal5Trace26InvalidateCurrentCharacterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Trace26InvalidateCurrentCharacterEv
	.type	_ZN2v88internal5Trace26InvalidateCurrentCharacterEv, @function
_ZN2v88internal5Trace26InvalidateCurrentCharacterEv:
.LFB20459:
	.cfi_startproc
	endbr64
	movl	$0, 40(%rdi)
	ret
	.cfi_endproc
.LFE20459:
	.size	_ZN2v88internal5Trace26InvalidateCurrentCharacterEv, .-_ZN2v88internal5Trace26InvalidateCurrentCharacterEv
	.section	.text._ZN2v88internal5Trace29AdvanceCurrentPositionInTraceEiPNS0_14RegExpCompilerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Trace29AdvanceCurrentPositionInTraceEiPNS0_14RegExpCompilerE
	.type	_ZN2v88internal5Trace29AdvanceCurrentPositionInTraceEiPNS0_14RegExpCompilerE, @function
_ZN2v88internal5Trace29AdvanceCurrentPositionInTraceEiPNS0_14RegExpCompilerE:
.LFB20460:
	.cfi_startproc
	endbr64
	movl	48(%rdi), %eax
	movl	$0, 40(%rdi)
	cmpl	%eax, %esi
	jge	.L1328
	testl	%esi, %esi
	js	.L1328
	movslq	%esi, %rcx
	leaq	52(%rdi), %rax
	xorl	%r8d, %r8d
	leaq	(%rcx,%rcx,2), %r9
	addq	%r9, %r9
	.p2align 4,,10
	.p2align 3
.L1324:
	movl	(%rax,%r9), %ecx
	addl	$1, %r8d
	addq	$6, %rax
	movl	%ecx, -6(%rax)
	movzbl	-2(%rax,%r9), %ecx
	movb	%cl, -2(%rax)
	movl	48(%rdi), %r10d
	movl	%r10d, %ecx
	subl	%esi, %ecx
	cmpl	%r8d, %ecx
	jg	.L1324
	cmpl	%ecx, %r10d
	jle	.L1325
	movslq	%ecx, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,2), %rax
	movl	$0, 52(%rax)
	movb	$0, 56(%rax)
	leal	1(%rcx), %eax
	cmpl	%r10d, %eax
	jge	.L1325
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,2), %rax
	movl	$0, 52(%rax)
	movb	$0, 56(%rax)
	leal	2(%rcx), %eax
	cmpl	%eax, %r10d
	jle	.L1325
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,2), %rax
	movl	$0, 52(%rax)
	movb	$0, 56(%rax)
	leal	3(%rcx), %eax
	cmpl	%eax, %r10d
	jle	.L1325
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,2), %rax
	movl	$0, 52(%rax)
	movb	$0, 56(%rax)
.L1325:
	movl	%ecx, 48(%rdi)
	jmp	.L1321
	.p2align 4,,10
	.p2align 3
.L1328:
	testl	%eax, %eax
	jle	.L1323
	movl	$0, 52(%rdi)
	movb	$0, 56(%rdi)
	cmpl	$1, %eax
	je	.L1323
	movl	$0, 58(%rdi)
	movb	$0, 62(%rdi)
	cmpl	$2, %eax
	je	.L1323
	movl	$0, 64(%rdi)
	movb	$0, 68(%rdi)
	cmpl	$3, %eax
	je	.L1323
	movl	$0, 70(%rdi)
	movb	$0, 74(%rdi)
.L1323:
	movl	$0, 48(%rdi)
.L1321:
	movl	(%rdi), %eax
	addl	%esi, %eax
	movl	%eax, (%rdi)
	cmpl	$32767, %eax
	jle	.L1327
	movb	$1, 49(%rdx)
	movl	$0, (%rdi)
.L1327:
	subl	%esi, 44(%rdi)
	movl	44(%rdi), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovns	44(%rdi), %eax
	movl	%eax, 44(%rdi)
	ret
	.cfi_endproc
.LFE20460:
	.size	_ZN2v88internal5Trace29AdvanceCurrentPositionInTraceEiPNS0_14RegExpCompilerE, .-_ZN2v88internal5Trace29AdvanceCurrentPositionInTraceEiPNS0_14RegExpCompilerE
	.section	.text._ZN2v88internal8TextNode19MakeCaseIndependentEPNS0_7IsolateEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode19MakeCaseIndependentEPNS0_7IsolateEb
	.type	_ZN2v88internal8TextNode19MakeCaseIndependentEPNS0_7IsolateEb, @function
_ZN2v88internal8TextNode19MakeCaseIndependentEPNS0_7IsolateEb:
.LFB20461:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	movl	12(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L1349
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leal	-1(%rcx), %r15d
	pushq	%r14
	salq	$4, %r15
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movzbl	%dl, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1342:
	cmpq	%r15, %rbx
	je	.L1339
.L1352:
	movq	64(%r14), %rax
	addq	$16, %rbx
.L1344:
	movq	(%rax), %rcx
	addq	%rbx, %rcx
	cmpl	$1, 4(%rcx)
	jne	.L1342
	movq	8(%rcx), %rdi
	movl	24(%rdi), %eax
	testb	$16, %al
	jne	.L1342
	testb	$2, %al
	je	.L1342
	movq	48(%r14), %rsi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal20RegExpCharacterClass11is_standardEPNS0_4ZoneE@PLT
	testb	%al, %al
	jne	.L1342
	movq	-56(%rbp), %rdi
	movq	48(%r14), %rsi
	addq	$8, %rdi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE@PLT
	movq	48(%r14), %rsi
	movl	%r12d, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14CharacterRange18AddCaseEquivalentsEPNS0_7IsolateEPNS0_4ZoneEPNS0_8ZoneListIS1_EEb@PLT
	cmpq	%r15, %rbx
	jne	.L1352
.L1339:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1349:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE20461:
	.size	_ZN2v88internal8TextNode19MakeCaseIndependentEPNS0_7IsolateEb, .-_ZN2v88internal8TextNode19MakeCaseIndependentEPNS0_7IsolateEb
	.section	.text._ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE9VisitTextEPNS0_8TextNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE9VisitTextEPNS0_8TextNodeE, @function
_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE9VisitTextEPNS0_8TextNodeE:
.LFB24795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	16(%rdi), %edx
	movq	%rdi, %rbx
	movq	8(%rdi), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8TextNode19MakeCaseIndependentEPNS0_7IsolateEb
	movq	8(%rbx), %r14
	movq	56(%r12), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L1372
	movzbl	25(%r13), %eax
	testb	$2, %al
	jne	.L1356
	testb	$1, %al
	jne	.L1356
	orl	$1, %eax
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movb	%al, 25(%r13)
	movq	0(%r13), %rax
	call	*16(%rax)
	movzbl	25(%r13), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%r13)
.L1356:
	cmpq	$0, 24(%rbx)
	je	.L1373
.L1353:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1373:
	.cfi_restore_state
	movq	64(%r12), %rdx
	movl	12(%rdx), %eax
	testl	%eax, %eax
	jle	.L1358
	leal	-1(%rax), %edi
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	salq	$4, %rdi
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1359:
	movq	8(%rbx), %rdx
	movl	16(%rdx), %edx
	addl	%edx, %ecx
	cmpq	%rdi, %rax
	je	.L1358
.L1374:
	movq	64(%r12), %rdx
	addq	$16, %rax
.L1362:
	movq	(%rdx), %rbx
	addq	%rax, %rbx
	movl	4(%rbx), %esi
	movl	%ecx, (%rbx)
	testl	%esi, %esi
	je	.L1359
	cmpl	$1, %esi
	jne	.L1361
	movl	$1, %edx
	addl	%edx, %ecx
	cmpq	%rdi, %rax
	jne	.L1374
.L1358:
	cmpb	$0, 72(%r12)
	jne	.L1353
	movq	64(%r12), %rdx
	movl	12(%rdx), %eax
	subl	$1, %eax
	cltq
	salq	$4, %rax
	addq	(%rdx), %rax
	movl	4(%rax), %ecx
	movl	(%rax), %edx
	testl	%ecx, %ecx
	je	.L1364
	cmpl	$1, %ecx
	jne	.L1361
	movl	$1, %ecx
.L1365:
	movq	56(%r12), %rax
	addl	%ecx, %edx
	movzbl	27(%rax), %eax
	addl	%edx, %eax
	movl	$-1, %edx
	cmpl	$255, %eax
	jg	.L1366
	testl	%eax, %eax
	movl	$0, %edx
	cmovns	%eax, %edx
.L1366:
	movb	%dl, 26(%r12)
	movb	%dl, 27(%r12)
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1372:
	leaq	.LC1(%rip), %rax
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1364:
	.cfi_restore_state
	movq	8(%rax), %rax
	movl	16(%rax), %ecx
	jmp	.L1365
.L1361:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24795:
	.size	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE9VisitTextEPNS0_8TextNodeE, .-_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE9VisitTextEPNS0_8TextNodeE
	.section	.text._ZN2v88internal8TextNode6AcceptEPNS0_11NodeVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode6AcceptEPNS0_11NodeVisitorE
	.type	_ZN2v88internal8TextNode6AcceptEPNS0_11NodeVisitorE, @function
_ZN2v88internal8TextNode6AcceptEPNS0_11NodeVisitorE:
.LFB20379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE9VisitTextEPNS0_8TextNodeE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1376
	movzbl	16(%rsi), %edx
	movq	8(%rsi), %rsi
	call	_ZN2v88internal8TextNode19MakeCaseIndependentEPNS0_7IsolateEb
	movq	8(%r12), %r14
	movq	56(%r13), %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L1395
	movzbl	25(%rbx), %eax
	testb	$2, %al
	jne	.L1379
	testb	$1, %al
	jne	.L1379
	orl	$1, %eax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movb	%al, 25(%rbx)
	movq	(%rbx), %rax
	call	*16(%rax)
	movzbl	25(%rbx), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%rbx)
.L1379:
	cmpq	$0, 24(%r12)
	je	.L1396
.L1375:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1396:
	.cfi_restore_state
	movq	64(%r13), %rdx
	movl	12(%rdx), %eax
	testl	%eax, %eax
	jle	.L1381
	leal	-1(%rax), %edi
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	salq	$4, %rdi
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1382:
	movq	8(%rbx), %rdx
	movl	16(%rdx), %edx
	addl	%edx, %ecx
	cmpq	%rdi, %rax
	je	.L1381
.L1397:
	movq	64(%r13), %rdx
	addq	$16, %rax
.L1385:
	movq	(%rdx), %rbx
	addq	%rax, %rbx
	movl	4(%rbx), %esi
	movl	%ecx, (%rbx)
	testl	%esi, %esi
	je	.L1382
	cmpl	$1, %esi
	jne	.L1384
	movl	$1, %edx
	addl	%edx, %ecx
	cmpq	%rdi, %rax
	jne	.L1397
.L1381:
	cmpb	$0, 72(%r13)
	jne	.L1375
	movq	64(%r13), %rdx
	movl	12(%rdx), %eax
	subl	$1, %eax
	cltq
	salq	$4, %rax
	addq	(%rdx), %rax
	movl	4(%rax), %ecx
	movl	(%rax), %edx
	testl	%ecx, %ecx
	je	.L1387
	cmpl	$1, %ecx
	jne	.L1384
	movl	$1, %ecx
.L1388:
	movq	56(%r13), %rax
	addl	%ecx, %edx
	movzbl	27(%rax), %eax
	addl	%edx, %eax
	movl	$-1, %edx
	cmpl	$255, %eax
	jg	.L1389
	testl	%eax, %eax
	movl	$0, %edx
	cmovns	%eax, %edx
.L1389:
	movb	%dl, 26(%r13)
	movb	%dl, 27(%r13)
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1395:
	leaq	.LC1(%rip), %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1376:
	.cfi_restore_state
	popq	%rbx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1387:
	.cfi_restore_state
	movq	8(%rax), %rax
	movl	16(%rax), %ecx
	jmp	.L1388
.L1384:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20379:
	.size	_ZN2v88internal8TextNode6AcceptEPNS0_11NodeVisitorE, .-_ZN2v88internal8TextNode6AcceptEPNS0_11NodeVisitorE
	.section	.text._ZN2v88internal10ChoiceNode34GreedyLoopTextLengthForAlternativeEPNS0_18GuardedAlternativeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode34GreedyLoopTextLengthForAlternativeEPNS0_18GuardedAlternativeE
	.type	_ZN2v88internal10ChoiceNode34GreedyLoopTextLengthForAlternativeEPNS0_18GuardedAlternativeE, @function
_ZN2v88internal10ChoiceNode34GreedyLoopTextLengthForAlternativeEPNS0_18GuardedAlternativeE:
.LFB20464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	%rdi, %r14
	je	.L1403
	movl	$101, %ebx
	xorl	%r12d, %r12d
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1410:
	movq	56(%r14), %r14
	addl	%eax, %r12d
	cmpq	%r13, %r14
	je	.L1399
	subl	$1, %ebx
	je	.L1404
.L1400:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*56(%rax)
	cmpl	$-2147483648, %eax
	jne	.L1410
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1403:
	.cfi_restore_state
	xorl	%r12d, %r12d
.L1399:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*96(%rax)
	popq	%rbx
	movl	%eax, %r8d
	movl	%r12d, %eax
	negl	%eax
	testb	%r8b, %r8b
	cmove	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1404:
	.cfi_restore_state
	popq	%rbx
	movl	$-2147483648, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20464:
	.size	_ZN2v88internal10ChoiceNode34GreedyLoopTextLengthForAlternativeEPNS0_18GuardedAlternativeE, .-_ZN2v88internal10ChoiceNode34GreedyLoopTextLengthForAlternativeEPNS0_18GuardedAlternativeE
	.section	.text._ZN2v88internal14LoopChoiceNode18AddLoopAlternativeENS0_18GuardedAlternativeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LoopChoiceNode18AddLoopAlternativeENS0_18GuardedAlternativeE
	.type	_ZN2v88internal14LoopChoiceNode18AddLoopAlternativeENS0_18GuardedAlternativeE, @function
_ZN2v88internal14LoopChoiceNode18AddLoopAlternativeENS0_18GuardedAlternativeE:
.LFB20465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	56(%rdi), %rbx
	movq	%rdx, -40(%rbp)
	movslq	12(%rbx), %rax
	movl	8(%rbx), %edx
	cmpl	%edx, %eax
	jge	.L1412
	movq	%rsi, %xmm1
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%rbx), %rax
	movhps	-40(%rbp), %xmm1
	movl	%edx, 12(%rbx)
	movups	%xmm1, (%rax)
.L1413:
	movq	%xmm0, 72(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1412:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	leal	1(%rdx,%rdx), %r13d
	movslq	%r13d, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	salq	$4, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1418
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1415:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rax
	salq	$4, %rdx
	testl	%eax, %eax
	jg	.L1419
.L1416:
	movdqa	%xmm0, %xmm1
	addl	$1, %eax
	movq	%rcx, (%rbx)
	movhps	-40(%rbp), %xmm1
	movl	%r13d, 8(%rbx)
	movl	%eax, 12(%rbx)
	movups	%xmm1, (%rcx,%rdx)
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	movq	%xmm0, -48(%rbp)
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	-48(%rbp), %xmm0
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$4, %rdx
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1418:
	movq	%xmm0, -48(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-48(%rbp), %xmm0
	movq	%rax, %rcx
	jmp	.L1415
	.cfi_endproc
.LFE20465:
	.size	_ZN2v88internal14LoopChoiceNode18AddLoopAlternativeENS0_18GuardedAlternativeE, .-_ZN2v88internal14LoopChoiceNode18AddLoopAlternativeENS0_18GuardedAlternativeE
	.section	.text._ZN2v88internal14LoopChoiceNode22AddContinueAlternativeENS0_18GuardedAlternativeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LoopChoiceNode22AddContinueAlternativeENS0_18GuardedAlternativeE
	.type	_ZN2v88internal14LoopChoiceNode22AddContinueAlternativeENS0_18GuardedAlternativeE, @function
_ZN2v88internal14LoopChoiceNode22AddContinueAlternativeENS0_18GuardedAlternativeE:
.LFB20466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	56(%rdi), %rbx
	movq	%rdx, -40(%rbp)
	movslq	12(%rbx), %rax
	movl	8(%rbx), %edx
	cmpl	%edx, %eax
	jge	.L1421
	movq	%rsi, %xmm1
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%rbx), %rax
	movhps	-40(%rbp), %xmm1
	movl	%edx, 12(%rbx)
	movups	%xmm1, (%rax)
.L1422:
	movq	%xmm0, 80(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1421:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	leal	1(%rdx,%rdx), %r13d
	movslq	%r13d, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	salq	$4, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1427
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1424:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rax
	salq	$4, %rdx
	testl	%eax, %eax
	jg	.L1428
.L1425:
	movdqa	%xmm0, %xmm1
	addl	$1, %eax
	movq	%rcx, (%rbx)
	movhps	-40(%rbp), %xmm1
	movl	%r13d, 8(%rbx)
	movl	%eax, 12(%rbx)
	movups	%xmm1, (%rcx,%rdx)
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1428:
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	movq	%xmm0, -48(%rbp)
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	-48(%rbp), %xmm0
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$4, %rdx
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1427:
	movq	%xmm0, -48(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-48(%rbp), %xmm0
	movq	%rax, %rcx
	jmp	.L1424
	.cfi_endproc
.LFE20466:
	.size	_ZN2v88internal14LoopChoiceNode22AddContinueAlternativeENS0_18GuardedAlternativeE, .-_ZN2v88internal14LoopChoiceNode22AddContinueAlternativeENS0_18GuardedAlternativeE
	.section	.text._ZN2v88internal10ChoiceNode26CalculatePreloadCharactersEPNS0_14RegExpCompilerEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode26CalculatePreloadCharactersEPNS0_14RegExpCompilerEi
	.type	_ZN2v88internal10ChoiceNode26CalculatePreloadCharactersEPNS0_14RegExpCompilerEi, @function
_ZN2v88internal10ChoiceNode26CalculatePreloadCharactersEPNS0_14RegExpCompilerEi:
.LFB20468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	$4, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$8, %rsp
	movq	40(%rsi), %rdi
	cmpl	$4, %edx
	cmovle	%edx, %r12d
	leaq	_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1430
	movq	32(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	32(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1431
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1430
	movq	32(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	32(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1431
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L1434:
	testb	%al, %al
	je	.L1435
.L1441:
	cmpb	$0, 48(%r13)
	je	.L1436
	cmpl	$3, %ebx
	movl	$2, %eax
	cmove	%eax, %r12d
	addq	$8, %rsp
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1431:
	.cfi_restore_state
	call	*%rdx
	testb	%al, %al
	jne	.L1441
.L1435:
	cmpl	$2, %ebx
	movl	$1, %eax
	cmovge	%eax, %r12d
	addq	$8, %rsp
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1430:
	.cfi_restore_state
	call	*%rax
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L1436:
	cmpl	$3, %ebx
	movl	$2, %eax
	cmovge	%eax, %r12d
	addq	$8, %rsp
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20468:
	.size	_ZN2v88internal10ChoiceNode26CalculatePreloadCharactersEPNS0_14RegExpCompilerEi, .-_ZN2v88internal10ChoiceNode26CalculatePreloadCharactersEPNS0_14RegExpCompilerEi
	.section	.text._ZN2v88internal22BoyerMoorePositionInfo3SetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22BoyerMoorePositionInfo3SetEi
	.type	_ZN2v88internal22BoyerMoorePositionInfo3SetEi, @function
_ZN2v88internal22BoyerMoorePositionInfo3SetEi:
.LFB20480:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	movl	%esi, %ecx
	cmpl	$3, %eax
	je	.L1443
	cmpl	$47, %esi
	jg	.L1460
	testl	%esi, %esi
	jns	.L1454
	movl	$3, %eax
.L1443:
	movl	%eax, 20(%rdi)
	movq	%rcx, %rax
	movl	$1, %edx
	movl	16(%rdi), %esi
	shrq	$3, %rax
	salq	%cl, %rdx
	andl	$8, %eax
	addq	%rdi, %rax
	testq	%rdx, (%rax)
	je	.L1461
	ret
	.p2align 4,,10
	.p2align 3
.L1461:
	addl	$1, %esi
	movl	%esi, 16(%rdi)
	orq	%rdx, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L1460:
	movl	$1, %edx
	cmpl	$57, %esi
	jg	.L1462
.L1445:
	orl	%edx, %eax
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1462:
	movl	$2, %edx
	cmpl	$64, %esi
	jle	.L1445
	movl	$1, %edx
	cmpl	$90, %esi
	jle	.L1445
	movl	$2, %edx
	cmpl	$94, %esi
	jle	.L1445
	movl	$1, %edx
	cmpl	$95, %esi
	je	.L1445
	movl	$2, %edx
	cmpl	$96, %esi
	je	.L1445
	movl	$1, %edx
	cmpl	$122, %esi
	jle	.L1445
	cmpl	$1114111, %esi
	jg	.L1443
	.p2align 4,,10
	.p2align 3
.L1454:
	movl	$2, %edx
	orl	%edx, %eax
	jmp	.L1443
	.cfi_endproc
.LFE20480:
	.size	_ZN2v88internal22BoyerMoorePositionInfo3SetEi, .-_ZN2v88internal22BoyerMoorePositionInfo3SetEi
	.section	.text._ZN2v88internal22BoyerMoorePositionInfo11SetIntervalERKNS0_8IntervalE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22BoyerMoorePositionInfo11SetIntervalERKNS0_8IntervalE
	.type	_ZN2v88internal22BoyerMoorePositionInfo11SetIntervalERKNS0_8IntervalE, @function
_ZN2v88internal22BoyerMoorePositionInfo11SetIntervalERKNS0_8IntervalE:
.LFB20490:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	movl	(%rsi), %edx
	movl	4(%rsi), %ecx
	cmpl	$3, %eax
	je	.L1464
	xorl	%r9d, %r9d
	movl	$48, %r8d
	cmpl	$47, %edx
	jg	.L1503
.L1465:
	cmpl	%r8d, %ecx
	jge	.L1487
	cmpl	%r9d, %edx
	jge	.L1484
.L1487:
	movl	$3, %eax
.L1464:
	movl	%eax, 20(%rdi)
	movl	4(%rsi), %eax
	movl	(%rsi), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	cmpl	$126, %edx
	jg	.L1504
	cmpl	%eax, %ecx
	jg	.L1463
	movl	16(%rdi), %edx
	movl	$1, %r9d
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1470:
	cmpl	$128, %edx
	je	.L1463
.L1506:
	addl	$1, %ecx
	cmpl	4(%rsi), %ecx
	jg	.L1505
.L1472:
	movq	%rcx, %rax
	movq	%r9, %r8
	shrq	$3, %rax
	salq	%cl, %r8
	andl	$8, %eax
	addq	%rdi, %rax
	testq	%r8, (%rax)
	jne	.L1470
	addl	$1, %edx
	movl	%edx, 16(%rdi)
	orq	%r8, (%rax)
	movl	16(%rdi), %edx
	cmpl	$128, %edx
	jne	.L1506
.L1463:
	ret
	.p2align 4,,10
	.p2align 3
.L1504:
	pcmpeqd	%xmm0, %xmm0
	movl	$128, 16(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1505:
	ret
	.p2align 4,,10
	.p2align 3
.L1503:
	movl	$58, %r9d
	cmpl	$57, %edx
	jg	.L1507
.L1466:
	cmpl	%r8d, %edx
	jl	.L1487
	movl	$1, %edx
	cmpl	%r9d, %ecx
	jge	.L1487
	orl	%edx, %eax
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1507:
	movl	$65, %r8d
	cmpl	$64, %edx
	jle	.L1465
	movl	$91, %r9d
	cmpl	$90, %edx
	jle	.L1466
	movl	$95, %r8d
	cmpl	$94, %edx
	jle	.L1465
	movl	$96, %r9d
	cmpl	$95, %edx
	je	.L1466
	movl	$97, %r8d
	cmpl	$96, %edx
	je	.L1465
	cmpl	$122, %edx
	jg	.L1508
	movl	$97, %r8d
	movl	$123, %r9d
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1484:
	movl	$2, %edx
	orl	%edx, %eax
	jmp	.L1464
.L1508:
	cmpl	$1114111, %edx
	jg	.L1464
	movl	$123, %r9d
	movl	$1114112, %r8d
	jmp	.L1465
	.cfi_endproc
.LFE20490:
	.size	_ZN2v88internal22BoyerMoorePositionInfo11SetIntervalERKNS0_8IntervalE, .-_ZN2v88internal22BoyerMoorePositionInfo11SetIntervalERKNS0_8IntervalE
	.section	.text._ZN2v88internal22BoyerMoorePositionInfo6SetAllEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22BoyerMoorePositionInfo6SetAllEv
	.type	_ZN2v88internal22BoyerMoorePositionInfo6SetAllEv, @function
_ZN2v88internal22BoyerMoorePositionInfo6SetAllEv:
.LFB20491:
	.cfi_startproc
	endbr64
	cmpl	$128, 16(%rdi)
	movl	$3, 20(%rdi)
	je	.L1509
	movl	$128, 16(%rdi)
	pcmpeqd	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
.L1509:
	ret
	.cfi_endproc
.LFE20491:
	.size	_ZN2v88internal22BoyerMoorePositionInfo6SetAllEv, .-_ZN2v88internal22BoyerMoorePositionInfo6SetAllEv
	.section	.text._ZN2v88internal19BoyerMooreLookaheadC2EiPNS0_14RegExpCompilerEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19BoyerMooreLookaheadC2EiPNS0_14RegExpCompilerEPNS0_4ZoneE
	.type	_ZN2v88internal19BoyerMooreLookaheadC2EiPNS0_14RegExpCompilerEPNS0_4ZoneE, @function
_ZN2v88internal19BoyerMooreLookaheadC2EiPNS0_14RegExpCompilerEPNS0_4ZoneE:
.LFB20502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	cmpb	$1, 48(%rdx)
	movq	16(%rcx), %r12
	movl	%esi, (%rdi)
	sbbl	%eax, %eax
	movq	%rdx, 8(%rdi)
	andl	$65280, %eax
	addl	$255, %eax
	movl	%eax, 16(%rdi)
	movq	24(%rcx), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L1529
	leaq	16(%r12), %rax
	movq	%rax, 16(%rcx)
.L1514:
	movq	%r12, %r15
	testl	%r14d, %r14d
	jg	.L1530
	movq	$0, (%r12)
	movl	%r14d, 8(%r12)
	movl	$0, 12(%r12)
	movq	%r12, 24(%r13)
.L1511:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1530:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	movslq	%r14d, %rsi
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1531
	addq	%rax, %rsi
	movq	%rsi, 16(%rbx)
.L1517:
	movq	%rax, (%r12)
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	movl	%r14d, 8(%r12)
	movl	$0, 12(%r12)
	movq	%r12, 24(%r13)
.L1526:
	movq	16(%rbx), %r12
	movq	24(%rbx), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L1532
	leaq	24(%r12), %rax
	movq	%rax, 16(%rbx)
.L1519:
	movq	$0, 16(%r12)
	movups	%xmm0, (%r12)
	movslq	12(%r15), %rax
	movl	8(%r15), %edx
	cmpl	%edx, %eax
	jge	.L1520
	movq	(%r15), %rdx
	leal	1(%rax), %esi
	movl	%esi, 12(%r15)
	movq	%r12, (%rdx,%rax,8)
.L1521:
	addl	$1, %ecx
	cmpl	%ecx, %r14d
	je	.L1511
	movq	24(%r13), %r15
	jmp	.L1526
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	16(%rbx), %rdi
	movq	24(%rbx), %rax
	leal	1(%rdx,%rdx), %r8d
	movslq	%r8d, %rsi
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1533
	addq	%rdi, %rsi
	movq	%rsi, 16(%rbx)
.L1523:
	movslq	12(%r15), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L1524
	movq	(%r15), %rsi
	movl	%r8d, -56(%rbp)
	movl	%ecx, -52(%rbp)
	call	memcpy@PLT
	movslq	12(%r15), %rdx
	movl	-52(%rbp), %ecx
	pxor	%xmm0, %xmm0
	movl	-56(%rbp), %r8d
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L1524:
	addl	$1, %eax
	movq	%rdi, (%r15)
	movl	%r8d, 8(%r15)
	movl	%eax, 12(%r15)
	movq	%r12, (%rdi,%rdx)
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1529:
	movl	$16, %esi
	movq	%rcx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1514
	.p2align 4,,10
	.p2align 3
.L1532:
	movl	$24, %esi
	movq	%rbx, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1531:
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1517
	.p2align 4,,10
	.p2align 3
.L1533:
	movq	%rbx, %rdi
	movl	%r8d, -56(%rbp)
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	movl	-56(%rbp), %r8d
	pxor	%xmm0, %xmm0
	movq	%rax, %rdi
	jmp	.L1523
	.cfi_endproc
.LFE20502:
	.size	_ZN2v88internal19BoyerMooreLookaheadC2EiPNS0_14RegExpCompilerEPNS0_4ZoneE, .-_ZN2v88internal19BoyerMooreLookaheadC2EiPNS0_14RegExpCompilerEPNS0_4ZoneE
	.globl	_ZN2v88internal19BoyerMooreLookaheadC1EiPNS0_14RegExpCompilerEPNS0_4ZoneE
	.set	_ZN2v88internal19BoyerMooreLookaheadC1EiPNS0_14RegExpCompilerEPNS0_4ZoneE,_ZN2v88internal19BoyerMooreLookaheadC2EiPNS0_14RegExpCompilerEPNS0_4ZoneE
	.section	.text._ZN2v88internal13AssertionNode17EmitBoundaryCheckEPNS0_14RegExpCompilerEPNS0_5TraceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13AssertionNode17EmitBoundaryCheckEPNS0_14RegExpCompilerEPNS0_5TraceE
	.type	_ZN2v88internal13AssertionNode17EmitBoundaryCheckEPNS0_14RegExpCompilerEPNS0_5TraceE, @function
_ZN2v88internal13AssertionNode17EmitBoundaryCheckEPNS0_14RegExpCompilerEPNS0_5TraceE:
.LFB20447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	92(%rdx), %edx
	movq	40(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L1580
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L1581
.L1554:
	movq	24(%rax), %rax
	movl	64(%r12), %ebx
	movq	(%rax), %rax
	movq	(%rax), %rax
	movl	20(%rax), %eax
	cmpl	$2, %eax
	je	.L1545
.L1578:
	cmpl	$1, %eax
	je	.L1546
.L1539:
	cmpl	$1, 40(%r13)
	movq	$0, -80(%rbp)
	leaq	-80(%rbp), %r11
	movq	$0, -72(%rbp)
	jne	.L1582
.L1547:
	leaq	-72(%rbp), %rax
	movq	%r11, -96(%rbp)
	movl	$87, %esi
	movq	%r15, %rdi
	movq	%rax, %rcx
	movq	(%r15), %rax
	movq	%rcx, -88(%rbp)
	movq	%rcx, %rdx
	call	*200(%rax)
	movq	-96(%rbp), %r11
	testb	%al, %al
	jne	.L1548
	movq	-88(%rbp), %rsi
	movq	%r11, %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_113EmitWordCheckEPNS0_20RegExpMacroAssemblerEPNS0_5LabelES5_b.part.0
	movq	-96(%rbp), %r11
.L1548:
	movq	(%r15), %rax
	movq	%r11, %rsi
	movq	%r15, %rdi
	call	*64(%rax)
	movq	$0, -64(%rbp)
	cmpl	$2, %ebx
	je	.L1583
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13AssertionNode19BacktrackIfPreviousEPNS0_14RegExpCompilerEPNS0_5TraceENS1_10IfPreviousE
	movq	(%r15), %rax
	leaq	-64(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	*224(%rax)
	movq	(%r15), %rax
	movq	-88(%rbp), %rsi
	movq	%r15, %rdi
	call	*64(%rax)
	xorl	%ecx, %ecx
.L1552:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal13AssertionNode19BacktrackIfPreviousEPNS0_14RegExpCompilerEPNS0_5TraceENS1_10IfPreviousE
	movq	(%r15), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	*64(%rax)
	jmp	.L1534
	.p2align 4,,10
	.p2align 3
.L1538:
	movq	48(%r12), %r8
	movq	16(%r15), %r11
	movq	16(%r8), %r10
	movq	24(%r8), %rax
	subq	%r10, %rax
	cmpq	$31, %rax
	jbe	.L1584
	leaq	32(%r10), %rax
	movq	%rax, 16(%r8)
.L1541:
	testl	%edx, %edx
	movl	$8, %esi
	movq	%r14, %rdx
	movq	%r10, %rdi
	sete	-108(%rbp)
	sete	%r9b
	cmpl	$8, %ecx
	cmovle	%ecx, %esi
	movzbl	%r9b, %r9d
	movq	%r8, %rcx
	movq	%r11, -104(%rbp)
	movl	%r9d, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	_ZN2v88internal19BoyerMooreLookaheadC1EiPNS0_14RegExpCompilerEPNS0_4ZoneE
	movq	(%r12), %rax
	leaq	_ZN2v88internal13AssertionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb(%rip), %rdx
	movq	-88(%rbp), %r10
	movl	-96(%rbp), %r9d
	movq	-104(%rbp), %r11
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1542
	cmpl	$1, 64(%r12)
	je	.L1585
.L1558:
	movq	56(%r12), %rdi
	movq	%r10, -88(%rbp)
	movq	%r10, %r8
	xorl	%edx, %edx
	movl	$199, %ecx
	movq	%r11, %rsi
	addq	$4, %rbx
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	-88(%rbp), %r10
	movq	%r10, (%r12,%rbx,8)
	movl	64(%r12), %ebx
.L1543:
	movq	24(%r10), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movl	20(%rax), %eax
	cmpl	$2, %eax
	jne	.L1578
	.p2align 4,,10
	.p2align 3
.L1545:
	cmpl	$2, %ebx
	setne	%cl
.L1579:
	movzbl	%cl, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13AssertionNode19BacktrackIfPreviousEPNS0_14RegExpCompilerEPNS0_5TraceENS1_10IfPreviousE
.L1534:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1586
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1546:
	.cfi_restore_state
	cmpl	$2, %ebx
	sete	%cl
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1580:
	movq	32(%rdi), %rax
	testq	%rax, %rax
	jne	.L1554
	movzbl	26(%r12), %ecx
	xorl	%ebx, %ebx
.L1537:
	testl	%ecx, %ecx
	jne	.L1538
	movl	64(%r12), %ebx
	jmp	.L1539
	.p2align 4,,10
	.p2align 3
.L1582:
	movl	0(%r13), %esi
	movq	%r11, %rdx
	movl	$-1, %r9d
	movq	%r15, %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii@PLT
	movq	-88(%rbp), %r11
	jmp	.L1547
	.p2align 4,,10
	.p2align 3
.L1581:
	movzbl	27(%r12), %ecx
	movl	$1, %ebx
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1583:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13AssertionNode19BacktrackIfPreviousEPNS0_14RegExpCompilerEPNS0_5TraceENS1_10IfPreviousE
	movq	(%r15), %rax
	leaq	-64(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	*224(%rax)
	movq	(%r15), %rax
	movq	-88(%rbp), %rsi
	movq	%r15, %rdi
	call	*64(%rax)
	movl	$1, %ecx
	jmp	.L1552
	.p2align 4,,10
	.p2align 3
.L1585:
	cmpb	$0, -108(%rbp)
	je	.L1558
	movl	$1, %ebx
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	%r10, -88(%rbp)
	movq	%r10, %r8
	movl	$200, %ecx
	xorl	%edx, %edx
	movq	%r11, %rsi
	movq	%r12, %rdi
	call	*%rax
	movl	64(%r12), %ebx
	movq	-88(%rbp), %r10
	jmp	.L1543
.L1584:
	movq	%r8, %rdi
	movl	$32, %esi
	movl	%edx, -108(%rbp)
	movq	%r11, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r8
	movl	-96(%rbp), %ecx
	movq	-104(%rbp), %r11
	movl	-108(%rbp), %edx
	movq	%rax, %r10
	jmp	.L1541
.L1586:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20447:
	.size	_ZN2v88internal13AssertionNode17EmitBoundaryCheckEPNS0_14RegExpCompilerEPNS0_5TraceE, .-_ZN2v88internal13AssertionNode17EmitBoundaryCheckEPNS0_14RegExpCompilerEPNS0_5TraceE
	.section	.text._ZN2v88internal13AssertionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13AssertionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.type	_ZN2v88internal13AssertionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, @function
_ZN2v88internal13AssertionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE:
.LFB20450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	40(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	64(%rdi), %eax
	cmpl	$3, %eax
	ja	.L1588
	cmpl	$1, %eax
	ja	.L1589
	testl	%eax, %eax
	je	.L1603
	movl	92(%rdx), %eax
	testl	%eax, %eax
	je	.L1604
	cmpl	$-1, %eax
	je	.L1605
	.p2align 4,,10
	.p2align 3
.L1593:
	movq	56(%r13), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
.L1587:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1606
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1588:
	.cfi_restore_state
	cmpl	$4, %eax
	jne	.L1593
	movl	(%rdx), %eax
	movdqu	(%rdx), %xmm0
	movq	$0, -168(%rbp)
	leaq	-168(%rbp), %rbx
	movdqu	16(%rdx), %xmm1
	movdqu	32(%rdx), %xmm2
	movdqu	48(%rdx), %xmm3
	movdqu	64(%rdx), %xmm4
	movl	%eax, %r12d
	movl	%eax, %esi
	movdqu	80(%rdx), %xmm5
	movaps	%xmm2, -128(%rbp)
	movq	56(%rdi), %r13
	movl	$0, -120(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	testl	%eax, %eax
	jle	.L1607
.L1596:
	movq	-144(%rbp), %rdx
	xorl	%ecx, %ecx
	testl	%r12d, %r12d
	movq	%r15, %rdi
	setg	%cl
	movl	$-1, %r9d
	subl	$1, %esi
	movl	$1, %r8d
	call	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii@PLT
	movq	(%r15), %rax
	movl	$110, %esi
	movq	%r15, %rdi
	movq	-144(%rbp), %rdx
	call	*200(%rax)
	testb	%al, %al
	jne	.L1597
	cmpb	$0, 48(%r14)
	movq	(%r15), %rax
	jne	.L1598
	movq	%rbx, %rcx
	movl	$65534, %edx
	movl	$8232, %esi
	movq	%r15, %rdi
	call	*80(%rax)
	movq	(%r15), %rax
.L1598:
	movq	%rbx, %rdx
	movl	$10, %esi
	movq	%r15, %rdi
	call	*72(%rax)
	movq	(%r15), %rax
	movq	-144(%rbp), %rdx
	movq	%r15, %rdi
	movl	$13, %esi
	call	*144(%rax)
.L1597:
	movq	(%r15), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	*64(%rax)
	movq	0(%r13), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-160(%rbp), %rdx
	call	*24(%rax)
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1603:
	leaq	-168(%rbp), %rbx
	movl	(%r12), %esi
	movq	%r15, %rdi
	movq	$0, -168(%rbp)
	movq	(%r15), %rax
	movq	%rbx, %rdx
	call	*192(%rax)
	movq	(%r15), %rax
	movq	16(%r12), %rsi
	movq	%r15, %rdi
	call	*224(%rax)
	movq	(%r15), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	*64(%rax)
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1589:
	call	_ZN2v88internal13AssertionNode17EmitBoundaryCheckEPNS0_14RegExpCompilerEPNS0_5TraceE
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1607:
	movq	(%r15), %rax
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	*112(%rax)
	movl	-160(%rbp), %esi
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1604:
	movq	(%r15), %rax
	movq	16(%rdx), %rsi
	movq	%r15, %rdi
	call	*224(%rax)
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	(%r15), %rax
	movl	(%r12), %esi
	movq	%r15, %rdi
	movq	16(%rdx), %rdx
	call	*120(%rax)
	movdqu	(%r12), %xmm6
	movq	56(%r13), %rdi
	movq	%r14, %rsi
	movdqu	16(%r12), %xmm7
	leaq	-160(%rbp), %rdx
	movaps	%xmm6, -160(%rbp)
	movdqu	32(%r12), %xmm6
	movaps	%xmm7, -144(%rbp)
	movdqu	48(%r12), %xmm7
	movaps	%xmm6, -128(%rbp)
	movdqu	64(%r12), %xmm6
	movaps	%xmm7, -112(%rbp)
	movdqu	80(%r12), %xmm7
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movl	$1, -68(%rbp)
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L1587
.L1606:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20450:
	.size	_ZN2v88internal13AssertionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, .-_ZN2v88internal13AssertionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.section	.rodata._ZN2v88internal19BoyerMooreLookahead16FindBestIntervalEiiPiS2_.str1.1,"aMS",@progbits,1
.LC6:
	.string	"bitset::reset"
	.section	.rodata._ZN2v88internal19BoyerMooreLookahead16FindBestIntervalEiiPiS2_.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"%s: __position (which is %zu) >= _Nb (which is %zu)"
	.section	.text._ZN2v88internal19BoyerMooreLookahead16FindBestIntervalEiiPiS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19BoyerMooreLookahead16FindBestIntervalEiiPiS2_
	.type	_ZN2v88internal19BoyerMooreLookahead16FindBestIntervalEiiPiS2_, @function
_ZN2v88internal19BoyerMooreLookahead16FindBestIntervalEiiPiS2_:
.LFB20505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -88(%rbp)
	movl	(%rdi), %r10d
	movq	%r8, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L1631
	movq	24(%rdi), %r15
	movq	%rdi, %r12
	movl	%esi, %r11d
	xorl	%r8d, %r8d
	pxor	%xmm0, %xmm0
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L1625:
	movq	(%r15), %rdi
	movslq	%r8d, %rax
	.p2align 4,,10
	.p2align 3
.L1611:
	movq	(%rdi,%rax,8), %rdx
	movl	%eax, %r9d
	cmpl	16(%rdx), %r11d
	jge	.L1610
	leal	1(%rax), %r9d
	addq	$1, %rax
	cmpl	%eax, %r10d
	jg	.L1611
.L1610:
	cmpl	%r9d, %r10d
	je	.L1631
	movaps	%xmm0, -80(%rbp)
	movslq	%r9d, %rax
	movl	$0, %esi
	movl	$0, %ecx
	jg	.L1616
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1640:
	orq	(%rdx), %rcx
	orq	8(%rdx), %rsi
	leal	1(%rax), %r8d
	addq	$1, %rax
	movq	%rcx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	cmpl	%eax, %r10d
	jle	.L1615
.L1616:
	movq	(%rdi,%rax,8), %rdx
	movl	%eax, %r8d
	cmpl	16(%rdx), %r11d
	jge	.L1640
.L1615:
	xorl	%ebx, %ebx
	jmp	.L1614
	.p2align 4,,10
	.p2align 3
.L1641:
	movl	60(%rax,%rsi,8), %eax
	sall	$7, %eax
	cltd
	idivl	%edi
	leal	1(%rbx,%rax), %ebx
	cmpq	$127, %rsi
	ja	.L1619
.L1620:
	movq	%r13, %rax
	shrq	$6, %rsi
	salq	%cl, %rax
	notq	%rax
	andq	%rax, -80(%rbp,%rsi,8)
.L1614:
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %rsi
	call	_ZN2v88internal12_GLOBAL__N_117BitsetFirstSetBitESt6bitsetILm128EE
	movl	%eax, %ecx
	cmpl	$-1, %eax
	je	.L1617
	movq	8(%r12), %rax
	movslq	%ecx, %rsi
	movl	1084(%rax), %edi
	testl	%edi, %edi
	jg	.L1641
	addl	$2, %ebx
	cmpq	$127, %rsi
	jbe	.L1620
.L1619:
	movq	%rsi, %rdx
	movl	$128, %ecx
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1617:
	movl	%r8d, %edx
	movl	$64, %eax
	subl	%r9d, %edx
	cmpl	$3, %edx
	jle	.L1621
	movq	8(%r12), %rax
	cmpb	$0, 48(%rax)
	je	.L1622
	cmpl	$5, %r9d
	movl	$128, %eax
	movl	$64, %ecx
	cmovl	%ecx, %eax
.L1621:
	subl	%ebx, %eax
	imull	%edx, %eax
	cmpl	%eax, %r14d
	jge	.L1623
	movq	-88(%rbp), %rbx
	leal	-1(%r8), %edx
	movl	%r9d, (%rbx)
	movq	-96(%rbp), %rbx
	movl	%edx, (%rbx)
	movl	(%r12), %r10d
	cmpl	%r10d, %r8d
	jge	.L1608
	movq	24(%r12), %r15
	movl	%eax, %r14d
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1623:
	cmpl	%r8d, %r10d
	jg	.L1625
.L1631:
	movl	%r14d, %eax
.L1608:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1642
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1622:
	.cfi_restore_state
	cmpl	$3, %r9d
	movl	$128, %eax
	movl	$64, %ecx
	cmovl	%ecx, %eax
	jmp	.L1621
.L1639:
	movl	%r9d, %r8d
	jmp	.L1615
.L1642:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20505:
	.size	_ZN2v88internal19BoyerMooreLookahead16FindBestIntervalEiiPiS2_, .-_ZN2v88internal19BoyerMooreLookahead16FindBestIntervalEiiPiS2_
	.section	.text._ZN2v88internal19BoyerMooreLookahead22FindWorthwhileIntervalEPiS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19BoyerMooreLookahead22FindWorthwhileIntervalEPiS2_
	.type	_ZN2v88internal19BoyerMooreLookahead22FindWorthwhileIntervalEPiS2_, @function
_ZN2v88internal19BoyerMooreLookahead22FindWorthwhileIntervalEPiS2_:
.LFB20504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movl	$3, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$4, %ebx
	subq	$8, %rsp
.L1644:
	movl	%ebx, %esi
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal19BoyerMooreLookahead16FindBestIntervalEiiPiS2_
	addl	%ebx, %ebx
	movl	%eax, %edx
	subl	$1, %r12d
	jne	.L1644
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20504:
	.size	_ZN2v88internal19BoyerMooreLookahead22FindWorthwhileIntervalEPiS2_, .-_ZN2v88internal19BoyerMooreLookahead22FindWorthwhileIntervalEPiS2_
	.section	.text._ZN2v88internal19BoyerMooreLookahead12GetSkipTableEiiNS0_6HandleINS0_9ByteArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19BoyerMooreLookahead12GetSkipTableEiiNS0_6HandleINS0_9ByteArrayEEE
	.type	_ZN2v88internal19BoyerMooreLookahead12GetSkipTableEiiNS0_6HandleINS0_9ByteArrayEEE, @function
_ZN2v88internal19BoyerMooreLookahead12GetSkipTableEiiNS0_6HandleINS0_9ByteArrayEEE:
.LFB20506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	movslq	11(%rax), %rdx
	leaq	15(%rax), %rdi
	call	memset@PLT
	cmpl	%r14d, %r13d
	jl	.L1648
	movl	%r13d, %eax
	movslq	%r13d, %rdi
	movl	$1, %edx
	subl	%r14d, %eax
	leaq	0(,%rdi,8), %rsi
	subq	%rax, %rdi
	salq	$3, %rdi
	.p2align 4,,10
	.p2align 3
.L1652:
	movq	24(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax,%rsi), %r8
	movdqu	(%r8), %xmm0
	movq	(%r8), %rcx
	movq	8(%r8), %rax
	movaps	%xmm0, -64(%rbp)
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1657:
	rep bsfq	%rcx, %rcx
	movl	%ecx, %eax
.L1650:
	leal	16(%rax), %r8d
	movq	(%rbx), %r9
	movq	%rdx, %r10
	cltq
	salq	%cl, %r10
	movslq	%r8d, %r8
	shrq	$6, %rax
	movq	%r10, %rcx
	movb	$1, -1(%r8,%r9)
	notq	%rcx
	andq	%rcx, -64(%rbp,%rax,8)
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rcx
.L1653:
	testq	%rcx, %rcx
	jne	.L1657
	testq	%rax, %rax
	je	.L1658
	xorl	%ecx, %ecx
	rep bsfq	%rax, %rcx
	leal	64(%rcx), %eax
	andl	$63, %ecx
	jmp	.L1650
.L1658:
	leaq	-8(%rsi), %rax
	cmpq	%rsi, %rdi
	je	.L1648
	movq	%rax, %rsi
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1648:
	leal	1(%r13), %eax
	subl	%r14d, %eax
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1659
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1659:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20506:
	.size	_ZN2v88internal19BoyerMooreLookahead12GetSkipTableEiiNS0_6HandleINS0_9ByteArrayEEE, .-_ZN2v88internal19BoyerMooreLookahead12GetSkipTableEiiNS0_6HandleINS0_9ByteArrayEEE
	.section	.text._ZN2v88internal19BoyerMooreLookahead20EmitSkipInstructionsEPNS0_20RegExpMacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19BoyerMooreLookahead20EmitSkipInstructionsEPNS0_20RegExpMacroAssemblerE
	.type	_ZN2v88internal19BoyerMooreLookahead20EmitSkipInstructionsEPNS0_20RegExpMacroAssemblerE, @function
_ZN2v88internal19BoyerMooreLookahead20EmitSkipInstructionsEPNS0_20RegExpMacroAssemblerE:
.LFB20507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-76(%rbp), %r15
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$4, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -80(%rbp)
	movl	$0, -76(%rbp)
	movl	$3, -96(%rbp)
.L1661:
	movl	%ebx, %esi
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal19BoyerMooreLookahead16FindBestIntervalEiiPiS2_
	addl	%ebx, %ebx
	subl	$1, -96(%rbp)
	movl	%eax, %edx
	jne	.L1661
	testl	%eax, %eax
	je	.L1660
	movl	-76(%rbp), %r10d
	movl	-80(%rbp), %r8d
	cmpl	%r8d, %r10d
	jl	.L1663
	movq	24(%r13), %rax
	movslq	%r10d, %rdx
	xorl	%edi, %edi
	movq	(%rax), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1665:
	movq	(%r9,%rdx,8), %rsi
	movl	16(%rsi), %ecx
	testl	%ecx, %ecx
	je	.L1664
	cmpl	$1, %ecx
	jg	.L1663
	testb	%dil, %dil
	jne	.L1663
	movdqu	(%rsi), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-88(%rbp), %rsi
	movq	%xmm0, %rdi
	call	_ZN2v88internal12_GLOBAL__N_117BitsetFirstSetBitESt6bitsetILm128EE
	movl	$1, %edi
.L1664:
	subq	$1, %rdx
	cmpl	%edx, %r8d
	jle	.L1665
	leal	1(%r10), %r14d
	subl	%r8d, %r14d
	cmpl	$1, %r14d
	jne	.L1666
	testb	%dil, %dil
	je	.L1663
	cmpl	$2, %r10d
	jle	.L1660
.L1667:
	movq	(%r12), %rdx
	leaq	-64(%rbp), %r15
	movl	%eax, -96(%rbp)
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	$0, -72(%rbp)
	leaq	-72(%rbp), %rbx
	movq	$0, -64(%rbp)
	call	*64(%rdx)
	movl	-76(%rbp), %esi
	movl	$-1, %r9d
	movq	%rbx, %rdx
	movl	$1, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii@PLT
	cmpl	$128, 16(%r13)
	movl	-96(%rbp), %eax
	jg	.L1688
	movq	(%r12), %rcx
	movq	%rbx, %rdx
	movl	%eax, %esi
	movq	%r12, %rdi
	call	*72(%rcx)
.L1669:
	movq	(%r12), %rax
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*40(%rax)
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*224(%rax)
	movq	(%r12), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*64(%rax)
.L1660:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1689
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1666:
	.cfi_restore_state
	testb	%dil, %dil
	jne	.L1667
	.p2align 4,,10
	.p2align 3
.L1663:
	movq	16(%r12), %rdi
	movl	$1, %edx
	movl	$128, %esi
	leaq	-64(%rbp), %rbx
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movl	-76(%rbp), %edx
	movl	-80(%rbp), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	%rax, %r14
	leaq	-72(%rbp), %r13
	call	_ZN2v88internal19BoyerMooreLookahead12GetSkipTableEiiNS0_6HandleINS0_9ByteArrayEEE
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	$0, -72(%rbp)
	movl	%eax, %r15d
	movq	(%r12), %rax
	movq	$0, -64(%rbp)
	call	*64(%rax)
	movl	-76(%rbp), %esi
	movl	$-1, %r9d
	movq	%r13, %rdx
	movl	$1, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii@PLT
	movq	(%r12), %rax
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*184(%rax)
	movq	(%r12), %rax
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	*40(%rax)
	movq	(%r12), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*224(%rax)
	movq	(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*64(%rax)
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1688:
	movq	(%r12), %r8
	movq	%rbx, %rcx
	movl	$127, %edx
	movl	%eax, %esi
	movq	%r12, %rdi
	call	*80(%r8)
	jmp	.L1669
.L1689:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20507:
	.size	_ZN2v88internal19BoyerMooreLookahead20EmitSkipInstructionsEPNS0_20RegExpMacroAssemblerE, .-_ZN2v88internal19BoyerMooreLookahead20EmitSkipInstructionsEPNS0_20RegExpMacroAssemblerE
	.section	.text._ZN2v88internal15GreedyLoopStateC2Eb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15GreedyLoopStateC2Eb
	.type	_ZN2v88internal15GreedyLoopStateC2Eb, @function
_ZN2v88internal15GreedyLoopStateC2Eb:
.LFB20509:
	.cfi_startproc
	endbr64
	movabsq	$-4294967196, %rax
	movq	$0, (%rdi)
	movl	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movb	$0, 64(%rdi)
	movl	$0, 66(%rdi)
	movb	$0, 70(%rdi)
	movl	$0, 72(%rdi)
	movb	$0, 76(%rdi)
	movl	$0, 78(%rdi)
	movb	$0, 82(%rdi)
	movq	$0, 84(%rdi)
	movb	$0, 92(%rdi)
	movq	%rax, 96(%rdi)
	movq	%rdi, 24(%rdi)
	testb	%sil, %sil
	je	.L1690
	movl	$0, 100(%rdi)
.L1690:
	ret
	.cfi_endproc
.LFE20509:
	.size	_ZN2v88internal15GreedyLoopStateC2Eb, .-_ZN2v88internal15GreedyLoopStateC2Eb
	.globl	_ZN2v88internal15GreedyLoopStateC1Eb
	.set	_ZN2v88internal15GreedyLoopStateC1Eb,_ZN2v88internal15GreedyLoopStateC2Eb
	.section	.text._ZN2v88internal10ChoiceNode28AssertGuardsMentionRegistersEPNS0_5TraceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode28AssertGuardsMentionRegistersEPNS0_5TraceE
	.type	_ZN2v88internal10ChoiceNode28AssertGuardsMentionRegistersEPNS0_5TraceE, @function
_ZN2v88internal10ChoiceNode28AssertGuardsMentionRegistersEPNS0_5TraceE:
.LFB20511:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE20511:
	.size	_ZN2v88internal10ChoiceNode28AssertGuardsMentionRegistersEPNS0_5TraceE, .-_ZN2v88internal10ChoiceNode28AssertGuardsMentionRegistersEPNS0_5TraceE
	.section	.text._ZN2v88internal10ChoiceNode12SetUpPreLoadEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_12PreloadStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode12SetUpPreLoadEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_12PreloadStateE
	.type	_ZN2v88internal10ChoiceNode12SetUpPreLoadEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_12PreloadStateE, @function
_ZN2v88internal10ChoiceNode12SetUpPreLoadEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_12PreloadStateE:
.LFB20512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	movl	8(%rcx), %r13d
	cmpl	$-1, %r13d
	je	.L1706
.L1694:
	movq	40(%r14), %rdi
	cmpl	$4, %r13d
	movl	$4, %r12d
	leaq	_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv(%rip), %rdx
	cmovle	%r13d, %r12d
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1697
	movq	32(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	32(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1698
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1697
	movq	32(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	32(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1698
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L1701:
	testb	%al, %al
	je	.L1702
.L1707:
	cmpb	$0, 48(%r14)
	je	.L1703
	cmpl	$3, %r13d
	movl	$2, %eax
	cmove	%eax, %r12d
.L1704:
	movl	%r12d, 4(%rbx)
	cmpl	%r12d, 40(%r15)
	sete	%al
	movb	%al, (%rbx)
	movb	%al, 1(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1698:
	.cfi_restore_state
	call	*%rdx
	testb	%al, %al
	jne	.L1707
.L1702:
	cmpl	$2, %r13d
	movl	$1, %eax
	cmovge	%eax, %r12d
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1706:
	movl	92(%rdx), %eax
	testl	%eax, %eax
	je	.L1708
	movzbl	26(%rdi), %r13d
.L1696:
	movl	%r13d, 8(%rbx)
	jmp	.L1694
	.p2align 4,,10
	.p2align 3
.L1697:
	call	*%rax
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L1703:
	cmpl	$3, %r13d
	movl	$2, %eax
	cmovge	%eax, %r12d
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1708:
	movzbl	27(%rdi), %r13d
	jmp	.L1696
	.cfi_endproc
.LFE20512:
	.size	_ZN2v88internal10ChoiceNode12SetUpPreLoadEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_12PreloadStateE, .-_ZN2v88internal10ChoiceNode12SetUpPreLoadEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_12PreloadStateE
	.section	.text._ZN2v88internal10ChoiceNode29EmitOptimizedUnanchoredSearchEPNS0_14RegExpCompilerEPNS0_5TraceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode29EmitOptimizedUnanchoredSearchEPNS0_14RegExpCompilerEPNS0_5TraceE
	.type	_ZN2v88internal10ChoiceNode29EmitOptimizedUnanchoredSearchEPNS0_14RegExpCompilerEPNS0_5TraceE, @function
_ZN2v88internal10ChoiceNode29EmitOptimizedUnanchoredSearchEPNS0_14RegExpCompilerEPNS0_5TraceE:
.LFB20515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	56(%rdi), %rax
	cmpl	$2, 12(%rax)
	jne	.L1713
	movq	(%rax), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	16(%rax), %rdi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1712
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1713
.L1712:
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*64(%rax)
	cmpq	%rax, %rbx
	jne	.L1713
	movq	32(%rbx), %r13
	movq	40(%r12), %r15
	movl	$-1, %r14d
	testq	%r13, %r13
	je	.L1728
.L1714:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19BoyerMooreLookahead20EmitSkipInstructionsEPNS0_20RegExpMacroAssemblerE
.L1709:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1728:
	.cfi_restore_state
	movzbl	26(%rbx), %eax
	movl	$8, %r14d
	cmpl	$8, %eax
	cmovle	%eax, %r14d
	testl	%eax, %eax
	je	.L1709
	movq	48(%rbx), %rcx
	movq	16(%r15), %r10
	movq	16(%rcx), %r13
	movq	24(%rcx), %rax
	subq	%r13, %rax
	cmpq	$31, %rax
	jbe	.L1729
	leaq	32(%r13), %rax
	movq	%rax, 16(%rcx)
.L1716:
	movq	%r12, %rdx
	movl	%r14d, %esi
	movq	%r13, %rdi
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal19BoyerMooreLookaheadC1EiPNS0_14RegExpCompilerEPNS0_4ZoneE
	movq	56(%rbx), %rax
	movq	-56(%rbp), %r10
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movq	%r13, %r8
	movl	$200, %ecx
	movq	(%rax), %rax
	movq	%r10, %rsi
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*72(%rax)
	testq	%r13, %r13
	je	.L1709
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1713:
	movl	$-1, %r14d
	jmp	.L1709
.L1729:
	movq	%rcx, %rdi
	movl	$32, %esi
	movq	%r10, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r10
	movq	%rax, %r13
	jmp	.L1716
	.cfi_endproc
.LFE20515:
	.size	_ZN2v88internal10ChoiceNode29EmitOptimizedUnanchoredSearchEPNS0_14RegExpCompilerEPNS0_5TraceE, .-_ZN2v88internal10ChoiceNode29EmitOptimizedUnanchoredSearchEPNS0_14RegExpCompilerEPNS0_5TraceE
	.section	.text._ZN2v88internal10ChoiceNode11EmitChoicesEPNS0_14RegExpCompilerEPNS0_25AlternativeGenerationListEiPNS0_5TraceEPNS0_12PreloadStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode11EmitChoicesEPNS0_14RegExpCompilerEPNS0_25AlternativeGenerationListEiPNS0_5TraceEPNS0_12PreloadStateE
	.type	_ZN2v88internal10ChoiceNode11EmitChoicesEPNS0_14RegExpCompilerEPNS0_25AlternativeGenerationListEiPNS0_5TraceEPNS0_12PreloadStateE, @function
_ZN2v88internal10ChoiceNode11EmitChoicesEPNS0_14RegExpCompilerEPNS0_25AlternativeGenerationListEiPNS0_5TraceEPNS0_12PreloadStateE:
.LFB20516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$216, %rsp
	movq	%rsi, -200(%rbp)
	movq	40(%rsi), %r14
	movq	%rdx, -224(%rbp)
	movq	%r8, %rdx
	movl	%ecx, -216(%rbp)
	movq	%r9, %rcx
	movq	%rdi, -184(%rbp)
	movq	%r9, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal10ChoiceNode12SetUpPreLoadEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_12PreloadStateE
	movq	56(%r13), %rcx
	movl	88(%rbx), %eax
	movl	12(%rcx), %esi
	cltd
	idivl	%esi
	movl	%esi, -212(%rbp)
	movl	%eax, -244(%rbp)
	cmpl	%esi, %r15d
	jge	.L1730
	leal	-1(%rsi), %eax
	movl	%r15d, -164(%rbp)
	movl	%eax, -168(%rbp)
	movslq	%r15d, %rax
	salq	$3, %rax
	movq	%rax, -192(%rbp)
	jmp	.L1756
	.p2align 4,,10
	.p2align 3
.L1744:
	movl	-168(%rbp), %esi
	cmpl	%esi, -164(%rbp)
	je	.L1779
.L1748:
	addl	$1, -164(%rbp)
	movl	-164(%rbp), %eax
	addq	$8, -192(%rbp)
	cmpl	%eax, -212(%rbp)
	je	.L1730
	movq	-184(%rbp), %rax
	movq	56(%rax), %rcx
.L1756:
	movq	(%rcx), %rax
	movq	-192(%rbp), %rdi
	xorl	%r15d, %r15d
	leaq	(%rax,%rdi,2), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r13
	movq	-224(%rbp), %rax
	movq	%rcx, -208(%rbp)
	movq	(%rax), %rax
	movq	(%rax,%rdi), %r12
	movq	-176(%rbp), %rax
	movl	4(%rax), %eax
	movl	%eax, 20(%r12)
	testq	%r13, %r13
	je	.L1732
	movl	12(%r13), %r15d
.L1732:
	movq	-176(%rbp), %rsi
	movl	$0, %ecx
	movdqu	(%rbx), %xmm0
	movdqu	16(%rbx), %xmm1
	movdqu	32(%rbx), %xmm2
	movzbl	(%rsi), %edx
	movdqu	48(%rbx), %xmm3
	movaps	%xmm0, -160(%rbp)
	movdqu	64(%rbx), %xmm4
	movdqu	80(%rbx), %xmm5
	movaps	%xmm2, -128(%rbp)
	testb	%dl, %dl
	movaps	%xmm1, -144(%rbp)
	cmovne	%eax, %ecx
	cmpb	$0, 1(%rsi)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movl	%ecx, -120(%rbp)
	movaps	%xmm5, -80(%rbp)
	je	.L1734
	movl	%eax, -116(%rbp)
.L1734:
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jle	.L1739
	movl	$0, -108(%rbp)
	movb	$0, -104(%rbp)
	cmpl	$1, %eax
	je	.L1739
	movl	$0, -102(%rbp)
	movb	$0, -98(%rbp)
	cmpl	$2, %eax
	je	.L1739
	movl	$0, -96(%rbp)
	movb	$0, -92(%rbp)
	cmpl	$3, %eax
	je	.L1739
	movl	$0, -90(%rbp)
	movb	$0, -86(%rbp)
.L1739:
	movq	-184(%rbp), %rax
	movl	$0, -112(%rbp)
	cmpb	$0, 64(%rax)
	je	.L1737
	movl	$0, -68(%rbp)
.L1737:
	movl	-168(%rbp), %edi
	cmpl	%edi, -164(%rbp)
	je	.L1740
	leaq	12(%r12), %rax
	movq	%rax, -144(%rbp)
.L1740:
	movq	-200(%rbp), %rax
	movb	%dl, 8(%r12)
	cmpb	$0, 51(%rax)
	je	.L1741
	movq	-184(%rbp), %rdi
	movl	-164(%rbp), %edx
	xorl	%esi, %esi
	movq	(%rdi), %rax
	testl	%edx, %edx
	sete	%sil
	call	*88(%rax)
	testb	%al, %al
	jne	.L1780
.L1741:
	cmpb	$0, 56(%r12)
	jne	.L1744
	leaq	-160(%rbp), %rax
	movl	-164(%rbp), %esi
	movq	%rax, -232(%rbp)
	cmpl	%esi, -216(%rbp)
	je	.L1746
	movb	$0, 8(%r12)
	movl	$0, -120(%rbp)
.L1746:
	cmpq	$0, -152(%rbp)
	je	.L1749
	movl	-244(%rbp), %eax
	movl	%eax, -72(%rbp)
.L1749:
	testl	%r15d, %r15d
	jle	.L1754
	leal	-1(%r15), %eax
	movq	%rbx, -240(%rbp)
	xorl	%r15d, %r15d
	movq	%r13, %rbx
	leaq	8(,%rax,8), %rax
	movq	%rax, %r13
	jmp	.L1755
	.p2align 4,,10
	.p2align 3
.L1782:
	cmpl	$1, %edx
	jne	.L1753
	movq	(%r14), %r9
	movl	8(%rax), %edx
	movq	%r14, %rdi
	movq	-144(%rbp), %rcx
	movl	(%rax), %esi
	call	*240(%r9)
.L1753:
	addq	$8, %r15
	cmpq	%r15, %r13
	je	.L1781
.L1755:
	movq	(%rbx), %rax
	movq	(%rax,%r15), %rax
	movl	4(%rax), %edx
	testl	%edx, %edx
	jne	.L1782
	movq	(%r14), %r9
	addq	$8, %r15
	movl	8(%rax), %edx
	movq	%r14, %rdi
	movq	-144(%rbp), %rcx
	movl	(%rax), %esi
	call	*232(%r9)
	cmpq	%r15, %r13
	jne	.L1755
.L1781:
	movq	-240(%rbp), %rbx
.L1754:
	movq	-208(%rbp), %rdi
	movq	-232(%rbp), %rdx
	movq	-200(%rbp), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-176(%rbp), %rax
	movb	$0, (%rax)
.L1743:
	movq	(%r14), %rax
	leaq	12(%r12), %rsi
	movq	%r14, %rdi
	call	*64(%rax)
	jmp	.L1748
	.p2align 4,,10
	.p2align 3
.L1780:
	subq	$8, %rsp
	movl	-168(%rbp), %esi
	xorl	%eax, %eax
	movq	%r12, %r9
	cmpl	%esi, -164(%rbp)
	pushq	-184(%rbp)
	leaq	-160(%rbp), %rcx
	movq	%rbx, %rdx
	setne	%al
	movq	-176(%rbp), %rdi
	movq	%rcx, -232(%rbp)
	pushq	%rax
	leaq	20(%r12), %rax
	movq	-200(%rbp), %rsi
	pushq	%rax
	movzbl	1(%rdi), %r8d
	movq	-208(%rbp), %rdi
	call	_ZN2v88internal10RegExpNode14EmitQuickCheckEPNS0_14RegExpCompilerEPNS0_5TraceES5_bPNS0_5LabelEPNS0_17QuickCheckDetailsEbPNS0_10ChoiceNodeE
	addq	$32, %rsp
	testb	%al, %al
	je	.L1741
	movq	-176(%rbp), %rdi
	movl	$257, %eax
	movl	-168(%rbp), %esi
	movw	%ax, (%rdi)
	cmpl	%esi, -164(%rbp)
	jne	.L1743
	movq	(%r14), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*64(%rax)
	movdqu	20(%r12), %xmm6
	movaps	%xmm6, -112(%rbp)
	movdqu	36(%r12), %xmm7
	movaps	%xmm7, -96(%rbp)
	movl	52(%r12), %eax
	movl	%eax, -80(%rbp)
	movzbl	56(%r12), %eax
	movb	%al, -76(%rbp)
	movq	-176(%rbp), %rax
	movl	4(%rax), %eax
	movl	%eax, -120(%rbp)
	movl	%eax, -116(%rbp)
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L1730:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1783
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1779:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	call	*224(%rax)
	jmp	.L1748
.L1783:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20516:
	.size	_ZN2v88internal10ChoiceNode11EmitChoicesEPNS0_14RegExpCompilerEPNS0_25AlternativeGenerationListEiPNS0_5TraceEPNS0_12PreloadStateE, .-_ZN2v88internal10ChoiceNode11EmitChoicesEPNS0_14RegExpCompilerEPNS0_25AlternativeGenerationListEiPNS0_5TraceEPNS0_12PreloadStateE
	.section	.text._ZN2v88internal10ChoiceNode14EmitGreedyLoopEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_25AlternativeGenerationListEPNS0_12PreloadStateEPNS0_15GreedyLoopStateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode14EmitGreedyLoopEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_25AlternativeGenerationListEPNS0_12PreloadStateEPNS0_15GreedyLoopStateEi
	.type	_ZN2v88internal10ChoiceNode14EmitGreedyLoopEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_25AlternativeGenerationListEPNS0_12PreloadStateEPNS0_15GreedyLoopStateEi, @function
_ZN2v88internal10ChoiceNode14EmitGreedyLoopEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_25AlternativeGenerationListEPNS0_12PreloadStateEPNS0_15GreedyLoopStateEi:
.LFB20514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	%rdx, -200(%rbp)
	movq	40(%rsi), %r14
	movq	%rcx, -208(%rbp)
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r14), %rax
	call	*296(%rax)
	pxor	%xmm0, %xmm0
	movabsq	$-4294967196, %rax
	cmpb	$0, 64(%rbx)
	movq	$0, -184(%rbp)
	movl	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -120(%rbp)
	movl	$0, -112(%rbp)
	movl	$0, -108(%rbp)
	movb	$0, -104(%rbp)
	movl	$0, -102(%rbp)
	movb	$0, -98(%rbp)
	movl	$0, -96(%rbp)
	movb	$0, -92(%rbp)
	movl	$0, -90(%rbp)
	movb	$0, -86(%rbp)
	movq	$0, -84(%rbp)
	movb	$0, -76(%rbp)
	movq	%rax, -72(%rbp)
	movups	%xmm0, -136(%rbp)
	je	.L1785
	movl	$0, -68(%rbp)
.L1785:
	leaq	-184(%rbp), %r8
	leaq	-176(%rbp), %rax
	movq	%r14, %rdi
	movq	$0, -176(%rbp)
	movq	%r8, -144(%rbp)
	movq	(%r14), %rdx
	movq	%rax, %rsi
	movq	%r8, -224(%rbp)
	movq	%rax, -216(%rbp)
	call	*64(%rdx)
	movq	-216(%rbp), %rax
	movq	%rbx, -136(%rbp)
	movq	%r12, %rsi
	leaq	-160(%rbp), %rdx
	movq	%rax, -128(%rbp)
	movq	56(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	(%r14), %rax
	movq	-224(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	*64(%rax)
	leaq	-168(%rbp), %r10
	movq	%r14, %rdi
	movq	$0, -168(%rbp)
	movq	(%r14), %rdx
	movq	%r10, %rsi
	movq	%r10, -224(%rbp)
	call	*64(%rdx)
	movq	-208(%rbp), %rdx
	movq	%r15, %r9
	movq	%r12, %rsi
	leaq	8(%r13), %r8
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal10ChoiceNode11EmitChoicesEPNS0_14RegExpCompilerEPNS0_25AlternativeGenerationListEiPNS0_5TraceEPNS0_12PreloadStateE
	movq	(%r14), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*64(%rdx)
	movq	-200(%rbp), %rax
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rax), %rsi
	call	*104(%rdx)
	movl	16(%rbp), %esi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	negl	%esi
	call	*40(%rdx)
	movq	-224(%rbp), %r10
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	%r10, %rsi
	call	*224(%rdx)
	movq	-216(%rbp), %r8
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1788
	addq	$184, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1788:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20514:
	.size	_ZN2v88internal10ChoiceNode14EmitGreedyLoopEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_25AlternativeGenerationListEPNS0_12PreloadStateEPNS0_15GreedyLoopStateEi, .-_ZN2v88internal10ChoiceNode14EmitGreedyLoopEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_25AlternativeGenerationListEPNS0_12PreloadStateEPNS0_15GreedyLoopStateEi
	.section	.text._ZN2v88internal10ChoiceNode25EmitOutOfLineContinuationEPNS0_14RegExpCompilerEPNS0_5TraceENS0_18GuardedAlternativeEPNS0_21AlternativeGenerationEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode25EmitOutOfLineContinuationEPNS0_14RegExpCompilerEPNS0_5TraceENS0_18GuardedAlternativeEPNS0_21AlternativeGenerationEib
	.type	_ZN2v88internal10ChoiceNode25EmitOutOfLineContinuationEPNS0_14RegExpCompilerEPNS0_5TraceENS0_18GuardedAlternativeEPNS0_21AlternativeGenerationEib, @function
_ZN2v88internal10ChoiceNode25EmitOutOfLineContinuationEPNS0_14RegExpCompilerEPNS0_5TraceENS0_18GuardedAlternativeEPNS0_21AlternativeGenerationEib:
.LFB20517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	24(%rbp), %eax
	movq	%rsi, -192(%rbp)
	movq	%rcx, -200(%rbp)
	movl	%eax, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	testl	%eax, %eax
	jle	.L1789
	movq	40(%rsi), %r14
	movq	%rdx, %r15
	movq	%r9, %r12
	movq	%rdi, %rbx
	movq	%r8, %r13
	movq	%r9, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	call	*64(%rdx)
	movl	52(%r12), %edx
	movdqu	80(%r15), %xmm3
	movl	16(%rbp), %eax
	movdqu	(%r15), %xmm0
	movaps	%xmm3, -80(%rbp)
	movdqu	16(%r15), %xmm1
	movdqu	32(%r15), %xmm2
	movdqu	20(%r12), %xmm4
	movdqu	36(%r12), %xmm5
	movl	%edx, -80(%rbp)
	movzbl	56(%r12), %edx
	cmpb	$0, 64(%rbx)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm0, -160(%rbp)
	movl	%eax, -120(%rbp)
	movl	-184(%rbp), %eax
	movb	%dl, -76(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	jne	.L1812
.L1791:
	leaq	12(%r12), %rcx
	movq	%rcx, -184(%rbp)
	testq	%r13, %r13
	je	.L1813
	movl	12(%r13), %edx
	testb	%al, %al
	je	.L1795
	leaq	-168(%rbp), %rax
	movq	$0, -168(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -144(%rbp)
	testl	%edx, %edx
	jle	.L1794
	subl	$1, %edx
	xorl	%r12d, %r12d
	leaq	8(,%rdx,8), %rbx
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1814:
	cmpl	$1, %edx
	jne	.L1799
	movq	(%r14), %r11
	movl	8(%rsi), %edx
	movq	%r14, %rdi
	movq	-144(%rbp), %rcx
	movl	(%rsi), %esi
	call	*240(%r11)
.L1799:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L1794
.L1801:
	movq	0(%r13), %rdx
	movq	(%rdx,%r12), %rsi
	movl	4(%rsi), %edx
	testl	%edx, %edx
	jne	.L1814
	movq	(%r14), %r11
	addq	$8, %r12
	movl	8(%rsi), %edx
	movq	%r14, %rdi
	movq	-144(%rbp), %rcx
	movl	(%rsi), %esi
	call	*232(%r11)
	cmpq	%r12, %rbx
	jne	.L1801
.L1794:
	movq	-200(%rbp), %rdi
	movq	-192(%rbp), %rsi
	leaq	-160(%rbp), %rdx
	movq	(%rdi), %rcx
	call	*24(%rcx)
	movq	(%r14), %rdx
	movq	-208(%rbp), %rsi
	movq	%r14, %rdi
	call	*64(%rdx)
	movl	(%r15), %esi
	movl	16(%rbp), %r8d
	movq	%r14, %rdi
	movl	$-1, %r9d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii@PLT
	movq	(%r14), %rax
	movq	-184(%rbp), %rsi
	movq	%r14, %rdi
	call	*224(%rax)
.L1789:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1815
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1812:
	.cfi_restore_state
	movl	$0, -68(%rbp)
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1795:
	movq	%rcx, -144(%rbp)
	testl	%edx, %edx
	jle	.L1803
	leal	-1(%rdx), %eax
	xorl	%ebx, %ebx
	leaq	8(,%rax,8), %r12
	jmp	.L1809
	.p2align 4,,10
	.p2align 3
.L1816:
	cmpl	$1, %edx
	jne	.L1807
	movq	(%r14), %r9
	movl	8(%rax), %edx
	movq	%r14, %rdi
	movq	-144(%rbp), %rcx
	movl	(%rax), %esi
	call	*240(%r9)
.L1807:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L1803
.L1809:
	movq	0(%r13), %rax
	movq	(%rax,%rbx), %rax
	movl	4(%rax), %edx
	testl	%edx, %edx
	jne	.L1816
	movq	(%r14), %r9
	addq	$8, %rbx
	movl	8(%rax), %edx
	movq	%r14, %rdi
	movq	-144(%rbp), %rcx
	movl	(%rax), %esi
	call	*232(%r9)
	cmpq	%rbx, %r12
	jne	.L1809
.L1803:
	movq	-200(%rbp), %rdi
	movq	-192(%rbp), %rsi
	leaq	-160(%rbp), %rdx
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L1789
	.p2align 4,,10
	.p2align 3
.L1813:
	testb	%al, %al
	je	.L1793
	leaq	-168(%rbp), %rax
	movq	$0, -168(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L1793:
	movq	%rcx, -144(%rbp)
	jmp	.L1803
.L1815:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20517:
	.size	_ZN2v88internal10ChoiceNode25EmitOutOfLineContinuationEPNS0_14RegExpCompilerEPNS0_5TraceENS0_18GuardedAlternativeEPNS0_21AlternativeGenerationEib, .-_ZN2v88internal10ChoiceNode25EmitOutOfLineContinuationEPNS0_14RegExpCompilerEPNS0_5TraceENS0_18GuardedAlternativeEPNS0_21AlternativeGenerationEib
	.section	.text._ZN2v88internal8TextNode16CalculateOffsetsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode16CalculateOffsetsEv
	.type	_ZN2v88internal8TextNode16CalculateOffsetsEv, @function
_ZN2v88internal8TextNode16CalculateOffsetsEv:
.LFB20520:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdx
	movl	12(%rdx), %eax
	testl	%eax, %eax
	jle	.L1817
	leal	-1(%rax), %r8d
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	salq	$4, %r8
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1819:
	movq	8(%rdx), %rdx
	movl	16(%rdx), %edx
	addl	%edx, %ecx
	cmpq	%rax, %r8
	je	.L1817
.L1829:
	movq	64(%rdi), %rdx
	addq	$16, %rax
.L1822:
	movq	(%rdx), %rsi
	addq	%rax, %rsi
	movl	%ecx, (%rsi)
	movq	%rsi, %rdx
	movl	4(%rsi), %esi
	testl	%esi, %esi
	je	.L1819
	cmpl	$1, %esi
	jne	.L1828
	movl	$1, %edx
	addl	%edx, %ecx
	cmpq	%rax, %r8
	jne	.L1829
.L1817:
	ret
.L1828:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20520:
	.size	_ZN2v88internal8TextNode16CalculateOffsetsEv, .-_ZN2v88internal8TextNode16CalculateOffsetsEv
	.section	.text._ZN2v88internal13AnalyzeRegExpEPNS0_7IsolateEbPNS0_10RegExpNodeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal13AnalyzeRegExpEPNS0_7IsolateEbPNS0_10RegExpNodeE
	.type	_ZN2v88internal13AnalyzeRegExpEPNS0_7IsolateEbPNS0_10RegExpNodeE, @function
_ZN2v88internal13AnalyzeRegExpEPNS0_7IsolateEbPNS0_10RegExpNodeE:
.LFB20553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEEE(%rip), %rax
	movq	%rdi, -56(%rbp)
	movq	%rax, -64(%rbp)
	movb	%sil, -48(%rbp)
	movq	$0, -40(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	%rax, %r8
	leaq	.LC1(%rip), %rax
	cmpq	37528(%r12), %r8
	jb	.L1830
	movzbl	25(%rbx), %eax
	testb	$2, %al
	jne	.L1837
	testb	$1, %al
	jne	.L1837
	orl	$1, %eax
	leaq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	movb	%al, 25(%rbx)
	movq	(%rbx), %rax
	call	*16(%rax)
	movzbl	25(%rbx), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 25(%rbx)
.L1837:
	movq	-40(%rbp), %rax
.L1830:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1838
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1838:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20553:
	.size	_ZN2v88internal13AnalyzeRegExpEPNS0_7IsolateEbPNS0_10RegExpNodeE, .-_ZN2v88internal13AnalyzeRegExpEPNS0_7IsolateEbPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal8ZoneListINS0_11TextElementEE3AddERKS2_PNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8ZoneListINS0_11TextElementEE3AddERKS2_PNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8ZoneListINS0_11TextElementEE3AddERKS2_PNS0_4ZoneE
	.type	_ZN2v88internal8ZoneListINS0_11TextElementEE3AddERKS2_PNS0_4ZoneE, @function
_ZN2v88internal8ZoneListINS0_11TextElementEE3AddERKS2_PNS0_4ZoneE:
.LFB21075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movslq	12(%rdi), %rax
	movq	%rdi, %rbx
	movl	8(%rdi), %ecx
	cmpl	%ecx, %eax
	jge	.L1840
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%rdi), %rax
	movl	%edx, 12(%rdi)
	movdqu	(%rsi), %xmm0
	movups	%xmm0, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1840:
	.cfi_restore_state
	leal	1(%rcx,%rcx), %r14d
	movq	24(%rdx), %rax
	movq	16(%rdx), %rcx
	movq	8(%rsi), %r12
	movq	(%rsi), %r13
	movslq	%r14d, %rsi
	salq	$4, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1846
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdx)
.L1843:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rsi
	salq	$4, %rdx
	testl	%esi, %esi
	jg	.L1847
.L1844:
	movq	%rcx, (%rbx)
	addl	$1, %esi
	addq	%rdx, %rcx
	movl	%r14d, 8(%rbx)
	movl	%esi, 12(%rbx)
	movq	%r13, (%rcx)
	movq	%r12, 8(%rcx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1847:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rsi
	salq	$4, %rdx
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1846:
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1843
	.cfi_endproc
.LFE21075:
	.size	_ZN2v88internal8ZoneListINS0_11TextElementEE3AddERKS2_PNS0_4ZoneE, .-_ZN2v88internal8ZoneListINS0_11TextElementEE3AddERKS2_PNS0_4ZoneE
	.section	.text._ZN2v88internal8TextNode22CreateForSurrogatePairEPNS0_4ZoneENS0_14CharacterRangeES4_bPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode22CreateForSurrogatePairEPNS0_4ZoneENS0_14CharacterRangeES4_bPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.type	_ZN2v88internal8TextNode22CreateForSurrogatePairEPNS0_4ZoneENS0_14CharacterRangeES4_bPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, @function
_ZN2v88internal8TextNode22CreateForSurrogatePairEPNS0_4ZoneENS0_14CharacterRangeES4_bPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE:
.LFB20457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	24(%rdi), %rsi
	movq	16(%rdi), %r15
	movl	%ecx, -92(%rbp)
	movq	%r8, -88(%rbp)
	movl	%r9d, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L1869
	leaq	16(%r15), %rax
	subq	%rax, %rsi
	movq	%rax, 16(%rdi)
	cmpq	$7, %rsi
	jbe	.L1870
.L1851:
	leaq	8(%rax), %rsi
	movq	%rsi, 16(%r12)
.L1852:
	movabsq	$4294967297, %rdi
	movq	%rax, (%r15)
	movq	%rdi, 8(%r15)
	movq	%r13, (%rax)
	movq	24(%r12), %rsi
	movq	16(%r12), %r13
	movq	%rsi, %rax
	subq	%r13, %rax
	cmpq	$15, %rax
	jbe	.L1871
	leaq	16(%r13), %rax
	subq	%rax, %rsi
	movq	%rax, 16(%r12)
	cmpq	$7, %rsi
	jbe	.L1872
.L1855:
	leaq	8(%rax), %rsi
	movq	%rsi, 16(%r12)
.L1856:
	movabsq	$4294967297, %rdi
	movq	%rax, 0(%r13)
	movq	%rdi, 8(%r13)
	movq	%rbx, (%rax)
	movq	24(%r12), %rsi
	movq	16(%r12), %rbx
	movq	%rsi, %rax
	subq	%rbx, %rax
	cmpq	$15, %rax
	jbe	.L1873
	leaq	16(%rbx), %rax
	subq	%rax, %rsi
	movq	%rax, 16(%r12)
	cmpq	$31, %rsi
	jbe	.L1874
.L1859:
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%r12)
.L1860:
	movq	%rax, (%rbx)
	movq	$2, 8(%rbx)
	movq	16(%r12), %r14
	movq	24(%r12), %rdx
	subq	%r14, %rdx
	cmpq	$31, %rdx
	jbe	.L1875
	leaq	32(%r14), %rdx
	movq	%rdx, 16(%r12)
.L1862:
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movl	%ecx, -96(%rbp)
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movq	%r14, -72(%rbp)
	leaq	-80(%rbp), %r14
	movq	%r12, %rdx
	movabsq	$8589934591, %rax
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8ZoneListINS0_11TextElementEE3AddERKS2_PNS0_4ZoneE
	movq	16(%r12), %r15
	movq	24(%r12), %rax
	movl	-96(%rbp), %ecx
	subq	%r15, %rax
	cmpq	$31, %rax
	jbe	.L1876
	leaq	32(%r15), %rax
	movq	%rax, 16(%r12)
.L1864:
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movabsq	$8589934591, %rax
	movq	%r15, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8ZoneListINS0_11TextElementEE3AddERKS2_PNS0_4ZoneE
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$79, %rdx
	jbe	.L1877
	leaq	80(%rax), %rdx
	movq	%rdx, 16(%r12)
.L1866:
	movq	-88(%rbp), %rcx
	leaq	16+_ZTVN2v88internal8TextNodeE(%rip), %rdi
	pxor	%xmm1, %xmm1
	movq	48(%rcx), %xmm0
	movq	%rbx, 64(%rax)
	movzbl	-92(%rbp), %ebx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movhps	-88(%rbp), %xmm0
	movq	$0, 24(%rax)
	movq	%rdi, (%rax)
	movb	%bl, 72(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1878
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1869:
	.cfi_restore_state
	movl	$16, %esi
	movl	%r9d, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r12), %rsi
	movl	-96(%rbp), %ecx
	movq	%rax, %r15
	movq	16(%r12), %rax
	subq	%rax, %rsi
	cmpq	$7, %rsi
	ja	.L1851
	.p2align 4,,10
	.p2align 3
.L1870:
	movl	$8, %esi
	movq	%r12, %rdi
	movl	%ecx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-96(%rbp), %ecx
	jmp	.L1852
	.p2align 4,,10
	.p2align 3
.L1871:
	movl	$16, %esi
	movq	%r12, %rdi
	movl	%ecx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r12), %rsi
	movl	-96(%rbp), %ecx
	movq	%rax, %r13
	movq	16(%r12), %rax
	subq	%rax, %rsi
	cmpq	$7, %rsi
	ja	.L1855
	.p2align 4,,10
	.p2align 3
.L1872:
	movl	$8, %esi
	movq	%r12, %rdi
	movl	%ecx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-96(%rbp), %ecx
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1873:
	movl	$16, %esi
	movq	%r12, %rdi
	movl	%ecx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r12), %rsi
	movl	-96(%rbp), %ecx
	movq	%rax, %rbx
	movq	16(%r12), %rax
	subq	%rax, %rsi
	cmpq	$31, %rsi
	ja	.L1859
	.p2align 4,,10
	.p2align 3
.L1874:
	movl	$32, %esi
	movq	%r12, %rdi
	movl	%ecx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-96(%rbp), %ecx
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1875:
	movl	$32, %esi
	movq	%r12, %rdi
	movl	%ecx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-96(%rbp), %ecx
	movq	%rax, %r14
	jmp	.L1862
	.p2align 4,,10
	.p2align 3
.L1876:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-96(%rbp), %ecx
	movq	%rax, %r15
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L1877:
	movl	$80, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1866
.L1878:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20457:
	.size	_ZN2v88internal8TextNode22CreateForSurrogatePairEPNS0_4ZoneENS0_14CharacterRangeES4_bPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, .-_ZN2v88internal8TextNode22CreateForSurrogatePairEPNS0_4ZoneENS0_14CharacterRangeES4_bPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.section	.text._ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	.type	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE, @function
_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE:
.LFB21081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movslq	12(%rdi), %rax
	movq	%rdi, %rbx
	movl	8(%rdi), %ecx
	cmpl	%ecx, %eax
	jge	.L1880
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%rdi), %rax
	movl	%edx, 12(%rdi)
	movdqu	(%rsi), %xmm1
	movups	%xmm1, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1880:
	.cfi_restore_state
	leal	1(%rcx,%rcx), %r14d
	movq	24(%rdx), %rax
	movq	16(%rdx), %rcx
	movq	(%rsi), %r12
	movq	8(%rsi), %r13
	movslq	%r14d, %rsi
	salq	$4, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1886
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdx)
.L1883:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rax
	salq	$4, %rdx
	testl	%eax, %eax
	jg	.L1887
.L1884:
	movq	%r12, %xmm0
	movq	%r13, %xmm2
	addl	$1, %eax
	movq	%rcx, (%rbx)
	punpcklqdq	%xmm2, %xmm0
	movl	%r14d, 8(%rbx)
	movl	%eax, 12(%rbx)
	movups	%xmm0, (%rcx,%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1887:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$4, %rdx
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1886:
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1883
	.cfi_endproc
.LFE21081:
	.size	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE, .-_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	.section	.text._ZN2v88internal10ChoiceNode13FilterOneByteEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode13FilterOneByteEi
	.type	_ZN2v88internal10ChoiceNode13FilterOneByteEi, @function
_ZN2v88internal10ChoiceNode13FilterOneByteEi:
.LFB20439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	25(%rdi), %eax
	testb	%al, %al
	jns	.L1889
	movq	8(%rdi), %r15
.L1888:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1889:
	.cfi_restore_state
	testl	%esi, %esi
	js	.L1892
	testb	$64, %al
	jne	.L1892
	orl	$64, %eax
	movb	%al, 25(%rdi)
	movq	56(%rdi), %rax
	movl	12(%rax), %ebx
	movl	%ebx, -68(%rbp)
	testl	%ebx, %ebx
	jle	.L1909
	leal	-1(%rbx), %ecx
	movq	(%rax), %rdi
	movq	%rcx, -80(%rbp)
	salq	$4, %rcx
	movq	%rcx, -56(%rbp)
	leaq	8(%rdi), %rax
	leaq	24(%rdi,%rcx), %r8
	.p2align 4,,10
	.p2align 3
.L1896:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1894
	movl	12(%rdx), %edx
	testl	%edx, %edx
	jne	.L1921
.L1894:
	addq	$16, %rax
	cmpq	%rax, %r8
	jne	.L1896
	movq	$0, -64(%rbp)
	leal	-1(%rsi), %ebx
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L1900
	.p2align 4,,10
	.p2align 3
.L1922:
	movq	56(%r13), %rax
	addl	$1, %r12d
	movq	(%rax), %rax
	movq	%r15, (%rax,%r14)
	cmpq	%r14, -56(%rbp)
	je	.L1898
	movq	%r15, -64(%rbp)
.L1899:
	movq	56(%r13), %rax
	addq	$16, %r14
	movq	(%rax), %rdi
.L1900:
	movq	(%rdi,%r14), %rdi
	movl	%ebx, %esi
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L1922
	cmpq	%r14, -56(%rbp)
	jne	.L1899
	movq	-64(%rbp), %r15
.L1898:
	cmpl	$1, %r12d
	jle	.L1893
	orb	$-128, 25(%r13)
	movq	%r13, %r15
	movq	%r13, 8(%r13)
	cmpl	%r12d, -68(%rbp)
	je	.L1895
	movq	48(%r13), %r14
	movq	16(%r14), %rax
	movq	%rax, %rcx
	movq	%rax, -56(%rbp)
	movq	24(%r14), %rax
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	$15, %rdx
	jbe	.L1923
	leaq	16(%rcx), %rdx
	movq	%rdx, 16(%r14)
.L1903:
	movslq	%r12d, %rsi
	subq	%rdx, %rax
	salq	$4, %rsi
	cmpq	%rax, %rsi
	ja	.L1924
	addq	%rdx, %rsi
	movq	%rsi, 16(%r14)
.L1904:
	movq	-56(%rbp), %rax
	xorl	%r14d, %r14d
	movl	%r12d, 8(%rax)
	movq	-80(%rbp), %r12
	movq	%rdx, (%rax)
	movl	$0, 12(%rax)
	addq	$1, %r12
	salq	$4, %r12
	.p2align 4,,10
	.p2align 3
.L1906:
	movq	56(%r13), %rax
	movl	%ebx, %esi
	movq	(%rax), %rax
	movq	(%rax,%r14), %rdi
	movq	(%rdi), %rax
	call	*80(%rax)
	testq	%rax, %rax
	je	.L1905
	movq	56(%r13), %rdx
	movq	-56(%rbp), %rdi
	movq	(%rdx), %rdx
	movq	%rax, (%rdx,%r14)
	movq	56(%r13), %rax
	movq	48(%r13), %rdx
	movq	(%rax), %rsi
	addq	%r14, %rsi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
.L1905:
	addq	$16, %r14
	cmpq	%r14, %r12
	jne	.L1906
	movq	-56(%rbp), %rax
	movq	%rax, 56(%r13)
.L1895:
	andb	$-65, 25(%r13)
	jmp	.L1888
	.p2align 4,,10
	.p2align 3
.L1892:
	movq	%r13, %r15
	jmp	.L1888
	.p2align 4,,10
	.p2align 3
.L1921:
	orb	$-128, 25(%r13)
	movq	%r13, %r15
	movq	%r13, 8(%r13)
	jmp	.L1895
.L1909:
	xorl	%r15d, %r15d
.L1893:
	orb	$-128, 25(%r13)
	movq	%r15, 8(%r13)
	jmp	.L1895
.L1924:
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1904
.L1923:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, -56(%rbp)
	movq	24(%r14), %rax
	jmp	.L1903
	.cfi_endproc
.LFE20439:
	.size	_ZN2v88internal10ChoiceNode13FilterOneByteEi, .-_ZN2v88internal10ChoiceNode13FilterOneByteEi
	.section	.text._ZN2v88internal14LoopChoiceNode13FilterOneByteEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LoopChoiceNode13FilterOneByteEi
	.type	_ZN2v88internal14LoopChoiceNode13FilterOneByteEi, @function
_ZN2v88internal14LoopChoiceNode13FilterOneByteEi:
.LFB20438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movzbl	25(%rdi), %eax
	movq	%rdi, %r12
	testb	%al, %al
	jns	.L1926
	movq	8(%rdi), %rax
.L1925:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1926:
	.cfi_restore_state
	testl	%esi, %esi
	js	.L1929
	testb	$64, %al
	jne	.L1929
	orl	$64, %eax
	leal	-1(%rsi), %r13d
	movb	%al, 25(%rdi)
	movq	80(%rdi), %rdi
	movl	%r13d, %esi
	movq	(%rdi), %rax
	call	*80(%rax)
	testq	%rax, %rax
	je	.L1932
	andb	$-65, 25(%r12)
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10ChoiceNode13FilterOneByteEi
	.p2align 4,,10
	.p2align 3
.L1929:
	.cfi_restore_state
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1932:
	.cfi_restore_state
	movq	$0, 8(%r12)
	movzbl	25(%r12), %edx
	andl	$63, %edx
	orl	$-128, %edx
	movb	%dl, 25(%r12)
	jmp	.L1925
	.cfi_endproc
.LFE20438:
	.size	_ZN2v88internal14LoopChoiceNode13FilterOneByteEi, .-_ZN2v88internal14LoopChoiceNode13FilterOneByteEi
	.section	.text._ZN2v88internal14RegExpCompiler33OptionallyStepBackToLeadSurrogateEPS1_PNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpCompiler33OptionallyStepBackToLeadSurrogateEPS1_PNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.type	_ZN2v88internal14RegExpCompiler33OptionallyStepBackToLeadSurrogateEPS1_PNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, @function
_ZN2v88internal14RegExpCompiler33OptionallyStepBackToLeadSurrogateEPS1_PNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE:
.LFB20557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$104, %rsp
	movq	1096(%rdi), %r12
	movq	%rsi, -120(%rbp)
	movq	24(%r12), %rdx
	movq	16(%r12), %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	subq	%r9, %rax
	cmpq	$15, %rax
	jbe	.L1956
	leaq	16(%r9), %rax
	subq	%rax, %rdx
	movq	%rax, 16(%r12)
	cmpq	$7, %rdx
	jbe	.L1957
.L1936:
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r12)
.L1937:
	movabsq	$4294967297, %rsi
	movq	%rax, (%r9)
	movq	%rsi, 8(%r9)
	movabsq	$241888263198720, %rsi
	movq	%rsi, (%rax)
	movq	24(%r12), %rdx
	movq	16(%r12), %r14
	movq	%rdx, %rax
	subq	%r14, %rax
	cmpq	$15, %rax
	jbe	.L1958
	leaq	16(%r14), %rax
	subq	%rax, %rdx
	movq	%rax, 16(%r12)
	cmpq	$7, %rdx
	jbe	.L1959
.L1940:
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r12)
.L1941:
	movabsq	$4294967297, %rdi
	movq	%rax, (%r14)
	movabsq	$246286309710848, %rsi
	movq	%rdi, 8(%r14)
	movq	%rsi, (%rax)
	movq	16(%r12), %r13
	movq	24(%r12), %rax
	subq	%r13, %rax
	cmpq	$71, %rax
	jbe	.L1960
	leaq	72(%r13), %rax
	movq	%rax, 16(%r12)
.L1943:
	leaq	16+_ZTVN2v88internal10ChoiceNodeE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, 48(%r13)
	movq	%rax, 0(%r13)
	movq	$0, 8(%r13)
	movq	$0, 16(%r13)
	movq	$0, 24(%r13)
	movups	%xmm0, 32(%r13)
	movq	24(%r12), %rdx
	movq	16(%r12), %r15
	movq	%rdx, %rax
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L1961
	leaq	16(%r15), %rax
	movq	%rax, 16(%r12)
.L1945:
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1962
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%r12)
.L1947:
	movq	%rax, (%r15)
	xorl	%eax, %eax
	movq	$2, 8(%r15)
	movq	%r15, 56(%r13)
	movw	%ax, 64(%r13)
	movl	12(%rcx), %r15d
	cmpl	$-1, %r15d
	je	.L1963
	movl	16(%rcx), %r10d
	cmpl	$-1, %r10d
	je	.L1964
.L1951:
	movq	-120(%rbp), %rcx
	movq	%r9, %rsi
	movq	%r12, %rdi
	movl	%ebx, %r8d
	movl	$1, %edx
	movl	%r10d, -128(%rbp)
	call	_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	subq	$8, %rsp
	movl	-128(%rbp), %r10d
	xorl	%r9d, %r9d
	pushq	$0
	leaq	-96(%rbp), %r11
	movq	%rax, %rdx
	movl	%r15d, %ecx
	movl	%r10d, %r8d
	movq	%r11, %rdi
	movl	$1, %esi
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal16RegExpLookaround7BuilderC1EbPNS0_10RegExpNodeEiiii@PLT
	movq	-88(%rbp), %rcx
	movl	%ebx, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	leaq	-112(%rbp), %r12
	call	_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	movq	-128(%rbp), %r11
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE@PLT
	movq	$0, -104(%rbp)
	movq	%r12, %rsi
	movq	%rax, -112(%rbp)
	movq	48(%r13), %rdx
	movq	56(%r13), %rdi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	movq	-120(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%r12, %rsi
	movq	%rax, -112(%rbp)
	movq	48(%r13), %rdx
	movq	56(%r13), %rdi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1965
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1963:
	.cfi_restore_state
	movl	8(%rcx), %r15d
	cmpl	$65534, %r15d
	jg	.L1966
	leal	1(%r15), %eax
	movl	%eax, 8(%rcx)
.L1950:
	movl	16(%rcx), %r10d
	movl	%r15d, 12(%rcx)
	cmpl	$-1, %r10d
	jne	.L1951
.L1964:
	movl	8(%rcx), %r10d
	cmpl	$65534, %r10d
	jg	.L1967
	leal	1(%r10), %eax
	movl	%eax, 8(%rcx)
.L1953:
	movl	%r10d, 16(%rcx)
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1966:
	movb	$1, 49(%rcx)
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L1967:
	movb	$1, 49(%rcx)
	jmp	.L1953
	.p2align 4,,10
	.p2align 3
.L1962:
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rcx, -136(%rbp)
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %r9
	movq	-136(%rbp), %rcx
	jmp	.L1947
	.p2align 4,,10
	.p2align 3
.L1956:
	movq	%rdi, -128(%rbp)
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r12), %rdx
	movq	-128(%rbp), %rcx
	movq	%rax, %r9
	movq	16(%r12), %rax
	subq	%rax, %rdx
	cmpq	$7, %rdx
	ja	.L1936
	.p2align 4,,10
	.p2align 3
.L1957:
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rcx, -136(%rbp)
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %r9
	movq	-136(%rbp), %rcx
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L1958:
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rcx, -136(%rbp)
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r12), %rdx
	movq	-128(%rbp), %r9
	movq	%rax, %r14
	movq	16(%r12), %rax
	movq	-136(%rbp), %rcx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	ja	.L1940
	.p2align 4,,10
	.p2align 3
.L1959:
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rcx, -136(%rbp)
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %r9
	movq	-136(%rbp), %rcx
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1960:
	movl	$72, %esi
	movq	%r12, %rdi
	movq	%rcx, -136(%rbp)
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %r9
	movq	-136(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L1943
	.p2align 4,,10
	.p2align 3
.L1961:
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rcx, -136(%rbp)
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r12), %rdx
	movq	-128(%rbp), %r9
	movq	%rax, %r15
	movq	-136(%rbp), %rcx
	movq	16(%r12), %rax
	jmp	.L1945
.L1965:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20557:
	.size	_ZN2v88internal14RegExpCompiler33OptionallyStepBackToLeadSurrogateEPS1_PNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, .-_ZN2v88internal14RegExpCompiler33OptionallyStepBackToLeadSurrogateEPS1_PNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.section	.rodata._ZNSt6vectorIPN2v88internal10RegExpNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC8:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal10RegExpNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal10RegExpNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal10RegExpNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal10RegExpNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal10RegExpNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB22794:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1982
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1978
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1983
.L1970:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1977:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1984
	testq	%r13, %r13
	jg	.L1973
	testq	%r9, %r9
	jne	.L1976
.L1974:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1984:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1973
.L1976:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1974
	.p2align 4,,10
	.p2align 3
.L1983:
	testq	%rsi, %rsi
	jne	.L1971
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L1973:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1974
	jmp	.L1976
	.p2align 4,,10
	.p2align 3
.L1978:
	movl	$8, %r14d
	jmp	.L1970
.L1982:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1971:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1970
	.cfi_endproc
.LFE22794:
	.size	_ZNSt6vectorIPN2v88internal10RegExpNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIPN2v88internal10RegExpNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal5Trace5FlushEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Trace5FlushEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal5Trace5FlushEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal5Trace5FlushEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB20358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	40(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 8(%rdi)
	movq	16(%rdi), %rax
	je	.L2005
	movl	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	testq	%rax, %rax
	je	.L1991
.L1990:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*296(%rax)
.L1991:
	leaq	-208(%rbp), %rcx
	movq	1096(%r13), %rdx
	movq	%rbx, %rdi
	leaq	-176(%rbp), %r15
	movq	%rcx, %rsi
	movq	%rcx, -248(%rbp)
	call	_ZN2v88internal5Trace21FindAffectedRegistersEPNS0_13DynamicBitSetEPNS0_4ZoneE
	subq	$8, %rsp
	movq	%r14, %rsi
	movq	%r15, %r9
	pushq	1096(%r13)
	leaq	-192(%rbp), %rcx
	movl	%eax, %edx
	movq	%rbx, %rdi
	movq	%rcx, -240(%rbp)
	movq	%rcx, %r8
	movq	-248(%rbp), %rcx
	movl	%eax, -228(%rbp)
	movl	$0, -192(%rbp)
	movq	$0, -184(%rbp)
	movl	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	call	_ZN2v88internal5Trace22PerformDeferredActionsEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetEPS4_S7_PNS0_4ZoneE
	movl	(%rbx), %esi
	popq	%rdx
	popq	%rcx
	testl	%esi, %esi
	je	.L1993
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
.L1993:
	movq	$0, -216(%rbp)
	movq	(%r14), %rax
	leaq	-216(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, -248(%rbp)
	movq	%r8, %rsi
	call	*288(%rax)
	cmpb	$0, 50(%r13)
	movq	-248(%rbp), %r8
	jne	.L1994
	cmpl	$100, 32(%r13)
	jg	.L1994
	pxor	%xmm0, %xmm0
	movb	$0, -104(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$-4294967196, %rax
	movq	$0, -120(%rbp)
	leaq	-160(%rbp), %rdx
	movq	%rax, -72(%rbp)
	movq	(%r12), %rax
	movl	$0, -160(%rbp)
	movl	$0, -112(%rbp)
	movl	$0, -108(%rbp)
	movl	$0, -102(%rbp)
	movb	$0, -98(%rbp)
	movl	$0, -96(%rbp)
	movb	$0, -92(%rbp)
	movl	$0, -90(%rbp)
	movb	$0, -86(%rbp)
	movq	$0, -84(%rbp)
	movb	$0, -76(%rbp)
	movups	%xmm0, -152(%rbp)
	movups	%xmm0, -136(%rbp)
	call	*24(%rax)
	movq	-248(%rbp), %r8
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L1994:
	cmpb	$0, 24(%r12)
	movq	%r12, -224(%rbp)
	je	.L2006
.L1996:
	movq	(%r14), %rax
	movq	%r8, -248(%rbp)
	leaq	16(%r12), %rsi
	movq	%r14, %rdi
	call	*224(%rax)
	movq	-248(%rbp), %r8
.L1995:
	movq	(%r14), %rax
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	*64(%rax)
	movq	-240(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %r8
	movl	-228(%rbp), %edx
	movq	%r14, %rsi
	call	_ZN2v88internal5Trace24RestoreAffectedRegistersEPNS0_20RegExpMacroAssemblerEiRKNS0_13DynamicBitSetES6_
	cmpq	$0, 16(%rbx)
	movq	(%r14), %rax
	movq	%r14, %rdi
	je	.L2007
	call	*272(%rax)
	movq	(%r14), %rax
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	call	*224(%rax)
.L1985:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2008
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2006:
	.cfi_restore_state
	movl	16(%r12), %eax
	testl	%eax, %eax
	js	.L1996
	movb	$1, 24(%r12)
	movq	24(%r13), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L1997
	movq	%r12, (%rsi)
	addq	$8, 8(%rdi)
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L2005:
	testq	%rax, %rax
	je	.L2009
	movl	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	jmp	.L1990
	.p2align 4,,10
	.p2align 3
.L2007:
	call	*56(%rax)
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L2009:
	movl	(%rdi), %esi
	testl	%esi, %esi
	je	.L1989
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
.L1989:
	pxor	%xmm0, %xmm0
	movb	$0, -104(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$-4294967196, %rax
	movq	$0, -120(%rbp)
	leaq	-160(%rbp), %rdx
	movq	%rax, -72(%rbp)
	movq	(%r12), %rax
	movl	$0, -160(%rbp)
	movl	$0, -112(%rbp)
	movl	$0, -108(%rbp)
	movl	$0, -102(%rbp)
	movb	$0, -98(%rbp)
	movl	$0, -96(%rbp)
	movb	$0, -92(%rbp)
	movl	$0, -90(%rbp)
	movb	$0, -86(%rbp)
	movq	$0, -84(%rbp)
	movb	$0, -76(%rbp)
	movups	%xmm0, -152(%rbp)
	movups	%xmm0, -136(%rbp)
	call	*24(%rax)
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L1997:
	leaq	-224(%rbp), %rdx
	movq	%r8, -248(%rbp)
	call	_ZNSt6vectorIPN2v88internal10RegExpNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	-248(%rbp), %r8
	jmp	.L1996
.L2008:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20358:
	.size	_ZN2v88internal5Trace5FlushEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal5Trace5FlushEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.rodata._ZN2v88internal7EndNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"unimplemented code"
	.section	.text._ZN2v88internal7EndNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7EndNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.type	_ZN2v88internal7EndNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, @function
_ZN2v88internal7EndNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE:
.LFB20363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpq	$0, 16(%rdx)
	je	.L2018
.L2011:
	addq	$8, %rsp
	movq	%r12, %rdx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal5Trace5FlushEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.p2align 4,,10
	.p2align 3
.L2018:
	.cfi_restore_state
	cmpq	$0, 8(%rdx)
	jne	.L2011
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L2011
	cmpq	$0, 40(%rdx)
	jne	.L2011
	movl	48(%rdx), %edx
	testl	%edx, %edx
	jne	.L2011
	cmpl	$-1, 92(%r13)
	jne	.L2011
	movl	16(%rdi), %eax
	movq	40(%rsi), %r14
	testl	%eax, %eax
	js	.L2013
	movq	(%r14), %rax
	leaq	16(%rdi), %rsi
	movq	%r14, %rdi
	call	*64(%rax)
.L2013:
	movl	56(%r12), %eax
	cmpl	$1, %eax
	jne	.L2019
	movq	(%r14), %rax
	movq	16(%r13), %rsi
	movq	%r14, %rdi
	movq	224(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2019:
	.cfi_restore_state
	cmpl	$2, %eax
	je	.L2015
	testl	%eax, %eax
	jne	.L2016
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	344(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L2015:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2016:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20363:
	.size	_ZN2v88internal7EndNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, .-_ZN2v88internal7EndNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.section	.text._ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE.part.0, @function
_ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE.part.0:
.LFB25100:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 16(%rdx)
	je	.L2032
.L2021:
	movl	28(%r12), %eax
	addl	$1, %eax
	movl	%eax, 28(%r12)
	movzbl	50(%rbx), %r13d
	testb	%r13b, %r13b
	je	.L2033
.L2026:
	movb	$1, 50(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal5Trace5FlushEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	movb	%r13b, 50(%rbx)
.L2031:
	xorl	%r8d, %r8d
.L2020:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2034
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2033:
	.cfi_restore_state
	cmpl	$100, 32(%rbx)
	jg	.L2026
	cmpb	$0, 51(%rbx)
	je	.L2026
	movl	$1, %r8d
	cmpl	$9, %eax
	jg	.L2026
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2032:
	cmpq	$0, 8(%rdx)
	jne	.L2021
	movl	(%rdx), %esi
	testl	%esi, %esi
	jne	.L2021
	cmpq	$0, 40(%rdx)
	jne	.L2021
	movl	48(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L2021
	cmpl	$-1, 92(%rdx)
	jne	.L2021
	movq	40(%rbx), %rdi
	movl	16(%r12), %edx
	leaq	16(%r12), %rsi
	movq	(%rdi), %rax
	testl	%edx, %edx
	js	.L2022
	cmpb	$0, 24(%r12)
	jne	.L2022
	cmpb	$0, 50(%rbx)
	jne	.L2022
	cmpl	$100, 32(%rbx)
	jg	.L2022
	call	*64(%rax)
	movl	$1, %r8d
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2022:
	call	*224(%rax)
	cmpb	$0, 24(%r12)
	movq	%r12, -48(%rbp)
	jne	.L2031
	movl	16(%r12), %eax
	testl	%eax, %eax
	js	.L2031
	movb	$1, 24(%r12)
	movq	24(%rbx), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L2025
	movq	%r12, (%rsi)
	addq	$8, 8(%rdi)
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2025:
	leaq	-48(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal10RegExpNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2031
.L2034:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25100:
	.size	_ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE.part.0, .-_ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE.part.0
	.section	.text._ZN2v88internal8TextNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.type	_ZN2v88internal8TextNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, @function
_ZN2v88internal8TextNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE:
.LFB20458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 24(%rdx)
	je	.L2036
.L2041:
	movq	64(%r13), %rdx
	movl	(%rbx), %esi
	movl	12(%rdx), %eax
	subl	$1, %eax
	cltq
	salq	$4, %rax
	addq	(%rdx), %rax
	movl	4(%rax), %ecx
	movl	(%rax), %edx
	testl	%ecx, %ecx
	je	.L2037
	cmpl	$1, %ecx
	jne	.L2039
	movl	$1, %eax
	addl	%eax, %edx
	addl	%esi, %edx
	cmpl	$32767, %edx
	jg	.L2085
.L2042:
	leaq	-164(%rbp), %rax
	cmpb	$0, 48(%r12)
	movq	%rax, -184(%rbp)
	jne	.L2086
.L2043:
	movl	44(%rbx), %eax
	movl	40(%rbx), %r15d
	leal	-1(%rsi,%rax), %eax
	movl	%eax, -164(%rbp)
	cmpl	$1, %r15d
	je	.L2087
	xorl	%r15d, %r15d
.L2044:
	movl	$1, %r14d
.L2046:
	subq	$8, %rsp
	pushq	-184(%rbp)
	movl	%r14d, %edx
	xorl	%ecx, %ecx
	movl	%r15d, %r9d
	movq	%rbx, %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, %r14d
	call	_ZN2v88internal8TextNode12TextEmitPassEPNS0_14RegExpCompilerENS1_16TextEmitPassTypeEbPNS0_5TraceEbPi
	popq	%rdx
	popq	%rcx
	cmpl	$5, %r14d
	jne	.L2046
	movq	64(%r13), %rdx
	movdqu	(%rbx), %xmm0
	movdqu	16(%rbx), %xmm1
	movdqu	32(%rbx), %xmm2
	movdqu	48(%rbx), %xmm3
	movdqu	64(%rbx), %xmm4
	movaps	%xmm0, -160(%rbp)
	movdqu	80(%rbx), %xmm5
	movaps	%xmm2, -128(%rbp)
	movzbl	72(%r13), %r9d
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movl	12(%rdx), %eax
	subl	$1, %eax
	cltq
	salq	$4, %rax
	addq	(%rdx), %rax
	movl	(%rax), %ecx
	movl	4(%rax), %edx
	testb	%r9b, %r9b
	je	.L2047
	testl	%edx, %edx
	je	.L2048
	cmpl	$1, %edx
	jne	.L2039
	movl	$1, %eax
.L2049:
	addl	%eax, %ecx
	negl	%ecx
.L2050:
	movl	-112(%rbp), %eax
	movl	$0, -120(%rbp)
	cmpl	%eax, %ecx
	jge	.L2068
	testl	%ecx, %ecx
	js	.L2068
	movslq	%ecx, %rdx
	leaq	-108(%rbp), %rax
	leaq	(%rdx,%rdx,2), %rdi
	xorl	%edx, %edx
	addq	%rdi, %rdi
	.p2align 4,,10
	.p2align 3
.L2058:
	movl	(%rax,%rdi), %esi
	addl	$1, %edx
	addq	$6, %rax
	movl	%esi, -6(%rax)
	movzbl	-2(%rax,%rdi), %esi
	movb	%sil, -2(%rax)
	movl	-112(%rbp), %r8d
	movl	%r8d, %esi
	subl	%ecx, %esi
	cmpl	%edx, %esi
	jg	.L2058
	cmpl	%esi, %r8d
	jle	.L2061
	movslq	%esi, %rax
	leaq	(%rax,%rax,2), %rax
	addq	%rax, %rax
	movl	$0, -108(%rbp,%rax)
	movb	$0, -104(%rbp,%rax)
	leal	1(%rsi), %eax
	cmpl	%eax, %r8d
	jle	.L2061
	cltq
	leaq	(%rax,%rax,2), %rax
	addq	%rax, %rax
	movl	$0, -108(%rbp,%rax)
	movb	$0, -104(%rbp,%rax)
	leal	2(%rsi), %eax
	cmpl	%eax, %r8d
	jle	.L2061
	cltq
	leaq	(%rax,%rax,2), %rax
	addq	%rax, %rax
	movl	$0, -108(%rbp,%rax)
	movb	$0, -104(%rbp,%rax)
	leal	3(%rsi), %eax
	cmpl	%eax, %r8d
	jle	.L2061
	cltq
	leaq	(%rax,%rax,2), %rax
	addq	%rax, %rax
	movl	$0, -108(%rbp,%rax)
	movb	$0, -104(%rbp,%rax)
.L2061:
	movl	%esi, -112(%rbp)
.L2056:
	movl	-160(%rbp), %eax
	addl	%ecx, %eax
	movl	%eax, -160(%rbp)
	cmpl	$32767, %eax
	jle	.L2062
	movl	$0, -160(%rbp)
	movb	$1, 49(%r12)
	movzbl	72(%r13), %r9d
.L2062:
	subl	%ecx, -116(%rbp)
	movl	-116(%rbp), %eax
	leaq	-160(%rbp), %rdx
	movq	%r12, %rsi
	testl	%eax, %eax
	movl	$0, %eax
	cmovns	-116(%rbp), %eax
	negl	%r9d
	addl	$1, 32(%r12)
	movq	56(%r13), %rdi
	movl	%eax, -116(%rbp)
	movl	%r9d, -68(%rbp)
	movq	(%rdi), %rax
	call	*24(%rax)
	subl	$1, 32(%r12)
.L2035:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2088
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2037:
	.cfi_restore_state
	movq	8(%rax), %rax
	movl	16(%rax), %eax
	addl	%eax, %edx
	addl	%esi, %edx
	cmpl	$32767, %edx
	jle	.L2042
.L2085:
	movb	$1, 49(%r12)
	jmp	.L2035
	.p2align 4,,10
	.p2align 3
.L2036:
	call	_ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE.part.0
	testl	%eax, %eax
	jne	.L2041
	jmp	.L2035
	.p2align 4,,10
	.p2align 3
.L2068:
	testl	%eax, %eax
	jle	.L2057
	movl	$0, -108(%rbp)
	movb	$0, -104(%rbp)
	cmpl	$1, %eax
	je	.L2057
	movl	$0, -102(%rbp)
	movb	$0, -98(%rbp)
	cmpl	$2, %eax
	je	.L2057
	movl	$0, -96(%rbp)
	movb	$0, -92(%rbp)
	cmpl	$3, %eax
	je	.L2057
	movl	$0, -90(%rbp)
	movb	$0, -86(%rbp)
.L2057:
	movl	$0, -112(%rbp)
	jmp	.L2056
	.p2align 4,,10
	.p2align 3
.L2047:
	testl	%edx, %edx
	je	.L2051
	cmpl	$1, %edx
	jne	.L2039
	movl	$1, %eax
	addl	%eax, %ecx
	jmp	.L2050
	.p2align 4,,10
	.p2align 3
.L2087:
	movl	$1, %r14d
.L2045:
	subq	$8, %rsp
	pushq	-184(%rbp)
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movl	$1, %ecx
	addl	$1, %r14d
	call	_ZN2v88internal8TextNode12TextEmitPassEPNS0_14RegExpCompilerENS1_16TextEmitPassTypeEbPNS0_5TraceEbPi
	popq	%rsi
	popq	%rdi
	cmpl	$5, %r14d
	jne	.L2045
	jmp	.L2044
	.p2align 4,,10
	.p2align 3
.L2086:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r12, %rsi
	pushq	%rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	$0, -164(%rbp)
	call	_ZN2v88internal8TextNode12TextEmitPassEPNS0_14RegExpCompilerENS1_16TextEmitPassTypeEbPNS0_5TraceEbPi
	popq	%r8
	movl	(%rbx), %esi
	popq	%r9
	jmp	.L2043
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	8(%rax), %rax
	movl	16(%rax), %eax
	jmp	.L2049
	.p2align 4,,10
	.p2align 3
.L2051:
	movq	8(%rax), %rax
	movl	16(%rax), %eax
	addl	%eax, %ecx
	jmp	.L2050
.L2039:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2088:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20458:
	.size	_ZN2v88internal8TextNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, .-_ZN2v88internal8TextNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.section	.text._ZN2v88internal10ChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.type	_ZN2v88internal10ChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, @function
_ZN2v88internal10ChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE:
.LFB20513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$920, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -936(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	56(%rdi), %rax
	movl	12(%rax), %ebx
	cmpl	$1, %ebx
	je	.L2157
.L2090:
	cmpq	$0, 24(%r15)
	je	.L2092
.L2095:
	movl	88(%r15), %r8d
	testl	%r8d, %r8d
	jne	.L2094
	cmpq	$0, 8(%r15)
	jne	.L2096
.L2094:
	movq	-936(%rbp), %rax
	movl	$-1, -884(%rbp)
	movq	$0, -784(%rbp)
	addl	$1, 32(%rax)
	movabsq	$-4294967196, %rax
	cmpb	$0, 64(%r12)
	movq	%rax, -688(%rbp)
	leaq	-784(%rbp), %rax
	movl	$0, -776(%rbp)
	movq	$0, -768(%rbp)
	movq	$0, -752(%rbp)
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	movl	$0, -728(%rbp)
	movl	$0, -724(%rbp)
	movb	$0, -720(%rbp)
	movl	$0, -718(%rbp)
	movb	$0, -714(%rbp)
	movl	$0, -712(%rbp)
	movb	$0, -708(%rbp)
	movl	$0, -706(%rbp)
	movb	$0, -702(%rbp)
	movq	$0, -700(%rbp)
	movb	$0, -692(%rbp)
	movq	%rax, -960(%rbp)
	movq	%rax, -760(%rbp)
	jne	.L2158
.L2097:
	movq	56(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %r13
	cmpq	%r13, %r12
	je	.L2137
	movl	$101, %eax
	movl	%ebx, -920(%rbp)
	xorl	%r14d, %r14d
	movq	%r13, %rbx
	movl	%eax, %r13d
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2160:
	movq	56(%rbx), %rbx
	addl	%eax, %r14d
	cmpq	%rbx, %r12
	je	.L2159
	subl	$1, %r13d
	je	.L2138
.L2099:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	cmpl	$-2147483648, %eax
	jne	.L2160
	movl	-920(%rbp), %ebx
	movb	$0, -920(%rbp)
	movl	%eax, %ecx
.L2100:
	movq	48(%r12), %r8
	xorl	%eax, %eax
	testl	%ebx, %ebx
	jg	.L2161
.L2103:
	movq	%rax, -672(%rbp)
	movl	%ebx, -664(%rbp)
	movl	$0, -660(%rbp)
	movq	$0, -656(%rbp)
	movb	$0, -648(%rbp)
	movq	$0, -644(%rbp)
	movl	$0, -636(%rbp)
	movl	$0, -632(%rbp)
	movb	$0, -628(%rbp)
	movl	$0, -626(%rbp)
	movb	$0, -622(%rbp)
	movl	$0, -620(%rbp)
	movb	$0, -616(%rbp)
	movl	$0, -614(%rbp)
	movb	$0, -610(%rbp)
	movq	$0, -608(%rbp)
	movb	$0, -600(%rbp)
	movq	$0, -596(%rbp)
	movb	$0, -588(%rbp)
	movq	$0, -584(%rbp)
	movl	$0, -576(%rbp)
	movl	$0, -572(%rbp)
	movb	$0, -568(%rbp)
	movl	$0, -566(%rbp)
	movb	$0, -562(%rbp)
	movl	$0, -560(%rbp)
	movb	$0, -556(%rbp)
	movl	$0, -554(%rbp)
	movb	$0, -550(%rbp)
	movq	$0, -548(%rbp)
	movb	$0, -540(%rbp)
	movq	$0, -536(%rbp)
	movb	$0, -528(%rbp)
	movq	$0, -524(%rbp)
	movl	$0, -516(%rbp)
	movl	$0, -512(%rbp)
	movb	$0, -508(%rbp)
	movl	$0, -506(%rbp)
	movb	$0, -502(%rbp)
	movl	$0, -500(%rbp)
	movb	$0, -496(%rbp)
	movl	$0, -494(%rbp)
	movb	$0, -490(%rbp)
	movq	$0, -488(%rbp)
	movb	$0, -480(%rbp)
	movq	$0, -476(%rbp)
	movb	$0, -468(%rbp)
	movq	$0, -464(%rbp)
	movl	$0, -456(%rbp)
	movl	$0, -452(%rbp)
	movb	$0, -448(%rbp)
	movl	$0, -446(%rbp)
	movb	$0, -442(%rbp)
	movl	$0, -440(%rbp)
	movb	$0, -436(%rbp)
	movl	$0, -434(%rbp)
	movb	$0, -430(%rbp)
	movq	$0, -428(%rbp)
	movb	$0, -420(%rbp)
	movq	$0, -416(%rbp)
	movb	$0, -408(%rbp)
	movq	$0, -404(%rbp)
	movl	$0, -396(%rbp)
	movl	$0, -392(%rbp)
	movb	$0, -388(%rbp)
	movl	$0, -386(%rbp)
	movb	$0, -382(%rbp)
	movl	$0, -380(%rbp)
	movb	$0, -376(%rbp)
	movl	$0, -374(%rbp)
	movb	$0, -370(%rbp)
	movq	$0, -368(%rbp)
	movb	$0, -360(%rbp)
	movq	$0, -356(%rbp)
	movb	$0, -348(%rbp)
	movq	$0, -344(%rbp)
	movl	$0, -336(%rbp)
	movl	$0, -332(%rbp)
	movb	$0, -328(%rbp)
	movl	$0, -326(%rbp)
	movb	$0, -322(%rbp)
	movl	$0, -320(%rbp)
	movb	$0, -316(%rbp)
	movl	$0, -314(%rbp)
	movb	$0, -310(%rbp)
	movq	$0, -308(%rbp)
	movb	$0, -300(%rbp)
	movq	$0, -296(%rbp)
	movb	$0, -288(%rbp)
	movq	$0, -284(%rbp)
	movl	$0, -276(%rbp)
	movl	$0, -272(%rbp)
	movb	$0, -268(%rbp)
	movl	$0, -266(%rbp)
	movb	$0, -262(%rbp)
	movl	$0, -260(%rbp)
	movb	$0, -256(%rbp)
	movl	$0, -254(%rbp)
	movb	$0, -250(%rbp)
	movq	$0, -248(%rbp)
	movb	$0, -240(%rbp)
	movq	$0, -236(%rbp)
	movb	$0, -228(%rbp)
	movq	$0, -224(%rbp)
	movl	$0, -216(%rbp)
	movl	$0, -212(%rbp)
	movb	$0, -208(%rbp)
	movl	$0, -206(%rbp)
	movb	$0, -202(%rbp)
	movl	$0, -200(%rbp)
	movb	$0, -196(%rbp)
	movl	$0, -194(%rbp)
	movb	$0, -190(%rbp)
	movq	$0, -188(%rbp)
	movb	$0, -180(%rbp)
	movq	$0, -176(%rbp)
	movb	$0, -168(%rbp)
	movq	$0, -164(%rbp)
	movl	$0, -156(%rbp)
	movl	$0, -152(%rbp)
	movb	$0, -148(%rbp)
	movl	$0, -146(%rbp)
	movb	$0, -142(%rbp)
	movl	$0, -140(%rbp)
	movb	$0, -136(%rbp)
	movl	$0, -134(%rbp)
	movb	$0, -130(%rbp)
	movq	$0, -128(%rbp)
	movb	$0, -120(%rbp)
	movq	$0, -116(%rbp)
	movb	$0, -108(%rbp)
	movq	$0, -104(%rbp)
	movl	$0, -96(%rbp)
	movl	$0, -92(%rbp)
	movb	$0, -88(%rbp)
	movl	$0, -86(%rbp)
	movb	$0, -82(%rbp)
	movl	$0, -80(%rbp)
	movb	$0, -76(%rbp)
	movl	$0, -74(%rbp)
	movb	$0, -70(%rbp)
	movq	$0, -68(%rbp)
	movb	$0, -60(%rbp)
	testl	%ebx, %ebx
	jle	.L2123
	leaq	-656(%rbp), %r14
	xorl	%r13d, %r13d
	movq	%r12, -944(%rbp)
	movl	%ebx, %r12d
	movl	%ecx, -928(%rbp)
	movq	%r14, %rbx
	movl	%r13d, %r14d
	movq	%r8, %r13
	movq	%r15, -952(%rbp)
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L2162:
	leal	1(%rax), %edx
	addl	$1, %r14d
	movl	%edx, -660(%rbp)
	movq	-672(%rbp), %rdx
	movq	%rbx, (%rdx,%rax,8)
	addq	$60, %rbx
	cmpl	%r14d, %r12d
	jle	.L2152
.L2164:
	cmpl	$9, %r14d
	jg	.L2152
.L2117:
	movslq	-660(%rbp), %rax
	movl	-664(%rbp), %edx
	cmpl	%edx, %eax
	jl	.L2162
	leal	1(%rdx,%rdx), %r15d
	movq	16(%r13), %rdi
	movq	24(%r13), %rdx
	movslq	%r15d, %rsi
	salq	$3, %rsi
	subq	%rdi, %rdx
	cmpq	%rdx, %rsi
	ja	.L2163
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L2114:
	movslq	%eax, %rdx
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L2115
	movq	-672(%rbp), %rsi
	call	memcpy@PLT
	movslq	-660(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L2115:
	addl	$1, %eax
	addl	$1, %r14d
	movq	%rdi, -672(%rbp)
	movl	%r15d, -664(%rbp)
	movl	%eax, -660(%rbp)
	movq	%rbx, (%rdi,%rdx)
	addq	$60, %rbx
	cmpl	%r14d, %r12d
	jg	.L2164
	.p2align 4,,10
	.p2align 3
.L2152:
	movl	%r12d, %ebx
	movl	-928(%rbp), %ecx
	movq	-944(%rbp), %r12
	movq	-952(%rbp), %r15
	cmpl	$10, %ebx
	jle	.L2123
	movl	$10, %r14d
	movq	%r12, -944(%rbp)
	movl	%ecx, -928(%rbp)
	movl	%r14d, %r12d
	movq	%r15, -952(%rbp)
	jmp	.L2108
	.p2align 4,,10
	.p2align 3
.L2166:
	leal	1(%rax), %edx
	addl	$1, %r12d
	movl	%edx, -660(%rbp)
	movq	-672(%rbp), %rdx
	movq	%r14, (%rdx,%rax,8)
	cmpl	%r12d, %ebx
	je	.L2165
.L2108:
	movl	$60, %edi
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	$0, (%rax)
	movq	%rax, %r14
	movb	$0, 8(%rax)
	movl	-664(%rbp), %edx
	movq	$0, 12(%rax)
	movq	$0, 20(%rax)
	movb	$0, 28(%rax)
	movl	$0, 30(%rax)
	movb	$0, 34(%rax)
	movl	$0, 36(%rax)
	movb	$0, 40(%rax)
	movl	$0, 42(%rax)
	movb	$0, 46(%rax)
	movq	$0, 48(%rax)
	movb	$0, 56(%rax)
	movslq	-660(%rbp), %rax
	cmpl	%edx, %eax
	jl	.L2166
	leal	1(%rdx,%rdx), %r15d
	movq	16(%r13), %rdi
	movq	24(%r13), %rdx
	movslq	%r15d, %rsi
	salq	$3, %rsi
	subq	%rdi, %rdx
	cmpq	%rdx, %rsi
	ja	.L2167
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L2121:
	movslq	%eax, %rdx
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L2122
	movq	-672(%rbp), %rsi
	call	memcpy@PLT
	movslq	-660(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L2122:
	addl	$1, %eax
	addl	$1, %r12d
	movq	%rdi, -672(%rbp)
	movl	%r15d, -664(%rbp)
	movl	%eax, -660(%rbp)
	movq	%r14, (%rdi,%rdx)
	cmpl	%r12d, %ebx
	jne	.L2108
.L2165:
	movl	-928(%rbp), %ecx
	movq	-944(%rbp), %r12
	movq	-952(%rbp), %r15
.L2123:
	cmpb	$0, -920(%rbp)
	jne	.L2168
	movq	-936(%rbp), %r14
	leaq	-900(%rbp), %rsi
	movq	$0, -900(%rbp)
	movq	40(%r14), %rdi
	movq	(%rdi), %rax
	call	*64(%rax)
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ChoiceNode29EmitOptimizedUnanchoredSearchEPNS0_14RegExpCompilerEPNS0_5TraceE
	movq	%r15, %r8
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	-672(%rbp), %rdx
	leaq	-892(%rbp), %r9
	movq	%r12, %rdi
	movl	%eax, -884(%rbp)
	call	_ZN2v88internal10ChoiceNode11EmitChoicesEPNS0_14RegExpCompilerEPNS0_25AlternativeGenerationListEiPNS0_5TraceEPNS0_12PreloadStateE
.L2124:
	movl	88(%r15), %eax
	cltd
	idivl	%ebx
	movl	%eax, -944(%rbp)
	testl	%ebx, %ebx
	jle	.L2130
	leal	-1(%rbx), %eax
	xorl	%r14d, %r14d
	leaq	-880(%rbp), %r13
	movq	%r15, %rbx
	movq	%rax, -920(%rbp)
	movq	%r14, %r15
	movq	%r13, %r14
	movq	-936(%rbp), %r13
	movl	%eax, -928(%rbp)
	jmp	.L2132
	.p2align 4,,10
	.p2align 3
.L2169:
	movq	56(%r12), %r8
	movq	%r15, %rdx
	salq	$4, %rdx
	addq	(%r8), %rdx
	movq	(%rdx), %r11
	movq	8(%rdx), %r8
	movq	8(%rcx,%rsi), %rdx
	movq	%r11, %rcx
	movzbl	8(%rdx), %edx
	pushq	%rdx
	movl	-888(%rbp), %edx
	pushq	%rdx
.L2155:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ChoiceNode25EmitOutOfLineContinuationEPNS0_14RegExpCompilerEPNS0_5TraceENS0_18GuardedAlternativeEPNS0_21AlternativeGenerationEib
	popq	%rax
	leaq	1(%r15), %rdx
	popq	%rcx
	cmpq	%r15, -920(%rbp)
	je	.L2130
	movq	%rdx, %r15
.L2132:
	movdqu	(%rbx), %xmm0
	movq	-672(%rbp), %rcx
	leaq	0(,%r15,8), %rsi
	movq	(%rcx,%r15,8), %r9
	movaps	%xmm0, -880(%rbp)
	movdqu	16(%rbx), %xmm1
	cmpq	$0, -872(%rbp)
	movaps	%xmm1, -864(%rbp)
	movdqu	32(%rbx), %xmm2
	movaps	%xmm2, -848(%rbp)
	movdqu	48(%rbx), %xmm3
	movaps	%xmm3, -832(%rbp)
	movdqu	64(%rbx), %xmm4
	movaps	%xmm4, -816(%rbp)
	movdqu	80(%rbx), %xmm5
	movaps	%xmm5, -800(%rbp)
	je	.L2128
	movl	-944(%rbp), %eax
	movl	%eax, -792(%rbp)
.L2128:
	cmpl	%r15d, -928(%rbp)
	jne	.L2169
	movq	56(%r12), %rcx
	movq	%r15, %rdx
	salq	$4, %rdx
	addq	(%rcx), %rdx
	movq	(%rdx), %rcx
	movq	8(%rdx), %r8
	pushq	$0
	movl	-888(%rbp), %edx
	pushq	%rdx
	jmp	.L2155
	.p2align 4,,10
	.p2align 3
.L2158:
	movl	$0, -684(%rbp)
	jmp	.L2097
	.p2align 4,,10
	.p2align 3
.L2130:
	cmpl	$10, -660(%rbp)
	movl	$80, %ebx
	movl	$10, %r12d
	jg	.L2156
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2170:
	call	_ZN2v88internal8MalloceddlEPv@PLT
	movq	-672(%rbp), %rax
	addl	$1, %r12d
	movq	$0, (%rax,%rbx)
	addq	$8, %rbx
	cmpl	-660(%rbp), %r12d
	jge	.L2135
.L2156:
	movq	-672(%rbp), %rax
.L2126:
	movq	(%rax,%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L2170
	addl	$1, %r12d
	addq	$8, %rbx
	cmpl	%r12d, -660(%rbp)
	jg	.L2126
.L2135:
	movq	-936(%rbp), %rax
	subl	$1, 32(%rax)
.L2089:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2171
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2092:
	.cfi_restore_state
	movq	-936(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE.part.0
	testl	%eax, %eax
	jne	.L2095
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2157:
	movq	(%rax), %rax
	cmpq	$0, 8(%rax)
	jne	.L2090
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2159:
	movl	-920(%rbp), %ebx
.L2098:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*96(%rax)
	cmpl	$1, %ebx
	movl	%r14d, %ecx
	setg	-920(%rbp)
	negl	%ecx
	movzbl	-920(%rbp), %esi
	testb	%al, %al
	jne	.L2100
	cmpl	$-2147483648, %r14d
	movq	48(%r12), %r8
	movl	%r14d, %ecx
	setne	%al
	andl	%eax, %esi
	xorl	%eax, %eax
	movb	%sil, -920(%rbp)
	testl	%ebx, %ebx
	jle	.L2103
.L2161:
	movq	16(%r8), %rax
	movq	24(%r8), %rdx
	movslq	%ebx, %rsi
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L2172
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L2096:
	movq	-936(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal5Trace5FlushEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2168:
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-936(%rbp), %rsi
	pushq	%rcx
	leaq	-672(%rbp), %r11
	movq	-960(%rbp), %r9
	leaq	-892(%rbp), %r8
	movq	%r11, %rcx
	call	_ZN2v88internal10ChoiceNode14EmitGreedyLoopEPNS0_14RegExpCompilerEPNS0_5TraceEPNS0_25AlternativeGenerationListEPNS0_12PreloadStateEPNS0_15GreedyLoopStateEi
	popq	%rsi
	popq	%rdi
	movq	%rax, %r15
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2167:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	movl	-660(%rbp), %eax
	jmp	.L2121
	.p2align 4,,10
	.p2align 3
.L2163:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	movl	-660(%rbp), %eax
	jmp	.L2114
	.p2align 4,,10
	.p2align 3
.L2138:
	movl	-920(%rbp), %ebx
	movl	$-2147483648, %ecx
	movb	$0, -920(%rbp)
	jmp	.L2100
.L2172:
	movq	%r8, %rdi
	movl	%ecx, -944(%rbp)
	movq	%r8, -928(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-928(%rbp), %r8
	movl	-944(%rbp), %ecx
	jmp	.L2103
.L2137:
	xorl	%r14d, %r14d
	jmp	.L2098
.L2171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20513:
	.size	_ZN2v88internal10ChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, .-_ZN2v88internal10ChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.section	.text._ZN2v88internal14LoopChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LoopChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.type	_ZN2v88internal14LoopChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, @function
_ZN2v88internal14LoopChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE:
.LFB20467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpq	24(%rdx), %rdi
	je	.L2187
	cmpq	$0, 16(%rdx)
	je	.L2188
.L2179:
	addq	$24, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal5Trace5FlushEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.p2align 4,,10
	.p2align 3
.L2188:
	.cfi_restore_state
	cmpq	$0, 8(%rdx)
	jne	.L2179
	movl	(%rdx), %edx
	testl	%edx, %edx
	jne	.L2179
	cmpq	$0, 40(%r12)
	jne	.L2179
	movl	48(%r12), %eax
	testl	%eax, %eax
	jne	.L2179
	cmpl	$-1, 92(%r12)
	jne	.L2179
	addq	$24, %rsp
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10ChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.p2align 4,,10
	.p2align 3
.L2187:
	.cfi_restore_state
	movq	40(%rsi), %rax
	movq	%rax, -56(%rbp)
	movq	56(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax), %r15
	cmpq	%r15, %rdi
	je	.L2180
	movl	$101, %ebx
	xorl	%r14d, %r14d
	jmp	.L2176
	.p2align 4,,10
	.p2align 3
.L2189:
	movq	56(%r15), %r15
	addl	%eax, %r14d
	cmpq	%r15, %r13
	je	.L2175
	subl	$1, %ebx
	je	.L2181
.L2176:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*56(%rax)
	cmpl	$-2147483648, %eax
	jne	.L2189
.L2177:
	movq	-56(%rbp), %rbx
	movl	%eax, %esi
	movq	(%rbx), %rdx
	movq	%rbx, %rdi
	call	*40(%rdx)
	movq	(%rbx), %rax
	movq	32(%r12), %rsi
	movq	%rbx, %rdi
	movq	224(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L2180:
	.cfi_restore_state
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L2175:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*96(%rax)
	movl	%r14d, %esi
	negl	%esi
	testb	%al, %al
	movl	%r14d, %eax
	cmovne	%esi, %eax
	jmp	.L2177
	.p2align 4,,10
	.p2align 3
.L2181:
	movl	$-2147483648, %eax
	jmp	.L2177
	.cfi_endproc
.LFE20467:
	.size	_ZN2v88internal14LoopChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, .-_ZN2v88internal14LoopChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.section	.text._ZN2v88internal10ActionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ActionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.type	_ZN2v88internal10ActionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, @function
_ZN2v88internal10ActionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE:
.LFB20518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$168, %rsp
	movq	40(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 24(%rdx)
	je	.L2191
.L2202:
	addl	$1, 32(%rbx)
	cmpl	$6, 80(%r13)
	ja	.L2192
	movl	80(%r13), %eax
	leaq	.L2194(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10ActionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE,"a",@progbits
	.align 4
	.align 4
.L2194:
	.long	.L2200-.L2194
	.long	.L2199-.L2194
	.long	.L2198-.L2194
	.long	.L2197-.L2194
	.long	.L2196-.L2194
	.long	.L2195-.L2194
	.long	.L2193-.L2194
	.section	.text._ZN2v88internal10ActionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.p2align 4,,10
	.p2align 3
.L2191:
	call	_ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE.part.0
	testl	%eax, %eax
	jne	.L2202
	.p2align 4,,10
	.p2align 3
.L2190:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2234
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2195:
	.cfi_restore_state
	movq	8(%r12), %rax
	movl	64(%r13), %esi
	movl	68(%r13), %r8d
	testq	%rax, %rax
	jne	.L2212
	jmp	.L2205
	.p2align 4,,10
	.p2align 3
.L2206:
	cmpl	4(%rax), %esi
	je	.L2235
.L2209:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L2216
.L2212:
	movl	(%rax), %edx
	cmpl	$6, %edx
	jne	.L2206
	cmpl	16(%rax), %esi
	jl	.L2209
	cmpl	20(%rax), %esi
	jg	.L2209
.L2216:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Trace5FlushEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.p2align 4,,10
	.p2align 3
.L2203:
	subl	$1, 32(%rbx)
	jmp	.L2190
	.p2align 4,,10
	.p2align 3
.L2193:
	movq	64(%r13), %rax
	movdqu	(%r12), %xmm2
	movabsq	$-4294967290, %rcx
	movdqu	16(%r12), %xmm3
	movdqu	32(%r12), %xmm4
	movq	%rcx, -192(%rbp)
	movdqu	48(%r12), %xmm5
	movdqu	64(%r12), %xmm6
	movq	%rax, -176(%rbp)
	movdqu	80(%r12), %xmm7
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
.L2232:
	movq	-152(%rbp), %rax
	movq	56(%r13), %rdi
	leaq	-160(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%rax, -184(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2203
	.p2align 4,,10
	.p2align 3
.L2200:
	movl	68(%r13), %eax
	movl	64(%r13), %edx
	movl	$0, -192(%rbp)
	movdqu	(%r12), %xmm4
	movdqu	16(%r12), %xmm5
	movdqu	32(%r12), %xmm6
	movdqu	48(%r12), %xmm7
	movl	%edx, -188(%rbp)
	movdqu	64(%r12), %xmm0
	movdqu	80(%r12), %xmm1
	movl	%eax, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2199:
	movl	64(%r13), %eax
	movdqu	(%r12), %xmm6
	movl	$1, -192(%rbp)
	movdqu	16(%r12), %xmm7
	movdqu	32(%r12), %xmm0
	movdqu	48(%r12), %xmm1
	movdqu	64(%r12), %xmm2
	movl	%eax, -188(%rbp)
	movdqu	80(%r12), %xmm3
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2198:
	movl	64(%r13), %edx
	movzbl	68(%r13), %eax
	movl	$2, -192(%rbp)
	movdqu	(%r12), %xmm0
	movdqu	16(%r12), %xmm1
	movl	%edx, -188(%rbp)
	movdqu	32(%r12), %xmm2
	movl	(%r12), %edx
	movdqu	48(%r12), %xmm3
	movb	%al, -172(%rbp)
	movdqu	64(%r12), %xmm4
	movdqu	80(%r12), %xmm5
	movaps	%xmm2, -128(%rbp)
	movl	%edx, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2197:
	cmpq	$0, 16(%r12)
	jne	.L2216
	cmpq	$0, 8(%r12)
	jne	.L2216
	movl	(%r12), %r9d
	testl	%r9d, %r9d
	jne	.L2216
	cmpq	$0, 40(%r12)
	jne	.L2216
	movl	48(%r12), %r8d
	testl	%r8d, %r8d
	jne	.L2216
	cmpl	$-1, 92(%r12)
	jne	.L2216
	movq	(%r14), %rax
	movl	68(%r13), %esi
	movq	%r14, %rdi
	xorl	%edx, %edx
	call	*352(%rax)
	movq	(%r14), %rax
	movl	64(%r13), %esi
	movq	%r14, %rdi
	call	*368(%rax)
.L2233:
	movq	56(%r13), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2203
	.p2align 4,,10
	.p2align 3
.L2196:
	cmpq	$0, 16(%r12)
	jne	.L2216
	cmpq	$0, 8(%r12)
	jne	.L2216
	movl	(%r12), %edx
	testl	%edx, %edx
	jne	.L2216
	cmpq	$0, 40(%r12)
	jne	.L2216
	movl	48(%r12), %eax
	testl	%eax, %eax
	jne	.L2216
	cmpl	$-1, 92(%r12)
	jne	.L2216
	movq	(%r14), %rax
	movl	68(%r13), %esi
	movq	%r14, %rdi
	call	*312(%rax)
	movq	(%r14), %rax
	movl	64(%r13), %esi
	movq	%r14, %rdi
	call	*320(%rax)
	movl	72(%r13), %r15d
	testl	%r15d, %r15d
	je	.L2233
	movdqu	(%r12), %xmm0
	movdqu	16(%r12), %xmm1
	movq	$0, -192(%rbp)
	leaq	-160(%rbp), %rdx
	movdqu	32(%r12), %xmm2
	movq	56(%r13), %rdi
	movq	%rbx, %rsi
	movdqu	48(%r12), %xmm3
	movdqu	64(%r12), %xmm4
	movaps	%xmm1, -144(%rbp)
	movdqu	80(%r12), %xmm5
	leaq	-192(%rbp), %r12
	movl	76(%r13), %r8d
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	%r12, -144(%rbp)
	movq	(%rdi), %rax
	movl	%r8d, -196(%rbp)
	call	*24(%rax)
	movq	(%r14), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*64(%rax)
	movl	-196(%rbp), %r8d
	movq	(%r14), %rax
	movq	%r14, %rdi
	leal	-1(%r15,%r8), %edx
	movl	%r8d, %esi
	call	*360(%rax)
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*56(%rax)
	jmp	.L2203
	.p2align 4,,10
	.p2align 3
.L2235:
	cmpl	$2, %edx
	jne	.L2216
	movl	16(%rax), %edx
	movl	(%r12), %eax
	cmpl	$-1, %r8d
	jne	.L2211
	cmpl	%eax, %edx
	je	.L2236
.L2211:
	cmpl	%eax, %edx
	jl	.L2233
	jmp	.L2216
	.p2align 4,,10
	.p2align 3
.L2236:
	movq	(%r14), %rax
	movq	16(%r12), %rsi
	movq	%r14, %rdi
	call	*224(%rax)
	jmp	.L2203
.L2205:
	movq	16(%r12), %rdx
	testq	%rdx, %rdx
	jne	.L2216
	movl	(%r12), %edi
	testl	%edi, %edi
	jne	.L2216
	cmpq	$0, 40(%r12)
	jne	.L2216
	movl	48(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L2216
	cmpl	$-1, 92(%r12)
	jne	.L2216
	movq	$0, -192(%rbp)
	leaq	-192(%rbp), %r15
	cmpl	$-1, %r8d
	je	.L2215
	movq	(%r14), %rax
	movl	72(%r13), %edx
	movl	%r8d, %esi
	movq	%r15, %rcx
	movq	%r14, %rdi
	call	*240(%rax)
	movl	64(%r13), %esi
	movq	16(%r12), %rdx
.L2215:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*248(%rax)
	movq	(%r14), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*64(%rax)
	jmp	.L2233
.L2234:
	call	__stack_chk_fail@PLT
.L2192:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20518:
	.size	_ZN2v88internal10ActionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, .-_ZN2v88internal10ActionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.section	.text._ZN2v88internal17BackReferenceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17BackReferenceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.type	_ZN2v88internal17BackReferenceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, @function
_ZN2v88internal17BackReferenceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE:
.LFB20519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	cmpq	$0, 16(%rdx)
	movq	%rsi, %r12
	je	.L2256
.L2238:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal5Trace5FlushEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.p2align 4,,10
	.p2align 3
.L2256:
	.cfi_restore_state
	cmpq	$0, 8(%rdx)
	jne	.L2238
	movl	(%rdx), %edx
	testl	%edx, %edx
	jne	.L2238
	cmpq	$0, 40(%r13)
	jne	.L2238
	movl	48(%r13), %eax
	testl	%eax, %eax
	jne	.L2238
	cmpl	$-1, 92(%r13)
	jne	.L2238
	cmpq	$0, 24(%r13)
	movq	40(%rsi), %r14
	je	.L2239
	addl	$1, 32(%r12)
	movl	72(%r15), %ecx
	testb	$2, %cl
	je	.L2257
.L2240:
	movq	(%r14), %rax
	shrl	$4, %ecx
	movzbl	76(%r15), %edx
	movq	%r14, %rdi
	movl	64(%r15), %esi
	movq	16(%r13), %r8
	andl	$1, %ecx
	call	*136(%rax)
.L2244:
	cmpb	$0, 76(%r15)
	je	.L2245
	movl	$-1, 92(%r13)
.L2245:
	testb	$16, 72(%r15)
	je	.L2246
	cmpb	$0, 48(%r12)
	je	.L2258
.L2246:
	movq	56(%r15), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	subl	$1, 32(%r12)
.L2237:
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2239:
	.cfi_restore_state
	movq	%r13, %rdx
	call	_ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE.part.0
	testl	%eax, %eax
	je	.L2237
	addl	$1, 32(%r12)
	movl	72(%r15), %ecx
	testb	$2, %cl
	jne	.L2240
.L2257:
	movq	(%r14), %rax
	movq	16(%r13), %rcx
	movq	%r14, %rdi
	movzbl	76(%r15), %edx
	movl	64(%r15), %esi
	call	*128(%rax)
	jmp	.L2244
	.p2align 4,,10
	.p2align 3
.L2258:
	movq	16(%r13), %rdx
	movl	0(%r13), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal20RegExpMacroAssembler23CheckNotInSurrogatePairEiPNS0_5LabelE@PLT
	jmp	.L2246
	.cfi_endproc
.LFE20519:
	.size	_ZN2v88internal17BackReferenceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE, .-_ZN2v88internal17BackReferenceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.section	.text._ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE
	.type	_ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE, @function
_ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE:
.LFB20397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 24(%rdx)
	je	.L2260
.L2271:
	movl	$1, %eax
.L2259:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2273
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2260:
	.cfi_restore_state
	cmpq	$0, 16(%rdx)
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%rdx, %rdi
	je	.L2274
.L2262:
	movl	28(%r12), %eax
	addl	$1, %eax
	movl	%eax, 28(%r12)
	movzbl	50(%rbx), %r13d
	testb	%r13b, %r13b
	jne	.L2266
	cmpl	$100, 32(%rbx)
	jg	.L2266
	cmpb	$0, 51(%rbx)
	je	.L2266
	cmpl	$9, %eax
	jle	.L2271
	.p2align 4,,10
	.p2align 3
.L2266:
	movb	$1, 50(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal5Trace5FlushEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	movb	%r13b, 50(%rbx)
.L2272:
	xorl	%eax, %eax
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L2274:
	cmpq	$0, 8(%rdx)
	jne	.L2262
	movl	(%rdx), %esi
	testl	%esi, %esi
	jne	.L2262
	cmpq	$0, 40(%rdx)
	jne	.L2262
	movl	48(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L2262
	cmpl	$-1, 92(%rdx)
	jne	.L2262
	movq	40(%rbx), %rdi
	movl	16(%r12), %edx
	leaq	16(%r12), %rsi
	movq	(%rdi), %rax
	testl	%edx, %edx
	js	.L2263
	cmpb	$0, 24(%r12)
	jne	.L2263
	cmpb	$0, 50(%rbx)
	jne	.L2263
	cmpl	$100, 32(%rbx)
	jg	.L2263
	call	*64(%rax)
	jmp	.L2271
	.p2align 4,,10
	.p2align 3
.L2263:
	call	*224(%rax)
	cmpb	$0, 24(%r12)
	movq	%r12, -48(%rbp)
	jne	.L2272
	movl	16(%r12), %eax
	testl	%eax, %eax
	js	.L2272
	movb	$1, 24(%r12)
	movq	24(%rbx), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L2265
	movq	%r12, (%rsi)
	xorl	%eax, %eax
	addq	$8, 8(%rdi)
	jmp	.L2259
.L2265:
	leaq	-48(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal10RegExpNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	xorl	%eax, %eax
	jmp	.L2259
.L2273:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20397:
	.size	_ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE, .-_ZN2v88internal10RegExpNode13LimitVersionsEPNS0_14RegExpCompilerEPNS0_5TraceE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE:
.LFB24894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24894:
	.size	_GLOBAL__sub_I__ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.section	.rodata._ZN2v88internalL25GetCaseIndependentLettersEPNS0_7IsolateEtbPji.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"end - start + items <= letter_length"
	.section	.rodata._ZN2v88internalL25GetCaseIndependentLettersEPNS0_7IsolateEtbPji.constprop.0.str1.1,"aMS",@progbits,1
.LC11:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL25GetCaseIndependentLettersEPNS0_7IsolateEtbPji.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25GetCaseIndependentLettersEPNS0_7IsolateEtbPji.constprop.0, @function
_ZN2v88internalL25GetCaseIndependentLettersEPNS0_7IsolateEtbPji.constprop.0:
.LFB25112:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-256(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, %ebx
	movq	%r13, %rdi
	subq	$232, %rsp
	movb	%sil, -257(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movzwl	%bx, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movl	%eax, -264(%rbp)
	testl	%eax, %eax
	jle	.L2278
	movzbl	-257(%rbp), %r8d
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L2285:
	movl	%r12d, %esi
	movq	%r13, %rdi
	movb	%r8b, -257(%rbp)
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movzbl	-257(%rbp), %r8d
	movl	%eax, %ecx
	subl	%ebx, %ecx
	addl	%r14d, %ecx
	cmpl	$4, %ecx
	jg	.L2297
	cmpl	%eax, %ebx
	jg	.L2280
	cmpl	$255, %ebx
	jle	.L2288
	testb	%r8b, %r8b
	jne	.L2280
.L2288:
	leal	1(%r14), %ecx
	movslq	%ecx, %rcx
	.p2align 4,,10
	.p2align 3
.L2282:
	movl	%ebx, -4(%r15,%rcx,4)
	addl	$1, %ebx
	movl	%ecx, %r14d
	cmpl	%ebx, %eax
	jl	.L2280
	addq	$1, %rcx
	cmpl	$255, %ebx
	jle	.L2282
	testb	%r8b, %r8b
	je	.L2282
.L2280:
	addl	$1, %r12d
	cmpl	%r12d, -264(%rbp)
	jne	.L2285
.L2278:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2298
	addq	$232, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2297:
	.cfi_restore_state
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25112:
	.size	_ZN2v88internalL25GetCaseIndependentLettersEPNS0_7IsolateEtbPji.constprop.0, .-_ZN2v88internalL25GetCaseIndependentLettersEPNS0_7IsolateEtbPji.constprop.0
	.section	.text._ZN2v88internal8TextNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.type	_ZN2v88internal8TextNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, @function
_ZN2v88internal8TextNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb:
.LFB20556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -132(%rbp)
	movl	(%r8), %edx
	movq	%rdi, -112(%rbp)
	movq	%rsi, -144(%rbp)
	movl	%ecx, -148(%rbp)
	movl	%r9d, -136(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpl	%edx, %eax
	jge	.L2299
	movq	64(%rdi), %rdi
	movl	%eax, %r15d
	leaq	-96(%rbp), %rax
	movl	16(%r8), %ebx
	movq	%rax, -160(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r8, %r13
	movl	12(%rdi), %r11d
	movq	$0, -104(%rbp)
	movq	%rax, -120(%rbp)
	testl	%r11d, %r11d
	jle	.L2303
	movl	%r15d, %r13d
	movl	%r11d, %r10d
	movq	%r8, %r15
.L2302:
	movq	-104(%rbp), %rax
	salq	$4, %rax
	addq	(%rdi), %rax
	movl	4(%rax), %esi
	movq	8(%rax), %r14
	testl	%esi, %esi
	je	.L2349
	movq	-112(%rbp), %rax
	leaq	8(%r14), %rdi
	movq	48(%rax), %rsi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE@PLT
	movq	%rax, %r11
	testb	$1, 28(%r14)
	je	.L2319
	movq	24(%r15), %rdx
	movslq	%r13d, %rax
	movq	(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	cmpl	$128, 16(%rax)
	movl	$3, 20(%rax)
	je	.L2321
	movl	$128, 16(%rax)
	pcmpeqd	%xmm0, %xmm0
	movups	%xmm0, (%rax)
.L2321:
	movq	-112(%rbp), %rax
	movl	(%r15), %edx
	addl	$1, %r13d
	movq	64(%rax), %rdi
	movl	12(%rdi), %r10d
.L2307:
	movl	-104(%rbp), %eax
	addl	$1, %eax
	cmpl	%r10d, %eax
	jge	.L2350
	addq	$1, -104(%rbp)
	cmpl	%edx, %r13d
	jl	.L2302
	movl	-132(%rbp), %edi
	movq	%r15, %r13
	testl	%edi, %edi
	je	.L2348
	.p2align 4,,10
	.p2align 3
.L2299:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2351
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2349:
	.cfi_restore_state
	movslq	%r13d, %rax
	movq	16(%r14), %rsi
	xorl	%r12d, %r12d
	salq	$3, %rax
	movq	%rax, -128(%rbp)
	testl	%esi, %esi
	jle	.L2307
	movq	%r15, %rax
	movq	%r14, %r15
	movq	%rax, %r14
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2309:
	cmpl	%edi, %ebx
	jl	.L2311
	cmpl	%edi, 16(%r14)
	jl	.L2311
	movq	24(%r14), %rdx
	movq	-128(%rbp), %rcx
	leaq	0(,%r12,8), %rax
	movq	-120(%rbp), %rsi
	addq	(%rdx), %rax
	movq	(%rax,%rcx), %r9
	movl	%edi, -88(%rbp)
	movl	%edi, -84(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal22BoyerMoorePositionInfo11SetIntervalERKNS0_8IntervalE
.L2347:
	movq	16(%r15), %rsi
	movl	(%r14), %edx
.L2311:
	leal	1(%r12), %eax
	addl	$1, %r13d
	cmpl	%esi, %eax
	jge	.L2352
	addq	$1, %r12
	cmpl	%edx, %r13d
	jge	.L2353
.L2308:
	movq	8(%r15), %rax
	movzwl	(%rax,%r12,2), %edi
	testb	$2, 24(%r15)
	je	.L2309
	xorl	%esi, %esi
	cmpl	$255, 16(%r14)
	leaq	-80(%rbp), %rdx
	sete	%sil
	call	_ZN2v88internalL25GetCaseIndependentLettersEPNS0_7IsolateEtbPji.constprop.0
	movl	%eax, %r10d
	testl	%eax, %eax
	jle	.L2347
	movq	-128(%rbp), %rax
	leaq	(%rax,%r12,8), %r11
	movl	-80(%rbp), %eax
	cmpl	%eax, 16(%r14)
	jl	.L2312
	movq	24(%r14), %rdx
	movq	-120(%rbp), %rsi
	movq	(%rdx), %rdx
	movq	(%rdx,%r11), %rdi
	movl	%eax, -88(%rbp)
	movl	%eax, -84(%rbp)
	call	_ZN2v88internal22BoyerMoorePositionInfo11SetIntervalERKNS0_8IntervalE
.L2312:
	cmpl	$1, %r10d
	je	.L2347
	movl	-76(%rbp), %eax
	cmpl	%eax, 16(%r14)
	jge	.L2354
.L2314:
	cmpl	$2, %r10d
	je	.L2347
	movl	-72(%rbp), %eax
	cmpl	%eax, 16(%r14)
	jge	.L2355
.L2315:
	cmpl	$3, %r10d
	je	.L2347
	movl	-68(%rbp), %eax
	cmpl	16(%r14), %eax
	jg	.L2347
	movq	24(%r14), %rdx
	movq	-120(%rbp), %rsi
	movq	(%rdx), %rdx
	movq	(%rdx,%r11), %rdi
	movl	%eax, -88(%rbp)
	movl	%eax, -84(%rbp)
	call	_ZN2v88internal22BoyerMoorePositionInfo11SetIntervalERKNS0_8IntervalE
	jmp	.L2347
	.p2align 4,,10
	.p2align 3
.L2354:
	movq	24(%r14), %rdx
	movq	-120(%rbp), %rsi
	movq	(%rdx), %rdx
	movq	(%rdx,%r11), %rdi
	movl	%eax, -88(%rbp)
	movl	%eax, -84(%rbp)
	call	_ZN2v88internal22BoyerMoorePositionInfo11SetIntervalERKNS0_8IntervalE
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2355:
	movq	24(%r14), %rdx
	movq	-120(%rbp), %rsi
	movq	(%rdx), %rdx
	movq	(%rdx,%r11), %rdi
	movl	%eax, -88(%rbp)
	movl	%eax, -84(%rbp)
	call	_ZN2v88internal22BoyerMoorePositionInfo11SetIntervalERKNS0_8IntervalE
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2353:
	movl	-132(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L2299
	movzbl	-136(%rbp), %r9d
	movq	-112(%rbp), %rax
	movq	%r14, 32(%rax,%r9,8)
	jmp	.L2299
	.p2align 4,,10
	.p2align 3
.L2352:
	movq	-112(%rbp), %rax
	movq	%r14, %r15
	movq	64(%rax), %rdi
	movl	12(%rdi), %r10d
	jmp	.L2307
	.p2align 4,,10
	.p2align 3
.L2319:
	movl	12(%rax), %edi
	testl	%edi, %edi
	jle	.L2321
	movq	%r15, %rax
	movslq	%r13d, %r10
	movq	%r11, %r15
	movq	-160(%rbp), %r12
	salq	$3, %r10
	xorl	%r14d, %r14d
	movq	%rax, %r11
	jmp	.L2326
	.p2align 4,,10
	.p2align 3
.L2357:
	movq	-120(%rbp), %rsi
	movl	%eax, -88(%rbp)
	movl	%edx, -84(%rbp)
	call	_ZN2v88internal22BoyerMoorePositionInfo11SetIntervalERKNS0_8IntervalE
	movl	12(%r15), %edi
	.p2align 4,,10
	.p2align 3
.L2323:
	addq	$1, %r14
	cmpl	%r14d, %edi
	jle	.L2356
.L2326:
	movq	(%r15), %rax
	leaq	(%rax,%r14,8), %rdx
	movl	(%rdx), %eax
	cmpl	%eax, %ebx
	jl	.L2323
	cmpl	%ebx, 4(%rdx)
	movl	%ebx, %esi
	cmovle	4(%rdx), %esi
	movl	%eax, -96(%rbp)
	movl	16(%r11), %edx
	movl	%esi, -92(%rbp)
	cmpl	%edx, %eax
	jg	.L2323
	movq	24(%r11), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi,%r10), %rdi
	cmpl	%edx, %esi
	jg	.L2357
	movq	%r12, %rsi
	addq	$1, %r14
	call	_ZN2v88internal22BoyerMoorePositionInfo11SetIntervalERKNS0_8IntervalE
	movl	12(%r15), %edi
	cmpl	%r14d, %edi
	jg	.L2326
	.p2align 4,,10
	.p2align 3
.L2356:
	movq	%r11, %r15
	jmp	.L2321
.L2350:
	movq	%r15, %rax
	movl	%r13d, %r15d
	movq	%rax, %r13
	cmpl	%edx, %r15d
	jl	.L2303
	movl	-132(%rbp), %edx
	testl	%edx, %edx
	jne	.L2299
.L2348:
	movzbl	-136(%rbp), %eax
	movq	-112(%rbp), %rcx
	movq	%r13, 32(%rcx,%rax,8)
	jmp	.L2299
.L2303:
	movq	-112(%rbp), %rbx
	movl	-148(%rbp), %ecx
	movq	%r13, %r8
	movl	%r15d, %edx
	movq	-144(%rbp), %rsi
	movl	$1, %r9d
	movq	56(%rbx), %rdi
	subl	$1, %ecx
	movq	(%rdi), %rax
	call	*72(%rax)
	movl	-132(%rbp), %eax
	testl	%eax, %eax
	jne	.L2299
	movzbl	-136(%rbp), %eax
	movq	%r13, 32(%rbx,%rax,8)
	jmp	.L2299
.L2351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20556:
	.size	_ZN2v88internal8TextNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb, .-_ZN2v88internal8TextNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.section	.text._ZN2v88internalL17EmitAtomNonLetterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL17EmitAtomNonLetterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb, @function
_ZN2v88internalL17EmitAtomNonLetterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb:
.LFB20383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	leaq	-80(%rbp), %rdx
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movzwl	%r15w, %r13d
	pushq	%r12
	movl	%r13d, %edi
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	16(%rbp), %eax
	movq	40(%rsi), %r14
	movl	%r8d, -88(%rbp)
	movl	%r9d, -92(%rbp)
	movzbl	48(%rsi), %esi
	movl	%eax, -84(%rbp)
	movl	%esi, %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internalL25GetCaseIndependentLettersEPNS0_7IsolateEtbPji.constprop.0
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$1, %r8d
	je	.L2365
.L2358:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L2366
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2365:
	.cfi_restore_state
	cmpw	$255, %r15w
	seta	%al
	andb	%bl, %al
	jne	.L2363
	cmpb	$0, -84(%rbp)
	je	.L2367
.L2360:
	movq	(%r14), %rcx
	movb	%al, -84(%rbp)
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	*144(%rcx)
	movzbl	-84(%rbp), %eax
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2363:
	xorl	%eax, %eax
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2367:
	movl	-92(%rbp), %ebx
	movl	-88(%rbp), %esi
	movl	$-1, %r9d
	movq	%r12, %rdx
	movq	%r14, %rdi
	movzbl	%bl, %ecx
	call	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii@PLT
	movl	%ebx, %eax
	jmp	.L2360
.L2366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20383:
	.size	_ZN2v88internalL17EmitAtomNonLetterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb, .-_ZN2v88internalL17EmitAtomNonLetterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb
	.section	.text._ZN2v88internalL14EmitAtomLetterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL14EmitAtomLetterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb, @function
_ZN2v88internalL14EmitAtomLetterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb:
.LFB20385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	%dx, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	leaq	-80(%rbp), %r8
	pushq	%r12
	movq	%r8, %rdx
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	40(%rsi), %r15
	movzbl	48(%rsi), %esi
	movl	%r9d, -100(%rbp)
	movl	16(%rbp), %ebx
	movl	%esi, %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internalL25GetCaseIndependentLettersEPNS0_7IsolateEtbPji.constprop.0
	cmpl	$1, %eax
	jle	.L2378
	testb	%bl, %bl
	je	.L2385
.L2370:
	movq	$0, -88(%rbp)
	leaq	-88(%rbp), %r13
	cmpl	$3, %eax
	je	.L2371
	cmpl	$4, %eax
	je	.L2372
	cmpl	$2, %eax
	je	.L2386
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2386:
	movl	-76(%rbp), %edi
	movl	-80(%rbp), %esi
	cmpb	$1, %r14b
	sbbl	%edx, %edx
	movq	(%r15), %r9
	movl	%edi, %eax
	orb	$-1, %dl
	xorl	%esi, %eax
	movzwl	%ax, %ecx
	leal	-1(%rcx), %r8d
	testl	%ecx, %r8d
	je	.L2387
	movl	%edi, %ecx
	subl	%esi, %ecx
	movzwl	%cx, %r11d
	leal	-1(%r11), %eax
	testl	%r11d, %eax
	sete	%r8b
	cmpw	%cx, %si
	setnb	%al
	andb	%r8b, %al
	jne	.L2388
	leaq	-88(%rbp), %r13
	movq	%r15, %rdi
	movq	%r13, %rdx
	call	*72(%r9)
	movq	(%r15), %rax
	movl	-76(%rbp), %esi
	movq	%r12, %rdx
	jmp	.L2384
	.p2align 4,,10
	.p2align 3
.L2372:
	movq	(%r15), %rax
	leaq	-88(%rbp), %r13
	movl	-68(%rbp), %esi
	movq	%r15, %rdi
	movq	%r13, %rdx
	call	*72(%rax)
.L2371:
	movq	(%r15), %rax
	movl	-80(%rbp), %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	*72(%rax)
	movq	(%r15), %rax
	movl	-76(%rbp), %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	*72(%rax)
	movq	(%r15), %rax
	movl	-72(%rbp), %esi
	movq	%r12, %rdx
.L2384:
	movq	%r15, %rdi
	call	*144(%rax)
	movq	(%r15), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*64(%rax)
	movl	$1, %eax
.L2368:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2389
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2385:
	.cfi_restore_state
	movzbl	-100(%rbp), %ecx
	movl	$-1, %r9d
	movq	%r12, %rdx
	movl	%r13d, %esi
	movl	$1, %r8d
	movq	%r15, %rdi
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii@PLT
	movl	-104(%rbp), %eax
	jmp	.L2370
	.p2align 4,,10
	.p2align 3
.L2378:
	xorl	%eax, %eax
	jmp	.L2368
	.p2align 4,,10
	.p2align 3
.L2387:
	xorl	%eax, %edx
	movzwl	%si, %esi
	movq	%r12, %rcx
	movq	%r15, %rdi
	movzwl	%dx, %edx
	call	*152(%r9)
	movl	$1, %eax
	jmp	.L2368
	.p2align 4,,10
	.p2align 3
.L2388:
	addl	%esi, %esi
	xorl	%ecx, %edx
	movb	%al, -100(%rbp)
	movq	%r12, %r8
	subl	%edi, %esi
	movzwl	%dx, %ecx
	movq	%r15, %rdi
	movl	%r11d, %edx
	movzwl	%si, %esi
	call	*160(%r9)
	movzbl	-100(%rbp), %eax
	jmp	.L2368
.L2389:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20385:
	.size	_ZN2v88internalL14EmitAtomLetterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb, .-_ZN2v88internalL14EmitAtomLetterEPNS0_7IsolateEPNS0_14RegExpCompilerEtPNS0_5LabelEibb
	.section	.text._ZN2v88internal8TextNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8TextNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.type	_ZN2v88internal8TextNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, @function
_ZN2v88internal8TextNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib:
.LFB20410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -136(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpb	$0, 72(%rdi)
	jne	.L2390
	movl	(%rsi), %ecx
	cmpb	$1, 48(%rdx)
	movq	%rsi, %r15
	sbbl	%r13d, %r13d
	movl	%ecx, -148(%rbp)
	movq	64(%rdi), %rcx
	andl	$65280, %r13d
	addl	$255, %r13d
	movl	12(%rcx), %edx
	testl	%edx, %edx
	jle	.L2394
	leaq	4(%rsi), %rax
	movq	$0, -88(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
.L2418:
	movq	-88(%rbp), %rax
	salq	$4, %rax
	addq	(%rcx), %rax
	movq	8(%rax), %r8
	movl	4(%rax), %eax
	testl	%eax, %eax
	je	.L2432
	movq	-144(%rbp), %rdx
	movslq	%r14d, %rax
	leaq	8(%r8), %rdi
	movq	%r8, -96(%rbp)
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,2), %rbx
	movq	-136(%rbp), %rax
	movq	48(%rax), %rsi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE@PLT
	movq	-96(%rbp), %r8
	testb	$1, 28(%r8)
	je	.L2408
	movl	$0, (%rbx)
.L2409:
	addl	$1, %r14d
	cmpl	(%r15), %r14d
	je	.L2390
	movq	-136(%rbp), %rax
	movq	64(%rax), %rcx
.L2396:
	addq	$1, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpl	%eax, 12(%rcx)
	jg	.L2418
.L2394:
	cmpb	$0, 36(%r15)
	jne	.L2390
	movq	-120(%rbp), %rdx
	movl	$1, %r8d
	movl	%r14d, %ecx
	movq	%r15, %rsi
	movq	-136(%rbp), %rax
	movq	56(%rax), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L2390:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2433
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2432:
	.cfi_restore_state
	movl	-148(%rbp), %esi
	movq	8(%r8), %rax
	movq	16(%r8), %rdx
	testl	%esi, %esi
	jle	.L2396
	testl	%edx, %edx
	jle	.L2396
	leal	1(%r14), %ebx
	subl	$1, %edx
	movslq	%r14d, %r14
	leaq	2(%r14,%rdx), %r11
	leal	-1(%rsi), %edx
	movslq	%ebx, %rbx
	leaq	(%r14,%r14,2), %rcx
	leaq	1(%r14,%rdx), %r10
	negq	%r14
	leaq	4(%r15,%rcx,2), %r12
	leaq	(%rax,%r14,2), %rcx
	movq	%r15, %rax
	movq	%r10, %r14
	movq	%r8, %r15
	movq	%rax, %r8
	jmp	.L2397
	.p2align 4,,10
	.p2align 3
.L2398:
	cmpl	%edi, %r13d
	jl	.L2431
	movw	%r13w, (%r12)
	movw	%di, 2(%r12)
	movb	$1, 4(%r12)
.L2404:
	movl	%ebx, %eax
	cmpl	%ebx, (%r8)
	je	.L2390
	cmpq	%rbx, %r14
	je	.L2430
	addq	$1, %rbx
	addq	$6, %r12
	cmpq	%rbx, %r11
	je	.L2430
.L2397:
	movzwl	-2(%rcx,%rbx,2), %edi
	testb	$2, 24(%r15)
	je	.L2398
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movzbl	48(%rax), %esi
	movq	%r11, -96(%rbp)
	call	_ZN2v88internalL25GetCaseIndependentLettersEPNS0_7IsolateEtbPji.constprop.0
	movq	-96(%rbp), %r11
	movq	-104(%rbp), %rcx
	testl	%eax, %eax
	movq	-112(%rbp), %r8
	je	.L2431
	movl	-80(%rbp), %edx
	cmpl	$1, %eax
	jne	.L2400
	movw	%r13w, (%r12)
	movw	%dx, 2(%r12)
	movb	$1, 4(%r12)
	jmp	.L2404
	.p2align 4,,10
	.p2align 3
.L2400:
	movl	%r13d, %r9d
	movl	%r13d, %esi
	notl	%r9d
	jle	.L2402
	movl	%r13d, %edi
	andl	-76(%rbp), %esi
	xorl	%edx, %edi
	xorl	%edi, %esi
	movl	%esi, %edi
	andl	%edx, %edi
	cmpl	$2, %eax
	je	.L2421
	xorl	-72(%rbp), %edx
	notl	%edx
	andl	%edx, %esi
	movl	%esi, %edx
	andl	%edi, %edx
	cmpl	$3, %eax
	je	.L2403
	xorl	-68(%rbp), %edi
	notl	%edi
	andl	%edi, %esi
	andl	%esi, %edx
.L2403:
	orl	%esi, %r9d
	cmpl	$2, %eax
	je	.L2434
.L2402:
	movw	%si, (%r12)
	movw	%dx, 2(%r12)
	jmp	.L2404
	.p2align 4,,10
	.p2align 3
.L2430:
	movl	%eax, %r14d
	movq	-136(%rbp), %rax
	movq	%r8, %r15
	movq	64(%rax), %rcx
	jmp	.L2396
	.p2align 4,,10
	.p2align 3
.L2434:
	movl	%r9d, %edi
	movl	$-2, %eax
	notl	%edi
	subl	%r9d, %eax
	testl	%eax, %edi
	jne	.L2402
	movb	$1, 4(%r12)
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2408:
	movq	(%rax), %rcx
	xorl	%edx, %edx
	jmp	.L2411
	.p2align 4,,10
	.p2align 3
.L2436:
	addq	$8, %rcx
	cmpl	%edx, 12(%rax)
	je	.L2435
.L2411:
	movl	(%rcx), %r8d
	addl	$1, %edx
	cmpl	%r8d, %r13d
	jl	.L2436
	movl	4(%rcx), %ecx
	movzwl	%cx, %esi
	cmpl	%esi, %r13d
	jge	.L2412
	movl	%r13d, %esi
	movl	%r13d, %ecx
.L2412:
	xorl	%r8d, %ecx
	movzwl	%r8w, %r8d
	movzwl	%cx, %ecx
	leal	1(%rcx), %edi
	testl	%ecx, %edi
	jne	.L2413
	leal	(%rcx,%r8), %edi
	cmpl	%esi, %edi
	jne	.L2413
	movb	$1, 4(%rbx)
	.p2align 4,,10
	.p2align 3
.L2413:
	movl	%ecx, %esi
	movl	12(%rax), %edi
	shrl	%esi
	orl	%esi, %ecx
	movl	%ecx, %esi
	shrl	$2, %esi
	orl	%esi, %ecx
	movl	%ecx, %esi
	shrl	$4, %esi
	orl	%esi, %ecx
	movl	%ecx, %esi
	shrl	$8, %esi
	orl	%esi, %ecx
	notl	%ecx
	andl	%ecx, %r8d
	cmpl	%edx, %edi
	jle	.L2414
	movslq	%edx, %r10
	salq	$3, %r10
	.p2align 4,,10
	.p2align 3
.L2417:
	movq	(%rax), %rsi
	addq	%r10, %rsi
	movl	(%rsi), %r9d
	movzwl	%r9w, %r11d
	cmpl	%r11d, %r13d
	jl	.L2415
	movl	4(%rsi), %esi
	movb	$0, 4(%rbx)
	movzwl	%si, %edi
	cmpl	%edi, %r13d
	cmovl	%r13d, %esi
	andl	%ecx, %r11d
	xorl	%esi, %r9d
	movzwl	%r9w, %r9d
	movl	%r9d, %esi
	shrl	%esi
	orl	%r9d, %esi
	movl	%esi, %r9d
	shrl	$2, %r9d
	orl	%r9d, %esi
	movl	%r8d, %r9d
	movl	%esi, %edi
	xorl	%ecx, %r9d
	shrl	$4, %edi
	xorl	%r9d, %r11d
	orl	%edi, %esi
	movl	%esi, %edi
	movl	%esi, %ecx
	shrl	$8, %edi
	orl	%edi, %ecx
	movl	12(%rax), %edi
	notl	%ecx
	andl	%r11d, %ecx
	andl	%ecx, %r8d
.L2415:
	addl	$1, %edx
	addq	$8, %r10
	cmpl	%edi, %edx
	jl	.L2417
.L2414:
	movw	%cx, (%rbx)
	movw	%r8w, 2(%rbx)
	jmp	.L2409
	.p2align 4,,10
	.p2align 3
.L2421:
	movl	%edi, %edx
	jmp	.L2403
	.p2align 4,,10
	.p2align 3
.L2431:
	movb	$1, 36(%r8)
	movb	$0, 4(%r12)
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2435:
	movb	$1, 36(%r15)
	movb	$0, 4(%rbx)
	jmp	.L2390
.L2433:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20410:
	.size	_ZN2v88internal8TextNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib, .-_ZN2v88internal8TextNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.weak	_ZTVN2v88internal10RegExpTreeE
	.section	.data.rel.ro._ZTVN2v88internal10RegExpTreeE,"awG",@progbits,_ZTVN2v88internal10RegExpTreeE,comdat
	.align 8
	.type	_ZTVN2v88internal10RegExpTreeE, @object
	.size	_ZTVN2v88internal10RegExpTreeE, 296
_ZTVN2v88internal10RegExpTreeE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal10RegExpTree13IsTextElementEv
	.quad	_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv
	.quad	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal10RegExpTree16CaptureRegistersEv
	.quad	_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.quad	_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13AsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree13IsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree11AsAssertionEv
	.quad	_ZN2v88internal10RegExpTree11IsAssertionEv
	.quad	_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree16IsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree6AsAtomEv
	.quad	_ZN2v88internal10RegExpTree6IsAtomEv
	.quad	_ZN2v88internal10RegExpTree12AsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree12IsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree9AsCaptureEv
	.quad	_ZN2v88internal10RegExpTree9IsCaptureEv
	.quad	_ZN2v88internal10RegExpTree7AsGroupEv
	.quad	_ZN2v88internal10RegExpTree7IsGroupEv
	.quad	_ZN2v88internal10RegExpTree12AsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree12IsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree15IsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree7AsEmptyEv
	.quad	_ZN2v88internal10RegExpTree7IsEmptyEv
	.quad	_ZN2v88internal10RegExpTree6AsTextEv
	.quad	_ZN2v88internal10RegExpTree6IsTextEv
	.weak	_ZTVN2v88internal23NegativeSubmatchSuccessE
	.section	.data.rel.ro.local._ZTVN2v88internal23NegativeSubmatchSuccessE,"awG",@progbits,_ZTVN2v88internal23NegativeSubmatchSuccessE,comdat
	.align 8
	.type	_ZTVN2v88internal23NegativeSubmatchSuccessE, @object
	.size	_ZTVN2v88internal23NegativeSubmatchSuccessE, 104
_ZTVN2v88internal23NegativeSubmatchSuccessE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23NegativeSubmatchSuccessD1Ev
	.quad	_ZN2v88internal23NegativeSubmatchSuccessD0Ev
	.quad	_ZN2v88internal7EndNode6AcceptEPNS0_11NodeVisitorE
	.quad	_ZN2v88internal23NegativeSubmatchSuccess4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.quad	_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv
	.quad	_ZN2v88internal7EndNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv
	.quad	_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.quad	_ZN2v88internal7EndNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.quad	_ZN2v88internal10RegExpNode13FilterOneByteEi
	.weak	_ZTVN2v88internal7EndNodeE
	.section	.data.rel.ro.local._ZTVN2v88internal7EndNodeE,"awG",@progbits,_ZTVN2v88internal7EndNodeE,comdat
	.align 8
	.type	_ZTVN2v88internal7EndNodeE, @object
	.size	_ZTVN2v88internal7EndNodeE, 104
_ZTVN2v88internal7EndNodeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal7EndNodeD1Ev
	.quad	_ZN2v88internal7EndNodeD0Ev
	.quad	_ZN2v88internal7EndNode6AcceptEPNS0_11NodeVisitorE
	.quad	_ZN2v88internal7EndNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.quad	_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv
	.quad	_ZN2v88internal7EndNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv
	.quad	_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.quad	_ZN2v88internal7EndNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.quad	_ZN2v88internal10RegExpNode13FilterOneByteEi
	.weak	_ZTVN2v88internal10ActionNodeE
	.section	.data.rel.ro.local._ZTVN2v88internal10ActionNodeE,"awG",@progbits,_ZTVN2v88internal10ActionNodeE,comdat
	.align 8
	.type	_ZTVN2v88internal10ActionNodeE, @object
	.size	_ZTVN2v88internal10ActionNodeE, 104
_ZTVN2v88internal10ActionNodeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal10ActionNodeD1Ev
	.quad	_ZN2v88internal10ActionNodeD0Ev
	.quad	_ZN2v88internal10ActionNode6AcceptEPNS0_11NodeVisitorE
	.quad	_ZN2v88internal10ActionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.quad	_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv
	.quad	_ZN2v88internal10ActionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10ActionNode20GreedyLoopTextLengthEv
	.quad	_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.quad	_ZN2v88internal10ActionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.quad	_ZN2v88internal13SeqRegExpNode13FilterOneByteEi
	.weak	_ZTVN2v88internal10ChoiceNodeE
	.section	.data.rel.ro.local._ZTVN2v88internal10ChoiceNodeE,"awG",@progbits,_ZTVN2v88internal10ChoiceNodeE,comdat
	.align 8
	.type	_ZTVN2v88internal10ChoiceNodeE, @object
	.size	_ZTVN2v88internal10ChoiceNodeE, 120
_ZTVN2v88internal10ChoiceNodeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal10ChoiceNodeD1Ev
	.quad	_ZN2v88internal10ChoiceNodeD0Ev
	.quad	_ZN2v88internal10ChoiceNode6AcceptEPNS0_11NodeVisitorE
	.quad	_ZN2v88internal10ChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.quad	_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv
	.quad	_ZN2v88internal10ChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv
	.quad	_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.quad	_ZN2v88internal10ChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.quad	_ZN2v88internal10ChoiceNode13FilterOneByteEi
	.quad	_ZN2v88internal10ChoiceNode39try_to_emit_quick_check_for_alternativeEb
	.quad	_ZN2v88internal10ChoiceNode13read_backwardEv
	.weak	_ZTVN2v88internal17BackReferenceNodeE
	.section	.data.rel.ro.local._ZTVN2v88internal17BackReferenceNodeE,"awG",@progbits,_ZTVN2v88internal17BackReferenceNodeE,comdat
	.align 8
	.type	_ZTVN2v88internal17BackReferenceNodeE, @object
	.size	_ZTVN2v88internal17BackReferenceNodeE, 104
_ZTVN2v88internal17BackReferenceNodeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal17BackReferenceNodeD1Ev
	.quad	_ZN2v88internal17BackReferenceNodeD0Ev
	.quad	_ZN2v88internal17BackReferenceNode6AcceptEPNS0_11NodeVisitorE
	.quad	_ZN2v88internal17BackReferenceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.quad	_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv
	.quad	_ZN2v88internal17BackReferenceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv
	.quad	_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.quad	_ZN2v88internal17BackReferenceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.quad	_ZN2v88internal13SeqRegExpNode13FilterOneByteEi
	.weak	_ZTVN2v88internal13AssertionNodeE
	.section	.data.rel.ro.local._ZTVN2v88internal13AssertionNodeE,"awG",@progbits,_ZTVN2v88internal13AssertionNodeE,comdat
	.align 8
	.type	_ZTVN2v88internal13AssertionNodeE, @object
	.size	_ZTVN2v88internal13AssertionNodeE, 104
_ZTVN2v88internal13AssertionNodeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal13AssertionNodeD1Ev
	.quad	_ZN2v88internal13AssertionNodeD0Ev
	.quad	_ZN2v88internal13AssertionNode6AcceptEPNS0_11NodeVisitorE
	.quad	_ZN2v88internal13AssertionNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.quad	_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv
	.quad	_ZN2v88internal13AssertionNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv
	.quad	_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.quad	_ZN2v88internal13AssertionNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.quad	_ZN2v88internal13SeqRegExpNode13FilterOneByteEi
	.weak	_ZTVN2v88internal8TextNodeE
	.section	.data.rel.ro.local._ZTVN2v88internal8TextNodeE,"awG",@progbits,_ZTVN2v88internal8TextNodeE,comdat
	.align 8
	.type	_ZTVN2v88internal8TextNodeE, @object
	.size	_ZTVN2v88internal8TextNodeE, 104
_ZTVN2v88internal8TextNodeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8TextNodeD1Ev
	.quad	_ZN2v88internal8TextNodeD0Ev
	.quad	_ZN2v88internal8TextNode6AcceptEPNS0_11NodeVisitorE
	.quad	_ZN2v88internal8TextNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.quad	_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv
	.quad	_ZN2v88internal8TextNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal8TextNode20GreedyLoopTextLengthEv
	.quad	_ZN2v88internal8TextNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.quad	_ZN2v88internal8TextNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.quad	_ZN2v88internal8TextNode13FilterOneByteEi
	.weak	_ZTVN2v88internal10RegExpNodeE
	.section	.data.rel.ro._ZTVN2v88internal10RegExpNodeE,"awG",@progbits,_ZTVN2v88internal10RegExpNodeE,comdat
	.align 8
	.type	_ZTVN2v88internal10RegExpNodeE, @object
	.size	_ZTVN2v88internal10RegExpNodeE, 104
_ZTVN2v88internal10RegExpNodeE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv
	.quad	_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.quad	_ZN2v88internal10RegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.quad	_ZN2v88internal10RegExpNode13FilterOneByteEi
	.weak	_ZTVN2v88internal28NegativeLookaroundChoiceNodeE
	.section	.data.rel.ro.local._ZTVN2v88internal28NegativeLookaroundChoiceNodeE,"awG",@progbits,_ZTVN2v88internal28NegativeLookaroundChoiceNodeE,comdat
	.align 8
	.type	_ZTVN2v88internal28NegativeLookaroundChoiceNodeE, @object
	.size	_ZTVN2v88internal28NegativeLookaroundChoiceNodeE, 120
_ZTVN2v88internal28NegativeLookaroundChoiceNodeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal28NegativeLookaroundChoiceNodeD1Ev
	.quad	_ZN2v88internal28NegativeLookaroundChoiceNodeD0Ev
	.quad	_ZN2v88internal28NegativeLookaroundChoiceNode6AcceptEPNS0_11NodeVisitorE
	.quad	_ZN2v88internal10ChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.quad	_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv
	.quad	_ZN2v88internal28NegativeLookaroundChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv
	.quad	_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.quad	_ZN2v88internal28NegativeLookaroundChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.quad	_ZN2v88internal28NegativeLookaroundChoiceNode13FilterOneByteEi
	.quad	_ZN2v88internal28NegativeLookaroundChoiceNode39try_to_emit_quick_check_for_alternativeEb
	.quad	_ZN2v88internal10ChoiceNode13read_backwardEv
	.weak	_ZTVN2v88internal13SeqRegExpNodeE
	.section	.data.rel.ro._ZTVN2v88internal13SeqRegExpNodeE,"awG",@progbits,_ZTVN2v88internal13SeqRegExpNodeE,comdat
	.align 8
	.type	_ZTVN2v88internal13SeqRegExpNodeE, @object
	.size	_ZTVN2v88internal13SeqRegExpNodeE, 104
_ZTVN2v88internal13SeqRegExpNodeE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal10RegExpNode24EatsAtLeastFromLoopEntryEv
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal10RegExpNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv
	.quad	_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.quad	_ZN2v88internal13SeqRegExpNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.quad	_ZN2v88internal13SeqRegExpNode13FilterOneByteEi
	.weak	_ZTVN2v88internal14LoopChoiceNodeE
	.section	.data.rel.ro.local._ZTVN2v88internal14LoopChoiceNodeE,"awG",@progbits,_ZTVN2v88internal14LoopChoiceNodeE,comdat
	.align 8
	.type	_ZTVN2v88internal14LoopChoiceNodeE, @object
	.size	_ZTVN2v88internal14LoopChoiceNodeE, 120
_ZTVN2v88internal14LoopChoiceNodeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14LoopChoiceNodeD1Ev
	.quad	_ZN2v88internal14LoopChoiceNodeD0Ev
	.quad	_ZN2v88internal14LoopChoiceNode6AcceptEPNS0_11NodeVisitorE
	.quad	_ZN2v88internal14LoopChoiceNode4EmitEPNS0_14RegExpCompilerEPNS0_5TraceE
	.quad	_ZN2v88internal14LoopChoiceNode24EatsAtLeastFromLoopEntryEv
	.quad	_ZN2v88internal14LoopChoiceNode20GetQuickCheckDetailsEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal14LoopChoiceNode33GetQuickCheckDetailsFromLoopEntryEPNS0_17QuickCheckDetailsEPNS0_14RegExpCompilerEib
	.quad	_ZN2v88internal10RegExpNode20GreedyLoopTextLengthEv
	.quad	_ZN2v88internal10RegExpNode32GetSuccessorOfOmnivorousTextNodeEPNS0_14RegExpCompilerE
	.quad	_ZN2v88internal14LoopChoiceNode12FillInBMInfoEPNS0_7IsolateEiiPNS0_19BoyerMooreLookaheadEb
	.quad	_ZN2v88internal14LoopChoiceNode13FilterOneByteEi
	.quad	_ZN2v88internal10ChoiceNode39try_to_emit_quick_check_for_alternativeEb
	.quad	_ZN2v88internal14LoopChoiceNode13read_backwardEv
	.section	.data.rel.ro.local._ZTVN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEEE,"aw"
	.align 8
	.type	_ZTVN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEEE, @object
	.size	_ZTVN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEEE, 96
_ZTVN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED1Ev
	.quad	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEED0Ev
	.quad	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE8VisitEndEPNS0_7EndNodeE
	.quad	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitActionEPNS0_10ActionNodeE
	.quad	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE11VisitChoiceEPNS0_10ChoiceNodeE
	.quad	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE15VisitLoopChoiceEPNS0_14LoopChoiceNodeE
	.quad	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE29VisitNegativeLookaroundChoiceEPNS0_28NegativeLookaroundChoiceNodeE
	.quad	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE18VisitBackReferenceEPNS0_17BackReferenceNodeE
	.quad	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE14VisitAssertionEPNS0_13AssertionNodeE
	.quad	_ZN2v88internal8AnalysisIJNS0_12_GLOBAL__N_119AssertionPropagatorENS2_21EatsAtLeastPropagatorEEE9VisitTextEPNS0_8TextNodeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	0
	.long	1
	.long	2
	.long	3
	.align 16
.LC3:
	.long	0
	.long	-1
	.long	0
	.long	-1
	.align 16
.LC4:
	.long	4
	.long	4
	.long	4
	.long	4
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
