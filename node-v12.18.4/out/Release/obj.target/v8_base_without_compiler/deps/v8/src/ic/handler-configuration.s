	.file	"handler-configuration.cc"
	.text
	.section	.text._ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb0EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb0EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0, @function
_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb0EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0:
.LFB24807:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdx), %rax
	cmpw	$67, 11(%rax)
	jbe	.L2
	testb	$32, 13(%rax)
	je	.L3
.L2:
	movq	(%r12), %rax
	movq	41112(%rbx), %rdi
	movq	(%rax), %rsi
	sarq	$32, %rsi
	orl	$16, %esi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L4
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L5:
	movq	%rax, (%r12)
	movl	$2, %edx
	movl	$3, %eax
.L7:
	testq	%r13, %r13
	cmove	%edx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L17
.L6:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L3:
	movl	15(%rax), %edx
	andl	$2097152, %edx
	je	.L13
	cmpw	$1025, 11(%rax)
	je	.L13
	movq	(%rsi), %rax
	movq	41112(%rdi), %rdi
	movq	(%rax), %rsi
	sarq	$32, %rsi
	orl	$32, %esi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L8
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L9:
	movq	%rax, (%r12)
	movl	$1, %edx
	movl	$2, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$2, %eax
	movl	$1, %edx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L8:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L18
.L10:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L9
.L18:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L10
	.cfi_endproc
.LFE24807:
	.size	_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb0EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0, .-_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb0EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0
	.set	_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_12StoreHandlerELb0EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0,_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb0EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb1EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"(location_) != nullptr"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb1EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb1EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0, @function
_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb1EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0:
.LFB24816:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%rbp), %rax
	movq	24(%rbp), %r13
	movq	%rax, -56(%rbp)
	movq	(%rdx), %rax
	cmpw	$67, 11(%rax)
	jbe	.L20
	testb	$32, 13(%rax)
	je	.L21
.L20:
	movq	12464(%rbx), %rax
	movq	39(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L22
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L23:
	movq	(%r12), %rdi
	movq	%rsi, %rax
	orq	$2, %rax
	movq	%rax, 31(%rdi)
	leaq	31(%rdi), %r8
	testb	$1, %al
	je	.L49
	cmpl	$3, %eax
	je	.L49
	movq	%rsi, %rbx
	movq	%rsi, %rdx
	andq	$-262144, %rbx
	andq	$-3, %rdx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L87
	testb	$24, %al
	je	.L49
.L93:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L49
	movq	%r8, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	movq	(%r12), %rdi
	movl	$2, %r8d
	testl	%r15d, %r15d
	je	.L88
.L29:
	testq	%r14, %r14
	je	.L32
	movq	(%r14), %rax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L22:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L89
.L24:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L21:
	movl	15(%rax), %eax
	movl	$1, %r8d
	movq	(%r12), %rdi
	testl	%r15d, %r15d
	jne	.L29
.L88:
	testq	%r14, %r14
	je	.L32
	movq	(%r14), %rax
	orq	$2, %rax
.L31:
	movq	%rax, 23(%rdi)
	leaq	23(%rdi), %r14
	testb	$1, %al
	je	.L48
	cmpl	$3, %eax
	je	.L48
	movq	%rax, %r15
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r15
	testl	$262144, %eax
	jne	.L90
	testb	$24, %al
	je	.L48
.L92:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L48
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	%r8d, -64(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-64(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L48:
	testq	%r13, %r13
	je	.L19
	movq	0(%r13), %rax
	movl	-56(%rbp), %ecx
	movq	(%r12), %r12
	movq	%rax, %rdx
	orq	$2, %rdx
	testl	%ecx, %ecx
	cmove	%rdx, %rax
	cmpl	$1, %r8d
	je	.L91
	movq	%rax, 39(%r12)
	leaq	39(%r12), %r13
	testb	$1, %al
	je	.L42
.L86:
	cmpl	$3, %eax
	je	.L42
	movq	%rax, %r14
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r14
	testl	$262144, %eax
	je	.L45
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movl	-56(%rbp), %r8d
.L45:
	testb	$24, %al
	je	.L42
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L42
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-56(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L42:
	addl	$1, %r8d
.L19:
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	%rax, 31(%r12)
	leaq	31(%r12), %r13
	testb	$1, %al
	je	.L42
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	%r8d, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movl	-72(%rbp), %r8d
	movq	-64(%rbp), %rdi
	testb	$24, %al
	jne	.L92
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r8, %rsi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rdi
	testb	$24, %al
	jne	.L93
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	jmp	.L24
	.cfi_endproc
.LFE24816:
	.size	_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb1EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0, .-_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb1EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0
	.section	.text._ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_
	.type	_ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_, @function
_ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_:
.LFB20269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -56(%rbp)
	movl	%r8d, -68(%rbp)
	movq	%rax, -80(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -64(%rbp)
	testq	%r9, %r9
	je	.L119
.L96:
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb0EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	movl	%r15d, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal7Factory14NewLoadHandlerEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %rdi
	movq	%rax, %r15
	movq	-56(%rbp), %rax
	movq	(%rax), %rdx
	leaq	7(%rdi), %rsi
	movq	%rdx, 7(%rdi)
	testb	$1, %dl
	je	.L104
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -88(%rbp)
	testl	$262144, %ecx
	jne	.L120
	andl	$24, %ecx
	je	.L104
.L125:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L121
	.p2align 4,,10
	.p2align 3
.L104:
	movq	(%r15), %rdi
	movq	0(%r13), %r13
	movq	%r13, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r13b
	je	.L103
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L122
	testb	$24, %al
	je	.L103
.L124:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L123
.L103:
	movq	-64(%rbp), %rdx
	movq	-80(%rbp), %rax
	movq	%r14, %r8
	movq	%r15, %rsi
	movl	-68(%rbp), %ecx
	movq	%r12, %rdi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	%rax
	call	_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb1EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L124
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L120:
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rax
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rax), %rcx
	andl	$24, %ecx
	jne	.L125
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$0, -68(%rbp)
	movq	%rdx, %r14
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L121:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L103
	.cfi_endproc
.LFE20269:
	.size	_ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_, .-_ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_
	.section	.text._ZN2v88internal11LoadHandler13LoadFullChainEPNS0_7IsolateENS0_6HandleINS0_3MapEEERKNS0_17MaybeObjectHandleENS4_INS0_3SmiEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11LoadHandler13LoadFullChainEPNS0_7IsolateENS0_6HandleINS0_3MapEEERKNS0_17MaybeObjectHandleENS4_INS0_3SmiEEE
	.type	_ZN2v88internal11LoadHandler13LoadFullChainEPNS0_7IsolateENS0_6HandleINS0_3MapEEERKNS0_17MaybeObjectHandleENS4_INS0_3SmiEEE, @function
_ZN2v88internal11LoadHandler13LoadFullChainEPNS0_7IsolateENS0_6HandleINS0_3MapEEERKNS0_17MaybeObjectHandleENS4_INS0_3SmiEEE:
.LFB20270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	-56(%rbp), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %eax
	movq	8(%rdx), %r15
	movq	%rcx, -56(%rbp)
	movq	%r13, %rdx
	xorl	%ecx, %ecx
	movl	%eax, -60(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb0EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	testb	$1, (%rax)
	jne	.L127
	movq	-56(%rbp), %rax
	testb	$32, 4(%rax)
	jne	.L127
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movl	%ebx, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NewLoadHandlerEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %rdi
	movq	%rax, %rbx
	movq	-56(%rbp), %rax
	movq	(%rax), %rdx
	leaq	7(%rdi), %rsi
	movq	%rdx, 7(%rdi)
	testb	$1, %dl
	je	.L136
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -72(%rbp)
	testl	$262144, %eax
	je	.L130
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%r8), %rax
.L130:
	testb	$24, %al
	je	.L136
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L153
	.p2align 4,,10
	.p2align 3
.L136:
	movq	(%rbx), %rdi
	movq	(%r14), %rdx
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L135
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	je	.L133
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
.L133:
	testb	$24, %al
	je	.L135
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L154
.L135:
	xorl	%eax, %eax
	xorl	%edx, %edx
	movl	-60(%rbp), %ecx
	movq	%r15, %r8
	movabsq	$-4294967296, %rdi
	movq	%rax, %rsi
	pushq	%rdx
	movq	%r13, %rdx
	andq	%rdi, %rsi
	movq	%r12, %rdi
	orq	$1, %rsi
	pushq	%rsi
	movq	%rbx, %rsi
	call	_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_11LoadHandlerELb1EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0
	popq	%rdx
	popq	%rcx
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L154:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L135
	.cfi_endproc
.LFE20270:
	.size	_ZN2v88internal11LoadHandler13LoadFullChainEPNS0_7IsolateENS0_6HandleINS0_3MapEEERKNS0_17MaybeObjectHandleENS4_INS0_3SmiEEE, .-_ZN2v88internal11LoadHandler13LoadFullChainEPNS0_7IsolateENS0_6HandleINS0_3MapEEERKNS0_17MaybeObjectHandleENS4_INS0_3SmiEEE
	.section	.text._ZN2v88internal11LoadHandler22GetKeyedAccessLoadModeENS0_11MaybeObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11LoadHandler22GetKeyedAccessLoadModeENS0_11MaybeObjectE
	.type	_ZN2v88internal11LoadHandler22GetKeyedAccessLoadModeENS0_11MaybeObjectE, @function
_ZN2v88internal11LoadHandler22GetKeyedAccessLoadModeENS0_11MaybeObjectE:
.LFB20271:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$1, %dil
	jne	.L155
	movq	%rdi, %rdx
	shrq	$38, %rdi
	sarq	$32, %rdx
	andl	$1, %edi
	andl	$14, %edx
	cmove	%edi, %eax
.L155:
	ret
	.cfi_endproc
.LFE20271:
	.size	_ZN2v88internal11LoadHandler22GetKeyedAccessLoadModeENS0_11MaybeObjectE, .-_ZN2v88internal11LoadHandler22GetKeyedAccessLoadModeENS0_11MaybeObjectE
	.section	.text._ZN2v88internal12StoreHandler22StoreElementTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEES6_NS0_20KeyedAccessStoreModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StoreHandler22StoreElementTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEES6_NS0_20KeyedAccessStoreModeE
	.type	_ZN2v88internal12StoreHandler22StoreElementTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEES6_NS0_20KeyedAccessStoreModeE, @function
_ZN2v88internal12StoreHandler22StoreElementTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEES6_NS0_20KeyedAccessStoreModeE:
.LFB20275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	%ecx, %edx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11CodeFactory26ElementsTransitionAndStoreEPNS0_7IsolateENS0_20KeyedAccessStoreModeE@PLT
	movq	-80(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal7Factory15NewStoreHandlerEi@PLT
	movq	-88(%rbp), %rdx
	movq	(%rax), %r15
	movq	%rax, %r12
	movq	(%rdx), %r13
	movq	%r13, 7(%r15)
	testb	$1, %r13b
	je	.L172
	movq	%r13, %rcx
	leaq	7(%r15), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L193
	testb	$24, %al
	je	.L172
.L201:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L194
	.p2align 4,,10
	.p2align 3
.L172:
	movq	(%r12), %r15
	movq	(%r14), %r13
	movq	%r13, 15(%r15)
	leaq	15(%r15), %rsi
	testb	$1, %r13b
	je	.L171
	movq	%r13, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L195
	testb	$24, %al
	je	.L171
.L200:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L196
	.p2align 4,,10
	.p2align 3
.L171:
	movq	(%rbx), %rdx
	movq	(%r12), %r13
	movq	%rdx, %rax
	leaq	23(%r13), %r14
	orq	$2, %rax
	movq	%rax, 23(%r13)
	testb	$1, %al
	je	.L170
	cmpl	$3, %eax
	je	.L170
	movq	%rdx, %rbx
	movq	%rdx, %r15
	andq	$-262144, %rbx
	andq	$-3, %r15
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L197
.L168:
	testb	$24, %al
	je	.L170
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L198
	.p2align 4,,10
	.p2align 3
.L170:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-88(%rbp), %rsi
	testb	$24, %al
	jne	.L200
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L201
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L170
.L199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20275:
	.size	_ZN2v88internal12StoreHandler22StoreElementTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEES6_NS0_20KeyedAccessStoreModeE, .-_ZN2v88internal12StoreHandler22StoreElementTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEES6_NS0_20KeyedAccessStoreModeE
	.section	.text._ZN2v88internal12StoreHandler15StoreTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StoreHandler15StoreTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEE
	.type	_ZN2v88internal12StoreHandler15StoreTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal12StoreHandler15StoreTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEE:
.LFB20279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movl	15(%rax), %edx
	andl	$2097152, %edx
	jne	.L203
	movq	63(%rax), %rax
	testb	$1, %al
	jne	.L234
	testq	%rax, %rax
	je	.L206
.L205:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L206
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 63(%r14)
	leaq	63(%r14), %r15
	testb	$1, %r13b
	je	.L206
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L235
	testb	$24, %al
	je	.L206
.L237:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L206
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L206:
	addq	$8, %rsp
	movq	%r12, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	7(%rax), %rax
	testq	%rax, %rax
	jne	.L205
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L203:
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	xorl	%esi, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal7Factory15NewStoreHandlerEi@PLT
	movabsq	$171798691840, %rdx
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%rdx, 7(%rax)
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %r13b
	je	.L213
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L236
	testb	$24, %al
	je	.L213
.L238:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L213
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L213:
	addq	$8, %rsp
	movq	%r12, %rdx
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L237
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L238
	jmp	.L213
	.cfi_endproc
.LFE20279:
	.size	_ZN2v88internal12StoreHandler15StoreTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal12StoreHandler15StoreTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEE
	.section	.text._ZN2v88internal12StoreHandler21StoreThroughPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StoreHandler21StoreThroughPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_
	.type	_ZN2v88internal12StoreHandler21StoreThroughPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_, @function
_ZN2v88internal12StoreHandler21StoreThroughPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_:
.LFB20281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	24(%rbp), %r15
	movq	%rcx, -56(%rbp)
	movl	%r8d, -64(%rbp)
	movq	%rax, -80(%rbp)
	testq	%r9, %r9
	je	.L330
.L241:
	movq	%r12, %rdx
	movq	%r15, %rcx
	leaq	-56(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_123InitPrototypeChecksImplINS0_12StoreHandlerELb0EEEiPNS0_7IsolateENS0_6HandleIT_EEPNS6_INS0_3SmiEEENS6_INS0_3MapEEENS6_INS0_10JSReceiverEEENS0_17MaybeObjectHandleESG_.isra.0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, -72(%rbp)
	call	_ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	movl	-72(%rbp), %r8d
	movq	%r13, %rdi
	movq	%rax, %r14
	movl	%r8d, %esi
	call	_ZN2v88internal7Factory15NewStoreHandlerEi@PLT
	movq	(%rax), %rdi
	movq	%rax, %r8
	movq	-56(%rbp), %rax
	movq	(%rax), %rdx
	leaq	7(%rdi), %rsi
	movq	%rdx, 7(%rdi)
	testb	$1, %dl
	je	.L278
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L331
	testb	$24, %al
	je	.L278
.L339:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L332
	.p2align 4,,10
	.p2align 3
.L278:
	movq	(%r8), %rdi
	movq	(%r14), %r14
	movq	%r14, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r14b
	je	.L277
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L333
	testb	$24, %al
	je	.L277
.L338:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L334
	.p2align 4,,10
	.p2align 3
.L277:
	movq	(%r12), %rax
	cmpw	$67, 11(%rax)
	jbe	.L248
	testb	$32, 13(%rax)
	je	.L249
.L248:
	movq	12464(%r13), %rax
	movq	39(%rax), %r12
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L250
	movq	%r12, %rsi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r8
	movq	(%rax), %r12
.L251:
	movq	(%r8), %r14
	movq	%r12, %rax
	orq	$2, %rax
	movq	%rax, 31(%r14)
	leaq	31(%r14), %r9
	testb	$1, %al
	je	.L276
	cmpl	$3, %eax
	je	.L276
	movq	%r12, %rdx
	andq	$-262144, %r12
	movq	8(%r12), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	jne	.L335
	testb	$24, %al
	je	.L276
.L343:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L276
	movq	%r9, %rsi
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L276:
	movl	-64(%rbp), %esi
	movq	(%r8), %r12
	movl	$2, %r14d
	testl	%esi, %esi
	je	.L336
.L257:
	testq	%rbx, %rbx
	je	.L260
	movq	(%rbx), %rax
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L250:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L337
.L252:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r12, (%rax)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%r14, %rdx
	movq	%r8, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L338
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L331:
	movq	%r8, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	movq	-88(%rbp), %rdi
	testb	$24, %al
	jne	.L339
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L249:
	movl	15(%rax), %eax
	movl	-64(%rbp), %esi
	movl	$1, %r14d
	movq	(%r8), %r12
	testl	%esi, %esi
	jne	.L257
.L336:
	testq	%rbx, %rbx
	je	.L260
	movq	(%rbx), %rax
	orq	$2, %rax
.L259:
	movq	%rax, 23(%r12)
	leaq	23(%r12), %r13
	testb	$1, %al
	je	.L275
	cmpl	$3, %eax
	je	.L275
	movq	%rax, %rdx
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	jne	.L340
	testb	$24, %al
	je	.L275
.L342:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L275
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L275:
	testq	%r15, %r15
	je	.L293
	movq	(%r15), %rax
	movl	-80(%rbp), %ecx
	movq	(%r8), %r12
	movq	%rax, %rdx
	orq	$2, %rdx
	testl	%ecx, %ecx
	cmove	%rdx, %rax
	cmpl	$1, %r14d
	je	.L341
	movq	%rax, 39(%r12)
	leaq	39(%r12), %r13
	testb	$1, %al
	je	.L293
.L329:
	cmpl	$3, %eax
	je	.L293
	movq	%rax, %r14
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r14
	testl	$262144, %eax
	je	.L272
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-64(%rbp), %r8
.L272:
	testb	$24, %al
	je	.L293
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L293
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %r8
.L293:
	addq	$72, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	movl	$0, -64(%rbp)
	movq	%rdx, %rbx
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L341:
	movq	%rax, 31(%r12)
	leaq	31(%r12), %r13
	testb	$1, %al
	je	.L293
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L332:
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L334:
	movq	%r14, %rdx
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rdx
	testb	$24, %al
	jne	.L342
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%r9, %rsi
	movq	%r14, %rdi
	movq	%r8, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %r9
	testb	$24, %al
	jne	.L343
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L260:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L337:
	movq	%r13, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	jmp	.L252
	.cfi_endproc
.LFE20281:
	.size	_ZN2v88internal12StoreHandler21StoreThroughPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_, .-_ZN2v88internal12StoreHandler21StoreThroughPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_
	.section	.text._ZN2v88internal12StoreHandler11StoreGlobalENS0_6HandleINS0_12PropertyCellEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StoreHandler11StoreGlobalENS0_6HandleINS0_12PropertyCellEEE
	.type	_ZN2v88internal12StoreHandler11StoreGlobalENS0_6HandleINS0_12PropertyCellEEE, @function
_ZN2v88internal12StoreHandler11StoreGlobalENS0_6HandleINS0_12PropertyCellEEE:
.LFB20282:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE20282:
	.size	_ZN2v88internal12StoreHandler11StoreGlobalENS0_6HandleINS0_12PropertyCellEEE, .-_ZN2v88internal12StoreHandler11StoreGlobalENS0_6HandleINS0_12PropertyCellEEE
	.section	.text._ZN2v88internal12StoreHandler10StoreProxyEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_7JSProxyEEENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StoreHandler10StoreProxyEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_7JSProxyEEENS4_INS0_10JSReceiverEEE
	.type	_ZN2v88internal12StoreHandler10StoreProxyEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_7JSProxyEEENS4_INS0_10JSReceiverEEE, @function
_ZN2v88internal12StoreHandler10StoreProxyEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_7JSProxyEEENS4_INS0_10JSReceiverEEE:
.LFB20283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	41112(%rdi), %rdi
	movq	%rcx, %rbx
	testq	%rdi, %rdi
	je	.L346
	movabsq	$38654705664, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L347:
	cmpq	%r12, %rbx
	je	.L349
	testq	%rbx, %rbx
	je	.L350
	testq	%r12, %r12
	je	.L350
	movq	(%r12), %rax
	cmpq	%rax, (%rbx)
	je	.L349
.L350:
	xorl	%eax, %eax
	xorl	%edx, %edx
	movq	%r12, %r9
	xorl	%r8d, %r8d
	movabsq	$-4294967296, %rdi
	movq	%rax, %rsi
	pushq	%rdx
	movq	%r12, %rdx
	andq	%rdi, %rsi
	movq	%r13, %rdi
	orq	$1, %rsi
	pushq	%rsi
	movq	%r14, %rsi
	call	_ZN2v88internal12StoreHandler21StoreThroughPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_
	popq	%rdx
	popq	%rcx
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore_state
	leaq	-32(%rbp), %rsp
	movq	%rcx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	movq	41088(%r13), %rcx
	cmpq	41096(%r13), %rcx
	je	.L353
.L348:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movabsq	$38654705664, %rax
	movq	%rax, (%rcx)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L353:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L348
	.cfi_endproc
.LFE20283:
	.size	_ZN2v88internal12StoreHandler10StoreProxyEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_7JSProxyEEENS4_INS0_10JSReceiverEEE, .-_ZN2v88internal12StoreHandler10StoreProxyEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_7JSProxyEEENS4_INS0_10JSReceiverEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_, @function
_GLOBAL__sub_I__ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_:
.LFB24788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24788:
	.size	_GLOBAL__sub_I__ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_, .-_GLOBAL__sub_I__ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
