	.file	"managed.cc"
	.text
	.section	.text._ZN2v88internal12_GLOBAL__N_132ManagedObjectFinalizerSecondPassERKNS_16WeakCallbackInfoIvEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132ManagedObjectFinalizerSecondPassERKNS_16WeakCallbackInfoIvEE, @function
_ZN2v88internal12_GLOBAL__N_132ManagedObjectFinalizerSecondPassERKNS_16WeakCallbackInfoIvEE:
.LFB8652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal7Isolate30UnregisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	movq	(%r12), %r13
	movq	24(%r12), %rdi
	call	*32(%r12)
	movq	%r12, %rdi
	movl	$48, %esi
	call	_ZdlPvm@PLT
	movq	(%rbx), %r12
	movq	32(%r12), %rbx
	subq	%r13, %rbx
	movq	%rbx, %rax
	subq	48(%r12), %rax
	movq	%rbx, 32(%r12)
	cmpq	$33554432, %rax
	jg	.L10
.L2:
	testq	%r13, %r13
	jg	.L11
	je	.L1
	cmpq	40(%r12), %rbx
	jg	.L12
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	40(%r12), %rax
	subq	%r13, %rax
	cmpq	$67108864, %rax
	jle	.L1
	movq	%rax, 40(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L12:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	.cfi_endproc
.LFE8652:
	.size	_ZN2v88internal12_GLOBAL__N_132ManagedObjectFinalizerSecondPassERKNS_16WeakCallbackInfoIvEE, .-_ZN2v88internal12_GLOBAL__N_132ManagedObjectFinalizerSecondPassERKNS_16WeakCallbackInfoIvEE
	.section	.text._ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE
	.type	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE, @function
_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE:
.LFB8653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	40(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	16(%rbx), %rax
	leaq	_ZN2v88internal12_GLOBAL__N_132ManagedObjectFinalizerSecondPassERKNS_16WeakCallbackInfoIvEE(%rip), %rdx
	movq	%rdx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8653:
	.size	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE, .-_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE, @function
_GLOBAL__sub_I__ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE:
.LFB9910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE9910:
	.size	_GLOBAL__sub_I__ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE, .-_GLOBAL__sub_I__ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
