	.file	"bit-vector.cc"
	.text
	.section	.text._ZN2v88internal9BitVector8Iterator7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9BitVector8Iterator7AdvanceEv
	.type	_ZN2v88internal9BitVector8Iterator7AdvanceEv, @function
_ZN2v88internal9BitVector8Iterator7AdvanceEv:
.LFB5165:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	leal	1(%rax), %ecx
	movq	16(%rdi), %rax
	movl	%ecx, 24(%rdi)
	testq	%rax, %rax
	jne	.L6
	movslq	8(%rdi), %rax
	movq	(%rdi), %r8
	leal	1(%rax), %edx
	leaq	8(,%rax,8), %rsi
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L18:
	movq	8(%r8), %rax
	movl	%edx, %ecx
	addl	$1, %edx
	sall	$6, %ecx
	movq	(%rax,%rsi), %rax
	movl	%ecx, 24(%rdi)
	addq	$8, %rsi
	testq	%rax, %rax
	jne	.L6
.L7:
	movl	%edx, 8(%rdi)
	cmpl	4(%r8), %edx
	jl	.L18
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	leal	8(%rcx), %edx
	testb	%al, %al
	jne	.L3
	.p2align 4,,10
	.p2align 3
.L11:
	shrq	$8, %rax
	movl	%edx, %ecx
	leal	8(%rdx), %edx
	testb	%al, %al
	je	.L11
	movl	%ecx, 24(%rdi)
.L3:
	addl	$1, %ecx
	testb	$1, %al
	jne	.L8
	.p2align 4,,10
	.p2align 3
.L12:
	shrq	%rax
	movl	%ecx, %edx
	addl	$1, %ecx
	testb	$1, %al
	je	.L12
	movl	%edx, 24(%rdi)
.L8:
	shrq	%rax
	movq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE5165:
	.size	_ZN2v88internal9BitVector8Iterator7AdvanceEv, .-_ZN2v88internal9BitVector8Iterator7AdvanceEv
	.globl	__popcountdi2
	.section	.text._ZNK2v88internal9BitVector5CountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9BitVector5CountEv
	.type	_ZNK2v88internal9BitVector5CountEv, @function
_ZNK2v88internal9BitVector5CountEv:
.LFB5166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	4(%rdi), %edx
	testl	%edx, %edx
	jne	.L20
	movq	8(%rdi), %rdi
	call	__popcountdi2@PLT
	movl	%eax, %r12d
.L19:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	jle	.L23
	movq	8(%rdi), %rax
	subl	$1, %edx
	xorl	%r12d, %r12d
	leaq	8(%rax), %rbx
	leaq	(%rbx,%rdx,8), %r13
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$8, %rbx
.L22:
	movq	(%rax), %rdi
	call	__popcountdi2@PLT
	addl	%eax, %r12d
	movq	%rbx, %rax
	cmpq	%rbx, %r13
	jne	.L27
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L19
	.cfi_endproc
.LFE5166:
	.size	_ZNK2v88internal9BitVector5CountEv, .-_ZNK2v88internal9BitVector5CountEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal9BitVector8Iterator7AdvanceEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal9BitVector8Iterator7AdvanceEv, @function
_GLOBAL__sub_I__ZN2v88internal9BitVector8Iterator7AdvanceEv:
.LFB5937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5937:
	.size	_GLOBAL__sub_I__ZN2v88internal9BitVector8Iterator7AdvanceEv, .-_GLOBAL__sub_I__ZN2v88internal9BitVector8Iterator7AdvanceEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal9BitVector8Iterator7AdvanceEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
