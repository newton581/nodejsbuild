	.file	"property-descriptor.cc"
	.text
	.section	.text._ZN2v88internal12_GLOBAL__N_120GetPropertyIfPresentENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEEPNS2_INS0_6ObjectEEE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120GetPropertyIfPresentENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEEPNS2_INS0_6ObjectEEE.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_120GetPropertyIfPresentENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEEPNS2_INS0_6ObjectEEE.constprop.0:
.LFB24484:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %rdx
	movq	(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	movq	-1(%rcx), %r8
	movl	$3, %edx
	subq	$37592, %rdi
	cmpw	$64, 11(%r8)
	jne	.L2
	movl	11(%rcx), %edx
	andl	$1, %edx
	cmpb	$1, %dl
	sbbl	%edx, %edx
	andl	$3, %edx
.L2:
	movabsq	$824633720832, %rcx
	movl	%edx, -128(%rbp)
	movq	(%rsi), %rdx
	movq	%rcx, -116(%rbp)
	movq	%rdi, -104(%rbp)
	movq	-1(%rdx), %rdx
	movzwl	11(%rdx), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L18
.L3:
	leaq	-128(%rbp), %r13
	movq	%rax, -96(%rbp)
	movq	%r13, %rdi
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	movl	%eax, %r8d
	testb	%al, %al
	je	.L1
	movzbl	%ah, %ebx
	testb	%bl, %bl
	jne	.L19
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$104, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L21
	movq	%rax, (%r12)
	movl	%ebx, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L21:
	movq	$0, (%r12)
	xorl	%r8d, %r8d
	jmp	.L1
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24484:
	.size	_ZN2v88internal12_GLOBAL__N_120GetPropertyIfPresentENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEEPNS2_INS0_6ObjectEEE.constprop.0, .-_ZN2v88internal12_GLOBAL__N_120GetPropertyIfPresentENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEEPNS2_INS0_6ObjectEEE.constprop.0
	.section	.rodata._ZN2v88internal12_GLOBAL__N_118CreateDataPropertyENS0_6HandleINS0_8JSObjectEEENS2_INS0_6StringEEENS2_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"result.IsJust() && result.FromJust()"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_118CreateDataPropertyENS0_6HandleINS0_8JSObjectEEENS2_INS0_6StringEEENS2_INS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12_GLOBAL__N_118CreateDataPropertyENS0_6HandleINS0_8JSObjectEEENS2_INS0_6StringEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118CreateDataPropertyENS0_6HandleINS0_8JSObjectEEENS2_INS0_6StringEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_118CreateDataPropertyENS0_6HandleINS0_8JSObjectEEENS2_INS0_6StringEEENS2_INS0_6ObjectEEE:
.LFB19527:
	.cfi_startproc
	movabsq	$824633720832, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	(%rsi), %rax
	subq	$37592, %rdi
	movq	-1(%rax), %rdx
	movl	$0, -128(%rbp)
	movq	%rcx, -116(%rbp)
	movq	%rdi, -104(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L31
.L23:
	leaq	-128(%rbp), %r13
	movq	%rsi, -96(%rbp)
	movq	%r13, %rdi
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movabsq	$4294967297, %rdx
	call	_ZN2v88internal8JSObject18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L24
	shrw	$8, %ax
	je	.L24
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L32
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L32:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19527:
	.size	_ZN2v88internal12_GLOBAL__N_118CreateDataPropertyENS0_6HandleINS0_8JSObjectEEENS2_INS0_6StringEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal12_GLOBAL__N_118CreateDataPropertyENS0_6HandleINS0_8JSObjectEEENS2_INS0_6StringEEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE
	.type	_ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE, @function
_ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE:
.LFB19528:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movzbl	(%rdi), %eax
	movq	12464(%rsi), %rdx
	testb	$8, %al
	jne	.L186
.L34:
	movq	39(%rdx), %rax
	movq	879(%rax), %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L77
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L78:
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	8(%r13), %rdx
	movq	%rax, %r12
	testq	%rdx, %rdx
	je	.L80
	leaq	3544(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118CreateDataPropertyENS0_6HandleINS0_8JSObjectEEENS2_INS0_6StringEEENS2_INS0_6ObjectEEE
.L80:
	movzbl	0(%r13), %esi
	testb	$32, %sil
	jne	.L187
.L81:
	movq	16(%r13), %rdx
	testq	%rdx, %rdx
	je	.L82
	leaq	2608(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118CreateDataPropertyENS0_6HandleINS0_8JSObjectEEENS2_INS0_6StringEEENS2_INS0_6ObjectEEE
.L82:
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	je	.L83
	leaq	3272(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118CreateDataPropertyENS0_6HandleINS0_8JSObjectEEENS2_INS0_6StringEEENS2_INS0_6ObjectEEE
.L83:
	movzbl	0(%r13), %esi
	testb	$2, %sil
	jne	.L188
.L84:
	testb	$8, %sil
	jne	.L189
.L85:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	testb	$2, %al
	je	.L34
	cmpq	$0, 8(%rdi)
	je	.L190
	testb	$32, %al
	je	.L34
	cmpq	$0, 16(%rdi)
	jne	.L34
	cmpq	$0, 24(%rdi)
	jne	.L34
	movq	39(%rdx), %rax
	movq	311(%rax), %r12
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L56
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L57:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	(%rax), %r15
	movq	%rax, %r12
	movq	8(%r13), %rax
	movq	(%rax), %r14
	movq	-1(%r15), %rax
	movzbl	8(%rax), %eax
	salq	$3, %rax
	andl	$2040, %eax
	leaq	-1(%r15,%rax), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L93
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L60
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L60:
	testb	$24, %al
	je	.L93
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L93
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L93:
	movq	(%r12), %r15
	testb	$16, 0(%r13)
	je	.L62
	movq	112(%rbx), %r14
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L77:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L191
.L79:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L190:
	testb	$32, %al
	jne	.L34
	cmpq	$0, 16(%rdi)
	je	.L34
	cmpq	$0, 24(%rdi)
	je	.L34
	movq	39(%rdx), %rax
	movq	63(%rax), %r12
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L36
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L37:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	(%rax), %r15
	movq	%rax, %r12
	movq	16(%r13), %rax
	movq	(%rax), %r14
	movq	-1(%r15), %rax
	movzbl	8(%rax), %eax
	salq	$3, %rax
	andl	$2040, %eax
	leaq	-1(%r15,%rax), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L89
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L40
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L40:
	testb	$24, %al
	je	.L89
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L89
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	movq	24(%r13), %rax
	movq	(%r12), %r15
	movq	(%rax), %r14
.L63:
	movq	-1(%r15), %rax
	movzbl	8(%rax), %eax
	leaq	7(%r15,%rax,8), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L92
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L65
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L65:
	testb	$24, %al
	je	.L92
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L92
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L92:
	movq	(%r12), %r15
	testb	$1, 0(%r13)
	je	.L67
	movq	112(%rbx), %r14
.L68:
	movq	-1(%r15), %rax
	movzbl	8(%rax), %eax
	leaq	15(%r15,%rax,8), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L91
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L70
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L70:
	testb	$24, %al
	je	.L91
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L91
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L91:
	movq	(%r12), %r14
	testb	$4, 0(%r13)
	je	.L72
	movq	112(%rbx), %r13
.L73:
	movq	-1(%r14), %rax
	movzbl	8(%rax), %eax
	leaq	23(%r14,%rax,8), %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L85
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L75
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L75:
	testb	$24, %al
	je	.L85
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L85
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L187:
	shrb	$4, %sil
	movq	%rbx, %rdi
	andl	$1, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	leaq	3600(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118CreateDataPropertyENS0_6HandleINS0_8JSObjectEEENS2_INS0_6StringEEENS2_INS0_6ObjectEEE
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L189:
	shrb	$2, %sil
	movq	%rbx, %rdi
	andl	$1, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	leaq	2280(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118CreateDataPropertyENS0_6HandleINS0_8JSObjectEEENS2_INS0_6StringEEENS2_INS0_6ObjectEEE
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L188:
	andl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	leaq	2464(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118CreateDataPropertyENS0_6HandleINS0_8JSObjectEEENS2_INS0_6StringEEENS2_INS0_6ObjectEEE
	movzbl	0(%r13), %esi
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L72:
	movq	120(%rbx), %r13
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L67:
	movq	120(%rbx), %r14
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L62:
	movq	120(%rbx), %r14
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L56:
	movq	41088(%rsi), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L192
.L58:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L36:
	movq	41088(%rsi), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L193
.L38:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L38
	.cfi_endproc
.LFE19528:
	.size	_ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE, .-_ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal18PropertyDescriptor20ToPropertyDescriptorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPS1_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unreachable code"
	.section	.text._ZN2v88internal18PropertyDescriptor20ToPropertyDescriptorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18PropertyDescriptor20ToPropertyDescriptorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPS1_
	.type	_ZN2v88internal18PropertyDescriptor20ToPropertyDescriptorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPS1_, @function
_ZN2v88internal18PropertyDescriptor20ToPropertyDescriptorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPS1_:
.LFB19529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, %r12
	notq	%r12
	andl	$1, %r12d
	je	.L195
.L197:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$114, %esi
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L194:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L301
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	%rdx, %rbx
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L197
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L199
	movq	-1(%rax), %r15
	cmpw	$1057, 11(%r15)
	jne	.L199
	testb	$32, 13(%r15)
	jne	.L199
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	519(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L201
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L202:
	cmpq	%rsi, 23(%r15)
	jne	.L199
	movq	40936(%r13), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L199
	movq	12464(%r13), %rax
	movq	39(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L204
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L205:
	movq	887(%rsi), %rax
	movq	23(%r15), %rdx
	movq	-1(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L199
	leaq	15(%r15), %rax
	movq	%rax, -144(%rbp)
	movl	15(%r15), %eax
	testl	$2097152, %eax
	jne	.L199
	movq	41112(%r13), %rdi
	leaq	39(%r15), %rax
	movq	39(%r15), %rsi
	movq	%rax, -152(%rbp)
	testq	%rdi, %rdi
	je	.L207
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -128(%rbp)
.L208:
	movq	%r14, -120(%rbp)
	xorl	%ecx, %ecx
	movb	%r12b, -129(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
.L210:
	movq	-144(%rbp), %rax
	movl	(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%ebx, %eax
	jle	.L211
	movq	-128(%rbp), %rcx
	addq	$1, %rbx
	leaq	(%rbx,%rbx,2), %rax
	movq	(%rcx), %rcx
	salq	$3, %rax
	movq	7(%rax,%rcx), %rdx
	movq	-1(%rax,%rcx), %r14
	sarq	$32, %rdx
	movl	%edx, %esi
	andl	$1, %esi
	testb	$2, %dl
	jne	.L212
	testl	%esi, %esi
	jne	.L298
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	7(%rax,%rcx), %rcx
	movq	%rcx, %rax
	shrq	$38, %rcx
	shrq	$51, %rax
	andl	$7, %ecx
	movq	%rax, %r10
	movzbl	7(%r15), %eax
	movzbl	8(%r15), %esi
	andl	$1023, %r10d
	subl	%esi, %eax
	cmpl	%eax, %r10d
	setl	%r11b
	jl	.L302
	subl	%eax, %r10d
	movl	$16, %esi
	leal	16(,%r10,8), %edi
.L215:
	cmpl	$2, %ecx
	jne	.L303
	movl	$32768, %ecx
.L216:
	movzbl	%r11b, %r11d
	cltq
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	salq	$14, %r11
	salq	$17, %rax
	salq	$27, %rsi
	orq	%r11, %rax
	shrl	$6, %edx
	orq	%rdi, %rax
	andl	$7, %edx
	movq	-120(%rbp), %rdi
	orq	%rsi, %rax
	movl	%edx, %esi
	orq	%rax, %rcx
	movq	%rcx, %rdx
	call	_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE@PLT
.L219:
	cmpq	2464(%r13), %r14
	je	.L304
	cmpq	2280(%r13), %r14
	je	.L305
	cmpq	3544(%r13), %r14
	je	.L306
	cmpq	3600(%r13), %r14
	je	.L307
	cmpq	2608(%r13), %r14
	je	.L308
	cmpq	3272(%r13), %r14
	jne	.L210
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L309
.L298:
	movq	%r12, %rbx
	movq	-120(%rbp), %r14
	movzbl	-129(%rbp), %r12d
	.p2align 4,,10
	.p2align 3
.L199:
	leaq	2464(%r13), %rsi
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	movq	$0, -104(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_120GetPropertyIfPresentENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEEPNS2_INS0_6ObjectEEE.constprop.0
	testb	%al, %al
	je	.L194
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L235
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movzbl	(%rbx), %edx
	orl	$2, %eax
	andl	$-4, %edx
	andl	$3, %eax
	orl	%eax, %edx
	movb	%dl, (%rbx)
.L235:
	leaq	2280(%r13), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r14, %rdi
	movq	$0, -96(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_120GetPropertyIfPresentENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEEPNS2_INS0_6ObjectEEE.constprop.0
	testb	%al, %al
	je	.L194
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L237
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movzbl	(%rbx), %edx
	sall	$2, %eax
	orl	$8, %eax
	andl	$-13, %edx
	andl	$12, %eax
	orl	%edx, %eax
	movb	%al, (%rbx)
.L237:
	leaq	3544(%r13), %rsi
	leaq	-88(%rbp), %rdx
	movq	%r14, %rdi
	movq	$0, -88(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_120GetPropertyIfPresentENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEEPNS2_INS0_6ObjectEEE.constprop.0
	testb	%al, %al
	je	.L194
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L238
	movq	%rax, 8(%rbx)
.L238:
	leaq	3600(%r13), %rsi
	leaq	-80(%rbp), %rdx
	movq	%r14, %rdi
	movq	$0, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_120GetPropertyIfPresentENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEEPNS2_INS0_6ObjectEEE.constprop.0
	testb	%al, %al
	je	.L194
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L239
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movzbl	(%rbx), %edx
	sall	$4, %eax
	orl	$32, %eax
	andl	$-49, %edx
	andl	$48, %eax
	orl	%edx, %eax
	movb	%al, (%rbx)
.L239:
	leaq	2608(%r13), %rsi
	leaq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movq	$0, -72(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_120GetPropertyIfPresentENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEEPNS2_INS0_6ObjectEEE.constprop.0
	testb	%al, %al
	je	.L194
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L240
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L310
.L241:
	cmpq	%rdx, 88(%r13)
	jne	.L244
.L243:
	movq	%rax, 16(%rbx)
.L240:
	leaq	3272(%r13), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movq	$0, -64(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_120GetPropertyIfPresentENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEEPNS2_INS0_6ObjectEEE.constprop.0
	testb	%al, %al
	je	.L194
	movq	-64(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L245
	movq	(%rdx), %rcx
	testb	$1, %cl
	jne	.L311
.L246:
	cmpq	%rcx, 88(%r13)
	jne	.L249
.L248:
	movq	%rdx, 24(%rbx)
.L245:
	cmpq	$0, 16(%rbx)
	je	.L312
.L250:
	cmpq	$0, 8(%rbx)
	jne	.L251
	testb	$32, (%rbx)
	jne	.L251
.L256:
	movl	%eax, %r12d
	jmp	.L194
.L201:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L313
.L203:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L202
.L310:
	movq	-1(%rdx), %rcx
	testb	$2, 13(%rcx)
	jne	.L243
	jmp	.L241
.L212:
	testl	%esi, %esi
	jne	.L298
	movq	15(%rax,%rcx), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L220
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L219
.L303:
	cmpb	$2, %cl
	jg	.L217
	je	.L218
.L254:
	xorl	%ecx, %ecx
	jmp	.L216
.L217:
	subl	$3, %ecx
	cmpb	$1, %cl
	jbe	.L254
.L218:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L244:
	movq	-72(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$106, %esi
.L300:
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L194
.L204:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L314
.L206:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L205
.L251:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$174, %esi
	jmp	.L300
.L313:
	movq	%r13, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	jmp	.L203
.L311:
	movq	-1(%rcx), %rsi
	testb	$2, 13(%rsi)
	jne	.L248
	jmp	.L246
.L220:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L315
.L221:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L219
.L312:
	cmpq	$0, 24(%rbx)
	jne	.L250
	jmp	.L256
.L306:
	movq	%rax, 8(%r12)
	jmp	.L210
.L304:
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movl	%eax, %edx
	movzbl	(%r12), %eax
	orl	$2, %edx
	andl	$-4, %eax
	andl	$3, %edx
	orl	%edx, %eax
	movb	%al, (%r12)
	jmp	.L210
.L249:
	movq	-64(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$109, %esi
	jmp	.L300
.L302:
	movzbl	8(%r15), %esi
	movzbl	8(%r15), %edi
	addl	%r10d, %edi
	sall	$3, %esi
	sall	$3, %edi
	jmp	.L215
.L305:
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movzbl	(%r12), %edx
	sall	$2, %eax
	orl	$8, %eax
	andl	$-13, %edx
	andl	$12, %eax
	orl	%edx, %eax
	movb	%al, (%r12)
	jmp	.L210
.L308:
	movq	(%rax), %rdx
	testb	$1, %dl
	je	.L298
	movq	-1(%rdx), %rdx
	testb	$2, 13(%rdx)
	je	.L298
	movq	%rax, 16(%r12)
	jmp	.L210
.L207:
	movq	41088(%r13), %rax
	movq	%rax, -128(%rbp)
	cmpq	41096(%r13), %rax
	je	.L316
.L209:
	movq	-128(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rcx)
	jmp	.L208
.L314:
	movq	%r13, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	jmp	.L206
.L301:
	call	__stack_chk_fail@PLT
.L307:
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movzbl	(%r12), %edx
	sall	$4, %eax
	orl	$32, %eax
	andl	$-49, %edx
	andl	$48, %eax
	orl	%edx, %eax
	movb	%al, (%r12)
	jmp	.L210
.L315:
	movq	%r13, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	jmp	.L221
.L316:
	movq	%r13, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, -128(%rbp)
	jmp	.L209
.L211:
	movq	%r12, %rbx
	movq	-120(%rbp), %r14
	movzbl	-129(%rbp), %r12d
	cmpq	$0, 16(%rbx)
	je	.L317
.L232:
	cmpq	$0, 8(%rbx)
	jne	.L199
	testb	$32, (%rbx)
	jne	.L199
.L233:
	movl	$1, %r12d
	jmp	.L194
.L309:
	movq	-1(%rdx), %rdx
	testb	$2, 13(%rdx)
	je	.L298
	movq	%rax, 24(%r12)
	jmp	.L210
.L317:
	cmpq	$0, 24(%rbx)
	jne	.L232
	jmp	.L233
	.cfi_endproc
.LFE19529:
	.size	_ZN2v88internal18PropertyDescriptor20ToPropertyDescriptorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPS1_, .-_ZN2v88internal18PropertyDescriptor20ToPropertyDescriptorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPS1_
	.section	.text._ZN2v88internal18PropertyDescriptor26CompletePropertyDescriptorEPNS0_7IsolateEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18PropertyDescriptor26CompletePropertyDescriptorEPNS0_7IsolateEPS1_
	.type	_ZN2v88internal18PropertyDescriptor26CompletePropertyDescriptorEPNS0_7IsolateEPS1_, @function
_ZN2v88internal18PropertyDescriptor26CompletePropertyDescriptorEPNS0_7IsolateEPS1_:
.LFB19530:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rsi)
	movq	24(%rsi), %rax
	je	.L330
	testq	%rax, %rax
	je	.L328
.L329:
	movzbl	(%rsi), %eax
.L323:
	testb	$2, %al
	jne	.L325
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, (%rsi)
.L325:
	testb	$8, %al
	jne	.L318
	andl	$-13, %eax
	orl	$8, %eax
	movb	%al, (%rsi)
.L318:
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	addq	$88, %rdi
	movq	%rdi, 24(%rsi)
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L330:
	testq	%rax, %rax
	je	.L331
	addq	$88, %rdi
	movzbl	(%rsi), %eax
	movq	%rdi, 16(%rsi)
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L331:
	cmpq	$0, 8(%rsi)
	je	.L332
.L321:
	movzbl	(%rsi), %eax
	testb	$32, %al
	jne	.L323
	andl	$-49, %eax
	orl	$32, %eax
	movb	%al, (%rsi)
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L332:
	addq	$88, %rdi
	movq	%rdi, 8(%rsi)
	jmp	.L321
	.cfi_endproc
.LFE19530:
	.size	_ZN2v88internal18PropertyDescriptor26CompletePropertyDescriptorEPNS0_7IsolateEPS1_, .-_ZN2v88internal18PropertyDescriptor26CompletePropertyDescriptorEPNS0_7IsolateEPS1_
	.section	.text._ZN2v88internal18PropertyDescriptor26ToPropertyDescriptorObjectEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18PropertyDescriptor26ToPropertyDescriptorObjectEPNS0_7IsolateE
	.type	_ZN2v88internal18PropertyDescriptor26ToPropertyDescriptorObjectEPNS0_7IsolateE, @function
_ZN2v88internal18PropertyDescriptor26ToPropertyDescriptorObjectEPNS0_7IsolateE:
.LFB19531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$4, %esi
	movq	%rbx, %rdi
	subq	$24, %rsp
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movzbl	(%r12), %ecx
	movq	%rax, %r13
	movq	(%rax), %rax
	movl	%ecx, %edx
	movl	%ecx, %esi
	shrb	$2, %dl
	shrb	%sil
	andl	$1, %edx
	andl	$1, %esi
	addl	%esi, %esi
	sall	$2, %edx
	orl	%esi, %edx
	movl	%ecx, %esi
	andl	$1, %esi
	orl	%esi, %edx
	movl	%ecx, %esi
	shrb	$3, %sil
	andl	$1, %esi
	sall	$3, %esi
	orl	%esi, %edx
	movl	%ecx, %esi
	shrb	$5, %cl
	shrb	$4, %sil
	andl	$1, %ecx
	andl	$1, %esi
	sall	$5, %ecx
	sall	$4, %esi
	orl	%esi, %edx
	orl	%ecx, %edx
	xorl	%ecx, %ecx
	cmpq	$0, 8(%r12)
	setne	%cl
	sall	$6, %ecx
	orl	%ecx, %edx
	xorl	%ecx, %ecx
	cmpq	$0, 16(%r12)
	setne	%cl
	sall	$7, %ecx
	orl	%ecx, %edx
	xorl	%ecx, %ecx
	cmpq	$0, 24(%r12)
	setne	%cl
	sall	$8, %ecx
	orl	%ecx, %edx
	salq	$32, %rdx
	movq	%rdx, 15(%rax)
	movq	8(%r12), %rax
	movq	0(%r13), %r15
	testq	%rax, %rax
	je	.L334
	movq	(%rax), %r14
.L335:
	movq	%r14, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r14b
	je	.L351
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L371
	testb	$24, %al
	je	.L351
.L379:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L372
	.p2align 4,,10
	.p2align 3
.L351:
	movq	16(%r12), %rax
	movq	0(%r13), %r15
	testq	%rax, %rax
	je	.L339
	movq	(%rax), %r14
.L340:
	movq	%r14, 31(%r15)
	leaq	31(%r15), %rsi
	testb	$1, %r14b
	je	.L350
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L373
	testb	$24, %al
	je	.L350
.L378:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L374
	.p2align 4,,10
	.p2align 3
.L350:
	movq	24(%r12), %rax
	movq	0(%r13), %r14
	testq	%rax, %rax
	je	.L344
	movq	(%rax), %r12
.L345:
	movq	%r12, 39(%r14)
	leaq	39(%r14), %r15
	testb	$1, %r12b
	je	.L349
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L375
	testb	$24, %al
	je	.L349
.L377:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L376
.L349:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L377
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L373:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L378
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L371:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L379
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L334:
	movq	96(%rbx), %r14
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L344:
	movq	96(%rbx), %r12
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L339:
	movq	96(%rbx), %r14
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L374:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L376:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L349
	.cfi_endproc
.LFE19531:
	.size	_ZN2v88internal18PropertyDescriptor26ToPropertyDescriptorObjectEPNS0_7IsolateE, .-_ZN2v88internal18PropertyDescriptor26ToPropertyDescriptorObjectEPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE:
.LFB24429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24429:
	.size	_GLOBAL__sub_I__ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
