	.file	"bytecode-source-info.cc"
	.text
	.section	.text._ZN2v88internal11interpreterlsERSoRKNS1_18BytecodeSourceInfoE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11interpreterlsERSoRKNS1_18BytecodeSourceInfoE
	.type	_ZN2v88internal11interpreterlsERSoRKNS1_18BytecodeSourceInfoE, @function
_ZN2v88internal11interpreterlsERSoRKNS1_18BytecodeSourceInfoE:
.LFB7129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L2
	movl	4(%rsi), %esi
	cmpb	$2, %al
	movl	$83, %ebx
	movl	$69, %eax
	cmovne	%eax, %ebx
	leaq	-41(%rbp), %r13
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -41(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	%bl, -41(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$62, -41(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7129:
	.size	_ZN2v88internal11interpreterlsERSoRKNS1_18BytecodeSourceInfoE, .-_ZN2v88internal11interpreterlsERSoRKNS1_18BytecodeSourceInfoE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreterlsERSoRKNS1_18BytecodeSourceInfoE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreterlsERSoRKNS1_18BytecodeSourceInfoE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreterlsERSoRKNS1_18BytecodeSourceInfoE:
.LFB7969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE7969:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreterlsERSoRKNS1_18BytecodeSourceInfoE, .-_GLOBAL__sub_I__ZN2v88internal11interpreterlsERSoRKNS1_18BytecodeSourceInfoE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreterlsERSoRKNS1_18BytecodeSourceInfoE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
