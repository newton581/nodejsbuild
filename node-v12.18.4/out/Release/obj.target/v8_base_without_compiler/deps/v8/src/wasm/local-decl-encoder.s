	.file	"local-decl-encoder.cc"
	.text
	.section	.rodata._ZNK2v88internal4wasm16LocalDeclEncoder4EmitEPh.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal4wasm16LocalDeclEncoder4EmitEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm16LocalDeclEncoder4EmitEPh
	.type	_ZNK2v88internal4wasm16LocalDeclEncoder4EmitEPh, @function
_ZNK2v88internal4wasm16LocalDeclEncoder4EmitEPh:
.LFB5702:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	subq	16(%rdi), %rax
	movq	%rdi, %r8
	movq	%rsi, %rcx
	sarq	$3, %rax
	movl	%eax, %edx
	cmpl	$127, %eax
	jbe	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%edx, %eax
	addq	$1, %rcx
	shrl	$7, %edx
	orl	$-128, %eax
	movb	%al, -1(%rcx)
	cmpl	$127, %edx
	ja	.L3
.L2:
	movb	%dl, (%rcx)
	movq	16(%r8), %rdi
	leaq	1(%rcx), %rax
	movq	24(%r8), %r9
	cmpq	%r9, %rdi
	je	.L4
	leaq	.L9(%rip), %r8
	.p2align 4,,10
	.p2align 3
.L18:
	movl	(%rdi), %edx
	cmpl	$127, %edx
	jbe	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%edx, %ecx
	addq	$1, %rax
	shrl	$7, %edx
	orl	$-128, %ecx
	movb	%cl, -1(%rax)
	cmpl	$127, %edx
	ja	.L6
.L5:
	movb	%dl, (%rax)
	cmpb	$9, 4(%rdi)
	ja	.L7
	movzbl	4(%rdi), %edx
	movslq	(%r8,%rdx,4), %rdx
	addq	%r8, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZNK2v88internal4wasm16LocalDeclEncoder4EmitEPh,"a",@progbits
	.align 4
	.align 4
.L9:
	.long	.L17-.L9
	.long	.L16-.L9
	.long	.L15-.L9
	.long	.L20-.L9
	.long	.L13-.L9
	.long	.L12-.L9
	.long	.L11-.L9
	.long	.L10-.L9
	.long	.L7-.L9
	.long	.L8-.L9
	.section	.text._ZNK2v88internal4wasm16LocalDeclEncoder4EmitEPh
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$104, %edx
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$8, %rdi
	movb	%dl, 1(%rax)
	addq	$2, %rax
	cmpq	%rdi, %r9
	jne	.L18
.L4:
	subq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$112, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$111, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$123, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$124, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$127, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$64, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$125, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$126, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L7:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5702:
	.size	_ZNK2v88internal4wasm16LocalDeclEncoder4EmitEPh, .-_ZNK2v88internal4wasm16LocalDeclEncoder4EmitEPh
	.section	.text._ZNK2v88internal4wasm16LocalDeclEncoder7PrependEPNS0_4ZoneEPPKhS7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm16LocalDeclEncoder7PrependEPNS0_4ZoneEPPKhS7_
	.type	_ZNK2v88internal4wasm16LocalDeclEncoder7PrependEPNS0_4ZoneEPPKhS7_, @function
_ZNK2v88internal4wasm16LocalDeclEncoder7PrependEPNS0_4ZoneEPPKhS7_:
.LFB5701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rcx), %rbx
	movq	24(%r12), %rcx
	movq	16(%rdi), %rdi
	subq	(%rdx), %rbx
	movq	%rcx, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	.p2align 4,,10
	.p2align 3
.L28:
	addq	$1, %r8
	shrq	$7, %rax
	jne	.L28
	cmpq	%rcx, %rdi
	je	.L29
	.p2align 4,,10
	.p2align 3
.L31:
	movl	(%rdi), %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rax, %rsi
	addq	$1, %rax
	shrq	$7, %rdx
	jne	.L30
	addq	$8, %rdi
	leaq	2(%r8,%rsi), %r8
	cmpq	%rdi, %rcx
	jne	.L31
.L29:
	movq	16(%r9), %r15
	movq	24(%r9), %rax
	leaq	7(%rbx,%r8), %rsi
	andq	$-8, %rsi
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L42
	addq	%r15, %rsi
	movq	%rsi, 16(%r9)
.L33:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal4wasm16LocalDeclEncoder4EmitEPh
	movq	%rax, %r12
	testq	%rbx, %rbx
	jne	.L43
.L34:
	addq	%r12, %rbx
	movq	%r15, 0(%r13)
	addq	%rbx, %r15
	movq	%r15, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L43:
	.cfi_restore_state
	movq	0(%r13), %rsi
	leaq	(%r15,%rax), %rdi
	movq	%rbx, %rdx
	call	memcpy@PLT
	jmp	.L34
.L42:
	movq	%r9, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L33
	.cfi_endproc
.LFE5701:
	.size	_ZNK2v88internal4wasm16LocalDeclEncoder7PrependEPNS0_4ZoneEPPKhS7_, .-_ZNK2v88internal4wasm16LocalDeclEncoder7PrependEPNS0_4ZoneEPPKhS7_
	.section	.text._ZNK2v88internal4wasm16LocalDeclEncoder4SizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm16LocalDeclEncoder4SizeEv
	.type	_ZNK2v88internal4wasm16LocalDeclEncoder4SizeEv, @function
_ZNK2v88internal4wasm16LocalDeclEncoder4SizeEv:
.LFB5712:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rdi
	xorl	%r8d, %r8d
	movq	%rdi, %rax
	subq	%rsi, %rax
	sarq	$3, %rax
	.p2align 4,,10
	.p2align 3
.L45:
	addq	$1, %r8
	shrq	$7, %rax
	jne	.L45
	cmpq	%rdi, %rsi
	je	.L44
	.p2align 4,,10
	.p2align 3
.L48:
	movl	(%rsi), %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%rax, %rcx
	addq	$1, %rax
	shrq	$7, %rdx
	jne	.L47
	addq	$8, %rsi
	leaq	2(%rcx,%r8), %r8
	cmpq	%rsi, %rdi
	jne	.L48
.L44:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE5712:
	.size	_ZNK2v88internal4wasm16LocalDeclEncoder4SizeEv, .-_ZNK2v88internal4wasm16LocalDeclEncoder4SizeEv
	.section	.rodata._ZNSt6vectorISt4pairIjN2v88internal4wasm9ValueTypeEENS2_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt4pairIjN2v88internal4wasm9ValueTypeEENS2_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIjN2v88internal4wasm9ValueTypeEENS2_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIjN2v88internal4wasm9ValueTypeEENS2_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.type	_ZNSt6vectorISt4pairIjN2v88internal4wasm9ValueTypeEENS2_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, @function
_ZNSt6vectorISt4pairIjN2v88internal4wasm9ValueTypeEENS2_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_:
.LFB6212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r14
	movq	%r13, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L68
	movq	%rsi, %rcx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%rdx, %r15
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L62
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L69
	movl	$2147483640, %esi
	movl	$2147483640, %r8d
.L54:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L70
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L57:
	addq	%rax, %r8
	leaq	8(%rax), %r9
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L69:
	testq	%rdx, %rdx
	jne	.L71
	movl	$8, %r9d
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L55:
	movl	(%r15), %esi
	movzbl	4(%r15), %edx
	addq	%rax, %rcx
	movl	%esi, (%rcx)
	movb	%dl, 4(%rcx)
	cmpq	%r14, %rbx
	je	.L58
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L59:
	movl	(%rdx), %edi
	movzbl	4(%rdx), %esi
	addq	$8, %rdx
	addq	$8, %rcx
	movl	%edi, -8(%rcx)
	movb	%sil, -4(%rcx)
	cmpq	%rdx, %rbx
	jne	.L59
	movq	%rbx, %rdx
	subq	%r14, %rdx
	leaq	8(%rax,%rdx), %r9
.L58:
	cmpq	%r13, %rbx
	je	.L60
	movq	%rbx, %rdx
	movq	%r9, %rcx
	.p2align 4,,10
	.p2align 3
.L61:
	movl	(%rdx), %edi
	movzbl	4(%rdx), %esi
	addq	$8, %rdx
	addq	$8, %rcx
	movl	%edi, -8(%rcx)
	movb	%sil, -4(%rcx)
	cmpq	%rdx, %r13
	jne	.L61
	subq	%rbx, %r13
	addq	%r13, %r9
.L60:
	movq	%rax, %xmm0
	movq	%r9, %xmm1
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %r8d
	jmp	.L54
.L70:
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	jmp	.L57
.L71:
	cmpq	$268435455, %rdx
	movl	$268435455, %r8d
	cmovbe	%rdx, %r8
	salq	$3, %r8
	movq	%r8, %rsi
	jmp	.L54
.L68:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6212:
	.size	_ZNSt6vectorISt4pairIjN2v88internal4wasm9ValueTypeEENS2_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, .-_ZNSt6vectorISt4pairIjN2v88internal4wasm9ValueTypeEENS2_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.section	.text._ZN2v88internal4wasm16LocalDeclEncoder9AddLocalsEjNS1_9ValueTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LocalDeclEncoder9AddLocalsEjNS1_9ValueTypeE
	.type	_ZN2v88internal4wasm16LocalDeclEncoder9AddLocalsEjNS1_9ValueTypeE, @function
_ZN2v88internal4wasm16LocalDeclEncoder9AddLocalsEjNS1_9ValueTypeE:
.LFB5703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	40(%rdi), %rsi
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	movq	(%rdi), %rcx
	movl	%esi, %r12d
	testq	%rcx, %rcx
	je	.L73
	addl	8(%rcx), %r12d
.L73:
	movl	%eax, %ecx
	addq	%rsi, %rcx
	movq	24(%rdi), %rsi
	movq	%rcx, 40(%rdi)
	cmpq	%rsi, 16(%rdi)
	je	.L74
	cmpb	%dl, -4(%rsi)
	je	.L82
.L74:
	movl	%eax, -32(%rbp)
	movb	%dl, -28(%rbp)
	cmpq	%rsi, 32(%rdi)
	je	.L75
.L84:
	movl	%eax, (%rsi)
	movb	%dl, 4(%rsi)
	addq	$8, 24(%rdi)
.L72:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	addl	-8(%rsi), %eax
	subq	$8, %rsi
	movb	%dl, -28(%rbp)
	movq	%rsi, 24(%rdi)
	movl	%eax, -32(%rbp)
	cmpq	%rsi, 32(%rdi)
	jne	.L84
.L75:
	leaq	-32(%rbp), %rdx
	addq	$8, %rdi
	call	_ZNSt6vectorISt4pairIjN2v88internal4wasm9ValueTypeEENS2_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L72
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5703:
	.size	_ZN2v88internal4wasm16LocalDeclEncoder9AddLocalsEjNS1_9ValueTypeE, .-_ZN2v88internal4wasm16LocalDeclEncoder9AddLocalsEjNS1_9ValueTypeE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
