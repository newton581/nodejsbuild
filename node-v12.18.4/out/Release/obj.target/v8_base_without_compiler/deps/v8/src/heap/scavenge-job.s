	.file	"scavenge-job.cc"
	.text
	.section	.text._ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB2687:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2687:
	.size	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB3575:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE3575:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE,"axG",@progbits,_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE
	.type	_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE, @function
_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE:
.LFB3589:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3589:
	.size	_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE, .-_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE
	.section	.text._ZN2v88internal18CancelableIdleTask3RunEd,"axG",@progbits,_ZN2v88internal18CancelableIdleTask3RunEd,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18CancelableIdleTask3RunEd
	.type	_ZN2v88internal18CancelableIdleTask3RunEd, @function
_ZN2v88internal18CancelableIdleTask3RunEd:
.LFB3730:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L5
	movq	(%rdi), %rax
	jmp	*24(%rax)
.L5:
	ret
	.cfi_endproc
.LFE3730:
	.size	_ZN2v88internal18CancelableIdleTask3RunEd, .-_ZN2v88internal18CancelableIdleTask3RunEd
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB23650:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE23650:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN2v88internal8NewSpace4SizeEv,"axG",@progbits,_ZN2v88internal8NewSpace4SizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpace4SizeEv
	.type	_ZN2v88internal8NewSpace4SizeEv, @function
_ZN2v88internal8NewSpace4SizeEv:
.LFB10212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movslq	360(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	352(%rbx), %rcx
	movq	104(%rbx), %rdx
	imulq	%rax, %r12
	popq	%rbx
	subq	40(%rcx), %rdx
	leaq	(%rdx,%r12), %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10212:
	.size	_ZN2v88internal8NewSpace4SizeEv, .-_ZN2v88internal8NewSpace4SizeEv
	.section	.text._ZN2v88internal11ScavengeJob8IdleTaskD2Ev,"axG",@progbits,_ZN2v88internal11ScavengeJob8IdleTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11ScavengeJob8IdleTaskD2Ev
	.type	_ZN2v88internal11ScavengeJob8IdleTaskD2Ev, @function
_ZN2v88internal11ScavengeJob8IdleTaskD2Ev:
.LFB23548:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal18CancelableIdleTaskE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE23548:
	.size	_ZN2v88internal11ScavengeJob8IdleTaskD2Ev, .-_ZN2v88internal11ScavengeJob8IdleTaskD2Ev
	.weak	_ZN2v88internal11ScavengeJob8IdleTaskD1Ev
	.set	_ZN2v88internal11ScavengeJob8IdleTaskD1Ev,_ZN2v88internal11ScavengeJob8IdleTaskD2Ev
	.section	.text._ZN2v88internal11ScavengeJob8IdleTaskD0Ev,"axG",@progbits,_ZN2v88internal11ScavengeJob8IdleTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11ScavengeJob8IdleTaskD0Ev
	.type	_ZN2v88internal11ScavengeJob8IdleTaskD0Ev, @function
_ZN2v88internal11ScavengeJob8IdleTaskD0Ev:
.LFB23550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal18CancelableIdleTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23550:
	.size	_ZN2v88internal11ScavengeJob8IdleTaskD0Ev, .-_ZN2v88internal11ScavengeJob8IdleTaskD0Ev
	.section	.text._ZN2v88internal11ScavengeJob8IdleTaskD2Ev,"axG",@progbits,_ZN2v88internal11ScavengeJob8IdleTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal11ScavengeJob8IdleTaskD1Ev
	.type	_ZThn32_N2v88internal11ScavengeJob8IdleTaskD1Ev, @function
_ZThn32_N2v88internal11ScavengeJob8IdleTaskD1Ev:
.LFB25401:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal18CancelableIdleTaskE(%rip), %rax
	subq	$32, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE25401:
	.size	_ZThn32_N2v88internal11ScavengeJob8IdleTaskD1Ev, .-_ZThn32_N2v88internal11ScavengeJob8IdleTaskD1Ev
	.section	.text._ZN2v88internal11ScavengeJob8IdleTaskD0Ev,"axG",@progbits,_ZN2v88internal11ScavengeJob8IdleTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal11ScavengeJob8IdleTaskD0Ev
	.type	_ZThn32_N2v88internal11ScavengeJob8IdleTaskD0Ev, @function
_ZThn32_N2v88internal11ScavengeJob8IdleTaskD0Ev:
.LFB25402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal18CancelableIdleTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25402:
	.size	_ZThn32_N2v88internal11ScavengeJob8IdleTaskD0Ev, .-_ZThn32_N2v88internal11ScavengeJob8IdleTaskD0Ev
	.section	.text._ZN2v88internal18CancelableIdleTask3RunEd,"axG",@progbits,_ZN2v88internal18CancelableIdleTask3RunEd,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal18CancelableIdleTask3RunEd
	.type	_ZThn32_N2v88internal18CancelableIdleTask3RunEd, @function
_ZThn32_N2v88internal18CancelableIdleTask3RunEd:
.LFB25403:
	.cfi_startproc
	endbr64
	leaq	-32(%rdi), %r8
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L16
	movq	-32(%rdi), %rax
	movq	%r8, %rdi
	movq	24(%rax), %rax
	jmp	*%rax
.L16:
	ret
	.cfi_endproc
.LFE25403:
	.size	_ZThn32_N2v88internal18CancelableIdleTask3RunEd, .-_ZThn32_N2v88internal18CancelableIdleTask3RunEd
	.section	.text._ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE.part.0, @function
_ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE.part.0:
.LFB25399:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	112(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L40
.L18:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	subq	$37592, %r12
	movq	%r12, %rsi
	call	*%rax
	testb	%al, %al
	je	.L18
	movb	$1, (%rbx)
	movl	$56, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal18CancelableIdleTaskC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal11ScavengeJob8IdleTaskE(%rip), %rax
	movq	%r12, 40(%r13)
	addq	$32, %r13
	movq	%rax, -32(%r13)
	addq	$48, %rax
	movq	%rax, 0(%r13)
	movq	%rbx, 16(%r13)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	-64(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	movq	(%rax), %rax
	call	*48(%rax)
	movq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	movq	(%rdi), %rax
	movq	%r13, -72(%rbp)
	call	*32(%rax)
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L18
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L25
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L26:
	cmpl	$1, %eax
	jne	.L18
	movq	(%r12), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L42
.L28:
	testq	%rbx, %rbx
	je	.L29
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L30:
	cmpl	$1, %eax
	jne	.L18
	movq	(%r12), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L31
	call	*8(%rax)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L25:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L29:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L28
.L31:
	call	*%rdx
	jmp	.L18
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25399:
	.size	_ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE.part.0, .-_ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE.part.0
	.section	.rodata._ZN2v88internal11ScavengeJob8IdleTask11RunInternalEd.str1.1,"aMS",@progbits,1
.LC2:
	.string	"v8"
.LC3:
	.string	"V8.Task"
	.section	.text._ZN2v88internal11ScavengeJob8IdleTask11RunInternalEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ScavengeJob8IdleTask11RunInternalEd
	.type	_ZN2v88internal11ScavengeJob8IdleTask11RunInternalEd, @function
_ZN2v88internal11ScavengeJob8IdleTask11RunInternalEd:
.LFB20162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movsd	%xmm0, -136(%rbp)
	movq	40(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	12616(%r13), %r15d
	movl	$1, 12616(%r13)
	movq	_ZZN2v88internal11ScavengeJob8IdleTask11RunInternalEdE27trace_event_unique_atomic24(%rip), %rdx
	testq	%rdx, %rdx
	je	.L79
.L45:
	movq	$0, -88(%rbp)
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L80
.L47:
	movq	40(%rbx), %r12
	leaq	37592(%r12), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	39600(%r12), %rdi
	xorl	%esi, %esi
	movsd	%xmm0, -128(%rbp)
	call	_ZNK2v88internal8GCTracer34ScavengeSpeedInBytesPerMillisecondENS0_17ScavengeSpeedModeE@PLT
	movq	37840(%r12), %rdi
	leaq	_ZN2v88internal8NewSpace4SizeEv(%rip), %rdx
	movsd	%xmm0, -104(%rbp)
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L48
	movslq	360(%rdi), %rax
	movq	%rdi, -120(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	-120(%rbp), %rdi
	imulq	-112(%rbp), %rax
	movq	352(%rdi), %rcx
	movq	104(%rdi), %rdx
	subq	40(%rcx), %rdx
	leaq	(%rdx,%rax), %rcx
.L49:
	movq	37840(%r12), %rax
	movq	%rcx, -120(%rbp)
	movq	312(%rax), %rax
	shrq	$18, %rax
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movsd	-104(%rbp), %xmm2
	imulq	-112(%rbp), %rax
	ucomisd	.LC4(%rip), %xmm2
	movq	48(%rbx), %rdx
	movq	-120(%rbp), %rcx
	movb	$0, (%rdx)
	jp	.L70
	jne	.L70
	testq	%rax, %rax
	js	.L64
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L65:
	mulsd	.LC6(%rip), %xmm0
	comisd	.LC9(%rip), %xmm0
	ja	.L69
	movsd	.LC0(%rip), %xmm6
	movsd	%xmm6, -104(%rbp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L70:
	movsd	-104(%rbp), %xmm0
	mulsd	.LC5(%rip), %xmm0
	testq	%rax, %rax
	js	.L52
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
.L53:
	mulsd	.LC6(%rip), %xmm1
	minsd	%xmm1, %xmm0
.L54:
	subsd	.LC7(%rip), %xmm0
	movsd	.LC1(%rip), %xmm1
	maxsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
.L55:
	testq	%rcx, %rcx
	js	.L56
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
.L57:
	comisd	%xmm0, %xmm1
	jb	.L58
	movsd	-136(%rbp), %xmm0
	mulsd	.LC8(%rip), %xmm0
	subsd	-128(%rbp), %xmm0
	mulsd	-104(%rbp), %xmm0
	comisd	%xmm1, %xmm0
	jnb	.L81
	movq	48(%rbx), %rbx
	cmpb	$0, 1(%rbx)
	je	.L82
.L58:
	cmpq	$0, -88(%rbp)
	jne	.L83
.L63:
	movl	%r15d, 12616(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm1, %xmm1
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%rcx, %rdx
	andl	$1, %ecx
	pxor	%xmm1, %xmm1
	shrq	%rdx
	orq	%rcx, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L80:
	movq	40(%rbx), %rsi
	leaq	-96(%rbp), %rdi
	leaq	.LC3(%rip), %rcx
	call	_ZN2v88internal7tracing21CallStatsScopedTracer10InitializeEPNS0_7IsolateEPKhPKc@PLT
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L79:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L85
.L46:
	movq	%rdx, _ZZN2v88internal11ScavengeJob8IdleTask11RunInternalEdE27trace_event_unique_atomic24(%rip)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L69:
	movsd	.LC0(%rip), %xmm5
	movsd	.LC1(%rip), %xmm0
	movsd	%xmm5, -104(%rbp)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L82:
	cmpb	$0, (%rbx)
	jne	.L62
	cmpl	$4, 37984(%r12)
	jne	.L86
.L62:
	movb	$1, 1(%rbx)
	cmpq	$0, -88(%rbp)
	je	.L63
	.p2align 4,,10
	.p2align 3
.L83:
	movq	-80(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L63
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing21CallStatsScopedTracer16AddEndTraceEventEv@PLT
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L48:
	call	*%rax
	movq	%rax, %rcx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L81:
	xorl	%ecx, %ecx
	movl	$12, %edx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap14CollectGarbageENS0_15AllocationSpaceENS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE.part.0
	jmp	.L62
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20162:
	.size	_ZN2v88internal11ScavengeJob8IdleTask11RunInternalEd, .-_ZN2v88internal11ScavengeJob8IdleTask11RunInternalEd
	.section	.text._ZN2v88internal11ScavengeJob26ReachedIdleAllocationLimitEdmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ScavengeJob26ReachedIdleAllocationLimitEdmm
	.type	_ZN2v88internal11ScavengeJob26ReachedIdleAllocationLimitEdmm, @function
_ZN2v88internal11ScavengeJob26ReachedIdleAllocationLimitEdmm:
.LFB20163:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	js	.L88
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rsi, %xmm1
.L89:
	mulsd	.LC6(%rip), %xmm1
	ucomisd	.LC4(%rip), %xmm0
	jp	.L99
	jne	.L99
	comisd	.LC9(%rip), %xmm1
	movsd	.LC1(%rip), %xmm2
	jbe	.L92
	testq	%rdi, %rdi
	js	.L94
.L101:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdi, %xmm0
	comisd	%xmm2, %xmm0
	setnb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	mulsd	.LC5(%rip), %xmm0
	minsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
.L92:
	movapd	%xmm1, %xmm2
	subsd	.LC7(%rip), %xmm2
	movsd	.LC1(%rip), %xmm0
	maxsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm2
	testq	%rdi, %rdi
	jns	.L101
.L94:
	movq	%rdi, %rax
	andl	$1, %edi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	comisd	%xmm2, %xmm0
	setnb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm1, %xmm1
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L89
	.cfi_endproc
.LFE20163:
	.size	_ZN2v88internal11ScavengeJob26ReachedIdleAllocationLimitEdmm, .-_ZN2v88internal11ScavengeJob26ReachedIdleAllocationLimitEdmm
	.section	.text._ZN2v88internal11ScavengeJob25EnoughIdleTimeForScavengeEddm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ScavengeJob25EnoughIdleTimeForScavengeEddm
	.type	_ZN2v88internal11ScavengeJob25EnoughIdleTimeForScavengeEddm, @function
_ZN2v88internal11ScavengeJob25EnoughIdleTimeForScavengeEddm:
.LFB20164:
	.cfi_startproc
	endbr64
	ucomisd	.LC4(%rip), %xmm1
	jp	.L103
	jne	.L103
	movsd	.LC0(%rip), %xmm1
.L103:
	testq	%rdi, %rdi
	js	.L105
	mulsd	%xmm0, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdi, %xmm2
	comisd	%xmm2, %xmm1
	setnb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	mulsd	%xmm0, %xmm1
	movq	%rdi, %rax
	pxor	%xmm2, %xmm2
	andl	$1, %edi
	shrq	%rax
	orq	%rdi, %rax
	cvtsi2sdq	%rax, %xmm2
	addsd	%xmm2, %xmm2
	comisd	%xmm2, %xmm1
	setnb	%al
	ret
	.cfi_endproc
.LFE20164:
	.size	_ZN2v88internal11ScavengeJob25EnoughIdleTimeForScavengeEddm, .-_ZN2v88internal11ScavengeJob25EnoughIdleTimeForScavengeEddm
	.section	.text._ZN2v88internal11ScavengeJob18RescheduleIdleTaskEPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ScavengeJob18RescheduleIdleTaskEPNS0_4HeapE
	.type	_ZN2v88internal11ScavengeJob18RescheduleIdleTaskEPNS0_4HeapE, @function
_ZN2v88internal11ScavengeJob18RescheduleIdleTaskEPNS0_4HeapE:
.LFB20165:
	.cfi_startproc
	endbr64
	cmpb	$0, 1(%rdi)
	jne	.L112
	cmpb	$0, (%rdi)
	jne	.L113
	cmpl	$4, 392(%rsi)
	jne	.L116
.L113:
	movb	$1, 1(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	_ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE.part.0
	movq	-8(%rbp), %rdi
	movb	$1, 1(%rdi)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20165:
	.size	_ZN2v88internal11ScavengeJob18RescheduleIdleTaskEPNS0_4HeapE, .-_ZN2v88internal11ScavengeJob18RescheduleIdleTaskEPNS0_4HeapE
	.section	.text._ZN2v88internal11ScavengeJob24ScheduleIdleTaskIfNeededEPNS0_4HeapEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ScavengeJob24ScheduleIdleTaskIfNeededEPNS0_4HeapEi
	.type	_ZN2v88internal11ScavengeJob24ScheduleIdleTaskIfNeededEPNS0_4HeapEi, @function
_ZN2v88internal11ScavengeJob24ScheduleIdleTaskIfNeededEPNS0_4HeapEi:
.LFB20166:
	.cfi_startproc
	endbr64
	addl	4(%rdi), %edx
	movl	%edx, 4(%rdi)
	cmpl	$1048575, %edx
	jle	.L121
	cmpb	$0, (%rdi)
	jne	.L122
	cmpl	$4, 392(%rsi)
	jne	.L125
.L122:
	movl	$0, 4(%rdi)
	movb	$0, 1(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	_ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE.part.0
	movq	-8(%rbp), %rdi
	movl	$0, 4(%rdi)
	movb	$0, 1(%rdi)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20166:
	.size	_ZN2v88internal11ScavengeJob24ScheduleIdleTaskIfNeededEPNS0_4HeapEi, .-_ZN2v88internal11ScavengeJob24ScheduleIdleTaskIfNeededEPNS0_4HeapEi
	.section	.text._ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE
	.type	_ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE, @function
_ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE:
.LFB20167:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	jne	.L126
	cmpl	$4, 392(%rsi)
	jne	.L128
.L126:
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	jmp	_ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE.part.0
	.cfi_endproc
.LFE20167:
	.size	_ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE, .-_ZN2v88internal11ScavengeJob16ScheduleIdleTaskEPNS0_4HeapE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11ScavengeJob39kMaxAllocationLimitAsFractionOfNewSpaceE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11ScavengeJob39kMaxAllocationLimitAsFractionOfNewSpaceE, @function
_GLOBAL__sub_I__ZN2v88internal11ScavengeJob39kMaxAllocationLimitAsFractionOfNewSpaceE:
.LFB25384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25384:
	.size	_GLOBAL__sub_I__ZN2v88internal11ScavengeJob39kMaxAllocationLimitAsFractionOfNewSpaceE, .-_GLOBAL__sub_I__ZN2v88internal11ScavengeJob39kMaxAllocationLimitAsFractionOfNewSpaceE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11ScavengeJob39kMaxAllocationLimitAsFractionOfNewSpaceE
	.weak	_ZTVN2v88internal18CancelableIdleTaskE
	.section	.data.rel.ro._ZTVN2v88internal18CancelableIdleTaskE,"awG",@progbits,_ZTVN2v88internal18CancelableIdleTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal18CancelableIdleTaskE, @object
	.size	_ZTVN2v88internal18CancelableIdleTaskE, 88
_ZTVN2v88internal18CancelableIdleTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal18CancelableIdleTask3RunEd
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal18CancelableIdleTask3RunEd
	.weak	_ZTVN2v88internal11ScavengeJob8IdleTaskE
	.section	.data.rel.ro.local._ZTVN2v88internal11ScavengeJob8IdleTaskE,"awG",@progbits,_ZTVN2v88internal11ScavengeJob8IdleTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal11ScavengeJob8IdleTaskE, @object
	.size	_ZTVN2v88internal11ScavengeJob8IdleTaskE, 88
_ZTVN2v88internal11ScavengeJob8IdleTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11ScavengeJob8IdleTaskD1Ev
	.quad	_ZN2v88internal11ScavengeJob8IdleTaskD0Ev
	.quad	_ZN2v88internal18CancelableIdleTask3RunEd
	.quad	_ZN2v88internal11ScavengeJob8IdleTask11RunInternalEd
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal11ScavengeJob8IdleTaskD1Ev
	.quad	_ZThn32_N2v88internal11ScavengeJob8IdleTaskD0Ev
	.quad	_ZThn32_N2v88internal18CancelableIdleTask3RunEd
	.section	.bss._ZZN2v88internal11ScavengeJob8IdleTask11RunInternalEdE27trace_event_unique_atomic24,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal11ScavengeJob8IdleTask11RunInternalEdE27trace_event_unique_atomic24, @object
	.size	_ZZN2v88internal11ScavengeJob8IdleTask11RunInternalEdE27trace_event_unique_atomic24, 8
_ZZN2v88internal11ScavengeJob8IdleTask11RunInternalEdE27trace_event_unique_atomic24:
	.zero	8
	.globl	_ZN2v88internal11ScavengeJob39kMaxAllocationLimitAsFractionOfNewSpaceE
	.section	.rodata._ZN2v88internal11ScavengeJob39kMaxAllocationLimitAsFractionOfNewSpaceE,"a"
	.align 8
	.type	_ZN2v88internal11ScavengeJob39kMaxAllocationLimitAsFractionOfNewSpaceE, @object
	.size	_ZN2v88internal11ScavengeJob39kMaxAllocationLimitAsFractionOfNewSpaceE, 8
_ZN2v88internal11ScavengeJob39kMaxAllocationLimitAsFractionOfNewSpaceE:
	.long	2576980378
	.long	1072273817
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1091567616
	.align 8
.LC1:
	.long	0
	.long	1092616192
	.align 8
.LC4:
	.long	0
	.long	0
	.align 8
.LC5:
	.long	0
	.long	1075052544
	.align 8
.LC6:
	.long	2576980378
	.long	1072273817
	.align 8
.LC7:
	.long	0
	.long	1093664768
	.align 8
.LC8:
	.long	0
	.long	1083129856
	.align 8
.LC9:
	.long	0
	.long	1093926912
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
