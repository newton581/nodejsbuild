	.file	"handler-shared.cc"
	.text
	.section	.text.unlikely._ZN2v88internal12trap_handler12MetadataLockC2Ev,"ax",@progbits
	.align 2
.LCOLDB0:
	.section	.text._ZN2v88internal12trap_handler12MetadataLockC2Ev,"ax",@progbits
.LHOTB0:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12trap_handler12MetadataLockC2Ev
	.type	_ZN2v88internal12trap_handler12MetadataLockC2Ev, @function
_ZN2v88internal12trap_handler12MetadataLockC2Ev:
.LFB3448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%fs:_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@tpoff, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L5
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%edx, %eax
	xchgb	_ZN2v88internal12trap_handler12MetadataLock9spinlock_E(%rip), %al
	testb	%al, %al
	jne	.L2
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal12trap_handler12MetadataLockC2Ev
	.cfi_startproc
	.type	_ZN2v88internal12trap_handler12MetadataLockC2Ev.cold, @function
_ZN2v88internal12trap_handler12MetadataLockC2Ev.cold:
.LFSB3448:
.L5:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3448:
	.section	.text._ZN2v88internal12trap_handler12MetadataLockC2Ev
	.size	_ZN2v88internal12trap_handler12MetadataLockC2Ev, .-_ZN2v88internal12trap_handler12MetadataLockC2Ev
	.section	.text.unlikely._ZN2v88internal12trap_handler12MetadataLockC2Ev
	.size	_ZN2v88internal12trap_handler12MetadataLockC2Ev.cold, .-_ZN2v88internal12trap_handler12MetadataLockC2Ev.cold
.LCOLDE0:
	.section	.text._ZN2v88internal12trap_handler12MetadataLockC2Ev
.LHOTE0:
	.globl	_ZN2v88internal12trap_handler12MetadataLockC1Ev
	.set	_ZN2v88internal12trap_handler12MetadataLockC1Ev,_ZN2v88internal12trap_handler12MetadataLockC2Ev
	.section	.text.unlikely._ZN2v88internal12trap_handler12MetadataLockD2Ev,"ax",@progbits
	.align 2
.LCOLDB1:
	.section	.text._ZN2v88internal12trap_handler12MetadataLockD2Ev,"ax",@progbits
.LHOTB1:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12trap_handler12MetadataLockD2Ev
	.type	_ZN2v88internal12trap_handler12MetadataLockD2Ev, @function
_ZN2v88internal12trap_handler12MetadataLockD2Ev:
.LFB3451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%fs:_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@tpoff, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L10
	movb	$0, _ZN2v88internal12trap_handler12MetadataLock9spinlock_E(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal12trap_handler12MetadataLockD2Ev
	.cfi_startproc
	.type	_ZN2v88internal12trap_handler12MetadataLockD2Ev.cold, @function
_ZN2v88internal12trap_handler12MetadataLockD2Ev.cold:
.LFSB3451:
.L10:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3451:
	.section	.text._ZN2v88internal12trap_handler12MetadataLockD2Ev
	.size	_ZN2v88internal12trap_handler12MetadataLockD2Ev, .-_ZN2v88internal12trap_handler12MetadataLockD2Ev
	.section	.text.unlikely._ZN2v88internal12trap_handler12MetadataLockD2Ev
	.size	_ZN2v88internal12trap_handler12MetadataLockD2Ev.cold, .-_ZN2v88internal12trap_handler12MetadataLockD2Ev.cold
.LCOLDE1:
	.section	.text._ZN2v88internal12trap_handler12MetadataLockD2Ev
.LHOTE1:
	.globl	_ZN2v88internal12trap_handler12MetadataLockD1Ev
	.set	_ZN2v88internal12trap_handler12MetadataLockD1Ev,_ZN2v88internal12trap_handler12MetadataLockD2Ev
	.globl	_ZN2v88internal12trap_handler12MetadataLock9spinlock_E
	.section	.bss._ZN2v88internal12trap_handler12MetadataLock9spinlock_E,"aw",@nobits
	.type	_ZN2v88internal12trap_handler12MetadataLock9spinlock_E, @object
	.size	_ZN2v88internal12trap_handler12MetadataLock9spinlock_E, 1
_ZN2v88internal12trap_handler12MetadataLock9spinlock_E:
	.zero	1
	.globl	_ZN2v88internal12trap_handler19gRecoveredTrapCountE
	.section	.bss._ZN2v88internal12trap_handler19gRecoveredTrapCountE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal12trap_handler19gRecoveredTrapCountE, @object
	.size	_ZN2v88internal12trap_handler19gRecoveredTrapCountE, 8
_ZN2v88internal12trap_handler19gRecoveredTrapCountE:
	.zero	8
	.globl	_ZN2v88internal12trap_handler12gCodeObjectsE
	.section	.bss._ZN2v88internal12trap_handler12gCodeObjectsE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal12trap_handler12gCodeObjectsE, @object
	.size	_ZN2v88internal12trap_handler12gCodeObjectsE, 8
_ZN2v88internal12trap_handler12gCodeObjectsE:
	.zero	8
	.globl	_ZN2v88internal12trap_handler15gNumCodeObjectsE
	.section	.bss._ZN2v88internal12trap_handler15gNumCodeObjectsE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal12trap_handler15gNumCodeObjectsE, @object
	.size	_ZN2v88internal12trap_handler15gNumCodeObjectsE, 8
_ZN2v88internal12trap_handler15gNumCodeObjectsE:
	.zero	8
	.globl	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE
	.section	.tbss._ZN2v88internal12trap_handler21g_thread_in_wasm_codeE,"awT",@nobits
	.align 4
	.type	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE, @object
	.size	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE, 4
_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE:
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
