	.file	"bootstrapper.cc"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"getColumnNumber"
.LC1:
	.string	"getEvalOrigin"
.LC2:
	.string	"getFileName"
.LC3:
	.string	"getFunction"
.LC4:
	.string	"getFunctionName"
.LC5:
	.string	"getLineNumber"
.LC6:
	.string	"getMethodName"
.LC7:
	.string	"getPosition"
.LC8:
	.string	"getPromiseIndex"
.LC9:
	.string	"getScriptNameOrSourceURL"
.LC10:
	.string	"getThis"
.LC11:
	.string	"getTypeName"
.LC12:
	.string	"isAsync"
.LC13:
	.string	"isConstructor"
.LC14:
	.string	"isEval"
.LC15:
	.string	"isNative"
.LC16:
	.string	"isPromiseAll"
.LC17:
	.string	"isToplevel"
.LC18:
	.string	"toString"
	.text
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB7135:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7135:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,"axG",@progbits,_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.type	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, @function
_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE:
.LFB7136:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7136:
	.size	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, .-_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.text._ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE, @function
_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE:
.LFB21265:
	.cfi_startproc
	movq	%rdx, %rcx
	movl	$3, %r8d
	leaq	3952(%rdi), %rdx
	jmp	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	.cfi_endproc
.LFE21265:
	.size	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE, .-_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	.section	.rodata._ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv.part.0.str1.1,"aMS",@progbits,1
.LC20:
	.string	"Bootstrapping"
	.section	.text._ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv.part.0, @function
_ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv.part.0:
.LFB28254:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movl	$167, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r12, %rdi
	leaq	128(%rax), %rsi
	call	_ZN2v88internal15NewFunctionArgs26ForBuiltinWithoutPrototypeENS0_6HandleINS0_6StringEEEiNS0_12LanguageModeE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	movl	$-1, %edx
	movl	$7, %ecx
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%r12, %rdi
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	(%rbx), %rsi
	leaq	128(%rsi), %rdx
	addq	$2872, %rsi
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r12), %rax
	movq	(%rbx), %r13
	movq	23(%rax), %rax
	movq	41112(%r13), %rdi
	movzwl	39(%rax), %esi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L6
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L7:
	movq	(%rbx), %rax
	movl	$7, %ecx
	movq	%r12, %rdi
	leaq	2768(%rax), %rsi
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE@PLT
	xorl	%esi, %esi
	leaq	.LC20(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc@PLT
	movq	%r12, 48(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L12
.L8:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%r13, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L8
.L11:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28254:
	.size	_ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv.part.0, .-_ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv.part.0
	.section	.text._ZN2v88internal12_GLOBAL__N_137SimpleCreateBuiltinSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEi.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_137SimpleCreateBuiltinSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEi.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_137SimpleCreateBuiltinSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEi.constprop.0:
.LFB28360:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$498, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Factory31NewSharedFunctionInfoForBuiltinENS0_11MaybeHandleINS0_6StringEEEiNS0_12FunctionKindE@PLT
	movl	$2, %ecx
	movl	$2, %esi
	movq	(%rax), %rdx
	movw	%cx, 41(%rdx)
	movq	(%rax), %rdx
	movw	%si, 39(%rdx)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28360:
	.size	_ZN2v88internal12_GLOBAL__N_137SimpleCreateBuiltinSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEi.constprop.0, .-_ZN2v88internal12_GLOBAL__N_137SimpleCreateBuiltinSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEi.constprop.0
	.section	.text._ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0:
.LFB28352:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movzbl	%cl, %ecx
	movq	%rdx, %rsi
	movl	%r8d, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Factory31NewSharedFunctionInfoForBuiltinENS0_11MaybeHandleINS0_6StringEEEiNS0_12FunctionKindE@PLT
	movl	$1, %ecx
	movl	$1, %esi
	movq	(%rax), %rdx
	movw	%cx, 41(%rdx)
	movq	(%rax), %rdx
	movw	%si, 39(%rdx)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28352:
	.size	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0, .-_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	.section	.text._ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE, @function
_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE:
.LFB21248:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r9d
	movq	%rdx, %rsi
	movl	%r9d, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%ecx, %ebx
	movzbl	%r8b, %ecx
	subq	$8, %rsp
	call	_ZN2v88internal7Factory31NewSharedFunctionInfoForBuiltinENS0_11MaybeHandleINS0_6StringEEEiNS0_12FunctionKindE@PLT
	movq	(%rax), %rdx
	movw	%bx, 41(%rdx)
	movq	(%rax), %rdx
	movw	%bx, 39(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21248:
	.size	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE, .-_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE
	.section	.rodata._ZN2v88internalL21PropertyAlreadyExistsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_4NameEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"LookupIterator::ACCESS_CHECK != it.state()"
	.section	.rodata._ZN2v88internalL21PropertyAlreadyExistsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_4NameEEE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL21PropertyAlreadyExistsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_4NameEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL21PropertyAlreadyExistsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_4NameEEE, @function
_ZN2v88internalL21PropertyAlreadyExistsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_4NameEEE:
.LFB21343:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L20
.L22:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r14
.L21:
	movabsq	$824633720832, %rcx
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	movq	%rcx, -132(%rbp)
	movl	$0, -144(%rbp)
	movq	%r13, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L27
.L23:
	leaq	-144(%rbp), %rdi
	movq	%r15, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r14, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movl	-140(%rbp), %eax
	testl	%eax, %eax
	je	.L28
	cmpl	$4, %eax
	setne	%al
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L29
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L22
	movq	%rsi, %r14
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r15
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	.LC21(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L29:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21343:
	.size	_ZN2v88internalL21PropertyAlreadyExistsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_4NameEEE, .-_ZN2v88internalL21PropertyAlreadyExistsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_4NameEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib, @function
_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib:
.LFB21254:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -48
	movl	%ecx, %ebx
	movl	$1, %ecx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal15NewFunctionArgs26ForBuiltinWithoutPrototypeENS0_6HandleINS0_6StringEEEiNS0_12LanguageModeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v88internal8JSObject18MakePrototypesFastENS0_6HandleINS0_6ObjectEEENS0_12WhereToStartEPNS0_7IsolateE@PLT
	movq	(%r12), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %eax
	orl	$32, %eax
	movl	%eax, 47(%rdx)
	movq	(%r12), %rax
	movq	23(%rax), %rax
	testb	%r14b, %r14b
	je	.L31
	movw	%bx, 41(%rax)
.L32:
	movq	(%r12), %rax
	movq	23(%rax), %rax
	movw	%bx, 39(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L35
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movl	$-1, %edx
	movw	%dx, 41(%rax)
	jmp	.L32
.L35:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21254:
	.size	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib, .-_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	.section	.rodata._ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_.str1.1,"aMS",@progbits,1
.LC23:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_, @function
_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_:
.LFB21258:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	leaq	2608(%rdi), %rdx
	pushq	%r12
	movq	%r13, %rsi
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal4Name14ToFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L38
	movl	%ebx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movq	%r13, %rsi
	leaq	3272(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal4Name14ToFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L38
	movl	$1, %r8d
	movl	$1, %ecx
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	je	.L38
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21258:
	.size	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_, .-_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	.section	.text._ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEES8_NS0_8Builtins4NameEb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEES8_NS0_8Builtins4NameEb, @function
_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEES8_NS0_8Builtins4NameEb:
.LFB21260:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	leaq	2608(%rdi), %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal4Name14ToFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L48
	movzbl	%r14b, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	movl	%r15d, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	leaq	88(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$2, %r8d
	movq	%rax, %rdx
	movq	%rax, %r14
	call	_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	je	.L48
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21260:
	.size	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEES8_NS0_8Builtins4NameEb, .-_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEES8_NS0_8Builtins4NameEb
	.section	.text._ZN2v88internal12_GLOBAL__N_120InstallSpeciesGetterEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120InstallSpeciesGetterEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal12_GLOBAL__N_120InstallSpeciesGetterEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE:
.LFB21264:
	.cfi_startproc
	leaq	3904(%rdi), %rcx
	leaq	3368(%rdi), %rdx
	movl	$1, %r9d
	movl	$169, %r8d
	jmp	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEES8_NS0_8Builtins4NameEb
	.cfi_endproc
.LFE21264:
	.size	_ZN2v88internal12_GLOBAL__N_120InstallSpeciesGetterEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal12_GLOBAL__N_120InstallSpeciesGetterEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0:
.LFB28356:
	.cfi_startproc
	movl	%ecx, %r8d
	xorl	%r9d, %r9d
	movq	%rdx, %rcx
	jmp	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEES8_NS0_8Builtins4NameEb
	.cfi_endproc
.LFE28356:
	.size	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0, .-_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	.section	.text._ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1, @function
_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1:
.LFB28355:
	.cfi_startproc
	movl	%ecx, %r8d
	movl	$1, %r9d
	movq	%rdx, %rcx
	jmp	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEES8_NS0_8Builtins4NameEb
	.cfi_endproc
.LFE28355:
	.size	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1, .-_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"static_cast<unsigned>(value) < 256"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc.str1.1,"aMS",@progbits,1
.LC25:
	.string	"IsJSObjectMap()"
.LC26:
	.string	"0 == value"
.LC27:
	.string	"0 <= value"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc.str1.8
	.align 8
.LC28:
	.string	"static_cast<unsigned>(value) <= 255"
	.section	.text._ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc, @function
_ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc:
.LFB21275:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	movq	%rcx, %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movzbl	13(%rax), %edx
	testb	%dl, %dl
	jns	.L71
.L57:
	andb	$-65, 13(%rax)
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movzbl	9(%rax), %edx
	cmpl	$2, %edx
	jg	.L72
.L58:
	movzbl	7(%rax), %ecx
	leal	8(,%rcx,8), %esi
	movl	%esi, %ecx
	sarl	$3, %ecx
	cmpl	$2048, %esi
	je	.L61
	movb	%cl, 7(%rax)
	movq	(%r12), %rcx
	movzbl	8(%rcx), %eax
	addl	$1, %eax
	cmpw	$1024, 11(%rcx)
	jbe	.L73
	cmpl	$256, %eax
	je	.L61
	movb	%al, 8(%rcx)
	movq	(%r12), %rcx
	movzbl	13(%rcx), %eax
	orl	$-128, %eax
	movb	%al, 13(%rcx)
	movq	(%r12), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L74
	testl	%edx, %edx
	js	.L75
	movzbl	7(%rcx), %eax
	movzbl	8(%rcx), %edi
	movzbl	8(%rcx), %esi
	subl	%edi, %eax
	subl	%edx, %eax
	movzbl	%sil, %edx
	addl	%edx, %eax
	cmpl	$255, %eax
	ja	.L76
	movb	%al, 9(%rcx)
	movq	(%r12), %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L74:
	testl	%edx, %edx
	jne	.L77
	movb	$0, 9(%rcx)
	movq	(%r12), %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L72:
	movzbl	7(%rax), %ecx
	subl	%edx, %ecx
	movl	%ecx, %edx
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	.LC24(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	.LC25(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	.LC26(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	.LC27(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	.LC28(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21275:
	.size	_ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc, .-_ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc
	.section	.text._ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE:
.LFB21262:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$7, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21262:
	.size	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE, .-_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc, @function
_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc:
.LFB21263:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	112(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L85:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21263:
	.size	_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc, .-_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc
	.section	.text._ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1, @function
_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1:
.LFB28350:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movzbl	%r14b, %r8d
	movl	$3, %ecx
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%rax, %r14
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28350:
	.size	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1, .-_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	.section	.text._ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2, @function
_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2:
.LFB28349:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movzbl	%r14b, %r8d
	movl	$2, %ecx
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%rax, %r14
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L93:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28349:
	.size	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2, .-_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	.section	.text._ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3, @function
_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3:
.LFB28348:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$56, %rsp
	movl	%r9d, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	%r15d, %edx
	movzbl	%bl, %r8d
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movl	-84(%rbp), %r9d
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	%rax, %r15
	movl	%r9d, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L97:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28348:
	.size	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3, .-_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	.section	.text._ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE, @function
_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE:
.LFB21256:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$56, %rsp
	movl	%r8d, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	-84(%rbp), %ecx
	movl	%r15d, %edx
	movzbl	%bl, %r8d
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movl	16(%rbp), %r8d
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L101:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21256:
	.size	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE, .-_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	.section	.text._ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE, @function
_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE:
.LFB21257:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$56, %rsp
	movl	%r9d, -84(%rbp)
	movl	16(%rbp), %r13d
	movl	%r8d, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	-84(%rbp), %ecx
	movl	-88(%rbp), %edx
	movzbl	%r13b, %r8d
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movl	24(%rbp), %r8d
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21257:
	.size	_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE, .-_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	.section	.text._ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0:
.LFB28351:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$56, %rsp
	movl	%r9d, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	xorl	%ecx, %ecx
	movl	%r15d, %edx
	movzbl	%bl, %r8d
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movl	-84(%rbp), %r9d
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	%rax, %r15
	movl	%r9d, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L109:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28351:
	.size	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0, .-_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	.section	.text._ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0:
.LFB28358:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movzbl	%r14b, %r8d
	movl	$1, %ecx
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%rax, %r14
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L113:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28358:
	.size	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0, .-_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	.section	.text._ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1, @function
_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1:
.LFB28357:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	%r14d, %edx
	movl	$1, %r8d
	movl	%ebx, %ecx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%rax, %r14
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L117:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28357:
	.size	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1, .-_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	.section	.rodata._ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv.part.0.str1.1,"aMS",@progbits,1
.LC29:
	.string	"SharedArrayBuffer"
.LC30:
	.string	"Atomics"
	.section	.text._ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv.part.0, @function
_ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv.part.0:
.LFB28210:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L119
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L120:
	movq	(%rbx), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1183(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L122
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L123:
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	movl	$2, %r8d
	leaq	.LC29(%rip), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	223(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L125
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L126:
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	movl	$2, %r8d
	leaq	.LC30(%rip), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	223(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L128
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L129:
	movq	(%rbx), %r14
	leaq	.LC30(%rip), %rax
	movq	%r13, %rsi
	movq	$7, -56(%rbp)
	movq	%rax, -64(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L133
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L134
.L121:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L128:
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L135
.L130:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L125:
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L136
.L127:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L122:
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L137
.L124:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L134:
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L136:
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L130
.L133:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28210:
	.size	_ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv.part.0, .-_ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv.part.0
	.section	.text._ZNK2v88internal10JSFunction9prototypeEPNS0_7IsolateE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal10JSFunction9prototypeEPNS0_7IsolateE.constprop.0, @function
_ZNK2v88internal10JSFunction9prototypeEPNS0_7IsolateE.constprop.0:
.LFB28344:
	.cfi_startproc
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	movzbl	13(%rdx), %edx
	andl	$1, %edx
	je	.L139
	movq	-1(%rax), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L141
.L142:
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	movq	55(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L142
	movq	23(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L142
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L141
	ret
	.cfi_endproc
.LFE28344:
	.size	_ZNK2v88internal10JSFunction9prototypeEPNS0_7IsolateE.constprop.0, .-_ZNK2v88internal10JSFunction9prototypeEPNS0_7IsolateE.constprop.0
	.section	.rodata._ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv.part.0.str1.1,"aMS",@progbits,1
.LC31:
	.string	"allSettled"
	.section	.text._ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv.part.0, @function
_ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv.part.0:
.LFB28255:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1711(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L148
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L149:
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movl	$1, %r8d
	movl	$520, %ecx
	leaq	.LC31(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$521, %esi
	xorl	%ecx, %ecx
	leaq	128(%rdi), %r14
	movq	%r14, %rdx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rax), %r12
	movq	(%rdx), %r13
	movq	%r12, 1015(%r13)
	leaq	1015(%r13), %rsi
	testb	$1, %r12b
	je	.L158
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L172
	testb	$24, %al
	je	.L158
.L177:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L173
	.p2align 4,,10
	.p2align 3
.L158:
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movl	$522, %esi
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rax), %r12
	movq	(%rdx), %r13
	movq	%r12, 1023(%r13)
	leaq	1023(%r13), %r14
	testb	$1, %r12b
	je	.L147
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L174
	testb	$24, %al
	je	.L147
.L176:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L175
.L147:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L176
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L172:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L177
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L148:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L178
.L150:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L173:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L175:
	addq	$24, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L150
	.cfi_endproc
.LFE28255:
	.size	_ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv.part.0, .-_ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv.part.0
	.section	.text._ZN2v88internal12_GLOBAL__N_116InstallMakeErrorEPNS0_7IsolateEii,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_116InstallMakeErrorEPNS0_7IsolateEii, @function
_ZN2v88internal12_GLOBAL__N_116InstallMakeErrorEPNS0_7IsolateEii:
.LFB21290:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movl	$1057, %ecx
	leaq	128(%rdi), %rsi
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	leaq	96(%rdi), %rdx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	pushq	$0
	pushq	%r8
	movl	$24, %r8d
	call	_ZN2v88internal15NewFunctionArgs23ForBuiltinWithPrototypeENS0_6HandleINS0_6StringEEENS2_INS0_10HeapObjectEEENS0_12InstanceTypeEiiiNS0_11MutableModeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	movl	$-1, %edx
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	12464(%rbx), %rax
	movq	39(%rax), %r13
	movq	41112(%rbx), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L180
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L181:
	leal	16(,%r14,8), %eax
	movq	(%r12), %r12
	cltq
	leaq	-1(%r13,%rax), %r14
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L179
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L195
	testb	$24, %al
	je	.L179
.L198:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L196
	.p2align 4,,10
	.p2align 3
.L179:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L198
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L180:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L199
.L182:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L182
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21290:
	.size	_ZN2v88internal12_GLOBAL__N_116InstallMakeErrorEPNS0_7IsolateEii, .-_ZN2v88internal12_GLOBAL__N_116InstallMakeErrorEPNS0_7IsolateEii
	.section	.text._ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi, @function
_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi:
.LFB21288:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rsi
	salq	$32, %rsi
	subq	$16, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L201
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L202:
	movq	%rbx, %rdi
	leaq	3744(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	12464(%rbx), %rax
	movq	39(%rax), %r14
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L204
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
.L205:
	leal	16(,%r12,8), %eax
	movq	0(%r13), %r13
	cltq
	leaq	-1(%r14,%rax), %r12
	movq	%r13, (%r12)
	testb	$1, %r13b
	je	.L200
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L218
	testb	$24, %al
	je	.L200
.L220:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L219
.L200:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L220
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L204:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L221
.L206:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r14, (%rax)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L201:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L222
.L203:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L219:
	addq	$16, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L206
	.cfi_endproc
.LFE21288:
	.size	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi, .-_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	.section	.text._ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE, @function
_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE:
.LFB21250:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r10d
	movl	%ecx, %r11d
	movq	%r9, %rdx
	movzwl	%r10w, %ecx
	movl	%r8d, %r9d
	movl	%r11d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	16(%rbp), %eax
	pushq	$1
	pushq	%rax
	call	_ZN2v88internal15NewFunctionArgs23ForBuiltinWithPrototypeENS0_6HandleINS0_6StringEEENS2_INS0_10HeapObjectEEENS0_12InstanceTypeEiiiNS0_11MutableModeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	movzbl	13(%rdx), %edx
	popq	%rcx
	popq	%rsi
	andl	$1, %edx
	je	.L224
	movq	-1(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L226
.L227:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L230
.L238:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L231:
	xorl	%esi, %esi
	movq	%r12, %rdx
	call	_ZN2v88internal8JSObject18MakePrototypesFastENS0_6HandleINS0_6ObjectEEENS0_12WhereToStartEPNS0_7IsolateE@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject18MakePrototypesFastENS0_6HandleINS0_6ObjectEEENS0_12WhereToStartEPNS0_7IsolateE@PLT
	movq	0(%r13), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %eax
	orl	$32, %eax
	movl	%eax, 47(%rdx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L237
	leaq	-16(%rbp), %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L227
	movq	41112(%r12), %rdi
	movq	23(%rsi), %rsi
	testq	%rdi, %rdi
	jne	.L238
.L230:
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L239
.L232:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L226:
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L227
	movq	31(%rsi), %rsi
	testb	$1, %sil
	je	.L227
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L239:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L232
.L237:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21250:
	.size	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE, .-_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	.section	.text._ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE, @function
_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE:
.LFB21252:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r8d, %r10d
	movl	%r9d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	24(%rbp), %eax
	movq	16(%rbp), %r9
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	movzwl	%cx, %edx
	movq	%r14, %rsi
	movl	%r10d, %ecx
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r13
	subq	$8, %rsp
	pushq	%rax
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%rax, %r12
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21252:
	.size	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE, .-_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	.section	.text._ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE, @function
_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE:
.LFB21253:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movl	%r9d, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	-84(%rbp), %r9d
	movzwl	%bx, %ecx
	movl	%r15d, %r8d
	movq	%rax, %rdx
	movl	24(%rbp), %eax
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	%rax
	pushq	16(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L245
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L245:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21253:
	.size	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE, .-_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	.section	.text._ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0:
.LFB28354:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movl	%r9d, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	-84(%rbp), %r9d
	movzwl	%bx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$32, %r8d
	movq	%r12, %rdi
	pushq	%r9
	xorl	%r9d, %r9d
	pushq	%r15
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L249
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L249:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28354:
	.size	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0, .-_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	.section	.text._ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1, @function
_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1:
.LFB28353:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movq	%r9, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %r9
	movzwl	%bx, %ecx
	movl	%r15d, %r8d
	movq	%rax, %rdx
	movl	16(%rbp), %eax
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	%rax
	pushq	%r9
	xorl	%r9d, %r9d
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L253
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L253:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28353:
	.size	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1, .-_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	.section	.text._ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateEPKcNS0_12InstanceTypeEiiNS0_6HandleINS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateEPKcNS0_12InstanceTypeEiiNS0_6HandleINS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateEPKcNS0_12InstanceTypeEiiNS0_6HandleINS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0:
.LFB28359:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movl	%r9d, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	-84(%rbp), %r9d
	subq	$8, %rsp
	movzwl	%bx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	%r14d, %ecx
	movq	%r12, %rdi
	pushq	%r9
	movq	%r15, %r9
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L257
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L257:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28359:
	.size	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateEPKcNS0_12InstanceTypeEiiNS0_6HandleINS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0, .-_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateEPKcNS0_12InstanceTypeEiiNS0_6HandleINS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	.section	.rodata._ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv.part.0.str1.1,"aMS",@progbits,1
.LC32:
	.string	"Intl"
.LC33:
	.string	"DateTimeFormat"
.LC34:
	.string	"formatRange"
.LC35:
	.string	"formatRangeToParts"
	.section	.text._ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv.part.0, @function
_ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv.part.0:
.LFB28294:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC32(%rip), %rax
	movq	$4, -136(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rbx), %r15
	leaq	-152(%rbp), %rdi
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L259
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L260:
	movq	(%r12), %rdx
	movq	(%rbx), %rdi
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L262
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L262:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%rdi, -120(%rbp)
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L291
.L263:
	movq	%r13, %rdi
	movq	%r12, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r14, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L264
	movq	-120(%rbp), %rax
	leaq	88(%rax), %r12
.L265:
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	leaq	.LC33(%rip), %rax
	movq	$14, -136(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rbx), %rdi
	movq	(%rax), %rdx
	movq	%rax, %rsi
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L266
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L266:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%rdi, -120(%rbp)
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L292
.L267:
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L268
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L269:
	movq	(%rax), %rax
	movq	(%rbx), %r13
	movq	-1(%rax), %rdx
	movzbl	13(%rdx), %edx
	andl	$1, %edx
	je	.L271
	movq	-1(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L273
.L274:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L277
.L295:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L278:
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$1002, %ecx
	movq	%r12, %rsi
	leaq	.LC34(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$1003, %ecx
	leaq	.LC35(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L293
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L269
.L270:
	leaq	.LC23(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L265
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L259:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L294
.L261:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L271:
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L274
	movq	41112(%r13), %rdi
	movq	23(%rsi), %rsi
	testq	%rdi, %rdi
	jne	.L295
.L277:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L296
.L279:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L291:
	movq	%r12, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r12
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L273:
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L274
	movq	31(%rsi), %rsi
	testb	$1, %sil
	je	.L274
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L292:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%r13, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L279
.L293:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28294:
	.size	_ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv.part.0, .-_ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv.part.0
	.section	.rodata._ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"!constructor_or_backpointer().IsMap()"
	.section	.text._ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0, @function
_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0:
.LFB28347:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	leaq	31(%rax), %rdx
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L311
.L298:
	movq	%r12, (%rdx)
	testb	$1, %r12b
	je	.L297
	movq	%r12, %r13
	movq	(%rbx), %rdi
	andq	$-262144, %r13
	movq	8(%r13), %rax
	leaq	31(%rdi), %rsi
	testl	$262144, %eax
	jne	.L312
	testb	$24, %al
	je	.L297
.L314:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L313
.L297:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r13), %rax
	leaq	31(%rdi), %rsi
	testb	$24, %al
	jne	.L314
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L311:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L298
	leaq	.LC36(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L313:
	addq	$8, %rsp
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE28347:
	.size	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0, .-_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0
	.section	.rodata._ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv.part.0.str1.1,"aMS",@progbits,1
.LC37:
	.string	"FinalizationGroup"
.LC38:
	.string	"register"
.LC39:
	.string	"unregister"
.LC40:
	.string	"cleanupSome"
.LC41:
	.string	"deref"
.LC42:
	.string	"WeakRef"
	.section	.rodata._ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC43:
	.string	"FinalizationGroup Cleanup Iterator"
	.section	.rodata._ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv.part.0.str1.1
.LC44:
	.string	"next"
	.section	.text._ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv.part.0, @function
_ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv.part.0:
.LFB28293:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L316
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -88(%rbp)
.L317:
	leaq	.LC37(%rip), %rax
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$17, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L344
	movq	(%rbx), %rdx
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L320
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L321:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	pushq	$721
	movq	(%rbx), %rdi
	movq	%rax, %r9
	movl	$80, %ecx
	movl	$1083, %edx
	movq	%rax, %r14
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movq	(%rbx), %rdi
	movl	$97, %edx
	movq	%rax, %rsi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	-96(%rbp), %rcx
	movq	%r14, %rsi
	movl	$-1, %r10d
	movl	$1, %r11d
	leaq	2304(%r12), %rdx
	movl	$2, %r8d
	movq	(%rcx), %rax
	movq	23(%rax), %rax
	movw	%r10w, 41(%rax)
	movq	(%rcx), %rax
	movq	23(%rax), %rax
	movq	%rcx, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movw	%r11w, 39(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	-104(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	%r13, %rdx
	movq	(%rbx), %rdi
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$722, %ecx
	leaq	.LC38(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$723, %ecx
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$720, %ecx
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movl	$3, %ecx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movl	$32, %edx
	movl	$1081, %esi
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	(%rbx), %rdx
	movq	%rax, %r13
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r14
	movq	41112(%rdx), %rdi
	popq	%rax
	popq	%rcx
	testq	%rdi, %rdi
	je	.L323
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L324:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%rax, %r14
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	(%rbx), %rdi
	leaq	3568(%r12), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$725, %ecx
	movl	$2, %r9d
	leaq	.LC41(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	leaq	.LC42(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$7, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r14, %r9
	pushq	$724
	xorl	%r8d, %r8d
	movl	$32, %ecx
	movq	%rax, %rsi
	movl	$1081, %edx
	movq	%rax, %r13
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movq	(%rbx), %rdi
	movl	$96, %edx
	movq	%rax, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	-104(%rbp), %rcx
	movl	$-1, %edx
	movl	$1, %esi
	movl	$2, %r8d
	movq	(%rcx), %rax
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	(%rcx), %rax
	movq	23(%rax), %rax
	movq	-96(%rbp), %rdx
	movw	%si, 39(%rax)
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-88(%rbp), %rsi
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	movq	-104(%rbp), %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	16(%rbx), %rax
	movq	(%rbx), %r13
	movq	(%rax), %rax
	movq	495(%rax), %rsi
	movq	41112(%r13), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L326
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L327:
	movq	(%rbx), %r13
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r8
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L329
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L330:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	(%rbx), %r14
	movq	%r15, %rsi
	leaq	.LC43(%rip), %rax
	movq	%rax, -80(%rbp)
	movq	%r14, %rdi
	movq	$34, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$719, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC44(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$32, %edx
	movl	$1082, %esi
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	(%r12), %r12
	movq	(%rax), %r13
	movq	%r12, 759(%r13)
	leaq	759(%r13), %r14
	testb	$1, %r12b
	je	.L315
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L333
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L333:
	testb	$24, %al
	je	.L315
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L345
	.p2align 4,,10
	.p2align 3
.L315:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L346
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L347
.L322:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r14, (%rsi)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L316:
	movq	41088(%r12), %rax
	movq	%rax, -88(%rbp)
	cmpq	41096(%r12), %rax
	je	.L348
.L318:
	movq	-88(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L329:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L349
.L331:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r8, (%rsi)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L326:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L350
.L328:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L323:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L351
.L325:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r14, (%rsi)
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L345:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L344:
	leaq	.LC23(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L348:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, -88(%rbp)
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L349:
	movq	%r13, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L351:
	movq	%rdx, %rdi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L350:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L347:
	movq	%rdx, %rdi
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L322
.L346:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28293:
	.size	_ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv.part.0, .-_ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv.part.0
	.section	.rodata._ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv.part.0.str1.1,"aMS",@progbits,1
.LC45:
	.string	"Segmenter"
.LC46:
	.string	"supportedLocalesOf"
.LC47:
	.string	"Intl.Segmenter"
.LC48:
	.string	"resolvedOptions"
.LC49:
	.string	"segment"
.LC50:
	.string	"following"
.LC51:
	.string	"preceding"
	.section	.text._ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv.part.0, @function
_ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv.part.0:
.LFB28295:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC32(%rip), %rax
	movq	$4, -136(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rbx), %r15
	leaq	-152(%rbp), %rdi
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L353
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L354:
	movq	(%r12), %rdx
	movq	(%rbx), %rdi
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L356
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L356:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%rdi, -120(%rbp)
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L392
.L357:
	movq	%r13, %rdi
	movq	%r12, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r14, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L358
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rsi
.L359:
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movl	$48, %r8d
	movl	$1097, %ecx
	pushq	$1042
	leaq	.LC45(%rip), %rdx
	leaq	96(%rdi), %r9
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	xorl	%ecx, %ecx
	movl	$-1, %esi
	xorl	%r8d, %r8d
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	$2, %r9d
	leaq	.LC46(%rip), %rdx
	movq	23(%rax), %rax
	movw	%cx, 39(%rax)
	movq	(%r12), %rax
	movl	$1045, %ecx
	movq	23(%rax), %rax
	movw	%si, 41(%rax)
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%r12), %rax
	movq	(%rbx), %r14
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	popq	%rdi
	popq	%r8
	cmpw	$68, 11(%rax)
	jne	.L361
	movq	23(%rsi), %rsi
.L361:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L362
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L363:
	movq	(%rbx), %r14
	movq	%r13, %rsi
	leaq	.LC47(%rip), %rax
	movq	$14, -136(%rbp)
	movq	%rax, -144(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$1043, %ecx
	leaq	.LC48(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$1044, %ecx
	leaq	.LC49(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	16(%rbx), %rax
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	495(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L365
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L366:
	movq	(%rbx), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L368
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L369:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	leaq	1808(%rdi), %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$1050, %ecx
	movl	$2, %r9d
	leaq	.LC44(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$1047, %ecx
	movl	$2, %r9d
	leaq	.LC50(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$1048, %ecx
	movl	$2, %r9d
	leaq	.LC51(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movl	$1049, %ecx
	movq	%r12, %rsi
	leaq	2688(%rdi), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	movq	(%rbx), %rdi
	movl	$1046, %ecx
	movq	%r12, %rsi
	leaq	1208(%rdi), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	movq	(%rbx), %rdi
	leaq	1808(%rdi), %rsi
	call	_ZN2v88internal4Name14ToFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L371
	subq	$8, %rsp
	movq	%r12, %r9
	movl	$48, %ecx
	xorl	%r8d, %r8d
	pushq	$166
	movq	(%rbx), %rdi
	movl	$1096, %edx
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movq	(%rax), %rdx
	movq	23(%rdx), %rcx
	movl	47(%rcx), %edx
	andl	$-33, %edx
	movl	%edx, 47(%rcx)
	movq	(%rbx), %r13
	movq	(%rax), %rax
	movq	41112(%r13), %rdi
	movq	55(%rax), %r12
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L372
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
.L373:
	movq	16(%rbx), %rax
	movq	(%rax), %r13
	movq	%r12, 647(%r13)
	leaq	647(%r13), %r14
	testb	$1, %r12b
	je	.L352
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L376
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L376:
	testb	$24, %al
	je	.L352
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L393
	.p2align 4,,10
	.p2align 3
.L352:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L394
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L395
.L364:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L358:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L359
.L371:
	leaq	.LC23(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L353:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L396
.L355:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L365:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L397
.L367:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L368:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L398
.L370:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L372:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L399
.L374:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r12, (%rax)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L392:
	movq	%r12, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r12
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%r14, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L399:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L374
.L394:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28295:
	.size	_ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv.part.0, .-_ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv.part.0
	.section	.text._ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_,"axG",@progbits,_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_,comdat
	.p2align 4
	.weak	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	.type	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_, @function
_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_:
.LFB10518:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L400
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L405
.L400:
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE10518:
	.size	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_, .-_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	.section	.text._ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE,"axG",@progbits,_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE,comdat
	.p2align 4
	.weak	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	.type	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE, @function
_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE:
.LFB10525:
	.cfi_startproc
	endbr64
	testb	$1, %dl
	jne	.L412
.L406:
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L406
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L406
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE10525:
	.size	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE, .-_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	.section	.text._ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE,"axG",@progbits,_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE,comdat
	.p2align 4
	.weak	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	.type	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE, @function
_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE:
.LFB10529:
	.cfi_startproc
	endbr64
	testb	$1, %dl
	je	.L413
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L419
.L413:
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	jmp	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE10529:
	.size	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE, .-_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	.section	.text._ZN2v88internal7Isolate24initial_object_prototypeEv,"axG",@progbits,_ZN2v88internal7Isolate24initial_object_prototypeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Isolate24initial_object_prototypeEv
	.type	_ZN2v88internal7Isolate24initial_object_prototypeEv, @function
_ZN2v88internal7Isolate24initial_object_prototypeEv:
.LFB10796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	519(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L421
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L425
.L423:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L423
	.cfi_endproc
.LFE10796:
	.size	_ZN2v88internal7Isolate24initial_object_prototypeEv, .-_ZN2v88internal7Isolate24initial_object_prototypeEv
	.section	.text._ZN2v88internal7Isolate15object_functionEv,"axG",@progbits,_ZN2v88internal7Isolate15object_functionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Isolate15object_functionEv
	.type	_ZN2v88internal7Isolate15object_functionEv, @function
_ZN2v88internal7Isolate15object_functionEv:
.LFB10886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L427
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L431
.L429:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L429
	.cfi_endproc
.LFE10886:
	.size	_ZN2v88internal7Isolate15object_functionEv, .-_ZN2v88internal7Isolate15object_functionEv
	.section	.text._ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE,"axG",@progbits,_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	.type	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE, @function
_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE:
.LFB12040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movswq	9(%rax), %rdx
	movl	%edx, -76(%rbp)
	movq	%rdx, %r12
	addl	$1, %edx
	movw	%dx, 9(%rax)
	movq	(%rsi), %rax
	movq	(%rax), %rdx
	movq	16(%rsi), %rax
	movl	8(%rsi), %esi
	testl	%esi, %esi
	jne	.L433
	testq	%rax, %rax
	je	.L436
	movq	(%rax), %rax
	orq	$2, %rax
	movq	%rax, %r13
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L433:
	testq	%rax, %rax
	je	.L436
	movq	(%rax), %r13
.L435:
	movswl	%r12w, %eax
	movq	(%rcx), %rsi
	movl	24(%rbx), %r14d
	leal	3(%rax,%rax,2), %r8d
	sall	$3, %r8d
	movslq	%r8d, %r15
	movq	%rdx, -1(%r15,%rsi)
	movq	(%rcx), %rdi
	testb	$1, %dl
	je	.L453
	movq	%rdx, %r9
	leaq	-1(%r15,%rdi), %r10
	andq	$-262144, %r9
	movq	8(%r9), %rsi
	movq	%r9, -72(%rbp)
	testl	$262144, %esi
	je	.L438
	movq	%r10, %rsi
	movl	%r8d, -80(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	-72(%rbp), %r9
	movl	-80(%rbp), %r8d
	movq	-88(%rbp), %rdx
	movq	(%rcx), %rdi
	movq	8(%r9), %rsi
	leaq	-1(%r15,%rdi), %r10
.L438:
	andl	$24, %esi
	je	.L453
	movq	%rdi, %rsi
	andq	$-262144, %rsi
	testb	$24, 8(%rsi)
	je	.L471
	.p2align 4,,10
	.p2align 3
.L453:
	addl	%r14d, %r14d
	sarl	%r14d
	salq	$32, %r14
	movq	%r14, 7(%r15,%rdi)
	movq	(%rcx), %rdx
	movq	%r13, 15(%r15,%rdx)
	testb	$1, %r13b
	je	.L452
	cmpl	$3, %r13d
	je	.L452
	movq	%r13, %r14
	movq	(%rcx), %rdi
	addl	$16, %r8d
	movq	%r13, %rdx
	andq	$-262144, %r14
	movslq	%r8d, %r8
	andq	$-3, %rdx
	movq	8(%r14), %rax
	movq	%r8, -72(%rbp)
	leaq	-1(%rdi,%r8), %rsi
	testl	$262144, %eax
	jne	.L472
	testb	$24, %al
	je	.L452
.L477:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L452
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L452:
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movl	7(%rax), %ebx
	testb	$1, %bl
	jne	.L443
	shrl	$2, %ebx
.L444:
	movswl	%r12w, %eax
	movq	(%rcx), %rsi
	testl	%eax, %eax
	jle	.L445
	leal	(%rax,%rax,2), %edx
	subl	$1, %eax
	leaq	-64(%rbp), %r14
	subq	%rax, %r12
	sall	$3, %edx
	leaq	(%r12,%r12,2), %r12
	movslq	%edx, %r13
	salq	$3, %r12
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L474:
	shrl	$2, %eax
	leaq	24(%r13), %r15
	leaq	7(%rsi), %rdi
	cmpl	%eax, %ebx
	jnb	.L447
.L475:
	movq	7(%r13,%rsi), %rdi
	movq	7(%r15,%rsi), %rax
	sarq	$32, %rdi
	sarq	$32, %rax
	andl	$-523777, %eax
	andl	$523776, %edi
	orl	%edi, %eax
	addl	%eax, %eax
	sarl	%eax
	salq	$32, %rax
	movq	%rax, 7(%r15,%rsi)
	movq	(%rcx), %rsi
	cmpq	%r12, %r13
	je	.L473
	subq	$24, %r13
.L446:
	movq	7(%r13,%rsi), %rax
	shrq	$41, %rax
	andl	$1023, %eax
	leal	3(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rsi), %rdi
	movl	7(%rdi), %eax
	testb	$1, %al
	je	.L474
	movq	%rdi, -64(%rbp)
	movq	%r14, %rdi
	leaq	24(%r13), %r15
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-72(%rbp), %rcx
	movq	(%rcx), %rsi
	leaq	7(%rsi), %rdi
	cmpl	%eax, %ebx
	jb	.L475
.L447:
	movq	(%r15,%rdi), %rax
	movl	-76(%rbp), %r13d
	sarq	$32, %rax
	sall	$9, %r13d
	andl	$-523777, %eax
	orl	%r13d, %eax
	addl	%eax, %eax
	sarl	%eax
	salq	$32, %rax
	movq	%rax, 7(%r15,%rsi)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L476
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	leaq	-64(%rbp), %rdi
	movq	%rcx, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-72(%rbp), %rcx
	movl	%eax, %ebx
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L473:
	leaq	7(%rsi), %rdi
	movq	%r13, %r15
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	8(%r14), %rax
	movq	-88(%rbp), %rdx
	movq	(%rcx), %rdi
	leaq	-1(%rdi,%r8), %rsi
	testb	$24, %al
	jne	.L477
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%r10, %rsi
	movl	%r8d, -88(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movl	-88(%rbp), %r8d
	movq	(%rcx), %rdi
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L436:
	leaq	.LC23(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L445:
	leaq	7(%rsi), %rdi
	jmp	.L447
.L476:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12040:
	.size	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE, .-_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	.section	.rodata._ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi.str1.1,"aMS",@progbits,1
.LC52:
	.string	"captureStackTrace"
	.section	.rodata._ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi.str1.8,"aMS",@progbits,1
	.align 8
.LC53:
	.string	"JSReceiver::SetPrototype(error_fun, global_error, false, kThrowOnError) .FromMaybe(false)"
	.align 8
.LC54:
	.string	"JSReceiver::SetPrototype(prototype, handle(global_error->prototype(), isolate), false, kThrowOnError) .FromMaybe(false)"
	.align 8
.LC55:
	.string	"static_cast<unsigned>(number) <= static_cast<unsigned>(kMaxNumberOfDescriptors)"
	.section	.text._ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi, @function
_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi:
.LFB21289:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r9d
	movl	$40, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	movl	$1067, %ecx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	96(%rdi), %rax
	pushq	$333
	pushq	%rax
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movl	$-1, %edx
	movl	$1, %ecx
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	(%r12), %rax
	movq	23(%rax), %rax
	movw	%cx, 39(%rax)
	popq	%rsi
	popq	%rdi
	cmpl	$199, %r14d
	je	.L559
.L479:
	movq	%r12, %rsi
	movl	%r14d, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%r12), %rax
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L481
	movq	23(%rsi), %rsi
.L481:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L482
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L483:
	leaq	2872(%rbx), %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	leaq	128(%rbx), %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	leaq	2832(%rbx), %rdx
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	cmpl	$199, %r14d
	je	.L560
	movq	12464(%rbx), %rax
	movq	39(%rax), %rax
	movq	1615(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L499
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L500:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	leaq	3456(%rbx), %rdx
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	12464(%rbx), %rax
	movq	39(%rax), %rax
	movq	1607(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L502
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L503:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE@PLT
	testb	%al, %al
	jne	.L561
.L506:
	leaq	.LC53(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L499:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L562
.L501:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L482:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L563
.L484:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L560:
	movq	%rbx, %rdi
	movl	$2, %r9d
	movq	%r13, %rsi
	movl	$1, %r8d
	movl	$335, %ecx
	leaq	.LC18(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	%rax, %r15
	movq	12464(%rbx), %rax
	movq	39(%rax), %r14
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L486
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
.L487:
	movq	(%r15), %r15
	leaq	1615(%r14), %rsi
	movq	%r15, 1615(%r14)
	testb	$1, %r15b
	je	.L538
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -120(%rbp)
	testl	$262144, %eax
	je	.L490
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rsi
	movq	8(%rcx), %rax
.L490:
	testb	$24, %al
	je	.L538
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L538
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L538:
	movq	12464(%rbx), %rax
	movq	39(%rax), %r14
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L492
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
.L493:
	movq	0(%r13), %r13
	leaq	471(%r14), %rsi
	movq	%r13, 471(%r14)
	testb	$1, %r13b
	je	.L498
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	je	.L496
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-120(%rbp), %rsi
.L496:
	testb	$24, %al
	je	.L498
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L498
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L561:
	shrw	$8, %ax
	je	.L506
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	movzbl	13(%rdx), %edx
	andl	$1, %edx
	je	.L508
	movq	-1(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L510
.L511:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L514
.L566:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L515:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE@PLT
	testb	%al, %al
	jne	.L564
.L518:
	leaq	.LC54(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L502:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L565
.L504:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L508:
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L511
	movq	41112(%rbx), %rdi
	movq	23(%rsi), %rsi
	testq	%rdi, %rdi
	jne	.L566
.L514:
	movq	41088(%rbx), %r8
	cmpq	41096(%rbx), %r8
	je	.L567
.L516:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r8)
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L564:
	shrw	$8, %ax
	je	.L518
.L498:
	movq	(%r12), %rax
	movq	41112(%rbx), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L520
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L521:
	movq	%rbx, %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	4400(%rbx), %rax
	movq	41112(%rbx), %rdi
	leaq	4400(%rbx), %r13
	movq	7(%rax), %r14
	testq	%rdi, %rdi
	je	.L523
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L524:
	leaq	-96(%rbp), %r14
	movl	$2, %ecx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r12), %r13
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	movq	39(%r13), %rax
	movq	%rax, -104(%rbp)
	movl	15(%r13), %r12d
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	shrl	$10, %r12d
	andl	$1023, %r12d
	leal	1(%r12), %ecx
	cmpl	$1020, %ecx
	jg	.L568
	movl	15(%r13), %eax
	movl	%ecx, %edx
	sall	$10, %edx
	andl	$-1047553, %eax
	orl	%edx, %eax
	movl	%eax, 15(%r13)
	movq	-104(%rbp), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L527
	leaq	37592(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal41Heap_MarkingBarrierForDescriptorArraySlowEPNS0_4HeapENS0_10HeapObjectES3_i@PLT
.L527:
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	je	.L569
.L530:
	testb	$2, -72(%rbp)
	je	.L570
.L478:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L571
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L572
.L525:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%rsi)
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L520:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L573
.L522:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L569:
	testb	$8, 11(%rax)
	je	.L530
	movl	15(%r13), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%r13)
	testb	$2, -72(%rbp)
	jne	.L478
.L570:
	movzbl	9(%r13), %eax
	cmpl	$2, %eax
	jle	.L532
	movzbl	7(%r13), %edx
	cmpl	%edx, %eax
	je	.L574
	leal	1(%rax), %edx
	cmpl	$255, %eax
	je	.L575
	movb	%dl, 9(%r13)
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L559:
	xorl	%r8d, %r8d
	movl	$334, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC52(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L486:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L576
.L488:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r14, (%rax)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L532:
	leal	-1(%rax), %edx
	testl	%eax, %eax
	movl	$2, %eax
	cmovne	%edx, %eax
	movb	%al, 9(%r13)
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L577:
	movq	31(%rsi), %rsi
	testb	$1, %sil
	je	.L511
	.p2align 4,,10
	.p2align 3
.L510:
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L511
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L492:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L578
.L494:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r14, (%rax)
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L568:
	leaq	.LC55(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L563:
	movq	%rbx, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L573:
	movq	%rbx, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L572:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L565:
	movq	%rbx, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L562:
	movq	%rbx, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L567:
	movq	%rbx, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L574:
	movb	$2, 9(%r13)
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L578:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L576:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L488
.L575:
	leaq	.LC28(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L571:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21289:
	.size	_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi, .-_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi
	.section	.text._ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE,"axG",@progbits,_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	.type	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE, @function
_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE:
.LFB16131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%r14, %rsi
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-48(%rbp), %rdi
	movq	39(%rax), %rdx
	movq	%rdx, -48(%rbp)
	movl	15(%rax), %ebx
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	shrl	$10, %ebx
	andl	$1023, %ebx
	leal	1(%rbx), %ecx
	cmpl	$1020, %ecx
	jg	.L600
	movq	0(%r13), %rdx
	movl	%ecx, %esi
	sall	$10, %esi
	movl	15(%rdx), %eax
	andl	$-1047553, %eax
	orl	%esi, %eax
	movl	%eax, 15(%rdx)
	movq	-48(%rbp), %rdx
	movq	0(%r13), %rsi
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L601
.L581:
	movq	(%r14), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	je	.L602
.L583:
	testb	$2, 24(%r14)
	je	.L603
.L579:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L604
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	leaq	37592(%r12), %rdi
	call	_ZN2v88internal41Heap_MarkingBarrierForDescriptorArraySlowEPNS0_4HeapENS0_10HeapObjectES3_i@PLT
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L602:
	testb	$8, 11(%rax)
	je	.L583
	movq	0(%r13), %rdx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	testb	$2, 24(%r14)
	jne	.L579
.L603:
	movq	0(%r13), %rdx
	movzbl	9(%rdx), %eax
	cmpl	$2, %eax
	jle	.L586
	movzbl	7(%rdx), %ecx
	cmpl	%ecx, %eax
	je	.L605
	leal	1(%rax), %ecx
	cmpl	$255, %eax
	je	.L606
	movb	%cl, 9(%rdx)
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L586:
	leal	-1(%rax), %ecx
	testl	%eax, %eax
	movl	$2, %eax
	cmovne	%ecx, %eax
	movb	%al, 9(%rdx)
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L600:
	leaq	.LC55(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L605:
	movb	$2, 9(%rdx)
	jmp	.L579
.L606:
	leaq	.LC28(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L604:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16131:
	.size	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE, .-_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	.section	.text._ZN2v88internal15SourceCodeCache10InitializeEPNS0_7IsolateEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15SourceCodeCache10InitializeEPNS0_7IsolateEb
	.type	_ZN2v88internal15SourceCodeCache10InitializeEPNS0_7IsolateEb, @function
_ZN2v88internal15SourceCodeCache10InitializeEPNS0_7IsolateEb:
.LFB21206:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L610
	movq	%rax, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	movq	288(%rsi), %rax
	movq	%rax, 8(%rdi)
	ret
	.cfi_endproc
.LFE21206:
	.size	_ZN2v88internal15SourceCodeCache10InitializeEPNS0_7IsolateEb, .-_ZN2v88internal15SourceCodeCache10InitializeEPNS0_7IsolateEb
	.section	.text._ZN2v88internal15SourceCodeCache7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15SourceCodeCache7IterateEPNS0_11RootVisitorE
	.type	_ZN2v88internal15SourceCodeCache7IterateEPNS0_11RootVisitorE, @function
_ZN2v88internal15SourceCodeCache7IterateEPNS0_11RootVisitorE:
.LFB21207:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rdi, %r8
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdx
	movq	%rsi, %rdi
	leaq	8(%r8), %rcx
	movq	24(%rax), %r9
	cmpq	%rdx, %r9
	jne	.L612
	movq	16(%rax), %rax
	addq	$16, %r8
	xorl	%edx, %edx
	movl	$17, %esi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L612:
	xorl	%edx, %edx
	movl	$17, %esi
	jmp	*%r9
	.cfi_endproc
.LFE21207:
	.size	_ZN2v88internal15SourceCodeCache7IterateEPNS0_11RootVisitorE, .-_ZN2v88internal15SourceCodeCache7IterateEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal15SourceCodeCache6LookupEPNS0_7IsolateENS0_6VectorIKcEEPNS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15SourceCodeCache6LookupEPNS0_7IsolateENS0_6VectorIKcEEPNS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal15SourceCodeCache6LookupEPNS0_7IsolateENS0_6VectorIKcEEPNS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal15SourceCodeCache6LookupEPNS0_7IsolateENS0_6VectorIKcEEPNS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB21208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	%rdi, -72(%rbp)
	movq	%r8, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movl	11(%rax), %esi
	testl	%esi, %esi
	jle	.L621
	movq	%rdx, %r13
	movslq	%ecx, %rbx
	movl	$15, %r12d
	xorl	%r15d, %r15d
	leaq	-64(%rbp), %r14
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L616:
	movq	-72(%rbp), %rax
	addq	$16, %r12
	movq	8(%rax), %rax
	cmpl	%r15d, 11(%rax)
	jle	.L621
.L620:
	movl	%r15d, -76(%rbp)
	movq	(%rax,%r12), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	addl	$2, %r15d
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	testb	%al, %al
	je	.L616
	movl	-76(%rbp), %eax
	movq	-72(%rbp), %rcx
	leal	24(,%rax,8), %eax
	movq	8(%rcx), %rdx
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	-88(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L617
	movb	%r8b, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-72(%rbp), %r8d
.L618:
	movq	-96(%rbp), %rcx
	movq	%rax, (%rcx)
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L621:
	xorl	%r8d, %r8d
.L613:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L625
	addq	$56, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L617:
	.cfi_restore_state
	movq	%rax, %rcx
	movq	41088(%rax), %rax
	cmpq	41096(%rcx), %rax
	je	.L626
.L619:
	movq	-88(%rbp), %rcx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L618
.L626:
	movq	%rcx, %rdi
	movb	%r8b, -76(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movzbl	-76(%rbp), %r8d
	movq	-72(%rbp), %rsi
	jmp	.L619
.L625:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21208:
	.size	_ZN2v88internal15SourceCodeCache6LookupEPNS0_7IsolateENS0_6VectorIKcEEPNS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal15SourceCodeCache6LookupEPNS0_7IsolateENS0_6VectorIKcEEPNS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal15SourceCodeCache3AddEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15SourceCodeCache3AddEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal15SourceCodeCache3AddEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal15SourceCodeCache3AddEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB21209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	movslq	%ebx, %rbx
	subq	$72, %rsp
	movq	%rdx, -104(%rbp)
	movq	41096(%rsi), %r14
	movl	$1, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rsi), %rax
	addl	$1, 41104(%rsi)
	movq	%rax, -112(%rbp)
	movq	8(%rdi), %rax
	movq	%r12, %rdi
	movslq	11(%rax), %r9
	addl	$2, %r9d
	movl	%r9d, %esi
	movl	%r9d, -96(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	8(%r15), %rcx
	xorl	%esi, %esi
	leaq	8(%r15), %rdi
	movq	(%rax), %rdx
	movq	%rax, -88(%rbp)
	movl	11(%rcx), %r8d
	xorl	%ecx, %ecx
	call	_ZNK2v88internal10FixedArray6CopyToEiS1_ii@PLT
	movq	-88(%rbp), %rax
	movq	-104(%rbp), %r11
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$1, %edx
	movq	(%rax), %rax
	movq	%r11, -80(%rbp)
	movq	%rbx, -72(%rbp)
	movq	%rax, 8(%r15)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movl	-96(%rbp), %r9d
	testq	%rax, %rax
	je	.L654
	movq	(%rax), %rdx
	leal	0(,%r9,8), %ebx
	movq	8(%r15), %rax
	movslq	%ebx, %rcx
	movq	%rdx, -1(%rcx,%rax)
	movq	8(%r15), %rdi
	leaq	-1(%rdi), %rax
	testb	$1, %dl
	je	.L639
	movq	%rdx, %r8
	movq	%rcx, -96(%rbp)
	leaq	(%rcx,%rax), %r9
	andq	$-262144, %r8
	movq	8(%r8), %rsi
	movq	%r8, -88(%rbp)
	testl	$262144, %esi
	jne	.L655
	andl	$24, %esi
	je	.L639
.L661:
	movq	%rdi, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	je	.L656
	.p2align 4,,10
	.p2align 3
.L639:
	movq	0(%r13), %rdx
	addl	$8, %ebx
	movslq	%ebx, %rbx
	movq	%rdx, (%rbx,%rax)
	testb	$1, %dl
	je	.L638
	movq	%rdx, %rcx
	movq	8(%r15), %rdi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	leaq	-1(%rdi,%rbx), %rsi
	testl	$262144, %eax
	jne	.L657
	testb	$24, %al
	je	.L638
.L662:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L658
	.p2align 4,,10
	.p2align 3
.L638:
	movq	0(%r13), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L659
.L635:
	movslq	(%r15), %rdx
	salq	$32, %rdx
	movq	%rdx, 47(%rax)
	subl	$1, 41104(%r12)
	movq	-112(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L627
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L627:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L660
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	8(%r15), %rdi
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	8(%r8), %rsi
	leaq	-1(%rdi), %rax
	leaq	(%rax,%rcx), %r9
	andl	$24, %esi
	jne	.L661
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L657:
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	8(%r15), %rdi
	movq	-96(%rbp), %rdx
	movq	8(%rcx), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testb	$24, %al
	jne	.L662
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L659:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L635
	movq	23(%rax), %rax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L656:
	movq	%r9, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	subq	$1, %rax
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L658:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L654:
	leaq	.LC23(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L660:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21209:
	.size	_ZN2v88internal15SourceCodeCache3AddEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal15SourceCodeCache3AddEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal12BootstrapperC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12BootstrapperC2EPNS0_7IsolateE
	.type	_ZN2v88internal12BootstrapperC2EPNS0_7IsolateE, @function
_ZN2v88internal12BootstrapperC2EPNS0_7IsolateE:
.LFB21211:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	$0, 8(%rdi)
	movl	$1, 16(%rdi)
	movq	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE21211:
	.size	_ZN2v88internal12BootstrapperC2EPNS0_7IsolateE, .-_ZN2v88internal12BootstrapperC2EPNS0_7IsolateE
	.globl	_ZN2v88internal12BootstrapperC1EPNS0_7IsolateE
	.set	_ZN2v88internal12BootstrapperC1EPNS0_7IsolateE,_ZN2v88internal12BootstrapperC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal12Bootstrapper15GetNativeSourceENS0_10NativeTypeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper15GetNativeSourceENS0_10NativeTypeEi
	.type	_ZN2v88internal12Bootstrapper15GetNativeSourceENS0_10NativeTypeEi, @function
_ZN2v88internal12Bootstrapper15GetNativeSourceENS0_10NativeTypeEi:
.LFB21213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$32, %edi
	call	_Znwm@PLT
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal29NativesExternalStringResourceC1ENS0_10NativeTypeEi@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal7Factory21NewNativeSourceStringEPKNS_6String29ExternalOneByteStringResourceE@PLT
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21213:
	.size	_ZN2v88internal12Bootstrapper15GetNativeSourceENS0_10NativeTypeEi, .-_ZN2v88internal12Bootstrapper15GetNativeSourceENS0_10NativeTypeEi
	.section	.text._ZN2v88internal12Bootstrapper10InitializeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper10InitializeEb
	.type	_ZN2v88internal12Bootstrapper10InitializeEb, @function
_ZN2v88internal12Bootstrapper10InitializeEb:
.LFB21214:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	%sil, %sil
	jne	.L669
	movq	%rax, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L669:
	movq	(%rdi), %rax
	movq	288(%rax), %rax
	movq	%rax, 24(%rdi)
	ret
	.cfi_endproc
.LFE21214:
	.size	_ZN2v88internal12Bootstrapper10InitializeEb, .-_ZN2v88internal12Bootstrapper10InitializeEb
	.section	.rodata._ZN2v88internal12Bootstrapper24InitializeOncePerProcessEv.str1.1,"aMS",@progbits,1
.LC56:
	.string	"gc"
.LC57:
	.string	"native function freeBuffer();"
.LC58:
	.string	"v8/free-buffer"
.LC59:
	.string	"native function %s();"
.LC60:
	.string	"v8/gc"
.LC61:
	.string	"v8/externalize"
.LC62:
	.string	"v8/statistics"
.LC63:
	.string	"v8/trigger-failure"
.LC64:
	.string	"v8/ignition-statistics"
.LC65:
	.string	"v8/cpumark"
	.section	.text._ZN2v88internal12Bootstrapper24InitializeOncePerProcessEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper24InitializeOncePerProcessEv
	.type	_ZN2v88internal12Bootstrapper24InitializeOncePerProcessEv, @function
_ZN2v88internal12Bootstrapper24InitializeOncePerProcessEv:
.LFB21217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$56, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-48(%rbp), %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	leaq	.LC57(%rip), %rdx
	leaq	.LC58(%rip), %rsi
	movq	%rax, %rbx
	call	_ZN2v89ExtensionC2EPKcS2_iPS2_i@PLT
	leaq	16+_ZTVN2v88internal19FreeBufferExtensionE(%rip), %rax
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	%rax, (%rbx)
	call	_ZN2v817RegisterExtensionESt10unique_ptrINS_9ExtensionESt14default_deleteIS1_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L671
	movq	(%rdi), %rax
	call	*8(%rax)
.L671:
	movq	_ZN2v88internal17FLAG_expose_gc_asE(%rip), %r13
	testq	%r13, %r13
	je	.L681
	cmpb	$0, 0(%r13)
	leaq	.LC56(%rip), %rax
	cmove	%rax, %r13
.L672:
	movl	$104, %edi
	call	_Znwm@PLT
	movq	%r13, %rcx
	movl	$50, %esi
	leaq	.LC59(%rip), %rdx
	leaq	49(%rax), %r14
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	leaq	.LC60(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v89ExtensionC2EPKcS2_iPS2_i@PLT
	leaq	16+_ZTVN2v88internal11GCExtensionE(%rip), %rax
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	%rax, (%rbx)
	call	_ZN2v817RegisterExtensionESt10unique_ptrINS_9ExtensionESt14default_deleteIS1_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L673
	movq	(%rdi), %rax
	call	*8(%rax)
.L673:
	movl	$56, %edi
	call	_Znwm@PLT
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	_ZN2v88internal26ExternalizeStringExtension7kSourceE(%rip), %rdx
	movq	%rax, %rdi
	leaq	.LC61(%rip), %rsi
	movq	%rax, %rbx
	call	_ZN2v89ExtensionC2EPKcS2_iPS2_i@PLT
	leaq	16+_ZTVN2v88internal26ExternalizeStringExtensionE(%rip), %rax
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	%rax, (%rbx)
	call	_ZN2v817RegisterExtensionESt10unique_ptrINS_9ExtensionESt14default_deleteIS1_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L674
	movq	(%rdi), %rax
	call	*8(%rax)
.L674:
	movl	$56, %edi
	call	_Znwm@PLT
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	_ZN2v88internal19StatisticsExtension7kSourceE(%rip), %rdx
	movq	%rax, %rdi
	leaq	.LC62(%rip), %rsi
	movq	%rax, %rbx
	call	_ZN2v89ExtensionC2EPKcS2_iPS2_i@PLT
	leaq	16+_ZTVN2v88internal19StatisticsExtensionE(%rip), %rax
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	%rax, (%rbx)
	call	_ZN2v817RegisterExtensionESt10unique_ptrINS_9ExtensionESt14default_deleteIS1_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L675
	movq	(%rdi), %rax
	call	*8(%rax)
.L675:
	movl	$56, %edi
	call	_Znwm@PLT
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	_ZN2v88internal23TriggerFailureExtension7kSourceE(%rip), %rdx
	movq	%rax, %rdi
	leaq	.LC63(%rip), %rsi
	movq	%rax, %rbx
	call	_ZN2v89ExtensionC2EPKcS2_iPS2_i@PLT
	leaq	16+_ZTVN2v88internal23TriggerFailureExtensionE(%rip), %rax
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	%rax, (%rbx)
	call	_ZN2v817RegisterExtensionESt10unique_ptrINS_9ExtensionESt14default_deleteIS1_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L676
	movq	(%rdi), %rax
	call	*8(%rax)
.L676:
	movl	$56, %edi
	call	_Znwm@PLT
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	_ZN2v88internal27IgnitionStatisticsExtension7kSourceE(%rip), %rdx
	movq	%rax, %rdi
	leaq	.LC64(%rip), %rsi
	movq	%rax, %rbx
	call	_ZN2v89ExtensionC2EPKcS2_iPS2_i@PLT
	leaq	16+_ZTVN2v88internal27IgnitionStatisticsExtensionE(%rip), %rax
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	%rax, (%rbx)
	call	_ZN2v817RegisterExtensionESt10unique_ptrINS_9ExtensionESt14default_deleteIS1_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L677
	movq	(%rdi), %rax
	call	*8(%rax)
.L677:
	movq	_ZN2v88internal27FLAG_expose_cputracemark_asE(%rip), %r13
	testq	%r13, %r13
	je	.L670
	cmpb	$0, 0(%r13)
	je	.L670
	movl	$104, %edi
	call	_Znwm@PLT
	movq	%r13, %rcx
	movl	$50, %esi
	leaq	.LC59(%rip), %rdx
	leaq	49(%rax), %r14
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	leaq	.LC65(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v89ExtensionC2EPKcS2_iPS2_i@PLT
	leaq	16+_ZTVN2v88internal21CpuTraceMarkExtensionE(%rip), %rax
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	%rax, (%rbx)
	call	_ZN2v817RegisterExtensionESt10unique_ptrINS_9ExtensionESt14default_deleteIS1_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L670
	movq	(%rdi), %rax
	call	*8(%rax)
.L670:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L708
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L681:
	.cfi_restore_state
	leaq	.LC56(%rip), %r13
	jmp	.L672
.L708:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21217:
	.size	_ZN2v88internal12Bootstrapper24InitializeOncePerProcessEv, .-_ZN2v88internal12Bootstrapper24InitializeOncePerProcessEv
	.section	.text._ZN2v88internal12Bootstrapper8TearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper8TearDownEv
	.type	_ZN2v88internal12Bootstrapper8TearDownEv, @function
_ZN2v88internal12Bootstrapper8TearDownEv:
.LFB21232:
	.cfi_startproc
	endbr64
	movq	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE21232:
	.size	_ZN2v88internal12Bootstrapper8TearDownEv, .-_ZN2v88internal12Bootstrapper8TearDownEv
	.section	.text._ZN2v88internal12Bootstrapper7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper7IterateEPNS0_11RootVisitorE
	.type	_ZN2v88internal12Bootstrapper7IterateEPNS0_11RootVisitorE, @function
_ZN2v88internal12Bootstrapper7IterateEPNS0_11RootVisitorE:
.LFB21240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdx
	leaq	24(%rdi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	24(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L711
	leaq	32(%rdi), %r8
	xorl	%edx, %edx
	movl	$17, %esi
	movq	%r12, %rdi
	call	*16(%rax)
.L712:
	movq	(%r12), %rax
	leaq	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE(%rip), %rdx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L715
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L711:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$17, %esi
	movq	%r12, %rdi
	call	*%r8
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L715:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$17, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE21240:
	.size	_ZN2v88internal12Bootstrapper7IterateEPNS0_11RootVisitorE, .-_ZN2v88internal12Bootstrapper7IterateEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal12Bootstrapper10LogAllMapsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper10LogAllMapsEv
	.type	_ZN2v88internal12Bootstrapper10LogAllMapsEv, @function
_ZN2v88internal12Bootstrapper10LogAllMapsEv:
.LFB21246:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	je	.L724
	movq	(%rdi), %rax
	cmpb	$0, 41458(%rax)
	je	.L727
.L724:
	ret
	.p2align 4,,10
	.p2align 3
.L727:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	41016(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L728
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L728:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Logger10LogAllMapsEv@PLT
	.cfi_endproc
.LFE21246:
	.size	_ZN2v88internal12Bootstrapper10LogAllMapsEv, .-_ZN2v88internal12Bootstrapper10LogAllMapsEv
	.section	.text._ZN2v88internal12Bootstrapper12DetachGlobalENS0_6HandleINS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper12DetachGlobalENS0_6HandleINS0_7ContextEEE
	.type	_ZN2v88internal12Bootstrapper12DetachGlobalENS0_6HandleINS0_7ContextEEE, @function
_ZN2v88internal12Bootstrapper12DetachGlobalENS0_6HandleINS0_7ContextEEE:
.LFB21247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r15, %rdi
	movq	40960(%rax), %r14
	movq	(%rsi), %rax
	addq	$208, %r14
	movq	39(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal13NativeContext15GetErrorsThrownEv@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	(%r12), %r14
	movq	0(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L730
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L731:
	movq	(%rbx), %rdi
	movq	104(%r14), %r15
	movq	%r15, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %r15b
	je	.L742
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L757
	testb	$24, %al
	je	.L742
.L764:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L758
	.p2align 4,,10
	.p2align 3
.L742:
	movq	(%r12), %rax
	movq	%rbx, %rdi
	leaq	104(%rax), %rsi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	(%rbx), %rax
	movq	-1(%rax), %r15
	movq	104(%r14), %r14
	movq	31(%r15), %rax
	leaq	31(%r15), %rsi
	testb	$1, %al
	jne	.L759
.L736:
	movq	%r14, 31(%r15)
	testb	$1, %r14b
	je	.L741
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L760
	testb	$24, %al
	je	.L741
.L765:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L761
	.p2align 4,,10
	.p2align 3
.L741:
	cmpb	$0, _ZN2v88internal28FLAG_track_detached_contextsE(%rip)
	jne	.L762
.L740:
	movq	0(%r13), %rax
	movq	39(%rax), %rax
	movq	$0, 1967(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L763
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L757:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L764
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L760:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-72(%rbp), %rsi
	testb	$24, %al
	jne	.L765
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L730:
	movq	41088(%r14), %rbx
	cmpq	41096(%r14), %rbx
	je	.L766
.L732:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rbx)
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L759:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L736
	leaq	.LC36(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L762:
	movq	(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal7Isolate18AddDetachedContextENS0_6HandleINS0_7ContextEEE@PLT
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L758:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L761:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L766:
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L732
.L763:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21247:
	.size	_ZN2v88internal12Bootstrapper12DetachGlobalENS0_6HandleINS0_7ContextEEE, .-_ZN2v88internal12Bootstrapper12DetachGlobalENS0_6HandleINS0_7ContextEEE
	.section	.rodata._ZN2v88internal7Genesis19CreateEmptyFunctionEv.str1.1,"aMS",@progbits,1
.LC66:
	.string	"() {}"
	.section	.text._ZN2v88internal7Genesis19CreateEmptyFunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis19CreateEmptyFunctionEv
	.type	_ZN2v88internal7Genesis19CreateEmptyFunctionEv, @function
_ZN2v88internal7Genesis19CreateEmptyFunctionEv:
.LFB21267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory23CreateSloppyFunctionMapENS0_12FunctionModeENS0_11MaybeHandleINS0_10JSFunctionEEE@PLT
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	%rax, %rdx
	movl	15(%rcx), %eax
	orl	$1048576, %eax
	movl	%eax, 15(%rcx)
	movl	$165, %ecx
	movq	(%rbx), %rax
	leaq	128(%rax), %rsi
	call	_ZN2v88internal15NewFunctionArgs10ForBuiltinENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEEi@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	(%r12), %r13
	movq	(%rax), %r14
	movq	%r13, 343(%r14)
	testb	$1, %r13b
	je	.L783
	movq	%r13, %r15
	leaq	343(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L804
	testb	$24, %al
	je	.L783
.L813:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L805
	.p2align 4,,10
	.p2align 3
.L783:
	movq	(%rbx), %rdi
	leaq	.LC66(%rip), %rax
	leaq	-144(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -144(%rbp)
	movq	$5, -136(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L806
	movq	(%rbx), %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory9NewScriptENS0_6HandleINS0_6StringEEENS0_14AllocationTypeE@PLT
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	$0, 47(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Factory17NewWeakFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	0(%r13), %r15
	movq	(%rax), %r14
	leaq	87(%r15), %rsi
	movq	%r14, 87(%r15)
	testb	$1, %r14b
	je	.L782
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -152(%rbp)
	testl	$262144, %eax
	jne	.L807
	testb	$24, %al
	je	.L782
.L815:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L808
	.p2align 4,,10
	.p2align 3
.L782:
	movq	(%r12), %rax
	movq	23(%rax), %r15
	movq	(%rbx), %rax
	movq	1168(%rax), %r14
	leaq	15(%r15), %rsi
	movq	%r14, 15(%r15)
	testb	$1, %r14b
	je	.L781
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -152(%rbp)
	testl	$262144, %eax
	jne	.L809
	testb	$24, %al
	je	.L781
.L814:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L810
	.p2align 4,,10
	.p2align 3
.L781:
	movq	(%r12), %rax
	movl	$-1, %edx
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	(%rbx), %rbx
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L778
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L779:
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal18SharedFunctionInfo9SetScriptENS0_6HandleIS1_EENS2_INS0_6ObjectEEEib@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L811
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L778:
	.cfi_restore_state
	movq	41088(%rbx), %rdi
	cmpq	%rdi, 41096(%rbx)
	je	.L812
.L780:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L804:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-152(%rbp), %rsi
	testb	$24, %al
	jne	.L813
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L809:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L814
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L807:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L815
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L805:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L808:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L810:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L806:
	leaq	.LC23(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L812:
	movq	%rbx, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L780
.L811:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21267:
	.size	_ZN2v88internal7Genesis19CreateEmptyFunctionEv, .-_ZN2v88internal7Genesis19CreateEmptyFunctionEv
	.section	.text._ZN2v88internal7Genesis28CreateSloppyModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis28CreateSloppyModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal7Genesis28CreateSloppyModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal7Genesis28CreateSloppyModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE:
.LFB21271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory23CreateSloppyFunctionMapENS0_12FunctionModeENS0_11MaybeHandleINS0_10JSFunctionEEE@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 1263(%r15)
	testb	$1, %r14b
	je	.L832
	movq	%r14, %rcx
	leaq	1263(%r15), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L858
	testb	$24, %al
	je	.L832
.L867:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L859
	.p2align 4,,10
	.p2align 3
.L832:
	movq	%r12, %rdx
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory23CreateSloppyFunctionMapENS0_12FunctionModeENS0_11MaybeHandleINS0_10JSFunctionEEE@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 1271(%r15)
	leaq	1271(%r15), %rsi
	testb	$1, %r14b
	je	.L831
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L860
	testb	$24, %al
	je	.L831
.L866:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L861
	.p2align 4,,10
	.p2align 3
.L831:
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory23CreateSloppyFunctionMapENS0_12FunctionModeENS0_11MaybeHandleINS0_10JSFunctionEEE@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 1247(%r15)
	leaq	1247(%r15), %rsi
	testb	$1, %r14b
	je	.L830
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L862
	testb	$24, %al
	je	.L830
.L869:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L863
	.p2align 4,,10
	.p2align 3
.L830:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$5, %esi
	call	_ZN2v88internal7Factory23CreateSloppyFunctionMapENS0_12FunctionModeENS0_11MaybeHandleINS0_10JSFunctionEEE@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %r12
	movq	(%rdx), %r13
	movq	%r12, 1255(%r13)
	leaq	1255(%r13), %r14
	testb	$1, %r12b
	je	.L816
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L864
	testb	$24, %al
	je	.L816
.L868:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L865
.L816:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L860:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L866
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L858:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L867
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L864:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L868
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L862:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L869
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L859:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L861:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L863:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L865:
	addq	$24, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE21271:
	.size	_ZN2v88internal7Genesis28CreateSloppyModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal7Genesis28CreateSloppyModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv
	.type	_ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv, @function
_ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv:
.LFB21272:
	.cfi_startproc
	endbr64
	cmpq	$0, 48(%rdi)
	je	.L871
	movq	48(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L871:
	jmp	_ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv.part.0
	.cfi_endproc
.LFE21272:
	.size	_ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv, .-_ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv
	.section	.rodata._ZN2v88internal7Genesis20CreateObjectFunctionENS0_6HandleINS0_10JSFunctionEEE.str1.1,"aMS",@progbits,1
.LC67:
	.string	"EmptyObjectPrototype"
	.section	.rodata._ZN2v88internal7Genesis20CreateObjectFunctionENS0_6HandleINS0_10JSFunctionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC68:
	.string	"slow_object_with_object_prototype_map"
	.section	.text._ZN2v88internal7Genesis20CreateObjectFunctionENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis20CreateObjectFunctionENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal7Genesis20CreateObjectFunctionENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal7Genesis20CreateObjectFunctionENS0_6HandleINS0_10JSFunctionEEE:
.LFB21274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %r8d
	movl	$56, %ecx
	movl	$1057, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	(%rdi), %r13
	pushq	$458
	leaq	104(%r13), %r14
	leaq	3000(%r13), %rsi
	movq	%r13, %rdi
	movq	%r14, %r9
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movw	%dx, 39(%rax)
	movq	(%r12), %rax
	movq	23(%rax), %rax
	movw	%cx, 41(%rax)
	movq	16(%rbx), %rax
	movq	(%r12), %rdx
	movq	(%rax), %rdi
	movq	%rdx, 879(%rdi)
	popq	%rsi
	popq	%r8
	testb	$1, %dl
	je	.L902
	movq	%rdx, %rcx
	leaq	879(%rdi), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L934
	testb	$24, %al
	je	.L902
.L943:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L935
	.p2align 4,,10
	.p2align 3
.L902:
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	55(%rax), %rdx
	movzbl	14(%rdx), %eax
	andb	$7, %al
	orl	$24, %eax
	movb	%al, 14(%rdx)
	call	_ZN2v88internal7Factory20NewFunctionPrototypeENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	(%rbx), %rdx
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	-1(%rax), %r8
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L878
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L879:
	movq	(%rbx), %rdi
	leaq	.LC67(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	(%rax), %rcx
	movl	15(%rcx), %edx
	orl	$1048576, %edx
	movl	%edx, 15(%rcx)
	movq	(%rax), %rdx
	orb	$2, 14(%rdx)
	movq	0(%r13), %rdi
	movq	(%rax), %rdx
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	jne	.L936
.L881:
	movq	(%rbx), %rdx
	movq	(%r15), %rax
	movq	-1(%rax), %r15
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L884
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L885:
	movq	(%rbx), %rdi
	movl	$1, %ecx
	movq	%r13, %rdx
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	0(%r13), %r15
	movq	(%rax), %rdi
	movq	%r15, 519(%rdi)
	leaq	519(%rdi), %rsi
	testb	$1, %r15b
	je	.L901
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L937
	testb	$24, %al
	je	.L901
.L946:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L938
	.p2align 4,,10
	.p2align 3
.L901:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	(%rbx), %r15
	movq	(%r12), %rax
	movq	41112(%r15), %rdi
	movq	55(%rax), %r12
	testq	%rdi, %rdi
	je	.L890
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L891:
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal3Map24CopyInitialMapNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	movl	$1, %ecx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	(%r12), %r14
	movq	(%rax), %r15
	movq	%r14, 1215(%r15)
	leaq	1215(%r15), %rsi
	testb	$1, %r14b
	je	.L900
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L939
	testb	$24, %al
	je	.L900
.L945:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L940
	.p2align 4,,10
	.p2align 3
.L900:
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	leaq	.LC68(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	(%r12), %r12
	movq	(%rax), %r13
	movq	%r12, 1223(%r13)
	leaq	1223(%r13), %r14
	testb	$1, %r12b
	je	.L874
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L941
	testb	$24, %al
	je	.L874
.L944:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L942
.L874:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L934:
	.cfi_restore_state
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L943
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L941:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L944
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L939:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L945
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L937:
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L946
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L890:
	movq	41088(%r15), %rsi
	cmpq	%rsi, 41096(%r15)
	je	.L947
.L892:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r12, (%rsi)
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L884:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L948
.L886:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r15, (%rsi)
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L878:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L949
.L880:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r8, (%rsi)
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L936:
	testb	$1, %dl
	je	.L881
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L881
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L935:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L938:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L940:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L942:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L949:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L948:
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L947:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L892
	.cfi_endproc
.LFE21274:
	.size	_ZN2v88internal7Genesis20CreateObjectFunctionENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal7Genesis20CreateObjectFunctionENS0_6HandleINS0_10JSFunctionEEE
	.section	.rodata._ZN2v88internal7Genesis18CreateIteratorMapsENS0_6HandleINS0_10JSFunctionEEE.str1.1,"aMS",@progbits,1
.LC69:
	.string	"[Symbol.iterator]"
.LC70:
	.string	"GeneratorFunction"
.LC71:
	.string	"Generator"
.LC72:
	.string	"return"
.LC73:
	.string	"throw"
.LC74:
	.string	"GeneratorFunction with name"
	.section	.rodata._ZN2v88internal7Genesis18CreateIteratorMapsENS0_6HandleINS0_10JSFunctionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC75:
	.string	"GeneratorFunction with home object"
	.align 8
.LC76:
	.string	"GeneratorFunction with name and home object"
	.section	.text._ZN2v88internal7Genesis18CreateIteratorMapsENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis18CreateIteratorMapsENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal7Genesis18CreateIteratorMapsENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal7Genesis18CreateIteratorMapsENS0_6HandleINS0_10JSFunctionEEE:
.LFB21276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L951
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L952:
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	pushq	$2
	xorl	%r9d, %r9d
	pushq	$1
	movq	%rax, %rsi
	movl	$169, %r8d
	movq	%rax, %r13
	leaq	3856(%rdi), %rdx
	leaq	.LC69(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	16(%rbx), %rax
	movq	0(%r13), %r12
	movq	(%rax), %r15
	movq	%r12, 495(%r15)
	leaq	495(%r15), %rsi
	popq	%rax
	popq	%rdx
	testb	$1, %r12b
	je	.L1000
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1051
	testb	$24, %al
	je	.L1000
.L1068:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1052
	.p2align 4,,10
	.p2align 3
.L1000:
	movq	(%rbx), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L957
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L958:
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	(%r12), %r15
	movq	(%rax), %rdi
	movq	%r15, 479(%rdi)
	leaq	479(%rdi), %rsi
	testb	$1, %r15b
	je	.L999
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1053
	testb	$24, %al
	je	.L999
.L1070:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1054
	.p2align 4,,10
	.p2align 3
.L999:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	(%rbx), %r13
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r15
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L963
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L964:
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	-80(%rbp), %r15
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	(%rbx), %r14
	movq	%r15, %rsi
	leaq	.LC70(%rip), %rax
	movq	%rax, -80(%rbp)
	movq	%r14, %rdi
	movq	$17, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	movq	%r12, %rcx
	movq	%r13, %rsi
	movl	$3, %r8d
	leaq	3104(%rdi), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r12, %rsi
	movl	$3, %r8d
	leaq	2304(%rdi), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %r14
	movq	%r15, %rsi
	leaq	.LC71(%rip), %rax
	movq	%rax, -80(%rbp)
	movq	%r14, %rdi
	movq	$9, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$353, %ecx
	movl	$2, %r9d
	leaq	.LC44(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$354, %ecx
	movl	$2, %r9d
	leaq	.LC72(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$355, %ecx
	movl	$2, %r9d
	leaq	.LC73(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movl	$353, %edx
	leaq	2912(%rdi), %rsi
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movq	(%rax), %rdx
	movq	23(%rdx), %rcx
	movl	47(%rcx), %edx
	andl	$-33, %edx
	movl	%edx, 47(%rcx)
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 1831(%r15)
	leaq	1831(%r15), %rsi
	testb	$1, %r14b
	je	.L998
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1055
	testb	$24, %al
	je	.L998
.L1069:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1056
	.p2align 4,,10
	.p2align 3
.L998:
	movq	(%rbx), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1279(%rax), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L969
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L970:
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	leaq	.LC70(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 1367(%r15)
	leaq	1367(%r15), %rsi
	testb	$1, %r14b
	je	.L997
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1057
	testb	$24, %al
	je	.L997
.L1072:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1058
	.p2align 4,,10
	.p2align 3
.L997:
	movq	(%rbx), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1287(%rax), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L975
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L976:
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	leaq	.LC74(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 1375(%r15)
	leaq	1375(%r15), %rsi
	testb	$1, %r14b
	je	.L996
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1059
	testb	$24, %al
	je	.L996
.L1071:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1060
	.p2align 4,,10
	.p2align 3
.L996:
	movq	32(%rbx), %rsi
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	leaq	.LC75(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 1383(%r15)
	leaq	1383(%r15), %rsi
	testb	$1, %r14b
	je	.L995
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1061
	testb	$24, %al
	je	.L995
.L1074:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1062
	.p2align 4,,10
	.p2align 3
.L995:
	movq	40(%rbx), %rsi
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	leaq	.LC76(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc
	movq	16(%rbx), %rdx
	movq	(%rax), %r13
	movq	(%rdx), %r14
	movq	%r13, 1391(%r14)
	leaq	1391(%r14), %rsi
	testb	$1, %r13b
	je	.L994
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1063
	testb	$24, %al
	je	.L994
.L1073:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1064
	.p2align 4,,10
	.p2align 3
.L994:
	movq	16(%rbx), %rax
	movq	(%rbx), %r13
	movq	(%rax), %rax
	movq	879(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L987
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L988:
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal3Map6CreateEPNS0_7IsolateEi@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	movl	$1, %ecx
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	0(%r13), %r12
	movq	(%rax), %r14
	movq	%r12, 431(%r14)
	leaq	431(%r14), %r13
	testb	$1, %r12b
	je	.L950
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1065
	testb	$24, %al
	je	.L950
.L1075:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1066
	.p2align 4,,10
	.p2align 3
.L950:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1067
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1051:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1068
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1069
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	%r15, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1070
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1071
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1072
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1063:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-88(%rbp), %rsi
	testb	$24, %al
	jne	.L1073
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1074
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1075
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L987:
	movq	41088(%r13), %rax
	cmpq	%rax, 41096(%r13)
	je	.L1076
.L989:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L975:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L1077
.L977:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L969:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L1078
.L971:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L963:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L1079
.L965:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rsi)
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L957:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1080
.L959:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L951:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1081
.L953:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1054:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1060:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1066:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L1079:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L1078:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1076:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L989
.L1067:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21276:
	.size	_ZN2v88internal7Genesis18CreateIteratorMapsENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal7Genesis18CreateIteratorMapsENS0_6HandleINS0_10JSFunctionEEE
	.section	.rodata._ZN2v88internal7Genesis23CreateAsyncIteratorMapsENS0_6HandleINS0_10JSFunctionEEE.str1.1,"aMS",@progbits,1
.LC77:
	.string	"[Symbol.asyncIterator]"
.LC78:
	.string	"Async-from-Sync Iterator"
.LC79:
	.string	"AsyncGeneratorFunction"
.LC80:
	.string	"AsyncGenerator"
	.section	.rodata._ZN2v88internal7Genesis23CreateAsyncIteratorMapsENS0_6HandleINS0_10JSFunctionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC81:
	.string	"AsyncGeneratorFunction with name"
	.align 8
.LC82:
	.string	"AsyncGeneratorFunction with home object"
	.align 8
.LC83:
	.string	"AsyncGeneratorFunction with name and home object"
	.section	.text._ZN2v88internal7Genesis23CreateAsyncIteratorMapsENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis23CreateAsyncIteratorMapsENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal7Genesis23CreateAsyncIteratorMapsENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal7Genesis23CreateAsyncIteratorMapsENS0_6HandleINS0_10JSFunctionEEE:
.LFB21277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rsi, -88(%rbp)
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1083
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1084:
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	pushq	$2
	xorl	%r9d, %r9d
	pushq	$1
	movl	$169, %r8d
	movq	%rax, %rsi
	movq	%rax, %r14
	leaq	3848(%rdi), %rdx
	leaq	.LC77(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	(%rbx), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%r12), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1086
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1087:
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	-80(%rbp), %r15
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movl	$1, %r8d
	movl	$691, %ecx
	leaq	.LC44(%rip), %rdx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	movl	$693, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC72(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$692, %ecx
	movq	%r12, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC73(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %r13
	movq	%r15, %rsi
	leaq	.LC78(%rip), %rax
	movq	%rax, -80(%rbp)
	movq	%r13, %rdi
	movq	$24, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$40, %edx
	movl	$1062, %esi
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	movl	$1, %ecx
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	0(%r13), %r12
	movq	(%rax), %rdi
	movq	%r12, 119(%rdi)
	leaq	119(%rdi), %rsi
	testb	$1, %r12b
	je	.L1131
	movq	%r12, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testl	$262144, %eax
	jne	.L1176
	testb	$24, %al
	je	.L1131
.L1192:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1177
	.p2align 4,,10
	.p2align 3
.L1131:
	movq	(%rbx), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1092
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1093:
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %r13
	movq	%rax, %r12
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r8
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1095
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1096:
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rcx
	movq	%r13, %rsi
	movl	$3, %r8d
	leaq	3104(%rdi), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r12, %rsi
	movl	$3, %r8d
	leaq	2304(%rdi), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC79(%rip), %rax
	movq	%rax, -80(%rbp)
	movq	%rdi, -88(%rbp)
	movq	$22, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	16(%rbx), %rax
	movq	(%r12), %r14
	movq	(%rax), %rdi
	movq	%r14, 487(%rdi)
	leaq	487(%rdi), %rsi
	testb	$1, %r14b
	je	.L1130
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1178
	testb	$24, %al
	je	.L1130
.L1191:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1179
	.p2align 4,,10
	.p2align 3
.L1130:
	movq	(%rbx), %r14
	movq	%r15, %rsi
	leaq	.LC80(%rip), %rax
	movq	$14, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$680, %ecx
	movl	$2, %r9d
	leaq	.LC44(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$681, %ecx
	movl	$2, %r9d
	leaq	.LC72(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$682, %ecx
	movl	$2, %r9d
	leaq	.LC73(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1279(%rax), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1101
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1102:
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	leaq	.LC79(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 1399(%r15)
	leaq	1399(%r15), %rsi
	testb	$1, %r14b
	je	.L1129
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1180
	testb	$24, %al
	je	.L1129
.L1193:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1181
	.p2align 4,,10
	.p2align 3
.L1129:
	movq	(%rbx), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1287(%rax), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1107
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1108:
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	leaq	.LC81(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 1407(%r15)
	leaq	1407(%r15), %rsi
	testb	$1, %r14b
	je	.L1128
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1182
	testb	$24, %al
	je	.L1128
.L1197:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1183
	.p2align 4,,10
	.p2align 3
.L1128:
	movq	32(%rbx), %rsi
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	leaq	.LC82(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 1415(%r15)
	leaq	1415(%r15), %rsi
	testb	$1, %r14b
	je	.L1127
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1184
	testb	$24, %al
	je	.L1127
.L1196:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1185
	.p2align 4,,10
	.p2align 3
.L1127:
	movq	40(%rbx), %rsi
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	leaq	.LC83(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123CreateNonConstructorMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_8JSObjectEEEPKc
	movq	16(%rbx), %rdx
	movq	(%rax), %r13
	movq	(%rdx), %r14
	movq	%r13, 1423(%r14)
	leaq	1423(%r14), %rsi
	testb	$1, %r13b
	je	.L1126
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1186
	testb	$24, %al
	je	.L1126
.L1195:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1187
	.p2align 4,,10
	.p2align 3
.L1126:
	movq	16(%rbx), %rax
	movq	(%rbx), %r13
	movq	(%rax), %rax
	movq	879(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1119
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1120:
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal3Map6CreateEPNS0_7IsolateEi@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	movl	$1, %ecx
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	0(%r13), %r12
	movq	(%rax), %r14
	movq	%r12, 439(%r14)
	leaq	439(%r14), %r13
	testb	$1, %r12b
	je	.L1082
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1188
	testb	$24, %al
	je	.L1082
.L1194:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1189
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1190
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1178:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1191
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1176:
	movq	%r12, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rax
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L1192
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1193
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1194
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1186:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-88(%rbp), %rsi
	testb	$24, %al
	jne	.L1195
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1196
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1197
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1119:
	movq	41088(%r13), %rax
	cmpq	%rax, 41096(%r13)
	je	.L1198
.L1121:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L1199
.L1109:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1086:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1200
.L1088:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1101:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L1201
.L1103:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1092:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1202
.L1094:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L1203
.L1097:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r8, (%rsi)
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1204
.L1085:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1177:
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1179:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1181:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1183:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1185:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1187:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1189:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1204:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1203:
	movq	%r13, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1202:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1201:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1198:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L1121
.L1190:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21277:
	.size	_ZN2v88internal7Genesis23CreateAsyncIteratorMapsENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal7Genesis23CreateAsyncIteratorMapsENS0_6HandleINS0_10JSFunctionEEE
	.section	.rodata._ZN2v88internal7Genesis23CreateAsyncFunctionMapsENS0_6HandleINS0_10JSFunctionEEE.str1.1,"aMS",@progbits,1
.LC84:
	.string	"AsyncFunction"
.LC85:
	.string	"AsyncFunction with name"
	.section	.rodata._ZN2v88internal7Genesis23CreateAsyncFunctionMapsENS0_6HandleINS0_10JSFunctionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC86:
	.string	"AsyncFunction with home object"
	.align 8
.LC87:
	.string	"AsyncFunction with name and home object"
	.section	.text._ZN2v88internal7Genesis23CreateAsyncFunctionMapsENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis23CreateAsyncFunctionMapsENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal7Genesis23CreateAsyncFunctionMapsENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal7Genesis23CreateAsyncFunctionMapsENS0_6HandleINS0_10JSFunctionEEE:
.LFB21278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1206
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1207:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	(%rbx), %r12
	leaq	.LC84(%rip), %rax
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	%r12, %rdi
	movq	$13, -72(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1303(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1209
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1210:
	movq	(%rbx), %rdi
	leaq	.LC84(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	(%rbx), %rdi
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	(%r12), %r12
	movq	(%rax), %r14
	movq	%r12, 1335(%r14)
	leaq	1335(%r14), %rsi
	testb	$1, %r12b
	je	.L1236
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1263
	testb	$24, %al
	je	.L1236
.L1275:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1264
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	(%rbx), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1311(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1215
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1216:
	movq	(%rbx), %rdi
	leaq	.LC85(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	(%rbx), %rdi
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	(%r12), %r12
	movq	(%rax), %r14
	movq	%r12, 1343(%r14)
	leaq	1343(%r14), %rsi
	testb	$1, %r12b
	je	.L1235
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1265
	testb	$24, %al
	je	.L1235
.L1272:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1266
	.p2align 4,,10
	.p2align 3
.L1235:
	movq	(%rbx), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1319(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1221
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1222:
	movq	(%rbx), %rdi
	leaq	.LC86(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	(%rbx), %rdi
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	(%r12), %r12
	movq	(%rax), %r14
	movq	%r12, 1351(%r14)
	leaq	1351(%r14), %rsi
	testb	$1, %r12b
	je	.L1234
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1267
	testb	$24, %al
	je	.L1234
.L1274:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1268
	.p2align 4,,10
	.p2align 3
.L1234:
	movq	(%rbx), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1327(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1227
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1228:
	movq	(%rbx), %rdi
	leaq	.LC87(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	(%r12), %r12
	movq	(%rax), %r13
	movq	%r12, 1359(%r13)
	leaq	1359(%r13), %r14
	testb	$1, %r12b
	je	.L1205
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1269
	testb	$24, %al
	je	.L1205
.L1273:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1270
	.p2align 4,,10
	.p2align 3
.L1205:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1271
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1265:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-88(%rbp), %rsi
	testb	$24, %al
	jne	.L1272
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1269:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1273
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1267:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-88(%rbp), %rsi
	testb	$24, %al
	jne	.L1274
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-88(%rbp), %rsi
	testb	$24, %al
	jne	.L1275
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1215:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1276
.L1217:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1227:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1277
.L1229:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1228
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1278
.L1223:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1209:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1279
.L1211:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1206:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1280
.L1208:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1264:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1268:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1270:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1279:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1277:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1276:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1217
.L1271:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21278:
	.size	_ZN2v88internal7Genesis23CreateAsyncFunctionMapsENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal7Genesis23CreateAsyncFunctionMapsENS0_6HandleINS0_10JSFunctionEEE
	.section	.rodata._ZN2v88internal7Genesis17CreateJSProxyMapsEv.str1.1,"aMS",@progbits,1
.LC88:
	.string	"callable Proxy"
.LC89:
	.string	"constructor Proxy"
	.section	.text._ZN2v88internal7Genesis17CreateJSProxyMapsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis17CreateJSProxyMapsEv
	.type	_ZN2v88internal7Genesis17CreateJSProxyMapsEv, @function
_ZN2v88internal7Genesis17CreateJSProxyMapsEv:
.LFB21279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$32, %edx
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$1024, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	(%rax), %rdx
	movq	%rax, %r12
	movl	15(%rdx), %eax
	orl	$35651584, %eax
	movl	%eax, 15(%rdx)
	movq	(%r12), %rdx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	movq	16(%rbx), %rax
	movq	(%r12), %r13
	movq	(%rax), %r14
	movq	%r13, 927(%r14)
	testb	$1, %r13b
	je	.L1333
	movq	%r13, %r15
	leaq	927(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1383
	testb	$24, %al
	je	.L1333
.L1403:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1384
	.p2align 4,,10
	.p2align 3
.L1333:
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	leaq	.LC88(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	orb	$2, 13(%rax)
	movq	16(%rbx), %rax
	movq	(%r12), %r13
	movq	(%rax), %r14
	movq	%r13, 903(%r14)
	leaq	903(%r14), %rsi
	testb	$1, %r13b
	je	.L1332
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1385
	testb	$24, %al
	je	.L1332
.L1402:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1386
	.p2align 4,,10
	.p2align 3
.L1332:
	movq	16(%rbx), %rax
	movq	(%r12), %r13
	movq	(%rax), %rax
	leaq	31(%r13), %rsi
	movq	415(%rax), %r14
	movq	31(%r13), %rax
	testb	$1, %al
	jne	.L1387
.L1288:
	movq	%r14, 31(%r13)
	testb	$1, %r14b
	je	.L1331
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1388
	testb	$24, %al
	je	.L1331
.L1406:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1389
	.p2align 4,,10
	.p2align 3
.L1331:
	movq	(%rbx), %rdi
	leaq	.LC89(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	(%rax), %rdx
	orb	$64, 13(%rdx)
	movq	16(%rbx), %rdx
	movq	(%rax), %r12
	movq	(%rdx), %r13
	movq	%r12, 911(%r13)
	leaq	911(%r13), %r15
	testb	$1, %r12b
	je	.L1330
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L1390
	testb	$24, %al
	je	.L1330
.L1405:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1391
	.p2align 4,,10
	.p2align 3
.L1330:
	movq	(%rbx), %rdi
	movl	$2, %r8d
	movl	$3, %ecx
	movl	$40, %edx
	movl	$1057, %esi
	leaq	-128(%rbp), %r14
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	(%rbx), %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rbx), %rsi
	xorl	%ecx, %ecx
	movl	$4, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	leaq	3112(%rsi), %rdx
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	0(%r13), %r15
	movq	(%rbx), %rax
	movq	%r14, %rsi
	leaq	-136(%rbp), %rdi
	movq	%rax, -152(%rbp)
	movq	39(%r15), %rax
	movq	%rax, -136(%rbp)
	movl	15(%r15), %r12d
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	shrl	$10, %r12d
	andl	$1023, %r12d
	leal	1(%r12), %ecx
	cmpl	$1020, %ecx
	jg	.L1306
	movl	15(%r15), %eax
	movl	%ecx, %edx
	sall	$10, %edx
	andl	$-1047553, %eax
	orl	%edx, %eax
	movl	%eax, 15(%r15)
	movq	-136(%rbp), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L1392
.L1296:
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	je	.L1393
.L1299:
	testb	$2, -104(%rbp)
	je	.L1394
.L1300:
	movq	(%rbx), %rsi
	leaq	-96(%rbp), %r14
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movl	$4, %r9d
	movq	%r14, %rdi
	leaq	3208(%rsi), %rdx
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	0(%r13), %r15
	movq	(%rbx), %rax
	movq	%r14, %rsi
	leaq	-144(%rbp), %rdi
	movq	%rax, -152(%rbp)
	movq	39(%r15), %rax
	movq	%rax, -144(%rbp)
	movl	15(%r15), %r12d
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	shrl	$10, %r12d
	andl	$1023, %r12d
	leal	1(%r12), %ecx
	cmpl	$1020, %ecx
	jg	.L1306
	movl	15(%r15), %eax
	movl	%ecx, %edx
	sall	$10, %edx
	andl	$-1047553, %eax
	orl	%edx, %eax
	movl	%eax, 15(%r15)
	movq	-144(%rbp), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L1395
.L1307:
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	je	.L1396
.L1310:
	testb	$2, -72(%rbp)
	je	.L1397
.L1311:
	movq	(%rbx), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	519(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1317
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1318:
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$1, %ecx
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	0(%r13), %r12
	movq	(%rax), %rax
	leaq	31(%r12), %rsi
	movq	879(%rax), %r14
	movq	31(%r12), %rax
	testb	$1, %al
	jne	.L1398
.L1320:
	movq	%r14, 31(%r12)
	testb	$1, %r14b
	je	.L1329
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	je	.L1323
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-152(%rbp), %rsi
.L1323:
	testb	$24, %al
	je	.L1329
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1399
	.p2align 4,,10
	.p2align 3
.L1329:
	movq	16(%rbx), %rax
	movq	0(%r13), %r12
	movq	(%rax), %r14
	movq	%r12, 935(%r14)
	leaq	935(%r14), %r13
	testb	$1, %r12b
	je	.L1281
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L1326
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L1326:
	testb	$24, %al
	je	.L1281
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1400
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1401
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1392:
	.cfi_restore_state
	movq	-152(%rbp), %rdi
	movq	%r15, %rsi
	addq	$37592, %rdi
	call	_ZN2v88internal41Heap_MarkingBarrierForDescriptorArraySlowEPNS0_4HeapENS0_10HeapObjectES3_i@PLT
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	-152(%rbp), %rdi
	movq	%r15, %rsi
	addq	$37592, %rdi
	call	_ZN2v88internal41Heap_MarkingBarrierForDescriptorArraySlowEPNS0_4HeapENS0_10HeapObjectES3_i@PLT
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1385:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-152(%rbp), %rsi
	testb	$24, %al
	jne	.L1402
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-152(%rbp), %rsi
	testb	$24, %al
	jne	.L1403
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L1404
.L1319:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1390:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	testb	$24, %al
	jne	.L1405
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1388:
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-152(%rbp), %rsi
	testb	$24, %al
	jne	.L1406
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1396:
	testb	$8, 11(%rax)
	je	.L1310
	movl	15(%r15), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%r15)
	testb	$2, -72(%rbp)
	jne	.L1311
.L1397:
	movzbl	9(%r15), %eax
	cmpl	$2, %eax
	jle	.L1312
	movzbl	7(%r15), %edx
	cmpl	%edx, %eax
	je	.L1407
	leal	1(%rax), %edx
	cmpl	$255, %eax
	je	.L1314
	movb	%dl, 9(%r15)
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1387:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L1288
.L1321:
	leaq	.LC36(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1393:
	testb	$8, 11(%rax)
	je	.L1299
	movl	15(%r15), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%r15)
	testb	$2, -104(%rbp)
	jne	.L1300
.L1394:
	movzbl	9(%r15), %eax
	cmpl	$2, %eax
	jle	.L1301
	movzbl	7(%r15), %edx
	cmpl	%edx, %eax
	je	.L1408
	leal	1(%rax), %edx
	cmpl	$255, %eax
	je	.L1314
	movb	%dl, 9(%r15)
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1398:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L1320
	jmp	.L1321
	.p2align 4,,10
	.p2align 3
.L1384:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1386:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1391:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1301:
	leal	-1(%rax), %edx
	testl	%eax, %eax
	movl	$2, %eax
	cmovne	%edx, %eax
	movb	%al, 9(%r15)
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1312:
	leal	-1(%rax), %edx
	testl	%eax, %eax
	movl	$2, %eax
	cmovne	%edx, %eax
	movb	%al, 9(%r15)
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1399:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1400:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1306:
	leaq	.LC55(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1408:
	movb	$2, 9(%r15)
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1407:
	movb	$2, 9(%r15)
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1314:
	leaq	.LC28(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21279:
	.size	_ZN2v88internal7Genesis17CreateJSProxyMapsEv, .-_ZN2v88internal7Genesis17CreateJSProxyMapsEv
	.section	.text._ZN2v88internal7Genesis11CreateRootsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis11CreateRootsEv
	.type	_ZN2v88internal7Genesis11CreateRootsEv, @function
_ZN2v88internal7Genesis11CreateRootsEv:
.LFB21283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	_ZN2v88internal7Factory16NewNativeContextEv@PLT
	movq	(%rbx), %r13
	movq	%rax, 16(%rbx)
	movq	(%rax), %r12
	movq	39120(%r13), %rdx
	movq	%rdx, 1959(%r12)
	testb	$1, %dl
	jne	.L1426
.L1410:
	movq	%r12, 39120(%r13)
	movq	16(%rbx), %rdx
	movl	$1, %esi
	movq	(%rbx), %rax
	movq	(%rdx), %rdx
	movq	%rdx, 12464(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal12TemplateList3NewEPNS0_7IsolateEi@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %r12
	movq	(%rdx), %r13
	movq	%r12, 855(%r13)
	leaq	855(%r13), %r14
	testb	$1, %r12b
	je	.L1409
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1427
	testb	$24, %al
	je	.L1409
.L1429:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1428
.L1409:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1427:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1429
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1410
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1410
	leaq	1959(%r12), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1428:
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE21283:
	.size	_ZN2v88internal7Genesis11CreateRootsEv, .-_ZN2v88internal7Genesis11CreateRootsEv
	.section	.text._ZN2v88internal7Genesis24InstallGlobalThisBindingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis24InstallGlobalThisBindingEv
	.type	_ZN2v88internal7Genesis24InstallGlobalThisBindingEv, @function
_ZN2v88internal7Genesis24InstallGlobalThisBindingEv:
.LFB21284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	movq	1135(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1431
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L1432:
	movq	(%rbx), %r12
	movq	16(%rbx), %rsi
	leaq	1160(%r12), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory16NewScriptContextENS0_6HandleINS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE@PLT
	movq	%rax, %r13
	movq	1160(%r12), %rax
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo24ReceiverContextSlotIndexEv@PLT
	movq	0(%r13), %r8
	movq	%r12, %rdi
	movl	%eax, %r15d
	movq	16(%rbx), %rax
	movq	%r8, -72(%rbp)
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r12
	leal	16(,%r15,8), %eax
	cltq
	leaq	-1(%r8,%rax), %r15
	movq	%r12, (%r15)
	testb	$1, %r12b
	je	.L1441
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L1456
	testb	$24, %al
	je	.L1441
.L1462:
	movq	%r8, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1457
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %r12
	movq	(%rdx), %r13
	movq	%r12, 1135(%r13)
	leaq	1135(%r13), %r14
	testb	$1, %r12b
	je	.L1430
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1458
	testb	$24, %al
	je	.L1430
.L1461:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1459
	.p2align 4,,10
	.p2align 3
.L1430:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1460
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1458:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1461
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1456:
	movq	%r8, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1462
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1431:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L1463
.L1433:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1457:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1463:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1433
.L1460:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21284:
	.size	_ZN2v88internal7Genesis24InstallGlobalThisBindingEv, .-_ZN2v88internal7Genesis24InstallGlobalThisBindingEv
	.section	.text._ZN2v88internal7Genesis16CreateNewGlobalsENS_5LocalINS_14ObjectTemplateEEENS0_6HandleINS0_13JSGlobalProxyEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis16CreateNewGlobalsENS_5LocalINS_14ObjectTemplateEEENS0_6HandleINS0_13JSGlobalProxyEEE
	.type	_ZN2v88internal7Genesis16CreateNewGlobalsENS_5LocalINS_14ObjectTemplateEEENS0_6HandleINS0_13JSGlobalProxyEEE, @function
_ZN2v88internal7Genesis16CreateNewGlobalsENS_5LocalINS_14ObjectTemplateEEENS0_6HandleINS0_13JSGlobalProxyEEE:
.LFB21285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1465
	movq	(%rsi), %rax
	movq	41112(%r12), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1466
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1467:
	movq	71(%rsi), %rax
	andq	$-262144, %rsi
	movq	(%rbx), %r12
	movq	24(%rsi), %rdx
	movq	-37504(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L1469
	movq	7(%rax), %rsi
.L1469:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1470
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rbx), %r12
	movq	(%rax), %rsi
	cmpq	88(%r12), %rsi
	je	.L1465
.L1508:
	movq	41112(%r12), %rdi
	movq	47(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L1540
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1477:
	movq	(%rbx), %r12
	movq	12464(%r12), %rax
	leaq	96(%r12), %rcx
	movq	39(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1479
	movq	%r8, %rsi
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-136(%rbp), %rcx
	movq	%rax, %rsi
.L1480:
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	movl	$1025, %r8d
	movq	%r15, %rdx
	leaq	-128(%rbp), %r12
	call	_ZN2v88internal10ApiNatives17CreateApiFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS4_INS0_6ObjectEEENS0_12InstanceTypeENS0_11MaybeHandleINS0_4NameEEE@PLT
	movq	%rax, %rsi
.L1476:
	movq	(%rsi), %rax
	movq	55(%rax), %rdx
	movl	15(%rdx), %eax
	orl	$1048576, %eax
	movl	%eax, 15(%rdx)
	movq	(%rsi), %rax
	movq	55(%rax), %rdx
	movl	15(%rdx), %eax
	orl	$35651584, %eax
	movl	%eax, 15(%rdx)
	movq	(%rsi), %rax
	movq	55(%rax), %rdx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Factory17NewJSGlobalObjectENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	%rax, %r15
	testq	%r14, %r14
	je	.L1541
	movq	(%rbx), %rcx
	movq	(%r14), %rax
	movq	41112(%rcx), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1484
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1485:
	movq	(%rbx), %r14
	movq	12464(%r14), %rax
	leaq	96(%r14), %rcx
	movq	39(%rax), %r8
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1487
	movq	%r8, %rsi
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rcx
	movq	%rax, %rsi
.L1488:
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	movl	$1026, %r8d
	call	_ZN2v88internal10ApiNatives17CreateApiFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS4_INS0_6ObjectEEENS0_12InstanceTypeENS0_11MaybeHandleINS0_4NameEEE@PLT
	movq	%rax, %r14
.L1483:
	movq	(%r14), %rax
	movq	55(%rax), %rax
	orb	$32, 13(%rax)
	movq	(%r14), %rax
	movq	55(%rax), %rdx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	movq	16(%rbx), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rdi
	movq	%rdx, 1639(%rdi)
	leaq	1639(%rdi), %rsi
	testb	$1, %dl
	je	.L1505
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	je	.L1491
	movq	%rdx, -160(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
.L1491:
	testb	$24, %al
	je	.L1505
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1542
	.p2align 4,,10
	.p2align 3
.L1505:
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal7Factory25ReinitializeJSGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEENS2_INS0_10JSFunctionEEE@PLT
	movq	16(%rbx), %rax
	movq	(%r15), %rdi
	movq	(%rax), %r14
	leaq	23(%rdi), %rsi
	movq	%r14, 23(%rdi)
	testb	$1, %r14b
	je	.L1504
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	je	.L1494
	movq	%r14, %rdx
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
.L1494:
	testb	$24, %al
	je	.L1504
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1543
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	(%r15), %rdi
	movq	0(%r13), %r14
	movq	%r14, 31(%rdi)
	leaq	31(%rdi), %rsi
	testb	$1, %r14b
	je	.L1503
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	je	.L1497
	movq	%r14, %rdx
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
.L1497:
	testb	$24, %al
	je	.L1503
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1544
	.p2align 4,,10
	.p2align 3
.L1503:
	movq	16(%rbx), %rax
	movq	0(%r13), %rdi
	movq	(%rax), %r14
	leaq	23(%rdi), %rsi
	movq	%r14, 23(%rdi)
	testb	$1, %r14b
	je	.L1502
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	je	.L1500
	movq	%r14, %rdx
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
.L1500:
	testb	$24, %al
	je	.L1502
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1545
	.p2align 4,,10
	.p2align 3
.L1502:
	movq	16(%rbx), %rax
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal7Context16set_global_proxyENS0_13JSGlobalProxyE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1546
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1466:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	je	.L1547
.L1468:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1540:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L1548
.L1478:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1487:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L1549
.L1489:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r8, (%rsi)
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L1550
.L1486:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1479:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1551
.L1481:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1470:
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	je	.L1552
.L1472:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	(%rbx), %r12
	cmpq	88(%r12), %rsi
	jne	.L1508
.L1465:
	movq	12464(%r12), %rax
	leaq	128(%r12), %r15
	movq	39(%rax), %rax
	movq	879(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1473
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1474:
	movq	%r12, %rdi
	leaq	-128(%rbp), %r12
	call	_ZN2v88internal7Factory20NewFunctionPrototypeENS0_6HandleINS0_10JSFunctionEEE@PLT
	pushq	$0
	movl	$1025, %ecx
	xorl	%r9d, %r9d
	pushq	$166
	movq	%rax, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$40, %r8d
	call	_ZN2v88internal15NewFunctionArgs23ForBuiltinWithPrototypeENS0_6HandleINS0_6StringEEENS2_INS0_10HeapObjectEEENS0_12InstanceTypeEiiiNS0_11MutableModeE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1545:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1544:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1504
	.p2align 4,,10
	.p2align 3
.L1542:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1505
	.p2align 4,,10
	.p2align 3
.L1541:
	pushq	$0
	movq	(%rbx), %rdx
	xorl	%r9d, %r9d
	movl	$32, %r8d
	pushq	$166
	movl	$1026, %ecx
	movq	%r12, %rdi
	leaq	128(%rdx), %rsi
	addq	$96, %rdx
	call	_ZN2v88internal15NewFunctionArgs23ForBuiltinWithPrototypeENS0_6HandleINS0_6StringEEENS2_INS0_10HeapObjectEEENS0_12InstanceTypeEiiiNS0_11MutableModeE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
	jmp	.L1483
	.p2align 4,,10
	.p2align 3
.L1473:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1553
.L1475:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1474
	.p2align 4,,10
	.p2align 3
.L1551:
	movq	%r12, %rdi
	movq	%rcx, -144(%rbp)
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1481
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	%rcx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	%r14, %rdi
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rcx
	movq	-144(%rbp), %r8
	movq	-136(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1548:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1547:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	%r12, %rdi
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1475
.L1546:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21285:
	.size	_ZN2v88internal7Genesis16CreateNewGlobalsENS_5LocalINS_14ObjectTemplateEEENS0_6HandleINS0_13JSGlobalProxyEEE, .-_ZN2v88internal7Genesis16CreateNewGlobalsENS_5LocalINS_14ObjectTemplateEEENS0_6HandleINS0_13JSGlobalProxyEEE
	.section	.text._ZN2v88internal7Genesis17HookUpGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis17HookUpGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEE
	.type	_ZN2v88internal7Genesis17HookUpGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEE, @function
_ZN2v88internal7Genesis17HookUpGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEE:
.LFB21286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	movq	1639(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1555
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1556:
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal7Factory25ReinitializeJSGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEENS2_INS0_10JSFunctionEEE@PLT
	movq	16(%rbx), %rax
	movq	(%rbx), %r14
	leaq	-48(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L1558
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1559:
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	16(%rbx), %rax
	movq	(%r12), %r13
	movq	(%rax), %r12
	leaq	23(%r13), %r14
	movq	%r12, 23(%r13)
	testb	$1, %r12b
	je	.L1554
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1573
	testb	$24, %al
	je	.L1554
.L1576:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1574
	.p2align 4,,10
	.p2align 3
.L1554:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1575
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1573:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1576
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1558:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L1577
.L1560:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r13, (%rsi)
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L1578
.L1557:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L1556
	.p2align 4,,10
	.p2align 3
.L1574:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1578:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1577:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1560
.L1575:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21286:
	.size	_ZN2v88internal7Genesis17HookUpGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEE, .-_ZN2v88internal7Genesis17HookUpGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEE
	.section	.rodata._ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE.str1.8,"aMS",@progbits,1
	.align 8
.LC90:
	.string	"static_cast<int>(elements_kind) < kElementsKindCount"
	.align 8
.LC91:
	.string	"JSObject::SetPrototype(result, typed_array_function, false, kDontThrow) .FromJust()"
	.section	.rodata._ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE.str1.1,"aMS",@progbits,1
.LC92:
	.string	"BYTES_PER_ELEMENT"
	.section	.rodata._ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE.str1.8
	.align 8
.LC93:
	.string	"JSObject::SetPrototype(prototype, typed_array_prototype, false, kDontThrow) .FromJust()"
	.section	.text._ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE
	.type	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE, @function
_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE:
.LFB21292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	leaq	-64(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1580
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L1581:
	movq	(%rbx), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1535(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1583
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r10
	movq	%rax, %r13
.L1584:
	movq	(%rbx), %rdx
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	1527(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1586
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r10
	movq	%rax, %r14
.L1587:
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movl	$88, %r8d
	movq	%r12, %rdx
	pushq	$613
	movq	%r10, %rsi
	movl	$1086, %ecx
	leaq	96(%rdi), %r9
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	popq	%rsi
	movzbl	%r15b, %r8d
	popq	%rdi
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	55(%rax), %rax
	cmpb	$27, %r15b
	ja	.L1610
	movzbl	14(%rax), %edx
	movl	%r8d, -72(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leal	0(,%r8,8), %ecx
	andb	$7, %dl
	orl	%ecx, %edx
	movl	$3, %ecx
	movb	%dl, 14(%rax)
	movq	(%r12), %rax
	movl	$-1, %edx
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	(%r12), %rax
	xorl	%edx, %edx
	movq	23(%rax), %rax
	movw	%cx, 39(%rax)
	movl	$1, %ecx
	call	_ZN2v88internal8JSObject12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE@PLT
	movl	-72(%rbp), %r8d
	testb	%al, %al
	je	.L1611
.L1590:
	shrw	$8, %ax
	je	.L1612
	movq	(%rbx), %r14
	movl	%r8d, %edi
	call	_ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE@PLT
	movl	$1, %esi
	movl	%eax, %ecx
	movq	41112(%r14), %rdi
	sall	%cl, %esi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L1592
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1593:
	movq	(%rbx), %rdi
	movq	%r15, %rcx
	leaq	.LC92(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	(%r12), %rax
	movq	(%rbx), %rdx
	movq	-1(%rax), %rcx
	movzbl	13(%rcx), %ecx
	andl	$1, %ecx
	je	.L1595
	movq	-1(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L1597
.L1598:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1601
.L1620:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L1602:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE@PLT
	testb	%al, %al
	je	.L1613
.L1604:
	shrw	$8, %ax
	je	.L1614
	movq	(%rbx), %rdi
	movq	%r15, %rcx
	leaq	.LC92(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1615
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1592:
	.cfi_restore_state
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L1616
.L1594:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1580:
	movq	41088(%r13), %r10
	cmpq	41096(%r13), %r10
	je	.L1617
.L1582:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r10)
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L1586:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L1618
.L1588:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1583:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L1619
.L1585:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1595:
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L1598
	movq	41112(%rdx), %rdi
	movq	23(%rsi), %rsi
	testq	%rdi, %rdi
	jne	.L1620
.L1601:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L1621
.L1603:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L1598
	movq	31(%rsi), %rsi
	testb	$1, %sil
	je	.L1598
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1610:
	leaq	.LC90(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1611:
	movl	%eax, -80(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-80(%rbp), %eax
	movl	-72(%rbp), %r8d
	jmp	.L1590
	.p2align 4,,10
	.p2align 3
.L1612:
	leaq	.LC91(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1619:
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %r10
	movq	%rax, %r13
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1618:
	movq	%rdx, %rdi
	movq	%rsi, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L1617:
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1614:
	leaq	.LC93(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1613:
	movl	%eax, -72(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-72(%rbp), %eax
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1621:
	movq	%rdx, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1594
.L1615:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21292:
	.size	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE, .-_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE
	.section	.text._ZN2v88internal7Genesis28InitializeExperimentalGlobalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis28InitializeExperimentalGlobalEv
	.type	_ZN2v88internal7Genesis28InitializeExperimentalGlobalEv, @function
_ZN2v88internal7Genesis28InitializeExperimentalGlobalEv:
.LFB21293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, _ZN2v88internal22FLAG_harmony_weak_refsE(%rip)
	je	.L1623
	call	_ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv.part.0
.L1623:
	cmpb	$0, _ZN2v88internal27FLAG_harmony_intl_segmenterE(%rip)
	je	.L1624
	movq	%r12, %rdi
	call	_ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv.part.0
.L1624:
	cmpb	$0, _ZN2v88internal30FLAG_harmony_sharedarraybufferE(%rip)
	je	.L1625
	movq	%r12, %rdi
	call	_ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv.part.0
.L1625:
	cmpb	$0, _ZN2v88internal32FLAG_harmony_promise_all_settledE(%rip)
	je	.L1626
	movq	%r12, %rdi
	call	_ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv.part.0
.L1626:
	cmpb	$0, _ZN2v88internal35FLAG_harmony_intl_date_format_rangeE(%rip)
	jne	.L1629
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1629:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv.part.0
	.cfi_endproc
.LFE21293:
	.size	_ZN2v88internal7Genesis28InitializeExperimentalGlobalEv, .-_ZN2v88internal7Genesis28InitializeExperimentalGlobalEv
	.section	.text._ZN2v88internal12Bootstrapper13CompileNativeEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_6StringEEEiPNS7_INS0_6ObjectEEENS0_11NativesFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper13CompileNativeEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_6StringEEEiPNS7_INS0_6ObjectEEENS0_11NativesFlagE
	.type	_ZN2v88internal12Bootstrapper13CompileNativeEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_6StringEEEiPNS7_INS0_6ObjectEEENS0_11NativesFlagE, @function
_ZN2v88internal12Bootstrapper13CompileNativeEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_6StringEEEiPNS7_INS0_6ObjectEEENS0_11NativesFlagE:
.LFB21295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -112(%rbp)
	movq	41472(%rdi), %rbx
	movq	%rdx, -104(%rbp)
	movl	%r8d, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	10(%rbx), %eax
	movb	$1, 10(%rbx)
	movq	12464(%rdi), %rsi
	movq	41112(%rdi), %rdi
	movb	%al, -113(%rbp)
	testq	%rdi, %rdi
	je	.L1631
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L1632:
	xorl	%edx, %edx
	leaq	-112(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1640
	movq	%rax, -88(%rbp)
	movl	16(%rbp), %eax
	subq	$8, %rsp
	movq	%r13, %rsi
	leaq	-96(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%rax
	movq	%r14, %rdi
	pushq	$0
	pushq	$0
	movq	$0, -96(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagE@PLT
	addq	$32, %rsp
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1638
	movq	%r12, %rdx
	movl	$1, %ecx
	leaq	88(%r14), %r12
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	pushq	$0
	movq	%rax, %rsi
	movl	$1, %r9d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	call	_ZN2v88internal9Execution7TryCallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_NS1_15MessageHandlingEPNS0_11MaybeHandleIS5_EE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1638
	subq	$8, %rsp
	movl	-120(%rbp), %ecx
	movq	%r12, %rdx
	movl	$1, %r9d
	pushq	$0
	movq	%r15, %r8
	movq	%r14, %rdi
	call	_ZN2v88internal9Execution7TryCallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_NS1_15MessageHandlingEPNS0_11MaybeHandleIS5_EE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	setne	%al
.L1635:
	movzbl	-113(%rbp), %ecx
	movb	%cl, 10(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1641
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1631:
	.cfi_restore_state
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L1642
.L1633:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1638:
	xorl	%eax, %eax
	jmp	.L1635
	.p2align 4,,10
	.p2align 3
.L1640:
	leaq	.LC23(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1642:
	movq	%r14, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1633
.L1641:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21295:
	.size	_ZN2v88internal12Bootstrapper13CompileNativeEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_6StringEEEiPNS7_INS0_6ObjectEEENS0_11NativesFlagE, .-_ZN2v88internal12Bootstrapper13CompileNativeEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_6StringEEEiPNS7_INS0_6ObjectEEENS0_11NativesFlagE
	.section	.text._ZN2v88internal12Bootstrapper19CompileExtraBuiltinEPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper19CompileExtraBuiltinEPNS0_7IsolateEi
	.type	_ZN2v88internal12Bootstrapper19CompileExtraBuiltinEPNS0_7IsolateEi, @function
_ZN2v88internal12Bootstrapper19CompileExtraBuiltinEPNS0_7IsolateEi:
.LFB21294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$88, %rsp
	movq	41096(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdi)
	movq	41088(%rdi), %rax
	movl	%esi, %edi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE13GetScriptNameEi@PLT
	movq	40936(%r14), %rcx
	movl	$32, %edi
	movq	%rdx, -120(%rbp)
	movq	%rax, %r15
	movq	%rcx, -104(%rbp)
	call	_Znwm@PLT
	movl	%ebx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal29NativesExternalStringResourceC1ENS0_10NativeTypeEi@PLT
	movq	-104(%rbp), %rcx
	movq	%r12, %rsi
	movq	(%rcx), %rdi
	call	_ZN2v88internal7Factory21NewNativeSourceStringEPKNS_6String29ExternalOneByteStringResourceE@PLT
	leaq	-88(%rbp), %rdi
	movq	%rax, %r12
	movq	12464(%r14), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1644
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L1645:
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	367(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1647
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1648:
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	375(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1650
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %rdx
.L1651:
	subq	$8, %rsp
	movq	%rdx, %xmm1
	movq	-120(%rbp), %rdx
	movq	%r12, %rcx
	pushq	$1
	movq	%rbx, %xmm0
	leaq	-80(%rbp), %r9
	movq	%r15, %rsi
	punpcklqdq	%xmm1, %xmm0
	movl	$3, %r8d
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal12Bootstrapper13CompileNativeEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_6StringEEEiPNS7_INS0_6ObjectEEENS0_11NativesFlagE
	subl	$1, 41104(%r14)
	movl	%eax, %r12d
	movq	-112(%rbp), %rax
	movq	%rax, 41088(%r14)
	popq	%rax
	popq	%rdx
	cmpq	41096(%r14), %r13
	je	.L1654
	movq	%r13, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1654:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1658
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1650:
	.cfi_restore_state
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1659
.L1652:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1647:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L1660
.L1649:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	41088(%r14), %rbx
	cmpq	41096(%r14), %rbx
	je	.L1661
.L1646:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rbx)
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1659:
	movq	%r14, %rdi
	movq	%rsi, -128(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	-104(%rbp), %rdx
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1661:
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1660:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1649
.L1658:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21294:
	.size	_ZN2v88internal12Bootstrapper19CompileExtraBuiltinEPNS0_7IsolateEi, .-_ZN2v88internal12Bootstrapper19CompileExtraBuiltinEPNS0_7IsolateEi
	.section	.text._ZN2v88internal7Genesis16CompileExtensionEPNS0_7IsolateEPNS_9ExtensionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis16CompileExtensionEPNS0_7IsolateEPNS_9ExtensionE
	.type	_ZN2v88internal7Genesis16CompileExtensionEPNS0_7IsolateEPNS_9ExtensionE, @function
_ZN2v88internal7Genesis16CompileExtensionEPNS0_7IsolateEPNS_9ExtensionE:
.LFB21296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdi)
	movq	24(%rsi), %rsi
	movq	41088(%rdi), %rax
	movq	$0, -120(%rbp)
	movq	%rax, -144(%rbp)
	movq	41096(%rdi), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal7Factory28NewExternalStringFromOneByteEPKNS_6String29ExternalOneByteStringResourceE@PLT
	testq	%rax, %rax
	je	.L1668
	movq	8(%rbx), %r13
	movq	%rax, %r12
	movq	%r13, %rdi
	call	strlen@PLT
	movq	41112(%r15), %rdi
	movq	%r13, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	40936(%r15), %rax
	movq	12464(%r15), %rsi
	leaq	16(%rax), %r14
	testq	%rdi, %rdi
	je	.L1664
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L1665:
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rcx
	leaq	-120(%rbp), %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal15SourceCodeCache6LookupEPNS0_7IsolateENS0_6VectorIKcEEPNS0_6HandleINS0_18SharedFunctionInfoEEE
	testb	%al, %al
	je	.L1681
	leaq	-96(%rbp), %rax
	movq	%rax, -152(%rbp)
.L1667:
	movq	-120(%rbp), %rsi
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE@PLT
	movq	-152(%rbp), %rdi
	movq	%rax, %r12
	movq	12464(%r15), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1671
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1672:
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$1, %r9d
	pushq	$0
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal9Execution7TryCallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_NS1_15MessageHandlingEPNS0_11MaybeHandleIS5_EE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	setne	%r11b
.L1670:
	movq	-144(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-136(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L1662
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	movb	%r11b, -144(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movzbl	-144(%rbp), %r11d
.L1662:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1683
	leaq	-40(%rbp), %rsp
	movl	%r11d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1664:
	.cfi_restore_state
	movq	41088(%r15), %r13
	cmpq	%r13, 41096(%r15)
	je	.L1684
.L1666:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1681:
	xorl	%edx, %edx
	leaq	-112(%rbp), %rsi
	movq	%r15, %rdi
	movb	%al, -153(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1668
	subq	$8, %rsp
	movq	%rax, -88(%rbp)
	leaq	-96(%rbp), %rax
	xorl	%r9d, %r9d
	pushq	$1
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	pushq	$9
	movq	%r12, %rsi
	movq	%r15, %rdi
	pushq	$0
	movq	$0, -96(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagE@PLT
	addq	$32, %rsp
	movzbl	-153(%rbp), %r11d
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1685
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal15SourceCodeCache3AddEPNS0_7IsolateENS0_6VectorIKcEENS0_6HandleINS0_18SharedFunctionInfoEEE
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1671:
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L1686
.L1673:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1668:
	leaq	.LC23(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1684:
	movq	%r15, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1666
	.p2align 4,,10
	.p2align 3
.L1685:
	movq	$0, -120(%rbp)
	jmp	.L1670
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	%r15, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1673
.L1683:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21296:
	.size	_ZN2v88internal7Genesis16CompileExtensionEPNS0_7IsolateEPNS_9ExtensionE, .-_ZN2v88internal7Genesis16CompileExtensionEPNS0_7IsolateEPNS_9ExtensionE
	.section	.text._ZN2v88internal7Genesis20ConfigureUtilsObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis20ConfigureUtilsObjectEv
	.type	_ZN2v88internal7Genesis20ConfigureUtilsObjectEv, @function
_ZN2v88internal7Genesis20ConfigureUtilsObjectEv:
.LFB21297:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpb	$0, 41456(%rax)
	je	.L1704
	ret
	.p2align 4,,10
	.p2align 3
.L1704:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	88(%rax), %r12
	movq	16(%rdi), %rax
	movq	(%rax), %r13
	movq	%r12, 375(%r13)
	leaq	375(%r13), %r14
	testb	$1, %r12b
	je	.L1687
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1705
.L1691:
	testb	$24, %al
	je	.L1687
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1706
.L1687:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1705:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L1691
	.p2align 4,,10
	.p2align 3
.L1706:
	popq	%rbx
	.cfi_restore 3
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	.cfi_restore 12
	movq	%r13, %rdi
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE21297:
	.size	_ZN2v88internal7Genesis20ConfigureUtilsObjectEv, .-_ZN2v88internal7Genesis20ConfigureUtilsObjectEv
	.section	.rodata._ZN2v88internal7Genesis27InitializeIteratorFunctionsEv.str1.1,"aMS",@progbits,1
.LC94:
	.string	"SetIterator"
	.section	.rodata._ZN2v88internal7Genesis27InitializeIteratorFunctionsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC95:
	.string	"JS_SET_KEY_VALUE_ITERATOR_TYPE"
	.section	.rodata._ZN2v88internal7Genesis27InitializeIteratorFunctionsEv.str1.1
.LC96:
	.string	"MapIterator"
	.section	.rodata._ZN2v88internal7Genesis27InitializeIteratorFunctionsEv.str1.8
	.align 8
.LC97:
	.string	"JS_MAP_KEY_VALUE_ITERATOR_TYPE"
	.section	.rodata._ZN2v88internal7Genesis27InitializeIteratorFunctionsEv.str1.1
.LC98:
	.string	"JS_MAP_VALUE_ITERATOR_TYPE"
	.section	.text._ZN2v88internal7Genesis27InitializeIteratorFunctionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis27InitializeIteratorFunctionsEv
	.type	_ZN2v88internal7Genesis27InitializeIteratorFunctionsEv, @function
_ZN2v88internal7Genesis27InitializeIteratorFunctionsEv:
.LFB21298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	%rax, -88(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -96(%rbp)
	movq	12464(%r15), %rax
	movq	39(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1708
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L1709:
	movq	495(%rsi), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1711
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -72(%rbp)
.L1712:
	movq	(%rbx), %rax
	movq	%r15, %rsi
	movq	1367(%rax), %rax
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE@PLT
	movq	41112(%r15), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1714
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L1715:
	movq	%r15, %rdi
	movq	%r12, %r8
	movl	$64, %ecx
	movl	$1105, %edx
	leaq	.LC70(%rip), %rsi
	movl	$352, %r9d
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateEPKcNS0_12InstanceTypeEiiNS0_6HandleINS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movq	(%rax), %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	1367(%rax), %r14
	leaq	55(%rdi), %rsi
	movq	%r14, 55(%rdi)
	testb	$1, %r14b
	je	.L1812
	movq	%r14, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -104(%rbp)
	testl	$262144, %eax
	jne	.L1911
	testb	$24, %al
	je	.L1812
.L1933:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1912
	.p2align 4,,10
	.p2align 3
.L1812:
	movq	0(%r13), %rax
	movl	$-1, %r11d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$1, %r14d
	movl	$51, %edx
	movq	23(%rax), %rax
	movw	%r11w, 41(%rax)
	movq	0(%r13), %rax
	movq	23(%rax), %rax
	movw	%r14w, 39(%rax)
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	415(%rax), %r14
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1720
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1721:
	movq	%r13, %rdi
	leaq	2304(%r15), %r14
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movl	$3, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rax
	movq	1367(%rax), %r12
	movq	0(%r13), %rdx
	movq	31(%r12), %rax
	leaq	31(%r12), %r13
	testb	$1, %al
	jne	.L1913
.L1723:
	movq	%rdx, 31(%r12)
	testb	$1, %dl
	je	.L1811
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	testl	$262144, %eax
	jne	.L1914
	testb	$24, %al
	je	.L1811
.L1934:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1915
	.p2align 4,,10
	.p2align 3
.L1811:
	movq	(%rbx), %rax
	movq	%r15, %rsi
	movq	1399(%rax), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE@PLT
	movq	41112(%r15), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1727
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L1728:
	movl	$1105, %edx
	movq	%r15, %rdi
	movq	%r12, %r8
	movl	$64, %ecx
	leaq	.LC79(%rip), %rsi
	movl	$679, %r9d
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateEPKcNS0_12InstanceTypeEiiNS0_6HandleINS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movq	(%rax), %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	1399(%rax), %rdx
	leaq	55(%rdi), %rsi
	movq	%rdx, 55(%rdi)
	testb	$1, %dl
	je	.L1810
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -104(%rbp)
	testl	$262144, %eax
	je	.L1731
	movq	%rdx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %r8
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	8(%r8), %rax
.L1731:
	testb	$24, %al
	je	.L1810
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1916
	.p2align 4,,10
	.p2align 3
.L1810:
	movq	0(%r13), %rax
	movl	$-1, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$1, %r10d
	movl	$18, %edx
	movq	23(%rax), %rax
	movw	%r9w, 41(%rax)
	movq	0(%r13), %rax
	movq	23(%rax), %rax
	movw	%r10w, 39(%rax)
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	415(%rax), %r8
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1733
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1734:
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r13, %rcx
	movl	$3, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rax
	movq	1399(%rax), %r12
	movq	0(%r13), %rdx
	movq	31(%r12), %rax
	leaq	31(%r12), %rsi
	testb	$1, %al
	jne	.L1917
.L1736:
	movq	%rdx, 31(%r12)
	testb	$1, %dl
	je	.L1809
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	testl	$262144, %eax
	je	.L1739
	movq	%r12, %rdi
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	8(%rcx), %rax
.L1739:
	testb	$24, %al
	je	.L1809
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1918
	.p2align 4,,10
	.p2align 3
.L1809:
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r12
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1741
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1742:
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	leaq	3280(%r15), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	leaq	.LC44(%rip), %rdx
	movl	$571, %ecx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	movq	(%rbx), %rdi
	movq	(%r12), %rdx
	movq	%rdx, 527(%rdi)
	leaq	527(%rdi), %rsi
	testb	$1, %dl
	je	.L1808
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	testl	$262144, %eax
	je	.L1745
	movq	%rdx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	8(%rcx), %rax
.L1745:
	testb	$24, %al
	je	.L1808
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1919
	.p2align 4,,10
	.p2align 3
.L1808:
	movq	%r12, %r8
	movl	$40, %ecx
	movl	$1079, %edx
	movq	%r15, %rdi
	movl	$166, %r9d
	leaq	.LC94(%rip), %rsi
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateEPKcNS0_12InstanceTypeEiiNS0_6HandleINS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movq	(%rax), %rdx
	movq	23(%rdx), %rcx
	movl	47(%rcx), %edx
	andl	$-33, %edx
	movl	%edx, 47(%rcx)
	movq	(%rax), %rax
	movq	41112(%r15), %rdi
	movq	55(%rax), %r12
	testq	%rdi, %rdi
	je	.L1747
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %r8
.L1748:
	movq	(%rbx), %rdi
	movq	%r12, 1167(%rdi)
	leaq	1167(%rdi), %rsi
	testb	$1, %r12b
	je	.L1807
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	testl	$262144, %eax
	je	.L1751
	movq	%r12, %rdx
	movq	%r8, -128(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	8(%rcx), %rax
.L1751:
	testb	$24, %al
	je	.L1807
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1920
	.p2align 4,,10
	.p2align 3
.L1807:
	movq	%r8, %rsi
	leaq	.LC95(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movl	$1078, %r8d
	movq	(%rax), %rdx
	movw	%r8w, 11(%rdx)
	movq	(%rbx), %rdi
	movq	(%rax), %r12
	leaq	1175(%rdi), %rsi
	movq	%r12, 1175(%rdi)
	testb	$1, %r12b
	je	.L1806
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	testl	$262144, %eax
	je	.L1754
	movq	%r12, %rdx
	movq	%rsi, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	8(%rcx), %rax
.L1754:
	testb	$24, %al
	je	.L1806
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1921
	.p2align 4,,10
	.p2align 3
.L1806:
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r12
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1756
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1757:
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	leaq	2816(%r15), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	$407, %ecx
	leaq	.LC44(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	movq	(%rbx), %rdi
	movq	(%r12), %r13
	movq	%r13, 503(%rdi)
	leaq	503(%rdi), %rsi
	testb	$1, %r13b
	je	.L1805
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	je	.L1760
	movq	%r13, %rdx
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%rcx), %rax
.L1760:
	testb	$24, %al
	je	.L1805
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1922
	.p2align 4,,10
	.p2align 3
.L1805:
	movl	$40, %ecx
	movl	$1070, %edx
	movq	%r15, %rdi
	movq	%r12, %r8
	movl	$166, %r9d
	leaq	.LC96(%rip), %rsi
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateEPKcNS0_12InstanceTypeEiiNS0_6HandleINS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movq	(%rax), %rdx
	movq	23(%rdx), %rcx
	movl	47(%rcx), %edx
	andl	$-33, %edx
	movl	%edx, 47(%rcx)
	movq	(%rax), %rax
	movq	41112(%r15), %rdi
	movq	55(%rax), %r13
	testq	%rdi, %rdi
	je	.L1762
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %r12
.L1763:
	movq	(%rbx), %rdi
	movq	%r13, 807(%rdi)
	leaq	807(%rdi), %rsi
	testb	$1, %r13b
	je	.L1804
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	je	.L1766
	movq	%r13, %rdx
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%rcx), %rax
.L1766:
	testb	$24, %al
	je	.L1804
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1923
	.p2align 4,,10
	.p2align 3
.L1804:
	leaq	.LC97(%rip), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movl	$1071, %edi
	movq	(%rax), %rdx
	movw	%di, 11(%rdx)
	movq	(%rbx), %rdi
	movq	(%rax), %r13
	leaq	815(%rdi), %rsi
	movq	%r13, 815(%rdi)
	testb	$1, %r13b
	je	.L1803
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	je	.L1769
	movq	%r13, %rdx
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%rcx), %rax
.L1769:
	testb	$24, %al
	je	.L1803
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1924
	.p2align 4,,10
	.p2align 3
.L1803:
	movq	%r12, %rsi
	leaq	.LC98(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movl	$1072, %esi
	movq	(%rax), %rdx
	movw	%si, 11(%rdx)
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	823(%r13), %rsi
	movq	%r12, 823(%r13)
	testb	$1, %r12b
	je	.L1802
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	je	.L1772
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	8(%rcx), %rax
.L1772:
	testb	$24, %al
	je	.L1802
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1925
	.p2align 4,,10
	.p2align 3
.L1802:
	movq	(%rbx), %rax
	movq	%r15, %rsi
	movq	1335(%rax), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE@PLT
	movq	41112(%r15), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1774
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L1775:
	movl	$1105, %edx
	movq	%r15, %rdi
	movq	%r12, %r8
	movl	$64, %ecx
	leaq	.LC84(%rip), %rsi
	movl	$356, %r9d
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateEPKcNS0_12InstanceTypeEiiNS0_6HandleINS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movq	(%rax), %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	1335(%rax), %rdx
	leaq	55(%rdi), %rsi
	movq	%rdx, 55(%rdi)
	testb	$1, %dl
	je	.L1801
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	je	.L1778
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
.L1778:
	testb	$24, %al
	je	.L1801
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1926
	.p2align 4,,10
	.p2align 3
.L1801:
	movq	0(%r13), %rax
	movl	$-1, %edx
	movl	$1, %ecx
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	0(%r13), %rax
	movq	23(%rax), %rax
	movw	%cx, 39(%rax)
	movq	(%rbx), %rdi
	movq	0(%r13), %rdx
	leaq	143(%rdi), %rsi
	movq	%rdx, 143(%rdi)
	testb	$1, %dl
	je	.L1800
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	je	.L1781
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
.L1781:
	testb	$24, %al
	je	.L1800
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1927
	.p2align 4,,10
	.p2align 3
.L1800:
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	415(%rax), %r8
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1783
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1784:
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movl	$3, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movl	$1063, %esi
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	$3, %ecx
	movl	$88, %edx
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	151(%r13), %rsi
	movq	%r12, 151(%r13)
	testb	$1, %r12b
	je	.L1799
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L1787
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-72(%rbp), %rsi
.L1787:
	testb	$24, %al
	je	.L1799
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1928
	.p2align 4,,10
	.p2align 3
.L1799:
	leaq	128(%r15), %r14
	movl	$232, %esi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	127(%r13), %rsi
	movq	%r12, 127(%r13)
	testb	$1, %r12b
	je	.L1798
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	je	.L1790
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
.L1790:
	testb	$24, %al
	je	.L1798
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1929
	.p2align 4,,10
	.p2align 3
.L1798:
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movl	$233, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	135(%r13), %r14
	movq	%r12, 135(%r13)
	testb	$1, %r12b
	je	.L1795
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L1793
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L1793:
	testb	$24, %al
	je	.L1795
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1930
	.p2align 4,,10
	.p2align 3
.L1795:
	movq	-88(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-96(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L1707
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1707:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1931
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1727:
	.cfi_restore_state
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L1932
.L1729:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	jmp	.L1728
	.p2align 4,,10
	.p2align 3
.L1911:
	movq	%r14, %rdx
	movq	%rsi, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %r8
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	8(%r8), %rax
	testb	$24, %al
	jne	.L1933
	jmp	.L1812
	.p2align 4,,10
	.p2align 3
.L1914:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1934
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1708:
	movq	-88(%rbp), %rax
	movq	%rax, %rbx
	cmpq	41096(%r15), %rax
	je	.L1935
.L1710:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L1709
	.p2align 4,,10
	.p2align 3
.L1720:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1936
.L1722:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r14, (%rsi)
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1714:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L1937
.L1716:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	jmp	.L1715
	.p2align 4,,10
	.p2align 3
.L1711:
	movq	41088(%r15), %rax
	movq	%rax, -72(%rbp)
	cmpq	41096(%r15), %rax
	je	.L1938
.L1713:
	movq	-72(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L1712
	.p2align 4,,10
	.p2align 3
.L1733:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1939
.L1735:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r8, (%rsi)
	jmp	.L1734
	.p2align 4,,10
	.p2align 3
.L1783:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1940
.L1785:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r8, (%rsi)
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1741:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1941
.L1743:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r12, (%rsi)
	jmp	.L1742
	.p2align 4,,10
	.p2align 3
.L1774:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L1942
.L1776:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L1762:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L1943
.L1764:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%r12)
	jmp	.L1763
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1944
.L1758:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r12, (%rsi)
	jmp	.L1757
	.p2align 4,,10
	.p2align 3
.L1747:
	movq	41088(%r15), %r8
	cmpq	41096(%r15), %r8
	je	.L1945
.L1749:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r15)
	movq	%r12, (%r8)
	jmp	.L1748
	.p2align 4,,10
	.p2align 3
.L1913:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L1723
.L1737:
	leaq	.LC36(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1917:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L1736
	jmp	.L1737
	.p2align 4,,10
	.p2align 3
.L1912:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1812
	.p2align 4,,10
	.p2align 3
.L1916:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1915:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1919:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1918:
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1809
	.p2align 4,,10
	.p2align 3
.L1923:
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1922:
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1805
	.p2align 4,,10
	.p2align 3
.L1921:
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1920:
	movq	%r12, %rdx
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %r8
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1930:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1928:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1927:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1926:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1925:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1802
	.p2align 4,,10
	.p2align 3
.L1924:
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	%r15, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, -72(%rbp)
	jmp	.L1713
	.p2align 4,,10
	.p2align 3
.L1937:
	movq	%r15, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1936:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1935:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1932:
	movq	%r15, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1939:
	movq	%r15, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1940:
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1942:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1941:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1945:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r8
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L1944:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1943:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L1764
.L1931:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21298:
	.size	_ZN2v88internal7Genesis27InitializeIteratorFunctionsEv, .-_ZN2v88internal7Genesis27InitializeIteratorFunctionsEv
	.section	.rodata._ZN2v88internal7Genesis26InitializeCallSiteBuiltinsEv.str1.1,"aMS",@progbits,1
.LC99:
	.string	"CallSite"
	.data
	.align 32
.LC100:
	.quad	.LC0
	.long	242
	.zero	4
	.quad	.LC1
	.long	243
	.zero	4
	.quad	.LC2
	.long	244
	.zero	4
	.quad	.LC3
	.long	245
	.zero	4
	.quad	.LC4
	.long	246
	.zero	4
	.quad	.LC5
	.long	247
	.zero	4
	.quad	.LC6
	.long	248
	.zero	4
	.quad	.LC7
	.long	249
	.zero	4
	.quad	.LC8
	.long	250
	.zero	4
	.quad	.LC9
	.long	251
	.zero	4
	.quad	.LC10
	.long	252
	.zero	4
	.quad	.LC11
	.long	253
	.zero	4
	.quad	.LC12
	.long	254
	.zero	4
	.quad	.LC13
	.long	255
	.zero	4
	.quad	.LC14
	.long	256
	.zero	4
	.quad	.LC15
	.long	257
	.zero	4
	.quad	.LC16
	.long	258
	.zero	4
	.quad	.LC17
	.long	259
	.zero	4
	.quad	.LC18
	.long	260
	.zero	4
	.section	.text._ZN2v88internal7Genesis26InitializeCallSiteBuiltinsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis26InitializeCallSiteBuiltinsEv
	.type	_ZN2v88internal7Genesis26InitializeCallSiteBuiltinsEv, @function
_ZN2v88internal7Genesis26InitializeCallSiteBuiltinsEv:
.LFB21299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1057, %edx
	movl	$168, %r9d
	movl	$24, %ecx
	leaq	.LC99(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$360, %rsp
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	leaq	96(%r14), %r8
	movq	(%rdi), %rdi
	movq	%rax, -376(%rbp)
	movq	41096(%r14), %rax
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateEPKcNS0_12InstanceTypeEiiNS0_6HandleINS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movl	$-1, %edx
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	(%rbx), %r15
	movq	12464(%r15), %rax
	movq	39(%rax), %r13
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1947
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L1948:
	movq	(%r12), %rdx
	leaq	295(%r13), %r15
	movq	%rdx, 295(%r13)
	testb	$1, %dl
	je	.L1961
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -392(%rbp)
	testl	$262144, %eax
	jne	.L1972
	testb	$24, %al
	je	.L1961
.L1974:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1973
	.p2align 4,,10
	.p2align 3
.L1961:
	movq	(%r12), %rax
	movq	(%rbx), %r13
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L1954
	movq	23(%rsi), %rsi
.L1954:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1955
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L1956:
	leaq	-368(%rbp), %rdi
	movl	$38, %ecx
	leaq	-64(%rbp), %r13
	leaq	.LC100(%rip), %rsi
	leaq	-368(%rbp), %r15
	rep movsq
	.p2align 4,,10
	.p2align 3
.L1958:
	movl	8(%r15), %ecx
	movq	(%r15), %rdx
	movl	$7, %r9d
	movq	%r12, %rsi
	movq	(%rbx), %rdi
	movl	$1, %r8d
	addq	$16, %r15
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	cmpq	%r15, %r13
	jne	.L1958
	movq	-376(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-384(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L1946
	movq	%rax, 41096(%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1971
	addq	$360, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L1946:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1971
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1972:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rdx, -400(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-392(%rbp), %rcx
	movq	-400(%rbp), %rdx
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1974
	jmp	.L1961
	.p2align 4,,10
	.p2align 3
.L1955:
	movq	41088(%r13), %r12
	cmpq	%r12, 41096(%r13)
	je	.L1975
.L1957:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L1947:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L1976
.L1949:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r15)
	movq	%r13, (%rax)
	jmp	.L1948
.L1973:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1961
.L1976:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1949
.L1975:
	movq	%r13, %rdi
	movq	%rsi, -392(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-392(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1957
.L1971:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21299:
	.size	_ZN2v88internal7Genesis26InitializeCallSiteBuiltinsEv, .-_ZN2v88internal7Genesis26InitializeCallSiteBuiltinsEv
	.section	.text._ZN2v88internal7Genesis42InitializeGlobal_harmony_namespace_exportsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis42InitializeGlobal_harmony_namespace_exportsEv
	.type	_ZN2v88internal7Genesis42InitializeGlobal_harmony_namespace_exportsEv, @function
_ZN2v88internal7Genesis42InitializeGlobal_harmony_namespace_exportsEv:
.LFB28331:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28331:
	.size	_ZN2v88internal7Genesis42InitializeGlobal_harmony_namespace_exportsEv, .-_ZN2v88internal7Genesis42InitializeGlobal_harmony_namespace_exportsEv
	.section	.text._ZN2v88internal7Genesis40InitializeGlobal_harmony_private_methodsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis40InitializeGlobal_harmony_private_methodsEv
	.type	_ZN2v88internal7Genesis40InitializeGlobal_harmony_private_methodsEv, @function
_ZN2v88internal7Genesis40InitializeGlobal_harmony_private_methodsEv:
.LFB28315:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28315:
	.size	_ZN2v88internal7Genesis40InitializeGlobal_harmony_private_methodsEv, .-_ZN2v88internal7Genesis40InitializeGlobal_harmony_private_methodsEv
	.section	.text._ZN2v88internal7Genesis39InitializeGlobal_harmony_dynamic_importEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis39InitializeGlobal_harmony_dynamic_importEv
	.type	_ZN2v88internal7Genesis39InitializeGlobal_harmony_dynamic_importEv, @function
_ZN2v88internal7Genesis39InitializeGlobal_harmony_dynamic_importEv:
.LFB28335:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28335:
	.size	_ZN2v88internal7Genesis39InitializeGlobal_harmony_dynamic_importEv, .-_ZN2v88internal7Genesis39InitializeGlobal_harmony_dynamic_importEv
	.section	.text._ZN2v88internal7Genesis36InitializeGlobal_harmony_import_metaEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis36InitializeGlobal_harmony_import_metaEv
	.type	_ZN2v88internal7Genesis36InitializeGlobal_harmony_import_metaEv, @function
_ZN2v88internal7Genesis36InitializeGlobal_harmony_import_metaEv:
.LFB28333:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28333:
	.size	_ZN2v88internal7Genesis36InitializeGlobal_harmony_import_metaEv, .-_ZN2v88internal7Genesis36InitializeGlobal_harmony_import_metaEv
	.section	.text._ZN2v88internal7Genesis40InitializeGlobal_harmony_regexp_sequenceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis40InitializeGlobal_harmony_regexp_sequenceEv
	.type	_ZN2v88internal7Genesis40InitializeGlobal_harmony_regexp_sequenceEv, @function
_ZN2v88internal7Genesis40InitializeGlobal_harmony_regexp_sequenceEv:
.LFB28317:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28317:
	.size	_ZN2v88internal7Genesis40InitializeGlobal_harmony_regexp_sequenceEv, .-_ZN2v88internal7Genesis40InitializeGlobal_harmony_regexp_sequenceEv
	.section	.text._ZN2v88internal7Genesis42InitializeGlobal_harmony_optional_chainingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis42InitializeGlobal_harmony_optional_chainingEv
	.type	_ZN2v88internal7Genesis42InitializeGlobal_harmony_optional_chainingEv, @function
_ZN2v88internal7Genesis42InitializeGlobal_harmony_optional_chainingEv:
.LFB28319:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28319:
	.size	_ZN2v88internal7Genesis42InitializeGlobal_harmony_optional_chainingEv, .-_ZN2v88internal7Genesis42InitializeGlobal_harmony_optional_chainingEv
	.section	.text._ZN2v88internal7Genesis32InitializeGlobal_harmony_nullishEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis32InitializeGlobal_harmony_nullishEv
	.type	_ZN2v88internal7Genesis32InitializeGlobal_harmony_nullishEv, @function
_ZN2v88internal7Genesis32InitializeGlobal_harmony_nullishEv:
.LFB28321:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28321:
	.size	_ZN2v88internal7Genesis32InitializeGlobal_harmony_nullishEv, .-_ZN2v88internal7Genesis32InitializeGlobal_harmony_nullishEv
	.section	.text._ZN2v88internal7Genesis59InitializeGlobal_harmony_intl_add_calendar_numbering_systemEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis59InitializeGlobal_harmony_intl_add_calendar_numbering_systemEv
	.type	_ZN2v88internal7Genesis59InitializeGlobal_harmony_intl_add_calendar_numbering_systemEv, @function
_ZN2v88internal7Genesis59InitializeGlobal_harmony_intl_add_calendar_numbering_systemEv:
.LFB28325:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28325:
	.size	_ZN2v88internal7Genesis59InitializeGlobal_harmony_intl_add_calendar_numbering_systemEv, .-_ZN2v88internal7Genesis59InitializeGlobal_harmony_intl_add_calendar_numbering_systemEv
	.section	.text._ZN2v88internal7Genesis36InitializeGlobal_harmony_intl_bigintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis36InitializeGlobal_harmony_intl_bigintEv
	.type	_ZN2v88internal7Genesis36InitializeGlobal_harmony_intl_bigintEv, @function
_ZN2v88internal7Genesis36InitializeGlobal_harmony_intl_bigintEv:
.LFB28337:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28337:
	.size	_ZN2v88internal7Genesis36InitializeGlobal_harmony_intl_bigintEv, .-_ZN2v88internal7Genesis36InitializeGlobal_harmony_intl_bigintEv
	.section	.text._ZN2v88internal7Genesis51InitializeGlobal_harmony_intl_dateformat_day_periodEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis51InitializeGlobal_harmony_intl_dateformat_day_periodEv
	.type	_ZN2v88internal7Genesis51InitializeGlobal_harmony_intl_dateformat_day_periodEv, @function
_ZN2v88internal7Genesis51InitializeGlobal_harmony_intl_dateformat_day_periodEv:
.LFB28327:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28327:
	.size	_ZN2v88internal7Genesis51InitializeGlobal_harmony_intl_dateformat_day_periodEv, .-_ZN2v88internal7Genesis51InitializeGlobal_harmony_intl_dateformat_day_periodEv
	.section	.text._ZN2v88internal7Genesis65InitializeGlobal_harmony_intl_dateformat_fractional_second_digitsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis65InitializeGlobal_harmony_intl_dateformat_fractional_second_digitsEv
	.type	_ZN2v88internal7Genesis65InitializeGlobal_harmony_intl_dateformat_fractional_second_digitsEv, @function
_ZN2v88internal7Genesis65InitializeGlobal_harmony_intl_dateformat_fractional_second_digitsEv:
.LFB28329:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28329:
	.size	_ZN2v88internal7Genesis65InitializeGlobal_harmony_intl_dateformat_fractional_second_digitsEv, .-_ZN2v88internal7Genesis65InitializeGlobal_harmony_intl_dateformat_fractional_second_digitsEv
	.section	.text._ZN2v88internal7Genesis48InitializeGlobal_harmony_intl_dateformat_quarterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis48InitializeGlobal_harmony_intl_dateformat_quarterEv
	.type	_ZN2v88internal7Genesis48InitializeGlobal_harmony_intl_dateformat_quarterEv, @function
_ZN2v88internal7Genesis48InitializeGlobal_harmony_intl_dateformat_quarterEv:
.LFB28323:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28323:
	.size	_ZN2v88internal7Genesis48InitializeGlobal_harmony_intl_dateformat_quarterEv, .-_ZN2v88internal7Genesis48InitializeGlobal_harmony_intl_dateformat_quarterEv
	.section	.text._ZN2v88internal7Genesis44InitializeGlobal_harmony_intl_datetime_styleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis44InitializeGlobal_harmony_intl_datetime_styleEv
	.type	_ZN2v88internal7Genesis44InitializeGlobal_harmony_intl_datetime_styleEv, @function
_ZN2v88internal7Genesis44InitializeGlobal_harmony_intl_datetime_styleEv:
.LFB28339:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28339:
	.size	_ZN2v88internal7Genesis44InitializeGlobal_harmony_intl_datetime_styleEv, .-_ZN2v88internal7Genesis44InitializeGlobal_harmony_intl_datetime_styleEv
	.section	.text._ZN2v88internal7Genesis50InitializeGlobal_harmony_intl_numberformat_unifiedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis50InitializeGlobal_harmony_intl_numberformat_unifiedEv
	.type	_ZN2v88internal7Genesis50InitializeGlobal_harmony_intl_numberformat_unifiedEv, @function
_ZN2v88internal7Genesis50InitializeGlobal_harmony_intl_numberformat_unifiedEv:
.LFB28341:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28341:
	.size	_ZN2v88internal7Genesis50InitializeGlobal_harmony_intl_numberformat_unifiedEv, .-_ZN2v88internal7Genesis50InitializeGlobal_harmony_intl_numberformat_unifiedEv
	.section	.text._ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv
	.type	_ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv, @function
_ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv:
.LFB21314:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal30FLAG_harmony_sharedarraybufferE(%rip)
	jne	.L1993
	ret
	.p2align 4,,10
	.p2align 3
.L1993:
	jmp	_ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv.part.0
	.cfi_endproc
.LFE21314:
	.size	_ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv, .-_ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv
	.section	.text._ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv
	.type	_ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv, @function
_ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv:
.LFB21315:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal22FLAG_harmony_weak_refsE(%rip)
	jne	.L1996
	ret
	.p2align 4,,10
	.p2align 3
.L1996:
	jmp	_ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv.part.0
	.cfi_endproc
.LFE21315:
	.size	_ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv, .-_ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv
	.section	.text._ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv
	.type	_ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv, @function
_ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv:
.LFB21316:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal32FLAG_harmony_promise_all_settledE(%rip)
	jne	.L1999
	ret
	.p2align 4,,10
	.p2align 3
.L1999:
	jmp	_ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv.part.0
	.cfi_endproc
.LFE21316:
	.size	_ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv, .-_ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv
	.section	.text._ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv
	.type	_ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv, @function
_ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv:
.LFB21317:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal35FLAG_harmony_intl_date_format_rangeE(%rip)
	jne	.L2002
	ret
	.p2align 4,,10
	.p2align 3
.L2002:
	jmp	_ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv.part.0
	.cfi_endproc
.LFE21317:
	.size	_ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv, .-_ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv
	.section	.text._ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv
	.type	_ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv, @function
_ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv:
.LFB21318:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal27FLAG_harmony_intl_segmenterE(%rip)
	jne	.L2005
	ret
	.p2align 4,,10
	.p2align 3
.L2005:
	jmp	_ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv.part.0
	.cfi_endproc
.LFE21318:
	.size	_ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv, .-_ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv
	.section	.rodata._ZN2v88internal7Genesis17CreateArrayBufferENS0_6HandleINS0_6StringEEENS1_15ArrayBufferKindE.str1.1,"aMS",@progbits,1
.LC101:
	.string	"isView"
.LC102:
	.string	"slice"
	.section	.text._ZN2v88internal7Genesis17CreateArrayBufferENS0_6HandleINS0_6StringEEENS1_15ArrayBufferKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis17CreateArrayBufferENS0_6HandleINS0_6StringEEENS1_15ArrayBufferKindE
	.type	_ZN2v88internal7Genesis17CreateArrayBufferENS0_6HandleINS0_6StringEEENS1_15ArrayBufferKindE, @function
_ZN2v88internal7Genesis17CreateArrayBufferENS0_6HandleINS0_6StringEEENS1_15ArrayBufferKindE:
.LFB21319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2007
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2008:
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rdx
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r13, %r9
	pushq	$221
	xorl	%r8d, %r8d
	movl	$64, %ecx
	movq	%r15, %rsi
	movl	$1059, %edx
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movl	$-1, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	$2, %r8d
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	(%r12), %rax
	movq	23(%rax), %rax
	movw	%cx, 39(%rax)
	movq	(%rbx), %rdi
	movq	%r12, %rcx
	leaq	2304(%rdi), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	popq	%rsi
	popq	%rdi
	testl	%r14d, %r14d
	je	.L2010
	cmpl	$1, %r14d
	jne	.L2012
	movq	(%rbx), %rdi
	movl	$573, %ecx
	movq	%r13, %rsi
	leaq	2200(%rdi), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	movq	(%rbx), %rdi
	movl	$574, %ecx
	movq	%r13, %rsi
	movl	$1, %r8d
	leaq	.LC102(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
.L2012:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2007:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2014
.L2009:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L2008
	.p2align 4,,10
	.p2align 3
.L2010:
	movq	(%rbx), %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movl	$224, %ecx
	leaq	.LC101(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$223, %ecx
	leaq	2200(%rdi), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	movq	(%rbx), %rdi
	movl	$225, %ecx
	movq	%r13, %rsi
	movl	$1, %r8d
	leaq	.LC102(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L2014:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2009
	.cfi_endproc
.LFE21319:
	.size	_ZN2v88internal7Genesis17CreateArrayBufferENS0_6HandleINS0_6StringEEENS1_15ArrayBufferKindE, .-_ZN2v88internal7Genesis17CreateArrayBufferENS0_6HandleINS0_6StringEEENS1_15ArrayBufferKindE
	.section	.rodata._ZN2v88internal7Genesis16InitializeGlobalENS0_6HandleINS0_14JSGlobalObjectEEENS2_INS0_10JSFunctionEEE.str1.1,"aMS",@progbits,1
.LC103:
	.string	"assign"
.LC104:
	.string	"getOwnPropertyDescriptor"
.LC105:
	.string	"getOwnPropertyDescriptors"
.LC106:
	.string	"getOwnPropertyNames"
.LC107:
	.string	"getOwnPropertySymbols"
.LC108:
	.string	"is"
.LC109:
	.string	"preventExtensions"
.LC110:
	.string	"seal"
.LC111:
	.string	"create"
.LC112:
	.string	"defineProperties"
.LC113:
	.string	"defineProperty"
.LC114:
	.string	"freeze"
.LC115:
	.string	"getPrototypeOf"
.LC116:
	.string	"setPrototypeOf"
.LC117:
	.string	"isExtensible"
.LC118:
	.string	"isFrozen"
.LC119:
	.string	"isSealed"
.LC120:
	.string	"keys"
.LC121:
	.string	"entries"
.LC122:
	.string	"fromEntries"
.LC123:
	.string	"values"
.LC124:
	.string	"__defineGetter__"
.LC125:
	.string	"__defineSetter__"
.LC126:
	.string	"hasOwnProperty"
.LC127:
	.string	"__lookupGetter__"
.LC128:
	.string	"__lookupSetter__"
.LC129:
	.string	"isPrototypeOf"
.LC130:
	.string	"propertyIsEnumerable"
.LC131:
	.string	"valueOf"
.LC132:
	.string	"toLocaleString"
.LC133:
	.string	"Function"
.LC134:
	.string	"apply"
.LC135:
	.string	"bind"
.LC136:
	.string	"call"
.LC137:
	.string	"[Symbol.hasInstance]"
.LC138:
	.string	"Array"
.LC139:
	.string	"isArray"
.LC140:
	.string	"from"
.LC141:
	.string	"of"
.LC142:
	.string	"concat"
.LC143:
	.string	"copyWithin"
.LC144:
	.string	"fill"
.LC145:
	.string	"find"
.LC146:
	.string	"findIndex"
.LC147:
	.string	"lastIndexOf"
.LC148:
	.string	"pop"
.LC149:
	.string	"push"
.LC150:
	.string	"reverse"
.LC151:
	.string	"shift"
.LC152:
	.string	"unshift"
.LC153:
	.string	"sort"
.LC154:
	.string	"splice"
.LC155:
	.string	"includes"
.LC156:
	.string	"indexOf"
.LC157:
	.string	"join"
.LC158:
	.string	"forEach"
.LC159:
	.string	"filter"
.LC160:
	.string	"flat"
.LC161:
	.string	"flatMap"
.LC162:
	.string	"map"
.LC163:
	.string	"every"
.LC164:
	.string	"some"
.LC165:
	.string	"reduce"
.LC166:
	.string	"reduceRight"
.LC167:
	.string	"Number"
.LC168:
	.string	"toExponential"
.LC169:
	.string	"toFixed"
.LC170:
	.string	"toPrecision"
.LC171:
	.string	"isFinite"
.LC172:
	.string	"isInteger"
.LC173:
	.string	"isNaN"
.LC174:
	.string	"isSafeInteger"
.LC175:
	.string	"parseFloat"
.LC176:
	.string	"parseInt"
.LC178:
	.string	"MAX_VALUE"
.LC180:
	.string	"MIN_VALUE"
.LC181:
	.string	"NaN"
.LC183:
	.string	"NEGATIVE_INFINITY"
.LC184:
	.string	"POSITIVE_INFINITY"
.LC186:
	.string	"MAX_SAFE_INTEGER"
.LC188:
	.string	"MIN_SAFE_INTEGER"
.LC190:
	.string	"EPSILON"
.LC191:
	.string	"Infinity"
.LC192:
	.string	"undefined"
.LC193:
	.string	"Boolean"
.LC194:
	.string	"String"
.LC195:
	.string	"fromCharCode"
.LC196:
	.string	"fromCodePoint"
.LC197:
	.string	"raw"
.LC198:
	.string	"anchor"
.LC199:
	.string	"big"
.LC200:
	.string	"blink"
.LC201:
	.string	"bold"
.LC202:
	.string	"charAt"
.LC203:
	.string	"charCodeAt"
.LC204:
	.string	"codePointAt"
.LC205:
	.string	"endsWith"
.LC206:
	.string	"fontcolor"
.LC207:
	.string	"fontsize"
.LC208:
	.string	"fixed"
.LC209:
	.string	"italics"
.LC210:
	.string	"link"
.LC211:
	.string	"localeCompare"
.LC212:
	.string	"match"
.LC213:
	.string	"matchAll"
.LC214:
	.string	"normalize"
.LC215:
	.string	"padEnd"
.LC216:
	.string	"padStart"
.LC217:
	.string	"repeat"
.LC218:
	.string	"replace"
.LC219:
	.string	"search"
.LC220:
	.string	"small"
.LC221:
	.string	"split"
.LC222:
	.string	"strike"
.LC223:
	.string	"sub"
.LC224:
	.string	"substr"
.LC225:
	.string	"substring"
.LC226:
	.string	"sup"
.LC227:
	.string	"startsWith"
.LC228:
	.string	"trim"
.LC229:
	.string	"trimStart"
.LC230:
	.string	"trimLeft"
.LC231:
	.string	"trimEnd"
.LC232:
	.string	"trimRight"
.LC233:
	.string	"toLocaleLowerCase"
.LC234:
	.string	"toLocaleUpperCase"
.LC235:
	.string	"toLowerCase"
.LC236:
	.string	"toUpperCase"
.LC237:
	.string	"String Iterator"
.LC238:
	.string	"StringIterator"
.LC239:
	.string	"Symbol"
.LC240:
	.string	"for"
.LC241:
	.string	"keyFor"
.LC242:
	.string	"asyncIterator"
.LC243:
	.string	"hasInstance"
.LC244:
	.string	"isConcatSpreadable"
.LC245:
	.string	"iterator"
.LC246:
	.string	"species"
.LC247:
	.string	"toPrimitive"
.LC248:
	.string	"toStringTag"
.LC249:
	.string	"unscopables"
.LC250:
	.string	"description"
.LC251:
	.string	"[Symbol.toPrimitive]"
.LC252:
	.string	"Date"
.LC253:
	.string	"now"
.LC254:
	.string	"parse"
.LC255:
	.string	"UTC"
.LC256:
	.string	"toDateString"
.LC257:
	.string	"toTimeString"
.LC258:
	.string	"toISOString"
.LC259:
	.string	"toUTCString"
.LC260:
	.string	"toGMTString"
.LC261:
	.string	"getDate"
.LC262:
	.string	"setDate"
.LC263:
	.string	"getDay"
.LC264:
	.string	"getFullYear"
.LC265:
	.string	"setFullYear"
.LC266:
	.string	"getHours"
.LC267:
	.string	"setHours"
.LC268:
	.string	"getMilliseconds"
.LC269:
	.string	"setMilliseconds"
.LC270:
	.string	"getMinutes"
.LC271:
	.string	"setMinutes"
.LC272:
	.string	"getMonth"
.LC273:
	.string	"setMonth"
.LC274:
	.string	"getSeconds"
.LC275:
	.string	"setSeconds"
.LC276:
	.string	"getTime"
.LC277:
	.string	"setTime"
.LC278:
	.string	"getTimezoneOffset"
.LC279:
	.string	"getUTCDate"
.LC280:
	.string	"setUTCDate"
.LC281:
	.string	"getUTCDay"
.LC282:
	.string	"getUTCFullYear"
.LC283:
	.string	"setUTCFullYear"
.LC284:
	.string	"getUTCHours"
.LC285:
	.string	"setUTCHours"
.LC286:
	.string	"getUTCMilliseconds"
.LC287:
	.string	"setUTCMilliseconds"
.LC288:
	.string	"getUTCMinutes"
.LC289:
	.string	"setUTCMinutes"
.LC290:
	.string	"getUTCMonth"
.LC291:
	.string	"setUTCMonth"
.LC292:
	.string	"getUTCSeconds"
.LC293:
	.string	"setUTCSeconds"
.LC294:
	.string	"getYear"
.LC295:
	.string	"setYear"
.LC296:
	.string	"toJSON"
.LC297:
	.string	"toLocaleDateString"
.LC298:
	.string	"toLocaleTimeString"
.LC299:
	.string	"Promise"
.LC300:
	.string	"all"
.LC301:
	.string	"race"
.LC302:
	.string	"resolve"
.LC303:
	.string	"reject"
.LC304:
	.string	"then"
.LC305:
	.string	"catch"
.LC306:
	.string	"finally"
.LC307:
	.string	"RegExp"
.LC308:
	.string	"exec"
.LC309:
	.string	"compile"
.LC310:
	.string	"test"
.LC311:
	.string	"[Symbol.match]"
.LC312:
	.string	"[Symbol.matchAll]"
.LC313:
	.string	"[Symbol.replace]"
.LC314:
	.string	"[Symbol.search]"
.LC315:
	.string	"[Symbol.split]"
.LC316:
	.string	"$_"
.LC317:
	.string	"lastMatch"
.LC318:
	.string	"$&"
.LC319:
	.string	"lastParen"
.LC320:
	.string	"$+"
.LC321:
	.string	"leftContext"
.LC322:
	.string	"$`"
.LC323:
	.string	"rightContext"
.LC324:
	.string	"$'"
.LC325:
	.string	"$1"
.LC326:
	.string	"$2"
.LC327:
	.string	"$3"
.LC328:
	.string	"$4"
.LC329:
	.string	"$5"
.LC330:
	.string	"$6"
.LC331:
	.string	"$7"
.LC332:
	.string	"$8"
.LC333:
	.string	"$9"
.LC334:
	.string	"RegExp String Iterator"
.LC335:
	.string	"RegExpStringIterator"
.LC336:
	.string	"JSON"
.LC337:
	.string	"stringify"
.LC338:
	.string	"Math"
.LC339:
	.string	"abs"
.LC340:
	.string	"acos"
.LC341:
	.string	"acosh"
.LC342:
	.string	"asin"
.LC343:
	.string	"asinh"
.LC344:
	.string	"atan"
.LC345:
	.string	"atanh"
.LC346:
	.string	"atan2"
.LC347:
	.string	"ceil"
.LC348:
	.string	"cbrt"
.LC349:
	.string	"expm1"
.LC350:
	.string	"clz32"
.LC351:
	.string	"cos"
.LC352:
	.string	"cosh"
.LC353:
	.string	"exp"
.LC354:
	.string	"floor"
.LC355:
	.string	"fround"
.LC356:
	.string	"hypot"
.LC357:
	.string	"imul"
.LC358:
	.string	"log"
.LC359:
	.string	"log1p"
.LC360:
	.string	"log2"
.LC361:
	.string	"log10"
.LC362:
	.string	"max"
.LC363:
	.string	"min"
.LC364:
	.string	"pow"
.LC365:
	.string	"random"
.LC366:
	.string	"round"
.LC367:
	.string	"sign"
.LC368:
	.string	"sin"
.LC369:
	.string	"sinh"
.LC370:
	.string	"sqrt"
.LC371:
	.string	"tan"
.LC372:
	.string	"tanh"
.LC373:
	.string	"trunc"
.LC375:
	.string	"E"
.LC377:
	.string	"LN10"
.LC379:
	.string	"LN2"
.LC380:
	.string	"LOG10E"
.LC381:
	.string	"LOG2E"
.LC383:
	.string	"PI"
.LC385:
	.string	"SQRT1_2"
.LC387:
	.string	"SQRT2"
.LC388:
	.string	"console"
.LC389:
	.string	"debug"
.LC390:
	.string	"error"
.LC391:
	.string	"info"
.LC392:
	.string	"warn"
.LC393:
	.string	"dir"
.LC394:
	.string	"dirxml"
.LC395:
	.string	"table"
.LC396:
	.string	"trace"
.LC397:
	.string	"group"
.LC398:
	.string	"groupCollapsed"
.LC399:
	.string	"groupEnd"
.LC400:
	.string	"clear"
.LC401:
	.string	"count"
.LC402:
	.string	"countReset"
.LC403:
	.string	"assert"
.LC404:
	.string	"profile"
.LC405:
	.string	"profileEnd"
.LC406:
	.string	"time"
.LC407:
	.string	"timeLog"
.LC408:
	.string	"timeEnd"
.LC409:
	.string	"timeStamp"
.LC410:
	.string	"context"
.LC411:
	.string	"Object"
.LC412:
	.string	"getCanonicalLocales"
.LC413:
	.string	"formatToParts"
.LC414:
	.string	"NumberFormat"
.LC415:
	.string	"Collator"
.LC416:
	.string	"v8BreakIterator"
.LC417:
	.string	"PluralRules"
.LC418:
	.string	"select"
.LC419:
	.string	"RelativeTimeFormat"
.LC420:
	.string	"Intl.RelativeTimeFormat"
.LC421:
	.string	"format"
.LC422:
	.string	"ListFormat"
.LC423:
	.string	"Intl.ListFormat"
.LC424:
	.string	"Locale"
.LC425:
	.string	"Intl.Locale"
.LC426:
	.string	"maximize"
.LC427:
	.string	"minimize"
	.section	.rodata._ZN2v88internal7Genesis16InitializeGlobalENS0_6HandleINS0_14JSGlobalObjectEEENS2_INS0_10JSFunctionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC428:
	.string	"arrayBufferConstructor_DoNotInitialize"
	.section	.rodata._ZN2v88internal7Genesis16InitializeGlobalENS0_6HandleINS0_14JSGlobalObjectEEENS2_INS0_10JSFunctionEEE.str1.1
.LC429:
	.string	"load"
.LC430:
	.string	"store"
.LC431:
	.string	"add"
.LC432:
	.string	"and"
.LC433:
	.string	"or"
.LC434:
	.string	"xor"
.LC435:
	.string	"exchange"
.LC436:
	.string	"compareExchange"
.LC437:
	.string	"isLockFree"
.LC438:
	.string	"wait"
.LC439:
	.string	"wake"
.LC440:
	.string	"notify"
.LC441:
	.string	"TypedArray"
.LC442:
	.string	"set"
.LC443:
	.string	"subarray"
.LC444:
	.string	"Uint8Array"
.LC445:
	.string	"Int8Array"
.LC446:
	.string	"Uint16Array"
.LC447:
	.string	"Int16Array"
.LC448:
	.string	"Uint32Array"
.LC449:
	.string	"Int32Array"
.LC450:
	.string	"Float32Array"
.LC451:
	.string	"Float64Array"
.LC452:
	.string	"Uint8ClampedArray"
.LC453:
	.string	"BigUint64Array"
.LC454:
	.string	"BigInt64Array"
.LC455:
	.string	"DataView"
.LC456:
	.string	"getInt8"
.LC457:
	.string	"setInt8"
.LC458:
	.string	"getUint8"
.LC459:
	.string	"setUint8"
.LC460:
	.string	"getInt16"
.LC461:
	.string	"setInt16"
.LC462:
	.string	"getUint16"
.LC463:
	.string	"setUint16"
.LC464:
	.string	"getInt32"
.LC465:
	.string	"setInt32"
.LC466:
	.string	"getUint32"
.LC467:
	.string	"setUint32"
.LC468:
	.string	"getFloat32"
.LC469:
	.string	"setFloat32"
.LC470:
	.string	"getFloat64"
.LC471:
	.string	"setFloat64"
.LC472:
	.string	"getBigInt64"
.LC473:
	.string	"setBigInt64"
.LC474:
	.string	"getBigUint64"
.LC475:
	.string	"setBigUint64"
.LC476:
	.string	"Map"
.LC477:
	.string	"get"
.LC478:
	.string	"has"
.LC479:
	.string	"delete"
.LC480:
	.string	"size"
.LC481:
	.string	"BigInt"
.LC482:
	.string	"asUintN"
.LC483:
	.string	"asIntN"
.LC484:
	.string	"Set"
.LC485:
	.string	"WeakMap"
.LC486:
	.string	"WeakSet"
.LC487:
	.string	"Proxy"
.LC488:
	.string	"revocable"
.LC489:
	.string	"Reflect"
.LC490:
	.string	"deleteProperty"
.LC491:
	.string	"construct"
.LC492:
	.string	"ownKeys"
.LC493:
	.string	"IsConstructor"
.LC494:
	.string	"FastAliasedArguments"
.LC495:
	.string	"SlowAliasedArguments"
	.section	.text._ZN2v88internal7Genesis16InitializeGlobalENS0_6HandleINS0_14JSGlobalObjectEEENS2_INS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis16InitializeGlobalENS0_6HandleINS0_14JSGlobalObjectEEENS2_INS0_10JSFunctionEEE
	.type	_ZN2v88internal7Genesis16InitializeGlobalENS0_6HandleINS0_14JSGlobalObjectEEENS2_INS0_10JSFunctionEEE, @function
_ZN2v88internal7Genesis16InitializeGlobalENS0_6HandleINS0_14JSGlobalObjectEEENS2_INS0_10JSFunctionEEE:
.LFB21291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$296, %rsp
	movq	%rsi, -240(%rbp)
	movq	%rdx, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	(%rax), %r14
	movq	(%rdx), %rax
	leaq	15(%r14), %r13
	movq	23(%rax), %rax
	movq	15(%rax), %r12
	testb	$1, %r12b
	jne	.L2973
.L2016:
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
	movq	%rax, 15(%r14)
	movq	%rax, %r12
	testb	$1, %al
	je	.L2505
.L2017:
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L2974
	testb	$24, %al
	je	.L2505
.L3113:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2975
	.p2align 4,,10
	.p2align 3
.L2505:
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	$0, 23(%rax)
	movq	16(%rbx), %rax
	movq	(%rax), %r13
	movq	-240(%rbp), %rax
	movq	(%rax), %r12
	leaq	31(%r13), %r15
	movq	%r12, 31(%r13)
	testb	$1, %r12b
	je	.L2504
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L2976
	testb	$24, %al
	je	.L2504
.L3112:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2977
	.p2align 4,,10
	.p2align 3
.L2504:
	movq	16(%rbx), %rax
	movq	(%rax), %r13
	movq	-240(%rbp), %rax
	movq	(%rax), %r12
	leaq	1151(%r13), %r15
	movq	%r12, 1151(%r13)
	testb	$1, %r12b
	je	.L2503
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L2978
	testb	$24, %al
	je	.L2503
.L3117:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2979
	.p2align 4,,10
	.p2align 3
.L2503:
	movq	(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory21NewScriptContextTableEv@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %r13
	movq	(%rdx), %r14
	movq	%r13, 1135(%r14)
	leaq	1135(%r14), %rsi
	testb	$1, %r13b
	je	.L2502
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L2980
	testb	$24, %al
	je	.L2502
.L3116:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2981
	.p2align 4,,10
	.p2align 3
.L2502:
	movq	%rbx, %rdi
	leaq	3000(%r12), %r15
	call	_ZN2v88internal7Genesis24InstallGlobalThisBindingEv
	movq	(%rbx), %rdi
	movq	%r15, -216(%rbp)
	call	_ZN2v88internal7Isolate15object_functionEv
	movq	-240(%rbp), %rsi
	movq	(%rbx), %rdi
	movq	%r15, %rdx
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%rax, %r13
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$459, %ecx
	leaq	.LC103(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$468, %ecx
	leaq	.LC104(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$469, %ecx
	movl	$2, %r9d
	leaq	.LC105(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$470, %ecx
	movq	%r13, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC106(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$471, %ecx
	movl	$2, %r9d
	leaq	.LC107(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$472, %ecx
	movq	%r13, %rsi
	movl	$1, %r8d
	leaq	.LC108(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movl	$847, %ecx
	movq	%r13, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC109(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$486, %ecx
	movl	$2, %r9d
	leaq	.LC110(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	xorl	%r8d, %r8d
	leaq	.LC111(%rip), %rdx
	movl	$460, %ecx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 1879(%r15)
	leaq	1879(%r15), %rsi
	testb	$1, %r14b
	je	.L2501
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -184(%rbp)
	testl	$262144, %eax
	jne	.L2982
	testb	$24, %al
	je	.L2501
.L3115:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2983
	.p2align 4,,10
	.p2align 3
.L2501:
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$1, %r8d
	movl	$463, %ecx
	leaq	.LC112(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$464, %ecx
	movl	$1, %r8d
	leaq	.LC113(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movl	$2, %r9d
	movl	$467, %ecx
	leaq	.LC114(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$848, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC115(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$849, %ecx
	movl	$1, %r8d
	leaq	.LC116(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$846, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC117(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movl	$2, %r9d
	movl	$473, %ecx
	leaq	.LC118(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movl	$2, %r9d
	movl	$474, %ecx
	leaq	.LC119(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$475, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC120(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$466, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC121(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movl	$2, %r9d
	movl	$845, %ecx
	leaq	.LC122(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$488, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC123(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	(%rbx), %rdi
	movl	$1, %r8d
	movl	$462, %ecx
	movq	%rax, %rsi
	leaq	.LC124(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	(%rbx), %rdi
	movl	$1, %r8d
	movl	$465, %ecx
	movq	%rax, %rsi
	leaq	.LC125(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movl	$1, %r8d
	movq	%rax, %rsi
	movl	$480, %ecx
	leaq	.LC126(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movl	$1, %r8d
	movq	%rax, %rsi
	movl	$476, %ecx
	leaq	.LC127(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movl	$1, %r8d
	movq	%rax, %rsi
	movl	$477, %ecx
	leaq	.LC128(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movl	$1, %r8d
	movq	%rax, %rsi
	movl	$481, %ecx
	leaq	.LC129(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$482, %ecx
	movq	%rax, %rsi
	movl	$2, %r9d
	leaq	.LC130(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	(%rbx), %rdi
	movl	$478, %ecx
	leaq	.LC18(%rip), %rdx
	movq	%rax, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rax), %r13
	movq	(%rdx), %r14
	movq	%r13, 1687(%r14)
	leaq	1687(%r14), %rsi
	testb	$1, %r13b
	je	.L2500
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L2984
	testb	$24, %al
	je	.L2500
.L3114:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2985
	.p2align 4,,10
	.p2align 3
.L2500:
	movq	(%rbx), %rdi
	leaq	3096(%r12), %r13
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movl	$1, %r8d
	movq	%rax, %rsi
	movl	$479, %ecx
	leaq	.LC131(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	movl	$483, %ecx
	movq	%rax, %rsi
	movl	$484, %r8d
	leaq	-128(%rbp), %r13
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movl	$1, %r8d
	movq	%rax, %rsi
	movl	$485, %ecx
	leaq	.LC132(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	16(%rbx), %rax
	movq	(%rbx), %r14
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2036
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -184(%rbp)
	movq	%rax, %rsi
.L2037:
	subq	$8, %rsp
	movq	-248(%rbp), %r9
	movq	(%rbx), %rdi
	movl	$64, %r8d
	pushq	$343
	movl	$1105, %ecx
	leaq	.LC133(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	movq	(%rbx), %r15
	movq	(%rax), %r9
	movq	%rax, %r14
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	1247(%rax), %r8
	movq	41112(%r15), %rdi
	popq	%r10
	popq	%r11
	testq	%rdi, %rdi
	je	.L2039
	movq	%r8, %rsi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-192(%rbp), %r9
	movq	(%rax), %r8
.L2040:
	movq	%r8, 55(%r9)
	leaq	55(%r9), %rsi
	testb	$1, %r8b
	je	.L2499
	movq	%r8, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L2986
	testb	$24, %al
	je	.L2499
.L3132:
	movq	%r9, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2987
	.p2align 4,,10
	.p2align 3
.L2499:
	movq	(%r14), %rax
	movl	$-1, %esi
	movl	$1, %edi
	movl	$50, %edx
	movq	23(%rax), %rax
	movw	%si, 41(%rax)
	movq	(%r14), %rax
	movq	%r14, %rsi
	movq	23(%rax), %rax
	movw	%di, 39(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	-248(%rbp), %r15
	movq	(%rbx), %rdi
	leaq	2304(%r12), %rax
	movq	%rax, %rdx
	movl	$2, %r8d
	movq	%r14, %rcx
	movq	%rax, -192(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movl	$344, %ecx
	leaq	.LC134(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$346, %ecx
	leaq	.LC135(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$347, %ecx
	leaq	.LC136(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$349, %ecx
	leaq	.LC18(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	pushq	$7
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	pushq	$1
	leaq	3936(%r12), %rax
	movl	$1, %r9d
	movl	$348, %r8d
	movq	%rax, %rdx
	leaq	.LC137(%rip), %rcx
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1679(%rdi)
	leaq	1679(%rdi), %rsi
	popq	%r8
	popq	%r9
	testb	$1, %r15b
	je	.L2498
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -200(%rbp)
	testl	$262144, %eax
	jne	.L2988
	testb	$24, %al
	je	.L2498
.L3133:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2989
	.p2align 4,,10
	.p2align 3
.L2498:
	movq	(%rbx), %r15
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	1247(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2048
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2049:
	movq	%rsi, -128(%rbp)
	movq	(%r14), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0
	movq	(%rbx), %r15
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	1255(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2051
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2052:
	movq	%rsi, -128(%rbp)
	movq	(%r14), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0
	movq	(%rbx), %r15
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	1271(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2054
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2055:
	movq	%rsi, -128(%rbp)
	movq	(%r14), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0
	movq	(%rbx), %r15
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	1279(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2057
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2058:
	movq	%rsi, -128(%rbp)
	movq	(%r14), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0
	movq	(%rbx), %r15
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	1287(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2060
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2061:
	movq	%rsi, -128(%rbp)
	movq	(%r14), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0
	movq	32(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
	movq	(%r14), %rsi
	call	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0
	movq	40(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
	movq	(%r14), %rsi
	call	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0
	movq	(%rbx), %r15
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	1295(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2063
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2064:
	movq	%rsi, -128(%rbp)
	movq	(%r14), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0
	movq	(%rbx), %r15
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	1431(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2066
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2067:
	movq	%rsi, -128(%rbp)
	movq	(%r14), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0
	movq	(%rbx), %rdi
	movl	$694, %esi
	xorl	%ecx, %ecx
	leaq	128(%r12), %rax
	movq	%rax, %rdx
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 167(%r15)
	leaq	167(%r15), %rsi
	testb	$1, %r14b
	je	.L2497
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -208(%rbp)
	testl	$262144, %eax
	jne	.L2990
	testb	$24, %al
	je	.L2497
.L3138:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2991
	.p2align 4,,10
	.p2align 3
.L2497:
	movq	-200(%rbp), %rdx
	movq	(%rbx), %rdi
	movl	$685, %esi
	xorl	%ecx, %ecx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 183(%r15)
	leaq	183(%r15), %rsi
	testb	$1, %r14b
	je	.L2496
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -208(%rbp)
	testl	$262144, %eax
	jne	.L2992
	testb	$24, %al
	je	.L2496
.L3137:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2993
	.p2align 4,,10
	.p2align 3
.L2496:
	movq	-200(%rbp), %rdx
	movq	(%rbx), %rdi
	movl	$686, %esi
	xorl	%ecx, %ecx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 175(%r15)
	leaq	175(%r15), %rsi
	testb	$1, %r14b
	je	.L2495
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -208(%rbp)
	testl	$262144, %eax
	jne	.L2994
	testb	$24, %al
	je	.L2495
.L3136:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2995
	.p2align 4,,10
	.p2align 3
.L2495:
	movq	-200(%rbp), %rdx
	movq	(%rbx), %rdi
	movl	$687, %esi
	xorl	%ecx, %ecx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 191(%r15)
	leaq	191(%r15), %rsi
	testb	$1, %r14b
	je	.L2494
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -208(%rbp)
	testl	$262144, %eax
	jne	.L2996
	testb	$24, %al
	je	.L2494
.L3135:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2997
	.p2align 4,,10
	.p2align 3
.L2494:
	movq	-200(%rbp), %rdx
	movq	(%rbx), %rdi
	movl	$690, %esi
	xorl	%ecx, %ecx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 199(%r15)
	leaq	199(%r15), %rsi
	testb	$1, %r14b
	je	.L2493
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -208(%rbp)
	testl	$262144, %eax
	jne	.L2998
	testb	$24, %al
	je	.L2493
.L3134:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2999
	.p2align 4,,10
	.p2align 3
.L2493:
	movq	-200(%rbp), %rdx
	movq	(%rbx), %rdi
	movl	$688, %esi
	xorl	%ecx, %ecx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 207(%r15)
	leaq	207(%r15), %rsi
	testb	$1, %r14b
	je	.L2492
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -208(%rbp)
	testl	$262144, %eax
	jne	.L3000
	testb	$24, %al
	je	.L2492
.L3128:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3001
	.p2align 4,,10
	.p2align 3
.L2492:
	movq	-200(%rbp), %rdx
	movq	(%rbx), %rdi
	movl	$689, %esi
	xorl	%ecx, %ecx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 215(%r15)
	leaq	215(%r15), %rsi
	testb	$1, %r14b
	je	.L2491
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -208(%rbp)
	testl	$262144, %eax
	jne	.L3002
	testb	$24, %al
	je	.L2491
.L3127:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3003
	.p2align 4,,10
	.p2align 3
.L2491:
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	-184(%rbp), %rsi
	movq	(%rbx), %rdi
	movl	$1061, %ecx
	leaq	.LC138(%rip), %rdx
	movq	%rax, %r8
	movl	$170, %r9d
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movl	$-1, %edx
	movl	$1, %ecx
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	(%r15), %rax
	movq	23(%rax), %rax
	movw	%cx, 39(%rax)
	movq	(%rbx), %rdx
	movq	(%r15), %rax
	movq	41112(%rdx), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2090
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2091:
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$1, %edx
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	leaq	2768(%r12), %rax
	movl	$6, %ecx
	movq	%r13, %rdi
	movq	%rax, %rsi
	leaq	4376(%r12), %rdx
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r14), %rax
	movq	(%rbx), %rsi
	movq	%r13, %rdx
	movq	%rax, -160(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rdi
	movl	$11, %edx
	movq	%r15, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_120InstallSpeciesGetterEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	movq	16(%rbx), %rsi
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal23CacheInitialJSArrayMapsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS3_INS0_3MapEEE@PLT
	xorl	%edx, %edx
	movl	$1, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	16(%rbx), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rdi
	movq	%rdx, 463(%rdi)
	leaq	463(%rdi), %rsi
	testb	$1, %dl
	je	.L2490
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -224(%rbp)
	testl	$262144, %eax
	jne	.L3004
	testb	$24, %al
	je	.L2490
.L3126:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3005
	.p2align 4,,10
	.p2align 3
.L2490:
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$193, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC139(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$195, %ecx
	leaq	.LC140(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$765, %ecx
	leaq	.LC141(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	-192(%rbp), %rdx
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$192, %ecx
	movl	$2, %r9d
	leaq	.LC142(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$726, %ecx
	leaq	.LC143(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$194, %ecx
	movl	$2, %r9d
	leaq	.LC144(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$739, %ecx
	movl	$2, %r9d
	leaq	.LC145(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$744, %ecx
	movl	$2, %r9d
	leaq	.LC146(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$760, %ecx
	movl	$2, %r9d
	leaq	.LC147(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$205, %ecx
	movl	$2, %r9d
	leaq	.LC148(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$207, %ecx
	movl	$2, %r9d
	leaq	.LC149(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$776, %ecx
	movl	$2, %r9d
	leaq	.LC150(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$777, %ecx
	movl	$2, %r9d
	leaq	.LC151(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$784, %ecx
	movl	$2, %r9d
	leaq	.LC152(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$778, %ecx
	leaq	.LC102(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$945, %ecx
	movl	$2, %r9d
	leaq	.LC153(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$783, %ecx
	leaq	.LC154(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$199, %ecx
	movl	$2, %r9d
	leaq	.LC155(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$203, %ecx
	movl	$2, %r9d
	leaq	.LC156(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$755, %ecx
	movl	$2, %r9d
	leaq	.LC157(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	leaq	.LC120(%rip), %rdx
	movl	$214, %ecx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1591(%rdi)
	leaq	1591(%rdi), %rsi
	testb	$1, %r15b
	je	.L2489
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -224(%rbp)
	testl	$262144, %eax
	jne	.L3006
	testb	$24, %al
	je	.L2489
.L3129:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3007
	.p2align 4,,10
	.p2align 3
.L2489:
	movq	(%rbx), %rdi
	leaq	.LC121(%rip), %rdx
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$213, %ecx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1575(%rdi)
	leaq	1575(%rdi), %rsi
	testb	$1, %r15b
	je	.L2099
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L3008
.L2099:
	movq	%r15, %rdx
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$215, %ecx
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$2, %r8d
	movq	%rax, %r15
	leaq	3856(%r12), %rax
	movq	%r15, %rcx
	movq	%rax, %rdx
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	16(%rbx), %rax
	movq	(%r15), %r15
	movq	(%rax), %rdi
	movq	%r15, 1599(%rdi)
	leaq	1599(%rdi), %rsi
	testb	$1, %r15b
	je	.L2488
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -256(%rbp)
	testl	$262144, %eax
	jne	.L3009
	testb	$24, %al
	je	.L2488
.L3131:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3010
	.p2align 4,,10
	.p2align 3
.L2488:
	movq	(%rbx), %rdi
	leaq	.LC158(%rip), %rdx
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$748, %ecx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1583(%rdi)
	leaq	1583(%rdi), %rsi
	testb	$1, %r15b
	je	.L2487
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -256(%rbp)
	testl	$262144, %eax
	jne	.L3011
	testb	$24, %al
	je	.L2487
.L3130:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3012
	.p2align 4,,10
	.p2align 3
.L2487:
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movq	%r14, %rsi
	movl	$734, %ecx
	leaq	.LC159(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$219, %ecx
	movl	$2, %r9d
	leaq	.LC160(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$220, %ecx
	movl	$2, %r9d
	leaq	.LC161(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$764, %ecx
	movl	$2, %r9d
	leaq	.LC162(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$730, %ecx
	movl	$2, %r9d
	leaq	.LC163(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$782, %ecx
	movl	$2, %r9d
	leaq	.LC164(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$775, %ecx
	movl	$2, %r9d
	leaq	.LC165(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$770, %ecx
	movl	$2, %r9d
	leaq	.LC166(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$756, %ecx
	movl	$2, %r9d
	leaq	.LC132(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$757, %ecx
	movl	$2, %r9d
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	leaq	.LC143(%rip), %rdx
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC121(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC144(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC145(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC146(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC160(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC161(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC155(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC120(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC123(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_125InstallTrueValuedPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKc
	xorl	%esi, %esi
	movq	%r15, %rdi
	leaq	.LC20(%rip), %rdx
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rcx
	movq	%r14, %rsi
	leaq	3928(%r12), %rax
	movl	$3, %r8d
	movq	%rax, %rdx
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %r15
	movq	(%r14), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2107
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L2108:
	movq	(%rbx), %rdx
	movl	$1, %esi
	call	_ZN2v88internal3Map27SetShouldBeFastPrototypeMapENS0_6HandleIS1_EEbPNS0_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	(%rbx), %r14
	movq	(%rax), %rax
	movq	495(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2110
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2111:
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate15object_functionEv
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%r15, %rsi
	leaq	2080(%r12), %r15
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$216, %ecx
	leaq	.LC44(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	pushq	$166
	movl	$48, %ecx
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movl	$1060, %edx
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movq	(%rax), %rdx
	movq	23(%rdx), %rcx
	movl	47(%rcx), %edx
	andl	$-33, %edx
	movl	%edx, 47(%rcx)
	movq	(%rax), %rax
	movq	16(%rbx), %rdx
	movq	55(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 447(%rdi)
	leaq	447(%rdi), %rsi
	popq	%r11
	popq	%rax
	testb	$1, %r15b
	je	.L2486
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -280(%rbp)
	testl	$262144, %eax
	jne	.L3013
	testb	$24, %al
	je	.L2486
.L3125:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3014
	.p2align 4,,10
	.p2align 3
.L2486:
	movq	16(%rbx), %rax
	movq	(%r14), %r14
	movq	(%rax), %r15
	movq	%r14, 455(%r15)
	leaq	455(%r15), %rsi
	testb	$1, %r14b
	je	.L2485
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -280(%rbp)
	testl	$262144, %eax
	jne	.L3015
	testb	$24, %al
	je	.L2485
.L3124:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3016
	.p2align 4,,10
	.p2align 3
.L2485:
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	-184(%rbp), %rsi
	movq	(%rbx), %rdi
	movl	$1041, %ecx
	movq	%rax, %r8
	movl	$420, %r9d
	leaq	.LC167(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movl	$-1, %r9d
	movl	$1, %r10d
	movl	$107, %edx
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	%r15, %rsi
	movq	23(%rax), %rax
	movw	%r9w, 41(%rax)
	movq	(%r15), %rax
	movq	23(%rax), %rax
	movw	%r10w, 39(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rdx
	movq	(%rax), %rdi
	movq	%rax, %r14
	movq	%rdx, 23(%rdi)
	leaq	23(%rdi), %rsi
	movq	%rdx, -280(%rbp)
	movq	%rsi, -296(%rbp)
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-280(%rbp), %rdx
	testb	$1, %dl
	jne	.L3017
.L2119:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	-192(%rbp), %rdx
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$428, %ecx
	leaq	.LC168(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$429, %ecx
	leaq	.LC169(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$431, %ecx
	leaq	.LC170(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$432, %ecx
	leaq	.LC18(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$433, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC131(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$430, %ecx
	leaq	.LC132(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$421, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC171(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$422, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC172(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$423, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC173(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$424, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC174(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$425, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC175(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	-240(%rbp), %r14
	movq	(%rbx), %rdi
	movl	$2, %r8d
	movq	%rax, %rcx
	leaq	.LC175(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$426, %ecx
	movl	$1, %r8d
	leaq	.LC176(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movl	$2, %r8d
	movq	%r14, %rsi
	movq	%rax, %rcx
	leaq	.LC176(%rip), %rdx
	leaq	1088(%r12), %r14
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movsd	.LC177(%rip), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	leaq	.LC178(%rip), %rdx
	movq	%r15, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movsd	.LC179(%rip), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC180(%rip), %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r15, %rsi
	leaq	.LC181(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movsd	.LC182(%rip), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC183(%rip), %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	leaq	1104(%r12), %r8
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movq	%r8, %rcx
	leaq	.LC184(%rip), %rdx
	movq	%r8, -240(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movsd	.LC185(%rip), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	leaq	.LC186(%rip), %rdx
	movq	%r15, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movsd	.LC187(%rip), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	leaq	.LC188(%rip), %rdx
	movq	%r15, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movsd	.LC189(%rip), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC190(%rip), %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	-240(%rbp), %r8
	movq	(%rbx), %rdi
	leaq	.LC191(%rip), %rdx
	movq	-184(%rbp), %r15
	movq	%r8, %rcx
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r15, %rsi
	leaq	.LC181(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	88(%r12), %rcx
	leaq	.LC192(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	(%rbx), %rdi
	movl	$1041, %ecx
	movq	%r15, %rsi
	movl	$791, %r9d
	movq	%rax, %r8
	leaq	.LC193(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movl	$-1, %edi
	movl	$1, %r8d
	movl	$30, %edx
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	%r15, %rsi
	movq	23(%rax), %rax
	movw	%di, 41(%rax)
	movq	(%r15), %rax
	movq	23(%rax), %rax
	movw	%r8w, 39(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	120(%rax), %rdx
	leaq	23(%rdi), %rsi
	movq	%rdx, 23(%rdi)
	movq	%rdx, -240(%rbp)
	movq	%rsi, -288(%rbp)
	movq	%rdi, -280(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-240(%rbp), %rdx
	testb	$1, %dl
	jne	.L3018
.L2121:
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	-192(%rbp), %rdx
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movl	$240, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC18(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movl	$241, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC131(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	-184(%rbp), %rsi
	movq	(%rbx), %rdi
	movl	$888, %r9d
	movq	%rax, %r8
	movl	$1041, %ecx
	leaq	.LC194(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movl	$-1, %ecx
	movl	$1, %esi
	movl	$178, %edx
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movw	%cx, 41(%rax)
	movq	(%r15), %rax
	movq	23(%rax), %rax
	movw	%si, 39(%rax)
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	16(%rbx), %rax
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	1439(%rax), %rax
	movq	41112(%rdx), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2123
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L2124:
	movzbl	14(%rsi), %eax
	movl	$1, %edx
	andb	$7, %al
	orl	$120, %eax
	movb	%al, 14(%rsi)
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	-232(%rbp), %rsi
	movl	$7, %ecx
	movq	%r13, %rdi
	leaq	4448(%r12), %rdx
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r14), %rax
	movq	(%rbx), %rsi
	movq	%r13, %rdx
	movq	-208(%rbp), %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$589, %ecx
	movl	$2, %r9d
	leaq	.LC195(%rip), %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$588, %ecx
	movl	$2, %r9d
	leaq	.LC196(%rip), %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$603, %ecx
	movl	$2, %r9d
	leaq	.LC197(%rip), %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	128(%rax), %rdx
	leaq	23(%rdi), %rsi
	movq	%rdx, 23(%rdi)
	movq	%rdx, -240(%rbp)
	movq	%rsi, -288(%rbp)
	movq	%rdi, -280(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-240(%rbp), %rdx
	testb	$1, %dl
	jne	.L3019
.L2126:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	16(%rbx), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rdi
	movq	%rdx, 567(%rdi)
	leaq	567(%rdi), %rsi
	testb	$1, %dl
	je	.L2484
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -240(%rbp)
	testl	$262144, %eax
	jne	.L3020
	testb	$24, %al
	je	.L2484
.L3140:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3021
	.p2align 4,,10
	.p2align 3
.L2484:
	movq	-192(%rbp), %rdx
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r15, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$893, %ecx
	leaq	.LC198(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$894, %ecx
	leaq	.LC199(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$895, %ecx
	leaq	.LC200(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$896, %ecx
	leaq	.LC201(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$884, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC202(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$885, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC203(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$886, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC204(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$887, %ecx
	leaq	.LC142(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$891, %ecx
	leaq	.LC205(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$897, %ecx
	leaq	.LC206(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$898, %ecx
	leaq	.LC207(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$899, %ecx
	leaq	.LC208(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$590, %ecx
	leaq	.LC155(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$591, %ecx
	leaq	.LC156(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$900, %ecx
	leaq	.LC209(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$592, %ecx
	leaq	.LC147(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$901, %ecx
	leaq	.LC210(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$595, %ecx
	leaq	.LC211(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$593, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC212(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$594, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC213(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1051, %ecx
	leaq	.LC214(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$909, %ecx
	leaq	.LC215(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$908, %ecx
	leaq	.LC216(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$911, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC217(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$596, %ecx
	movl	$1, %r8d
	leaq	.LC218(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$597, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC219(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$912, %ecx
	leaq	.LC102(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$902, %ecx
	leaq	.LC220(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$598, %ecx
	leaq	.LC221(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$903, %ecx
	leaq	.LC222(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$904, %ecx
	leaq	.LC223(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$599, %ecx
	leaq	.LC224(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$914, %ecx
	leaq	.LC225(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$905, %ecx
	leaq	.LC226(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$913, %ecx
	leaq	.LC227(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$881, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC18(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$600, %ecx
	leaq	.LC228(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$602, %ecx
	leaq	.LC229(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$2, %r8d
	movq	%rax, %rcx
	leaq	.LC230(%rip), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$601, %ecx
	leaq	.LC231(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$2, %r8d
	movq	%rax, %rcx
	leaq	.LC232(%rip), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$1052, %ecx
	leaq	.LC233(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$1053, %ecx
	leaq	.LC234(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$1054, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC235(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$1055, %ecx
	leaq	.LC236(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$882, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC131(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	pushq	$2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	pushq	$1
	movq	-224(%rbp), %rdx
	xorl	%r9d, %r9d
	movl	$906, %r8d
	leaq	.LC69(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	16(%rbx), %rax
	movq	(%rbx), %r14
	movq	(%rax), %rax
	movq	495(%rax), %rsi
	movq	41112(%r14), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L2131
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2132:
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate15object_functionEv
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC237(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$15, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$907, %ecx
	leaq	.LC44(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	leaq	.LC238(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	$14, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r14, %r9
	pushq	$166
	movq	%rax, %rsi
	movl	$40, %ecx
	xorl	%r8d, %r8d
	movl	$1080, %edx
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movq	(%rax), %rdx
	movq	23(%rdx), %rcx
	movl	47(%rcx), %edx
	andl	$-33, %edx
	movl	%edx, 47(%rcx)
	movq	(%rax), %rax
	movq	16(%rbx), %rdx
	movq	55(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 551(%rdi)
	leaq	551(%rdi), %rsi
	popq	%r11
	popq	%rax
	testb	$1, %r15b
	je	.L2134
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L3022
.L2134:
	movq	%r15, %rdx
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	16(%rbx), %rax
	movq	(%r14), %r14
	movq	(%rax), %r15
	movq	%r14, 559(%r15)
	leaq	559(%r15), %rsi
	testb	$1, %r14b
	je	.L2483
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -192(%rbp)
	testl	$262144, %eax
	jne	.L3023
	testb	$24, %al
	je	.L2483
.L3139:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3024
	.p2align 4,,10
	.p2align 3
.L2483:
	leaq	96(%r12), %rax
	movq	-184(%rbp), %rsi
	movq	(%rbx), %rdi
	movl	$604, %r9d
	movq	%rax, %r8
	movl	$1041, %ecx
	leaq	.LC239(%rip), %rdx
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	xorl	%r9d, %r9d
	movl	$-1, %r10d
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movw	%r9w, 39(%rax)
	movq	(%r14), %rax
	movq	23(%rax), %rax
	movw	%r10w, 41(%rax)
	movq	16(%rbx), %rax
	movq	(%r14), %r15
	movq	(%rax), %rdi
	movq	%r15, 1455(%rdi)
	leaq	1455(%rdi), %rsi
	testb	$1, %r15b
	je	.L2482
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -240(%rbp)
	testl	$262144, %eax
	jne	.L3025
	testb	$24, %al
	je	.L2482
.L3146:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3026
	.p2align 4,,10
	.p2align 3
.L2482:
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movq	%r14, %rsi
	movl	$605, %ecx
	leaq	.LC240(%rip), %rdx
	leaq	3920(%r12), %r15
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$606, %ecx
	movl	$2, %r9d
	leaq	.LC241(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	leaq	3848(%r12), %rcx
	movq	%r14, %rsi
	leaq	.LC242(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	-264(%rbp), %rcx
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	leaq	.LC243(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	(%rbx), %rdi
	leaq	3944(%r12), %rcx
	movq	%r14, %rsi
	leaq	.LC244(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	-224(%rbp), %rcx
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	leaq	.LC245(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	leaq	3880(%r12), %rax
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %rcx
	leaq	.LC212(%rip), %rdx
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	leaq	3872(%r12), %rax
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %rcx
	leaq	.LC213(%rip), %rdx
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	leaq	3888(%r12), %rax
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %rcx
	leaq	.LC218(%rip), %rdx
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	leaq	3896(%r12), %rax
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %rcx
	leaq	.LC219(%rip), %rdx
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	(%rbx), %rdi
	leaq	3904(%r12), %rcx
	movq	%r14, %rsi
	leaq	.LC246(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	leaq	3912(%r12), %rax
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %rcx
	leaq	.LC221(%rip), %rdx
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	(%rbx), %rdi
	movq	%r15, %rcx
	movq	%r14, %rsi
	leaq	.LC247(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	leaq	3952(%r12), %rax
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %rcx
	leaq	.LC248(%rip), %rdx
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	-272(%rbp), %rcx
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	leaq	.LC249(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	(%r14), %rax
	movq	(%rbx), %rdx
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L2143
	movq	23(%rsi), %rsi
.L2143:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2144
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2145:
	movq	(%rbx), %rdi
	leaq	.LC239(%rip), %rax
	movq	%r13, %rsi
	movq	$6, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-272(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$609, %ecx
	leaq	.LC18(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$610, %ecx
	leaq	.LC131(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	leaq	.LC250(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	$11, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$607, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	pushq	$3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	pushq	$1
	movl	$1, %r9d
	movl	$608, %r8d
	movq	%r15, %rdx
	leaq	.LC251(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	-192(%rbp), %r9
	movq	(%rbx), %rdi
	movl	$96, %r8d
	movq	-184(%rbp), %rsi
	movl	$1066, %ecx
	leaq	.LC252(%rip), %rdx
	movl	$286, (%rsp)
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	movq	(%rbx), %rdi
	movl	$39, %edx
	movq	%rax, %r14
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%r14), %rax
	movl	$7, %ecx
	xorl	%r8d, %r8d
	movl	$-1, %esi
	movl	$2, %r9d
	leaq	.LC253(%rip), %rdx
	movq	23(%rax), %rax
	movw	%cx, 39(%rax)
	movq	(%r14), %rax
	movl	$309, %ecx
	movq	23(%rax), %rax
	movw	%si, 41(%rax)
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$310, %ecx
	movl	$2, %r9d
	leaq	.LC254(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	movq	%r14, %rsi
	movl	$7, %r8d
	leaq	.LC255(%rip), %rdx
	movl	$332, %ecx
	movl	$2, (%rsp)
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	(%r14), %rax
	movq	(%rbx), %rdx
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	popq	%rdi
	popq	%r8
	cmpw	$68, 11(%rax)
	jne	.L2148
	movq	23(%rsi), %rsi
.L2148:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2149
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2150:
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$329, %ecx
	leaq	.LC18(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$326, %ecx
	leaq	.LC256(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$330, %ecx
	leaq	.LC257(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$327, %ecx
	leaq	.LC258(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$328, %ecx
	leaq	.LC259(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$2, %r8d
	movq	%rax, %rcx
	leaq	.LC260(%rip), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$287, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC261(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$311, %ecx
	leaq	.LC262(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$288, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC263(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$289, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC264(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$312, %ecx
	leaq	.LC265(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$290, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC266(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	subq	$8, %rsp
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	pushq	$2
	movq	%r14, %rsi
	movl	$4, %r8d
	movl	$313, %ecx
	leaq	.LC267(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$291, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC268(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$314, %ecx
	leaq	.LC269(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$292, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC270(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$315, %ecx
	leaq	.LC271(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$293, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC272(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$316, %ecx
	leaq	.LC273(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$294, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC274(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$317, %ecx
	leaq	.LC275(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$295, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC276(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$318, %ecx
	leaq	.LC277(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$296, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC278(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$297, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC279(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$319, %ecx
	leaq	.LC280(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$298, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC281(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$299, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC282(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$320, %ecx
	leaq	.LC283(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$300, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC284(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	movq	%r14, %rsi
	movl	$4, %r8d
	movl	$321, %ecx
	leaq	.LC285(%rip), %rdx
	movl	$2, (%rsp)
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$301, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC286(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$322, %ecx
	leaq	.LC287(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$302, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC288(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$323, %ecx
	leaq	.LC289(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$303, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC290(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$324, %ecx
	leaq	.LC291(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$304, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC292(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$325, %ecx
	leaq	.LC293(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$305, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC131(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$307, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC294(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$308, %ecx
	leaq	.LC295(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$331, %ecx
	leaq	.LC296(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$997, %ecx
	leaq	.LC132(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$996, %ecx
	leaq	.LC297(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$998, %ecx
	leaq	.LC298(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	pushq	$3
	movq	(%rbx), %rdi
	movq	%r15, %rdx
	pushq	$1
	movq	%r14, %rsi
	movl	$1, %r9d
	movl	$306, %r8d
	leaq	.LC251(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	-200(%rbp), %rsi
	movq	(%rbx), %rdi
	addq	$32, %rsp
	call	_ZN2v88internal12_GLOBAL__N_137SimpleCreateBuiltinSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEi.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 951(%r15)
	leaq	951(%r15), %rsi
	testb	$1, %r14b
	je	.L2481
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3027
	testb	$24, %al
	je	.L2481
.L3145:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3028
	.p2align 4,,10
	.p2align 3
.L2481:
	subq	$8, %rsp
	movq	-192(%rbp), %r9
	movq	-184(%rbp), %rsi
	movl	$48, %r8d
	pushq	$501
	movq	(%rbx), %rdi
	movl	$1074, %ecx
	leaq	.LC299(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	movq	(%rbx), %rdi
	movl	$212, %edx
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%rbx), %r15
	movq	(%r14), %rax
	movq	23(%rax), %rsi
	movq	41112(%r15), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L2155
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2156:
	movl	$1, %r11d
	movl	$1, %r15d
	movw	%r11w, 41(%rsi)
	movq	(%rax), %rax
	movq	%r14, %rsi
	movw	%r15w, 39(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal12_GLOBAL__N_120InstallSpeciesGetterEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$517, %ecx
	leaq	.LC300(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1695(%rdi)
	leaq	1695(%rdi), %rsi
	testb	$1, %r15b
	je	.L2480
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3029
	testb	$24, %al
	je	.L2480
.L3148:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3030
	.p2align 4,,10
	.p2align 3
.L2480:
	movq	(%rbx), %rdi
	movl	$1, %r8d
	movl	$519, %ecx
	movq	%r14, %rsi
	leaq	.LC301(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	(%rbx), %rdi
	movl	$509, %ecx
	movq	%r14, %rsi
	movl	$1, %r8d
	leaq	.LC302(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	(%rbx), %rdi
	movl	$511, %ecx
	movq	%r14, %rsi
	movl	$1, %r8d
	leaq	.LC303(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	(%r14), %rax
	movq	(%rbx), %rdx
	movq	55(%rax), %r8
	movq	-1(%r8), %rax
	cmpw	$68, 11(%rax)
	jne	.L2162
	movq	23(%r8), %r8
.L2162:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2163
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r8
	movq	%rax, %r15
.L2164:
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	movq	%r8, 1031(%rdi)
	leaq	1031(%rdi), %rsi
	testb	$1, %r8b
	je	.L2479
	movq	%r8, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3031
	testb	$24, %al
	je	.L2479
.L3147:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3032
	.p2align 4,,10
	.p2align 3
.L2479:
	movq	(%rbx), %rdi
	leaq	3088(%r12), %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$503, %ecx
	leaq	.LC304(%rip), %rdx
	movl	$2, %r8d
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1935(%rdi)
	leaq	1935(%rdi), %rsi
	testb	$1, %dl
	je	.L2478
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3033
	testb	$24, %al
	je	.L2478
.L3142:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3034
	.p2align 4,,10
	.p2align 3
.L2478:
	movq	(%rbx), %rdi
	leaq	.LC305(%rip), %rdx
	movq	%r15, %rsi
	movl	$1, %r8d
	movl	$505, %ecx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1703(%rdi)
	leaq	1703(%rdi), %rsi
	testb	$1, %dl
	je	.L2477
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3035
	testb	$24, %al
	je	.L2477
.L3141:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3036
	.p2align 4,,10
	.p2align 3
.L2477:
	movq	(%rbx), %rdi
	movl	$1, %r8d
	movl	$512, %ecx
	movq	%r15, %rsi
	leaq	.LC306(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	movl	$513, %esi
	leaq	128(%rdi), %rdx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	(%rax), %rcx
	movl	47(%rcx), %edx
	orl	$32, %edx
	movl	%edx, 47(%rcx)
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 975(%rdi)
	leaq	975(%rdi), %rsi
	testb	$1, %dl
	je	.L2476
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3037
	testb	$24, %al
	je	.L2476
.L3144:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3038
	.p2align 4,,10
	.p2align 3
.L2476:
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	movl	$514, %esi
	leaq	128(%rdi), %rdx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	(%rax), %rcx
	movl	47(%rcx), %edx
	orl	$32, %edx
	movl	%edx, 47(%rcx)
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 983(%rdi)
	leaq	983(%rdi), %rsi
	testb	$1, %dl
	je	.L2475
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3039
	testb	$24, %al
	je	.L2475
.L3143:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3040
	.p2align 4,,10
	.p2align 3
.L2475:
	movq	(%rbx), %rdi
	movl	$515, %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	128(%rdi), %rdx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 991(%rdi)
	leaq	991(%rdi), %rsi
	testb	$1, %dl
	je	.L2474
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3041
	testb	$24, %al
	je	.L2474
.L3121:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3042
	.p2align 4,,10
	.p2align 3
.L2474:
	movq	(%rbx), %rdi
	movl	$516, %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	128(%rdi), %rdx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 999(%rdi)
	leaq	999(%rdi), %rsi
	testb	$1, %dl
	je	.L2473
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3043
	testb	$24, %al
	je	.L2473
.L3120:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3044
	.p2align 4,,10
	.p2align 3
.L2473:
	leaq	.LC20(%rip), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc@PLT
	movq	(%rbx), %rdx
	movq	(%r15), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2187
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L2188:
	movq	(%rbx), %rdx
	movl	$1, %esi
	call	_ZN2v88internal3Map27SetShouldBeFastPrototypeMapENS0_6HandleIS1_EEbPNS0_7IsolateE@PLT
	movq	(%rbx), %rdi
	movl	$502, %edx
	xorl	%r8d, %r8d
	movq	-200(%rbp), %rsi
	movl	$1, %ecx
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1927(%rdi)
	leaq	1927(%rdi), %rsi
	testb	$1, %r15b
	je	.L2472
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3045
	testb	$24, %al
	je	.L2472
.L3119:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3046
	.p2align 4,,10
	.p2align 3
.L2472:
	movq	-200(%rbp), %rdx
	movq	(%rbx), %rdi
	movl	$16, %ecx
	movl	$497, %esi
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	(%rax), %rcx
	movl	47(%rcx), %edx
	orl	$32, %edx
	movl	%edx, 47(%rcx)
	movq	(%rax), %rcx
	movl	47(%rcx), %edx
	andl	$-1015809, %edx
	orl	$229376, %edx
	movl	%edx, 47(%rcx)
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 967(%rdi)
	leaq	967(%rdi), %rsi
	testb	$1, %r15b
	je	.L2193
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L3047
.L2193:
	movq	%r15, %rdx
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-200(%rbp), %rdx
	movq	(%rbx), %rdi
	movl	$16, %ecx
	movl	$496, %esi
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	(%rax), %rcx
	movl	47(%rcx), %edx
	orl	$32, %edx
	movl	%edx, 47(%rcx)
	movq	(%rax), %rcx
	movl	47(%rcx), %edx
	andl	$-1015809, %edx
	orl	$229376, %edx
	movl	%edx, 47(%rcx)
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 959(%rdi)
	leaq	959(%rdi), %rsi
	testb	$1, %r15b
	je	.L2471
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3048
	testb	$24, %al
	je	.L2471
.L3118:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3049
	.p2align 4,,10
	.p2align 3
.L2471:
	movq	-200(%rbp), %rdx
	movq	(%rbx), %rdi
	movl	$518, %esi
	xorl	%ecx, %ecx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE.constprop.0
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1007(%rdi)
	leaq	1007(%rdi), %rsi
	testb	$1, %r15b
	je	.L2470
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3050
	testb	$24, %al
	je	.L2470
.L3123:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3051
	.p2align 4,,10
	.p2align 3
.L2470:
	movq	%r14, %rdi
	leaq	.LC20(%rip), %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc@PLT
	pushq	$542
	movq	(%rbx), %rdi
	movl	$1, %r9d
	pushq	-192(%rbp)
	movl	$56, %r8d
	movq	-184(%rbp), %rsi
	movl	$1075, %ecx
	leaq	.LC307(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movq	(%rbx), %rdi
	movl	$129, %edx
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%rbx), %r15
	movq	(%r14), %rax
	movq	23(%rax), %rsi
	movq	41112(%r15), %rdi
	popq	%r9
	popq	%r10
	testq	%rdi, %rdi
	je	.L2201
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2202:
	movl	$2, %edi
	movl	$2, %r8d
	movw	%di, 41(%rsi)
	movq	(%rax), %rax
	movw	%r8w, 39(%rax)
	movq	(%r14), %rax
	movq	(%rbx), %rdx
	movq	55(%rax), %r8
	movq	-1(%r8), %rax
	cmpw	$68, 11(%rax)
	jne	.L2205
	movq	23(%r8), %r8
.L2205:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2206
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r8
	movq	%rax, %r15
.L2207:
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	movq	%r8, 1079(%rdi)
	leaq	1079(%rdi), %rsi
	testb	$1, %r8b
	je	.L2469
	movq	%r8, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3052
	testb	$24, %al
	je	.L2469
.L3122:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3053
	.p2align 4,,10
	.p2align 3
.L2469:
	movq	(%rbx), %rdi
	leaq	.LC308(%rip), %rdx
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	movl	$549, %ecx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1039(%rdi)
	leaq	1039(%rdi), %rsi
	testb	$1, %dl
	je	.L2468
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	jne	.L3054
	testb	$24, %al
	je	.L2468
.L3155:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3055
	.p2align 4,,10
	.p2align 3
.L2468:
	movq	(%rbx), %rdi
	leaq	2456(%r12), %rdx
	movl	$877, %ecx
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movl	$880, %ecx
	movq	%r15, %rsi
	leaq	2528(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movl	$874, %ecx
	movq	%r15, %rsi
	leaq	2632(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movl	$875, %ecx
	movq	%r15, %rsi
	leaq	2664(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movl	$876, %ecx
	movq	%r15, %rsi
	leaq	2864(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movl	$871, %ecx
	movq	%r15, %rsi
	leaq	3304(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movl	$878, %ecx
	movq	%r15, %rsi
	leaq	3336(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movl	$879, %ecx
	movq	%r15, %rsi
	leaq	3528(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movl	$548, %ecx
	movq	%r15, %rsi
	movl	$1, %r8d
	leaq	.LC309(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$552, %ecx
	movl	$2, %r9d
	leaq	.LC18(%rip), %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movl	$872, %ecx
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC310(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	pushq	$2
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	pushq	$1
	movq	-264(%rbp), %rdx
	leaq	.LC311(%rip), %rcx
	movl	$1, %r9d
	movl	$868, %r8d
	call	_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1071(%rdi)
	leaq	1071(%rdi), %rsi
	popq	%rax
	popq	%rcx
	testb	$1, %dl
	je	.L2467
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L3056
	testb	$24, %al
	je	.L2467
.L3154:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3057
	.p2align 4,,10
	.p2align 3
.L2467:
	pushq	$2
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$1, %r9d
	pushq	$1
	movq	-280(%rbp), %rdx
	movl	$550, %r8d
	leaq	.LC312(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1063(%rdi)
	leaq	1063(%rdi), %rsi
	popq	%r10
	popq	%r11
	testb	$1, %dl
	je	.L2466
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L3058
	testb	$24, %al
	je	.L2466
.L3157:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3059
	.p2align 4,,10
	.p2align 3
.L2466:
	pushq	$2
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movq	%r15, %rsi
	pushq	$0
	movq	-288(%rbp), %rdx
	movl	$870, %r8d
	leaq	.LC313(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1095(%rdi)
	leaq	1095(%rdi), %rsi
	popq	%r8
	popq	%r9
	testb	$1, %dl
	je	.L2465
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L3060
	testb	$24, %al
	je	.L2465
.L3156:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3061
	.p2align 4,,10
	.p2align 3
.L2465:
	pushq	$2
	movq	(%rbx), %rdi
	leaq	.LC314(%rip), %rcx
	movq	%r15, %rsi
	pushq	$1
	movq	-296(%rbp), %rdx
	movl	$1, %r9d
	movl	$551, %r8d
	call	_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1111(%rdi)
	leaq	1111(%rdi), %rsi
	popq	%rax
	popq	%rcx
	testb	$1, %dl
	je	.L2464
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L3062
	testb	$24, %al
	je	.L2464
.L3159:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3063
	.p2align 4,,10
	.p2align 3
.L2464:
	pushq	$2
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$2, %r9d
	pushq	$0
	movq	-304(%rbp), %rdx
	movl	$554, %r8d
	leaq	.LC315(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_123InstallFunctionAtSymbolEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6SymbolEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1119(%rdi)
	leaq	1119(%rdi), %rsi
	popq	%r10
	popq	%r11
	testb	$1, %dl
	je	.L2463
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L3064
	testb	$24, %al
	je	.L2463
.L3158:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3065
	.p2align 4,,10
	.p2align 3
.L2463:
	movq	(%rbx), %rdx
	movq	(%r15), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2230
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2231:
	movq	(%rbx), %rdx
	movq	%r15, %rdi
	movl	$1, %esi
	call	_ZN2v88internal3Map27SetShouldBeFastPrototypeMapENS0_6HandleIS1_EEbPNS0_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	(%r15), %r15
	movq	(%rax), %rdi
	movq	%r15, 1087(%rdi)
	leaq	1087(%rdi), %rsi
	testb	$1, %r15b
	je	.L2462
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L3066
	testb	$24, %al
	je	.L2462
.L3150:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3067
	.p2align 4,,10
	.p2align 3
.L2462:
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_120InstallSpeciesGetterEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	movq	(%rbx), %rdi
	movl	$543, %ecx
	movq	%r14, %rsi
	leaq	2712(%r12), %rdx
	movl	$544, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC316(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$543, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$544, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC317(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$9, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$545, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC318(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$545, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC319(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$9, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$546, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC320(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$546, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC321(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$11, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$547, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC322(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$547, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC323(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$12, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$553, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC324(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$553, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC325(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$533, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC326(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$534, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC327(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$535, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC328(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$536, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC329(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$537, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC330(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$538, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC331(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$539, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC332(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$540, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC333(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$2, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	movl	$541, %ecx
	movq	%rax, %rdx
	movl	$165, %r8d
	call	_ZN2v88internal12_GLOBAL__N_125SimpleInstallGetterSetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEENS0_8Builtins4NameESA_
	movq	(%rbx), %r15
	movq	(%r14), %rax
	movq	41112(%r15), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2236
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2237:
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rbx), %rsi
	movl	$4, %r9d
	xorl	%ecx, %ecx
	movl	$6, %r8d
	leaq	2760(%r12), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	-264(%rbp), %rax
	movq	(%rbx), %rsi
	movq	%r13, %rdx
	movq	-208(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewRegExpMatchInfoEv@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1055(%rdi)
	leaq	1055(%rdi), %rsi
	testb	$1, %r15b
	je	.L2461
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L3068
	testb	$24, %al
	je	.L2461
.L3149:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3069
	.p2align 4,,10
	.p2align 3
.L2461:
	movq	-200(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movabsq	$4294967296, %rcx
	movq	(%rax), %rdx
	movq	%rcx, 23(%rdx)
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1239(%rdi)
	leaq	1239(%rdi), %rsi
	testb	$1, %r15b
	je	.L2460
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L3070
	testb	$24, %al
	je	.L2460
.L3153:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3071
	.p2align 4,,10
	.p2align 3
.L2460:
	movq	%r14, %rdi
	xorl	%esi, %esi
	leaq	.LC20(%rip), %rdx
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc@PLT
	movq	16(%rbx), %rax
	movq	(%rbx), %r14
	movq	(%rax), %rax
	movq	495(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2245
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2246:
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate15object_functionEv
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC334(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$22, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$561, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC44(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %r8
	movl	$48, %ecx
	movl	$1076, %edx
	leaq	.LC335(%rip), %rsi
	movl	$166, %r9d
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateEPKcNS0_12InstanceTypeEiiNS0_6HandleINS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movq	(%rax), %rdx
	movq	23(%rdx), %rcx
	movl	47(%rcx), %edx
	andl	$-33, %edx
	movl	%edx, 47(%rcx)
	movq	16(%rbx), %rdx
	movq	(%rax), %rax
	movq	(%rdx), %r15
	movq	55(%rax), %r14
	movq	%r14, 1127(%r15)
	leaq	1127(%r15), %rsi
	testb	$1, %r14b
	je	.L2459
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L3072
	testb	$24, %al
	je	.L2459
.L3151:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3073
	.p2align 4,,10
	.p2align 3
.L2459:
	movq	-184(%rbp), %r15
	movq	(%rbx), %rdi
	movl	$199, %ecx
	leaq	2480(%r12), %rdx
	movq	%r15, %rsi
	call	_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi
	movq	(%rbx), %rdi
	movl	$228, %edx
	movl	$336, %esi
	call	_ZN2v88internal12_GLOBAL__N_116InstallMakeErrorEPNS0_7IsolateEii
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$201, %ecx
	leaq	2504(%r12), %rdx
	call	_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi
	movq	(%rbx), %rdi
	movl	$213, %ecx
	movq	%r15, %rsi
	leaq	3136(%r12), %rdx
	call	_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi
	movq	(%rbx), %rdi
	movl	$229, %edx
	movl	$337, %esi
	call	_ZN2v88internal12_GLOBAL__N_116InstallMakeErrorEPNS0_7IsolateEii
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$214, %ecx
	leaq	3152(%r12), %rdx
	call	_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi
	movq	(%rbx), %rdi
	movl	$218, %ecx
	movq	%r15, %rsi
	leaq	3392(%r12), %rdx
	call	_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi
	movq	(%rbx), %rdi
	movl	$230, %edx
	movl	$338, %esi
	call	_ZN2v88internal12_GLOBAL__N_116InstallMakeErrorEPNS0_7IsolateEii
	movq	(%rbx), %rdi
	movl	$219, %ecx
	movq	%r15, %rsi
	leaq	3472(%r12), %rdx
	call	_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi
	movq	(%rbx), %rdi
	movl	$231, %edx
	movl	$339, %esi
	call	_ZN2v88internal12_GLOBAL__N_116InstallMakeErrorEPNS0_7IsolateEii
	movq	(%rbx), %rdi
	movl	$220, %ecx
	movq	%r15, %rsi
	leaq	3536(%r12), %rdx
	call	_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi
	movq	(%rbx), %rdi
	movl	$232, %edx
	movl	$340, %esi
	call	_ZN2v88internal12_GLOBAL__N_116InstallMakeErrorEPNS0_7IsolateEii
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate15object_functionEv
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movl	$221, %ecx
	leaq	2216(%r12), %rdx
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$222, %ecx
	leaq	2792(%r12), %rdx
	call	_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$223, %ecx
	leaq	3216(%r12), %rdx
	call	_ZN2v88internalL12InstallErrorEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6StringEEEi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewEmbedderDataArrayEiNS0_14AllocationTypeE@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 55(%r15)
	leaq	55(%r15), %rsi
	testb	$1, %r14b
	je	.L2458
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L3074
	testb	$24, %al
	je	.L2458
.L3152:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3075
	.p2align 4,,10
	.p2align 3
.L2458:
	movq	16(%rbx), %rax
	movq	(%rbx), %r14
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2254
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2255:
	movq	-184(%rbp), %r15
	movq	(%rbx), %rdi
	movl	$2, %r8d
	leaq	2640(%r12), %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate15object_functionEv
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$2, %r8d
	movq	%rax, %rcx
	leaq	.LC336(%rip), %rdx
	movq	%rax, %r14
	movq	%r15, -184(%rbp)
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$366, %ecx
	leaq	.LC254(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movl	$367, %ecx
	movq	%r14, %rsi
	movl	$1, %r8d
	leaq	.LC337(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC336(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$4, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate15object_functionEv
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	-184(%rbp), %rsi
	movq	(%rbx), %rdi
	movl	$2, %r8d
	movq	%rax, %rcx
	leaq	.LC338(%rip), %rdx
	movq	%rax, %r14
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movl	$409, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC339(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$820, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC340(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$821, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC341(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$822, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC342(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$823, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC343(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$824, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC344(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$826, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC345(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$825, %ecx
	movq	%r14, %rsi
	movl	$1, %r8d
	leaq	.LC346(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movl	$410, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC347(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$827, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC348(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$832, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC349(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$828, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC350(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$829, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC351(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$830, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC352(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$831, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC353(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$411, %ecx
	leaq	.LC354(%rip), %rdx
	movl	$2, %r9d
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1903(%rdi)
	leaq	1903(%rdi), %rsi
	testb	$1, %r15b
	je	.L2457
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L3076
	testb	$24, %al
	je	.L2457
.L3161:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3077
	.p2align 4,,10
	.p2align 3
.L2457:
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movq	%r14, %rsi
	movl	$1, %r8d
	movl	$833, %ecx
	leaq	.LC355(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$844, %ecx
	leaq	.LC356(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movl	$412, %ecx
	movq	%r14, %rsi
	movl	$1, %r8d
	leaq	.LC357(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movl	$834, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC358(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$835, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC359(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$837, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC360(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$836, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC361(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$413, %ecx
	leaq	.LC362(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$414, %ecx
	leaq	.LC363(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$415, %ecx
	leaq	.LC364(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1911(%rdi)
	leaq	1911(%rdi), %rsi
	testb	$1, %r15b
	je	.L2456
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L3078
	testb	$24, %al
	je	.L2456
.L3164:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3079
	.p2align 4,,10
	.p2align 3
.L2456:
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$416, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC365(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$417, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC366(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$839, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC367(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$838, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC368(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$840, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC369(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$841, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC370(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$842, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC371(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$843, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC372(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$418, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC373(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movsd	.LC374(%rip), %xmm0
	call	_ZN2v84base7ieee7543expEd@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movsd	%xmm0, -264(%rbp)
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	leaq	.LC375(%rip), %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movsd	.LC376(%rip), %xmm0
	call	_ZN2v84base7ieee7543logEd@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	leaq	.LC377(%rip), %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movsd	.LC378(%rip), %xmm0
	call	_ZN2v84base7ieee7543logEd@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	leaq	.LC379(%rip), %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movsd	-264(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	call	_ZN2v84base7ieee7545log10Ed@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	leaq	.LC380(%rip), %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movsd	-264(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	call	_ZN2v84base7ieee7544log2Ed@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	leaq	.LC381(%rip), %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movsd	.LC382(%rip), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	leaq	.LC383(%rip), %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movsd	.LC384(%rip), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	leaq	.LC385(%rip), %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movsd	.LC386(%rip), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	leaq	.LC387(%rip), %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_115InstallConstantEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS4_INS0_6ObjectEEE
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC338(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$4, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	leaq	.LC388(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	$7, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rbx), %r14
	movq	%rax, %r15
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1279(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2263
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L2264:
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal15NewFunctionArgs22ForFunctionWithoutCodeENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEENS0_12LanguageModeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal7Isolate15object_functionEv
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	-184(%rbp), %rsi
	movq	(%rbx), %rdi
	movq	%r15, %rdx
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%rax, %r14
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$261, %ecx
	leaq	.LC389(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$262, %ecx
	leaq	.LC390(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$263, %ecx
	leaq	.LC391(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$264, %ecx
	leaq	.LC358(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$265, %ecx
	leaq	.LC392(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$266, %ecx
	leaq	.LC393(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$267, %ecx
	leaq	.LC394(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	xorl	%r9d, %r9d
	movl	$268, %ecx
	leaq	.LC395(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$269, %ecx
	leaq	.LC396(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$270, %ecx
	leaq	.LC397(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$271, %ecx
	leaq	.LC398(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$272, %ecx
	leaq	.LC399(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$273, %ecx
	leaq	.LC400(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$274, %ecx
	leaq	.LC401(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$275, %ecx
	leaq	.LC402(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$277, %ecx
	leaq	.LC403(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$278, %ecx
	leaq	.LC404(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$279, %ecx
	leaq	.LC405(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$280, %ecx
	leaq	.LC406(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$281, %ecx
	leaq	.LC407(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$282, %ecx
	leaq	.LC408(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$283, %ecx
	leaq	.LC409(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r9d, %r9d
	movl	$284, %ecx
	movl	$1, %r8d
	leaq	.LC410(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %r15
	movq	-208(%rbp), %rsi
	leaq	.LC411(%rip), %rax
	movq	%rax, -160(%rbp)
	movq	$6, -152(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate15object_functionEv
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	-184(%rbp), %rsi
	movq	(%rbx), %rdi
	movl	$2, %r8d
	movq	%rax, %rcx
	leaq	.LC32(%rip), %rdx
	movq	%rax, %r14
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$1007, %ecx
	movl	$2, %r9d
	leaq	.LC412(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	pushq	$999
	movl	$64, %r8d
	movq	-192(%rbp), %r9
	movl	$1090, %ecx
	leaq	.LC33(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	%r15, %rsi
	movq	23(%rax), %rax
	movw	%dx, 39(%rax)
	movq	(%r15), %rax
	movl	$76, %edx
	movq	23(%rax), %rax
	movw	%cx, 41(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$1006, %ecx
	leaq	.LC46(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdx
	movq	(%r15), %rax
	movq	%r13, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal10JSFunction9prototypeEPNS0_7IsolateE.constprop.0
	movq	-264(%rbp), %rdx
	popq	%r8
	movq	%rax, %rsi
	popq	%r9
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2266
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2267:
	movq	-216(%rbp), %rdx
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$1005, %ecx
	leaq	.LC48(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$1004, %ecx
	leaq	.LC413(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$1001, %ecx
	leaq	1400(%r12), %rax
	movq	%rax, %rdx
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	pushq	$1027
	movl	$56, %r8d
	movq	-192(%rbp), %r9
	movl	$1093, %ecx
	leaq	.LC414(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	xorl	%r9d, %r9d
	movl	$-1, %r10d
	movl	$77, %edx
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	%r15, %rsi
	movq	23(%rax), %rax
	movw	%r9w, 39(%rax)
	movq	(%r15), %rax
	movq	23(%rax), %rax
	movw	%r10w, 41(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$1032, %ecx
	leaq	.LC46(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdx
	movq	(%r15), %rax
	movq	%r13, %rdi
	movq	%rdx, -272(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal10JSFunction9prototypeEPNS0_7IsolateE.constprop.0
	movq	-272(%rbp), %rdx
	popq	%r11
	movq	%rax, %rsi
	popq	%r15
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2269
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2270:
	movq	-216(%rbp), %rdx
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$1031, %ecx
	leaq	.LC48(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$1030, %ecx
	leaq	.LC413(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$1029, %ecx
	movq	-264(%rbp), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	pushq	$991
	movl	$40, %r8d
	movq	-192(%rbp), %r9
	movl	$1089, %ecx
	leaq	.LC415(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	movl	$-1, %edx
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	%r15, %rsi
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	(%rbx), %rdi
	movl	$75, %edx
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$994, %ecx
	movq	%r15, %rsi
	movl	$2, %r9d
	leaq	.LC46(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdx
	movq	(%r15), %rax
	movq	%r13, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal10JSFunction9prototypeEPNS0_7IsolateE.constprop.0
	movq	-264(%rbp), %rdx
	popq	%rcx
	movq	%rax, %rsi
	popq	%r8
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2272
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2273:
	movq	-216(%rbp), %rdx
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$995, %ecx
	leaq	.LC48(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$993, %ecx
	leaq	1240(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	pushq	$1057
	movl	$96, %r8d
	movq	-192(%rbp), %r9
	movl	$1088, %ecx
	leaq	.LC416(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	movl	$-1, %r11d
	movl	$2, %r9d
	xorl	%r8d, %r8d
	movq	%rax, %r15
	movq	(%rax), %rax
	movl	$1069, %ecx
	leaq	.LC46(%rip), %rdx
	movq	%r15, %rsi
	movq	23(%rax), %rax
	movw	%r11w, 41(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdx
	movq	(%r15), %rax
	movq	%r13, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal10JSFunction9prototypeEPNS0_7IsolateE.constprop.0
	movq	-264(%rbp), %rdx
	popq	%r15
	movq	%rax, %rsi
	popq	%rax
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2275
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2276:
	movq	-216(%rbp), %rdx
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$1068, %ecx
	leaq	.LC48(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$1063, %ecx
	leaq	1184(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$1066, %ecx
	leaq	1392(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$1067, %ecx
	leaq	2912(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$1065, %ecx
	leaq	1248(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$1064, %ecx
	leaq	1208(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	pushq	$1033
	movl	$56, %r8d
	movq	-192(%rbp), %r9
	movl	$1094, %ecx
	leaq	.LC417(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	movl	$-1, %r8d
	movl	$2, %r9d
	movl	$1036, %ecx
	movq	%rax, %r15
	movq	(%rax), %rax
	leaq	.LC46(%rip), %rdx
	movq	%r15, %rsi
	movq	23(%rax), %rax
	movw	%r8w, 41(%rax)
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdx
	movq	(%r15), %rax
	movq	%r13, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal10JSFunction9prototypeEPNS0_7IsolateE.constprop.0
	movq	-264(%rbp), %rdx
	popq	%r9
	movq	%rax, %rsi
	popq	%r10
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2278
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2279:
	movq	-216(%rbp), %rdx
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$1034, %ecx
	leaq	.LC48(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$1035, %ecx
	leaq	.LC418(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	pushq	$1037
	movl	$48, %r8d
	movq	-192(%rbp), %r9
	movl	$1095, %ecx
	leaq	.LC419(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	xorl	%r11d, %r11d
	movl	$-1, %edx
	xorl	%r8d, %r8d
	movq	%rax, %r15
	movq	(%rax), %rax
	movl	$1041, %ecx
	movl	$2, %r9d
	movq	%r15, %rsi
	movq	23(%rax), %rax
	movw	%r11w, 39(%rax)
	movq	(%r15), %rax
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	(%rbx), %rdi
	leaq	.LC46(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%r15), %rax
	movq	(%rbx), %rdx
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	popq	%rcx
	popq	%rdi
	cmpw	$68, 11(%rax)
	jne	.L2282
	movq	23(%rsi), %rsi
.L2282:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2283
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2284:
	movq	(%rbx), %rdi
	leaq	.LC420(%rip), %rax
	movq	%r13, %rsi
	movq	$23, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-216(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$1040, %ecx
	leaq	.LC48(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$1038, %ecx
	leaq	.LC421(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$1039, %ecx
	leaq	.LC413(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	pushq	$1008
	movl	$48, %r8d
	movq	-192(%rbp), %r9
	movl	$1091, %ecx
	leaq	.LC422(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	xorl	%edi, %edi
	movl	$-1, %r8d
	movl	$2, %r9d
	movq	%rax, %r15
	movq	(%rax), %rax
	leaq	.LC46(%rip), %rdx
	movl	$1012, %ecx
	movq	%r15, %rsi
	movq	23(%rax), %rax
	movw	%di, 39(%rax)
	movq	(%r15), %rax
	movq	23(%rax), %rax
	movw	%r8w, 41(%rax)
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%r15), %rax
	movq	(%rbx), %rdx
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	popq	%r9
	popq	%r10
	cmpw	$68, 11(%rax)
	jne	.L2287
	movq	23(%rsi), %rsi
.L2287:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2288
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2289:
	movq	(%rbx), %rdi
	leaq	.LC423(%rip), %rax
	movq	%r13, %rsi
	movq	$15, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-216(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$1011, %ecx
	leaq	.LC48(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$1009, %ecx
	leaq	.LC421(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$2, %r9d
	movl	$1010, %ecx
	leaq	.LC413(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$1092, %ecx
	movq	%r14, %rsi
	movq	-192(%rbp), %r8
	movl	$1013, %r9d
	leaq	.LC424(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movq	(%rbx), %rdi
	movl	$78, %edx
	movq	%rax, %r14
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%r14), %rax
	movl	$1, %ecx
	movl	$-1, %esi
	movq	23(%rax), %rax
	movw	%cx, 39(%rax)
	movq	(%r14), %rax
	movq	23(%rax), %rax
	movw	%si, 41(%rax)
	movq	(%r14), %rax
	movq	(%rbx), %r15
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L2292
	movq	23(%rsi), %rsi
.L2292:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2293
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2294:
	movq	(%rbx), %r15
	leaq	.LC425(%rip), %rax
	movq	%r13, %rsi
	movq	$11, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	2072(%r12), %r15
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$1026, %ecx
	leaq	.LC18(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$1020, %ecx
	leaq	.LC426(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1021, %ecx
	leaq	.LC427(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$1019, %ecx
	leaq	1560(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$1025, %ecx
	leaq	3232(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$1024, %ecx
	leaq	1776(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$1014, %ecx
	leaq	1192(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$1015, %ecx
	leaq	1216(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$1016, %ecx
	leaq	1232(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$1017, %ecx
	leaq	1256(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$1018, %ecx
	leaq	1512(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$1022, %ecx
	leaq	1736(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movl	$1023, %ecx
	movq	%r14, %rsi
	leaq	1728(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17CreateArrayBufferENS0_6HandleINS0_6StringEEENS1_15ArrayBufferKindE
	movq	-184(%rbp), %rsi
	movq	(%rbx), %rdi
	movq	%r15, %rdx
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%rax, %r14
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movl	$8, %edx
	movq	%r14, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_120InstallSpeciesGetterEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	leaq	.LC428(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	$38, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rbx), %rdi
	movl	$222, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %ecx
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 95(%r15)
	leaq	95(%r15), %rsi
	testb	$1, %r14b
	je	.L2455
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	jne	.L3080
	testb	$24, %al
	je	.L2455
.L3160:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3081
	.p2align 4,,10
	.p2align 3
.L2455:
	leaq	3296(%r12), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17CreateArrayBufferENS0_6HandleINS0_6StringEEENS1_15ArrayBufferKindE
	movq	(%rbx), %rdi
	movl	$146, %edx
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_120InstallSpeciesGetterEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate15object_functionEv
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	(%r14), %r15
	movq	(%rax), %rdi
	movq	%r15, 223(%rdi)
	leaq	223(%rdi), %rsi
	testb	$1, %r15b
	je	.L2454
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	jne	.L3082
	testb	$24, %al
	je	.L2454
.L3163:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3083
	.p2align 4,,10
	.p2align 3
.L2454:
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$1, %r8d
	movl	$575, %ecx
	leaq	.LC429(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$576, %ecx
	movl	$1, %r8d
	leaq	.LC430(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$579, %ecx
	movl	$1, %r8d
	leaq	.LC431(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$580, %ecx
	movl	$1, %r8d
	leaq	.LC223(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$581, %ecx
	movl	$1, %r8d
	leaq	.LC432(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$582, %ecx
	movl	$1, %r8d
	leaq	.LC433(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$583, %ecx
	movl	$1, %r8d
	leaq	.LC434(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$577, %ecx
	movl	$1, %r8d
	leaq	.LC435(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	pushq	$2
	movl	$1, %r9d
	movl	$4, %r8d
	movl	$578, %ecx
	leaq	.LC436(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$585, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC437(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$586, %ecx
	movl	$1, %r9d
	movl	$4, %r8d
	leaq	.LC438(%rip), %rdx
	movl	$2, (%rsp)
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$587, %ecx
	movl	$1, %r8d
	leaq	.LC439(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	movl	$584, %ecx
	movq	%r14, %rsi
	movl	$1, %r8d
	leaq	.LC440(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	leaq	.LC441(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	$10, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$72, %ecx
	movq	-192(%rbp), %r9
	movq	%rax, %rsi
	movl	$1086, %edx
	movl	$611, (%rsp)
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	xorl	%r11d, %r11d
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	%r14, %rsi
	movq	23(%rax), %rdx
	movl	47(%rdx), %eax
	andl	$-33, %eax
	movl	%eax, 47(%rdx)
	movq	(%r14), %rax
	movq	23(%rax), %rax
	movw	%r11w, 39(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal12_GLOBAL__N_120InstallSpeciesGetterEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	movq	16(%rbx), %rax
	movq	(%r14), %r15
	movq	(%rax), %rdi
	movq	%r15, 1527(%rdi)
	leaq	1527(%rdi), %rsi
	popq	%rax
	popq	%rdx
	testb	$1, %r15b
	je	.L2453
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	jne	.L3084
	testb	$24, %al
	je	.L2453
.L3162:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3085
	.p2align 4,,10
	.p2align 3
.L2453:
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movq	%r14, %rsi
	movl	$630, %ecx
	leaq	.LC141(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$631, %ecx
	leaq	.LC140(%rip), %rdx
	movl	$2, %r9d
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%r14), %rax
	movq	(%rbx), %rdx
	movq	55(%rax), %r15
	movq	-1(%r15), %rax
	cmpw	$68, 11(%rax)
	jne	.L2306
	movq	23(%r15), %r15
.L2306:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2307
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
	movq	%rax, %r14
.L2308:
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	movq	%r15, 1535(%rdi)
	leaq	1535(%rdi), %rsi
	testb	$1, %r15b
	je	.L2452
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	jne	.L3086
	testb	$24, %al
	je	.L2452
.L3173:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3087
	.p2align 4,,10
	.p2align 3
.L2452:
	movq	(%rbx), %rdi
	leaq	2192(%r12), %r15
	movq	%r14, %rsi
	movl	$614, %ecx
	movq	%r15, %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$615, %ecx
	leaq	2200(%r12), %rax
	movq	%rax, %rdx
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$616, %ecx
	leaq	2208(%r12), %rax
	movq	%rax, %rdx
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$617, %ecx
	movq	-232(%rbp), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$628, %ecx
	movq	-240(%rbp), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$618, %ecx
	leaq	.LC121(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$619, %ecx
	leaq	.LC120(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$620, %ecx
	leaq	.LC123(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.1
	movq	-224(%rbp), %rdx
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$621, %ecx
	leaq	.LC143(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$916, %ecx
	leaq	.LC163(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$622, %ecx
	leaq	.LC144(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$917, %ecx
	leaq	.LC159(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$918, %ecx
	leaq	.LC145(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$919, %ecx
	leaq	.LC146(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$920, %ecx
	leaq	.LC158(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$623, %ecx
	leaq	.LC155(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$624, %ecx
	leaq	.LC156(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$758, %ecx
	leaq	.LC157(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$625, %ecx
	leaq	.LC147(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$629, %ecx
	leaq	.LC162(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$626, %ecx
	leaq	.LC150(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$921, %ecx
	leaq	.LC165(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$922, %ecx
	leaq	.LC166(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$627, %ecx
	leaq	.LC442(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$923, %ecx
	leaq	.LC102(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$924, %ecx
	leaq	.LC164(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$927, %ecx
	leaq	.LC153(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$925, %ecx
	leaq	.LC443(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$759, %ecx
	leaq	.LC132(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	-256(%rbp), %rcx
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$2, %r8d
	leaq	3456(%r12), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movl	$17, %edx
	leaq	.LC444(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE
	movq	(%rbx), %rdi
	movl	$193, %edx
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movl	$18, %edx
	leaq	.LC445(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE
	movq	(%rbx), %rdi
	movl	$74, %edx
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movl	$19, %edx
	leaq	.LC446(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE
	movq	(%rbx), %rdi
	movl	$191, %edx
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movl	$20, %edx
	leaq	.LC447(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE
	movq	(%rbx), %rdi
	movl	$72, %edx
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movl	$21, %edx
	leaq	.LC448(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE
	movq	(%rbx), %rdi
	movl	$192, %edx
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movl	$22, %edx
	leaq	.LC449(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE
	movq	(%rbx), %rdi
	movl	$73, %edx
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movl	$23, %edx
	leaq	.LC450(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE
	movq	(%rbx), %rdi
	movl	$48, %edx
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movl	$24, %edx
	leaq	.LC451(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE
	movq	(%rbx), %rdi
	movl	$49, %edx
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movl	$25, %edx
	leaq	.LC452(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE
	movq	(%rbx), %rdi
	movl	$194, %edx
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movl	$26, %edx
	leaq	.LC453(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE
	movq	(%rbx), %rdi
	movl	$29, %edx
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movl	$27, %edx
	leaq	.LC454(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17InstallTypedArrayEPKcNS0_12ElementsKindE
	movq	(%rbx), %rdi
	movl	$28, %edx
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movl	$1087, %ecx
	pushq	$285
	movq	-192(%rbp), %r9
	movl	$72, %r8d
	leaq	.LC455(%rip), %rdx
	movq	-184(%rbp), %rsi
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.1
	movq	(%rbx), %rdi
	movl	$38, %edx
	movq	%rax, %r14
	movq	%rax, %rsi
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%r14), %rax
	movl	$1, %edi
	movl	$-1, %r8d
	movq	23(%rax), %rax
	movw	%di, 39(%rax)
	movq	(%r14), %rax
	movq	23(%rax), %rax
	movw	%r8w, 41(%rax)
	movq	(%r14), %rax
	movq	(%rbx), %rdx
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	popq	%r9
	popq	%r10
	cmpw	$68, 11(%rax)
	jne	.L2314
	movq	23(%rsi), %rsi
.L2314:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2315
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2316:
	movq	(%rbx), %rdi
	leaq	.LC455(%rip), %rax
	movq	%r13, %rsi
	movq	$8, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-256(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	$792, %ecx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$793, %ecx
	movq	-216(%rbp), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$794, %ecx
	movq	-264(%rbp), %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$796, %ecx
	leaq	.LC456(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$806, %ecx
	leaq	.LC457(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$795, %ecx
	leaq	.LC458(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$805, %ecx
	leaq	.LC459(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$798, %ecx
	leaq	.LC460(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$808, %ecx
	leaq	.LC461(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$797, %ecx
	leaq	.LC462(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$807, %ecx
	leaq	.LC463(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$800, %ecx
	leaq	.LC464(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$810, %ecx
	leaq	.LC465(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$799, %ecx
	leaq	.LC466(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$809, %ecx
	leaq	.LC467(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$801, %ecx
	leaq	.LC468(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$811, %ecx
	leaq	.LC469(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$802, %ecx
	leaq	.LC470(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$812, %ecx
	leaq	.LC471(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$804, %ecx
	leaq	.LC472(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$814, %ecx
	leaq	.LC473(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$803, %ecx
	leaq	.LC474(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$813, %ecx
	leaq	.LC475(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	-192(%rbp), %r8
	movq	(%rbx), %rdi
	movl	$396, %r9d
	movq	-184(%rbp), %rsi
	movl	$1069, %ecx
	leaq	.LC476(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movq	(%rbx), %rdi
	movl	$87, %edx
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%rbx), %r14
	movq	(%r15), %rax
	movq	23(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2318
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2319:
	movl	$-1, %ecx
	movw	%cx, 41(%rsi)
	movq	(%rax), %rax
	xorl	%esi, %esi
	movw	%si, 39(%rax)
	movq	(%r15), %rax
	movq	(%rbx), %rdx
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L2322
	movq	23(%rsi), %rsi
.L2322:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2323
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2324:
	movq	(%rbx), %rdi
	leaq	2808(%r12), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$399, %ecx
	leaq	.LC477(%rip), %rdx
	movl	$2, %r9d
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1655(%rdi)
	leaq	1655(%rdi), %rsi
	testb	$1, %dl
	je	.L2451
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	jne	.L3088
	testb	$24, %al
	je	.L2451
.L3174:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3089
	.p2align 4,,10
	.p2align 3
.L2451:
	movq	(%rbx), %rdi
	leaq	.LC442(%rip), %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movl	$397, %ecx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1671(%rdi)
	leaq	1671(%rdi), %rsi
	testb	$1, %dl
	je	.L2450
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	jne	.L3090
	testb	$24, %al
	je	.L2450
.L3172:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3091
	.p2align 4,,10
	.p2align 3
.L2450:
	movq	(%rbx), %rdi
	leaq	.LC478(%rip), %rdx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	movl	$400, %ecx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1663(%rdi)
	leaq	1663(%rdi), %rsi
	testb	$1, %dl
	je	.L2449
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	jne	.L3092
	testb	$24, %al
	je	.L2449
.L3171:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3093
	.p2align 4,,10
	.p2align 3
.L2449:
	movq	(%rbx), %rdi
	leaq	.LC479(%rip), %rdx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	movl	$398, %ecx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1647(%rdi)
	leaq	1647(%rdi), %rsi
	testb	$1, %dl
	je	.L2448
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	jne	.L3094
	testb	$24, %al
	je	.L2448
.L3170:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3095
	.p2align 4,,10
	.p2align 3
.L2448:
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$401, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC400(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$402, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC121(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	-224(%rbp), %rdx
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$404, %ecx
	leaq	.LC158(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$405, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC120(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	leaq	.LC480(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	$4, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$403, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$406, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC123(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	movq	(%r14), %rax
	movq	-1(%rax), %r14
	leaq	511(%rdi), %rsi
	movq	%r14, 511(%rdi)
	testb	$1, %r14b
	je	.L2447
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	jne	.L3096
	testb	$24, %al
	je	.L2447
.L3169:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3097
	.p2align 4,,10
	.p2align 3
.L2447:
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$-1, %r15d
	call	_ZN2v88internal12_GLOBAL__N_120InstallSpeciesGetterEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	movq	-192(%rbp), %r8
	movq	(%rbx), %rdi
	movl	$234, %r9d
	movq	-184(%rbp), %rsi
	movl	$1041, %ecx
	leaq	.LC481(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movl	$1, %edx
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	%r14, %rsi
	movq	23(%rax), %rax
	movw	%r15w, 41(%rax)
	movq	(%r14), %rax
	movq	23(%rax), %rax
	movw	%dx, 39(%rax)
	movq	(%rbx), %rdi
	movl	$27, %edx
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$235, %ecx
	leaq	.LC482(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	leaq	.LC483(%rip), %rdx
	movl	$236, %ecx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%r14), %rax
	movq	(%rbx), %rdx
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L2342
	movq	23(%rsi), %rsi
.L2342:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2343
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2344:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$237, %ecx
	leaq	.LC132(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	$238, %ecx
	leaq	.LC18(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$239, %ecx
	movl	$2, %r9d
	movq	%r15, %rsi
	leaq	.LC131(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	leaq	2120(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	-192(%rbp), %r8
	movq	(%rbx), %rdi
	movl	$562, %r9d
	movq	-184(%rbp), %rsi
	movl	$1077, %ecx
	leaq	.LC484(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movq	(%rbx), %rdi
	movl	$90, %edx
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%rbx), %r14
	movq	(%r15), %rax
	movq	23(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2346
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2347:
	movl	$-1, %r10d
	xorl	%r11d, %r11d
	movw	%r10w, 41(%rsi)
	movq	(%rax), %rax
	movw	%r11w, 39(%rax)
	movq	(%r15), %rax
	movq	(%rbx), %rdx
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L2350
	movq	23(%rsi), %rsi
.L2350:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2351
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2352:
	movq	(%rbx), %rdi
	leaq	3248(%r12), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$563, %ecx
	leaq	.LC478(%rip), %rdx
	movl	$2, %r9d
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1751(%rdi)
	leaq	1751(%rdi), %rsi
	testb	$1, %dl
	je	.L2446
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	jne	.L3098
	testb	$24, %al
	je	.L2446
.L3166:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3099
	.p2align 4,,10
	.p2align 3
.L2446:
	movq	(%rbx), %rdi
	leaq	.LC431(%rip), %rdx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	movl	$564, %ecx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1735(%rdi)
	leaq	1735(%rdi), %rsi
	testb	$1, %dl
	je	.L2445
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	jne	.L3100
	testb	$24, %al
	je	.L2445
.L3165:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3101
	.p2align 4,,10
	.p2align 3
.L2445:
	movq	(%rbx), %rdi
	leaq	.LC479(%rip), %rdx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	movl	$565, %ecx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1743(%rdi)
	leaq	1743(%rdi), %rsi
	testb	$1, %dl
	je	.L2444
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	jne	.L3102
	testb	$24, %al
	je	.L2444
.L3168:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3103
	.p2align 4,,10
	.p2align 3
.L2444:
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movq	%r14, %rsi
	movl	$1, %r8d
	movl	$566, %ecx
	leaq	.LC400(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	movl	$567, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC121(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$569, %ecx
	movl	$2, %r9d
	leaq	.LC158(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	leaq	.LC480(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	$4, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rbx), %rdi
	movl	$568, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_119SimpleInstallGetterEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_4NameEEENS0_8Builtins4NameEb.constprop.1
	movq	(%rbx), %rdi
	movl	$570, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC123(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.0
	movq	(%rbx), %rdi
	leaq	2752(%r12), %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-224(%rbp), %rdx
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movq	-216(%rbp), %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	leaq	543(%rdi), %rsi
	movq	%rdx, 543(%rdi)
	testb	$1, %dl
	je	.L2443
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	jne	.L3104
	testb	$24, %al
	je	.L2443
.L3167:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3105
	.p2align 4,,10
	.p2align 3
.L2443:
	movq	16(%rbx), %rax
	movq	(%r14), %r14
	movq	(%rax), %rdi
	movq	%r14, 535(%rdi)
	leaq	535(%rdi), %rsi
	testb	$1, %r14b
	je	.L2366
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L3106
.L2366:
	movq	%r14, %rdx
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal12_GLOBAL__N_120InstallSpeciesGetterEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	movl	$1, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$40, %edx
	movl	$1027, %esi
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	(%rbx), %rdi
	movl	$1, %ecx
	movq	%rax, %rsi
	movq	%rax, %r14
	leaq	104(%rdi), %rdx
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$1, %edx
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	16(%rbx), %rax
	movq	(%r14), %r15
	movq	(%rax), %rdi
	movq	%r15, 727(%rdi)
	leaq	727(%rdi), %rsi
	testb	$1, %r15b
	je	.L2368
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L3107
.L2368:
	movq	%r15, %rdx
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %rsi
	movl	$4, %r9d
	xorl	%ecx, %ecx
	movq	-240(%rbp), %rdx
	movl	$7, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r14), %rax
	movq	(%rbx), %rsi
	movq	%r13, %rdx
	movq	-208(%rbp), %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	16(%rbx), %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory25ObjectLiteralMapFromCacheENS0_6HandleINS0_13NativeContextEEEi@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE@PLT
	pushq	$0
	movq	(%rbx), %rdi
	movl	$1, %r9d
	pushq	$4
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movq	%rax, %rcx
	leaq	3544(%r12), %rdx
	call	_ZN2v88internal3Map13CopyWithFieldEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_9FieldTypeEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_14TransitionFlagE@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2371
	movq	(%rbx), %rdi
	call	_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE@PLT
	pushq	$0
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	pushq	$4
	movq	%rax, %rcx
	leaq	2368(%r12), %rdx
	xorl	%r8d, %r8d
	movl	$1, %r9d
	call	_ZN2v88internal3Map13CopyWithFieldEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_9FieldTypeEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_14TransitionFlagE@PLT
	popq	%rsi
	popq	%rdi
	testq	%rax, %rax
	je	.L2371
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 655(%r15)
	leaq	655(%r15), %rsi
	testb	$1, %r14b
	je	.L2372
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L3108
.L2372:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-192(%rbp), %r8
	movq	(%rbx), %rdi
	movl	$662, %r9d
	movq	-184(%rbp), %rsi
	movl	$1084, %ecx
	leaq	.LC485(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movq	(%rbx), %rdi
	movl	$94, %edx
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%rbx), %r15
	movq	(%r14), %rax
	movq	23(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2374
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2375:
	movl	$-1, %edx
	xorl	%ecx, %ecx
	movw	%dx, 41(%rsi)
	movq	(%rax), %rax
	movw	%cx, 39(%rax)
	movq	(%r14), %rax
	movq	(%rbx), %r15
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L2378
	movq	23(%rsi), %rsi
.L2378:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2379
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2380:
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movq	%r14, %rsi
	movl	$1, %r8d
	movl	$667, %ecx
	leaq	.LC479(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$664, %ecx
	leaq	.LC477(%rip), %rdx
	movl	$2, %r9d
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1815(%rdi)
	leaq	1815(%rdi), %rsi
	testb	$1, %r15b
	je	.L2382
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2382
	movq	%r15, %rdx
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
.L2382:
	movq	%r15, %rdx
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$666, %ecx
	leaq	.LC442(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1807(%rdi)
	leaq	1807(%rdi), %rsi
	testb	$1, %r15b
	je	.L2384
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2384
	movq	%r15, %rdx
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
.L2384:
	movq	%r15, %rdx
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %rdi
	movl	$665, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC478(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %r15
	movq	%r13, %rsi
	leaq	.LC485(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	movq	$7, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	16(%rbx), %rax
	movq	(%rax), %r15
	movq	(%r14), %rax
	movq	-1(%rax), %r14
	leaq	575(%r15), %rsi
	movq	%r14, 575(%r15)
	testb	$1, %r14b
	je	.L2386
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2386
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rsi
.L2386:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-192(%rbp), %r8
	movq	(%rbx), %rdi
	movl	$668, %r9d
	movq	-184(%rbp), %rsi
	movl	$1085, %ecx
	leaq	.LC486(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movq	(%rbx), %rdi
	movl	$95, %edx
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internalL32InstallWithIntrinsicDefaultProtoEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEi
	movq	(%rbx), %r15
	movq	(%r14), %rax
	movq	23(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2388
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2389:
	movl	$-1, %r11d
	xorl	%r15d, %r15d
	movw	%r11w, 41(%rsi)
	movq	(%rax), %rax
	movw	%r15w, 39(%rax)
	movq	(%r14), %rax
	movq	(%rbx), %r15
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L2392
	movq	23(%rsi), %rsi
.L2392:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2393
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2394:
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movq	%r14, %rsi
	movl	$1, %r8d
	movl	$671, %ecx
	leaq	.LC479(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movl	$669, %ecx
	movq	%r14, %rsi
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC478(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$670, %ecx
	leaq	.LC431(%rip), %rdx
	movl	$2, %r9d
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1823(%rdi)
	leaq	1823(%rdi), %rsi
	testb	$1, %r15b
	je	.L2396
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2396
	movq	%r15, %rdx
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
.L2396:
	movq	%r15, %rdx
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	leaq	.LC486(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	$7, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118InstallToStringTagEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEE
	movq	16(%rbx), %rax
	movq	(%rax), %r15
	movq	(%r14), %rax
	movq	-1(%rax), %r14
	leaq	583(%r15), %rsi
	movq	%r14, 583(%r15)
	testb	$1, %r14b
	je	.L2398
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2398
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rsi
.L2398:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis17CreateJSProxyMapsEv
	movq	(%rbx), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1303(%rax), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2400
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2401:
	movq	(%rbx), %rdi
	leaq	.LC487(%rip), %rdx
	leaq	3120(%r12), %r14
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movl	$850, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	(%rax), %rax
	orb	$64, 13(%rax)
	call	_ZN2v88internal15NewFunctionArgs10ForBuiltinENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	movq	(%rbx), %rdx
	movq	%rax, %r15
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	927(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2403
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2404:
	movq	%rsi, -160(%rbp)
	movq	-208(%rbp), %rdi
	movq	(%r15), %rsi
	call	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0
	movq	(%r15), %rax
	movl	$2, %r9d
	movl	$2, %r10d
	movq	23(%rax), %rax
	movw	%r9w, 41(%rax)
	movq	(%r15), %rax
	movq	23(%rax), %rax
	movw	%r10w, 39(%rax)
	movq	16(%rbx), %rax
	movq	(%r15), %rdx
	movq	(%rax), %rdi
	movq	%rdx, 919(%rdi)
	leaq	919(%rdi), %rsi
	testb	$1, %dl
	je	.L2406
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2406
	movq	%rdx, -240(%rbp)
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-240(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
.L2406:
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	-184(%rbp), %rsi
	movl	$2, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$857, %ecx
	movl	$1, %r8d
	leaq	.LC488(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movl	$858, %esi
	xorl	%r8d, %r8d
	movq	-200(%rbp), %rdx
	xorl	%ecx, %ecx
	call	_ZN2v88internal12_GLOBAL__N_130SimpleCreateSharedFunctionInfoEPNS0_7IsolateENS0_8Builtins4NameENS0_6HandleINS0_6StringEEEiNS0_12FunctionKindE
	movq	16(%rbx), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 943(%r15)
	leaq	943(%r15), %rsi
	testb	$1, %r14b
	je	.L2408
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2408
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rsi
.L2408:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC489(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	$7, -120(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r15
	call	_ZN2v88internal7Isolate15object_functionEv
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	-184(%rbp), %rsi
	movq	(%rbx), %rdi
	movq	%r15, %rdx
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%rax, %r14
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movl	$528, %ecx
	movq	%r14, %rsi
	movl	$1, %r8d
	leaq	.LC113(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	movl	$866, %ecx
	movq	%r14, %rsi
	movl	$1, %r8d
	leaq	.LC490(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	leaq	.LC134(%rip), %rdx
	movl	$526, %ecx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1887(%rdi)
	leaq	1887(%rdi), %rsi
	testb	$1, %r15b
	je	.L2410
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2410
	movq	%r15, %rdx
	movq	%rsi, -216(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rsi
	movq	-184(%rbp), %rdi
.L2410:
	movq	%r15, %rdx
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	leaq	.LC491(%rip), %rdx
	movl	$527, %ecx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1895(%rdi)
	leaq	1895(%rdi), %rsi
	testb	$1, %r15b
	je	.L2412
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2412
	movq	%r15, %rdx
	movq	%rsi, -216(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rsi
	movq	-184(%rbp), %rdi
.L2412:
	movq	%r15, %rdx
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$865, %ecx
	leaq	.LC477(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$529, %ecx
	movl	$1, %r8d
	leaq	.LC104(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$863, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC115(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$530, %ecx
	movl	$1, %r8d
	leaq	.LC478(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$861, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC117(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$531, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC492(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$862, %ecx
	movl	$2, %r9d
	movl	$1, %r8d
	leaq	.LC109(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	movl	$532, %ecx
	leaq	.LC442(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$864, %ecx
	movl	$1, %r8d
	leaq	.LC116(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.2
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$48, %edx
	movl	$1104, %esi
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	879(%rax), %rsi
	call	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0
	movq	(%r14), %rax
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	-248(%rbp), %rdx
	orb	$2, 13(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	(%rbx), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	-232(%rbp), %rsi
	movl	$3, %ecx
	movq	%r13, %rdi
	leaq	4384(%r12), %rdx
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r14), %rax
	movq	(%rbx), %rsi
	movq	%r13, %rdx
	movq	-208(%rbp), %r15
	movq	%rax, -160(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	leaq	4392(%r12), %rdx
	movl	$3, %ecx
	movq	%r13, %rdi
	leaq	2872(%r12), %rsi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r14), %rax
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	16(%rbx), %rax
	movq	(%r14), %r15
	movq	(%rax), %rdi
	movq	%r15, 271(%rdi)
	leaq	271(%rdi), %rsi
	testb	$1, %r15b
	je	.L2414
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2414
	movq	%r15, %rdx
	movq	%rsi, -216(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rsi
	movq	-184(%rbp), %rdi
.L2414:
	movq	%r15, %rdx
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	leaq	.LC493(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r14
	movq	(%rax), %rax
	orb	$64, 13(%rax)
	movq	16(%rbx), %rax
	movq	(%r14), %r14
	movq	(%rax), %r15
	movq	%r14, 263(%r15)
	leaq	263(%r15), %rsi
	testb	$1, %r14b
	je	.L2416
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2416
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %rsi
.L2416:
	movq	%r14, %rdx
	movq	%r15, %rdi
	leaq	2032(%r12), %r14
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	pushq	$0
	movl	$40, %r8d
	movq	%r14, %rsi
	pushq	$166
	movl	$1058, %ecx
	movq	%rax, %rdx
	movq	%r13, %rdi
	movl	$2, %r9d
	call	_ZN2v88internal15NewFunctionArgs23ForBuiltinWithPrototypeENS0_6HandleINS0_6StringEEENS2_INS0_10HeapObjectEEENS0_12InstanceTypeEiiiNS0_11MutableModeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	movq	(%rbx), %r15
	popq	%rcx
	movq	(%rax), %rax
	popq	%r8
	movq	41112(%r15), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2418
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2419:
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movl	$2, %edx
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rbx), %rsi
	movl	$4, %r9d
	xorl	%ecx, %ecx
	movq	-208(%rbp), %r15
	movq	-232(%rbp), %rdx
	movl	$2, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r14), %rax
	movq	(%rbx), %rsi
	movq	%r15, %rdx
	leaq	-168(%rbp), %r10
	movq	%r15, -208(%rbp)
	leaq	2224(%r12), %r15
	movq	%r10, %rdi
	movq	%rax, -168(%rbp)
	movq	%r10, -184(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	-208(%rbp), %rdi
	movl	$4, %r9d
	movl	$2, %r8d
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	-184(%rbp), %r10
	movq	(%r14), %rax
	movq	(%rbx), %rsi
	movq	-208(%rbp), %rdx
	movq	%r10, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	16(%rbx), %rax
	movq	(%r14), %r14
	movq	(%rax), %rdi
	movq	%r14, 1191(%rdi)
	leaq	1191(%rdi), %rsi
	testb	$1, %r14b
	je	.L2421
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2421
	movq	%r14, %rdx
	movq	%rsi, -216(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rsi
	movq	-184(%rbp), %rdi
.L2421:
	movq	%r14, %rdx
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %r14
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1191(%rax), %r8
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2423
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2424:
	movq	(%rbx), %rdi
	leaq	.LC494(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	(%rax), %rdx
	movq	%rax, %r14
	movzbl	14(%rdx), %eax
	andb	$7, %al
	orl	$104, %eax
	movb	%al, 14(%rdx)
	movq	16(%rbx), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rdi
	movq	%rdx, 383(%rdi)
	leaq	383(%rdi), %rsi
	testb	$1, %dl
	je	.L2426
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2426
	movq	%rdx, -224(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-224(%rbp), %rdx
	movq	-216(%rbp), %rsi
	movq	-184(%rbp), %rdi
.L2426:
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	leaq	.LC495(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	(%rax), %rdx
	movq	%rax, %r14
	movzbl	14(%rdx), %eax
	andb	$7, %al
	orl	$112, %eax
	movb	%al, 14(%rdx)
	movq	16(%rbx), %rax
	movq	(%r14), %r14
	movq	(%rax), %rdi
	movq	%r14, 1199(%rdi)
	leaq	1199(%rdi), %rsi
	testb	$1, %r14b
	je	.L2428
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2428
	movq	%r14, %rdx
	movq	%rsi, -216(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rsi
	movq	-184(%rbp), %rdi
.L2428:
	movq	%r14, %rdx
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory15NewAccessorPairEv@PLT
	movq	%rax, %r14
	movq	48(%rbx), %rax
	testq	%rax, %rax
	je	.L3109
.L2430:
	movq	(%rax), %rdx
	movq	(%r14), %rdi
	movq	%rax, -216(%rbp)
	movq	%rdx, 7(%rdi)
	leaq	7(%rdi), %rsi
	movq	%rdx, -184(%rbp)
	movq	%rsi, -240(%rbp)
	movq	%rdi, -224(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-184(%rbp), %rdx
	movq	-216(%rbp), %rax
	testb	$1, %dl
	je	.L2431
	movq	-240(%rbp), %rsi
	movq	-224(%rbp), %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	movq	-184(%rbp), %rax
.L2431:
	movq	(%r14), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	movq	%rdx, -184(%rbp)
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-184(%rbp), %rdx
	testb	$1, %dl
	je	.L2432
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L2432:
	movl	$1, %r8d
	movl	$2, %ecx
	movq	%r12, %rdi
	movl	$32, %edx
	movl	$1058, %esi
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	(%rbx), %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rbx), %rsi
	movl	$4, %r9d
	xorl	%ecx, %ecx
	movq	-232(%rbp), %rdx
	movl	$2, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r12), %rax
	movq	(%rbx), %rsi
	movq	%r13, %rdx
	movq	-208(%rbp), %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movl	$7, %ecx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r12), %rax
	movq	(%rbx), %rsi
	movq	%r13, %rdx
	movq	-208(%rbp), %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate24initial_object_prototypeEv
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	(%r12), %rax
	movq	%rax, -128(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	1191(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L2434
.L2433:
	movq	%r13, %rdi
	call	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.constprop.0
	movq	16(%rbx), %rax
	movq	(%r12), %r12
	movq	(%rax), %r13
	movq	%r12, 1207(%r13)
	leaq	1207(%r13), %r14
	testb	$1, %r12b
	je	.L2435
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2435
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L2435:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	subq	$8, %rsp
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	pushq	$166
	movl	$1065, %edx
	movl	$24, %ecx
	movq	-192(%rbp), %r9
	movq	-200(%rbp), %rsi
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movq	16(%rbx), %rdx
	movq	(%rax), %r12
	movq	(%rdx), %r13
	movq	%r12, 303(%r13)
	leaq	303(%r13), %r14
	popq	%rax
	popq	%rdx
	testb	$1, %r12b
	je	.L2437
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2437
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L2437:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %rdi
	movl	$78, %edx
	xorl	%r8d, %r8d
	movq	-200(%rbp), %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movq	16(%rbx), %rdx
	movq	(%rax), %r12
	movq	(%rdx), %r13
	movq	%r12, 287(%r13)
	leaq	287(%r13), %r14
	testb	$1, %r12b
	je	.L2439
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2439
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L2439:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%rbx), %rdi
	movl	$79, %edx
	xorl	%r8d, %r8d
	movq	-200(%rbp), %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movq	16(%rbx), %rdx
	movq	(%rax), %r12
	movq	(%rdx), %r13
	movq	%r12, 279(%r13)
	leaq	279(%r13), %r14
	testb	$1, %r12b
	je	.L2441
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2441
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L2441:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19GenerationalBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3110
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2374:
	.cfi_restore_state
	movq	41088(%r15), %rax
	cmpq	%rax, 41096(%r15)
	je	.L3111
.L2376:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2375
	.p2align 4,,10
	.p2align 3
.L2976:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	testb	$24, %al
	jne	.L3112
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2974:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	testb	$24, %al
	jne	.L3113
	jmp	.L2505
	.p2align 4,,10
	.p2align 3
.L2984:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-184(%rbp), %rsi
	testb	$24, %al
	jne	.L3114
	jmp	.L2500
	.p2align 4,,10
	.p2align 3
.L2982:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3115
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2980:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-184(%rbp), %rsi
	testb	$24, %al
	jne	.L3116
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2978:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	testb	$24, %al
	jne	.L3117
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L3048:
	movq	%r15, %rdx
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3118
	jmp	.L2471
	.p2align 4,,10
	.p2align 3
.L3047:
	movq	%r15, %rdx
	movq	%rsi, -312(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-312(%rbp), %rsi
	movq	-272(%rbp), %rdi
	jmp	.L2193
	.p2align 4,,10
	.p2align 3
.L3045:
	movq	%r15, %rdx
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3119
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L3043:
	movq	%rdx, -328(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-328(%rbp), %rdx
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3120
	jmp	.L2473
	.p2align 4,,10
	.p2align 3
.L3041:
	movq	%rdx, -328(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-328(%rbp), %rdx
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3121
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L3052:
	movq	%r8, %rdx
	movq	%r8, -328(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-328(%rbp), %r8
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3122
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L3050:
	movq	%r15, %rdx
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3123
	jmp	.L2470
	.p2align 4,,10
	.p2align 3
.L3015:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -288(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-280(%rbp), %rcx
	movq	-288(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3124
	jmp	.L2485
	.p2align 4,,10
	.p2align 3
.L3013:
	movq	%r15, %rdx
	movq	%rsi, -296(%rbp)
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-280(%rbp), %rcx
	movq	-296(%rbp), %rsi
	movq	-288(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3125
	jmp	.L2486
	.p2align 4,,10
	.p2align 3
.L3022:
	movq	%r15, %rdx
	movq	%rsi, -240(%rbp)
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-240(%rbp), %rsi
	movq	-192(%rbp), %rdi
	jmp	.L2134
	.p2align 4,,10
	.p2align 3
.L3107:
	movq	%r15, %rdx
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
	jmp	.L2368
	.p2align 4,,10
	.p2align 3
.L3106:
	movq	%r14, %rdx
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
	jmp	.L2366
	.p2align 4,,10
	.p2align 3
.L3004:
	movq	%rdx, -280(%rbp)
	movq	%rsi, -272(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-224(%rbp), %rcx
	movq	-280(%rbp), %rdx
	movq	-272(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3126
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L3002:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-208(%rbp), %rcx
	movq	-224(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3127
	jmp	.L2491
	.p2align 4,,10
	.p2align 3
.L3000:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-208(%rbp), %rcx
	movq	-224(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3128
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L3008:
	movq	%r15, %rdx
	movq	%rsi, -256(%rbp)
	movq	%rdi, -224(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-256(%rbp), %rsi
	movq	-224(%rbp), %rdi
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L3006:
	movq	%r15, %rdx
	movq	%rsi, -272(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-224(%rbp), %rcx
	movq	-272(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3129
	jmp	.L2489
	.p2align 4,,10
	.p2align 3
.L3011:
	movq	%r15, %rdx
	movq	%rsi, -280(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-256(%rbp), %rcx
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3130
	jmp	.L2487
	.p2align 4,,10
	.p2align 3
.L3009:
	movq	%r15, %rdx
	movq	%rsi, -280(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-256(%rbp), %rcx
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3131
	jmp	.L2488
	.p2align 4,,10
	.p2align 3
.L2986:
	movq	%r8, %rdx
	movq	%r9, %rdi
	movq	%r8, -208(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-208(%rbp), %r8
	movq	-200(%rbp), %rsi
	movq	-192(%rbp), %r9
	testb	$24, %al
	jne	.L3132
	jmp	.L2499
	.p2align 4,,10
	.p2align 3
.L2988:
	movq	%r15, %rdx
	movq	%rsi, -224(%rbp)
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-200(%rbp), %rcx
	movq	-224(%rbp), %rsi
	movq	-208(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3133
	jmp	.L2498
	.p2align 4,,10
	.p2align 3
.L2998:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-208(%rbp), %rcx
	movq	-224(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3134
	jmp	.L2493
	.p2align 4,,10
	.p2align 3
.L2996:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-208(%rbp), %rcx
	movq	-224(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3135
	jmp	.L2494
	.p2align 4,,10
	.p2align 3
.L2994:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-208(%rbp), %rcx
	movq	-224(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3136
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2992:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-208(%rbp), %rcx
	movq	-224(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3137
	jmp	.L2496
	.p2align 4,,10
	.p2align 3
.L2990:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-208(%rbp), %rcx
	movq	-224(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3138
	jmp	.L2497
	.p2align 4,,10
	.p2align 3
.L3023:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -240(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-192(%rbp), %rcx
	movq	-240(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3139
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L3020:
	movq	%rdx, -296(%rbp)
	movq	%rsi, -288(%rbp)
	movq	%rdi, -280(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-240(%rbp), %rcx
	movq	-296(%rbp), %rdx
	movq	-288(%rbp), %rsi
	movq	-280(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3140
	jmp	.L2484
	.p2align 4,,10
	.p2align 3
.L3035:
	movq	%rdx, -328(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-328(%rbp), %rdx
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3141
	jmp	.L2477
	.p2align 4,,10
	.p2align 3
.L3033:
	movq	%rdx, -328(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-328(%rbp), %rdx
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3142
	jmp	.L2478
	.p2align 4,,10
	.p2align 3
.L3039:
	movq	%rdx, -328(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-328(%rbp), %rdx
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3143
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L3037:
	movq	%rdx, -328(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-328(%rbp), %rdx
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3144
	jmp	.L2476
	.p2align 4,,10
	.p2align 3
.L3027:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3145
	jmp	.L2481
	.p2align 4,,10
	.p2align 3
.L3025:
	movq	%r15, %rdx
	movq	%rsi, -288(%rbp)
	movq	%rdi, -280(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-240(%rbp), %rcx
	movq	-288(%rbp), %rsi
	movq	-280(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3146
	jmp	.L2482
	.p2align 4,,10
	.p2align 3
.L3031:
	movq	%r8, %rdx
	movq	%r8, -328(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-328(%rbp), %r8
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3147
	jmp	.L2479
	.p2align 4,,10
	.p2align 3
.L3029:
	movq	%r15, %rdx
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3148
	jmp	.L2480
	.p2align 4,,10
	.p2align 3
.L3068:
	movq	%r15, %rdx
	movq	%rsi, -280(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3149
	jmp	.L2461
	.p2align 4,,10
	.p2align 3
.L3066:
	movq	%r15, %rdx
	movq	%rsi, -280(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3150
	jmp	.L2462
	.p2align 4,,10
	.p2align 3
.L3072:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-272(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3151
	jmp	.L2459
	.p2align 4,,10
	.p2align 3
.L3074:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-272(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3152
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L3070:
	movq	%r15, %rdx
	movq	%rsi, -280(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3153
	jmp	.L2460
	.p2align 4,,10
	.p2align 3
.L3056:
	movq	%rdx, -320(%rbp)
	movq	%rsi, -312(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-320(%rbp), %rdx
	movq	-312(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3154
	jmp	.L2467
	.p2align 4,,10
	.p2align 3
.L3054:
	movq	%rdx, -328(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-328(%rbp), %rdx
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3155
	jmp	.L2468
	.p2align 4,,10
	.p2align 3
.L3060:
	movq	%rdx, -288(%rbp)
	movq	%rsi, -280(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-288(%rbp), %rdx
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3156
	jmp	.L2465
	.p2align 4,,10
	.p2align 3
.L3058:
	movq	%rdx, -312(%rbp)
	movq	%rsi, -280(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-312(%rbp), %rdx
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3157
	jmp	.L2466
	.p2align 4,,10
	.p2align 3
.L3064:
	movq	%rdx, -288(%rbp)
	movq	%rsi, -280(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-288(%rbp), %rdx
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3158
	jmp	.L2463
	.p2align 4,,10
	.p2align 3
.L3062:
	movq	%rdx, -288(%rbp)
	movq	%rsi, -280(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-288(%rbp), %rdx
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3159
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L3080:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-264(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3160
	jmp	.L2455
	.p2align 4,,10
	.p2align 3
.L3076:
	movq	%r15, %rdx
	movq	%rsi, -280(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3161
	jmp	.L2457
	.p2align 4,,10
	.p2align 3
.L3084:
	movq	%r15, %rdx
	movq	%rsi, -272(%rbp)
	movq	%rdi, -264(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3162
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L3082:
	movq	%r15, %rdx
	movq	%rsi, -272(%rbp)
	movq	%rdi, -264(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3163
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L3078:
	movq	%r15, %rdx
	movq	%rsi, -280(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3164
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L3100:
	movq	%rdx, -272(%rbp)
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3165
	jmp	.L2445
	.p2align 4,,10
	.p2align 3
.L3098:
	movq	%rdx, -272(%rbp)
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3166
	jmp	.L2446
	.p2align 4,,10
	.p2align 3
.L3104:
	movq	%rdx, -264(%rbp)
	movq	%rsi, -256(%rbp)
	movq	%rdi, -224(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-264(%rbp), %rdx
	movq	-256(%rbp), %rsi
	movq	-224(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3167
	jmp	.L2443
	.p2align 4,,10
	.p2align 3
.L3102:
	movq	%rdx, -272(%rbp)
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3168
	jmp	.L2444
	.p2align 4,,10
	.p2align 3
.L3096:
	movq	%r14, %rdx
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-264(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3169
	jmp	.L2447
	.p2align 4,,10
	.p2align 3
.L3094:
	movq	%rdx, -272(%rbp)
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3170
	jmp	.L2448
	.p2align 4,,10
	.p2align 3
.L3092:
	movq	%rdx, -272(%rbp)
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3171
	jmp	.L2449
	.p2align 4,,10
	.p2align 3
.L3090:
	movq	%rdx, -272(%rbp)
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3172
	jmp	.L2450
	.p2align 4,,10
	.p2align 3
.L3086:
	movq	%r15, %rdx
	movq	%rsi, -272(%rbp)
	movq	%rdi, -264(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3173
	jmp	.L2452
	.p2align 4,,10
	.p2align 3
.L3088:
	movq	%rdx, -272(%rbp)
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3174
	jmp	.L2451
	.p2align 4,,10
	.p2align 3
.L3108:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rsi
	jmp	.L2372
	.p2align 4,,10
	.p2align 3
.L3175:
	movq	31(%rsi), %rsi
	testb	$1, %sil
	je	.L2433
	.p2align 4,,10
	.p2align 3
.L2434:
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L2433
	jmp	.L3175
	.p2align 4,,10
	.p2align 3
.L3018:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2121
	movq	-280(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2121
	movq	-288(%rbp), %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2121
	.p2align 4,,10
	.p2align 3
.L3017:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2119
	movq	-288(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2119
	movq	-296(%rbp), %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2119
	.p2align 4,,10
	.p2align 3
.L3019:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2126
	movq	-280(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2126
	movq	-288(%rbp), %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2126
	.p2align 4,,10
	.p2align 3
.L2323:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L3176
.L2325:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L2324
	.p2align 4,,10
	.p2align 3
.L2293:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L3177
.L2295:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L2294
	.p2align 4,,10
	.p2align 3
.L2254:
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L3178
.L2256:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L2255
	.p2align 4,,10
	.p2align 3
.L2266:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L3179
.L2268:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L2267
	.p2align 4,,10
	.p2align 3
.L2263:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L3180
.L2265:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L2264
	.p2align 4,,10
	.p2align 3
.L2278:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L3181
.L2280:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L2279
	.p2align 4,,10
	.p2align 3
.L2275:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L3182
.L2277:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2272:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L3183
.L2274:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L2273
	.p2align 4,,10
	.p2align 3
.L2269:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L3184
.L2271:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L2270
	.p2align 4,,10
	.p2align 3
.L2155:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3185
.L2157:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2156
	.p2align 4,,10
	.p2align 3
.L2236:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3186
.L2238:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2237
	.p2align 4,,10
	.p2align 3
.L2230:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L3187
.L2232:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L2231
	.p2align 4,,10
	.p2align 3
.L2245:
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L3188
.L2247:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	jmp	.L2246
	.p2align 4,,10
	.p2align 3
.L2206:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L3189
.L2208:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%r8, (%r15)
	jmp	.L2207
	.p2align 4,,10
	.p2align 3
.L2201:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3190
.L2203:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2202
	.p2align 4,,10
	.p2align 3
.L2288:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L3191
.L2290:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L2289
	.p2align 4,,10
	.p2align 3
.L2283:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L3192
.L2285:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2123:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L3193
.L2125:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2054:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3194
.L2056:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2055
	.p2align 4,,10
	.p2align 3
.L2051:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3195
.L2053:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2052
	.p2align 4,,10
	.p2align 3
.L2149:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L3196
.L2151:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2144:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L3197
.L2146:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L2145
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3198
.L2050:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2049
	.p2align 4,,10
	.p2align 3
.L2163:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L3199
.L2165:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%r8, (%r15)
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2039:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3200
.L2041:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%r8, (%rax)
	jmp	.L2040
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	41088(%r14), %rax
	movq	%rax, -184(%rbp)
	cmpq	41096(%r14), %rax
	je	.L3201
.L2038:
	movq	-184(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	movq	%rcx, %rsi
	jmp	.L2037
	.p2align 4,,10
	.p2align 3
.L2131:
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L3202
.L2133:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	jmp	.L2132
	.p2align 4,,10
	.p2align 3
.L2090:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L3203
.L2092:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L2091
	.p2align 4,,10
	.p2align 3
.L2066:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3204
.L2068:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2067
	.p2align 4,,10
	.p2align 3
.L2063:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3205
.L2065:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2064
	.p2align 4,,10
	.p2align 3
.L2060:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3206
.L2062:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2061
	.p2align 4,,10
	.p2align 3
.L2057:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3207
.L2059:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2058
	.p2align 4,,10
	.p2align 3
.L2307:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L3208
.L2309:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%r15, (%r14)
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2343:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L3209
.L2345:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2351:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L3210
.L2353:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L2352
	.p2align 4,,10
	.p2align 3
.L2346:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L3211
.L2348:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L2347
	.p2align 4,,10
	.p2align 3
.L2318:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L3212
.L2320:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L2319
	.p2align 4,,10
	.p2align 3
.L2315:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L3213
.L2317:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L2316
	.p2align 4,,10
	.p2align 3
.L2187:
	movq	41088(%rdx), %rdi
	cmpq	41096(%rdx), %rdi
	je	.L3214
.L2189:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rdi)
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2110:
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L3215
.L2112:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	jmp	.L2111
	.p2align 4,,10
	.p2align 3
.L2107:
	movq	41088(%r15), %rdi
	cmpq	41096(%r15), %rdi
	je	.L3216
.L2109:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdi)
	jmp	.L2108
	.p2align 4,,10
	.p2align 3
.L2423:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L3217
.L2425:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r8, (%rsi)
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2418:
	movq	41088(%r15), %r14
	cmpq	%r14, 41096(%r15)
	je	.L3218
.L2420:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L2419
	.p2align 4,,10
	.p2align 3
.L2403:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L3219
.L2405:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L2404
	.p2align 4,,10
	.p2align 3
.L2400:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L3220
.L2402:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L2401
	.p2align 4,,10
	.p2align 3
.L2393:
	movq	41088(%r15), %r14
	cmpq	%r14, 41096(%r15)
	je	.L3221
.L2395:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L2394
	.p2align 4,,10
	.p2align 3
.L2388:
	movq	41088(%r15), %rax
	cmpq	%rax, 41096(%r15)
	je	.L3222
.L2390:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2389
	.p2align 4,,10
	.p2align 3
.L2379:
	movq	41088(%r15), %r14
	cmpq	%r14, 41096(%r15)
	je	.L3223
.L2381:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2973:
	movq	-1(%r12), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L2016
	movq	%r12, 15(%r14)
	jmp	.L2017
	.p2align 4,,10
	.p2align 3
.L2975:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2505
	.p2align 4,,10
	.p2align 3
.L2977:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2979:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2981:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2983:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2985:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2500
	.p2align 4,,10
	.p2align 3
.L2987:
	movq	%r8, %rdx
	movq	%r9, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2499
	.p2align 4,,10
	.p2align 3
.L2989:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2498
	.p2align 4,,10
	.p2align 3
.L2991:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2497
	.p2align 4,,10
	.p2align 3
.L2993:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2496
	.p2align 4,,10
	.p2align 3
.L2995:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2997:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2494
	.p2align 4,,10
	.p2align 3
.L2999:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2493
	.p2align 4,,10
	.p2align 3
.L3001:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L3003:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2491
	.p2align 4,,10
	.p2align 3
.L3005:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L3007:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2489
	.p2align 4,,10
	.p2align 3
.L3010:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2488
	.p2align 4,,10
	.p2align 3
.L3012:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2487
	.p2align 4,,10
	.p2align 3
.L3014:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2486
	.p2align 4,,10
	.p2align 3
.L3016:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2485
	.p2align 4,,10
	.p2align 3
.L3021:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2484
	.p2align 4,,10
	.p2align 3
.L3024:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L3026:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2482
	.p2align 4,,10
	.p2align 3
.L3028:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2481
	.p2align 4,,10
	.p2align 3
.L3030:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2480
	.p2align 4,,10
	.p2align 3
.L3032:
	movq	%r8, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2479
	.p2align 4,,10
	.p2align 3
.L3034:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2478
	.p2align 4,,10
	.p2align 3
.L3036:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2477
	.p2align 4,,10
	.p2align 3
.L3038:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2476
	.p2align 4,,10
	.p2align 3
.L3040:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L3042:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L3044:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2473
	.p2align 4,,10
	.p2align 3
.L3046:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L3049:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2471
	.p2align 4,,10
	.p2align 3
.L3051:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2470
	.p2align 4,,10
	.p2align 3
.L3053:
	movq	%r8, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L3055:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2468
	.p2align 4,,10
	.p2align 3
.L3057:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2467
	.p2align 4,,10
	.p2align 3
.L3059:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2466
	.p2align 4,,10
	.p2align 3
.L3061:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2465
	.p2align 4,,10
	.p2align 3
.L3063:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L3065:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2463
	.p2align 4,,10
	.p2align 3
.L3067:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2462
	.p2align 4,,10
	.p2align 3
.L3069:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2461
	.p2align 4,,10
	.p2align 3
.L3071:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2460
	.p2align 4,,10
	.p2align 3
.L3073:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2459
	.p2align 4,,10
	.p2align 3
.L3075:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L3077:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2457
	.p2align 4,,10
	.p2align 3
.L3079:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L3081:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2455
	.p2align 4,,10
	.p2align 3
.L3083:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L3085:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L3087:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2452
	.p2align 4,,10
	.p2align 3
.L3089:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2451
	.p2align 4,,10
	.p2align 3
.L3091:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2450
	.p2align 4,,10
	.p2align 3
.L3093:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2449
	.p2align 4,,10
	.p2align 3
.L3095:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2448
	.p2align 4,,10
	.p2align 3
.L3097:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2447
	.p2align 4,,10
	.p2align 3
.L3099:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2446
	.p2align 4,,10
	.p2align 3
.L3101:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2445
	.p2align 4,,10
	.p2align 3
.L3103:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2444
	.p2align 4,,10
	.p2align 3
.L3105:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2443
	.p2align 4,,10
	.p2align 3
.L2371:
	leaq	.LC23(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3109:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv.part.0
	jmp	.L2430
	.p2align 4,,10
	.p2align 3
.L3216:
	movq	%r15, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L2109
	.p2align 4,,10
	.p2align 3
.L3215:
	movq	%r14, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L2112
	.p2align 4,,10
	.p2align 3
.L3214:
	movq	%rdx, %rdi
	movq	%rsi, -312(%rbp)
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	movq	-272(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L2189
	.p2align 4,,10
	.p2align 3
.L3213:
	movq	%rdx, %rdi
	movq	%rsi, -272(%rbp)
	movq	%rdx, -256(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	-256(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2317
	.p2align 4,,10
	.p2align 3
.L3212:
	movq	%r14, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	jmp	.L2320
	.p2align 4,,10
	.p2align 3
.L3211:
	movq	%r14, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L3210:
	movq	%rdx, %rdi
	movq	%rsi, -256(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	movq	-216(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L3209:
	movq	%rdx, %rdi
	movq	%rsi, -256(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	movq	-216(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2345
	.p2align 4,,10
	.p2align 3
.L3208:
	movq	%rdx, %rdi
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2309
	.p2align 4,,10
	.p2align 3
.L3207:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L2059
	.p2align 4,,10
	.p2align 3
.L3206:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L3205:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L2065
	.p2align 4,,10
	.p2align 3
.L3204:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L3203:
	movq	%rdx, %rdi
	movq	%rsi, -224(%rbp)
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	-208(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L3202:
	movq	%r14, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L2133
	.p2align 4,,10
	.p2align 3
.L3201:
	movq	%r14, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	movq	%rax, -184(%rbp)
	jmp	.L2038
	.p2align 4,,10
	.p2align 3
.L3200:
	movq	%r15, %rdi
	movq	%r8, -200(%rbp)
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
	jmp	.L2041
	.p2align 4,,10
	.p2align 3
.L3199:
	movq	%rdx, %rdi
	movq	%r8, -312(%rbp)
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %r8
	movq	-272(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2165
	.p2align 4,,10
	.p2align 3
.L3198:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L2050
	.p2align 4,,10
	.p2align 3
.L3197:
	movq	%rdx, %rdi
	movq	%rsi, -312(%rbp)
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	movq	-272(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2146
	.p2align 4,,10
	.p2align 3
.L3196:
	movq	%rdx, %rdi
	movq	%rsi, -312(%rbp)
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	movq	-272(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2151
	.p2align 4,,10
	.p2align 3
.L3195:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L2053
	.p2align 4,,10
	.p2align 3
.L3194:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L2056
	.p2align 4,,10
	.p2align 3
.L3193:
	movq	%rdx, %rdi
	movq	%rsi, -280(%rbp)
	movq	%rdx, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L3192:
	movq	%rdx, %rdi
	movq	%rsi, -264(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	-216(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2285
	.p2align 4,,10
	.p2align 3
.L3191:
	movq	%rdx, %rdi
	movq	%rsi, -264(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	-216(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2290
	.p2align 4,,10
	.p2align 3
.L3190:
	movq	%r15, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	jmp	.L2203
	.p2align 4,,10
	.p2align 3
.L3189:
	movq	%rdx, %rdi
	movq	%r8, -312(%rbp)
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %r8
	movq	-272(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2208
	.p2align 4,,10
	.p2align 3
.L3188:
	movq	%r14, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L2247
	.p2align 4,,10
	.p2align 3
.L3187:
	movq	%rdx, %rdi
	movq	%rsi, -272(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L3186:
	movq	%r15, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	jmp	.L2238
	.p2align 4,,10
	.p2align 3
.L3185:
	movq	%r15, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L3184:
	movq	%rdx, %rdi
	movq	%rdx, -272(%rbp)
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2271
	.p2align 4,,10
	.p2align 3
.L3183:
	movq	%rdx, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2274
	.p2align 4,,10
	.p2align 3
.L3182:
	movq	%rdx, %rdi
	movq	%rsi, -272(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2277
	.p2align 4,,10
	.p2align 3
.L3181:
	movq	%rdx, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2280
	.p2align 4,,10
	.p2align 3
.L3180:
	movq	%r14, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L3179:
	movq	%rdx, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2268
	.p2align 4,,10
	.p2align 3
.L3178:
	movq	%r14, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L3177:
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2295
	.p2align 4,,10
	.p2align 3
.L3176:
	movq	%rdx, %rdi
	movq	%rsi, -256(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	movq	-216(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2325
	.p2align 4,,10
	.p2align 3
.L3217:
	movq	%r14, %rdi
	movq	%r8, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2425
	.p2align 4,,10
	.p2align 3
.L3111:
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L3219:
	movq	%rdx, %rdi
	movq	%rsi, -224(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdx
	jmp	.L2405
	.p2align 4,,10
	.p2align 3
.L3218:
	movq	%r15, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2420
	.p2align 4,,10
	.p2align 3
.L3223:
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L3222:
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L3221:
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L3220:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2402
.L3110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21291:
	.size	_ZN2v88internal7Genesis16InitializeGlobalENS0_6HandleINS0_14JSGlobalObjectEEENS2_INS0_10JSFunctionEEE, .-_ZN2v88internal7Genesis16InitializeGlobalENS0_6HandleINS0_14JSGlobalObjectEEENS2_INS0_10JSFunctionEEE
	.section	.text._ZN2v88internal7Genesis34InstallInternalPackedArrayFunctionENS0_6HandleINS0_8JSObjectEEEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis34InstallInternalPackedArrayFunctionENS0_6HandleINS0_8JSObjectEEEPKc
	.type	_ZN2v88internal7Genesis34InstallInternalPackedArrayFunctionENS0_6HandleINS0_8JSObjectEEEPKc, @function
_ZN2v88internal7Genesis34InstallInternalPackedArrayFunctionENS0_6HandleINS0_8JSObjectEEEPKc:
.LFB21320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%rsi, -152(%rbp)
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	movq	463(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3225
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3226:
	movq	(%rbx), %r14
	movq	%r12, %rdi
	leaq	-144(%rbp), %r15
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, -144(%rbp)
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rax), %rdx
	movq	%rax, %rsi
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L3228
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L3228:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%r14, -120(%rbp)
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L3236
.L3229:
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L3230
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rcx
.L3231:
	movq	-152(%rbp), %rsi
	movq	(%rbx), %rdi
	movl	$7, %r8d
	movq	%r12, %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3237
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3230:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L3231
	leaq	.LC23(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3225:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L3238
.L3227:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L3226
	.p2align 4,,10
	.p2align 3
.L3236:
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L3229
	.p2align 4,,10
	.p2align 3
.L3238:
	movq	%r15, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3227
.L3237:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21320:
	.size	_ZN2v88internal7Genesis34InstallInternalPackedArrayFunctionENS0_6HandleINS0_8JSObjectEEEPKc, .-_ZN2v88internal7Genesis34InstallInternalPackedArrayFunctionENS0_6HandleINS0_8JSObjectEEEPKc
	.section	.rodata._ZN2v88internal7Genesis26InstallInternalPackedArrayENS0_6HandleINS0_8JSObjectEEEPKc.str1.1,"aMS",@progbits,1
.LC496:
	.string	"InternalArray"
	.section	.rodata._ZN2v88internal7Genesis26InstallInternalPackedArrayENS0_6HandleINS0_8JSObjectEEEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC497:
	.string	"OptimizeInternalPackedArrayPrototypeForAdding"
	.section	.text._ZN2v88internal7Genesis26InstallInternalPackedArrayENS0_6HandleINS0_8JSObjectEEEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis26InstallInternalPackedArrayENS0_6HandleINS0_8JSObjectEEEPKc
	.type	_ZN2v88internal7Genesis26InstallInternalPackedArrayENS0_6HandleINS0_8JSObjectEEEPKc, @function
_ZN2v88internal7Genesis26InstallInternalPackedArrayENS0_6HandleINS0_8JSObjectEEEPKc:
.LFB21321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3240
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3241:
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	$189, %r9d
	movq	%rax, %r8
	movl	$1061, %ecx
	movq	%rax, %r12
	call	_ZN2v88internal12_GLOBAL__N_115InstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE.constprop.0
	movl	$-1, %edx
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movw	%dx, 41(%rax)
	movq	(%rbx), %r13
	movq	(%r14), %rax
	movq	41112(%r13), %rdi
	movq	55(%rax), %r15
	testq	%rdi, %rdi
	je	.L3243
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3244:
	movq	(%rbx), %rdi
	leaq	.LC496(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%r14, %rdi
	leaq	-96(%rbp), %r14
	movq	(%rax), %rdx
	movq	%rax, %r13
	movq	%r13, %rsi
	movzbl	14(%rdx), %eax
	andb	$7, %al
	orl	$16, %eax
	movb	%al, 14(%rdx)
	movq	%r12, %rdx
	call	_ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$1, %edx
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rbx), %rsi
	movl	$6, %ecx
	movq	%r14, %rdi
	leaq	4376(%rsi), %rdx
	addq	$2768, %rsi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	0(%r13), %r15
	movq	(%rbx), %rax
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, -120(%rbp)
	movq	39(%r15), %rax
	movq	%rax, -104(%rbp)
	movl	15(%r15), %r13d
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	shrl	$10, %r13d
	andl	$1023, %r13d
	leal	1(%r13), %ecx
	cmpl	$1020, %ecx
	jg	.L3266
	movl	15(%r15), %eax
	movl	%ecx, %edx
	sall	$10, %edx
	andl	$-1047553, %eax
	orl	%edx, %eax
	movl	%eax, 15(%r15)
	movq	-104(%rbp), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L3267
.L3247:
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	je	.L3268
.L3249:
	testb	$2, -72(%rbp)
	je	.L3269
.L3251:
	movq	(%rbx), %rdi
	movl	$6, %ecx
	movl	$1, %edx
	movq	%r12, %rsi
	leaq	.LC497(%rip), %r8
	call	_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc@PLT
	leaq	.LC149(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis34InstallInternalPackedArrayFunctionENS0_6HandleINS0_8JSObjectEEEPKc
	leaq	.LC148(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis34InstallInternalPackedArrayFunctionENS0_6HandleINS0_8JSObjectEEEPKc
	leaq	.LC151(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis34InstallInternalPackedArrayFunctionENS0_6HandleINS0_8JSObjectEEEPKc
	leaq	.LC152(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis34InstallInternalPackedArrayFunctionENS0_6HandleINS0_8JSObjectEEEPKc
	leaq	.LC154(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis34InstallInternalPackedArrayFunctionENS0_6HandleINS0_8JSObjectEEEPKc
	leaq	.LC102(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis34InstallInternalPackedArrayFunctionENS0_6HandleINS0_8JSObjectEEEPKc
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	addq	$104, %rsi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	xorl	%esi, %esi
	leaq	.LC20(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3270
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3267:
	.cfi_restore_state
	movq	-120(%rbp), %rdi
	movq	%r15, %rsi
	addq	$37592, %rdi
	call	_ZN2v88internal41Heap_MarkingBarrierForDescriptorArraySlowEPNS0_4HeapENS0_10HeapObjectES3_i@PLT
	jmp	.L3247
	.p2align 4,,10
	.p2align 3
.L3243:
	movq	41088(%r13), %rsi
	cmpq	%rsi, 41096(%r13)
	je	.L3271
.L3245:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rsi)
	jmp	.L3244
	.p2align 4,,10
	.p2align 3
.L3240:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3272
.L3242:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L3241
	.p2align 4,,10
	.p2align 3
.L3268:
	testb	$8, 11(%rax)
	je	.L3249
	movl	15(%r15), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%r15)
	testb	$2, -72(%rbp)
	jne	.L3251
.L3269:
	movzbl	9(%r15), %eax
	cmpl	$2, %eax
	jle	.L3252
	movzbl	7(%r15), %edx
	cmpl	%edx, %eax
	je	.L3273
	leal	1(%rax), %edx
	cmpl	$255, %eax
	je	.L3274
	movb	%dl, 9(%r15)
	jmp	.L3251
	.p2align 4,,10
	.p2align 3
.L3252:
	leal	-1(%rax), %edx
	testl	%eax, %eax
	movl	$2, %eax
	cmovne	%edx, %eax
	movb	%al, 9(%r15)
	jmp	.L3251
	.p2align 4,,10
	.p2align 3
.L3266:
	leaq	.LC55(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3272:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3242
	.p2align 4,,10
	.p2align 3
.L3271:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3245
	.p2align 4,,10
	.p2align 3
.L3273:
	movb	$2, 9(%r15)
	jmp	.L3251
.L3274:
	leaq	.LC28(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21321:
	.size	_ZN2v88internal7Genesis26InstallInternalPackedArrayENS0_6HandleINS0_8JSObjectEEEPKc, .-_ZN2v88internal7Genesis26InstallInternalPackedArrayENS0_6HandleINS0_8JSObjectEEEPKc
	.section	.rodata._ZN2v88internal7Genesis19InstallExtraNativesEv.str1.1,"aMS",@progbits,1
.LC498:
	.string	"isTraceCategoryEnabled"
	.section	.text._ZN2v88internal7Genesis19InstallExtraNativesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis19InstallExtraNativesEv
	.type	_ZN2v88internal7Genesis19InstallExtraNativesEv, @function
_ZN2v88internal7Genesis19InstallExtraNativesEv:
.LFB21323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r12
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rdi), %rdi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movl	$2, %r9d
	movl	$1, %r8d
	movl	$717, %ecx
	leaq	.LC498(%rip), %rdx
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	pushq	$2
	leaq	.LC396(%rip), %rdx
	movl	$718, %ecx
	movl	$1, %r9d
	movl	$5, %r8d
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE
	movq	16(%rbx), %rax
	movq	(%r15), %r15
	movq	(%rax), %rdi
	movq	%r15, 367(%rdi)
	popq	%rax
	popq	%rdx
	testb	$1, %r15b
	je	.L3283
	movq	%r15, %rcx
	leaq	367(%rdi), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L3295
	testb	$24, %al
	je	.L3283
.L3298:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3296
	.p2align 4,,10
	.p2align 3
.L3283:
	xorl	%r15d, %r15d
	jmp	.L3280
	.p2align 4,,10
	.p2align 3
.L3297:
	movq	(%rbx), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal12Bootstrapper19CompileExtraBuiltinEPNS0_7IsolateEi
	testb	%al, %al
	je	.L3279
	addl	$1, %r15d
.L3280:
	call	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetBuiltinsCountEv@PLT
	cmpl	%r15d, %eax
	jg	.L3297
	movl	$1, %eax
.L3279:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L3275
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	movb	%al, -56(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movzbl	-56(%rbp), %eax
.L3275:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3295:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3298
	jmp	.L3283
	.p2align 4,,10
	.p2align 3
.L3296:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3283
	.cfi_endproc
.LFE21323:
	.size	_ZN2v88internal7Genesis19InstallExtraNativesEv, .-_ZN2v88internal7Genesis19InstallExtraNativesEv
	.section	.text._ZN2v88internal7Genesis29InitializeNormalizedMapCachesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis29InitializeNormalizedMapCachesEv
	.type	_ZN2v88internal7Genesis29InitializeNormalizedMapCachesEv, @function
_ZN2v88internal7Genesis29InitializeNormalizedMapCachesEv:
.LFB21324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	_ZN2v88internal18NormalizedMapCache3NewEPNS0_7IsolateE@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %r12
	movq	(%rdx), %r13
	movq	%r12, 863(%r13)
	testb	$1, %r12b
	je	.L3299
	movq	%r12, %rbx
	leaq	863(%r13), %r14
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L3311
	testb	$24, %al
	je	.L3299
.L3313:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3312
.L3299:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3311:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L3313
	jmp	.L3299
	.p2align 4,,10
	.p2align 3
.L3312:
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE21324:
	.size	_ZN2v88internal7Genesis29InitializeNormalizedMapCachesEv, .-_ZN2v88internal7Genesis29InitializeNormalizedMapCachesEv
	.section	.text._ZN2v88internal7Genesis21InstallSpecialObjectsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis21InstallSpecialObjectsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE
	.type	_ZN2v88internal7Genesis21InstallSpecialObjectsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE, @function
_ZN2v88internal7Genesis21InstallSpecialObjectsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE:
.LFB21326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	12464(%rdi), %rax
	addl	$1, 41104(%rdi)
	movq	41088(%rdi), %rbx
	movq	41096(%rdi), %r13
	movq	39(%rax), %rax
	movq	1607(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3315
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3316:
	movslq	_ZN2v88internal22FLAG_stack_trace_limitE(%rip), %rsi
	movq	41112(%r12), %rdi
	leaq	3328(%r12), %r15
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L3318
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L3319:
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	cmpb	$0, _ZN2v88internal16FLAG_expose_wasmE(%rip)
	jne	.L3327
	cmpb	$0, _ZN2v88internal17FLAG_validate_asmE(%rip)
	jne	.L3328
.L3322:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L3325
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3325:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3328:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6WasmJs7InstallEPNS0_7IsolateEb@PLT
	jmp	.L3322
	.p2align 4,,10
	.p2align 3
.L3318:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L3329
.L3320:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L3319
	.p2align 4,,10
	.p2align 3
.L3315:
	movq	%rbx, %r14
	cmpq	41096(%r12), %rbx
	je	.L3330
.L3317:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L3316
	.p2align 4,,10
	.p2align 3
.L3327:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6WasmJs7InstallEPNS0_7IsolateEb@PLT
	jmp	.L3322
	.p2align 4,,10
	.p2align 3
.L3329:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L3320
	.p2align 4,,10
	.p2align 3
.L3330:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3317
	.cfi_endproc
.LFE21326:
	.size	_ZN2v88internal7Genesis21InstallSpecialObjectsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE, .-_ZN2v88internal7Genesis21InstallSpecialObjectsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE
	.section	.rodata._ZN2v88internal7Genesis15ExtensionStatesC2Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC499:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v88internal7Genesis15ExtensionStatesC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis15ExtensionStatesC2Ev
	.type	_ZN2v88internal7Genesis15ExtensionStatesC2Ev, @function
_ZN2v88internal7Genesis15ExtensionStatesC2Ev:
.LFB21329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$192, %edi
	subq	$8, %rsp
	call	malloc@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L3334
	movq	$0, (%rax)
	movq	$0, 24(%rax)
	movq	$0, 48(%rax)
	movq	$0, 72(%rax)
	movq	$0, 96(%rax)
	movq	$0, 120(%rax)
	movq	$0, 144(%rax)
	movq	$0, 168(%rax)
	movq	$8, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3334:
	.cfi_restore_state
	leaq	.LC499(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21329:
	.size	_ZN2v88internal7Genesis15ExtensionStatesC2Ev, .-_ZN2v88internal7Genesis15ExtensionStatesC2Ev
	.globl	_ZN2v88internal7Genesis15ExtensionStatesC1Ev
	.set	_ZN2v88internal7Genesis15ExtensionStatesC1Ev,_ZN2v88internal7Genesis15ExtensionStatesC2Ev
	.section	.text._ZN2v88internal7Genesis15ExtensionStates9get_stateEPNS_19RegisteredExtensionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis15ExtensionStates9get_stateEPNS_19RegisteredExtensionE
	.type	_ZN2v88internal7Genesis15ExtensionStates9get_stateEPNS_19RegisteredExtensionE, @function
_ZN2v88internal7Genesis15ExtensionStates9get_stateEPNS_19RegisteredExtensionE:
.LFB21331:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movq	(%rdi), %r8
	movl	8(%rdi), %edi
	sall	$15, %eax
	subl	%esi, %eax
	subl	$1, %edi
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	%edi, %eax
	andl	$1073741823, %eax
	jmp	.L3341
	.p2align 4,,10
	.p2align 3
.L3342:
	cmpq	%rdx, %rsi
	je	.L3337
	addq	$1, %rax
	andq	%rdi, %rax
.L3341:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r8,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L3342
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3337:
	movl	8(%rcx), %eax
	ret
	.cfi_endproc
.LFE21331:
	.size	_ZN2v88internal7Genesis15ExtensionStates9get_stateEPNS_19RegisteredExtensionE, .-_ZN2v88internal7Genesis15ExtensionStates9get_stateEPNS_19RegisteredExtensionE
	.section	.rodata._ZN2v88internal7Genesis23TransferNamedPropertiesENS0_6HandleINS0_8JSObjectEEES4_.str1.1,"aMS",@progbits,1
.LC500:
	.string	"unreachable code"
	.section	.text._ZN2v88internal7Genesis23TransferNamedPropertiesENS0_6HandleINS0_8JSObjectEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis23TransferNamedPropertiesENS0_6HandleINS0_8JSObjectEEES4_
	.type	_ZN2v88internal7Genesis23TransferNamedPropertiesENS0_6HandleINS0_8JSObjectEEES4_, @function
_ZN2v88internal7Genesis23TransferNamedPropertiesENS0_6HandleINS0_8JSObjectEEES4_:
.LFB21344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	%rdx, -64(%rbp)
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	jne	.L3344
	movq	(%rdi), %rbx
	movq	-1(%rax), %rax
	movq	%rsi, %r15
	movq	41112(%rbx), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3345
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -96(%rbp)
.L3346:
	movl	$0, -56(%rbp)
	movl	$24, %r12d
	jmp	.L3348
	.p2align 4,,10
	.p2align 3
.L3419:
	testb	$1, %r8b
	jne	.L3351
	movq	(%r14), %rbx
	movq	41088(%rbx), %rax
	addl	$1, 41104(%rbx)
	movq	(%r14), %rdx
	movq	%rax, -80(%rbp)
	movq	41096(%rbx), %rax
	movq	%rax, -88(%rbp)
	movq	(%rcx), %rax
	movq	-1(%r12,%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3352
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L3353:
	movq	-64(%rbp), %rsi
	movq	(%r14), %rdi
	movq	%r10, %rdx
	movq	%r10, -72(%rbp)
	call	_ZN2v88internalL21PropertyAlreadyExistsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_4NameEEE
	movq	-72(%rbp), %r10
	testb	%al, %al
	jne	.L3416
	movq	(%r15), %rax
	movq	-1(%rax), %r11
	movq	39(%r11), %rax
	movq	7(%r12,%rax), %rcx
	movzbl	7(%r11), %r8d
	movq	%rcx, %rax
	shrq	$38, %rcx
	shrq	$51, %rax
	andl	$7, %ecx
	movq	%rax, %rdi
	movzbl	8(%r11), %eax
	andl	$1023, %edi
	subl	%eax, %r8d
	cmpl	%r8d, %edi
	setl	%al
	jl	.L3417
	subl	%r8d, %edi
	movl	$16, %esi
	leal	16(,%rdi,8), %edi
.L3359:
	cmpl	$2, %ecx
	jne	.L3418
	movl	$32768, %ecx
.L3360:
	movzbl	%al, %edx
	movslq	%r8d, %r8
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	salq	$17, %r8
	salq	$14, %rdx
	movq	%r10, -72(%rbp)
	orq	%r8, %rdx
	salq	$27, %rsi
	orq	%rdi, %rdx
	movq	%r15, %rdi
	orq	%rsi, %rdx
	movl	%r13d, %esi
	shrl	$6, %esi
	orq	%rcx, %rdx
	andl	$7, %esi
	call	_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE@PLT
	movq	-72(%rbp), %r10
	movl	%r13d, %r8d
	movq	-64(%rbp), %rsi
	shrl	$3, %r8d
	movq	(%r14), %rdi
	movq	%rax, %rcx
	andl	$7, %r8d
	movq	%r10, %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L3416:
	movq	-80(%rbp), %rax
	subl	$1, 41104(%rbx)
	movq	%rax, 41088(%rbx)
	movq	-88(%rbp), %rax
	cmpq	41096(%rbx), %rax
	je	.L3357
.L3405:
	movq	%rax, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3357:
	addq	$24, %r12
.L3348:
	movq	(%r15), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	movl	-56(%rbp), %ebx
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%eax, %ebx
	jge	.L3343
	movq	-96(%rbp), %rcx
	addl	$1, %ebx
	movl	%ebx, -56(%rbp)
	movq	(%rcx), %rax
	movq	7(%r12,%rax), %r8
	sarq	$32, %r8
	movq	%r8, %r13
	testb	$2, %r8b
	je	.L3419
	movq	(%r14), %rbx
	movq	-1(%r12,%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3364
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L3365:
	movq	-64(%rbp), %rsi
	movq	(%r14), %rdi
	movq	%r8, %rdx
	movq	%r8, -72(%rbp)
	call	_ZN2v88internalL21PropertyAlreadyExistsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_4NameEEE
	testb	%al, %al
	jne	.L3357
	movq	(%r14), %rbx
	movq	41088(%rbx), %rax
	addl	$1, 41104(%rbx)
	movq	(%r14), %rcx
	movq	%rax, -88(%rbp)
	movq	41096(%rbx), %rax
	movq	%rax, -80(%rbp)
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	15(%r12,%rax), %rsi
	movq	41112(%rcx), %rdi
	movq	-72(%rbp), %r8
	testq	%rdi, %rdi
	je	.L3368
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rdx
.L3369:
	movl	%r13d, %ecx
	movq	-64(%rbp), %rdi
	movq	%r8, %rsi
	andl	$56, %ecx
	orb	$-63, %cl
	call	_ZN2v88internal8JSObject21SetNormalizedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_15PropertyDetailsE@PLT
	movq	-88(%rbp), %rax
	subl	$1, 41104(%rbx)
	movq	%rax, 41088(%rbx)
	movq	-80(%rbp), %rax
	cmpq	41096(%rbx), %rax
	jne	.L3405
	jmp	.L3357
	.p2align 4,,10
	.p2align 3
.L3344:
	movq	-1(%rax), %rdx
	movq	(%rdi), %rbx
	movq	7(%rax), %rsi
	cmpw	$1025, 11(%rdx)
	je	.L3420
	testb	$1, %sil
	jne	.L3390
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %rsi
.L3390:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3391
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3392:
	movq	(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE16IterationIndicesEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	movq	%rax, -72(%rbp)
	movq	(%rax), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L3343
	movq	-64(%rbp), %r15
	xorl	%r12d, %r12d
	jmp	.L3402
	.p2align 4,,10
	.p2align 3
.L3421:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L3395:
	movq	(%r14), %rdi
	movq	%r15, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internalL21PropertyAlreadyExistsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_4NameEEE
	testb	%al, %al
	jne	.L3398
	movl	-56(%rbp), %eax
	movq	(%r14), %rsi
	movq	0(%r13), %rcx
	addl	$8, %eax
	cltq
	movq	-1(%rax,%rcx), %r8
	movq	41112(%rsi), %rdi
	movq	-64(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L3399
	movq	%r8, %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rcx
.L3400:
	movq	0(%r13), %rsi
	leal	72(,%rbx,8), %eax
	cltq
	movq	-1(%rax,%rsi), %r8
	movq	(%r14), %rdi
	movq	%r15, %rsi
	shrq	$35, %r8
	andl	$7, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L3398:
	movq	-72(%rbp), %rax
	addq	$1, %r12
	movq	(%rax), %rax
	cmpl	%r12d, 11(%rax)
	jle	.L3343
.L3402:
	movq	15(%rax,%r12,8), %rax
	sarq	$32, %rax
	leal	(%rax,%rax,2), %ebx
	leal	56(,%rbx,8), %eax
	movl	%eax, -56(%rbp)
	movslq	%eax, %rdx
	movq	0(%r13), %rax
	movq	-1(%rdx,%rax), %rsi
	movq	(%r14), %rcx
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	jne	.L3421
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L3422
.L3396:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L3395
	.p2align 4,,10
	.p2align 3
.L3345:
	movq	41088(%rbx), %rax
	movq	%rax, -96(%rbp)
	cmpq	%rax, 41096(%rbx)
	je	.L3423
.L3347:
	movq	-96(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L3346
	.p2align 4,,10
	.p2align 3
.L3418:
	cmpb	$2, %cl
	jg	.L3361
	je	.L3351
.L3407:
	xorl	%ecx, %ecx
	jmp	.L3360
	.p2align 4,,10
	.p2align 3
.L3361:
	subl	$3, %ecx
	cmpb	$1, %cl
	jbe	.L3407
.L3351:
	leaq	.LC500(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3352:
	movq	41088(%rdx), %r10
	cmpq	%r10, 41096(%rdx)
	je	.L3424
.L3354:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r10)
	jmp	.L3353
	.p2align 4,,10
	.p2align 3
.L3364:
	movq	41088(%rbx), %r8
	cmpq	%r8, 41096(%rbx)
	je	.L3425
.L3366:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r8)
	jmp	.L3365
	.p2align 4,,10
	.p2align 3
.L3417:
	movzbl	8(%r11), %esi
	movzbl	8(%r11), %edx
	addl	%edx, %edi
	sall	$3, %esi
	sall	$3, %edi
	jmp	.L3359
	.p2align 4,,10
	.p2align 3
.L3343:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3368:
	.cfi_restore_state
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L3426
.L3370:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L3369
	.p2align 4,,10
	.p2align 3
.L3424:
	movq	%rdx, %rdi
	movq	%rsi, -104(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%rax, %r10
	jmp	.L3354
	.p2align 4,,10
	.p2align 3
.L3425:
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L3366
	.p2align 4,,10
	.p2align 3
.L3420:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3373
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L3374:
	movq	(%r14), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal18BaseNameDictionaryINS0_16GlobalDictionaryENS0_21GlobalDictionaryShapeEE16IterationIndicesEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	movq	%rax, -56(%rbp)
	movq	(%rax), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L3343
	xorl	%ebx, %ebx
	jmp	.L3388
	.p2align 4,,10
	.p2align 3
.L3427:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L3378:
	movq	(%r14), %rcx
	movq	7(%rsi), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L3380
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L3381:
	movq	-64(%rbp), %rsi
	movq	(%r14), %rdi
	movq	%r15, %rdx
	call	_ZN2v88internalL21PropertyAlreadyExistsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_4NameEEE
	testb	%al, %al
	jne	.L3387
	movq	(%r14), %rdi
	movq	0(%r13), %rax
	movq	41112(%rdi), %r9
	movq	23(%rax), %rsi
	testq	%r9, %r9
	je	.L3384
	movq	%r9, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L3385:
	movq	(%r14), %rdi
	cmpq	%rsi, 96(%rdi)
	je	.L3387
	movq	0(%r13), %rax
	movslq	19(%rax), %r8
	testb	$1, %r8b
	jne	.L3387
	shrl	$3, %r8d
	movq	-64(%rbp), %rsi
	movq	%r15, %rdx
	andl	$7, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L3387:
	movq	-56(%rbp), %rax
	addq	$1, %rbx
	movq	(%rax), %rax
	cmpl	%ebx, 11(%rax)
	jle	.L3343
.L3388:
	movq	15(%rax,%rbx,8), %rax
	movq	(%r14), %r15
	movq	(%r12), %rcx
	sarq	$32, %rax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L3427
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L3428
.L3379:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L3378
	.p2align 4,,10
	.p2align 3
.L3391:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L3429
.L3393:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L3392
	.p2align 4,,10
	.p2align 3
.L3380:
	movq	41088(%rcx), %r15
	cmpq	41096(%rcx), %r15
	je	.L3430
.L3382:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%r15)
	jmp	.L3381
	.p2align 4,,10
	.p2align 3
.L3399:
	movq	41088(%rsi), %rcx
	cmpq	41096(%rsi), %rcx
	je	.L3431
.L3401:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rsi)
	movq	%r8, (%rcx)
	jmp	.L3400
	.p2align 4,,10
	.p2align 3
.L3384:
	movq	41088(%rdi), %rcx
	cmpq	41096(%rdi), %rcx
	je	.L3432
.L3386:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%rcx)
	jmp	.L3385
	.p2align 4,,10
	.p2align 3
.L3422:
	movq	%rcx, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L3396
	.p2align 4,,10
	.p2align 3
.L3426:
	movq	%rcx, %rdi
	movq	%rsi, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L3370
	.p2align 4,,10
	.p2align 3
.L3428:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3379
	.p2align 4,,10
	.p2align 3
.L3430:
	movq	%rcx, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rcx
	movq	%rax, %r15
	jmp	.L3382
	.p2align 4,,10
	.p2align 3
.L3431:
	movq	%rsi, %rdi
	movq	%r8, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L3401
.L3373:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L3433
.L3375:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L3374
.L3432:
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	%rax, %rcx
	jmp	.L3386
.L3423:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, -96(%rbp)
	jmp	.L3347
.L3429:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3393
.L3433:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L3375
	.cfi_endproc
.LFE21344:
	.size	_ZN2v88internal7Genesis23TransferNamedPropertiesENS0_6HandleINS0_8JSObjectEEES4_, .-_ZN2v88internal7Genesis23TransferNamedPropertiesENS0_6HandleINS0_8JSObjectEEES4_
	.section	.text._ZN2v88internal7Genesis25TransferIndexedPropertiesENS0_6HandleINS0_8JSObjectEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis25TransferIndexedPropertiesENS0_6HandleINS0_8JSObjectEEES4_
	.type	_ZN2v88internal7Genesis25TransferIndexedPropertiesENS0_6HandleINS0_8JSObjectEEES4_, @function
_ZN2v88internal7Genesis25TransferIndexedPropertiesENS0_6HandleINS0_8JSObjectEEES4_:
.LFB21345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	movq	%rdi, %rbx
	movq	(%rsi), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %r14
	testq	%rdi, %rdi
	je	.L3435
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3436:
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Factory14CopyFixedArrayENS0_6HandleINS0_10FixedArrayEEE@PLT
	movq	(%r12), %r13
	movq	(%rax), %r12
	leaq	15(%r13), %r14
	movq	%r12, 15(%r13)
	testb	$1, %r12b
	je	.L3434
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L3449
	testb	$24, %al
	je	.L3434
.L3451:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3450
.L3434:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3449:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L3451
	jmp	.L3434
	.p2align 4,,10
	.p2align 3
.L3435:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L3452
.L3437:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L3436
	.p2align 4,,10
	.p2align 3
.L3450:
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L3452:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3437
	.cfi_endproc
.LFE21345:
	.size	_ZN2v88internal7Genesis25TransferIndexedPropertiesENS0_6HandleINS0_8JSObjectEEES4_, .-_ZN2v88internal7Genesis25TransferIndexedPropertiesENS0_6HandleINS0_8JSObjectEEES4_
	.section	.text._ZN2v88internal7Genesis18HookUpGlobalObjectENS0_6HandleINS0_14JSGlobalObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis18HookUpGlobalObjectENS0_6HandleINS0_14JSGlobalObjectEEE
	.type	_ZN2v88internal7Genesis18HookUpGlobalObjectENS0_6HandleINS0_14JSGlobalObjectEEE, @function
_ZN2v88internal7Genesis18HookUpGlobalObjectENS0_6HandleINS0_14JSGlobalObjectEEE:
.LFB21287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	(%rdi), %rbx
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3454
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3455:
	movq	16(%r12), %rax
	movq	0(%r13), %r15
	movq	(%rax), %rbx
	movq	%r15, 31(%rbx)
	leaq	31(%rbx), %rsi
	testb	$1, %r15b
	je	.L3464
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L3478
	testb	$24, %al
	je	.L3464
.L3483:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3479
	.p2align 4,,10
	.p2align 3
.L3464:
	movq	16(%r12), %rax
	movq	0(%r13), %r15
	movq	(%rax), %rbx
	movq	%r15, 1151(%rbx)
	leaq	1151(%rbx), %rsi
	testb	$1, %r15b
	je	.L3463
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L3480
	testb	$24, %al
	je	.L3463
.L3482:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3481
.L3463:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Genesis23TransferNamedPropertiesENS0_6HandleINS0_8JSObjectEEES4_
	addq	$24, %rsp
	movq	%r13, %rdx
	movq	%r14, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Genesis25TransferIndexedPropertiesENS0_6HandleINS0_8JSObjectEEES4_
	.p2align 4,,10
	.p2align 3
.L3480:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3482
	jmp	.L3463
	.p2align 4,,10
	.p2align 3
.L3478:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3483
	jmp	.L3464
	.p2align 4,,10
	.p2align 3
.L3454:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L3484
.L3456:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L3455
	.p2align 4,,10
	.p2align 3
.L3479:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3464
	.p2align 4,,10
	.p2align 3
.L3481:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3463
	.p2align 4,,10
	.p2align 3
.L3484:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3456
	.cfi_endproc
.LFE21287:
	.size	_ZN2v88internal7Genesis18HookUpGlobalObjectENS0_6HandleINS0_14JSGlobalObjectEEE, .-_ZN2v88internal7Genesis18HookUpGlobalObjectENS0_6HandleINS0_14JSGlobalObjectEEE
	.section	.text._ZN2v88internal7Genesis14TransferObjectENS0_6HandleINS0_8JSObjectEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis14TransferObjectENS0_6HandleINS0_8JSObjectEEES4_
	.type	_ZN2v88internal7Genesis14TransferObjectENS0_6HandleINS0_8JSObjectEEES4_, @function
_ZN2v88internal7Genesis14TransferObjectENS0_6HandleINS0_8JSObjectEEES4_:
.LFB21346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r12
	movq	41088(%r12), %rax
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Genesis23TransferNamedPropertiesENS0_6HandleINS0_8JSObjectEEES4_
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal7Genesis25TransferIndexedPropertiesENS0_6HandleINS0_8JSObjectEEES4_
	movq	(%rbx), %rbx
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	23(%rax), %r13
	testq	%rdi, %rdi
	je	.L3486
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3487:
	movq	%r15, %rdi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	-56(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L3492
	movq	%r14, 41096(%r12)
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L3492:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3486:
	.cfi_restore_state
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L3493
.L3488:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	jmp	.L3487
	.p2align 4,,10
	.p2align 3
.L3493:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3488
	.cfi_endproc
.LFE21346:
	.size	_ZN2v88internal7Genesis14TransferObjectENS0_6HandleINS0_8JSObjectEEES4_, .-_ZN2v88internal7Genesis14TransferObjectENS0_6HandleINS0_8JSObjectEEES4_
	.section	.text._ZN2v88internal7Genesis18ConfigureApiObjectENS0_6HandleINS0_8JSObjectEEENS2_INS0_18ObjectTemplateInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis18ConfigureApiObjectENS0_6HandleINS0_8JSObjectEEENS2_INS0_18ObjectTemplateInfoEEE
	.type	_ZN2v88internal7Genesis18ConfigureApiObjectENS0_6HandleINS0_8JSObjectEEENS2_INS0_18ObjectTemplateInfoEEE, @function
_ZN2v88internal7Genesis18ConfigureApiObjectENS0_6HandleINS0_8JSObjectEEENS2_INS0_18ObjectTemplateInfoEEE:
.LFB21342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movq	%rdx, %rsi
	xorl	%edx, %edx
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal10ApiNatives17InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L3498
	movq	%rax, %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis14TransferObjectENS0_6HandleINS0_8JSObjectEEES4_
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3498:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	96(%rax), %rdx
	movq	%rdx, 12480(%rax)
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21342:
	.size	_ZN2v88internal7Genesis18ConfigureApiObjectENS0_6HandleINS0_8JSObjectEEENS2_INS0_18ObjectTemplateInfoEEE, .-_ZN2v88internal7Genesis18ConfigureApiObjectENS0_6HandleINS0_8JSObjectEEENS2_INS0_18ObjectTemplateInfoEEE
	.section	.text._ZN2v88internal7Genesis22ConfigureGlobalObjectsENS_5LocalINS_14ObjectTemplateEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis22ConfigureGlobalObjectsENS_5LocalINS_14ObjectTemplateEEE
	.type	_ZN2v88internal7Genesis22ConfigureGlobalObjectsENS_5LocalINS_14ObjectTemplateEEE, @function
_ZN2v88internal7Genesis22ConfigureGlobalObjectsENS_5LocalINS_14ObjectTemplateEEE:
.LFB21341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-64(%rbp), %r8
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r8, %rdi
	movq	%r8, -72(%rbp)
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%r14), %rdi
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3500
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r12
.L3501:
	movq	16(%rbx), %rax
	movq	(%rbx), %r15
	movq	%r8, %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3503
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3504:
	testq	%r13, %r13
	je	.L3507
	movq	(%r12), %rax
	movq	%r13, %rsi
	xorl	%edx, %edx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal10ApiNatives17InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3557
	movq	%rbx, %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal7Genesis14TransferObjectENS0_6HandleINS0_8JSObjectEEES4_
	movq	(%rbx), %r15
	movq	0(%r13), %rax
	movq	41112(%r15), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3510
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3511:
	movq	71(%rsi), %rax
	andq	$-262144, %rsi
	movq	24(%rsi), %rdx
	movq	-37504(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L3513
	movq	7(%rax), %rsi
.L3513:
	movq	(%rbx), %r13
	cmpq	%rsi, 88(%r13)
	je	.L3507
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3515
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L3516:
	movq	(%r14), %rax
	movq	%r8, %rsi
	xorl	%edx, %edx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal10ApiNatives17InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3557
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis14TransferObjectENS0_6HandleINS0_8JSObjectEEES4_
.L3507:
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	16(%rbx), %rax
	movq	(%rax), %r12
	movq	79(%r12), %rax
	leaq	87(%r12), %r15
	movq	55(%rax), %r13
	movq	%r13, 87(%r12)
	testb	$1, %r13b
	je	.L3536
	movq	%r13, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L3520
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
.L3520:
	testb	$24, %al
	je	.L3536
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3558
	.p2align 4,,10
	.p2align 3
.L3536:
	movq	16(%rbx), %rax
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	711(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3522
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3523:
	movq	16(%rbx), %rax
	movq	(%rbx), %r14
	movq	(%rax), %rax
	movq	735(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3525
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L3526:
	movq	%r13, %rdi
	leaq	.LC20(%rip), %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc@PLT
	xorl	%esi, %esi
	leaq	.LC20(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc@PLT
	movq	16(%rbx), %rax
	movq	(%rax), %r14
	movq	0(%r13), %rax
	movq	55(%rax), %r13
	leaq	719(%r14), %rsi
	movq	%r13, 719(%r14)
	testb	$1, %r13b
	je	.L3535
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	je	.L3529
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-72(%rbp), %rsi
.L3529:
	testb	$24, %al
	je	.L3535
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3559
	.p2align 4,,10
	.p2align 3
.L3535:
	movq	16(%rbx), %rax
	movq	(%rax), %r13
	movq	(%r12), %rax
	movq	55(%rax), %r12
	leaq	743(%r13), %r14
	movq	%r12, 743(%r13)
	testb	$1, %r12b
	je	.L3534
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L3532
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L3532:
	testb	$24, %al
	je	.L3534
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3560
	.p2align 4,,10
	.p2align 3
.L3534:
	movl	$1, %eax
.L3499:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3561
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3522:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3562
.L3524:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3523
	.p2align 4,,10
	.p2align 3
.L3510:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3563
.L3512:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L3511
	.p2align 4,,10
	.p2align 3
.L3503:
	movq	41088(%r15), %r14
	cmpq	%r14, 41096(%r15)
	je	.L3564
.L3505:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L3504
	.p2align 4,,10
	.p2align 3
.L3500:
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L3565
.L3502:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L3501
	.p2align 4,,10
	.p2align 3
.L3525:
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L3566
.L3527:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L3526
	.p2align 4,,10
	.p2align 3
.L3559:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3535
	.p2align 4,,10
	.p2align 3
.L3558:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3536
	.p2align 4,,10
	.p2align 3
.L3560:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3534
	.p2align 4,,10
	.p2align 3
.L3515:
	movq	41088(%r13), %r8
	cmpq	41096(%r13), %r8
	je	.L3567
.L3517:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r8)
	jmp	.L3516
	.p2align 4,,10
	.p2align 3
.L3557:
	movq	(%rbx), %rax
	movq	96(%rax), %rdx
	movq	%rdx, 12480(%rax)
	xorl	%eax, %eax
	jmp	.L3499
	.p2align 4,,10
	.p2align 3
.L3565:
	movq	%r14, %rdi
	movq	%r8, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L3502
	.p2align 4,,10
	.p2align 3
.L3564:
	movq	%r15, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3505
	.p2align 4,,10
	.p2align 3
.L3566:
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L3527
	.p2align 4,,10
	.p2align 3
.L3562:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3524
	.p2align 4,,10
	.p2align 3
.L3563:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L3512
	.p2align 4,,10
	.p2align 3
.L3567:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L3517
.L3561:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21341:
	.size	_ZN2v88internal7Genesis22ConfigureGlobalObjectsENS_5LocalINS_14ObjectTemplateEEE, .-_ZN2v88internal7Genesis22ConfigureGlobalObjectsENS_5LocalINS_14ObjectTemplateEEE
	.section	.text._ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE
	.type	_ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE, @function
_ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE:
.LFB21351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 8(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	40936(%rsi), %rax
	movq	%rsi, (%rdi)
	addl	$1, 8(%rax)
	movq	%rax, 56(%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 24(%rdi)
	movq	%r14, %rdi
	call	_ZN2v88internal11SaveContextC1EPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	call	_ZN2v814ObjectTemplate18InternalFieldCountEv@PLT
	leal	32(,%rax,8), %eax
	movl	%eax, -88(%rbp)
	testq	%r12, %r12
	je	.L3601
.L3569:
	movq	0(%r13), %rax
	movq	41112(%r15), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3570
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L3571:
	movq	71(%rsi), %rax
	andq	$-262144, %rsi
	movq	24(%rsi), %rdx
	movq	-37504(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L3573
	movq	7(%rax), %rsi
.L3573:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3574
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal10ApiNatives23InstantiateRemoteObjectENS0_6HandleINS0_18ObjectTemplateInfoEEE@PLT
	testq	%rax, %rax
	je	.L3602
.L3577:
	movl	-88(%rbp), %edx
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	$1026, %esi
	movl	$3, %ecx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	(%rax), %rdx
	orb	$32, 13(%rdx)
	movq	(%rax), %rdx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	movq	(%rbx), %rax
	movq	(%r12), %rdi
	movq	104(%rax), %r15
	leaq	23(%rdi), %rsi
	movq	%r15, 23(%rdi)
	testb	$1, %r15b
	movq	-96(%rbp), %r10
	je	.L3586
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L3603
	testb	$24, %al
	je	.L3586
.L3609:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3604
	.p2align 4,,10
	.p2align 3
.L3586:
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE@PLT
	movq	(%r12), %rax
	movq	-1(%rax), %r15
	movq	0(%r13), %r13
	movq	31(%r15), %rax
	leaq	31(%r15), %rsi
	testb	$1, %al
	jne	.L3605
.L3581:
	movq	%r13, 31(%r15)
	testb	$1, %r13b
	je	.L3585
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L3606
	testb	$24, %al
	je	.L3585
.L3610:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3607
	.p2align 4,,10
	.p2align 3
.L3585:
	movq	%r12, 24(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88internal11SaveContextD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3608
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3603:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r10, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3609
	jmp	.L3586
	.p2align 4,,10
	.p2align 3
.L3606:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3610
	jmp	.L3585
	.p2align 4,,10
	.p2align 3
.L3574:
	movq	41088(%r15), %rdi
	cmpq	41096(%r15), %rdi
	je	.L3611
.L3576:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdi)
	call	_ZN2v88internal10ApiNatives23InstantiateRemoteObjectENS0_6HandleINS0_18ObjectTemplateInfoEEE@PLT
	testq	%rax, %rax
	jne	.L3577
.L3602:
	leaq	.LC23(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3570:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L3612
.L3572:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L3571
	.p2align 4,,10
	.p2align 3
.L3605:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L3581
	leaq	.LC36(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3604:
	movq	%r15, %rdx
	movq	%r10, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r10
	jmp	.L3586
	.p2align 4,,10
	.p2align 3
.L3607:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3585
	.p2align 4,,10
	.p2align 3
.L3601:
	movq	(%rbx), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal7Factory29NewUninitializedJSGlobalProxyEi@PLT
	movq	%rax, %r12
	jmp	.L3569
	.p2align 4,,10
	.p2align 3
.L3612:
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3572
	.p2align 4,,10
	.p2align 3
.L3611:
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L3576
.L3608:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21351:
	.size	_ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE, .-_ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE
	.globl	_ZN2v88internal7GenesisC1EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE
	.set	_ZN2v88internal7GenesisC1EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE,_ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE
	.section	.text._ZN2v88internal12Bootstrapper16NewRemoteContextENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper16NewRemoteContextENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE
	.type	_ZN2v88internal12Bootstrapper16NewRemoteContextENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE, @function
_ZN2v88internal12Bootstrapper16NewRemoteContextENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE:
.LFB21245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rdi), %r12
	leaq	-128(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r12), %r14
	movq	41096(%r12), %r15
	addl	$1, 41104(%r12)
	movq	(%rbx), %rsi
	call	_ZN2v88internal7GenesisC1EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE
	movq	-104(%rbp), %r13
	movq	-72(%rbp), %rax
	subl	$1, 8(%rax)
	testq	%r13, %r13
	je	.L3629
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	je	.L3618
	movq	(%rbx), %rax
	cmpb	$0, 41458(%rax)
	je	.L3630
.L3618:
	movq	0(%r13), %rbx
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r15
	je	.L3620
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3620:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3621
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3616:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3631
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3621:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3632
.L3623:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rbx, 0(%r13)
	jmp	.L3616
	.p2align 4,,10
	.p2align 3
.L3629:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L3616
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L3616
	.p2align 4,,10
	.p2align 3
.L3630:
	movq	41016(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L3618
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger10LogAllMapsEv@PLT
	jmp	.L3618
	.p2align 4,,10
	.p2align 3
.L3632:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L3623
.L3631:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21245:
	.size	_ZN2v88internal12Bootstrapper16NewRemoteContextENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE, .-_ZN2v88internal12Bootstrapper16NewRemoteContextENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEE
	.section	.text._ZN2v88internal12Bootstrapper21ArchiveSpacePerThreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper21ArchiveSpacePerThreadEv
	.type	_ZN2v88internal12Bootstrapper21ArchiveSpacePerThreadEv, @function
_ZN2v88internal12Bootstrapper21ArchiveSpacePerThreadEv:
.LFB21353:
	.cfi_startproc
	endbr64
	movl	$4, %eax
	ret
	.cfi_endproc
.LFE21353:
	.size	_ZN2v88internal12Bootstrapper21ArchiveSpacePerThreadEv, .-_ZN2v88internal12Bootstrapper21ArchiveSpacePerThreadEv
	.section	.text._ZN2v88internal12Bootstrapper12ArchiveStateEPc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper12ArchiveStateEPc
	.type	_ZN2v88internal12Bootstrapper12ArchiveStateEPc, @function
_ZN2v88internal12Bootstrapper12ArchiveStateEPc:
.LFB21354:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	movl	%eax, (%rsi)
	leaq	4(%rsi), %rax
	movl	$0, 8(%rdi)
	ret
	.cfi_endproc
.LFE21354:
	.size	_ZN2v88internal12Bootstrapper12ArchiveStateEPc, .-_ZN2v88internal12Bootstrapper12ArchiveStateEPc
	.section	.text._ZN2v88internal12Bootstrapper12RestoreStateEPc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper12RestoreStateEPc
	.type	_ZN2v88internal12Bootstrapper12RestoreStateEPc, @function
_ZN2v88internal12Bootstrapper12RestoreStateEPc:
.LFB21355:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	movl	%eax, 8(%rdi)
	leaq	4(%rsi), %rax
	ret
	.cfi_endproc
.LFE21355:
	.size	_ZN2v88internal12Bootstrapper12RestoreStateEPc, .-_ZN2v88internal12Bootstrapper12RestoreStateEPc
	.section	.text._ZN2v88internal12Bootstrapper19FreeThreadResourcesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper19FreeThreadResourcesEv
	.type	_ZN2v88internal12Bootstrapper19FreeThreadResourcesEv, @function
_ZN2v88internal12Bootstrapper19FreeThreadResourcesEv:
.LFB21356:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21356:
	.size	_ZN2v88internal12Bootstrapper19FreeThreadResourcesEv, .-_ZN2v88internal12Bootstrapper19FreeThreadResourcesEv
	.section	.text._ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi,"axG",@progbits,_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi,comdat
	.p2align 4
	.weak	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	.type	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi, @function
_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi:
.LFB24408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rbx
	movswl	9(%rbx), %r10d
	movl	7(%rsi), %r9d
	subl	$1, %r10d
	je	.L3648
	movl	%r10d, %r12d
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L3639:
	movl	%r12d, %ecx
	subl	%r8d, %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	addl	%r8d, %eax
	leal	1(%rax), %r11d
	leal	(%r11,%r11,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	7(%rcx,%rbx), %rcx
	shrq	$41, %rcx
	andl	$1023, %ecx
	leal	3(%rcx,%rcx,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rbx), %rcx
	cmpl	7(%rcx), %r9d
	ja	.L3653
	cmpl	%r8d, %eax
	je	.L3638
	movl	%eax, %r12d
	jmp	.L3639
	.p2align 4,,10
	.p2align 3
.L3653:
	cmpl	%r12d, %r11d
	je	.L3649
	movl	%r11d, %r8d
	jmp	.L3639
	.p2align 4,,10
	.p2align 3
.L3648:
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L3638:
	leal	3(%r8,%r8,2), %r11d
	sall	$3, %r11d
	movslq	%r11d, %r11
	jmp	.L3647
	.p2align 4,,10
	.p2align 3
.L3644:
	movq	(%rdi), %rbx
	addl	$1, %r8d
	movq	7(%r11,%rbx), %rax
	shrq	$41, %rax
	andl	$1023, %eax
	leal	3(%rax,%rax,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rbx), %rcx
	cmpl	7(%rcx), %r9d
	jne	.L3646
	addq	$24, %r11
	cmpq	%rcx, %rsi
	je	.L3654
.L3647:
	cmpl	%r10d, %r8d
	jle	.L3644
.L3646:
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3649:
	.cfi_restore_state
	movl	%r12d, %r8d
	jmp	.L3638
	.p2align 4,,10
	.p2align 3
.L3654:
	cmpl	%eax, %edx
	jle	.L3646
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24408:
	.size	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi, .-_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	.section	.rodata._ZN2v88internal7Genesis14InstallNativesEv.str1.1,"aMS",@progbits,1
.LC501:
	.string	"InternalPackedArray"
.LC502:
	.string	"createPrivateSymbol"
.LC503:
	.string	"uncurryThis"
.LC504:
	.string	"markPromiseAsHandled"
.LC505:
	.string	"promiseState"
.LC506:
	.string	"kPROMISE_PENDING"
.LC507:
	.string	"kPROMISE_FULFILLED"
.LC508:
	.string	"kPROMISE_REJECTED"
.LC509:
	.string	"createPromise"
.LC510:
	.string	"rejectPromise"
.LC511:
	.string	"resolvePromise"
.LC512:
	.string	"isPromise"
.LC513:
	.string	"decodeURI"
.LC514:
	.string	"decodeURIComponent"
.LC515:
	.string	"encodeURI"
.LC516:
	.string	"encodeURIComponent"
.LC517:
	.string	"escape"
.LC518:
	.string	"unescape"
.LC519:
	.string	"eval"
.LC520:
	.string	"length.IsSmi()"
.LC521:
	.string	"Smi::ToInt(length) == 0"
	.section	.rodata._ZN2v88internal7Genesis14InstallNativesEv.str1.8,"aMS",@progbits,1
	.align 8
.LC522:
	.string	"proto->HasSmiOrObjectElements()"
	.section	.text._ZN2v88internal7Genesis14InstallNativesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis14InstallNativesEv
	.type	_ZN2v88internal7Genesis14InstallNativesEv, @function
_ZN2v88internal7Genesis14InstallNativesEv:
.LFB21322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	movq	(%rdi), %rdi
	movq	%rax, -120(%rbp)
	movq	41096(%r14), %rax
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	(%r12), %r13
	movq	(%rax), %r15
	movq	%r13, 375(%r15)
	testb	$1, %r13b
	je	.L3783
	movq	%r13, %rcx
	leaq	375(%r15), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	jne	.L3878
	testb	$24, %al
	je	.L3783
.L3914:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3879
	.p2align 4,,10
	.p2align 3
.L3783:
	leaq	.LC501(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Genesis26InstallInternalPackedArrayENS0_6HandleINS0_8JSObjectEEEPKc
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$815, %ecx
	movl	$2, %r9d
	leaq	.LC502(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$341, %ecx
	movl	$2, %r9d
	leaq	.LC503(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$816, %ecx
	movl	$2, %r9d
	leaq	.LC504(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$817, %ecx
	movl	$2, %r9d
	leaq	.LC505(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movl	$2, %r8d
	movq	%r12, %rsi
	movq	%rax, %rcx
	leaq	.LC506(%rip), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movl	$2, %r8d
	movq	%r12, %rsi
	movq	%rax, %rcx
	leaq	.LC507(%rip), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	movl	$2, %esi
	call	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movl	$2, %r8d
	movq	%r12, %rsi
	movq	%rax, %rcx
	leaq	.LC508(%rip), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$523, %edx
	leaq	128(%rdi), %rsi
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movl	$2, %r8d
	movq	%r12, %rsi
	movq	%rax, %rcx
	movq	(%rax), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %eax
	andl	$-33, %eax
	movl	%eax, 47(%rdx)
	leaq	.LC509(%rip), %rdx
	movq	(%rbx), %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movl	$1, %r8d
	movl	$2, %ecx
	movl	$524, %edx
	leaq	128(%rdi), %rsi
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movl	$2, %r8d
	movq	%r12, %rsi
	movq	%rax, %rcx
	movq	(%rax), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %eax
	andl	$-33, %eax
	movl	%eax, 47(%rdx)
	leaq	.LC510(%rip), %rdx
	movq	(%rbx), %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rdi
	movl	$1, %r8d
	movl	$2, %ecx
	movl	$525, %edx
	leaq	128(%rdi), %rsi
	call	_ZN2v88internal12_GLOBAL__N_120SimpleCreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_8Builtins4NameEib
	movq	%r12, %rsi
	movl	$2, %r8d
	movq	%rax, %rcx
	movq	(%rax), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %eax
	andl	$-33, %eax
	movl	%eax, 47(%rdx)
	leaq	.LC511(%rip), %rdx
	movq	(%rbx), %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %r13
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	1927(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3659
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L3660:
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	movl	$2, %r8d
	leaq	.LC512(%rip), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	%r12, %rdi
	leaq	.LC20(%rip), %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc@PLT
	movq	(%rbx), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3662
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3663:
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	subq	$8, %rsp
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	pushq	$166
	movl	$1041, %edx
	movq	%rax, %r9
	movl	$32, %ecx
	leaq	128(%rdi), %rsi
	call	_ZN2v88internal12_GLOBAL__N_114CreateFunctionEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_12InstanceTypeEiiNS4_INS0_10HeapObjectEEENS0_8Builtins4NameE
	movq	16(%rbx), %rdx
	movq	(%rax), %r12
	movq	(%rdx), %r13
	movq	%r12, 895(%r13)
	leaq	895(%r13), %rsi
	popq	%rax
	popq	%rdx
	testb	$1, %r12b
	je	.L3782
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L3880
	testb	$24, %al
	je	.L3782
.L3913:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3881
	.p2align 4,,10
	.p2align 3
.L3782:
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	movl	$1024, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %r12
	movq	(%rdx), %r13
	movq	%r12, 391(%r13)
	leaq	391(%r13), %rsi
	testb	$1, %r12b
	je	.L3781
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L3882
	testb	$24, %al
	je	.L3781
.L3916:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3883
	.p2align 4,,10
	.p2align 3
.L3781:
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	movl	$256, %esi
	xorl	%ecx, %ecx
	call	_ZN2v88internal9HashTableINS0_22SimpleNumberDictionaryENS0_27SimpleNumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %r12
	movq	(%rdx), %r13
	movq	%r12, 1231(%r13)
	leaq	1231(%r13), %rsi
	testb	$1, %r12b
	je	.L3780
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L3884
	testb	$24, %al
	je	.L3780
.L3915:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3885
	.p2align 4,,10
	.p2align 3
.L3780:
	movq	16(%rbx), %rax
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	879(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3674
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3675:
	movq	16(%rbx), %rax
	movq	(%rax), %r13
	movq	55(%rsi), %rax
	movq	23(%rax), %rax
	leaq	887(%r13), %rsi
	movq	-1(%rax), %r12
	movq	%r12, 887(%r13)
	testb	$1, %r12b
	je	.L3779
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L3886
	testb	$24, %al
	je	.L3779
.L3919:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3887
	.p2align 4,,10
	.p2align 3
.L3779:
	movq	16(%rbx), %rax
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	1439(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3680
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3681:
	movq	16(%rbx), %rax
	movq	(%rax), %r13
	movq	55(%rsi), %rax
	movq	23(%rax), %rax
	leaq	1447(%r13), %rsi
	movq	-1(%rax), %r12
	movq	%r12, 1447(%r13)
	testb	$1, %r12b
	je	.L3778
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L3888
	testb	$24, %al
	je	.L3778
.L3918:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3889
	.p2align 4,,10
	.p2align 3
.L3778:
	movq	16(%rbx), %rax
	movq	(%rbx), %r15
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3686
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3687:
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$357, %ecx
	movq	%r13, %rsi
	leaq	.LC513(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$358, %ecx
	leaq	.LC514(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$359, %ecx
	leaq	.LC515(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$360, %ecx
	leaq	.LC516(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$361, %ecx
	leaq	.LC517(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$362, %ecx
	leaq	.LC518(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	xorl	%r8d, %r8d
	leaq	.LC519(%rip), %rdx
	movl	$2, %r9d
	movl	$363, %ecx
	call	_ZN2v88internal12_GLOBAL__N_121SimpleInstallFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEibNS0_18PropertyAttributesE.constprop.3
	movq	16(%rbx), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rdi
	movq	%r15, 1631(%rdi)
	leaq	1631(%rdi), %rsi
	testb	$1, %r15b
	je	.L3777
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	jne	.L3890
	testb	$24, %al
	je	.L3777
.L3917:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3891
	.p2align 4,,10
	.p2align 3
.L3777:
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$1, %r8d
	movl	$364, %ecx
	leaq	.LC171(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$365, %ecx
	movl	$1, %r8d
	leaq	.LC173(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128InstallFunctionWithBuiltinIdEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEPKcNS0_8Builtins4NameEib.constprop.0
	movq	16(%rbx), %rax
	movq	(%rbx), %r13
	movq	(%rax), %rax
	movq	103(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3692
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3693:
	movq	(%rbx), %r13
	movq	-1(%rsi), %rax
	movzbl	13(%rax), %eax
	testb	$1, %al
	je	.L3695
	movq	-1(%rsi), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L3697
.L3698:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3701
.L3910:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	23(%rsi), %rdx
	testb	$1, %dl
	jne	.L3892
.L3704:
	shrq	$32, %rdx
	jne	.L3893
	movq	-1(%rsi), %rdx
	cmpb	$31, 14(%rdx)
	ja	.L3894
	movq	(%rax), %r15
	movq	(%rbx), %rax
	movq	288(%rax), %r13
	leaq	15(%r15), %rsi
	movq	%r15, %rdi
	movq	%r13, 15(%r15)
	movq	%r13, %rdx
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L3895
.L3707:
	movq	(%rbx), %rdi
	movl	$4, %r8d
	movl	$3, %ecx
	movl	$56, %edx
	movl	$1057, %esi
	leaq	-104(%rbp), %r13
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	(%rbx), %rdi
	movl	$4, %edx
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rbx), %rsi
	movl	$4, %r9d
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	2608(%rsi), %rdx
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$4, %r9d
	movq	%r12, %rdi
	leaq	3272(%rsi), %rdx
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rsi
	xorl	%r8d, %r8d
	movl	$2, %ecx
	movl	$4, %r9d
	movq	%r12, %rdi
	leaq	2464(%rsi), %rdx
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rsi
	movl	$3, %ecx
	xorl	%r8d, %r8d
	movl	$4, %r9d
	movq	%r12, %rdi
	leaq	2280(%rsi), %rdx
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rcx
	movq	12464(%rcx), %rax
	movq	39(%rax), %rax
	movq	519(%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L3709
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L3710:
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$1, %ecx
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	(%r15), %rdi
	movq	(%rax), %rax
	leaq	31(%rdi), %rsi
	movq	879(%rax), %rdx
	movq	31(%rdi), %rax
	testb	$1, %al
	jne	.L3896
.L3712:
	movq	%rdx, 31(%rdi)
	testb	$1, %dl
	je	.L3776
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	je	.L3714
	movq	%rdx, -160(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
.L3714:
	testb	$24, %al
	je	.L3776
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3897
	.p2align 4,,10
	.p2align 3
.L3776:
	movq	16(%rbx), %rax
	movq	(%r15), %r15
	movq	(%rax), %rdi
	movq	%r15, 63(%rdi)
	leaq	63(%rdi), %rsi
	testb	$1, %r15b
	je	.L3775
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	je	.L3717
	movq	%r15, %rdx
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
.L3717:
	testb	$24, %al
	je	.L3775
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3898
	.p2align 4,,10
	.p2align 3
.L3775:
	movq	(%rbx), %rdi
	movl	$4, %r8d
	movl	$3, %ecx
	movl	$56, %edx
	movl	$1057, %esi
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	(%rbx), %rdi
	movl	$4, %edx
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rbx), %rsi
	movl	$4, %r9d
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	3544(%rsi), %rdx
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$4, %r9d
	movq	%r12, %rdi
	leaq	3600(%rsi), %rdx
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rsi
	xorl	%r8d, %r8d
	movl	$2, %ecx
	movl	$4, %r9d
	movq	%r12, %rdi
	leaq	2464(%rsi), %rdx
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rsi
	movl	$3, %ecx
	xorl	%r8d, %r8d
	movl	$4, %r9d
	movq	%r12, %rdi
	leaq	2280(%rsi), %rdx
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rcx
	movq	12464(%rcx), %rax
	movq	39(%rax), %rax
	movq	519(%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L3719
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L3720:
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movl	$1, %ecx
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	16(%rbx), %rax
	movq	(%r15), %rdi
	movq	(%rax), %rax
	leaq	31(%rdi), %rsi
	movq	879(%rax), %rdx
	movq	31(%rdi), %rax
	testb	$1, %al
	jne	.L3899
.L3722:
	movq	%rdx, 31(%rdi)
	testb	$1, %dl
	je	.L3774
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	jne	.L3900
.L3725:
	testb	$24, %al
	je	.L3774
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3901
	.p2align 4,,10
	.p2align 3
.L3774:
	movq	16(%rbx), %rax
	movq	(%r15), %r15
	movq	(%rax), %rdi
	movq	%r15, 311(%rdi)
	leaq	311(%rdi), %rsi
	testb	$1, %r15b
	je	.L3773
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	jne	.L3902
.L3728:
	testb	$24, %al
	je	.L3773
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3903
	.p2align 4,,10
	.p2align 3
.L3773:
	movq	16(%rbx), %rax
	movq	(%rbx), %r15
	movq	(%rax), %rax
	movq	103(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3730
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r9
.L3731:
	movq	55(%rsi), %rsi
	movq	(%rbx), %r15
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L3734
	movq	23(%rsi), %rsi
.L3734:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3735
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-136(%rbp), %r9
	movq	%rax, %r10
.L3736:
	movq	(%rbx), %rdi
	movl	$56, %edx
	movl	$1061, %esi
	movl	$3, %r8d
	movl	$3, %ecx
	movq	%r9, -144(%rbp)
	movq	%r10, -136(%rbp)
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %r10
	movq	(%rax), %rdi
	movq	%rax, %r15
	movq	(%r9), %rdx
	movq	31(%rdi), %rax
	leaq	31(%rdi), %rsi
	testb	$1, %al
	jne	.L3904
.L3738:
	movq	%rdx, 31(%rdi)
	testb	$1, %dl
	je	.L3772
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	jne	.L3905
.L3740:
	testb	$24, %al
	je	.L3772
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3906
	.p2align 4,,10
	.p2align 3
.L3772:
	movq	(%r15), %rdx
	movl	$1, %ecx
	movq	%r15, %rsi
	movzbl	13(%rdx), %eax
	andl	$-2, %eax
	movb	%al, 13(%rdx)
	movq	%r10, %rdx
	movq	(%rbx), %rdi
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	(%rbx), %rdi
	movl	$4, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	103(%rax), %rcx
	movq	(%rbx), %rdx
	movq	55(%rcx), %rax
	movq	41112(%rdx), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3742
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-136(%rbp), %rcx
	movq	(%rax), %rsi
	movq	%rax, %r8
.L3743:
	movq	(%rbx), %rdi
	movq	%rsi, -96(%rbp)
	movq	55(%rcx), %r9
	leaq	2768(%rdi), %rax
	movq	2768(%rdi), %rsi
	movq	%rax, -136(%rbp)
	movl	15(%r9), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	movl	%edx, %r11d
	je	.L3785
	movl	%r9d, %eax
	movq	41080(%rdi), %r10
	shrl	$3, %eax
	movl	%eax, -144(%rbp)
	xorl	7(%rsi), %eax
	andl	$63, %eax
	movq	%rax, %rdx
	salq	$4, %rdx
	cmpq	(%r10,%rdx), %r9
	jne	.L3746
	cmpq	8(%r10,%rdx), %rsi
	jne	.L3746
	movl	1024(%r10,%rax,4), %eax
	cmpl	$-2, %eax
	je	.L3746
	leal	3(%rax,%rax,2), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
.L3745:
	movq	(%r8), %rax
	movq	7(%rdx,%rax), %rcx
	movq	15(%rdx,%rax), %rsi
	movq	41112(%rdi), %r8
	shrq	$35, %rcx
	andl	$7, %ecx
	testq	%r8, %r8
	je	.L3752
	movq	%r8, %rdi
	movl	%ecx, -144(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-144(%rbp), %ecx
	movq	%rax, %rdx
.L3753:
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rsi
	movl	$4, %r9d
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	2688(%rsi), %rdx
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$4, %r9d
	movq	%r12, %rdi
	leaq	2712(%rsi), %rdx
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rsi
	xorl	%r8d, %r8d
	movl	$2, %ecx
	movl	$4, %r9d
	movq	%r12, %rdi
	leaq	2648(%rsi), %rdx
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	16(%rbx), %rax
	movq	(%r15), %r15
	movq	(%rax), %rdi
	movq	%r15, 1103(%rdi)
	leaq	1103(%rdi), %rsi
	testb	$1, %r15b
	je	.L3771
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	jne	.L3907
.L3756:
	testb	$24, %al
	je	.L3771
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3908
	.p2align 4,,10
	.p2align 3
.L3771:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	movl	$2, %ecx
	leaq	4368(%rsi), %r15
	addq	$3856, %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	16(%rbx), %rax
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	1191(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3758
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3759:
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rax
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$2, %ecx
	leaq	3856(%rax), %rsi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	16(%rbx), %rax
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	383(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3761
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3762:
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rax
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$2, %ecx
	leaq	3856(%rax), %rsi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	16(%rbx), %rax
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	1199(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3764
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3765:
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	(%rbx), %rax
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$2, %ecx
	leaq	3856(%rax), %rsi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	16(%rbx), %rax
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	1207(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3767
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L3768:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	-120(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-128(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L3770
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3770:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3909
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3695:
	.cfi_restore_state
	movq	55(%rsi), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L3698
	movq	41112(%r13), %rdi
	movq	23(%rsi), %rsi
	testq	%rdi, %rdi
	jne	.L3910
.L3701:
	movq	41088(%r13), %rax
	cmpq	%rax, 41096(%r13)
	je	.L3911
.L3703:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	movq	23(%rsi), %rdx
	testb	$1, %dl
	je	.L3704
.L3892:
	leaq	.LC520(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3895:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3707
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3707
	movq	-136(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3707
	.p2align 4,,10
	.p2align 3
.L3709:
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L3912
.L3711:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L3710
	.p2align 4,,10
	.p2align 3
.L3880:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-136(%rbp), %rsi
	testb	$24, %al
	jne	.L3913
	jmp	.L3782
	.p2align 4,,10
	.p2align 3
.L3878:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3914
	jmp	.L3783
	.p2align 4,,10
	.p2align 3
.L3884:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-136(%rbp), %rsi
	testb	$24, %al
	jne	.L3915
	jmp	.L3780
	.p2align 4,,10
	.p2align 3
.L3882:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-136(%rbp), %rsi
	testb	$24, %al
	jne	.L3916
	jmp	.L3781
	.p2align 4,,10
	.p2align 3
.L3890:
	movq	%r15, %rdx
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3917
	jmp	.L3777
	.p2align 4,,10
	.p2align 3
.L3888:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-136(%rbp), %rsi
	testb	$24, %al
	jne	.L3918
	jmp	.L3778
	.p2align 4,,10
	.p2align 3
.L3886:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-136(%rbp), %rsi
	testb	$24, %al
	jne	.L3919
	jmp	.L3779
	.p2align 4,,10
	.p2align 3
.L3680:
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	je	.L3920
.L3682:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3681
	.p2align 4,,10
	.p2align 3
.L3674:
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	je	.L3921
.L3676:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3675
	.p2align 4,,10
	.p2align 3
.L3686:
	movq	41088(%r15), %r13
	cmpq	%r13, 41096(%r15)
	je	.L3922
.L3688:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L3687
	.p2align 4,,10
	.p2align 3
.L3692:
	movq	41088(%r13), %rax
	cmpq	%rax, 41096(%r13)
	je	.L3923
.L3694:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L3693
	.p2align 4,,10
	.p2align 3
.L3662:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3924
.L3664:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L3663
	.p2align 4,,10
	.p2align 3
.L3659:
	movq	41088(%r13), %rcx
	cmpq	41096(%r13), %rcx
	je	.L3925
.L3661:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rcx)
	jmp	.L3660
	.p2align 4,,10
	.p2align 3
.L3719:
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L3926
.L3721:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L3720
	.p2align 4,,10
	.p2align 3
.L3735:
	movq	41088(%r15), %r10
	cmpq	41096(%r15), %r10
	je	.L3927
.L3737:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r10)
	jmp	.L3736
	.p2align 4,,10
	.p2align 3
.L3730:
	movq	41088(%r15), %r9
	cmpq	41096(%r15), %r9
	je	.L3928
.L3732:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r9)
	jmp	.L3731
	.p2align 4,,10
	.p2align 3
.L3742:
	movq	41088(%rdx), %r8
	cmpq	41096(%rdx), %r8
	je	.L3929
.L3744:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r8)
	jmp	.L3743
	.p2align 4,,10
	.p2align 3
.L3767:
	movq	41088(%rdx), %r15
	cmpq	%r15, 41096(%rdx)
	je	.L3930
.L3769:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L3768
	.p2align 4,,10
	.p2align 3
.L3764:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L3931
.L3766:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L3765
	.p2align 4,,10
	.p2align 3
.L3761:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L3932
.L3763:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L3762
	.p2align 4,,10
	.p2align 3
.L3758:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L3933
.L3760:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L3759
	.p2align 4,,10
	.p2align 3
.L3752:
	movq	41088(%rdi), %rdx
	cmpq	41096(%rdi), %rdx
	je	.L3934
.L3754:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%rdx)
	jmp	.L3753
	.p2align 4,,10
	.p2align 3
.L3896:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L3712
.L3723:
	leaq	.LC36(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3899:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L3722
	jmp	.L3723
	.p2align 4,,10
	.p2align 3
.L3902:
	movq	%r15, %rdx
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L3728
	.p2align 4,,10
	.p2align 3
.L3900:
	movq	%rdx, -160(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L3725
	.p2align 4,,10
	.p2align 3
.L3907:
	movq	%r15, %rdx
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L3756
	.p2align 4,,10
	.p2align 3
.L3905:
	movq	%r10, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-168(%rbp), %r10
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	8(%rcx), %rax
	movq	-144(%rbp), %rdi
	jmp	.L3740
	.p2align 4,,10
	.p2align 3
.L3697:
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L3698
	movq	31(%rsi), %rsi
	testb	$1, %sil
	je	.L3698
	jmp	.L3697
	.p2align 4,,10
	.p2align 3
.L3879:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3783
	.p2align 4,,10
	.p2align 3
.L3881:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3782
	.p2align 4,,10
	.p2align 3
.L3883:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3781
	.p2align 4,,10
	.p2align 3
.L3885:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3780
	.p2align 4,,10
	.p2align 3
.L3887:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3779
	.p2align 4,,10
	.p2align 3
.L3889:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3778
	.p2align 4,,10
	.p2align 3
.L3891:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3777
	.p2align 4,,10
	.p2align 3
.L3897:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3776
	.p2align 4,,10
	.p2align 3
.L3898:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3775
	.p2align 4,,10
	.p2align 3
.L3903:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3773
	.p2align 4,,10
	.p2align 3
.L3901:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3774
	.p2align 4,,10
	.p2align 3
.L3906:
	movq	%r10, -136(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %r10
	jmp	.L3772
	.p2align 4,,10
	.p2align 3
.L3908:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3771
	.p2align 4,,10
	.p2align 3
.L3904:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L3738
	jmp	.L3723
	.p2align 4,,10
	.p2align 3
.L3746:
	cmpl	$8, %r11d
	jg	.L3935
	movq	-96(%rbp), %rax
	movq	%r14, -152(%rbp)
	movl	$24, %edx
	xorl	%ecx, %ecx
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L3747:
	movq	-1(%rdx,%r14), %rdi
	movl	%ecx, %eax
	leal	1(%rcx), %ecx
	cmpq	%rdi, %rsi
	je	.L3936
	addq	$24, %rdx
	cmpl	%ecx, %r11d
	jne	.L3747
	movq	-152(%rbp), %r14
	xorl	%edx, %edx
	movl	$-1, %eax
	jmp	.L3751
	.p2align 4,,10
	.p2align 3
.L3893:
	leaq	.LC521(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3925:
	movq	%r13, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L3661
	.p2align 4,,10
	.p2align 3
.L3924:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3664
	.p2align 4,,10
	.p2align 3
.L3911:
	movq	%r13, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L3703
	.p2align 4,,10
	.p2align 3
.L3923:
	movq	%r13, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L3694
	.p2align 4,,10
	.p2align 3
.L3922:
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3688
	.p2align 4,,10
	.p2align 3
.L3921:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L3676
	.p2align 4,,10
	.p2align 3
.L3920:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L3682
	.p2align 4,,10
	.p2align 3
.L3894:
	leaq	.LC522(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3935:
	movl	%r11d, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r9, -176(%rbp)
	movq	%r8, -168(%rbp)
	movq	%r10, -160(%rbp)
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	movq	-176(%rbp), %r9
	movq	-168(%rbp), %r8
	leal	3(%rax,%rax,2), %edx
	movq	-160(%rbp), %r10
	movq	-152(%rbp), %rsi
	sall	$3, %edx
	movslq	%edx, %rdx
.L3751:
	movl	-144(%rbp), %ecx
	xorl	7(%rsi), %ecx
	movq	%rsi, %xmm1
	movq	%r9, %xmm0
	andl	$63, %ecx
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, %rsi
	salq	$4, %rsi
	movups	%xmm0, (%r10,%rsi)
	movl	%eax, 1024(%r10,%rcx,4)
	movq	(%rbx), %rdi
	jmp	.L3745
	.p2align 4,,10
	.p2align 3
.L3912:
	movq	%rcx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L3711
	.p2align 4,,10
	.p2align 3
.L3926:
	movq	%rcx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L3721
	.p2align 4,,10
	.p2align 3
.L3928:
	movq	%r15, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L3732
	.p2align 4,,10
	.p2align 3
.L3927:
	movq	%r15, %rdi
	movq	%rsi, -144(%rbp)
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %r9
	movq	%rax, %r10
	jmp	.L3737
	.p2align 4,,10
	.p2align 3
.L3934:
	movq	%rsi, -160(%rbp)
	movl	%ecx, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movl	-152(%rbp), %ecx
	movq	-144(%rbp), %rdi
	movq	%rax, %rdx
	jmp	.L3754
	.p2align 4,,10
	.p2align 3
.L3929:
	movq	%rdx, %rdi
	movq	%rcx, -152(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rcx
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L3744
	.p2align 4,,10
	.p2align 3
.L3930:
	movq	%rdx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L3769
	.p2align 4,,10
	.p2align 3
.L3931:
	movq	%rdx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	jmp	.L3766
	.p2align 4,,10
	.p2align 3
.L3932:
	movq	%rdx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	jmp	.L3763
	.p2align 4,,10
	.p2align 3
.L3933:
	movq	%rdx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	jmp	.L3760
	.p2align 4,,10
	.p2align 3
.L3936:
	movq	-152(%rbp), %r14
	jmp	.L3751
	.p2align 4,,10
	.p2align 3
.L3785:
	xorl	%edx, %edx
	jmp	.L3745
.L3909:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21322:
	.size	_ZN2v88internal7Genesis14InstallNativesEv, .-_ZN2v88internal7Genesis14InstallNativesEv
	.section	.text._ZN2v88internal12_GLOBAL__N_116ReplaceAccessorsEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS0_18PropertyAttributesENS4_INS0_12AccessorPairEEE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_116ReplaceAccessorsEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS0_18PropertyAttributesENS4_INS0_12AccessorPairEEE.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_116ReplaceAccessorsEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS0_18PropertyAttributesENS4_INS0_12AccessorPairEEE.constprop.0:
.LFB28362:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rbx
	movq	(%rdx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	39(%rbx), %rax
	movq	%rax, -104(%rbp)
	movl	15(%rbx), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	je	.L3946
	movl	7(%r14), %eax
	movl	%ebx, %r9d
	movq	41080(%rdi), %r8
	shrl	$3, %r9d
	xorl	%r9d, %eax
	andl	$63, %eax
	movq	%rax, %rcx
	salq	$4, %rcx
	cmpq	(%r8,%rcx), %rbx
	jne	.L3939
	cmpq	8(%r8,%rcx), %r14
	jne	.L3939
	movl	1024(%r8,%rax,4), %r12d
	leaq	-104(%rbp), %r10
	cmpl	$-2, %r12d
	je	.L3939
.L3938:
	leaq	-96(%rbp), %r14
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-120(%rbp), %r10
	movq	%r14, %rdx
	movl	%r12d, %esi
	movq	%r10, %rdi
	call	_ZN2v88internal15DescriptorArray7ReplaceEiPNS0_10DescriptorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3953
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3939:
	.cfi_restore_state
	cmpl	$8, %edx
	jg	.L3954
	movq	-104(%rbp), %rdi
	movl	$24, %ecx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3940:
	movq	-1(%rcx,%rdi), %rsi
	movl	%eax, %r12d
	leal	1(%rax), %eax
	cmpq	%rsi, %r14
	je	.L3952
	addq	$24, %rcx
	cmpl	%eax, %edx
	jne	.L3940
	movl	$-1, %r12d
.L3952:
	leaq	-104(%rbp), %r10
	jmp	.L3944
	.p2align 4,,10
	.p2align 3
.L3954:
	leaq	-104(%rbp), %r10
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movl	%r9d, -132(%rbp)
	movq	%r10, %rdi
	movq	%r8, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	movl	-132(%rbp), %r9d
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %r10
	movl	%eax, %r12d
.L3944:
	xorl	7(%r14), %r9d
	movq	%rbx, %xmm0
	movq	%r14, %xmm1
	andl	$63, %r9d
	punpcklqdq	%xmm1, %xmm0
	movq	%r9, %rax
	salq	$4, %rax
	movups	%xmm0, (%r8,%rax)
	movl	%r12d, 1024(%r8,%r9,4)
	jmp	.L3938
	.p2align 4,,10
	.p2align 3
.L3946:
	movl	$-1, %r12d
	leaq	-104(%rbp), %r10
	jmp	.L3938
.L3953:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28362:
	.size	_ZN2v88internal12_GLOBAL__N_116ReplaceAccessorsEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS0_18PropertyAttributesENS4_INS0_12AccessorPairEEE.constprop.0, .-_ZN2v88internal12_GLOBAL__N_116ReplaceAccessorsEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS0_18PropertyAttributesENS4_INS0_12AccessorPairEEE.constprop.0
	.section	.text._ZN2v88internal7Genesis31AddRestrictedFunctionPropertiesENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis31AddRestrictedFunctionPropertiesENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal7Genesis31AddRestrictedFunctionPropertiesENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal7Genesis31AddRestrictedFunctionPropertiesENS0_6HandleINS0_10JSFunctionEEE:
.LFB21281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	48(%rdi), %r13
	testq	%r13, %r13
	je	.L3981
.L3956:
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Factory15NewAccessorPairEv@PLT
	movq	0(%r13), %r15
	movq	(%rax), %rdi
	movq	%rax, %r12
	movq	%r15, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %r15b
	je	.L3967
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L3982
	testb	$24, %al
	je	.L3967
.L3987:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3983
	.p2align 4,,10
	.p2align 3
.L3967:
	movq	(%r12), %r15
	movq	0(%r13), %r13
	movq	%r13, 15(%r15)
	leaq	15(%r15), %rsi
	testb	$1, %r13b
	je	.L3966
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L3984
	testb	$24, %al
	je	.L3966
.L3986:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3985
	.p2align 4,,10
	.p2align 3
.L3966:
	movq	(%rbx), %r15
	movq	(%r14), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3963
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3964:
	movq	(%rbx), %rdi
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	2040(%rdi), %rdx
	call	_ZN2v88internal12_GLOBAL__N_116ReplaceAccessorsEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS0_18PropertyAttributesENS4_INS0_12AccessorPairEEE.constprop.0
	movq	(%rbx), %rdi
	addq	$40, %rsp
	movq	%r12, %rcx
	popq	%rbx
	movq	%r13, %rsi
	popq	%r12
	leaq	2232(%rdi), %rdx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12_GLOBAL__N_116ReplaceAccessorsEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS0_18PropertyAttributesENS4_INS0_12AccessorPairEEE.constprop.0
	.p2align 4,,10
	.p2align 3
.L3984:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3986
	jmp	.L3966
	.p2align 4,,10
	.p2align 3
.L3982:
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3987
	jmp	.L3967
	.p2align 4,,10
	.p2align 3
.L3963:
	movq	41088(%r15), %r13
	cmpq	%r13, 41096(%r15)
	je	.L3988
.L3965:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L3964
	.p2align 4,,10
	.p2align 3
.L3983:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3967
	.p2align 4,,10
	.p2align 3
.L3985:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3966
	.p2align 4,,10
	.p2align 3
.L3981:
	call	_ZN2v88internal7Genesis26GetThrowTypeErrorIntrinsicEv.part.0
	movq	%rax, %r13
	jmp	.L3956
	.p2align 4,,10
	.p2align 3
.L3988:
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3965
	.cfi_endproc
.LFE21281:
	.size	_ZN2v88internal7Genesis31AddRestrictedFunctionPropertiesENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal7Genesis31AddRestrictedFunctionPropertiesENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal7Genesis28CreateStrictModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis28CreateStrictModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal7Genesis28CreateStrictModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal7Genesis28CreateStrictModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE:
.LFB21273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	16(%r12), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rbx
	movq	%r15, 1303(%rbx)
	testb	$1, %r15b
	je	.L4021
	movq	%r15, %rcx
	leaq	1303(%rbx), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L4071
	testb	$24, %al
	je	.L4021
.L4088:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4072
	.p2align 4,,10
	.p2align 3
.L4021:
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	16(%r12), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rbx
	movq	%r15, 1311(%rbx)
	leaq	1311(%rbx), %rsi
	testb	$1, %r15b
	je	.L4020
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L4073
	testb	$24, %al
	je	.L4020
.L4087:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4074
	.p2align 4,,10
	.p2align 3
.L4020:
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	16(%r12), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rbx
	movq	%r15, 1319(%rbx)
	leaq	1319(%rbx), %rsi
	testb	$1, %r15b
	je	.L4019
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L4075
	testb	$24, %al
	je	.L4019
.L4090:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4076
	.p2align 4,,10
	.p2align 3
.L4019:
	movq	%r13, %rdx
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	16(%r12), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rbx
	movq	%r15, 1327(%rbx)
	leaq	1327(%rbx), %rsi
	testb	$1, %r15b
	je	.L4018
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L4077
	testb	$24, %al
	je	.L4018
.L4089:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4078
	.p2align 4,,10
	.p2align 3
.L4018:
	movq	%r13, %rdx
	movl	$4, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	16(%r12), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rbx
	movq	%r15, 1279(%rbx)
	leaq	1279(%rbx), %rsi
	testb	$1, %r15b
	je	.L4017
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L4079
	testb	$24, %al
	je	.L4017
.L4092:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4080
	.p2align 4,,10
	.p2align 3
.L4017:
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	16(%r12), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rbx
	movq	%r15, 1287(%rbx)
	leaq	1287(%rbx), %rsi
	testb	$1, %r15b
	je	.L4016
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L4081
	testb	$24, %al
	je	.L4016
.L4091:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4082
	.p2align 4,,10
	.p2align 3
.L4016:
	movq	%r13, %rdx
	movl	$6, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	%r13, %rdx
	movl	$7, %esi
	movq	%r14, %rdi
	movq	%rax, 32(%r12)
	call	_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	%r13, %rdx
	movl	$8, %esi
	movq	%r14, %rdi
	movq	%rax, 40(%r12)
	call	_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	16(%r12), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %rbx
	movq	%r15, 1295(%rbx)
	leaq	1295(%rbx), %rsi
	testb	$1, %r15b
	je	.L4015
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L4083
	testb	$24, %al
	je	.L4015
.L4094:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4084
	.p2align 4,,10
	.p2align 3
.L4015:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory22CreateClassFunctionMapENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	16(%r12), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r15
	movq	%r14, 1431(%r15)
	leaq	1431(%r15), %rsi
	testb	$1, %r14b
	je	.L4014
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L4085
	testb	$24, %al
	je	.L4014
.L4093:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4086
.L4014:
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Genesis31AddRestrictedFunctionPropertiesENS0_6HandleINS0_10JSFunctionEEE
	.p2align 4,,10
	.p2align 3
.L4073:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L4087
	jmp	.L4020
	.p2align 4,,10
	.p2align 3
.L4071:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L4088
	jmp	.L4021
	.p2align 4,,10
	.p2align 3
.L4077:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L4089
	jmp	.L4018
	.p2align 4,,10
	.p2align 3
.L4075:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L4090
	jmp	.L4019
	.p2align 4,,10
	.p2align 3
.L4081:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L4091
	jmp	.L4016
	.p2align 4,,10
	.p2align 3
.L4079:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L4092
	jmp	.L4017
	.p2align 4,,10
	.p2align 3
.L4085:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L4093
	jmp	.L4014
	.p2align 4,,10
	.p2align 3
.L4083:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L4094
	jmp	.L4015
	.p2align 4,,10
	.p2align 3
.L4072:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4021
	.p2align 4,,10
	.p2align 3
.L4074:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4020
	.p2align 4,,10
	.p2align 3
.L4076:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4019
	.p2align 4,,10
	.p2align 3
.L4078:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4018
	.p2align 4,,10
	.p2align 3
.L4080:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4017
	.p2align 4,,10
	.p2align 3
.L4082:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4016
	.p2align 4,,10
	.p2align 3
.L4084:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4015
	.p2align 4,,10
	.p2align 3
.L4086:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4014
	.cfi_endproc
.LFE21273:
	.size	_ZN2v88internal7Genesis28CreateStrictModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal7Genesis28CreateStrictModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE
	.section	.rodata._ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE.str1.8,"aMS",@progbits,1
	.align 8
.LC523:
	.string	"[Initializing context from scratch took %0.3f ms]\n"
	.section	.text._ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE
	.type	_ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE, @function
_ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE:
.LFB21348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%rax, -168(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	40936(%rsi), %rax
	movq	%rsi, (%rdi)
	movq	%rax, 56(%rdi)
	addl	$1, 8(%rax)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4193
.L4096:
	movq	$0, 8(%r13)
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	$0, 24(%r13)
	movq	%r15, %rdi
	call	_ZN2v88internal11SaveContextC1EPNS0_7IsolateE@PLT
	testq	%rbx, %rbx
	je	.L4194
.L4100:
	cmpb	$0, 41458(%r14)
	jne	.L4103
.L4190:
	movq	16(%r13), %rax
	testq	%rax, %rax
	jne	.L4106
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	movq	$0, -168(%rbp)
	jne	.L4195
.L4117:
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis11CreateRootsEv
	movq	16(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10MathRandom17InitializeContextEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis19CreateEmptyFunctionEv
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	%rax, %rsi
	call	_ZN2v88internal7Genesis28CreateSloppyModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis28CreateStrictModeFunctionMapsENS0_6HandleINS0_10JSFunctionEEE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis20CreateObjectFunctionENS0_6HandleINS0_10JSFunctionEEE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis18CreateIteratorMapsENS0_6HandleINS0_10JSFunctionEEE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis23CreateAsyncIteratorMapsENS0_6HandleINS0_10JSFunctionEEE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis23CreateAsyncFunctionMapsENS0_6HandleINS0_10JSFunctionEEE
	movq	%rbx, %rdx
	movq	-160(%rbp), %rbx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal7Genesis16CreateNewGlobalsENS_5LocalINS_14ObjectTemplateEEENS0_6HandleINS0_13JSGlobalProxyEEE
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Genesis16InitializeGlobalENS0_6HandleINS0_14JSGlobalObjectEEENS2_INS0_10JSFunctionEEE
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis29InitializeNormalizedMapCachesEv
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis27InitializeIteratorFunctionsEv
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis26InitializeCallSiteBuiltinsEv
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis14InstallNativesEv
	testb	%al, %al
	je	.L4115
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis19InstallExtraNativesEv
	testb	%al, %al
	je	.L4115
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis22ConfigureGlobalObjectsENS_5LocalINS_14ObjectTemplateEEE
	testb	%al, %al
	je	.L4115
	movq	40960(%r14), %rbx
	cmpb	$0, 6560(%rbx)
	je	.L4119
	movq	6552(%rbx), %rax
.L4120:
	testq	%rax, %rax
	je	.L4121
	addl	$1, (%rax)
.L4121:
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	leaq	-136(%rbp), %r12
	je	.L4116
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r12, %rdi
	subq	-168(%rbp), %rax
	movq	%rax, -136(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	leaq	.LC523(%rip), %rdi
	movl	$1, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4116:
	movq	16(%r13), %rax
	cmpq	$0, -152(%rbp)
	movq	(%rax), %rax
	je	.L4196
.L4123:
	movq	-152(%rbp), %rcx
	movq	%rcx, 1967(%rax)
	cmpb	$0, 41456(%r14)
	je	.L4197
.L4124:
	cmpb	$0, _ZN2v88internal42FLAG_disallow_code_generation_from_stringsE(%rip)
	jne	.L4198
.L4136:
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis20ConfigureUtilsObjectEv
	movq	41472(%r14), %rdi
	cmpb	$0, 8(%rdi)
	jne	.L4199
.L4140:
	movq	16(%r13), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal13NativeContext17ResetErrorsThrownEv@PLT
	movq	16(%r13), %rax
	movq	%rax, 8(%r13)
.L4115:
	movq	%r15, %rdi
	call	_ZN2v88internal11SaveContextD1Ev@PLT
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4200
.L4095:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4201
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4103:
	.cfi_restore_state
	movq	-176(%rbp), %r8
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-168(%rbp), %rcx
	call	_ZN2v88internal8Snapshot22NewContextFromSnapshotEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEEmNS_33DeserializeInternalFieldsCallbackE@PLT
	testq	%rax, %rax
	je	.L4190
	movq	%rax, 16(%r13)
.L4106:
	movq	(%rax), %rax
	movq	39120(%r14), %rdx
	movq	%rdx, 1959(%rax)
	leaq	1959(%rax), %rsi
	testb	$1, %dl
	jne	.L4202
.L4108:
	movq	%rax, 39120(%r14)
	movq	16(%r13), %rax
	movq	40960(%r14), %rdx
	movq	(%rax), %rax
	movq	%rax, 12464(%r14)
	cmpb	$0, 6592(%rdx)
	je	.L4110
	movq	6584(%rdx), %rax
.L4111:
	testq	%rax, %rax
	je	.L4112
	addl	$1, (%rax)
.L4112:
	testq	%r12, %r12
	jne	.L4113
	movq	%rbx, %rdx
	movq	-160(%rbp), %rbx
	movq	%r13, %rdi
	leaq	-136(%rbp), %r12
	movq	%rbx, %rsi
	call	_ZN2v88internal7Genesis16CreateNewGlobalsENS_5LocalINS_14ObjectTemplateEEENS0_6HandleINS0_13JSGlobalProxyEEE
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Genesis18HookUpGlobalObjectENS0_6HandleINS0_14JSGlobalObjectEEE
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis22ConfigureGlobalObjectsENS_5LocalINS_14ObjectTemplateEEE
	testb	%al, %al
	jne	.L4116
	jmp	.L4115
	.p2align 4,,10
	.p2align 3
.L4110:
	movb	$1, 6592(%rdx)
	leaq	6568(%rdx), %rdi
	movq	%rdx, -168(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	-168(%rbp), %rdx
	movq	%rax, 6584(%rdx)
	jmp	.L4111
	.p2align 4,,10
	.p2align 3
.L4202:
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	je	.L4108
	movq	%rax, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	jne	.L4108
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %rax
	jmp	.L4108
	.p2align 4,,10
	.p2align 3
.L4113:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	-136(%rbp), %r12
	call	_ZN2v88internal7Genesis17HookUpGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEE
	jmp	.L4116
	.p2align 4,,10
	.p2align 3
.L4194:
	testq	%r12, %r12
	jne	.L4203
	movq	-160(%rbp), %rax
	movl	$32, %esi
	testq	%rax, %rax
	je	.L4101
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate18InternalFieldCountEv@PLT
	leal	32(,%rax,8), %esi
.L4101:
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory29NewUninitializedJSGlobalProxyEi@PLT
	movq	%rax, %rbx
	jmp	.L4100
	.p2align 4,,10
	.p2align 3
.L4199:
	call	_ZN2v88internal5Debug27InstallDebugBreakTrampolineEv@PLT
	jmp	.L4140
	.p2align 4,,10
	.p2align 3
.L4198:
	movq	16(%r13), %rax
	movq	120(%r14), %rdx
	movq	(%rax), %rdi
	movq	%rdx, 71(%rdi)
	leaq	71(%rdi), %rsi
	testb	$1, %dl
	je	.L4136
	movq	%rdx, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L4138
	movq	%rdx, -168(%rbp)
	movq	%rsi, -160(%rbp)
	movq	%rdi, -152(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-168(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	-152(%rbp), %rdi
.L4138:
	testb	$24, %al
	je	.L4136
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4136
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4136
	.p2align 4,,10
	.p2align 3
.L4197:
	cmpb	$0, _ZN2v88internal22FLAG_harmony_weak_refsE(%rip)
	je	.L4125
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis34InitializeGlobal_harmony_weak_refsEv.part.0
.L4125:
	cmpb	$0, _ZN2v88internal27FLAG_harmony_intl_segmenterE(%rip)
	je	.L4126
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis39InitializeGlobal_harmony_intl_segmenterEv.part.0
.L4126:
	cmpb	$0, _ZN2v88internal30FLAG_harmony_sharedarraybufferE(%rip)
	je	.L4127
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis42InitializeGlobal_harmony_sharedarraybufferEv.part.0
.L4127:
	cmpb	$0, _ZN2v88internal32FLAG_harmony_promise_all_settledE(%rip)
	je	.L4128
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis44InitializeGlobal_harmony_promise_all_settledEv.part.0
.L4128:
	cmpb	$0, _ZN2v88internal35FLAG_harmony_intl_date_format_rangeE(%rip)
	je	.L4129
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis47InitializeGlobal_harmony_intl_date_format_rangeEv.part.0
.L4129:
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	1439(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4130
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L4131:
	movq	16(%r13), %rax
	movq	(%rax), %rdi
	movq	55(%rsi), %rax
	movq	23(%rax), %rax
	leaq	1447(%rdi), %rsi
	movq	-1(%rax), %rdx
	movq	%rdx, 1447(%rdi)
	testb	$1, %dl
	je	.L4124
	movq	%rdx, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L4134
	movq	%rdx, -168(%rbp)
	movq	%rsi, -160(%rbp)
	movq	%rdi, -152(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-168(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	-152(%rbp), %rdi
.L4134:
	testb	$24, %al
	je	.L4124
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4124
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4124
	.p2align 4,,10
	.p2align 3
.L4195:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, -168(%rbp)
	jmp	.L4117
	.p2align 4,,10
	.p2align 3
.L4196:
	movq	41752(%r14), %rcx
	movq	%rcx, -152(%rbp)
	jmp	.L4123
	.p2align 4,,10
	.p2align 3
.L4193:
	movq	40960(%rsi), %rax
	movl	$152, %edx
	leaq	-88(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4096
	.p2align 4,,10
	.p2align 3
.L4200:
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4095
	.p2align 4,,10
	.p2align 3
.L4130:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L4204
.L4132:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L4131
	.p2align 4,,10
	.p2align 3
.L4203:
	movq	4736(%r14), %rdx
	leal	8(,%r12,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	shrq	$32, %rsi
	jmp	.L4101
	.p2align 4,,10
	.p2align 3
.L4119:
	movb	$1, 6560(%rbx)
	leaq	6536(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6552(%rbx)
	jmp	.L4120
	.p2align 4,,10
	.p2align 3
.L4204:
	movq	%r14, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L4132
.L4201:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21348:
	.size	_ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE, .-_ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE
	.globl	_ZN2v88internal7GenesisC1EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE
	.set	_ZN2v88internal7GenesisC1EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE,_ZN2v88internal7GenesisC2EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE
	.section	.text._ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	.type	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_, @function
_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_:
.LFB27188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	(%rdi), %r12
	movl	12(%rdi), %r14d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, %rbx
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L4245
	movl	%ebx, 8(%r13)
	testl	%ebx, %ebx
	je	.L4207
	movq	$0, (%rax)
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L4208:
	movq	0(%r13), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	8(%r13), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L4208
.L4207:
	movl	$0, 12(%r13)
	movq	%r12, %r15
	testl	%r14d, %r14d
	je	.L4215
.L4209:
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	jne	.L4246
.L4210:
	movq	24(%r15), %rsi
	addq	$24, %r15
	testq	%rsi, %rsi
	je	.L4210
.L4246:
	movl	8(%r13), %eax
	movl	16(%r15), %ebx
	movq	0(%r13), %r8
	leal	-1(%rax), %edi
	movl	%ebx, %eax
	andl	%edi, %eax
	jmp	.L4244
	.p2align 4,,10
	.p2align 3
.L4247:
	testq	%rdx, %rdx
	je	.L4211
	addq	$1, %rax
	andq	%rdi, %rax
.L4244:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r8,%rdx,8), %rcx
	movq	(%rcx), %rdx
	cmpq	%rdx, %rsi
	jne	.L4247
.L4211:
	movq	8(%r15), %rax
	movq	%rsi, (%rcx)
	movl	%ebx, 16(%rcx)
	movq	%rax, 8(%rcx)
	movl	12(%r13), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r13)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r13), %eax
	jnb	.L4214
.L4217:
	addq	$24, %r15
	subl	$1, %r14d
	jne	.L4209
.L4215:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L4214:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	movl	8(%r13), %eax
	movq	0(%r13), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L4217
	movq	(%r15), %rdi
	jmp	.L4218
	.p2align 4,,10
	.p2align 3
.L4248:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	movq	(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L4217
.L4218:
	cmpq	%rdx, %rdi
	jne	.L4248
	jmp	.L4217
.L4245:
	leaq	.LC499(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27188:
	.size	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_, .-_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	.section	.text._ZN2v88internal7Genesis15ExtensionStates9set_stateEPNS_19RegisteredExtensionENS1_23ExtensionTraversalStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis15ExtensionStates9set_stateEPNS_19RegisteredExtensionENS1_23ExtensionTraversalStateE
	.type	_ZN2v88internal7Genesis15ExtensionStates9set_stateEPNS_19RegisteredExtensionENS1_23ExtensionTraversalStateE, @function
_ZN2v88internal7Genesis15ExtensionStates9set_stateEPNS_19RegisteredExtensionENS1_23ExtensionTraversalStateE:
.LFB21332:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$15, %eax
	subl	%esi, %eax
	subl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	movl	%eax, %ecx
	pushq	%r13
	shrl	$12, %ecx
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	xorl	%ecx, %eax
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	(%rdi), %r8
	leal	(%rax,%rax,4), %eax
	movl	%eax, %ecx
	shrl	$4, %ecx
	xorl	%ecx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %ecx
	shrl	$16, %ecx
	xorl	%ecx, %eax
	andl	$1073741823, %eax
	movl	%eax, %r13d
	movl	8(%rdi), %eax
	leal	-1(%rax), %esi
	movl	%r13d, %eax
	andl	%esi, %eax
	jmp	.L4272
	.p2align 4,,10
	.p2align 3
.L4275:
	cmpq	%rdi, %rbx
	je	.L4251
	addq	$1, %rax
	andq	%rsi, %rax
.L4272:
	leaq	(%rax,%rax,2), %rcx
	leaq	(%r8,%rcx,8), %rcx
	movq	(%rcx), %rdi
	testq	%rdi, %rdi
	jne	.L4275
	movq	%rbx, (%rcx)
	movq	$0, 8(%rcx)
	movl	%r13d, 16(%rcx)
	movl	12(%r12), %eax
	addl	$1, %eax
	movl	%eax, %esi
	movl	%eax, 12(%r12)
	shrl	$2, %esi
	addl	%esi, %eax
	cmpl	8(%r12), %eax
	jnb	.L4276
.L4251:
	movq	%r14, 8(%rcx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4276:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	movl	8(%r12), %eax
	movq	(%r12), %r8
	leal	-1(%rax), %edi
	movl	%r13d, %eax
	andl	%edi, %eax
	jmp	.L4274
	.p2align 4,,10
	.p2align 3
.L4277:
	cmpq	%rsi, %rbx
	je	.L4251
	addq	$1, %rax
	andq	%rdi, %rax
.L4274:
	leaq	(%rax,%rax,2), %rcx
	leaq	(%r8,%rcx,8), %rcx
	movq	(%rcx), %rsi
	testq	%rsi, %rsi
	jne	.L4277
	jmp	.L4251
	.cfi_endproc
.LFE21332:
	.size	_ZN2v88internal7Genesis15ExtensionStates9set_stateEPNS_19RegisteredExtensionENS1_23ExtensionTraversalStateE, .-_ZN2v88internal7Genesis15ExtensionStates9set_stateEPNS_19RegisteredExtensionENS1_23ExtensionTraversalStateE
	.section	.rodata._ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE.str1.1,"aMS",@progbits,1
.LC524:
	.string	"Circular extension dependency"
.LC525:
	.string	"v8::Context::New()"
	.section	.rodata._ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE.str1.8,"aMS",@progbits,1
	.align 8
.LC526:
	.string	"Cannot find required extension"
	.align 8
.LC527:
	.string	"Error installing extension '%s'.\n"
	.section	.text._ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE
	.type	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE, @function
_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE:
.LFB21340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	41088(%rdi), %rax
	movl	41104(%rdi), %ecx
	movq	%rsi, -64(%rbp)
	movq	%rax, -80(%rbp)
	movq	41096(%rdi), %rax
	movq	%rax, -72(%rbp)
	leal	1(%rcx), %eax
	movl	%eax, 41104(%rdi)
	movl	%esi, %eax
	movl	8(%r13), %ebx
	sall	$15, %eax
	movq	0(%r13), %rdi
	subl	%esi, %eax
	leal	-1(%rbx), %r8d
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	%r8d, %eax
	andl	$1073741823, %eax
	jmp	.L4307
	.p2align 4,,10
	.p2align 3
.L4309:
	cmpq	%rdx, -64(%rbp)
	je	.L4280
	addq	$1, %rax
	andq	%r8, %rax
.L4307:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rsi
	movq	(%rsi), %rdx
	testq	%rdx, %rdx
	jne	.L4309
.L4279:
	movq	-64(%rbp), %rbx
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal7Genesis15ExtensionStates9set_stateEPNS_19RegisteredExtensionENS1_23ExtensionTraversalStateE
	movq	(%rbx), %r14
	xorl	%ebx, %ebx
	movl	32(%r14), %eax
	testl	%eax, %eax
	jle	.L4292
	.p2align 4,,10
	.p2align 3
.L4288:
	movq	40(%r14), %rax
	movq	_ZN2v819RegisteredExtension16first_extension_E(%rip), %r12
	movq	(%rax,%rbx,8), %rdx
	testq	%r12, %r12
	jne	.L4287
	jmp	.L4283
	.p2align 4,,10
	.p2align 3
.L4284:
	movq	8(%r12), %r12
	testq	%r12, %r12
	je	.L4283
.L4287:
	movq	(%r12), %rax
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	movq	8(%rax), %rsi
	call	strcmp@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	jne	.L4284
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE
	movl	%eax, %r8d
	testb	%al, %al
	je	.L4310
	addq	$1, %rbx
	cmpl	%ebx, 32(%r14)
	jg	.L4288
.L4292:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Genesis16CompileExtensionEPNS0_7IsolateEPNS_9ExtensionE
	movl	%eax, %r8d
	testb	%al, %al
	je	.L4311
.L4289:
	movq	-64(%rbp), %rsi
	movl	$2, %edx
	movq	%r13, %rdi
	movb	%r8b, -56(%rbp)
	call	_ZN2v88internal7Genesis15ExtensionStates9set_stateEPNS_19RegisteredExtensionENS1_23ExtensionTraversalStateE
	movl	41104(%r15), %eax
	movzbl	-56(%rbp), %r8d
	movq	41096(%r15), %rdx
	subl	$1, %eax
	jmp	.L4282
	.p2align 4,,10
	.p2align 3
.L4283:
	leaq	.LC526(%rip), %rsi
.L4308:
	leaq	.LC525(%rip), %rdi
	call	_ZN2v85Utils16ReportApiFailureEPKcS2_@PLT
	movl	41104(%r15), %eax
	movq	41096(%r15), %rdx
	xorl	%r8d, %r8d
	subl	$1, %eax
.L4282:
	movq	-80(%rbp), %rcx
	movl	%eax, 41104(%r15)
	movq	%rcx, 41088(%r15)
	cmpq	%rdx, -72(%rbp)
	je	.L4278
	movq	-72(%rbp), %rax
	movq	%r15, %rdi
	movb	%r8b, -56(%rbp)
	movq	%rax, 41096(%r15)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movzbl	-56(%rbp), %r8d
.L4278:
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4280:
	.cfi_restore_state
	movq	8(%rsi), %rax
	cmpl	$2, %eax
	je	.L4312
	leaq	.LC524(%rip), %rsi
	cmpl	$1, %eax
	jne	.L4279
	jmp	.L4308
	.p2align 4,,10
	.p2align 3
.L4312:
	movq	-80(%rbp), %rax
	movl	%ecx, 41104(%r15)
	movl	$1, %r8d
	movq	%rax, 41088(%r15)
	jmp	.L4278
	.p2align 4,,10
	.p2align 3
.L4311:
	movb	%al, -56(%rbp)
	movq	-64(%rbp), %rax
	leaq	.LC527(%rip), %rdi
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	96(%r15), %rax
	movzbl	-56(%rbp), %r8d
	movq	%rax, 12480(%r15)
	jmp	.L4289
.L4310:
	movl	41104(%r15), %eax
	movq	41096(%r15), %rdx
	subl	$1, %eax
	jmp	.L4282
	.cfi_endproc
.LFE21340:
	.size	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE, .-_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE
	.section	.text._ZN2v88internal7Genesis21InstallAutoExtensionsEPNS0_7IsolateEPNS1_15ExtensionStatesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis21InstallAutoExtensionsEPNS0_7IsolateEPNS1_15ExtensionStatesE
	.type	_ZN2v88internal7Genesis21InstallAutoExtensionsEPNS0_7IsolateEPNS1_15ExtensionStatesE, @function
_ZN2v88internal7Genesis21InstallAutoExtensionsEPNS0_7IsolateEPNS1_15ExtensionStatesE:
.LFB21337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	_ZN2v819RegisteredExtension16first_extension_E(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4314
	movq	%rdi, %r13
	movq	%rsi, %r12
	jmp	.L4316
	.p2align 4,,10
	.p2align 3
.L4317:
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4314
.L4316:
	movq	(%rbx), %rax
	cmpb	$0, 48(%rax)
	je	.L4317
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE
	testb	%al, %al
	jne	.L4317
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4314:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21337:
	.size	_ZN2v88internal7Genesis21InstallAutoExtensionsEPNS0_7IsolateEPNS1_15ExtensionStatesE, .-_ZN2v88internal7Genesis21InstallAutoExtensionsEPNS0_7IsolateEPNS1_15ExtensionStatesE
	.section	.text._ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPKcPNS1_15ExtensionStatesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPKcPNS1_15ExtensionStatesE
	.type	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPKcPNS1_15ExtensionStatesE, @function
_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPKcPNS1_15ExtensionStatesE:
.LFB21339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	_ZN2v819RegisteredExtension16first_extension_E(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	testq	%r12, %r12
	je	.L4327
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	%rdx, %r14
	jmp	.L4329
	.p2align 4,,10
	.p2align 3
.L4328:
	movq	8(%r12), %r12
	testq	%r12, %r12
	je	.L4327
.L4329:
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L4328
	popq	%rbx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE
	.p2align 4,,10
	.p2align 3
.L4327:
	.cfi_restore_state
	leaq	.LC526(%rip), %rsi
	leaq	.LC525(%rip), %rdi
	call	_ZN2v85Utils16ReportApiFailureEPKcS2_@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21339:
	.size	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPKcPNS1_15ExtensionStatesE, .-_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPKcPNS1_15ExtensionStatesE
	.section	.text._ZN2v88internal7Genesis26InstallRequestedExtensionsEPNS0_7IsolateEPNS_22ExtensionConfigurationEPNS1_15ExtensionStatesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis26InstallRequestedExtensionsEPNS0_7IsolateEPNS_22ExtensionConfigurationEPNS1_15ExtensionStatesE
	.type	_ZN2v88internal7Genesis26InstallRequestedExtensionsEPNS0_7IsolateEPNS_22ExtensionConfigurationEPNS1_15ExtensionStatesE, @function
_ZN2v88internal7Genesis26InstallRequestedExtensionsEPNS0_7IsolateEPNS_22ExtensionConfigurationEPNS1_15ExtensionStatesE:
.LFB21338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	(%rsi), %rax
	movq	8(%rsi), %rbx
	movq	%rdx, -56(%rbp)
	salq	$3, %rax
	je	.L4336
	movq	%rdi, %r14
	movq	%rsi, %r15
	.p2align 4,,10
	.p2align 3
.L4342:
	movq	_ZN2v819RegisteredExtension16first_extension_E(%rip), %r12
	movq	(%rbx), %r13
	testq	%r12, %r12
	jne	.L4341
	jmp	.L4337
	.p2align 4,,10
	.p2align 3
.L4338:
	movq	8(%r12), %r12
	testq	%r12, %r12
	je	.L4337
.L4341:
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L4338
	movq	-56(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE
	testb	%al, %al
	je	.L4335
	movslq	(%r15), %rdx
	movq	8(%r15), %rax
	addq	$8, %rbx
	leaq	(%rax,%rdx,8), %rax
	cmpq	%rax, %rbx
	jne	.L4342
.L4336:
	movl	$1, %eax
.L4335:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4337:
	.cfi_restore_state
	leaq	.LC526(%rip), %rsi
	leaq	.LC525(%rip), %rdi
	call	_ZN2v85Utils16ReportApiFailureEPKcS2_@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21338:
	.size	_ZN2v88internal7Genesis26InstallRequestedExtensionsEPNS0_7IsolateEPNS_22ExtensionConfigurationEPNS1_15ExtensionStatesE, .-_ZN2v88internal7Genesis26InstallRequestedExtensionsEPNS0_7IsolateEPNS_22ExtensionConfigurationEPNS1_15ExtensionStatesE
	.section	.text._ZN2v88internal7Genesis17InstallExtensionsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Genesis17InstallExtensionsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE
	.type	_ZN2v88internal7Genesis17InstallExtensionsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE, @function
_ZN2v88internal7Genesis17InstallExtensionsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE:
.LFB21333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$192, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	malloc@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L4415
	movl	$8, -72(%rbp)
	movl	$24, %edx
	movq	$0, (%rax)
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L4354:
	movq	-80(%rbp), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	-72(%rbp), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L4354
	movq	_ZN2v819RegisteredExtension16first_extension_E(%rip), %rbx
	movl	$0, -68(%rbp)
	testq	%rbx, %rbx
	je	.L4355
	leaq	-80(%rbp), %r12
	jmp	.L4358
	.p2align 4,,10
	.p2align 3
.L4359:
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4416
.L4358:
	movq	(%rbx), %rax
	cmpb	$0, 48(%rax)
	je	.L4359
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE
	testb	%al, %al
	jne	.L4359
	.p2align 4,,10
	.p2align 3
.L4414:
	xorl	%r12d, %r12d
.L4360:
	movq	-80(%rbp), %rdi
	call	free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4417
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4416:
	.cfi_restore_state
	cmpb	$0, _ZN2v88internal23FLAG_expose_free_bufferE(%rip)
	jne	.L4418
.L4363:
	cmpb	$0, _ZN2v88internal14FLAG_expose_gcE(%rip)
	jne	.L4419
.L4366:
	cmpb	$0, _ZN2v88internal30FLAG_expose_externalize_stringE(%rip)
	je	.L4369
	movq	_ZN2v819RegisteredExtension16first_extension_E(%rip), %r8
	testq	%r8, %r8
	je	.L4365
	leaq	.LC61(%rip), %rdx
	jmp	.L4371
	.p2align 4,,10
	.p2align 3
.L4370:
	movq	8(%r8), %r8
	testq	%r8, %r8
	je	.L4365
.L4371:
	movq	(%r8), %rax
	movl	$15, %ecx
	movq	%rdx, %rsi
	movq	8(%rax), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L4370
	leaq	-80(%rbp), %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE
	testb	%al, %al
	je	.L4414
.L4369:
	movl	_ZN2v88internal12TracingFlags8gc_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4372
.L4375:
	cmpb	$0, _ZN2v88internal27FLAG_expose_trigger_failureE(%rip)
	jne	.L4420
.L4374:
	cmpb	$0, _ZN2v88internal30FLAG_trace_ignition_dispatchesE(%rip)
	jne	.L4421
.L4377:
	movq	_ZN2v88internal27FLAG_expose_cputracemark_asE(%rip), %rax
	testq	%rax, %rax
	je	.L4379
	cmpb	$0, (%rax)
	je	.L4379
	leaq	-80(%rbp), %rdx
	leaq	.LC65(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPKcPNS1_15ExtensionStatesE
	testb	%al, %al
	je	.L4414
.L4379:
	movslq	0(%r13), %rax
	leaq	-80(%rbp), %rcx
	movq	8(%r13), %rbx
	movq	%rcx, -88(%rbp)
	salq	$3, %rax
	je	.L4381
	.p2align 4,,10
	.p2align 3
.L4380:
	movq	_ZN2v819RegisteredExtension16first_extension_E(%rip), %r12
	movq	(%rbx), %r15
	testq	%r12, %r12
	jne	.L4384
	jmp	.L4365
	.p2align 4,,10
	.p2align 3
.L4382:
	movq	8(%r12), %r12
	testq	%r12, %r12
	je	.L4365
.L4384:
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L4382
	movq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE
	testb	%al, %al
	je	.L4414
	movslq	0(%r13), %rdx
	movq	8(%r13), %rax
	addq	$8, %rbx
	leaq	(%rax,%rdx,8), %rax
	cmpq	%rax, %rbx
	jne	.L4380
.L4381:
	movl	$1, %r12d
	jmp	.L4360
	.p2align 4,,10
	.p2align 3
.L4372:
	leaq	-80(%rbp), %rdx
	leaq	.LC62(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPKcPNS1_15ExtensionStatesE
	testb	%al, %al
	jne	.L4375
	jmp	.L4414
	.p2align 4,,10
	.p2align 3
.L4420:
	leaq	-80(%rbp), %rdx
	leaq	.LC63(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPKcPNS1_15ExtensionStatesE
	testb	%al, %al
	jne	.L4374
	jmp	.L4414
	.p2align 4,,10
	.p2align 3
.L4355:
	cmpb	$0, _ZN2v88internal23FLAG_expose_free_bufferE(%rip)
	je	.L4422
	.p2align 4,,10
	.p2align 3
.L4365:
	leaq	.LC526(%rip), %rsi
	leaq	.LC525(%rip), %rdi
	call	_ZN2v85Utils16ReportApiFailureEPKcS2_@PLT
	jmp	.L4414
	.p2align 4,,10
	.p2align 3
.L4418:
	movq	_ZN2v819RegisteredExtension16first_extension_E(%rip), %r8
	testq	%r8, %r8
	je	.L4365
	leaq	.LC58(%rip), %rdx
	jmp	.L4361
	.p2align 4,,10
	.p2align 3
.L4362:
	movq	8(%r8), %r8
	testq	%r8, %r8
	je	.L4365
.L4361:
	movq	(%r8), %rax
	movl	$15, %ecx
	movq	%rdx, %rsi
	movq	8(%rax), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L4362
	leaq	-80(%rbp), %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE
	testb	%al, %al
	jne	.L4363
	jmp	.L4414
	.p2align 4,,10
	.p2align 3
.L4419:
	movq	_ZN2v819RegisteredExtension16first_extension_E(%rip), %r8
	testq	%r8, %r8
	je	.L4365
	leaq	.LC60(%rip), %rdx
	jmp	.L4368
	.p2align 4,,10
	.p2align 3
.L4367:
	movq	8(%r8), %r8
	testq	%r8, %r8
	je	.L4365
.L4368:
	movq	(%r8), %rax
	movl	$6, %ecx
	movq	%rdx, %rsi
	movq	8(%rax), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L4367
	leaq	-80(%rbp), %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPNS_19RegisteredExtensionEPNS1_15ExtensionStatesE
	testb	%al, %al
	jne	.L4366
	jmp	.L4414
	.p2align 4,,10
	.p2align 3
.L4421:
	leaq	-80(%rbp), %rdx
	leaq	.LC64(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Genesis16InstallExtensionEPNS0_7IsolateEPKcPNS1_15ExtensionStatesE
	testb	%al, %al
	jne	.L4377
	jmp	.L4414
	.p2align 4,,10
	.p2align 3
.L4422:
	cmpb	$0, _ZN2v88internal14FLAG_expose_gcE(%rip)
	je	.L4366
	jmp	.L4365
.L4415:
	leaq	.LC499(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L4417:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21333:
	.size	_ZN2v88internal7Genesis17InstallExtensionsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE, .-_ZN2v88internal7Genesis17InstallExtensionsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE
	.section	.text._ZN2v88internal12Bootstrapper17InstallExtensionsENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper17InstallExtensionsENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE
	.type	_ZN2v88internal12Bootstrapper17InstallExtensionsENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE, @function
_ZN2v88internal12Bootstrapper17InstallExtensionsENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE:
.LFB21325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	41456(%rsi), %r12d
	testb	%r12b, %r12b
	je	.L4431
.L4423:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4432
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4431:
	.cfi_restore_state
	addl	$1, 8(%rdi)
	movq	%rdx, %r14
	leaq	-80(%rbp), %r15
	movq	0(%r13), %rdx
	movq	%rdi, %rbx
	movq	%r15, %rdi
	call	_ZN2v88internal20SaveAndSwitchContextC1EPNS0_7IsolateENS0_7ContextE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal7Genesis17InstallExtensionsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L4433
.L4425:
	movq	%r15, %rdi
	call	_ZN2v88internal11SaveContextD2Ev@PLT
	subl	$1, 8(%rbx)
	jmp	.L4423
	.p2align 4,,10
	.p2align 3
.L4433:
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal7Genesis21InstallSpecialObjectsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE
	movl	%eax, %r12d
	jmp	.L4425
.L4432:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21325:
	.size	_ZN2v88internal12Bootstrapper17InstallExtensionsENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE, .-_ZN2v88internal12Bootstrapper17InstallExtensionsENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE
	.section	.text._ZN2v88internal12Bootstrapper17CreateEnvironmentENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEPNS_22ExtensionConfigurationEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Bootstrapper17CreateEnvironmentENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEPNS_22ExtensionConfigurationEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE
	.type	_ZN2v88internal12Bootstrapper17CreateEnvironmentENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEPNS_22ExtensionConfigurationEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE, @function
_ZN2v88internal12Bootstrapper17CreateEnvironmentENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEPNS_22ExtensionConfigurationEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE:
.LFB21241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rcx, -168(%rbp)
	movq	(%rdi), %r12
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	leaq	-128(%rbp), %rdi
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%r12)
	pushq	24(%rbp)
	pushq	16(%rbp)
	movq	(%rbx), %rsi
	call	_ZN2v88internal7GenesisC1EPNS0_7IsolateENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE
	movq	-120(%rbp), %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L4435
	movq	(%rbx), %rsi
	cmpb	$0, 41456(%rsi)
	je	.L4456
.L4437:
	movq	-72(%rbp), %rax
	subl	$1, 8(%rax)
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	movq	(%rbx), %rdi
	je	.L4443
	cmpb	$0, 41458(%rdi)
	jne	.L4443
	movq	41016(%rdi), %rdi
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-168(%rbp), %rdi
	testb	%al, %al
	jne	.L4444
.L4455:
	movq	(%rbx), %rdi
.L4443:
	addq	$37592, %rdi
	call	_ZN2v88internal4Heap23NotifyBootstrapCompleteEv@PLT
	movq	0(%r13), %rbx
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L4445
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4445:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4446
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	jmp	.L4441
	.p2align 4,,10
	.p2align 3
.L4456:
	addl	$1, 8(%rbx)
	leaq	-160(%rbp), %r8
	movq	0(%r13), %rdx
	movq	%r8, %rdi
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal20SaveAndSwitchContextC1EPNS0_7IsolateENS0_7ContextE@PLT
	movq	-168(%rbp), %rdx
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal7Genesis17InstallExtensionsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS_22ExtensionConfigurationE
	movq	-176(%rbp), %r8
	testb	%al, %al
	jne	.L4457
	movq	%r8, %rdi
.L4454:
	call	_ZN2v88internal11SaveContextD2Ev@PLT
	subl	$1, 8(%rbx)
.L4435:
	movq	-72(%rbp), %rax
	xorl	%r13d, %r13d
	subl	$1, 8(%rax)
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L4441
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4441:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4458
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4457:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal7Genesis21InstallSpecialObjectsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE
	movq	-168(%rbp), %r8
	testb	%al, %al
	movq	%r8, %rdi
	je	.L4454
	call	_ZN2v88internal11SaveContextD2Ev@PLT
	subl	$1, 8(%rbx)
	jmp	.L4437
	.p2align 4,,10
	.p2align 3
.L4446:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L4459
.L4448:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rbx, 0(%r13)
	jmp	.L4441
	.p2align 4,,10
	.p2align 3
.L4444:
	call	_ZN2v88internal6Logger10LogAllMapsEv@PLT
	jmp	.L4455
	.p2align 4,,10
	.p2align 3
.L4459:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L4448
.L4458:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21241:
	.size	_ZN2v88internal12Bootstrapper17CreateEnvironmentENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEPNS_22ExtensionConfigurationEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE, .-_ZN2v88internal12Bootstrapper17CreateEnvironmentENS0_11MaybeHandleINS0_13JSGlobalProxyEEENS_5LocalINS_14ObjectTemplateEEEPNS_22ExtensionConfigurationEmNS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15SourceCodeCache10InitializeEPNS0_7IsolateEb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15SourceCodeCache10InitializeEPNS0_7IsolateEb, @function
_GLOBAL__sub_I__ZN2v88internal15SourceCodeCache10InitializeEPNS0_7IsolateEb:
.LFB28101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28101:
	.size	_GLOBAL__sub_I__ZN2v88internal15SourceCodeCache10InitializeEPNS0_7IsolateEb, .-_GLOBAL__sub_I__ZN2v88internal15SourceCodeCache10InitializeEPNS0_7IsolateEb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15SourceCodeCache10InitializeEPNS0_7IsolateEb
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC177:
	.long	4294967295
	.long	2146435071
	.align 8
.LC179:
	.long	1
	.long	0
	.align 8
.LC182:
	.long	0
	.long	-1048576
	.align 8
.LC185:
	.long	4294967295
	.long	1128267775
	.align 8
.LC187:
	.long	4294967295
	.long	-1019215873
	.align 8
.LC189:
	.long	0
	.long	1018167296
	.align 8
.LC374:
	.long	0
	.long	1072693248
	.align 8
.LC376:
	.long	0
	.long	1076101120
	.align 8
.LC378:
	.long	0
	.long	1073741824
	.align 8
.LC382:
	.long	1413754136
	.long	1074340347
	.align 8
.LC384:
	.long	1719614413
	.long	1072079006
	.align 8
.LC386:
	.long	1719614413
	.long	1073127582
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
