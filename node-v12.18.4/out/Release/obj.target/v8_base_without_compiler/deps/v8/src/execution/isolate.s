	.file	"isolate.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB4056:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE4056:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4860:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4860:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4861:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4861:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB7689:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7689:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci,"axG",@progbits,_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci,comdat
	.p2align 4
	.weak	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci
	.type	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci, @function
_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci:
.LFB21669:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21669:
	.size	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci, .-_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci
	.section	.text._ZN2v88internal11NoExtensionERKNS_20FunctionCallbackInfoINS_5ValueEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11NoExtensionERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal11NoExtensionERKNS_20FunctionCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal11NoExtensionERKNS_20FunctionCallbackInfoINS_5ValueEEE:
.LFB24999:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE24999:
	.size	_ZN2v88internal11NoExtensionERKNS_20FunctionCallbackInfoINS_5ValueEEE, .-_ZN2v88internal11NoExtensionERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB34342:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE34342:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB34343:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L10
	cmpl	$3, %edx
	je	.L11
	cmpl	$1, %edx
	je	.L15
.L11:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE34343:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB34982:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE34982:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv:
.LFB32475:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base5MutexC1Ev@PLT
	.cfi_endproc
.LFE32475:
	.size	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.section	.text._ZN2v88internal9DateCacheD0Ev,"axG",@progbits,_ZN2v88internal9DateCacheD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9DateCacheD0Ev
	.type	_ZN2v88internal9DateCacheD0Ev, @function
_ZN2v88internal9DateCacheD0Ev:
.LFB20899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9DateCacheE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	592(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L19
	movq	(%rdi), %rax
	call	*40(%rax)
.L19:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$600, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20899:
	.size	_ZN2v88internal9DateCacheD0Ev, .-_ZN2v88internal9DateCacheD0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB34984:
	.cfi_startproc
	endbr64
	movl	$50976, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE34984:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZN2v88internal20SetupIsolateDelegateD0Ev,"axG",@progbits,_ZN2v88internal20SetupIsolateDelegateD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20SetupIsolateDelegateD0Ev
	.type	_ZN2v88internal20SetupIsolateDelegateD0Ev, @function
_ZN2v88internal20SetupIsolateDelegateD0Ev:
.LFB34972:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE34972:
	.size	_ZN2v88internal20SetupIsolateDelegateD0Ev, .-_ZN2v88internal20SetupIsolateDelegateD0Ev
	.section	.text._ZN2v88internal11interpreter11InterpreterD2Ev,"axG",@progbits,_ZN2v88internal11interpreter11InterpreterD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter11InterpreterD2Ev
	.type	_ZN2v88internal11interpreter11InterpreterD2Ev, @function
_ZN2v88internal11interpreter11InterpreterD2Ev:
.LFB34966:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal11interpreter11InterpreterE(%rip), %rax
	movq	%rax, (%rdi)
	movq	6160(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L26
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	ret
	.cfi_endproc
.LFE34966:
	.size	_ZN2v88internal11interpreter11InterpreterD2Ev, .-_ZN2v88internal11interpreter11InterpreterD2Ev
	.weak	_ZN2v88internal11interpreter11InterpreterD1Ev
	.set	_ZN2v88internal11interpreter11InterpreterD1Ev,_ZN2v88internal11interpreter11InterpreterD2Ev
	.section	.text._ZN2v88internal11interpreter11InterpreterD0Ev,"axG",@progbits,_ZN2v88internal11interpreter11InterpreterD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter11InterpreterD0Ev
	.type	_ZN2v88internal11interpreter11InterpreterD0Ev, @function
_ZN2v88internal11interpreter11InterpreterD0Ev:
.LFB34968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter11InterpreterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	6160(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L29
	call	_ZdaPv@PLT
.L29:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$6176, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE34968:
	.size	_ZN2v88internal11interpreter11InterpreterD0Ev, .-_ZN2v88internal11interpreter11InterpreterD0Ev
	.section	.text._ZN2v88internalL11PrintFramesEPNS0_7IsolateEPNS0_12StringStreamENS0_10StackFrame9PrintModeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL11PrintFramesEPNS0_7IsolateEPNS0_12StringStreamENS0_10StackFrame9PrintModeE, @function
_ZN2v88internalL11PrintFramesEPNS0_7IsolateEPNS0_12StringStreamENS0_10StackFrame9PrintModeE:
.LFB25025:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-1488(%rbp), %r14
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%rdi, %rsi
	movq	%r14, %rdi
	pushq	%rbx
	subq	$1456, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L34
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L36:
	movq	(%rdi), %rax
	movl	%ebx, %ecx
	movl	%r13d, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	call	*32(%rax)
	movq	%r14, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L36
.L34:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	addq	$1456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L43:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25025:
	.size	_ZN2v88internalL11PrintFramesEPNS0_7IsolateEPNS0_12StringStreamENS0_10StackFrame9PrintModeE, .-_ZN2v88internalL11PrintFramesEPNS0_7IsolateEPNS0_12StringStreamENS0_10StackFrame9PrintModeE
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB34986:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE34986:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN2v88internal6Logger27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal6Logger27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.type	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, @function
_ZN2v88internal6Logger27is_listening_to_code_eventsEv:
.LFB21670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L45
	cmpq	$0, 80(%rbx)
	setne	%al
.L45:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21670:
	.size	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, .-_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB34987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L48
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L48:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE34987:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.rodata._ZN2v88internal26VerboseAccountingAllocator15ZoneDestructionEPKNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"zonedestruction"
	.section	.rodata._ZN2v88internal26VerboseAccountingAllocator15ZoneDestructionEPKNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"{\"type\": \"%s\", \"isolate\": \"%p\", \"time\": %f, \"ptr\": \"%p\", \"name\": \"%s\", \"size\": %zu,\"nesting\": %zu}\n"
	.section	.text._ZN2v88internal26VerboseAccountingAllocator15ZoneDestructionEPKNS0_4ZoneE,"axG",@progbits,_ZN2v88internal26VerboseAccountingAllocator15ZoneDestructionEPKNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26VerboseAccountingAllocator15ZoneDestructionEPKNS0_4ZoneE
	.type	_ZN2v88internal26VerboseAccountingAllocator15ZoneDestructionEPKNS0_4ZoneE, @function
_ZN2v88internal26VerboseAccountingAllocator15ZoneDestructionEPKNS0_4ZoneE:
.LFB25128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	40(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	lock subq	$1, (%rax)
	xorl	%r9d, %r9d
	movq	40(%rdi), %r15
	movq	40(%rsi), %rax
	testq	%rax, %rax
	je	.L53
	movq	16(%rsi), %rcx
	leaq	-24(%rcx), %r9
	subq	%rax, %r9
.L53:
	movq	24(%r13), %rdi
	movq	(%r12), %rbx
	movq	48(%r12), %r14
	leaq	-37592(%rdi), %rax
	addq	%r9, %rbx
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	-56(%rbp), %rax
	subq	$8, %rsp
	movq	%rbx, %r9
	movq	24(%r13), %rdx
	movq	%r14, %r8
	movq	%r12, %rcx
	leaq	.LC0(%rip), %rsi
	subsd	41464(%rax), %xmm0
	pushq	%r15
	movl	$1, %eax
	leaq	.LC1(%rip), %rdi
	subq	$37592, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25128:
	.size	_ZN2v88internal26VerboseAccountingAllocator15ZoneDestructionEPKNS0_4ZoneE, .-_ZN2v88internal26VerboseAccountingAllocator15ZoneDestructionEPKNS0_4ZoneE
	.section	.text._ZN2v88internal26VerboseAccountingAllocatorD2Ev,"axG",@progbits,_ZN2v88internal26VerboseAccountingAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26VerboseAccountingAllocatorD2Ev
	.type	_ZN2v88internal26VerboseAccountingAllocatorD2Ev, @function
_ZN2v88internal26VerboseAccountingAllocatorD2Ev:
.LFB34829:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal26VerboseAccountingAllocatorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal19AccountingAllocatorD2Ev@PLT
	.cfi_endproc
.LFE34829:
	.size	_ZN2v88internal26VerboseAccountingAllocatorD2Ev, .-_ZN2v88internal26VerboseAccountingAllocatorD2Ev
	.weak	_ZN2v88internal26VerboseAccountingAllocatorD1Ev
	.set	_ZN2v88internal26VerboseAccountingAllocatorD1Ev,_ZN2v88internal26VerboseAccountingAllocatorD2Ev
	.section	.text._ZN2v88internal26VerboseAccountingAllocatorD0Ev,"axG",@progbits,_ZN2v88internal26VerboseAccountingAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26VerboseAccountingAllocatorD0Ev
	.type	_ZN2v88internal26VerboseAccountingAllocatorD0Ev, @function
_ZN2v88internal26VerboseAccountingAllocatorD0Ev:
.LFB34831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal26VerboseAccountingAllocatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal19AccountingAllocatorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE34831:
	.size	_ZN2v88internal26VerboseAccountingAllocatorD0Ev, .-_ZN2v88internal26VerboseAccountingAllocatorD0Ev
	.section	.text._ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv,"axG",@progbits,_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv
	.type	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv, @function
_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv:
.LFB34989:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE34989:
	.size	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv, .-_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB34834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE34834:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm,"axG",@progbits,_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm
	.type	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm, @function
_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm:
.LFB34988:
	.cfi_startproc
	endbr64
	leaq	0(,%rsi,8), %rdi
	jmp	_ZN2v88internal8MallocednwEm@PLT
	.cfi_endproc
.LFE34988:
	.size	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm, .-_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm
	.section	.text._ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev,"axG",@progbits,_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev
	.type	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev, @function
_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev:
.LFB28878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	.cfi_endproc
.LFE28878:
	.size	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev, .-_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev
	.weak	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED1Ev
	.set	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED1Ev,_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED2Ev
	.section	.text._ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev,"axG",@progbits,_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev
	.type	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev, @function
_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev:
.LFB28880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28880:
	.size	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev, .-_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev
	.section	.rodata._ZN2v88internal26VerboseAccountingAllocator15AllocateSegmentEm.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"{\"type\": \"zone\", \"isolate\": \"%p\", \"time\": %f, \"allocated\": %zu}\n"
	.section	.text._ZN2v88internal26VerboseAccountingAllocator15AllocateSegmentEm,"axG",@progbits,_ZN2v88internal26VerboseAccountingAllocator15AllocateSegmentEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26VerboseAccountingAllocator15AllocateSegmentEm
	.type	_ZN2v88internal26VerboseAccountingAllocator15AllocateSegmentEm, @function
_ZN2v88internal26VerboseAccountingAllocator15AllocateSegmentEm:
.LFB25125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal19AccountingAllocator15AllocateSegmentEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L69
	movq	8(%rbx), %r13
	movq	32(%rbx), %rax
	addq	48(%rbx), %rax
	cmpq	%r13, %rax
	jb	.L75
.L69:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	leaq	-37592(%rdi), %r14
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	24(%rbx), %rax
	movq	%r13, %rdx
	subsd	41464(%r14), %xmm0
	leaq	.LC2(%rip), %rdi
	leaq	-37592(%rax), %rsi
	movl	$1, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r12, %rax
	movq	%r13, 32(%rbx)
	mfence
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25125:
	.size	_ZN2v88internal26VerboseAccountingAllocator15AllocateSegmentEm, .-_ZN2v88internal26VerboseAccountingAllocator15AllocateSegmentEm
	.section	.text._ZN2v88internal26VerboseAccountingAllocator13ReturnSegmentEPNS0_7SegmentE,"axG",@progbits,_ZN2v88internal26VerboseAccountingAllocator13ReturnSegmentEPNS0_7SegmentE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26VerboseAccountingAllocator13ReturnSegmentEPNS0_7SegmentE
	.type	_ZN2v88internal26VerboseAccountingAllocator13ReturnSegmentEPNS0_7SegmentE, @function
_ZN2v88internal26VerboseAccountingAllocator13ReturnSegmentEPNS0_7SegmentE:
.LFB25126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal19AccountingAllocator13ReturnSegmentEPNS0_7SegmentE@PLT
	movq	8(%rbx), %r12
	movq	48(%rbx), %rdx
	addq	%r12, %rdx
	movq	32(%rbx), %rax
	cmpq	%rax, %rdx
	jb	.L79
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	leaq	-37592(%rdi), %r13
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	24(%rbx), %rax
	movq	%r12, %rdx
	subsd	41464(%r13), %xmm0
	leaq	.LC2(%rip), %rdi
	leaq	-37592(%rax), %rsi
	movl	$1, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r12, 32(%rbx)
	mfence
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25126:
	.size	_ZN2v88internal26VerboseAccountingAllocator13ReturnSegmentEPNS0_7SegmentE, .-_ZN2v88internal26VerboseAccountingAllocator13ReturnSegmentEPNS0_7SegmentE
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB34985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$50904, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal28WorkerThreadRuntimeCallStatsD1Ev@PLT
	leaq	8432(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	leaq	8368(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	leaq	8304(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	leaq	8240(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	leaq	8176(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L80
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L83
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L87
.L80:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L80
.L87:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE34985:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB35665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE35665:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB34835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE34835:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB35666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE35666:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.rodata._ZN2v88internal26VerboseAccountingAllocator12ZoneCreationEPKNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"zonecreation"
	.section	.text._ZN2v88internal26VerboseAccountingAllocator12ZoneCreationEPKNS0_4ZoneE,"axG",@progbits,_ZN2v88internal26VerboseAccountingAllocator12ZoneCreationEPKNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26VerboseAccountingAllocator12ZoneCreationEPKNS0_4ZoneE
	.type	_ZN2v88internal26VerboseAccountingAllocator12ZoneCreationEPKNS0_4ZoneE, @function
_ZN2v88internal26VerboseAccountingAllocator12ZoneCreationEPKNS0_4ZoneE:
.LFB25127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	40(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %rcx
	movq	40(%rsi), %rax
	testq	%rax, %rax
	je	.L95
	movq	16(%rsi), %rsi
	leaq	-24(%rsi), %r9
	subq	%rax, %r9
.L95:
	movq	24(%r13), %rdi
	movq	(%r12), %rbx
	movq	%rcx, -64(%rbp)
	movq	48(%r12), %r15
	leaq	-37592(%rdi), %rax
	addq	%r9, %rbx
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rcx
	movq	%rbx, %r9
	subq	$8, %rsp
	movq	24(%r13), %rdx
	movq	%r15, %r8
	leaq	.LC3(%rip), %rsi
	subsd	41464(%rax), %xmm0
	pushq	%rcx
	movq	%r12, %rcx
	leaq	.LC1(%rip), %rdi
	subq	$37592, %rdx
	movl	$1, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	lock addq	$1, (%r14)
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25127:
	.size	_ZN2v88internal26VerboseAccountingAllocator12ZoneCreationEPKNS0_4ZoneE, .-_ZN2v88internal26VerboseAccountingAllocator12ZoneCreationEPKNS0_4ZoneE
	.section	.rodata._ZN2v88internalL15AddressToStringEm.str1.1,"aMS",@progbits,1
.LC4:
	.string	"0x"
	.section	.text._ZN2v88internalL15AddressToStringEm,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL15AddressToStringEm, @function
_ZN2v88internalL15AddressToStringEm:
.LFB25315:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	leaq	-368(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-448(%rbp), %r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -480(%rbp)
	movq	.LC5(%rip), %xmm1
	movhps	.LC6(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rbx, -448(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r12
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	leaq	-432(%rbp), %r8
	movq	%r12, -432(%rbp)
	movq	-24(%r12), %rax
	movq	%r8, %rdi
	movq	%r8, -472(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	addq	-24(%rax), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-464(%rbp), %xmm1
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-472(%rbp), %r8
	movl	$2, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-432(%rbp), %rax
	movq	-472(%rbp), %r8
	movq	-480(%rbp), %r10
	movq	-24(%rax), %rdx
	movq	%r8, %rdi
	movq	%r10, %rsi
	addq	%r8, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	16(%r13), %rax
	movb	$0, 16(%r13)
	movq	%rax, 0(%r13)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r13)
	testq	%rax, %rax
	je	.L101
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L107
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L103:
	movq	.LC5(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC7(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L104
	call	_ZdlPv@PLT
.L104:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r14, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	%r12, -432(%rbp)
	movq	-24(%r12), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	%rbx, -448(%rbp)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	addq	$440, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L103
.L108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25315:
	.size	_ZN2v88internalL15AddressToStringEm, .-_ZN2v88internalL15AddressToStringEm
	.section	.text._ZN2v88internal7Isolate18InitializeCountersEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Isolate18InitializeCountersEv.part.0, @function
_ZN2v88internal7Isolate18InitializeCountersEv.part.0:
.LFB35639:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$50976, %edi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rbx
	movabsq	$4294967297, %rax
	leaq	16(%rbx), %r13
	movq	%rax, 8(%rbx)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, (%rbx)
	call	_ZN2v88internal8CountersC1EPNS0_7IsolateE@PLT
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L110
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L110
.L111:
	movq	40968(%r12), %r14
	movq	%r13, %xmm0
	movq	%rbx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40960(%r12)
	testq	%r14, %r14
	je	.L129
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L120
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L136
.L129:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	movq	%r13, 16(%rbx)
	testq	%r15, %r15
	je	.L137
	lock addl	$1, 12(%rbx)
.L112:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L114
	testq	%r15, %r15
	je	.L115
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L116:
	cmpl	$1, %eax
	jne	.L114
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L114:
	movq	%rbx, 24(%rbx)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L120:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L129
.L136:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L123
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L124:
	cmpl	$1, %eax
	jne	.L129
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L137:
	addl	$1, 12(%rbx)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L115:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L123:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L124
	.cfi_endproc
.LFE35639:
	.size	_ZN2v88internal7Isolate18InitializeCountersEv.part.0, .-_ZN2v88internal7Isolate18InitializeCountersEv.part.0
	.section	.rodata._ZN2v88internal12_GLOBAL__N_116PredictExceptionEPNS0_15JavaScriptFrameE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"AbstractCode::INTERPRETED_FUNCTION == code->kind()"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_116PredictExceptionEPNS0_15JavaScriptFrameE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12_GLOBAL__N_116PredictExceptionEPNS0_15JavaScriptFrameE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_116PredictExceptionEPNS0_15JavaScriptFrameE, @function
_ZN2v88internal12_GLOBAL__N_116PredictExceptionEPNS0_15JavaScriptFrameE:
.LFB25051:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$4, %eax
	je	.L178
	movq	(%r12), %rax
	xorl	%esi, %esi
	leaq	-80(%rbp), %rdx
	movq	%r12, %rdi
	call	*168(%rax)
	testl	%eax, %eax
	jg	.L179
.L159:
	xorl	%r13d, %r13d
.L138:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movl	-80(%rbp), %r13d
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L178:
	movq	(%r12), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*168(%rax)
	testl	%eax, %eax
	jle	.L159
	movq	(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	*136(%rax)
	movq	-72(%rbp), %r13
	movq	-80(%rbp), %r12
	movabsq	$7905747460161236407, %rax
	movq	%r13, %r15
	subq	%r12, %r15
	movq	%r15, %rbx
	sarq	$3, %rbx
	imulq	%rax, %rbx
	testq	%rbx, %rbx
	je	.L142
	subq	$56, %r15
	leaq	-96(%rbp), %r14
.L152:
	leaq	(%r12,%r15), %rdi
	subq	$1, %rbx
	movq	32(%rdi), %r13
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$69, 11(%rdx)
	je	.L181
.L145:
	movq	-1(%rax), %rdx
	cmpw	$69, 11(%rdx)
	jne	.L151
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$12, %eax
	jne	.L182
.L151:
	call	_ZNK2v88internal12FrameSummary11code_offsetEv@PLT
	movq	0(%r13), %rsi
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	_ZN2v88internal12HandlerTableC1ENS0_13BytecodeArrayE@PLT
	xorl	%edx, %edx
	leaq	-100(%rbp), %rcx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal12HandlerTable11LookupRangeEiPiPNS1_15CatchPredictionE@PLT
	testl	%eax, %eax
	jle	.L149
	movl	-100(%rbp), %r13d
	testl	%r13d, %r13d
	je	.L149
.L148:
	movq	-72(%rbp), %rbx
	movq	-80(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L153
	.p2align 4,,10
	.p2align 3
.L154:
	movq	%r12, %rdi
	addq	$56, %r12
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%r12, %rbx
	jne	.L154
	movq	-80(%rbp), %r12
.L153:
	testq	%r12, %r12
	je	.L138
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L181:
	movq	-1(%rax), %rdx
	cmpw	$69, 11(%rdx)
	jne	.L145
	movl	43(%rax), %edx
	shrl	%edx
	andl	$31, %edx
	cmpl	$3, %edx
	jne	.L145
	movq	31(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$16, %edx
	jne	.L146
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$32, %al
	jne	.L183
	movl	$0, -100(%rbp)
	.p2align 4,,10
	.p2align 3
.L149:
	movq	-80(%rbp), %r12
	subq	$56, %r15
	testq	%rbx, %rbx
	jne	.L152
	movq	-72(%rbp), %r13
.L142:
	cmpq	%r13, %r12
	je	.L157
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%r12, %rdi
	addq	$56, %r12
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%r12, %r13
	jne	.L158
	movq	-80(%rbp), %r13
.L157:
	testq	%r13, %r13
	je	.L159
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L182:
	leaq	.LC8(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L146:
	movl	$2, -100(%rbp)
	movl	$2, %r13d
	jmp	.L148
.L183:
	movl	$1, -100(%rbp)
	movl	$1, %r13d
	jmp	.L148
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25051:
	.size	_ZN2v88internal12_GLOBAL__N_116PredictExceptionEPNS0_15JavaScriptFrameE, .-_ZN2v88internal12_GLOBAL__N_116PredictExceptionEPNS0_15JavaScriptFrameE
	.section	.text._ZN2v88internal12_GLOBAL__N_142InternalPromiseHasUserDefinedRejectHandlerEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_142InternalPromiseHasUserDefinedRejectHandlerEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEE, @function
_ZN2v88internal12_GLOBAL__N_142InternalPromiseHasUserDefinedRejectHandlerEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEE:
.LFB25079:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	35(%rdx), %r13
	sarl	$3, %r13d
	andl	$1, %r13d
	je	.L185
.L219:
	movl	$1, %r13d
.L184:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L241
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movq	%rdi, %rbx
	andq	$-262144, %rdx
	movq	%rsi, %r12
	movq	3792(%rbx), %rax
	leaq	3792(%rdi), %rsi
	movq	24(%rdx), %rdi
	movl	$2, %edx
	movq	-1(%rax), %rcx
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L187
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
.L187:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	3792(%rbx), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L242
.L188:
	leaq	-144(%rbp), %r14
	movq	%rsi, -112(%rbp)
	movq	%r14, %rdi
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L189
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rsi
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L191
.L194:
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal9JSPromise6statusEv@PLT
	testl	%eax, %eax
	jne	.L184
	movq	(%r12), %rax
	movq	41112(%rbx), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L222
	.p2align 4,,10
	.p2align 3
.L236:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L223:
	testb	$1, %sil
	je	.L184
	movq	41112(%rbx), %rdi
	movq	31(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L200
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L201:
	cmpq	%rsi, 88(%rbx)
	je	.L204
	movq	-1(%rsi), %rax
	cmpw	$1074, 11(%rax)
	je	.L205
	movq	(%r12), %rax
	movq	41112(%rbx), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L206
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L205:
	movq	(%r15), %rax
	movq	15(%rax), %rsi
	cmpq	%rsi, 88(%rbx)
	je	.L238
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L211
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L212:
	movq	(%rcx), %rdi
	movq	3784(%rbx), %rax
	leaq	3784(%rbx), %rsi
	movl	$2, %r8d
	andq	$-262144, %rdi
	movq	24(%rdi), %rdi
	movq	-1(%rax), %r9
	subq	$37592, %rdi
	cmpw	$64, 11(%r9)
	jne	.L214
	xorl	%r8d, %r8d
	testb	$1, 11(%rax)
	sete	%r8b
	addl	%r8d, %r8d
.L214:
	movabsq	$824633720832, %rax
	movl	%r8d, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	3784(%rbx), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %r8d
	andl	$-32, %r8d
	cmpl	$32, %r8d
	je	.L243
.L215:
	movq	%r14, %rdi
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L216
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L217:
	movq	88(%rbx), %rcx
	cmpq	%rcx, (%rax)
	je	.L219
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L219
.L238:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_142InternalPromiseHasUserDefinedRejectHandlerEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEE
	testb	%al, %al
	jne	.L219
.L204:
	movq	(%r15), %rax
	movq	41112(%rbx), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	jne	.L236
.L222:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L244
.L224:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L200:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L245
.L202:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L216:
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L211:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L246
.L213:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L206:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L247
.L207:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	%rax, %rsi
	movq	(%rsi), %rax
	testb	$1, %al
	je	.L194
.L191:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L194
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_142InternalPromiseHasUserDefinedRejectHandlerEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEE
	testb	%al, %al
	je	.L194
	movl	%eax, %r13d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L244:
	movq	%rbx, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%rbx, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%rcx, -152(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-152(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L242:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L246:
	movq	%rbx, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%rbx, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L207
.L241:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25079:
	.size	_ZN2v88internal12_GLOBAL__N_142InternalPromiseHasUserDefinedRejectHandlerEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEE, .-_ZN2v88internal12_GLOBAL__N_142InternalPromiseHasUserDefinedRejectHandlerEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEE
	.section	.text._ZNK2v88internal13BytecodeArray22HasSourcePositionTableEv,"axG",@progbits,_ZNK2v88internal13BytecodeArray22HasSourcePositionTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal13BytecodeArray22HasSourcePositionTableEv
	.type	_ZNK2v88internal13BytecodeArray22HasSourcePositionTableEv, @function
_ZNK2v88internal13BytecodeArray22HasSourcePositionTableEv:
.LFB13318:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	31(%rax), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	je	.L251
.L248:
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	%rdx, -37504(%rcx)
	je	.L248
	cmpq	%rdx, -37280(%rcx)
	setne	%al
	ret
	.cfi_endproc
.LFE13318:
	.size	_ZNK2v88internal13BytecodeArray22HasSourcePositionTableEv, .-_ZNK2v88internal13BytecodeArray22HasSourcePositionTableEv
	.section	.text._ZNK2v88internal18SharedFunctionInfo16HasBytecodeArrayEv,"axG",@progbits,_ZNK2v88internal18SharedFunctionInfo16HasBytecodeArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal18SharedFunctionInfo16HasBytecodeArrayEv
	.type	_ZNK2v88internal18SharedFunctionInfo16HasBytecodeArrayEv, @function
_ZNK2v88internal18SharedFunctionInfo16HasBytecodeArrayEv:
.LFB15710:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L253
.L256:
	movq	(%rdi), %rax
	movq	7(%rax), %rdx
	xorl	%eax, %eax
	testb	$1, %dl
	jne	.L260
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	movq	-1(%rdx), %rax
	cmpw	$91, 11(%rax)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	movq	-1(%rax), %rdx
	movl	$1, %eax
	cmpw	$72, 11(%rdx)
	jne	.L256
	ret
	.cfi_endproc
.LFE15710:
	.size	_ZNK2v88internal18SharedFunctionInfo16HasBytecodeArrayEv, .-_ZNK2v88internal18SharedFunctionInfo16HasBytecodeArrayEv
	.section	.text._ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv,"axG",@progbits,_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv
	.type	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv, @function
_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv:
.LFB15711:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L267
.L262:
	movq	7(%rdx), %rax
	testb	$1, %al
	jne	.L268
.L266:
	movq	(%rdi), %rax
	movq	7(%rax), %rax
	movq	7(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L262
	movq	39(%rax), %rcx
	testb	$1, %cl
	je	.L262
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L262
	movq	31(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L266
	movq	(%rdi), %rax
	movq	7(%rax), %rax
	ret
	.cfi_endproc
.LFE15711:
	.size	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv, .-_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv
	.section	.text._ZN2v88internal9DateCacheD2Ev,"axG",@progbits,_ZN2v88internal9DateCacheD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9DateCacheD2Ev
	.type	_ZN2v88internal9DateCacheD2Ev, @function
_ZN2v88internal9DateCacheD2Ev:
.LFB20897:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal9DateCacheE(%rip), %rax
	movq	%rax, (%rdi)
	movq	592(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L269
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L269:
	ret
	.cfi_endproc
.LFE20897:
	.size	_ZN2v88internal9DateCacheD2Ev, .-_ZN2v88internal9DateCacheD2Ev
	.weak	_ZN2v88internal9DateCacheD1Ev
	.set	_ZN2v88internal9DateCacheD1Ev,_ZN2v88internal9DateCacheD2Ev
	.section	.rodata._ZN2v88internal10CodeTracerC2Ei.str1.1,"aMS",@progbits,1
.LC10:
	.string	"code-%d-%d.asm"
.LC11:
	.string	"code-%d.asm"
.LC12:
	.string	""
	.section	.text._ZN2v88internal10CodeTracerC2Ei,"axG",@progbits,_ZN2v88internal10CodeTracerC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10CodeTracerC2Ei
	.type	_ZN2v88internal10CodeTracerC2Ei, @function
_ZN2v88internal10CodeTracerC2Ei:
.LFB20916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$16, %rdi
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	%rdi, (%rbx)
	movq	$128, 8(%rbx)
	movq	$0, 144(%rbx)
	movl	$0, 152(%rbx)
	je	.L279
	movq	_ZN2v88internal28FLAG_redirect_code_traces_toE(%rip), %rdx
	testq	%rdx, %rdx
	je	.L274
	movl	$128, %ecx
	movl	$128, %esi
	call	_ZN2v88internal7StrNCpyENS0_6VectorIcEEPKcm@PLT
.L275:
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	popq	%rbx
	xorl	%edx, %edx
	popq	%r12
	leaq	.LC12(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10WriteCharsEPKcS2_ib@PLT
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movl	%esi, %r12d
	testl	%esi, %esi
	js	.L276
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movq	8(%rbx), %r9
	movq	(%rbx), %rdi
	movl	%r12d, %r8d
	movl	%eax, %ecx
	leaq	.LC10(%rip), %rdx
	xorl	%eax, %eax
	movq	%r9, %rsi
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L279:
	movq	stdout(%rip), %rax
	movq	%rax, 144(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	leaq	.LC11(%rip), %rdx
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L275
	.cfi_endproc
.LFE20916:
	.size	_ZN2v88internal10CodeTracerC2Ei, .-_ZN2v88internal10CodeTracerC2Ei
	.weak	_ZN2v88internal10CodeTracerC1Ei
	.set	_ZN2v88internal10CodeTracerC1Ei,_ZN2v88internal10CodeTracerC2Ei
	.section	.rodata._ZN2v88internal32CodeSpaceMemoryModificationScopeC2EPNS0_4HeapE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"heap_->memory_allocator()->IsMemoryChunkExecutable(page)"
	.section	.text._ZN2v88internal32CodeSpaceMemoryModificationScopeC2EPNS0_4HeapE,"axG",@progbits,_ZN2v88internal32CodeSpaceMemoryModificationScopeC5EPNS0_4HeapE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal32CodeSpaceMemoryModificationScopeC2EPNS0_4HeapE
	.type	_ZN2v88internal32CodeSpaceMemoryModificationScopeC2EPNS0_4HeapE, @function
_ZN2v88internal32CodeSpaceMemoryModificationScopeC2EPNS0_4HeapE:
.LFB23542:
	.cfi_startproc
	endbr64
	cmpb	$0, 376(%rsi)
	movq	%rsi, (%rdi)
	jne	.L302
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	264(%rsi), %rdi
	addq	$1, 384(%rsi)
	call	_ZN2v88internal10PagedSpace18SetReadAndWritableEv@PLT
	movq	(%rbx), %rax
	movq	288(%rax), %rdx
	movq	32(%rdx), %r12
	testq	%r12, %r12
	je	.L280
	.p2align 4,,10
	.p2align 3
.L285:
	movq	2048(%rax), %rcx
	xorl	%edx, %edx
	movq	%r12, %rax
	movq	352(%rcx), %rdi
	divq	%rdi
	movq	344(%rcx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L282
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L303:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L282
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L282
.L284:
	cmpq	%r12, %rsi
	jne	.L303
	movq	%r12, %rdi
	call	_ZN2v88internal11MemoryChunk18SetReadAndWritableEv@PLT
	movq	224(%r12), %r12
	testq	%r12, %r12
	je	.L280
	movq	(%rbx), %rax
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L280:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	leaq	.LC13(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23542:
	.size	_ZN2v88internal32CodeSpaceMemoryModificationScopeC2EPNS0_4HeapE, .-_ZN2v88internal32CodeSpaceMemoryModificationScopeC2EPNS0_4HeapE
	.weak	_ZN2v88internal32CodeSpaceMemoryModificationScopeC1EPNS0_4HeapE
	.set	_ZN2v88internal32CodeSpaceMemoryModificationScopeC1EPNS0_4HeapE,_ZN2v88internal32CodeSpaceMemoryModificationScopeC2EPNS0_4HeapE
	.section	.text._ZN2v88internal19DefaultEmbeddedBlobEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19DefaultEmbeddedBlobEv
	.type	_ZN2v88internal19DefaultEmbeddedBlobEv, @function
_ZN2v88internal19DefaultEmbeddedBlobEv:
.LFB24932:
	.cfi_startproc
	endbr64
	movq	v8_Default_embedded_blob_(%rip), %rax
	ret
	.cfi_endproc
.LFE24932:
	.size	_ZN2v88internal19DefaultEmbeddedBlobEv, .-_ZN2v88internal19DefaultEmbeddedBlobEv
	.section	.text._ZN2v88internal23DefaultEmbeddedBlobSizeEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23DefaultEmbeddedBlobSizeEv
	.type	_ZN2v88internal23DefaultEmbeddedBlobSizeEv, @function
_ZN2v88internal23DefaultEmbeddedBlobSizeEv:
.LFB24933:
	.cfi_startproc
	endbr64
	movl	v8_Default_embedded_blob_size_(%rip), %eax
	ret
	.cfi_endproc
.LFE24933:
	.size	_ZN2v88internal23DefaultEmbeddedBlobSizeEv, .-_ZN2v88internal23DefaultEmbeddedBlobSizeEv
	.section	.text._ZN2v88internal30DisableEmbeddedBlobRefcountingEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30DisableEmbeddedBlobRefcountingEv
	.type	_ZN2v88internal30DisableEmbeddedBlobRefcountingEv, @function
_ZN2v88internal30DisableEmbeddedBlobRefcountingEv:
.LFB24943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L317
.L307:
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	movb	$0, _ZN2v88internal12_GLOBAL__N_133enable_embedded_blob_refcounting_E(%rip)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L318
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rax
	leaq	-64(%rbp), %r12
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L307
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L307
.L318:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24943:
	.size	_ZN2v88internal30DisableEmbeddedBlobRefcountingEv, .-_ZN2v88internal30DisableEmbeddedBlobRefcountingEv
	.section	.rodata._ZN2v88internal23FreeCurrentEmbeddedBlobEv.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"!enable_embedded_blob_refcounting_"
	.align 8
.LC15:
	.string	"StickyEmbeddedBlob() == Isolate::CurrentEmbeddedBlob()"
	.section	.text._ZN2v88internal23FreeCurrentEmbeddedBlobEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23FreeCurrentEmbeddedBlobEv
	.type	_ZN2v88internal23FreeCurrentEmbeddedBlobEv, @function
_ZN2v88internal23FreeCurrentEmbeddedBlobEv:
.LFB24944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal12_GLOBAL__N_133enable_embedded_blob_refcounting_E(%rip)
	jne	.L335
	movzbl	_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L336
.L321:
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpq	$0, _ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E(%rip)
	je	.L334
	movq	_ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E(%rip), %rax
	cmpq	%rax, _ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E(%rip)
	jne	.L337
	movl	_ZN2v88internal12_GLOBAL__N_127current_embedded_blob_size_E(%rip), %esi
	movq	_ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E(%rip), %rdi
	call	_ZN2v88internal17InstructionStream28FreeOffHeapInstructionStreamEPhj@PLT
	movq	$0, _ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E(%rip)
	movl	$0, _ZN2v88internal12_GLOBAL__N_127current_embedded_blob_size_E(%rip)
	movq	$0, _ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E(%rip)
	movl	$0, _ZN2v88internal12_GLOBAL__N_126sticky_embedded_blob_size_E(%rip)
.L334:
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rax
	leaq	-64(%rbp), %r12
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L321
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L335:
	leaq	.LC14(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L337:
	leaq	.LC15(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24944:
	.size	_ZN2v88internal23FreeCurrentEmbeddedBlobEv, .-_ZN2v88internal23FreeCurrentEmbeddedBlobEv
	.section	.text._ZN2v88internal7Isolate35CurrentEmbeddedBlobIsBinaryEmbeddedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate35CurrentEmbeddedBlobIsBinaryEmbeddedEv
	.type	_ZN2v88internal7Isolate35CurrentEmbeddedBlobIsBinaryEmbeddedEv, @function
_ZN2v88internal7Isolate35CurrentEmbeddedBlobIsBinaryEmbeddedEv:
.LFB24945:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E(%rip), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L339
	cmpq	%rdx, v8_Default_embedded_blob_(%rip)
	sete	%al
.L339:
	ret
	.cfi_endproc
.LFE24945:
	.size	_ZN2v88internal7Isolate35CurrentEmbeddedBlobIsBinaryEmbeddedEv, .-_ZN2v88internal7Isolate35CurrentEmbeddedBlobIsBinaryEmbeddedEv
	.section	.rodata._ZN2v88internal7Isolate15SetEmbeddedBlobEPKhj.str1.1,"aMS",@progbits,1
.LC16:
	.string	"(blob) != nullptr"
	.section	.text._ZN2v88internal7Isolate15SetEmbeddedBlobEPKhj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate15SetEmbeddedBlobEPKhj
	.type	_ZN2v88internal7Isolate15SetEmbeddedBlobEPKhj, @function
_ZN2v88internal7Isolate15SetEmbeddedBlobEPKhj:
.LFB24946:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L348
	movq	%rsi, 45528(%rdi)
	movl	%edx, 45536(%rdi)
	movq	%rsi, _ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E(%rip)
	movl	%edx, _ZN2v88internal12_GLOBAL__N_127current_embedded_blob_size_E(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC16(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24946:
	.size	_ZN2v88internal7Isolate15SetEmbeddedBlobEPKhj, .-_ZN2v88internal7Isolate15SetEmbeddedBlobEPKhj
	.section	.rodata._ZN2v88internal7Isolate17ClearEmbeddedBlobEv.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"enable_embedded_blob_refcounting_"
	.align 8
.LC18:
	.string	"embedded_blob_ == CurrentEmbeddedBlob()"
	.align 8
.LC19:
	.string	"embedded_blob_ == StickyEmbeddedBlob()"
	.section	.text._ZN2v88internal7Isolate17ClearEmbeddedBlobEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate17ClearEmbeddedBlobEv
	.type	_ZN2v88internal7Isolate17ClearEmbeddedBlobEv, @function
_ZN2v88internal7Isolate17ClearEmbeddedBlobEv:
.LFB24947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, _ZN2v88internal12_GLOBAL__N_133enable_embedded_blob_refcounting_E(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	je	.L354
	movq	_ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E(%rip), %rdx
	movq	45528(%rdi), %rax
	cmpq	%rdx, %rax
	jne	.L355
	cmpq	_ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E(%rip), %rax
	jne	.L356
	movq	$0, 45528(%rdi)
	movl	$0, 45536(%rdi)
	movq	$0, _ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E(%rip)
	movl	$0, _ZN2v88internal12_GLOBAL__N_127current_embedded_blob_size_E(%rip)
	movq	$0, _ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E(%rip)
	movl	$0, _ZN2v88internal12_GLOBAL__N_126sticky_embedded_blob_size_E(%rip)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	leaq	.LC17(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L355:
	leaq	.LC18(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L356:
	leaq	.LC19(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24947:
	.size	_ZN2v88internal7Isolate17ClearEmbeddedBlobEv, .-_ZN2v88internal7Isolate17ClearEmbeddedBlobEv
	.section	.text._ZNK2v88internal7Isolate13embedded_blobEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Isolate13embedded_blobEv
	.type	_ZNK2v88internal7Isolate13embedded_blobEv, @function
_ZNK2v88internal7Isolate13embedded_blobEv:
.LFB24948:
	.cfi_startproc
	endbr64
	movq	45528(%rdi), %rax
	ret
	.cfi_endproc
.LFE24948:
	.size	_ZNK2v88internal7Isolate13embedded_blobEv, .-_ZNK2v88internal7Isolate13embedded_blobEv
	.section	.text._ZNK2v88internal7Isolate18embedded_blob_sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Isolate18embedded_blob_sizeEv
	.type	_ZNK2v88internal7Isolate18embedded_blob_sizeEv, @function
_ZNK2v88internal7Isolate18embedded_blob_sizeEv:
.LFB24949:
	.cfi_startproc
	endbr64
	movl	45536(%rdi), %eax
	ret
	.cfi_endproc
.LFE24949:
	.size	_ZNK2v88internal7Isolate18embedded_blob_sizeEv, .-_ZNK2v88internal7Isolate18embedded_blob_sizeEv
	.section	.text._ZN2v88internal7Isolate19CurrentEmbeddedBlobEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv
	.type	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv, @function
_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv:
.LFB24950:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E(%rip), %rax
	ret
	.cfi_endproc
.LFE24950:
	.size	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv, .-_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv
	.section	.text._ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv
	.type	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv, @function
_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv:
.LFB24951:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12_GLOBAL__N_127current_embedded_blob_size_E(%rip), %eax
	ret
	.cfi_endproc
.LFE24951:
	.size	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv, .-_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv
	.section	.text._ZN2v88internal7Isolate26HashIsolateForEmbeddedBlobEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate26HashIsolateForEmbeddedBlobEv
	.type	_ZN2v88internal7Isolate26HashIsolateForEmbeddedBlobEv, @function
_ZN2v88internal7Isolate26HashIsolateForEmbeddedBlobEv:
.LFB24952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	37592(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	.p2align 4,,10
	.p2align 3
.L363:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Heap7builtinEi@PLT
	leaq	47(%rax), %rbx
	leaq	63(%rax), %r13
	.p2align 4,,10
	.p2align 3
.L362:
	movzbl	(%rbx), %esi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %r12
	cmpq	%rbx, %r13
	jne	.L362
	addl	$1, %r14d
	cmpl	$1553, %r14d
	jne	.L363
	movq	-32952(%r15), %rax
	movq	%r12, %rdi
	movslq	11(%rax), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base12hash_combineEmm@PLT
	.cfi_endproc
.LFE24952:
	.size	_ZN2v88internal7Isolate26HashIsolateForEmbeddedBlobEv, .-_ZN2v88internal7Isolate26HashIsolateForEmbeddedBlobEv
	.section	.text._ZN2v88internal7Isolate30FindPerThreadDataForThisThreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate30FindPerThreadDataForThisThreadEv
	.type	_ZN2v88internal7Isolate30FindPerThreadDataForThisThreadEv, @function
_ZN2v88internal7Isolate30FindPerThreadDataForThisThreadEv:
.LFB24955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	leaq	45800(%r12), %r13
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	45848(%r12), %rcx
	movslq	%ebx, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	movq	45840(%r12), %rax
	movq	(%rax,%rdx,8), %r12
	testq	%r12, %r12
	je	.L368
	movq	(%r12), %r12
	movq	%rdx, %r8
	movq	24(%r12), %rsi
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L369:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L368
	movq	24(%r12), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r8
	jne	.L381
.L371:
	cmpq	%rsi, %rdi
	jne	.L369
	cmpl	8(%r12), %ebx
	jne	.L369
	movq	16(%r12), %r12
.L368:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L368
	.cfi_endproc
.LFE24955:
	.size	_ZN2v88internal7Isolate30FindPerThreadDataForThisThreadEv, .-_ZN2v88internal7Isolate30FindPerThreadDataForThisThreadEv
	.section	.text._ZN2v88internal7Isolate26FindPerThreadDataForThreadENS0_8ThreadIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate26FindPerThreadDataForThreadENS0_8ThreadIdE
	.type	_ZN2v88internal7Isolate26FindPerThreadDataForThreadENS0_8ThreadIdE, @function
_ZN2v88internal7Isolate26FindPerThreadDataForThreadENS0_8ThreadIdE:
.LFB24956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	45800(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	45848(%r12), %rcx
	movslq	%ebx, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	movq	45840(%r12), %rax
	movq	(%rax,%rdx,8), %r12
	testq	%r12, %r12
	je	.L383
	movq	(%r12), %r12
	movq	%rdx, %r8
	movq	24(%r12), %rsi
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L384:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L383
	movq	24(%r12), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r8
	jne	.L396
.L386:
	cmpq	%rsi, %rdi
	jne	.L384
	cmpl	%ebx, 8(%r12)
	jne	.L384
	movq	16(%r12), %r12
.L383:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L383
	.cfi_endproc
.LFE24956:
	.size	_ZN2v88internal7Isolate26FindPerThreadDataForThreadENS0_8ThreadIdE, .-_ZN2v88internal7Isolate26FindPerThreadDataForThreadENS0_8ThreadIdE
	.section	.text._ZN2v88internal7Isolate24InitializeOncePerProcessEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate24InitializeOncePerProcessEv
	.type	_ZN2v88internal7Isolate24InitializeOncePerProcessEv, @function
_ZN2v88internal7Isolate24InitializeOncePerProcessEv:
.LFB24957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	movl	%eax, _ZN2v88internal7Isolate12isolate_key_E(%rip)
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, _ZN2v88internal7Isolate28per_isolate_thread_data_key_E(%rip)
	ret
	.cfi_endproc
.LFE24957:
	.size	_ZN2v88internal7Isolate24InitializeOncePerProcessEv, .-_ZN2v88internal7Isolate24InitializeOncePerProcessEv
	.section	.text._ZN2v88internal7Isolate19get_address_from_idENS0_16IsolateAddressIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate19get_address_from_idENS0_16IsolateAddressIdE
	.type	_ZN2v88internal7Isolate19get_address_from_idENS0_16IsolateAddressIdE, @function
_ZN2v88internal7Isolate19get_address_from_idENS0_16IsolateAddressIdE:
.LFB24958:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movq	40832(%rdi,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE24958:
	.size	_ZN2v88internal7Isolate19get_address_from_idENS0_16IsolateAddressIdE, .-_ZN2v88internal7Isolate19get_address_from_idENS0_16IsolateAddressIdE
	.section	.text._ZN2v88internal7Isolate13IterateThreadEPNS0_13ThreadVisitorEPc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate13IterateThreadEPNS0_13ThreadVisitorEPc
	.type	_ZN2v88internal7Isolate13IterateThreadEPNS0_13ThreadVisitorEPc, @function
_ZN2v88internal7Isolate13IterateThreadEPNS0_13ThreadVisitorEPc:
.LFB24960:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%r8, %rsi
	movq	(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE24960:
	.size	_ZN2v88internal7Isolate13IterateThreadEPNS0_13ThreadVisitorEPc, .-_ZN2v88internal7Isolate13IterateThreadEPNS0_13ThreadVisitorEPc
	.section	.text._ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPNS0_14ThreadLocalTopE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPNS0_14ThreadLocalTopE
	.type	_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPNS0_14ThreadLocalTopE, @function
_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPNS0_14ThreadLocalTopE:
.LFB24961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rdx), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1544, %rsp
	movq	%rdi, -1576(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	24(%rax), %r8
	cmpq	%r15, %r8
	jne	.L402
	leaq	40(%rdx), %r8
	movl	$6, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	*16(%rax)
	movq	(%rbx), %rax
	leaq	88(%r13), %rcx
	movq	24(%rax), %r8
	cmpq	%r15, %r8
	jne	.L404
.L434:
	leaq	96(%r13), %r8
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	*16(%rax)
	movq	(%rbx), %rax
	leaq	16(%r13), %rcx
	movq	24(%rax), %r8
	cmpq	%r15, %r8
	jne	.L406
.L435:
	leaq	24(%r13), %r8
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	*16(%rax)
	movq	(%rbx), %rax
	leaq	104(%r13), %rcx
	movq	24(%rax), %r8
	cmpq	%r15, %r8
	jne	.L408
.L436:
	leaq	112(%r13), %r8
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	*16(%rax)
.L409:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L410
	.p2align 4,,10
	.p2align 3
.L415:
	movq	(%rbx), %rax
	leaq	16(%r12), %rcx
	movq	24(%rax), %r8
	cmpq	%r15, %r8
	jne	.L411
	leaq	24(%r12), %r14
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	movq	%r14, %r8
	call	*16(%rax)
.L412:
	movq	(%rbx), %rax
	movq	24(%rax), %r8
	cmpq	%r15, %r8
	jne	.L413
	leaq	32(%r12), %r8
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	*16(%rax)
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.L415
.L410:
	leaq	-1568(%rbp), %r15
	leaq	-1504(%rbp), %r12
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	-1576(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L416
	.p2align 4,,10
	.p2align 3
.L417:
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	call	*24(%rax)
	movq	%r12, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L417
.L416:
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L433
	addq	$1544, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	*%r8
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.L415
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L411:
	xorl	%edx, %edx
	movl	$6, %esi
	leaq	24(%r12), %r14
	movq	%rbx, %rdi
	call	*%r8
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L402:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	*%r8
	movq	(%rbx), %rax
	leaq	88(%r13), %rcx
	movq	24(%rax), %r8
	cmpq	%r15, %r8
	je	.L434
.L404:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	*%r8
	movq	(%rbx), %rax
	leaq	16(%r13), %rcx
	movq	24(%rax), %r8
	cmpq	%r15, %r8
	je	.L435
.L406:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	*%r8
	movq	(%rbx), %rax
	leaq	104(%r13), %rcx
	movq	24(%rax), %r8
	cmpq	%r15, %r8
	je	.L436
.L408:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	*%r8
	jmp	.L409
.L433:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24961:
	.size	_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPNS0_14ThreadLocalTopE, .-_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPNS0_14ThreadLocalTopE
	.section	.text._ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPc
	.type	_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPc, @function
_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPc:
.LFB24959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPNS0_14ThreadLocalTopE
	addq	$8, %rsp
	leaq	192(%rbx), %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24959:
	.size	_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPc, .-_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPc
	.section	.text._ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorE
	.type	_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorE, @function
_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorE:
.LFB24962:
	.cfi_startproc
	endbr64
	leaq	12448(%rdi), %rdx
	jmp	_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPNS0_14ThreadLocalTopE
	.cfi_endproc
.LFE24962:
	.size	_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorE, .-_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal7Isolate22IterateDeferredHandlesEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate22IterateDeferredHandlesEPNS0_11RootVisitorE
	.type	_ZN2v88internal7Isolate22IterateDeferredHandlesEPNS0_11RootVisitorE, @function
_ZN2v88internal7Isolate22IterateDeferredHandlesEPNS0_11RootVisitorE:
.LFB24963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	45408(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L440
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L442:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal15DeferredHandles7IterateEPNS0_11RootVisitorE@PLT
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L442
.L440:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24963:
	.size	_ZN2v88internal7Isolate22IterateDeferredHandlesEPNS0_11RootVisitorE, .-_ZN2v88internal7Isolate22IterateDeferredHandlesEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal7Isolate23RegisterTryCatchHandlerEPNS_8TryCatchE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate23RegisterTryCatchHandlerEPNS_8TryCatchE
	.type	_ZN2v88internal7Isolate23RegisterTryCatchHandlerEPNS_8TryCatchE, @function
_ZN2v88internal7Isolate23RegisterTryCatchHandlerEPNS_8TryCatchE:
.LFB24964:
	.cfi_startproc
	endbr64
	movq	%rsi, 12448(%rdi)
	ret
	.cfi_endproc
.LFE24964:
	.size	_ZN2v88internal7Isolate23RegisterTryCatchHandlerEPNS_8TryCatchE, .-_ZN2v88internal7Isolate23RegisterTryCatchHandlerEPNS_8TryCatchE
	.section	.text._ZN2v88internal7Isolate25UnregisterTryCatchHandlerEPNS_8TryCatchE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate25UnregisterTryCatchHandlerEPNS_8TryCatchE
	.type	_ZN2v88internal7Isolate25UnregisterTryCatchHandlerEPNS_8TryCatchE, @function
_ZN2v88internal7Isolate25UnregisterTryCatchHandlerEPNS_8TryCatchE:
.LFB24965:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	movq	%rax, 12448(%rdi)
	ret
	.cfi_endproc
.LFE24965:
	.size	_ZN2v88internal7Isolate25UnregisterTryCatchHandlerEPNS_8TryCatchE, .-_ZN2v88internal7Isolate25UnregisterTryCatchHandlerEPNS_8TryCatchE
	.section	.rodata._ZNV2v88internal24StackTraceFailureMessage5PrintEv.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"Stacktrace:\n   ptr1=%p\n    ptr2=%p\n    ptr3=%p\n    ptr4=%p\n    failure_message_object=%p\n%s"
	.section	.text._ZNV2v88internal24StackTraceFailureMessage5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNV2v88internal24StackTraceFailureMessage5PrintEv
	.type	_ZNV2v88internal24StackTraceFailureMessage5PrintEv, @function
_ZNV2v88internal24StackTraceFailureMessage5PrintEv:
.LFB24971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80(%rdi), %rax
	movq	40(%rdi), %r8
	movq	%rdi, %r9
	movq	32(%rdi), %rcx
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rsi
	leaq	.LC20(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%rax
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24971:
	.size	_ZNV2v88internal24StackTraceFailureMessage5PrintEv, .-_ZNV2v88internal24StackTraceFailureMessage5PrintEv
	.section	.rodata._ZN2v88internal17FrameArrayBuilder16AppendAsyncFrameENS0_6HandleINS0_17JSGeneratorObjectEEE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"unreachable code"
	.section	.text._ZN2v88internal17FrameArrayBuilder16AppendAsyncFrameENS0_6HandleINS0_17JSGeneratorObjectEEE,"axG",@progbits,_ZN2v88internal17FrameArrayBuilder16AppendAsyncFrameENS0_6HandleINS0_17JSGeneratorObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17FrameArrayBuilder16AppendAsyncFrameENS0_6HandleINS0_17JSGeneratorObjectEEE
	.type	_ZN2v88internal17FrameArrayBuilder16AppendAsyncFrameENS0_6HandleINS0_17JSGeneratorObjectEEE, @function
_ZN2v88internal17FrameArrayBuilder16AppendAsyncFrameENS0_6HandleINS0_17JSGeneratorObjectEEE:
.LFB24983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rax
	sarq	$32, %rax
	cmpl	%eax, 12(%rdi)
	jle	.L452
	movq	(%rdi), %r14
	movq	(%rsi), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	41112(%r14), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L454
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	movl	8(%rbx), %eax
	cmpl	$1, %eax
	je	.L457
.L515:
	cmpl	$2, %eax
	jne	.L508
.L464:
	cmpb	$0, _ZN2v88internal29FLAG_builtins_in_stack_tracesE(%rip)
	je	.L509
.L471:
	cmpb	$0, 26(%rbx)
	je	.L473
	movq	(%rbx), %rax
	movq	12464(%rax), %rdx
	movq	0(%r13), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %rax
	movq	1151(%rax), %rax
	movq	39(%rdx), %rdx
	movq	1151(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L452
.L473:
	cmpb	$0, 25(%rbx)
	je	.L474
.L476:
	movl	$72, %r9d
.L475:
	movq	(%rbx), %r15
	movq	(%r12), %rax
	movq	41112(%r15), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L477
	movl	%r9d, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-72(%rbp), %r9d
	movq	%rax, %r14
.L478:
	movq	0(%r13), %rax
	movq	(%rbx), %rdx
	movq	23(%rax), %rax
	movq	31(%rax), %rcx
	testb	$1, %cl
	jne	.L510
.L480:
	movq	7(%rax), %rcx
	testb	$1, %cl
	jne	.L511
.L482:
	movq	7(%rax), %rax
	movq	7(%rax), %rsi
.L481:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L483
	movl	%r9d, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-72(%rbp), %r9d
	movq	%rax, %r15
.L484:
	movq	(%r12), %rax
	movq	(%rbx), %rdi
	movslq	51(%rax), %r8
	leaq	288(%rdi), %rax
	subl	$53, %r8d
	cmpb	$0, _ZN2v88internal31FLAG_detailed_error_stack_traceE(%rip)
	jne	.L512
.L486:
	subq	$8, %rsp
	movq	32(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r15, %rcx
	pushq	%rax
	movq	%r14, %rsi
	call	_ZN2v88internal10FrameArray13AppendJSFrameENS0_6HandleIS1_EENS2_INS0_6ObjectEEENS2_INS0_10JSFunctionEEENS2_INS0_12AbstractCodeEEEiiNS2_INS0_10FixedArrayEEE@PLT
	movq	%rax, 32(%rbx)
	popq	%rax
	popq	%rdx
.L452:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L513
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	.cfi_restore_state
	movq	41088(%r14), %r13
	cmpq	%r13, 41096(%r14)
	je	.L514
.L456:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	movl	8(%rbx), %eax
	cmpl	$1, %eax
	jne	.L515
.L457:
	cmpb	$0, 24(%rbx)
	je	.L464
	movq	16(%rbx), %rax
	movq	0(%r13), %rcx
	cmpq	%rcx, (%rax)
	jne	.L452
	movb	$0, 24(%rbx)
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L508:
	testl	%eax, %eax
	je	.L516
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L516:
	cmpb	$0, 24(%rbx)
	je	.L464
	movb	$0, 24(%rbx)
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L509:
	movq	0(%r13), %rdx
	movq	23(%rdx), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L517
.L466:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L471
	movq	0(%r13), %rdx
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L474:
	movq	0(%r13), %rax
	movq	23(%rax), %rax
	movl	47(%rax), %eax
	andl	$64, %eax
	setne	25(%rbx)
	jne	.L476
	movl	$64, %r9d
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L483:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L518
.L485:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L477:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L519
.L479:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L510:
	movq	-1(%rcx), %rsi
	cmpw	$86, 11(%rsi)
	jne	.L480
	movq	39(%rcx), %rsi
	testb	$1, %sil
	je	.L480
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L480
	movq	31(%rcx), %rsi
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L511:
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L482
	movq	7(%rax), %rsi
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L514:
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L517:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	je	.L520
.L467:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	%rax, -37504(%rcx)
	jne	.L466
.L468:
	movq	23(%rdx), %rax
	movl	47(%rax), %eax
	testb	$32, %al
	jne	.L471
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	je	.L452
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L452
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L512:
	movq	0(%r13), %rax
	movl	%r8d, -88(%rbp)
	xorl	%edx, %edx
	movl	%r9d, -80(%rbp)
	movq	23(%rax), %rax
	movzwl	41(%rax), %ecx
	movl	%ecx, %esi
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movl	-72(%rbp), %ecx
	movl	-80(%rbp), %r9d
	movl	-88(%rbp), %r8d
	testl	%ecx, %ecx
	je	.L486
	leal	-1(%rcx), %edx
	movl	%r9d, -92(%rbp)
	movl	$16, %ecx
	movl	%r8d, -96(%rbp)
	leaq	24(,%rdx,8), %rdx
	movq	%r15, -112(%rbp)
	movq	%r13, -104(%rbp)
	movq	%rcx, %r13
	movq	%r14, -120(%rbp)
	movq	%r12, %r14
	movq	%rdx, %r12
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L490:
	movq	(%r14), %rdx
	movq	(%rbx), %r15
	movq	71(%rdx), %rdx
	leaq	-1(%r15,%r13), %rsi
	movq	-1(%r13,%rdx), %rdx
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L492
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %r10
	movq	%r8, -72(%rbp)
	testl	$262144, %r10d
	je	.L488
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	8(%r8), %r10
.L488:
	andl	$24, %r10d
	je	.L492
	movq	%r15, %r8
	andq	$-262144, %r8
	testb	$24, 8(%r8)
	jne	.L492
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L492:
	addq	$8, %r13
	cmpq	%r12, %r13
	jne	.L490
	movq	%rbx, %rax
	movl	-92(%rbp), %r9d
	movq	-104(%rbp), %r13
	movl	-96(%rbp), %r8d
	movq	-112(%rbp), %r15
	movq	-120(%rbp), %r14
	movq	-128(%rbp), %rbx
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L519:
	movq	%r15, %rdi
	movq	%rsi, -80(%rbp)
	movl	%r9d, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %r9d
	movq	%rax, %r14
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L518:
	movq	%rdx, %rdi
	movq	%rsi, -88(%rbp)
	movl	%r9d, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movl	-80(%rbp), %r9d
	movq	-72(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L485
.L520:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L467
	jmp	.L466
.L513:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24983:
	.size	_ZN2v88internal17FrameArrayBuilder16AppendAsyncFrameENS0_6HandleINS0_17JSGeneratorObjectEEE, .-_ZN2v88internal17FrameArrayBuilder16AppendAsyncFrameENS0_6HandleINS0_17JSGeneratorObjectEEE
	.section	.text._ZN2v88internal17FrameArrayBuilder21AppendPromiseAllFrameENS0_6HandleINS0_7ContextEEEi,"axG",@progbits,_ZN2v88internal17FrameArrayBuilder21AppendPromiseAllFrameENS0_6HandleINS0_7ContextEEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17FrameArrayBuilder21AppendPromiseAllFrameENS0_6HandleINS0_7ContextEEEi
	.type	_ZN2v88internal17FrameArrayBuilder21AppendPromiseAllFrameENS0_6HandleINS0_7ContextEEEi, @function
_ZN2v88internal17FrameArrayBuilder21AppendPromiseAllFrameENS0_6HandleINS0_7ContextEEEi:
.LFB24984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rax
	sarq	$32, %rax
	cmpl	%eax, 12(%rdi)
	jg	.L560
.L521:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L561
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	movq	(%rdi), %r13
	movq	(%rsi), %rax
	movq	%rdi, %rbx
	movl	%edx, %r12d
	movq	39(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L523
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L524:
	movq	(%rbx), %r15
	movq	1695(%rsi), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L526
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	movl	8(%rbx), %eax
	cmpl	$1, %eax
	je	.L529
.L567:
	cmpl	$2, %eax
	jne	.L562
.L536:
	cmpb	$0, _ZN2v88internal29FLAG_builtins_in_stack_tracesE(%rip)
	je	.L563
.L543:
	cmpb	$0, 26(%rbx)
	movq	(%rbx), %r15
	jne	.L564
.L547:
	movq	(%r14), %rax
	movq	1711(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L548
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L549:
	movq	(%rbx), %r15
	movq	0(%r13), %rax
	movq	47(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L551
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L552:
	movq	(%rbx), %rax
	subq	$8, %rsp
	movq	32(%rbx), %rdi
	movq	%r13, %rdx
	movl	$192, %r9d
	movl	%r12d, %r8d
	movq	%r14, %rsi
	addq	$288, %rax
	pushq	%rax
	call	_ZN2v88internal10FrameArray13AppendJSFrameENS0_6HandleIS1_EENS2_INS0_6ObjectEEENS2_INS0_10JSFunctionEEENS2_INS0_12AbstractCodeEEEiiNS2_INS0_10FixedArrayEEE@PLT
	movq	%rax, 32(%rbx)
	popq	%rax
	popq	%rdx
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L562:
	testl	%eax, %eax
	je	.L565
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L526:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L566
.L528:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	movl	8(%rbx), %eax
	cmpl	$1, %eax
	jne	.L567
.L529:
	cmpb	$0, 24(%rbx)
	je	.L536
	movq	16(%rbx), %rax
	movq	0(%r13), %rcx
	cmpq	%rcx, (%rax)
	jne	.L521
	movb	$0, 24(%rbx)
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L523:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L568
.L525:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L563:
	movq	0(%r13), %rdx
	movq	23(%rdx), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L569
.L538:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L543
	movq	0(%r13), %rdx
.L540:
	movq	23(%rdx), %rax
	movl	47(%rax), %eax
	testb	$32, %al
	jne	.L543
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	je	.L521
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L521
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L565:
	cmpb	$0, 24(%rbx)
	je	.L536
	movb	$0, 24(%rbx)
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L551:
	movq	41088(%r15), %rcx
	cmpq	41096(%r15), %rcx
	je	.L570
.L553:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L548:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L571
.L550:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L568:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L566:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L564:
	movq	0(%r13), %rax
	movq	12464(%r15), %rdx
	movq	31(%rax), %rax
	movq	39(%rax), %rax
	movq	1151(%rax), %rax
	movq	39(%rdx), %rdx
	movq	1151(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L521
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L569:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	je	.L572
.L539:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37504(%rcx), %rax
	je	.L540
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L570:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L571:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L550
.L572:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L539
	jmp	.L538
.L561:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24984:
	.size	_ZN2v88internal17FrameArrayBuilder21AppendPromiseAllFrameENS0_6HandleINS0_7ContextEEEi, .-_ZN2v88internal17FrameArrayBuilder21AppendPromiseAllFrameENS0_6HandleINS0_7ContextEEEi
	.section	.text._ZN2v88internal17FrameArrayBuilder33GetElementsAsStackTraceFrameArrayEb,"axG",@progbits,_ZN2v88internal17FrameArrayBuilder33GetElementsAsStackTraceFrameArrayEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17FrameArrayBuilder33GetElementsAsStackTraceFrameArrayEb
	.type	_ZN2v88internal17FrameArrayBuilder33GetElementsAsStackTraceFrameArrayEb, @function
_ZN2v88internal17FrameArrayBuilder33GetElementsAsStackTraceFrameArrayEb:
.LFB24991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	movq	%r13, %r15
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movb	%sil, -73(%rbp)
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal10FrameArray11ShrinkToFitEPNS0_7IsolateE@PLT
	movq	32(%r13), %rax
	xorl	%edx, %edx
	movq	(%rax), %rax
	movq	15(%rax), %rbx
	movq	0(%r13), %rdi
	movl	%r14d, %r13d
	sarq	$32, %rbx
	movl	%ebx, %esi
	movl	%ebx, -80(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -72(%rbp)
	testq	%rbx, %rbx
	jg	.L574
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L578:
	movq	(%r15), %r10
	xorl	%r12d, %r12d
	leal	2(%rdx), %ebx
.L581:
	movq	32(%r15), %rsi
	movq	%r10, %rdi
	sall	$3, %ebx
	movslq	%ebx, %rbx
	call	_ZN2v88internal7Factory18NewStackTraceFrameENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	%rax, %r14
	movq	-72(%rbp), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rdi
	leaq	-1(%rdi,%rbx), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L629
	movq	%rdx, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L609
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
.L609:
	testb	$24, %al
	je	.L629
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L629
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L629:
	testb	%r12b, %r12b
	jne	.L668
.L607:
	cmpl	%r13d, -80(%rbp)
	je	.L628
.L574:
	movl	%r13d, %edx
	addl	$1, %r13d
	cmpb	$0, -73(%rbp)
	je	.L578
	movq	32(%r15), %rax
	leal	0(%r13,%r13,2), %ebx
	sall	$4, %ebx
	movq	(%rax), %rcx
	leal	8(%rbx), %eax
	cltq
	leaq	-1(%rcx,%rax), %rax
	movq	(%rax), %rsi
	btq	$32, %rsi
	jc	.L578
	movq	(%rax), %rsi
	btq	$33, %rsi
	jc	.L578
	movq	(%rax), %rax
	btq	$34, %rax
	jc	.L578
	leal	-16(%rbx), %eax
	cltq
	movq	-1(%rax,%rcx), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L669
.L576:
	movq	-112(%rbp), %rdi
	movl	%edx, -88(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movl	-88(%rbp), %edx
	testb	%al, %al
	movl	%eax, %r12d
	je	.L578
	movq	32(%r15), %rax
	movslq	%ebx, %rcx
	subl	$8, %ebx
	movslq	%ebx, %rbx
	movq	(%rax), %rax
	movq	-1(%rcx,%rax), %r14
	movq	(%r15), %rcx
	movq	-1(%rbx,%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L670
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-88(%rbp), %edx
.L582:
	cmpb	$0, _ZN2v88internal22FLAG_optimize_for_sizeE(%rip)
	movq	(%r15), %r10
	leal	2(%rdx), %ebx
	jne	.L581
	movq	(%rax), %rax
	movq	-1(%rax), %rcx
	cmpw	$69, 11(%rcx)
	je	.L671
	movq	31(%rax), %rax
.L586:
	testb	$1, %al
	jne	.L672
.L587:
	xorl	%esi, %esi
.L588:
	movq	41112(%r10), %rdi
	testq	%rdi, %rdi
	je	.L589
	movl	%edx, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %r10
	movl	-96(%rbp), %edx
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L590:
	testb	$1, %sil
	jne	.L592
.L600:
	movq	(%r15), %r10
	leal	2(%rdx), %ebx
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L668:
	movq	32(%r15), %rdx
	leal	0(%r13,%r13,2), %eax
	sall	$4, %eax
	movq	(%rdx), %rdx
	movslq	%eax, %rcx
	subl	$8, %eax
	cltq
	movq	-1(%rcx,%rdx), %rcx
	movq	(%r15), %r12
	movq	%rcx, -88(%rbp)
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L612
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L613:
	cmpb	$0, _ZN2v88internal22FLAG_optimize_for_sizeE(%rip)
	movq	(%r15), %r11
	jne	.L607
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	cmpw	$69, 11(%rdx)
	je	.L673
	movq	31(%rax), %rax
.L617:
	testb	$1, %al
	jne	.L674
.L618:
	movq	41112(%r11), %rdi
	xorl	%esi, %esi
	testq	%rdi, %rdi
	je	.L620
.L677:
	movq	%r11, -96(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-96(%rbp), %r11
	movq	(%rax), %rsi
	movq	%rax, %r12
	testb	$1, %sil
	jne	.L623
.L625:
	movl	$1, %esi
	movq	%r11, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, -96(%rbp)
	call	_ZN2v88internal9HashTableINS0_22SimpleNumberDictionaryENS0_27SimpleNumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	-96(%rbp), %r11
	movq	%rax, %rsi
.L624:
	movq	-88(%rbp), %rdx
	movq	%r11, %rdi
	movq	%r14, %rcx
	movq	%rsi, -88(%rbp)
	sarq	$32, %rdx
	call	_ZN2v88internal22SimpleNumberDictionary3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r11
	movq	(%rax), %rax
	cmpq	%rax, (%rsi)
	jne	.L626
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L675
.L626:
	movq	%r11, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12AbstractCode18SetStackFrameCacheENS0_6HandleIS1_EENS2_INS0_22SimpleNumberDictionaryEEE@PLT
	cmpl	%r13d, -80(%rbp)
	jne	.L574
	.p2align 4,,10
	.p2align 3
.L628:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L676
	movq	-72(%rbp), %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$97, 11(%rdx)
	jne	.L618
	movq	41112(%r11), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	jne	.L677
	.p2align 4,,10
	.p2align 3
.L620:
	movq	41088(%r11), %r12
	cmpq	41096(%r11), %r12
	je	.L678
.L622:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r11)
	movq	%rsi, (%r12)
	testb	$1, %sil
	je	.L625
.L623:
	movq	-1(%rsi), %rax
	cmpw	$133, 11(%rax)
	jne	.L625
	movq	%r12, %rsi
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L612:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L679
.L614:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L669:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	je	.L680
.L577:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	%rax, -37504(%rcx)
	je	.L578
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L673:
	movq	23(%rax), %rax
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L670:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L681
.L583:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L680:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L577
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L675:
	movq	-1(%rax), %rax
	cmpw	$133, 11(%rax)
	jne	.L626
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L679:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L678:
	movq	%r11, %rdi
	movq	%rsi, -104(%rbp)
	movq	%r11, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %r11
	movq	%rax, %r12
	jmp	.L622
.L672:
	movq	-1(%rax), %rcx
	cmpw	$97, 11(%rcx)
	jne	.L587
	movq	15(%rax), %rsi
	jmp	.L588
.L589:
	movq	41088(%r10), %rbx
	cmpq	41096(%r10), %rbx
	je	.L682
.L591:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r10)
	movq	%rsi, (%rbx)
	jmp	.L590
.L592:
	movq	-1(%rsi), %rax
	cmpw	$133, 11(%rax)
	jne	.L600
	movq	1176(%r10), %rax
	movq	(%rbx), %r11
	movq	%r14, %rdi
	movl	%edx, -116(%rbp)
	sarq	$32, %rdi
	movq	%r10, -88(%rbp)
	movq	15(%rax), %rsi
	movq	%r11, -104(%rbp)
	movl	%edi, -96(%rbp)
	call	_Z11halfsiphashjm@PLT
	movq	-104(%rbp), %r11
	movq	-88(%rbp), %r10
	movl	$1, %ecx
	movl	-116(%rbp), %edx
	movslq	35(%r11), %rsi
	movq	88(%r10), %r14
	subq	$1, %r11
	movl	%edx, -88(%rbp)
	movq	96(%r10), %r8
	leal	-1(%rsi), %r9d
	movl	-96(%rbp), %edx
	andl	%r9d, %eax
	movl	%eax, %esi
	jmp	.L599
.L683:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L597:
	cvttsd2siq	%xmm0, %rax
	cmpl	%eax, %edx
	je	.L598
.L595:
	addl	%ecx, %esi
	addl	$1, %ecx
	andl	%r9d, %esi
.L599:
	leal	(%rsi,%rsi), %edi
	leal	40(,%rdi,8), %eax
	cltq
	movq	(%rax,%r11), %rax
	cmpq	%rax, %r14
	je	.L666
	cmpq	%rax, %r8
	je	.L595
	testb	$1, %al
	je	.L683
	movsd	7(%rax), %xmm0
	jmp	.L597
.L681:
	movq	%rcx, %rdi
	movl	%edx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-104(%rbp), %edx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rcx
	jmp	.L583
.L671:
	movq	23(%rax), %rax
	jmp	.L586
.L682:
	movq	%r10, %rdi
	movl	%edx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-104(%rbp), %edx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %r10
	movq	%rax, %rbx
	jmp	.L591
.L666:
	movl	-88(%rbp), %edx
	jmp	.L600
.L598:
	movl	-88(%rbp), %edx
	cmpl	$-1, %esi
	je	.L600
	leal	48(,%rdi,8), %eax
	movq	(%rbx), %rcx
	cltq
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%r10), %rdi
	testq	%rdi, %rdi
	je	.L601
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-88(%rbp), %edx
	leal	2(%rdx), %ebx
	leal	0(,%rbx,8), %ecx
	movslq	%ecx, %rcx
	testq	%rax, %rax
	je	.L684
.L602:
	movq	-72(%rbp), %rdi
	movq	(%rax), %r12
	movq	(%rdi), %rbx
	leaq	-1(%rbx,%rcx), %r14
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L607
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L605
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	8(%rcx), %rax
.L605:
	testb	$24, %al
	je	.L607
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L607
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L607
.L601:
	movq	41088(%r10), %rax
	cmpq	41096(%r10), %rax
	je	.L685
.L604:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r10)
	leal	16(,%rdx,8), %ecx
	movq	%rsi, (%rax)
	movslq	%ecx, %rcx
	jmp	.L602
.L684:
	movq	(%r15), %r10
	jmp	.L581
.L676:
	call	__stack_chk_fail@PLT
.L685:
	movq	%r10, %rdi
	movl	%edx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-104(%rbp), %edx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %r10
	jmp	.L604
	.cfi_endproc
.LFE24991:
	.size	_ZN2v88internal17FrameArrayBuilder33GetElementsAsStackTraceFrameArrayEb, .-_ZN2v88internal17FrameArrayBuilder33GetElementsAsStackTraceFrameArrayEb
	.section	.text._ZN2v88internal18GetStackTraceLimitEPNS0_7IsolateEPi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18GetStackTraceLimitEPNS0_7IsolateEPi
	.type	_ZN2v88internal18GetStackTraceLimitEPNS0_7IsolateEPi, @function
_ZN2v88internal18GetStackTraceLimitEPNS0_7IsolateEPi:
.LFB24998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1607(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L687
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L688:
	movq	(%rbx), %rdx
	movq	3328(%r12), %rax
	leaq	3328(%r12), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	movq	-1(%rax), %rcx
	movl	$2, %edx
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L690
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
.L690:
	movabsq	$824633720832, %rax
	movl	%edx, -128(%rbp)
	movq	%rax, -116(%rbp)
	movq	3328(%r12), %rax
	movq	%rdi, -104(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L713
.L691:
	leaq	-128(%rbp), %r14
	movq	%rsi, -96(%rbp)
	movq	%r14, %rdi
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -124(%rbp)
	jne	.L692
	movq	-104(%rbp), %rax
	addq	$88, %rax
.L693:
	movq	(%rax), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	je	.L714
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
.L697:
	comisd	.LC22(%rip), %xmm0
	jb	.L711
	comisd	.LC23(%rip), %xmm0
	movl	$2147483647, %edx
	ja	.L698
	cvttsd2sil	%xmm0, %edx
	movl	$0, %eax
	testl	%edx, %edx
	cmovs	%eax, %edx
.L698:
	movl	%edx, 0(%r13)
	cmpl	%edx, _ZN2v88internal22FLAG_stack_trace_limitE(%rip)
	movl	$1, %eax
	jne	.L715
.L686:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L716
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	movl	37984(%r12), %edx
	testl	%edx, %edx
	jne	.L700
	movq	45488(%r12), %rdx
	testq	%rdx, %rdx
	je	.L686
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	movl	$45, %esi
	movq	%r12, %rdi
	addl	$1, 41104(%r12)
	call	*%rdx
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L701
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L701:
	movl	$1, %eax
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L714:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L686
	movsd	7(%rdx), %xmm0
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L692:
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L687:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L717
.L689:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L713:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L700:
	leaq	37592(%r12), %rdi
	movl	$45, %esi
	movb	%al, -136(%rbp)
	call	_ZN2v88internal4Heap22IncrementDeferredCountENS_7Isolate17UseCounterFeatureE@PLT
	movzbl	-136(%rbp), %eax
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L717:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L689
.L711:
	xorl	%edx, %edx
	jmp	.L698
.L716:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24998:
	.size	_ZN2v88internal18GetStackTraceLimitEPNS0_7IsolateEPi, .-_ZN2v88internal18GetStackTraceLimitEPNS0_7IsolateEPi
	.section	.text._ZN2v88internal17IsBuiltinFunctionEPNS0_7IsolateENS0_10HeapObjectENS0_8Builtins4NameE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17IsBuiltinFunctionEPNS0_7IsolateENS0_10HeapObjectENS0_8Builtins4NameE
	.type	_ZN2v88internal17IsBuiltinFunctionEPNS0_7IsolateENS0_10HeapObjectENS0_8Builtins4NameE, @function
_ZN2v88internal17IsBuiltinFunctionEPNS0_7IsolateENS0_10HeapObjectENS0_8Builtins4NameE:
.LFB25000:
	.cfi_startproc
	endbr64
	movq	-1(%rsi), %rcx
	cmpw	$1105, 11(%rcx)
	je	.L726
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L726:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$41184, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	movl	%edx, %esi
	subq	$8, %rsp
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, %r8
	movq	47(%rbx), %rax
	cmpq	%rax, %r8
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25000:
	.size	_ZN2v88internal17IsBuiltinFunctionEPNS0_7IsolateENS0_10HeapObjectENS0_8Builtins4NameE, .-_ZN2v88internal17IsBuiltinFunctionEPNS0_7IsolateENS0_10HeapObjectENS0_8Builtins4NameE
	.section	.rodata._ZN2v88internal22CaptureAsyncStackTraceEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEEPNS0_17FrameArrayBuilderE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"generator_object->is_suspended()"
	.align 8
.LC25:
	.string	"promise_or_capability->IsUndefined(isolate)"
	.section	.text._ZN2v88internal22CaptureAsyncStackTraceEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEEPNS0_17FrameArrayBuilderE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22CaptureAsyncStackTraceEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEEPNS0_17FrameArrayBuilderE
	.type	_ZN2v88internal22CaptureAsyncStackTraceEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEEPNS0_17FrameArrayBuilderE, @function
_ZN2v88internal22CaptureAsyncStackTraceEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEEPNS0_17FrameArrayBuilderE:
.LFB25001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.L728:
	movq	32(%r12), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rax
	sarq	$32, %rax
	cmpl	%eax, 12(%r12)
	jle	.L727
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9JSPromise6statusEv@PLT
	testl	%eax, %eax
	jne	.L727
	movq	(%rbx), %rax
	movq	23(%rax), %rbx
	testb	$1, %bl
	jne	.L806
.L727:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L807
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L806:
	.cfi_restore_state
	movq	-1(%rbx), %rax
	cmpw	$94, 11(%rax)
	jne	.L727
	movq	41112(%r13), %rdi
	movq	%rbx, %rsi
	testq	%rdi, %rdi
	je	.L730
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L731:
	testb	$1, 7(%rsi)
	jne	.L727
	movq	23(%rsi), %rbx
	movq	-1(%rbx), %rax
	cmpw	$1105, 11(%rax)
	je	.L808
.L734:
	movq	(%r15), %rax
	movq	23(%rax), %rbx
	movq	-1(%rbx), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L809
.L737:
	movq	23(%rax), %rbx
	movq	-1(%rbx), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L810
.L739:
	movq	23(%rax), %rbx
	movq	-1(%rbx), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L811
.L741:
	movq	23(%rax), %rbx
	movq	-1(%rbx), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L812
.L774:
	movq	41112(%r13), %rdi
	movq	31(%rax), %r15
	testq	%rdi, %rdi
	je	.L783
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L784:
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	cmpw	$1074, 11(%rdx)
	je	.L728
	movq	-1(%rax), %rdx
	cmpw	$93, 11(%rdx)
	jne	.L786
	movq	7(%rax), %r15
	movq	-1(%r15), %rax
	cmpw	$1074, 11(%rax)
	jne	.L727
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L787
.L798:
	movq	%r15, %rsi
	.p2align 4,,10
	.p2align 3
.L797:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L730:
	movq	41088(%r13), %r15
	cmpq	%r15, 41096(%r13)
	je	.L813
.L732:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rbx, (%r15)
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L809:
	leaq	41184(%r13), %rdi
	movl	$685, %esi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	47(%rbx), %rdx
	cmpq	%rax, %rdx
	jne	.L814
.L735:
	movq	(%r15), %rax
	movq	41112(%r13), %rdi
	movq	23(%rax), %rax
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L743
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L744:
	movq	31(%rsi), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L746
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L747:
	testb	$-128, 70(%rsi)
	jne	.L815
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17FrameArrayBuilder16AppendAsyncFrameENS0_6HandleINS0_17JSGeneratorObjectEEE
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	movq	79(%rax), %rsi
	cmpw	$1063, 11(%rdx)
	je	.L805
	cmpq	%rsi, 88(%r13)
	je	.L727
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L755
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L756:
	movq	31(%rsi), %rsi
.L805:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L797
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L816
.L760:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L728
.L813:
	movq	%r13, %rdi
	movq	%rbx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L746:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L817
.L748:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L743:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L818
.L745:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L808:
	leaq	41184(%r13), %rdi
	movl	$233, %esi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	47(%rbx), %rdx
	cmpq	%rax, %rdx
	je	.L735
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L755:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L819
.L757:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L815:
	leaq	.LC24(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L817:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L748
.L818:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L745
.L816:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L760
.L819:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L757
.L810:
	leaq	41184(%r13), %rdi
	movl	$687, %esi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	47(%rbx), %rdx
	cmpq	%rax, %rdx
	je	.L735
	movq	(%r15), %rax
	jmp	.L739
.L811:
	leaq	41184(%r13), %rdi
	movl	$518, %esi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	47(%rbx), %rdx
	cmpq	%rax, %rdx
	movq	(%r15), %rax
	jne	.L741
	movq	41112(%r13), %rdi
	movq	23(%rax), %rbx
	testq	%rdi, %rdi
	je	.L761
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rbx
	movq	%rax, %r15
.L762:
	movq	41112(%r13), %rdi
	movq	31(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L764
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L765:
	movq	(%r15), %rax
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	sarq	$32, %rax
	movq	%rax, %rdx
	subl	$1, %edx
	call	_ZN2v88internal17FrameArrayBuilder21AppendPromiseAllFrameENS0_6HandleINS0_7ContextEEEi
	movq	(%rbx), %rax
	movq	55(%rax), %r15
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L767
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
.L768:
	movq	7(%r15), %rdx
	movq	-1(%rdx), %rdx
	cmpw	$1074, 11(%rdx)
	jne	.L727
	movq	(%rax), %rax
	movq	41112(%r13), %rdi
	movq	7(%rax), %r15
	testq	%rdi, %rdi
	jne	.L798
.L787:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L820
.L789:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rbx)
	jmp	.L728
.L814:
	movq	(%r15), %rax
	jmp	.L737
.L767:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L821
.L769:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r15, (%rax)
	jmp	.L768
.L764:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L822
.L766:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L765
.L761:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L823
.L763:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rbx, (%r15)
	jmp	.L762
.L807:
	call	__stack_chk_fail@PLT
.L812:
	leaq	41184(%r13), %rdi
	movl	$497, %esi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	47(%rbx), %rdx
	cmpq	%rax, %rdx
	movq	(%r15), %rax
	jne	.L774
	movq	41112(%r13), %rdi
	movq	23(%rax), %r15
	testq	%rdi, %rdi
	je	.L775
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L776:
	movq	(%rax), %rax
	movq	41112(%r13), %rdi
	movq	31(%rax), %r15
	testq	%rdi, %rdi
	je	.L778
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L779:
	movq	(%rax), %rax
	movq	47(%rax), %r15
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L798
	jmp	.L787
.L783:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L824
.L785:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rbx)
	jmp	.L784
.L820:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rbx
	jmp	.L789
.L778:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L825
.L780:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r15, (%rax)
	jmp	.L779
.L775:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L826
.L777:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r15, (%rax)
	jmp	.L776
.L821:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L769
.L822:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L766
.L823:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r15
	jmp	.L763
.L826:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L777
.L825:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L780
.L824:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rbx
	jmp	.L785
.L786:
	movq	(%rbx), %rax
	cmpq	%rax, 88(%r13)
	je	.L727
	leaq	.LC25(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25001:
	.size	_ZN2v88internal22CaptureAsyncStackTraceEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEEPNS0_17FrameArrayBuilderE, .-_ZN2v88internal22CaptureAsyncStackTraceEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEEPNS0_17FrameArrayBuilderE
	.section	.text._ZN2v88internal12_GLOBAL__N_117CaptureStackTraceEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS1_24CaptureStackTraceOptionsE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117CaptureStackTraceEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS1_24CaptureStackTraceOptionsE, @function
_ZN2v88internal12_GLOBAL__N_117CaptureStackTraceEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS1_24CaptureStackTraceOptionsE:
.LFB25002:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%r14, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1752, %rsp
	movq	%rdi, -1736(%rbp)
	leaq	-1664(%rbp), %rdi
	movq	%rcx, -1696(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movq	%rdi, -1752(%rbp)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	leaq	-1568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1744(%rbp)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	%rbx, %rax
	movl	$1, %r9d
	movq	%r14, -1616(%rbp)
	shrq	$32, %rax
	cmpl	$1, %r15d
	movl	%ebx, -1604(%rbp)
	movl	%eax, -1608(%rbp)
	movq	%r12, -1600(%rbp)
	movw	%r9w, -1592(%rbp)
	movq	$0, -1584(%rbp)
	sete	-1590(%rbp)
	cmpl	$2, %eax
	jne	.L828
	movb	$0, -1592(%rbp)
.L828:
	cmpl	$10, %ebx
	movl	$10, %esi
	cmovle	%ebx, %esi
	movq	-1736(%rbp), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory13NewFrameArrayEiNS0_14AllocationTypeE@PLT
	movq	%rbx, %rsi
	movq	%rax, -1584(%rbp)
	leaq	-1504(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1688(%rbp)
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L829
	movq	-1696(%rbp), %rax
	shrq	$40, %rax
	movq	%rax, -1728(%rbp)
	.p2align 4,,10
	.p2align 3
.L911:
	movq	-1584(%rbp), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rax
	sarq	$32, %rax
	cmpl	%eax, -1604(%rbp)
	jle	.L829
	movq	-88(%rbp), %r13
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	subl	$4, %eax
	cmpl	$17, %eax
	ja	.L830
	leaq	.L832(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12_GLOBAL__N_117CaptureStackTraceEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS1_24CaptureStackTraceOptionsE,"a",@progbits
	.align 4
	.align 4
.L832:
	.long	.L833-.L832
	.long	.L833-.L832
	.long	.L830-.L832
	.long	.L830-.L832
	.long	.L833-.L832
	.long	.L830-.L832
	.long	.L830-.L832
	.long	.L830-.L832
	.long	.L833-.L832
	.long	.L830-.L832
	.long	.L830-.L832
	.long	.L833-.L832
	.long	.L833-.L832
	.long	.L830-.L832
	.long	.L830-.L832
	.long	.L830-.L832
	.long	.L833-.L832
	.long	.L831-.L832
	.section	.text._ZN2v88internal12_GLOBAL__N_117CaptureStackTraceEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS1_24CaptureStackTraceOptionsE
	.p2align 4,,10
	.p2align 3
.L833:
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	$0, -1632(%rbp)
	leaq	-1648(%rbp), %rsi
	movaps	%xmm0, -1648(%rbp)
	movq	0(%r13), %rax
	call	*136(%rax)
	movq	-1640(%rbp), %r15
	movq	-1648(%rbp), %r13
	movabsq	$7905747460161236407, %rax
	movq	%r15, %r10
	movq	%r13, %r12
	subq	%r13, %r10
	movq	%r10, %r14
	leaq	-56(%r10), %rbx
	sarq	$3, %r14
	imulq	%rax, %r14
	testq	%r14, %r14
	je	.L837
	movq	-1728(%rbp), %r12
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L991:
	movl	8(%r13), %eax
	testl	%eax, %eax
	je	.L987
.L978:
	cmpl	$1, %eax
	je	.L988
	cmpl	$2, %eax
	je	.L989
.L864:
	subq	$56, %rbx
	subq	$1, %r14
	je	.L977
.L995:
	movq	-1648(%rbp), %r13
.L838:
	movq	-1584(%rbp), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rax
	sarq	$32, %rax
	cmpl	%eax, -1604(%rbp)
	jle	.L990
	addq	%rbx, %r13
	testb	%r12b, %r12b
	je	.L991
	movq	%r13, %rdi
	call	_ZNK2v88internal12FrameSummary23is_subject_to_debuggingEv@PLT
	testb	%al, %al
	je	.L864
	movl	8(%r13), %eax
	testl	%eax, %eax
	jne	.L978
.L987:
	movl	-1608(%rbp), %eax
	movq	24(%r13), %r15
	cmpl	$1, %eax
	je	.L848
	cmpl	$2, %eax
	jne	.L992
.L855:
	cmpb	$0, _ZN2v88internal29FLAG_builtins_in_stack_tracesE(%rip)
	je	.L993
.L862:
	cmpb	$0, -1590(%rbp)
	je	.L865
	movq	-1616(%rbp), %rax
	movq	12464(%rax), %rdx
	movq	(%r15), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %rax
	movq	1151(%rax), %rax
	movq	39(%rdx), %rdx
	movq	1151(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L864
.L865:
	cmpb	$0, -1591(%rbp)
	movq	32(%r13), %rcx
	movl	$24, %eax
	movl	$8, %r9d
	movl	40(%r13), %r8d
	movzbl	44(%r13), %esi
	movq	24(%r13), %r10
	jne	.L867
	movq	(%r10), %rax
	movq	23(%rax), %rax
	movl	47(%rax), %edx
	andl	$64, %edx
	setne	-1591(%rbp)
	cmpl	$1, %edx
	sbbl	%eax, %eax
	andl	$-8, %eax
	addl	$24, %eax
	cmpl	$1, %edx
	sbbl	%r9d, %r9d
	notl	%r9d
	andl	$8, %r9d
.L867:
	testb	%sil, %sil
	cmovne	%eax, %r9d
	movq	-1616(%rbp), %rax
	cmpb	$0, _ZN2v88internal31FLAG_detailed_error_stack_traceE(%rip)
	leaq	288(%rax), %rdx
	jne	.L994
.L869:
	movq	16(%r13), %rsi
	movq	96(%rax), %rdi
	cmpq	%rdi, (%rsi)
	jne	.L870
	leaq	88(%rax), %rsi
.L870:
	subq	$8, %rsp
	movq	-1584(%rbp), %rdi
	subq	$56, %rbx
	pushq	%rdx
	movq	%r10, %rdx
	call	_ZN2v88internal10FrameArray13AppendJSFrameENS0_6HandleIS1_EENS2_INS0_6ObjectEEENS2_INS0_10JSFunctionEEENS2_INS0_12AbstractCodeEEEiiNS2_INS0_10FixedArrayEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, -1584(%rbp)
	subq	$1, %r14
	jne	.L995
	.p2align 4,,10
	.p2align 3
.L977:
	movq	-1640(%rbp), %r15
	movq	-1648(%rbp), %r12
.L837:
	cmpq	%r12, %r15
	je	.L840
	.p2align 4,,10
	.p2align 3
.L841:
	movq	%r12, %rdi
	addq	$56, %r12
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%r15, %r12
	jne	.L841
	movq	-1648(%rbp), %r12
.L840:
	testq	%r12, %r12
	je	.L830
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L830:
	movq	-1688(%rbp), %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	cmpq	$0, -88(%rbp)
	jne	.L911
.L829:
	movq	-1696(%rbp), %rax
	leaq	-1616(%rbp), %r12
	shrq	$48, %rax
	testb	%al, %al
	jne	.L996
.L835:
	movq	-1696(%rbp), %rax
	movq	%r12, %rdi
	shrq	$56, %rax
	movq	%rax, %rsi
	call	_ZN2v88internal17FrameArrayBuilder33GetElementsAsStackTraceFrameArrayEb
	movq	-1744(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	-1752(%rbp), %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L997
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L831:
	.cfi_restore_state
	movq	-1696(%rbp), %rax
	shrq	$32, %rax
	testb	%al, %al
	je	.L830
	movq	-1616(%rbp), %r14
	movq	%r13, %rdi
	call	_ZNK2v88internal16BuiltinExitFrame8functionEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L873
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -1704(%rbp)
	movl	-1608(%rbp), %eax
	cmpl	$1, %eax
	je	.L876
.L1007:
	cmpl	$2, %eax
	jne	.L998
.L882:
	cmpb	$0, _ZN2v88internal29FLAG_builtins_in_stack_tracesE(%rip)
	je	.L999
.L889:
	cmpb	$0, -1590(%rbp)
	jne	.L1000
.L892:
	cmpb	$0, _ZN2v88internal36FLAG_experimental_stack_trace_framesE(%rip)
	jne	.L893
	movq	-1704(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L1001
.L893:
	movq	-1616(%rbp), %r15
	movq	%r13, %rdi
	call	_ZNK2v88internal16BuiltinExitFrame8receiverEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L894
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -1712(%rbp)
.L895:
	movq	-1616(%rbp), %r15
	movq	%r13, %rdi
	call	_ZNK2v88internal10StackFrame10LookupCodeEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L897
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L898:
	movq	40(%r13), %rax
	movq	(%rax), %r15
	movq	%rsi, -1648(%rbp)
	leaq	63(%rsi), %rax
	movl	43(%rsi), %ecx
	testl	%ecx, %ecx
	js	.L1002
.L901:
	movl	%r15d, %edx
	subl	%eax, %edx
	cmpb	$0, -1591(%rbp)
	movl	%edx, -1720(%rbp)
	je	.L902
.L904:
	movl	$24, %r15d
	movl	$8, %r9d
.L903:
	movq	%r13, %rdi
	movl	%r9d, -1760(%rbp)
	call	_ZNK2v88internal16BuiltinExitFrame13IsConstructorEv@PLT
	movl	-1760(%rbp), %r9d
	testb	%al, %al
	movq	-1616(%rbp), %rax
	cmovne	%r15d, %r9d
	cmpb	$0, _ZN2v88internal31FLAG_detailed_error_stack_traceE(%rip)
	leaq	288(%rax), %rcx
	jne	.L1003
.L906:
	subq	$8, %rsp
	movq	-1704(%rbp), %rdx
	movl	-1720(%rbp), %r8d
	movq	-1712(%rbp), %rsi
	movq	-1584(%rbp), %rdi
	pushq	%rcx
	movq	%r14, %rcx
	call	_ZN2v88internal10FrameArray13AppendJSFrameENS0_6HandleIS1_EENS2_INS0_6ObjectEEENS2_INS0_10JSFunctionEEENS2_INS0_12AbstractCodeEEEiiNS2_INS0_10FixedArrayEEE@PLT
	movq	%rax, -1584(%rbp)
	popq	%rax
	popq	%rdx
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L992:
	testl	%eax, %eax
	je	.L1004
.L850:
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L988:
	movq	32(%r13), %rcx
	movl	60(%rcx), %esi
	testl	%esi, %esi
	jne	.L864
	movq	16(%r13), %rsi
	movl	$1, %r15d
	movq	(%rsi), %rax
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	cmpb	$0, 392(%rax)
	je	.L872
	cmpb	$1, 24(%r13)
	sbbl	%r15d, %r15d
	andl	$-32, %r15d
	addl	$36, %r15d
.L872:
	movl	40(%r13), %r8d
	movq	%r13, %rdi
	movq	%rsi, -1720(%rbp)
	movq	%rcx, -1712(%rbp)
	movl	%r8d, -1704(%rbp)
	call	_ZNK2v88internal12FrameSummary24WasmCompiledFrameSummary14function_indexEv@PLT
	movl	-1704(%rbp), %r8d
	movl	%r15d, %r9d
	movq	-1712(%rbp), %rcx
	movq	-1720(%rbp), %rsi
	movq	-1584(%rbp), %rdi
	movl	%eax, %edx
	call	_ZN2v88internal10FrameArray15AppendWasmFrameENS0_6HandleIS1_EENS2_INS0_18WasmInstanceObjectEEEiPNS0_4wasm8WasmCodeEii@PLT
	movq	%rax, -1584(%rbp)
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L848:
	cmpb	$0, -1592(%rbp)
	je	.L855
	movq	-1600(%rbp), %rax
	movq	(%r15), %rdx
	cmpq	%rdx, (%rax)
	jne	.L864
	movb	$0, -1592(%rbp)
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L1004:
	cmpb	$0, -1592(%rbp)
	je	.L855
	movb	$0, -1592(%rbp)
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L993:
	movq	(%r15), %rdx
	movq	23(%rdx), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1005
.L857:
	leaq	-1672(%rbp), %rdi
	movq	%rax, -1672(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L862
	movq	(%r15), %rdx
	movq	23(%rdx), %rax
	movl	47(%rax), %eax
	testb	$32, %al
	jne	.L862
.L1013:
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	je	.L864
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L864
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L989:
	movq	16(%r13), %rsi
	movl	28(%r13), %edx
	movl	$2, %r9d
	xorl	%ecx, %ecx
	movl	32(%r13), %r8d
	movq	-1584(%rbp), %rdi
	call	_ZN2v88internal10FrameArray15AppendWasmFrameENS0_6HandleIS1_EENS2_INS0_18WasmInstanceObjectEEEiPNS0_4wasm8WasmCodeEii@PLT
	movq	%rax, -1584(%rbp)
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L998:
	testl	%eax, %eax
	jne	.L850
	cmpb	$0, -1592(%rbp)
	je	.L882
.L880:
	movb	$0, -1592(%rbp)
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L990:
	movq	-1640(%rbp), %r15
	movq	%r13, %r12
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L873:
	movq	41088(%r14), %rax
	movq	%rax, -1704(%rbp)
	cmpq	41096(%r14), %rax
	je	.L1006
.L875:
	movq	-1704(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	movl	-1608(%rbp), %eax
	cmpl	$1, %eax
	jne	.L1007
.L876:
	cmpb	$0, -1592(%rbp)
	je	.L882
	movq	-1704(%rbp), %rdx
	movq	-1600(%rbp), %rax
	movq	(%rdx), %rdx
	cmpq	%rdx, (%rax)
	jne	.L830
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L996:
	movq	-1736(%rbp), %rax
	leaq	-1616(%rbp), %r12
	movq	4752(%rax), %rax
	testb	$1, %al
	je	.L835
	movq	-1(%rax), %rdx
	cmpw	$112, 11(%rdx)
	je	.L913
	movq	-1(%rax), %rdx
	cmpw	$113, 11(%rdx)
	jne	.L835
.L913:
	movq	23(%rax), %rbx
	movq	-1(%rbx), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L1008
.L916:
	movq	23(%rax), %rbx
	movq	-1(%rbx), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L1009
.L919:
	movq	23(%rax), %rbx
	movq	-1(%rbx), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L1010
.L921:
	movq	31(%rax), %r12
	movq	-1736(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1011
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %rsi
.L941:
	movq	-1(%r12), %rax
	leaq	-1616(%rbp), %r12
	cmpw	$1074, 11(%rax)
	jne	.L835
	movq	-1736(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal22CaptureAsyncStackTraceEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEEPNS0_17FrameArrayBuilderE
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	je	.L1012
.L858:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	%rax, -37504(%rcx)
	jne	.L857
	movq	23(%rdx), %rax
	movl	47(%rax), %eax
	testb	$32, %al
	jne	.L862
	jmp	.L1013
.L902:
	movq	-1704(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movl	47(%rax), %eax
	andl	$64, %eax
	setne	-1591(%rbp)
	jne	.L904
	movl	$16, %r15d
	xorl	%r9d, %r9d
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L897:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L1014
.L899:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L894:
	movq	41088(%r15), %rax
	movq	%rax, -1712(%rbp)
	cmpq	41096(%r15), %rax
	je	.L1015
.L896:
	movq	-1712(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L994:
	movq	48(%r13), %rdx
	jmp	.L869
.L999:
	movq	-1704(%rbp), %rax
	movq	(%rax), %rdx
	movq	23(%rdx), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1016
.L884:
	leaq	-1648(%rbp), %rdi
	movq	%rax, -1648(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L889
	movq	-1704(%rbp), %rax
	movq	(%rax), %rdx
.L886:
	movq	23(%rdx), %rax
	movl	47(%rax), %eax
	testb	$32, %al
	jne	.L889
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	je	.L830
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L830
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L1000:
	movq	-1616(%rbp), %rax
	movq	12464(%rax), %rdx
	movq	-1704(%rbp), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %rax
	movq	1151(%rax), %rax
	movq	39(%rdx), %rdx
	movq	1151(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L830
	jmp	.L892
.L1012:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L858
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L1002:
	leaq	-1648(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L901
.L1001:
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L893
	jmp	.L830
.L1006:
	movq	%r14, %rdi
	movq	%rsi, -1712(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1712(%rbp), %rsi
	movq	%rax, -1704(%rbp)
	jmp	.L875
.L1003:
	movq	%r13, %rdi
	movl	%r9d, -1760(%rbp)
	call	_ZNK2v88internal16BuiltinExitFrame22ComputeParametersCountEv@PLT
	movq	-1616(%rbp), %rdi
	xorl	%edx, %edx
	movl	%eax, %r15d
	movl	%eax, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	testl	%r15d, %r15d
	movl	-1760(%rbp), %r9d
	movq	%rax, %rcx
	jle	.L906
	xorl	%r10d, %r10d
	movq	%r14, -1784(%rbp)
	leal	-1(%r15), %r12d
	movq	%rax, %rbx
	movl	%r9d, -1788(%rbp)
	movq	%r10, %r14
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L948:
	movq	%rax, %r14
.L910:
	movq	(%rbx), %r15
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal16BuiltinExitFrame12GetParameterEi@PLT
	leaq	15(%r15,%r14,8), %rsi
	movq	%rax, %rdx
	movq	%rax, (%rsi)
	testb	$1, %al
	je	.L943
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -1760(%rbp)
	testl	$262144, %eax
	je	.L908
	movq	%r15, %rdi
	movq	%rdx, -1776(%rbp)
	movq	%rsi, -1768(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-1760(%rbp), %rcx
	movq	-1776(%rbp), %rdx
	movq	-1768(%rbp), %rsi
	movq	8(%rcx), %rax
.L908:
	testb	$24, %al
	je	.L943
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L943
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L943:
	leaq	1(%r14), %rax
	cmpq	%r12, %r14
	jne	.L948
	movq	-1784(%rbp), %r14
	movl	-1788(%rbp), %r9d
	movq	%rbx, %rcx
	jmp	.L906
.L1015:
	movq	%r15, %rdi
	movq	%rsi, -1720(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1720(%rbp), %rsi
	movq	%rax, -1712(%rbp)
	jmp	.L896
.L1014:
	movq	%r15, %rdi
	movq	%rax, -1720(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1720(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L899
.L1009:
	movq	-1736(%rbp), %rax
	movl	$685, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	47(%rbx), %rdx
	cmpq	%rax, %rdx
	jne	.L1017
.L917:
	movq	-1736(%rbp), %rdx
	movq	4752(%rdx), %rax
	movq	41112(%rdx), %rdi
	movq	23(%rax), %rax
	movq	31(%rax), %r12
	testq	%rdi, %rdi
	je	.L924
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
.L925:
	movq	31(%r12), %r13
	movq	-1736(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L927
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L928:
	cmpl	$-2, 67(%r13)
	leaq	-1616(%rbp), %r12
	jne	.L835
	movq	-1(%r13), %rdx
	movq	(%rax), %rax
	cmpw	$1063, 11(%rdx)
	je	.L1018
	movq	79(%rax), %r13
	movq	-1736(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L935
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L936:
	movq	-1736(%rbp), %rax
	leaq	-1616(%rbp), %r12
	cmpq	%r13, 88(%rax)
	je	.L835
	movq	41112(%rax), %rdi
	movq	31(%r13), %r12
	testq	%rdi, %rdi
	je	.L938
.L982:
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L939:
	leaq	-1616(%rbp), %r12
	movq	-1736(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal22CaptureAsyncStackTraceEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEEPNS0_17FrameArrayBuilderE
	jmp	.L835
.L1016:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	je	.L1019
.L885:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	%rax, -37504(%rcx)
	je	.L886
	jmp	.L884
.L927:
	movq	%rax, %rdx
	movq	41088(%rax), %rax
	cmpq	41096(%rdx), %rax
	je	.L1020
.L929:
	movq	-1736(%rbp), %rbx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	jmp	.L928
.L924:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1021
.L926:
	movq	-1736(%rbp), %rbx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r12, (%rax)
	jmp	.L925
.L1008:
	movq	-1736(%rbp), %rax
	movl	$233, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	47(%rbx), %rdx
	cmpq	%rax, %rdx
	je	.L917
	movq	-1736(%rbp), %rax
	movq	4752(%rax), %rax
	jmp	.L916
.L1019:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L885
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	79(%rax), %r12
	movq	-1736(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L982
.L938:
	movq	41088(%rax), %rsi
	cmpq	41096(%rax), %rsi
	je	.L1022
.L940:
	movq	-1736(%rbp), %rdx
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r12, (%rsi)
	jmp	.L939
.L935:
	movq	%rax, %rdx
	movq	41088(%rax), %rax
	cmpq	41096(%rdx), %rax
	je	.L1023
.L937:
	movq	-1736(%rbp), %rbx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	jmp	.L936
.L1021:
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L926
.L1020:
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L929
.L1010:
	movq	-1736(%rbp), %rax
	movl	$687, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	47(%rbx), %rdx
	cmpq	%rax, %rdx
	je	.L917
	movq	-1736(%rbp), %rax
	movq	4752(%rax), %rax
	jmp	.L921
.L1011:
	movq	41088(%rax), %rsi
	cmpq	41096(%rax), %rsi
	je	.L1024
.L942:
	movq	-1736(%rbp), %rdx
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r12, (%rsi)
	jmp	.L941
.L1022:
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L940
.L1017:
	movq	-1736(%rbp), %rax
	movq	4752(%rax), %rax
	jmp	.L919
.L1024:
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L942
.L997:
	call	__stack_chk_fail@PLT
.L1023:
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L937
	.cfi_endproc
.LFE25002:
	.size	_ZN2v88internal12_GLOBAL__N_117CaptureStackTraceEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS1_24CaptureStackTraceOptionsE, .-_ZN2v88internal12_GLOBAL__N_117CaptureStackTraceEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS1_24CaptureStackTraceOptionsE
	.section	.text._ZN2v88internal7Isolate23CaptureSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate23CaptureSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal7Isolate23CaptureSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE, @function
_ZN2v88internal7Isolate23CaptureSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE:
.LFB25012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	leaq	-44(%rbp), %rsi
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18GetStackTraceLimitEPNS0_7IsolateEPi
	testb	%al, %al
	jne	.L1026
	leaq	88(%r12), %rax
.L1027:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1030
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1026:
	.cfi_restore_state
	movq	$0, -64(%rbp)
	movq	-64(%rbp), %rcx
	salq	$32, %rbx
	movq	%r12, %rdi
	movl	-44(%rbp), %eax
	movq	$0, -56(%rbp)
	movabsq	$-4294967296, %rsi
	andq	%rsi, %rcx
	movzbl	_ZN2v88internal23FLAG_async_stack_tracesE(%rip), %edx
	orq	%rax, %rcx
	movq	-56(%rbp), %rax
	movl	%ecx, %ecx
	salq	$48, %rdx
	orq	%rbx, %rcx
	movq	%rcx, -64(%rbp)
	movabsq	$-71776119061217281, %rcx
	andq	%rcx, %rax
	orq	%rdx, %rax
	andq	%rsi, %rax
	movq	%r13, %rsi
	orq	$1, %rax
	movq	%rax, -56(%rbp)
	movdqa	-64(%rbp), %xmm0
	movl	$1, %eax
	pinsrw	$6, %eax, %xmm0
	movabsq	$72057594037927935, %rax
	movaps	%xmm0, -80(%rbp)
	movq	-72(%rbp), %rdx
	movdqa	-80(%rbp), %xmm1
	andq	%rax, %rdx
	movaps	%xmm1, -64(%rbp)
	movq	%rdx, -56(%rbp)
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rcx
	call	_ZN2v88internal12_GLOBAL__N_117CaptureStackTraceEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS1_24CaptureStackTraceOptionsE
	jmp	.L1027
.L1030:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25012:
	.size	_ZN2v88internal7Isolate23CaptureSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE, .-_ZN2v88internal7Isolate23CaptureSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal7Isolate31CaptureAndSetDetailedStackTraceENS0_6HandleINS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate31CaptureAndSetDetailedStackTraceENS0_6HandleINS0_10JSReceiverEEE
	.type	_ZN2v88internal7Isolate31CaptureAndSetDetailedStackTraceENS0_6HandleINS0_10JSReceiverEEE, @function
_ZN2v88internal7Isolate31CaptureAndSetDetailedStackTraceENS0_6HandleINS0_10JSReceiverEEE:
.LFB25013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 41064(%rdi)
	jne	.L1036
.L1032:
	movq	%rbx, %rax
.L1034:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1036:
	.cfi_restore_state
	movl	41068(%rdi), %eax
	movl	$0, %edx
	leaq	88(%rdi), %rsi
	movq	%rdi, %r12
	leaq	3664(%rdi), %r13
	testl	%eax, %eax
	cmovns	41068(%rdi), %edx
	xorl	%ecx, %ecx
	movabsq	$72058693549555712, %rax
	movl	%edx, %edx
	btsq	$33, %rdx
	testb	$1, 41073(%rdi)
	sete	%cl
	orq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_117CaptureStackTraceEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS1_24CaptureStackTraceOptionsE
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	jne	.L1032
	jmp	.L1034
	.cfi_endproc
.LFE25013:
	.size	_ZN2v88internal7Isolate31CaptureAndSetDetailedStackTraceENS0_6HandleINS0_10JSReceiverEEE, .-_ZN2v88internal7Isolate31CaptureAndSetDetailedStackTraceENS0_6HandleINS0_10JSReceiverEEE
	.section	.text._ZN2v88internal7Isolate29CaptureAndSetSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate29CaptureAndSetSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal7Isolate29CaptureAndSetSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE, @function
_ZN2v88internal7Isolate29CaptureAndSetSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE:
.LFB25017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	3808(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	-60(%rbp), %rsi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18GetStackTraceLimitEPNS0_7IsolateEPi
	leaq	88(%r12), %rcx
	testb	%al, %al
	jne	.L1044
.L1039:
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movl	$1, %r9d
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	movl	$0, %eax
	cmovne	%rbx, %rax
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1045
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1044:
	.cfi_restore_state
	movq	$0, -80(%rbp)
	movq	-80(%rbp), %rcx
	salq	$32, %r13
	movq	%r12, %rdi
	movl	-60(%rbp), %eax
	movabsq	$-4294967296, %rdx
	movq	$0, -72(%rbp)
	movabsq	$-71776119061217281, %rsi
	andq	%rdx, %rcx
	orq	%rax, %rcx
	movzbl	_ZN2v88internal23FLAG_async_stack_tracesE(%rip), %eax
	movl	%ecx, %ecx
	orq	%r13, %rcx
	salq	$48, %rax
	movq	%rcx, -80(%rbp)
	movq	%rax, %rcx
	movq	-72(%rbp), %rax
	andq	%rsi, %rax
	movq	%r14, %rsi
	orq	%rcx, %rax
	andq	%rdx, %rax
	orq	$1, %rax
	movq	%rax, -72(%rbp)
	movdqa	-80(%rbp), %xmm0
	movl	$1, %eax
	pinsrw	$6, %eax, %xmm0
	movabsq	$72057594037927935, %rax
	movaps	%xmm0, -96(%rbp)
	movq	-88(%rbp), %rdx
	movdqa	-96(%rbp), %xmm1
	andq	%rax, %rdx
	movaps	%xmm1, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rcx
	call	_ZN2v88internal12_GLOBAL__N_117CaptureStackTraceEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS1_24CaptureStackTraceOptionsE
	movq	%rax, %rcx
	jmp	.L1039
.L1045:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25017:
	.size	_ZN2v88internal7Isolate29CaptureAndSetSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE, .-_ZN2v88internal7Isolate29CaptureAndSetSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal7Isolate21GetDetailedStackTraceENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate21GetDetailedStackTraceENS0_6HandleINS0_8JSObjectEEE
	.type	_ZN2v88internal7Isolate21GetDetailedStackTraceENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal7Isolate21GetDetailedStackTraceENS0_6HandleINS0_8JSObjectEEE:
.LFB25018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	3664(%rdi), %rsi
	subq	$96, %rsp
	movq	(%rbx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	3664(%rdi), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %r8
	movl	$2, %edx
	subq	$37592, %r8
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L1047
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
.L1047:
	movabsq	$824633720832, %rax
	movl	%edx, -112(%rbp)
	movq	%rax, -100(%rbp)
	movq	3664(%rdi), %rax
	movq	%r8, -88(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L1059
.L1048:
	leaq	-112(%rbp), %r12
	movq	%rsi, -80(%rbp)
	movq	%r12, %rdi
	movq	$0, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	%rbx, -48(%rbp)
	movq	$-1, -40(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -108(%rbp)
	jne	.L1049
	movq	-88(%rbp), %rax
	addq	$88, %rax
.L1050:
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L1051
.L1053:
	xorl	%eax, %eax
.L1052:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1060
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1051:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	movzwl	11(%rdx), %edx
	subl	$123, %edx
	cmpw	$14, %dx
	ja	.L1053
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	%r8, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L1048
.L1060:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25018:
	.size	_ZN2v88internal7Isolate21GetDetailedStackTraceENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal7Isolate21GetDetailedStackTraceENS0_6HandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal7Isolate13GetAbstractPCEPiS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate13GetAbstractPCEPiS2_
	.type	_ZN2v88internal7Isolate13GetAbstractPCEPiS2_, @function
_ZN2v88internal7Isolate13GetAbstractPCEPiS2_:
.LFB25019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1504(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$1496, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L1064
	movq	%r12, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L1064
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*152(%rax)
	movq	23(%rax), %r15
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1065
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1066:
	movq	%rbx, %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*104(%rax)
	movq	%r12, %rdi
	movl	%eax, %r15d
	movq	(%r12), %rax
	call	*152(%rax)
	movq	23(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L1083
.L1077:
	movl	%r15d, (%r14)
	movl	$-1, 0(%r13)
.L1074:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	cmpl	$12, %eax
	je	.L1084
	movq	40(%r12), %rax
	movq	(%rax), %rax
.L1061:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1085
	addq	$1496, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1065:
	.cfi_restore_state
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L1086
.L1067:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r15, (%rsi)
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1064:
	movl	$-1, (%r14)
	xorl	%eax, %eax
	movl	$-1, 0(%r13)
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	-1(%rsi), %rdx
	leaq	-1(%rsi), %rax
	cmpw	$86, 11(%rdx)
	je	.L1087
.L1076:
	movq	(%rax), %rax
	cmpw	$96, 11(%rax)
	jne	.L1077
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1071
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L1072:
	pcmpeqd	%xmm0, %xmm0
	leaq	-1520(%rbp), %rdx
	movl	$1, %ecx
	movl	%r15d, %esi
	movaps	%xmm0, -1520(%rbp)
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	movl	-1520(%rbp), %eax
	addl	$1, %eax
	movl	%eax, (%r14)
	movl	-1516(%rbp), %eax
	addl	$1, %eax
	movl	%eax, 0(%r13)
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1087:
	movq	23(%rsi), %rsi
	testb	$1, %sil
	je	.L1077
	leaq	-1(%rsi), %rax
	jmp	.L1076
	.p2align 4,,10
	.p2align 3
.L1084:
	movq	%r12, %rdi
	call	_ZNK2v88internal16InterpretedFrame16GetBytecodeArrayEv@PLT
	movq	%r12, %rdi
	leaq	53(%rax), %rbx
	call	_ZNK2v88internal16InterpretedFrame17GetBytecodeOffsetEv@PLT
	cltq
	addq	%rbx, %rax
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1086:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1071:
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L1088
.L1073:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L1072
.L1088:
	movq	%rbx, %rdi
	movq	%rsi, -1528(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1528(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1073
.L1085:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25019:
	.size	_ZN2v88internal7Isolate13GetAbstractPCEPiS2_, .-_ZN2v88internal7Isolate13GetAbstractPCEPiS2_
	.section	.text._ZN2v88internal7Isolate24CaptureCurrentStackTraceEiNS_10StackTrace17StackTraceOptionsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate24CaptureCurrentStackTraceEiNS_10StackTrace17StackTraceOptionsE
	.type	_ZN2v88internal7Isolate24CaptureCurrentStackTraceEiNS_10StackTrace17StackTraceOptionsE, @function
_ZN2v88internal7Isolate24CaptureCurrentStackTraceEiNS_10StackTrace17StackTraceOptionsE:
.LFB25023:
	.cfi_startproc
	endbr64
	movabsq	$72058693549555712, %rax
	testl	%esi, %esi
	movl	%edx, %r8d
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$0, %edx
	cmovns	%esi, %edx
	xorl	%ecx, %ecx
	leaq	88(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edx, %edx
	btsq	$33, %rdx
	andl	$256, %r8d
	sete	%cl
	orq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_117CaptureStackTraceEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS1_24CaptureStackTraceOptionsE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25023:
	.size	_ZN2v88internal7Isolate24CaptureCurrentStackTraceEiNS_10StackTrace17StackTraceOptionsE, .-_ZN2v88internal7Isolate24CaptureCurrentStackTraceEiNS_10StackTrace17StackTraceOptionsE
	.section	.rodata._ZN2v88internal7Isolate10PrintStackEPNS0_12StringStreamENS1_14PrintStackModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"\n==== JS stack trace =========================================\n\n"
	.align 8
.LC27:
	.string	"\n==== Details ================================================\n\n"
	.section	.rodata._ZN2v88internal7Isolate10PrintStackEPNS0_12StringStreamENS1_14PrintStackModeE.str1.1,"aMS",@progbits,1
.LC28:
	.string	"=====================\n\n"
	.section	.text._ZN2v88internal7Isolate10PrintStackEPNS0_12StringStreamENS1_14PrintStackModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate10PrintStackEPNS0_12StringStreamENS1_14PrintStackModeE
	.type	_ZN2v88internal7Isolate10PrintStackEPNS0_12StringStreamENS1_14PrintStackModeE, @function
_ZN2v88internal7Isolate10PrintStackEPNS0_12StringStreamENS1_14PrintStackModeE:
.LFB25026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movl	%edx, -132(%rbp)
	movq	41088(%rdi), %rbx
	movq	41096(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdi)
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	cmpq	$0, 12560(%r15)
	je	.L1101
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	movl	$64, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internalL11PrintFramesEPNS0_7IsolateEPNS0_12StringStreamENS0_10StackFrame9PrintModeE
	cmpl	$1, -132(%rbp)
	je	.L1102
.L1095:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC28(%rip), %rsi
	movq	%r12, %rdi
	movl	$23, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
.L1101:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	subl	$1, 41104(%r15)
	movq	%rbx, 41088(%r15)
	cmpq	41096(%r15), %r14
	je	.L1091
	movq	%r14, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1091:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1103
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1102:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$64, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$1, %edx
	call	_ZN2v88internalL11PrintFramesEPNS0_7IsolateEPNS0_12StringStreamENS0_10StackFrame9PrintModeE
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream25PrintMentionedObjectCacheEPNS0_7IsolateE@PLT
	jmp	.L1095
.L1103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25026:
	.size	_ZN2v88internal7Isolate10PrintStackEPNS0_12StringStreamENS1_14PrintStackModeE, .-_ZN2v88internal7Isolate10PrintStackEPNS0_12StringStreamENS1_14PrintStackModeE
	.section	.rodata._ZN2v88internal7Isolate16StackTraceStringEv.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"\n\nAttempt to print stack while printing stack (double fault)\n"
	.align 8
.LC30:
	.string	"If you are lucky you may find a partial stack dump on stdout.\n\n"
	.section	.text._ZN2v88internal7Isolate16StackTraceStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate16StackTraceStringEv
	.type	_ZN2v88internal7Isolate16StackTraceStringEv, @function
_ZN2v88internal7Isolate16StackTraceStringEv:
.LFB24966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	40816(%rdi), %eax
	testl	%eax, %eax
	je	.L1114
	cmpl	$1, %eax
	jne	.L1108
	movl	$2, 40816(%rdi)
	xorl	%eax, %eax
	leaq	.LC29(%rip), %rdi
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	xorl	%eax, %eax
	leaq	.LC30(%rip), %rdi
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	40824(%rbx), %rdi
	movq	stdout(%rip), %rsi
	call	_ZN2v88internal12StringStream12OutputToFileEP8_IO_FILE@PLT
	leaq	128(%rbx), %rax
.L1107:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1115
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1114:
	.cfi_restore_state
	movl	$1, 40816(%rdi)
	leaq	16+_ZTVN2v88internal19HeapStringAllocatorE(%rip), %r13
	leaq	-80(%rbp), %r12
	movq	%r13, -96(%rbp)
	call	_ZN2v88internal12StringStream25ClearMentionedObjectCacheEPNS0_7IsolateE@PLT
	leaq	-96(%rbp), %rdi
	movabsq	$68719476737, %rax
	movl	$16, %esi
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal19HeapStringAllocator8allocateEj@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	movb	$0, (%rax)
	movq	%r12, 40824(%rbx)
	call	_ZN2v88internal7Isolate10PrintStackEPNS0_12StringStreamENS1_14PrintStackModeE
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal12StringStream8ToStringEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rdi
	movq	%r13, -96(%rbp)
	movq	$0, 40824(%rbx)
	movl	$0, 40816(%rbx)
	testq	%rdi, %rdi
	je	.L1107
	movq	%rax, -104(%rbp)
	call	_ZdaPv@PLT
	movq	-104(%rbp), %rax
	jmp	.L1107
.L1115:
	call	__stack_chk_fail@PLT
.L1108:
	call	_ZN2v84base2OS5AbortEv@PLT
	.cfi_endproc
.LFE24966:
	.size	_ZN2v88internal7Isolate16StackTraceStringEv, .-_ZN2v88internal7Isolate16StackTraceStringEv
	.section	.text._ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE.part.0, @function
_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE.part.0:
.LFB35660:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	16+_ZTVN2v88internal19HeapStringAllocatorE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, 40816(%rdi)
	call	_ZN2v88internal12StringStream25ClearMentionedObjectCacheEPNS0_7IsolateE@PLT
	leaq	-112(%rbp), %rdi
	movq	%r13, -112(%rbp)
	movabsq	$68719476737, %rax
	movl	$16, %esi
	movq	%rdi, -96(%rbp)
	movq	%rax, -88(%rbp)
	movl	$0, -80(%rbp)
	call	_ZN2v88internal19HeapStringAllocator8allocateEj@PLT
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	movb	$0, (%rax)
	movq	%r12, 40824(%rbx)
	call	_ZN2v88internal7Isolate10PrintStackEPNS0_12StringStreamENS1_14PrintStackModeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream12OutputToFileEP8_IO_FILE@PLT
	cmpq	$0, 41016(%rbx)
	je	.L1125
.L1117:
	cmpq	$0, 40960(%rbx)
	je	.L1126
.L1118:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal12StringStream3LogEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rdi
	movq	%r13, -112(%rbp)
	movq	$0, 40824(%rbx)
	movl	$0, 40816(%rbx)
	testq	%rdi, %rdi
	je	.L1116
	call	_ZdaPv@PLT
.L1116:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1127
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1126:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate18InitializeCountersEv.part.0
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1125:
	movl	$168, %edi
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN2v88internal6LoggerC1EPNS0_7IsolateE@PLT
	movq	%r14, 41016(%rbx)
	jmp	.L1117
.L1127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE35660:
	.size	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE.part.0, .-_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE.part.0
	.section	.text._ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE
	.type	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE, @function
_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE:
.LFB25024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	40816(%rdi), %eax
	testl	%eax, %eax
	je	.L1132
	cmpl	$1, %eax
	je	.L1133
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1132:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE.part.0
	.p2align 4,,10
	.p2align 3
.L1133:
	.cfi_restore_state
	movl	$2, 40816(%rdi)
	xorl	%eax, %eax
	leaq	.LC29(%rip), %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	40824(%r12), %rdi
	movq	-24(%rbp), %rsi
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringStream12OutputToFileEP8_IO_FILE@PLT
	.cfi_endproc
.LFE25024:
	.size	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE, .-_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE
	.section	.text._ZN2v88internal24StackTraceFailureMessageC2EPNS0_7IsolateEPvS4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24StackTraceFailureMessageC2EPNS0_7IsolateEPvS4_S4_S4_
	.type	_ZN2v88internal24StackTraceFailureMessageC2EPNS0_7IsolateEPvS4_S4_S4_, @function
_ZN2v88internal24StackTraceFailureMessageC2EPNS0_7IsolateEPvS4_S4_S4_:
.LFB24973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	movl	$32768, %edx
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-1488(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1536, %rsp
	.cfi_offset 3, -48
	movq	%r8, -1560(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$3737837104, %eax
	movq	%r9, 40(%rdi)
	movq	%rax, (%rdi)
	addq	$1, %rax
	movups	%xmm0, 8(%rdi)
	movq	%rcx, %xmm0
	leaq	80(%rdi), %rcx
	movq	%rax, 32848(%rdi)
	movhps	-1560(%rbp), %xmm0
	movups	%xmm0, 24(%rdi)
	movq	%rcx, %rdi
	call	memset@PLT
	leaq	-1552(%rbp), %rdi
	movl	$16, %esi
	movl	$32767, -1536(%rbp)
	movq	%rax, %rcx
	leaq	16+_ZTVN2v88internal20FixedStringAllocatorE(%rip), %rax
	movq	%rdi, -1520(%rbp)
	movq	%rax, -1552(%rbp)
	movabsq	$68719476736, %rax
	movq	%rcx, -1544(%rbp)
	movq	%rax, -1512(%rbp)
	movl	$0, -1504(%rbp)
	call	_ZN2v88internal20FixedStringAllocator8allocateEj@PLT
	leaq	-1520(%rbp), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rax, -1496(%rbp)
	movb	$0, (%rax)
	call	_ZN2v88internal7Isolate10PrintStackEPNS0_12StringStreamENS1_14PrintStackModeE
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1134
	xorl	%ebx, %ebx
.L1136:
	movq	(%rdi), %rax
	addq	$1, %rbx
	call	*16(%rax)
	movq	%r13, %rdi
	movq	%rax, 40(%r12,%rbx,8)
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-72(%rbp), %rdi
	cmpq	$4, %rbx
	je	.L1134
	testq	%rdi, %rdi
	jne	.L1136
.L1134:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1147
	addq	$1536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1147:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24973:
	.size	_ZN2v88internal24StackTraceFailureMessageC2EPNS0_7IsolateEPvS4_S4_S4_, .-_ZN2v88internal24StackTraceFailureMessageC2EPNS0_7IsolateEPvS4_S4_S4_
	.globl	_ZN2v88internal24StackTraceFailureMessageC1EPNS0_7IsolateEPvS4_S4_S4_
	.set	_ZN2v88internal24StackTraceFailureMessageC1EPNS0_7IsolateEPvS4_S4_S4_,_ZN2v88internal24StackTraceFailureMessageC2EPNS0_7IsolateEPvS4_S4_S4_
	.section	.text._ZN2v88internal7Isolate20PushStackTraceAndDieEPvS2_S2_S2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate20PushStackTraceAndDieEPvS2_S2_S2_
	.type	_ZN2v88internal7Isolate20PushStackTraceAndDieEPvS2_S2_S2_, @function
_ZN2v88internal7Isolate20PushStackTraceAndDieEPvS2_S2_S2_:
.LFB24970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-32768(%rsp), %r11
.LPSRL0:
	subq	$4096, %rsp
	orq	$0, (%rsp)
	cmpq	%r11, %rsp
	jne	.LPSRL0
	subq	$96, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-32880(%rbp), %r13
	movq	%r8, %r9
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24StackTraceFailureMessageC1EPNS0_7IsolateEPvS4_S4_S4_
	movq	%r13, %rdi
	call	_ZNV2v88internal24StackTraceFailureMessage5PrintEv
	call	_ZN2v84base2OS5AbortEv@PLT
	.cfi_endproc
.LFE24970:
	.size	_ZN2v88internal7Isolate20PushStackTraceAndDieEPvS2_S2_S2_, .-_ZN2v88internal7Isolate20PushStackTraceAndDieEPvS2_S2_S2_
	.section	.text._ZN2v88internal7Isolate28SetFailedAccessCheckCallbackEPFvNS_5LocalINS_6ObjectEEENS_10AccessTypeENS2_INS_5ValueEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate28SetFailedAccessCheckCallbackEPFvNS_5LocalINS_6ObjectEEENS_10AccessTypeENS2_INS_5ValueEEEE
	.type	_ZN2v88internal7Isolate28SetFailedAccessCheckCallbackEPFvNS_5LocalINS_6ObjectEEENS_10AccessTypeENS2_INS_5ValueEEEE, @function
_ZN2v88internal7Isolate28SetFailedAccessCheckCallbackEPFvNS_5LocalINS_6ObjectEEENS_10AccessTypeENS2_INS_5ValueEEEE:
.LFB25027:
	.cfi_startproc
	endbr64
	movq	%rsi, 12624(%rdi)
	ret
	.cfi_endproc
.LFE25027:
	.size	_ZN2v88internal7Isolate28SetFailedAccessCheckCallbackEPFvNS_5LocalINS_6ObjectEEENS_10AccessTypeENS2_INS_5ValueEEEE, .-_ZN2v88internal7Isolate28SetFailedAccessCheckCallbackEPFvNS_5LocalINS_6ObjectEEENS_10AccessTypeENS2_INS_5ValueEEEE
	.section	.text._ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE
	.type	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE, @function
_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE:
.LFB25029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40936(%rdi), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L1153
.L1160:
	movl	$1, %eax
.L1152:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1177
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1153:
	.cfi_restore_state
	movq	(%rdx), %rax
	movq	%rdx, %r14
	movq	%rdi, %r12
	movq	%rsi, %r15
	movq	-1(%rax), %rdx
	cmpw	$1026, 11(%rdx)
	je	.L1155
.L1161:
	addl	$1, 41104(%r12)
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal15AccessCheckInfo3GetEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	%rax, %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1156
	movq	7(%rdx), %rax
	cmpq	%rax, _ZN2v88internal3Smi5kZeroE(%rip)
	je	.L1170
	movq	7(%rax), %rax
	movq	%rax, -88(%rbp)
.L1162:
	movq	41112(%r12), %rdi
	movq	31(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1163
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1164:
	movq	41016(%r12), %rdi
	movq	%rdx, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %rdx
	testb	%al, %al
	jne	.L1178
.L1166:
	movl	12616(%r12), %ecx
	movq	-88(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	$6, 12616(%r12)
	movl	%ecx, -72(%rbp)
	call	*%rax
	movl	-72(%rbp), %ecx
	movl	%ecx, 12616(%r12)
.L1156:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1152
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movb	%al, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movzbl	-72(%rbp), %eax
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	23(%rax), %rbx
	testb	$1, %bl
	jne	.L1158
.L1159:
	xorl	%eax, %eax
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1158:
	movq	-1(%rbx), %rax
	movzwl	11(%rax), %eax
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L1159
	movq	(%rsi), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	23(%rax), %rax
	cmpq	%rax, %rbx
	je	.L1160
	movq	1151(%rax), %rax
	movq	1151(%rbx), %rdx
	cmpq	%rax, %rdx
	jne	.L1161
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L1179
.L1165:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1178:
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal6Logger16ApiSecurityCheckEv@PLT
	movq	-72(%rbp), %rdx
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	$0, -88(%rbp)
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1179:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1165
.L1177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25029:
	.size	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE, .-_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE
	.section	.text._ZN2v88internal7Isolate24CancelTerminateExecutionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate24CancelTerminateExecutionEv
	.type	_ZN2v88internal7Isolate24CancelTerminateExecutionEv, @function
_ZN2v88internal7Isolate24CancelTerminateExecutionEv:
.LFB25032:
	.cfi_startproc
	endbr64
	movq	12448(%rdi), %rax
	testq	%rax, %rax
	je	.L1181
	andb	$-17, 40(%rax)
.L1181:
	movq	12480(%rdi), %rax
	movq	96(%rdi), %rdx
	cmpq	%rax, %rdx
	je	.L1182
	cmpq	%rax, 320(%rdi)
	je	.L1187
.L1182:
	movq	12552(%rdi), %rax
	cmpq	%rdx, %rax
	je	.L1180
	cmpq	%rax, 320(%rdi)
	je	.L1188
.L1180:
	ret
	.p2align 4,,10
	.p2align 3
.L1188:
	movb	$0, 12545(%rdi)
	movq	%rdx, 12552(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1187:
	movb	$0, 12545(%rdi)
	movq	%rdx, 12480(%rdi)
	jmp	.L1182
	.cfi_endproc
.LFE25032:
	.size	_ZN2v88internal7Isolate24CancelTerminateExecutionEv, .-_ZN2v88internal7Isolate24CancelTerminateExecutionEv
	.section	.rodata._ZN2v88internal7Isolate16RequestInterruptEPFvPNS_7IsolateEPvES4_.str1.8,"aMS",@progbits,1
	.align 8
.LC31:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN2v88internal7Isolate16RequestInterruptEPFvPNS_7IsolateEPvES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate16RequestInterruptEPFvPNS_7IsolateEPvES4_
	.type	_ZN2v88internal7Isolate16RequestInterruptEPFvPNS_7IsolateEPvES4_, @function
_ZN2v88internal7Isolate16RequestInterruptEPFvPNS_7IsolateEPvES4_:
.LFB25033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	40976(%rdi), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movq	41600(%r15), %rcx
	movq	41584(%r15), %rax
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L1190
	movq	%r12, (%rax)
	movq	%rbx, 8(%rax)
	addq	$16, 41584(%r15)
.L1191:
	leaq	37512(%r15), %rdi
	movl	$8, %esi
	call	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE@PLT
	addq	$40, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1190:
	.cfi_restore_state
	movq	41608(%r15), %r14
	movq	41576(%r15), %rsi
	subq	41592(%r15), %rax
	movq	%r14, %r8
	sarq	$4, %rax
	subq	%rsi, %r8
	movq	%r8, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	41568(%r15), %rax
	subq	41552(%r15), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	movabsq	$576460752303423487, %rdx
	cmpq	%rdx, %rax
	je	.L1200
	movq	41536(%r15), %r9
	movq	41544(%r15), %rdx
	movq	%r14, %rax
	subq	%r9, %rax
	movq	%rdx, %rcx
	sarq	$3, %rax
	subq	%rax, %rcx
	cmpq	$1, %rcx
	jbe	.L1201
.L1193:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	41584(%r15), %rax
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movq	41608(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 41608(%r15)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 41592(%r15)
	movq	%rdx, 41600(%r15)
	movq	%rax, 41584(%r15)
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1201:
	leaq	2(%rdi), %r10
	leaq	(%r10,%r10), %rax
	cmpq	%rax, %rdx
	ja	.L1202
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r14
	ja	.L1203
	leaq	0(,%r14,8), %rdi
	movq	%r10, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r10
	movq	41576(%r15), %rsi
	movq	%rax, %rcx
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	movq	-64(%rbp), %r8
	subq	%r10, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r9
	movq	41608(%r15), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1198
	movq	%r9, %rdi
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	-64(%rbp), %r8
	movq	%rax, %r9
.L1198:
	movq	41536(%r15), %rdi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 41544(%r15)
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r8
	movq	%rax, 41536(%r15)
.L1196:
	movq	%r9, 41576(%r15)
	movq	(%r9), %rax
	leaq	(%r9,%r8), %r14
	movq	(%r9), %xmm0
	movq	%r14, 41608(%r15)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 41560(%r15)
	movq	(%r14), %rax
	movq	%rax, 41592(%r15)
	addq	$512, %rax
	movq	%rax, 41600(%r15)
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1202:
	subq	%r10, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r9,%rdx,8), %r9
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r9, %rsi
	jbe	.L1195
	cmpq	%r14, %rsi
	je	.L1196
	movq	%r9, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %r9
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1195:
	cmpq	%r14, %rsi
	je	.L1196
	leaq	8(%r8), %rdi
	movq	%r8, -64(%rbp)
	subq	%rdx, %rdi
	movq	%r9, -56(%rbp)
	addq	%r9, %rdi
	call	memmove@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	jmp	.L1196
.L1200:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1203:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25033:
	.size	_ZN2v88internal7Isolate16RequestInterruptEPFvPNS_7IsolateEPvES4_, .-_ZN2v88internal7Isolate16RequestInterruptEPFvPNS_7IsolateEPvES4_
	.section	.text._ZN2v88internal7Isolate27InvokeApiInterruptCallbacksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate27InvokeApiInterruptCallbacksEv
	.type	_ZN2v88internal7Isolate27InvokeApiInterruptCallbacksEv, @function
_ZN2v88internal7Isolate27InvokeApiInterruptCallbacksEv:
.LFB25042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1220
.L1205:
	leaq	40976(%rbx), %r12
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1222:
	addq	$16, %rax
	movq	%rax, 41552(%rbx)
.L1209:
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	movq	41088(%rbx), %rax
	movq	-112(%rbp), %rsi
	movq	%rbx, %rdi
	movl	12616(%rbx), %r15d
	addl	$1, 41104(%rbx)
	movq	%rax, -104(%rbp)
	movq	41096(%rbx), %r14
	movl	$6, 12616(%rbx)
	call	*%r13
	movq	-104(%rbp), %rax
	subl	$1, 41104(%rbx)
	movq	%rax, 41088(%rbx)
	cmpq	41096(%rbx), %r14
	je	.L1210
	movq	%r14, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1210:
	movl	%r15d, 12616(%rbx)
.L1211:
	movq	%r12, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movq	41552(%rbx), %rax
	cmpq	41584(%rbx), %rax
	je	.L1221
	movq	41568(%rbx), %rcx
	movq	(%rax), %r13
	movq	8(%rax), %rsi
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	jne	.L1222
	movq	41560(%rbx), %rdi
	movq	%rsi, -104(%rbp)
	call	_ZdlPv@PLT
	movq	41576(%rbx), %rax
	movq	-104(%rbp), %rsi
	leaq	8(%rax), %rdx
	movq	%rdx, 41576(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 41560(%rbx)
	movq	%rdx, 41568(%rbx)
	movq	%rax, 41552(%rbx)
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	%r12, %rdi
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1223
.L1204:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1224
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1220:
	.cfi_restore_state
	movq	40960(%rdi), %rax
	leaq	-88(%rbp), %rsi
	movl	$163, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1223:
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1204
.L1224:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25042:
	.size	_ZN2v88internal7Isolate27InvokeApiInterruptCallbacksEv, .-_ZN2v88internal7Isolate27InvokeApiInterruptCallbacksEv
	.section	.rodata._ZN2v88internal28ReportBootstrappingExceptionENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"Exception thrown during bootstrapping\n"
	.align 8
.LC33:
	.string	"Extension or internal compilation error: %s in %s at line %d.\n"
	.align 8
.LC34:
	.string	"Extension or internal compilation error in %s at line %d.\n"
	.align 8
.LC35:
	.string	"Extension or internal compilation error.\n"
	.align 8
.LC36:
	.string	"Extension or internal compilation error: %s.\n"
	.section	.text._ZN2v88internal28ReportBootstrappingExceptionENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28ReportBootstrappingExceptionENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE
	.type	_ZN2v88internal28ReportBootstrappingExceptionENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE, @function
_ZN2v88internal28ReportBootstrappingExceptionENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE:
.LFB25046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	.LC32(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	testq	%rbx, %rbx
	je	.L1225
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L1225
	movq	(%rax), %rax
	movl	8(%rbx), %esi
	leaq	-48(%rbp), %r14
	movq	%r14, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal6Script13GetLineNumberEi@PLT
	movq	(%r12), %rdx
	leal	1(%rax), %r13d
	testb	$1, %dl
	jne	.L1263
	movq	(%rbx), %rsi
	movq	(%rsi), %rax
	movq	15(%rax), %rax
.L1259:
	movq	%rax, %rcx
	notq	%rcx
	andl	$1, %ecx
.L1229:
	testb	%cl, %cl
	je	.L1264
.L1234:
	testb	$1, %dl
	jne	.L1265
.L1238:
	leaq	.LC35(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
.L1225:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1266
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1263:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	movq	(%rbx), %rsi
	cmpw	$63, 11(%rax)
	movq	(%rsi), %rax
	movq	15(%rax), %rax
	ja	.L1259
	testb	$1, %al
	je	.L1234
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L1231
	xorl	%ecx, %ecx
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1264:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1234
	movq	(%rsi), %rax
	xorl	%r8d, %r8d
	leaq	-56(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movq	15(%rax), %rax
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-48(%rbp), %rsi
	movl	%r13d, %edx
	xorl	%eax, %eax
	leaq	.LC34(%rip), %rdi
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1225
.L1260:
	call	_ZdaPv@PLT
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L1238
	movq	%rdx, -56(%rbp)
	leaq	-56(%rbp), %rsi
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-48(%rbp), %rsi
	leaq	.LC36(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
.L1262:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1260
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1231:
	movq	%r14, %rdi
	leaq	-56(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	(%r12), %rax
	movq	-48(%rbp), %r14
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-64(%rbp), %rsi
	movl	%r13d, %ecx
	movq	%r14, %rdx
	leaq	.LC33(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1262
	call	_ZdaPv@PLT
	jmp	.L1262
.L1266:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25046:
	.size	_ZN2v88internal28ReportBootstrappingExceptionENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE, .-_ZN2v88internal28ReportBootstrappingExceptionENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE
	.section	.text._ZN2v88internal7Isolate7ReThrowENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate7ReThrowENS0_6ObjectE
	.type	_ZN2v88internal7Isolate7ReThrowENS0_6ObjectE, @function
_ZN2v88internal7Isolate7ReThrowENS0_6ObjectE:
.LFB25048:
	.cfi_startproc
	endbr64
	movq	%rsi, 12480(%rdi)
	movq	312(%rdi), %rax
	ret
	.cfi_endproc
.LFE25048:
	.size	_ZN2v88internal7Isolate7ReThrowENS0_6ObjectE, .-_ZN2v88internal7Isolate7ReThrowENS0_6ObjectE
	.section	.rodata._ZN2v88internal7Isolate20UnwindAndFindHandlerEv.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"wasm::WasmCode::kRuntimeStub == wasm_code->kind()"
	.align 8
.LC38:
	.string	"0 == wasm_code->handler_table_size()"
	.align 8
.LC39:
	.string	"-1 == JavaScriptFrame::cast(frame)->LookupExceptionHandlerInTable( nullptr, nullptr)"
	.section	.text._ZN2v88internal7Isolate20UnwindAndFindHandlerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate20UnwindAndFindHandlerEv
	.type	_ZN2v88internal7Isolate20UnwindAndFindHandlerEv, @function
_ZN2v88internal7Isolate20UnwindAndFindHandlerEv:
.LFB25049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-1504(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	.L1271(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1576, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12480(%rdi), %rax
	movq	%rax, -1592(%rbp)
	movq	320(%rdi), %rax
	movq	%r13, %rdi
	movq	%rax, -1600(%rbp)
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	leaq	-1576(%rbp), %rax
	movq	%rax, -1608(%rbp)
	.p2align 4,,10
	.p2align 3
.L1333:
	movq	-88(%rbp), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	cmpl	$20, %eax
	ja	.L1325
	movl	%eax, %eax
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Isolate20UnwindAndFindHandlerEv,"a",@progbits
	.align 4
	.align 4
.L1271:
	.long	.L1325-.L1271
	.long	.L1280-.L1271
	.long	.L1280-.L1271
	.long	.L1325-.L1271
	.long	.L1279-.L1271
	.long	.L1278-.L1271
	.long	.L1325-.L1271
	.long	.L1325-.L1271
	.long	.L1277-.L1271
	.long	.L1276-.L1271
	.long	.L1325-.L1271
	.long	.L1275-.L1271
	.long	.L1274-.L1271
	.long	.L1273-.L1271
	.long	.L1325-.L1271
	.long	.L1325-.L1271
	.long	.L1272-.L1271
	.long	.L1325-.L1271
	.long	.L1325-.L1271
	.long	.L1325-.L1271
	.long	.L1270-.L1271
	.section	.text._ZN2v88internal7Isolate20UnwindAndFindHandlerEv
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ecx
	testl	%ecx, %ecx
	je	.L1293
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1293
	movl	$0, %fs:(%rax)
.L1293:
	movq	-1592(%rbp), %rcx
	cmpq	%rcx, -1600(%rbp)
	je	.L1325
	leaq	-1568(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	-1608(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -1576(%rbp)
	call	_ZN2v88internal17WasmCompiledFrame29LookupExceptionHandlerInTableEPi@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jns	.L1354
.L1309:
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1325:
	movq	(%r14), %rax
.L1281:
	movq	%r14, %rdi
	call	*8(%rax)
	cmpl	$4, %eax
	je	.L1355
.L1332:
	movq	%r13, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1355:
	movq	32(%r14), %rsi
	movq	41056(%rbx), %rdi
	call	_ZN2v88internal23MaterializedObjectStore6RemoveEm@PLT
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	8(%r14), %rax
	movq	%r14, %rdi
	leaq	-1568(%rbp), %r15
	leaq	-1576(%rbp), %r12
	xorl	%r14d, %r14d
	movq	1424(%rax), %r13
	movq	0(%r13), %rax
	addq	$16, %r13
	movq	%rax, 12568(%rbx)
	call	_ZNK2v88internal10StackFrame10LookupCodeEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -1576(%rbp)
	call	_ZN2v88internal12HandlerTableC1ENS0_4CodeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal4Code17has_constant_poolEv@PLT
	testb	%al, %al
	je	.L1282
	movq	-1576(%rbp), %rcx
	movl	43(%rcx), %r11d
	leaq	63(%rcx), %rax
	testl	%r11d, %r11d
	js	.L1356
.L1284:
	movslq	55(%rcx), %rdx
	leaq	(%rdx,%rax), %r14
.L1282:
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal12HandlerTable12LookupReturnEi@PLT
	movq	-1576(%rbp), %rcx
	movslq	%eax, %r15
	movl	43(%rcx), %r10d
	leaq	63(%rcx), %rax
	testl	%r10d, %r10d
	js	.L1357
.L1286:
	addq	%rax, %r15
	movq	96(%rbx), %rax
	movq	$0, 12488(%rbx)
	movq	%r15, 12496(%rbx)
	movq	%r14, 12504(%rbx)
	movq	$0, 12512(%rbx)
	movq	%r13, 12520(%rbx)
	movq	%rax, 12480(%rbx)
.L1287:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1358
	movq	-1592(%rbp), %rax
	addq	$1576, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1270:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	-1592(%rbp), %rcx
	cmpq	%rcx, -1600(%rbp)
	je	.L1281
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	*168(%rax)
	cmpl	$-1, %eax
	je	.L1325
	leaq	.LC39(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1272:
	movq	-1592(%rbp), %rdx
	cmpq	%rdx, -1600(%rbp)
	je	.L1325
	movq	%rdx, %rsi
	movq	%r14, %rdi
	leaq	-1568(%rbp), %r15
	call	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrame12SetExceptionENS0_6ObjectE@PLT
	movq	32(%r14), %r12
	movq	%r14, %rdi
	call	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame14GetSPToFPDeltaEv@PLT
	movq	%r14, %rdi
	subq	%rax, %r12
	call	_ZNK2v88internal10StackFrame10LookupCodeEv@PLT
	movq	%r15, %rdi
	movq	%rax, -1568(%rbp)
	movq	32(%r14), %r13
	call	_ZNK2v88internal4Code17has_constant_poolEv@PLT
	testb	%al, %al
	movq	-1568(%rbp), %rax
	movl	43(%rax), %edx
	je	.L1359
	leaq	63(%rax), %rcx
	testl	%edx, %edx
	js	.L1360
.L1329:
	movslq	55(%rax), %rsi
	leaq	(%rsi,%rcx), %r14
.L1327:
	addq	$63, %rax
	testl	%edx, %edx
	js	.L1361
.L1331:
	movq	$0, 12488(%rbx)
.L1353:
	movq	%rax, 12496(%rbx)
	movq	96(%rbx), %rax
	movq	%r14, 12504(%rbx)
	movq	%r13, 12512(%rbx)
	movq	%r12, 12520(%rbx)
	movq	%rax, 12480(%rbx)
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	-1592(%rbp), %rdx
	cmpq	%rdx, -1600(%rbp)
	je	.L1325
	leaq	-1568(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	40(%r14), %rax
	movq	45752(%rbx), %rcx
	movq	(%rax), %rsi
	leaq	280(%rcx), %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1307
	cmpl	$3, 60(%rax)
	jne	.L1362
	call	_ZNK2v88internal4wasm8WasmCode18handler_table_sizeEv@PLT
	testl	%eax, %eax
	je	.L1309
	leaq	.LC38(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1274:
	movq	-1592(%rbp), %rcx
	cmpq	%rcx, -1600(%rbp)
	je	.L1325
	movq	%r14, %rdi
	call	_ZNK2v88internal16InterpretedFrame16GetBytecodeArrayEv@PLT
	movl	39(%rax), %eax
	testl	%eax, %eax
	leal	7(%rax), %edi
	cmovns	%eax, %edi
	sarl	$3, %edi
	call	_ZN2v88internal25InterpreterFrameConstants22RegisterStackSlotCountEi@PLT
	movq	-1608(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	%eax, -1612(%rbp)
	movl	$0, -1576(%rbp)
	movq	(%r14), %rax
	call	*168(%rax)
	movl	%eax, %r15d
	testl	%eax, %eax
	js	.L1325
	movq	32(%r14), %rax
	movl	-1576(%rbp), %esi
	movq	%r14, %rdi
	leaq	-32(%rax), %r12
	movl	-1612(%rbp), %eax
	sall	$3, %eax
	cltq
	subq	%rax, %r12
	call	_ZNK2v88internal16InterpretedFrame23ReadInterpreterRegisterEi@PLT
	movl	%r15d, %esi
	movq	%r14, %rdi
	leaq	-1568(%rbp), %r15
	movq	%rax, %r13
	call	_ZN2v88internal16InterpretedFrame19PatchBytecodeOffsetEi@PLT
	leaq	41184(%rbx), %rdi
	movl	$65, %esi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%r15, %rdi
	movq	%rax, -1568(%rbp)
	movq	32(%r14), %r14
	call	_ZNK2v88internal4Code17has_constant_poolEv@PLT
	testb	%al, %al
	movq	-1568(%rbp), %rax
	movl	43(%rax), %ecx
	je	.L1363
	leaq	63(%rax), %rsi
	testl	%ecx, %ecx
	js	.L1364
.L1319:
	movslq	55(%rax), %rdx
	addq	%rsi, %rdx
.L1317:
	addq	$63, %rax
	testl	%ecx, %ecx
	js	.L1365
.L1321:
	movq	%rax, 12496(%rbx)
	movq	96(%rbx), %rax
	movq	%r13, 12488(%rbx)
	movq	%rdx, 12504(%rbx)
	movq	%r14, 12512(%rbx)
	movq	%r12, 12520(%rbx)
	movq	%rax, 12480(%rbx)
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1275:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1325
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	movq	(%r14), %rax
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1276:
	movq	8(%r14), %rax
	movq	%r14, %rdi
	leaq	-1568(%rbp), %r15
	leaq	-1576(%rbp), %r12
	movq	1424(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 12568(%rbx)
	call	_ZNK2v88internal10StackFrame10LookupCodeEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -1576(%rbp)
	call	_ZN2v88internal12HandlerTableC1ENS0_4CodeE@PLT
	movq	-1576(%rbp), %rax
	movl	43(%rax), %r9d
	leaq	63(%rax), %rdx
	testl	%r9d, %r9d
	js	.L1366
.L1289:
	movq	40(%r14), %rax
	movq	%r15, %rdi
	movq	%rdx, -1600(%rbp)
	movq	(%rax), %rsi
	subl	%edx, %esi
	call	_ZN2v88internal12HandlerTable12LookupReturnEi@PLT
	movq	32(%r14), %r14
	movq	%r12, %rdi
	movslq	%eax, %r13
	movq	-1576(%rbp), %rax
	movq	%r14, %rcx
	movl	43(%rax), %eax
	shrl	$4, %eax
	andl	$134217720, %eax
	subq	%rax, %rcx
	leaq	16(%rcx), %r15
	call	_ZNK2v88internal4Code17has_constant_poolEv@PLT
	movq	-1600(%rbp), %rdx
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L1290
	movq	-1576(%rbp), %rsi
	movl	43(%rsi), %r8d
	leaq	63(%rsi), %rdi
	testl	%r8d, %r8d
	js	.L1367
.L1292:
	movslq	55(%rsi), %rax
	addq	%rdi, %rax
.L1290:
	movq	%rax, 12504(%rbx)
	movq	96(%rbx), %rax
	addq	%rdx, %r13
	movq	$0, 12488(%rbx)
	movq	%r13, 12496(%rbx)
	movq	%r14, 12512(%rbx)
	movq	%r15, 12520(%rbx)
	movq	%rax, 12480(%rbx)
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1277:
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	%fs:(%rax), %edx
	testl	%edx, %edx
	je	.L1325
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1325
	movl	$0, %fs:(%rax)
	movq	(%r14), %rax
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1279:
	movq	-1592(%rbp), %rcx
	cmpq	%rcx, -1600(%rbp)
	je	.L1325
	movl	$0, -1576(%rbp)
	movq	(%r14), %rax
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	-1608(%rbp), %rsi
	call	*168(%rax)
	testl	%eax, %eax
	js	.L1325
	movl	%eax, -1600(%rbp)
	movl	-1576(%rbp), %eax
	movl	$16, %r12d
	movq	%r14, %rdi
	sall	$3, %eax
	cltq
	subq	%rax, %r12
	addq	32(%r14), %r12
	call	_ZNK2v88internal10StackFrame10LookupCodeEv@PLT
	movl	-1600(%rbp), %edx
	movq	%rax, -1568(%rbp)
	testb	$62, 43(%rax)
	je	.L1368
.L1299:
	leaq	-1568(%rbp), %r15
	movl	%edx, -1600(%rbp)
	movq	32(%r14), %r13
	movq	%r15, %rdi
	call	_ZNK2v88internal4Code17has_constant_poolEv@PLT
	movl	-1600(%rbp), %edx
	testb	%al, %al
	movq	-1568(%rbp), %rax
	movl	43(%rax), %esi
	je	.L1369
	leaq	63(%rax), %rcx
	testl	%esi, %esi
	js	.L1370
.L1303:
	movslq	55(%rax), %rdi
	leaq	(%rdi,%rcx), %r14
.L1301:
	leaq	63(%rax), %rcx
	testl	%esi, %esi
	js	.L1371
.L1305:
	movq	$0, 12488(%rbx)
	movslq	%edx, %rax
	addq	%rcx, %rax
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1357:
	movq	%r12, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1366:
	movq	%r12, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rdx
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1356:
	movq	%r12, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-1576(%rbp), %rcx
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1367:
	movq	%r12, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-1576(%rbp), %rsi
	movq	-1600(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L1292
.L1307:
	movq	%r14, %rdi
	call	_ZNK2v88internal10StackFrame10LookupCodeEv@PLT
	movq	%rax, -1576(%rbp)
	movq	-1(%rax), %rdx
	cmpw	$69, 11(%rdx)
	jne	.L1309
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$3, %eax
	jne	.L1309
	movq	-1608(%rbp), %rdi
	call	_ZNK2v88internal4Code17has_handler_tableEv@PLT
	testb	%al, %al
	je	.L1309
	movq	-1576(%rbp), %rax
	testb	$64, 43(%rax)
	je	.L1309
	leaq	-1580(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -1580(%rbp)
	call	_ZN2v88internal9StubFrame29LookupExceptionHandlerInTableEPi@PLT
	movl	%eax, -1612(%rbp)
	testl	%eax, %eax
	js	.L1309
	movl	-1580(%rbp), %eax
	movq	32(%r14), %r12
	movq	-1608(%rbp), %rdi
	sall	$3, %eax
	movq	%r12, %rdx
	cltq
	subq	%rax, %rdx
	leaq	16(%rdx), %r13
	call	_ZNK2v88internal4Code17has_constant_poolEv@PLT
	testb	%al, %al
	je	.L1337
	movq	-1576(%rbp), %rax
	cmpl	$0, 43(%rax)
	leaq	63(%rax), %rdx
	js	.L1372
.L1312:
	movslq	55(%rax), %rcx
	leaq	(%rcx,%rdx), %r14
.L1310:
	cmpl	$0, 43(%rax)
	leaq	63(%rax), %rdx
	js	.L1373
.L1314:
	movslq	-1612(%rbp), %rax
	movq	%r14, 12504(%rbx)
	movq	%r15, %rdi
	movq	$0, 12488(%rbx)
	addq	%rdx, %rax
	movq	%r12, 12512(%rbx)
	movq	%rax, 12496(%rbx)
	movq	96(%rbx), %rax
	movq	%r13, 12520(%rbx)
	movq	%rax, 12480(%rbx)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1362:
	leaq	.LC37(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1354:
	movl	-1576(%rbp), %eax
	movl	$16, %r12d
	sall	$3, %eax
	cltq
	subq	%rax, %r12
	addq	32(%r14), %r12
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L1296
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
.L1296:
	movq	40(%r14), %rax
	movl	%edx, -1600(%rbp)
	movq	45752(%rbx), %rdx
	movq	(%rax), %rsi
	leaq	280(%rdx), %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm@PLT
	movq	32(%r14), %r14
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal4wasm8WasmCode13constant_poolEv@PLT
	movq	0(%r13), %rcx
	movq	%r14, 12512(%rbx)
	movq	%r15, %rdi
	movq	%rax, %r8
	movslq	-1600(%rbp), %rax
	movq	$0, 12488(%rbx)
	movq	%r8, 12504(%rbx)
	addq	%rcx, %rax
	movq	%r12, 12520(%rbx)
	movq	%rax, 12496(%rbx)
	movq	96(%rbx), %rax
	movq	%rax, 12480(%rbx)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	jmp	.L1287
.L1359:
	xorl	%r14d, %r14d
	jmp	.L1327
.L1361:
	movq	%r15, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L1331
.L1369:
	xorl	%r14d, %r14d
	jmp	.L1301
.L1368:
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L1299
	movq	40(%r14), %rax
	movq	(%rax), %rax
	subl	-1568(%rbp), %eax
	movb	$1, 41048(%rbx)
	leal	-63(%rax), %edx
	jmp	.L1299
.L1371:
	movq	%r15, %rdi
	movl	%edx, -1600(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movl	-1600(%rbp), %edx
	movq	%rax, %rcx
	jmp	.L1305
.L1360:
	movq	%r15, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rcx
	movq	-1568(%rbp), %rax
	movl	43(%rax), %edx
	jmp	.L1329
.L1363:
	xorl	%edx, %edx
	jmp	.L1317
.L1365:
	movq	%r15, %rdi
	movq	%rdx, -1600(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-1600(%rbp), %rdx
	jmp	.L1321
.L1370:
	movq	%r15, %rdi
	movl	%edx, -1600(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movl	-1600(%rbp), %edx
	movq	%rax, %rcx
	movq	-1568(%rbp), %rax
	movl	43(%rax), %esi
	jmp	.L1303
.L1358:
	call	__stack_chk_fail@PLT
.L1364:
	movq	%r15, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rsi
	movq	-1568(%rbp), %rax
	movl	43(%rax), %ecx
	jmp	.L1319
.L1373:
	movq	-1608(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rdx
	jmp	.L1314
.L1372:
	movq	-1608(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rdx
	movq	-1576(%rbp), %rax
	jmp	.L1312
.L1337:
	movq	-1576(%rbp), %rax
	xorl	%r14d, %r14d
	jmp	.L1310
	.cfi_endproc
.LFE25049:
	.size	_ZN2v88internal7Isolate20UnwindAndFindHandlerEv, .-_ZN2v88internal7Isolate20UnwindAndFindHandlerEv
	.section	.text._ZN2v88internal7Isolate23PredictExceptionCatcherEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate23PredictExceptionCatcherEv
	.type	_ZN2v88internal7Isolate23PredictExceptionCatcherEv, @function
_ZN2v88internal7Isolate23PredictExceptionCatcherEv:
.LFB25053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1512, %rsp
	movq	12448(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L1401
	movq	32(%r14), %r14
	movq	%r14, -1528(%rbp)
	testq	%r14, %r14
	je	.L1402
	cmpq	$0, 320(%rdi)
	je	.L1404
	movq	12568(%rdi), %rax
	testq	%rax, %rax
	je	.L1404
	cmpq	%rax, %r14
	jb	.L1404
.L1375:
	leaq	-1504(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r15
	testq	%r15, %r15
	je	.L1399
	leaq	-1512(%rbp), %rax
	leaq	.L1380(%rip), %r12
	movq	%rax, -1536(%rbp)
	.p2align 4,,10
	.p2align 3
.L1377:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	cmpl	$20, %eax
	ja	.L1378
	movl	%eax, %eax
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Isolate23PredictExceptionCatcherEv,"a",@progbits
	.align 4
	.align 4
.L1380:
	.long	.L1378-.L1380
	.long	.L1383-.L1380
	.long	.L1383-.L1380
	.long	.L1378-.L1380
	.long	.L1379-.L1380
	.long	.L1378-.L1380
	.long	.L1378-.L1380
	.long	.L1378-.L1380
	.long	.L1378-.L1380
	.long	.L1378-.L1380
	.long	.L1378-.L1380
	.long	.L1378-.L1380
	.long	.L1379-.L1380
	.long	.L1382-.L1380
	.long	.L1378-.L1380
	.long	.L1378-.L1380
	.long	.L1381-.L1380
	.long	.L1378-.L1380
	.long	.L1378-.L1380
	.long	.L1378-.L1380
	.long	.L1379-.L1380
	.section	.text._ZN2v88internal7Isolate23PredictExceptionCatcherEv
	.p2align 4,,10
	.p2align 3
.L1383:
	testq	%r14, %r14
	je	.L1378
	movq	12448(%rbx), %rax
	testb	$1, 40(%rax)
	jne	.L1378
	movq	8(%r15), %rax
	movq	1424(%rax), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L1404
	cmpq	-1528(%rbp), %rax
	ja	.L1404
	.p2align 4,,10
	.p2align 3
.L1378:
	movq	%r13, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r15
	testq	%r15, %r15
	jne	.L1377
.L1399:
	xorl	%eax, %eax
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1379:
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_116PredictExceptionEPNS0_15JavaScriptFrameE
	cmpl	$4, %eax
	ja	.L1387
	movl	%eax, %eax
	leaq	CSWTCH.699(%rip), %rcx
	movl	(%rcx,%rax,4), %eax
	testl	%eax, %eax
	je	.L1378
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	%r15, %rdi
	call	_ZNK2v88internal10StackFrame10LookupCodeEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1394
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	31(%rsi), %rax
	movl	15(%rax), %eax
	testb	$16, %al
	jne	.L1408
.L1425:
	movq	31(%rsi), %rax
	movl	15(%rax), %eax
	testb	$32, %al
	je	.L1378
.L1409:
	movl	$1, %eax
.L1374:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1422
	addq	$1512, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1382:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK2v88internal10StackFrame10LookupCodeEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1388
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L1389:
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	jne	.L1378
	movq	(%r15), %rcx
	movl	43(%rcx), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$3, %eax
	jne	.L1378
	movq	-1536(%rbp), %rdi
	movq	%rcx, -1512(%rbp)
	call	_ZNK2v88internal4Code17has_handler_tableEv@PLT
	testb	%al, %al
	je	.L1378
	movq	(%r15), %rax
	testb	$64, 43(%rax)
	je	.L1378
	movq	31(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$16, %edx
	jne	.L1408
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$32, %al
	je	.L1378
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1404:
	movl	$2, %eax
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1388:
	movq	41088(%rbx), %r15
	cmpq	%r15, 41096(%rbx)
	je	.L1423
.L1390:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1424
.L1396:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movq	31(%rsi), %rax
	movl	15(%rax), %eax
	testb	$16, %al
	je	.L1425
.L1408:
	movl	$4, %eax
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1423:
	movq	%rbx, %rdi
	movq	%rax, -1544(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1544(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1390
	.p2align 4,,10
	.p2align 3
.L1424:
	movq	%rbx, %rdi
	movq	%rsi, -1544(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1544(%rbp), %rsi
	jmp	.L1396
.L1402:
	movq	%r14, -1528(%rbp)
	jmp	.L1375
.L1401:
	movq	$0, -1528(%rbp)
	jmp	.L1375
.L1387:
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1422:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25053:
	.size	_ZN2v88internal7Isolate23PredictExceptionCatcherEv, .-_ZN2v88internal7Isolate23PredictExceptionCatcherEv
	.section	.text._ZN2v88internal7Isolate33RestorePendingMessageFromTryCatchEPNS_8TryCatchE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate33RestorePendingMessageFromTryCatchEPNS_8TryCatchE
	.type	_ZN2v88internal7Isolate33RestorePendingMessageFromTryCatchEPNS_8TryCatchE, @function
_ZN2v88internal7Isolate33RestorePendingMessageFromTryCatchEPNS_8TryCatchE:
.LFB25056:
	.cfi_startproc
	endbr64
	movq	24(%rsi), %rax
	movq	%rax, 12536(%rdi)
	ret
	.cfi_endproc
.LFE25056:
	.size	_ZN2v88internal7Isolate33RestorePendingMessageFromTryCatchEPNS_8TryCatchE, .-_ZN2v88internal7Isolate33RestorePendingMessageFromTryCatchEPNS_8TryCatchE
	.section	.text._ZN2v88internal7Isolate36CancelScheduledExceptionFromTryCatchEPNS_8TryCatchE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate36CancelScheduledExceptionFromTryCatchEPNS_8TryCatchE
	.type	_ZN2v88internal7Isolate36CancelScheduledExceptionFromTryCatchEPNS_8TryCatchE, @function
_ZN2v88internal7Isolate36CancelScheduledExceptionFromTryCatchEPNS_8TryCatchE:
.LFB25057:
	.cfi_startproc
	endbr64
	movq	12552(%rdi), %rax
	cmpq	%rax, 16(%rsi)
	je	.L1431
	cmpq	$0, 12528(%rdi)
	jne	.L1429
	movb	$0, 12545(%rdi)
.L1431:
	movq	96(%rdi), %rax
	movq	%rax, 12552(%rdi)
.L1429:
	movq	12536(%rdi), %rax
	cmpq	%rax, 24(%rsi)
	je	.L1432
	ret
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	96(%rdi), %rax
	movq	%rax, 12536(%rdi)
	ret
	.cfi_endproc
.LFE25057:
	.size	_ZN2v88internal7Isolate36CancelScheduledExceptionFromTryCatchEPNS_8TryCatchE, .-_ZN2v88internal7Isolate36CancelScheduledExceptionFromTryCatchEPNS_8TryCatchE
	.section	.text._ZN2v88internal7Isolate25PromoteScheduledExceptionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv
	.type	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv, @function
_ZN2v88internal7Isolate25PromoteScheduledExceptionEv:
.LFB25058:
	.cfi_startproc
	endbr64
	movq	12552(%rdi), %rax
	movq	96(%rdi), %rdx
	movq	%rax, 12480(%rdi)
	movq	312(%rdi), %rax
	movq	%rdx, 12552(%rdi)
	ret
	.cfi_endproc
.LFE25058:
	.size	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv, .-_ZN2v88internal7Isolate25PromoteScheduledExceptionEv
	.section	.rodata._ZN2v88internal7Isolate22PrintCurrentStackTraceEP8_IO_FILE.str1.1,"aMS",@progbits,1
.LC40:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal7Isolate22PrintCurrentStackTraceEP8_IO_FILE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate22PrintCurrentStackTraceEP8_IO_FILE
	.type	_ZN2v88internal7Isolate22PrintCurrentStackTraceEP8_IO_FILE, @function
_ZN2v88internal7Isolate22PrintCurrentStackTraceEP8_IO_FILE:
.LFB25059:
	.cfi_startproc
	endbr64
	movabsq	$-71776119061217281, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	88(%rdi), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -120(%rbp)
	movzbl	_ZN2v88internal23FLAG_async_stack_tracesE(%rip), %eax
	movq	-120(%rbp), %rdx
	salq	$48, %rax
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movabsq	$8589934592, %rax
	movq	%rax, -128(%rbp)
	movq	%rdx, %rax
	movabsq	$-4294967296, %rdx
	andq	%rdx, %rax
	orq	$1, %rax
	movq	%rax, -120(%rbp)
	movdqa	-128(%rbp), %xmm0
	movl	$1, %eax
	pinsrw	$6, %eax, %xmm0
	movabsq	$72057594037927935, %rax
	movaps	%xmm0, -144(%rbp)
	movq	-136(%rbp), %rdx
	movdqa	-144(%rbp), %xmm1
	andq	%rax, %rdx
	movaps	%xmm1, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rcx
	call	_ZN2v88internal12_GLOBAL__N_117CaptureStackTraceEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS1_24CaptureStackTraceOptionsE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	(%rbx), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L1435
	xorl	%r15d, %r15d
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1437:
	movq	%r12, %rdx
	movq	%r14, %rdi
	addq	$1, %r15
	call	_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEERNS0_24IncrementalStringBuilderE@PLT
	movq	(%rbx), %rax
	cmpl	%r15d, 11(%rax)
	jle	.L1435
.L1439:
	movq	15(%rax,%r15,8), %r8
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L1444
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L1445
.L1438:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r8, (%rsi)
	jmp	.L1437
	.p2align 4,,10
	.p2align 3
.L1435:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	je	.L1446
	movq	(%rax), %rax
	leaq	-104(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal6String7PrintOnEP8_IO_FILE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1447
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1445:
	.cfi_restore_state
	movq	%r14, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1446:
	leaq	.LC40(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1447:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25059:
	.size	_ZN2v88internal7Isolate22PrintCurrentStackTraceEP8_IO_FILE, .-_ZN2v88internal7Isolate22PrintCurrentStackTraceEP8_IO_FILE
	.section	.text._ZN2v88internal7Isolate15ComputeLocationEPNS0_15MessageLocationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate15ComputeLocationEPNS0_15MessageLocationE
	.type	_ZN2v88internal7Isolate15ComputeLocationEPNS0_15MessageLocationE, @function
_ZN2v88internal7Isolate15ComputeLocationEPNS0_15MessageLocationE:
.LFB25060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-1504(%rbp), %rdi
	pushq	%rbx
	subq	$1608, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -1640(%rbp)
	movq	%r12, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r13
	testq	%r13, %r13
	je	.L1448
	leaq	-1568(%rbp), %r15
	pxor	%xmm0, %xmm0
	movq	$0, -1616(%rbp)
	movq	%r15, %rdi
	movaps	%xmm0, -1632(%rbp)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	leaq	-1632(%rbp), %rsi
	call	*136(%rax)
	movq	-1624(%rbp), %r14
	movq	$0, -1648(%rbp)
	leaq	-56(%r14), %rbx
	movq	%rbx, %rdi
	call	_ZNK2v88internal12FrameSummary6scriptEv@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L1469
.L1450:
	xorl	%r14d, %r14d
.L1453:
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	-1624(%rbp), %rbx
	movq	-1632(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1458
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	%r12, %rdi
	addq	$56, %r12
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%r12, %rbx
	jne	.L1459
	movq	-1632(%rbp), %r12
.L1458:
	testq	%r12, %r12
	je	.L1448
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1448:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1470
	addq	$1608, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1469:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L1450
	movq	7(%rax), %rax
	cmpq	%rax, 88(%r12)
	je	.L1450
	movl	-48(%r14), %eax
	testl	%eax, %eax
	je	.L1471
.L1452:
	movq	%rbx, %rdi
	call	_ZNK2v88internal12FrameSummary27AreSourcePositionsAvailableEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %r14d
	testb	%al, %al
	je	.L1457
	call	_ZNK2v88internal12FrameSummary14SourcePositionEv@PLT
	movq	-1648(%rbp), %r8
	leaq	-1600(%rbp), %rdi
	movq	%r13, %rsi
	movl	%eax, %edx
	leal	1(%rax), %ecx
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEiiNS2_INS0_18SharedFunctionInfoEEE@PLT
	movq	-1640(%rbp), %rax
	movdqa	-1600(%rbp), %xmm1
	movdqa	-1584(%rbp), %xmm2
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1457:
	call	_ZNK2v88internal12FrameSummary11code_offsetEv@PLT
	movq	-1648(%rbp), %rdx
	movq	%r13, %rsi
	leaq	-1600(%rbp), %rdi
	movl	%eax, %ecx
	movl	$1, %r14d
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEENS2_INS0_18SharedFunctionInfoEEEi@PLT
	movq	-1640(%rbp), %rax
	movdqa	-1600(%rbp), %xmm3
	movdqa	-1584(%rbp), %xmm4
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1471:
	movq	-32(%r14), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1454
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1455:
	movq	%rdx, -1648(%rbp)
	jmp	.L1452
.L1454:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L1472
.L1456:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L1455
.L1472:
	movq	%r12, %rdi
	movq	%rsi, -1648(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1648(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1456
.L1470:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25060:
	.size	_ZN2v88internal7Isolate15ComputeLocationEPNS0_15MessageLocationE, .-_ZN2v88internal7Isolate15ComputeLocationEPNS0_15MessageLocationE
	.section	.text._ZN2v88internal7Isolate28ComputeLocationFromExceptionEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate28ComputeLocationFromExceptionEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal7Isolate28ComputeLocationFromExceptionEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal7Isolate28ComputeLocationFromExceptionEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE:
.LFB25064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$136, %rsp
	movq	(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	notq	%rax
	movl	%eax, %r12d
	andl	$1, %r12d
	je	.L1474
.L1476:
	xorl	%r12d, %r12d
.L1473:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1507
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1474:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1476
	movq	%rdi, %r14
	andq	$-262144, %rdx
	movq	%rsi, %r15
	movq	3696(%r14), %rax
	leaq	3696(%rdi), %rsi
	movq	24(%rdx), %rdi
	movl	$2, %edx
	movq	-1(%rax), %rcx
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L1477
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
.L1477:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	3696(%r14), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L1508
.L1478:
	leaq	-144(%rbp), %r13
	movq	%rsi, -112(%rbp)
	movq	%r13, %rdi
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L1479
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L1480:
	movq	(%rax), %rax
	movq	%rax, -152(%rbp)
	testb	$1, %al
	jne	.L1473
	movq	(%rbx), %rdx
	movq	3680(%r14), %rax
	leaq	3680(%r14), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	movq	-1(%rax), %rcx
	movl	$2, %edx
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L1482
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
.L1482:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	3680(%r14), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L1509
.L1483:
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L1484
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L1485:
	movq	(%rax), %rcx
	movq	%rcx, %rax
	notq	%rax
	andl	$1, %eax
	movb	%al, -153(%rbp)
	je	.L1473
	movq	(%rbx), %rdx
	movq	3688(%r14), %rax
	leaq	3688(%r14), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	movq	-1(%rax), %r10
	movl	$2, %edx
	subq	$37592, %rdi
	cmpw	$64, 11(%r10)
	jne	.L1487
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
.L1487:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	3688(%r14), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L1510
.L1488:
	movq	%r13, %rdi
	movq	%rcx, -168(%rbp)
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movq	-168(%rbp), %rcx
	jne	.L1489
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L1490:
	movq	(%rax), %rbx
	testb	$1, %bl
	je	.L1473
	movq	-1(%rbx), %rax
	cmpw	$96, 11(%rax)
	jne	.L1473
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1493
	movq	%rbx, %rsi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %rcx
	movq	%rax, %rsi
.L1494:
	movq	-152(%rbp), %rdx
	sarq	$32, %rcx
	movq	%r13, %rdi
	sarq	$32, %rdx
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	movdqa	-144(%rbp), %xmm0
	movdqa	-128(%rbp), %xmm1
	movzbl	-153(%rbp), %r12d
	movups	%xmm0, (%r15)
	movups	%xmm1, 16(%r15)
	jmp	.L1473
	.p2align 4,,10
	.p2align 3
.L1479:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1508:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1489:
	movq	%r13, %rdi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-168(%rbp), %rcx
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1509:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L1483
.L1510:
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-168(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L1488
.L1493:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L1511
.L1495:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%rbx, (%rsi)
	jmp	.L1494
.L1507:
	call	__stack_chk_fail@PLT
.L1511:
	movq	%r14, %rdi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L1495
	.cfi_endproc
.LFE25064:
	.size	_ZN2v88internal7Isolate28ComputeLocationFromExceptionEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal7Isolate28ComputeLocationFromExceptionEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal7Isolate29ComputeLocationFromStackTraceEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate29ComputeLocationFromStackTraceEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal7Isolate29ComputeLocationFromStackTraceEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal7Isolate29ComputeLocationFromStackTraceEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE:
.LFB25065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %r9
	notq	%r9
	andl	$1, %r9d
	je	.L1513
.L1555:
	xorl	%r9d, %r9d
.L1512:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1573
	addq	$184, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1513:
	.cfi_restore_state
	movq	-1(%rcx), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1555
	movq	%rdi, %r15
	andq	$-262144, %rcx
	movq	%rsi, %rbx
	movq	3808(%r15), %rax
	leaq	3808(%rdi), %rsi
	movq	24(%rcx), %rdi
	movl	$2, %ecx
	movq	-1(%rax), %r8
	subq	$37592, %rdi
	cmpw	$64, 11(%r8)
	jne	.L1516
	xorl	%ecx, %ecx
	testb	$1, 11(%rax)
	sete	%cl
	addl	%ecx, %ecx
.L1516:
	movabsq	$824633720832, %rax
	movl	%ecx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	3808(%r15), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %ecx
	andl	$-32, %ecx
	cmpl	$32, %ecx
	je	.L1574
.L1517:
	leaq	-144(%rbp), %r13
	movb	%r9b, -184(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movzbl	-184(%rbp), %r9d
	jne	.L1518
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rsi
.L1519:
	movq	(%rsi), %rax
	testb	$1, %al
	je	.L1512
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L1512
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal27GetFrameArrayFromStackTraceEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE@PLT
	movl	$56, %edx
	movq	%rax, -200(%rbp)
	movq	(%rax), %rax
	movq	15(%rax), %rsi
	sarq	$32, %rsi
	movl	%esi, -216(%rbp)
	testq	%rsi, %rsi
	jg	.L1522
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1524:
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-184(%rbp), %rdx
	movq	(%rax), %rsi
	movq	%rax, %r12
	movq	23(%rsi), %r8
	movq	31(%r8), %rax
	testb	$1, %al
	jne	.L1575
.L1538:
	movq	%r13, %rdi
	movq	%rdx, -184(%rbp)
	movq	%r8, -192(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movq	-184(%rbp), %rdx
	testb	%al, %al
	movl	%eax, %r9d
	jne	.L1576
.L1542:
	addl	$1, %r14d
	addq	$48, %rdx
	cmpl	-216(%rbp), %r14d
	je	.L1555
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
.L1522:
	leal	(%r14,%r14,2), %r12d
	leaq	-24(%rdx), %r8
	leal	(%r12,%r12), %ecx
	movl	%ecx, -208(%rbp)
	movq	-1(%rdx,%rax), %rsi
	btq	$32, %rsi
	jc	.L1523
	movq	-1(%rdx,%rax), %rsi
	btq	$34, %rsi
	jc	.L1523
	movq	-1(%r8,%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L1524
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L1577
.L1537:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	movq	23(%rsi), %r8
	movq	31(%r8), %rax
	testb	$1, %al
	je	.L1538
.L1575:
	movq	-1(%rax), %rdi
	cmpw	$86, 11(%rdi)
	je	.L1578
.L1539:
	movq	%rax, %rdi
	andq	$-262144, %rdi
	movq	24(%rdi), %rdi
	cmpq	-37504(%rdi), %rax
	je	.L1542
	jmp	.L1538
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	%r13, %rdi
	movb	%r9b, -184(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movzbl	-184(%rbp), %r9d
	movq	%rax, %rsi
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1576:
	movq	-192(%rbp), %r8
	movq	7(%r8), %rax
	testb	$1, %al
	jne	.L1579
.L1541:
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	movq	31(%rsi), %r8
	testb	$1, %r8b
	je	.L1542
	movq	-1(%r8), %rdi
	leaq	-1(%r8), %rsi
	cmpw	$86, 11(%rdi)
	je	.L1580
.L1556:
	movq	(%rsi), %rsi
	cmpw	$96, 11(%rsi)
	jne	.L1542
	movq	7(%r8), %rcx
	cmpq	%rcx, 88(%r15)
	je	.L1542
	movq	23(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1546
	movb	%r9b, -192(%rbp)
	movq	%r8, -184(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-184(%rbp), %r8
	movzbl	-192(%rbp), %r9d
	movq	%rax, %r10
.L1547:
	movq	-200(%rbp), %rcx
	movl	-208(%rbp), %eax
	movq	(%rcx), %rdx
	leal	40(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	movq	%rax, -168(%rbp)
	leal	1(%r14), %eax
	movq	(%rcx), %rdx
	imull	$48, %eax, %eax
	cltq
	movq	-1(%rax,%rdx), %r12
	movq	41112(%r15), %rdi
	sarq	$32, %r12
	testq	%rdi, %rdi
	je	.L1549
	movq	%r8, %rsi
	movb	%r9b, -192(%rbp)
	movq	%r10, -184(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-184(%rbp), %r10
	movzbl	-192(%rbp), %r9d
	movq	%rax, %r14
.L1550:
	movq	(%r10), %rax
	leaq	-160(%rbp), %rdi
	movb	%r9b, -192(%rbp)
	movq	%r10, -184(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo16HasBytecodeArrayEv
	movq	-184(%rbp), %r10
	movzbl	-192(%rbp), %r9d
	testb	%al, %al
	je	.L1554
	movq	(%r10), %rax
	leaq	-152(%rbp), %rdi
	movb	%r9b, -192(%rbp)
	movq	%r10, -184(%rbp)
	movq	%rax, -152(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv
	movq	%r13, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal13BytecodeArray22HasSourcePositionTableEv
	movq	-184(%rbp), %r10
	movzbl	-192(%rbp), %r9d
	testb	%al, %al
	jne	.L1581
.L1554:
	movl	%r12d, %ecx
	movq	%r10, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	%r9b, -184(%rbp)
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEENS2_INS0_18SharedFunctionInfoEEEi@PLT
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm3
	movzbl	-184(%rbp), %r9d
	movups	%xmm2, (%rbx)
	movups	%xmm3, 16(%rbx)
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1578:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1539
	jmp	.L1538
	.p2align 4,,10
	.p2align 3
.L1574:
	movq	%rdx, -192(%rbp)
	movb	%r9b, -184(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-192(%rbp), %rdx
	movzbl	-184(%rbp), %r9d
	movq	%rax, %rsi
	jmp	.L1517
	.p2align 4,,10
	.p2align 3
.L1579:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L1541
	jmp	.L1542
	.p2align 4,,10
	.p2align 3
.L1577:
	movq	%r15, %rdi
	movq	%rsi, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1523:
	movl	-208(%rbp), %ecx
	leal	24(,%rcx,8), %r9d
	movslq	%r9d, %rsi
	movq	-1(%rsi,%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1526
	movq	%rdx, -208(%rbp)
	movq	%r8, -192(%rbp)
	movl	%r9d, -184(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-184(%rbp), %r9d
	movq	-192(%rbp), %r8
	movq	-208(%rbp), %rdx
	movq	%rax, %r10
.L1527:
	movq	-200(%rbp), %rax
	leal	3(%r14,%r14,2), %ecx
	sall	$4, %ecx
	movq	(%rax), %rax
	movslq	%ecx, %rcx
	movq	-1(%r8,%rax), %r12
	movq	-1(%rcx,%rax), %rsi
	movq	-1(%rdx,%rax), %r14
	sarq	$32, %r12
	sarq	$32, %rsi
	sarq	$32, %r14
	andl	$4, %r14d
	jne	.L1582
.L1529:
	addl	$16, %r9d
	movq	%r10, -184(%rbp)
	movslq	%r9d, %r9
	movq	-1(%r9,%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal12FrameSummary24WasmCompiledFrameSummary21GetWasmSourcePositionEPKNS0_4wasm8WasmCodeEi@PLT
	movq	-184(%rbp), %r10
	movq	41112(%r15), %rdi
	movl	%eax, %edx
	movq	(%r10), %rax
	movq	135(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1530
	movq	%r10, -192(%rbp)
	movl	%edx, -184(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-184(%rbp), %edx
	movq	-192(%rbp), %r10
	movq	%rax, %rdi
.L1531:
	movl	%r14d, %ecx
	movl	%r12d, %esi
	movq	%r10, -184(%rbp)
	call	_ZN2v88internal16WasmModuleObject17GetSourcePositionENS0_6HandleIS1_EEjjb@PLT
	movq	-184(%rbp), %r10
	movq	41112(%r15), %rdi
	movl	%eax, %r12d
	movq	(%r10), %rax
	movq	135(%rax), %rax
	movq	39(%rax), %r14
	testq	%rdi, %rdi
	je	.L1533
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1534:
	leal	1(%r12), %ecx
	movl	%r12d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	movdqa	-128(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm0
	movl	$1, %r9d
	movups	%xmm0, (%rbx)
	movups	%xmm1, 16(%rbx)
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1580:
	movq	23(%r8), %r8
	testb	$1, %r8b
	je	.L1542
	leaq	-1(%r8), %rsi
	jmp	.L1556
.L1533:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1583
.L1535:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r14, (%rsi)
	jmp	.L1534
.L1530:
	movq	41088(%r15), %rdi
	cmpq	41096(%r15), %rdi
	je	.L1584
.L1532:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdi)
	jmp	.L1531
.L1526:
	movq	41088(%r15), %r10
	cmpq	41096(%r15), %r10
	je	.L1585
.L1528:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r10)
	jmp	.L1527
.L1582:
	movq	-1(%rdx,%rax), %r14
	shrq	$37, %r14
	andl	$1, %r14d
	jmp	.L1529
.L1583:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1535
.L1584:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	movq	%r10, -192(%rbp)
	movl	%edx, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	-192(%rbp), %r10
	movl	-184(%rbp), %edx
	movq	%rax, %rdi
	jmp	.L1532
.L1585:
	movq	%r15, %rdi
	movq	%rdx, -216(%rbp)
	movq	%r8, -208(%rbp)
	movq	%rsi, -192(%rbp)
	movl	%r9d, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rdx
	movq	-208(%rbp), %r8
	movq	-192(%rbp), %rsi
	movl	-184(%rbp), %r9d
	movq	%rax, %r10
	jmp	.L1528
.L1573:
	call	__stack_chk_fail@PLT
.L1549:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L1586
.L1551:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%r8, (%r14)
	jmp	.L1550
.L1546:
	movq	41088(%r15), %r10
	cmpq	41096(%r15), %r10
	je	.L1587
.L1548:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r10)
	jmp	.L1547
.L1581:
	leaq	-168(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	movq	-184(%rbp), %r10
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %edx
	leal	1(%rax), %ecx
	movq	%r10, %r8
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEiiNS2_INS0_18SharedFunctionInfoEEE@PLT
	movdqa	-144(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm5
	movzbl	-192(%rbp), %r9d
	movups	%xmm4, (%rbx)
	movups	%xmm5, 16(%rbx)
	jmp	.L1512
.L1586:
	movq	%r15, %rdi
	movb	%r9b, -200(%rbp)
	movq	%r8, -192(%rbp)
	movq	%r10, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movzbl	-200(%rbp), %r9d
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %r10
	movq	%rax, %r14
	jmp	.L1551
.L1587:
	movq	%r15, %rdi
	movb	%r9b, -216(%rbp)
	movq	%r8, -192(%rbp)
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movzbl	-216(%rbp), %r9d
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L1548
	.cfi_endproc
.LFE25065:
	.size	_ZN2v88internal7Isolate29ComputeLocationFromStackTraceEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal7Isolate29ComputeLocationFromStackTraceEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal7Isolate13CreateMessageENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate13CreateMessageENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE
	.type	_ZN2v88internal7Isolate13CreateMessageENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE, @function
_ZN2v88internal7Isolate13CreateMessageENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE:
.LFB25066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 41064(%rdi)
	je	.L1602
	movq	(%rsi), %rdx
	leaq	-144(%rbp), %rbx
	testb	$1, %dl
	jne	.L1610
.L1591:
	movl	41068(%r12), %eax
	movl	$0, %edx
	leaq	88(%r12), %rsi
	movq	%r12, %rdi
	testl	%eax, %eax
	cmovns	41068(%r12), %edx
	xorl	%ecx, %ecx
	movabsq	$72058693549555712, %rax
	movl	%edx, %edx
	btsq	$33, %rdx
	testb	$1, 41073(%r12)
	sete	%cl
	orq	%rax, %rcx
	call	_ZN2v88internal12_GLOBAL__N_117CaptureStackTraceEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS1_24CaptureStackTraceOptionsE
	movq	%rax, %r15
	jmp	.L1589
	.p2align 4,,10
	.p2align 3
.L1602:
	xorl	%r15d, %r15d
	leaq	-144(%rbp), %rbx
.L1589:
	movq	%rbx, %rdi
	call	_ZN2v88internal15MessageLocationC1Ev@PLT
	testq	%r13, %r13
	je	.L1611
.L1599:
	movq	%r14, %rcx
	movq	%r15, %r8
	movq	%r13, %rdx
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MessageHandler17MakeMessageObjectEPNS0_7IsolateENS0_15MessageTemplateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS8_INS0_10FixedArrayEEE@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1612
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1610:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	leaq	-144(%rbp), %rbx
	cmpw	$1067, 11(%rax)
	jne	.L1591
	movq	3664(%r12), %rax
	andq	$-262144, %rdx
	leaq	3664(%rdi), %rsi
	movq	24(%rdx), %rdi
	movl	$2, %edx
	movq	-1(%rax), %rcx
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	je	.L1613
.L1593:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	3664(%r12), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L1614
.L1594:
	leaq	-144(%rbp), %rbx
	movq	%rsi, -112(%rbp)
	movq	%rbx, %rdi
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r14, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L1595
	movq	-120(%rbp), %rax
	leaq	88(%rax), %r15
.L1596:
	movq	(%r15), %rax
	testb	$1, %al
	je	.L1591
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	jbe	.L1589
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L1611:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate28ComputeLocationFromExceptionEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1615
.L1600:
	movq	%rbx, %r13
	jmp	.L1599
	.p2align 4,,10
	.p2align 3
.L1615:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate29ComputeLocationFromStackTraceEPNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	jne	.L1600
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate15ComputeLocationEPNS0_15MessageLocationE
	testb	%al, %al
	je	.L1599
	jmp	.L1600
	.p2align 4,,10
	.p2align 3
.L1595:
	movq	%rbx, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	%rax, %r15
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1613:
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1614:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L1594
.L1612:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25066:
	.size	_ZN2v88internal7Isolate13CreateMessageENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE, .-_ZN2v88internal7Isolate13CreateMessageENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE
	.section	.rodata._ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE.str1.8,"aMS",@progbits,1
	.align 8
.LC41:
	.string	"========================================================="
	.section	.rodata._ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE.str1.1,"aMS",@progbits,1
.LC42:
	.string	"Exception thrown:"
.LC43:
	.string	"at "
.LC44:
	.string	"<anonymous>"
.LC45:
	.string	", line %d\n"
.LC46:
	.string	"Stack Trace:"
.LC47:
	.string	"%s\n\nFROM\n"
	.section	.text._ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE
	.type	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE, @function
_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE:
.LFB25047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -120(%rbp)
	movq	41096(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdi), %rax
	addl	$1, 41104(%rdi)
	movq	41112(%rdi), %rdi
	movq	%rax, -128(%rbp)
	testq	%rdi, %rdi
	je	.L1617
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpb	$0, _ZN2v88internal25FLAG_print_all_exceptionsE(%rip)
	movq	%rax, %rbx
	jne	.L1656
.L1620:
	movq	12448(%r13), %rax
	movzbl	12544(%r13), %r15d
	movq	-120(%rbp), %rdx
	testq	%rax, %rax
	je	.L1630
	movzbl	40(%rax), %eax
	testb	$1, %al
	je	.L1657
.L1630:
	movb	$0, 12544(%r13)
	cmpq	%rdx, 320(%r13)
	jne	.L1658
.L1632:
	testb	%r15b, %r15b
	jne	.L1634
	leaq	-96(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal15MessageLocationC1Ev@PLT
	testq	%r12, %r12
	je	.L1659
.L1635:
	movq	40936(%r13), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L1636
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal28ReportBootstrappingExceptionENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE
.L1634:
	movq	(%rbx), %rax
	movq	312(%r13), %r12
	subl	$1, 41104(%r13)
	movq	%rax, 12480(%r13)
	movq	-128(%rbp), %rax
	movq	%rax, 41088(%r13)
	cmpq	41096(%r13), %r14
	je	.L1643
	movq	%r14, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1643:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1660
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1658:
	.cfi_restore_state
	movq	41472(%r13), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal5Debug7OnThrowENS0_6HandleINS0_6ObjectEEE@PLT
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1657:
	testb	$4, %al
	jne	.L1630
	movb	$0, 12544(%r13)
	cmpq	%rdx, 320(%r13)
	je	.L1634
	movq	41472(%r13), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal5Debug7OnThrowENS0_6HandleINS0_6ObjectEEE@PLT
	jmp	.L1634
	.p2align 4,,10
	.p2align 3
.L1617:
	movq	%rax, %rbx
	cmpq	%r14, %rax
	je	.L1661
.L1619:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	cmpb	$0, _ZN2v88internal25FLAG_print_all_exceptionsE(%rip)
	je	.L1620
.L1656:
	leaq	.LC41(%rip), %rdi
	call	puts@PLT
	leaq	.LC42(%rip), %rdi
	call	puts@PLT
	testq	%r12, %r12
	je	.L1621
	movq	(%r12), %r15
	movq	(%r15), %rax
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6Script18GetNameOrSourceURLEv@PLT
	movq	41112(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1622
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1623:
	leaq	.LC43(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	movq	%rdx, -144(%rbp)
	call	__printf_chk@PLT
	movq	-144(%rbp), %rdx
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L1625
.L1627:
	leaq	.LC44(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
.L1626:
	movq	(%r15), %rax
	movl	8(%r12), %esi
	movq	-136(%rbp), %rdi
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal6Script13GetLineNumberEi@PLT
	leaq	.LC45(%rip), %rsi
	movl	$1, %edi
	leal	1(%rax), %edx
	xorl	%eax, %eax
	call	__printf_chk@PLT
.L1621:
	movq	stdout(%rip), %rsi
	leaq	-120(%rbp), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC46(%rip), %rdi
	call	puts@PLT
	movl	40816(%r13), %eax
	movq	stdout(%rip), %r15
	testl	%eax, %eax
	je	.L1662
	cmpl	$1, %eax
	je	.L1663
.L1629:
	leaq	.LC41(%rip), %rdi
	call	puts@PLT
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1636:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate13CreateMessageENS0_6HandleINS0_6ObjectEEEPNS0_15MessageLocationE
	cmpb	$0, _ZN2v88internal32FLAG_abort_on_uncaught_exceptionE(%rip)
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%rax, 12536(%r13)
	je	.L1634
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate23PredictExceptionCatcherEv
	andl	$-3, %eax
	jne	.L1634
	movq	45672(%r13), %rax
	testq	%rax, %rax
	je	.L1642
	movq	%r13, %rdi
	call	*%rax
	testb	%al, %al
	je	.L1634
.L1642:
	leaq	-104(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	movb	$0, _ZN2v88internal32FLAG_abort_on_uncaught_exceptionE(%rip)
	call	_ZN2v88internal14MessageHandler19GetLocalizedMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	stderr(%rip), %rdi
	movq	-104(%rbp), %rdx
	xorl	%eax, %eax
	leaq	.LC47(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1641
	call	_ZdaPv@PLT
.L1641:
	movq	stderr(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate22PrintCurrentStackTraceEP8_IO_FILE
	call	_ZN2v84base2OS5AbortEv@PLT
	.p2align 4,,10
	.p2align 3
.L1659:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate15ComputeLocationEPNS0_15MessageLocationE
	testb	%al, %al
	cmovne	%r15, %r12
	jmp	.L1635
	.p2align 4,,10
	.p2align 3
.L1662:
	movq	%r13, %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE.part.0
	leaq	.LC41(%rip), %rdi
	call	puts@PLT
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1622:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L1664
.L1624:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L1623
	.p2align 4,,10
	.p2align 3
.L1661:
	movq	%r13, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1619
	.p2align 4,,10
	.p2align 3
.L1625:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1627
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L1627
	movq	stdout(%rip), %rsi
	movq	-136(%rbp), %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String7PrintOnEP8_IO_FILE@PLT
	jmp	.L1626
	.p2align 4,,10
	.p2align 3
.L1663:
	movl	$2, 40816(%r13)
	xorl	%eax, %eax
	leaq	.LC29(%rip), %rdi
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	40824(%r13), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal12StringStream12OutputToFileEP8_IO_FILE@PLT
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1664:
	movq	%r13, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1624
.L1660:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25047:
	.size	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE, .-_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE
	.section	.rodata._ZN2v88internal7Isolate13StackOverflowEv.str1.1,"aMS",@progbits,1
.LC48:
	.string	"Aborting on stack overflow"
	.section	.text._ZN2v88internal7Isolate13StackOverflowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate13StackOverflowEv
	.type	_ZN2v88internal7Isolate13StackOverflowEv, @function
_ZN2v88internal7Isolate13StackOverflowEv:
.LFB25030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L1677
	leaq	-96(%rbp), %r14
	movq	%rdi, %r12
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	movq	12464(%r12), %rax
	addl	$1, 41104(%r12)
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r15
	movq	39(%rax), %rax
	movq	1719(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1667
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L1668:
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	strlen@PLT
	movq	-104(%rbp), %rdx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	movq	%rdx, -80(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1678
	subq	$8, %rsp
	movq	%r13, %rdx
	xorl	%r9d, %r9d
	movl	$2, %r8d
	pushq	$1
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ErrorUtils9ConstructEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEES8_NS0_13FrameSkipModeES8_NS1_20StackTraceCollectionE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L1676
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE
.L1676:
	movq	%rbx, 41088(%r12)
	movq	312(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r15
	je	.L1673
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1673:
	movq	%r14, %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1679
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1667:
	.cfi_restore_state
	movq	%rbx, %r13
	cmpq	41096(%r12), %rbx
	je	.L1680
.L1669:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L1678:
	leaq	.LC40(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1680:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1669
.L1679:
	call	__stack_chk_fail@PLT
.L1677:
	leaq	.LC48(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25030:
	.size	_ZN2v88internal7Isolate13StackOverflowEv, .-_ZN2v88internal7Isolate13StackOverflowEv
	.section	.text._ZN2v88internal7Isolate18TerminateExecutionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate18TerminateExecutionEv
	.type	_ZN2v88internal7Isolate18TerminateExecutionEv, @function
_ZN2v88internal7Isolate18TerminateExecutionEv:
.LFB25031:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rsi
	xorl	%edx, %edx
	jmp	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE
	.cfi_endproc
.LFE25031:
	.size	_ZN2v88internal7Isolate18TerminateExecutionEv, .-_ZN2v88internal7Isolate18TerminateExecutionEv
	.section	.text._ZN2v88internal7Isolate21ThrowIllegalOperationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate21ThrowIllegalOperationEv
	.type	_ZN2v88internal7Isolate21ThrowIllegalOperationEv, @function
_ZN2v88internal7Isolate21ThrowIllegalOperationEv:
.LFB25054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, _ZN2v88internal27FLAG_stack_trace_on_illegalE(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	jne	.L1686
.L1683:
	movq	2672(%r12), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE
	.p2align 4,,10
	.p2align 3
.L1686:
	.cfi_restore_state
	movl	40816(%rdi), %eax
	movq	stdout(%rip), %r13
	testl	%eax, %eax
	je	.L1687
	cmpl	$1, %eax
	jne	.L1683
	movl	$2, 40816(%rdi)
	xorl	%eax, %eax
	leaq	.LC29(%rip), %rdi
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	40824(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal12StringStream12OutputToFileEP8_IO_FILE@PLT
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L1687:
	movl	$1, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE.part.0
	jmp	.L1683
	.cfi_endproc
.LFE25054:
	.size	_ZN2v88internal7Isolate21ThrowIllegalOperationEv, .-_ZN2v88internal7Isolate21ThrowIllegalOperationEv
	.section	.text._ZN2v88internal7Isolate13ScheduleThrowENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate13ScheduleThrowENS0_6ObjectE
	.type	_ZN2v88internal7Isolate13ScheduleThrowENS0_6ObjectE, @function
_ZN2v88internal7Isolate13ScheduleThrowENS0_6ObjectE:
.LFB25055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE
	movq	12480(%rbx), %rax
	movq	320(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L1689
	movq	12568(%rbx), %rcx
	movq	12448(%rbx), %rdx
	testq	%rcx, %rcx
	je	.L1690
	testq	%rdx, %rdx
	je	.L1695
	movq	32(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L1695
	cmpq	%rsi, %rcx
	jb	.L1695
.L1692:
	testq	%rcx, %rcx
	je	.L1696
	cmpq	%rcx, %rsi
	jnb	.L1695
.L1696:
	movb	$1, 12545(%rbx)
	cmpq	320(%rbx), %rax
	je	.L1717
	movzbl	40(%rdx), %eax
	andl	$-19, %eax
	orl	$2, %eax
	movb	%al, 40(%rdx)
	movq	12480(%rbx), %rax
	movq	%rax, 16(%rdx)
	movq	96(%rbx), %rcx
	movq	12536(%rbx), %rax
	cmpq	%rax, %rcx
	je	.L1716
	movq	%rax, 24(%rdx)
	movq	96(%rbx), %rcx
.L1716:
	movq	12480(%rbx), %rax
	jmp	.L1694
	.p2align 4,,10
	.p2align 3
.L1695:
	movb	$0, 12545(%rbx)
	movq	96(%rbx), %rcx
.L1694:
	cmpq	%rax, %rcx
	je	.L1688
	movq	%rax, 12552(%rbx)
	movq	96(%rbx), %rax
	movb	$0, 12545(%rbx)
	movq	%rax, 12480(%rbx)
.L1688:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1689:
	.cfi_restore_state
	movq	12448(%rbx), %rdx
.L1690:
	testq	%rdx, %rdx
	je	.L1695
	movq	32(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L1695
	cmpq	%rdi, %rax
	je	.L1696
	movq	12568(%rbx), %rcx
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1717:
	andb	$-3, 40(%rdx)
	movq	12448(%rbx), %rax
	orb	$16, 40(%rax)
	movq	12448(%rbx), %rax
	movq	104(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	96(%rbx), %rcx
	movq	12480(%rbx), %rax
	jmp	.L1694
	.cfi_endproc
.LFE25055:
	.size	_ZN2v88internal7Isolate13ScheduleThrowENS0_6ObjectE, .-_ZN2v88internal7Isolate13ScheduleThrowENS0_6ObjectE
	.section	.text._ZN2v88internal7Isolate24IsJavaScriptHandlerOnTopENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate24IsJavaScriptHandlerOnTopENS0_6ObjectE
	.type	_ZN2v88internal7Isolate24IsJavaScriptHandlerOnTopENS0_6ObjectE, @function
_ZN2v88internal7Isolate24IsJavaScriptHandlerOnTopENS0_6ObjectE:
.LFB25067:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	%rsi, 320(%rdi)
	je	.L1718
	movq	12568(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1718
	movq	12448(%rdi), %rax
	testq	%rax, %rax
	je	.L1723
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L1723
	cmpq	%rax, %rdx
	setb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1723:
	movl	$1, %eax
.L1718:
	ret
	.cfi_endproc
.LFE25067:
	.size	_ZN2v88internal7Isolate24IsJavaScriptHandlerOnTopENS0_6ObjectE, .-_ZN2v88internal7Isolate24IsJavaScriptHandlerOnTopENS0_6ObjectE
	.section	.text._ZN2v88internal7Isolate22IsExternalHandlerOnTopENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate22IsExternalHandlerOnTopENS0_6ObjectE
	.type	_ZN2v88internal7Isolate22IsExternalHandlerOnTopENS0_6ObjectE, @function
_ZN2v88internal7Isolate22IsExternalHandlerOnTopENS0_6ObjectE:
.LFB25068:
	.cfi_startproc
	endbr64
	movq	12448(%rdi), %rax
	testq	%rax, %rax
	je	.L1728
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1728
	movl	$1, %eax
	cmpq	320(%rdi), %rsi
	je	.L1725
	movq	12568(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L1725
	cmpq	%rcx, %rdx
	setb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1728:
	xorl	%eax, %eax
.L1725:
	ret
	.cfi_endproc
.LFE25068:
	.size	_ZN2v88internal7Isolate22IsExternalHandlerOnTopENS0_6ObjectE, .-_ZN2v88internal7Isolate22IsExternalHandlerOnTopENS0_6ObjectE
	.section	.text._ZN2v88internal7Isolate25ReportPendingMessagesImplEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate25ReportPendingMessagesImplEb
	.type	_ZN2v88internal7Isolate25ReportPendingMessagesImplEb, @function
_ZN2v88internal7Isolate25ReportPendingMessagesImplEb:
.LFB25069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	12536(%rdi), %r14
	movq	12480(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	96(%rdi), %rax
	movq	%rax, 12536(%rdi)
	cmpq	320(%rdi), %r13
	je	.L1732
	movq	%rdi, %r12
	testb	%sil, %sil
	je	.L1734
	movq	12448(%rdi), %rax
	movzbl	40(%rax), %eax
	andl	$1, %eax
.L1735:
	cmpq	%r14, 96(%r12)
	je	.L1732
	testb	%al, %al
	je	.L1732
	movq	41096(%r12), %rax
	movq	41112(%r12), %rdi
	addl	$1, 41104(%r12)
	movq	41088(%r12), %rbx
	movq	%rax, -104(%rbp)
	testq	%rdi, %rdi
	je	.L1737
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1740
.L1765:
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L1741:
	movq	(%r15), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1743
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L1744:
	movq	96(%r12), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, 12480(%r12)
	call	_ZN2v88internal15JSMessageObject30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%r14), %rax
	leaq	-96(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, 12480(%r12)
	movq	(%r15), %rax
	movslq	75(%rax), %rdx
	movslq	83(%rax), %rcx
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE@PLT
	movq	%rbx, 41088(%r12)
	movq	-104(%rbp), %rax
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rax
	je	.L1732
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L1732:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1763
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1734:
	.cfi_restore_state
	movq	12568(%rdi), %rax
	testq	%rax, %rax
	je	.L1748
	movq	12448(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1732
	movq	32(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L1732
	cmpq	%rdx, %rax
	setnb	%al
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1737:
	movq	%rbx, %r15
	cmpq	-104(%rbp), %rbx
	je	.L1764
.L1739:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%r15)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L1765
.L1740:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L1766
.L1742:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%r14)
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1748:
	movl	$1, %eax
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1743:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L1767
.L1745:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L1744
	.p2align 4,,10
	.p2align 3
.L1764:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r15
	jmp	.L1739
	.p2align 4,,10
	.p2align 3
.L1766:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L1742
	.p2align 4,,10
	.p2align 3
.L1767:
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1745
.L1763:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25069:
	.size	_ZN2v88internal7Isolate25ReportPendingMessagesImplEb, .-_ZN2v88internal7Isolate25ReportPendingMessagesImplEb
	.section	.text._ZN2v88internal7Isolate21ReportPendingMessagesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate21ReportPendingMessagesEv
	.type	_ZN2v88internal7Isolate21ReportPendingMessagesEv, @function
_ZN2v88internal7Isolate21ReportPendingMessagesEv:
.LFB25070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	12480(%rdi), %rdx
	movq	320(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12448(%r12), %rax
	cmpq	%rdi, %rdx
	je	.L1769
	movq	12568(%r12), %rcx
	testq	%rcx, %rcx
	je	.L1769
	testq	%rax, %rax
	je	.L1770
	movq	32(%rax), %rsi
	cmpq	%rsi, %rcx
	jb	.L1770
	testq	%rsi, %rsi
	je	.L1770
.L1771:
	cmpq	%rsi, %rcx
	ja	.L1775
	testq	%rcx, %rcx
	jne	.L1774
.L1775:
	movb	$1, 12545(%r12)
	cmpq	320(%r12), %rdx
	je	.L1830
	movzbl	40(%rax), %ecx
	andl	$-19, %ecx
	orl	$2, %ecx
	movb	%cl, 40(%rax)
	movq	12480(%r12), %rcx
	movq	%rcx, 16(%rax)
	movq	12536(%r12), %rsi
	cmpq	%rsi, 96(%r12)
	je	.L1831
	movq	%rsi, 24(%rax)
	movq	12480(%r12), %r13
	movq	12536(%r12), %rsi
	movq	12448(%r12), %rax
.L1776:
	movq	96(%r12), %rdi
	testq	%rax, %rax
	je	.L1832
	movq	32(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1833
	cmpq	%rdx, 320(%r12)
	je	.L1829
	movq	12568(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1829
	movq	%rdi, 12536(%r12)
	cmpq	%r13, 320(%r12)
	je	.L1768
	cmpq	%rdx, %rcx
	jb	.L1787
.L1788:
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L1768
	cmpq	%rdx, %rax
	setbe	%al
	cmpq	%rsi, 96(%r12)
	jne	.L1834
	.p2align 4,,10
	.p2align 3
.L1768:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1835
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1774:
	.cfi_restore_state
	movb	$0, 12545(%r12)
	movq	12536(%r12), %rsi
	movq	%rdx, %r13
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1829:
	movq	%rdi, 12536(%r12)
	cmpq	%r13, 320(%r12)
	je	.L1768
.L1787:
	movzbl	40(%rax), %eax
	andl	$1, %eax
.L1785:
	cmpq	%rsi, 96(%r12)
	je	.L1768
.L1834:
	testb	%al, %al
	je	.L1768
	movq	41088(%r12), %rax
	movq	41112(%r12), %rdi
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r14
	movq	%rax, -104(%rbp)
	testq	%rdi, %rdi
	je	.L1790
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1791:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1793
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L1794:
	movq	(%r15), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1796
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L1797:
	movq	96(%r12), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, 12480(%r12)
	call	_ZN2v88internal15JSMessageObject30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rbx), %rax
	leaq	-96(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%r8, -112(%rbp)
	movq	%rax, 12480(%r12)
	movq	(%r15), %rax
	movslq	75(%rax), %rdx
	movslq	83(%rax), %rcx
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	movq	-112(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE@PLT
	movq	-104(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1768
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1770:
	movb	$0, 12545(%r12)
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1769:
	testq	%rax, %rax
	je	.L1774
	movq	32(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1774
	cmpq	%rdi, %rdx
	je	.L1775
	movq	12568(%r12), %rcx
	jmp	.L1771
	.p2align 4,,10
	.p2align 3
.L1830:
	andb	$-3, 40(%rax)
	movq	12448(%r12), %rax
	orb	$16, 40(%rax)
	movq	12448(%r12), %rax
	movq	104(%r12), %rcx
	movq	%rcx, 16(%rax)
	movq	12480(%r12), %r13
	movq	12536(%r12), %rsi
	movq	12448(%r12), %rax
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1833:
	movq	%rdi, 12536(%r12)
	cmpq	%r13, 320(%r12)
	je	.L1768
	movq	12568(%r12), %rdx
	testq	%rdx, %rdx
	jne	.L1788
	movl	$1, %eax
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1796:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L1836
.L1798:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L1797
	.p2align 4,,10
	.p2align 3
.L1793:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L1837
.L1795:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rbx)
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	%rax, %r15
	cmpq	%r14, %rax
	je	.L1838
.L1792:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1831:
	movq	12480(%r12), %r13
	movq	12448(%r12), %rax
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1832:
	movq	%rdi, 12536(%r12)
	cmpq	%r13, 320(%r12)
	je	.L1768
	cmpq	$0, 12568(%r12)
	sete	%al
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1838:
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1792
	.p2align 4,,10
	.p2align 3
.L1837:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rbx
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1836:
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1798
.L1835:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25070:
	.size	_ZN2v88internal7Isolate21ReportPendingMessagesEv, .-_ZN2v88internal7Isolate21ReportPendingMessagesEv
	.section	.text._ZN2v88internal7Isolate35ReportPendingMessagesFromJavaScriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate35ReportPendingMessagesFromJavaScriptEv
	.type	_ZN2v88internal7Isolate35ReportPendingMessagesFromJavaScriptEv, @function
_ZN2v88internal7Isolate35ReportPendingMessagesFromJavaScriptEv:
.LFB25071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12568(%rdi), %rax
	movq	(%rax), %rcx
	movq	12448(%rdi), %rax
	testq	%rax, %rax
	je	.L1840
	movq	32(%rax), %rdx
	cmpq	%rcx, %rdx
	ja	.L1840
	testq	%rdx, %rdx
	je	.L1840
	cmpq	%rdx, %rcx
	jbe	.L1869
	movb	$1, 12545(%rdi)
	movzbl	40(%rax), %edx
	andl	$-19, %edx
	orl	$2, %edx
	movb	%dl, 40(%rax)
	movq	12480(%rdi), %rdx
	movq	%rdx, 16(%rax)
	movq	12536(%rdi), %rsi
	cmpq	%rsi, 96(%rdi)
	je	.L1845
	movq	%rsi, 24(%rax)
	movq	12536(%rdi), %rsi
.L1845:
	movq	96(%r12), %rax
	movq	12480(%r12), %r13
	movq	%rax, 12536(%r12)
	cmpq	320(%r12), %r13
	je	.L1839
	cmpq	%rsi, %rax
	je	.L1839
	movq	12448(%r12), %rax
	testb	$1, 40(%rax)
	je	.L1839
	movq	41088(%r12), %rax
	movq	41112(%r12), %rdi
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r14
	movq	%rax, -104(%rbp)
	testq	%rdi, %rdi
	je	.L1847
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1848:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1850
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L1851:
	movq	(%r15), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1853
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L1854:
	movq	96(%r12), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, 12480(%r12)
	call	_ZN2v88internal15JSMessageObject30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rbx), %rax
	leaq	-96(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%r8, -112(%rbp)
	movq	%rax, 12480(%r12)
	movq	(%r15), %rax
	movslq	75(%rax), %rdx
	movslq	83(%rax), %rcx
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	movq	-112(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE@PLT
	movq	-104(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1839
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1840:
	movb	$0, 12545(%r12)
.L1839:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1870
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1869:
	.cfi_restore_state
	movb	$0, 12545(%rdi)
	movq	12536(%rdi), %rsi
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1847:
	movq	%rax, %r15
	cmpq	%r14, %rax
	je	.L1871
.L1849:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1848
	.p2align 4,,10
	.p2align 3
.L1853:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L1872
.L1855:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L1854
	.p2align 4,,10
	.p2align 3
.L1850:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L1873
.L1852:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rbx)
	jmp	.L1851
	.p2align 4,,10
	.p2align 3
.L1873:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rbx
	jmp	.L1852
	.p2align 4,,10
	.p2align 3
.L1872:
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1855
	.p2align 4,,10
	.p2align 3
.L1871:
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1849
.L1870:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25071:
	.size	_ZN2v88internal7Isolate35ReportPendingMessagesFromJavaScriptEv, .-_ZN2v88internal7Isolate35ReportPendingMessagesFromJavaScriptEv
	.section	.text._ZN2v88internal7Isolate27OptionalRescheduleExceptionEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb
	.type	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb, @function
_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb:
.LFB25075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$1456, %rsp
	movq	12480(%rdi), %rdx
	movq	320(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	12448(%rbx), %rax
	cmpq	%rdi, %rdx
	je	.L1875
	movq	12568(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L1875
	testq	%rax, %rax
	je	.L1876
	movq	32(%rax), %rsi
	cmpq	%rsi, %rcx
	jb	.L1876
	testq	%rsi, %rsi
	je	.L1876
.L1877:
	cmpq	%rsi, %rcx
	ja	.L1881
	testq	%rcx, %rcx
	jne	.L1876
.L1881:
	movb	$1, 12545(%rbx)
	cmpq	320(%rbx), %rdx
	je	.L1918
	movzbl	40(%rax), %edx
	andl	$-19, %edx
	orl	$2, %edx
	movb	%dl, 40(%rax)
	movq	12480(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	12536(%rbx), %rdx
	cmpq	%rdx, 96(%rbx)
	je	.L1884
	movq	%rdx, 24(%rax)
.L1884:
	movq	12480(%rbx), %rax
	cmpq	%rax, 320(%rbx)
	je	.L1879
	cmpb	$0, 12545(%rbx)
	je	.L1889
	movq	12448(%rbx), %rax
	testq	%rax, %rax
	je	.L1895
	movq	32(%rax), %r14
.L1890:
	leaq	-1488(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -72(%rbp)
	je	.L1893
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L1893
	cmpq	%r14, 24(%rax)
	ja	.L1893
.L1889:
	movq	96(%rbx), %rdx
	testb	%r12b, %r12b
	jne	.L1892
	movq	12480(%rbx), %rax
.L1886:
	movq	%rax, 12552(%rbx)
	movl	$1, %eax
	movq	%rdx, 12480(%rbx)
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1876:
	movq	320(%rbx), %rax
	movb	$0, 12545(%rbx)
	cmpq	%rax, %rdx
	jne	.L1889
.L1879:
	movq	96(%rbx), %rdx
	testb	%r12b, %r12b
	je	.L1886
	.p2align 4,,10
	.p2align 3
.L1892:
	movb	$0, 12545(%rbx)
	xorl	%eax, %eax
	movq	%rdx, 12480(%rbx)
.L1874:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1919
	addq	$1456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1893:
	.cfi_restore_state
	movq	96(%rbx), %rdx
	jmp	.L1892
	.p2align 4,,10
	.p2align 3
.L1875:
	testq	%rax, %rax
	je	.L1876
	movq	32(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1876
	cmpq	%rdi, %rdx
	je	.L1881
	movq	12568(%rbx), %rcx
	jmp	.L1877
	.p2align 4,,10
	.p2align 3
.L1918:
	andb	$-3, 40(%rax)
	movq	12448(%rbx), %rax
	orb	$16, 40(%rax)
	movq	12448(%rbx), %rax
	movq	104(%rbx), %rdx
	movq	%rdx, 16(%rax)
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1895:
	xorl	%r14d, %r14d
	jmp	.L1890
.L1919:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25075:
	.size	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb, .-_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb
	.section	.text._ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE
	.type	_ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE:
.LFB25076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	12584(%rdi), %r12
	movq	(%rsi), %rsi
	movq	41152(%rdi), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movl	$16, %edi
	movq	%rax, -24(%rbp)
	call	_Znwm@PLT
	movq	-24(%rbp), %xmm0
	movq	%r12, %xmm1
	movq	%rax, 12584(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25076:
	.size	_ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal7Isolate10PopPromiseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate10PopPromiseEv
	.type	_ZN2v88internal7Isolate10PopPromiseEv, @function
_ZN2v88internal7Isolate10PopPromiseEv:
.LFB25077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	12584(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1922
	movq	8(%rdi), %r13
	movl	$16, %esi
	movq	(%rdi), %r12
	call	_ZdlPvm@PLT
	movq	%r13, 12584(%rbx)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	.p2align 4,,10
	.p2align 3
.L1922:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25077:
	.size	_ZN2v88internal7Isolate10PopPromiseEv, .-_ZN2v88internal7Isolate10PopPromiseEv
	.section	.text._ZN2v88internal7Isolate34PromiseHasUserDefinedRejectHandlerENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate34PromiseHasUserDefinedRejectHandlerENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal7Isolate34PromiseHasUserDefinedRejectHandlerENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal7Isolate34PromiseHasUserDefinedRejectHandlerENS0_6HandleINS0_6ObjectEEE:
.LFB25080:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1929
.L1927:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L1927
	jmp	_ZN2v88internal12_GLOBAL__N_142InternalPromiseHasUserDefinedRejectHandlerEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEE
	.cfi_endproc
.LFE25080:
	.size	_ZN2v88internal7Isolate34PromiseHasUserDefinedRejectHandlerENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal7Isolate34PromiseHasUserDefinedRejectHandlerENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal7Isolate24GetPromiseOnStackOnThrowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate24GetPromiseOnStackOnThrowEv
	.type	_ZN2v88internal7Isolate24GetPromiseOnStackOnThrowEv, @function
_ZN2v88internal7Isolate24GetPromiseOnStackOnThrowEv:
.LFB25081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	88(%rdi), %r12
	pushq	%rbx
	subq	$1496, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 12584(%rdi)
	je	.L1983
	movq	%rdi, %rbx
	call	_ZN2v88internal7Isolate23PredictExceptionCatcherEv
	andl	$-3, %eax
	jne	.L1933
.L1983:
	movq	%r12, %rax
.L1932:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1984
	addq	$1496, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1933:
	.cfi_restore_state
	movq	12584(%rbx), %rax
	leaq	-1504(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r12, %r14
	movq	%r13, %rdi
	movq	%rax, -1528(%rbp)
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r15
	testq	%r15, %r15
	je	.L1934
	leaq	-1512(%rbp), %rax
	movq	%rax, -1536(%rbp)
.L1956:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	cmpl	$20, %eax
	ja	.L1935
	movl	$1150992, %edx
	btq	%rax, %rdx
	jnc	.L1935
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_116PredictExceptionEPNS0_15JavaScriptFrameE
	cmpl	$3, %eax
	je	.L1939
	ja	.L1940
	cmpl	$1, %eax
	je	.L1939
	cmpl	$2, %eax
	jne	.L1938
.L1941:
	movq	-1528(%rbp), %rsi
	movq	%r12, %rax
	testq	%rsi, %rsi
	je	.L1932
	movq	(%rsi), %rax
	jmp	.L1932
	.p2align 4,,10
	.p2align 3
.L1940:
	cmpl	$4, %eax
	jne	.L1938
	cmpq	$0, -1528(%rbp)
	je	.L1934
	movq	-1528(%rbp), %rax
	movq	(%rax), %r14
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L1985
.L1954:
	movq	-1528(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -1528(%rbp)
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	%r13, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r15
	testq	%r15, %r15
	jne	.L1956
	.p2align 4,,10
	.p2align 3
.L1934:
	movq	%r14, %rax
	jmp	.L1932
	.p2align 4,,10
	.p2align 3
.L1935:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	cmpl	$13, %eax
	jne	.L1938
	movq	%r15, %rdi
	call	_ZNK2v88internal10StackFrame10LookupCodeEv@PLT
	movq	%rax, -1512(%rbp)
	movq	-1(%rax), %rdx
	cmpw	$69, 11(%rdx)
	jne	.L1938
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$3, %eax
	jne	.L1938
	movq	-1536(%rbp), %rdi
	call	_ZNK2v88internal4Code17has_handler_tableEv@PLT
	testb	%al, %al
	je	.L1938
	movq	-1512(%rbp), %rax
	testb	$64, 43(%rax)
	je	.L1938
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$16, %al
	jne	.L1941
	movq	-1512(%rbp), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$32, %al
	je	.L1938
	.p2align 4,,10
	.p2align 3
.L1939:
	movq	(%r14), %rax
	testb	$1, %al
	je	.L1934
	movq	-1(%rax), %rdx
	cmpw	$1074, 11(%rdx)
	jne	.L1934
	movslq	35(%rax), %rdx
	orl	$8, %edx
	salq	$32, %rdx
	movq	%rdx, 31(%rax)
	jmp	.L1934
	.p2align 4,,10
	.p2align 3
.L1985:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L1954
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_142InternalPromiseHasUserDefinedRejectHandlerEPNS0_7IsolateENS0_6HandleINS0_9JSPromiseEEE
	testb	%al, %al
	jne	.L1934
	jmp	.L1954
.L1984:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25081:
	.size	_ZN2v88internal7Isolate24GetPromiseOnStackOnThrowEv, .-_ZN2v88internal7Isolate24GetPromiseOnStackOnThrowEv
	.section	.text._ZN2v88internal7Isolate41SetCaptureStackTraceForUncaughtExceptionsEbiNS_10StackTrace17StackTraceOptionsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate41SetCaptureStackTraceForUncaughtExceptionsEbiNS_10StackTrace17StackTraceOptionsE
	.type	_ZN2v88internal7Isolate41SetCaptureStackTraceForUncaughtExceptionsEbiNS_10StackTrace17StackTraceOptionsE, @function
_ZN2v88internal7Isolate41SetCaptureStackTraceForUncaughtExceptionsEbiNS_10StackTrace17StackTraceOptionsE:
.LFB25082:
	.cfi_startproc
	endbr64
	movb	%sil, 41064(%rdi)
	movl	%edx, 41068(%rdi)
	movl	%ecx, 41072(%rdi)
	ret
	.cfi_endproc
.LFE25082:
	.size	_ZN2v88internal7Isolate41SetCaptureStackTraceForUncaughtExceptionsEbiNS_10StackTrace17StackTraceOptionsE, .-_ZN2v88internal7Isolate41SetCaptureStackTraceForUncaughtExceptionsEbiNS_10StackTrace17StackTraceOptionsE
	.section	.text._ZN2v88internal7Isolate35SetAbortOnUncaughtExceptionCallbackEPFbPNS_7IsolateEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate35SetAbortOnUncaughtExceptionCallbackEPFbPNS_7IsolateEE
	.type	_ZN2v88internal7Isolate35SetAbortOnUncaughtExceptionCallbackEPFbPNS_7IsolateEE, @function
_ZN2v88internal7Isolate35SetAbortOnUncaughtExceptionCallbackEPFbPNS_7IsolateEE:
.LFB25083:
	.cfi_startproc
	endbr64
	movq	%rsi, 45672(%rdi)
	ret
	.cfi_endproc
.LFE25083:
	.size	_ZN2v88internal7Isolate35SetAbortOnUncaughtExceptionCallbackEPFbPNS_7IsolateEE, .-_ZN2v88internal7Isolate35SetAbortOnUncaughtExceptionCallbackEPFbPNS_7IsolateEE
	.section	.text._ZN2v88internal7Isolate21AreWasmThreadsEnabledENS0_6HandleINS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate21AreWasmThreadsEnabledENS0_6HandleINS0_7ContextEEE
	.type	_ZN2v88internal7Isolate21AreWasmThreadsEnabledENS0_6HandleINS0_7ContextEEE, @function
_ZN2v88internal7Isolate21AreWasmThreadsEnabledENS0_6HandleINS0_7ContextEEE:
.LFB25084:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	41688(%r8), %rax
	testq	%rax, %rax
	je	.L1989
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1989:
	movzbl	_ZN2v88internal30FLAG_experimental_wasm_threadsE(%rip), %eax
	ret
	.cfi_endproc
.LFE25084:
	.size	_ZN2v88internal7Isolate21AreWasmThreadsEnabledENS0_6HandleINS0_7ContextEEE, .-_ZN2v88internal7Isolate21AreWasmThreadsEnabledENS0_6HandleINS0_7ContextEEE
	.section	.text._ZN2v88internal7Isolate19GetIncumbentContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate19GetIncumbentContextEv
	.type	_ZN2v88internal7Isolate19GetIncumbentContextEv, @function
_ZN2v88internal7Isolate19GetIncumbentContextEv:
.LFB25085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-1472(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$1472, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -56(%rbp)
	je	.L1991
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	45784(%r12), %rax
	testq	%rax, %rax
	je	.L2009
	movq	-56(%rbp), %rdi
	movq	8(%rax), %rdx
	testq	%rdi, %rdi
	je	.L1995
	testq	%rdx, %rdx
	je	.L1993
	cmpq	24(%rdi), %rdx
	jbe	.L1995
.L1993:
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1996
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1999:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2010
	addq	$1472, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1995:
	.cfi_restore_state
	movq	(%rax), %rax
	jmp	.L1999
	.p2align 4,,10
	.p2align 3
.L2009:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1993
	jmp	.L1994
	.p2align 4,,10
	.p2align 3
.L1991:
	movq	45784(%r12), %rax
	testq	%rax, %rax
	jne	.L1995
.L1994:
	movq	%r12, %rdi
	call	_ZN2v87Isolate28GetEnteredOrMicrotaskContextEv@PLT
	jmp	.L1999
	.p2align 4,,10
	.p2align 3
.L1996:
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	je	.L2011
.L1998:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1999
	.p2align 4,,10
	.p2align 3
.L2011:
	movq	%r12, %rdi
	movq	%rsi, -1480(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1480(%rbp), %rsi
	jmp	.L1998
.L2010:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25085:
	.size	_ZN2v88internal7Isolate19GetIncumbentContextEv, .-_ZN2v88internal7Isolate19GetIncumbentContextEv
	.section	.text._ZN2v88internal7Isolate13ArchiveThreadEPc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate13ArchiveThreadEPc
	.type	_ZN2v88internal7Isolate13ArchiveThreadEPc, @function
_ZN2v88internal7Isolate13ArchiveThreadEPc:
.LFB25086:
	.cfi_startproc
	endbr64
	movdqu	12448(%rdi), %xmm0
	leaq	192(%rsi), %rax
	movups	%xmm0, (%rsi)
	movdqu	12464(%rdi), %xmm1
	movups	%xmm1, 16(%rsi)
	movdqu	12480(%rdi), %xmm2
	movups	%xmm2, 32(%rsi)
	movdqu	12496(%rdi), %xmm3
	movups	%xmm3, 48(%rsi)
	movdqu	12512(%rdi), %xmm4
	movups	%xmm4, 64(%rsi)
	movdqu	12528(%rdi), %xmm5
	movups	%xmm5, 80(%rsi)
	movdqu	12544(%rdi), %xmm6
	movups	%xmm6, 96(%rsi)
	movdqu	12560(%rdi), %xmm7
	movups	%xmm7, 112(%rsi)
	movdqu	12576(%rdi), %xmm0
	movups	%xmm0, 128(%rsi)
	movdqu	12592(%rdi), %xmm1
	movups	%xmm1, 144(%rsi)
	movdqu	12608(%rdi), %xmm2
	movups	%xmm2, 160(%rsi)
	movdqu	12624(%rdi), %xmm3
	movups	%xmm3, 176(%rsi)
	ret
	.cfi_endproc
.LFE25086:
	.size	_ZN2v88internal7Isolate13ArchiveThreadEPc, .-_ZN2v88internal7Isolate13ArchiveThreadEPc
	.section	.text._ZN2v88internal7Isolate13RestoreThreadEPc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate13RestoreThreadEPc
	.type	_ZN2v88internal7Isolate13RestoreThreadEPc, @function
_ZN2v88internal7Isolate13RestoreThreadEPc:
.LFB25087:
	.cfi_startproc
	endbr64
	movdqu	(%rsi), %xmm0
	leaq	192(%rsi), %rax
	movups	%xmm0, 12448(%rdi)
	movdqu	16(%rsi), %xmm1
	movups	%xmm1, 12464(%rdi)
	movdqu	32(%rsi), %xmm2
	movups	%xmm2, 12480(%rdi)
	movdqu	48(%rsi), %xmm3
	movups	%xmm3, 12496(%rdi)
	movdqu	64(%rsi), %xmm4
	movups	%xmm4, 12512(%rdi)
	movdqu	80(%rsi), %xmm5
	movups	%xmm5, 12528(%rdi)
	movdqu	96(%rsi), %xmm6
	movups	%xmm6, 12544(%rdi)
	movdqu	112(%rsi), %xmm7
	movups	%xmm7, 12560(%rdi)
	movdqu	128(%rsi), %xmm0
	movups	%xmm0, 12576(%rdi)
	movdqu	144(%rsi), %xmm1
	movups	%xmm1, 12592(%rdi)
	movdqu	160(%rsi), %xmm2
	movups	%xmm2, 12608(%rdi)
	movdqu	176(%rsi), %xmm3
	movups	%xmm3, 12624(%rdi)
	ret
	.cfi_endproc
.LFE25087:
	.size	_ZN2v88internal7Isolate13RestoreThreadEPc, .-_ZN2v88internal7Isolate13RestoreThreadEPc
	.section	.text._ZN2v88internal7Isolate17ReleaseSharedPtrsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate17ReleaseSharedPtrsEv
	.type	_ZN2v88internal7Isolate17ReleaseSharedPtrsEv, @function
_ZN2v88internal7Isolate17ReleaseSharedPtrsEv:
.LFB25088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	45688(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	45728(%r12), %rbx
	testq	%rbx, %rbx
	je	.L2015
	.p2align 4,,10
	.p2align 3
.L2016:
	movq	$0, 45728(%r12)
	.p2align 4,,10
	.p2align 3
.L2018:
	movq	24(%rbx), %rdi
	call	*32(%rbx)
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	movl	$48, %esi
	call	_ZdlPvm@PLT
	testq	%rbx, %rbx
	jne	.L2018
	movq	45728(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L2016
.L2015:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE25088:
	.size	_ZN2v88internal7Isolate17ReleaseSharedPtrsEv, .-_ZN2v88internal7Isolate17ReleaseSharedPtrsEv
	.section	.text._ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE
	.type	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE, @function
_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE:
.LFB25089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	45688(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	45728(%rbx), %rax
	testq	%rax, %rax
	je	.L2027
	movq	%r12, 8(%rax)
	movq	45728(%rbx), %rax
.L2027:
	movq	%rax, 16(%r12)
	movq	%r13, %rdi
	movq	%r12, 45728(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE25089:
	.size	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE, .-_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE
	.section	.text._ZN2v88internal7Isolate30UnregisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate30UnregisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE
	.type	_ZN2v88internal7Isolate30UnregisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE, @function
_ZN2v88internal7Isolate30UnregisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE:
.LFB25090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	45688(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L2033
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
.L2034:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L2035
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%rax)
.L2035:
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movups	%xmm0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L2033:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%rax, 45728(%r12)
	jmp	.L2034
	.cfi_endproc
.LFE25090:
	.size	_ZN2v88internal7Isolate30UnregisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE, .-_ZN2v88internal7Isolate30UnregisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE
	.section	.text._ZN2v88internal7Isolate13SetWasmEngineESt10shared_ptrINS0_4wasm10WasmEngineEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate13SetWasmEngineESt10shared_ptrINS0_4wasm10WasmEngineEE
	.type	_ZN2v88internal7Isolate13SetWasmEngineESt10shared_ptrINS0_4wasm10WasmEngineEE, @function
_ZN2v88internal7Isolate13SetWasmEngineESt10shared_ptrINS0_4wasm10WasmEngineEE:
.LFB25091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rsi), %rdi
	movdqu	(%rsi), %xmm0
	movups	%xmm1, (%rsi)
	movq	45760(%r12), %r13
	movups	%xmm0, 45752(%r12)
	testq	%r13, %r13
	je	.L2041
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2042
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L2044
.L2052:
	movq	45752(%r12), %rdi
.L2041:
	addq	$8, %rsp
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm10WasmEngine10AddIsolateEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L2042:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L2052
.L2044:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L2045
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L2046:
	cmpl	$1, %eax
	jne	.L2052
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L2052
	.p2align 4,,10
	.p2align 3
.L2045:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L2046
	.cfi_endproc
.LFE25091:
	.size	_ZN2v88internal7Isolate13SetWasmEngineESt10shared_ptrINS0_4wasm10WasmEngineEE, .-_ZN2v88internal7Isolate13SetWasmEngineESt10shared_ptrINS0_4wasm10WasmEngineEE
	.section	.text._ZN2v88internal7Isolate20PerIsolateThreadDataD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate20PerIsolateThreadDataD2Ev
	.type	_ZN2v88internal7Isolate20PerIsolateThreadDataD2Ev, @function
_ZN2v88internal7Isolate20PerIsolateThreadDataD2Ev:
.LFB25094:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25094:
	.size	_ZN2v88internal7Isolate20PerIsolateThreadDataD2Ev, .-_ZN2v88internal7Isolate20PerIsolateThreadDataD2Ev
	.globl	_ZN2v88internal7Isolate20PerIsolateThreadDataD1Ev
	.set	_ZN2v88internal7Isolate20PerIsolateThreadDataD1Ev,_ZN2v88internal7Isolate20PerIsolateThreadDataD2Ev
	.section	.text._ZN2v88internal7Isolate15ThreadDataTable6LookupENS0_8ThreadIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate15ThreadDataTable6LookupENS0_8ThreadIdE
	.type	_ZN2v88internal7Isolate15ThreadDataTable6LookupENS0_8ThreadIdE, @function
_ZN2v88internal7Isolate15ThreadDataTable6LookupENS0_8ThreadIdE:
.LFB25096:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movslq	%esi, %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%rcx
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r10
	testq	%r8, %r8
	je	.L2054
	movq	(%r8), %r8
	movq	24(%r8), %rdi
	jmp	.L2058
	.p2align 4,,10
	.p2align 3
.L2056:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L2054
	movq	24(%r8), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L2066
.L2058:
	cmpq	%rdi, %r9
	jne	.L2056
	cmpl	8(%r8), %esi
	jne	.L2056
	movq	16(%r8), %r8
.L2054:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2066:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE25096:
	.size	_ZN2v88internal7Isolate15ThreadDataTable6LookupENS0_8ThreadIdE, .-_ZN2v88internal7Isolate15ThreadDataTable6LookupENS0_8ThreadIdE
	.section	.rodata._ZN2v88internal7Isolate15ThreadDataTable6InsertEPNS1_20PerIsolateThreadDataE.str1.1,"aMS",@progbits,1
.LC49:
	.string	"inserted"
	.section	.text._ZN2v88internal7Isolate15ThreadDataTable6InsertEPNS1_20PerIsolateThreadDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate15ThreadDataTable6InsertEPNS1_20PerIsolateThreadDataE
	.type	_ZN2v88internal7Isolate15ThreadDataTable6InsertEPNS1_20PerIsolateThreadDataE, @function
_ZN2v88internal7Isolate15ThreadDataTable6InsertEPNS1_20PerIsolateThreadDataE:
.LFB25097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$24, %rsp
	movl	8(%rsi), %r14d
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r13, 16(%rax)
	movslq	%r14d, %r13
	movq	%rax, %r12
	movq	$0, (%rax)
	movl	%r14d, 8(%rax)
	movq	%r13, %rax
	divq	%rsi
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L2068
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	24(%rcx), %rdi
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2069:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2068
	movq	24(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L2068
.L2071:
	cmpq	%r13, %rdi
	jne	.L2069
	cmpl	8(%rcx), %r14d
	jne	.L2069
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	leaq	.LC49(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2068:
	movq	24(%rbx), %rdx
	leaq	32(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L2072
	movq	(%rbx), %r8
.L2073:
	leaq	(%r8,%r15), %rax
	movq	%r13, 24(%r12)
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2082
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rax
	movq	%r12, (%rax)
.L2083:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2072:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L2104
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2105
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L2075:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L2077
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L2078
	.p2align 4,,10
	.p2align 3
.L2079:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2080:
	testq	%rsi, %rsi
	je	.L2077
.L2078:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	24(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L2079
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L2085
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L2078
	.p2align 4,,10
	.p2align 3
.L2077:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L2081
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L2081:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r14, 8(%rbx)
	divq	%r14
	movq	%r8, (%rbx)
	leaq	0(,%rdx,8), %r15
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2082:
	movq	16(%rbx), %rdx
	movq	%r12, 16(%rbx)
	movq	%rdx, (%r12)
	testq	%rdx, %rdx
	je	.L2084
	movq	24(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r12, (%r8,%rdx,8)
	movq	(%rbx), %rax
	addq	%r15, %rax
.L2084:
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax)
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2085:
	movq	%rdx, %rdi
	jmp	.L2080
	.p2align 4,,10
	.p2align 3
.L2104:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L2075
.L2105:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25097:
	.size	_ZN2v88internal7Isolate15ThreadDataTable6InsertEPNS1_20PerIsolateThreadDataE, .-_ZN2v88internal7Isolate15ThreadDataTable6InsertEPNS1_20PerIsolateThreadDataE
	.section	.text._ZN2v88internal7Isolate40FindOrAllocatePerThreadDataForThisThreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate40FindOrAllocatePerThreadDataForThisThreadEv
	.type	_ZN2v88internal7Isolate40FindOrAllocatePerThreadDataForThisThreadEv, @function
_ZN2v88internal7Isolate40FindOrAllocatePerThreadDataForThisThreadEv:
.LFB24953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	45800(%rbx), %r14
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	45848(%rbx), %rsi
	movslq	%r13d, %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rsi
	movq	45840(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L2111
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movq	24(%rcx), %rdi
	jmp	.L2110
	.p2align 4,,10
	.p2align 3
.L2108:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2111
	movq	24(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L2111
.L2110:
	cmpq	%r8, %rdi
	jne	.L2108
	cmpl	8(%rcx), %r13d
	jne	.L2108
	movq	16(%rcx), %r12
	testq	%r12, %r12
	je	.L2111
.L2112:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2111:
	.cfi_restore_state
	call	_ZN2v84base2OS22AdjustSchedulingParamsEv@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	45840(%rbx), %rdi
	movq	%rbx, (%rax)
	movq	%rax, %rsi
	movq	%rax, %r12
	movl	%r13d, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	call	_ZN2v88internal7Isolate15ThreadDataTable6InsertEPNS1_20PerIsolateThreadDataE
	jmp	.L2112
	.cfi_endproc
.LFE24953:
	.size	_ZN2v88internal7Isolate40FindOrAllocatePerThreadDataForThisThreadEv, .-_ZN2v88internal7Isolate40FindOrAllocatePerThreadDataForThisThreadEv
	.section	.text._ZN2v88internal7Isolate15ThreadDataTable6RemoveEPNS1_20PerIsolateThreadDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate15ThreadDataTable6RemoveEPNS1_20PerIsolateThreadDataE
	.type	_ZN2v88internal7Isolate15ThreadDataTable6RemoveEPNS1_20PerIsolateThreadDataE, @function
_ZN2v88internal7Isolate15ThreadDataTable6RemoveEPNS1_20PerIsolateThreadDataE:
.LFB25117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movslq	8(%rsi), %r9
	movq	8(%rdi), %rcx
	movq	(%rdi), %r15
	movq	%r9, %rax
	divq	%rcx
	leaq	0(,%rdx,8), %rax
	movq	%rax, -64(%rbp)
	addq	%r15, %rax
	movq	(%rax), %r8
	movq	%rax, -56(%rbp)
	testq	%r8, %r8
	je	.L2126
	movq	%rdi, %rbx
	movq	(%r8), %rdi
	movq	%r9, %r14
	movq	%rdx, %r13
	movq	%r8, %r11
	movq	24(%rdi), %rsi
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2127:
	movq	(%rdi), %r10
	testq	%r10, %r10
	je	.L2126
	movq	24(%r10), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r11
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r13
	jne	.L2126
	movq	%r10, %rdi
.L2129:
	cmpq	%rsi, %r9
	jne	.L2127
	cmpl	8(%rdi), %r14d
	jne	.L2127
	movq	(%rdi), %rsi
	cmpq	%r11, %r8
	je	.L2149
	testq	%rsi, %rsi
	je	.L2131
	movq	24(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r13
	je	.L2131
	movq	%r11, (%r15,%rdx,8)
	movq	(%rdi), %rsi
.L2131:
	movq	%rsi, (%r11)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
.L2126:
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L2149:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L2135
	movq	24(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r13
	je	.L2131
	movq	%r11, (%r15,%rdx,8)
	movq	-64(%rbp), %rax
	addq	(%rbx), %rax
	movq	%rax, -56(%rbp)
	movq	(%rax), %rax
.L2130:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L2150
.L2132:
	movq	-56(%rbp), %rax
	movq	$0, (%rax)
	movq	(%rdi), %rsi
	jmp	.L2131
	.p2align 4,,10
	.p2align 3
.L2135:
	movq	%r11, %rax
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2150:
	movq	%rsi, 16(%rbx)
	jmp	.L2132
	.cfi_endproc
.LFE25117:
	.size	_ZN2v88internal7Isolate15ThreadDataTable6RemoveEPNS1_20PerIsolateThreadDataE, .-_ZN2v88internal7Isolate15ThreadDataTable6RemoveEPNS1_20PerIsolateThreadDataE
	.section	.text._ZN2v88internal7Isolate33DiscardPerThreadDataForThisThreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate33DiscardPerThreadDataForThisThreadEv
	.type	_ZN2v88internal7Isolate33DiscardPerThreadDataForThisThreadEv, @function
_ZN2v88internal7Isolate33DiscardPerThreadDataForThisThreadEv:
.LFB24954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal8ThreadId13TryGetCurrentEv@PLT
	cmpl	$-1, %eax
	jne	.L2167
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2167:
	.cfi_restore_state
	leaq	45800(%r12), %r13
	movl	%eax, %ebx
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	45848(%r12), %r8
	movslq	%ebx, %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%r8
	movq	45840(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rdi
	testq	%rax, %rax
	je	.L2157
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L2156
	.p2align 4,,10
	.p2align 3
.L2154:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2157
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %rdi
	jne	.L2157
.L2156:
	cmpq	%rsi, %r9
	jne	.L2154
	cmpl	8(%rcx), %ebx
	jne	.L2154
	movq	16(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L2157
	leaq	45840(%r12), %rdi
	call	_ZN2v88internal7Isolate15ThreadDataTable6RemoveEPNS1_20PerIsolateThreadDataE
.L2157:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE24954:
	.size	_ZN2v88internal7Isolate33DiscardPerThreadDataForThisThreadEv, .-_ZN2v88internal7Isolate33DiscardPerThreadDataForThisThreadEv
	.section	.text._ZN2v88internal7Isolate15ThreadDataTable16RemoveAllThreadsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate15ThreadDataTable16RemoveAllThreadsEv
	.type	_ZN2v88internal7Isolate15ThreadDataTable16RemoveAllThreadsEv, @function
_ZN2v88internal7Isolate15ThreadDataTable16RemoveAllThreadsEv:
.LFB25118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L2169
	.p2align 4,,10
	.p2align 3
.L2173:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2170
	movl	$32, %esi
	call	_ZdlPvm@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2173
.L2172:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	je	.L2169
	.p2align 4,,10
	.p2align 3
.L2174:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2174
.L2169:
	movq	8(%r12), %rax
	movq	(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	popq	%rbx
	movq	$0, 24(%r12)
	movq	$0, 16(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2170:
	.cfi_restore_state
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2173
	jmp	.L2172
	.cfi_endproc
.LFE25118:
	.size	_ZN2v88internal7Isolate15ThreadDataTable16RemoveAllThreadsEv, .-_ZN2v88internal7Isolate15ThreadDataTable16RemoveAllThreadsEv
	.section	.text._ZN2v88internal7Isolate21SetUpFromReadOnlyHeapEPNS0_12ReadOnlyHeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate21SetUpFromReadOnlyHeapEPNS0_12ReadOnlyHeapE
	.type	_ZN2v88internal7Isolate21SetUpFromReadOnlyHeapEPNS0_12ReadOnlyHeapE, @function
_ZN2v88internal7Isolate21SetUpFromReadOnlyHeapEPNS0_12ReadOnlyHeapE:
.LFB25134:
	.cfi_startproc
	endbr64
	movq	%rsi, 40792(%rdi)
	addq	$37592, %rdi
	jmp	_ZN2v88internal4Heap21SetUpFromReadOnlyHeapEPNS0_12ReadOnlyHeapE@PLT
	.cfi_endproc
.LFE25134:
	.size	_ZN2v88internal7Isolate21SetUpFromReadOnlyHeapEPNS0_12ReadOnlyHeapE, .-_ZN2v88internal7Isolate21SetUpFromReadOnlyHeapEPNS0_12ReadOnlyHeapE
	.section	.text._ZN2v88internal7Isolate14page_allocatorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate14page_allocatorEv
	.type	_ZN2v88internal7Isolate14page_allocatorEv, @function
_ZN2v88internal7Isolate14page_allocatorEv:
.LFB25135:
	.cfi_startproc
	endbr64
	movq	37584(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE25135:
	.size	_ZN2v88internal7Isolate14page_allocatorEv, .-_ZN2v88internal7Isolate14page_allocatorEv
	.section	.text._ZN2v88internal7Isolate18CheckIsolateLayoutEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate18CheckIsolateLayoutEv
	.type	_ZN2v88internal7Isolate18CheckIsolateLayoutEv, @function
_ZN2v88internal7Isolate18CheckIsolateLayoutEv:
.LFB25272:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25272:
	.size	_ZN2v88internal7Isolate18CheckIsolateLayoutEv, .-_ZN2v88internal7Isolate18CheckIsolateLayoutEv
	.section	.text._ZN2v88internal7Isolate19ClearSerializerDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate19ClearSerializerDataEv
	.type	_ZN2v88internal7Isolate19ClearSerializerDataEv, @function
_ZN2v88internal7Isolate19ClearSerializerDataEv:
.LFB25273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	41736(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L2190
	movq	(%r12), %rdi
	call	free@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2190:
	movq	$0, 41736(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25273:
	.size	_ZN2v88internal7Isolate19ClearSerializerDataEv, .-_ZN2v88internal7Isolate19ClearSerializerDataEv
	.section	.text._ZN2v88internal7Isolate19LogObjectRelocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate19LogObjectRelocationEv
	.type	_ZN2v88internal7Isolate19LogObjectRelocationEv, @function
_ZN2v88internal7Isolate19LogObjectRelocationEv:
.LFB25280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	41016(%rdi), %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L2198
	cmpb	$0, 41812(%rbx)
	je	.L2204
.L2198:
	movl	$1, %eax
.L2195:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2204:
	.cfi_restore_state
	movq	41016(%rbx), %r12
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2199
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L2198
	cmpq	$0, 80(%r12)
	jne	.L2198
.L2202:
	movq	41480(%rbx), %rax
	testq	%rax, %rax
	je	.L2201
	cmpb	$0, 56(%rax)
	jne	.L2198
.L2201:
	movq	40768(%rbx), %rax
	cmpq	%rax, 40776(%rbx)
	setne	%al
	jmp	.L2195
	.p2align 4,,10
	.p2align 3
.L2199:
	call	*%rax
	testb	%al, %al
	je	.L2202
	jmp	.L2198
	.cfi_endproc
.LFE25280:
	.size	_ZN2v88internal7Isolate19LogObjectRelocationEv, .-_ZN2v88internal7Isolate19LogObjectRelocationEv
	.section	.text._ZN2v88internal7Isolate22SetIsolateThreadLocalsEPS1_PNS1_20PerIsolateThreadDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate22SetIsolateThreadLocalsEPS1_PNS1_20PerIsolateThreadDataE
	.type	_ZN2v88internal7Isolate22SetIsolateThreadLocalsEPS1_PNS1_20PerIsolateThreadDataE, @function
_ZN2v88internal7Isolate22SetIsolateThreadLocalsEPS1_PNS1_20PerIsolateThreadDataE:
.LFB25285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	%rdi, %rsi
	subq	$8, %rsp
	movl	_ZN2v88internal7Isolate12isolate_key_E(%rip), %edi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	movl	_ZN2v88internal7Isolate28per_isolate_thread_data_key_E(%rip), %edi
	addq	$8, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	.cfi_endproc
.LFE25285:
	.size	_ZN2v88internal7Isolate22SetIsolateThreadLocalsEPS1_PNS1_20PerIsolateThreadDataE, .-_ZN2v88internal7Isolate22SetIsolateThreadLocalsEPS1_PNS1_20PerIsolateThreadDataE
	.section	.text._ZN2v88internal7Isolate21InitializeThreadLocalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate21InitializeThreadLocalEv
	.type	_ZN2v88internal7Isolate21InitializeThreadLocalEv, @function
_ZN2v88internal7Isolate21InitializeThreadLocalEv:
.LFB25304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	12448(%rdi), %rdi
	movq	%rbx, %rsi
	subq	$8, %rsp
	call	_ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE@PLT
	movq	96(%rbx), %rax
	movq	%rax, 12480(%rbx)
	movq	%rax, 12536(%rbx)
	movq	%rax, 12552(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25304:
	.size	_ZN2v88internal7Isolate21InitializeThreadLocalEv, .-_ZN2v88internal7Isolate21InitializeThreadLocalEv
	.section	.text._ZN2v88internal7Isolate32SetTerminationOnExternalTryCatchEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate32SetTerminationOnExternalTryCatchEv
	.type	_ZN2v88internal7Isolate32SetTerminationOnExternalTryCatchEv, @function
_ZN2v88internal7Isolate32SetTerminationOnExternalTryCatchEv:
.LFB25305:
	.cfi_startproc
	endbr64
	movq	12448(%rdi), %rax
	testq	%rax, %rax
	je	.L2209
	andb	$-3, 40(%rax)
	movq	12448(%rdi), %rax
	orb	$16, 40(%rax)
	movq	12448(%rdi), %rax
	movq	104(%rdi), %rdx
	movq	%rdx, 16(%rax)
.L2209:
	ret
	.cfi_endproc
.LFE25305:
	.size	_ZN2v88internal7Isolate32SetTerminationOnExternalTryCatchEv, .-_ZN2v88internal7Isolate32SetTerminationOnExternalTryCatchEv
	.section	.text._ZN2v88internal7Isolate43PropagatePendingExceptionToExternalTryCatchEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate43PropagatePendingExceptionToExternalTryCatchEv
	.type	_ZN2v88internal7Isolate43PropagatePendingExceptionToExternalTryCatchEv, @function
_ZN2v88internal7Isolate43PropagatePendingExceptionToExternalTryCatchEv:
.LFB25306:
	.cfi_startproc
	endbr64
	movq	12480(%rdi), %rcx
	movq	320(%rdi), %r8
	movq	12448(%rdi), %rax
	cmpq	%rcx, %r8
	je	.L2215
	movq	12568(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L2215
	testq	%rax, %rax
	je	.L2216
	movq	32(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2216
	cmpq	%rsi, %rdx
	jb	.L2216
.L2217:
	testq	%rdx, %rdx
	je	.L2221
	cmpq	%rdx, %rsi
	jnb	.L2220
.L2221:
	movb	$1, 12545(%rdi)
	cmpq	320(%rdi), %rcx
	je	.L2239
	movzbl	40(%rax), %edx
	movl	$1, %r8d
	andl	$-19, %edx
	orl	$2, %edx
	movb	%dl, 40(%rax)
	movq	12480(%rdi), %rdx
	movq	%rdx, 16(%rax)
	movq	12536(%rdi), %rdx
	cmpq	96(%rdi), %rdx
	je	.L2214
	movq	%rdx, 24(%rax)
.L2214:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2220:
	movl	$1, %r8d
	movb	$0, 12545(%rdi)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2216:
	xorl	%r8d, %r8d
	movb	$0, 12545(%rdi)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2215:
	testq	%rax, %rax
	je	.L2220
	movq	32(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2220
	cmpq	%rcx, %r8
	je	.L2221
	movq	12568(%rdi), %rdx
	jmp	.L2217
	.p2align 4,,10
	.p2align 3
.L2239:
	andb	$-3, 40(%rax)
	movq	12448(%rdi), %rax
	movl	$1, %r8d
	orb	$16, 40(%rax)
	movq	12448(%rdi), %rax
	movq	104(%rdi), %rdx
	movq	%rdx, 16(%rax)
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE25306:
	.size	_ZN2v88internal7Isolate43PropagatePendingExceptionToExternalTryCatchEv, .-_ZN2v88internal7Isolate43PropagatePendingExceptionToExternalTryCatchEv
	.section	.text._ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE
	.type	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE:
.LFB25028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpq	$0, 12624(%rdi)
	je	.L2254
	addl	$1, 41104(%rdi)
	movq	41088(%rdi), %r14
	movq	%rsi, %r13
	movq	41096(%rdi), %rbx
	call	_ZN2v88internal15AccessCheckInfo3GetEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE@PLT
	testq	%rax, %rax
	je	.L2255
	movq	41112(%r12), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2247
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L2248:
	movl	12616(%r12), %r15d
	movl	$2, %esi
	movq	%r13, %rdi
	movl	$6, 12616(%r12)
	call	*12624(%r12)
	movl	%r15d, 12616(%r12)
.L2253:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2240
	movq	%rbx, 41096(%r12)
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L2247:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L2256
.L2249:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L2248
	.p2align 4,,10
	.p2align 3
.L2254:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$79, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate43PropagatePendingExceptionToExternalTryCatchEv
	movq	12480(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L2240
	movq	%rax, 12552(%r12)
	movq	96(%r12), %rax
	movb	$0, 12545(%r12)
	movq	%rax, 12480(%r12)
.L2240:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2255:
	.cfi_restore_state
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$79, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate43PropagatePendingExceptionToExternalTryCatchEv
	movq	12480(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L2253
	movq	%rax, 12552(%r12)
	movq	96(%r12), %rax
	movb	$0, 12545(%r12)
	movq	%rax, 12480(%r12)
	jmp	.L2253
	.p2align 4,,10
	.p2align 3
.L2256:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L2249
	.cfi_endproc
.LFE25028:
	.size	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal7Isolate18InitializeCountersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate18InitializeCountersEv
	.type	_ZN2v88internal7Isolate18InitializeCountersEv, @function
_ZN2v88internal7Isolate18InitializeCountersEv:
.LFB25307:
	.cfi_startproc
	endbr64
	cmpq	$0, 40960(%rdi)
	je	.L2259
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2259:
	jmp	_ZN2v88internal7Isolate18InitializeCountersEv.part.0
	.cfi_endproc
.LFE25307:
	.size	_ZN2v88internal7Isolate18InitializeCountersEv, .-_ZN2v88internal7Isolate18InitializeCountersEv
	.section	.text._ZN2v88internal7Isolate28InitializeLoggingAndCountersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate28InitializeLoggingAndCountersEv
	.type	_ZN2v88internal7Isolate28InitializeLoggingAndCountersEv, @function
_ZN2v88internal7Isolate28InitializeLoggingAndCountersEv:
.LFB25308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	$0, 41016(%rdi)
	je	.L2264
.L2261:
	cmpq	$0, 40960(%r12)
	je	.L2265
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2265:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate18InitializeCountersEv.part.0
	.p2align 4,,10
	.p2align 3
.L2264:
	.cfi_restore_state
	movl	$168, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN2v88internal6LoggerC1EPNS0_7IsolateE@PLT
	movq	%rbx, 41016(%r12)
	jmp	.L2261
	.cfi_endproc
.LFE25308:
	.size	_ZN2v88internal7Isolate28InitializeLoggingAndCountersEv, .-_ZN2v88internal7Isolate28InitializeLoggingAndCountersEv
	.section	.rodata._ZN2v88internal7Isolate29InitializeDefaultEmbeddedBlobEv.str1.1,"aMS",@progbits,1
.LC50:
	.string	"0 == size"
	.section	.text._ZN2v88internal7Isolate29InitializeDefaultEmbeddedBlobEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate29InitializeDefaultEmbeddedBlobEv
	.type	_ZN2v88internal7Isolate29InitializeDefaultEmbeddedBlobEv, @function
_ZN2v88internal7Isolate29InitializeDefaultEmbeddedBlobEv:
.LFB25310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	v8_Default_embedded_blob_(%rip), %rbx
	movl	v8_Default_embedded_blob_size_(%rip), %r12d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, _ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E(%rip)
	je	.L2267
	movzbl	_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L2287
.L2268:
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	_ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E(%rip), %rax
	testq	%rax, %rax
	je	.L2270
	addl	$1, _ZN2v88internal12_GLOBAL__N_127current_embedded_blob_refs_E(%rip)
	movl	_ZN2v88internal12_GLOBAL__N_126sticky_embedded_blob_size_E(%rip), %r12d
	movq	%rax, %rbx
.L2270:
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L2267:
	testq	%rbx, %rbx
	je	.L2288
	movq	%rbx, 45528(%r13)
	movl	%r12d, 45536(%r13)
	movq	%rbx, _ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E(%rip)
	movl	%r12d, _ZN2v88internal12_GLOBAL__N_127current_embedded_blob_size_E(%rip)
.L2266:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2289
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2287:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rax
	leaq	-80(%rbp), %r14
	movq	%rax, -72(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r14, %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L2268
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2268
	.p2align 4,,10
	.p2align 3
.L2288:
	testl	%r12d, %r12d
	je	.L2266
	leaq	.LC50(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2289:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25310:
	.size	_ZN2v88internal7Isolate29InitializeDefaultEmbeddedBlobEv, .-_ZN2v88internal7Isolate29InitializeDefaultEmbeddedBlobEv
	.section	.text._ZN2v88internal7IsolateC2ESt10unique_ptrINS0_16IsolateAllocatorESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7IsolateC2ESt10unique_ptrINS0_16IsolateAllocatorESt14default_deleteIS3_EE
	.type	_ZN2v88internal7IsolateC2ESt10unique_ptrINS0_16IsolateAllocatorESt14default_deleteIS3_EE, @function
_ZN2v88internal7IsolateC2ESt10unique_ptrINS0_16IsolateAllocatorESt14default_deleteIS3_EE:
.LFB25270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movl	$600, %ecx
	movl	$12424, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	movq	%r13, %rax
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$56, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movups	%xmm0, -56(%rdi)
	movups	%xmm0, -40(%rdi)
	movdqa	.LC51(%rip), %xmm0
	movq	$0, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	pxor	%xmm0, %xmm0
	rep stosq
	xorl	%edi, %edi
	movq	$0, 12440(%r12)
	movw	%di, 12544(%r12)
	leaq	12640(%r12), %rdi
	movq	$0, 12464(%r12)
	movl	$-1, 12472(%r12)
	movq	$0, 12600(%r12)
	movq	$0, 12608(%r12)
	movl	$6, 12616(%r12)
	movq	$0, 12624(%r12)
	movq	$0, 12632(%r12)
	movups	%xmm0, 12448(%r12)
	movups	%xmm0, 12480(%r12)
	movups	%xmm0, 12496(%r12)
	movups	%xmm0, 12512(%r12)
	movups	%xmm0, 12528(%r12)
	movups	%xmm0, 12552(%r12)
	movups	%xmm0, 12568(%r12)
	movups	%xmm0, 12584(%r12)
	call	memset@PLT
	leaq	25064(%r12), %rdi
	movl	$12424, %edx
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	pxor	%xmm0, %xmm0
	movdqa	.LC52(%rip), %xmm1
	movq	$0, 37488(%r12)
	leaq	37592(%r12), %rbx
	movq	%rax, 37584(%r12)
	movq	%rbx, %rdi
	movq	%r12, 37512(%r12)
	movq	$0, 37552(%r12)
	movq	$0, 37560(%r12)
	movb	$1, 37568(%r12)
	movups	%xmm0, 37496(%r12)
	movups	%xmm1, 37520(%r12)
	movups	%xmm1, 37536(%r12)
	call	_ZN2v88internal4HeapC1Ev@PLT
	movl	$1, %eax
	movq	$0, 40792(%r12)
	lock xaddl	%eax, _ZN2v88internal12_GLOBAL__N_115isolate_counterE(%rip)
	movl	$13, %ecx
	pxor	%xmm0, %xmm0
	movl	%eax, 40800(%r12)
	movq	%r13, %rax
	leaq	40832(%r12), %rdi
	movq	$0, 40808(%r12)
	movl	$0, 40816(%r12)
	movq	$0, 40824(%r12)
	rep stosq
	leaq	40976(%r12), %rdi
	movq	$0, 40968(%r12)
	movups	%xmm0, 40936(%r12)
	movups	%xmm0, 40952(%r12)
	call	_ZN2v84base14RecursiveMutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	cmpb	$0, _ZN2v88internal21FLAG_trace_zone_statsE(%rip)
	movabsq	$64424509440, %rax
	movb	$0, 41048(%r12)
	movq	$0, 41056(%r12)
	movb	$0, 41064(%r12)
	movq	%rax, 41068(%r12)
	movq	$0, 41080(%r12)
	movups	%xmm0, 41016(%r12)
	movups	%xmm0, 41032(%r12)
	movups	%xmm0, 41120(%r12)
	je	.L2291
	movl	$56, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal26VerboseAccountingAllocatorE(%rip), %rsi
	pxor	%xmm0, %xmm0
	movq	%rsi, (%rax)
	movq	%rbx, 24(%rax)
	movq	$262144, 48(%rax)
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 32(%rax)
.L2292:
	movq	%rax, 41136(%r12)
	pxor	%xmm0, %xmm0
	leaq	41320(%r12), %rdi
	xorl	%r14d, %r14d
	movq	$0, 41144(%r12)
	leaq	_ZN2v88internal11NoExtensionERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %r13
	movq	$0, 41152(%r12)
	movq	$0, 41160(%r12)
	movq	$0, 41168(%r12)
	movq	$0, 41176(%r12)
	movq	%r12, 41184(%r12)
	movb	$0, 41192(%r12)
	movl	$0, 41196(%r12)
	movl	$1, 41264(%r12)
	movq	$0, 41272(%r12)
	movq	$0, 41280(%r12)
	movups	%xmm0, 41200(%r12)
	movups	%xmm0, 41216(%r12)
	movups	%xmm0, 41232(%r12)
	movups	%xmm0, 41248(%r12)
	movups	%xmm0, 41288(%r12)
	movups	%xmm0, 41304(%r12)
	call	_ZN2v84base5MutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	leaq	41384(%r12), %rax
	movq	%rax, 41368(%r12)
	leaq	41448(%r12), %rax
	movq	%rax, 41400(%r12)
	xorl	%eax, %eax
	movq	$0x000000000, 41360(%r12)
	movq	$0, 41376(%r12)
	movb	$0, 41384(%r12)
	movq	$1, 41408(%r12)
	movq	$0, 41416(%r12)
	movq	$0, 41424(%r12)
	movl	$0x3f800000, 41432(%r12)
	movq	$0, 41440(%r12)
	movq	$0, 41448(%r12)
	movl	$16777216, 41456(%r12)
	movw	%ax, 41460(%r12)
	movq	$0x000000000, 41464(%r12)
	movups	%xmm0, 41472(%r12)
	call	_Znwm@PLT
	movl	$12, %ecx
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%r14, %rax
	rep stosq
	leaq	48(%rbx), %rax
	movq	$1, 8(%rbx)
	leaq	56(%rbx), %rdi
	movq	%rax, (%rbx)
	movl	$0x3f800000, 32(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	%rbx, 41488(%r12)
	movq	$0, 41496(%r12)
	movq	$0, 41504(%r12)
	movq	$0, 41512(%r12)
	movq	$0, 41520(%r12)
	movq	$0, 41528(%r12)
	movq	$0, 41536(%r12)
	movq	$8, 41544(%r12)
	movups	%xmm0, 41552(%r12)
	movups	%xmm0, 41568(%r12)
	movups	%xmm0, 41584(%r12)
	movups	%xmm0, 41600(%r12)
	call	_Znwm@PLT
	movq	41544(%r12), %rdx
	movl	$512, %edi
	movq	%rax, 41536(%r12)
	leaq	-4(,%rdx,4), %rbx
	andq	$-8, %rbx
	addq	%rax, %rbx
	call	_Znwm@PLT
	movq	%rbx, 41576(%r12)
	pxor	%xmm0, %xmm0
	leaq	45552(%r12), %rdi
	leaq	512(%rax), %rdx
	movq	%rax, (%rbx)
	movq	%rax, %xmm1
	movq	%rdx, 41568(%r12)
	punpcklqdq	%xmm1, %xmm1
	movq	%rbx, 41608(%r12)
	movq	%rax, 41592(%r12)
	movq	%rdx, 41600(%r12)
	movq	%rax, 41584(%r12)
	movq	$0, 41720(%r12)
	movl	$0, 45424(%r12)
	movb	$0, 45428(%r12)
	movl	$0, 45432(%r12)
	movq	$0, 45488(%r12)
	movq	$0, 45528(%r12)
	movl	$0, 45536(%r12)
	movq	$0, 45544(%r12)
	movups	%xmm1, 41552(%r12)
	movups	%xmm0, 45408(%r12)
	movups	%xmm0, 45440(%r12)
	movups	%xmm0, 45456(%r12)
	movups	%xmm0, 45472(%r12)
	movups	%xmm0, 45496(%r12)
	movups	%xmm0, 45512(%r12)
	call	_ZN2v84base17ConditionVariableC1Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movl	$160, %edi
	movw	%dx, 45632(%r12)
	movq	$0, 45616(%r12)
	movq	$0, 45624(%r12)
	movups	%xmm0, 45600(%r12)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal21CancelableTaskManagerC1Ev@PLT
	xorl	%ecx, %ecx
	movq	%rbx, 45640(%r12)
	leaq	45688(%r12), %rdi
	movw	%cx, 45664(%r12)
	movq	$0, 45648(%r12)
	movq	$0, 45656(%r12)
	movl	$0, 45668(%r12)
	movq	$0, 45672(%r12)
	movb	$1, 45680(%r12)
	call	_ZN2v84base5MutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 45728(%r12)
	leaq	45800(%r12), %rdi
	movq	$0, 45784(%r12)
	movq	$0, 45792(%r12)
	movups	%xmm0, 45736(%r12)
	movups	%xmm0, 45752(%r12)
	movups	%xmm0, 45768(%r12)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	45888(%r12), %rax
	movl	$80, %edi
	movq	$1, 45848(%r12)
	movq	%rax, 45840(%r12)
	movq	$0, 45856(%r12)
	movq	$0, 45864(%r12)
	movl	$0x3f800000, 45872(%r12)
	movq	$0, 45880(%r12)
	movq	$0, 45888(%r12)
	movq	$0, 45896(%r12)
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal13ThreadManagerC1EPNS0_7IsolateE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %xmm1
	xorl	%esi, %esi
	movzbl	_ZN2v88internal23FLAG_detailed_line_infoE(%rip), %eax
	punpcklqdq	%xmm1, %xmm1
	leaq	41868(%r12), %rdi
	movups	%xmm0, 41088(%r12)
	andq	$-8, %rdi
	movups	%xmm0, 41616(%r12)
	leaq	42380(%r12), %rdx
	movb	%al, 41858(%r12)
	movl	%r12d, %eax
	andq	$-8, %rdx
	subl	%edi, %eax
	movups	%xmm0, 41632(%r12)
	leal	42372(%rax), %ecx
	movq	%r14, %rax
	movups	%xmm0, 41648(%r12)
	movups	%xmm0, 41680(%r12)
	shrl	$3, %ecx
	movups	%xmm0, 41704(%r12)
	movups	%xmm0, 41728(%r12)
	movups	%xmm0, 41744(%r12)
	movups	%xmm0, 41760(%r12)
	pxor	%xmm0, %xmm0
	movw	%si, 41856(%r12)
	leaq	43404(%r12), %rsi
	movq	%rbx, 41168(%r12)
	andq	$-8, %rsi
	movq	$0, 41104(%r12)
	movq	$0, 41112(%r12)
	movq	$0, 41696(%r12)
	movq	$0, 41720(%r12)
	movl	$-1, 41776(%r12)
	movq	$0, 41784(%r12)
	movq	$0, 41792(%r12)
	movq	$0, 41800(%r12)
	movl	$0, 41808(%r12)
	movb	$0, 41812(%r12)
	movq	$0, 41816(%r12)
	movb	$0, 41824(%r12)
	movl	$0, 41828(%r12)
	movq	$0, 41848(%r12)
	movq	$0, 41860(%r12)
	movq	$0, 42364(%r12)
	movups	%xmm1, 41664(%r12)
	movups	%xmm0, 41832(%r12)
	rep stosq
	movl	%r12d, %eax
	movq	%rdx, %rdi
	movq	$0, 42372(%r12)
	movq	$0, 43388(%r12)
	subl	%edx, %eax
	leaq	44408(%r12), %rdx
	leal	43396(%rax), %ecx
	movq	%r14, %rax
	andq	$-8, %rdx
	shrl	$3, %ecx
	rep stosq
	movl	%r12d, %eax
	movq	%rsi, %rdi
	movq	$0, 43396(%r12)
	movq	$0, 44392(%r12)
	subl	%esi, %eax
	leal	44400(%rax), %ecx
	movq	%r14, %rax
	shrl	$3, %ecx
	rep stosq
	movl	%r12d, %eax
	movq	%rdx, %rdi
	movq	$0, 44400(%r12)
	movq	$0, 45396(%r12)
	subl	%edx, %eax
	leal	45404(%rax), %ecx
	movq	%r14, %rax
	shrl	$3, %ecx
	rep stosq
	cmpq	$0, 41016(%r12)
	je	.L2296
.L2293:
	cmpq	$0, 40960(%r12)
	je	.L2297
.L2294:
	movl	$144, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN2v88internal5DebugC1EPNS0_7IsolateE@PLT
	movq	%rbx, 41472(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate29InitializeDefaultEmbeddedBlobEv
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14MicrotaskQueue26SetUpDefaultMicrotaskQueueEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L2291:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rsi
	pxor	%xmm0, %xmm0
	movq	%rsi, (%rax)
	movups	%xmm0, 8(%rax)
	jmp	.L2292
	.p2align 4,,10
	.p2align 3
.L2297:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate18InitializeCountersEv.part.0
	jmp	.L2294
	.p2align 4,,10
	.p2align 3
.L2296:
	movl	$168, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN2v88internal6LoggerC1EPNS0_7IsolateE@PLT
	movq	%rbx, 41016(%r12)
	jmp	.L2293
	.cfi_endproc
.LFE25270:
	.size	_ZN2v88internal7IsolateC2ESt10unique_ptrINS0_16IsolateAllocatorESt14default_deleteIS3_EE, .-_ZN2v88internal7IsolateC2ESt10unique_ptrINS0_16IsolateAllocatorESt14default_deleteIS3_EE
	.globl	_ZN2v88internal7IsolateC1ESt10unique_ptrINS0_16IsolateAllocatorESt14default_deleteIS3_EE
	.set	_ZN2v88internal7IsolateC1ESt10unique_ptrINS0_16IsolateAllocatorESt14default_deleteIS3_EE,_ZN2v88internal7IsolateC2ESt10unique_ptrINS0_16IsolateAllocatorESt14default_deleteIS3_EE
	.section	.text._ZN2v88internal7Isolate3NewENS0_21IsolateAllocationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate3NewENS0_21IsolateAllocationModeE
	.type	_ZN2v88internal7Isolate3NewENS0_21IsolateAllocationModeE, @function
_ZN2v88internal7Isolate3NewENS0_21IsolateAllocationModeE:
.LFB25131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edi, %r12d
	movl	$48, %edi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movl	%r12d, %esi
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN2v88internal16IsolateAllocatorC1ENS0_21IsolateAllocationModeE@PLT
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	movq	%rbx, -48(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal7IsolateC1ESt10unique_ptrINS0_16IsolateAllocatorESt14default_deleteIS3_EE
	movq	-48(%rbp), %r12
	testq	%r12, %r12
	je	.L2298
	movq	%r12, %rdi
	call	_ZN2v88internal16IsolateAllocatorD1Ev@PLT
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2298:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2305
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2305:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25131:
	.size	_ZN2v88internal7Isolate3NewENS0_21IsolateAllocationModeE, .-_ZN2v88internal7Isolate3NewENS0_21IsolateAllocationModeE
	.section	.rodata._ZN2v88internal7Isolate24CreateAndSetEmbeddedBlobEv.str1.8,"aMS",@progbits,1
	.align 8
.LC55:
	.string	"embedded_blob() == StickyEmbeddedBlob()"
	.align 8
.LC56:
	.string	"CurrentEmbeddedBlob() == StickyEmbeddedBlob()"
	.align 8
.LC57:
	.string	"0 == current_embedded_blob_refs_"
	.section	.text._ZN2v88internal7Isolate24CreateAndSetEmbeddedBlobEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate24CreateAndSetEmbeddedBlobEv
	.type	_ZN2v88internal7Isolate24CreateAndSetEmbeddedBlobEv, @function
_ZN2v88internal7Isolate24CreateAndSetEmbeddedBlobEv:
.LFB25311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L2330
.L2307:
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	45776(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2309
	movq	(%rdi), %rax
	leaq	41184(%r12), %rsi
	call	*24(%rax)
.L2309:
	movq	_ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E(%rip), %rax
	testq	%rax, %rax
	je	.L2310
	cmpq	45528(%r12), %rax
	jne	.L2331
	movq	_ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E(%rip), %rdx
	leaq	-112(%rbp), %r15
	cmpq	%rdx, %rax
	jne	.L2332
.L2312:
	movq	41088(%r12), %rax
	movq	41096(%r12), %r14
	xorl	%ebx, %ebx
	leaq	41184(%r12), %r13
	addl	$1, 41104(%r12)
	movq	%rax, -144(%rbp)
	movl	_ZN2v88internal12_GLOBAL__N_127current_embedded_blob_size_E(%rip), %eax
	movq	_ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E(%rip), %rdx
	movl	%eax, -104(%rbp)
	movq	%rdx, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L2315:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movl	%ebx, %esi
	movq	%r13, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	-136(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory23NewOffHeapTrampolineForENS0_6HandleINS0_4CodeEEEm@PLT
	movl	%ebx, %esi
	movq	%r13, %rdi
	addl	$1, %ebx
	movq	(%rax), %rdx
	call	_ZN2v88internal8Builtins11set_builtinEiNS0_4CodeE@PLT
	cmpl	$1553, %ebx
	jne	.L2315
	subl	$1, 41104(%r12)
	movq	-144(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L2316
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2316:
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2333
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2330:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rax
	leaq	-96(%rbp), %r13
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L2307
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2307
	.p2align 4,,10
	.p2align 3
.L2310:
	leaq	-112(%rbp), %r15
	leaq	-116(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17InstructionStream30CreateOffHeapInstructionStreamEPNS0_7IsolateEPPhPj@PLT
	movl	_ZN2v88internal12_GLOBAL__N_127current_embedded_blob_refs_E(%rip), %eax
	testl	%eax, %eax
	jne	.L2334
	movq	-112(%rbp), %rax
	movl	-116(%rbp), %edx
	testq	%rax, %rax
	je	.L2335
	movq	%rax, 45528(%r12)
	movl	%edx, 45536(%r12)
	movq	%rax, _ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E(%rip)
	movl	%edx, _ZN2v88internal12_GLOBAL__N_127current_embedded_blob_size_E(%rip)
	movq	%rax, _ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E(%rip)
	movl	-116(%rbp), %eax
	addl	$1, _ZN2v88internal12_GLOBAL__N_127current_embedded_blob_refs_E(%rip)
	movl	%eax, _ZN2v88internal12_GLOBAL__N_126sticky_embedded_blob_size_E(%rip)
	jmp	.L2312
	.p2align 4,,10
	.p2align 3
.L2331:
	leaq	.LC55(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2332:
	leaq	.LC56(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2334:
	leaq	.LC57(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2335:
	leaq	.LC16(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L2333:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25311:
	.size	_ZN2v88internal7Isolate24CreateAndSetEmbeddedBlobEv, .-_ZN2v88internal7Isolate24CreateAndSetEmbeddedBlobEv
	.section	.text._ZN2v88internal7Isolate20TearDownEmbeddedBlobEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate20TearDownEmbeddedBlobEv
	.type	_ZN2v88internal7Isolate20TearDownEmbeddedBlobEv, @function
_ZN2v88internal7Isolate20TearDownEmbeddedBlobEv:
.LFB25312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	_ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E(%rip), %rax
	testq	%rax, %rax
	je	.L2336
	movq	%rdi, %r12
	cmpq	%rax, 45528(%rdi)
	jne	.L2354
	movq	_ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L2355
	movzbl	_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L2356
.L2340:
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	subl	$1, _ZN2v88internal12_GLOBAL__N_127current_embedded_blob_refs_E(%rip)
	je	.L2357
.L2342:
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L2336:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2358
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2357:
	.cfi_restore_state
	cmpb	$0, _ZN2v88internal12_GLOBAL__N_133enable_embedded_blob_refcounting_E(%rip)
	je	.L2342
	movq	45528(%r12), %rdi
	movl	45536(%r12), %esi
	call	_ZN2v88internal17InstructionStream28FreeOffHeapInstructionStreamEPhj@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate17ClearEmbeddedBlobEv
	jmp	.L2342
	.p2align 4,,10
	.p2align 3
.L2356:
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rax
	leaq	-64(%rbp), %r13
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L2340
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2340
	.p2align 4,,10
	.p2align 3
.L2354:
	leaq	.LC55(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2355:
	leaq	.LC56(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25312:
	.size	_ZN2v88internal7Isolate20TearDownEmbeddedBlobEv, .-_ZN2v88internal7Isolate20TearDownEmbeddedBlobEv
	.section	.text._ZN2v88internal7Isolate37AddCrashKeysForIsolateAndHeapPointersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate37AddCrashKeysForIsolateAndHeapPointersEv
	.type	_ZN2v88internal7Isolate37AddCrashKeysForIsolateAndHeapPointersEv, @function
_ZN2v88internal7Isolate37AddCrashKeysForIsolateAndHeapPointersEv:
.LFB25316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	45896(%rdi), %r13
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internalL15AddressToStringEm
	xorl	%edi, %edi
	movq	%r12, %rsi
	call	*%r13
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %r13
	cmpq	%r13, %rdi
	je	.L2360
	call	_ZdlPv@PLT
.L2360:
	movq	37896(%rbx), %rax
	movq	%r12, %rdi
	movq	45896(%rbx), %r14
	movq	32(%rax), %rsi
	call	_ZN2v88internalL15AddressToStringEm
	movl	$1, %edi
	movq	%r12, %rsi
	call	*%r14
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2361
	call	_ZdlPv@PLT
.L2361:
	movq	37864(%rbx), %rax
	movq	%r12, %rdi
	movq	45896(%rbx), %r14
	movq	32(%rax), %rsi
	call	_ZN2v88internalL15AddressToStringEm
	movl	$2, %edi
	movq	%r12, %rsi
	call	*%r14
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2362
	call	_ZdlPv@PLT
.L2362:
	movq	37856(%rbx), %rax
	movq	%r12, %rdi
	movq	45896(%rbx), %r14
	movq	32(%rax), %rsi
	call	_ZN2v88internalL15AddressToStringEm
	movl	$3, %edi
	movq	%r12, %rsi
	call	*%r14
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2359
	call	_ZdlPv@PLT
.L2359:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2366
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2366:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25316:
	.size	_ZN2v88internal7Isolate37AddCrashKeysForIsolateAndHeapPointersEv, .-_ZN2v88internal7Isolate37AddCrashKeysForIsolateAndHeapPointersEv
	.section	.text._ZN2v88internal7Isolate5EnterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate5EnterEv
	.type	_ZN2v88internal7Isolate5EnterEv, @function
_ZN2v88internal7Isolate5EnterEv:
.LFB25324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	_ZN2v88internal7Isolate28per_isolate_thread_data_key_E(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2370
	movq	(%rax), %r13
	cmpq	%rbx, %r13
	je	.L2372
.L2368:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate40FindOrAllocatePerThreadDataForThisThreadEv
	movl	$32, %edi
	movq	%rax, %r14
	call	_Znwm@PLT
	movq	40808(%rbx), %rdx
	movq	%r13, %xmm1
	movq	%r12, %xmm0
	movq	%rax, 40808(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movl	_ZN2v88internal7Isolate12isolate_key_E(%rip), %edi
	movq	%rbx, %rsi
	movl	$1, (%rax)
	movq	%rdx, 24(%rax)
	movups	%xmm0, 8(%rax)
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	movl	_ZN2v88internal7Isolate28per_isolate_thread_data_key_E(%rip), %edi
	movq	%r14, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	movl	8(%r14), %eax
	movl	%eax, 12472(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2370:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L2368
	.p2align 4,,10
	.p2align 3
.L2372:
	movq	40808(%r13), %rax
	addl	$1, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25324:
	.size	_ZN2v88internal7Isolate5EnterEv, .-_ZN2v88internal7Isolate5EnterEv
	.section	.text._ZN2v88internal7Isolate4ExitEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate4ExitEv
	.type	_ZN2v88internal7Isolate4ExitEv, @function
_ZN2v88internal7Isolate4ExitEv:
.LFB25325:
	.cfi_startproc
	endbr64
	movq	40808(%rdi), %rdx
	movl	(%rdx), %eax
	subl	$1, %eax
	movl	%eax, (%rdx)
	testl	%eax, %eax
	jle	.L2378
	ret
	.p2align 4,,10
	.p2align 3
.L2378:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$32, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	40808(%rdi), %r8
	movq	24(%r8), %rax
	movq	%rax, 40808(%rdi)
	movq	%r8, %rdi
	movq	16(%r8), %r13
	movq	8(%r8), %r12
	call	_ZdlPvm@PLT
	movl	_ZN2v88internal7Isolate12isolate_key_E(%rip), %edi
	movq	%r13, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	movl	_ZN2v88internal7Isolate28per_isolate_thread_data_key_E(%rip), %edi
	movq	%r12, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	.cfi_endproc
.LFE25325:
	.size	_ZN2v88internal7Isolate4ExitEv, .-_ZN2v88internal7Isolate4ExitEv
	.section	.text._ZN2v88internal7Isolate19LinkDeferredHandlesEPNS0_15DeferredHandlesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate19LinkDeferredHandlesEPNS0_15DeferredHandlesE
	.type	_ZN2v88internal7Isolate19LinkDeferredHandlesEPNS0_15DeferredHandlesE, @function
_ZN2v88internal7Isolate19LinkDeferredHandlesEPNS0_15DeferredHandlesE:
.LFB25326:
	.cfi_startproc
	endbr64
	movq	45408(%rdi), %rax
	movq	%rax, 24(%rsi)
	testq	%rax, %rax
	je	.L2380
	movq	%rsi, 32(%rax)
.L2380:
	movq	%rsi, 45408(%rdi)
	ret
	.cfi_endproc
.LFE25326:
	.size	_ZN2v88internal7Isolate19LinkDeferredHandlesEPNS0_15DeferredHandlesE, .-_ZN2v88internal7Isolate19LinkDeferredHandlesEPNS0_15DeferredHandlesE
	.section	.text._ZN2v88internal7Isolate21UnlinkDeferredHandlesEPNS0_15DeferredHandlesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate21UnlinkDeferredHandlesEPNS0_15DeferredHandlesE
	.type	_ZN2v88internal7Isolate21UnlinkDeferredHandlesEPNS0_15DeferredHandlesE, @function
_ZN2v88internal7Isolate21UnlinkDeferredHandlesEPNS0_15DeferredHandlesE:
.LFB25327:
	.cfi_startproc
	endbr64
	movq	24(%rsi), %rdx
	cmpq	%rsi, 45408(%rdi)
	je	.L2394
.L2385:
	movq	32(%rsi), %rax
	testq	%rdx, %rdx
	je	.L2386
	movq	%rax, 32(%rdx)
	movq	32(%rsi), %rax
.L2386:
	testq	%rax, %rax
	je	.L2384
	movq	24(%rsi), %rdx
	movq	%rdx, 24(%rax)
.L2384:
	ret
	.p2align 4,,10
	.p2align 3
.L2394:
	movq	%rdx, 45408(%rdi)
	movq	24(%rsi), %rdx
	jmp	.L2385
	.cfi_endproc
.LFE25327:
	.size	_ZN2v88internal7Isolate21UnlinkDeferredHandlesEPNS0_15DeferredHandlesE, .-_ZN2v88internal7Isolate21UnlinkDeferredHandlesEPNS0_15DeferredHandlesE
	.section	.text._ZN2v88internal7Isolate27AbortConcurrentOptimizationENS0_16BlockingBehaviorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate27AbortConcurrentOptimizationENS0_16BlockingBehaviorE
	.type	_ZN2v88internal7Isolate27AbortConcurrentOptimizationENS0_16BlockingBehaviorE, @function
_ZN2v88internal7Isolate27AbortConcurrentOptimizationENS0_16BlockingBehaviorE:
.LFB25341:
	.cfi_startproc
	endbr64
	movq	45416(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2395
	jmp	_ZN2v88internal27OptimizingCompileDispatcher5FlushENS0_16BlockingBehaviorE@PLT
	.p2align 4,,10
	.p2align 3
.L2395:
	ret
	.cfi_endproc
.LFE25341:
	.size	_ZN2v88internal7Isolate27AbortConcurrentOptimizationENS0_16BlockingBehaviorE, .-_ZN2v88internal7Isolate27AbortConcurrentOptimizationENS0_16BlockingBehaviorE
	.section	.text._ZN2v88internal7Isolate18GetTurboStatisticsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate18GetTurboStatisticsEv
	.type	_ZN2v88internal7Isolate18GetTurboStatisticsEv, @function
_ZN2v88internal7Isolate18GetTurboStatisticsEv:
.LFB25342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	41760(%rdi), %r12
	testq	%r12, %r12
	je	.L2400
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2400:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	$208, %edi
	call	_ZN2v88internal8MallocednwEm@PLT
	movl	$26, %ecx
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%r12, %rdi
	rep stosq
	leaq	48(%r12), %rax
	leaq	168(%r12), %rdi
	movq	%rax, 32(%r12)
	leaq	80(%r12), %rax
	movq	%rax, 96(%r12)
	movq	%rax, 104(%r12)
	leaq	128(%r12), %rax
	movq	%rax, 144(%r12)
	movq	%rax, 152(%r12)
	call	_ZN2v84base5MutexC1Ev@PLT
	movq	%r12, 41760(%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25342:
	.size	_ZN2v88internal7Isolate18GetTurboStatisticsEv, .-_ZN2v88internal7Isolate18GetTurboStatisticsEv
	.section	.text._ZN2v88internal7Isolate13GetCodeTracerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate13GetCodeTracerEv
	.type	_ZN2v88internal7Isolate13GetCodeTracerEv, @function
_ZN2v88internal7Isolate13GetCodeTracerEv:
.LFB25364:
	.cfi_startproc
	endbr64
	movq	41768(%rdi), %rax
	testq	%rax, %rax
	je	.L2407
	ret
	.p2align 4,,10
	.p2align 3
.L2407:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	40800(%rdi), %r12d
	movl	$160, %edi
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	%rax, %rdi
	movl	%r12d, %esi
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal10CodeTracerC1Ei
	movq	-24(%rbp), %rax
	movq	%rax, 41768(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25364:
	.size	_ZN2v88internal7Isolate13GetCodeTracerEv, .-_ZN2v88internal7Isolate13GetCodeTracerEv
	.section	.text._ZN2v88internal7Isolate13use_optimizerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate13use_optimizerEv
	.type	_ZN2v88internal7Isolate13use_optimizerEv, @function
_ZN2v88internal7Isolate13use_optimizerEv:
.LFB25365:
	.cfi_startproc
	endbr64
	movzbl	_ZN2v88internal8FLAG_optE(%rip), %eax
	testb	%al, %al
	je	.L2408
	xorl	%eax, %eax
	cmpb	$0, 41456(%rdi)
	jne	.L2408
	cmpl	$1, 41832(%rdi)
	setne	%al
.L2408:
	ret
	.cfi_endproc
.LFE25365:
	.size	_ZN2v88internal7Isolate13use_optimizerEv, .-_ZN2v88internal7Isolate13use_optimizerEv
	.section	.text._ZNK2v88internal7Isolate34NeedsDetailedOptimizedCodeLineInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Isolate34NeedsDetailedOptimizedCodeLineInfoEv
	.type	_ZNK2v88internal7Isolate34NeedsDetailedOptimizedCodeLineInfoEv, @function
_ZNK2v88internal7Isolate34NeedsDetailedOptimizedCodeLineInfoEv:
.LFB25366:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal16FLAG_trace_deoptE(%rip)
	je	.L2415
.L2419:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2415:
	cmpb	$0, _ZN2v88internal16FLAG_trace_turboE(%rip)
	jne	.L2419
	cmpb	$0, _ZN2v88internal22FLAG_trace_turbo_graphE(%rip)
	jne	.L2419
	cmpb	$0, _ZN2v88internal20FLAG_turbo_profilingE(%rip)
	jne	.L2419
	cmpb	$0, _ZN2v88internal14FLAG_perf_profE(%rip)
	jne	.L2419
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 41812(%rdi)
	jne	.L2417
	movq	41472(%rdi), %rax
	cmpb	$0, 8(%rax)
	je	.L2422
.L2417:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2422:
	.cfi_restore_state
	movq	41016(%rdi), %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L2417
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	jne	.L2417
	movzbl	41858(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25366:
	.size	_ZNK2v88internal7Isolate34NeedsDetailedOptimizedCodeLineInfoEv, .-_ZNK2v88internal7Isolate34NeedsDetailedOptimizedCodeLineInfoEv
	.section	.text._ZNK2v88internal7Isolate32NeedsSourcePositionsForProfilingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Isolate32NeedsSourcePositionsForProfilingEv
	.type	_ZNK2v88internal7Isolate32NeedsSourcePositionsForProfilingEv, @function
_ZNK2v88internal7Isolate32NeedsSourcePositionsForProfilingEv:
.LFB25367:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal16FLAG_trace_deoptE(%rip)
	jne	.L2428
	cmpb	$0, _ZN2v88internal16FLAG_trace_turboE(%rip)
	je	.L2431
.L2428:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2431:
	cmpb	$0, _ZN2v88internal22FLAG_trace_turbo_graphE(%rip)
	jne	.L2428
	cmpb	$0, _ZN2v88internal20FLAG_turbo_profilingE(%rip)
	jne	.L2428
	cmpb	$0, _ZN2v88internal14FLAG_perf_profE(%rip)
	jne	.L2428
	cmpb	$0, 41812(%rdi)
	jne	.L2428
	movq	41472(%rdi), %rax
	cmpb	$0, 8(%rax)
	jne	.L2428
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	41016(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L2426
	movzbl	_ZN2v88internal15FLAG_trace_mapsE(%rip), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2426:
	.cfi_restore_state
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25367:
	.size	_ZNK2v88internal7Isolate32NeedsSourcePositionsForProfilingEv, .-_ZNK2v88internal7Isolate32NeedsSourcePositionsForProfilingEv
	.section	.text._ZN2v88internal7Isolate35SetFeedbackVectorsForProfilingToolsENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate35SetFeedbackVectorsForProfilingToolsENS0_6ObjectE
	.type	_ZN2v88internal7Isolate35SetFeedbackVectorsForProfilingToolsENS0_6ObjectE, @function
_ZN2v88internal7Isolate35SetFeedbackVectorsForProfilingToolsENS0_6ObjectE:
.LFB25368:
	.cfi_startproc
	endbr64
	movq	%rsi, 4720(%rdi)
	ret
	.cfi_endproc
.LFE25368:
	.size	_ZN2v88internal7Isolate35SetFeedbackVectorsForProfilingToolsENS0_6ObjectE, .-_ZN2v88internal7Isolate35SetFeedbackVectorsForProfilingToolsENS0_6ObjectE
	.section	.text._ZN2v88internal7Isolate14set_date_cacheEPNS0_9DateCacheE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate14set_date_cacheEPNS0_9DateCacheE
	.type	_ZN2v88internal7Isolate14set_date_cacheEPNS0_9DateCacheE, @function
_ZN2v88internal7Isolate14set_date_cacheEPNS0_9DateCacheE:
.LFB25379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	41240(%rdi), %r13
	cmpq	%rsi, %r13
	je	.L2434
	testq	%r13, %r13
	je	.L2434
	movq	0(%r13), %rax
	leaq	_ZN2v88internal9DateCacheD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2435
	movq	592(%r13), %rdi
	leaq	16+_ZTVN2v88internal9DateCacheE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L2436
	movq	(%rdi), %rax
	call	*40(%rax)
.L2436:
	movl	$600, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2434:
	movq	%r12, 41240(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2435:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2434
	.cfi_endproc
.LFE25379:
	.size	_ZN2v88internal7Isolate14set_date_cacheEPNS0_9DateCacheE, .-_ZN2v88internal7Isolate14set_date_cacheEPNS0_9DateCacheE
	.section	.text._ZN2v88internal7Isolate32IsArrayOrObjectOrStringPrototypeENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate32IsArrayOrObjectOrStringPrototypeENS0_6ObjectE
	.type	_ZN2v88internal7Isolate32IsArrayOrObjectOrStringPrototypeENS0_6ObjectE, @function
_ZN2v88internal7Isolate32IsArrayOrObjectOrStringPrototypeENS0_6ObjectE:
.LFB25380:
	.cfi_startproc
	endbr64
	movq	39120(%rdi), %rax
	cmpq	88(%rdi), %rax
	jne	.L2452
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L2450:
	movq	463(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L2451
	movq	567(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L2451
	movq	1959(%rax), %rax
	cmpq	%rax, 88(%rdi)
	je	.L2453
.L2452:
	movq	519(%rax), %rdx
	cmpq	%rdx, %rsi
	jne	.L2450
.L2451:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2453:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25380:
	.size	_ZN2v88internal7Isolate32IsArrayOrObjectOrStringPrototypeENS0_6ObjectE, .-_ZN2v88internal7Isolate32IsArrayOrObjectOrStringPrototypeENS0_6ObjectE
	.section	.text._ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj
	.type	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj, @function
_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj:
.LFB25381:
	.cfi_startproc
	endbr64
	movq	39120(%rdi), %rax
	cmpq	88(%rdi), %rax
	je	.L2460
	leal	16(,%rdx,8), %ecx
	movslq	%ecx, %rcx
	subq	$1, %rcx
	jmp	.L2459
	.p2align 4,,10
	.p2align 3
.L2463:
	movq	1959(%rax), %rax
	cmpq	%rax, 88(%rdi)
	je	.L2460
.L2459:
	movq	(%rcx,%rax), %rdx
	cmpq	%rdx, %rsi
	jne	.L2463
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2460:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25381:
	.size	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj, .-_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj
	.section	.text._ZN2v88internal7Isolate27IsNoElementsProtectorIntactENS0_7ContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate27IsNoElementsProtectorIntactENS0_7ContextE
	.type	_ZN2v88internal7Isolate27IsNoElementsProtectorIntactENS0_7ContextE, @function
_ZN2v88internal7Isolate27IsNoElementsProtectorIntactENS0_7ContextE:
.LFB25382:
	.cfi_startproc
	endbr64
	movq	4504(%rdi), %rax
	movq	23(%rax), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	je	.L2464
	sarq	$32, %rdx
	cmpq	$1, %rdx
	sete	%al
.L2464:
	ret
	.cfi_endproc
.LFE25382:
	.size	_ZN2v88internal7Isolate27IsNoElementsProtectorIntactENS0_7ContextE, .-_ZN2v88internal7Isolate27IsNoElementsProtectorIntactENS0_7ContextE
	.section	.text._ZN2v88internal7Isolate27IsNoElementsProtectorIntactEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate27IsNoElementsProtectorIntactEv
	.type	_ZN2v88internal7Isolate27IsNoElementsProtectorIntactEv, @function
_ZN2v88internal7Isolate27IsNoElementsProtectorIntactEv:
.LFB25383:
	.cfi_startproc
	endbr64
	movq	4504(%rdi), %rax
	movq	23(%rax), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	je	.L2469
	sarq	$32, %rdx
	cmpq	$1, %rdx
	sete	%al
.L2469:
	ret
	.cfi_endproc
.LFE25383:
	.size	_ZN2v88internal7Isolate27IsNoElementsProtectorIntactEv, .-_ZN2v88internal7Isolate27IsNoElementsProtectorIntactEv
	.section	.text._ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactEv
	.type	_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactEv, @function
_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactEv:
.LFB25384:
	.cfi_startproc
	endbr64
	movq	4512(%rdi), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE25384:
	.size	_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactEv, .-_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactEv
	.section	.text._ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactENS0_10JSReceiverE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactENS0_10JSReceiverE
	.type	_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactENS0_10JSReceiverE, @function
_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactENS0_10JSReceiverE:
.LFB25385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	4512(%rdi), %rdx
	movq	%rsi, -8(%rbp)
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jne	.L2481
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2481:
	.cfi_restore_state
	movq	%rdi, %r8
	leaq	-8(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal10JSReceiver19HasProxyInPrototypeEPNS0_7IsolateE@PLT
	leave
	.cfi_def_cfa 7, 8
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE25385:
	.size	_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactENS0_10JSReceiverE, .-_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactENS0_10JSReceiverE
	.section	.text._ZN2v88internal7Isolate28IsPromiseHookProtectorIntactEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate28IsPromiseHookProtectorIntactEv
	.type	_ZN2v88internal7Isolate28IsPromiseHookProtectorIntactEv, @function
_ZN2v88internal7Isolate28IsPromiseHookProtectorIntactEv:
.LFB25386:
	.cfi_startproc
	endbr64
	movq	4568(%rdi), %rax
	cmpl	$1, 27(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE25386:
	.size	_ZN2v88internal7Isolate28IsPromiseHookProtectorIntactEv, .-_ZN2v88internal7Isolate28IsPromiseHookProtectorIntactEv
	.section	.text._ZN2v88internal7Isolate33IsPromiseResolveLookupChainIntactEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate33IsPromiseResolveLookupChainIntactEv
	.type	_ZN2v88internal7Isolate33IsPromiseResolveLookupChainIntactEv, @function
_ZN2v88internal7Isolate33IsPromiseResolveLookupChainIntactEv:
.LFB25387:
	.cfi_startproc
	endbr64
	movq	4576(%rdi), %rax
	cmpl	$1, 11(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE25387:
	.size	_ZN2v88internal7Isolate33IsPromiseResolveLookupChainIntactEv, .-_ZN2v88internal7Isolate33IsPromiseResolveLookupChainIntactEv
	.section	.text._ZN2v88internal7Isolate30IsPromiseThenLookupChainIntactEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate30IsPromiseThenLookupChainIntactEv
	.type	_ZN2v88internal7Isolate30IsPromiseThenLookupChainIntactEv, @function
_ZN2v88internal7Isolate30IsPromiseThenLookupChainIntactEv:
.LFB25388:
	.cfi_startproc
	endbr64
	movq	4592(%rdi), %rax
	cmpl	$1, 27(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE25388:
	.size	_ZN2v88internal7Isolate30IsPromiseThenLookupChainIntactEv, .-_ZN2v88internal7Isolate30IsPromiseThenLookupChainIntactEv
	.section	.text._ZN2v88internal7Isolate30IsPromiseThenLookupChainIntactENS0_6HandleINS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate30IsPromiseThenLookupChainIntactENS0_6HandleINS0_10JSReceiverEEE
	.type	_ZN2v88internal7Isolate30IsPromiseThenLookupChainIntactENS0_6HandleINS0_10JSReceiverEEE, @function
_ZN2v88internal7Isolate30IsPromiseThenLookupChainIntactENS0_6HandleINS0_10JSReceiverEEE:
.LFB25389:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	xorl	%eax, %eax
	movq	-1(%rdx), %rcx
	cmpw	$1074, 11(%rcx)
	je	.L2492
	ret
	.p2align 4,,10
	.p2align 3
.L2492:
	movq	-1(%rdx), %rax
	leaq	37592(%rdi), %rsi
	movq	23(%rax), %rcx
	movq	39120(%rdi), %rax
	cmpq	88(%rdi), %rax
	jne	.L2489
	jmp	.L2487
	.p2align 4,,10
	.p2align 3
.L2493:
	movq	1959(%rax), %rax
	cmpq	%rax, 88(%rdi)
	je	.L2487
.L2489:
	movq	1031(%rax), %rdx
	cmpq	%rdx, %rcx
	jne	.L2493
	movq	-33000(%rsi), %rax
	cmpl	$1, 27(%rax)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L2487:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25389:
	.size	_ZN2v88internal7Isolate30IsPromiseThenLookupChainIntactENS0_6HandleINS0_10JSReceiverEEE, .-_ZN2v88internal7Isolate30IsPromiseThenLookupChainIntactENS0_6HandleINS0_10JSReceiverEEE
	.section	.rodata._ZN2v88internal7Isolate37UpdateNoElementsProtectorOnSetElementENS0_6HandleINS0_8JSObjectEEE.str1.1,"aMS",@progbits,1
.LC58:
	.string	"no_elements_protector"
	.section	.text._ZN2v88internal7Isolate37UpdateNoElementsProtectorOnSetElementENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate37UpdateNoElementsProtectorOnSetElementENS0_6HandleINS0_8JSObjectEEE
	.type	_ZN2v88internal7Isolate37UpdateNoElementsProtectorOnSetElementENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal7Isolate37UpdateNoElementsProtectorOnSetElementENS0_6HandleINS0_8JSObjectEEE:
.LFB25390:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rsi
	movq	-1(%rsi), %rax
	movl	15(%rax), %eax
	testl	$1048576, %eax
	je	.L2508
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	4504(%rdi), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L2494
	sarq	$32, %rax
	cmpq	$1, %rax
	je	.L2511
.L2494:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2508:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L2511:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	call	_ZN2v88internal7Isolate32IsArrayOrObjectOrStringPrototypeENS0_6ObjectE
	testb	%al, %al
	je	.L2494
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2498
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2499:
	addq	$8, %rsp
	leaq	4504(%r12), %rdx
	movq	%r12, %rdi
	leaq	.LC58(%rip), %rsi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2498:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L2512
.L2500:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	$0, (%rcx)
	jmp	.L2499
.L2512:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L2500
	.cfi_endproc
.LFE25390:
	.size	_ZN2v88internal7Isolate37UpdateNoElementsProtectorOnSetElementENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal7Isolate37UpdateNoElementsProtectorOnSetElementENS0_6HandleINS0_8JSObjectEEE
	.section	.rodata._ZN2v88internal7Isolate26TraceProtectorInvalidationEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC59:
	.string	"Invalidating protector cell %s in isolate %p\n"
	.section	.rodata._ZN2v88internal7Isolate26TraceProtectorInvalidationEPKc.str1.1,"aMS",@progbits,1
.LC60:
	.string	"v8"
	.section	.text._ZN2v88internal7Isolate26TraceProtectorInvalidationEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate26TraceProtectorInvalidationEPKc
	.type	_ZN2v88internal7Isolate26TraceProtectorInvalidationEPKc, @function
_ZN2v88internal7Isolate26TraceProtectorInvalidationEPKc:
.LFB25391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	leaq	.LC59(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	_ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE29trace_event_unique_atomic3974(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L2533
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L2534
.L2513:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2535
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2533:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2536
.L2516:
	movq	%r12, _ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE29trace_event_unique_atomic3974(%rip)
	movzbl	(%r12), %eax
	testb	$5, %al
	je	.L2513
.L2534:
	leaq	_ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE30kInvalidateProtectorTracingArg(%rip), %rax
	pxor	%xmm0, %xmm0
	movb	$6, -65(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rbx, -56(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2537
.L2518:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2519
	movq	(%rdi), %rax
	call	*8(%rax)
.L2519:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2513
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2513
	.p2align 4,,10
	.p2align 3
.L2536:
	leaq	.LC60(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L2516
	.p2align 4,,10
	.p2align 3
.L2537:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$16
	leaq	_ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE35kInvalidateProtectorTracingCategory(%rip), %rcx
	movl	$73, %esi
	pushq	%rdx
	leaq	-56(%rbp), %rdx
	pushq	%rdx
	leaq	-65(%rbp), %rdx
	pushq	%rdx
	leaq	-64(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L2518
.L2535:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25391:
	.size	_ZN2v88internal7Isolate26TraceProtectorInvalidationEPKc, .-_ZN2v88internal7Isolate26TraceProtectorInvalidationEPKc
	.section	.rodata._ZN2v88internal7Isolate37InvalidateIsConcatSpreadableProtectorEv.str1.8,"aMS",@progbits,1
	.align 8
.LC61:
	.string	"is_concat_spreadable_protector"
	.section	.text._ZN2v88internal7Isolate37InvalidateIsConcatSpreadableProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate37InvalidateIsConcatSpreadableProtectorEv
	.type	_ZN2v88internal7Isolate37InvalidateIsConcatSpreadableProtectorEv, @function
_ZN2v88internal7Isolate37InvalidateIsConcatSpreadableProtectorEv:
.LFB25392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, _ZN2v88internal33FLAG_trace_protector_invalidationE(%rip)
	jne	.L2541
.L2539:
	movq	4512(%rbx), %rax
	movq	$0, 7(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2541:
	.cfi_restore_state
	leaq	.LC61(%rip), %rsi
	call	_ZN2v88internal7Isolate26TraceProtectorInvalidationEPKc
	jmp	.L2539
	.cfi_endproc
.LFE25392:
	.size	_ZN2v88internal7Isolate37InvalidateIsConcatSpreadableProtectorEv, .-_ZN2v88internal7Isolate37InvalidateIsConcatSpreadableProtectorEv
	.section	.rodata._ZN2v88internal7Isolate35InvalidateArrayConstructorProtectorEv.str1.1,"aMS",@progbits,1
.LC62:
	.string	"array_constructor_protector"
	.section	.text._ZN2v88internal7Isolate35InvalidateArrayConstructorProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate35InvalidateArrayConstructorProtectorEv
	.type	_ZN2v88internal7Isolate35InvalidateArrayConstructorProtectorEv, @function
_ZN2v88internal7Isolate35InvalidateArrayConstructorProtectorEv:
.LFB25393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, _ZN2v88internal33FLAG_trace_protector_invalidationE(%rip)
	jne	.L2545
.L2543:
	movq	4496(%rbx), %rax
	movq	$0, 7(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2545:
	.cfi_restore_state
	leaq	.LC62(%rip), %rsi
	call	_ZN2v88internal7Isolate26TraceProtectorInvalidationEPKc
	jmp	.L2543
	.cfi_endproc
.LFE25393:
	.size	_ZN2v88internal7Isolate35InvalidateArrayConstructorProtectorEv, .-_ZN2v88internal7Isolate35InvalidateArrayConstructorProtectorEv
	.section	.rodata._ZN2v88internal7Isolate36InvalidateTypedArraySpeciesProtectorEv.str1.1,"aMS",@progbits,1
.LC63:
	.string	"typed_array_species_protector"
	.section	.text._ZN2v88internal7Isolate36InvalidateTypedArraySpeciesProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate36InvalidateTypedArraySpeciesProtectorEv
	.type	_ZN2v88internal7Isolate36InvalidateTypedArraySpeciesProtectorEv, @function
_ZN2v88internal7Isolate36InvalidateTypedArraySpeciesProtectorEv:
.LFB25394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2547
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2548:
	addq	$8, %rsp
	leaq	4528(%r12), %rdx
	movq	%r12, %rdi
	leaq	.LC63(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2547:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L2551
.L2549:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	$0, (%rcx)
	jmp	.L2548
	.p2align 4,,10
	.p2align 3
.L2551:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L2549
	.cfi_endproc
.LFE25394:
	.size	_ZN2v88internal7Isolate36InvalidateTypedArraySpeciesProtectorEv, .-_ZN2v88internal7Isolate36InvalidateTypedArraySpeciesProtectorEv
	.section	.rodata._ZN2v88internal7Isolate33InvalidatePromiseSpeciesProtectorEv.str1.1,"aMS",@progbits,1
.LC64:
	.string	"promise_species_protector"
	.section	.text._ZN2v88internal7Isolate33InvalidatePromiseSpeciesProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate33InvalidatePromiseSpeciesProtectorEv
	.type	_ZN2v88internal7Isolate33InvalidatePromiseSpeciesProtectorEv, @function
_ZN2v88internal7Isolate33InvalidatePromiseSpeciesProtectorEv:
.LFB25395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2553
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2554:
	addq	$8, %rsp
	leaq	4536(%r12), %rdx
	movq	%r12, %rdi
	leaq	.LC64(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2553:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L2557
.L2555:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	$0, (%rcx)
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2557:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L2555
	.cfi_endproc
.LFE25395:
	.size	_ZN2v88internal7Isolate33InvalidatePromiseSpeciesProtectorEv, .-_ZN2v88internal7Isolate33InvalidatePromiseSpeciesProtectorEv
	.section	.rodata._ZN2v88internal7Isolate39InvalidateStringLengthOverflowProtectorEv.str1.1,"aMS",@progbits,1
.LC65:
	.string	"string_length_protector"
	.section	.text._ZN2v88internal7Isolate39InvalidateStringLengthOverflowProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate39InvalidateStringLengthOverflowProtectorEv
	.type	_ZN2v88internal7Isolate39InvalidateStringLengthOverflowProtectorEv, @function
_ZN2v88internal7Isolate39InvalidateStringLengthOverflowProtectorEv:
.LFB25396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, _ZN2v88internal33FLAG_trace_protector_invalidationE(%rip)
	jne	.L2561
.L2559:
	movq	4544(%rbx), %rax
	movq	$0, 7(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2561:
	.cfi_restore_state
	leaq	.LC65(%rip), %rsi
	call	_ZN2v88internal7Isolate26TraceProtectorInvalidationEPKc
	jmp	.L2559
	.cfi_endproc
.LFE25396:
	.size	_ZN2v88internal7Isolate39InvalidateStringLengthOverflowProtectorEv, .-_ZN2v88internal7Isolate39InvalidateStringLengthOverflowProtectorEv
	.section	.rodata._ZN2v88internal7Isolate32InvalidateArrayIteratorProtectorEv.str1.1,"aMS",@progbits,1
.LC66:
	.string	"array_iterator_protector"
	.section	.text._ZN2v88internal7Isolate32InvalidateArrayIteratorProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate32InvalidateArrayIteratorProtectorEv
	.type	_ZN2v88internal7Isolate32InvalidateArrayIteratorProtectorEv, @function
_ZN2v88internal7Isolate32InvalidateArrayIteratorProtectorEv:
.LFB25397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2563
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2564:
	addq	$8, %rsp
	leaq	4552(%r12), %rdx
	movq	%r12, %rdi
	leaq	.LC66(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2563:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L2567
.L2565:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	$0, (%rcx)
	jmp	.L2564
	.p2align 4,,10
	.p2align 3
.L2567:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L2565
	.cfi_endproc
.LFE25397:
	.size	_ZN2v88internal7Isolate32InvalidateArrayIteratorProtectorEv, .-_ZN2v88internal7Isolate32InvalidateArrayIteratorProtectorEv
	.section	.rodata._ZN2v88internal7Isolate30InvalidateMapIteratorProtectorEv.str1.1,"aMS",@progbits,1
.LC67:
	.string	"map_iterator_protector"
	.section	.text._ZN2v88internal7Isolate30InvalidateMapIteratorProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate30InvalidateMapIteratorProtectorEv
	.type	_ZN2v88internal7Isolate30InvalidateMapIteratorProtectorEv, @function
_ZN2v88internal7Isolate30InvalidateMapIteratorProtectorEv:
.LFB25398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2569
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2570:
	addq	$8, %rsp
	leaq	4584(%r12), %rdx
	movq	%r12, %rdi
	leaq	.LC67(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2569:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L2573
.L2571:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	$0, (%rcx)
	jmp	.L2570
	.p2align 4,,10
	.p2align 3
.L2573:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L2571
	.cfi_endproc
.LFE25398:
	.size	_ZN2v88internal7Isolate30InvalidateMapIteratorProtectorEv, .-_ZN2v88internal7Isolate30InvalidateMapIteratorProtectorEv
	.section	.rodata._ZN2v88internal7Isolate30InvalidateSetIteratorProtectorEv.str1.1,"aMS",@progbits,1
.LC68:
	.string	"set_iterator_protector"
	.section	.text._ZN2v88internal7Isolate30InvalidateSetIteratorProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate30InvalidateSetIteratorProtectorEv
	.type	_ZN2v88internal7Isolate30InvalidateSetIteratorProtectorEv, @function
_ZN2v88internal7Isolate30InvalidateSetIteratorProtectorEv:
.LFB25399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2575
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2576:
	addq	$8, %rsp
	leaq	4600(%r12), %rdx
	movq	%r12, %rdi
	leaq	.LC68(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2575:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L2579
.L2577:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	$0, (%rcx)
	jmp	.L2576
	.p2align 4,,10
	.p2align 3
.L2579:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L2577
	.cfi_endproc
.LFE25399:
	.size	_ZN2v88internal7Isolate30InvalidateSetIteratorProtectorEv, .-_ZN2v88internal7Isolate30InvalidateSetIteratorProtectorEv
	.section	.rodata._ZN2v88internal7Isolate33InvalidateStringIteratorProtectorEv.str1.1,"aMS",@progbits,1
.LC69:
	.string	"string_iterator_protector"
	.section	.text._ZN2v88internal7Isolate33InvalidateStringIteratorProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate33InvalidateStringIteratorProtectorEv
	.type	_ZN2v88internal7Isolate33InvalidateStringIteratorProtectorEv, @function
_ZN2v88internal7Isolate33InvalidateStringIteratorProtectorEv:
.LFB25400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2581
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2582:
	addq	$8, %rsp
	leaq	4608(%r12), %rdx
	movq	%r12, %rdi
	leaq	.LC69(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2581:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L2585
.L2583:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	$0, (%rcx)
	jmp	.L2582
	.p2align 4,,10
	.p2align 3
.L2585:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L2583
	.cfi_endproc
.LFE25400:
	.size	_ZN2v88internal7Isolate33InvalidateStringIteratorProtectorEv, .-_ZN2v88internal7Isolate33InvalidateStringIteratorProtectorEv
	.section	.rodata._ZN2v88internal7Isolate39InvalidateArrayBufferDetachingProtectorEv.str1.8,"aMS",@progbits,1
	.align 8
.LC70:
	.string	"array_buffer_detaching_protector"
	.section	.text._ZN2v88internal7Isolate39InvalidateArrayBufferDetachingProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate39InvalidateArrayBufferDetachingProtectorEv
	.type	_ZN2v88internal7Isolate39InvalidateArrayBufferDetachingProtectorEv, @function
_ZN2v88internal7Isolate39InvalidateArrayBufferDetachingProtectorEv:
.LFB25401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2587
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2588:
	addq	$8, %rsp
	leaq	4560(%r12), %rdx
	movq	%r12, %rdi
	leaq	.LC70(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2587:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L2591
.L2589:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	$0, (%rcx)
	jmp	.L2588
	.p2align 4,,10
	.p2align 3
.L2591:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L2589
	.cfi_endproc
.LFE25401:
	.size	_ZN2v88internal7Isolate39InvalidateArrayBufferDetachingProtectorEv, .-_ZN2v88internal7Isolate39InvalidateArrayBufferDetachingProtectorEv
	.section	.rodata._ZN2v88internal7Isolate30InvalidatePromiseHookProtectorEv.str1.1,"aMS",@progbits,1
.LC71:
	.string	"promise_hook_protector"
	.section	.text._ZN2v88internal7Isolate30InvalidatePromiseHookProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate30InvalidatePromiseHookProtectorEv
	.type	_ZN2v88internal7Isolate30InvalidatePromiseHookProtectorEv, @function
_ZN2v88internal7Isolate30InvalidatePromiseHookProtectorEv:
.LFB25402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2593
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2594:
	addq	$8, %rsp
	leaq	4568(%r12), %rdx
	movq	%r12, %rdi
	leaq	.LC71(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2593:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L2597
.L2595:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	$0, (%rcx)
	jmp	.L2594
	.p2align 4,,10
	.p2align 3
.L2597:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L2595
	.cfi_endproc
.LFE25402:
	.size	_ZN2v88internal7Isolate30InvalidatePromiseHookProtectorEv, .-_ZN2v88internal7Isolate30InvalidatePromiseHookProtectorEv
	.section	.rodata._ZN2v88internal7Isolate33InvalidatePromiseResolveProtectorEv.str1.1,"aMS",@progbits,1
.LC72:
	.string	"promise_resolve_protector"
	.section	.text._ZN2v88internal7Isolate33InvalidatePromiseResolveProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate33InvalidatePromiseResolveProtectorEv
	.type	_ZN2v88internal7Isolate33InvalidatePromiseResolveProtectorEv, @function
_ZN2v88internal7Isolate33InvalidatePromiseResolveProtectorEv:
.LFB25403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, _ZN2v88internal33FLAG_trace_protector_invalidationE(%rip)
	jne	.L2601
.L2599:
	movq	4576(%rbx), %rax
	movq	$0, 7(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2601:
	.cfi_restore_state
	leaq	.LC72(%rip), %rsi
	call	_ZN2v88internal7Isolate26TraceProtectorInvalidationEPKc
	jmp	.L2599
	.cfi_endproc
.LFE25403:
	.size	_ZN2v88internal7Isolate33InvalidatePromiseResolveProtectorEv, .-_ZN2v88internal7Isolate33InvalidatePromiseResolveProtectorEv
	.section	.rodata._ZN2v88internal7Isolate30InvalidatePromiseThenProtectorEv.str1.1,"aMS",@progbits,1
.LC73:
	.string	"promise_then_protector"
	.section	.text._ZN2v88internal7Isolate30InvalidatePromiseThenProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate30InvalidatePromiseThenProtectorEv
	.type	_ZN2v88internal7Isolate30InvalidatePromiseThenProtectorEv, @function
_ZN2v88internal7Isolate30InvalidatePromiseThenProtectorEv:
.LFB25404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2603
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2604:
	addq	$8, %rsp
	leaq	4592(%r12), %rdx
	movq	%r12, %rdi
	leaq	.LC73(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2603:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L2607
.L2605:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	$0, (%rcx)
	jmp	.L2604
	.p2align 4,,10
	.p2align 3
.L2607:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L2605
	.cfi_endproc
.LFE25404:
	.size	_ZN2v88internal7Isolate30InvalidatePromiseThenProtectorEv, .-_ZN2v88internal7Isolate30InvalidatePromiseThenProtectorEv
	.section	.text._ZN2v88internal7Isolate26IsAnyInitialArrayPrototypeENS0_6HandleINS0_7JSArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate26IsAnyInitialArrayPrototypeENS0_6HandleINS0_7JSArrayEEE
	.type	_ZN2v88internal7Isolate26IsAnyInitialArrayPrototypeENS0_6HandleINS0_7JSArrayEEE, @function
_ZN2v88internal7Isolate26IsAnyInitialArrayPrototypeENS0_6HandleINS0_7JSArrayEEE:
.LFB25405:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	39120(%rdi), %rax
	cmpq	88(%rdi), %rax
	jne	.L2611
	jmp	.L2609
	.p2align 4,,10
	.p2align 3
.L2614:
	movq	1959(%rax), %rax
	cmpq	%rax, 88(%rdi)
	je	.L2609
.L2611:
	movq	463(%rax), %rcx
	cmpq	%rcx, %rdx
	jne	.L2614
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2609:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25405:
	.size	_ZN2v88internal7Isolate26IsAnyInitialArrayPrototypeENS0_6HandleINS0_7JSArrayEEE, .-_ZN2v88internal7Isolate26IsAnyInitialArrayPrototypeENS0_6HandleINS0_7JSArrayEEE
	.section	.text._ZN2v88internal7Isolate23random_number_generatorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate23random_number_generatorEv
	.type	_ZN2v88internal7Isolate23random_number_generatorEv, @function
_ZN2v88internal7Isolate23random_number_generatorEv:
.LFB25407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	41248(%rdi), %r12
	testq	%r12, %r12
	je	.L2619
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2619:
	.cfi_restore_state
	movl	_ZN2v88internal16FLAG_random_seedE(%rip), %r13d
	movq	%rdi, %rbx
	movl	$24, %edi
	testl	%r13d, %r13d
	je	.L2617
	call	_Znwm@PLT
	movslq	%r13d, %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v84base21RandomNumberGenerator7SetSeedEl@PLT
	movq	%r12, 41248(%rbx)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2617:
	.cfi_restore_state
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v84base21RandomNumberGeneratorC1Ev@PLT
	movq	%r12, 41248(%rbx)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25407:
	.size	_ZN2v88internal7Isolate23random_number_generatorEv, .-_ZN2v88internal7Isolate23random_number_generatorEv
	.section	.text._ZN2v88internal7Isolate10fuzzer_rngEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate10fuzzer_rngEv
	.type	_ZN2v88internal7Isolate10fuzzer_rngEv, @function
_ZN2v88internal7Isolate10fuzzer_rngEv:
.LFB25408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	41256(%rdi), %r12
	testq	%r12, %r12
	je	.L2626
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2626:
	.cfi_restore_state
	movslq	_ZN2v88internal23FLAG_fuzzer_random_seedE(%rip), %r13
	movq	%rdi, %rbx
	testq	%r13, %r13
	je	.L2627
.L2622:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v84base21RandomNumberGenerator7SetSeedEl@PLT
	movq	%r12, 41256(%rbx)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2627:
	.cfi_restore_state
	movq	41248(%rdi), %r12
	testq	%r12, %r12
	je	.L2628
.L2623:
	movq	(%r12), %r13
	jmp	.L2622
	.p2align 4,,10
	.p2align 3
.L2628:
	movl	_ZN2v88internal16FLAG_random_seedE(%rip), %r13d
	movl	$24, %edi
	testl	%r13d, %r13d
	je	.L2624
	call	_Znwm@PLT
	movslq	%r13d, %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v84base21RandomNumberGenerator7SetSeedEl@PLT
	movq	%r12, 41248(%rbx)
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2624:
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v84base21RandomNumberGeneratorC1Ev@PLT
	movq	%r12, 41248(%rbx)
	jmp	.L2623
	.cfi_endproc
.LFE25408:
	.size	_ZN2v88internal7Isolate10fuzzer_rngEv, .-_ZN2v88internal7Isolate10fuzzer_rngEv
	.section	.text._ZN2v88internal7Isolate20GenerateIdentityHashEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate20GenerateIdentityHashEj
	.type	_ZN2v88internal7Isolate20GenerateIdentityHashEj, @function
_ZN2v88internal7Isolate20GenerateIdentityHashEj:
.LFB25409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$31, %ebx
	subq	$8, %rsp
	jmp	.L2633
	.p2align 4,,10
	.p2align 3
.L2630:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v84base21RandomNumberGenerator4NextEi@PLT
	andl	%r13d, %eax
	jne	.L2629
	subl	$1, %ebx
	je	.L2636
.L2633:
	movq	41248(%r14), %r12
	testq	%r12, %r12
	jne	.L2630
	movl	_ZN2v88internal16FLAG_random_seedE(%rip), %r15d
	movl	$24, %edi
	testl	%r15d, %r15d
	je	.L2631
	call	_Znwm@PLT
	movslq	%r15d, %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v84base21RandomNumberGenerator7SetSeedEl@PLT
	movq	%r12, 41248(%r14)
	jmp	.L2630
	.p2align 4,,10
	.p2align 3
.L2631:
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v84base21RandomNumberGeneratorC1Ev@PLT
	movq	%r12, 41248(%r14)
	jmp	.L2630
	.p2align 4,,10
	.p2align 3
.L2636:
	movl	$1, %eax
.L2629:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25409:
	.size	_ZN2v88internal7Isolate20GenerateIdentityHashEj, .-_ZN2v88internal7Isolate20GenerateIdentityHashEj
	.section	.text._ZN2v88internal7Isolate14FindCodeObjectEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate14FindCodeObjectEm
	.type	_ZN2v88internal7Isolate14FindCodeObjectEm, @function
_ZN2v88internal7Isolate14FindCodeObjectEm:
.LFB25410:
	.cfi_startproc
	endbr64
	addq	$37592, %rdi
	jmp	_ZN2v88internal4Heap29GcSafeFindCodeForInnerPointerEm@PLT
	.cfi_endproc
.LFE25410:
	.size	_ZN2v88internal7Isolate14FindCodeObjectEm, .-_ZN2v88internal7Isolate14FindCodeObjectEm
	.section	.text._ZN2v88internal7Isolate9SymbolForENS0_9RootIndexENS0_6HandleINS0_6StringEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate9SymbolForENS0_9RootIndexENS0_6HandleINS0_6StringEEEb
	.type	_ZN2v88internal7Isolate9SymbolForENS0_9RootIndexENS0_6HandleINS0_6StringEEEb, @function
_ZN2v88internal7Isolate9SymbolForENS0_9RootIndexENS0_6HandleINS0_6StringEEEb:
.LFB25411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movzwl	%si, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L2672
.L2639:
	movq	(%r15), %rcx
	salq	$3, %r12
	leaq	(%rbx,%r12), %rsi
	movl	7(%rcx), %eax
	movq	56(%rsi), %rdx
	testb	$1, %al
	jne	.L2640
	shrl	$2, %eax
.L2641:
	movslq	35(%rdx), %rcx
	movq	88(%rbx), %r10
	subq	$1, %rdx
	movl	$1, %edi
	subl	$1, %ecx
	andl	%ecx, %eax
	jmp	.L2644
	.p2align 4,,10
	.p2align 3
.L2673:
	cmpq	(%r15), %r8
	je	.L2643
	addl	%edi, %eax
	addl	$1, %edi
	andl	%ecx, %eax
.L2644:
	leal	(%rax,%rax,2), %r9d
	leal	56(,%r9,8), %r8d
	movslq	%r8d, %r8
	movq	(%r8,%rdx), %r8
	cmpq	%r8, %r10
	jne	.L2673
	movl	$-1, -64(%rbp)
.L2659:
	movl	$1, %esi
	movq	%rbx, %rdi
	testb	%r14b, %r14b
	jne	.L2674
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%rax, %r14
.L2647:
	movq	(%r14), %rdi
	movq	(%r15), %rdx
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L2658
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L2675
.L2649:
	testb	$24, %al
	je	.L2658
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2676
	.p2align 4,,10
	.p2align 3
.L2658:
	leaq	56(%rbx,%r12), %rsi
	leaq	-64(%rbp), %r9
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$192, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE3AddEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	cmpw	$576, %r13w
	je	.L2651
	cmpw	$577, %r13w
	jne	.L2677
	movq	(%rax), %rax
	movq	%rax, 4672(%rbx)
	.p2align 4,,10
	.p2align 3
.L2654:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2678
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2674:
	.cfi_restore_state
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	jmp	.L2647
	.p2align 4,,10
	.p2align 3
.L2677:
	cmpw	$575, %r13w
	je	.L2679
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2643:
	movl	%eax, -64(%rbp)
	cmpl	$-1, %eax
	je	.L2659
	movq	56(%rsi), %rdx
	leal	64(,%r9,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2655
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
	jmp	.L2654
	.p2align 4,,10
	.p2align 3
.L2640:
	leaq	-64(%rbp), %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	jmp	.L2641
	.p2align 4,,10
	.p2align 3
.L2672:
	movq	%rdx, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r15
	jmp	.L2639
	.p2align 4,,10
	.p2align 3
.L2655:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L2680
.L2657:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L2654
	.p2align 4,,10
	.p2align 3
.L2675:
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L2649
	.p2align 4,,10
	.p2align 3
.L2676:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2658
	.p2align 4,,10
	.p2align 3
.L2679:
	movq	(%r14), %rdx
	orl	$4, 11(%rdx)
	movq	(%rax), %rax
	movq	%rax, 4656(%rbx)
	jmp	.L2654
	.p2align 4,,10
	.p2align 3
.L2651:
	movq	(%rax), %rax
	movq	%rax, 4664(%rbx)
	jmp	.L2654
	.p2align 4,,10
	.p2align 3
.L2680:
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2657
.L2678:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25411:
	.size	_ZN2v88internal7Isolate9SymbolForENS0_9RootIndexENS0_6HandleINS0_6StringEEEb, .-_ZN2v88internal7Isolate9SymbolForENS0_9RootIndexENS0_6HandleINS0_6StringEEEb
	.section	.text._ZN2v88internal7Isolate31RemoveBeforeCallEnteredCallbackEPFvPNS_7IsolateEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate31RemoveBeforeCallEnteredCallbackEPFvPNS_7IsolateEE
	.type	_ZN2v88internal7Isolate31RemoveBeforeCallEnteredCallbackEPFvPNS_7IsolateEE, @function
_ZN2v88internal7Isolate31RemoveBeforeCallEnteredCallbackEPFvPNS_7IsolateEE:
.LFB25413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	45448(%rdi), %rdx
	movq	45440(%rdi), %rdi
	movq	%rdx, %rax
	subq	%rdi, %rax
	movq	%rax, %rcx
	sarq	$5, %rax
	sarq	$3, %rcx
	testq	%rax, %rax
	jle	.L2682
	salq	$5, %rax
	addq	%rdi, %rax
	jmp	.L2687
	.p2align 4,,10
	.p2align 3
.L2706:
	cmpq	8(%rdi), %rsi
	je	.L2702
	cmpq	16(%rdi), %rsi
	je	.L2703
	cmpq	24(%rdi), %rsi
	je	.L2704
	addq	$32, %rdi
	cmpq	%rax, %rdi
	je	.L2705
.L2687:
	cmpq	(%rdi), %rsi
	jne	.L2706
.L2683:
	cmpq	%rdi, %rdx
	je	.L2681
	leaq	8(%rdi), %rsi
	cmpq	%rsi, %rdx
	je	.L2693
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	45448(%rbx), %rsi
.L2693:
	subq	$8, %rsi
	movq	%rsi, 45448(%rbx)
.L2681:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2705:
	.cfi_restore_state
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	sarq	$3, %rcx
.L2682:
	cmpq	$2, %rcx
	je	.L2688
	cmpq	$3, %rcx
	je	.L2689
	cmpq	$1, %rcx
	jne	.L2681
.L2690:
	cmpq	(%rdi), %rsi
	jne	.L2681
	jmp	.L2683
	.p2align 4,,10
	.p2align 3
.L2689:
	cmpq	(%rdi), %rsi
	je	.L2683
	addq	$8, %rdi
.L2688:
	cmpq	(%rdi), %rsi
	je	.L2683
	addq	$8, %rdi
	jmp	.L2690
	.p2align 4,,10
	.p2align 3
.L2702:
	addq	$8, %rdi
	jmp	.L2683
	.p2align 4,,10
	.p2align 3
.L2703:
	addq	$16, %rdi
	jmp	.L2683
	.p2align 4,,10
	.p2align 3
.L2704:
	addq	$24, %rdi
	jmp	.L2683
	.cfi_endproc
.LFE25413:
	.size	_ZN2v88internal7Isolate31RemoveBeforeCallEnteredCallbackEPFvPNS_7IsolateEE, .-_ZN2v88internal7Isolate31RemoveBeforeCallEnteredCallbackEPFvPNS_7IsolateEE
	.section	.text._ZN2v88internal7Isolate27RemoveCallCompletedCallbackEPFvPNS_7IsolateEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate27RemoveCallCompletedCallbackEPFvPNS_7IsolateEE
	.type	_ZN2v88internal7Isolate27RemoveCallCompletedCallbackEPFvPNS_7IsolateEE, @function
_ZN2v88internal7Isolate27RemoveCallCompletedCallbackEPFvPNS_7IsolateEE:
.LFB25415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	45472(%rdi), %rdx
	movq	45464(%rdi), %rdi
	movq	%rdx, %rax
	subq	%rdi, %rax
	movq	%rax, %rcx
	sarq	$5, %rax
	sarq	$3, %rcx
	testq	%rax, %rax
	jle	.L2708
	salq	$5, %rax
	addq	%rdi, %rax
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2732:
	cmpq	8(%rdi), %rsi
	je	.L2728
	cmpq	16(%rdi), %rsi
	je	.L2729
	cmpq	24(%rdi), %rsi
	je	.L2730
	addq	$32, %rdi
	cmpq	%rax, %rdi
	je	.L2731
.L2713:
	cmpq	(%rdi), %rsi
	jne	.L2732
.L2709:
	cmpq	%rdi, %rdx
	je	.L2707
	leaq	8(%rdi), %rsi
	cmpq	%rsi, %rdx
	je	.L2719
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	45472(%rbx), %rsi
.L2719:
	subq	$8, %rsi
	movq	%rsi, 45472(%rbx)
.L2707:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2731:
	.cfi_restore_state
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	sarq	$3, %rcx
.L2708:
	cmpq	$2, %rcx
	je	.L2714
	cmpq	$3, %rcx
	je	.L2715
	cmpq	$1, %rcx
	jne	.L2707
.L2716:
	cmpq	(%rdi), %rsi
	jne	.L2707
	jmp	.L2709
	.p2align 4,,10
	.p2align 3
.L2715:
	cmpq	(%rdi), %rsi
	je	.L2709
	addq	$8, %rdi
.L2714:
	cmpq	(%rdi), %rsi
	je	.L2709
	addq	$8, %rdi
	jmp	.L2716
	.p2align 4,,10
	.p2align 3
.L2728:
	addq	$8, %rdi
	jmp	.L2709
	.p2align 4,,10
	.p2align 3
.L2729:
	addq	$16, %rdi
	jmp	.L2709
	.p2align 4,,10
	.p2align 3
.L2730:
	addq	$24, %rdi
	jmp	.L2709
	.cfi_endproc
.LFE25415:
	.size	_ZN2v88internal7Isolate27RemoveCallCompletedCallbackEPFvPNS_7IsolateEE, .-_ZN2v88internal7Isolate27RemoveCallCompletedCallbackEPFvPNS_7IsolateEE
	.section	.text._ZN2v88internal7Isolate25FireCallCompletedCallbackEPNS0_14MicrotaskQueueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate25FireCallCompletedCallbackEPNS0_14MicrotaskQueueE
	.type	_ZN2v88internal7Isolate25FireCallCompletedCallbackEPNS0_14MicrotaskQueueE, @function
_ZN2v88internal7Isolate25FireCallCompletedCallbackEPNS0_14MicrotaskQueueE:
.LFB25416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 12528(%rdi)
	jne	.L2733
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L2735
	cmpq	$0, 8(%rsi)
	je	.L2735
	movl	68(%rsi), %eax
	testl	%eax, %eax
	jne	.L2735
	cmpl	$2, 72(%rsi)
	je	.L2752
	.p2align 4,,10
	.p2align 3
.L2735:
	movq	45464(%r12), %rax
	cmpq	%rax, 45472(%r12)
	je	.L2733
	leaq	-80(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v87Isolate31SuppressMicrotaskExecutionScopeC1EPS0_@PLT
	movq	45472(%r12), %rax
	movq	45464(%r12), %rsi
	movq	%rax, %rdi
	subq	%rsi, %rdi
	movq	%rdi, %rdx
	sarq	$3, %rdx
	je	.L2746
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2753
	call	_Znwm@PLT
	movq	45464(%r12), %rsi
	movq	%rax, %r14
	movq	45472(%r12), %rax
	movq	%rax, %rdi
	subq	%rsi, %rdi
.L2736:
	leaq	(%r14,%rdi), %r13
	cmpq	%rsi, %rax
	je	.L2738
	movq	%rdi, %rdx
	movq	%r14, %rdi
	call	memmove@PLT
	cmpq	%r14, %r13
	je	.L2742
.L2739:
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L2741:
	movq	%r12, %rdi
	call	*(%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L2741
.L2743:
	testq	%r14, %r14
	je	.L2740
.L2742:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2740:
	movq	%r15, %rdi
	call	_ZN2v87Isolate31SuppressMicrotaskExecutionScopeD1Ev@PLT
.L2733:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2754
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2738:
	.cfi_restore_state
	cmpq	%r14, %r13
	jne	.L2739
	jmp	.L2743
	.p2align 4,,10
	.p2align 3
.L2746:
	xorl	%r14d, %r14d
	jmp	.L2736
.L2752:
	movq	%r12, %rsi
	call	_ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE@PLT
	jmp	.L2735
.L2754:
	call	__stack_chk_fail@PLT
.L2753:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25416:
	.size	_ZN2v88internal7Isolate25FireCallCompletedCallbackEPNS0_14MicrotaskQueueE, .-_ZN2v88internal7Isolate25FireCallCompletedCallbackEPNS0_14MicrotaskQueueE
	.section	.text._ZN2v88internal7Isolate23PromiseHookStateUpdatedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate23PromiseHookStateUpdatedEv
	.type	_ZN2v88internal7Isolate23PromiseHookStateUpdatedEv, @function
_ZN2v88internal7Isolate23PromiseHookStateUpdatedEv:
.LFB25417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	$1, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	$0, 41288(%rdi)
	movq	%rdi, %rbx
	je	.L2769
.L2756:
	movq	4568(%rbx), %rdx
	movl	$1, %eax
	cmpl	$1, 27(%rdx)
	je	.L2770
.L2757:
	movb	%r12b, 45664(%rbx)
	movb	%al, 45665(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2769:
	.cfi_restore_state
	cmpq	$0, 45656(%rdi)
	jne	.L2756
	movq	41472(%rdi), %rax
	movzbl	8(%rax), %r12d
	xorl	%eax, %eax
	testb	%r12b, %r12b
	je	.L2757
	xorl	%r12d, %r12d
	jmp	.L2756
	.p2align 4,,10
	.p2align 3
.L2770:
	movq	41112(%rbx), %rdi
	addl	$1, 41104(%rbx)
	movq	41088(%rbx), %r13
	movq	41096(%rbx), %r14
	testq	%rdi, %rdi
	je	.L2758
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2759:
	leaq	4568(%rbx), %rdx
	leaq	.LC71(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	subl	$1, 41104(%rbx)
	movq	%r13, 41088(%rbx)
	cmpq	41096(%rbx), %r14
	je	.L2761
	movq	%r14, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2761:
	movl	$1, %eax
	jmp	.L2757
	.p2align 4,,10
	.p2align 3
.L2758:
	movq	%r13, %rcx
	cmpq	%r14, %r13
	je	.L2771
.L2760:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	$0, (%rcx)
	jmp	.L2759
	.p2align 4,,10
	.p2align 3
.L2771:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L2760
	.cfi_endproc
.LFE25417:
	.size	_ZN2v88internal7Isolate23PromiseHookStateUpdatedEv, .-_ZN2v88internal7Isolate23PromiseHookStateUpdatedEv
	.section	.text._ZN2v88internal7Isolate38RunHostImportModuleDynamicallyCallbackENS0_6HandleINS0_6ScriptEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate38RunHostImportModuleDynamicallyCallbackENS0_6HandleINS0_6ScriptEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal7Isolate38RunHostImportModuleDynamicallyCallbackENS0_6HandleINS0_6ScriptEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal7Isolate38RunHostImportModuleDynamicallyCallbackENS0_6HandleINS0_6ScriptEEENS2_INS0_6ObjectEEE:
.LFB25422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2773
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpq	$0, 41304(%rbx)
	movq	%rax, %r13
	je	.L2811
.L2776:
	movq	(%r12), %rax
	movq	%r12, %rdx
	testb	$1, %al
	jne	.L2784
.L2787:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2812
.L2786:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*41304(%rbx)
	testq	%rax, %rax
	je	.L2813
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2784:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2787
	jmp	.L2786
	.p2align 4,,10
	.p2align 3
.L2773:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L2814
.L2775:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	cmpq	$0, 41304(%rbx)
	jne	.L2776
.L2811:
	movq	12464(%rbx), %rax
	movq	39(%rax), %rax
	movq	1607(%rax), %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2777
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2778:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$7, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v87Promise8Resolver3NewENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2815
.L2805:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v87Promise8Resolver6RejectENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L2816
	movq	12552(%rbx), %rax
	movq	96(%rbx), %rdx
	xorl	%r12d, %r12d
	movq	%rdx, 12552(%rbx)
	movq	%rax, 12480(%rbx)
.L2796:
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2813:
	.cfi_restore_state
	movq	12552(%rbx), %rax
	movq	96(%rbx), %rdx
	movq	%rax, 12480(%rbx)
	xorl	%eax, %eax
	movq	%rdx, 12552(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2777:
	.cfi_restore_state
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L2817
.L2779:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2812:
	movq	41112(%rbx), %rdi
	movq	12480(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L2818
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2792:
	movq	96(%rbx), %rax
	movq	%r13, %rdi
	movq	%rax, 12480(%rbx)
	call	_ZN2v87Promise8Resolver3NewENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L2805
.L2815:
	movq	12552(%rbx), %rax
	movq	96(%rbx), %rdx
	movq	%rax, 12480(%rbx)
	movq	%rdx, 12552(%rbx)
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2814:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2775
	.p2align 4,,10
	.p2align 3
.L2816:
	movq	%r12, %rdi
	call	_ZN2v87Promise8Resolver10GetPromiseEv@PLT
	movq	%rax, %r12
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2818:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L2819
.L2793:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L2792
.L2819:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2793
	.p2align 4,,10
	.p2align 3
.L2817:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2779
	.cfi_endproc
.LFE25422:
	.size	_ZN2v88internal7Isolate38RunHostImportModuleDynamicallyCallbackENS0_6HandleINS0_6ScriptEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal7Isolate38RunHostImportModuleDynamicallyCallbackENS0_6HandleINS0_6ScriptEEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal7Isolate16ClearKeptObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate16ClearKeptObjectsEv
	.type	_ZN2v88internal7Isolate16ClearKeptObjectsEv, @function
_ZN2v88internal7Isolate16ClearKeptObjectsEv:
.LFB25423:
	.cfi_startproc
	endbr64
	addq	$37592, %rdi
	jmp	_ZN2v88internal4Heap16ClearKeptObjectsEv@PLT
	.cfi_endproc
.LFE25423:
	.size	_ZN2v88internal7Isolate16ClearKeptObjectsEv, .-_ZN2v88internal7Isolate16ClearKeptObjectsEv
	.section	.text._ZN2v88internal7Isolate39SetHostCleanupFinalizationGroupCallbackEPFvNS_5LocalINS_7ContextEEENS2_INS_17FinalizationGroupEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate39SetHostCleanupFinalizationGroupCallbackEPFvNS_5LocalINS_7ContextEEENS2_INS_17FinalizationGroupEEEE
	.type	_ZN2v88internal7Isolate39SetHostCleanupFinalizationGroupCallbackEPFvNS_5LocalINS_7ContextEEENS2_INS_17FinalizationGroupEEEE, @function
_ZN2v88internal7Isolate39SetHostCleanupFinalizationGroupCallbackEPFvNS_5LocalINS_7ContextEEENS2_INS_17FinalizationGroupEEEE:
.LFB25424:
	.cfi_startproc
	endbr64
	movq	%rsi, 41296(%rdi)
	ret
	.cfi_endproc
.LFE25424:
	.size	_ZN2v88internal7Isolate39SetHostCleanupFinalizationGroupCallbackEPFvNS_5LocalINS_7ContextEEENS2_INS_17FinalizationGroupEEEE, .-_ZN2v88internal7Isolate39SetHostCleanupFinalizationGroupCallbackEPFvNS_5LocalINS_7ContextEEENS2_INS_17FinalizationGroupEEEE
	.section	.text._ZN2v88internal7Isolate39RunHostCleanupFinalizationGroupCallbackENS0_6HandleINS0_19JSFinalizationGroupEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate39RunHostCleanupFinalizationGroupCallbackENS0_6HandleINS0_19JSFinalizationGroupEEE
	.type	_ZN2v88internal7Isolate39RunHostCleanupFinalizationGroupCallbackENS0_6HandleINS0_19JSFinalizationGroupEEE, @function
_ZN2v88internal7Isolate39RunHostCleanupFinalizationGroupCallbackENS0_6HandleINS0_19JSFinalizationGroupEEE:
.LFB25425:
	.cfi_startproc
	endbr64
	cmpq	$0, 41296(%rdi)
	je	.L2822
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rsi), %rax
	movq	41112(%rdi), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2824
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L2825:
	movq	41296(%rbx), %rax
	addq	$16, %rsp
	movq	%r12, %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2824:
	.cfi_restore_state
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L2830
.L2826:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2822:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L2830:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L2826
	.cfi_endproc
.LFE25425:
	.size	_ZN2v88internal7Isolate39RunHostCleanupFinalizationGroupCallbackENS0_6HandleINS0_19JSFinalizationGroupEEE, .-_ZN2v88internal7Isolate39RunHostCleanupFinalizationGroupCallbackENS0_6HandleINS0_19JSFinalizationGroupEEE
	.section	.text._ZN2v88internal7Isolate38SetHostImportModuleDynamicallyCallbackEPFNS_10MaybeLocalINS_7PromiseEEENS_5LocalINS_7ContextEEENS5_INS_14ScriptOrModuleEEENS5_INS_6StringEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate38SetHostImportModuleDynamicallyCallbackEPFNS_10MaybeLocalINS_7PromiseEEENS_5LocalINS_7ContextEEENS5_INS_14ScriptOrModuleEEENS5_INS_6StringEEEE
	.type	_ZN2v88internal7Isolate38SetHostImportModuleDynamicallyCallbackEPFNS_10MaybeLocalINS_7PromiseEEENS_5LocalINS_7ContextEEENS5_INS_14ScriptOrModuleEEENS5_INS_6StringEEEE, @function
_ZN2v88internal7Isolate38SetHostImportModuleDynamicallyCallbackEPFNS_10MaybeLocalINS_7PromiseEEENS_5LocalINS_7ContextEEENS5_INS_14ScriptOrModuleEEENS5_INS_6StringEEEE:
.LFB25426:
	.cfi_startproc
	endbr64
	movq	%rsi, 41304(%rdi)
	ret
	.cfi_endproc
.LFE25426:
	.size	_ZN2v88internal7Isolate38SetHostImportModuleDynamicallyCallbackEPFNS_10MaybeLocalINS_7PromiseEEENS_5LocalINS_7ContextEEENS5_INS_14ScriptOrModuleEEENS5_INS_6StringEEEE, .-_ZN2v88internal7Isolate38SetHostImportModuleDynamicallyCallbackEPFNS_10MaybeLocalINS_7PromiseEEENS_5LocalINS_7ContextEEENS5_INS_14ScriptOrModuleEEENS5_INS_6StringEEEE
	.section	.text._ZN2v88internal7Isolate41RunHostInitializeImportMetaObjectCallbackENS0_6HandleINS0_16SourceTextModuleEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate41RunHostInitializeImportMetaObjectCallbackENS0_6HandleINS0_16SourceTextModuleEEE
	.type	_ZN2v88internal7Isolate41RunHostInitializeImportMetaObjectCallbackENS0_6HandleINS0_16SourceTextModuleEEE, @function
_ZN2v88internal7Isolate41RunHostInitializeImportMetaObjectCallbackENS0_6HandleINS0_16SourceTextModuleEEE:
.LFB25427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movq	41112(%rdi), %rdi
	movq	87(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2833
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
	cmpq	%rsi, 96(%rbx)
	je	.L2852
.L2836:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2833:
	.cfi_restore_state
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L2853
.L2835:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	cmpq	%rsi, 96(%rbx)
	jne	.L2836
.L2852:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	cmpq	$0, 41312(%rbx)
	movq	%rax, %r12
	je	.L2837
	movq	12464(%rbx), %rax
	movq	39(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2838
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L2839:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	*41312(%rbx)
.L2837:
	movq	0(%r13), %r14
	movq	(%r12), %r13
	movq	%r13, 87(%r14)
	leaq	87(%r14), %r15
	testb	$1, %r13b
	je	.L2836
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L2842
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L2842:
	testb	$24, %al
	je	.L2836
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2836
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2836
	.p2align 4,,10
	.p2align 3
.L2853:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2835
	.p2align 4,,10
	.p2align 3
.L2838:
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L2854
.L2840:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L2839
.L2854:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L2840
	.cfi_endproc
.LFE25427:
	.size	_ZN2v88internal7Isolate41RunHostInitializeImportMetaObjectCallbackENS0_6HandleINS0_16SourceTextModuleEEE, .-_ZN2v88internal7Isolate41RunHostInitializeImportMetaObjectCallbackENS0_6HandleINS0_16SourceTextModuleEEE
	.section	.text._ZN2v88internal7Isolate41SetHostInitializeImportMetaObjectCallbackEPFvNS_5LocalINS_7ContextEEENS2_INS_6ModuleEEENS2_INS_6ObjectEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate41SetHostInitializeImportMetaObjectCallbackEPFvNS_5LocalINS_7ContextEEENS2_INS_6ModuleEEENS2_INS_6ObjectEEEE
	.type	_ZN2v88internal7Isolate41SetHostInitializeImportMetaObjectCallbackEPFvNS_5LocalINS_7ContextEEENS2_INS_6ModuleEEENS2_INS_6ObjectEEEE, @function
_ZN2v88internal7Isolate41SetHostInitializeImportMetaObjectCallbackEPFvNS_5LocalINS_7ContextEEENS2_INS_6ModuleEEENS2_INS_6ObjectEEEE:
.LFB25428:
	.cfi_startproc
	endbr64
	movq	%rsi, 41312(%rdi)
	ret
	.cfi_endproc
.LFE25428:
	.size	_ZN2v88internal7Isolate41SetHostInitializeImportMetaObjectCallbackEPFvNS_5LocalINS_7ContextEEENS2_INS_6ModuleEEENS2_INS_6ObjectEEEE, .-_ZN2v88internal7Isolate41SetHostInitializeImportMetaObjectCallbackEPFvNS_5LocalINS_7ContextEEENS2_INS_6ModuleEEENS2_INS_6ObjectEEEE
	.section	.text._ZN2v88internal7Isolate28RunPrepareStackTraceCallbackENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEENS2_INS0_7JSArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate28RunPrepareStackTraceCallbackENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEENS2_INS0_7JSArrayEEE
	.type	_ZN2v88internal7Isolate28RunPrepareStackTraceCallbackENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEENS2_INS0_7JSArrayEEE, @function
_ZN2v88internal7Isolate28RunPrepareStackTraceCallbackENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEENS2_INS0_7JSArrayEEE:
.LFB25429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	subq	$8, %rsp
	call	*45792(%rbx)
	testq	%rax, %rax
	je	.L2862
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2862:
	.cfi_restore_state
	movq	12552(%rbx), %rdx
	movq	96(%rbx), %rcx
	movq	%rcx, 12552(%rbx)
	movq	%rdx, 12480(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25429:
	.size	_ZN2v88internal7Isolate28RunPrepareStackTraceCallbackENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEENS2_INS0_7JSArrayEEE, .-_ZN2v88internal7Isolate28RunPrepareStackTraceCallbackENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEENS2_INS0_7JSArrayEEE
	.section	.text._ZN2v88internal7Isolate37LookupOrAddExternallyCompiledFilenameEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate37LookupOrAddExternallyCompiledFilenameEPKc
	.type	_ZN2v88internal7Isolate37LookupOrAddExternallyCompiledFilenameEPKc, @function
_ZN2v88internal7Isolate37LookupOrAddExternallyCompiledFilenameEPKc:
.LFB25430:
	.cfi_startproc
	endbr64
	movq	45776(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2864
	movq	(%rdi), %rax
	jmp	*(%rax)
	.p2align 4,,10
	.p2align 3
.L2864:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25430:
	.size	_ZN2v88internal7Isolate37LookupOrAddExternallyCompiledFilenameEPKc, .-_ZN2v88internal7Isolate37LookupOrAddExternallyCompiledFilenameEPKc
	.section	.text._ZNK2v88internal7Isolate29GetExternallyCompiledFilenameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Isolate29GetExternallyCompiledFilenameEi
	.type	_ZNK2v88internal7Isolate29GetExternallyCompiledFilenameEi, @function
_ZNK2v88internal7Isolate29GetExternallyCompiledFilenameEi:
.LFB25431:
	.cfi_startproc
	endbr64
	movq	45776(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2866
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L2866:
	leaq	.LC12(%rip), %rax
	ret
	.cfi_endproc
.LFE25431:
	.size	_ZNK2v88internal7Isolate29GetExternallyCompiledFilenameEi, .-_ZNK2v88internal7Isolate29GetExternallyCompiledFilenameEi
	.section	.text._ZNK2v88internal7Isolate34GetExternallyCompiledFilenameCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Isolate34GetExternallyCompiledFilenameCountEv
	.type	_ZNK2v88internal7Isolate34GetExternallyCompiledFilenameCountEv, @function
_ZNK2v88internal7Isolate34GetExternallyCompiledFilenameCountEv:
.LFB25432:
	.cfi_startproc
	endbr64
	movq	45776(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2868
	movq	(%rdi), %rax
	jmp	*16(%rax)
	.p2align 4,,10
	.p2align 3
.L2868:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25432:
	.size	_ZNK2v88internal7Isolate34GetExternallyCompiledFilenameCountEv, .-_ZNK2v88internal7Isolate34GetExternallyCompiledFilenameCountEv
	.section	.text._ZN2v88internal7Isolate31PrepareBuiltinSourcePositionMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate31PrepareBuiltinSourcePositionMapEv
	.type	_ZN2v88internal7Isolate31PrepareBuiltinSourcePositionMapEv, @function
_ZN2v88internal7Isolate31PrepareBuiltinSourcePositionMapEv:
.LFB25433:
	.cfi_startproc
	endbr64
	movq	45776(%rdi), %r8
	testq	%r8, %r8
	je	.L2869
	movq	(%r8), %rax
	leaq	41184(%rdi), %rsi
	movq	%r8, %rdi
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2869:
	ret
	.cfi_endproc
.LFE25433:
	.size	_ZN2v88internal7Isolate31PrepareBuiltinSourcePositionMapEv, .-_ZN2v88internal7Isolate31PrepareBuiltinSourcePositionMapEv
	.section	.text._ZN2v88internal7Isolate28SetPrepareStackTraceCallbackEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENS5_IS3_EENS5_INS_5ArrayEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate28SetPrepareStackTraceCallbackEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENS5_IS3_EENS5_INS_5ArrayEEEE
	.type	_ZN2v88internal7Isolate28SetPrepareStackTraceCallbackEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENS5_IS3_EENS5_INS_5ArrayEEEE, @function
_ZN2v88internal7Isolate28SetPrepareStackTraceCallbackEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENS5_IS3_EENS5_INS_5ArrayEEEE:
.LFB25434:
	.cfi_startproc
	endbr64
	movq	%rsi, 45792(%rdi)
	ret
	.cfi_endproc
.LFE25434:
	.size	_ZN2v88internal7Isolate28SetPrepareStackTraceCallbackEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENS5_IS3_EENS5_INS_5ArrayEEEE, .-_ZN2v88internal7Isolate28SetPrepareStackTraceCallbackEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENS5_IS3_EENS5_INS_5ArrayEEEE
	.section	.text._ZNK2v88internal7Isolate28HasPrepareStackTraceCallbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Isolate28HasPrepareStackTraceCallbackEv
	.type	_ZNK2v88internal7Isolate28HasPrepareStackTraceCallbackEv, @function
_ZNK2v88internal7Isolate28HasPrepareStackTraceCallbackEv:
.LFB25435:
	.cfi_startproc
	endbr64
	cmpq	$0, 45792(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE25435:
	.size	_ZNK2v88internal7Isolate28HasPrepareStackTraceCallbackEv, .-_ZNK2v88internal7Isolate28HasPrepareStackTraceCallbackEv
	.section	.text._ZN2v88internal7Isolate22SetAddCrashKeyCallbackEPFvNS_10CrashKeyIdERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate22SetAddCrashKeyCallbackEPFvNS_10CrashKeyIdERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE
	.type	_ZN2v88internal7Isolate22SetAddCrashKeyCallbackEPFvNS_10CrashKeyIdERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE, @function
_ZN2v88internal7Isolate22SetAddCrashKeyCallbackEPFvNS_10CrashKeyIdERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE:
.LFB25436:
	.cfi_startproc
	endbr64
	movq	%rsi, 45896(%rdi)
	jmp	_ZN2v88internal7Isolate37AddCrashKeysForIsolateAndHeapPointersEv
	.cfi_endproc
.LFE25436:
	.size	_ZN2v88internal7Isolate22SetAddCrashKeyCallbackEPFvNS_10CrashKeyIdERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE, .-_ZN2v88internal7Isolate22SetAddCrashKeyCallbackEPFvNS_10CrashKeyIdERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE
	.section	.text._ZN2v88internal7Isolate22SetAtomicsWaitCallbackEPFvNS_7Isolate16AtomicsWaitEventENS_5LocalINS_17SharedArrayBufferEEEmldPNS2_21AtomicsWaitWakeHandleEPvES9_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate22SetAtomicsWaitCallbackEPFvNS_7Isolate16AtomicsWaitEventENS_5LocalINS_17SharedArrayBufferEEEmldPNS2_21AtomicsWaitWakeHandleEPvES9_
	.type	_ZN2v88internal7Isolate22SetAtomicsWaitCallbackEPFvNS_7Isolate16AtomicsWaitEventENS_5LocalINS_17SharedArrayBufferEEEmldPNS2_21AtomicsWaitWakeHandleEPvES9_, @function
_ZN2v88internal7Isolate22SetAtomicsWaitCallbackEPFvNS_7Isolate16AtomicsWaitEventENS_5LocalINS_17SharedArrayBufferEEEmldPNS2_21AtomicsWaitWakeHandleEPvES9_:
.LFB25437:
	.cfi_startproc
	endbr64
	movq	%rsi, 41272(%rdi)
	movq	%rdx, 41280(%rdi)
	ret
	.cfi_endproc
.LFE25437:
	.size	_ZN2v88internal7Isolate22SetAtomicsWaitCallbackEPFvNS_7Isolate16AtomicsWaitEventENS_5LocalINS_17SharedArrayBufferEEEmldPNS2_21AtomicsWaitWakeHandleEPvES9_, .-_ZN2v88internal7Isolate22SetAtomicsWaitCallbackEPFvNS_7Isolate16AtomicsWaitEventENS_5LocalINS_17SharedArrayBufferEEEmldPNS2_21AtomicsWaitWakeHandleEPvES9_
	.section	.text._ZN2v88internal7Isolate22RunAtomicsWaitCallbackENS_7Isolate16AtomicsWaitEventENS0_6HandleINS0_13JSArrayBufferEEEmldPNS0_21AtomicsWaitWakeHandleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate22RunAtomicsWaitCallbackENS_7Isolate16AtomicsWaitEventENS0_6HandleINS0_13JSArrayBufferEEEmldPNS0_21AtomicsWaitWakeHandleE
	.type	_ZN2v88internal7Isolate22RunAtomicsWaitCallbackENS_7Isolate16AtomicsWaitEventENS0_6HandleINS0_13JSArrayBufferEEEmldPNS0_21AtomicsWaitWakeHandleE, @function
_ZN2v88internal7Isolate22RunAtomicsWaitCallbackENS_7Isolate16AtomicsWaitEventENS0_6HandleINS0_13JSArrayBufferEEEmldPNS0_21AtomicsWaitWakeHandleE:
.LFB25438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	41272(%rdi), %rax
	testq	%rax, %rax
	je	.L2875
	movq	41088(%r12), %r13
	movl	%esi, %edi
	addl	$1, 41104(%r12)
	movq	%rdx, %rsi
	movq	41096(%r12), %rbx
	movq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%r9, %r8
	movq	41280(%r12), %r9
	call	*%rax
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2875
	movq	%rbx, 41096(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L2875:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25438:
	.size	_ZN2v88internal7Isolate22RunAtomicsWaitCallbackENS_7Isolate16AtomicsWaitEventENS0_6HandleINS0_13JSArrayBufferEEEmldPNS0_21AtomicsWaitWakeHandleE, .-_ZN2v88internal7Isolate22RunAtomicsWaitCallbackENS_7Isolate16AtomicsWaitEventENS0_6HandleINS0_13JSArrayBufferEEEmldPNS0_21AtomicsWaitWakeHandleE
	.section	.text._ZN2v88internal7Isolate14SetPromiseHookEPFvNS_15PromiseHookTypeENS_5LocalINS_7PromiseEEENS3_INS_5ValueEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate14SetPromiseHookEPFvNS_15PromiseHookTypeENS_5LocalINS_7PromiseEEENS3_INS_5ValueEEEE
	.type	_ZN2v88internal7Isolate14SetPromiseHookEPFvNS_15PromiseHookTypeENS_5LocalINS_7PromiseEEENS3_INS_5ValueEEEE, @function
_ZN2v88internal7Isolate14SetPromiseHookEPFvNS_15PromiseHookTypeENS_5LocalINS_7PromiseEEENS3_INS_5ValueEEEE:
.LFB25439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	$1, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, 41288(%rdi)
	testq	%rsi, %rsi
	je	.L2893
.L2880:
	movq	4568(%rbx), %rdx
	movl	$1, %eax
	cmpl	$1, 27(%rdx)
	je	.L2894
.L2881:
	movb	%r12b, 45664(%rbx)
	movb	%al, 45665(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2893:
	.cfi_restore_state
	cmpq	$0, 45656(%rdi)
	jne	.L2880
	movq	41472(%rdi), %rax
	movzbl	8(%rax), %r12d
	xorl	%eax, %eax
	testb	%r12b, %r12b
	je	.L2881
	xorl	%r12d, %r12d
	jmp	.L2880
	.p2align 4,,10
	.p2align 3
.L2894:
	movq	41112(%rbx), %rdi
	addl	$1, 41104(%rbx)
	movq	41088(%rbx), %r13
	movq	41096(%rbx), %r14
	testq	%rdi, %rdi
	je	.L2882
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2883:
	leaq	4568(%rbx), %rdx
	leaq	.LC71(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	subl	$1, 41104(%rbx)
	movq	%r13, 41088(%rbx)
	cmpq	41096(%rbx), %r14
	je	.L2885
	movq	%r14, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2885:
	movl	$1, %eax
	jmp	.L2881
	.p2align 4,,10
	.p2align 3
.L2882:
	movq	%r13, %rcx
	cmpq	%r14, %r13
	je	.L2895
.L2884:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	$0, (%rcx)
	jmp	.L2883
	.p2align 4,,10
	.p2align 3
.L2895:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L2884
	.cfi_endproc
.LFE25439:
	.size	_ZN2v88internal7Isolate14SetPromiseHookEPFvNS_15PromiseHookTypeENS_5LocalINS_7PromiseEEENS3_INS_5ValueEEEE, .-_ZN2v88internal7Isolate14SetPromiseHookEPFvNS_15PromiseHookTypeENS_5LocalINS_7PromiseEEENS3_INS_5ValueEEEE
	.section	.text._ZN2v88internal7Isolate35RunPromiseHookForAsyncEventDelegateENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate35RunPromiseHookForAsyncEventDelegateENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEE
	.type	_ZN2v88internal7Isolate35RunPromiseHookForAsyncEventDelegateENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEE, @function
_ZN2v88internal7Isolate35RunPromiseHookForAsyncEventDelegateENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEE:
.LFB25441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1544, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -1568(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 45656(%rdi)
	sete	%dl
	cmpl	$1, %esi
	sete	%al
	orb	%al, %dl
	jne	.L2896
	movq	%rdi, %r13
	cmpl	$2, %esi
	je	.L2949
	cmpl	$3, %esi
	je	.L2950
	leaq	-1504(%rbp), %rax
	movq	%rdi, %rsi
	movq	%rax, %rdi
	movq	%rax, -1584(%rbp)
	movq	%rax, %rbx
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L2896
	movq	%rbx, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2896
	leaq	-1536(%rbp), %rax
	xorl	%ebx, %ebx
	leaq	-1544(%rbp), %r12
	movl	$0, -1556(%rbp)
	movq	%rax, -1576(%rbp)
.L2914:
	movq	-1576(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, -1520(%rbp)
	movaps	%xmm0, -1536(%rbp)
	call	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE@PLT
	movq	-1536(%rbp), %rcx
	movq	-1528(%rbp), %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	je	.L2903
	movl	$1, %r15d
	jmp	.L2911
	.p2align 4,,10
	.p2align 3
.L2904:
	movq	%r12, %rdi
	movq	%rax, -1544(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L2946
	movq	(%r14), %rcx
	movq	7(%rcx), %rbx
	notq	%rbx
	andl	$1, %ebx
	jne	.L2951
.L2910:
	movq	-1536(%rbp), %rcx
	movq	-1528(%rbp), %rax
	addq	$1, %r15
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r15
	ja	.L2903
.L2911:
	subq	%r15, %rax
	movq	(%rcx,%rax,8), %r14
	movq	(%r14), %rcx
	movq	31(%rcx), %rax
	testb	$1, %al
	je	.L2904
	movq	-1(%rax), %rsi
	cmpw	$86, 11(%rsi)
	je	.L2952
.L2905:
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	%rax, -37504(%rsi)
	jne	.L2904
	movq	7(%rcx), %rbx
	notq	%rbx
	andl	$1, %ebx
	je	.L2910
	.p2align 4,,10
	.p2align 3
.L2951:
	movq	(%r14), %rax
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$503, %rax
	je	.L2916
	movq	(%r14), %rax
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$505, %rax
	je	.L2917
	movq	(%r14), %rax
	movl	$2, %edx
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$512, %rax
	cmovne	-1556(%rbp), %edx
	movl	$0, %eax
	cmovne	%eax, %ebx
	movl	%edx, -1556(%rbp)
	jmp	.L2910
	.p2align 4,,10
	.p2align 3
.L2949:
	movq	-1568(%rbp), %r15
	leaq	-1504(%rbp), %r12
	movq	%r12, %rdi
	movq	(%r15), %rax
	movq	%rax, -1504(%rbp)
	call	_ZNK2v88internal9JSPromise13async_task_idEv@PLT
	testl	%eax, %eax
	je	.L2896
	movq	45656(%r13), %r13
	movq	%r12, %rdi
	movq	0(%r13), %rax
	movq	16(%rax), %rbx
	movq	(%r15), %rax
	movq	%rax, -1504(%rbp)
	call	_ZNK2v88internal9JSPromise13async_task_idEv@PLT
	xorl	%ecx, %ecx
	movl	$3, %esi
	movq	%r13, %rdi
	movl	%eax, %edx
	call	*%rbx
	.p2align 4,,10
	.p2align 3
.L2896:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2953
	addq	$1544, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2916:
	.cfi_restore_state
	movl	$0, -1556(%rbp)
	jmp	.L2910
	.p2align 4,,10
	.p2align 3
.L2952:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L2905
	jmp	.L2904
	.p2align 4,,10
	.p2align 3
.L2917:
	movl	$1, -1556(%rbp)
	jmp	.L2910
	.p2align 4,,10
	.p2align 3
.L2903:
	movq	-1584(%rbp), %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-1536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2912
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2914
	jmp	.L2896
	.p2align 4,,10
	.p2align 3
.L2946:
	testb	%bl, %bl
	jne	.L2954
.L2907:
	movq	-1536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2896
	call	_ZdlPv@PLT
	jmp	.L2896
	.p2align 4,,10
	.p2align 3
.L2950:
	movq	-1568(%rbp), %r15
	leaq	-1504(%rbp), %r12
	movq	%r12, %rdi
	movq	(%r15), %rax
	movq	%rax, -1504(%rbp)
	call	_ZNK2v88internal9JSPromise13async_task_idEv@PLT
	testl	%eax, %eax
	je	.L2896
	movq	45656(%r13), %r13
	movq	%r12, %rdi
	movq	0(%r13), %rax
	movq	16(%rax), %rbx
	movq	(%r15), %rax
	movq	%rax, -1504(%rbp)
	call	_ZNK2v88internal9JSPromise13async_task_idEv@PLT
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r13, %rdi
	movl	%eax, %edx
	call	*%rbx
	jmp	.L2896
	.p2align 4,,10
	.p2align 3
.L2912:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2914
	jmp	.L2896
	.p2align 4,,10
	.p2align 3
.L2954:
	movq	-1568(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -1544(%rbp)
	call	_ZNK2v88internal9JSPromise13async_task_idEv@PLT
	testl	%eax, %eax
	je	.L2955
.L2908:
	movq	45656(%r13), %r9
	movq	41472(%r13), %rdi
	movq	%r14, %rsi
	movq	(%r9), %rax
	movq	%r9, -1576(%rbp)
	movq	16(%rax), %rbx
	call	_ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	movq	-1568(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -1544(%rbp)
	call	_ZNK2v88internal9JSPromise13async_task_idEv@PLT
	movq	-1576(%rbp), %r9
	movl	-1556(%rbp), %esi
	movzbl	%r13b, %ecx
	movl	%eax, %edx
	movq	%r9, %rdi
	call	*%rbx
	jmp	.L2907
.L2955:
	movq	-1568(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -1544(%rbp)
	movl	45668(%r13), %eax
	leal	1(%rax), %esi
	movl	%esi, 45668(%r13)
	call	_ZN2v88internal9JSPromise17set_async_task_idEi@PLT
	jmp	.L2908
.L2953:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25441:
	.size	_ZN2v88internal7Isolate35RunPromiseHookForAsyncEventDelegateENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEE, .-_ZN2v88internal7Isolate35RunPromiseHookForAsyncEventDelegateENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEE
	.section	.text._ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE
	.type	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE, @function
_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE:
.LFB25440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal7Isolate35RunPromiseHookForAsyncEventDelegateENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEE
	movq	41288(%rbx), %rax
	testq	%rax, %rax
	je	.L2956
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	%r12d, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2956:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25440:
	.size	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE, .-_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE
	.section	.text._ZN2v88internal7Isolate27OnAsyncFunctionStateChangedENS0_6HandleINS0_9JSPromiseEEENS_5debug20DebugAsyncActionTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate27OnAsyncFunctionStateChangedENS0_6HandleINS0_9JSPromiseEEENS_5debug20DebugAsyncActionTypeE
	.type	_ZN2v88internal7Isolate27OnAsyncFunctionStateChangedENS0_6HandleINS0_9JSPromiseEEENS_5debug20DebugAsyncActionTypeE, @function
_ZN2v88internal7Isolate27OnAsyncFunctionStateChangedENS0_6HandleINS0_9JSPromiseEEENS_5debug20DebugAsyncActionTypeE:
.LFB25451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 45656(%rdi)
	je	.L2959
	movq	(%rsi), %rax
	leaq	-64(%rbp), %r14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%r14, %rdi
	movl	%edx, %r13d
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9JSPromise13async_task_idEv@PLT
	testl	%eax, %eax
	je	.L2964
.L2961:
	movq	45656(%rbx), %r15
	movq	%r14, %rdi
	movq	(%r15), %rax
	movq	16(%rax), %rbx
	movq	(%r12), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9JSPromise13async_task_idEv@PLT
	xorl	%ecx, %ecx
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	%eax, %edx
	call	*%rbx
.L2959:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2965
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2964:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	movl	45668(%rbx), %eax
	leal	1(%rax), %esi
	movl	%esi, 45668(%rbx)
	call	_ZN2v88internal9JSPromise17set_async_task_idEi@PLT
	jmp	.L2961
.L2965:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25451:
	.size	_ZN2v88internal7Isolate27OnAsyncFunctionStateChangedENS0_6HandleINS0_9JSPromiseEEENS_5debug20DebugAsyncActionTypeE, .-_ZN2v88internal7Isolate27OnAsyncFunctionStateChangedENS0_6HandleINS0_9JSPromiseEEENS_5debug20DebugAsyncActionTypeE
	.section	.text._ZN2v88internal7Isolate24SetPromiseRejectCallbackEPFvNS_20PromiseRejectMessageEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate24SetPromiseRejectCallbackEPFvNS_20PromiseRejectMessageEE
	.type	_ZN2v88internal7Isolate24SetPromiseRejectCallbackEPFvNS_20PromiseRejectMessageEE, @function
_ZN2v88internal7Isolate24SetPromiseRejectCallbackEPFvNS_20PromiseRejectMessageEE:
.LFB25452:
	.cfi_startproc
	endbr64
	movq	%rsi, 41784(%rdi)
	ret
	.cfi_endproc
.LFE25452:
	.size	_ZN2v88internal7Isolate24SetPromiseRejectCallbackEPFvNS_20PromiseRejectMessageEE, .-_ZN2v88internal7Isolate24SetPromiseRejectCallbackEPFvNS_20PromiseRejectMessageEE
	.section	.text._ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE
	.type	_ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE, @function
_ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE:
.LFB25453:
	.cfi_startproc
	endbr64
	movq	41784(%rdi), %rax
	testq	%rax, %rax
	je	.L2973
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$40, %rsp
	movl	%ecx, -24(%rbp)
	movq	%rdx, -16(%rbp)
	pushq	-16(%rbp)
	pushq	-24(%rbp)
	pushq	%rsi
	call	*%rax
	addq	$32, %rsp
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2973:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE25453:
	.size	_ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE, .-_ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE
	.section	.text._ZN2v88internal7Isolate21SetUseCounterCallbackEPFvPNS_7IsolateENS2_17UseCounterFeatureEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate21SetUseCounterCallbackEPFvPNS_7IsolateENS2_17UseCounterFeatureEE
	.type	_ZN2v88internal7Isolate21SetUseCounterCallbackEPFvPNS_7IsolateENS2_17UseCounterFeatureEE, @function
_ZN2v88internal7Isolate21SetUseCounterCallbackEPFvPNS_7IsolateENS2_17UseCounterFeatureEE:
.LFB25454:
	.cfi_startproc
	endbr64
	movq	%rsi, 45488(%rdi)
	ret
	.cfi_endproc
.LFE25454:
	.size	_ZN2v88internal7Isolate21SetUseCounterCallbackEPFvPNS_7IsolateENS2_17UseCounterFeatureEE, .-_ZN2v88internal7Isolate21SetUseCounterCallbackEPFvPNS_7IsolateENS2_17UseCounterFeatureEE
	.section	.text._ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE
	.type	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE, @function
_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE:
.LFB25455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	37984(%rdi), %eax
	testl	%eax, %eax
	jne	.L2978
	movq	45488(%rdi), %rax
	testq	%rax, %rax
	je	.L2977
	movq	41088(%rdi), %r13
	addl	$1, 41104(%rdi)
	movq	41096(%rdi), %rbx
	call	*%rax
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2977
	movq	%rbx, 41096(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L2977:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2978:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	37592(%rdi), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4Heap22IncrementDeferredCountENS_7Isolate17UseCounterFeatureE@PLT
	.cfi_endproc
.LFE25455:
	.size	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE, .-_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE
	.section	.rodata._ZN2v88internal7Isolate19GetTurboCfgFileNameB5cxx11EPS1_.str1.1,"aMS",@progbits,1
.LC74:
	.string	"turbo-"
.LC75:
	.string	"-"
.LC76:
	.string	"any"
.LC77:
	.string	".cfg"
	.section	.text._ZN2v88internal7Isolate19GetTurboCfgFileNameB5cxx11EPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate19GetTurboCfgFileNameB5cxx11EPS1_
	.type	_ZN2v88internal7Isolate19GetTurboCfgFileNameB5cxx11EPS1_, @function
_ZN2v88internal7Isolate19GetTurboCfgFileNameB5cxx11EPS1_:
.LFB25456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	16(%rdi), %rbx
	subq	$440, %rsp
	movq	_ZN2v88internal25FLAG_trace_turbo_cfg_fileE(%rip), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L3003
	movq	%rbx, (%rdi)
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, -440(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L3004
	cmpq	$1, %rax
	jne	.L2996
	movzbl	0(%r13), %edx
	movb	%dl, 16(%r15)
.L2997:
	movq	%rax, 8(%r15)
	movb	$0, (%rbx,%rax)
.L2985:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3005
	addq	$440, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3004:
	.cfi_restore_state
	leaq	-440(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r15)
	movq	%rax, %rbx
	movq	-440(%rbp), %rax
	movq	%rax, 16(%r15)
.L2995:
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-440(%rbp), %rax
	movq	(%r15), %rbx
	jmp	.L2997
	.p2align 4,,10
	.p2align 3
.L3003:
	movq	.LC78(%rip), %xmm1
	leaq	-320(%rbp), %r13
	movq	%rsi, -472(%rbp)
	leaq	-432(%rbp), %r12
	movq	%r13, %rdi
	leaq	-368(%rbp), %r14
	movhps	.LC6(%rip), %xmm1
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	%rax, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-464(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$6, %edx
	leaq	.LC74(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC75(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-472(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L2987
	movl	40800(%rcx), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
.L2988:
	movl	$4, %edx
	leaq	.LC77(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-384(%rbp), %rax
	movq	%rbx, (%r15)
	movq	$0, 8(%r15)
	movb	$0, 16(%r15)
	testq	%rax, %rax
	je	.L2989
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L3006
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L2991:
	movq	.LC78(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC7(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L2992
	call	_ZdlPv@PLT
.L2992:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2985
	.p2align 4,,10
	.p2align 3
.L2996:
	testq	%rax, %rax
	je	.L2997
	jmp	.L2995
	.p2align 4,,10
	.p2align 3
.L3006:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2991
	.p2align 4,,10
	.p2align 3
.L2987:
	movl	$3, %edx
	leaq	.LC76(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L2988
	.p2align 4,,10
	.p2align 3
.L2989:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L2991
.L3005:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25456:
	.size	_ZN2v88internal7Isolate19GetTurboCfgFileNameB5cxx11EPS1_, .-_ZN2v88internal7Isolate19GetTurboCfgFileNameB5cxx11EPS1_
	.section	.text._ZN2v88internal7Isolate18AddDetachedContextENS0_6HandleINS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate18AddDetachedContextENS0_6HandleINS0_7ContextEEE
	.type	_ZN2v88internal7Isolate18AddDetachedContextENS0_6HandleINS0_7ContextEEE, @function
_ZN2v88internal7Isolate18AddDetachedContextENS0_6HandleINS0_7ContextEEE:
.LFB25457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	4696(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	41088(%rdi), %r13
	movq	41096(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdi)
	movq	41112(%rdi), %rdi
	movl	$1, -80(%rbp)
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rsi
	testq	%rdi, %rdi
	je	.L3008
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %r8
.L3009:
	leaq	-80(%rbp), %r15
	movq	%r8, %rsi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	movq	%r15, %rdx
	call	_ZN2v88internal13WeakArrayList8AddToEndEPNS0_7IsolateENS0_6HandleIS1_EERKNS0_17MaybeObjectHandleE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rbx, -72(%rbp)
	movq	%rax, %rsi
	movl	$0, -80(%rbp)
	call	_ZN2v88internal13WeakArrayList8AddToEndEPNS0_7IsolateENS0_6HandleIS1_EERKNS0_17MaybeObjectHandleE@PLT
	movq	(%rax), %rax
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, 4696(%r12)
	cmpq	41096(%r12), %r14
	je	.L3007
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3007:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3015
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3008:
	.cfi_restore_state
	movq	%r13, %rax
	cmpq	%r14, %r13
	je	.L3016
.L3010:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3009
	.p2align 4,,10
	.p2align 3
.L3016:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %r8
	jmp	.L3010
.L3015:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25457:
	.size	_ZN2v88internal7Isolate18AddDetachedContextENS0_6HandleINS0_7ContextEEE, .-_ZN2v88internal7Isolate18AddDetachedContextENS0_6HandleINS0_7ContextEEE
	.section	.rodata._ZN2v88internal7Isolate28CheckDetachedContextsAfterGCEv.str1.8,"aMS",@progbits,1
	.align 8
.LC79:
	.string	"%d detached contexts are collected out of %d\n"
	.align 8
.LC80:
	.string	"detached context %p\n survived %d GCs (leak?)\n"
	.section	.text._ZN2v88internal7Isolate28CheckDetachedContextsAfterGCEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate28CheckDetachedContextsAfterGCEv
	.type	_ZN2v88internal7Isolate28CheckDetachedContextsAfterGCEv, @function
_ZN2v88internal7Isolate28CheckDetachedContextsAfterGCEv:
.LFB25458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	41088(%rdi), %rax
	movq	41096(%rdi), %r12
	movq	%rax, -56(%rbp)
	movl	41104(%rdi), %eax
	leal	1(%rax), %edx
	movl	%edx, 41104(%rdi)
	movq	4696(%rdi), %rdx
	movslq	19(%rdx), %r9
	testq	%r9, %r9
	je	.L3049
	jle	.L3020
	leal	-1(%r9), %r10d
	xorl	%r13d, %r13d
	movq	%r12, -72(%rbp)
	movl	$24, %ebx
	movl	%r10d, %r8d
	movl	%r10d, -76(%rbp)
	movl	%r13d, %r14d
	movq	%r9, %r15
	shrl	%r8d
	movq	%rdi, %r13
	salq	$4, %r8
	addq	$40, %r8
	movq	%r8, %r12
	.p2align 4,,10
	.p2align 3
.L3025:
	movq	-1(%rdx,%rbx), %rax
	movq	7(%rbx,%rdx), %rcx
	cmpl	$3, %ecx
	je	.L3021
	sarq	$32, %rax
	leal	24(,%r14,8), %esi
	addl	$1, %eax
	movslq	%esi, %rdi
	addl	$8, %esi
	salq	$32, %rax
	movslq	%esi, %rsi
	movq	%rax, -1(%rdi,%rdx)
	movq	4696(%r13), %rdi
	leaq	-1(%rdi,%rsi), %rsi
	movq	%rcx, (%rsi)
	testb	$1, %cl
	je	.L3035
	movq	%rcx, %rdx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	andq	$-3, %rdx
	testl	$262144, %eax
	je	.L3023
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%rcx), %rax
.L3023:
	testb	$24, %al
	je	.L3035
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3050
	.p2align 4,,10
	.p2align 3
.L3035:
	movq	4696(%r13), %rdx
	addl	$2, %r14d
.L3021:
	addq	$16, %rbx
	cmpq	%rbx, %r12
	jne	.L3025
	movq	%r13, %rax
	movslq	%r14d, %rsi
	movq	-72(%rbp), %r12
	movl	-76(%rbp), %r10d
	movq	%rax, %r14
	movq	%rsi, %rax
	movq	%r15, %r9
	movq	%rsi, %r13
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	cmpl	%r15d, %esi
	jge	.L3026
	subl	%esi, %r10d
	leal	24(,%rsi,8), %eax
	leaq	4(%rsi,%r10), %rcx
	cltq
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L3028:
	movq	4696(%r14), %rdx
	addq	$8, %rax
	movq	$0, -9(%rax,%rdx)
	cmpq	%rax, %rcx
	jne	.L3028
	cmpb	$0, _ZN2v88internal28FLAG_trace_detached_contextsE(%rip)
	movl	%r9d, %r13d
	jne	.L3051
.L3032:
	movq	-56(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	cmpq	41096(%r14), %r12
	je	.L3017
	movq	%r12, 41096(%r14)
	addq	$72, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L3049:
	.cfi_restore_state
	movl	%eax, 41104(%rdi)
.L3017:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3050:
	.cfi_restore_state
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3035
	.p2align 4,,10
	.p2align 3
.L3051:
	movl	%r9d, %edx
	xorl	%esi, %esi
	leaq	.LC79(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L3036:
	movl	$24, %ebx
	xorl	%r15d, %r15d
	leaq	.LC80(%rip), %rdi
	jmp	.L3034
	.p2align 4,,10
	.p2align 3
.L3033:
	addl	$2, %r15d
	addq	$16, %rbx
	cmpl	%r13d, %r15d
	jge	.L3032
.L3034:
	movq	4696(%r14), %rax
	movq	-1(%rbx,%rax), %rdx
	movq	7(%rbx,%rax), %rsi
	sarq	$32, %rdx
	cmpq	$3, %rdx
	jle	.L3033
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	.LC80(%rip), %rdi
	jmp	.L3033
	.p2align 4,,10
	.p2align 3
.L3020:
	movq	$0, 15(%rdx)
	leaq	15(%rdx), %rcx
	xorl	%r13d, %r13d
.L3026:
	cmpb	$0, _ZN2v88internal28FLAG_trace_detached_contextsE(%rip)
	je	.L3032
	movl	%r9d, %esi
	xorl	%eax, %eax
	leaq	.LC79(%rip), %rdi
	movl	%r9d, %edx
	subl	%r13d, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	testl	%r13d, %r13d
	je	.L3032
	jmp	.L3036
	.cfi_endproc
.LFE25458:
	.size	_ZN2v88internal7Isolate28CheckDetachedContextsAfterGCEv, .-_ZN2v88internal7Isolate28CheckDetachedContextsAfterGCEv
	.section	.text._ZN2v88internal7Isolate15LoadStartTimeMsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate15LoadStartTimeMsEv
	.type	_ZN2v88internal7Isolate15LoadStartTimeMsEv, @function
_ZN2v88internal7Isolate15LoadStartTimeMsEv:
.LFB25459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	41320(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$16, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movsd	41360(%rbx), %xmm0
	movq	%r12, %rdi
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movsd	-24(%rbp), %xmm0
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25459:
	.size	_ZN2v88internal7Isolate15LoadStartTimeMsEv, .-_ZN2v88internal7Isolate15LoadStartTimeMsEv
	.section	.rodata._ZN2v88internal7Isolate11SetRAILModeENS_8RAILModeE.str1.1,"aMS",@progbits,1
.LC81:
	.string	"IDLE"
.LC82:
	.string	"ANIMATION"
.LC83:
	.string	"RESPONSE"
.LC84:
	.string	"LOAD"
.LC85:
	.string	"RAIL mode: %s\n"
	.section	.text._ZN2v88internal7Isolate11SetRAILModeENS_8RAILModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate11SetRAILModeENS_8RAILModeE
	.type	_ZN2v88internal7Isolate11SetRAILModeENS_8RAILModeE, @function
_ZN2v88internal7Isolate11SetRAILModeENS_8RAILModeE:
.LFB25460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movl	41264(%rdi), %eax
	cmpl	$3, %eax
	je	.L3055
	cmpl	$3, %esi
	je	.L3080
.L3055:
	movl	%ebx, 41264(%r12)
	mfence
	cmpl	$3, %eax
	jne	.L3058
	cmpl	$3, %ebx
	je	.L3058
	movq	39656(%r12), %rax
	leaq	37592(%r12), %rsi
	xorl	%edx, %edx
	leaq	89(%rax), %rdi
	call	_ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE@PLT
.L3058:
	cmpb	$0, _ZN2v88internal15FLAG_trace_railE(%rip)
	je	.L3054
	leaq	.LC82(%rip), %rdx
	cmpl	$1, %ebx
	je	.L3060
	leaq	.LC83(%rip), %rdx
	jbe	.L3060
	leaq	.LC81(%rip), %rdx
	cmpl	$2, %ebx
	jne	.L3081
.L3060:
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC85(%rip), %rsi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3080:
	.cfi_restore_state
	leaq	41320(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	37592(%r12), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, 41360(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$3, 41264(%r12)
	mfence
	cmpb	$0, _ZN2v88internal15FLAG_trace_railE(%rip)
	jne	.L3056
.L3054:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3081:
	.cfi_restore_state
	cmpl	$3, %ebx
	je	.L3056
	leaq	.LC12(%rip), %rdx
	jmp	.L3060
	.p2align 4,,10
	.p2align 3
.L3056:
	leaq	.LC84(%rip), %rdx
	jmp	.L3060
	.cfi_endproc
.LFE25460:
	.size	_ZN2v88internal7Isolate11SetRAILModeENS_8RAILModeE, .-_ZN2v88internal7Isolate11SetRAILModeENS_8RAILModeE
	.section	.text._ZN2v88internal7Isolate31IsolateInBackgroundNotificationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate31IsolateInBackgroundNotificationEv
	.type	_ZN2v88internal7Isolate31IsolateInBackgroundNotificationEv, @function
_ZN2v88internal7Isolate31IsolateInBackgroundNotificationEv:
.LFB25461:
	.cfi_startproc
	endbr64
	movb	$1, 41460(%rdi)
	addq	$37592, %rdi
	jmp	_ZN2v88internal4Heap29ActivateMemoryReducerIfNeededEv@PLT
	.cfi_endproc
.LFE25461:
	.size	_ZN2v88internal7Isolate31IsolateInBackgroundNotificationEv, .-_ZN2v88internal7Isolate31IsolateInBackgroundNotificationEv
	.section	.text._ZN2v88internal7Isolate31IsolateInForegroundNotificationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate31IsolateInForegroundNotificationEv
	.type	_ZN2v88internal7Isolate31IsolateInForegroundNotificationEv, @function
_ZN2v88internal7Isolate31IsolateInForegroundNotificationEv:
.LFB25462:
	.cfi_startproc
	endbr64
	movb	$0, 41460(%rdi)
	ret
	.cfi_endproc
.LFE25462:
	.size	_ZN2v88internal7Isolate31IsolateInForegroundNotificationEv, .-_ZN2v88internal7Isolate31IsolateInForegroundNotificationEv
	.section	.rodata._ZN2v88internal7Isolate18PrintWithTimestampEPKcz.str1.1,"aMS",@progbits,1
.LC86:
	.string	"[%d:%p] %8.0f ms: "
	.section	.text._ZN2v88internal7Isolate18PrintWithTimestampEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz
	.type	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz, @function
_ZN2v88internal7Isolate18PrintWithTimestampEPKcz:
.LFB25463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$224, %rsp
	movq	%rdx, -176(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L3085
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L3085:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	leaq	37592(%r12), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	subsd	41464(%r12), %xmm0
	movsd	%xmm0, -232(%rbp)
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movsd	-232(%rbp), %xmm0
	movq	%r12, %rdx
	leaq	.LC86(%rip), %rdi
	movl	%eax, %esi
	movl	$1, %eax
	call	_ZN2v84base2OS5PrintEPKcz@PLT
	leaq	16(%rbp), %rax
	leaq	-224(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -216(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -208(%rbp)
	movl	$16, -224(%rbp)
	movl	$48, -220(%rbp)
	call	_ZN2v84base2OS6VPrintEPKcP13__va_list_tag@PLT
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3088
	addq	$224, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3088:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25463:
	.size	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz, .-_ZN2v88internal7Isolate18PrintWithTimestampEPKcz
	.section	.text._ZN2v88internal7Isolate7SetIdleEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate7SetIdleEb
	.type	_ZN2v88internal7Isolate7SetIdleEb, @function
_ZN2v88internal7Isolate7SetIdleEb:
.LFB25464:
	.cfi_startproc
	endbr64
	cmpb	$0, 41812(%rdi)
	je	.L3089
	cmpq	$0, 12600(%rdi)
	jne	.L3089
	testb	%sil, %sil
	je	.L3091
	movl	$7, 12616(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L3091:
	cmpl	$7, 12616(%rdi)
	jne	.L3089
	movl	$6, 12616(%rdi)
.L3089:
	ret
	.cfi_endproc
.LFE25464:
	.size	_ZN2v88internal7Isolate7SetIdleEb, .-_ZN2v88internal7Isolate7SetIdleEb
	.section	.rodata._ZN2v88internal7Isolate42CollectSourcePositionsForAllBytecodeArraysEv.str1.1,"aMS",@progbits,1
.LC87:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal7Isolate42CollectSourcePositionsForAllBytecodeArraysEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate42CollectSourcePositionsForAllBytecodeArraysEv
	.type	_ZN2v88internal7Isolate42CollectSourcePositionsForAllBytecodeArraysEv, @function
_ZN2v88internal7Isolate42CollectSourcePositionsForAllBytecodeArraysEv:
.LFB25465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	37592(%rdi), %rsi
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	41096(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdi)
	movq	41088(%rdi), %rax
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L3093
	movq	$0, -112(%rbp)
	movq	%rax, %rsi
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3094:
	movq	%r15, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3148
.L3116:
	movq	-1(%rsi), %rax
	cmpw	$160, 11(%rax)
	jne	.L3094
	movq	7(%rsi), %rax
	leaq	7(%rsi), %rdx
	testb	$1, %al
	jne	.L3095
.L3098:
	movq	(%rdx), %rax
	testb	$1, %al
	je	.L3094
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L3094
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3149
.L3099:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
	cmpq	%rbx, -112(%rbp)
	je	.L3104
.L3152:
	movq	%rdx, (%rbx)
	movq	%r15, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L3116
	.p2align 4,,10
	.p2align 3
.L3148:
	movq	%r15, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	cmpq	%r13, %rbx
	je	.L3117
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L3118:
	movq	(%r15), %rsi
	movq	%r12, %rdi
	addq	$8, %r15
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	cmpq	%r15, %rbx
	jne	.L3118
.L3117:
	testq	%r13, %r13
	je	.L3119
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3119:
	subl	$1, 41104(%r12)
	movq	-104(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L3092
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3092:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3150
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3095:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L3098
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L3099
.L3149:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L3151
.L3103:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	cmpq	%rbx, -112(%rbp)
	jne	.L3152
.L3104:
	movabsq	$1152921504606846975, %rcx
	movq	-112(%rbp), %rsi
	subq	%r13, %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L3153
	testq	%rax, %rax
	je	.L3154
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	ja	.L3155
	testq	%rcx, %rcx
	jne	.L3156
	movq	$0, -112(%rbp)
	movl	$8, %ecx
	xorl	%eax, %eax
.L3109:
	movq	%rdx, (%rax,%rsi)
	cmpq	%r13, %rbx
	je	.L3125
	leaq	-8(%rbx), %rsi
	leaq	15(%rax), %rdx
	subq	%r13, %rsi
	subq	%r13, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L3126
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L3126
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L3112:
	movdqu	0(%r13,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L3112
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rcx
	leaq	0(%r13,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%rdi, %r8
	je	.L3114
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L3114:
	leaq	16(%rax,%rsi), %rbx
.L3110:
	testq	%r13, %r13
	je	.L3115
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L3115:
	movq	%rax, %r13
	jmp	.L3094
	.p2align 4,,10
	.p2align 3
.L3093:
	movq	%r15, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	jmp	.L3119
.L3151:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L3103
.L3156:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L3107:
	movq	%rcx, %rdi
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rcx, -112(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	addq	%rax, %rcx
	movq	%rcx, -112(%rbp)
	leaq	8(%rax), %rcx
	jmp	.L3109
.L3154:
	movl	$8, %ecx
	jmp	.L3107
.L3155:
	movabsq	$9223372036854775800, %rcx
	jmp	.L3107
.L3126:
	movq	%rax, %rcx
	movq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L3111:
	movq	(%rdx), %rdi
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%rdi, -8(%rcx)
	cmpq	%rbx, %rdx
	jne	.L3111
	jmp	.L3114
.L3125:
	movq	%rcx, %rbx
	jmp	.L3110
.L3153:
	leaq	.LC87(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25465:
	.size	_ZN2v88internal7Isolate42CollectSourcePositionsForAllBytecodeArraysEv, .-_ZN2v88internal7Isolate42CollectSourcePositionsForAllBytecodeArraysEv
	.section	.text._ZN2v88internal7Isolate23clear_cached_icu_objectENS1_18ICUObjectCacheTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate23clear_cached_icu_objectENS1_18ICUObjectCacheTypeE
	.type	_ZN2v88internal7Isolate23clear_cached_icu_objectENS1_18ICUObjectCacheTypeE, @function
_ZN2v88internal7Isolate23clear_cached_icu_objectENS1_18ICUObjectCacheTypeE:
.LFB25470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	41408(%rdi), %rcx
	movq	41400(%rdi), %r14
	divq	%rcx
	leaq	0(,%rdx,8), %rax
	leaq	(%r14,%rax), %r15
	movq	%rax, -56(%rbp)
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L3157
	movq	(%r8), %r12
	movq	%rdi, %rbx
	movq	%rdx, %r13
	movq	%r8, %r11
	movq	32(%r12), %rdi
	jmp	.L3161
	.p2align 4,,10
	.p2align 3
.L3159:
	movq	(%r12), %r10
	testq	%r10, %r10
	je	.L3157
	movq	32(%r10), %rdi
	xorl	%edx, %edx
	movq	%r12, %r11
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r13
	jne	.L3157
	movq	%r10, %r12
.L3161:
	cmpq	%rdi, %r9
	jne	.L3159
	cmpl	%esi, 8(%r12)
	jne	.L3159
	movq	(%r12), %rsi
	cmpq	%r11, %r8
	je	.L3186
	testq	%rsi, %rsi
	je	.L3163
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r13
	je	.L3163
	movq	%r11, (%r14,%rdx,8)
	movq	(%r12), %rsi
.L3163:
	movq	%rsi, (%r11)
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L3166
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L3167
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L3168:
	cmpl	$1, %eax
	jne	.L3166
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L3170
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L3171:
	cmpl	$1, %eax
	je	.L3187
	.p2align 4,,10
	.p2align 3
.L3166:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 41424(%rbx)
.L3157:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3186:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L3174
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r13
	je	.L3163
	movq	%r11, (%r14,%rdx,8)
	movq	-56(%rbp), %r15
	addq	41400(%rbx), %r15
	movq	(%r15), %rax
.L3162:
	leaq	41416(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L3188
.L3164:
	movq	$0, (%r15)
	movq	(%r12), %rsi
	jmp	.L3163
	.p2align 4,,10
	.p2align 3
.L3167:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L3168
	.p2align 4,,10
	.p2align 3
.L3174:
	movq	%r11, %rax
	jmp	.L3162
	.p2align 4,,10
	.p2align 3
.L3170:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L3171
	.p2align 4,,10
	.p2align 3
.L3187:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L3166
	.p2align 4,,10
	.p2align 3
.L3188:
	movq	%rsi, 41416(%rbx)
	jmp	.L3164
	.cfi_endproc
.LFE25470:
	.size	_ZN2v88internal7Isolate23clear_cached_icu_objectENS1_18ICUObjectCacheTypeE, .-_ZN2v88internal7Isolate23clear_cached_icu_objectENS1_18ICUObjectCacheTypeE
	.section	.text._ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm
	.type	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm, @function
_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm:
.LFB25471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	subq	%rbx, %rax
	cmpq	37528(%r12), %rax
	popq	%rbx
	setb	%al
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25471:
	.size	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm, .-_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm
	.section	.text._ZN2v88internal11SaveContextC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11SaveContextC2EPNS0_7IsolateE
	.type	_ZN2v88internal11SaveContextC2EPNS0_7IsolateE, @function
_ZN2v88internal11SaveContextC2EPNS0_7IsolateE:
.LFB25473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	12464(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L3192
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3193
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3194:
	movq	%rax, 8(%r12)
.L3192:
	movq	12560(%rbx), %rax
	movq	%rax, 16(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3193:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L3200
.L3195:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L3194
	.p2align 4,,10
	.p2align 3
.L3200:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L3195
	.cfi_endproc
.LFE25473:
	.size	_ZN2v88internal11SaveContextC2EPNS0_7IsolateE, .-_ZN2v88internal11SaveContextC2EPNS0_7IsolateE
	.globl	_ZN2v88internal11SaveContextC1EPNS0_7IsolateE
	.set	_ZN2v88internal11SaveContextC1EPNS0_7IsolateE,_ZN2v88internal11SaveContextC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal11SaveContextD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11SaveContextD2Ev
	.type	_ZN2v88internal11SaveContextD2Ev, @function
_ZN2v88internal11SaveContextD2Ev:
.LFB25476:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movq	(%rdi), %rcx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L3202
	movq	(%rdx), %rax
.L3202:
	movq	%rax, 12464(%rcx)
	ret
	.cfi_endproc
.LFE25476:
	.size	_ZN2v88internal11SaveContextD2Ev, .-_ZN2v88internal11SaveContextD2Ev
	.globl	_ZN2v88internal11SaveContextD1Ev
	.set	_ZN2v88internal11SaveContextD1Ev,_ZN2v88internal11SaveContextD2Ev
	.section	.text._ZN2v88internal11SaveContext12IsBelowFrameEPNS0_13StandardFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11SaveContext12IsBelowFrameEPNS0_13StandardFrameE
	.type	_ZN2v88internal11SaveContext12IsBelowFrameEPNS0_13StandardFrameE, @function
_ZN2v88internal11SaveContext12IsBelowFrameEPNS0_13StandardFrameE:
.LFB25478:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L3205
	cmpq	24(%rsi), %rdx
	seta	%al
.L3205:
	ret
	.cfi_endproc
.LFE25478:
	.size	_ZN2v88internal11SaveContext12IsBelowFrameEPNS0_13StandardFrameE, .-_ZN2v88internal11SaveContext12IsBelowFrameEPNS0_13StandardFrameE
	.section	.text._ZN2v88internal20SaveAndSwitchContextC2EPNS0_7IsolateENS0_7ContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SaveAndSwitchContextC2EPNS0_7IsolateENS0_7ContextE
	.type	_ZN2v88internal20SaveAndSwitchContextC2EPNS0_7IsolateENS0_7ContextE, @function
_ZN2v88internal20SaveAndSwitchContextC2EPNS0_7IsolateENS0_7ContextE:
.LFB25480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	12464(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L3210
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3211
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3212:
	movq	%rax, 8(%r13)
.L3210:
	movq	12560(%rbx), %rax
	movq	%rax, 16(%r13)
	movq	%r12, 12464(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3211:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L3218
.L3213:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L3212
	.p2align 4,,10
	.p2align 3
.L3218:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L3213
	.cfi_endproc
.LFE25480:
	.size	_ZN2v88internal20SaveAndSwitchContextC2EPNS0_7IsolateENS0_7ContextE, .-_ZN2v88internal20SaveAndSwitchContextC2EPNS0_7IsolateENS0_7ContextE
	.globl	_ZN2v88internal20SaveAndSwitchContextC1EPNS0_7IsolateENS0_7ContextE
	.set	_ZN2v88internal20SaveAndSwitchContextC1EPNS0_7IsolateENS0_7ContextE,_ZN2v88internal20SaveAndSwitchContextC2EPNS0_7IsolateENS0_7ContextE
	.section	.text._ZNSt6vectorIN2v88internal6ObjectESaIS2_EE12emplace_backIJS2_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE12emplace_backIJS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE12emplace_backIJS2_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE12emplace_backIJS2_EEEvDpOT_:
.LFB31140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L3220
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3220:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L3246
	testq	%rax, %rax
	je	.L3231
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L3247
.L3223:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L3224:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L3225
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L3234
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L3234
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L3227:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L3227
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L3229
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L3229:
	leaq	16(%r13,%rsi), %rax
.L3225:
	testq	%r14, %r14
	je	.L3230
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L3230:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3247:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L3248
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L3224
	.p2align 4,,10
	.p2align 3
.L3231:
	movl	$8, %r15d
	jmp	.L3223
	.p2align 4,,10
	.p2align 3
.L3234:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L3226:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L3226
	jmp	.L3229
.L3246:
	leaq	.LC87(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3248:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L3223
	.cfi_endproc
.LFE31140:
	.size	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE12emplace_backIJS2_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E:
.LFB31152:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L3265
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L3254:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L3251
	call	_ZdlPv@PLT
.L3251:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3252
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L3249
.L3253:
	movq	%rbx, %r12
	jmp	.L3254
	.p2align 4,,10
	.p2align 3
.L3252:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3253
.L3249:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3265:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE31152:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E:
.LFB31154:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L3285
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L3274:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	136(%r12), %rdi
	movq	16(%r12), %rbx
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3270
	call	_ZdlPv@PLT
.L3270:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3271
	call	_ZdlPv@PLT
.L3271:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3272
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L3268
.L3273:
	movq	%rbx, %r12
	jmp	.L3274
	.p2align 4,,10
	.p2align 3
.L3272:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3273
.L3268:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3285:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE31154:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	.section	.text._ZN2v88internal7Isolate17DumpAndResetStatsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate17DumpAndResetStatsEv
	.type	_ZN2v88internal7Isolate17DumpAndResetStatsEv, @function
_ZN2v88internal7Isolate17DumpAndResetStatsEv:
.LFB25328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$408, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 41760(%rdi)
	je	.L3289
	leaq	-320(%rbp), %r14
	leaq	-400(%rbp), %r13
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movw	%dx, -96(%rbp)
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -320(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	cmpb	$0, _ZN2v88internal16FLAG_turbo_statsE(%rip)
	movq	%rax, -320(%rbp)
	jne	.L3340
	cmpb	$0, _ZN2v88internal20FLAG_turbo_stats_nvpE(%rip)
	movq	41760(%rbx), %r12
	jne	.L3341
.L3294:
	testq	%r12, %r12
	je	.L3298
.L3345:
	leaq	168(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	136(%r12), %r15
	leaq	120(%r12), %rax
	movq	%rax, -440(%rbp)
	testq	%r15, %r15
	je	.L3305
.L3299:
	movq	-440(%rbp), %rdi
	movq	24(%r15), %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	136(%r15), %rdi
	leaq	152(%r15), %rax
	movq	16(%r15), %r13
	cmpq	%rax, %rdi
	je	.L3302
	call	_ZdlPv@PLT
.L3302:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3303
	call	_ZdlPv@PLT
.L3303:
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3304
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L3305
.L3306:
	movq	%r13, %r15
	jmp	.L3299
	.p2align 4,,10
	.p2align 3
.L3310:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3311
.L3300:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3308
	call	_ZdlPv@PLT
.L3308:
	movq	%r12, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L3298:
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	$0, 41760(%rbx)
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L3289:
	cmpb	$0, _ZN2v88internal21FLAG_turbo_stats_wasmE(%rip)
	jne	.L3342
.L3312:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	cmpl	$1, %eax
	je	.L3343
.L3288:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3344
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3304:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3306
.L3305:
	movq	88(%r12), %r15
	leaq	72(%r12), %rax
	movq	%rax, -440(%rbp)
	testq	%r15, %r15
	je	.L3300
.L3301:
	movq	-440(%rbp), %rdi
	movq	24(%r15), %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	movq	16(%r15), %r13
	cmpq	%rax, %rdi
	je	.L3309
	call	_ZdlPv@PLT
.L3309:
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3310
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L3300
.L3311:
	movq	%r13, %r15
	jmp	.L3301
	.p2align 4,,10
	.p2align 3
.L3342:
	movq	45752(%rbx), %rdi
	call	_ZN2v88internal4wasm10WasmEngine27DumpAndResetTurboStatisticsEv@PLT
	jmp	.L3312
	.p2align 4,,10
	.p2align 3
.L3340:
	movq	41760(%rbx), %rax
	movq	%r13, %rdi
	leaq	-432(%rbp), %rsi
	movq	$0, -424(%rbp)
	movq	%rax, -432(%rbp)
	call	_ZN2v88internallsERSoRKNS0_21AsPrintableStatisticsE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r15
	testq	%r15, %r15
	je	.L3295
	cmpb	$0, 56(%r15)
	je	.L3292
	movsbl	67(%r15), %esi
.L3293:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	cmpb	$0, _ZN2v88internal20FLAG_turbo_stats_nvpE(%rip)
	movq	41760(%rbx), %r12
	je	.L3294
.L3341:
	xorl	%eax, %eax
	movq	%r13, %rdi
	leaq	-416(%rbp), %rsi
	movq	%r12, -416(%rbp)
	movl	$0, -407(%rbp)
	movw	%ax, -403(%rbp)
	movb	$0, -401(%rbp)
	movb	$1, -408(%rbp)
	call	_ZN2v88internallsERSoRKNS0_21AsPrintableStatisticsE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L3295
	cmpb	$0, 56(%r13)
	je	.L3296
	movsbl	67(%r13), %esi
.L3297:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	41760(%rbx), %r12
	testq	%r12, %r12
	jne	.L3345
	jmp	.L3298
	.p2align 4,,10
	.p2align 3
.L3343:
	movq	40960(%rbx), %rdi
	leaq	23240(%rdi), %rsi
	addq	$50888, %rdi
	call	_ZN2v88internal28WorkerThreadRuntimeCallStats14AddToMainTableEPNS0_16RuntimeCallStatsE@PLT
	movq	40960(%rbx), %rax
	leaq	23240(%rax), %rdi
	call	_ZN2v88internal16RuntimeCallStats5PrintEv@PLT
	movq	40960(%rbx), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats5ResetEv@PLT
	jmp	.L3288
	.p2align 4,,10
	.p2align 3
.L3292:
	movq	%r15, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3293
	movq	%r15, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L3293
	.p2align 4,,10
	.p2align 3
.L3296:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3297
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L3297
.L3344:
	call	__stack_chk_fail@PLT
.L3295:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE25328:
	.size	_ZN2v88internal7Isolate17DumpAndResetStatsEv, .-_ZN2v88internal7Isolate17DumpAndResetStatsEv
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_14FeedbackVectorEEESaIS4_EE17_M_realloc_insertIJRS3_PNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_14FeedbackVectorEEESaIS4_EE17_M_realloc_insertIJRS3_PNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_14FeedbackVectorEEESaIS4_EE17_M_realloc_insertIJRS3_PNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_14FeedbackVectorEEESaIS4_EE17_M_realloc_insertIJRS3_PNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6HandleINS1_14FeedbackVectorEEESaIS4_EE17_M_realloc_insertIJRS3_PNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB31184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movabsq	$1152921504606846975, %rsi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r8
	movq	(%rdi), %r12
	movq	%r8, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L3376
	movq	%r15, %r10
	movq	%rdi, %r13
	subq	%r12, %r10
	testq	%rax, %rax
	je	.L3361
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L3377
.L3348:
	movq	%r14, %rdi
	movq	%rcx, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r10
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rcx
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L3360:
	movq	(%rcx), %rcx
	movq	(%rdx), %rsi
	addq	%rbx, %r10
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L3350
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
.L3351:
	movq	%rax, (%r10)
	cmpq	%r12, %r15
	je	.L3353
	leaq	-8(%r15), %rcx
	leaq	15(%rbx), %rax
	subq	%r12, %rcx
	subq	%r12, %rax
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rax
	jbe	.L3363
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L3363
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L3355:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L3355
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%rdi, %rsi
	je	.L3357
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L3357:
	leaq	16(%rbx,%rcx), %r14
.L3353:
	cmpq	%r8, %r15
	je	.L3358
	subq	%r15, %r8
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r8, %rdx
	movq	%r8, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	addq	%r8, %r14
.L3358:
	testq	%r12, %r12
	je	.L3359
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3359:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3377:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L3349
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L3360
	.p2align 4,,10
	.p2align 3
.L3350:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L3378
.L3352:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L3351
	.p2align 4,,10
	.p2align 3
.L3361:
	movl	$8, %r14d
	jmp	.L3348
	.p2align 4,,10
	.p2align 3
.L3363:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L3354:
	movq	(%rax), %rsi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %r15
	jne	.L3354
	jmp	.L3357
.L3378:
	movq	%rcx, %rdi
	movq	%r8, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rcx
	jmp	.L3352
.L3349:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r14
	salq	$3, %r14
	jmp	.L3348
.L3376:
	leaq	.LC87(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE31184:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_14FeedbackVectorEEESaIS4_EE17_M_realloc_insertIJRS3_PNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal6HandleINS1_14FeedbackVectorEEESaIS4_EE17_M_realloc_insertIJRS3_PNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal7Isolate33MaybeInitializeVectorListFromHeapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate33MaybeInitializeVectorListFromHeapEv
	.type	_ZN2v88internal7Isolate33MaybeInitializeVectorListFromHeapEv, @function
_ZN2v88internal7Isolate33MaybeInitializeVectorListFromHeapEv:
.LFB25369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	4720(%rdi), %rax
	cmpq	%rax, 88(%rdi)
	je	.L3413
.L3379:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3414
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3413:
	.cfi_restore_state
	leaq	-80(%rbp), %r12
	leaq	37592(%rdi), %rsi
	xorl	%edx, %edx
	movq	%rdi, %rbx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, -96(%rbp)
	leaq	-120(%rbp), %r13
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	jne	.L3381
	jmp	.L3397
	.p2align 4,,10
	.p2align 3
.L3396:
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L3397
.L3381:
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	jne	.L3396
	movq	%rax, -128(%rbp)
	movq	7(%rax), %r14
	movq	31(%r14), %rax
	testb	$1, %al
	jne	.L3415
.L3385:
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L3396
	movq	7(%r14), %rax
	testb	$1, %al
	jne	.L3388
.L3391:
	movq	%rbx, -120(%rbp)
	movq	-104(%rbp), %r14
	cmpq	-96(%rbp), %r14
	je	.L3416
	movq	41112(%rbx), %rdi
	movq	-128(%rbp), %rsi
	testq	%rdi, %rdi
	je	.L3392
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3393:
	movq	%rax, (%r14)
	movq	%r12, %rdi
	addq	$8, -104(%rbp)
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	jne	.L3381
	.p2align 4,,10
	.p2align 3
.L3397:
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-104(%rbp), %rsi
	subq	-112(%rbp), %rsi
	movq	%rbx, %rdi
	sarq	$3, %rsi
	call	_ZN2v88internal9ArrayList3NewEPNS0_7IsolateEi@PLT
	movq	-112(%rbp), %rdi
	movq	-104(%rbp), %r13
	movq	%rax, %rsi
	movq	%rdi, %r12
	cmpq	%rdi, %r13
	je	.L3383
	.p2align 4,,10
	.p2align 3
.L3398:
	movq	(%r12), %rdx
	movq	%rbx, %rdi
	addq	$8, %r12
	call	_ZN2v88internal9ArrayList3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %rsi
	cmpq	%r12, %r13
	jne	.L3398
	movq	-112(%rbp), %rdi
.L3383:
	movq	(%rsi), %rax
	movq	%rax, 4720(%rbx)
	testq	%rdi, %rdi
	je	.L3379
	call	_ZdlPv@PLT
	jmp	.L3379
	.p2align 4,,10
	.p2align 3
.L3415:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L3417
.L3386:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	je	.L3396
	jmp	.L3385
	.p2align 4,,10
	.p2align 3
.L3392:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L3418
.L3394:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L3393
	.p2align 4,,10
	.p2align 3
.L3417:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L3386
	jmp	.L3385
	.p2align 4,,10
	.p2align 3
.L3416:
	leaq	-128(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rsi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_14FeedbackVectorEEESaIS4_EE17_M_realloc_insertIJRS3_PNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L3396
	.p2align 4,,10
	.p2align 3
.L3388:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L3391
	jmp	.L3396
.L3418:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L3394
.L3414:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25369:
	.size	_ZN2v88internal7Isolate33MaybeInitializeVectorListFromHeapEv, .-_ZN2v88internal7Isolate33MaybeInitializeVectorListFromHeapEv
	.section	.text._ZNSt6vectorIPFvPN2v87IsolateEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPFvPN2v87IsolateEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPFvPN2v87IsolateEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPFvPN2v87IsolateEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPFvPN2v87IsolateEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB31209:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L3433
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L3429
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L3434
.L3421:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L3428:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L3435
	testq	%r13, %r13
	jg	.L3424
	testq	%r9, %r9
	jne	.L3427
.L3425:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3435:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L3424
.L3427:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L3425
	.p2align 4,,10
	.p2align 3
.L3434:
	testq	%rsi, %rsi
	jne	.L3422
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L3428
	.p2align 4,,10
	.p2align 3
.L3424:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L3425
	jmp	.L3427
	.p2align 4,,10
	.p2align 3
.L3429:
	movl	$8, %r14d
	jmp	.L3421
.L3433:
	leaq	.LC87(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3422:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L3421
	.cfi_endproc
.LFE31209:
	.size	_ZNSt6vectorIPFvPN2v87IsolateEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPFvPN2v87IsolateEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal7Isolate28AddBeforeCallEnteredCallbackEPFvPNS_7IsolateEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate28AddBeforeCallEnteredCallbackEPFvPNS_7IsolateEE
	.type	_ZN2v88internal7Isolate28AddBeforeCallEnteredCallbackEPFvPNS_7IsolateEE, @function
_ZN2v88internal7Isolate28AddBeforeCallEnteredCallbackEPFvPNS_7IsolateEE:
.LFB25412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	45448(%rdi), %rcx
	movq	%rsi, -8(%rbp)
	movq	45440(%rdi), %rsi
	movq	%rcx, %rax
	subq	%rsi, %rax
	movq	%rax, %rdx
	sarq	$5, %rax
	sarq	$3, %rdx
	testq	%rax, %rax
	jle	.L3437
	salq	$5, %rax
	movq	-8(%rbp), %rdx
	addq	%rsi, %rax
	jmp	.L3442
	.p2align 4,,10
	.p2align 3
.L3458:
	cmpq	8(%rsi), %rdx
	je	.L3454
	cmpq	16(%rsi), %rdx
	je	.L3455
	cmpq	24(%rsi), %rdx
	je	.L3456
	addq	$32, %rsi
	cmpq	%rax, %rsi
	je	.L3457
.L3442:
	cmpq	%rdx, (%rsi)
	jne	.L3458
.L3438:
	cmpq	%rsi, %rcx
	je	.L3449
.L3436:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3457:
	.cfi_restore_state
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
.L3437:
	cmpq	$2, %rdx
	je	.L3443
	cmpq	$3, %rdx
	je	.L3444
	cmpq	$1, %rdx
	jne	.L3446
	movq	-8(%rbp), %rax
.L3448:
	cmpq	%rax, (%rsi)
	je	.L3438
.L3446:
	movq	%rcx, %rsi
.L3449:
	cmpq	%rsi, 45456(%rdi)
	je	.L3451
	movq	-8(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 45448(%rdi)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3444:
	.cfi_restore_state
	movq	-8(%rbp), %rax
	cmpq	%rax, (%rsi)
	je	.L3438
	addq	$8, %rsi
	jmp	.L3447
	.p2align 4,,10
	.p2align 3
.L3443:
	movq	-8(%rbp), %rax
.L3447:
	cmpq	%rax, (%rsi)
	je	.L3438
	addq	$8, %rsi
	jmp	.L3448
	.p2align 4,,10
	.p2align 3
.L3454:
	addq	$8, %rsi
	cmpq	%rsi, %rcx
	jne	.L3436
	jmp	.L3449
	.p2align 4,,10
	.p2align 3
.L3455:
	addq	$16, %rsi
	cmpq	%rsi, %rcx
	jne	.L3436
	jmp	.L3449
	.p2align 4,,10
	.p2align 3
.L3456:
	addq	$24, %rsi
	cmpq	%rsi, %rcx
	jne	.L3436
	jmp	.L3449
	.p2align 4,,10
	.p2align 3
.L3451:
	leaq	-8(%rbp), %rdx
	addq	$45440, %rdi
	call	_ZNSt6vectorIPFvPN2v87IsolateEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25412:
	.size	_ZN2v88internal7Isolate28AddBeforeCallEnteredCallbackEPFvPNS_7IsolateEE, .-_ZN2v88internal7Isolate28AddBeforeCallEnteredCallbackEPFvPNS_7IsolateEE
	.section	.text._ZN2v88internal7Isolate24AddCallCompletedCallbackEPFvPNS_7IsolateEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate24AddCallCompletedCallbackEPFvPNS_7IsolateEE
	.type	_ZN2v88internal7Isolate24AddCallCompletedCallbackEPFvPNS_7IsolateEE, @function
_ZN2v88internal7Isolate24AddCallCompletedCallbackEPFvPNS_7IsolateEE:
.LFB25414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	45472(%rdi), %rcx
	movq	%rsi, -8(%rbp)
	movq	45464(%rdi), %rsi
	movq	%rcx, %rax
	subq	%rsi, %rax
	movq	%rax, %rdx
	sarq	$5, %rax
	sarq	$3, %rdx
	testq	%rax, %rax
	jle	.L3460
	salq	$5, %rax
	movq	-8(%rbp), %rdx
	addq	%rsi, %rax
	jmp	.L3465
	.p2align 4,,10
	.p2align 3
.L3481:
	cmpq	8(%rsi), %rdx
	je	.L3477
	cmpq	16(%rsi), %rdx
	je	.L3478
	cmpq	24(%rsi), %rdx
	je	.L3479
	addq	$32, %rsi
	cmpq	%rax, %rsi
	je	.L3480
.L3465:
	cmpq	%rdx, (%rsi)
	jne	.L3481
.L3461:
	cmpq	%rsi, %rcx
	je	.L3472
.L3459:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3480:
	.cfi_restore_state
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
.L3460:
	cmpq	$2, %rdx
	je	.L3466
	cmpq	$3, %rdx
	je	.L3467
	cmpq	$1, %rdx
	jne	.L3469
	movq	-8(%rbp), %rax
.L3471:
	cmpq	%rax, (%rsi)
	je	.L3461
.L3469:
	movq	%rcx, %rsi
.L3472:
	cmpq	%rsi, 45480(%rdi)
	je	.L3474
	movq	-8(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 45472(%rdi)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3467:
	.cfi_restore_state
	movq	-8(%rbp), %rax
	cmpq	%rax, (%rsi)
	je	.L3461
	addq	$8, %rsi
	jmp	.L3470
	.p2align 4,,10
	.p2align 3
.L3466:
	movq	-8(%rbp), %rax
.L3470:
	cmpq	%rax, (%rsi)
	je	.L3461
	addq	$8, %rsi
	jmp	.L3471
	.p2align 4,,10
	.p2align 3
.L3477:
	addq	$8, %rsi
	cmpq	%rsi, %rcx
	jne	.L3459
	jmp	.L3472
	.p2align 4,,10
	.p2align 3
.L3478:
	addq	$16, %rsi
	cmpq	%rsi, %rcx
	jne	.L3459
	jmp	.L3472
	.p2align 4,,10
	.p2align 3
.L3479:
	addq	$24, %rsi
	cmpq	%rsi, %rcx
	jne	.L3459
	jmp	.L3472
	.p2align 4,,10
	.p2align 3
.L3474:
	leaq	-8(%rbp), %rdx
	addq	$45464, %rdi
	call	_ZNSt6vectorIPFvPN2v87IsolateEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25414:
	.size	_ZN2v88internal7Isolate24AddCallCompletedCallbackEPFvPNS_7IsolateEE, .-_ZN2v88internal7Isolate24AddCallCompletedCallbackEPFvPNS_7IsolateEE
	.section	.text._ZNSt8__detail9_Map_baseIN2v88internal7Isolate18ICUObjectCacheTypeESt4pairIKS4_St10shared_ptrIN6icu_677UMemoryEEESaISB_ENS_10_Select1stESt8equal_toIS4_ENS3_22ICUObjectCacheTypeHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN2v88internal7Isolate18ICUObjectCacheTypeESt4pairIKS4_St10shared_ptrIN6icu_677UMemoryEEESaISB_ENS_10_Select1stESt8equal_toIS4_ENS3_22ICUObjectCacheTypeHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN2v88internal7Isolate18ICUObjectCacheTypeESt4pairIKS4_St10shared_ptrIN6icu_677UMemoryEEESaISB_ENS_10_Select1stESt8equal_toIS4_ENS3_22ICUObjectCacheTypeHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	.type	_ZNSt8__detail9_Map_baseIN2v88internal7Isolate18ICUObjectCacheTypeESt4pairIKS4_St10shared_ptrIN6icu_677UMemoryEEESaISB_ENS_10_Select1stESt8equal_toIS4_ENS3_22ICUObjectCacheTypeHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_, @function
_ZNSt8__detail9_Map_baseIN2v88internal7Isolate18ICUObjectCacheTypeESt4pairIKS4_St10shared_ptrIN6icu_677UMemoryEEESaISB_ENS_10_Select1stESt8equal_toIS4_ENS3_22ICUObjectCacheTypeHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_:
.LFB31256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movslq	(%rsi), %r13
	movq	8(%rdi), %rdi
	movq	%r13, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L3483
	movq	(%rax), %rcx
	movq	%r13, %r9
	movq	%rdx, %rsi
	movq	32(%rcx), %r8
	jmp	.L3486
	.p2align 4,,10
	.p2align 3
.L3484:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3483
	movq	32(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %rsi
	jne	.L3483
.L3486:
	cmpq	%r8, %r13
	jne	.L3484
	cmpl	8(%rcx), %r9d
	jne	.L3484
	addq	$24, %rsp
	leaq	16(%rcx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3483:
	.cfi_restore_state
	movl	$40, %edi
	call	_Znwm@PLT
	movq	24(%r12), %rdx
	movq	8(%r12), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, (%rax)
	movq	%rax, %rbx
	movl	(%r14), %eax
	leaq	32(%r12), %rdi
	movl	$1, %ecx
	movups	%xmm0, 16(%rbx)
	movl	%eax, 8(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L3487
	movq	(%r12), %r8
	movq	%r13, 32(%rbx)
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L3497
.L3521:
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	(%rax), %rax
	movq	%rbx, (%rax)
.L3498:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	leaq	16(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3487:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L3519
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L3520
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L3490:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L3492
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L3493
	.p2align 4,,10
	.p2align 3
.L3494:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L3495:
	testq	%rsi, %rsi
	je	.L3492
.L3493:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L3494
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L3501
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L3493
	.p2align 4,,10
	.p2align 3
.L3492:
	movq	(%r12), %rdi
	cmpq	%r10, %rdi
	je	.L3496
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L3496:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r14, 8(%r12)
	divq	%r14
	movq	%r8, (%r12)
	movq	%r13, 32(%rbx)
	leaq	0(,%rdx,8), %r15
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L3521
.L3497:
	movq	16(%r12), %rdx
	movq	%rbx, 16(%r12)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L3499
	movq	32(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
	movq	(%r12), %rax
	addq	%r15, %rax
.L3499:
	leaq	16(%r12), %rdx
	movq	%rdx, (%rax)
	jmp	.L3498
	.p2align 4,,10
	.p2align 3
.L3501:
	movq	%rdx, %rdi
	jmp	.L3495
	.p2align 4,,10
	.p2align 3
.L3519:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L3490
.L3520:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE31256:
	.size	_ZNSt8__detail9_Map_baseIN2v88internal7Isolate18ICUObjectCacheTypeESt4pairIKS4_St10shared_ptrIN6icu_677UMemoryEEESaISB_ENS_10_Select1stESt8equal_toIS4_ENS3_22ICUObjectCacheTypeHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_, .-_ZNSt8__detail9_Map_baseIN2v88internal7Isolate18ICUObjectCacheTypeESt4pairIKS4_St10shared_ptrIN6icu_677UMemoryEEESaISB_ENS_10_Select1stESt8equal_toIS4_ENS3_22ICUObjectCacheTypeHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	.section	.text._ZN2v88internal7Isolate21get_cached_icu_objectENS1_18ICUObjectCacheTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate21get_cached_icu_objectENS1_18ICUObjectCacheTypeE
	.type	_ZN2v88internal7Isolate21get_cached_icu_objectENS1_18ICUObjectCacheTypeE, @function
_ZN2v88internal7Isolate21get_cached_icu_objectENS1_18ICUObjectCacheTypeE:
.LFB25466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$41400, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%esi, -4(%rbp)
	leaq	-4(%rbp), %rsi
	call	_ZNSt8__detail9_Map_baseIN2v88internal7Isolate18ICUObjectCacheTypeESt4pairIKS4_St10shared_ptrIN6icu_677UMemoryEEESaISB_ENS_10_Select1stESt8equal_toIS4_ENS3_22ICUObjectCacheTypeHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	movq	(%rax), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25466:
	.size	_ZN2v88internal7Isolate21get_cached_icu_objectENS1_18ICUObjectCacheTypeE, .-_ZN2v88internal7Isolate21get_cached_icu_objectENS1_18ICUObjectCacheTypeE
	.section	.text._ZN2v88internal7Isolate23set_icu_object_in_cacheENS1_18ICUObjectCacheTypeESt10shared_ptrIN6icu_677UMemoryEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate23set_icu_object_in_cacheENS1_18ICUObjectCacheTypeESt10shared_ptrIN6icu_677UMemoryEE
	.type	_ZN2v88internal7Isolate23set_icu_object_in_cacheENS1_18ICUObjectCacheTypeESt10shared_ptrIN6icu_677UMemoryEE, @function
_ZN2v88internal7Isolate23set_icu_object_in_cacheENS1_18ICUObjectCacheTypeESt10shared_ptrIN6icu_677UMemoryEE:
.LFB25467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$41400, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	%esi, -36(%rbp)
	leaq	-36(%rbp), %rsi
	call	_ZNSt8__detail9_Map_baseIN2v88internal7Isolate18ICUObjectCacheTypeESt4pairIKS4_St10shared_ptrIN6icu_677UMemoryEEESaISB_ENS_10_Select1stESt8equal_toIS4_ENS3_22ICUObjectCacheTypeHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	movq	8(%r12), %r13
	movq	%rax, %rbx
	movq	(%r12), %rax
	movq	8(%rbx), %r12
	movq	%rax, (%rbx)
	cmpq	%r12, %r13
	je	.L3524
	testq	%r13, %r13
	je	.L3526
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L3527
	lock addl	$1, 8(%r13)
	movq	8(%rbx), %r12
.L3526:
	testq	%r12, %r12
	je	.L3529
.L3540:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L3530
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L3531:
	cmpl	$1, %eax
	je	.L3539
.L3529:
	movq	%r13, 8(%rbx)
.L3524:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3527:
	.cfi_restore_state
	addl	$1, 8(%r13)
	movq	8(%rbx), %r12
	testq	%r12, %r12
	jne	.L3540
	jmp	.L3529
	.p2align 4,,10
	.p2align 3
.L3539:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L3533
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L3534:
	cmpl	$1, %eax
	jne	.L3529
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L3529
	.p2align 4,,10
	.p2align 3
.L3530:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L3531
	.p2align 4,,10
	.p2align 3
.L3533:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L3534
	.cfi_endproc
.LFE25467:
	.size	_ZN2v88internal7Isolate23set_icu_object_in_cacheENS1_18ICUObjectCacheTypeESt10shared_ptrIN6icu_677UMemoryEE, .-_ZN2v88internal7Isolate23set_icu_object_in_cacheENS1_18ICUObjectCacheTypeESt10shared_ptrIN6icu_677UMemoryEE
	.section	.text._ZN2v88internal20SetupIsolateDelegateD2Ev,"axG",@progbits,_ZN2v88internal20SetupIsolateDelegateD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20SetupIsolateDelegateD2Ev
	.type	_ZN2v88internal20SetupIsolateDelegateD2Ev, @function
_ZN2v88internal20SetupIsolateDelegateD2Ev:
.LFB34970:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE34970:
	.size	_ZN2v88internal20SetupIsolateDelegateD2Ev, .-_ZN2v88internal20SetupIsolateDelegateD2Ev
	.weak	_ZN2v88internal20SetupIsolateDelegateD1Ev
	.set	_ZN2v88internal20SetupIsolateDelegateD1Ev,_ZN2v88internal20SetupIsolateDelegateD2Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19DefaultEmbeddedBlobEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19DefaultEmbeddedBlobEv, @function
_GLOBAL__sub_I__ZN2v88internal19DefaultEmbeddedBlobEv:
.LFB35027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE35027:
	.size	_GLOBAL__sub_I__ZN2v88internal19DefaultEmbeddedBlobEv, .-_GLOBAL__sub_I__ZN2v88internal19DefaultEmbeddedBlobEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19DefaultEmbeddedBlobEv
	.section	.rodata._ZN2v88internal7Isolate6DeinitEv.str1.1,"aMS",@progbits,1
.LC88:
	.string	"=== Stress deopt counter: %u\n"
	.section	.text._ZN2v88internal7Isolate6DeinitEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate6DeinitEv
	.type	_ZN2v88internal7Isolate6DeinitEv, @function
_ZN2v88internal7Isolate6DeinitEv:
.LFB25281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	45768(%rdi), %rdi
	movq	$0, 45768(%rbx)
	testq	%rdi, %rdi
	je	.L3545
	call	_ZN2v88internal22TracingCpuProfilerImplD0Ev@PLT
.L3545:
	movl	_ZN2v88internal40FLAG_stress_sampling_allocation_profilerE(%rip), %eax
	testl	%eax, %eax
	jle	.L3546
	movq	41480(%rbx), %rdi
	call	_ZN2v88internal12HeapProfiler24StopSamplingHeapProfilerEv@PLT
.L3546:
	movq	41472(%rbx), %rdi
	call	_ZN2v88internal5Debug6UnloadEv@PLT
	movq	45752(%rbx), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal4wasm10WasmEngine26DeleteCompileJobsOnIsolateEPNS0_7IsolateE@PLT
	movq	45416(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3547
	call	_ZN2v88internal27OptimizingCompileDispatcher4StopEv@PLT
	movq	45416(%rbx), %r12
	testq	%r12, %r12
	je	.L3548
	movq	%r12, %rdi
	call	_ZN2v88internal27OptimizingCompileDispatcherD1Ev@PLT
	movl	$304, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3548:
	movq	$0, 45416(%rbx)
.L3547:
	movq	45752(%rbx), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal4wasm17WasmMemoryTracker34DeleteSharedMemoryObjectsOnIsolateEPNS0_7IsolateE@PLT
	movq	39608(%rbx), %rdi
	call	_ZN2v88internal20MarkCompactCollector23EnsureSweepingCompletedEv@PLT
	movq	39640(%rbx), %rax
	leaq	136(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator8Unmapper24EnsureUnmappingCompletedEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate17DumpAndResetStatsEv
	cmpb	$0, _ZN2v88internal23FLAG_print_deopt_stressE(%rip)
	jne	.L3654
.L3549:
	movq	41016(%rbx), %rdi
	call	_ZN2v88internal6Logger7samplerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3551
	movzbl	32(%rax), %eax
	testb	%al, %al
	jne	.L3655
.L3551:
	leaq	12448(%rbx), %rdi
	leaq	37592(%rbx), %r13
	call	_ZN2v88internal14ThreadLocalTop4FreeEv@PLT
	movq	41016(%rbx), %rdi
	leaq	45688(%rbx), %r14
	call	_ZN2v88internal6Logger18StopProfilerThreadEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal4Heap13StartTearDownEv@PLT
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	45728(%rbx), %r12
	testq	%r12, %r12
	je	.L3553
	.p2align 4,,10
	.p2align 3
.L3554:
	movq	$0, 45728(%rbx)
	.p2align 4,,10
	.p2align 3
.L3556:
	movq	24(%r12), %rdi
	call	*32(%r12)
	movq	%r12, %rdi
	movq	16(%r12), %r12
	movl	$48, %esi
	call	_ZdlPvm@PLT
	testq	%r12, %r12
	jne	.L3556
	movq	45728(%rbx), %r12
	testq	%r12, %r12
	jne	.L3554
.L3553:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	41040(%rbx), %r12
	testq	%r12, %r12
	je	.L3557
	movq	%r12, %rdi
	call	_ZN2v88internal15DeoptimizerDataD1Ev@PLT
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3557:
	movq	$0, 41040(%rbx)
	leaq	41184(%rbx), %rdi
	call	_ZN2v88internal8Builtins8TearDownEv@PLT
	movq	40936(%rbx), %rdi
	call	_ZN2v88internal12Bootstrapper8TearDownEv@PLT
	movq	40944(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3558
	movl	$16, %esi
	call	_ZdlPvm@PLT
	movq	$0, 40944(%rbx)
.L3558:
	movq	41480(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3559
	movq	(%rdi), %rax
	call	*32(%rax)
.L3559:
	movq	$0, 41480(%rbx)
	movq	41528(%rbx), %rdi
	call	_ZN2v88internal18CompilerDispatcher8AbortAllEv@PLT
	movq	41528(%rbx), %r12
	testq	%r12, %r12
	je	.L3560
	movq	%r12, %rdi
	call	_ZN2v88internal18CompilerDispatcherD1Ev@PLT
	movl	$456, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3560:
	movq	$0, 41528(%rbx)
	movq	45640(%rbx), %rdi
	call	_ZN2v88internal21CancelableTaskManager13CancelAndWaitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal4Heap8TearDownEv@PLT
	movq	41016(%rbx), %rdi
	call	_ZN2v88internal6Logger8TearDownEv@PLT
	movq	45752(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3562
	movq	%rbx, %rsi
	call	_ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE@PLT
	movq	45760(%rbx), %r12
	pxor	%xmm0, %xmm0
	movups	%xmm0, 45752(%rbx)
	testq	%r12, %r12
	je	.L3562
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L3564
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L3565:
	cmpl	$1, %eax
	jne	.L3562
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L3566
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L3567:
	cmpl	$1, %eax
	je	.L3656
	.p2align 4,,10
	.p2align 3
.L3562:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate20TearDownEmbeddedBlobEv
	movq	41504(%rbx), %r12
	testq	%r12, %r12
	je	.L3568
	movq	(%r12), %rax
	leaq	_ZN2v88internal11interpreter11InterpreterD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3569
	movq	6160(%r12), %rdi
	leaq	16+_ZTVN2v88internal11interpreter11InterpreterE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3570
	call	_ZdaPv@PLT
.L3570:
	movl	$6176, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3568:
	movq	41496(%rbx), %r12
	movq	$0, 41504(%rbx)
	testq	%r12, %r12
	je	.L3571
	movq	64(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movl	$512, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3571:
	movq	41488(%rbx), %r12
	pxor	%xmm0, %xmm0
	movups	%xmm0, 41488(%rbx)
	testq	%r12, %r12
	je	.L3572
	leaq	56(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L3573
	.p2align 4,,10
	.p2align 3
.L3574:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3574
.L3573:
	movq	8(%r12), %rax
	movq	(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 24(%r12)
	movq	$0, 16(%r12)
	cmpq	%rax, %rdi
	je	.L3575
	call	_ZdlPv@PLT
.L3575:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3572:
	movq	41744(%rbx), %r12
	testq	%r12, %r12
	je	.L3576
	movq	(%r12), %rdi
	call	free@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3576:
	movq	41520(%rbx), %r12
	movq	$0, 41744(%rbx)
	testq	%r12, %r12
	je	.L3577
	movq	%r12, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3577:
	movq	41736(%rbx), %r12
	pxor	%xmm0, %xmm0
	movups	%xmm0, 41512(%rbx)
	testq	%r12, %r12
	je	.L3578
	movq	(%r12), %rdi
	call	free@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3578:
	movq	$0, 41736(%rbx)
	leaq	45800(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	45856(%rbx), %r12
	testq	%r12, %r12
	je	.L3579
	.p2align 4,,10
	.p2align 3
.L3583:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3580
	movl	$32, %esi
	call	_ZdlPvm@PLT
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L3583
.L3582:
	movq	45856(%rbx), %r12
	testq	%r12, %r12
	je	.L3579
	.p2align 4,,10
	.p2align 3
.L3584:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L3584
.L3579:
	movq	45848(%rbx), %rax
	movq	45840(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	%r13, %rdi
	movq	$0, 45864(%rbx)
	movq	$0, 45856(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L3580:
	.cfi_restore_state
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L3583
	jmp	.L3582
	.p2align 4,,10
	.p2align 3
.L3654:
	movl	45424(%rbx), %edx
	movq	stdout(%rip), %rdi
	leaq	.LC88(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L3549
	.p2align 4,,10
	.p2align 3
.L3655:
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	jmp	.L3551
	.p2align 4,,10
	.p2align 3
.L3564:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L3565
	.p2align 4,,10
	.p2align 3
.L3569:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3568
	.p2align 4,,10
	.p2align 3
.L3656:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L3562
	.p2align 4,,10
	.p2align 3
.L3566:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L3567
	.cfi_endproc
.LFE25281:
	.size	_ZN2v88internal7Isolate6DeinitEv, .-_ZN2v88internal7Isolate6DeinitEv
	.section	.rodata._ZN2v88internal7Isolate4InitEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE.str1.1,"aMS",@progbits,1
.LC90:
	.string	"heap object creation"
	.section	.rodata._ZN2v88internal7Isolate4InitEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE.str1.8,"aMS",@progbits,1
	.align 8
.LC91:
	.string	"Concurrent recompilation has been disabled for tracing.\n"
	.section	.rodata._ZN2v88internal7Isolate4InitEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE.str1.1
.LC92:
	.string	"NewArray"
	.section	.rodata._ZN2v88internal7Isolate4InitEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE.str1.8
	.align 8
.LC93:
	.string	"[Initializing isolate from scratch took %0.3f ms]\n"
	.section	.text._ZN2v88internal7Isolate4InitEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate4InitEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE
	.type	_ZN2v88internal7Isolate4InitEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE, @function
_ZN2v88internal7Isolate4InitEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE:
.LFB25317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$616, %rsp
	movq	%rdx, -640(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	movq	$0, -656(%rbp)
	sete	-648(%rbp)
	je	.L3743
.L3658:
	leaq	37592(%r13), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movl	_ZN2v88internal24FLAG_deopt_every_n_timesE(%rip), %eax
	movb	$0, 41457(%r13)
	movsd	%xmm0, 41464(%r13)
	movl	%eax, 45424(%r13)
	movzbl	_ZN2v88internal20FLAG_force_slow_pathE(%rip), %eax
	movb	%al, 45428(%r13)
	leaq	37792(%r13), %rax
	movq	%rax, -632(%rbp)
	lock addq	$1, (%rax)
	movl	$144, %edi
	leaq	12568(%r13), %rax
	movq	%rax, %xmm0
	leaq	12560(%r13), %rax
	movq	%rax, %xmm2
	leaq	12576(%r13), %rax
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 40832(%r13)
	movq	%rax, %xmm0
	leaq	12464(%r13), %rax
	movq	%rax, %xmm3
	leaq	12480(%r13), %rax
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 40848(%r13)
	movq	%rax, %xmm0
	leaq	12488(%r13), %rax
	movq	%rax, %xmm4
	leaq	12496(%r13), %rax
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 40864(%r13)
	movq	%rax, %xmm0
	leaq	12504(%r13), %rax
	movq	%rax, %xmm5
	leaq	12512(%r13), %rax
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 40880(%r13)
	movq	%rax, %xmm0
	leaq	12520(%r13), %rax
	movq	%rax, %xmm6
	leaq	12545(%r13), %rax
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 40896(%r13)
	movq	%rax, %xmm0
	leaq	12600(%r13), %rax
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 40912(%r13)
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN2v88internal16CompilationCacheC1EPNS0_7IsolateE@PLT
	movq	%r14, 40952(%r13)
	movl	$1280, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$40968, %edi
	movdqa	.LC89(%rip), %xmm1
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm1, 1024(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm1, 1040(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm1, 1056(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm1, 1072(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm1, 1088(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm1, 1104(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm1, 1120(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm1, 1136(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm1, 1152(%rax)
	movups	%xmm0, 576(%rax)
	movups	%xmm0, 592(%rax)
	movups	%xmm0, 608(%rax)
	movups	%xmm0, 624(%rax)
	movups	%xmm1, 1168(%rax)
	movups	%xmm0, 640(%rax)
	movups	%xmm0, 656(%rax)
	movups	%xmm0, 672(%rax)
	movups	%xmm0, 688(%rax)
	movups	%xmm1, 1184(%rax)
	movups	%xmm0, 704(%rax)
	movups	%xmm0, 720(%rax)
	movups	%xmm0, 736(%rax)
	movups	%xmm0, 752(%rax)
	movups	%xmm1, 1200(%rax)
	movups	%xmm0, 768(%rax)
	movups	%xmm0, 784(%rax)
	movups	%xmm0, 800(%rax)
	movups	%xmm0, 816(%rax)
	movups	%xmm1, 1216(%rax)
	movq	%rax, 41080(%r13)
	movups	%xmm0, 832(%rax)
	movups	%xmm0, 848(%rax)
	movups	%xmm0, 864(%rax)
	movups	%xmm0, 880(%rax)
	movups	%xmm1, 1232(%rax)
	movups	%xmm0, 896(%rax)
	movups	%xmm0, 912(%rax)
	movups	%xmm0, 928(%rax)
	movups	%xmm0, 944(%rax)
	movups	%xmm1, 1248(%rax)
	movups	%xmm0, 960(%rax)
	movups	%xmm0, 976(%rax)
	movups	%xmm0, 992(%rax)
	movups	%xmm0, 1008(%rax)
	movups	%xmm1, 1264(%rax)
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	8(%rax), %rdi
	movq	%r13, (%rax)
	leaq	40968(%r14), %rdx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L3659:
	movq	$0, 8(%rax)
	addq	$40, %rax
	movl	$0, -24(%rax)
	movq	$0, -16(%rax)
	movl	$-1, -8(%rax)
	cmpq	%rax, %rdx
	jne	.L3659
	movl	$40960, %edx
	xorl	%esi, %esi
	call	memset@PLT
	movq	%r14, 41144(%r13)
	movl	$168, %edi
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN2v88internal13GlobalHandlesC1EPNS0_7IsolateE@PLT
	movq	%r14, 41152(%r13)
	movl	$56, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	%rax, 41160(%r13)
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 24(%rax)
	movups	%xmm0, 40(%rax)
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN2v88internal12BootstrapperC1EPNS0_7IsolateE@PLT
	movq	%r14, 40936(%r13)
	movl	$152, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$61448, %edi
	movq	%r13, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 32(%rax)
	movq	$0, 56(%rax)
	movq	$0, 80(%rax)
	movq	%rax, 41120(%r13)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 40(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 88(%rax)
	movups	%xmm0, 104(%rax)
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN2v88internal9StubCacheC1EPNS0_7IsolateE@PLT
	movq	%r14, 41024(%r13)
	movl	$61448, %edi
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN2v88internal9StubCacheC1EPNS0_7IsolateE@PLT
	movq	%r14, 41032(%r13)
	movl	$32, %edi
	call	_Znwm@PLT
	movl	$40, %edi
	movq	%r13, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rax, 41056(%r13)
	call	_Znwm@PLT
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN2v88internal11RegExpStackC1Ev@PLT
	movq	%r13, 32(%r14)
	movl	$600, %edi
	movq	%r14, 41208(%r13)
	call	_Znwm@PLT
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN2v88internal9DateCacheC1Ev@PLT
	movq	%r14, 41240(%r13)
	movl	$136, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN2v88internal12HeapProfilerC1EPNS0_4HeapE@PLT
	movq	%r14, 41480(%r13)
	movl	$6176, %edi
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter11InterpreterC1EPNS0_7IsolateE@PLT
	movq	%r14, 41504(%r13)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movl	$456, %edi
	movq	%rax, %r15
	call	_Znwm@PLT
	movslq	_ZN2v88internal15FLAG_stack_sizeE(%rip), %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN2v88internal18CompilerDispatcherC1EPNS0_7IsolateEPNS_8PlatformEm@PLT
	movq	%r14, 41528(%r13)
	movq	41016(%r13), %rdi
	movq	%r13, %rsi
	leaq	-624(%rbp), %r14
	call	_ZN2v88internal6Logger5SetUpEPNS0_7IsolateE@PLT
	leaq	40976(%r13), %rdi
	movq	%r13, -624(%rbp)
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movq	%r14, %rsi
	leaq	37512(%r13), %rdi
	call	_ZN2v88internal10StackGuard10InitThreadERKNS0_15ExecutionAccessE@PLT
	movq	-624(%rbp), %rax
	leaq	40976(%rax), %rdi
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap5SetUpEv@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12ReadOnlyHeap5SetUpEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap11SetUpSpacesEv@PLT
	leaq	4856(%r13), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal22ExternalReferenceTable4InitEPNS0_7IsolateE@PLT
	cmpq	$0, 45752(%r13)
	je	.L3744
.L3660:
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal15DeoptimizerDataC1EPNS0_4HeapE@PLT
	movq	41200(%r13), %rdi
	movq	%r15, 41040(%r13)
	testq	%rdi, %rdi
	je	.L3745
.L3668:
	cmpb	$0, _ZN2v88internal15FLAG_inline_newE(%rip)
	je	.L3746
.L3669:
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*24(%rax)
	testb	%al, %al
	je	.L3747
	leaq	12448(%r13), %r15
	testq	%rbx, %rbx
	je	.L3748
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE@PLT
	movq	96(%r13), %rax
	movl	$72, %edi
	movq	%rax, 12480(%r13)
	movq	%rax, 12536(%r13)
	movq	%rax, 12552(%r13)
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal22TracingCpuProfilerImplC1EPNS0_7IsolateE@PLT
	movq	45768(%r13), %rdi
	movq	%r15, 45768(%r13)
	testq	%rdi, %rdi
	je	.L3742
	call	_ZN2v88internal22TracingCpuProfilerImplD0Ev@PLT
.L3742:
	movq	40936(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal12Bootstrapper10InitializeEb@PLT
	movq	41200(%r13), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
.L3704:
	call	_ZN2v88internal22init_memcopy_functionsEv@PLT
	cmpb	$0, _ZN2v88internal30FLAG_log_internal_timer_eventsE(%rip)
	jne	.L3749
.L3675:
	cmpb	$0, _ZN2v88internal16FLAG_trace_turboE(%rip)
	jne	.L3676
	cmpb	$0, _ZN2v88internal22FLAG_trace_turbo_graphE(%rip)
	je	.L3750
.L3676:
	leaq	.LC91(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L3678:
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal15RuntimeProfilerC1EPNS0_7IsolateE@PLT
	movq	-632(%rbp), %rax
	movq	%r15, 40944(%r13)
	lock addq	$1, (%rax)
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal32CodeSpaceMemoryModificationScopeC1EPNS0_4HeapE
	testq	%rbx, %rbx
	je	.L3751
	movq	-640(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal19StartupDeserializer15DeserializeIntoEPNS0_7IsolateE@PLT
.L3682:
	movq	41024(%r13), %rdi
	call	_ZN2v88internal9StubCache10InitializeEv@PLT
	movq	41032(%r13), %rdi
	call	_ZN2v88internal9StubCache10InitializeEv@PLT
	movq	41504(%r13), %rdi
	call	_ZN2v88internal11interpreter11Interpreter10InitializeEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap29NotifyDeserializationCompleteEv@PLT
	movq	-624(%rbp), %rax
	cmpb	$0, 376(%rax)
	jne	.L3752
.L3683:
	movq	-632(%rbp), %rax
	lock subq	$1, (%rax)
	movq	41200(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3693
	movq	(%rdi), %rax
	leaq	_ZN2v88internal20SetupIsolateDelegateD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3694
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L3693:
	movq	$0, 41200(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins23UpdateBuiltinEntryTableEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins20EmitCodeCreateEventsEPNS0_7IsolateE@PLT
	cmpb	$0, _ZN2v88internal23FLAG_print_builtin_codeE(%rip)
	jne	.L3753
	cmpb	$0, _ZN2v88internal23FLAG_print_builtin_sizeE(%rip)
	jne	.L3754
.L3696:
	movq	96(%r13), %rax
	cmpb	$0, _ZN2v88internal16FLAG_trace_turboE(%rip)
	movq	%rax, 12480(%r13)
	movq	%rax, 12536(%r13)
	movq	%rax, 12552(%r13)
	jne	.L3755
.L3697:
	addl	$1, 41104(%r13)
	movq	1176(%r13), %rax
	movl	$512, %edi
	movq	41096(%r13), %rcx
	movq	41088(%r13), %r15
	movq	15(%rax), %rdx
	movq	%rcx, -648(%rbp)
	movq	%rdx, -640(%rbp)
	call	_Znwm@PLT
	movq	-640(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v88internal18AstStringConstantsC1EPNS0_7IsolateEm@PLT
	subl	$1, 41104(%r13)
	movq	-648(%rbp), %rcx
	movq	%r12, 41496(%r13)
	movq	%r15, 41088(%r13)
	cmpq	41096(%r13), %rcx
	je	.L3701
	movq	%rcx, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3701:
	movl	_ZN2v88internal40FLAG_stress_sampling_allocation_profilerE(%rip), %eax
	testq	%rbx, %rbx
	setne	41458(%r13)
	testl	%eax, %eax
	jg	.L3756
	testq	%rbx, %rbx
	je	.L3757
.L3703:
	movq	-632(%rbp), %rax
	lock subq	$1, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3758
	addq	$616, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3749:
	.cfi_restore_state
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rax
	movq	%rax, 41632(%r13)
	jmp	.L3675
.L3750:
	cmpb	$0, _ZN2v88internal20FLAG_turbo_profilingE(%rip)
	jne	.L3676
	cmpb	$0, _ZN2v88internal29FLAG_concurrent_recompilationE(%rip)
	je	.L3678
	movl	$304, %edi
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%r13, (%rax)
	movl	_ZN2v88internal42FLAG_concurrent_recompilation_queue_lengthE(%rip), %eax
	movq	$0, 20(%r15)
	leaq	32(%r15), %rdi
	movl	%eax, 16(%r15)
	call	_ZN2v84base5MutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	$0, 72(%r15)
	movq	$8, 80(%r15)
	movups	%xmm0, 88(%r15)
	movups	%xmm0, 104(%r15)
	movups	%xmm0, 120(%r15)
	movups	%xmm0, 136(%r15)
	call	_Znwm@PLT
	movl	$512, %edi
	movq	%rax, 72(%r15)
	movq	%rax, %rdx
	movq	80(%r15), %rax
	leaq	-4(,%rax,4), %rax
	andq	$-8, %rax
	addq	%rax, %rdx
	movq	%rdx, -648(%rbp)
	call	_Znwm@PLT
	movq	-648(%rbp), %rdx
	leaq	152(%r15), %rdi
	leaq	512(%rax), %rcx
	movq	%rax, %xmm0
	movq	%rax, 128(%r15)
	movq	%rax, (%rdx)
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, 112(%r15)
	movq	%rdx, 144(%r15)
	movq	%rax, 120(%r15)
	movq	%rcx, 104(%r15)
	movq	%rcx, 136(%r15)
	movups	%xmm0, 88(%r15)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	208(%r15), %rdi
	movq	$0, 192(%r15)
	movl	$0, 200(%r15)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	248(%r15), %rdi
	call	_ZN2v84base17ConditionVariableC1Ev@PLT
	movl	_ZN2v88internal35FLAG_concurrent_recompilation_delayE(%rip), %eax
	movabsq	$1152921504606846975, %rdx
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	%eax, 296(%r15)
	movslq	16(%r15), %rax
	cmpq	%rdx, %rax
	leaq	0(,%rax,8), %r9
	movq	$-1, %rax
	cmova	%rax, %r9
	movq	%r9, %rdi
	movq	%r9, -648(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L3759
.L3680:
	movq	%rax, 8(%r15)
	movq	%r15, 45416(%r13)
	jmp	.L3678
.L3756:
	movq	41480(%r13), %rdi
	movslq	%eax, %rsi
	movl	$1, %ecx
	movl	$128, %edx
	call	_ZN2v88internal12HeapProfiler25StartSamplingHeapProfilerEmiNS_12HeapProfiler13SamplingFlagsE@PLT
	testq	%rbx, %rbx
	jne	.L3703
.L3757:
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	je	.L3703
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r14, %rdi
	subq	-656(%rbp), %rax
	movq	%rax, -624(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	leaq	.LC93(%rip), %rdi
	movl	$1, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3703
.L3752:
	subq	$1, 384(%rax)
	cmpb	$0, _ZN2v88internal12FLAG_jitlessE(%rip)
	movq	264(%rax), %rdi
	jne	.L3760
	call	_ZN2v88internal10PagedSpace20SetReadAndExecutableEv@PLT
.L3685:
	movq	-624(%rbp), %rax
	movq	288(%rax), %rdx
	movq	32(%rdx), %r12
	testq	%r12, %r12
	je	.L3683
	.p2align 4,,10
	.p2align 3
.L3692:
	movq	2048(%rax), %rcx
	xorl	%edx, %edx
	movq	%r12, %rax
	movq	352(%rcx), %rdi
	divq	%rdi
	movq	344(%rcx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L3686
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L3688
	.p2align 4,,10
	.p2align 3
.L3761:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3686
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L3686
.L3688:
	cmpq	%rsi, %r12
	jne	.L3761
	cmpb	$0, _ZN2v88internal12FLAG_jitlessE(%rip)
	je	.L3762
	movq	%r12, %rdi
	call	_ZN2v88internal11MemoryChunk11SetReadableEv@PLT
	movq	224(%r12), %r12
	testq	%r12, %r12
	je	.L3683
.L3691:
	movq	-624(%rbp), %rax
	jmp	.L3692
.L3746:
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap23DisableInlineAllocationEv@PLT
	movq	41200(%r13), %rdi
	jmp	.L3669
.L3743:
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	movq	%rsi, -656(%rbp)
	je	.L3658
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, -656(%rbp)
	jmp	.L3658
.L3751:
	movq	37896(%r13), %rdi
	call	_ZN2v88internal13ReadOnlySpace26ClearStringPaddingIfNeededEv@PLT
	movq	40792(%r13), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal12ReadOnlyHeap27OnCreateHeapObjectsCompleteEPNS0_7IsolateE@PLT
	jmp	.L3682
.L3753:
	leaq	41184(%r13), %rdi
	call	_ZN2v88internal8Builtins16PrintBuiltinCodeEv@PLT
	cmpb	$0, _ZN2v88internal23FLAG_print_builtin_sizeE(%rip)
	je	.L3696
.L3754:
	leaq	41184(%r13), %rdi
	call	_ZN2v88internal8Builtins16PrintBuiltinSizeEv@PLT
	jmp	.L3696
.L3755:
	movq	%r13, %rsi
	leaq	-608(%rbp), %rdi
	leaq	-328(%rbp), %r12
	call	_ZN2v88internal7Isolate19GetTurboCfgFileNameB5cxx11EPS1_
	movq	-608(%rbp), %r10
	leaq	-576(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, -648(%rbp)
	movq	%r10, -640(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -328(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rcx
	movw	%ax, -104(%rbp)
	movq	8+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -576(%rbp)
	movq	$0, -112(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -576(%rbp,%rax)
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	leaq	-568(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	24+_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -576(%rbp)
	addq	$40, %rax
	movq	%rax, -328(%rbp)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEEC1Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-640(%rbp), %r10
	movq	%r15, %rdi
	movl	$48, %edx
	movq	%r10, %rsi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	movq	-648(%rbp), %rdi
	testq	%rax, %rax
	movq	-576(%rbp), %rax
	je	.L3763
	addq	-24(%rax), %rdi
	xorl	%esi, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.L3699:
	leaq	64+_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	.LC94(%rip), %xmm0
	movq	%rax, -328(%rbp)
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	leaq	-464(%rbp), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-512(%rbp), %rdi
	movq	%rax, -568(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	16+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rcx
	movq	%r12, %rdi
	movq	%rax, -576(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -576(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -328(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-608(%rbp), %rdi
	leaq	-592(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3697
	call	_ZdlPv@PLT
	jmp	.L3697
.L3760:
	call	_ZN2v88internal10PagedSpace11SetReadableEv@PLT
	jmp	.L3685
.L3694:
	call	*%rax
	jmp	.L3693
.L3763:
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$4, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L3699
	.p2align 4,,10
	.p2align 3
.L3762:
	movq	%r12, %rdi
	call	_ZN2v88internal11MemoryChunk20SetReadAndExecutableEv@PLT
	movq	224(%r12), %r12
	testq	%r12, %r12
	jne	.L3691
	jmp	.L3683
	.p2align 4,,10
	.p2align 3
.L3686:
	leaq	.LC13(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3748:
	movq	88(%r13), %rax
	leaq	45496(%r13), %rdi
	movq	%r14, %rsi
	movq	%rax, -624(%rbp)
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE@PLT
	movq	96(%r13), %rax
	movl	$72, %edi
	movq	%rax, 12480(%r13)
	movq	%rax, 12536(%r13)
	movq	%rax, 12552(%r13)
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal22TracingCpuProfilerImplC1EPNS0_7IsolateE@PLT
	movq	45768(%r13), %rdi
	movq	%r15, 45768(%r13)
	testq	%rdi, %rdi
	je	.L3741
	call	_ZN2v88internal22TracingCpuProfilerImplD0Ev@PLT
.L3741:
	movq	40936(%r13), %rdi
	movl	$1, %esi
	call	_ZN2v88internal12Bootstrapper10InitializeEb@PLT
	movl	$72, %edi
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal29BuiltinsConstantsTableBuilderC1EPNS0_7IsolateE@PLT
	movq	41200(%r13), %rdi
	movq	%r15, 45520(%r13)
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	$57, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap7builtinEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal4Heap41SetInterpreterEntryTrampolineForProfilingENS0_4CodeE@PLT
	movq	45520(%r13), %rdi
	call	_ZN2v88internal29BuiltinsConstantsTableBuilder8FinalizeEv@PLT
	movq	45520(%r13), %r15
	testq	%r15, %r15
	je	.L3674
	leaq	16+_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE(%rip), %rax
	leaq	8(%r15), %rdi
	movq	%rax, 8(%r15)
	movq	%rdi, -648(%rbp)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	movq	-648(%rbp), %rdi
	call	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	movl	$72, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3674:
	movq	$0, 45520(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate24CreateAndSetEmbeddedBlobEv
	jmp	.L3704
.L3744:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10WasmEngine13GetWasmEngineEv@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate13SetWasmEngineESt10shared_ptrINS0_4wasm10WasmEngineEE
	movq	-616(%rbp), %r15
	testq	%r15, %r15
	je	.L3660
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L3663
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
.L3664:
	cmpl	$1, %eax
	jne	.L3660
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L3666
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L3667:
	cmpl	$1, %eax
	jne	.L3660
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L3660
	.p2align 4,,10
	.p2align 3
.L3745:
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	leaq	16+_ZTVN2v88internal20SetupIsolateDelegateE(%rip), %rax
	movq	%rax, (%rdi)
	movzbl	-648(%rbp), %eax
	movq	%rdi, 41200(%r13)
	movb	%al, 8(%rdi)
	jmp	.L3668
.L3663:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	jmp	.L3664
.L3666:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L3667
.L3747:
	xorl	%edx, %edx
	leaq	.LC90(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
.L3759:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	-648(%rbp), %r9
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r9, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L3680
	leaq	.LC92(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L3758:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25317:
	.size	_ZN2v88internal7Isolate4InitEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE, .-_ZN2v88internal7Isolate4InitEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE
	.section	.text._ZN2v88internal7Isolate19InitWithoutSnapshotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate19InitWithoutSnapshotEv
	.type	_ZN2v88internal7Isolate19InitWithoutSnapshotEv, @function
_ZN2v88internal7Isolate19InitWithoutSnapshotEv:
.LFB25313:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal7Isolate4InitEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE
	.cfi_endproc
.LFE25313:
	.size	_ZN2v88internal7Isolate19InitWithoutSnapshotEv, .-_ZN2v88internal7Isolate19InitWithoutSnapshotEv
	.section	.text._ZN2v88internal7Isolate16InitWithSnapshotEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate16InitWithSnapshotEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE
	.type	_ZN2v88internal7Isolate16InitWithSnapshotEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE, @function
_ZN2v88internal7Isolate16InitWithSnapshotEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE:
.LFB25314:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal7Isolate4InitEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE
	.cfi_endproc
.LFE25314:
	.size	_ZN2v88internal7Isolate16InitWithSnapshotEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE, .-_ZN2v88internal7Isolate16InitWithSnapshotEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE
	.section	.text._ZN2v88internal7IsolateD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7IsolateD2Ev
	.type	_ZN2v88internal7IsolateD2Ev, @function
_ZN2v88internal7IsolateD2Ev:
.LFB25302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	40808(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3767
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L3767:
	movq	41240(%rbx), %r12
	movq	$0, 40808(%rbx)
	testq	%r12, %r12
	je	.L3768
	movq	(%r12), %rax
	leaq	_ZN2v88internal9DateCacheD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3769
	movq	592(%r12), %rdi
	leaq	16+_ZTVN2v88internal9DateCacheE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3770
	movq	(%rdi), %rax
	call	*40(%rax)
.L3770:
	movl	$600, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3768:
	movq	41208(%rbx), %r12
	movq	$0, 41240(%rbx)
	testq	%r12, %r12
	je	.L3771
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpStackD1Ev@PLT
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3771:
	movq	41080(%rbx), %rdi
	movq	$0, 41208(%rbx)
	testq	%rdi, %rdi
	je	.L3772
	movl	$1280, %esi
	call	_ZdlPvm@PLT
.L3772:
	movq	41024(%rbx), %rdi
	movq	$0, 41080(%rbx)
	testq	%rdi, %rdi
	je	.L3773
	movl	$61448, %esi
	call	_ZdlPvm@PLT
.L3773:
	movq	41032(%rbx), %rdi
	movq	$0, 41024(%rbx)
	testq	%rdi, %rdi
	je	.L3774
	movl	$61448, %esi
	call	_ZdlPvm@PLT
.L3774:
	movq	41056(%rbx), %r12
	movq	$0, 41032(%rbx)
	testq	%r12, %r12
	je	.L3775
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3776
	call	_ZdlPv@PLT
.L3776:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3775:
	movq	41016(%rbx), %rdi
	movq	$0, 41056(%rbx)
	testq	%rdi, %rdi
	je	.L3777
	movq	(%rdi), %rax
	call	*8(%rax)
.L3777:
	movq	41120(%rbx), %r12
	movq	$0, 41016(%rbx)
	testq	%r12, %r12
	je	.L3778
	movq	104(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3779
	call	_ZdaPv@PLT
.L3779:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3780
	call	_ZdaPv@PLT
.L3780:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3781
	call	_ZdaPv@PLT
.L3781:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3782
	call	_ZdaPv@PLT
.L3782:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3783
	call	_ZdaPv@PLT
.L3783:
	movl	$152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3778:
	movq	41768(%rbx), %rdi
	movq	$0, 41120(%rbx)
	testq	%rdi, %rdi
	je	.L3784
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L3784:
	movq	40952(%rbx), %r12
	movq	$0, 41768(%rbx)
	testq	%r12, %r12
	je	.L3785
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3786
	call	_ZdaPv@PLT
.L3786:
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3787
	call	_ZdaPv@PLT
.L3787:
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3788
	call	_ZdaPv@PLT
.L3788:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3789
	call	_ZdaPv@PLT
.L3789:
	movl	$144, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3785:
	movq	40936(%rbx), %rdi
	movq	$0, 40952(%rbx)
	testq	%rdi, %rdi
	je	.L3790
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L3790:
	movq	41144(%rbx), %rdi
	movq	$0, 40936(%rbx)
	testq	%rdi, %rdi
	je	.L3791
	movl	$40968, %esi
	call	_ZdlPvm@PLT
.L3791:
	movq	41168(%rbx), %r12
	movq	$0, 41144(%rbx)
	testq	%r12, %r12
	je	.L3792
	movq	%r12, %rdi
	call	_ZN2v88internal13ThreadManagerD1Ev@PLT
	movl	$80, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3792:
	movq	41152(%rbx), %r12
	movq	$0, 41168(%rbx)
	testq	%r12, %r12
	je	.L3793
	movq	%r12, %rdi
	call	_ZN2v88internal13GlobalHandlesD1Ev@PLT
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3793:
	movq	41160(%rbx), %r12
	movq	$0, 41152(%rbx)
	testq	%r12, %r12
	je	.L3794
	movq	%r12, %rdi
	call	_ZN2v88internal14EternalHandlesD1Ev@PLT
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3794:
	movq	41712(%rbx), %r12
	movq	$0, 41160(%rbx)
	testq	%r12, %r12
	je	.L3795
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3796
	call	_ZdlPv@PLT
.L3796:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3795:
	movq	41248(%rbx), %rdi
	movq	$0, 41712(%rbx)
	testq	%rdi, %rdi
	je	.L3797
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3797:
	movq	41256(%rbx), %rdi
	movq	$0, 41248(%rbx)
	testq	%rdi, %rdi
	je	.L3798
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3798:
	movq	41472(%rbx), %r12
	movq	$0, 41256(%rbx)
	testq	%r12, %r12
	je	.L3799
	movq	%r12, %rdi
	call	_ZN2v88internal5DebugD1Ev@PLT
	movl	$144, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3799:
	movq	45640(%rbx), %r12
	movq	$0, 41472(%rbx)
	testq	%r12, %r12
	je	.L3800
	movq	%r12, %rdi
	call	_ZN2v88internal21CancelableTaskManagerD1Ev@PLT
	movl	$160, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3800:
	movq	41136(%rbx), %rdi
	movq	$0, 45640(%rbx)
	testq	%rdi, %rdi
	je	.L3801
	movq	(%rdi), %rax
	call	*8(%rax)
.L3801:
	movq	41752(%rbx), %rdi
	movq	$0, 41136(%rbx)
	testq	%rdi, %rdi
	je	.L3802
	call	_ZN2v88internal14MicrotaskQueueD0Ev@PLT
.L3802:
	movq	45856(%rbx), %r12
	movq	$0, 41752(%rbx)
	testq	%r12, %r12
	je	.L3806
	.p2align 4,,10
	.p2align 3
.L3803:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L3803
.L3806:
	movq	45848(%rbx), %rax
	movq	45840(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	45840(%rbx), %rdi
	leaq	45888(%rbx), %rax
	movq	$0, 45864(%rbx)
	movq	$0, 45856(%rbx)
	cmpq	%rax, %rdi
	je	.L3804
	call	_ZdlPv@PLT
.L3804:
	leaq	45800(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	45768(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3807
	call	_ZN2v88internal22TracingCpuProfilerImplD0Ev@PLT
.L3807:
	movq	45760(%rbx), %r12
	testq	%r12, %r12
	je	.L3809
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L3810
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L3999
	.p2align 4,,10
	.p2align 3
.L3809:
	leaq	45688(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	leaq	45552(%rbx), %rdi
	call	_ZN2v84base17ConditionVariableD1Ev@PLT
	movq	45496(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3815
	call	_ZdlPv@PLT
.L3815:
	movq	45464(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3816
	call	_ZdlPv@PLT
.L3816:
	movq	45440(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3817
	call	_ZdlPv@PLT
.L3817:
	movq	41536(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3818
	movq	41608(%rbx), %rax
	movq	41576(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L3819
	.p2align 4,,10
	.p2align 3
.L3820:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L3820
	movq	41536(%rbx), %rdi
.L3819:
	call	_ZdlPv@PLT
.L3818:
	movq	41488(%rbx), %r12
	testq	%r12, %r12
	je	.L3821
	leaq	56(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L3825
	.p2align 4,,10
	.p2align 3
.L3822:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3822
.L3825:
	movq	8(%r12), %rax
	movq	(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 24(%r12)
	movq	$0, 16(%r12)
	cmpq	%rax, %rdi
	je	.L3823
	call	_ZdlPv@PLT
.L3823:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3821:
	movq	41416(%rbx), %r12
	testq	%r12, %r12
	je	.L3839
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L3835
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L4000
	.p2align 4,,10
	.p2align 3
.L3837:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L3839
.L3829:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L3837
.L4000:
	lock subl	$1, 8(%r13)
	jne	.L3837
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L3837
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L3829
	.p2align 4,,10
	.p2align 3
.L3839:
	movq	41408(%rbx), %rax
	movq	41400(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	41400(%rbx), %rdi
	leaq	41448(%rbx), %rax
	movq	$0, 41424(%rbx)
	movq	$0, 41416(%rbx)
	cmpq	%rax, %rdi
	je	.L3827
	call	_ZdlPv@PLT
.L3827:
	movq	41368(%rbx), %rdi
	leaq	41384(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3840
	call	_ZdlPv@PLT
.L3840:
	leaq	41320(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	41216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3841
	call	_ZdlPv@PLT
.L3841:
	movq	41176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3842
	call	_ZdaPv@PLT
.L3842:
	leaq	40976(%rbx), %rdi
	call	_ZN2v84base14RecursiveMutexD1Ev@PLT
	movq	40968(%rbx), %r12
	testq	%r12, %r12
	je	.L3844
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L3845
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L4001
	.p2align 4,,10
	.p2align 3
.L3844:
	leaq	37592(%rbx), %rdi
	call	_ZN2v88internal4HeapD1Ev@PLT
	movq	37584(%rbx), %r12
	testq	%r12, %r12
	je	.L3766
	movq	%r12, %rdi
	call	_ZN2v88internal16IsolateAllocatorD1Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L3833:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L3832
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L3832:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L3839
.L3835:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L3832
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L3832
	jmp	.L3833
	.p2align 4,,10
	.p2align 3
.L3810:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L3809
.L3999:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L3813
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L3814:
	cmpl	$1, %eax
	jne	.L3809
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L3809
	.p2align 4,,10
	.p2align 3
.L3845:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L3844
.L4001:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L3848
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L3849:
	cmpl	$1, %eax
	jne	.L3844
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L3844
	.p2align 4,,10
	.p2align 3
.L3766:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3769:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3768
	.p2align 4,,10
	.p2align 3
.L3848:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L3849
	.p2align 4,,10
	.p2align 3
.L3813:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L3814
	.cfi_endproc
.LFE25302:
	.size	_ZN2v88internal7IsolateD2Ev, .-_ZN2v88internal7IsolateD2Ev
	.globl	_ZN2v88internal7IsolateD1Ev
	.set	_ZN2v88internal7IsolateD1Ev,_ZN2v88internal7IsolateD2Ev
	.section	.text._ZN2v88internal7Isolate6DeleteEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Isolate6DeleteEPS1_
	.type	_ZN2v88internal7Isolate6DeleteEPS1_, @function
_ZN2v88internal7Isolate6DeleteEPS1_:
.LFB25133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	_ZN2v88internal7Isolate28per_isolate_thread_data_key_E(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	_ZN2v88internal7Isolate12isolate_key_E(%rip), %edi
	movq	%rax, %r13
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	_ZN2v88internal7Isolate12isolate_key_E(%rip), %edi
	movq	%r12, %rsi
	movq	%rax, %r14
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	movl	_ZN2v88internal7Isolate28per_isolate_thread_data_key_E(%rip), %edi
	xorl	%esi, %esi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate6DeinitEv
	movq	37584(%r12), %r15
	movq	%r12, %rdi
	movq	$0, 37584(%r12)
	call	_ZN2v88internal7IsolateD1Ev
	testq	%r15, %r15
	je	.L4003
	movq	%r15, %rdi
	call	_ZN2v88internal16IsolateAllocatorD1Ev@PLT
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4003:
	movl	_ZN2v88internal7Isolate12isolate_key_E(%rip), %edi
	movq	%r14, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	movl	_ZN2v88internal7Isolate28per_isolate_thread_data_key_E(%rip), %edi
	popq	%r12
	movq	%r13, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	.cfi_endproc
.LFE25133:
	.size	_ZN2v88internal7Isolate6DeleteEPS1_, .-_ZN2v88internal7Isolate6DeleteEPS1_
	.section	.rodata.CSWTCH.699,"a"
	.align 16
	.type	CSWTCH.699, @object
	.size	CSWTCH.699, 20
CSWTCH.699:
	.long	0
	.long	1
	.long	4
	.long	3
	.long	5
	.weak	_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE
	.section	.data.rel.ro.local._ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE,"awG",@progbits,_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE,comdat
	.align 8
	.type	_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE, @object
	.size	_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE, 48
_ZTVN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED1Ev
	.quad	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEED0Ev
	.quad	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm
	.quad	_ZN2v88internal11IdentityMapIjNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv
	.weak	_ZTVN2v88internal11interpreter11InterpreterE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter11InterpreterE,"awG",@progbits,_ZTVN2v88internal11interpreter11InterpreterE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter11InterpreterE, @object
	.size	_ZTVN2v88internal11interpreter11InterpreterE, 32
_ZTVN2v88internal11interpreter11InterpreterE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter11InterpreterD1Ev
	.quad	_ZN2v88internal11interpreter11InterpreterD0Ev
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.weak	_ZTVN2v88internal26VerboseAccountingAllocatorE
	.section	.data.rel.ro.local._ZTVN2v88internal26VerboseAccountingAllocatorE,"awG",@progbits,_ZTVN2v88internal26VerboseAccountingAllocatorE,comdat
	.align 8
	.type	_ZTVN2v88internal26VerboseAccountingAllocatorE, @object
	.size	_ZTVN2v88internal26VerboseAccountingAllocatorE, 64
_ZTVN2v88internal26VerboseAccountingAllocatorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal26VerboseAccountingAllocatorD1Ev
	.quad	_ZN2v88internal26VerboseAccountingAllocatorD0Ev
	.quad	_ZN2v88internal26VerboseAccountingAllocator15AllocateSegmentEm
	.quad	_ZN2v88internal26VerboseAccountingAllocator13ReturnSegmentEPNS0_7SegmentE
	.quad	_ZN2v88internal26VerboseAccountingAllocator12ZoneCreationEPKNS0_4ZoneE
	.quad	_ZN2v88internal26VerboseAccountingAllocator15ZoneDestructionEPKNS0_4ZoneE
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal8CountersESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.bss._ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE29trace_event_unique_atomic3974,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE29trace_event_unique_atomic3974, @object
	.size	_ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE29trace_event_unique_atomic3974, 8
_ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE29trace_event_unique_atomic3974:
	.zero	8
	.section	.rodata._ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE30kInvalidateProtectorTracingArg,"a"
	.align 8
	.type	_ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE30kInvalidateProtectorTracingArg, @object
	.size	_ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE30kInvalidateProtectorTracingArg, 15
_ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE30kInvalidateProtectorTracingArg:
	.string	"protector-name"
	.section	.rodata._ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE35kInvalidateProtectorTracingCategory,"a"
	.align 16
	.type	_ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE35kInvalidateProtectorTracingCategory, @object
	.size	_ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE35kInvalidateProtectorTracingCategory, 23
_ZZN2v88internal7Isolate26TraceProtectorInvalidationEPKcE35kInvalidateProtectorTracingCategory:
	.string	"V8.InvalidateProtector"
	.section	.bss._ZN2v88internal12_GLOBAL__N_115isolate_counterE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal12_GLOBAL__N_115isolate_counterE, @object
	.size	_ZN2v88internal12_GLOBAL__N_115isolate_counterE, 4
_ZN2v88internal12_GLOBAL__N_115isolate_counterE:
	.zero	4
	.globl	_ZN2v88internal7Isolate28per_isolate_thread_data_key_E
	.section	.bss._ZN2v88internal7Isolate28per_isolate_thread_data_key_E,"aw",@nobits
	.align 4
	.type	_ZN2v88internal7Isolate28per_isolate_thread_data_key_E, @object
	.size	_ZN2v88internal7Isolate28per_isolate_thread_data_key_E, 4
_ZN2v88internal7Isolate28per_isolate_thread_data_key_E:
	.zero	4
	.globl	_ZN2v88internal7Isolate12isolate_key_E
	.section	.bss._ZN2v88internal7Isolate12isolate_key_E,"aw",@nobits
	.align 4
	.type	_ZN2v88internal7Isolate12isolate_key_E, @object
	.size	_ZN2v88internal7Isolate12isolate_key_E, 4
_ZN2v88internal7Isolate12isolate_key_E:
	.zero	4
	.section	.bss._ZN2v88internal12_GLOBAL__N_127current_embedded_blob_refs_E,"aw",@nobits
	.align 4
	.type	_ZN2v88internal12_GLOBAL__N_127current_embedded_blob_refs_E, @object
	.size	_ZN2v88internal12_GLOBAL__N_127current_embedded_blob_refs_E, 4
_ZN2v88internal12_GLOBAL__N_127current_embedded_blob_refs_E:
	.zero	4
	.section	.data._ZN2v88internal12_GLOBAL__N_133enable_embedded_blob_refcounting_E,"aw"
	.type	_ZN2v88internal12_GLOBAL__N_133enable_embedded_blob_refcounting_E, @object
	.size	_ZN2v88internal12_GLOBAL__N_133enable_embedded_blob_refcounting_E, 1
_ZN2v88internal12_GLOBAL__N_133enable_embedded_blob_refcounting_E:
	.byte	1
	.section	.bss._ZN2v88internal12_GLOBAL__N_126sticky_embedded_blob_size_E,"aw",@nobits
	.align 4
	.type	_ZN2v88internal12_GLOBAL__N_126sticky_embedded_blob_size_E, @object
	.size	_ZN2v88internal12_GLOBAL__N_126sticky_embedded_blob_size_E, 4
_ZN2v88internal12_GLOBAL__N_126sticky_embedded_blob_size_E:
	.zero	4
	.section	.bss._ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E,"aw",@nobits
	.align 8
	.type	_ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E, @object
	.size	_ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E, 8
_ZN2v88internal12_GLOBAL__N_121sticky_embedded_blob_E:
	.zero	8
	.section	.bss._ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E,"aw",@nobits
	.align 32
	.type	_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E, @object
	.size	_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E, 48
_ZN2v88internal12_GLOBAL__N_137current_embedded_blob_refcount_mutex_E:
	.zero	48
	.section	.bss._ZN2v88internal12_GLOBAL__N_127current_embedded_blob_size_E,"aw",@nobits
	.align 4
	.type	_ZN2v88internal12_GLOBAL__N_127current_embedded_blob_size_E, @object
	.size	_ZN2v88internal12_GLOBAL__N_127current_embedded_blob_size_E, 4
_ZN2v88internal12_GLOBAL__N_127current_embedded_blob_size_E:
	.zero	4
	.section	.bss._ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E,"aw",@nobits
	.align 8
	.type	_ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E, @object
	.size	_ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E, 8
_ZN2v88internal12_GLOBAL__N_122current_embedded_blob_E:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.data.rel.ro,"aw"
	.align 8
.LC5:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC6:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC7:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC22:
	.long	0
	.long	-1042284544
	.align 8
.LC23:
	.long	4290772992
	.long	1105199103
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC51:
	.quad	0
	.quad	67108864
	.align 16
.LC52:
	.quad	-8
	.quad	-8
	.section	.data.rel.ro
	.align 8
.LC78:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.section	.rodata.cst16
	.align 16
.LC89:
	.long	-2
	.long	-2
	.long	-2
	.long	-2
	.section	.data.rel.ro
	.align 8
.LC94:
	.quad	_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE+24
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
