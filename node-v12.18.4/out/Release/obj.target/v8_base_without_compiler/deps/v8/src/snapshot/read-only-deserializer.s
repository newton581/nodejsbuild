	.file	"read-only-deserializer.cc"
	.text
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE:
.LFB26002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26002:
	.size	_GLOBAL__sub_I__ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ReadOnlyDeserializer"
	.section	.text._ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE
	.type	_ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE, @function
_ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE:
.LFB20112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE@PLT
	leaq	328(%r12), %rdi
	call	_ZN2v88internal21DeserializerAllocator12ReserveSpaceEv@PLT
	testb	%al, %al
	je	.L11
	movq	40792(%r14), %r13
	leaq	-48(%rbp), %rdi
	addq	$56, %r14
	movq	%r12, %rsi
	movq	%r14, -48(%rbp)
	call	_ZN2v88internal13ReadOnlyRoots7IterateEPNS0_11RootVisitorE@PLT
	movq	8(%r13), %rdi
	call	_ZN2v88internal13ReadOnlySpace35RepairFreeListsAfterDeserializationEv@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%r13, %rdi
	call	_ZN2v88internal12ReadOnlyHeap25ExtendReadOnlyObjectCacheEv@PLT
	xorl	%edx, %edx
	movl	$20, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	8(%rax), %r8
	movq	%rax, %rbx
	call	_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_@PLT
	movq	-48(%rbp), %rax
	movq	(%rbx), %rcx
	cmpq	%rcx, 32(%rax)
	jne	.L6
	movq	%r12, %rdi
	call	_ZN2v88internal12Deserializer26DeserializeDeferredObjectsEv@PLT
	cmpb	$0, _ZN2v88internal20FLAG_rehash_snapshotE(%rip)
	jne	.L12
.L4:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L13
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	cmpb	$0, 593(%r12)
	je	.L4
	movq	80(%r12), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap18InitializeHashSeedEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal12Deserializer6RehashEv@PLT
	jmp	.L4
.L13:
	call	__stack_chk_fail@PLT
.L11:
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.cfi_endproc
.LFE20112:
	.size	_ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE, .-_ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
