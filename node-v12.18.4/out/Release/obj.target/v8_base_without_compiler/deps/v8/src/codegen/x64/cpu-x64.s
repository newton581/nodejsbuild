	.file	"cpu-x64.cc"
	.text
	.section	.text._ZN2v88internal11CpuFeatures11FlushICacheEPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CpuFeatures11FlushICacheEPvm
	.type	_ZN2v88internal11CpuFeatures11FlushICacheEPvm, @function
_ZN2v88internal11CpuFeatures11FlushICacheEPvm:
.LFB3147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	$4098, -64(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rdi, -56(%rbp)
	movq	%rsi, -48(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movq	$0, -24(%rbp)
#APP
# 34 "../deps/v8/src/codegen/x64/cpu-x64.cc" 1
	rolq $3,  %rdi ; rolq $13, %rdi
	rolq $61, %rdi ; rolq $51, %rdi
	xchgq %rbx,%rbx
# 0 "" 2
#NO_APP
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3147:
	.size	_ZN2v88internal11CpuFeatures11FlushICacheEPvm, .-_ZN2v88internal11CpuFeatures11FlushICacheEPvm
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
