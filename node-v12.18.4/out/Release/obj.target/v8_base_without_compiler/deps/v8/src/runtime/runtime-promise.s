	.file	"runtime-promise.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4860:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4860:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4861:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4861:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4863:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4863:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123AwaitPromisesInitCommonEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_9JSPromiseEEES8_NS4_INS0_10JSFunctionEEEb.str1.1,"aMS",@progbits,1
.LC0:
	.string	"(location_) != nullptr"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12_GLOBAL__N_123AwaitPromisesInitCommonEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_9JSPromiseEEES8_NS4_INS0_10JSFunctionEEEb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123AwaitPromisesInitCommonEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_9JSPromiseEEES8_NS4_INS0_10JSFunctionEEEb, @function
_ZN2v88internal12_GLOBAL__N_123AwaitPromisesInitCommonEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_9JSPromiseEEES8_NS4_INS0_10JSFunctionEEEb:
.LFB18641:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$56, %rsp
	movq	%r8, -88(%rbp)
	movl	%r9d, -92(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory23NewJSPromiseWithoutHookENS0_14AllocationTypeE@PLT
	xorl	%esi, %esi
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%rax, %r12
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	movq	(%r12), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -72(%rbp)
	movq	0(%r13), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9JSPromise13async_task_idEv@PLT
	leaq	-72(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal9JSPromise17set_async_task_idEi@PLT
	movq	(%r12), %rdx
	movslq	35(%rdx), %rax
	orl	$4, %eax
	salq	$32, %rax
	movq	%rax, 31(%rdx)
	movq	41472(%r15), %rax
	cmpb	$0, 8(%rax)
	je	.L6
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L19
.L8:
	xorl	%r8d, %r8d
	leaq	3792(%r15), %rdx
	movq	%r13, %rcx
	movq	%r12, %rsi
	movl	$1, %r9d
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L13
.L6:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L8
	movq	-88(%rbp), %rsi
	xorl	%r8d, %r8d
	leaq	112(%r15), %rcx
	movq	%r15, %rdi
	leaq	3784(%r15), %rdx
	movl	$1, %r9d
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L13
	movq	(%rbx), %rdx
	movslq	35(%rdx), %rax
	movl	%eax, %ecx
	andl	$-9, %eax
	orl	$8, %ecx
	cmpb	$0, -92(%rbp)
	cmovne	%ecx, %eax
	salq	$32, %rax
	movq	%rax, 31(%rdx)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18641:
	.size	_ZN2v88internal12_GLOBAL__N_123AwaitPromisesInitCommonEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_9JSPromiseEEES8_NS4_INS0_10JSFunctionEEEb, .-_ZN2v88internal12_GLOBAL__N_123AwaitPromisesInitCommonEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_9JSPromiseEEES8_NS4_INS0_10JSFunctionEEEb
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB18192:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L27
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L30
.L21:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L21
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE18192:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC3:
	.string	"V8.Runtime_Runtime_PromiseRejectAfterResolved"
	.section	.rodata._ZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC4:
	.string	"args[0].IsJSPromise()"
	.section	.text._ZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE.isra.0:
.LFB22790:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L63
.L32:
	movq	_ZZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateEE27trace_event_unique_atomic44(%rip), %rbx
	testq	%rbx, %rbx
	je	.L64
.L34:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L65
.L36:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L40
.L41:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L66
.L35:
	movq	%rbx, _ZZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateEE27trace_event_unique_atomic44(%rip)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L40:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L41
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movl	$2, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L42
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L42:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L67
.L31:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L68
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L69
.L37:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L38
	movq	(%rdi), %rax
	call	*8(%rax)
.L38:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L39
	movq	(%rdi), %rax
	call	*8(%rax)
.L39:
	leaq	.LC3(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L63:
	movq	40960(%rsi), %rax
	movl	$486, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L69:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L37
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22790:
	.size	_ZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL41Stats_Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"V8.Runtime_Runtime_PromiseResolveAfterResolved"
	.section	.text._ZN2v88internalL41Stats_Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Stats_Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL41Stats_Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateE.isra.0:
.LFB22791:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L102
.L71:
	movq	_ZZN2v88internalL41Stats_Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateEE27trace_event_unique_atomic54(%rip), %rbx
	testq	%rbx, %rbx
	je	.L103
.L73:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L104
.L75:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L79
.L80:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L103:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L105
.L74:
	movq	%rbx, _ZZN2v88internalL41Stats_Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateEE27trace_event_unique_atomic54(%rip)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L79:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L80
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movl	$3, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L81
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L81:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L106
.L70:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L108
.L76:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	movq	(%rdi), %rax
	call	*8(%rax)
.L77:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L78
	movq	(%rdi), %rax
	call	*8(%rax)
.L78:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L102:
	movq	40960(%rsi), %rax
	movl	$487, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L108:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L76
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22791:
	.size	_ZN2v88internalL41Stats_Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL41Stats_Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL40Stats_Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Runtime_Runtime_PerformMicrotaskCheckpoint"
	.section	.text._ZN2v88internalL40Stats_Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateE.isra.0:
.LFB22792:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L140
.L110:
	movq	_ZZN2v88internalL40Stats_Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateEE27trace_event_unique_atomic88(%rip), %rbx
	testq	%rbx, %rbx
	je	.L141
.L112:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L142
.L114:
	addl	$1, 41104(%r12)
	movq	41088(%r12), %r14
	movq	%r12, %rdi
	movq	41096(%r12), %rbx
	call	_ZN2v815MicrotasksScope17PerformCheckpointEPNS_7IsolateE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L120
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L120:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L143
.L109:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L144
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L145
.L115:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L116
	movq	(%rdi), %rax
	call	*8(%rax)
.L116:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L117
	movq	(%rdi), %rax
	call	*8(%rax)
.L117:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L141:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L146
.L113:
	movq	%rbx, _ZZN2v88internalL40Stats_Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateEE27trace_event_unique_atomic88(%rip)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L140:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$360, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L143:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L146:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L145:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L115
.L144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22792:
	.size	_ZN2v88internalL40Stats_Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL34Stats_Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"V8.Runtime_Runtime_RunMicrotaskCallback"
	.section	.text._ZN2v88internalL34Stats_Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateE.isra.0:
.LFB22793:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L184
.L148:
	movq	_ZZN2v88internalL34Stats_Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateEE27trace_event_unique_atomic95(%rip), %rbx
	testq	%rbx, %rbx
	je	.L185
.L150:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L186
.L152:
	addl	$1, 41104(%r12)
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	movq	0(%r13), %rcx
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	movq	-8(%r13), %rdx
	cmpq	%rax, %rcx
	je	.L166
	movq	7(%rcx), %rcx
.L156:
	cmpq	%rax, %rdx
	je	.L167
	movq	7(%rdx), %rdi
.L157:
	call	*%rcx
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L158
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
.L159:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L162
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L162:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L187
.L147:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L188
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	88(%r12), %r13
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L186:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L189
.L153:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L154
	movq	(%rdi), %rax
	call	*8(%rax)
.L154:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L155
	movq	(%rdi), %rax
	call	*8(%rax)
.L155:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L185:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L190
.L151:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateEE27trace_event_unique_atomic95(%rip)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L184:
	movq	40960(%rsi), %rax
	movl	$359, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L167:
	xorl	%edi, %edi
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L166:
	xorl	%ecx, %ecx
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L190:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L189:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L153
.L188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22793:
	.size	_ZN2v88internalL34Stats_Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL34Stats_Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"V8.Runtime_Runtime_PromiseMarkAsHandled"
	.section	.text._ZN2v88internalL34Stats_Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateE.isra.0:
.LFB22794:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L222
.L192:
	movq	_ZZN2v88internalL34Stats_Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateEE28trace_event_unique_atomic115(%rip), %rbx
	testq	%rbx, %rbx
	je	.L223
.L194:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L224
.L196:
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L200
.L201:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L223:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L225
.L195:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateEE28trace_event_unique_atomic115(%rip)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L200:
	movq	-1(%rdx), %rax
	cmpw	$1074, 11(%rax)
	jne	.L201
	movslq	35(%rdx), %rax
	leaq	-144(%rbp), %rdi
	orl	$4, %eax
	salq	$32, %rax
	movq	%rax, 31(%rdx)
	movq	88(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L226
.L191:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L227
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L228
.L197:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L198
	movq	(%rdi), %rax
	call	*8(%rax)
.L198:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L199
	movq	(%rdi), %rax
	call	*8(%rax)
.L199:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L222:
	movq	40960(%rsi), %rax
	movl	$480, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L226:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L228:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L197
.L227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22794:
	.size	_ZN2v88internalL34Stats_Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL29Stats_Runtime_PromiseHookInitEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"V8.Runtime_Runtime_PromiseHookInit"
	.section	.text._ZN2v88internalL29Stats_Runtime_PromiseHookInitEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_PromiseHookInitEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_PromiseHookInitEiPmPNS0_7IsolateE.isra.0:
.LFB22795:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L261
.L230:
	movq	_ZZN2v88internalL29Stats_Runtime_PromiseHookInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic124(%rip), %rbx
	testq	%rbx, %rbx
	je	.L262
.L232:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L263
.L234:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L238
.L239:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L262:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L264
.L233:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_PromiseHookInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic124(%rip)
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L238:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L239
	leaq	-8(%r13), %rcx
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L240
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L240:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L265
.L229:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L266
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L267
.L235:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L236
	movq	(%rdi), %rax
	call	*8(%rax)
.L236:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L237
	movq	(%rdi), %rax
	call	*8(%rax)
.L237:
	leaq	.LC9(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L261:
	movq	40960(%rsi), %rax
	movl	$477, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L265:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L264:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L267:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L235
.L266:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22795:
	.size	_ZN2v88internalL29Stats_Runtime_PromiseHookInitEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_PromiseHookInitEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_ResolvePromiseEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"V8.Runtime_Runtime_ResolvePromise"
	.section	.text._ZN2v88internalL28Stats_Runtime_ResolvePromiseEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_ResolvePromiseEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_ResolvePromiseEiPmPNS0_7IsolateE.isra.0:
.LFB22796:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L303
.L269:
	movq	_ZZN2v88internalL28Stats_Runtime_ResolvePromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic252(%rip), %rbx
	testq	%rbx, %rbx
	je	.L304
.L271:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L305
.L273:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L277
.L278:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L304:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L306
.L272:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_ResolvePromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic252(%rip)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L277:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L278
	leaq	-8(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal9JSPromise7ResolveENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L307
	movq	(%rax), %r13
.L280:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L283
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L283:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L308
.L268:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L309
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L310
.L274:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L275
	movq	(%rdi), %rax
	call	*8(%rax)
.L275:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L276
	movq	(%rdi), %rax
	call	*8(%rax)
.L276:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L307:
	movq	312(%r12), %r13
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L303:
	movq	40960(%rsi), %rax
	movl	$485, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L308:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L306:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L310:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L274
.L309:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22796:
	.size	_ZN2v88internalL28Stats_Runtime_ResolvePromiseEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_ResolvePromiseEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"V8.Runtime_Runtime_RejectPromise"
	.section	.rodata._ZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"args[2].IsOddball()"
	.section	.text._ZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateE, @function
_ZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateE:
.LFB18654:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L345
.L312:
	movq	_ZZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic242(%rip), %rbx
	testq	%rbx, %rbx
	je	.L346
.L314:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L347
.L316:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L320
.L321:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L346:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L348
.L315:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic242(%rip)
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L320:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L321
	movq	-16(%r13), %rax
	leaq	-8(%r13), %r15
	testb	$1, %al
	jne	.L349
.L322:
	leaq	.LC12(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L347:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L350
.L317:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L318
	movq	(%rdi), %rax
	call	*8(%rax)
.L318:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L319
	movq	(%rdi), %rax
	call	*8(%rax)
.L319:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L349:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L322
	leaq	-168(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movzbl	%al, %edx
	call	_ZN2v88internal9JSPromise6RejectENS0_6HandleIS1_EENS2_INS0_6ObjectEEEb@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L324
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L324:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L351
.L311:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L352
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore_state
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$484, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L351:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L350:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L315
.L352:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18654:
	.size	_ZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateE, .-_ZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8.Runtime_Runtime_EnqueueMicrotask"
	.section	.rodata._ZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC14:
	.string	"args[0].IsJSFunction()"
	.section	.text._ZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE.isra.0:
.LFB22807:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L394
.L354:
	movq	_ZZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateEE27trace_event_unique_atomic75(%rip), %r13
	testq	%r13, %r13
	je	.L395
.L356:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L396
.L358:
	movq	41088(%r12), %r13
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L397
.L362:
	leaq	.LC14(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L395:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L398
.L357:
	movq	%r13, _ZZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateEE27trace_event_unique_atomic75(%rip)
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L397:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L362
	movq	31(%rax), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L399
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L365:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal7Factory15NewCallableTaskENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7ContextEEE@PLT
	movq	(%rbx), %rdx
	movq	31(%rdx), %rdx
	movq	39(%rdx), %rdx
	movq	1967(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L367
	movq	(%rax), %rsi
	call	_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskENS0_9MicrotaskE@PLT
.L367:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r15
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L370
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L370:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L400
.L353:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L401
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	movq	%r13, %rdx
	cmpq	41096(%r12), %r13
	je	.L402
.L366:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L396:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L403
.L359:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L360
	movq	(%rdi), %rax
	call	*8(%rax)
.L360:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L361
	movq	(%rdi), %rax
	call	*8(%rax)
.L361:
	leaq	.LC13(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L394:
	movq	40960(%rsi), %rax
	movl	$474, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L400:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L402:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L403:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L398:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L357
.L401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22807:
	.size	_ZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL41Stats_Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"V8.Runtime_Runtime_PromiseRejectEventFromStack"
	.section	.text._ZN2v88internalL41Stats_Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Stats_Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL41Stats_Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE.isra.0:
.LFB22808:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L439
.L405:
	movq	_ZZN2v88internalL41Stats_Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateEE27trace_event_unique_atomic20(%rip), %rbx
	testq	%rbx, %rbx
	je	.L440
.L407:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L441
.L409:
	movq	41088(%r12), %rax
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	%rax, -168(%rbp)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L442
.L413:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L440:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L443
.L408:
	movq	%rbx, _ZZN2v88internalL41Stats_Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateEE27trace_event_unique_atomic20(%rip)
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L442:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L413
	movq	41472(%r12), %rax
	leaq	-8(%r13), %r14
	movq	%r13, %r15
	cmpb	$0, 8(%rax)
	jne	.L444
.L415:
	leaq	88(%r12), %rcx
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	movq	41472(%r12), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal5Debug15OnPromiseRejectENS0_6HandleINS0_6ObjectEEES4_@PLT
	movq	0(%r13), %rax
	testb	$4, 35(%rax)
	je	.L445
.L416:
	movq	-168(%rbp), %rax
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L419
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L419:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L446
.L404:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L447
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L448
.L410:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L411
	movq	(%rdi), %rax
	call	*8(%rax)
.L411:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L412
	movq	(%rdi), %rax
	call	*8(%rax)
.L412:
	leaq	.LC15(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L445:
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE@PLT
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L444:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate24GetPromiseOnStackOnThrowEv@PLT
	movq	%rax, %r15
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L439:
	movq	40960(%rsi), %rax
	movl	$481, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L446:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L443:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L448:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC15(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L410
.L447:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22808:
	.size	_ZN2v88internalL41Stats_Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL41Stats_Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"V8.Runtime_Runtime_PromiseRevokeReject"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC17:
	.string	"!promise->has_handler()"
	.section	.text._ZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE.isra.0:
.LFB22809:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L483
.L450:
	movq	_ZZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateEE27trace_event_unique_atomic64(%rip), %rbx
	testq	%rbx, %rbx
	je	.L484
.L452:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L485
.L454:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L486
.L458:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L484:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L487
.L453:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateEE27trace_event_unique_atomic64(%rip)
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L486:
	movq	-1(%rax), %rdx
	cmpw	$1074, 11(%rdx)
	jne	.L458
	testb	$4, 35(%rax)
	jne	.L488
	movq	%r13, %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L461
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L461:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L489
.L449:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L490
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L491
.L455:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L456
	movq	(%rdi), %rax
	call	*8(%rax)
.L456:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L457
	movq	(%rdi), %rax
	call	*8(%rax)
.L457:
	leaq	.LC16(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L483:
	movq	40960(%rsi), %rax
	movl	$482, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L488:
	leaq	.LC17(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L489:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L491:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC16(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L487:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L453
.L490:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22809:
	.size	_ZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL27Stats_Runtime_PromiseStatusEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"V8.Runtime_Runtime_PromiseStatus"
	.section	.text._ZN2v88internalL27Stats_Runtime_PromiseStatusEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_PromiseStatusEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_PromiseStatusEiPmPNS0_7IsolateE.isra.0:
.LFB22810:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L524
.L493:
	movq	_ZZN2v88internalL27Stats_Runtime_PromiseStatusEiPmPNS0_7IsolateEE28trace_event_unique_atomic107(%rip), %rbx
	testq	%rbx, %rbx
	je	.L525
.L495:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L526
.L497:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L527
.L501:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L525:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L528
.L496:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_PromiseStatusEiPmPNS0_7IsolateEE28trace_event_unique_atomic107(%rip)
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L527:
	movq	-1(%rax), %rdx
	cmpw	$1074, 11(%rdx)
	jne	.L501
	leaq	-152(%rbp), %rdi
	movq	%rax, -152(%rbp)
	call	_ZNK2v88internal9JSPromise6statusEv@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	salq	$32, %r13
	cmpq	41096(%r12), %rbx
	je	.L503
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L503:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L529
.L492:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L530
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L531
.L498:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L499
	movq	(%rdi), %rax
	call	*8(%rax)
.L499:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L500
	movq	(%rdi), %rax
	call	*8(%rax)
.L500:
	leaq	.LC18(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L524:
	movq	40960(%rsi), %rax
	movl	$483, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L529:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L528:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L531:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC18(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L498
.L530:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22810:
	.size	_ZN2v88internalL27Stats_Runtime_PromiseStatusEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_PromiseStatusEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"V8.Runtime_Runtime_PromiseHookBefore"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC20:
	.string	"args[0].IsJSReceiver()"
	.section	.text._ZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE.isra.0:
.LFB22811:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L570
.L533:
	movq	_ZZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateEE28trace_event_unique_atomic212(%rip), %rbx
	testq	%rbx, %rbx
	je	.L571
.L535:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L572
.L537:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L573
.L541:
	leaq	.LC20(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L571:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L574
.L536:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateEE28trace_event_unique_atomic212(%rip)
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L573:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L541
	movq	-1(%rax), %rdx
	cmpw	$1074, 11(%rdx)
	je	.L575
.L546:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L549
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L549:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L576
.L532:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L577
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L578
.L538:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L539
	movq	(%rdi), %rax
	call	*8(%rax)
.L539:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L540
	movq	(%rdi), %rax
	call	*8(%rax)
.L540:
	leaq	.LC19(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L575:
	movq	41472(%r12), %rdx
	cmpb	$0, 8(%rdx)
	jne	.L579
.L545:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L546
	leaq	88(%r12), %rcx
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L579:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	0(%r13), %rax
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L570:
	movq	40960(%rsi), %rax
	movl	$476, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L576:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L578:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC19(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L574:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L536
.L577:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22811:
	.size	_ZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL30Stats_Runtime_PromiseHookAfterEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"V8.Runtime_Runtime_PromiseHookAfter"
	.section	.text._ZN2v88internalL30Stats_Runtime_PromiseHookAfterEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_PromiseHookAfterEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL30Stats_Runtime_PromiseHookAfterEiPmPNS0_7IsolateE.isra.0:
.LFB22812:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L618
.L581:
	movq	_ZZN2v88internalL30Stats_Runtime_PromiseHookAfterEiPmPNS0_7IsolateEE28trace_event_unique_atomic227(%rip), %rbx
	testq	%rbx, %rbx
	je	.L619
.L583:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L620
.L585:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L621
.L589:
	leaq	.LC20(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L619:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L622
.L584:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_PromiseHookAfterEiPmPNS0_7IsolateEE28trace_event_unique_atomic227(%rip)
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L621:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L589
	movq	-1(%rax), %rdx
	cmpw	$1074, 11(%rdx)
	je	.L623
.L594:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L597
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L597:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L624
.L580:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L625
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L620:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L626
.L586:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L587
	movq	(%rdi), %rax
	call	*8(%rax)
.L587:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L588
	movq	(%rdi), %rax
	call	*8(%rax)
.L588:
	leaq	.LC21(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L623:
	movq	41472(%r12), %rdx
	cmpb	$0, 8(%rdx)
	jne	.L627
.L593:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L594
	leaq	88(%r12), %rcx
	movq	%r13, %rdx
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L627:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10PopPromiseEv@PLT
	movq	0(%r13), %rax
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L618:
	movq	40960(%rsi), %rax
	movl	$475, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L624:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L626:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC21(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L622:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L584
.L625:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22812:
	.size	_ZN2v88internalL30Stats_Runtime_PromiseHookAfterEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL30Stats_Runtime_PromiseHookAfterEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"V8.Runtime_Runtime_AwaitPromisesInit"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC23:
	.string	"args[1].IsJSPromise()"
.LC24:
	.string	"args[2].IsJSPromise()"
.LC25:
	.string	"args[3].IsJSFunction()"
.LC26:
	.string	"args[4].IsBoolean()"
	.section	.text._ZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE.isra.0:
.LFB22813:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L669
.L629:
	movq	_ZZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic184(%rip), %rbx
	testq	%rbx, %rbx
	je	.L670
.L631:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L671
.L633:
	addl	$1, 41104(%r12)
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	testb	$1, %al
	jne	.L672
.L637:
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L670:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L673
.L632:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic184(%rip)
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L672:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L637
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rcx
	testb	$1, %al
	jne	.L674
.L638:
	leaq	.LC24(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L671:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L675
.L634:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L635
	movq	(%rdi), %rax
	call	*8(%rax)
.L635:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L636
	movq	(%rdi), %rax
	call	*8(%rax)
.L636:
	leaq	.LC22(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L674:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L638
	movq	-24(%r13), %rax
	leaq	-24(%r13), %r8
	testb	$1, %al
	jne	.L676
.L640:
	leaq	.LC25(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L676:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L640
	movq	-32(%r13), %rax
	testb	$1, %al
	jne	.L677
.L642:
	leaq	.LC26(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L677:
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	jne	.L642
	testb	$-2, 43(%rax)
	jne	.L642
	xorl	%r9d, %r9d
	cmpq	%rax, 112(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	sete	%r9b
	call	_ZN2v88internal12_GLOBAL__N_123AwaitPromisesInitCommonEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_9JSPromiseEEES8_NS4_INS0_10JSFunctionEEEb
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L645
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L645:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L678
.L628:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L679
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L669:
	.cfi_restore_state
	movq	40960(%rsi), %rax
	movl	$478, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L675:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC22(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L673:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L678:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L628
.L679:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22813:
	.size	_ZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL34Stats_Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"V8.Runtime_Runtime_AwaitPromisesInitOld"
	.section	.text._ZN2v88internalL34Stats_Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateE.isra.0:
.LFB22814:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L721
.L681:
	movq	_ZZN2v88internalL34Stats_Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateEE28trace_event_unique_atomic196(%rip), %rbx
	testq	%rbx, %rbx
	je	.L722
.L683:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L723
.L685:
	addl	$1, 41104(%r12)
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r14
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	testb	$1, %al
	jne	.L724
.L689:
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L722:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L725
.L684:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateEE28trace_event_unique_atomic196(%rip)
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L724:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L689
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rcx
	testb	$1, %al
	jne	.L726
.L690:
	leaq	.LC24(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L723:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L727
.L686:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L687
	movq	(%rdi), %rax
	call	*8(%rax)
.L687:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L688
	movq	(%rdi), %rax
	call	*8(%rax)
.L688:
	leaq	.LC27(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L726:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L690
	movq	-24(%r13), %rax
	leaq	-24(%r13), %r8
	testb	$1, %al
	jne	.L728
.L692:
	leaq	.LC25(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L728:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L692
	movq	-32(%r13), %rax
	testb	$1, %al
	jne	.L729
.L694:
	leaq	.LC26(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L729:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L694
	movq	%r8, -184(%rbp)
	testb	$-2, 43(%rax)
	jne	.L694
	cmpq	%rax, 112(%r12)
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rcx, -168(%rbp)
	sete	%r9b
	xorl	%esi, %esi
	movb	%r9b, -169(%rbp)
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	movzbl	-169(%rbp), %r9d
	movq	-184(%rbp), %r8
	movq	-168(%rbp), %rcx
	andl	$1, %r9d
	call	_ZN2v88internal12_GLOBAL__N_123AwaitPromisesInitCommonEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_9JSPromiseEEES8_NS4_INS0_10JSFunctionEEEb
	movq	(%rax), %r13
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L697
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L697:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L730
.L680:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L731
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L721:
	.cfi_restore_state
	movq	40960(%rsi), %rax
	movl	$479, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L727:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC27(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L725:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L730:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L680
.L731:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22814:
	.size	_ZN2v88internalL34Stats_Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal35Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE:
.LFB18612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L741
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %rbx
	movq	%rax, -56(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L742
.L734:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L742:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L734
	movq	41472(%rdx), %rax
	leaq	-8(%rsi), %r14
	movq	%rsi, %r15
	cmpb	$0, 8(%rax)
	jne	.L743
.L736:
	leaq	88(%r12), %rcx
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	movq	41472(%r12), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal5Debug15OnPromiseRejectENS0_6HandleINS0_6ObjectEEES4_@PLT
	movq	0(%r13), %rax
	testb	$4, 35(%rax)
	je	.L744
.L737:
	movq	-56(%rbp), %rax
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L732
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L732:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L744:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE@PLT
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L743:
	movq	%rdx, %rdi
	call	_ZN2v88internal7Isolate24GetPromiseOnStackOnThrowEv@PLT
	movq	%rax, %r15
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L741:
	addq	$24, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL41Stats_Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18612:
	.size	_ZN2v88internal35Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE, .-_ZN2v88internal35Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE:
.LFB18615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L751
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L747
.L748:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L747:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L748
	leaq	-8(%rsi), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE@PLT
	movq	%r13, 41088(%r12)
	movq	88(%r12), %r14
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L745
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L745:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L751:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18615:
	.size	_ZN2v88internal34Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateE:
.LFB18618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L758
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L754
.L755:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L754:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L755
	leaq	-8(%rsi), %rdx
	movl	$3, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE@PLT
	movq	%r13, 41088(%r12)
	movq	88(%r12), %r14
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L752
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L752:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L758:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL41Stats_Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18618:
	.size	_ZN2v88internal35Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateE, .-_ZN2v88internal35Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE:
.LFB18621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L767
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L768
.L761:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L768:
	movq	-1(%rax), %rdx
	cmpw	$1074, 11(%rdx)
	jne	.L761
	testb	$4, 35(%rax)
	jne	.L769
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate19ReportPromiseRejectENS0_6HandleINS0_9JSPromiseEEENS2_INS0_6ObjectEEENS_18PromiseRejectEventE@PLT
	movq	%r13, 41088(%r12)
	movq	88(%r12), %r14
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L759
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L759:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L767:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L769:
	.cfi_restore_state
	leaq	.LC17(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18621:
	.size	_ZN2v88internal27Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE:
.LFB18624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L785
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L786
.L772:
	leaq	.LC14(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L786:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L772
	movq	31(%rax), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L787
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L775:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal7Factory15NewCallableTaskENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7ContextEEE@PLT
	movq	0(%r13), %rdx
	movq	31(%rdx), %rdx
	movq	39(%rdx), %rdx
	movq	1967(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L777
	movq	(%rax), %rsi
	call	_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskENS0_9MicrotaskE@PLT
.L777:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L770
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L770:
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	.cfi_restore_state
	movq	%rbx, %rdx
	cmpq	41096(%r12), %rbx
	je	.L788
.L776:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L785:
	addq	$16, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L788:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L776
	.cfi_endproc
.LFE18624:
	.size	_ZN2v88internal24Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateE:
.LFB18627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L793
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	%rdx, %rdi
	movq	41096(%rdx), %rbx
	call	_ZN2v815MicrotasksScope17PerformCheckpointEPNS_7IsolateE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L789
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L789:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L793:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Stats_Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18627:
	.size	_ZN2v88internal34Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateE:
.LFB18630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L805
	addl	$1, 41104(%rdx)
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	movq	(%rsi), %rcx
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	-8(%rsi), %rdx
	cmpq	%rax, %rcx
	je	.L802
	movq	7(%rcx), %rcx
.L796:
	cmpq	%rdx, %rax
	je	.L803
	movq	7(%rdx), %rdi
.L797:
	call	*%rcx
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L806
	movq	88(%r12), %r13
.L799:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L794
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L794:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L806:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L805:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL34Stats_Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L802:
	.cfi_restore_state
	xorl	%ecx, %ecx
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L803:
	xorl	%edi, %edi
	jmp	.L797
	.cfi_endproc
.LFE18630:
	.size	_ZN2v88internal28Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_PromiseStatusEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_PromiseStatusEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_PromiseStatusEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_PromiseStatusEiPmPNS0_7IsolateE:
.LFB18633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L815
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L816
.L810:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L816:
	movq	-1(%rax), %rdx
	cmpw	$1074, 11(%rdx)
	jne	.L810
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9JSPromise6statusEv@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	salq	$32, %rax
	cmpq	41096(%r12), %rbx
	je	.L807
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
.L807:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L817
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L815:
	.cfi_restore_state
	movq	%rdx, %rsi
	call	_ZN2v88internalL27Stats_Runtime_PromiseStatusEiPmPNS0_7IsolateE.isra.0
	jmp	.L807
.L817:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18633:
	.size	_ZN2v88internal21Runtime_PromiseStatusEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_PromiseStatusEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateE:
.LFB18636:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L824
	movq	(%rsi), %rcx
	testb	$1, %cl
	jne	.L820
.L821:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L820:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rcx), %rax
	cmpw	$1074, 11(%rax)
	jne	.L821
	movslq	35(%rcx), %rax
	orl	$4, %eax
	salq	$32, %rax
	movq	%rax, 31(%rcx)
	movq	88(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L824:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL34Stats_Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18636:
	.size	_ZN2v88internal28Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_PromiseHookInitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_PromiseHookInitEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_PromiseHookInitEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_PromiseHookInitEiPmPNS0_7IsolateE:
.LFB18639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L831
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L827
.L828:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L827:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L828
	leaq	-8(%rsi), %rcx
	movq	%rsi, %rdx
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, 41088(%r12)
	movq	88(%r12), %r14
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L825
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L825:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L831:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL29Stats_Runtime_PromiseHookInitEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18639:
	.size	_ZN2v88internal23Runtime_PromiseHookInitEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_PromiseHookInitEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE:
.LFB18643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L847
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L848
.L834:
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L848:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L834
	movq	-16(%rsi), %rax
	leaq	-16(%rsi), %rcx
	testb	$1, %al
	jne	.L849
.L835:
	leaq	.LC24(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L849:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L835
	movq	-24(%rsi), %rax
	leaq	-24(%rsi), %r8
	testb	$1, %al
	jne	.L850
.L837:
	leaq	.LC25(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L850:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L837
	movq	-32(%rsi), %rax
	testb	$1, %al
	jne	.L851
.L839:
	leaq	.LC26(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L851:
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	jne	.L839
	testb	$-2, 43(%rax)
	jne	.L839
	xorl	%r9d, %r9d
	cmpq	%rax, 112(%r12)
	movq	%r10, %rsi
	movq	%r12, %rdi
	sete	%r9b
	call	_ZN2v88internal12_GLOBAL__N_123AwaitPromisesInitCommonEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_9JSPromiseEEES8_NS4_INS0_10JSFunctionEEEb
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L832
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L832:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L847:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r10, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18643:
	.size	_ZN2v88internal25Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_AwaitPromisesInitEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateE:
.LFB18646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L867
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r13
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L868
.L854:
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L868:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L854
	movq	-16(%rsi), %rax
	leaq	-16(%rsi), %rcx
	testb	$1, %al
	jne	.L869
.L855:
	leaq	.LC24(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L869:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L855
	movq	-24(%rsi), %rax
	leaq	-24(%rsi), %r8
	testb	$1, %al
	jne	.L870
.L857:
	leaq	.LC25(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L870:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L857
	movq	-32(%rsi), %rax
	testb	$1, %al
	jne	.L871
.L859:
	leaq	.LC26(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L871:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L859
	movq	%r8, -72(%rbp)
	testb	$-2, 43(%rax)
	jne	.L859
	cmpq	%rax, 112(%r12)
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	sete	%r9b
	xorl	%esi, %esi
	movb	%r9b, -57(%rbp)
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	movzbl	-57(%rbp), %r9d
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %rcx
	movq	%r12, %rdi
	andl	$1, %r9d
	call	_ZN2v88internal12_GLOBAL__N_123AwaitPromisesInitCommonEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_9JSPromiseEEES8_NS4_INS0_10JSFunctionEEEb
	movq	(%rax), %r13
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L852
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L852:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L867:
	.cfi_restore_state
	addq	$40, %rsp
	movq	%r14, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL34Stats_Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18646:
	.size	_ZN2v88internal28Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE:
.LFB18649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L884
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L885
.L874:
	leaq	.LC20(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L885:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L874
	movq	-1(%rax), %rdx
	cmpw	$1074, 11(%rdx)
	je	.L886
.L879:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L872
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L872:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L886:
	.cfi_restore_state
	movq	41472(%r12), %rdx
	cmpb	$0, 8(%rdx)
	jne	.L887
.L878:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L879
	leaq	88(%r12), %rcx
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L887:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	0(%r13), %rax
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L884:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rdx, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18649:
	.size	_ZN2v88internal25Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_PromiseHookBeforeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_PromiseHookAfterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_PromiseHookAfterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_PromiseHookAfterEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_PromiseHookAfterEiPmPNS0_7IsolateE:
.LFB18652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L900
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L901
.L890:
	leaq	.LC20(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L901:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L890
	movq	-1(%rax), %rdx
	cmpw	$1074, 11(%rdx)
	je	.L902
.L895:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L888
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L888:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L902:
	.cfi_restore_state
	movq	41472(%r12), %rdx
	cmpb	$0, 8(%rdx)
	jne	.L903
.L894:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L895
	leaq	88(%r12), %rcx
	movq	%r13, %rdx
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L903:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10PopPromiseEv@PLT
	movq	0(%r13), %rax
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L900:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rdx, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL30Stats_Runtime_PromiseHookAfterEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18652:
	.size	_ZN2v88internal24Runtime_PromiseHookAfterEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_PromiseHookAfterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_RejectPromiseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_RejectPromiseEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_RejectPromiseEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_RejectPromiseEiPmPNS0_7IsolateE:
.LFB18655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L914
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L907
.L908:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L907:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L908
	movq	-16(%rsi), %rax
	leaq	-8(%rsi), %r15
	testb	$1, %al
	jne	.L915
.L909:
	leaq	.LC12(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L915:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L909
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movzbl	%al, %edx
	call	_ZN2v88internal9JSPromise6RejectENS0_6HandleIS1_EENS2_INS0_6ObjectEEEb@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L904
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L904:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L916
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L914:
	.cfi_restore_state
	call	_ZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L904
.L916:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18655:
	.size	_ZN2v88internal21Runtime_RejectPromiseEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_RejectPromiseEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_ResolvePromiseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_ResolvePromiseEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_ResolvePromiseEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_ResolvePromiseEiPmPNS0_7IsolateE:
.LFB18658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L926
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	testb	$1, %al
	jne	.L919
.L920:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L919:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L920
	leaq	-8(%rsi), %rsi
	call	_ZN2v88internal9JSPromise7ResolveENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L927
	movq	(%rax), %r14
.L922:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L917
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L917:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L927:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L926:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_ResolvePromiseEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18658:
	.size	_ZN2v88internal22Runtime_ResolvePromiseEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_ResolvePromiseEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal35Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal35Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal35Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE:
.LFB22773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22773:
	.size	_GLOBAL__sub_I__ZN2v88internal35Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal35Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal35Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL28Stats_Runtime_ResolvePromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic252,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_ResolvePromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic252, @object
	.size	_ZZN2v88internalL28Stats_Runtime_ResolvePromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic252, 8
_ZZN2v88internalL28Stats_Runtime_ResolvePromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic252:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic242,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic242, @object
	.size	_ZZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic242, 8
_ZZN2v88internalL27Stats_Runtime_RejectPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic242:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_PromiseHookAfterEiPmPNS0_7IsolateEE28trace_event_unique_atomic227,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_PromiseHookAfterEiPmPNS0_7IsolateEE28trace_event_unique_atomic227, @object
	.size	_ZZN2v88internalL30Stats_Runtime_PromiseHookAfterEiPmPNS0_7IsolateEE28trace_event_unique_atomic227, 8
_ZZN2v88internalL30Stats_Runtime_PromiseHookAfterEiPmPNS0_7IsolateEE28trace_event_unique_atomic227:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateEE28trace_event_unique_atomic212,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateEE28trace_event_unique_atomic212, @object
	.size	_ZZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateEE28trace_event_unique_atomic212, 8
_ZZN2v88internalL31Stats_Runtime_PromiseHookBeforeEiPmPNS0_7IsolateEE28trace_event_unique_atomic212:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateEE28trace_event_unique_atomic196,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateEE28trace_event_unique_atomic196, @object
	.size	_ZZN2v88internalL34Stats_Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateEE28trace_event_unique_atomic196, 8
_ZZN2v88internalL34Stats_Runtime_AwaitPromisesInitOldEiPmPNS0_7IsolateEE28trace_event_unique_atomic196:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic184,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic184, @object
	.size	_ZZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic184, 8
_ZZN2v88internalL31Stats_Runtime_AwaitPromisesInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic184:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_PromiseHookInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic124,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_PromiseHookInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic124, @object
	.size	_ZZN2v88internalL29Stats_Runtime_PromiseHookInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic124, 8
_ZZN2v88internalL29Stats_Runtime_PromiseHookInitEiPmPNS0_7IsolateEE28trace_event_unique_atomic124:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateEE28trace_event_unique_atomic115,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateEE28trace_event_unique_atomic115, @object
	.size	_ZZN2v88internalL34Stats_Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateEE28trace_event_unique_atomic115, 8
_ZZN2v88internalL34Stats_Runtime_PromiseMarkAsHandledEiPmPNS0_7IsolateEE28trace_event_unique_atomic115:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_PromiseStatusEiPmPNS0_7IsolateEE28trace_event_unique_atomic107,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_PromiseStatusEiPmPNS0_7IsolateEE28trace_event_unique_atomic107, @object
	.size	_ZZN2v88internalL27Stats_Runtime_PromiseStatusEiPmPNS0_7IsolateEE28trace_event_unique_atomic107, 8
_ZZN2v88internalL27Stats_Runtime_PromiseStatusEiPmPNS0_7IsolateEE28trace_event_unique_atomic107:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateEE27trace_event_unique_atomic95,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateEE27trace_event_unique_atomic95, @object
	.size	_ZZN2v88internalL34Stats_Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateEE27trace_event_unique_atomic95, 8
_ZZN2v88internalL34Stats_Runtime_RunMicrotaskCallbackEiPmPNS0_7IsolateEE27trace_event_unique_atomic95:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateEE27trace_event_unique_atomic88,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateEE27trace_event_unique_atomic88, @object
	.size	_ZZN2v88internalL40Stats_Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateEE27trace_event_unique_atomic88, 8
_ZZN2v88internalL40Stats_Runtime_PerformMicrotaskCheckpointEiPmPNS0_7IsolateEE27trace_event_unique_atomic88:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateEE27trace_event_unique_atomic75,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateEE27trace_event_unique_atomic75, @object
	.size	_ZZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateEE27trace_event_unique_atomic75, 8
_ZZN2v88internalL30Stats_Runtime_EnqueueMicrotaskEiPmPNS0_7IsolateEE27trace_event_unique_atomic75:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateEE27trace_event_unique_atomic64,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateEE27trace_event_unique_atomic64, @object
	.size	_ZZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateEE27trace_event_unique_atomic64, 8
_ZZN2v88internalL33Stats_Runtime_PromiseRevokeRejectEiPmPNS0_7IsolateEE27trace_event_unique_atomic64:
	.zero	8
	.section	.bss._ZZN2v88internalL41Stats_Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateEE27trace_event_unique_atomic54,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Stats_Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateEE27trace_event_unique_atomic54, @object
	.size	_ZZN2v88internalL41Stats_Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateEE27trace_event_unique_atomic54, 8
_ZZN2v88internalL41Stats_Runtime_PromiseResolveAfterResolvedEiPmPNS0_7IsolateEE27trace_event_unique_atomic54:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateEE27trace_event_unique_atomic44,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateEE27trace_event_unique_atomic44, @object
	.size	_ZZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateEE27trace_event_unique_atomic44, 8
_ZZN2v88internalL40Stats_Runtime_PromiseRejectAfterResolvedEiPmPNS0_7IsolateEE27trace_event_unique_atomic44:
	.zero	8
	.section	.bss._ZZN2v88internalL41Stats_Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateEE27trace_event_unique_atomic20,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Stats_Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateEE27trace_event_unique_atomic20, @object
	.size	_ZZN2v88internalL41Stats_Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateEE27trace_event_unique_atomic20, 8
_ZZN2v88internalL41Stats_Runtime_PromiseRejectEventFromStackEiPmPNS0_7IsolateEE27trace_event_unique_atomic20:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
