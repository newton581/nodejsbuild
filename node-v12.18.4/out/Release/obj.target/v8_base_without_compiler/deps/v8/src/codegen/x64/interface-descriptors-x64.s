	.file	"interface-descriptors-x64.cc"
	.text
	.section	.text._ZN2v88internal28RunMicrotasksEntryDescriptorD2Ev,"axG",@progbits,_ZN2v88internal28RunMicrotasksEntryDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal28RunMicrotasksEntryDescriptorD2Ev
	.type	_ZN2v88internal28RunMicrotasksEntryDescriptorD2Ev, @function
_ZN2v88internal28RunMicrotasksEntryDescriptorD2Ev:
.LFB12719:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12719:
	.size	_ZN2v88internal28RunMicrotasksEntryDescriptorD2Ev, .-_ZN2v88internal28RunMicrotasksEntryDescriptorD2Ev
	.weak	_ZN2v88internal28RunMicrotasksEntryDescriptorD1Ev
	.set	_ZN2v88internal28RunMicrotasksEntryDescriptorD1Ev,_ZN2v88internal28RunMicrotasksEntryDescriptorD2Ev
	.section	.text._ZN2v88internal32FrameDropperTrampolineDescriptorD2Ev,"axG",@progbits,_ZN2v88internal32FrameDropperTrampolineDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal32FrameDropperTrampolineDescriptorD2Ev
	.type	_ZN2v88internal32FrameDropperTrampolineDescriptorD2Ev, @function
_ZN2v88internal32FrameDropperTrampolineDescriptorD2Ev:
.LFB12723:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12723:
	.size	_ZN2v88internal32FrameDropperTrampolineDescriptorD2Ev, .-_ZN2v88internal32FrameDropperTrampolineDescriptorD2Ev
	.weak	_ZN2v88internal32FrameDropperTrampolineDescriptorD1Ev
	.set	_ZN2v88internal32FrameDropperTrampolineDescriptorD1Ev,_ZN2v88internal32FrameDropperTrampolineDescriptorD2Ev
	.section	.text._ZN2v88internal25ResumeGeneratorDescriptorD2Ev,"axG",@progbits,_ZN2v88internal25ResumeGeneratorDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25ResumeGeneratorDescriptorD2Ev
	.type	_ZN2v88internal25ResumeGeneratorDescriptorD2Ev, @function
_ZN2v88internal25ResumeGeneratorDescriptorD2Ev:
.LFB12727:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12727:
	.size	_ZN2v88internal25ResumeGeneratorDescriptorD2Ev, .-_ZN2v88internal25ResumeGeneratorDescriptorD2Ev
	.weak	_ZN2v88internal25ResumeGeneratorDescriptorD1Ev
	.set	_ZN2v88internal25ResumeGeneratorDescriptorD1Ev,_ZN2v88internal25ResumeGeneratorDescriptorD2Ev
	.section	.text._ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD2Ev,"axG",@progbits,_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD2Ev
	.type	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD2Ev, @function
_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD2Ev:
.LFB12731:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12731:
	.size	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD2Ev, .-_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD2Ev
	.weak	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD1Ev
	.set	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD1Ev,_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD2Ev
	.section	.text._ZN2v88internal37InterpreterPushArgsThenCallDescriptorD2Ev,"axG",@progbits,_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD2Ev
	.type	_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD2Ev, @function
_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD2Ev:
.LFB12735:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12735:
	.size	_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD2Ev, .-_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD2Ev
	.weak	_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD1Ev
	.set	_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD1Ev,_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD2Ev
	.section	.text._ZN2v88internal29InterpreterDispatchDescriptorD2Ev,"axG",@progbits,_ZN2v88internal29InterpreterDispatchDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29InterpreterDispatchDescriptorD2Ev
	.type	_ZN2v88internal29InterpreterDispatchDescriptorD2Ev, @function
_ZN2v88internal29InterpreterDispatchDescriptorD2Ev:
.LFB12739:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12739:
	.size	_ZN2v88internal29InterpreterDispatchDescriptorD2Ev, .-_ZN2v88internal29InterpreterDispatchDescriptorD2Ev
	.weak	_ZN2v88internal29InterpreterDispatchDescriptorD1Ev
	.set	_ZN2v88internal29InterpreterDispatchDescriptorD1Ev,_ZN2v88internal29InterpreterDispatchDescriptorD2Ev
	.section	.text._ZN2v88internal21ApiCallbackDescriptorD2Ev,"axG",@progbits,_ZN2v88internal21ApiCallbackDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21ApiCallbackDescriptorD2Ev
	.type	_ZN2v88internal21ApiCallbackDescriptorD2Ev, @function
_ZN2v88internal21ApiCallbackDescriptorD2Ev:
.LFB12743:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12743:
	.size	_ZN2v88internal21ApiCallbackDescriptorD2Ev, .-_ZN2v88internal21ApiCallbackDescriptorD2Ev
	.weak	_ZN2v88internal21ApiCallbackDescriptorD1Ev
	.set	_ZN2v88internal21ApiCallbackDescriptorD1Ev,_ZN2v88internal21ApiCallbackDescriptorD2Ev
	.section	.text._ZN2v88internal26ArgumentsAdaptorDescriptorD2Ev,"axG",@progbits,_ZN2v88internal26ArgumentsAdaptorDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ArgumentsAdaptorDescriptorD2Ev
	.type	_ZN2v88internal26ArgumentsAdaptorDescriptorD2Ev, @function
_ZN2v88internal26ArgumentsAdaptorDescriptorD2Ev:
.LFB12747:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12747:
	.size	_ZN2v88internal26ArgumentsAdaptorDescriptorD2Ev, .-_ZN2v88internal26ArgumentsAdaptorDescriptorD2Ev
	.weak	_ZN2v88internal26ArgumentsAdaptorDescriptorD1Ev
	.set	_ZN2v88internal26ArgumentsAdaptorDescriptorD1Ev,_ZN2v88internal26ArgumentsAdaptorDescriptorD2Ev
	.section	.text._ZN2v88internal18BinaryOpDescriptorD2Ev,"axG",@progbits,_ZN2v88internal18BinaryOpDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18BinaryOpDescriptorD2Ev
	.type	_ZN2v88internal18BinaryOpDescriptorD2Ev, @function
_ZN2v88internal18BinaryOpDescriptorD2Ev:
.LFB12751:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12751:
	.size	_ZN2v88internal18BinaryOpDescriptorD2Ev, .-_ZN2v88internal18BinaryOpDescriptorD2Ev
	.weak	_ZN2v88internal18BinaryOpDescriptorD1Ev
	.set	_ZN2v88internal18BinaryOpDescriptorD1Ev,_ZN2v88internal18BinaryOpDescriptorD2Ev
	.section	.text._ZN2v88internal17CompareDescriptorD2Ev,"axG",@progbits,_ZN2v88internal17CompareDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17CompareDescriptorD2Ev
	.type	_ZN2v88internal17CompareDescriptorD2Ev, @function
_ZN2v88internal17CompareDescriptorD2Ev:
.LFB12755:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12755:
	.size	_ZN2v88internal17CompareDescriptorD2Ev, .-_ZN2v88internal17CompareDescriptorD2Ev
	.weak	_ZN2v88internal17CompareDescriptorD1Ev
	.set	_ZN2v88internal17CompareDescriptorD1Ev,_ZN2v88internal17CompareDescriptorD2Ev
	.section	.text._ZN2v88internal28AllocateHeapNumberDescriptorD2Ev,"axG",@progbits,_ZN2v88internal28AllocateHeapNumberDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal28AllocateHeapNumberDescriptorD2Ev
	.type	_ZN2v88internal28AllocateHeapNumberDescriptorD2Ev, @function
_ZN2v88internal28AllocateHeapNumberDescriptorD2Ev:
.LFB12759:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12759:
	.size	_ZN2v88internal28AllocateHeapNumberDescriptorD2Ev, .-_ZN2v88internal28AllocateHeapNumberDescriptorD2Ev
	.weak	_ZN2v88internal28AllocateHeapNumberDescriptorD1Ev
	.set	_ZN2v88internal28AllocateHeapNumberDescriptorD1Ev,_ZN2v88internal28AllocateHeapNumberDescriptorD2Ev
	.section	.text._ZN2v88internal15AbortDescriptorD2Ev,"axG",@progbits,_ZN2v88internal15AbortDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15AbortDescriptorD2Ev
	.type	_ZN2v88internal15AbortDescriptorD2Ev, @function
_ZN2v88internal15AbortDescriptorD2Ev:
.LFB12763:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12763:
	.size	_ZN2v88internal15AbortDescriptorD2Ev, .-_ZN2v88internal15AbortDescriptorD2Ev
	.weak	_ZN2v88internal15AbortDescriptorD1Ev
	.set	_ZN2v88internal15AbortDescriptorD1Ev,_ZN2v88internal15AbortDescriptorD2Ev
	.section	.text._ZN2v88internal23ConstructStubDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23ConstructStubDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23ConstructStubDescriptorD2Ev
	.type	_ZN2v88internal23ConstructStubDescriptorD2Ev, @function
_ZN2v88internal23ConstructStubDescriptorD2Ev:
.LFB12767:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12767:
	.size	_ZN2v88internal23ConstructStubDescriptorD2Ev, .-_ZN2v88internal23ConstructStubDescriptorD2Ev
	.weak	_ZN2v88internal23ConstructStubDescriptorD1Ev
	.set	_ZN2v88internal23ConstructStubDescriptorD1Ev,_ZN2v88internal23ConstructStubDescriptorD2Ev
	.section	.text._ZN2v88internal32ConstructWithArrayLikeDescriptorD2Ev,"axG",@progbits,_ZN2v88internal32ConstructWithArrayLikeDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal32ConstructWithArrayLikeDescriptorD2Ev
	.type	_ZN2v88internal32ConstructWithArrayLikeDescriptorD2Ev, @function
_ZN2v88internal32ConstructWithArrayLikeDescriptorD2Ev:
.LFB12771:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12771:
	.size	_ZN2v88internal32ConstructWithArrayLikeDescriptorD2Ev, .-_ZN2v88internal32ConstructWithArrayLikeDescriptorD2Ev
	.weak	_ZN2v88internal32ConstructWithArrayLikeDescriptorD1Ev
	.set	_ZN2v88internal32ConstructWithArrayLikeDescriptorD1Ev,_ZN2v88internal32ConstructWithArrayLikeDescriptorD2Ev
	.section	.text._ZN2v88internal29ConstructWithSpreadDescriptorD2Ev,"axG",@progbits,_ZN2v88internal29ConstructWithSpreadDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29ConstructWithSpreadDescriptorD2Ev
	.type	_ZN2v88internal29ConstructWithSpreadDescriptorD2Ev, @function
_ZN2v88internal29ConstructWithSpreadDescriptorD2Ev:
.LFB12775:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12775:
	.size	_ZN2v88internal29ConstructWithSpreadDescriptorD2Ev, .-_ZN2v88internal29ConstructWithSpreadDescriptorD2Ev
	.weak	_ZN2v88internal29ConstructWithSpreadDescriptorD1Ev
	.set	_ZN2v88internal29ConstructWithSpreadDescriptorD1Ev,_ZN2v88internal29ConstructWithSpreadDescriptorD2Ev
	.section	.text._ZN2v88internal33ConstructForwardVarargsDescriptorD2Ev,"axG",@progbits,_ZN2v88internal33ConstructForwardVarargsDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal33ConstructForwardVarargsDescriptorD2Ev
	.type	_ZN2v88internal33ConstructForwardVarargsDescriptorD2Ev, @function
_ZN2v88internal33ConstructForwardVarargsDescriptorD2Ev:
.LFB12779:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12779:
	.size	_ZN2v88internal33ConstructForwardVarargsDescriptorD2Ev, .-_ZN2v88internal33ConstructForwardVarargsDescriptorD2Ev
	.weak	_ZN2v88internal33ConstructForwardVarargsDescriptorD1Ev
	.set	_ZN2v88internal33ConstructForwardVarargsDescriptorD1Ev,_ZN2v88internal33ConstructForwardVarargsDescriptorD2Ev
	.section	.text._ZN2v88internal26ConstructVarargsDescriptorD2Ev,"axG",@progbits,_ZN2v88internal26ConstructVarargsDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ConstructVarargsDescriptorD2Ev
	.type	_ZN2v88internal26ConstructVarargsDescriptorD2Ev, @function
_ZN2v88internal26ConstructVarargsDescriptorD2Ev:
.LFB12783:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12783:
	.size	_ZN2v88internal26ConstructVarargsDescriptorD2Ev, .-_ZN2v88internal26ConstructVarargsDescriptorD2Ev
	.weak	_ZN2v88internal26ConstructVarargsDescriptorD1Ev
	.set	_ZN2v88internal26ConstructVarargsDescriptorD1Ev,_ZN2v88internal26ConstructVarargsDescriptorD2Ev
	.section	.text._ZN2v88internal27CallWithArrayLikeDescriptorD2Ev,"axG",@progbits,_ZN2v88internal27CallWithArrayLikeDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal27CallWithArrayLikeDescriptorD2Ev
	.type	_ZN2v88internal27CallWithArrayLikeDescriptorD2Ev, @function
_ZN2v88internal27CallWithArrayLikeDescriptorD2Ev:
.LFB12787:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12787:
	.size	_ZN2v88internal27CallWithArrayLikeDescriptorD2Ev, .-_ZN2v88internal27CallWithArrayLikeDescriptorD2Ev
	.weak	_ZN2v88internal27CallWithArrayLikeDescriptorD1Ev
	.set	_ZN2v88internal27CallWithArrayLikeDescriptorD1Ev,_ZN2v88internal27CallWithArrayLikeDescriptorD2Ev
	.section	.text._ZN2v88internal24CallWithSpreadDescriptorD2Ev,"axG",@progbits,_ZN2v88internal24CallWithSpreadDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24CallWithSpreadDescriptorD2Ev
	.type	_ZN2v88internal24CallWithSpreadDescriptorD2Ev, @function
_ZN2v88internal24CallWithSpreadDescriptorD2Ev:
.LFB12791:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12791:
	.size	_ZN2v88internal24CallWithSpreadDescriptorD2Ev, .-_ZN2v88internal24CallWithSpreadDescriptorD2Ev
	.weak	_ZN2v88internal24CallWithSpreadDescriptorD1Ev
	.set	_ZN2v88internal24CallWithSpreadDescriptorD1Ev,_ZN2v88internal24CallWithSpreadDescriptorD2Ev
	.section	.text._ZN2v88internal30CallFunctionTemplateDescriptorD2Ev,"axG",@progbits,_ZN2v88internal30CallFunctionTemplateDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30CallFunctionTemplateDescriptorD2Ev
	.type	_ZN2v88internal30CallFunctionTemplateDescriptorD2Ev, @function
_ZN2v88internal30CallFunctionTemplateDescriptorD2Ev:
.LFB12795:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12795:
	.size	_ZN2v88internal30CallFunctionTemplateDescriptorD2Ev, .-_ZN2v88internal30CallFunctionTemplateDescriptorD2Ev
	.weak	_ZN2v88internal30CallFunctionTemplateDescriptorD1Ev
	.set	_ZN2v88internal30CallFunctionTemplateDescriptorD1Ev,_ZN2v88internal30CallFunctionTemplateDescriptorD2Ev
	.section	.text._ZN2v88internal28CallForwardVarargsDescriptorD2Ev,"axG",@progbits,_ZN2v88internal28CallForwardVarargsDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal28CallForwardVarargsDescriptorD2Ev
	.type	_ZN2v88internal28CallForwardVarargsDescriptorD2Ev, @function
_ZN2v88internal28CallForwardVarargsDescriptorD2Ev:
.LFB12799:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12799:
	.size	_ZN2v88internal28CallForwardVarargsDescriptorD2Ev, .-_ZN2v88internal28CallForwardVarargsDescriptorD2Ev
	.weak	_ZN2v88internal28CallForwardVarargsDescriptorD1Ev
	.set	_ZN2v88internal28CallForwardVarargsDescriptorD1Ev,_ZN2v88internal28CallForwardVarargsDescriptorD2Ev
	.section	.text._ZN2v88internal21CallVarargsDescriptorD2Ev,"axG",@progbits,_ZN2v88internal21CallVarargsDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21CallVarargsDescriptorD2Ev
	.type	_ZN2v88internal21CallVarargsDescriptorD2Ev, @function
_ZN2v88internal21CallVarargsDescriptorD2Ev:
.LFB12803:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12803:
	.size	_ZN2v88internal21CallVarargsDescriptorD2Ev, .-_ZN2v88internal21CallVarargsDescriptorD2Ev
	.weak	_ZN2v88internal21CallVarargsDescriptorD1Ev
	.set	_ZN2v88internal21CallVarargsDescriptorD1Ev,_ZN2v88internal21CallVarargsDescriptorD2Ev
	.section	.text._ZN2v88internal24CallTrampolineDescriptorD2Ev,"axG",@progbits,_ZN2v88internal24CallTrampolineDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24CallTrampolineDescriptorD2Ev
	.type	_ZN2v88internal24CallTrampolineDescriptorD2Ev, @function
_ZN2v88internal24CallTrampolineDescriptorD2Ev:
.LFB12807:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12807:
	.size	_ZN2v88internal24CallTrampolineDescriptorD2Ev, .-_ZN2v88internal24CallTrampolineDescriptorD2Ev
	.weak	_ZN2v88internal24CallTrampolineDescriptorD1Ev
	.set	_ZN2v88internal24CallTrampolineDescriptorD1Ev,_ZN2v88internal24CallTrampolineDescriptorD2Ev
	.section	.text._ZN2v88internal16TypeofDescriptorD2Ev,"axG",@progbits,_ZN2v88internal16TypeofDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16TypeofDescriptorD2Ev
	.type	_ZN2v88internal16TypeofDescriptorD2Ev, @function
_ZN2v88internal16TypeofDescriptorD2Ev:
.LFB12811:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12811:
	.size	_ZN2v88internal16TypeofDescriptorD2Ev, .-_ZN2v88internal16TypeofDescriptorD2Ev
	.weak	_ZN2v88internal16TypeofDescriptorD1Ev
	.set	_ZN2v88internal16TypeofDescriptorD1Ev,_ZN2v88internal16TypeofDescriptorD2Ev
	.section	.text._ZN2v88internal29EphemeronKeyBarrierDescriptorD2Ev,"axG",@progbits,_ZN2v88internal29EphemeronKeyBarrierDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29EphemeronKeyBarrierDescriptorD2Ev
	.type	_ZN2v88internal29EphemeronKeyBarrierDescriptorD2Ev, @function
_ZN2v88internal29EphemeronKeyBarrierDescriptorD2Ev:
.LFB12815:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12815:
	.size	_ZN2v88internal29EphemeronKeyBarrierDescriptorD2Ev, .-_ZN2v88internal29EphemeronKeyBarrierDescriptorD2Ev
	.weak	_ZN2v88internal29EphemeronKeyBarrierDescriptorD1Ev
	.set	_ZN2v88internal29EphemeronKeyBarrierDescriptorD1Ev,_ZN2v88internal29EphemeronKeyBarrierDescriptorD2Ev
	.section	.text._ZN2v88internal21RecordWriteDescriptorD2Ev,"axG",@progbits,_ZN2v88internal21RecordWriteDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21RecordWriteDescriptorD2Ev
	.type	_ZN2v88internal21RecordWriteDescriptorD2Ev, @function
_ZN2v88internal21RecordWriteDescriptorD2Ev:
.LFB12819:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12819:
	.size	_ZN2v88internal21RecordWriteDescriptorD2Ev, .-_ZN2v88internal21RecordWriteDescriptorD2Ev
	.weak	_ZN2v88internal21RecordWriteDescriptorD1Ev
	.set	_ZN2v88internal21RecordWriteDescriptorD1Ev,_ZN2v88internal21RecordWriteDescriptorD2Ev
	.section	.text._ZN2v88internal16TypeofDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16TypeofDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal16TypeofDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal16TypeofDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rdx
	movl	$3, -12(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L31:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11424:
	.size	_ZN2v88internal16TypeofDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal16TypeofDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.globl	_ZN2v88internal32FrameDropperTrampolineDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.set	_ZN2v88internal32FrameDropperTrampolineDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,_ZN2v88internal16TypeofDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal24CallTrampolineDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24CallTrampolineDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal24CallTrampolineDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal24CallTrampolineDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rdx
	movq	$7, -16(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L35
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L35:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11426:
	.size	_ZN2v88internal24CallTrampolineDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal24CallTrampolineDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal21CallVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21CallVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal21CallVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal21CallVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movdqa	.LC0(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdx
	movaps	%xmm0, -32(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L39:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11427:
	.size	_ZN2v88internal21CallVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal21CallVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal28CallForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28CallForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal28CallForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal28CallForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-20(%rbp), %rdx
	movl	$1, -12(%rbp)
	movq	$7, -20(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L43:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11428:
	.size	_ZN2v88internal28CallForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal28CallForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal30CallFunctionTemplateDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30CallFunctionTemplateDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal30CallFunctionTemplateDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal30CallFunctionTemplateDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rdx
	movabsq	$4294967298, %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L47:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11429:
	.size	_ZN2v88internal30CallFunctionTemplateDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal30CallFunctionTemplateDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal24CallWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24CallWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal24CallWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal24CallWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-20(%rbp), %rdx
	movl	$3, -12(%rbp)
	movq	$7, -20(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L51:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11430:
	.size	_ZN2v88internal24CallWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal24CallWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal27CallWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27CallWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal27CallWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal27CallWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rdx
	movabsq	$12884901895, %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11431:
	.size	_ZN2v88internal27CallWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal27CallWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal26ConstructVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ConstructVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal26ConstructVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal26ConstructVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movdqa	.LC1(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdx
	movl	$3, -16(%rbp)
	movaps	%xmm0, -32(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L59:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11432:
	.size	_ZN2v88internal26ConstructVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal26ConstructVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal33ConstructForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33ConstructForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal33ConstructForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal33ConstructForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movdqa	.LC1(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdx
	movaps	%xmm0, -32(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L63:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11433:
	.size	_ZN2v88internal33ConstructForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal33ConstructForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal29ConstructWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29ConstructWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal29ConstructWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal29ConstructWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movdqa	.LC2(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdx
	movaps	%xmm0, -32(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L67:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11434:
	.size	_ZN2v88internal29ConstructWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal29ConstructWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.globl	_ZN2v88internal26ArgumentsAdaptorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.set	_ZN2v88internal26ArgumentsAdaptorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,_ZN2v88internal29ConstructWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.globl	_ZN2v88internal23ConstructStubDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.set	_ZN2v88internal23ConstructStubDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,_ZN2v88internal29ConstructWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal32ConstructWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32ConstructWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal32ConstructWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal32ConstructWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-20(%rbp), %rdx
	movl	$3, -12(%rbp)
	movabsq	$8589934599, %rax
	movq	%rax, -20(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L71:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11435:
	.size	_ZN2v88internal32ConstructWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal32ConstructWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal15AbortDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15AbortDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal15AbortDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal15AbortDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rdx
	movl	$2, -12(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L75:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11437:
	.size	_ZN2v88internal15AbortDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal15AbortDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal28AllocateHeapNumberDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28AllocateHeapNumberDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal28AllocateHeapNumberDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal28AllocateHeapNumberDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11438:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	.cfi_endproc
.LFE11438:
	.size	_ZN2v88internal28AllocateHeapNumberDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal28AllocateHeapNumberDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal17CompareDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17CompareDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal17CompareDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal17CompareDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rdx
	movq	$2, -16(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L80:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11439:
	.size	_ZN2v88internal17CompareDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal17CompareDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.globl	_ZN2v88internal18BinaryOpDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.set	_ZN2v88internal18BinaryOpDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,_ZN2v88internal17CompareDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal21ApiCallbackDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ApiCallbackDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal21ApiCallbackDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal21ApiCallbackDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movdqa	.LC3(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdx
	movaps	%xmm0, -32(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L84:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11442:
	.size	_ZN2v88internal21ApiCallbackDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal21ApiCallbackDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal29InterpreterDispatchDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29InterpreterDispatchDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal29InterpreterDispatchDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal29InterpreterDispatchDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movdqa	.LC4(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdx
	movaps	%xmm0, -32(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L88:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11443:
	.size	_ZN2v88internal29InterpreterDispatchDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal29InterpreterDispatchDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal37InterpreterPushArgsThenCallDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal37InterpreterPushArgsThenCallDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal37InterpreterPushArgsThenCallDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal37InterpreterPushArgsThenCallDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-20(%rbp), %rdx
	movl	$7, -12(%rbp)
	movabsq	$12884901888, %rax
	movq	%rax, -20(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L92:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11444:
	.size	_ZN2v88internal37InterpreterPushArgsThenCallDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal37InterpreterPushArgsThenCallDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal42InterpreterPushArgsThenConstructDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal42InterpreterPushArgsThenConstructDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movdqa	.LC5(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdx
	movl	$3, -16(%rbp)
	movaps	%xmm0, -32(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L96:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11445:
	.size	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal42InterpreterPushArgsThenConstructDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal25ResumeGeneratorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ResumeGeneratorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal25ResumeGeneratorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal25ResumeGeneratorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rdx
	movabsq	$8589934592, %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L100:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11446:
	.size	_ZN2v88internal25ResumeGeneratorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal25ResumeGeneratorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal28RunMicrotasksEntryDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28RunMicrotasksEntryDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal28RunMicrotasksEntryDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal28RunMicrotasksEntryDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rdx
	movabsq	$25769803783, %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L104:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11448:
	.size	_ZN2v88internal28RunMicrotasksEntryDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal28RunMicrotasksEntryDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal21RecordWriteDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21RecordWriteDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal21RecordWriteDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal21RecordWriteDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$128, %edx
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movdqa	.LC6(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	16(%rsi), %eax
	movl	$0, -16(%rbp)
	movaps	%xmm0, -32(%rbp)
	movl	-28(%rbp), %ecx
	orl	%edx, %eax
	cmpl	$-1, %ecx
	je	.L107
	movl	$1, %edx
	sall	%cl, %edx
	orl	%edx, %eax
.L107:
	movl	-24(%rbp), %ecx
	cmpl	$-1, %ecx
	je	.L108
	movl	$1, %edx
	sall	%cl, %edx
	orl	%edx, %eax
.L108:
	movl	-20(%rbp), %ecx
	cmpl	$-1, %ecx
	je	.L109
	movl	$1, %edx
	sall	%cl, %edx
	orl	%edx, %eax
.L109:
	orl	$1, %eax
	leaq	-32(%rbp), %rdx
	movl	$4, %esi
	movl	%eax, 16(%rdi)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L124:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11404:
	.size	_ZN2v88internal21RecordWriteDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal21RecordWriteDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal29EphemeronKeyBarrierDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29EphemeronKeyBarrierDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal29EphemeronKeyBarrierDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal29EphemeronKeyBarrierDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$128, %edx
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movdqa	.LC6(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	16(%rsi), %eax
	movl	$0, -16(%rbp)
	movaps	%xmm0, -32(%rbp)
	movl	-28(%rbp), %ecx
	orl	%edx, %eax
	cmpl	$-1, %ecx
	je	.L127
	movl	$1, %edx
	sall	%cl, %edx
	orl	%edx, %eax
.L127:
	movl	-24(%rbp), %ecx
	cmpl	$-1, %ecx
	je	.L128
	movl	$1, %edx
	sall	%cl, %edx
	orl	%edx, %eax
.L128:
	movl	-20(%rbp), %ecx
	cmpl	$-1, %ecx
	je	.L129
	movl	$1, %edx
	sall	%cl, %edx
	orl	%edx, %eax
.L129:
	orl	$1, %eax
	leaq	-32(%rbp), %rdx
	movl	$3, %esi
	movl	%eax, 16(%rdi)
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L144
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L144:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11405:
	.size	_ZN2v88internal29EphemeronKeyBarrierDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal29EphemeronKeyBarrierDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal28RunMicrotasksEntryDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal28RunMicrotasksEntryDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal28RunMicrotasksEntryDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal28RunMicrotasksEntryDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal28RunMicrotasksEntryDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$3, %r9d
	movl	$2, %ecx
	movl	$1, %edx
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-14(%rbp), %r8
	movl	$5, %eax
	movl	$329480, -14(%rbp)
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L149:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9270:
	.size	_ZN2v88internal28RunMicrotasksEntryDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal28RunMicrotasksEntryDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal32FrameDropperTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal32FrameDropperTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal32FrameDropperTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal32FrameDropperTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal32FrameDropperTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$2, %r9d
	xorl	%esi, %esi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %r8
	movl	$329480, -12(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L153:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9263:
	.size	_ZN2v88internal32FrameDropperTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal32FrameDropperTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal25ResumeGeneratorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal25ResumeGeneratorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25ResumeGeneratorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal25ResumeGeneratorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal25ResumeGeneratorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$3, %r9d
	xorl	%esi, %esi
	movl	$2, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-14(%rbp), %r8
	movl	$1800, %eax
	movl	$117966600, -14(%rbp)
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L157
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L157:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9256:
	.size	_ZN2v88internal25ResumeGeneratorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal25ResumeGeneratorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal42InterpreterPushArgsThenConstructDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal42InterpreterPushArgsThenConstructDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal42InterpreterPushArgsThenConstructDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$6, %r9d
	xorl	%esi, %esi
	movl	$5, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-20(%rbp), %r8
	movl	$117966600, -12(%rbp)
	movabsq	$506654979587835656, %rax
	movq	%rax, -20(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L161
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L161:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9235:
	.size	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal42InterpreterPushArgsThenConstructDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal37InterpreterPushArgsThenCallDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal37InterpreterPushArgsThenCallDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal37InterpreterPushArgsThenCallDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal37InterpreterPushArgsThenCallDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal37InterpreterPushArgsThenCallDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$4, %r9d
	xorl	%esi, %esi
	movl	$3, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %r8
	movabsq	$506654979587835656, %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L165:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9228:
	.size	_ZN2v88internal37InterpreterPushArgsThenCallDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal37InterpreterPushArgsThenCallDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal29InterpreterDispatchDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal29InterpreterDispatchDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29InterpreterDispatchDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal29InterpreterDispatchDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal29InterpreterDispatchDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$5, %r9d
	xorl	%esi, %esi
	movl	$4, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-18(%rbp), %r8
	movabsq	$506659377718494984, %rax
	movq	%rax, -18(%rbp)
	movl	$1029, %eax
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L169
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L169:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9221:
	.size	_ZN2v88internal29InterpreterDispatchDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal29InterpreterDispatchDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal21ApiCallbackDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal21ApiCallbackDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21ApiCallbackDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal21ApiCallbackDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal21ApiCallbackDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$5, %r9d
	xorl	%esi, %esi
	movl	$4, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-18(%rbp), %r8
	movabsq	$506659377600857864, %rax
	movq	%rax, -18(%rbp)
	movl	$1800, %eax
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L173
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L173:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9193:
	.size	_ZN2v88internal21ApiCallbackDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal21ApiCallbackDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal26ArgumentsAdaptorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal26ArgumentsAdaptorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ArgumentsAdaptorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal26ArgumentsAdaptorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal26ArgumentsAdaptorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$5, %r9d
	xorl	%esi, %esi
	movl	$4, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-18(%rbp), %r8
	movabsq	$145248819041797896, %rax
	movq	%rax, -18(%rbp)
	movl	$516, %eax
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L177
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L177:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9171:
	.size	_ZN2v88internal26ArgumentsAdaptorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal26ArgumentsAdaptorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal28AllocateHeapNumberDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal28AllocateHeapNumberDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal28AllocateHeapNumberDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal28AllocateHeapNumberDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal28AllocateHeapNumberDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	xorl	%ecx, %ecx
	movl	$1, %r9d
	movl	$1, %edx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-10(%rbp), %r8
	movl	$1800, %eax
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L181
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L181:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9098:
	.size	_ZN2v88internal28AllocateHeapNumberDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal28AllocateHeapNumberDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal15AbortDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal15AbortDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15AbortDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal15AbortDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal15AbortDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$2, %r9d
	movl	$1, %ecx
	movl	$1, %edx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %r8
	movl	$117966600, -12(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L185:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9091:
	.size	_ZN2v88internal15AbortDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal15AbortDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23ConstructStubDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23ConstructStubDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23ConstructStubDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23ConstructStubDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23ConstructStubDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$5, %r9d
	xorl	%esi, %esi
	movl	$4, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-18(%rbp), %r8
	movabsq	$145248819041797896, %rax
	movq	%rax, -18(%rbp)
	movl	$1800, %eax
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L189
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L189:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9084:
	.size	_ZN2v88internal23ConstructStubDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23ConstructStubDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal32ConstructWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal32ConstructWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal32ConstructWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal32ConstructWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal32ConstructWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$4, %r9d
	xorl	%esi, %esi
	movl	$3, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %r8
	movabsq	$506662689138280200, %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L193
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L193:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9077:
	.size	_ZN2v88internal32ConstructWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal32ConstructWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal29ConstructWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal29ConstructWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29ConstructWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal29ConstructWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal29ConstructWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$5, %r9d
	xorl	%esi, %esi
	movl	$4, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-18(%rbp), %r8
	movabsq	$145248819041797896, %rax
	movq	%rax, -18(%rbp)
	movl	$1800, %eax
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L197:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9070:
	.size	_ZN2v88internal29ConstructWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal29ConstructWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal33ConstructForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal33ConstructForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal33ConstructForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal33ConstructForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal33ConstructForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$5, %r9d
	xorl	%esi, %esi
	movl	$4, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-18(%rbp), %r8
	movabsq	$145248819041797896, %rax
	movq	%rax, -18(%rbp)
	movl	$516, %eax
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L201
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L201:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9063:
	.size	_ZN2v88internal33ConstructForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal33ConstructForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal26ConstructVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal26ConstructVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ConstructVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal26ConstructVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal26ConstructVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$6, %r9d
	xorl	%esi, %esi
	movl	$5, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-20(%rbp), %r8
	movl	$117965316, -12(%rbp)
	movabsq	$145248819041797896, %rax
	movq	%rax, -20(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L205
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L205:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9056:
	.size	_ZN2v88internal26ConstructVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal26ConstructVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal27CallWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal27CallWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal27CallWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal27CallWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal27CallWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$3, %r9d
	xorl	%esi, %esi
	movl	$2, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-14(%rbp), %r8
	movl	$1800, %eax
	movl	$117966600, -14(%rbp)
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L209
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L209:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9049:
	.size	_ZN2v88internal27CallWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal27CallWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal24CallWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal24CallWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24CallWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal24CallWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal24CallWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$4, %r9d
	xorl	%esi, %esi
	movl	$3, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %r8
	movabsq	$506657174400272136, %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L213
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L213:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9042:
	.size	_ZN2v88internal24CallWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal24CallWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal30CallFunctionTemplateDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal30CallFunctionTemplateDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30CallFunctionTemplateDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal30CallFunctionTemplateDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal30CallFunctionTemplateDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$3, %r9d
	xorl	%esi, %esi
	movl	$2, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-14(%rbp), %r8
	movl	$1029, %eax
	movl	$117966600, -14(%rbp)
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L217:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9035:
	.size	_ZN2v88internal30CallFunctionTemplateDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal30CallFunctionTemplateDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal28CallForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal28CallForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal28CallForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal28CallForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal28CallForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$4, %r9d
	xorl	%esi, %esi
	movl	$3, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %r8
	movabsq	$145243304303789832, %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L221
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L221:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9028:
	.size	_ZN2v88internal28CallForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal28CallForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal21CallVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal21CallVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21CallVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal21CallVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal21CallVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$5, %r9d
	xorl	%esi, %esi
	movl	$4, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-18(%rbp), %r8
	movabsq	$145243304303789832, %rax
	movq	%rax, -18(%rbp)
	movl	$1800, %eax
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L225
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L225:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9021:
	.size	_ZN2v88internal21CallVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal21CallVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal24CallTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal24CallTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24CallTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal24CallTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal24CallTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$3, %r9d
	xorl	%esi, %esi
	movl	$2, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-14(%rbp), %r8
	movl	$516, %eax
	movl	$117966600, -14(%rbp)
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L229
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L229:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9014:
	.size	_ZN2v88internal24CallTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal24CallTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal16TypeofDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal16TypeofDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16TypeofDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal16TypeofDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal16TypeofDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$2, %r9d
	xorl	%esi, %esi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %r8
	movl	$117966600, -12(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L233
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L233:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9007:
	.size	_ZN2v88internal16TypeofDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal16TypeofDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal29EphemeronKeyBarrierDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal29EphemeronKeyBarrierDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29EphemeronKeyBarrierDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal29EphemeronKeyBarrierDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal29EphemeronKeyBarrierDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$4, %r9d
	movl	$3, %ecx
	movl	$1, %edx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %r8
	movabsq	$145804059528857352, %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L237
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L237:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8971:
	.size	_ZN2v88internal29EphemeronKeyBarrierDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal29EphemeronKeyBarrierDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal21RecordWriteDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal21RecordWriteDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21RecordWriteDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal21RecordWriteDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal21RecordWriteDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$5, %r9d
	movl	$4, %ecx
	movl	$1, %edx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-18(%rbp), %r8
	movabsq	$145804059528857352, %rax
	movq	%rax, -18(%rbp)
	movl	$518, %eax
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L241
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L241:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8964:
	.size	_ZN2v88internal21RecordWriteDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal21RecordWriteDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal28RunMicrotasksEntryDescriptorD0Ev,"axG",@progbits,_ZN2v88internal28RunMicrotasksEntryDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal28RunMicrotasksEntryDescriptorD0Ev
	.type	_ZN2v88internal28RunMicrotasksEntryDescriptorD0Ev, @function
_ZN2v88internal28RunMicrotasksEntryDescriptorD0Ev:
.LFB12721:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12721:
	.size	_ZN2v88internal28RunMicrotasksEntryDescriptorD0Ev, .-_ZN2v88internal28RunMicrotasksEntryDescriptorD0Ev
	.section	.text._ZN2v88internal32FrameDropperTrampolineDescriptorD0Ev,"axG",@progbits,_ZN2v88internal32FrameDropperTrampolineDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal32FrameDropperTrampolineDescriptorD0Ev
	.type	_ZN2v88internal32FrameDropperTrampolineDescriptorD0Ev, @function
_ZN2v88internal32FrameDropperTrampolineDescriptorD0Ev:
.LFB12725:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12725:
	.size	_ZN2v88internal32FrameDropperTrampolineDescriptorD0Ev, .-_ZN2v88internal32FrameDropperTrampolineDescriptorD0Ev
	.section	.text._ZN2v88internal25ResumeGeneratorDescriptorD0Ev,"axG",@progbits,_ZN2v88internal25ResumeGeneratorDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25ResumeGeneratorDescriptorD0Ev
	.type	_ZN2v88internal25ResumeGeneratorDescriptorD0Ev, @function
_ZN2v88internal25ResumeGeneratorDescriptorD0Ev:
.LFB12729:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12729:
	.size	_ZN2v88internal25ResumeGeneratorDescriptorD0Ev, .-_ZN2v88internal25ResumeGeneratorDescriptorD0Ev
	.section	.text._ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD0Ev,"axG",@progbits,_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD0Ev
	.type	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD0Ev, @function
_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD0Ev:
.LFB12733:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12733:
	.size	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD0Ev, .-_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD0Ev
	.section	.text._ZN2v88internal37InterpreterPushArgsThenCallDescriptorD0Ev,"axG",@progbits,_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD0Ev
	.type	_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD0Ev, @function
_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD0Ev:
.LFB12737:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12737:
	.size	_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD0Ev, .-_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD0Ev
	.section	.text._ZN2v88internal29InterpreterDispatchDescriptorD0Ev,"axG",@progbits,_ZN2v88internal29InterpreterDispatchDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29InterpreterDispatchDescriptorD0Ev
	.type	_ZN2v88internal29InterpreterDispatchDescriptorD0Ev, @function
_ZN2v88internal29InterpreterDispatchDescriptorD0Ev:
.LFB12741:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12741:
	.size	_ZN2v88internal29InterpreterDispatchDescriptorD0Ev, .-_ZN2v88internal29InterpreterDispatchDescriptorD0Ev
	.section	.text._ZN2v88internal21ApiCallbackDescriptorD0Ev,"axG",@progbits,_ZN2v88internal21ApiCallbackDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21ApiCallbackDescriptorD0Ev
	.type	_ZN2v88internal21ApiCallbackDescriptorD0Ev, @function
_ZN2v88internal21ApiCallbackDescriptorD0Ev:
.LFB12745:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12745:
	.size	_ZN2v88internal21ApiCallbackDescriptorD0Ev, .-_ZN2v88internal21ApiCallbackDescriptorD0Ev
	.section	.text._ZN2v88internal26ArgumentsAdaptorDescriptorD0Ev,"axG",@progbits,_ZN2v88internal26ArgumentsAdaptorDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ArgumentsAdaptorDescriptorD0Ev
	.type	_ZN2v88internal26ArgumentsAdaptorDescriptorD0Ev, @function
_ZN2v88internal26ArgumentsAdaptorDescriptorD0Ev:
.LFB12749:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12749:
	.size	_ZN2v88internal26ArgumentsAdaptorDescriptorD0Ev, .-_ZN2v88internal26ArgumentsAdaptorDescriptorD0Ev
	.section	.text._ZN2v88internal18BinaryOpDescriptorD0Ev,"axG",@progbits,_ZN2v88internal18BinaryOpDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18BinaryOpDescriptorD0Ev
	.type	_ZN2v88internal18BinaryOpDescriptorD0Ev, @function
_ZN2v88internal18BinaryOpDescriptorD0Ev:
.LFB12753:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12753:
	.size	_ZN2v88internal18BinaryOpDescriptorD0Ev, .-_ZN2v88internal18BinaryOpDescriptorD0Ev
	.section	.text._ZN2v88internal17CompareDescriptorD0Ev,"axG",@progbits,_ZN2v88internal17CompareDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17CompareDescriptorD0Ev
	.type	_ZN2v88internal17CompareDescriptorD0Ev, @function
_ZN2v88internal17CompareDescriptorD0Ev:
.LFB12757:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12757:
	.size	_ZN2v88internal17CompareDescriptorD0Ev, .-_ZN2v88internal17CompareDescriptorD0Ev
	.section	.text._ZN2v88internal28AllocateHeapNumberDescriptorD0Ev,"axG",@progbits,_ZN2v88internal28AllocateHeapNumberDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal28AllocateHeapNumberDescriptorD0Ev
	.type	_ZN2v88internal28AllocateHeapNumberDescriptorD0Ev, @function
_ZN2v88internal28AllocateHeapNumberDescriptorD0Ev:
.LFB12761:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12761:
	.size	_ZN2v88internal28AllocateHeapNumberDescriptorD0Ev, .-_ZN2v88internal28AllocateHeapNumberDescriptorD0Ev
	.section	.text._ZN2v88internal15AbortDescriptorD0Ev,"axG",@progbits,_ZN2v88internal15AbortDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15AbortDescriptorD0Ev
	.type	_ZN2v88internal15AbortDescriptorD0Ev, @function
_ZN2v88internal15AbortDescriptorD0Ev:
.LFB12765:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12765:
	.size	_ZN2v88internal15AbortDescriptorD0Ev, .-_ZN2v88internal15AbortDescriptorD0Ev
	.section	.text._ZN2v88internal23ConstructStubDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23ConstructStubDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23ConstructStubDescriptorD0Ev
	.type	_ZN2v88internal23ConstructStubDescriptorD0Ev, @function
_ZN2v88internal23ConstructStubDescriptorD0Ev:
.LFB12769:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12769:
	.size	_ZN2v88internal23ConstructStubDescriptorD0Ev, .-_ZN2v88internal23ConstructStubDescriptorD0Ev
	.section	.text._ZN2v88internal32ConstructWithArrayLikeDescriptorD0Ev,"axG",@progbits,_ZN2v88internal32ConstructWithArrayLikeDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal32ConstructWithArrayLikeDescriptorD0Ev
	.type	_ZN2v88internal32ConstructWithArrayLikeDescriptorD0Ev, @function
_ZN2v88internal32ConstructWithArrayLikeDescriptorD0Ev:
.LFB12773:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12773:
	.size	_ZN2v88internal32ConstructWithArrayLikeDescriptorD0Ev, .-_ZN2v88internal32ConstructWithArrayLikeDescriptorD0Ev
	.section	.text._ZN2v88internal29ConstructWithSpreadDescriptorD0Ev,"axG",@progbits,_ZN2v88internal29ConstructWithSpreadDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29ConstructWithSpreadDescriptorD0Ev
	.type	_ZN2v88internal29ConstructWithSpreadDescriptorD0Ev, @function
_ZN2v88internal29ConstructWithSpreadDescriptorD0Ev:
.LFB12777:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12777:
	.size	_ZN2v88internal29ConstructWithSpreadDescriptorD0Ev, .-_ZN2v88internal29ConstructWithSpreadDescriptorD0Ev
	.section	.text._ZN2v88internal33ConstructForwardVarargsDescriptorD0Ev,"axG",@progbits,_ZN2v88internal33ConstructForwardVarargsDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal33ConstructForwardVarargsDescriptorD0Ev
	.type	_ZN2v88internal33ConstructForwardVarargsDescriptorD0Ev, @function
_ZN2v88internal33ConstructForwardVarargsDescriptorD0Ev:
.LFB12781:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12781:
	.size	_ZN2v88internal33ConstructForwardVarargsDescriptorD0Ev, .-_ZN2v88internal33ConstructForwardVarargsDescriptorD0Ev
	.section	.text._ZN2v88internal26ConstructVarargsDescriptorD0Ev,"axG",@progbits,_ZN2v88internal26ConstructVarargsDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ConstructVarargsDescriptorD0Ev
	.type	_ZN2v88internal26ConstructVarargsDescriptorD0Ev, @function
_ZN2v88internal26ConstructVarargsDescriptorD0Ev:
.LFB12785:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12785:
	.size	_ZN2v88internal26ConstructVarargsDescriptorD0Ev, .-_ZN2v88internal26ConstructVarargsDescriptorD0Ev
	.section	.text._ZN2v88internal27CallWithArrayLikeDescriptorD0Ev,"axG",@progbits,_ZN2v88internal27CallWithArrayLikeDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal27CallWithArrayLikeDescriptorD0Ev
	.type	_ZN2v88internal27CallWithArrayLikeDescriptorD0Ev, @function
_ZN2v88internal27CallWithArrayLikeDescriptorD0Ev:
.LFB12789:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12789:
	.size	_ZN2v88internal27CallWithArrayLikeDescriptorD0Ev, .-_ZN2v88internal27CallWithArrayLikeDescriptorD0Ev
	.section	.text._ZN2v88internal24CallWithSpreadDescriptorD0Ev,"axG",@progbits,_ZN2v88internal24CallWithSpreadDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24CallWithSpreadDescriptorD0Ev
	.type	_ZN2v88internal24CallWithSpreadDescriptorD0Ev, @function
_ZN2v88internal24CallWithSpreadDescriptorD0Ev:
.LFB12793:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12793:
	.size	_ZN2v88internal24CallWithSpreadDescriptorD0Ev, .-_ZN2v88internal24CallWithSpreadDescriptorD0Ev
	.section	.text._ZN2v88internal30CallFunctionTemplateDescriptorD0Ev,"axG",@progbits,_ZN2v88internal30CallFunctionTemplateDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30CallFunctionTemplateDescriptorD0Ev
	.type	_ZN2v88internal30CallFunctionTemplateDescriptorD0Ev, @function
_ZN2v88internal30CallFunctionTemplateDescriptorD0Ev:
.LFB12797:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12797:
	.size	_ZN2v88internal30CallFunctionTemplateDescriptorD0Ev, .-_ZN2v88internal30CallFunctionTemplateDescriptorD0Ev
	.section	.text._ZN2v88internal28CallForwardVarargsDescriptorD0Ev,"axG",@progbits,_ZN2v88internal28CallForwardVarargsDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal28CallForwardVarargsDescriptorD0Ev
	.type	_ZN2v88internal28CallForwardVarargsDescriptorD0Ev, @function
_ZN2v88internal28CallForwardVarargsDescriptorD0Ev:
.LFB12801:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12801:
	.size	_ZN2v88internal28CallForwardVarargsDescriptorD0Ev, .-_ZN2v88internal28CallForwardVarargsDescriptorD0Ev
	.section	.text._ZN2v88internal21CallVarargsDescriptorD0Ev,"axG",@progbits,_ZN2v88internal21CallVarargsDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21CallVarargsDescriptorD0Ev
	.type	_ZN2v88internal21CallVarargsDescriptorD0Ev, @function
_ZN2v88internal21CallVarargsDescriptorD0Ev:
.LFB12805:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12805:
	.size	_ZN2v88internal21CallVarargsDescriptorD0Ev, .-_ZN2v88internal21CallVarargsDescriptorD0Ev
	.section	.text._ZN2v88internal24CallTrampolineDescriptorD0Ev,"axG",@progbits,_ZN2v88internal24CallTrampolineDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24CallTrampolineDescriptorD0Ev
	.type	_ZN2v88internal24CallTrampolineDescriptorD0Ev, @function
_ZN2v88internal24CallTrampolineDescriptorD0Ev:
.LFB12809:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12809:
	.size	_ZN2v88internal24CallTrampolineDescriptorD0Ev, .-_ZN2v88internal24CallTrampolineDescriptorD0Ev
	.section	.text._ZN2v88internal16TypeofDescriptorD0Ev,"axG",@progbits,_ZN2v88internal16TypeofDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16TypeofDescriptorD0Ev
	.type	_ZN2v88internal16TypeofDescriptorD0Ev, @function
_ZN2v88internal16TypeofDescriptorD0Ev:
.LFB12813:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12813:
	.size	_ZN2v88internal16TypeofDescriptorD0Ev, .-_ZN2v88internal16TypeofDescriptorD0Ev
	.section	.text._ZN2v88internal29EphemeronKeyBarrierDescriptorD0Ev,"axG",@progbits,_ZN2v88internal29EphemeronKeyBarrierDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29EphemeronKeyBarrierDescriptorD0Ev
	.type	_ZN2v88internal29EphemeronKeyBarrierDescriptorD0Ev, @function
_ZN2v88internal29EphemeronKeyBarrierDescriptorD0Ev:
.LFB12817:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12817:
	.size	_ZN2v88internal29EphemeronKeyBarrierDescriptorD0Ev, .-_ZN2v88internal29EphemeronKeyBarrierDescriptorD0Ev
	.section	.text._ZN2v88internal21RecordWriteDescriptorD0Ev,"axG",@progbits,_ZN2v88internal21RecordWriteDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21RecordWriteDescriptorD0Ev
	.type	_ZN2v88internal21RecordWriteDescriptorD0Ev, @function
_ZN2v88internal21RecordWriteDescriptorD0Ev:
.LFB12821:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12821:
	.size	_ZN2v88internal21RecordWriteDescriptorD0Ev, .-_ZN2v88internal21RecordWriteDescriptorD0Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor15ContextRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23CallInterfaceDescriptor15ContextRegisterEv
	.type	_ZN2v88internal23CallInterfaceDescriptor15ContextRegisterEv, @function
_ZN2v88internal23CallInterfaceDescriptor15ContextRegisterEv:
.LFB11402:
	.cfi_startproc
	endbr64
	movl	$6, %eax
	ret
	.cfi_endproc
.LFE11402:
	.size	_ZN2v88internal23CallInterfaceDescriptor15ContextRegisterEv, .-_ZN2v88internal23CallInterfaceDescriptor15ContextRegisterEv
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor33DefaultInitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataEi.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"static_cast<size_t>(register_parameter_count) <= (sizeof(ArraySizeHelper(default_stub_registers)))"
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor33DefaultInitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataEi.str1.1,"aMS",@progbits,1
.LC9:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal23CallInterfaceDescriptor33DefaultInitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23CallInterfaceDescriptor33DefaultInitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataEi
	.type	_ZN2v88internal23CallInterfaceDescriptor33DefaultInitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataEi, @function
_ZN2v88internal23CallInterfaceDescriptor33DefaultInitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataEi:
.LFB11403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movdqa	.LC7(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movslq	%esi, %rax
	movl	$7, -16(%rbp)
	movaps	%xmm0, -32(%rbp)
	cmpq	$5, %rax
	ja	.L273
	leaq	-32(%rbp), %rdx
	call	_ZN2v88internal27CallInterfaceDescriptorData26InitializePlatformSpecificEiPKNS0_8RegisterE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L274
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	leaq	.LC8(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L274:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11403:
	.size	_ZN2v88internal23CallInterfaceDescriptor33DefaultInitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataEi, .-_ZN2v88internal23CallInterfaceDescriptor33DefaultInitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataEi
	.section	.text._ZN2v88internal32FastNewFunctionContextDescriptor17ScopeInfoRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32FastNewFunctionContextDescriptor17ScopeInfoRegisterEv
	.type	_ZN2v88internal32FastNewFunctionContextDescriptor17ScopeInfoRegisterEv, @function
_ZN2v88internal32FastNewFunctionContextDescriptor17ScopeInfoRegisterEv:
.LFB13641:
	.cfi_startproc
	endbr64
	movl	$7, %eax
	ret
	.cfi_endproc
.LFE13641:
	.size	_ZN2v88internal32FastNewFunctionContextDescriptor17ScopeInfoRegisterEv, .-_ZN2v88internal32FastNewFunctionContextDescriptor17ScopeInfoRegisterEv
	.section	.text._ZN2v88internal32FastNewFunctionContextDescriptor13SlotsRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32FastNewFunctionContextDescriptor13SlotsRegisterEv
	.type	_ZN2v88internal32FastNewFunctionContextDescriptor13SlotsRegisterEv, @function
_ZN2v88internal32FastNewFunctionContextDescriptor13SlotsRegisterEv:
.LFB13633:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE13633:
	.size	_ZN2v88internal32FastNewFunctionContextDescriptor13SlotsRegisterEv, .-_ZN2v88internal32FastNewFunctionContextDescriptor13SlotsRegisterEv
	.section	.text._ZN2v88internal14LoadDescriptor16ReceiverRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LoadDescriptor16ReceiverRegisterEv
	.type	_ZN2v88internal14LoadDescriptor16ReceiverRegisterEv, @function
_ZN2v88internal14LoadDescriptor16ReceiverRegisterEv:
.LFB11408:
	.cfi_startproc
	endbr64
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE11408:
	.size	_ZN2v88internal14LoadDescriptor16ReceiverRegisterEv, .-_ZN2v88internal14LoadDescriptor16ReceiverRegisterEv
	.section	.text._ZN2v88internal14LoadDescriptor12NameRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LoadDescriptor12NameRegisterEv
	.type	_ZN2v88internal14LoadDescriptor12NameRegisterEv, @function
_ZN2v88internal14LoadDescriptor12NameRegisterEv:
.LFB11409:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE11409:
	.size	_ZN2v88internal14LoadDescriptor12NameRegisterEv, .-_ZN2v88internal14LoadDescriptor12NameRegisterEv
	.section	.text._ZN2v88internal14LoadDescriptor12SlotRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LoadDescriptor12SlotRegisterEv
	.type	_ZN2v88internal14LoadDescriptor12SlotRegisterEv, @function
_ZN2v88internal14LoadDescriptor12SlotRegisterEv:
.LFB11410:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11410:
	.size	_ZN2v88internal14LoadDescriptor12SlotRegisterEv, .-_ZN2v88internal14LoadDescriptor12SlotRegisterEv
	.section	.text._ZN2v88internal24LoadWithVectorDescriptor14VectorRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24LoadWithVectorDescriptor14VectorRegisterEv
	.type	_ZN2v88internal24LoadWithVectorDescriptor14VectorRegisterEv, @function
_ZN2v88internal24LoadWithVectorDescriptor14VectorRegisterEv:
.LFB13645:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE13645:
	.size	_ZN2v88internal24LoadWithVectorDescriptor14VectorRegisterEv, .-_ZN2v88internal24LoadWithVectorDescriptor14VectorRegisterEv
	.section	.text._ZN2v88internal15StoreDescriptor16ReceiverRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StoreDescriptor16ReceiverRegisterEv
	.type	_ZN2v88internal15StoreDescriptor16ReceiverRegisterEv, @function
_ZN2v88internal15StoreDescriptor16ReceiverRegisterEv:
.LFB13625:
	.cfi_startproc
	endbr64
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE13625:
	.size	_ZN2v88internal15StoreDescriptor16ReceiverRegisterEv, .-_ZN2v88internal15StoreDescriptor16ReceiverRegisterEv
	.section	.text._ZN2v88internal15StoreDescriptor12NameRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StoreDescriptor12NameRegisterEv
	.type	_ZN2v88internal15StoreDescriptor12NameRegisterEv, @function
_ZN2v88internal15StoreDescriptor12NameRegisterEv:
.LFB13627:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE13627:
	.size	_ZN2v88internal15StoreDescriptor12NameRegisterEv, .-_ZN2v88internal15StoreDescriptor12NameRegisterEv
	.section	.text._ZN2v88internal15StoreDescriptor13ValueRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StoreDescriptor13ValueRegisterEv
	.type	_ZN2v88internal15StoreDescriptor13ValueRegisterEv, @function
_ZN2v88internal15StoreDescriptor13ValueRegisterEv:
.LFB13631:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE13631:
	.size	_ZN2v88internal15StoreDescriptor13ValueRegisterEv, .-_ZN2v88internal15StoreDescriptor13ValueRegisterEv
	.section	.text._ZN2v88internal15StoreDescriptor12SlotRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StoreDescriptor12SlotRegisterEv
	.type	_ZN2v88internal15StoreDescriptor12SlotRegisterEv, @function
_ZN2v88internal15StoreDescriptor12SlotRegisterEv:
.LFB11415:
	.cfi_startproc
	endbr64
	movl	$7, %eax
	ret
	.cfi_endproc
.LFE11415:
	.size	_ZN2v88internal15StoreDescriptor12SlotRegisterEv, .-_ZN2v88internal15StoreDescriptor12SlotRegisterEv
	.section	.text._ZN2v88internal25StoreWithVectorDescriptor14VectorRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25StoreWithVectorDescriptor14VectorRegisterEv
	.type	_ZN2v88internal25StoreWithVectorDescriptor14VectorRegisterEv, @function
_ZN2v88internal25StoreWithVectorDescriptor14VectorRegisterEv:
.LFB13643:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE13643:
	.size	_ZN2v88internal25StoreWithVectorDescriptor14VectorRegisterEv, .-_ZN2v88internal25StoreWithVectorDescriptor14VectorRegisterEv
	.section	.text._ZN2v88internal25StoreTransitionDescriptor12SlotRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25StoreTransitionDescriptor12SlotRegisterEv
	.type	_ZN2v88internal25StoreTransitionDescriptor12SlotRegisterEv, @function
_ZN2v88internal25StoreTransitionDescriptor12SlotRegisterEv:
.LFB13639:
	.cfi_startproc
	endbr64
	movl	$7, %eax
	ret
	.cfi_endproc
.LFE13639:
	.size	_ZN2v88internal25StoreTransitionDescriptor12SlotRegisterEv, .-_ZN2v88internal25StoreTransitionDescriptor12SlotRegisterEv
	.section	.text._ZN2v88internal25StoreTransitionDescriptor14VectorRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25StoreTransitionDescriptor14VectorRegisterEv
	.type	_ZN2v88internal25StoreTransitionDescriptor14VectorRegisterEv, @function
_ZN2v88internal25StoreTransitionDescriptor14VectorRegisterEv:
.LFB11418:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE11418:
	.size	_ZN2v88internal25StoreTransitionDescriptor14VectorRegisterEv, .-_ZN2v88internal25StoreTransitionDescriptor14VectorRegisterEv
	.section	.text._ZN2v88internal25StoreTransitionDescriptor11MapRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25StoreTransitionDescriptor11MapRegisterEv
	.type	_ZN2v88internal25StoreTransitionDescriptor11MapRegisterEv, @function
_ZN2v88internal25StoreTransitionDescriptor11MapRegisterEv:
.LFB11419:
	.cfi_startproc
	endbr64
	movl	$11, %eax
	ret
	.cfi_endproc
.LFE11419:
	.size	_ZN2v88internal25StoreTransitionDescriptor11MapRegisterEv, .-_ZN2v88internal25StoreTransitionDescriptor11MapRegisterEv
	.section	.text._ZN2v88internal19ApiGetterDescriptor14HolderRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19ApiGetterDescriptor14HolderRegisterEv
	.type	_ZN2v88internal19ApiGetterDescriptor14HolderRegisterEv, @function
_ZN2v88internal19ApiGetterDescriptor14HolderRegisterEv:
.LFB13629:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE13629:
	.size	_ZN2v88internal19ApiGetterDescriptor14HolderRegisterEv, .-_ZN2v88internal19ApiGetterDescriptor14HolderRegisterEv
	.section	.text._ZN2v88internal19ApiGetterDescriptor16CallbackRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19ApiGetterDescriptor16CallbackRegisterEv
	.type	_ZN2v88internal19ApiGetterDescriptor16CallbackRegisterEv, @function
_ZN2v88internal19ApiGetterDescriptor16CallbackRegisterEv:
.LFB13647:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE13647:
	.size	_ZN2v88internal19ApiGetterDescriptor16CallbackRegisterEv, .-_ZN2v88internal19ApiGetterDescriptor16CallbackRegisterEv
	.section	.text._ZN2v88internal27GrowArrayElementsDescriptor14ObjectRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27GrowArrayElementsDescriptor14ObjectRegisterEv
	.type	_ZN2v88internal27GrowArrayElementsDescriptor14ObjectRegisterEv, @function
_ZN2v88internal27GrowArrayElementsDescriptor14ObjectRegisterEv:
.LFB13637:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE13637:
	.size	_ZN2v88internal27GrowArrayElementsDescriptor14ObjectRegisterEv, .-_ZN2v88internal27GrowArrayElementsDescriptor14ObjectRegisterEv
	.section	.text._ZN2v88internal27GrowArrayElementsDescriptor11KeyRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27GrowArrayElementsDescriptor11KeyRegisterEv
	.type	_ZN2v88internal27GrowArrayElementsDescriptor11KeyRegisterEv, @function
_ZN2v88internal27GrowArrayElementsDescriptor11KeyRegisterEv:
.LFB13649:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE13649:
	.size	_ZN2v88internal27GrowArrayElementsDescriptor11KeyRegisterEv, .-_ZN2v88internal27GrowArrayElementsDescriptor11KeyRegisterEv
	.section	.text._ZN2v88internal24TypeConversionDescriptor16ArgumentRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24TypeConversionDescriptor16ArgumentRegisterEv
	.type	_ZN2v88internal24TypeConversionDescriptor16ArgumentRegisterEv, @function
_ZN2v88internal24TypeConversionDescriptor16ArgumentRegisterEv:
.LFB13635:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE13635:
	.size	_ZN2v88internal24TypeConversionDescriptor16ArgumentRegisterEv, .-_ZN2v88internal24TypeConversionDescriptor16ArgumentRegisterEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal23CallInterfaceDescriptor15ContextRegisterEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal23CallInterfaceDescriptor15ContextRegisterEv, @function
_GLOBAL__sub_I__ZN2v88internal23CallInterfaceDescriptor15ContextRegisterEv:
.LFB13619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13619:
	.size	_GLOBAL__sub_I__ZN2v88internal23CallInterfaceDescriptor15ContextRegisterEv, .-_GLOBAL__sub_I__ZN2v88internal23CallInterfaceDescriptor15ContextRegisterEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal23CallInterfaceDescriptor15ContextRegisterEv
	.weak	_ZTVN2v88internal21RecordWriteDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal21RecordWriteDescriptorE,"awG",@progbits,_ZTVN2v88internal21RecordWriteDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal21RecordWriteDescriptorE, @object
	.size	_ZTVN2v88internal21RecordWriteDescriptorE, 48
_ZTVN2v88internal21RecordWriteDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal21RecordWriteDescriptorD1Ev
	.quad	_ZN2v88internal21RecordWriteDescriptorD0Ev
	.quad	_ZN2v88internal21RecordWriteDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal21RecordWriteDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal29EphemeronKeyBarrierDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal29EphemeronKeyBarrierDescriptorE,"awG",@progbits,_ZTVN2v88internal29EphemeronKeyBarrierDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal29EphemeronKeyBarrierDescriptorE, @object
	.size	_ZTVN2v88internal29EphemeronKeyBarrierDescriptorE, 48
_ZTVN2v88internal29EphemeronKeyBarrierDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal29EphemeronKeyBarrierDescriptorD1Ev
	.quad	_ZN2v88internal29EphemeronKeyBarrierDescriptorD0Ev
	.quad	_ZN2v88internal29EphemeronKeyBarrierDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal29EphemeronKeyBarrierDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal16TypeofDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal16TypeofDescriptorE,"awG",@progbits,_ZTVN2v88internal16TypeofDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal16TypeofDescriptorE, @object
	.size	_ZTVN2v88internal16TypeofDescriptorE, 48
_ZTVN2v88internal16TypeofDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal16TypeofDescriptorD1Ev
	.quad	_ZN2v88internal16TypeofDescriptorD0Ev
	.quad	_ZN2v88internal16TypeofDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal16TypeofDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal24CallTrampolineDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal24CallTrampolineDescriptorE,"awG",@progbits,_ZTVN2v88internal24CallTrampolineDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal24CallTrampolineDescriptorE, @object
	.size	_ZTVN2v88internal24CallTrampolineDescriptorE, 48
_ZTVN2v88internal24CallTrampolineDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal24CallTrampolineDescriptorD1Ev
	.quad	_ZN2v88internal24CallTrampolineDescriptorD0Ev
	.quad	_ZN2v88internal24CallTrampolineDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal24CallTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal21CallVarargsDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal21CallVarargsDescriptorE,"awG",@progbits,_ZTVN2v88internal21CallVarargsDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal21CallVarargsDescriptorE, @object
	.size	_ZTVN2v88internal21CallVarargsDescriptorE, 48
_ZTVN2v88internal21CallVarargsDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal21CallVarargsDescriptorD1Ev
	.quad	_ZN2v88internal21CallVarargsDescriptorD0Ev
	.quad	_ZN2v88internal21CallVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal21CallVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal28CallForwardVarargsDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal28CallForwardVarargsDescriptorE,"awG",@progbits,_ZTVN2v88internal28CallForwardVarargsDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal28CallForwardVarargsDescriptorE, @object
	.size	_ZTVN2v88internal28CallForwardVarargsDescriptorE, 48
_ZTVN2v88internal28CallForwardVarargsDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal28CallForwardVarargsDescriptorD1Ev
	.quad	_ZN2v88internal28CallForwardVarargsDescriptorD0Ev
	.quad	_ZN2v88internal28CallForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal28CallForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal30CallFunctionTemplateDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal30CallFunctionTemplateDescriptorE,"awG",@progbits,_ZTVN2v88internal30CallFunctionTemplateDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal30CallFunctionTemplateDescriptorE, @object
	.size	_ZTVN2v88internal30CallFunctionTemplateDescriptorE, 48
_ZTVN2v88internal30CallFunctionTemplateDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal30CallFunctionTemplateDescriptorD1Ev
	.quad	_ZN2v88internal30CallFunctionTemplateDescriptorD0Ev
	.quad	_ZN2v88internal30CallFunctionTemplateDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal30CallFunctionTemplateDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal24CallWithSpreadDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal24CallWithSpreadDescriptorE,"awG",@progbits,_ZTVN2v88internal24CallWithSpreadDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal24CallWithSpreadDescriptorE, @object
	.size	_ZTVN2v88internal24CallWithSpreadDescriptorE, 48
_ZTVN2v88internal24CallWithSpreadDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal24CallWithSpreadDescriptorD1Ev
	.quad	_ZN2v88internal24CallWithSpreadDescriptorD0Ev
	.quad	_ZN2v88internal24CallWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal24CallWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal27CallWithArrayLikeDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal27CallWithArrayLikeDescriptorE,"awG",@progbits,_ZTVN2v88internal27CallWithArrayLikeDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal27CallWithArrayLikeDescriptorE, @object
	.size	_ZTVN2v88internal27CallWithArrayLikeDescriptorE, 48
_ZTVN2v88internal27CallWithArrayLikeDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal27CallWithArrayLikeDescriptorD1Ev
	.quad	_ZN2v88internal27CallWithArrayLikeDescriptorD0Ev
	.quad	_ZN2v88internal27CallWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal27CallWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal26ConstructVarargsDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal26ConstructVarargsDescriptorE,"awG",@progbits,_ZTVN2v88internal26ConstructVarargsDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal26ConstructVarargsDescriptorE, @object
	.size	_ZTVN2v88internal26ConstructVarargsDescriptorE, 48
_ZTVN2v88internal26ConstructVarargsDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal26ConstructVarargsDescriptorD1Ev
	.quad	_ZN2v88internal26ConstructVarargsDescriptorD0Ev
	.quad	_ZN2v88internal26ConstructVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal26ConstructVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal33ConstructForwardVarargsDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal33ConstructForwardVarargsDescriptorE,"awG",@progbits,_ZTVN2v88internal33ConstructForwardVarargsDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal33ConstructForwardVarargsDescriptorE, @object
	.size	_ZTVN2v88internal33ConstructForwardVarargsDescriptorE, 48
_ZTVN2v88internal33ConstructForwardVarargsDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal33ConstructForwardVarargsDescriptorD1Ev
	.quad	_ZN2v88internal33ConstructForwardVarargsDescriptorD0Ev
	.quad	_ZN2v88internal33ConstructForwardVarargsDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal33ConstructForwardVarargsDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal29ConstructWithSpreadDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal29ConstructWithSpreadDescriptorE,"awG",@progbits,_ZTVN2v88internal29ConstructWithSpreadDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal29ConstructWithSpreadDescriptorE, @object
	.size	_ZTVN2v88internal29ConstructWithSpreadDescriptorE, 48
_ZTVN2v88internal29ConstructWithSpreadDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal29ConstructWithSpreadDescriptorD1Ev
	.quad	_ZN2v88internal29ConstructWithSpreadDescriptorD0Ev
	.quad	_ZN2v88internal29ConstructWithSpreadDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal29ConstructWithSpreadDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal32ConstructWithArrayLikeDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal32ConstructWithArrayLikeDescriptorE,"awG",@progbits,_ZTVN2v88internal32ConstructWithArrayLikeDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal32ConstructWithArrayLikeDescriptorE, @object
	.size	_ZTVN2v88internal32ConstructWithArrayLikeDescriptorE, 48
_ZTVN2v88internal32ConstructWithArrayLikeDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal32ConstructWithArrayLikeDescriptorD1Ev
	.quad	_ZN2v88internal32ConstructWithArrayLikeDescriptorD0Ev
	.quad	_ZN2v88internal32ConstructWithArrayLikeDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal32ConstructWithArrayLikeDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal23ConstructStubDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23ConstructStubDescriptorE,"awG",@progbits,_ZTVN2v88internal23ConstructStubDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23ConstructStubDescriptorE, @object
	.size	_ZTVN2v88internal23ConstructStubDescriptorE, 48
_ZTVN2v88internal23ConstructStubDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23ConstructStubDescriptorD1Ev
	.quad	_ZN2v88internal23ConstructStubDescriptorD0Ev
	.quad	_ZN2v88internal23ConstructStubDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23ConstructStubDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal15AbortDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal15AbortDescriptorE,"awG",@progbits,_ZTVN2v88internal15AbortDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal15AbortDescriptorE, @object
	.size	_ZTVN2v88internal15AbortDescriptorE, 48
_ZTVN2v88internal15AbortDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15AbortDescriptorD1Ev
	.quad	_ZN2v88internal15AbortDescriptorD0Ev
	.quad	_ZN2v88internal15AbortDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal15AbortDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal28AllocateHeapNumberDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal28AllocateHeapNumberDescriptorE,"awG",@progbits,_ZTVN2v88internal28AllocateHeapNumberDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal28AllocateHeapNumberDescriptorE, @object
	.size	_ZTVN2v88internal28AllocateHeapNumberDescriptorE, 48
_ZTVN2v88internal28AllocateHeapNumberDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal28AllocateHeapNumberDescriptorD1Ev
	.quad	_ZN2v88internal28AllocateHeapNumberDescriptorD0Ev
	.quad	_ZN2v88internal28AllocateHeapNumberDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal28AllocateHeapNumberDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal17CompareDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal17CompareDescriptorE,"awG",@progbits,_ZTVN2v88internal17CompareDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal17CompareDescriptorE, @object
	.size	_ZTVN2v88internal17CompareDescriptorE, 48
_ZTVN2v88internal17CompareDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal17CompareDescriptorD1Ev
	.quad	_ZN2v88internal17CompareDescriptorD0Ev
	.quad	_ZN2v88internal17CompareDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal18BinaryOpDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal18BinaryOpDescriptorE,"awG",@progbits,_ZTVN2v88internal18BinaryOpDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal18BinaryOpDescriptorE, @object
	.size	_ZTVN2v88internal18BinaryOpDescriptorE, 48
_ZTVN2v88internal18BinaryOpDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal18BinaryOpDescriptorD1Ev
	.quad	_ZN2v88internal18BinaryOpDescriptorD0Ev
	.quad	_ZN2v88internal18BinaryOpDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal26ArgumentsAdaptorDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal26ArgumentsAdaptorDescriptorE,"awG",@progbits,_ZTVN2v88internal26ArgumentsAdaptorDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal26ArgumentsAdaptorDescriptorE, @object
	.size	_ZTVN2v88internal26ArgumentsAdaptorDescriptorE, 48
_ZTVN2v88internal26ArgumentsAdaptorDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal26ArgumentsAdaptorDescriptorD1Ev
	.quad	_ZN2v88internal26ArgumentsAdaptorDescriptorD0Ev
	.quad	_ZN2v88internal26ArgumentsAdaptorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal26ArgumentsAdaptorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal21ApiCallbackDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal21ApiCallbackDescriptorE,"awG",@progbits,_ZTVN2v88internal21ApiCallbackDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal21ApiCallbackDescriptorE, @object
	.size	_ZTVN2v88internal21ApiCallbackDescriptorE, 48
_ZTVN2v88internal21ApiCallbackDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal21ApiCallbackDescriptorD1Ev
	.quad	_ZN2v88internal21ApiCallbackDescriptorD0Ev
	.quad	_ZN2v88internal21ApiCallbackDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal21ApiCallbackDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal29InterpreterDispatchDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal29InterpreterDispatchDescriptorE,"awG",@progbits,_ZTVN2v88internal29InterpreterDispatchDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal29InterpreterDispatchDescriptorE, @object
	.size	_ZTVN2v88internal29InterpreterDispatchDescriptorE, 48
_ZTVN2v88internal29InterpreterDispatchDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal29InterpreterDispatchDescriptorD1Ev
	.quad	_ZN2v88internal29InterpreterDispatchDescriptorD0Ev
	.quad	_ZN2v88internal29InterpreterDispatchDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal29InterpreterDispatchDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal37InterpreterPushArgsThenCallDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal37InterpreterPushArgsThenCallDescriptorE,"awG",@progbits,_ZTVN2v88internal37InterpreterPushArgsThenCallDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal37InterpreterPushArgsThenCallDescriptorE, @object
	.size	_ZTVN2v88internal37InterpreterPushArgsThenCallDescriptorE, 48
_ZTVN2v88internal37InterpreterPushArgsThenCallDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD1Ev
	.quad	_ZN2v88internal37InterpreterPushArgsThenCallDescriptorD0Ev
	.quad	_ZN2v88internal37InterpreterPushArgsThenCallDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal37InterpreterPushArgsThenCallDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal42InterpreterPushArgsThenConstructDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal42InterpreterPushArgsThenConstructDescriptorE,"awG",@progbits,_ZTVN2v88internal42InterpreterPushArgsThenConstructDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal42InterpreterPushArgsThenConstructDescriptorE, @object
	.size	_ZTVN2v88internal42InterpreterPushArgsThenConstructDescriptorE, 48
_ZTVN2v88internal42InterpreterPushArgsThenConstructDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD1Ev
	.quad	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptorD0Ev
	.quad	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal42InterpreterPushArgsThenConstructDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal25ResumeGeneratorDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal25ResumeGeneratorDescriptorE,"awG",@progbits,_ZTVN2v88internal25ResumeGeneratorDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal25ResumeGeneratorDescriptorE, @object
	.size	_ZTVN2v88internal25ResumeGeneratorDescriptorE, 48
_ZTVN2v88internal25ResumeGeneratorDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal25ResumeGeneratorDescriptorD1Ev
	.quad	_ZN2v88internal25ResumeGeneratorDescriptorD0Ev
	.quad	_ZN2v88internal25ResumeGeneratorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal25ResumeGeneratorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal32FrameDropperTrampolineDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal32FrameDropperTrampolineDescriptorE,"awG",@progbits,_ZTVN2v88internal32FrameDropperTrampolineDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal32FrameDropperTrampolineDescriptorE, @object
	.size	_ZTVN2v88internal32FrameDropperTrampolineDescriptorE, 48
_ZTVN2v88internal32FrameDropperTrampolineDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal32FrameDropperTrampolineDescriptorD1Ev
	.quad	_ZN2v88internal32FrameDropperTrampolineDescriptorD0Ev
	.quad	_ZN2v88internal32FrameDropperTrampolineDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal32FrameDropperTrampolineDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal28RunMicrotasksEntryDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal28RunMicrotasksEntryDescriptorE,"awG",@progbits,_ZTVN2v88internal28RunMicrotasksEntryDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal28RunMicrotasksEntryDescriptorE, @object
	.size	_ZTVN2v88internal28RunMicrotasksEntryDescriptorE, 48
_ZTVN2v88internal28RunMicrotasksEntryDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal28RunMicrotasksEntryDescriptorD1Ev
	.quad	_ZN2v88internal28RunMicrotasksEntryDescriptorD0Ev
	.quad	_ZN2v88internal28RunMicrotasksEntryDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal28RunMicrotasksEntryDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	7
	.long	0
	.long	1
	.long	3
	.align 16
.LC1:
	.long	7
	.long	2
	.long	0
	.long	1
	.align 16
.LC2:
	.long	7
	.long	2
	.long	0
	.long	3
	.align 16
.LC3:
	.long	2
	.long	1
	.long	3
	.long	7
	.align 16
.LC4:
	.long	0
	.long	9
	.long	14
	.long	15
	.align 16
.LC5:
	.long	0
	.long	1
	.long	7
	.long	2
	.align 16
.LC6:
	.long	7
	.long	6
	.long	2
	.long	1
	.align 16
.LC7:
	.long	0
	.long	3
	.long	1
	.long	2
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
