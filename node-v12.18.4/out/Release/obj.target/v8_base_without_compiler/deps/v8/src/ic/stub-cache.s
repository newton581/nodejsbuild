	.file	"stub-cache.cc"
	.text
	.section	.text._ZN2v88internal9StubCacheC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9StubCacheC2EPNS0_7IsolateE
	.type	_ZN2v88internal9StubCacheC2EPNS0_7IsolateE, @function
_ZN2v88internal9StubCacheC2EPNS0_7IsolateE:
.LFB23027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$49152, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	memset@PLT
	leaq	49152(%rbx), %rdi
	movl	$12288, %edx
	xorl	%esi, %esi
	call	memset@PLT
	movq	%r12, 61440(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23027:
	.size	_ZN2v88internal9StubCacheC2EPNS0_7IsolateE, .-_ZN2v88internal9StubCacheC2EPNS0_7IsolateE
	.globl	_ZN2v88internal9StubCacheC1EPNS0_7IsolateE
	.set	_ZN2v88internal9StubCacheC1EPNS0_7IsolateE,_ZN2v88internal9StubCacheC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal9StubCache10InitializeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9StubCache10InitializeEv
	.type	_ZN2v88internal9StubCache10InitializeEv, @function
_ZN2v88internal9StubCache10InitializeEv:
.LFB23029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$166, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	61440(%rdi), %rax
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	61440(%rbx), %rdx
	pxor	%xmm2, %xmm2
	movq	%rax, %xmm0
	leaq	49152(%rbx), %rax
	movq	128(%rdx), %xmm1
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, %rdx
	punpcklqdq	%xmm1, %xmm1
	movdqa	%xmm1, %xmm3
	punpcklqdq	%xmm0, %xmm3
	shufpd	$1, %xmm1, %xmm0
	shufpd	$2, %xmm2, %xmm0
	shufpd	$2, %xmm1, %xmm2
	.p2align 4,,10
	.p2align 3
.L5:
	movups	%xmm3, (%rdx)
	addq	$48, %rdx
	movups	%xmm2, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rdx, %rax
	jne	.L5
	pxor	%xmm2, %xmm2
	addq	$61440, %rbx
	shufpd	$2, %xmm1, %xmm2
	movdqa	%xmm2, %xmm1
	.p2align 4,,10
	.p2align 3
.L6:
	movups	%xmm3, (%rax)
	addq	$48, %rax
	movups	%xmm1, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rbx
	jne	.L6
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23029:
	.size	_ZN2v88internal9StubCache10InitializeEv, .-_ZN2v88internal9StubCache10InitializeEv
	.section	.text._ZN2v88internal9StubCache13PrimaryOffsetENS0_4NameENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9StubCache13PrimaryOffsetENS0_4NameENS0_3MapE
	.type	_ZN2v88internal9StubCache13PrimaryOffsetENS0_4NameENS0_3MapE, @function
_ZN2v88internal9StubCache13PrimaryOffsetENS0_4NameENS0_3MapE:
.LFB23030:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	shrq	$13, %rsi
	xorl	%eax, %esi
	movl	7(%rdi), %eax
	addl	%esi, %eax
	andl	$8188, %eax
	ret
	.cfi_endproc
.LFE23030:
	.size	_ZN2v88internal9StubCache13PrimaryOffsetENS0_4NameENS0_3MapE, .-_ZN2v88internal9StubCache13PrimaryOffsetENS0_4NameENS0_3MapE
	.section	.text._ZN2v88internal9StubCache15SecondaryOffsetENS0_4NameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9StubCache15SecondaryOffsetENS0_4NameEi
	.type	_ZN2v88internal9StubCache15SecondaryOffsetENS0_4NameEi, @function
_ZN2v88internal9StubCache15SecondaryOffsetENS0_4NameEi:
.LFB23031:
	.cfi_startproc
	endbr64
	subl	%edi, %esi
	leal	-1318279451(%rsi), %eax
	andl	$2044, %eax
	ret
	.cfi_endproc
.LFE23031:
	.size	_ZN2v88internal9StubCache15SecondaryOffsetENS0_4NameEi, .-_ZN2v88internal9StubCache15SecondaryOffsetENS0_4NameEi
	.section	.text._ZN2v88internal9StubCache23PrimaryOffsetForTestingENS0_4NameENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9StubCache23PrimaryOffsetForTestingENS0_4NameENS0_3MapE
	.type	_ZN2v88internal9StubCache23PrimaryOffsetForTestingENS0_4NameENS0_3MapE, @function
_ZN2v88internal9StubCache23PrimaryOffsetForTestingENS0_4NameENS0_3MapE:
.LFB23032:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	shrq	$13, %rsi
	xorl	%eax, %esi
	movl	7(%rdi), %eax
	addl	%esi, %eax
	andl	$8188, %eax
	ret
	.cfi_endproc
.LFE23032:
	.size	_ZN2v88internal9StubCache23PrimaryOffsetForTestingENS0_4NameENS0_3MapE, .-_ZN2v88internal9StubCache23PrimaryOffsetForTestingENS0_4NameENS0_3MapE
	.section	.text._ZN2v88internal9StubCache25SecondaryOffsetForTestingENS0_4NameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9StubCache25SecondaryOffsetForTestingENS0_4NameEi
	.type	_ZN2v88internal9StubCache25SecondaryOffsetForTestingENS0_4NameEi, @function
_ZN2v88internal9StubCache25SecondaryOffsetForTestingENS0_4NameEi:
.LFB23033:
	.cfi_startproc
	endbr64
	subl	%edi, %esi
	leal	-1318279451(%rsi), %eax
	andl	$2044, %eax
	ret
	.cfi_endproc
.LFE23033:
	.size	_ZN2v88internal9StubCache25SecondaryOffsetForTestingENS0_4NameEi, .-_ZN2v88internal9StubCache25SecondaryOffsetForTestingENS0_4NameEi
	.section	.text._ZN2v88internal9StubCache3SetENS0_4NameENS0_3MapENS0_11MaybeObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9StubCache3SetENS0_4NameENS0_3MapENS0_11MaybeObjectE
	.type	_ZN2v88internal9StubCache3SetENS0_4NameENS0_3MapENS0_11MaybeObjectE, @function
_ZN2v88internal9StubCache3SetENS0_4NameENS0_3MapENS0_11MaybeObjectE:
.LFB23034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	shrq	$13, %rax
	xorl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	addl	7(%rsi), %eax
	movl	$166, %esi
	movq	%rcx, -56(%rbp)
	andl	$8188, %eax
	leal	(%rax,%rax,2), %ebx
	movq	61440(%rdi), %rax
	addl	%ebx, %ebx
	movslq	%ebx, %rbx
	addq	%rdi, %rbx
	leaq	41184(%rax), %rdi
	movq	8(%rbx), %r15
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	-56(%rbp), %rcx
	cmpq	%rax, %r15
	je	.L16
	movq	16(%rbx), %rdx
	testb	$1, %dl
	jne	.L24
.L16:
	movq	%r14, (%rbx)
	movq	%rcx, 8(%rbx)
	movq	%r13, 16(%rbx)
	movq	61440(%r12), %rax
	movq	40960(%rax), %rbx
	cmpb	$0, 6976(%rbx)
	je	.L17
	movq	6968(%rbx), %rax
.L18:
	testq	%rax, %rax
	je	.L14
	addl	$1, (%rax)
.L14:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	%rdx, %rax
	movdqu	(%rbx), %xmm0
	shrq	$13, %rax
	xorl	%edx, %eax
	addl	7(%rsi), %eax
	andl	$8188, %eax
	subl	%esi, %eax
	subl	$1318279451, %eax
	andl	$2044, %eax
	leal	(%rax,%rax,2), %eax
	addl	%eax, %eax
	cltq
	movups	%xmm0, 49152(%r12,%rax)
	movq	16(%rbx), %rdx
	movq	%rdx, 49168(%r12,%rax)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L17:
	movb	$1, 6976(%rbx)
	leaq	6952(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6968(%rbx)
	jmp	.L18
	.cfi_endproc
.LFE23034:
	.size	_ZN2v88internal9StubCache3SetENS0_4NameENS0_3MapENS0_11MaybeObjectE, .-_ZN2v88internal9StubCache3SetENS0_4NameENS0_3MapENS0_11MaybeObjectE
	.section	.text._ZN2v88internal9StubCache3GetENS0_4NameENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9StubCache3GetENS0_4NameENS0_3MapE
	.type	_ZN2v88internal9StubCache3GetENS0_4NameENS0_3MapE, @function
_ZN2v88internal9StubCache3GetENS0_4NameENS0_3MapE:
.LFB23035:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	shrq	$13, %rax
	xorl	%edx, %eax
	addl	7(%rsi), %eax
	andl	$8188, %eax
	leal	(%rax,%rax,2), %ecx
	addl	%ecx, %ecx
	movslq	%ecx, %rcx
	addq	%rdi, %rcx
	cmpq	(%rcx), %rsi
	je	.L30
.L26:
	subl	%esi, %eax
	subl	$1318279451, %eax
	andl	$2044, %eax
	leal	(%rax,%rax,2), %eax
	addl	%eax, %eax
	cltq
	leaq	49152(%rdi,%rax), %rax
	cmpq	(%rax), %rsi
	je	.L31
.L28:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	cmpq	%rdx, 16(%rcx)
	jne	.L26
	movq	8(%rcx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	cmpq	%rdx, 16(%rax)
	jne	.L28
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE23035:
	.size	_ZN2v88internal9StubCache3GetENS0_4NameENS0_3MapE, .-_ZN2v88internal9StubCache3GetENS0_4NameENS0_3MapE
	.section	.text._ZN2v88internal9StubCache5ClearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9StubCache5ClearEv
	.type	_ZN2v88internal9StubCache5ClearEv, @function
_ZN2v88internal9StubCache5ClearEv:
.LFB23036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$166, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	61440(%rdi), %rax
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	61440(%rbx), %rdx
	pxor	%xmm2, %xmm2
	movq	%rax, %xmm0
	leaq	49152(%rbx), %rax
	movq	128(%rdx), %xmm3
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, %rdx
	punpcklqdq	%xmm3, %xmm3
	movdqa	%xmm3, %xmm1
	punpcklqdq	%xmm0, %xmm1
	shufpd	$1, %xmm3, %xmm0
	shufpd	$2, %xmm2, %xmm0
	shufpd	$2, %xmm3, %xmm2
	.p2align 4,,10
	.p2align 3
.L33:
	movups	%xmm1, (%rdx)
	addq	$48, %rdx
	movups	%xmm2, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rdx, %rax
	jne	.L33
	pxor	%xmm2, %xmm2
	leaq	61440(%rbx), %rdx
	shufpd	$2, %xmm3, %xmm2
	.p2align 4,,10
	.p2align 3
.L34:
	movups	%xmm1, (%rax)
	addq	$48, %rax
	movups	%xmm2, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L34
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23036:
	.size	_ZN2v88internal9StubCache5ClearEv, .-_ZN2v88internal9StubCache5ClearEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal9StubCacheC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal9StubCacheC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal9StubCacheC2EPNS0_7IsolateE:
.LFB28742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28742:
	.size	_GLOBAL__sub_I__ZN2v88internal9StubCacheC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal9StubCacheC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal9StubCacheC2EPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
