	.file	"stress-marking-observer.cc"
	.text
	.section	.text._ZN2v88internal18AllocationObserver15GetNextStepSizeEv,"axG",@progbits,_ZN2v88internal18AllocationObserver15GetNextStepSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18AllocationObserver15GetNextStepSizeEv
	.type	_ZN2v88internal18AllocationObserver15GetNextStepSizeEv, @function
_ZN2v88internal18AllocationObserver15GetNextStepSizeEv:
.LFB7286:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE7286:
	.size	_ZN2v88internal18AllocationObserver15GetNextStepSizeEv, .-_ZN2v88internal18AllocationObserver15GetNextStepSizeEv
	.section	.text._ZN2v88internal21StressMarkingObserverD2Ev,"axG",@progbits,_ZN2v88internal21StressMarkingObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21StressMarkingObserverD2Ev
	.type	_ZN2v88internal21StressMarkingObserverD2Ev, @function
_ZN2v88internal21StressMarkingObserverD2Ev:
.LFB10640:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10640:
	.size	_ZN2v88internal21StressMarkingObserverD2Ev, .-_ZN2v88internal21StressMarkingObserverD2Ev
	.weak	_ZN2v88internal21StressMarkingObserverD1Ev
	.set	_ZN2v88internal21StressMarkingObserverD1Ev,_ZN2v88internal21StressMarkingObserverD2Ev
	.section	.text._ZN2v88internal21StressMarkingObserver4StepEimm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21StressMarkingObserver4StepEimm
	.type	_ZN2v88internal21StressMarkingObserver4StepEimm, @function
_ZN2v88internal21StressMarkingObserver4StepEimm:
.LFB8663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	_ZN2v88internal4Heap49StartIncrementalMarkingIfAllocationLimitIsReachedEiNS_15GCCallbackFlagsE@PLT
	movq	24(%rbx), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2064(%rax), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal18IncrementalMarking20EnsureBlackAllocatedEmm@PLT
	.cfi_endproc
.LFE8663:
	.size	_ZN2v88internal21StressMarkingObserver4StepEimm, .-_ZN2v88internal21StressMarkingObserver4StepEimm
	.section	.text._ZN2v88internal21StressMarkingObserverD0Ev,"axG",@progbits,_ZN2v88internal21StressMarkingObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21StressMarkingObserverD0Ev
	.type	_ZN2v88internal21StressMarkingObserverD0Ev, @function
_ZN2v88internal21StressMarkingObserverD0Ev:
.LFB10642:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10642:
	.size	_ZN2v88internal21StressMarkingObserverD0Ev, .-_ZN2v88internal21StressMarkingObserverD0Ev
	.section	.text._ZN2v88internal21StressMarkingObserverC2EPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21StressMarkingObserverC2EPNS0_4HeapE
	.type	_ZN2v88internal21StressMarkingObserverC2EPNS0_4HeapE, @function
_ZN2v88internal21StressMarkingObserverC2EPNS0_4HeapE:
.LFB8661:
	.cfi_startproc
	endbr64
	movdqa	.LC0(%rip), %xmm0
	leaq	16+_ZTVN2v88internal21StressMarkingObserverE(%rip), %rax
	movq	%rsi, 24(%rdi)
	movq	%rax, (%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE8661:
	.size	_ZN2v88internal21StressMarkingObserverC2EPNS0_4HeapE, .-_ZN2v88internal21StressMarkingObserverC2EPNS0_4HeapE
	.globl	_ZN2v88internal21StressMarkingObserverC1EPNS0_4HeapE
	.set	_ZN2v88internal21StressMarkingObserverC1EPNS0_4HeapE,_ZN2v88internal21StressMarkingObserverC2EPNS0_4HeapE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21StressMarkingObserverC2EPNS0_4HeapE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21StressMarkingObserverC2EPNS0_4HeapE, @function
_GLOBAL__sub_I__ZN2v88internal21StressMarkingObserverC2EPNS0_4HeapE:
.LFB10684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10684:
	.size	_GLOBAL__sub_I__ZN2v88internal21StressMarkingObserverC2EPNS0_4HeapE, .-_GLOBAL__sub_I__ZN2v88internal21StressMarkingObserverC2EPNS0_4HeapE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21StressMarkingObserverC2EPNS0_4HeapE
	.weak	_ZTVN2v88internal21StressMarkingObserverE
	.section	.data.rel.ro.local._ZTVN2v88internal21StressMarkingObserverE,"awG",@progbits,_ZTVN2v88internal21StressMarkingObserverE,comdat
	.align 8
	.type	_ZTVN2v88internal21StressMarkingObserverE, @object
	.size	_ZTVN2v88internal21StressMarkingObserverE, 48
_ZTVN2v88internal21StressMarkingObserverE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal21StressMarkingObserverD1Ev
	.quad	_ZN2v88internal21StressMarkingObserverD0Ev
	.quad	_ZN2v88internal21StressMarkingObserver4StepEimm
	.quad	_ZN2v88internal18AllocationObserver15GetNextStepSizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	64
	.quad	64
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
