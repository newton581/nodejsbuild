	.file	"array-buffer-tracker.cc"
	.text
	.section	.rodata._ZN2v88internal23LocalArrayBufferTrackerD2Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"array_buffers_.empty()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal23LocalArrayBufferTrackerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalArrayBufferTrackerD2Ev
	.type	_ZN2v88internal23LocalArrayBufferTrackerD2Ev, @function
_ZN2v88internal23LocalArrayBufferTrackerD2Ev:
.LFB19532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 32(%rdi)
	jne	.L12
	movq	24(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L4
.L3:
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	addq	$56, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L1
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19532:
	.size	_ZN2v88internal23LocalArrayBufferTrackerD2Ev, .-_ZN2v88internal23LocalArrayBufferTrackerD2Ev
	.globl	_ZN2v88internal23LocalArrayBufferTrackerD1Ev
	.set	_ZN2v88internal23LocalArrayBufferTrackerD1Ev,_ZN2v88internal23LocalArrayBufferTrackerD2Ev
	.section	.text._ZN2v88internal18ArrayBufferTracker7FreeAllEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ArrayBufferTracker7FreeAllEPNS0_4PageE
	.type	_ZN2v88internal18ArrayBufferTracker7FreeAllEPNS0_4PageE, @function
_ZN2v88internal18ArrayBufferTracker7FreeAllEPNS0_4PageE:
.LFB19540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	248(%rdi), %rbx
	movq	%rdi, -72(%rbp)
	testq	%rbx, %rbx
	je	.L13
	movq	(%rbx), %rax
	movq	24(%rbx), %r12
	xorl	%r13d, %r13d
	movq	24(%rax), %rax
	subq	$37592, %rax
	movq	%rax, -56(%rbp)
	leaq	24(%rbx), %rax
	movq	%rax, -64(%rbp)
	testq	%r12, %r12
	je	.L41
	.p2align 4,,10
	.p2align 3
.L16:
	movq	24(%r12), %r15
	movq	-56(%rbp), %rdi
	pushq	40(%r12)
	pushq	32(%r12)
	pushq	24(%r12)
	pushq	16(%r12)
	call	_ZN2v88internal13JSArrayBuffer16FreeBackingStoreEPNS0_7IsolateENS1_10AllocationE@PLT
	movq	16(%rbx), %rcx
	movq	48(%r12), %rax
	xorl	%edx, %edx
	movq	8(%rbx), %r10
	addq	$32, %rsp
	divq	%rcx
	leaq	0(,%rdx,8), %r11
	movq	%rdx, %r9
	leaq	(%r10,%r11), %rax
	movq	(%rax), %rdx
	movq	%rdx, %r8
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r8, %rdi
	movq	(%r8), %r8
	cmpq	%r12, %r8
	jne	.L18
	movq	(%r12), %r14
	cmpq	%rdi, %rdx
	je	.L42
	testq	%r14, %r14
	je	.L24
	movq	48(%r14), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r9
	je	.L21
	movq	%rdi, (%r10,%rdx,8)
.L23:
	movq	(%r12), %r14
	addq	%r15, %r13
	movq	%r14, (%rdi)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	32(%rbx), %rax
	subq	$1, %rax
	movq	%rax, 32(%rbx)
	testq	%r14, %r14
	je	.L25
.L27:
	movq	%r14, %r12
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L42:
	testq	%r14, %r14
	je	.L29
	movq	48(%r14), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L43
.L21:
	movq	%r14, (%rdi)
	movq	%r12, %rdi
	addq	%r15, %r13
	call	_ZdlPv@PLT
	subq	$1, 32(%rbx)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L24:
	movq	$0, (%rdi)
	movq	%r8, %rdi
	addq	%r15, %r13
	call	_ZdlPv@PLT
	movq	32(%rbx), %rax
	subq	$1, %rax
	movq	%rax, 32(%rbx)
.L25:
	testq	%r13, %r13
	je	.L17
	movq	(%rbx), %rax
	lock subq	%r13, 200(%rax)
	movq	80(%rax), %rax
	movq	48(%rax), %rdx
	lock subq	%r13, (%rdx)
	movq	64(%rax), %rax
	lock subq	%r13, 176(%rax)
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	lock addq	%r13, 56(%rax)
.L41:
	movq	32(%rbx), %rax
.L17:
	testq	%rax, %rax
	je	.L44
.L13:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	%rdi, (%r10,%rdx,8)
	movq	8(%rbx), %rax
	addq	%r11, %rax
	movq	(%rax), %rdx
	cmpq	-64(%rbp), %rdx
	je	.L45
.L22:
	movq	$0, (%rax)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rdi, %rdx
	cmpq	-64(%rbp), %rdx
	jne	.L22
.L45:
	movq	%r14, 24(%rbx)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L44:
	movq	-72(%rbp), %rdi
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11MemoryChunk19ReleaseLocalTrackerEv@PLT
	.cfi_endproc
.LFE19540:
	.size	_ZN2v88internal18ArrayBufferTracker7FreeAllEPNS0_4PageE, .-_ZN2v88internal18ArrayBufferTracker7FreeAllEPNS0_4PageE
	.section	.text._ZN2v88internal18ArrayBufferTracker9IsTrackedENS0_13JSArrayBufferE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ArrayBufferTracker9IsTrackedENS0_13JSArrayBufferE
	.type	_ZN2v88internal18ArrayBufferTracker9IsTrackedENS0_13JSArrayBufferE, @function
_ZN2v88internal18ArrayBufferTracker9IsTrackedENS0_13JSArrayBufferE:
.LFB19546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	andq	$-262144, %r13
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	160(%r13), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	248(%r13), %rcx
	xorl	%r13d, %r13d
	testq	%rcx, %rcx
	je	.L47
	movq	%rbx, %r8
	movq	16(%rcx), %rdi
	xorl	%edx, %edx
	shrq	$3, %r8
	movq	%r8, %rax
	divq	%rdi
	movq	8(%rcx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L47
	movq	(%rax), %rcx
	movq	48(%rcx), %rsi
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L49:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L54
	movq	48(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L54
.L50:
	cmpq	%r8, %rsi
	jne	.L49
	cmpq	8(%rcx), %rbx
	jne	.L49
	movl	$1, %r13d
.L47:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L47
	.cfi_endproc
.LFE19546:
	.size	_ZN2v88internal18ArrayBufferTracker9IsTrackedENS0_13JSArrayBufferE, .-_ZN2v88internal18ArrayBufferTracker9IsTrackedENS0_13JSArrayBufferE
	.section	.text._ZN2v88internal18ArrayBufferTracker8TearDownEPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ArrayBufferTracker8TearDownEPNS0_4HeapE
	.type	_ZN2v88internal18ArrayBufferTracker8TearDownEPNS0_4HeapE, @function
_ZN2v88internal18ArrayBufferTracker8TearDownEPNS0_4HeapE:
.LFB19547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	256(%rdi), %rax
	movq	32(%rax), %rbx
	testq	%rbx, %rbx
	je	.L59
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%rbx, %rdi
	call	_ZN2v88internal18ArrayBufferTracker7FreeAllEPNS0_4PageE
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L60
.L59:
	movq	248(%r12), %rax
	cmpb	$0, 344(%rax)
	je	.L58
	movq	240(%rax), %rbx
	testq	%rbx, %rbx
	je	.L58
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%rbx, %rdi
	call	_ZN2v88internal18ArrayBufferTracker7FreeAllEPNS0_4PageE
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L62
.L58:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19547:
	.size	_ZN2v88internal18ArrayBufferTracker8TearDownEPNS0_4HeapE, .-_ZN2v88internal18ArrayBufferTracker8TearDownEPNS0_4HeapE
	.section	.rodata._ZNSt6vectorIN2v88internal13JSArrayBuffer10AllocationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal13JSArrayBuffer10AllocationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal13JSArrayBuffer10AllocationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal13JSArrayBuffer10AllocationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal13JSArrayBuffer10AllocationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal13JSArrayBuffer10AllocationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r14
	movabsq	$288230376151711743, %rdi
	movq	%rcx, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L89
	movq	%rsi, %r12
	subq	%r14, %rsi
	testq	%rax, %rax
	je	.L81
	movabsq	$9223372036854775776, %r13
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L90
.L74:
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	leaq	(%rax,%r13), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	32(%rbx), %r13
.L80:
	movdqu	(%rdx), %xmm3
	movdqu	16(%rdx), %xmm4
	movups	%xmm3, (%rbx,%rsi)
	movups	%xmm4, 16(%rbx,%rsi)
	cmpq	%r14, %r12
	je	.L76
	movq	%rbx, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L77:
	movdqu	(%rax), %xmm1
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rax), %xmm2
	movups	%xmm2, -16(%rdx)
	cmpq	%rax, %r12
	jne	.L77
	movq	%r12, %rax
	subq	%r14, %rax
	leaq	32(%rbx,%rax), %r13
.L76:
	cmpq	%rcx, %r12
	je	.L78
	subq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r13
.L78:
	testq	%r14, %r14
	je	.L79
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L79:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r13, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L75
	movq	$0, -56(%rbp)
	movl	$32, %r13d
	xorl	%ebx, %ebx
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$32, %r13d
	jmp	.L74
.L75:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	movq	%rdi, %r13
	salq	$5, %r13
	jmp	.L74
.L89:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23742:
	.size	_ZNSt6vectorIN2v88internal13JSArrayBuffer10AllocationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal13JSArrayBuffer10AllocationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE4swapERSJ_,"axG",@progbits,_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE4swapERSJ_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE4swapERSJ_
	.type	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE4swapERSJ_, @function
_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE4swapERSJ_:
.LFB23745:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movss	32(%rdi), %xmm0
	leaq	48(%rdi), %rcx
	leaq	48(%rsi), %r8
	movdqu	32(%rsi), %xmm1
	movups	%xmm1, 32(%rdi)
	movq	%rax, 40(%rsi)
	movss	%xmm0, 32(%rsi)
	movq	(%rdi), %rax
	cmpq	%rax, %rcx
	je	.L103
	movq	(%rsi), %rdx
	cmpq	%rdx, %r8
	je	.L104
	movq	%rdx, (%rdi)
	movq	%rax, (%rsi)
.L93:
	movq	8(%rdi), %rax
	movq	8(%rsi), %rdx
	movq	%rdx, 8(%rdi)
	movq	16(%rsi), %rdx
	movq	%rax, 8(%rsi)
	movq	16(%rdi), %rax
	movq	%rdx, 16(%rdi)
	movq	24(%rsi), %rdx
	movq	%rax, 16(%rsi)
	movq	24(%rdi), %rax
	movq	%rdx, 24(%rdi)
	movq	48(%rsi), %rdx
	movq	%rax, 24(%rsi)
	movq	48(%rdi), %rax
	movq	%rdx, 48(%rdi)
	movq	%rax, 48(%rsi)
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L95
	movq	48(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rdi)
	movq	(%rdi), %rax
	addq	$16, %rdi
	movq	%rdi, (%rax,%rdx,8)
.L95:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.L91
	movq	48(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rsi)
	movq	(%rsi), %rax
	addq	$16, %rsi
	movq	%rsi, (%rax,%rdx,8)
.L91:
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	movq	(%rsi), %rax
	cmpq	%r8, %rax
	je	.L93
	movq	%rax, (%rdi)
	movq	%r8, (%rsi)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%rax, (%rsi)
	movq	%rcx, (%rdi)
	jmp	.L93
	.cfi_endproc
.LFE23745:
	.size	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE4swapERSJ_, .-_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE4swapERSJ_
	.section	.text._ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm
	.type	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm, @function
_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm:
.LFB24097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L129
.L106:
	movq	%r14, 48(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L115
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L116:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L130
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L131
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L108:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L110
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L113:
	testq	%rsi, %rsi
	je	.L110
.L111:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	48(%rcx), %rax
	divq	%r13
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L112
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L118
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L111
	.p2align 4,,10
	.p2align 3
.L110:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L114
	call	_ZdlPv@PLT
.L114:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r15, (%rbx)
	divq	%r13
	movq	%r13, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L115:
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L117
	movq	48(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L117:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%rdx, %rdi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L108
.L131:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE24097:
	.size	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm, .-_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm
	.section	.rodata._ZN2v88internal18ArrayBufferTracker27PrepareToFreeDeadInNewSpaceEPNS0_4HeapE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"empty"
	.section	.text._ZN2v88internal18ArrayBufferTracker27PrepareToFreeDeadInNewSpaceEPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ArrayBufferTracker27PrepareToFreeDeadInNewSpaceEPNS0_4HeapE
	.type	_ZN2v88internal18ArrayBufferTracker27PrepareToFreeDeadInNewSpaceEPNS0_4HeapE, @function
_ZN2v88internal18ArrayBufferTracker27PrepareToFreeDeadInNewSpaceEPNS0_4HeapE:
.LFB19539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	248(%rdi), %rax
	movq	400(%rax), %rax
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L132
	leaq	-176(%rbp), %rax
	movq	%rax, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L133:
	movq	-184(%rbp), %rax
	movq	248(%rax), %r13
	testq	%r13, %r13
	je	.L134
	movq	$0, -88(%rbp)
	leaq	-64(%rbp), %rax
	pxor	%xmm2, %xmm2
	movl	$0x3f800000, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	24(%r13), %r12
	movq	$0, -160(%rbp)
	movq	%rax, -232(%rbp)
	movq	%rax, -112(%rbp)
	movq	$1, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	testq	%r12, %r12
	je	.L136
	movq	%r13, -200(%rbp)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L137:
	movq	24(%r12), %rcx
	movq	-168(%rbp), %rsi
	addq	%rcx, -192(%rbp)
	cmpq	-160(%rbp), %rsi
	je	.L191
	movdqu	16(%r12), %xmm0
	movups	%xmm0, (%rsi)
	movdqu	32(%r12), %xmm1
	addq	$32, -168(%rbp)
	movups	%xmm1, 16(%rsi)
.L147:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L192
.L135:
	movq	8(%r12), %rax
	movq	-1(%rax), %r14
	testb	$1, %r14b
	jne	.L137
	leaq	1(%r14), %r15
	movq	%r15, %rbx
	andq	$-262144, %rbx
	movq	160(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	248(%rbx), %r8
	testq	%r8, %r8
	je	.L193
.L139:
	movq	32(%r14), %rdx
	movq	24(%r12), %r13
	movl	$56, %edi
	movq	%r8, -224(%rbp)
	movq	%rdx, -216(%rbp)
	movl	40(%r14), %r14d
	call	_Znwm@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %r8
	movq	%r15, %r10
	shrl	$4, %r14d
	movq	%r15, 8(%rax)
	shrq	$3, %r10
	movq	%rax, %rcx
	andl	$1, %r14d
	movq	%rdx, 16(%rax)
	movq	%rdx, 32(%rax)
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%r13, 24(%rax)
	movb	%r14b, 40(%rax)
	movq	16(%r8), %r9
	movq	%r10, %rax
	divq	%r9
	movq	8(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rsi
	testq	%rax, %rax
	je	.L142
	movq	(%rax), %rdi
	movq	48(%rdi), %r14
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L143:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L142
	movq	48(%rdi), %r14
	xorl	%edx, %edx
	movq	%r14, %rax
	divq	%r9
	cmpq	%rdx, %rsi
	jne	.L142
.L145:
	cmpq	%r14, %r10
	jne	.L143
	cmpq	8(%rdi), %r15
	jne	.L143
	movq	%rcx, %rdi
	call	_ZdlPv@PLT
.L156:
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	lock subq	%r13, 200(%rax)
	lock addq	%r13, 200(%rbx)
	movq	80(%rbx), %rdx
	movq	80(%rax), %rax
	cmpq	%rax, %rdx
	je	.L146
	movq	48(%rax), %rax
	lock subq	%r13, (%rax)
	movq	48(%rdx), %rax
	lock addq	%r13, (%rax)
.L146:
	movq	-208(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L135
.L192:
	cmpq	$0, -192(%rbp)
	movq	-200(%rbp), %r13
	jne	.L194
.L136:
	leaq	-112(%rbp), %rsi
	leaq	8(%r13), %rdi
	call	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE4swapERSJ_
	movq	0(%r13), %rax
	pxor	%xmm4, %xmm4
	movdqa	-176(%rbp), %xmm3
	leaq	-144(%rbp), %rsi
	movaps	%xmm4, -176(%rbp)
	movq	24(%rax), %rax
	movaps	%xmm3, -144(%rbp)
	movq	2040(%rax), %rdi
	movq	-160(%rbp), %rax
	movq	$0, -160(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE@PLT
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L149
	call	_ZdlPv@PLT
.L149:
	movq	-96(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L153
	.p2align 4,,10
	.p2align 3
.L150:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L150
.L153:
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, -88(%rbp)
	movq	-112(%rbp), %rdi
	movq	$0, -96(%rbp)
	cmpq	-232(%rbp), %rdi
	je	.L151
	call	_ZdlPv@PLT
.L151:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L154
	call	_ZdlPv@PLT
.L154:
	cmpq	$0, 32(%r13)
	jne	.L195
.L134:
	movq	-184(%rbp), %rax
	movq	224(%rax), %rax
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	jne	.L133
.L132:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	leaq	8(%r8), %rdi
	movq	%r10, %rdx
	movl	$1, %r8d
	call	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L195:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L191:
	movq	-240(%rbp), %rdi
	leaq	16(%r12), %rdx
	call	_ZNSt6vectorIN2v88internal13JSArrayBuffer10AllocationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L194:
	movq	0(%r13), %rax
	movq	-192(%rbp), %rcx
	lock subq	%rcx, 200(%rax)
	movq	80(%rax), %rax
	movq	48(%rax), %rdx
	lock subq	%rcx, (%rdx)
	movq	64(%rax), %rax
	lock subq	%rcx, 176(%rax)
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	lock addq	%rcx, 56(%rax)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%rbx, %rdi
	call	_ZN2v88internal4Page20AllocateLocalTrackerEv@PLT
	movq	248(%rbx), %r8
	jmp	.L139
.L196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19539:
	.size	_ZN2v88internal18ArrayBufferTracker27PrepareToFreeDeadInNewSpaceEPNS0_4HeapE, .-_ZN2v88internal18ArrayBufferTracker27PrepareToFreeDeadInNewSpaceEPNS0_4HeapE
	.section	.text._ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRS6_EEES3_INS8_14_Node_iteratorIS6_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRS6_EEES3_INS8_14_Node_iteratorIS6_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRS6_EEES3_INS8_14_Node_iteratorIS6_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRS6_EEES3_INS8_14_Node_iteratorIS6_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRS6_EEES3_INS8_14_Node_iteratorIS6_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB24107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$56, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movq	(%rbx), %rcx
	movdqu	(%rbx), %xmm0
	xorl	%edx, %edx
	movdqu	16(%rbx), %xmm1
	movq	$0, (%rax)
	movq	%rax, %rdi
	movups	%xmm0, 8(%rax)
	movq	%rcx, %r9
	movq	8(%r12), %r8
	movups	%xmm1, 24(%rax)
	movq	32(%rbx), %rax
	shrq	$3, %r9
	movq	%rax, 40(%rdi)
	movq	%r9, %rax
	divq	%r8
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L198
	movq	(%rax), %rbx
	movq	48(%rbx), %rsi
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L199:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L198
	movq	48(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L198
.L201:
	cmpq	%rsi, %r9
	jne	.L199
	cmpq	8(%rbx), %rcx
	jne	.L199
	call	_ZdlPv@PLT
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	%rdi, %rcx
	movq	%r9, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	%r10, %rsi
	call	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24107:
	.size	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRS6_EEES3_INS8_14_Node_iteratorIS6_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRS6_EEES3_INS8_14_Node_iteratorIS6_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZN2v88internal18ArrayBufferTracker14ProcessBuffersEPNS0_4PageENS1_14ProcessingModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ArrayBufferTracker14ProcessBuffersEPNS0_4PageENS1_14ProcessingModeE
	.type	_ZN2v88internal18ArrayBufferTracker14ProcessBuffersEPNS0_4PageENS1_14ProcessingModeE, @function
_ZN2v88internal18ArrayBufferTracker14ProcessBuffersEPNS0_4PageENS1_14ProcessingModeE:
.LFB19544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -212(%rbp)
	movq	248(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	testq	%r13, %r13
	je	.L212
	leaq	-64(%rbp), %rax
	movq	$0, -88(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -240(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-112(%rbp), %rax
	movl	$0x3f800000, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	24(%r13), %r12
	movq	%rax, -192(%rbp)
	leaq	-176(%rbp), %rax
	movq	$0, -160(%rbp)
	movq	$1, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -184(%rbp)
	movq	%rax, -248(%rbp)
	movaps	%xmm0, -176(%rbp)
	testq	%r12, %r12
	je	.L215
	movq	%r13, -200(%rbp)
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L216:
	cmpl	$1, -212(%rbp)
	jne	.L219
	movq	-192(%rbp), %rdi
	leaq	8(%r12), %rsi
	call	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRS6_EEES3_INS8_14_Node_iteratorIS6_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
.L220:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L268
.L214:
	movq	8(%r12), %rax
	movq	-1(%rax), %r14
	testb	$1, %r14b
	jne	.L216
	leaq	1(%r14), %r8
	movq	%r8, %rbx
	movq	%r8, -224(%rbp)
	andq	$-262144, %rbx
	movq	160(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	248(%rbx), %r15
	movq	-224(%rbp), %r8
	testq	%r15, %r15
	je	.L269
.L218:
	movq	32(%r14), %rdx
	movq	24(%r12), %r13
	movl	$56, %edi
	movq	%r8, -232(%rbp)
	movq	%rdx, -224(%rbp)
	movl	40(%r14), %r14d
	call	_Znwm@PLT
	movq	-232(%rbp), %r8
	movq	-224(%rbp), %rdx
	shrl	$4, %r14d
	movq	$0, (%rax)
	movq	%rax, %rdi
	andl	$1, %r14d
	movq	%r8, %r10
	movq	%rdx, 16(%rax)
	movq	%rdx, 32(%rax)
	shrq	$3, %r10
	xorl	%edx, %edx
	movq	%r8, 8(%rax)
	movq	%r13, 24(%rax)
	movb	%r14b, 40(%rax)
	movq	16(%r15), %r14
	movq	%r10, %rax
	divq	%r14
	movq	8(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rsi
	testq	%rax, %rax
	je	.L223
	movq	(%rax), %rcx
	movq	48(%rcx), %r11
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L224:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L223
	movq	48(%rcx), %r11
	xorl	%edx, %edx
	movq	%r11, %rax
	divq	%r14
	cmpq	%rdx, %rsi
	jne	.L223
.L226:
	cmpq	%r11, %r10
	jne	.L224
	cmpq	8(%rcx), %r8
	jne	.L224
	call	_ZdlPv@PLT
.L235:
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	lock subq	%r13, 200(%rax)
	lock addq	%r13, 200(%rbx)
	movq	80(%rbx), %rdx
	movq	80(%rax), %rax
	cmpq	%rax, %rdx
	je	.L227
	movq	48(%rax), %rax
	lock subq	%r13, (%rax)
	movq	48(%rdx), %rax
	lock addq	%r13, (%rax)
.L227:
	movq	-208(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L214
.L268:
	cmpq	$0, -184(%rbp)
	movq	-200(%rbp), %r13
	jne	.L270
.L215:
	movq	-192(%rbp), %rsi
	leaq	8(%r13), %rdi
	call	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE4swapERSJ_
	movq	0(%r13), %rax
	pxor	%xmm0, %xmm0
	movdqa	-176(%rbp), %xmm3
	leaq	-144(%rbp), %rsi
	movaps	%xmm0, -176(%rbp)
	movq	24(%rax), %rax
	movaps	%xmm3, -144(%rbp)
	movq	2040(%rax), %rdi
	movq	-160(%rbp), %rax
	movq	$0, -160(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal20ArrayBufferCollector29QueueOrFreeGarbageAllocationsESt6vectorINS0_13JSArrayBuffer10AllocationESaIS4_EE@PLT
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L229
	call	_ZdlPv@PLT
.L229:
	movq	-96(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L233
	.p2align 4,,10
	.p2align 3
.L230:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L230
.L233:
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, -88(%rbp)
	movq	-112(%rbp), %rdi
	movq	$0, -96(%rbp)
	cmpq	-240(%rbp), %rdi
	je	.L231
	call	_ZdlPv@PLT
.L231:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L234
	call	_ZdlPv@PLT
.L234:
	cmpq	$0, 32(%r13)
	sete	%al
.L212:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L271
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	24(%r12), %rcx
	movq	-168(%rbp), %rsi
	addq	%rcx, -184(%rbp)
	cmpq	-160(%rbp), %rsi
	je	.L272
	movdqu	16(%r12), %xmm1
	movups	%xmm1, (%rsi)
	movdqu	32(%r12), %xmm2
	addq	$32, -168(%rbp)
	movups	%xmm2, 16(%rsi)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L223:
	addq	$8, %r15
	movq	%rdi, %rcx
	movl	$1, %r8d
	movq	%r10, %rdx
	movq	%r15, %rdi
	call	_ZNSt10_HashtableIN2v88internal13JSArrayBufferESt4pairIKS2_NS2_10AllocationEESaIS6_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_23LocalArrayBufferTracker6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb1EEEm
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L270:
	movq	0(%r13), %rax
	movq	-184(%rbp), %rcx
	lock subq	%rcx, 200(%rax)
	movq	80(%rax), %rax
	movq	48(%rax), %rdx
	lock subq	%rcx, (%rdx)
	movq	64(%rax), %rax
	lock subq	%rcx, 176(%rax)
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	lock addq	%rcx, 56(%rax)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L272:
	movq	-248(%rbp), %rdi
	leaq	16(%r12), %rdx
	call	_ZNSt6vectorIN2v88internal13JSArrayBuffer10AllocationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%rbx, %rdi
	movq	%r8, -224(%rbp)
	call	_ZN2v88internal4Page20AllocateLocalTrackerEv@PLT
	movq	248(%rbx), %r15
	movq	-224(%rbp), %r8
	jmp	.L218
.L271:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19544:
	.size	_ZN2v88internal18ArrayBufferTracker14ProcessBuffersEPNS0_4PageENS1_14ProcessingModeE, .-_ZN2v88internal18ArrayBufferTracker14ProcessBuffersEPNS0_4PageENS1_14ProcessingModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal23LocalArrayBufferTrackerD2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal23LocalArrayBufferTrackerD2Ev, @function
_GLOBAL__sub_I__ZN2v88internal23LocalArrayBufferTrackerD2Ev:
.LFB24735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24735:
	.size	_GLOBAL__sub_I__ZN2v88internal23LocalArrayBufferTrackerD2Ev, .-_GLOBAL__sub_I__ZN2v88internal23LocalArrayBufferTrackerD2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal23LocalArrayBufferTrackerD2Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
