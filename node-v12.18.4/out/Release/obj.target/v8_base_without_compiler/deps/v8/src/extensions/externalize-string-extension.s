	.file	"externalize-string-extension.cc"
	.text
	.section	.text._ZNK2v86String26ExternalStringResourceBase11IsCacheableEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.type	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv, @function
_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv:
.LFB2390:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2390:
	.size	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv, .-_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.section	.text._ZN2v86String26ExternalStringResourceBase7DisposeEv,"axG",@progbits,_ZN2v86String26ExternalStringResourceBase7DisposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.type	_ZN2v86String26ExternalStringResourceBase7DisposeEv, @function
_ZN2v86String26ExternalStringResourceBase7DisposeEv:
.LFB2391:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE2391:
	.size	_ZN2v86String26ExternalStringResourceBase7DisposeEv, .-_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.section	.text._ZNK2v86String26ExternalStringResourceBase4LockEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase4LockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase4LockEv
	.type	_ZNK2v86String26ExternalStringResourceBase4LockEv, @function
_ZNK2v86String26ExternalStringResourceBase4LockEv:
.LFB2392:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2392:
	.size	_ZNK2v86String26ExternalStringResourceBase4LockEv, .-_ZNK2v86String26ExternalStringResourceBase4LockEv
	.section	.text._ZNK2v86String26ExternalStringResourceBase6UnlockEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase6UnlockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.type	_ZNK2v86String26ExternalStringResourceBase6UnlockEv, @function
_ZNK2v86String26ExternalStringResourceBase6UnlockEv:
.LFB2393:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2393:
	.size	_ZNK2v86String26ExternalStringResourceBase6UnlockEv, .-_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.section	.text._ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,"axG",@progbits,_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.type	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, @function
_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE:
.LFB2488:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2488:
	.size	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, .-_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.text._ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE4dataEv,"axG",@progbits,_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE4dataEv
	.type	_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE4dataEv, @function
_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE4dataEv:
.LFB22070:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE22070:
	.size	_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE4dataEv, .-_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE4dataEv
	.section	.text._ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE6lengthEv,"axG",@progbits,_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE6lengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE6lengthEv
	.type	_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE6lengthEv, @function
_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE6lengthEv:
.LFB22071:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE22071:
	.size	_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE6lengthEv, .-_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE6lengthEv
	.section	.text._ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE4dataEv,"axG",@progbits,_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE4dataEv
	.type	_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE4dataEv, @function
_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE4dataEv:
.LFB22072:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE22072:
	.size	_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE4dataEv, .-_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE4dataEv
	.section	.text._ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE6lengthEv,"axG",@progbits,_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE6lengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE6lengthEv
	.type	_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE6lengthEv, @function
_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE6lengthEv:
.LFB22073:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE22073:
	.size	_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE6lengthEv, .-_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE6lengthEv
	.section	.text._ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED2Ev,"axG",@progbits,_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED2Ev
	.type	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED2Ev, @function
_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED2Ev:
.LFB20081:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L11
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE20081:
	.size	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED2Ev, .-_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED2Ev
	.weak	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED1Ev
	.set	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED1Ev,_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED2Ev
	.section	.text._ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED0Ev,"axG",@progbits,_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED0Ev
	.type	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED0Ev, @function
_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED0Ev:
.LFB20083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	_ZdaPv@PLT
.L14:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20083:
	.size	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED0Ev, .-_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED0Ev
	.section	.text._ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED2Ev,"axG",@progbits,_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED2Ev
	.type	_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED2Ev, @function
_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED2Ev:
.LFB20095:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L19
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	ret
	.cfi_endproc
.LFE20095:
	.size	_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED2Ev, .-_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED2Ev
	.weak	_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED1Ev
	.set	_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED1Ev,_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED2Ev
	.section	.text._ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED0Ev,"axG",@progbits,_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED0Ev
	.type	_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED0Ev, @function
_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED0Ev:
.LFB20097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L22
	call	_ZdaPv@PLT
.L22:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20097:
	.size	_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED0Ev, .-_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED0Ev
	.section	.rodata._ZN2v88internal26ExternalizeStringExtension9IsOneByteERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"isOneByteString() requires a single string argument."
	.section	.text._ZN2v88internal26ExternalizeStringExtension9IsOneByteERKNS_20FunctionCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ExternalizeStringExtension9IsOneByteERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal26ExternalizeStringExtension9IsOneByteERKNS_20FunctionCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal26ExternalizeStringExtension9IsOneByteERKNS_20FunctionCallbackInfoINS_5ValueEEE:
.LFB18036:
	.cfi_startproc
	endbr64
	cmpl	$1, 16(%rdi)
	jne	.L28
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L37
.L28:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	movq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L38
.L31:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L28
	movq	-1(%rax), %rax
	movq	(%rdi), %rdx
	movzwl	11(%rax), %eax
	movq	8(%rdx), %rcx
	andl	$8, %eax
	cmpw	$1, %ax
	sbbq	%rax, %rax
	andl	$8, %eax
	addq	$56, %rax
	movq	56(%rcx,%rax), %rax
	movq	%rax, 24(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rsi
	jmp	.L31
	.cfi_endproc
.LFE18036:
	.size	_ZN2v88internal26ExternalizeStringExtension9IsOneByteERKNS_20FunctionCallbackInfoINS_5ValueEEE, .-_ZN2v88internal26ExternalizeStringExtension9IsOneByteERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.section	.rodata._ZN2v88internal26ExternalizeStringExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"externalizeString"
	.section	.text._ZN2v88internal26ExternalizeStringExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ExternalizeStringExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.type	_ZN2v88internal26ExternalizeStringExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, @function
_ZN2v88internal26ExternalizeStringExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE:
.LFB18034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rsi, %r12
	pushq	%rbx
	movq	%r13, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-64(%rbp), %rsi
	movl	$18, %ecx
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	movq	%r13, %rdi
	seta	%bl
	sbbb	$0, %bl
	movsbl	%bl, %ebx
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	testl	%ebx, %ebx
	jne	.L40
	subq	$8, %rsp
	leaq	_ZN2v88internal26ExternalizeStringExtension11ExternalizeERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	pushq	$0
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	popq	%rsi
	popq	%rdi
.L41:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L44
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r9d
	pushq	$0
	xorl	%r8d, %r8d
	leaq	_ZN2v88internal26ExternalizeStringExtension9IsOneByteERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	popq	%rdx
	popq	%rcx
	jmp	.L41
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18034:
	.size	_ZN2v88internal26ExternalizeStringExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, .-_ZN2v88internal26ExternalizeStringExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.text._ZN2v88internal26ExternalizeStringExtensionD2Ev,"axG",@progbits,_ZN2v88internal26ExternalizeStringExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ExternalizeStringExtensionD2Ev
	.type	_ZN2v88internal26ExternalizeStringExtensionD2Ev, @function
_ZN2v88internal26ExternalizeStringExtensionD2Ev:
.LFB22050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	24(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L45
	movq	(%r12), %rax
	leaq	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L47
	movq	8(%r12), %rdi
	leaq	16+_ZTVN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEEE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L48
	call	_ZdaPv@PLT
.L48:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE22050:
	.size	_ZN2v88internal26ExternalizeStringExtensionD2Ev, .-_ZN2v88internal26ExternalizeStringExtensionD2Ev
	.weak	_ZN2v88internal26ExternalizeStringExtensionD1Ev
	.set	_ZN2v88internal26ExternalizeStringExtensionD1Ev,_ZN2v88internal26ExternalizeStringExtensionD2Ev
	.section	.text._ZN2v89ExtensionD2Ev,"axG",@progbits,_ZN2v89ExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v89ExtensionD2Ev
	.type	_ZN2v89ExtensionD2Ev, @function
_ZN2v89ExtensionD2Ev:
.LFB2485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	24(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L53
	movq	(%r12), %rax
	leaq	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L55
	movq	8(%r12), %rdi
	leaq	16+_ZTVN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEEE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L56
	call	_ZdaPv@PLT
.L56:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE2485:
	.size	_ZN2v89ExtensionD2Ev, .-_ZN2v89ExtensionD2Ev
	.weak	_ZN2v89ExtensionD1Ev
	.set	_ZN2v89ExtensionD1Ev,_ZN2v89ExtensionD2Ev
	.section	.text._ZN2v88internal26ExternalizeStringExtensionD0Ev,"axG",@progbits,_ZN2v88internal26ExternalizeStringExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ExternalizeStringExtensionD0Ev
	.type	_ZN2v88internal26ExternalizeStringExtensionD0Ev, @function
_ZN2v88internal26ExternalizeStringExtensionD0Ev:
.LFB22052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	24(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L62
	movq	0(%r13), %rax
	leaq	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L63
	movq	8(%r13), %rdi
	leaq	16+_ZTVN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEEE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L64
	call	_ZdaPv@PLT
.L64:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L62:
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L62
	.cfi_endproc
.LFE22052:
	.size	_ZN2v88internal26ExternalizeStringExtensionD0Ev, .-_ZN2v88internal26ExternalizeStringExtensionD0Ev
	.section	.text._ZN2v89ExtensionD0Ev,"axG",@progbits,_ZN2v89ExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v89ExtensionD0Ev
	.type	_ZN2v89ExtensionD0Ev, @function
_ZN2v89ExtensionD0Ev:
.LFB2487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	24(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L73
	movq	0(%r13), %rax
	leaq	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L74
	movq	8(%r13), %rdi
	leaq	16+_ZTVN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEEE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L75
	call	_ZdaPv@PLT
.L75:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L73:
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L73
	.cfi_endproc
.LFE2487:
	.size	_ZN2v89ExtensionD0Ev, .-_ZN2v89ExtensionD0Ev
	.section	.rodata._ZN2v88internal26ExternalizeStringExtension11ExternalizeERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"First parameter to externalizeString() must be a string."
	.align 8
.LC3:
	.string	"Second parameter to externalizeString() must be a boolean."
	.align 8
.LC4:
	.string	"string does not support externalization."
	.section	.rodata._ZN2v88internal26ExternalizeStringExtension11ExternalizeERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"externalizeString() failed."
	.section	.text._ZN2v88internal26ExternalizeStringExtension11ExternalizeERKNS_20FunctionCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ExternalizeStringExtension11ExternalizeERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal26ExternalizeStringExtension11ExternalizeERKNS_20FunctionCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal26ExternalizeStringExtension11ExternalizeERKNS_20FunctionCallbackInfoINS_5ValueEEE:
.LFB18035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	16(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L84
	movq	8(%rdi), %r12
	movq	(%r12), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L127
.L84:
	movq	(%rbx), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	8(%rax), %r12
.L124:
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L128
.L108:
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L83:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L84
	xorl	%r13d, %r13d
	cmpl	$1, %edx
	jne	.L130
.L94:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String23SupportsExternalizationEv@PLT
	testb	%al, %al
	je	.L131
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	testb	$8, 11(%rdx)
	je	.L110
	testb	%r13b, %r13b
	je	.L97
.L110:
	movslq	11(%rax), %rax
	leaq	16+_ZTVN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEEE(%rip), %r15
	movabsq	$4611686018427387900, %rdx
	cmpq	%rdx, %rax
	leaq	(%rax,%rax), %rdi
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movq	(%r12), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rax, %r14
	movl	11(%rdi), %ecx
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	movq	(%r12), %rax
	movl	$24, %edi
	movslq	11(%rax), %rdx
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, (%rax)
	movq	%rax, %rsi
	movq	%rax, %r13
	movq	%r14, 8(%rax)
	movq	%rdx, 16(%rax)
	call	_ZN2v86String12MakeExternalEPNS0_22ExternalStringResourceE@PLT
	testb	%al, %al
	jne	.L83
	movq	0(%r13), %rax
	leaq	_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L106
.L122:
	movq	8(%r13), %rdi
	movq	%r15, 0(%r13)
	testq	%rdi, %rdi
	je	.L107
	call	_ZdaPv@PLT
.L107:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L104:
	movq	(%rbx), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	8(%rax), %r12
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	-8(%r12), %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	movq	(%rbx), %rax
	je	.L89
	movq	8(%rax), %rsi
	cmpl	$1, 16(%rbx)
	leaq	88(%rsi), %rdi
	jle	.L91
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L91:
	call	_ZNK2v85Value12BooleanValueEPNS_7IsolateE@PLT
	movl	%eax, %r13d
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L132
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	leaq	88(%rax), %r12
	movq	88(%rax), %rax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L131:
	movq	(%rbx), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	movq	8(%rax), %r12
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rsi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L89:
	movq	8(%rax), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L97:
	movslq	11(%rax), %rdi
	leaq	16+_ZTVN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEEE(%rip), %r15
	call	_Znam@PLT
	movq	(%r12), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rax, %r14
	movl	11(%rdi), %ecx
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii@PLT
	movq	(%r12), %rax
	movl	$24, %edi
	movslq	11(%rax), %rdx
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, (%rax)
	movq	%rax, %rsi
	movq	%rax, %r13
	movq	%r14, 8(%rax)
	movq	%rdx, 16(%rax)
	call	_ZN2v86String12MakeExternalEPNS0_29ExternalOneByteStringResourceE@PLT
	testb	%al, %al
	jne	.L83
	movq	0(%r13), %rax
	leaq	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	je	.L122
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L104
.L129:
	call	__stack_chk_fail@PLT
.L132:
	movq	8(%rbx), %r12
	movq	(%r12), %rax
	jmp	.L94
	.cfi_endproc
.LFE18035:
	.size	_ZN2v88internal26ExternalizeStringExtension11ExternalizeERKNS_20FunctionCallbackInfoINS_5ValueEEE, .-_ZN2v88internal26ExternalizeStringExtension11ExternalizeERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal26ExternalizeStringExtension7kSourceE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal26ExternalizeStringExtension7kSourceE, @function
_GLOBAL__sub_I__ZN2v88internal26ExternalizeStringExtension7kSourceE:
.LFB22074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22074:
	.size	_GLOBAL__sub_I__ZN2v88internal26ExternalizeStringExtension7kSourceE, .-_GLOBAL__sub_I__ZN2v88internal26ExternalizeStringExtension7kSourceE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal26ExternalizeStringExtension7kSourceE
	.weak	_ZTVN2v89ExtensionE
	.section	.data.rel.ro.local._ZTVN2v89ExtensionE,"awG",@progbits,_ZTVN2v89ExtensionE,comdat
	.align 8
	.type	_ZTVN2v89ExtensionE, @object
	.size	_ZTVN2v89ExtensionE, 40
_ZTVN2v89ExtensionE:
	.quad	0
	.quad	0
	.quad	_ZN2v89ExtensionD1Ev
	.quad	_ZN2v89ExtensionD0Ev
	.quad	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.weak	_ZTVN2v88internal26ExternalizeStringExtensionE
	.section	.data.rel.ro.local._ZTVN2v88internal26ExternalizeStringExtensionE,"awG",@progbits,_ZTVN2v88internal26ExternalizeStringExtensionE,comdat
	.align 8
	.type	_ZTVN2v88internal26ExternalizeStringExtensionE, @object
	.size	_ZTVN2v88internal26ExternalizeStringExtensionE, 40
_ZTVN2v88internal26ExternalizeStringExtensionE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal26ExternalizeStringExtensionD1Ev
	.quad	_ZN2v88internal26ExternalizeStringExtensionD0Ev
	.quad	_ZN2v88internal26ExternalizeStringExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.weak	_ZTVN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEEE
	.section	.data.rel.ro.local._ZTVN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEEE,"awG",@progbits,_ZTVN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEEE,comdat
	.align 8
	.type	_ZTVN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEEE, @object
	.size	_ZTVN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEEE, 80
_ZTVN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED1Ev
	.quad	_ZN2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEED0Ev
	.quad	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.quad	_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.quad	_ZNK2v86String26ExternalStringResourceBase4LockEv
	.quad	_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.quad	_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE4dataEv
	.quad	_ZNK2v88internal20SimpleStringResourceIcNS_6String29ExternalOneByteStringResourceEE6lengthEv
	.weak	_ZTVN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEEE
	.section	.data.rel.ro.local._ZTVN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEEE,"awG",@progbits,_ZTVN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEEE,comdat
	.align 8
	.type	_ZTVN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEEE, @object
	.size	_ZTVN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEEE, 80
_ZTVN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED1Ev
	.quad	_ZN2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEED0Ev
	.quad	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.quad	_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.quad	_ZNK2v86String26ExternalStringResourceBase4LockEv
	.quad	_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.quad	_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE4dataEv
	.quad	_ZNK2v88internal20SimpleStringResourceItNS_6String22ExternalStringResourceEE6lengthEv
	.globl	_ZN2v88internal26ExternalizeStringExtension7kSourceE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"native function externalizeString();native function isOneByteString();function x() { return 1; }"
	.section	.data.rel.ro.local._ZN2v88internal26ExternalizeStringExtension7kSourceE,"aw"
	.align 8
	.type	_ZN2v88internal26ExternalizeStringExtension7kSourceE, @object
	.size	_ZN2v88internal26ExternalizeStringExtension7kSourceE, 8
_ZN2v88internal26ExternalizeStringExtension7kSourceE:
	.quad	.LC6
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
