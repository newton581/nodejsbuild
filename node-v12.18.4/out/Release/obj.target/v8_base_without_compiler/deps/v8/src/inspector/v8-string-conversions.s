	.file	"v8-string-conversions.cc"
	.text
	.section	.text._ZN12v8_inspector11UTF16ToUTF8B5cxx11EPKtm,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector11UTF16ToUTF8B5cxx11EPKtm
	.type	_ZN12v8_inspector11UTF16ToUTF8B5cxx11EPKtm, @function
_ZN12v8_inspector11UTF16ToUTF8B5cxx11EPKtm:
.LFB3056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L31
	testq	%rdx, %rdx
	je	.L31
	cmpq	$1431655765, %rdx
	jbe	.L5
.L31:
	movq	%r14, 0(%r13)
	movq	$0, 8(%r13)
	movb	$0, 16(%r13)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	(%rdx,%rdx), %r10
	leaq	-96(%rbp), %r15
	movq	%rsi, %rbx
	leaq	-80(%rbp), %r12
	leaq	(%r10,%rdx), %rsi
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r10, -104(%rbp)
	movq	%r12, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	movq	-104(%rbp), %r10
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %r9
	addq	%rbx, %r10
	addq	%rsi, %r9
	cmpq	%rbx, %r10
	jbe	.L6
	.p2align 4,,10
	.p2align 3
.L15:
	movzwl	(%rbx), %edx
	leaq	2(%rbx), %rdi
	leal	-55296(%rdx), %r8d
	cmpl	$1023, %r8d
	ja	.L7
	leaq	3(%rsi), %rcx
	cmpq	%rdi, %r10
	jbe	.L8
	movzwl	2(%rbx), %eax
	leal	-56320(%rax), %edx
	cmpl	$1023, %edx
	jbe	.L34
.L8:
	movl	$-16401, %eax
	movq	%rdi, %rbx
	movb	$-67, 2(%rsi)
	movw	%ax, (%rsi)
	movq	%rcx, %rsi
	cmpq	%rbx, %r10
	ja	.L15
	.p2align 4,,10
	.p2align 3
.L6:
	subq	-96(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6resizeEmc@PLT
	movq	-96(%rbp), %rax
	movq	%r14, 0(%r13)
	cmpq	%r12, %rax
	je	.L35
	movq	%rax, 0(%r13)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%r13)
.L17:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%r13)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	leal	-56320(%rdx), %eax
	cmpl	$1023, %eax
	jbe	.L30
	cmpl	$127, %edx
	ja	.L11
	leaq	1(%rsi), %rcx
	cmpq	%rcx, %r9
	jb	.L30
	movq	%rcx, %rsi
	movq	%rdi, %rbx
	xorl	%r8d, %r8d
.L12:
	orl	%r8d, %edx
	movb	%dl, -1(%rcx)
	cmpq	%rdi, %r10
	ja	.L15
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	4(%rsi), %rdx
	cmpq	%rdx, %r9
	jb	.L8
	sall	$10, %r8d
	leaq	4(%rbx), %rdi
	leal	9216(%rax,%r8), %edx
	andl	$63, %eax
	movl	$-16, %r8d
	orl	$-128, %eax
	shrl	$6, %edx
	movb	%al, 3(%rsi)
	movl	$3, %esi
.L13:
	movl	%edx, %r11d
	leaq	-1(%rcx), %rax
	shrl	$6, %edx
	andl	$63, %r11d
	orl	$-128, %r11d
	movb	%r11b, -1(%rcx)
.L14:
	movl	%edx, %r11d
	leaq	-1(%rax), %rcx
	shrl	$6, %edx
	movq	%rdi, %rbx
	andl	$63, %r11d
	addq	%rcx, %rsi
	orl	$-128, %r11d
	movb	%r11b, -1(%rax)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L11:
	cmpl	$2047, %edx
	ja	.L36
	leaq	2(%rsi), %rax
	cmpq	%r9, %rax
	ja	.L30
	movl	$1, %esi
	movl	$-64, %r8d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	3(%rsi), %rcx
	cmpq	%rcx, %r9
	jb	.L8
	movl	$2, %esi
	movl	$-32, %r8d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	3(%rsi), %rcx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L35:
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, 16(%r13)
	jmp	.L17
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3056:
	.size	_ZN12v8_inspector11UTF16ToUTF8B5cxx11EPKtm, .-_ZN12v8_inspector11UTF16ToUTF8B5cxx11EPKtm
	.section	.rodata._ZN12v8_inspector11UTF8ToUTF16B5cxx11EPKcm.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::vector larger than max_size()"
	.section	.rodata._ZN12v8_inspector11UTF8ToUTF16B5cxx11EPKcm.str1.1,"aMS",@progbits,1
.LC1:
	.string	"basic_string::_M_create"
	.section	.text._ZN12v8_inspector11UTF8ToUTF16B5cxx11EPKcm,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector11UTF8ToUTF16B5cxx11EPKcm
	.type	_ZN12v8_inspector11UTF8ToUTF16B5cxx11EPKcm, @function
_ZN12v8_inspector11UTF8ToUTF16B5cxx11EPKcm:
.LFB3057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L74
	movq	%rdx, %r14
	testq	%rdx, %rdx
	jne	.L38
.L74:
	movq	$0, 8(%r12)
	leaq	16(%r12), %rax
	xorl	%ecx, %ecx
	movq	%rax, (%r12)
	movw	%cx, 16(%r12)
.L37:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, %rdx
	ja	.L88
	leaq	(%rdx,%rdx), %r15
	movq	%rsi, %rbx
	movq	%r15, %rdi
	call	_Znwm@PLT
	xorl	%esi, %esi
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	(%rbx,%r14), %rsi
	leaq	0(%r13,%r15), %rax
	leaq	16(%r12), %r8
	cmpq	%rbx, %rsi
	jbe	.L42
	movq	%r13, %rdx
.L43:
	movsbw	(%rbx), %di
	movl	%edi, %ecx
	andl	$65408, %edi
	jne	.L45
	movq	%rsi, %rdi
	subq	%rbx, %rdi
	testq	%rdi, %rdi
	jle	.L46
	leal	-128(%rcx), %edi
	cmpb	$65, %dil
	jbe	.L46
	cmpb	$-12, %cl
	ja	.L46
	leaq	1(%rbx), %rdi
	movzbl	%cl, %ecx
	cmpq	%rdx, %rax
	jbe	.L46
.L72:
	movw	%cx, (%rdx)
	movq	%rdi, %rbx
	addq	$2, %rdx
.L65:
	cmpq	%rdi, %rsi
	ja	.L43
	subq	%r13, %rdx
	movq	%r8, (%r12)
	movq	%rdx, %rax
	movq	%rdx, %r14
	movq	%rdx, %r15
	sarq	%rax
	movq	%rax, %rbx
	cmpq	$7, %rax
	ja	.L89
.L71:
	cmpq	$2, %r14
	je	.L90
	testq	%r14, %r14
	je	.L69
	movq	%r8, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %r8
.L69:
	xorl	%eax, %eax
	movq	%rbx, 8(%r12)
	movw	%ax, (%r8,%r15)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L45:
	movl	%ecx, %r9d
	movsbl	%cl, %edi
	andl	$192, %r9d
	cmpl	$192, %r9d
	jne	.L46
	movl	%edi, %r9d
	andl	$224, %r9d
	cmpl	$192, %r9d
	je	.L48
	movl	%edi, %r9d
	andl	$240, %r9d
	cmpl	$224, %r9d
	je	.L49
	andl	$248, %edi
	cmpl	$240, %edi
	je	.L91
	.p2align 4,,10
	.p2align 3
.L46:
	xorl	%edx, %edx
	movq	%r8, (%r12)
	movq	$0, 8(%r12)
	movw	%dx, 16(%r12)
.L66:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rsi, %rdi
	subq	%rbx, %rdi
	cmpq	$3, %rdi
	jle	.L46
	movzbl	3(%rbx), %edi
	addl	$-128, %edi
	cmpb	$63, %dil
	ja	.L46
	leaq	3(%rbx), %r14
	movl	$-63447168, %r11d
	movl	$63447168, %r9d
	movl	$4, %r10d
.L51:
	movzbl	-1(%r14), %edi
	addl	$-128, %edi
	cmpb	$63, %dil
	ja	.L46
	subq	$1, %r14
.L52:
	movzbl	-1(%r14), %edi
	cmpb	$-65, %dil
	ja	.L46
	cmpb	$-16, %cl
	je	.L53
	ja	.L54
	cmpb	$-32, %cl
	je	.L55
	cmpb	$-19, %cl
	jne	.L57
	cmpb	$-97, %dil
	ja	.L46
.L59:
	leaq	1(%rbx), %rdi
	movzbl	%cl, %ecx
	cmpl	$3, %r10d
	je	.L73
	cmpl	$4, %r10d
	jne	.L92
	movzbl	1(%rbx), %r10d
	sall	$6, %ecx
	addq	$2, %rbx
	addl	%r10d, %ecx
	movq	%rdi, %r10
.L60:
	movzbl	1(%r10), %r10d
	sall	$6, %ecx
	leaq	1(%rbx), %rdi
	addl	%r10d, %ecx
.L62:
	sall	$6, %ecx
	addq	$1, %rdi
	subl	%r9d, %ecx
	movzbl	1(%rbx), %r9d
	addl	%r9d, %ecx
	cmpq	%rdx, %rax
	jbe	.L46
	cmpl	$65535, %ecx
	ja	.L64
	movl	%ecx, %r9d
	andl	$-2048, %r9d
	cmpl	$55296, %r9d
	jne	.L72
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L92:
	cmpl	$2, %r10d
	je	.L62
	addl	%r11d, %ecx
	cmpq	%rdx, %rax
	jbe	.L46
.L64:
	leal	-65536(%rcx), %r9d
	cmpl	$1048575, %r9d
	ja	.L46
	leaq	2(%rdx), %r9
	cmpq	%r9, %rax
	jbe	.L46
	movl	%ecx, %r9d
	andw	$1023, %cx
	movq	%rdi, %rbx
	addq	$4, %rdx
	shrl	$10, %r9d
	orw	$-9216, %cx
	subw	$10304, %r9w
	movw	%cx, -2(%rdx)
	movw	%r9w, -4(%rdx)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%rbx, %r10
	movq	%rdi, %rbx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L54:
	cmpb	$-12, %cl
	jne	.L57
	cmpb	$-113, %dil
	jbe	.L59
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L90:
	movzwl	0(%r13), %eax
	movw	%ax, (%r8)
	movq	(%r12), %r8
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L89:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %rax
	ja	.L93
	leaq	2(%r14), %rdi
	movq	%r14, %r15
	call	_Znwm@PLT
	movq	%rbx, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %r8
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L55:
	cmpb	$-97, %dil
	ja	.L59
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L53:
	cmpb	$-113, %dil
	ja	.L59
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L57:
	testb	%dil, %dil
	jns	.L46
	leal	-128(%rcx), %edi
	cmpb	$65, %dil
	jbe	.L46
	cmpb	$-12, %cl
	jbe	.L59
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rsi, %rdi
	subq	%rbx, %rdi
	cmpq	$1, %rdi
	jle	.L46
	leaq	2(%rbx), %r14
	movl	$-12416, %r11d
	movl	$12416, %r9d
	movl	$2, %r10d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rsi, %rdi
	subq	%rbx, %rdi
	cmpq	$2, %rdi
	jle	.L46
	leaq	3(%rbx), %r14
	movl	$-925824, %r11d
	movl	$925824, %r9d
	movl	$3, %r10d
	jmp	.L51
.L42:
	movq	%r8, (%r12)
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	jmp	.L69
.L88:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L93:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3057:
	.size	_ZN12v8_inspector11UTF8ToUTF16B5cxx11EPKcm, .-_ZN12v8_inspector11UTF8ToUTF16B5cxx11EPKcm
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
