	.file	"unwinding-info-writer-x64.cc"
	.text
	.section	.text._ZN2v88internal8compiler19UnwindingInfoWriter21BeginInstructionBlockEiPKNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19UnwindingInfoWriter21BeginInstructionBlockEiPKNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler19UnwindingInfoWriter21BeginInstructionBlockEiPKNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler19UnwindingInfoWriter21BeginInstructionBlockEiPKNS1_16InstructionBlockE:
.LFB10763:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L12
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movb	$0, 65(%rdi)
	movq	80(%rdi), %rax
	movslq	100(%rdx), %rdx
	movq	(%rax,%rdx,8), %r12
	testq	%r12, %r12
	je	.L1
	movl	4(%r12), %eax
	movl	24(%rdi), %edx
	movl	(%r12), %ecx
	cmpl	%ecx, 20(%rdi)
	je	.L5
	leaq	8(%rdi), %r13
	movq	%r13, %rdi
	cmpl	%eax, %edx
	jne	.L16
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	(%r12), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13EhFrameWriter22SetBaseAddressRegisterENS0_8RegisterE@PLT
	movzbl	8(%r12), %eax
	movb	%al, 64(%rbx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	cmpl	%eax, %edx
	je	.L7
	leaq	8(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	4(%r12), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
.L7:
	movzbl	8(%r12), %eax
	movb	%al, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	4(%r12), %edx
	movl	(%r12), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13EhFrameWriter31SetBaseAddressRegisterAndOffsetENS0_8RegisterEi@PLT
	jmp	.L7
	.cfi_endproc
.LFE10763:
	.size	_ZN2v88internal8compiler19UnwindingInfoWriter21BeginInstructionBlockEiPKNS1_16InstructionBlockE, .-_ZN2v88internal8compiler19UnwindingInfoWriter21BeginInstructionBlockEiPKNS1_16InstructionBlockE
	.section	.text._ZN2v88internal8compiler19UnwindingInfoWriter19EndInstructionBlockEPKNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19UnwindingInfoWriter19EndInstructionBlockEPKNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler19UnwindingInfoWriter19EndInstructionBlockEPKNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler19UnwindingInfoWriter19EndInstructionBlockEPKNS1_16InstructionBlockE:
.LFB10764:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L25
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpb	$0, 65(%rdi)
	jne	.L17
	movq	8(%rsi), %rbx
	movq	16(%rsi), %r14
	cmpq	%r14, %rbx
	jne	.L22
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$4, %rbx
	cmpq	%rbx, %r14
	je	.L17
.L22:
	movslq	(%rbx), %r13
	movq	80(%r12), %rax
	cmpq	$0, (%rax,%r13,8)
	jne	.L19
	movq	(%r12), %rdi
	movq	20(%r12), %r15
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L28
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L21:
	movzbl	64(%r12), %edx
	movq	%r15, (%rax)
	addq	$4, %rbx
	movb	%dl, 8(%rax)
	movq	80(%r12), %rdx
	movq	%rax, (%rdx,%r13,8)
	cmpq	%rbx, %r14
	jne	.L22
.L17:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L21
	.cfi_endproc
.LFE10764:
	.size	_ZN2v88internal8compiler19UnwindingInfoWriter19EndInstructionBlockEPKNS1_16InstructionBlockE, .-_ZN2v88internal8compiler19UnwindingInfoWriter19EndInstructionBlockEPKNS1_16InstructionBlockE
	.section	.text._ZN2v88internal8compiler19UnwindingInfoWriter20MarkFrameConstructedEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19UnwindingInfoWriter20MarkFrameConstructedEi
	.type	_ZN2v88internal8compiler19UnwindingInfoWriter20MarkFrameConstructedEi, @function
_ZN2v88internal8compiler19UnwindingInfoWriter20MarkFrameConstructedEi:
.LFB10765:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	jne	.L35
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	leal	1(%rsi), %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	24(%rbx), %eax
	movq	%r13, %rdi
	leal	8(%rax), %esi
	call	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %r15d
	movl	24(%rbx), %r14d
	movl	%r15d, %edi
	negl	%r14d
	call	_ZN2v88internal13EhFrameWriter19RegisterToDwarfCodeENS0_8RegisterE@PLT
	movl	%r14d, %edx
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal13EhFrameWriter26RecordRegisterSavedToStackEii@PLT
	leal	4(%r12), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13EhFrameWriter22SetBaseAddressRegisterENS0_8RegisterE@PLT
	movb	$1, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10765:
	.size	_ZN2v88internal8compiler19UnwindingInfoWriter20MarkFrameConstructedEi, .-_ZN2v88internal8compiler19UnwindingInfoWriter20MarkFrameConstructedEi
	.section	.text._ZN2v88internal8compiler19UnwindingInfoWriter22MarkFrameDeconstructedEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19UnwindingInfoWriter22MarkFrameDeconstructedEi
	.type	_ZN2v88internal8compiler19UnwindingInfoWriter22MarkFrameDeconstructedEi, @function
_ZN2v88internal8compiler19UnwindingInfoWriter22MarkFrameDeconstructedEi:
.LFB10766:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	jne	.L42
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	leal	3(%rsi), %esi
	subq	$8, %rsp
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13EhFrameWriter22SetBaseAddressRegisterENS0_8RegisterE@PLT
	leal	4(%rbx), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi@PLT
	movl	24(%r12), %eax
	movq	%r13, %rdi
	leal	-8(%rax), %esi
	call	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi@PLT
	movb	$0, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10766:
	.size	_ZN2v88internal8compiler19UnwindingInfoWriter22MarkFrameDeconstructedEi, .-_ZN2v88internal8compiler19UnwindingInfoWriter22MarkFrameDeconstructedEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler19UnwindingInfoWriter21BeginInstructionBlockEiPKNS1_16InstructionBlockE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler19UnwindingInfoWriter21BeginInstructionBlockEiPKNS1_16InstructionBlockE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler19UnwindingInfoWriter21BeginInstructionBlockEiPKNS1_16InstructionBlockE:
.LFB13081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13081:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler19UnwindingInfoWriter21BeginInstructionBlockEiPKNS1_16InstructionBlockE, .-_GLOBAL__sub_I__ZN2v88internal8compiler19UnwindingInfoWriter21BeginInstructionBlockEiPKNS1_16InstructionBlockE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler19UnwindingInfoWriter21BeginInstructionBlockEiPKNS1_16InstructionBlockE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata._ZN2v88internalL3rbpE,"a"
	.align 4
	.type	_ZN2v88internalL3rbpE, @object
	.size	_ZN2v88internalL3rbpE, 4
_ZN2v88internalL3rbpE:
	.long	5
	.section	.rodata._ZN2v88internalL3rspE,"a"
	.align 4
	.type	_ZN2v88internalL3rspE, @object
	.size	_ZN2v88internalL3rspE, 4
_ZN2v88internalL3rspE:
	.long	4
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
