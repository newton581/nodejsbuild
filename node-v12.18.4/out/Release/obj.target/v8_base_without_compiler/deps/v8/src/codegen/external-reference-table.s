	.file	"external-reference-table.cc"
	.text
	.section	.rodata._ZN2v88internal22ExternalReferenceTable13ResolveSymbolEPv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<unresolved>"
	.section	.text._ZN2v88internal22ExternalReferenceTable13ResolveSymbolEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22ExternalReferenceTable13ResolveSymbolEPv
	.type	_ZN2v88internal22ExternalReferenceTable13ResolveSymbolEPv, @function
_ZN2v88internal22ExternalReferenceTable13ResolveSymbolEPv:
.LFB9161:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE9161:
	.size	_ZN2v88internal22ExternalReferenceTable13ResolveSymbolEPv, .-_ZN2v88internal22ExternalReferenceTable13ResolveSymbolEPv
	.section	.text._ZN2v88internal22ExternalReferenceTable3AddEmPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22ExternalReferenceTable3AddEmPi
	.type	_ZN2v88internal22ExternalReferenceTable3AddEmPi, @function
_ZN2v88internal22ExternalReferenceTable3AddEmPi:
.LFB9162:
	.cfi_startproc
	endbr64
	movslq	(%rdx), %rax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdx)
	movq	%rsi, (%rdi,%rax,8)
	ret
	.cfi_endproc
.LFE9162:
	.size	_ZN2v88internal22ExternalReferenceTable3AddEmPi, .-_ZN2v88internal22ExternalReferenceTable3AddEmPi
	.section	.rodata._ZN2v88internal22ExternalReferenceTable13AddReferencesEPNS0_7IsolateEPi.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"kSpecialReferenceCount == *index"
	.section	.rodata._ZN2v88internal22ExternalReferenceTable13AddReferencesEPNS0_7IsolateEPi.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal22ExternalReferenceTable13AddReferencesEPNS0_7IsolateEPi.str1.8
	.align 8
.LC3:
	.string	"kSpecialReferenceCount + kExternalReferenceCount == *index"
	.section	.text._ZN2v88internal22ExternalReferenceTable13AddReferencesEPNS0_7IsolateEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22ExternalReferenceTable13AddReferencesEPNS0_7IsolateEPi
	.type	_ZN2v88internal22ExternalReferenceTable13AddReferencesEPNS0_7IsolateEPi, @function
_ZN2v88internal22ExternalReferenceTable13AddReferencesEPNS0_7IsolateEPi:
.LFB9163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpl	$1, (%rdx)
	jne	.L8
	movq	%rdx, %rbx
	movq	%rdi, %r12
	movq	%rsi, %r13
	call	_ZN2v88internal17ExternalReference17abort_with_reasonEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference30address_of_double_abs_constantEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference30address_of_double_neg_constantEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference29address_of_float_abs_constantEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference29address_of_float_neg_constantEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference18address_of_min_intEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference42address_of_mock_arraybuffer_allocator_flagEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference19address_of_one_halfEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference29address_of_runtime_stats_flagEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference23address_of_the_hole_nanEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference22address_of_uint32_biasEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference27bytecode_size_table_addressEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference17check_object_typeEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20compute_integer_hashEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference30compute_output_frames_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference48copy_fast_number_jsarray_elements_to_typed_arrayEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference31copy_typed_array_elements_sliceEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference40copy_typed_array_elements_to_typed_arrayEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference12cpu_featuresEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference30delete_handle_scope_extensionsEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference36ephemeron_key_write_barrier_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference25f64_acos_wrapper_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference25f64_asin_wrapper_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference24f64_mod_wrapper_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference23get_date_field_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference22get_or_create_hash_rawEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21ieee754_acos_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference22ieee754_acosh_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21ieee754_asin_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference22ieee754_asinh_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21ieee754_atan_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference22ieee754_atan2_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference22ieee754_atanh_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21ieee754_cbrt_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20ieee754_cos_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21ieee754_cosh_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20ieee754_exp_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference22ieee754_expm1_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20ieee754_log_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference22ieee754_log10_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference22ieee754_log1p_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21ieee754_log2_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20ieee754_pow_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20ieee754_sin_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21ieee754_sinh_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20ieee754_tan_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21ieee754_tanh_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference41incremental_marking_record_write_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference36invalidate_prototype_chains_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference31invoke_accessor_getter_callbackEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference24invoke_function_callbackEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference46jsarray_array_join_concat_to_sequential_stringEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference31jsreceiver_create_identity_hashEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20libc_memchr_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20libc_memcpy_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21libc_memmove_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20libc_memset_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference25mod_two_doubles_operationEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference54mutable_big_int_absolute_add_and_canonicalize_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference41mutable_big_int_absolute_compare_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference54mutable_big_int_absolute_sub_and_canonicalize_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference24new_deoptimizer_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference26orderedhashmap_gethash_rawEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference15printf_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference18refill_math_randomEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference25search_string_raw_one_oneEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference25search_string_raw_one_twoEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference25search_string_raw_two_oneEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference25search_string_raw_two_twoEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference34smi_lexicographic_compare_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference30store_buffer_overflow_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference31try_internalize_string_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference35wasm_call_trap_callback_for_testingEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference13wasm_f32_ceilEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference14wasm_f32_floorEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20wasm_f32_nearest_intEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference14wasm_f32_truncEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference13wasm_f64_ceilEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference14wasm_f64_floorEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20wasm_f64_nearest_intEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference14wasm_f64_truncEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21wasm_float32_to_int64Ev@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference22wasm_float32_to_uint64Ev@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference16wasm_float64_powEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21wasm_float64_to_int64Ev@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference22wasm_float64_to_uint64Ev@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference14wasm_int64_divEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference14wasm_int64_modEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21wasm_int64_to_float32Ev@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21wasm_int64_to_float64Ev@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference15wasm_uint64_divEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference15wasm_uint64_modEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference22wasm_uint64_to_float32Ev@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference22wasm_uint64_to_float64Ev@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference15wasm_word32_ctzEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference18wasm_word32_popcntEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference15wasm_word32_rolEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference15wasm_word32_rorEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference15wasm_word64_ctzEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference18wasm_word64_popcntEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference16wasm_memory_copyEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference16wasm_memory_fillEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference31call_enqueue_microtask_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference27call_enter_context_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference25atomic_pair_load_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference26atomic_pair_store_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference24atomic_pair_add_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference24atomic_pair_sub_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference24atomic_pair_and_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference23atomic_pair_or_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference24atomic_pair_xor_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference29atomic_pair_exchange_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference37atomic_pair_compare_exchange_functionEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference30intl_convert_one_byte_to_lowerEv@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference26intl_to_latin1_lower_tableEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference16builtins_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference32handle_scope_implementer_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference57address_of_interpreter_entry_trampoline_instruction_startEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference29interpreter_dispatch_countersEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference34interpreter_dispatch_table_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference16date_cache_stampEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference18stress_deopt_countEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference15force_slow_pathEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference12isolate_rootEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference29allocation_sites_list_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference18address_of_jslimitEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference23address_of_real_jslimitEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference16store_buffer_topEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference28heap_is_marking_flag_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference32new_space_allocation_top_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference34new_space_allocation_limit_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference32old_space_allocation_top_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference34old_space_allocation_limit_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference26handle_scope_level_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference25handle_scope_next_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference26handle_scope_limit_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference27scheduled_exception_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference30address_of_pending_message_objEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20promise_hook_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference28async_event_delegate_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference44promise_hook_or_async_event_delegate_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference63promise_hook_or_debug_is_active_or_async_event_delegate_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference28debug_execution_mode_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference23debug_is_active_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference35debug_hook_on_function_call_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference30runtime_function_table_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference20is_profiling_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference33debug_suspended_generator_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference24debug_restart_fp_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference29fast_c_call_caller_fp_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference29fast_c_call_caller_pc_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference25stack_is_iterable_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference37address_of_regexp_stack_limit_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference38address_of_regexp_stack_memory_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference35address_of_regexp_stack_memory_sizeEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference42address_of_regexp_stack_memory_top_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference32address_of_static_offsets_vectorEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference32re_case_insensitive_compare_uc16EPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference26re_check_stack_guard_stateEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference13re_grow_stackEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference25re_match_for_call_from_jsEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	call	_ZN2v88internal17ExternalReference21re_word_character_mapEPNS0_7IsolateE@PLT
	movq	%rax, %r8
	movslq	(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movq	%r8, (%r12,%rax,8)
	cmpl	$164, (%rbx)
	jne	.L9
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9163:
	.size	_ZN2v88internal22ExternalReferenceTable13AddReferencesEPNS0_7IsolateEPi, .-_ZN2v88internal22ExternalReferenceTable13AddReferencesEPNS0_7IsolateEPi
	.section	.rodata._ZN2v88internal22ExternalReferenceTable11AddBuiltinsEPi.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"kSpecialReferenceCount + kExternalReferenceCount + kBuiltinsReferenceCount == *index"
	.section	.text._ZN2v88internal22ExternalReferenceTable11AddBuiltinsEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22ExternalReferenceTable11AddBuiltinsEPi
	.type	_ZN2v88internal22ExternalReferenceTable11AddBuiltinsEPi, @function
_ZN2v88internal22ExternalReferenceTable11AddBuiltinsEPi:
.LFB9164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$164, (%rsi)
	jne	.L18
	leaq	_ZZN2v88internal22ExternalReferenceTable11AddBuiltinsEPiE10c_builtins(%rip), %rbx
	movq	%rdi, %r12
	movq	%rsi, %r14
	leaq	2128(%rbx), %r13
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZN2v88internal17ExternalReference6CreateEm@PLT
	movq	%rax, %r8
	movslq	(%r14), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r14)
	movq	%r8, (%r12,%rax,8)
	cmpq	%rbx, %r13
	jne	.L12
	cmpl	$430, (%r14)
	jne	.L19
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L19:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9164:
	.size	_ZN2v88internal22ExternalReferenceTable11AddBuiltinsEPi, .-_ZN2v88internal22ExternalReferenceTable11AddBuiltinsEPi
	.section	.rodata._ZN2v88internal22ExternalReferenceTable19AddRuntimeFunctionsEPi.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"kSpecialReferenceCount + kExternalReferenceCount + kBuiltinsReferenceCount + kRuntimeReferenceCount == *index"
	.section	.text._ZN2v88internal22ExternalReferenceTable19AddRuntimeFunctionsEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22ExternalReferenceTable19AddRuntimeFunctionsEPi
	.type	_ZN2v88internal22ExternalReferenceTable19AddRuntimeFunctionsEPi, @function
_ZN2v88internal22ExternalReferenceTable19AddRuntimeFunctionsEPi:
.LFB9165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$430, (%rsi)
	jne	.L28
	leaq	_ZZN2v88internal22ExternalReferenceTable19AddRuntimeFunctionsEPiE17runtime_functions(%rip), %rbx
	movq	%rdi, %r12
	movq	%rsi, %r14
	leaq	1868(%rbx), %r13
	.p2align 4,,10
	.p2align 3
.L22:
	movl	(%rbx), %edi
	addq	$4, %rbx
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movq	%rax, %r8
	movslq	(%r14), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r14)
	movq	%r8, (%r12,%rax,8)
	cmpq	%rbx, %r13
	jne	.L22
	cmpl	$897, (%r14)
	jne	.L29
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L28:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L29:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9165:
	.size	_ZN2v88internal22ExternalReferenceTable19AddRuntimeFunctionsEPi, .-_ZN2v88internal22ExternalReferenceTable19AddRuntimeFunctionsEPi
	.section	.rodata._ZN2v88internal22ExternalReferenceTable19AddIsolateAddressesEPNS0_7IsolateEPi.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"kSpecialReferenceCount + kExternalReferenceCount + kBuiltinsReferenceCount + kRuntimeReferenceCount + kIsolateAddressReferenceCount == *index"
	.section	.text._ZN2v88internal22ExternalReferenceTable19AddIsolateAddressesEPNS0_7IsolateEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22ExternalReferenceTable19AddIsolateAddressesEPNS0_7IsolateEPi
	.type	_ZN2v88internal22ExternalReferenceTable19AddIsolateAddressesEPNS0_7IsolateEPi, @function
_ZN2v88internal22ExternalReferenceTable19AddIsolateAddressesEPNS0_7IsolateEPi:
.LFB9166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$897, (%rdx)
	jne	.L38
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L31:
	movl	%ebx, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN2v88internal7Isolate19get_address_from_idENS0_16IsolateAddressIdE@PLT
	movq	%rax, %r8
	movslq	(%r14), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r14)
	movq	%r8, 0(%r13,%rax,8)
	cmpl	$12, %ebx
	jne	.L31
	cmpl	$909, (%r14)
	jne	.L39
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L38:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L39:
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9166:
	.size	_ZN2v88internal22ExternalReferenceTable19AddIsolateAddressesEPNS0_7IsolateEPi, .-_ZN2v88internal22ExternalReferenceTable19AddIsolateAddressesEPNS0_7IsolateEPi
	.section	.rodata._ZN2v88internal22ExternalReferenceTable12AddAccessorsEPi.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"kSpecialReferenceCount + kExternalReferenceCount + kBuiltinsReferenceCount + kRuntimeReferenceCount + kIsolateAddressReferenceCount + kAccessorReferenceCount == *index"
	.section	.text._ZN2v88internal22ExternalReferenceTable12AddAccessorsEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22ExternalReferenceTable12AddAccessorsEPi
	.type	_ZN2v88internal22ExternalReferenceTable12AddAccessorsEPi, @function
_ZN2v88internal22ExternalReferenceTable12AddAccessorsEPi:
.LFB9167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$909, (%rsi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L44
	movq	_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rax
	movl	$910, (%rsi)
	movq	%rax, 7272(%rdi)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	8+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	16+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	24+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	32+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	40+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	48+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	56+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	64+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	72+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	80+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	88+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	96+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	104+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	112+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movslq	(%rsi), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rsi)
	movq	120+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %rdx
	movq	%rdx, (%rdi,%rax,8)
	cmpl	$925, (%rsi)
	jne	.L45
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L45:
	leaq	.LC7(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9167:
	.size	_ZN2v88internal22ExternalReferenceTable12AddAccessorsEPi, .-_ZN2v88internal22ExternalReferenceTable12AddAccessorsEPi
	.section	.rodata._ZN2v88internal22ExternalReferenceTable12AddStubCacheEPNS0_7IsolateEPi.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"kSpecialReferenceCount + kExternalReferenceCount + kBuiltinsReferenceCount + kRuntimeReferenceCount + kIsolateAddressReferenceCount + kAccessorReferenceCount + kStubCacheReferenceCount == *index"
	.section	.text._ZN2v88internal22ExternalReferenceTable12AddStubCacheEPNS0_7IsolateEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22ExternalReferenceTable12AddStubCacheEPNS0_7IsolateEPi
	.type	_ZN2v88internal22ExternalReferenceTable12AddStubCacheEPNS0_7IsolateEPi, @function
_ZN2v88internal22ExternalReferenceTable12AddStubCacheEPNS0_7IsolateEPi:
.LFB9168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$925, (%rdx)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L50
	movq	41024(%rsi), %rax
	movl	$926, (%rdx)
	movq	%rax, 7400(%rdi)
	movslq	(%rdx), %rcx
	leal	1(%rcx), %r8d
	movl	%r8d, (%rdx)
	leaq	8(%rax), %r8
	movq	%r8, (%rdi,%rcx,8)
	movslq	(%rdx), %rcx
	leal	1(%rcx), %r8d
	movl	%r8d, (%rdx)
	leaq	16(%rax), %r8
	movq	%r8, (%rdi,%rcx,8)
	movslq	(%rdx), %rcx
	leal	1(%rcx), %r8d
	movl	%r8d, (%rdx)
	leaq	49152(%rax), %r8
	movq	%r8, (%rdi,%rcx,8)
	movslq	(%rdx), %rcx
	leal	1(%rcx), %r8d
	movl	%r8d, (%rdx)
	leaq	49160(%rax), %r8
	addq	$49168, %rax
	movq	%r8, (%rdi,%rcx,8)
	movslq	(%rdx), %rcx
	leal	1(%rcx), %r8d
	movl	%r8d, (%rdx)
	movq	%rax, (%rdi,%rcx,8)
	movslq	(%rdx), %rcx
	movq	41032(%rsi), %rax
	leal	1(%rcx), %esi
	movl	%esi, (%rdx)
	movq	%rax, (%rdi,%rcx,8)
	movslq	(%rdx), %rcx
	leal	1(%rcx), %esi
	movl	%esi, (%rdx)
	leaq	8(%rax), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movslq	(%rdx), %rcx
	leal	1(%rcx), %esi
	movl	%esi, (%rdx)
	leaq	16(%rax), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movslq	(%rdx), %rcx
	leal	1(%rcx), %esi
	movl	%esi, (%rdx)
	leaq	49152(%rax), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movslq	(%rdx), %rcx
	leal	1(%rcx), %esi
	movl	%esi, (%rdx)
	leaq	49160(%rax), %rsi
	addq	$49168, %rax
	movq	%rsi, (%rdi,%rcx,8)
	movslq	(%rdx), %rcx
	leal	1(%rcx), %esi
	movl	%esi, (%rdx)
	movq	%rax, (%rdi,%rcx,8)
	cmpl	$937, (%rdx)
	jne	.L51
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	leaq	.LC7(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	.LC8(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9168:
	.size	_ZN2v88internal22ExternalReferenceTable12AddStubCacheEPNS0_7IsolateEPi, .-_ZN2v88internal22ExternalReferenceTable12AddStubCacheEPNS0_7IsolateEPi
	.section	.text._ZN2v88internal22ExternalReferenceTable22GetStatsCounterAddressEPNS0_12StatsCounterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22ExternalReferenceTable22GetStatsCounterAddressEPNS0_12StatsCounterE
	.type	_ZN2v88internal22ExternalReferenceTable22GetStatsCounterAddressEPNS0_12StatsCounterE, @function
_ZN2v88internal22ExternalReferenceTable22GetStatsCounterAddressEPNS0_12StatsCounterE:
.LFB9169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, 24(%rsi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	je	.L53
	cmpq	$0, 16(%rsi)
	je	.L54
.L56:
	movq	16(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	leaq	7588(%r12), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movb	$1, 24(%rsi)
	movq	%rsi, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L54
	cmpb	$0, 24(%rbx)
	jne	.L56
	movb	$1, 24(%rbx)
	movq	%rbx, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9169:
	.size	_ZN2v88internal22ExternalReferenceTable22GetStatsCounterAddressEPNS0_12StatsCounterE, .-_ZN2v88internal22ExternalReferenceTable22GetStatsCounterAddressEPNS0_12StatsCounterE
	.section	.rodata._ZN2v88internal22ExternalReferenceTable26AddNativeCodeStatsCountersEPNS0_7IsolateEPi.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"kSpecialReferenceCount + kExternalReferenceCount + kBuiltinsReferenceCount + kRuntimeReferenceCount + kIsolateAddressReferenceCount + kAccessorReferenceCount + kStubCacheReferenceCount + kStatsCountersReferenceCount == *index"
	.section	.text._ZN2v88internal22ExternalReferenceTable26AddNativeCodeStatsCountersEPNS0_7IsolateEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22ExternalReferenceTable26AddNativeCodeStatsCountersEPNS0_7IsolateEPi
	.type	_ZN2v88internal22ExternalReferenceTable26AddNativeCodeStatsCountersEPNS0_7IsolateEPi, @function
_ZN2v88internal22ExternalReferenceTable26AddNativeCodeStatsCountersEPNS0_7IsolateEPi:
.LFB9170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	(%rdx), %rdx
	cmpl	$937, %edx
	jne	.L142
	movq	40960(%rsi), %rbx
	movq	%rdi, %r13
	cmpb	$0, 7808(%rbx)
	je	.L63
	cmpq	$0, 7800(%rbx)
	movl	%edx, %ecx
	je	.L64
.L67:
	movq	7800(%rbx), %rax
	movslq	%ecx, %rdx
.L65:
	leal	1(%rdx), %ecx
	movl	%ecx, (%r12)
	movq	%rax, 0(%r13,%rdx,8)
	cmpb	$0, 7840(%rbx)
	je	.L68
	cmpq	$0, 7832(%rbx)
	je	.L69
.L71:
	movq	7832(%rbx), %rax
.L70:
	movslq	(%r12), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r12)
	movq	%rax, 0(%r13,%rdx,8)
	cmpb	$0, 7872(%rbx)
	je	.L72
	cmpq	$0, 7864(%rbx)
	je	.L73
.L75:
	movq	7864(%rbx), %rax
.L74:
	movslq	(%r12), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r12)
	movq	%rax, 0(%r13,%rdx,8)
	cmpb	$0, 7904(%rbx)
	je	.L76
	cmpq	$0, 7896(%rbx)
	je	.L77
.L79:
	movq	7896(%rbx), %rax
.L78:
	movslq	(%r12), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r12)
	movq	%rax, 0(%r13,%rdx,8)
	cmpb	$0, 7936(%rbx)
	je	.L80
	cmpq	$0, 7928(%rbx)
	je	.L81
.L83:
	movq	7928(%rbx), %rax
.L82:
	movslq	(%r12), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r12)
	movq	%rax, 0(%r13,%rdx,8)
	cmpb	$0, 7968(%rbx)
	je	.L84
	cmpq	$0, 7960(%rbx)
	je	.L85
.L87:
	movq	7960(%rbx), %rax
.L86:
	movslq	(%r12), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r12)
	movq	%rax, 0(%r13,%rdx,8)
	cmpb	$0, 8000(%rbx)
	je	.L88
	cmpq	$0, 7992(%rbx)
	je	.L89
.L91:
	movq	7992(%rbx), %rax
.L90:
	movslq	(%r12), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r12)
	movq	%rax, 0(%r13,%rdx,8)
	cmpb	$0, 8032(%rbx)
	je	.L92
	cmpq	$0, 8024(%rbx)
	je	.L93
.L95:
	movq	8024(%rbx), %rax
.L94:
	movslq	(%r12), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r12)
	movq	%rax, 0(%r13,%rdx,8)
	cmpb	$0, 8064(%rbx)
	je	.L96
	cmpq	$0, 8056(%rbx)
	je	.L97
.L99:
	movq	8056(%rbx), %rax
.L98:
	movslq	(%r12), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r12)
	movq	%rax, 0(%r13,%rdx,8)
	cmpb	$0, 8096(%rbx)
	je	.L100
	cmpq	$0, 8088(%rbx)
	je	.L101
.L103:
	movq	8088(%rbx), %rax
.L102:
	movslq	(%r12), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r12)
	movq	%rax, 0(%r13,%rdx,8)
	cmpb	$0, 8128(%rbx)
	je	.L104
	cmpq	$0, 8120(%rbx)
	je	.L105
.L107:
	movq	8120(%rbx), %rax
.L106:
	movslq	(%r12), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r12)
	movq	%rax, 0(%r13,%rdx,8)
	cmpl	$948, (%r12)
	jne	.L143
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movb	$1, 7808(%rbx)
	leaq	7784(%rbx), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7800(%rbx)
	testq	%rax, %rax
	je	.L144
	cmpb	$0, 7808(%rbx)
	jne	.L145
	movb	$1, 7808(%rbx)
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7800(%rbx)
	movslq	(%r12), %rdx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L104:
	movb	$1, 8128(%rbx)
	leaq	8104(%rbx), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 8120(%rbx)
	testq	%rax, %rax
	je	.L105
	cmpb	$0, 8128(%rbx)
	jne	.L107
	movb	$1, 8128(%rbx)
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 8120(%rbx)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L100:
	movb	$1, 8096(%rbx)
	leaq	8072(%rbx), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 8088(%rbx)
	testq	%rax, %rax
	je	.L101
	cmpb	$0, 8096(%rbx)
	jne	.L103
	movb	$1, 8096(%rbx)
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 8088(%rbx)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L96:
	movb	$1, 8064(%rbx)
	leaq	8040(%rbx), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 8056(%rbx)
	testq	%rax, %rax
	je	.L97
	cmpb	$0, 8064(%rbx)
	jne	.L99
	movb	$1, 8064(%rbx)
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 8056(%rbx)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L92:
	movb	$1, 8032(%rbx)
	leaq	8008(%rbx), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 8024(%rbx)
	testq	%rax, %rax
	je	.L93
	cmpb	$0, 8032(%rbx)
	jne	.L95
	movb	$1, 8032(%rbx)
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 8024(%rbx)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L88:
	movb	$1, 8000(%rbx)
	leaq	7976(%rbx), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7992(%rbx)
	testq	%rax, %rax
	je	.L89
	cmpb	$0, 8000(%rbx)
	jne	.L91
	movb	$1, 8000(%rbx)
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7992(%rbx)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L84:
	movb	$1, 7968(%rbx)
	leaq	7944(%rbx), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7960(%rbx)
	testq	%rax, %rax
	je	.L85
	cmpb	$0, 7968(%rbx)
	jne	.L87
	movb	$1, 7968(%rbx)
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7960(%rbx)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L80:
	movb	$1, 7936(%rbx)
	leaq	7912(%rbx), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7928(%rbx)
	testq	%rax, %rax
	je	.L81
	cmpb	$0, 7936(%rbx)
	jne	.L83
	movb	$1, 7936(%rbx)
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7928(%rbx)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L76:
	movb	$1, 7904(%rbx)
	leaq	7880(%rbx), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7896(%rbx)
	testq	%rax, %rax
	je	.L77
	cmpb	$0, 7904(%rbx)
	jne	.L79
	movb	$1, 7904(%rbx)
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7896(%rbx)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L72:
	movb	$1, 7872(%rbx)
	leaq	7848(%rbx), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7864(%rbx)
	testq	%rax, %rax
	je	.L73
	cmpb	$0, 7872(%rbx)
	jne	.L75
	movb	$1, 7872(%rbx)
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7864(%rbx)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L68:
	movb	$1, 7840(%rbx)
	leaq	7816(%rbx), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7832(%rbx)
	testq	%rax, %rax
	je	.L69
	cmpb	$0, 7840(%rbx)
	jne	.L71
	movb	$1, 7840(%rbx)
	movq	%r14, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7832(%rbx)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	7588(%r13), %rax
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L144:
	movslq	(%r12), %rdx
.L64:
	leaq	7588(%r13), %rax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	7588(%r13), %rax
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	7588(%r13), %rax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	7588(%r13), %rax
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L81:
	leaq	7588(%r13), %rax
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	7588(%r13), %rax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	7588(%r13), %rax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	7588(%r13), %rax
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	7588(%r13), %rax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	7588(%r13), %rax
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	.LC8(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L143:
	leaq	.LC9(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L145:
	movl	(%r12), %ecx
	jmp	.L67
	.cfi_endproc
.LFE9170:
	.size	_ZN2v88internal22ExternalReferenceTable26AddNativeCodeStatsCountersEPNS0_7IsolateEPi, .-_ZN2v88internal22ExternalReferenceTable26AddNativeCodeStatsCountersEPNS0_7IsolateEPi
	.section	.rodata._ZN2v88internal22ExternalReferenceTable4InitEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"kSize == index"
	.section	.text._ZN2v88internal22ExternalReferenceTable4InitEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22ExternalReferenceTable4InitEPNS0_7IsolateE
	.type	_ZN2v88internal22ExternalReferenceTable4InitEPNS0_7IsolateE, @function
_ZN2v88internal22ExternalReferenceTable4InitEPNS0_7IsolateE:
.LFB9160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-60(%rbp), %r14
	pushq	%r13
	movq	%r14, %rdx
	.cfi_offset 13, -40
	leaq	_ZZN2v88internal22ExternalReferenceTable11AddBuiltinsEPiE10c_builtins(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	2128(%r13), %r15
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rdi)
	movl	$1, -60(%rbp)
	call	_ZN2v88internal22ExternalReferenceTable13AddReferencesEPNS0_7IsolateEPi
	cmpl	$164, -60(%rbp)
	jne	.L166
	.p2align 4,,10
	.p2align 3
.L148:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZN2v88internal17ExternalReference6CreateEm@PLT
	movq	%rax, %r8
	movslq	-60(%rbp), %rax
	leal	1(%rax), %edx
	movq	%r8, (%rbx,%rax,8)
	movl	%edx, -60(%rbp)
	cmpq	%r13, %r15
	jne	.L148
	leaq	_ZZN2v88internal22ExternalReferenceTable19AddRuntimeFunctionsEPiE17runtime_functions(%rip), %r15
	leaq	1868(%r15), %r13
	cmpl	$430, %edx
	jne	.L167
	.p2align 4,,10
	.p2align 3
.L150:
	movl	(%r15), %edi
	addq	$4, %r15
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movq	%rax, %r8
	movslq	-60(%rbp), %rax
	leal	1(%rax), %edx
	movq	%r8, (%rbx,%rax,8)
	movl	%edx, -60(%rbp)
	cmpq	%r15, %r13
	jne	.L150
	xorl	%r13d, %r13d
	cmpl	$897, %edx
	jne	.L168
	.p2align 4,,10
	.p2align 3
.L151:
	movl	%r13d, %esi
	movq	%r12, %rdi
	addl	$1, %r13d
	call	_ZN2v88internal7Isolate19get_address_from_idENS0_16IsolateAddressIdE@PLT
	movq	%rax, %r8
	movslq	-60(%rbp), %rax
	leal	1(%rax), %edx
	movq	%r8, (%rbx,%rax,8)
	movl	%edx, -60(%rbp)
	cmpl	$12, %r13d
	jne	.L151
	cmpl	$909, %edx
	jne	.L169
	movdqa	_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %xmm0
	movdqa	16+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %xmm1
	movq	%r14, %rdx
	movq	%r12, %rsi
	movdqa	32+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %xmm2
	movdqa	48+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %xmm3
	movq	%rbx, %rdi
	movl	$925, -60(%rbp)
	movdqa	64+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %xmm4
	movdqa	80+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %xmm5
	movups	%xmm0, 7272(%rbx)
	movdqa	96+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %xmm6
	movdqa	112+_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors(%rip), %xmm7
	movups	%xmm1, 7288(%rbx)
	movups	%xmm2, 7304(%rbx)
	movups	%xmm3, 7320(%rbx)
	movups	%xmm4, 7336(%rbx)
	movups	%xmm5, 7352(%rbx)
	movups	%xmm6, 7368(%rbx)
	movups	%xmm7, 7384(%rbx)
	call	_ZN2v88internal22ExternalReferenceTable12AddStubCacheEPNS0_7IsolateEPi
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal22ExternalReferenceTable26AddNativeCodeStatsCountersEPNS0_7IsolateEPi
	cmpl	$948, -60(%rbp)
	movl	$1, 7584(%rbx)
	jne	.L170
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L171
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L167:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L169:
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	leaq	.LC10(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9160:
	.size	_ZN2v88internal22ExternalReferenceTable4InitEPNS0_7IsolateE, .-_ZN2v88internal22ExternalReferenceTable4InitEPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal22ExternalReferenceTable9ref_name_E,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal22ExternalReferenceTable9ref_name_E, @function
_GLOBAL__sub_I__ZN2v88internal22ExternalReferenceTable9ref_name_E:
.LFB10476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10476:
	.size	_GLOBAL__sub_I__ZN2v88internal22ExternalReferenceTable9ref_name_E, .-_GLOBAL__sub_I__ZN2v88internal22ExternalReferenceTable9ref_name_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal22ExternalReferenceTable9ref_name_E
	.section	.data.rel.ro._ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors,"aw"
	.align 32
	.type	_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors, @object
	.size	_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors, 128
_ZZN2v88internal22ExternalReferenceTable12AddAccessorsEPiE9accessors:
	.quad	_ZN2v88internal9Accessors23ArgumentsIteratorGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.quad	_ZN2v88internal9Accessors17ArrayLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.quad	_ZN2v88internal9Accessors25BoundFunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.quad	_ZN2v88internal9Accessors23BoundFunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.quad	_ZN2v88internal9Accessors16ErrorStackGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.quad	_ZN2v88internal9Accessors23FunctionArgumentsGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.quad	_ZN2v88internal9Accessors20FunctionCallerGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.quad	_ZN2v88internal9Accessors18FunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.quad	_ZN2v88internal9Accessors20FunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.quad	_ZN2v88internal9Accessors23FunctionPrototypeGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.quad	_ZN2v88internal9Accessors18StringLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.quad	_ZN2v88internal9Accessors17ArrayLengthSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.quad	_ZN2v88internal9Accessors16ErrorStackSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.quad	_ZN2v88internal9Accessors23FunctionPrototypeSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.quad	_ZN2v88internal9Accessors26ModuleNamespaceEntrySetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.quad	_ZN2v88internal9Accessors25ReconfigureToDataPropertyENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.section	.rodata._ZZN2v88internal22ExternalReferenceTable19AddRuntimeFunctionsEPiE17runtime_functions,"a"
	.align 32
	.type	_ZZN2v88internal22ExternalReferenceTable19AddRuntimeFunctionsEPiE17runtime_functions, @object
	.size	_ZZN2v88internal22ExternalReferenceTable19AddRuntimeFunctionsEPiE17runtime_functions, 1868
_ZZN2v88internal22ExternalReferenceTable19AddRuntimeFunctionsEPiE17runtime_functions:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	33
	.long	34
	.long	35
	.long	36
	.long	37
	.long	38
	.long	39
	.long	40
	.long	41
	.long	42
	.long	43
	.long	44
	.long	45
	.long	46
	.long	47
	.long	48
	.long	49
	.long	50
	.long	51
	.long	52
	.long	53
	.long	54
	.long	55
	.long	56
	.long	57
	.long	58
	.long	59
	.long	60
	.long	61
	.long	62
	.long	63
	.long	64
	.long	65
	.long	66
	.long	67
	.long	68
	.long	69
	.long	70
	.long	71
	.long	72
	.long	73
	.long	74
	.long	75
	.long	76
	.long	77
	.long	78
	.long	79
	.long	80
	.long	81
	.long	82
	.long	83
	.long	84
	.long	85
	.long	86
	.long	87
	.long	88
	.long	89
	.long	90
	.long	91
	.long	92
	.long	93
	.long	94
	.long	95
	.long	96
	.long	97
	.long	98
	.long	99
	.long	100
	.long	101
	.long	102
	.long	103
	.long	104
	.long	105
	.long	106
	.long	107
	.long	108
	.long	109
	.long	110
	.long	111
	.long	112
	.long	113
	.long	114
	.long	115
	.long	116
	.long	117
	.long	118
	.long	119
	.long	120
	.long	121
	.long	122
	.long	123
	.long	124
	.long	125
	.long	126
	.long	127
	.long	128
	.long	129
	.long	130
	.long	131
	.long	132
	.long	133
	.long	134
	.long	135
	.long	136
	.long	137
	.long	138
	.long	139
	.long	140
	.long	141
	.long	142
	.long	143
	.long	144
	.long	145
	.long	146
	.long	147
	.long	148
	.long	149
	.long	150
	.long	151
	.long	152
	.long	153
	.long	154
	.long	155
	.long	156
	.long	157
	.long	158
	.long	159
	.long	160
	.long	161
	.long	162
	.long	163
	.long	164
	.long	165
	.long	166
	.long	167
	.long	168
	.long	169
	.long	170
	.long	171
	.long	172
	.long	173
	.long	174
	.long	175
	.long	176
	.long	177
	.long	178
	.long	179
	.long	180
	.long	181
	.long	182
	.long	183
	.long	184
	.long	185
	.long	186
	.long	187
	.long	188
	.long	189
	.long	190
	.long	191
	.long	192
	.long	193
	.long	194
	.long	195
	.long	196
	.long	197
	.long	198
	.long	199
	.long	200
	.long	201
	.long	202
	.long	203
	.long	204
	.long	205
	.long	206
	.long	207
	.long	208
	.long	209
	.long	210
	.long	211
	.long	212
	.long	213
	.long	214
	.long	215
	.long	216
	.long	217
	.long	218
	.long	219
	.long	220
	.long	221
	.long	222
	.long	223
	.long	224
	.long	225
	.long	226
	.long	227
	.long	228
	.long	229
	.long	230
	.long	231
	.long	232
	.long	233
	.long	234
	.long	235
	.long	236
	.long	237
	.long	238
	.long	239
	.long	240
	.long	241
	.long	242
	.long	243
	.long	244
	.long	245
	.long	246
	.long	247
	.long	248
	.long	249
	.long	250
	.long	251
	.long	252
	.long	253
	.long	254
	.long	255
	.long	256
	.long	257
	.long	258
	.long	259
	.long	260
	.long	261
	.long	262
	.long	263
	.long	264
	.long	265
	.long	266
	.long	267
	.long	268
	.long	269
	.long	270
	.long	271
	.long	272
	.long	273
	.long	274
	.long	275
	.long	276
	.long	277
	.long	278
	.long	279
	.long	280
	.long	281
	.long	282
	.long	283
	.long	284
	.long	285
	.long	286
	.long	287
	.long	288
	.long	289
	.long	290
	.long	291
	.long	292
	.long	293
	.long	294
	.long	295
	.long	296
	.long	297
	.long	298
	.long	299
	.long	300
	.long	301
	.long	302
	.long	303
	.long	304
	.long	305
	.long	306
	.long	307
	.long	308
	.long	309
	.long	310
	.long	311
	.long	312
	.long	313
	.long	314
	.long	315
	.long	316
	.long	317
	.long	318
	.long	319
	.long	320
	.long	321
	.long	322
	.long	323
	.long	324
	.long	325
	.long	326
	.long	327
	.long	328
	.long	329
	.long	330
	.long	331
	.long	332
	.long	333
	.long	334
	.long	335
	.long	336
	.long	337
	.long	338
	.long	339
	.long	340
	.long	341
	.long	342
	.long	343
	.long	344
	.long	345
	.long	346
	.long	347
	.long	348
	.long	349
	.long	350
	.long	351
	.long	352
	.long	353
	.long	354
	.long	355
	.long	356
	.long	357
	.long	358
	.long	359
	.long	360
	.long	361
	.long	362
	.long	363
	.long	364
	.long	365
	.long	366
	.long	367
	.long	368
	.long	369
	.long	370
	.long	371
	.long	372
	.long	373
	.long	374
	.long	375
	.long	376
	.long	377
	.long	378
	.long	379
	.long	380
	.long	381
	.long	382
	.long	383
	.long	384
	.long	385
	.long	386
	.long	387
	.long	388
	.long	389
	.long	390
	.long	391
	.long	392
	.long	393
	.long	394
	.long	395
	.long	396
	.long	397
	.long	398
	.long	399
	.long	400
	.long	401
	.long	402
	.long	403
	.long	404
	.long	405
	.long	406
	.long	407
	.long	408
	.long	409
	.long	410
	.long	411
	.long	412
	.long	413
	.long	414
	.long	415
	.long	416
	.long	417
	.long	418
	.long	419
	.long	420
	.long	421
	.long	422
	.long	423
	.long	424
	.long	425
	.long	426
	.long	427
	.long	428
	.long	429
	.long	430
	.long	431
	.long	432
	.long	433
	.long	434
	.long	435
	.long	436
	.long	437
	.long	438
	.long	439
	.long	440
	.long	441
	.long	442
	.long	443
	.long	444
	.long	445
	.long	446
	.long	447
	.long	448
	.long	449
	.long	450
	.long	451
	.long	452
	.long	453
	.long	454
	.long	455
	.long	456
	.long	457
	.long	458
	.long	459
	.long	460
	.long	461
	.long	462
	.long	463
	.long	464
	.long	465
	.long	466
	.section	.data.rel.ro._ZZN2v88internal22ExternalReferenceTable11AddBuiltinsEPiE10c_builtins,"aw"
	.align 32
	.type	_ZZN2v88internal22ExternalReferenceTable11AddBuiltinsEPiE10c_builtins, @object
	.size	_ZZN2v88internal22ExternalReferenceTable11AddBuiltinsEPiE10c_builtins, 2128
_ZZN2v88internal22ExternalReferenceTable11AddBuiltinsEPiE10c_builtins:
	.quad	_ZN2v88internal21Builtin_HandleApiCallEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_HandleApiCallAsFunctionEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal34Builtin_HandleApiCallAsConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal21Builtin_EmptyFunctionEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal15Builtin_IllegalEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_StrictPoisonPillThrowerEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal26Builtin_UnsupportedThrowerEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal19Builtin_ArrayConcatEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal26Builtin_ArrayPrototypeFillEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal16Builtin_ArrayPopEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal17Builtin_ArrayPushEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal18Builtin_ArrayShiftEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_ArrayUnshiftEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal30Builtin_ArrayBufferConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal46Builtin_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal41Builtin_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal25Builtin_ArrayBufferIsViewEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal33Builtin_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal25Builtin_BigIntConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal21Builtin_BigIntAsUintNEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_BigIntAsIntNEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal37Builtin_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_BigIntPrototypeToStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal30Builtin_BigIntPrototypeValueOfEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal40Builtin_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal38Builtin_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal40Builtin_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal38Builtin_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal38Builtin_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal40Builtin_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal49Builtin_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_CallSitePrototypeGetThisEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal38Builtin_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal33Builtin_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal37Builtin_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal35Builtin_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal33Builtin_CallSitePrototypeToStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_ConsoleDebugEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_ConsoleErrorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal19Builtin_ConsoleInfoEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal18Builtin_ConsoleLogEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal19Builtin_ConsoleWarnEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal18Builtin_ConsoleDirEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal21Builtin_ConsoleDirXmlEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_ConsoleTableEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_ConsoleTraceEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_ConsoleGroupEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal29Builtin_ConsoleGroupCollapsedEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal23Builtin_ConsoleGroupEndEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_ConsoleClearEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_ConsoleCountEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal25Builtin_ConsoleCountResetEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal21Builtin_ConsoleAssertEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal22Builtin_ConsoleProfileEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal25Builtin_ConsoleProfileEndEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal19Builtin_ConsoleTimeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal22Builtin_ConsoleTimeLogEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal22Builtin_ConsoleTimeEndEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal24Builtin_ConsoleTimeStampEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal22Builtin_ConsoleContextEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal27Builtin_DataViewConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal23Builtin_DateConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_DatePrototypeGetYearEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_DatePrototypeSetYearEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal15Builtin_DateNowEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal17Builtin_DateParseEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_DatePrototypeSetDateEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_DatePrototypeSetFullYearEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal29Builtin_DatePrototypeSetHoursEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_DatePrototypeSetMinutesEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal29Builtin_DatePrototypeSetMonthEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_DatePrototypeSetSecondsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_DatePrototypeSetTimeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal35Builtin_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal39Builtin_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal34Builtin_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal34Builtin_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal33Builtin_DatePrototypeToDateStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_DatePrototypeToISOStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_DatePrototypeToUTCStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal29Builtin_DatePrototypeToStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal33Builtin_DatePrototypeToTimeStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal27Builtin_DatePrototypeToJsonEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal15Builtin_DateUTCEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal24Builtin_ErrorConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal30Builtin_ErrorCaptureStackTraceEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal30Builtin_ErrorPrototypeToStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal17Builtin_MakeErrorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal22Builtin_MakeRangeErrorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal23Builtin_MakeSyntaxErrorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal21Builtin_MakeTypeErrorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_MakeURIErrorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal30Builtin_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal35Builtin_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal27Builtin_FunctionConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal29Builtin_FunctionPrototypeBindEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal33Builtin_FunctionPrototypeToStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_GeneratorFunctionConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_AsyncFunctionConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal23Builtin_GlobalDecodeURIEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_GlobalDecodeURIComponentEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal23Builtin_GlobalEncodeURIEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_GlobalEncodeURIComponentEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_GlobalEscapeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal22Builtin_GlobalUnescapeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal18Builtin_GlobalEvalEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal17Builtin_JsonParseEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal21Builtin_JsonStringifyEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal25Builtin_MapPrototypeClearEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_NumberPrototypeToExponentialEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal30Builtin_NumberPrototypeToFixedEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal37Builtin_NumberPrototypeToLocaleStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal34Builtin_NumberPrototypeToPrecisionEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_NumberPrototypeToStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal26Builtin_ObjectDefineGetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal30Builtin_ObjectDefinePropertiesEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_ObjectDefinePropertyEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal26Builtin_ObjectDefineSetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_ObjectFreezeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal39Builtin_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal35Builtin_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal22Builtin_ObjectIsFrozenEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal22Builtin_ObjectIsSealedEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal26Builtin_ObjectLookupGetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal26Builtin_ObjectLookupSetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal43Builtin_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal18Builtin_ObjectSealEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal17Builtin_IsPromiseEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal29Builtin_ReflectDefinePropertyEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal39Builtin_ReflectGetOwnPropertyDescriptorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal22Builtin_ReflectOwnKeysEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal18Builtin_ReflectSetEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_RegExpCapture1GetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_RegExpCapture2GetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_RegExpCapture3GetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_RegExpCapture4GetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_RegExpCapture5GetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_RegExpCapture6GetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_RegExpCapture7GetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_RegExpCapture8GetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_RegExpCapture9GetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal25Builtin_RegExpInputGetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal25Builtin_RegExpInputSetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal29Builtin_RegExpLastMatchGetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal29Builtin_RegExpLastParenGetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_RegExpLeftContextGetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_RegExpPrototypeToStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_RegExpRightContextGetterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal25Builtin_SetPrototypeClearEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal47Builtin_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal39Builtin_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal21Builtin_AtomicsNotifyEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal25Builtin_AtomicsIsLockFreeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal19Builtin_AtomicsWaitEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal19Builtin_AtomicsWakeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal27Builtin_StringFromCodePointEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal34Builtin_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal17Builtin_StringRawEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal25Builtin_SymbolConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal17Builtin_SymbolForEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_SymbolKeyForEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal33Builtin_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal37Builtin_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_TypedArrayPrototypeFillEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal35Builtin_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal34Builtin_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal38Builtin_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal34Builtin_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal41Builtin_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal30Builtin_IsTraceCategoryEnabledEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal13Builtin_TraceEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal44Builtin_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_FinalizationGroupConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal33Builtin_FinalizationGroupRegisterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal35Builtin_FinalizationGroupUnregisterEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal26Builtin_WeakRefConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal20Builtin_WeakRefDerefEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal27Builtin_CollatorConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_CollatorInternalCompareEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_CollatorPrototypeCompareEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal34Builtin_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal40Builtin_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal39Builtin_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal35Builtin_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal39Builtin_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal33Builtin_DateTimeFormatConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal37Builtin_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal42Builtin_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal49Builtin_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal44Builtin_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal46Builtin_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal40Builtin_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal29Builtin_ListFormatConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal42Builtin_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal25Builtin_LocaleConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_LocalePrototypeBaseNameEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_LocalePrototypeCalendarEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_LocalePrototypeCollationEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal32Builtin_LocalePrototypeHourCycleEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_LocalePrototypeLanguageEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_LocalePrototypeMaximizeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_LocalePrototypeMinimizeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal30Builtin_LocalePrototypeNumericEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal38Builtin_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal29Builtin_LocalePrototypeRegionEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal29Builtin_LocalePrototypeScriptEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_LocalePrototypeToStringEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal31Builtin_NumberFormatConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal40Builtin_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal41Builtin_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal42Builtin_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal44Builtin_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal38Builtin_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal30Builtin_PluralRulesConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal43Builtin_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal34Builtin_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal37Builtin_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal37Builtin_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal41Builtin_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal48Builtin_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal50Builtin_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal44Builtin_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal28Builtin_SegmenterConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal41Builtin_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal33Builtin_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal35Builtin_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal41Builtin_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal41Builtin_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal41Builtin_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal37Builtin_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal40Builtin_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal40Builtin_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal38Builtin_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal34Builtin_V8BreakIteratorConstructorEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal40Builtin_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal40Builtin_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal38Builtin_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal35Builtin_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal41Builtin_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal41Builtin_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal39Builtin_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal37Builtin_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal36Builtin_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal47Builtin_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.quad	_ZN2v88internal41Builtin_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE
	.globl	_ZN2v88internal22ExternalReferenceTable9ref_name_E
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC11:
	.string	"nullptr"
.LC12:
	.string	"abort_with_reason"
.LC13:
	.string	"double_absolute_constant"
.LC14:
	.string	"double_negate_constant"
.LC15:
	.string	"float_absolute_constant"
.LC16:
	.string	"float_negate_constant"
.LC17:
	.string	"LDoubleConstant::min_int"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"FLAG_mock_arraybuffer_allocator"
	.section	.rodata.str1.1
.LC19:
	.string	"LDoubleConstant::one_half"
.LC20:
	.string	"TracingFlags::runtime_stats"
.LC21:
	.string	"the_hole_nan"
.LC22:
	.string	"uint32_bias"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"Bytecodes::bytecode_size_table_address"
	.section	.rodata.str1.1
.LC24:
	.string	"check_object_type"
.LC25:
	.string	"ComputeSeededHash"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"Deoptimizer::ComputeOutputFrames()"
	.align 8
.LC27:
	.string	"copy_fast_number_jsarray_elements_to_typed_array"
	.align 8
.LC28:
	.string	"copy_typed_array_elements_slice"
	.align 8
.LC29:
	.string	"copy_typed_array_elements_to_typed_array"
	.section	.rodata.str1.1
.LC30:
	.string	"cpu_features"
.LC31:
	.string	"HandleScope::DeleteExtensions"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"Heap::EphemeronKeyWriteBarrierFromCode"
	.section	.rodata.str1.1
.LC33:
	.string	"f64_acos_wrapper"
.LC34:
	.string	"f64_asin_wrapper"
.LC35:
	.string	"f64_mod_wrapper"
.LC36:
	.string	"JSDate::GetField"
.LC37:
	.string	"get_or_create_hash_raw"
.LC38:
	.string	"base::ieee754::acos"
.LC39:
	.string	"base::ieee754::acosh"
.LC40:
	.string	"base::ieee754::asin"
.LC41:
	.string	"base::ieee754::asinh"
.LC42:
	.string	"base::ieee754::atan"
.LC43:
	.string	"base::ieee754::atan2"
.LC44:
	.string	"base::ieee754::atanh"
.LC45:
	.string	"base::ieee754::cbrt"
.LC46:
	.string	"base::ieee754::cos"
.LC47:
	.string	"base::ieee754::cosh"
.LC48:
	.string	"base::ieee754::exp"
.LC49:
	.string	"base::ieee754::expm1"
.LC50:
	.string	"base::ieee754::log"
.LC51:
	.string	"base::ieee754::log10"
.LC52:
	.string	"base::ieee754::log1p"
.LC53:
	.string	"base::ieee754::log2"
.LC54:
	.string	"base::ieee754::pow"
.LC55:
	.string	"base::ieee754::sin"
.LC56:
	.string	"base::ieee754::sinh"
.LC57:
	.string	"base::ieee754::tan"
.LC58:
	.string	"base::ieee754::tanh"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"IncrementalMarking::RecordWrite"
	.align 8
.LC60:
	.string	"JSObject::InvalidatePrototypeChains()"
	.section	.rodata.str1.1
.LC61:
	.string	"InvokeAccessorGetterCallback"
.LC62:
	.string	"InvokeFunctionCallback"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"jsarray_array_join_concat_to_sequential_string"
	.align 8
.LC64:
	.string	"jsreceiver_create_identity_hash"
	.section	.rodata.str1.1
.LC65:
	.string	"libc_memchr"
.LC66:
	.string	"libc_memcpy"
.LC67:
	.string	"libc_memmove"
.LC68:
	.string	"libc_memset"
.LC69:
	.string	"mod_two_doubles"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"MutableBigInt_AbsoluteAddAndCanonicalize"
	.section	.rodata.str1.1
.LC71:
	.string	"MutableBigInt_AbsoluteCompare"
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"MutableBigInt_AbsoluteSubAndCanonicalize"
	.section	.rodata.str1.1
.LC73:
	.string	"Deoptimizer::New()"
.LC74:
	.string	"orderedhashmap_gethash_raw"
.LC75:
	.string	"printf"
.LC76:
	.string	"MathRandom::RefillCache"
.LC77:
	.string	"search_string_raw_one_one"
.LC78:
	.string	"search_string_raw_one_two"
.LC79:
	.string	"search_string_raw_two_one"
.LC80:
	.string	"search_string_raw_two_two"
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"smi_lexicographic_compare_function"
	.align 8
.LC82:
	.string	"StoreBuffer::StoreBufferOverflow"
	.align 8
.LC83:
	.string	"try_internalize_string_function"
	.align 8
.LC84:
	.string	"wasm::call_trap_callback_for_testing"
	.section	.rodata.str1.1
.LC85:
	.string	"wasm::f32_ceil_wrapper"
.LC86:
	.string	"wasm::f32_floor_wrapper"
.LC87:
	.string	"wasm::f32_nearest_int_wrapper"
.LC88:
	.string	"wasm::f32_trunc_wrapper"
.LC89:
	.string	"wasm::f64_ceil_wrapper"
.LC90:
	.string	"wasm::f64_floor_wrapper"
.LC91:
	.string	"wasm::f64_nearest_int_wrapper"
.LC92:
	.string	"wasm::f64_trunc_wrapper"
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"wasm::float32_to_int64_wrapper"
	.align 8
.LC94:
	.string	"wasm::float32_to_uint64_wrapper"
	.section	.rodata.str1.1
.LC95:
	.string	"wasm::float64_pow"
	.section	.rodata.str1.8
	.align 8
.LC96:
	.string	"wasm::float64_to_int64_wrapper"
	.align 8
.LC97:
	.string	"wasm::float64_to_uint64_wrapper"
	.section	.rodata.str1.1
.LC98:
	.string	"wasm::int64_div"
.LC99:
	.string	"wasm::int64_mod"
	.section	.rodata.str1.8
	.align 8
.LC100:
	.string	"wasm::int64_to_float32_wrapper"
	.align 8
.LC101:
	.string	"wasm::int64_to_float64_wrapper"
	.section	.rodata.str1.1
.LC102:
	.string	"wasm::uint64_div"
.LC103:
	.string	"wasm::uint64_mod"
	.section	.rodata.str1.8
	.align 8
.LC104:
	.string	"wasm::uint64_to_float32_wrapper"
	.align 8
.LC105:
	.string	"wasm::uint64_to_float64_wrapper"
	.section	.rodata.str1.1
.LC106:
	.string	"wasm::word32_ctz"
.LC107:
	.string	"wasm::word32_popcnt"
.LC108:
	.string	"wasm::word32_rol"
.LC109:
	.string	"wasm::word32_ror"
.LC110:
	.string	"wasm::word64_ctz"
.LC111:
	.string	"wasm::word64_popcnt"
.LC112:
	.string	"wasm::memory_copy"
.LC113:
	.string	"wasm::memory_fill"
	.section	.rodata.str1.8
	.align 8
.LC114:
	.string	"MicrotaskQueue::CallEnqueueMicrotask"
	.section	.rodata.str1.1
.LC115:
	.string	"call_enter_context_function"
.LC116:
	.string	"atomic_pair_load_function"
.LC117:
	.string	"atomic_pair_store_function"
.LC118:
	.string	"atomic_pair_add_function"
.LC119:
	.string	"atomic_pair_sub_function"
.LC120:
	.string	"atomic_pair_and_function"
.LC121:
	.string	"atomic_pair_or_function"
.LC122:
	.string	"atomic_pair_xor_function"
.LC123:
	.string	"atomic_pair_exchange_function"
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"atomic_pair_compare_exchange_function"
	.align 8
.LC125:
	.string	"intl_convert_one_byte_to_lower"
	.section	.rodata.str1.1
.LC126:
	.string	"intl_to_latin1_lower_table"
.LC127:
	.string	"isolate"
.LC128:
	.string	"builtins"
	.section	.rodata.str1.8
	.align 8
.LC129:
	.string	"Isolate::handle_scope_implementer_address"
	.align 8
.LC130:
	.string	"Address of the InterpreterEntryTrampoline instruction start"
	.align 8
.LC131:
	.string	"Interpreter::dispatch_counters"
	.align 8
.LC132:
	.string	"Interpreter::dispatch_table_address"
	.section	.rodata.str1.1
.LC133:
	.string	"date_cache_stamp"
	.section	.rodata.str1.8
	.align 8
.LC134:
	.string	"Isolate::stress_deopt_count_address()"
	.align 8
.LC135:
	.string	"Isolate::force_slow_path_address()"
	.section	.rodata.str1.1
.LC136:
	.string	"Isolate::isolate_root()"
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"Heap::allocation_sites_list_address()"
	.align 8
.LC138:
	.string	"StackGuard::address_of_jslimit()"
	.align 8
.LC139:
	.string	"StackGuard::address_of_real_jslimit()"
	.section	.rodata.str1.1
.LC140:
	.string	"store_buffer_top"
.LC141:
	.string	"heap_is_marking_flag_address"
	.section	.rodata.str1.8
	.align 8
.LC142:
	.string	"Heap::NewSpaceAllocationTopAddress()"
	.align 8
.LC143:
	.string	"Heap::NewSpaceAllocationLimitAddress()"
	.align 8
.LC144:
	.string	"Heap::OldSpaceAllocationTopAddress"
	.align 8
.LC145:
	.string	"Heap::OldSpaceAllocationLimitAddress"
	.section	.rodata.str1.1
.LC146:
	.string	"HandleScope::level"
.LC147:
	.string	"HandleScope::next"
.LC148:
	.string	"HandleScope::limit"
.LC149:
	.string	"Isolate::scheduled_exception"
	.section	.rodata.str1.8
	.align 8
.LC150:
	.string	"address_of_pending_message_obj"
	.align 8
.LC151:
	.string	"Isolate::promise_hook_address()"
	.align 8
.LC152:
	.string	"Isolate::async_event_delegate_address()"
	.align 8
.LC153:
	.string	"Isolate::promise_hook_or_async_event_delegate_address()"
	.align 8
.LC154:
	.string	"Isolate::promise_hook_or_debug_is_active_or_async_event_delegate_address()"
	.align 8
.LC155:
	.string	"Isolate::debug_execution_mode_address()"
	.section	.rodata.str1.1
.LC156:
	.string	"Debug::is_active_address()"
	.section	.rodata.str1.8
	.align 8
.LC157:
	.string	"Debug::hook_on_function_call_address()"
	.align 8
.LC158:
	.string	"Runtime::runtime_function_table_address()"
	.section	.rodata.str1.1
.LC159:
	.string	"Isolate::is_profiling"
	.section	.rodata.str1.8
	.align 8
.LC160:
	.string	"Debug::step_suspended_generator_address()"
	.section	.rodata.str1.1
.LC161:
	.string	"Debug::restart_fp_address()"
	.section	.rodata.str1.8
	.align 8
.LC162:
	.string	"IsolateData::fast_c_call_caller_fp_address"
	.align 8
.LC163:
	.string	"IsolateData::fast_c_call_caller_pc_address"
	.align 8
.LC164:
	.string	"IsolateData::stack_is_iterable_address"
	.align 8
.LC165:
	.string	"RegExpStack::limit_address_address()"
	.align 8
.LC166:
	.string	"RegExpStack::memory_address_address()"
	.align 8
.LC167:
	.string	"RegExpStack::memory_size_address()"
	.align 8
.LC168:
	.string	"RegExpStack::memory_top_address_address()"
	.align 8
.LC169:
	.string	"OffsetsVector::static_offsets_vector"
	.align 8
.LC170:
	.string	"NativeRegExpMacroAssembler::CaseInsensitiveCompareUC16()"
	.align 8
.LC171:
	.string	"RegExpMacroAssembler*::CheckStackGuardState()"
	.align 8
.LC172:
	.string	"NativeRegExpMacroAssembler::GrowStack()"
	.align 8
.LC173:
	.string	"IrregexpInterpreter::MatchForCallFromJs"
	.align 8
.LC174:
	.string	"NativeRegExpMacroAssembler::word_character_map"
	.section	.rodata.str1.1
.LC175:
	.string	"Builtin_HandleApiCall"
	.section	.rodata.str1.8
	.align 8
.LC176:
	.string	"Builtin_HandleApiCallAsFunction"
	.align 8
.LC177:
	.string	"Builtin_HandleApiCallAsConstructor"
	.section	.rodata.str1.1
.LC178:
	.string	"Builtin_EmptyFunction"
.LC179:
	.string	"Builtin_Illegal"
	.section	.rodata.str1.8
	.align 8
.LC180:
	.string	"Builtin_StrictPoisonPillThrower"
	.section	.rodata.str1.1
.LC181:
	.string	"Builtin_UnsupportedThrower"
.LC182:
	.string	"Builtin_ArrayConcat"
.LC183:
	.string	"Builtin_ArrayPrototypeFill"
.LC184:
	.string	"Builtin_ArrayPop"
.LC185:
	.string	"Builtin_ArrayPush"
.LC186:
	.string	"Builtin_ArrayShift"
.LC187:
	.string	"Builtin_ArrayUnshift"
	.section	.rodata.str1.8
	.align 8
.LC188:
	.string	"Builtin_ArrayBufferConstructor"
	.align 8
.LC189:
	.string	"Builtin_ArrayBufferConstructor_DoNotInitialize"
	.align 8
.LC190:
	.string	"Builtin_ArrayBufferPrototypeGetByteLength"
	.section	.rodata.str1.1
.LC191:
	.string	"Builtin_ArrayBufferIsView"
	.section	.rodata.str1.8
	.align 8
.LC192:
	.string	"Builtin_ArrayBufferPrototypeSlice"
	.section	.rodata.str1.1
.LC193:
	.string	"Builtin_BigIntConstructor"
.LC194:
	.string	"Builtin_BigIntAsUintN"
.LC195:
	.string	"Builtin_BigIntAsIntN"
	.section	.rodata.str1.8
	.align 8
.LC196:
	.string	"Builtin_BigIntPrototypeToLocaleString"
	.align 8
.LC197:
	.string	"Builtin_BigIntPrototypeToString"
	.align 8
.LC198:
	.string	"Builtin_BigIntPrototypeValueOf"
	.align 8
.LC199:
	.string	"Builtin_CallSitePrototypeGetColumnNumber"
	.align 8
.LC200:
	.string	"Builtin_CallSitePrototypeGetEvalOrigin"
	.align 8
.LC201:
	.string	"Builtin_CallSitePrototypeGetFileName"
	.align 8
.LC202:
	.string	"Builtin_CallSitePrototypeGetFunction"
	.align 8
.LC203:
	.string	"Builtin_CallSitePrototypeGetFunctionName"
	.align 8
.LC204:
	.string	"Builtin_CallSitePrototypeGetLineNumber"
	.align 8
.LC205:
	.string	"Builtin_CallSitePrototypeGetMethodName"
	.align 8
.LC206:
	.string	"Builtin_CallSitePrototypeGetPosition"
	.align 8
.LC207:
	.string	"Builtin_CallSitePrototypeGetPromiseIndex"
	.align 8
.LC208:
	.string	"Builtin_CallSitePrototypeGetScriptNameOrSourceURL"
	.align 8
.LC209:
	.string	"Builtin_CallSitePrototypeGetThis"
	.align 8
.LC210:
	.string	"Builtin_CallSitePrototypeGetTypeName"
	.align 8
.LC211:
	.string	"Builtin_CallSitePrototypeIsAsync"
	.align 8
.LC212:
	.string	"Builtin_CallSitePrototypeIsConstructor"
	.align 8
.LC213:
	.string	"Builtin_CallSitePrototypeIsEval"
	.align 8
.LC214:
	.string	"Builtin_CallSitePrototypeIsNative"
	.align 8
.LC215:
	.string	"Builtin_CallSitePrototypeIsPromiseAll"
	.align 8
.LC216:
	.string	"Builtin_CallSitePrototypeIsToplevel"
	.align 8
.LC217:
	.string	"Builtin_CallSitePrototypeToString"
	.section	.rodata.str1.1
.LC218:
	.string	"Builtin_ConsoleDebug"
.LC219:
	.string	"Builtin_ConsoleError"
.LC220:
	.string	"Builtin_ConsoleInfo"
.LC221:
	.string	"Builtin_ConsoleLog"
.LC222:
	.string	"Builtin_ConsoleWarn"
.LC223:
	.string	"Builtin_ConsoleDir"
.LC224:
	.string	"Builtin_ConsoleDirXml"
.LC225:
	.string	"Builtin_ConsoleTable"
.LC226:
	.string	"Builtin_ConsoleTrace"
.LC227:
	.string	"Builtin_ConsoleGroup"
.LC228:
	.string	"Builtin_ConsoleGroupCollapsed"
.LC229:
	.string	"Builtin_ConsoleGroupEnd"
.LC230:
	.string	"Builtin_ConsoleClear"
.LC231:
	.string	"Builtin_ConsoleCount"
.LC232:
	.string	"Builtin_ConsoleCountReset"
.LC233:
	.string	"Builtin_ConsoleAssert"
.LC234:
	.string	"Builtin_ConsoleProfile"
.LC235:
	.string	"Builtin_ConsoleProfileEnd"
.LC236:
	.string	"Builtin_ConsoleTime"
.LC237:
	.string	"Builtin_ConsoleTimeLog"
.LC238:
	.string	"Builtin_ConsoleTimeEnd"
.LC239:
	.string	"Builtin_ConsoleTimeStamp"
.LC240:
	.string	"Builtin_ConsoleContext"
.LC241:
	.string	"Builtin_DataViewConstructor"
.LC242:
	.string	"Builtin_DateConstructor"
.LC243:
	.string	"Builtin_DatePrototypeGetYear"
.LC244:
	.string	"Builtin_DatePrototypeSetYear"
.LC245:
	.string	"Builtin_DateNow"
.LC246:
	.string	"Builtin_DateParse"
.LC247:
	.string	"Builtin_DatePrototypeSetDate"
	.section	.rodata.str1.8
	.align 8
.LC248:
	.string	"Builtin_DatePrototypeSetFullYear"
	.section	.rodata.str1.1
.LC249:
	.string	"Builtin_DatePrototypeSetHours"
	.section	.rodata.str1.8
	.align 8
.LC250:
	.string	"Builtin_DatePrototypeSetMilliseconds"
	.align 8
.LC251:
	.string	"Builtin_DatePrototypeSetMinutes"
	.section	.rodata.str1.1
.LC252:
	.string	"Builtin_DatePrototypeSetMonth"
	.section	.rodata.str1.8
	.align 8
.LC253:
	.string	"Builtin_DatePrototypeSetSeconds"
	.section	.rodata.str1.1
.LC254:
	.string	"Builtin_DatePrototypeSetTime"
	.section	.rodata.str1.8
	.align 8
.LC255:
	.string	"Builtin_DatePrototypeSetUTCDate"
	.align 8
.LC256:
	.string	"Builtin_DatePrototypeSetUTCFullYear"
	.align 8
.LC257:
	.string	"Builtin_DatePrototypeSetUTCHours"
	.align 8
.LC258:
	.string	"Builtin_DatePrototypeSetUTCMilliseconds"
	.align 8
.LC259:
	.string	"Builtin_DatePrototypeSetUTCMinutes"
	.align 8
.LC260:
	.string	"Builtin_DatePrototypeSetUTCMonth"
	.align 8
.LC261:
	.string	"Builtin_DatePrototypeSetUTCSeconds"
	.align 8
.LC262:
	.string	"Builtin_DatePrototypeToDateString"
	.align 8
.LC263:
	.string	"Builtin_DatePrototypeToISOString"
	.align 8
.LC264:
	.string	"Builtin_DatePrototypeToUTCString"
	.section	.rodata.str1.1
.LC265:
	.string	"Builtin_DatePrototypeToString"
	.section	.rodata.str1.8
	.align 8
.LC266:
	.string	"Builtin_DatePrototypeToTimeString"
	.section	.rodata.str1.1
.LC267:
	.string	"Builtin_DatePrototypeToJson"
.LC268:
	.string	"Builtin_DateUTC"
.LC269:
	.string	"Builtin_ErrorConstructor"
	.section	.rodata.str1.8
	.align 8
.LC270:
	.string	"Builtin_ErrorCaptureStackTrace"
	.align 8
.LC271:
	.string	"Builtin_ErrorPrototypeToString"
	.section	.rodata.str1.1
.LC272:
	.string	"Builtin_MakeError"
.LC273:
	.string	"Builtin_MakeRangeError"
.LC274:
	.string	"Builtin_MakeSyntaxError"
.LC275:
	.string	"Builtin_MakeTypeError"
.LC276:
	.string	"Builtin_MakeURIError"
	.section	.rodata.str1.8
	.align 8
.LC277:
	.string	"Builtin_ExtrasUtilsUncurryThis"
	.align 8
.LC278:
	.string	"Builtin_ExtrasUtilsCallReflectApply"
	.section	.rodata.str1.1
.LC279:
	.string	"Builtin_FunctionConstructor"
.LC280:
	.string	"Builtin_FunctionPrototypeBind"
	.section	.rodata.str1.8
	.align 8
.LC281:
	.string	"Builtin_FunctionPrototypeToString"
	.align 8
.LC282:
	.string	"Builtin_GeneratorFunctionConstructor"
	.align 8
.LC283:
	.string	"Builtin_AsyncFunctionConstructor"
	.section	.rodata.str1.1
.LC284:
	.string	"Builtin_GlobalDecodeURI"
	.section	.rodata.str1.8
	.align 8
.LC285:
	.string	"Builtin_GlobalDecodeURIComponent"
	.section	.rodata.str1.1
.LC286:
	.string	"Builtin_GlobalEncodeURI"
	.section	.rodata.str1.8
	.align 8
.LC287:
	.string	"Builtin_GlobalEncodeURIComponent"
	.section	.rodata.str1.1
.LC288:
	.string	"Builtin_GlobalEscape"
.LC289:
	.string	"Builtin_GlobalUnescape"
.LC290:
	.string	"Builtin_GlobalEval"
.LC291:
	.string	"Builtin_JsonParse"
.LC292:
	.string	"Builtin_JsonStringify"
.LC293:
	.string	"Builtin_MapPrototypeClear"
	.section	.rodata.str1.8
	.align 8
.LC294:
	.string	"Builtin_NumberPrototypeToExponential"
	.align 8
.LC295:
	.string	"Builtin_NumberPrototypeToFixed"
	.align 8
.LC296:
	.string	"Builtin_NumberPrototypeToLocaleString"
	.align 8
.LC297:
	.string	"Builtin_NumberPrototypeToPrecision"
	.align 8
.LC298:
	.string	"Builtin_NumberPrototypeToString"
	.section	.rodata.str1.1
.LC299:
	.string	"Builtin_ObjectDefineGetter"
	.section	.rodata.str1.8
	.align 8
.LC300:
	.string	"Builtin_ObjectDefineProperties"
	.section	.rodata.str1.1
.LC301:
	.string	"Builtin_ObjectDefineProperty"
.LC302:
	.string	"Builtin_ObjectDefineSetter"
.LC303:
	.string	"Builtin_ObjectFreeze"
	.section	.rodata.str1.8
	.align 8
.LC304:
	.string	"Builtin_ObjectGetOwnPropertyDescriptors"
	.align 8
.LC305:
	.string	"Builtin_ObjectGetOwnPropertySymbols"
	.section	.rodata.str1.1
.LC306:
	.string	"Builtin_ObjectIsFrozen"
.LC307:
	.string	"Builtin_ObjectIsSealed"
.LC308:
	.string	"Builtin_ObjectLookupGetter"
.LC309:
	.string	"Builtin_ObjectLookupSetter"
	.section	.rodata.str1.8
	.align 8
.LC310:
	.string	"Builtin_ObjectPrototypePropertyIsEnumerable"
	.align 8
.LC311:
	.string	"Builtin_ObjectPrototypeGetProto"
	.align 8
.LC312:
	.string	"Builtin_ObjectPrototypeSetProto"
	.section	.rodata.str1.1
.LC313:
	.string	"Builtin_ObjectSeal"
.LC314:
	.string	"Builtin_IsPromise"
.LC315:
	.string	"Builtin_ReflectDefineProperty"
	.section	.rodata.str1.8
	.align 8
.LC316:
	.string	"Builtin_ReflectGetOwnPropertyDescriptor"
	.section	.rodata.str1.1
.LC317:
	.string	"Builtin_ReflectOwnKeys"
.LC318:
	.string	"Builtin_ReflectSet"
.LC319:
	.string	"Builtin_RegExpCapture1Getter"
.LC320:
	.string	"Builtin_RegExpCapture2Getter"
.LC321:
	.string	"Builtin_RegExpCapture3Getter"
.LC322:
	.string	"Builtin_RegExpCapture4Getter"
.LC323:
	.string	"Builtin_RegExpCapture5Getter"
.LC324:
	.string	"Builtin_RegExpCapture6Getter"
.LC325:
	.string	"Builtin_RegExpCapture7Getter"
.LC326:
	.string	"Builtin_RegExpCapture8Getter"
.LC327:
	.string	"Builtin_RegExpCapture9Getter"
.LC328:
	.string	"Builtin_RegExpInputGetter"
.LC329:
	.string	"Builtin_RegExpInputSetter"
.LC330:
	.string	"Builtin_RegExpLastMatchGetter"
.LC331:
	.string	"Builtin_RegExpLastParenGetter"
	.section	.rodata.str1.8
	.align 8
.LC332:
	.string	"Builtin_RegExpLeftContextGetter"
	.align 8
.LC333:
	.string	"Builtin_RegExpPrototypeToString"
	.align 8
.LC334:
	.string	"Builtin_RegExpRightContextGetter"
	.section	.rodata.str1.1
.LC335:
	.string	"Builtin_SetPrototypeClear"
	.section	.rodata.str1.8
	.align 8
.LC336:
	.string	"Builtin_SharedArrayBufferPrototypeGetByteLength"
	.align 8
.LC337:
	.string	"Builtin_SharedArrayBufferPrototypeSlice"
	.section	.rodata.str1.1
.LC338:
	.string	"Builtin_AtomicsNotify"
.LC339:
	.string	"Builtin_AtomicsIsLockFree"
.LC340:
	.string	"Builtin_AtomicsWait"
.LC341:
	.string	"Builtin_AtomicsWake"
.LC342:
	.string	"Builtin_StringFromCodePoint"
	.section	.rodata.str1.8
	.align 8
.LC343:
	.string	"Builtin_StringPrototypeLastIndexOf"
	.align 8
.LC344:
	.string	"Builtin_StringPrototypeLocaleCompare"
	.section	.rodata.str1.1
.LC345:
	.string	"Builtin_StringRaw"
.LC346:
	.string	"Builtin_SymbolConstructor"
.LC347:
	.string	"Builtin_SymbolFor"
.LC348:
	.string	"Builtin_SymbolKeyFor"
	.section	.rodata.str1.8
	.align 8
.LC349:
	.string	"Builtin_TypedArrayPrototypeBuffer"
	.align 8
.LC350:
	.string	"Builtin_TypedArrayPrototypeCopyWithin"
	.align 8
.LC351:
	.string	"Builtin_TypedArrayPrototypeFill"
	.align 8
.LC352:
	.string	"Builtin_TypedArrayPrototypeIncludes"
	.align 8
.LC353:
	.string	"Builtin_TypedArrayPrototypeIndexOf"
	.align 8
.LC354:
	.string	"Builtin_TypedArrayPrototypeLastIndexOf"
	.align 8
.LC355:
	.string	"Builtin_TypedArrayPrototypeReverse"
	.align 8
.LC356:
	.string	"Builtin_AsyncGeneratorFunctionConstructor"
	.align 8
.LC357:
	.string	"Builtin_IsTraceCategoryEnabled"
	.section	.rodata.str1.1
.LC358:
	.string	"Builtin_Trace"
	.section	.rodata.str1.8
	.align 8
.LC359:
	.string	"Builtin_FinalizationGroupCleanupIteratorNext"
	.align 8
.LC360:
	.string	"Builtin_FinalizationGroupCleanupSome"
	.align 8
.LC361:
	.string	"Builtin_FinalizationGroupConstructor"
	.align 8
.LC362:
	.string	"Builtin_FinalizationGroupRegister"
	.align 8
.LC363:
	.string	"Builtin_FinalizationGroupUnregister"
	.section	.rodata.str1.1
.LC364:
	.string	"Builtin_WeakRefConstructor"
.LC365:
	.string	"Builtin_WeakRefDeref"
.LC366:
	.string	"Builtin_CollatorConstructor"
	.section	.rodata.str1.8
	.align 8
.LC367:
	.string	"Builtin_CollatorInternalCompare"
	.align 8
.LC368:
	.string	"Builtin_CollatorPrototypeCompare"
	.align 8
.LC369:
	.string	"Builtin_CollatorSupportedLocalesOf"
	.align 8
.LC370:
	.string	"Builtin_CollatorPrototypeResolvedOptions"
	.align 8
.LC371:
	.string	"Builtin_DatePrototypeToLocaleDateString"
	.align 8
.LC372:
	.string	"Builtin_DatePrototypeToLocaleString"
	.align 8
.LC373:
	.string	"Builtin_DatePrototypeToLocaleTimeString"
	.align 8
.LC374:
	.string	"Builtin_DateTimeFormatConstructor"
	.align 8
.LC375:
	.string	"Builtin_DateTimeFormatInternalFormat"
	.align 8
.LC376:
	.string	"Builtin_DateTimeFormatPrototypeFormat"
	.align 8
.LC377:
	.string	"Builtin_DateTimeFormatPrototypeFormatRange"
	.align 8
.LC378:
	.string	"Builtin_DateTimeFormatPrototypeFormatRangeToParts"
	.align 8
.LC379:
	.string	"Builtin_DateTimeFormatPrototypeFormatToParts"
	.align 8
.LC380:
	.string	"Builtin_DateTimeFormatPrototypeResolvedOptions"
	.align 8
.LC381:
	.string	"Builtin_DateTimeFormatSupportedLocalesOf"
	.align 8
.LC382:
	.string	"Builtin_IntlGetCanonicalLocales"
	.section	.rodata.str1.1
.LC383:
	.string	"Builtin_ListFormatConstructor"
	.section	.rodata.str1.8
	.align 8
.LC384:
	.string	"Builtin_ListFormatPrototypeResolvedOptions"
	.align 8
.LC385:
	.string	"Builtin_ListFormatSupportedLocalesOf"
	.section	.rodata.str1.1
.LC386:
	.string	"Builtin_LocaleConstructor"
	.section	.rodata.str1.8
	.align 8
.LC387:
	.string	"Builtin_LocalePrototypeBaseName"
	.align 8
.LC388:
	.string	"Builtin_LocalePrototypeCalendar"
	.align 8
.LC389:
	.string	"Builtin_LocalePrototypeCaseFirst"
	.align 8
.LC390:
	.string	"Builtin_LocalePrototypeCollation"
	.align 8
.LC391:
	.string	"Builtin_LocalePrototypeHourCycle"
	.align 8
.LC392:
	.string	"Builtin_LocalePrototypeLanguage"
	.align 8
.LC393:
	.string	"Builtin_LocalePrototypeMaximize"
	.align 8
.LC394:
	.string	"Builtin_LocalePrototypeMinimize"
	.align 8
.LC395:
	.string	"Builtin_LocalePrototypeNumeric"
	.align 8
.LC396:
	.string	"Builtin_LocalePrototypeNumberingSystem"
	.section	.rodata.str1.1
.LC397:
	.string	"Builtin_LocalePrototypeRegion"
.LC398:
	.string	"Builtin_LocalePrototypeScript"
	.section	.rodata.str1.8
	.align 8
.LC399:
	.string	"Builtin_LocalePrototypeToString"
	.align 8
.LC400:
	.string	"Builtin_NumberFormatConstructor"
	.align 8
.LC401:
	.string	"Builtin_NumberFormatInternalFormatNumber"
	.align 8
.LC402:
	.string	"Builtin_NumberFormatPrototypeFormatNumber"
	.align 8
.LC403:
	.string	"Builtin_NumberFormatPrototypeFormatToParts"
	.align 8
.LC404:
	.string	"Builtin_NumberFormatPrototypeResolvedOptions"
	.align 8
.LC405:
	.string	"Builtin_NumberFormatSupportedLocalesOf"
	.align 8
.LC406:
	.string	"Builtin_PluralRulesConstructor"
	.align 8
.LC407:
	.string	"Builtin_PluralRulesPrototypeResolvedOptions"
	.align 8
.LC408:
	.string	"Builtin_PluralRulesPrototypeSelect"
	.align 8
.LC409:
	.string	"Builtin_PluralRulesSupportedLocalesOf"
	.align 8
.LC410:
	.string	"Builtin_RelativeTimeFormatConstructor"
	.align 8
.LC411:
	.string	"Builtin_RelativeTimeFormatPrototypeFormat"
	.align 8
.LC412:
	.string	"Builtin_RelativeTimeFormatPrototypeFormatToParts"
	.align 8
.LC413:
	.string	"Builtin_RelativeTimeFormatPrototypeResolvedOptions"
	.align 8
.LC414:
	.string	"Builtin_RelativeTimeFormatSupportedLocalesOf"
	.section	.rodata.str1.1
.LC415:
	.string	"Builtin_SegmenterConstructor"
	.section	.rodata.str1.8
	.align 8
.LC416:
	.string	"Builtin_SegmenterPrototypeResolvedOptions"
	.align 8
.LC417:
	.string	"Builtin_SegmenterPrototypeSegment"
	.align 8
.LC418:
	.string	"Builtin_SegmenterSupportedLocalesOf"
	.align 8
.LC419:
	.string	"Builtin_SegmentIteratorPrototypeBreakType"
	.align 8
.LC420:
	.string	"Builtin_SegmentIteratorPrototypeFollowing"
	.align 8
.LC421:
	.string	"Builtin_SegmentIteratorPrototypePreceding"
	.align 8
.LC422:
	.string	"Builtin_SegmentIteratorPrototypeIndex"
	.align 8
.LC423:
	.string	"Builtin_SegmentIteratorPrototypeNext"
	.align 8
.LC424:
	.string	"Builtin_StringPrototypeNormalizeIntl"
	.align 8
.LC425:
	.string	"Builtin_StringPrototypeToLocaleLowerCase"
	.align 8
.LC426:
	.string	"Builtin_StringPrototypeToLocaleUpperCase"
	.align 8
.LC427:
	.string	"Builtin_StringPrototypeToUpperCaseIntl"
	.align 8
.LC428:
	.string	"Builtin_V8BreakIteratorConstructor"
	.align 8
.LC429:
	.string	"Builtin_V8BreakIteratorInternalAdoptText"
	.align 8
.LC430:
	.string	"Builtin_V8BreakIteratorInternalBreakType"
	.align 8
.LC431:
	.string	"Builtin_V8BreakIteratorInternalCurrent"
	.align 8
.LC432:
	.string	"Builtin_V8BreakIteratorInternalFirst"
	.align 8
.LC433:
	.string	"Builtin_V8BreakIteratorInternalNext"
	.align 8
.LC434:
	.string	"Builtin_V8BreakIteratorPrototypeAdoptText"
	.align 8
.LC435:
	.string	"Builtin_V8BreakIteratorPrototypeBreakType"
	.align 8
.LC436:
	.string	"Builtin_V8BreakIteratorPrototypeCurrent"
	.align 8
.LC437:
	.string	"Builtin_V8BreakIteratorPrototypeFirst"
	.align 8
.LC438:
	.string	"Builtin_V8BreakIteratorPrototypeNext"
	.align 8
.LC439:
	.string	"Builtin_V8BreakIteratorPrototypeResolvedOptions"
	.align 8
.LC440:
	.string	"Builtin_V8BreakIteratorSupportedLocalesOf"
	.section	.rodata.str1.1
.LC441:
	.string	"Runtime::DebugBreakOnBytecode"
	.section	.rodata.str1.8
	.align 8
.LC442:
	.string	"Runtime::LoadLookupSlotForCall"
	.section	.rodata.str1.1
.LC443:
	.string	"Runtime::ArrayIncludes_Slow"
.LC444:
	.string	"Runtime::ArrayIndexOf"
.LC445:
	.string	"Runtime::ArrayIsArray"
	.section	.rodata.str1.8
	.align 8
.LC446:
	.string	"Runtime::ArraySpeciesConstructor"
	.section	.rodata.str1.1
.LC447:
	.string	"Runtime::GrowArrayElements"
.LC448:
	.string	"Runtime::IsArray"
.LC449:
	.string	"Runtime::NewArray"
.LC450:
	.string	"Runtime::NormalizeElements"
	.section	.rodata.str1.8
	.align 8
.LC451:
	.string	"Runtime::TransitionElementsKind"
	.align 8
.LC452:
	.string	"Runtime::TransitionElementsKindWithKind"
	.section	.rodata.str1.1
.LC453:
	.string	"Runtime::AtomicsLoad64"
.LC454:
	.string	"Runtime::AtomicsStore64"
.LC455:
	.string	"Runtime::AtomicsAdd"
.LC456:
	.string	"Runtime::AtomicsAnd"
	.section	.rodata.str1.8
	.align 8
.LC457:
	.string	"Runtime::AtomicsCompareExchange"
	.section	.rodata.str1.1
.LC458:
	.string	"Runtime::AtomicsExchange"
	.section	.rodata.str1.8
	.align 8
.LC459:
	.string	"Runtime::AtomicsNumWaitersForTesting"
	.section	.rodata.str1.1
.LC460:
	.string	"Runtime::AtomicsOr"
.LC461:
	.string	"Runtime::AtomicsSub"
.LC462:
	.string	"Runtime::AtomicsXor"
.LC463:
	.string	"Runtime::SetAllowAtomicsWait"
.LC464:
	.string	"Runtime::BigIntBinaryOp"
	.section	.rodata.str1.8
	.align 8
.LC465:
	.string	"Runtime::BigIntCompareToBigInt"
	.align 8
.LC466:
	.string	"Runtime::BigIntCompareToNumber"
	.align 8
.LC467:
	.string	"Runtime::BigIntCompareToString"
	.section	.rodata.str1.1
.LC468:
	.string	"Runtime::BigIntEqualToBigInt"
.LC469:
	.string	"Runtime::BigIntEqualToNumber"
.LC470:
	.string	"Runtime::BigIntEqualToString"
.LC471:
	.string	"Runtime::BigIntToBoolean"
.LC472:
	.string	"Runtime::BigIntToNumber"
.LC473:
	.string	"Runtime::BigIntUnaryOp"
.LC474:
	.string	"Runtime::ToBigInt"
.LC475:
	.string	"Runtime::DefineClass"
.LC476:
	.string	"Runtime::HomeObjectSymbol"
.LC477:
	.string	"Runtime::LoadFromSuper"
.LC478:
	.string	"Runtime::LoadKeyedFromSuper"
.LC479:
	.string	"Runtime::StoreKeyedToSuper"
.LC480:
	.string	"Runtime::StoreToSuper"
	.section	.rodata.str1.8
	.align 8
.LC481:
	.string	"Runtime::ThrowConstructorNonCallableError"
	.align 8
.LC482:
	.string	"Runtime::ThrowNotSuperConstructor"
	.align 8
.LC483:
	.string	"Runtime::ThrowStaticPrototypeError"
	.align 8
.LC484:
	.string	"Runtime::ThrowSuperAlreadyCalledError"
	.section	.rodata.str1.1
.LC485:
	.string	"Runtime::ThrowSuperNotCalled"
	.section	.rodata.str1.8
	.align 8
.LC486:
	.string	"Runtime::ThrowUnsupportedSuperError"
	.section	.rodata.str1.1
.LC487:
	.string	"Runtime::MapGrow"
.LC488:
	.string	"Runtime::MapShrink"
.LC489:
	.string	"Runtime::SetGrow"
.LC490:
	.string	"Runtime::SetShrink"
.LC491:
	.string	"Runtime::TheHole"
.LC492:
	.string	"Runtime::WeakCollectionDelete"
.LC493:
	.string	"Runtime::WeakCollectionSet"
	.section	.rodata.str1.8
	.align 8
.LC494:
	.string	"Runtime::CompileForOnStackReplacement"
	.section	.rodata.str1.1
.LC495:
	.string	"Runtime::CompileLazy"
	.section	.rodata.str1.8
	.align 8
.LC496:
	.string	"Runtime::CompileOptimized_Concurrent"
	.align 8
.LC497:
	.string	"Runtime::CompileOptimized_NotConcurrent"
	.align 8
.LC498:
	.string	"Runtime::EvictOptimizedCodeSlot"
	.align 8
.LC499:
	.string	"Runtime::FunctionFirstExecution"
	.section	.rodata.str1.1
.LC500:
	.string	"Runtime::InstantiateAsmJs"
.LC501:
	.string	"Runtime::NotifyDeoptimized"
	.section	.rodata.str1.8
	.align 8
.LC502:
	.string	"Runtime::ResolvePossiblyDirectEval"
	.section	.rodata.str1.1
.LC503:
	.string	"Runtime::DateCurrentTime"
.LC504:
	.string	"Runtime::ClearStepping"
.LC505:
	.string	"Runtime::CollectGarbage"
	.section	.rodata.str1.8
	.align 8
.LC506:
	.string	"Runtime::DebugAsyncFunctionEntered"
	.align 8
.LC507:
	.string	"Runtime::DebugAsyncFunctionSuspended"
	.align 8
.LC508:
	.string	"Runtime::DebugAsyncFunctionResumed"
	.align 8
.LC509:
	.string	"Runtime::DebugAsyncFunctionFinished"
	.section	.rodata.str1.1
.LC510:
	.string	"Runtime::DebugBreakAtEntry"
.LC511:
	.string	"Runtime::DebugCollectCoverage"
	.section	.rodata.str1.8
	.align 8
.LC512:
	.string	"Runtime::DebugGetLoadedScriptIds"
	.section	.rodata.str1.1
.LC513:
	.string	"Runtime::DebugOnFunctionCall"
.LC514:
	.string	"Runtime::DebugPopPromise"
	.section	.rodata.str1.8
	.align 8
.LC515:
	.string	"Runtime::DebugPrepareStepInSuspendedGenerator"
	.section	.rodata.str1.1
.LC516:
	.string	"Runtime::DebugPushPromise"
	.section	.rodata.str1.8
	.align 8
.LC517:
	.string	"Runtime::DebugToggleBlockCoverage"
	.align 8
.LC518:
	.string	"Runtime::DebugTogglePreciseCoverage"
	.align 8
.LC519:
	.string	"Runtime::FunctionGetInferredName"
	.section	.rodata.str1.1
.LC520:
	.string	"Runtime::GetBreakLocations"
	.section	.rodata.str1.8
	.align 8
.LC521:
	.string	"Runtime::GetGeneratorScopeCount"
	.align 8
.LC522:
	.string	"Runtime::GetGeneratorScopeDetails"
	.section	.rodata.str1.1
.LC523:
	.string	"Runtime::GetHeapUsage"
	.section	.rodata.str1.8
	.align 8
.LC524:
	.string	"Runtime::HandleDebuggerStatement"
	.section	.rodata.str1.1
.LC525:
	.string	"Runtime::IsBreakOnException"
.LC526:
	.string	"Runtime::LiveEditPatchScript"
	.section	.rodata.str1.8
	.align 8
.LC527:
	.string	"Runtime::ProfileCreateSnapshotDataBlob"
	.section	.rodata.str1.1
.LC528:
	.string	"Runtime::ScheduleBreak"
	.section	.rodata.str1.8
	.align 8
.LC529:
	.string	"Runtime::ScriptLocationFromLine2"
	.align 8
.LC530:
	.string	"Runtime::SetGeneratorScopeVariableValue"
	.section	.rodata.str1.1
.LC531:
	.string	"Runtime::IncBlockCounter"
.LC532:
	.string	"Runtime::ForInEnumerate"
.LC533:
	.string	"Runtime::ForInHasProperty"
.LC534:
	.string	"Runtime::Call"
	.section	.rodata.str1.8
	.align 8
.LC535:
	.string	"Runtime::FunctionGetScriptSource"
	.section	.rodata.str1.1
.LC536:
	.string	"Runtime::FunctionGetScriptId"
	.section	.rodata.str1.8
	.align 8
.LC537:
	.string	"Runtime::FunctionGetScriptSourcePosition"
	.align 8
.LC538:
	.string	"Runtime::FunctionGetSourceCode"
	.align 8
.LC539:
	.string	"Runtime::FunctionIsAPIFunction"
	.section	.rodata.str1.1
.LC540:
	.string	"Runtime::IsFunction"
	.section	.rodata.str1.8
	.align 8
.LC541:
	.string	"Runtime::AsyncFunctionAwaitCaught"
	.align 8
.LC542:
	.string	"Runtime::AsyncFunctionAwaitUncaught"
	.section	.rodata.str1.1
.LC543:
	.string	"Runtime::AsyncFunctionEnter"
.LC544:
	.string	"Runtime::AsyncFunctionReject"
.LC545:
	.string	"Runtime::AsyncFunctionResolve"
	.section	.rodata.str1.8
	.align 8
.LC546:
	.string	"Runtime::AsyncGeneratorAwaitCaught"
	.align 8
.LC547:
	.string	"Runtime::AsyncGeneratorAwaitUncaught"
	.align 8
.LC548:
	.string	"Runtime::AsyncGeneratorHasCatchHandlerForPC"
	.section	.rodata.str1.1
.LC549:
	.string	"Runtime::AsyncGeneratorReject"
	.section	.rodata.str1.8
	.align 8
.LC550:
	.string	"Runtime::AsyncGeneratorResolve"
	.section	.rodata.str1.1
.LC551:
	.string	"Runtime::AsyncGeneratorYield"
	.section	.rodata.str1.8
	.align 8
.LC552:
	.string	"Runtime::CreateJSGeneratorObject"
	.section	.rodata.str1.1
.LC553:
	.string	"Runtime::GeneratorClose"
.LC554:
	.string	"Runtime::GeneratorGetFunction"
	.section	.rodata.str1.8
	.align 8
.LC555:
	.string	"Runtime::GeneratorGetResumeMode"
	.align 8
.LC556:
	.string	"Runtime::ElementsTransitionAndStoreIC_Miss"
	.section	.rodata.str1.1
.LC557:
	.string	"Runtime::KeyedLoadIC_Miss"
.LC558:
	.string	"Runtime::KeyedStoreIC_Miss"
	.section	.rodata.str1.8
	.align 8
.LC559:
	.string	"Runtime::StoreInArrayLiteralIC_Miss"
	.section	.rodata.str1.1
.LC560:
	.string	"Runtime::KeyedStoreIC_Slow"
	.section	.rodata.str1.8
	.align 8
.LC561:
	.string	"Runtime::LoadElementWithInterceptor"
	.section	.rodata.str1.1
.LC562:
	.string	"Runtime::LoadGlobalIC_Miss"
.LC563:
	.string	"Runtime::LoadGlobalIC_Slow"
.LC564:
	.string	"Runtime::LoadIC_Miss"
	.section	.rodata.str1.8
	.align 8
.LC565:
	.string	"Runtime::LoadPropertyWithInterceptor"
	.align 8
.LC566:
	.string	"Runtime::StoreCallbackProperty"
	.section	.rodata.str1.1
.LC567:
	.string	"Runtime::StoreGlobalIC_Miss"
	.section	.rodata.str1.8
	.align 8
.LC568:
	.string	"Runtime::StoreGlobalICNoFeedback_Miss"
	.section	.rodata.str1.1
.LC569:
	.string	"Runtime::StoreGlobalIC_Slow"
.LC570:
	.string	"Runtime::StoreIC_Miss"
	.section	.rodata.str1.8
	.align 8
.LC571:
	.string	"Runtime::StoreInArrayLiteralIC_Slow"
	.align 8
.LC572:
	.string	"Runtime::StorePropertyWithInterceptor"
	.section	.rodata.str1.1
.LC573:
	.string	"Runtime::CloneObjectIC_Miss"
.LC574:
	.string	"Runtime::KeyedHasIC_Miss"
	.section	.rodata.str1.8
	.align 8
.LC575:
	.string	"Runtime::HasElementWithInterceptor"
	.section	.rodata.str1.1
.LC576:
	.string	"Runtime::AccessCheck"
.LC577:
	.string	"Runtime::AllocateByteArray"
	.section	.rodata.str1.8
	.align 8
.LC578:
	.string	"Runtime::AllocateInYoungGeneration"
	.align 8
.LC579:
	.string	"Runtime::AllocateInOldGeneration"
	.align 8
.LC580:
	.string	"Runtime::AllocateSeqOneByteString"
	.align 8
.LC581:
	.string	"Runtime::AllocateSeqTwoByteString"
	.section	.rodata.str1.1
.LC582:
	.string	"Runtime::AllowDynamicFunction"
	.section	.rodata.str1.8
	.align 8
.LC583:
	.string	"Runtime::CreateAsyncFromSyncIterator"
	.align 8
.LC584:
	.string	"Runtime::CreateListFromArrayLike"
	.align 8
.LC585:
	.string	"Runtime::FatalProcessOutOfMemoryInAllocateRaw"
	.align 8
.LC586:
	.string	"Runtime::FatalProcessOutOfMemoryInvalidArrayLength"
	.align 8
.LC587:
	.string	"Runtime::GetAndResetRuntimeCallStats"
	.section	.rodata.str1.1
.LC588:
	.string	"Runtime::GetTemplateObject"
.LC589:
	.string	"Runtime::IncrementUseCounter"
	.section	.rodata.str1.8
	.align 8
.LC590:
	.string	"Runtime::BytecodeBudgetInterrupt"
	.section	.rodata.str1.1
.LC591:
	.string	"Runtime::NewReferenceError"
.LC592:
	.string	"Runtime::NewSyntaxError"
.LC593:
	.string	"Runtime::NewTypeError"
.LC594:
	.string	"Runtime::OrdinaryHasInstance"
	.section	.rodata.str1.8
	.align 8
.LC595:
	.string	"Runtime::PromoteScheduledException"
	.section	.rodata.str1.1
.LC596:
	.string	"Runtime::ReportMessage"
.LC597:
	.string	"Runtime::ReThrow"
.LC598:
	.string	"Runtime::RunMicrotaskCallback"
	.section	.rodata.str1.8
	.align 8
.LC599:
	.string	"Runtime::PerformMicrotaskCheckpoint"
	.section	.rodata.str1.1
.LC600:
	.string	"Runtime::StackGuard"
.LC601:
	.string	"Runtime::Throw"
	.section	.rodata.str1.8
	.align 8
.LC602:
	.string	"Runtime::ThrowApplyNonFunction"
	.align 8
.LC603:
	.string	"Runtime::ThrowCalledNonCallable"
	.align 8
.LC604:
	.string	"Runtime::ThrowConstructedNonConstructable"
	.align 8
.LC605:
	.string	"Runtime::ThrowConstructorReturnedNonObject"
	.align 8
.LC606:
	.string	"Runtime::ThrowInvalidStringLength"
	.align 8
.LC607:
	.string	"Runtime::ThrowInvalidTypedArrayAlignment"
	.section	.rodata.str1.1
.LC608:
	.string	"Runtime::ThrowIteratorError"
	.section	.rodata.str1.8
	.align 8
.LC609:
	.string	"Runtime::ThrowIteratorResultNotAnObject"
	.section	.rodata.str1.1
.LC610:
	.string	"Runtime::ThrowNotConstructor"
	.section	.rodata.str1.8
	.align 8
.LC611:
	.string	"Runtime::ThrowPatternAssignmentNonCoercible"
	.section	.rodata.str1.1
.LC612:
	.string	"Runtime::ThrowRangeError"
.LC613:
	.string	"Runtime::ThrowReferenceError"
	.section	.rodata.str1.8
	.align 8
.LC614:
	.string	"Runtime::ThrowAccessedUninitializedVariable"
	.section	.rodata.str1.1
.LC615:
	.string	"Runtime::ThrowStackOverflow"
	.section	.rodata.str1.8
	.align 8
.LC616:
	.string	"Runtime::ThrowSymbolAsyncIteratorInvalid"
	.align 8
.LC617:
	.string	"Runtime::ThrowSymbolIteratorInvalid"
	.align 8
.LC618:
	.string	"Runtime::ThrowThrowMethodMissing"
	.section	.rodata.str1.1
.LC619:
	.string	"Runtime::ThrowTypeError"
	.section	.rodata.str1.8
	.align 8
.LC620:
	.string	"Runtime::ThrowTypeErrorIfStrict"
	.section	.rodata.str1.1
.LC621:
	.string	"Runtime::Typeof"
	.section	.rodata.str1.8
	.align 8
.LC622:
	.string	"Runtime::UnwindAndFindExceptionHandler"
	.section	.rodata.str1.1
.LC623:
	.string	"Runtime::FormatList"
.LC624:
	.string	"Runtime::FormatListToParts"
	.section	.rodata.str1.8
	.align 8
.LC625:
	.string	"Runtime::StringToLowerCaseIntl"
	.align 8
.LC626:
	.string	"Runtime::StringToUpperCaseIntl"
	.section	.rodata.str1.1
.LC627:
	.string	"Runtime::CreateArrayLiteral"
	.section	.rodata.str1.8
	.align 8
.LC628:
	.string	"Runtime::CreateArrayLiteralWithoutAllocationSite"
	.section	.rodata.str1.1
.LC629:
	.string	"Runtime::CreateObjectLiteral"
	.section	.rodata.str1.8
	.align 8
.LC630:
	.string	"Runtime::CreateObjectLiteralWithoutAllocationSite"
	.section	.rodata.str1.1
.LC631:
	.string	"Runtime::CreateRegExpLiteral"
.LC632:
	.string	"Runtime::DynamicImportCall"
.LC633:
	.string	"Runtime::GetImportMetaObject"
.LC634:
	.string	"Runtime::GetModuleNamespace"
.LC635:
	.string	"Runtime::GetHoleNaNLower"
.LC636:
	.string	"Runtime::GetHoleNaNUpper"
.LC637:
	.string	"Runtime::IsSmi"
.LC638:
	.string	"Runtime::IsValidSmi"
.LC639:
	.string	"Runtime::MaxSmi"
.LC640:
	.string	"Runtime::NumberToString"
.LC641:
	.string	"Runtime::StringParseFloat"
.LC642:
	.string	"Runtime::StringParseInt"
.LC643:
	.string	"Runtime::StringToNumber"
	.section	.rodata.str1.8
	.align 8
.LC644:
	.string	"Runtime::AddDictionaryProperty"
	.section	.rodata.str1.1
.LC645:
	.string	"Runtime::AddPrivateField"
.LC646:
	.string	"Runtime::AddPrivateBrand"
.LC647:
	.string	"Runtime::AllocateHeapNumber"
.LC648:
	.string	"Runtime::ClassOf"
.LC649:
	.string	"Runtime::CollectTypeProfile"
	.section	.rodata.str1.8
	.align 8
.LC650:
	.string	"Runtime::CompleteInobjectSlackTrackingForMap"
	.section	.rodata.str1.1
.LC651:
	.string	"Runtime::CopyDataProperties"
	.section	.rodata.str1.8
	.align 8
.LC652:
	.string	"Runtime::CopyDataPropertiesWithExcludedProperties"
	.section	.rodata.str1.1
.LC653:
	.string	"Runtime::CreateDataProperty"
	.section	.rodata.str1.8
	.align 8
.LC654:
	.string	"Runtime::CreateIterResultObject"
	.align 8
.LC655:
	.string	"Runtime::CreatePrivateAccessors"
	.align 8
.LC656:
	.string	"Runtime::DefineAccessorPropertyUnchecked"
	.align 8
.LC657:
	.string	"Runtime::DefineDataPropertyInLiteral"
	.align 8
.LC658:
	.string	"Runtime::DefineGetterPropertyUnchecked"
	.align 8
.LC659:
	.string	"Runtime::DefineSetterPropertyUnchecked"
	.section	.rodata.str1.1
.LC660:
	.string	"Runtime::DeleteProperty"
.LC661:
	.string	"Runtime::GetDerivedMap"
.LC662:
	.string	"Runtime::GetFunctionName"
	.section	.rodata.str1.8
	.align 8
.LC663:
	.string	"Runtime::GetOwnPropertyDescriptor"
	.section	.rodata.str1.1
.LC664:
	.string	"Runtime::GetOwnPropertyKeys"
.LC665:
	.string	"Runtime::GetProperty"
	.section	.rodata.str1.8
	.align 8
.LC666:
	.string	"Runtime::HasFastPackedElements"
	.section	.rodata.str1.1
.LC667:
	.string	"Runtime::HasInPrototypeChain"
.LC668:
	.string	"Runtime::HasProperty"
.LC669:
	.string	"Runtime::InternalSetPrototype"
.LC670:
	.string	"Runtime::IsJSReceiver"
	.section	.rodata.str1.8
	.align 8
.LC671:
	.string	"Runtime::JSReceiverPreventExtensionsDontThrow"
	.align 8
.LC672:
	.string	"Runtime::JSReceiverPreventExtensionsThrow"
	.align 8
.LC673:
	.string	"Runtime::JSReceiverGetPrototypeOf"
	.align 8
.LC674:
	.string	"Runtime::JSReceiverSetPrototypeOfDontThrow"
	.align 8
.LC675:
	.string	"Runtime::JSReceiverSetPrototypeOfThrow"
	.section	.rodata.str1.1
.LC676:
	.string	"Runtime::LoadPrivateGetter"
.LC677:
	.string	"Runtime::LoadPrivateSetter"
.LC678:
	.string	"Runtime::NewObject"
.LC679:
	.string	"Runtime::ObjectCreate"
.LC680:
	.string	"Runtime::ObjectEntries"
	.section	.rodata.str1.8
	.align 8
.LC681:
	.string	"Runtime::ObjectEntriesSkipFastPath"
	.align 8
.LC682:
	.string	"Runtime::ObjectGetOwnPropertyNames"
	.align 8
.LC683:
	.string	"Runtime::ObjectGetOwnPropertyNamesTryFast"
	.section	.rodata.str1.1
.LC684:
	.string	"Runtime::ObjectHasOwnProperty"
.LC685:
	.string	"Runtime::ObjectIsExtensible"
.LC686:
	.string	"Runtime::ObjectKeys"
.LC687:
	.string	"Runtime::ObjectValues"
	.section	.rodata.str1.8
	.align 8
.LC688:
	.string	"Runtime::ObjectValuesSkipFastPath"
	.align 8
.LC689:
	.string	"Runtime::OptimizeObjectForAddingMultipleProperties"
	.align 8
.LC690:
	.string	"Runtime::PerformSideEffectCheckForObject"
	.section	.rodata.str1.1
.LC691:
	.string	"Runtime::SetDataProperties"
.LC692:
	.string	"Runtime::SetKeyedProperty"
.LC693:
	.string	"Runtime::SetNamedProperty"
	.section	.rodata.str1.8
	.align 8
.LC694:
	.string	"Runtime::StoreDataPropertyInLiteral"
	.align 8
.LC695:
	.string	"Runtime::ShrinkPropertyDictionary"
	.section	.rodata.str1.1
.LC696:
	.string	"Runtime::ToFastProperties"
.LC697:
	.string	"Runtime::ToLength"
.LC698:
	.string	"Runtime::ToName"
.LC699:
	.string	"Runtime::ToNumber"
.LC700:
	.string	"Runtime::ToNumeric"
.LC701:
	.string	"Runtime::ToObject"
.LC702:
	.string	"Runtime::ToStringRT"
.LC703:
	.string	"Runtime::TryMigrateInstance"
.LC704:
	.string	"Runtime::Add"
.LC705:
	.string	"Runtime::Equal"
.LC706:
	.string	"Runtime::GreaterThan"
.LC707:
	.string	"Runtime::GreaterThanOrEqual"
.LC708:
	.string	"Runtime::LessThan"
.LC709:
	.string	"Runtime::LessThanOrEqual"
.LC710:
	.string	"Runtime::NotEqual"
.LC711:
	.string	"Runtime::StrictEqual"
.LC712:
	.string	"Runtime::StrictNotEqual"
.LC713:
	.string	"Runtime::EnqueueMicrotask"
.LC714:
	.string	"Runtime::PromiseHookAfter"
.LC715:
	.string	"Runtime::PromiseHookBefore"
.LC716:
	.string	"Runtime::PromiseHookInit"
.LC717:
	.string	"Runtime::AwaitPromisesInit"
.LC718:
	.string	"Runtime::AwaitPromisesInitOld"
.LC719:
	.string	"Runtime::PromiseMarkAsHandled"
	.section	.rodata.str1.8
	.align 8
.LC720:
	.string	"Runtime::PromiseRejectEventFromStack"
	.section	.rodata.str1.1
.LC721:
	.string	"Runtime::PromiseRevokeReject"
.LC722:
	.string	"Runtime::PromiseStatus"
.LC723:
	.string	"Runtime::RejectPromise"
.LC724:
	.string	"Runtime::ResolvePromise"
	.section	.rodata.str1.8
	.align 8
.LC725:
	.string	"Runtime::PromiseRejectAfterResolved"
	.align 8
.LC726:
	.string	"Runtime::PromiseResolveAfterResolved"
	.align 8
.LC727:
	.string	"Runtime::CheckProxyGetSetTrapResult"
	.align 8
.LC728:
	.string	"Runtime::CheckProxyHasTrapResult"
	.align 8
.LC729:
	.string	"Runtime::CheckProxyDeleteTrapResult"
	.align 8
.LC730:
	.string	"Runtime::GetPropertyWithReceiver"
	.align 8
.LC731:
	.string	"Runtime::SetPropertyWithReceiver"
	.section	.rodata.str1.1
.LC732:
	.string	"Runtime::IsRegExp"
.LC733:
	.string	"Runtime::RegExpExec"
.LC734:
	.string	"Runtime::RegExpExecMultiple"
	.section	.rodata.str1.8
	.align 8
.LC735:
	.string	"Runtime::RegExpInitializeAndCompile"
	.section	.rodata.str1.1
.LC736:
	.string	"Runtime::RegExpReplaceRT"
.LC737:
	.string	"Runtime::RegExpSplit"
	.section	.rodata.str1.8
	.align 8
.LC738:
	.string	"Runtime::StringReplaceNonGlobalRegExpWithFunction"
	.section	.rodata.str1.1
.LC739:
	.string	"Runtime::StringSplit"
.LC740:
	.string	"Runtime::DeclareEvalFunction"
.LC741:
	.string	"Runtime::DeclareEvalVar"
.LC742:
	.string	"Runtime::DeclareGlobals"
.LC743:
	.string	"Runtime::DeleteLookupSlot"
.LC744:
	.string	"Runtime::LoadLookupSlot"
	.section	.rodata.str1.8
	.align 8
.LC745:
	.string	"Runtime::LoadLookupSlotInsideTypeof"
	.section	.rodata.str1.1
.LC746:
	.string	"Runtime::NewArgumentsElements"
.LC747:
	.string	"Runtime::NewClosure"
.LC748:
	.string	"Runtime::NewClosure_Tenured"
.LC749:
	.string	"Runtime::NewFunctionContext"
.LC750:
	.string	"Runtime::NewRestParameter"
.LC751:
	.string	"Runtime::NewScriptContext"
.LC752:
	.string	"Runtime::NewSloppyArguments"
	.section	.rodata.str1.8
	.align 8
.LC753:
	.string	"Runtime::NewSloppyArguments_Generic"
	.section	.rodata.str1.1
.LC754:
	.string	"Runtime::NewStrictArguments"
.LC755:
	.string	"Runtime::PushBlockContext"
.LC756:
	.string	"Runtime::PushCatchContext"
.LC757:
	.string	"Runtime::PushModuleContext"
.LC758:
	.string	"Runtime::PushWithContext"
	.section	.rodata.str1.8
	.align 8
.LC759:
	.string	"Runtime::StoreLookupSlot_Sloppy"
	.align 8
.LC760:
	.string	"Runtime::StoreLookupSlot_SloppyHoisting"
	.align 8
.LC761:
	.string	"Runtime::StoreLookupSlot_Strict"
	.align 8
.LC762:
	.string	"Runtime::ThrowConstAssignError"
	.section	.rodata.str1.1
.LC763:
	.string	"Runtime::FlattenString"
.LC764:
	.string	"Runtime::GetSubstitution"
.LC765:
	.string	"Runtime::InternalizeString"
.LC766:
	.string	"Runtime::StringAdd"
.LC767:
	.string	"Runtime::StringBuilderConcat"
.LC768:
	.string	"Runtime::StringCharCodeAt"
.LC769:
	.string	"Runtime::StringEqual"
.LC770:
	.string	"Runtime::StringEscapeQuotes"
.LC771:
	.string	"Runtime::StringGreaterThan"
	.section	.rodata.str1.8
	.align 8
.LC772:
	.string	"Runtime::StringGreaterThanOrEqual"
	.section	.rodata.str1.1
.LC773:
	.string	"Runtime::StringIncludes"
.LC774:
	.string	"Runtime::StringIndexOf"
	.section	.rodata.str1.8
	.align 8
.LC775:
	.string	"Runtime::StringIndexOfUnchecked"
	.section	.rodata.str1.1
.LC776:
	.string	"Runtime::StringLastIndexOf"
.LC777:
	.string	"Runtime::StringLessThan"
	.section	.rodata.str1.8
	.align 8
.LC778:
	.string	"Runtime::StringLessThanOrEqual"
	.section	.rodata.str1.1
.LC779:
	.string	"Runtime::StringMaxLength"
	.section	.rodata.str1.8
	.align 8
.LC780:
	.string	"Runtime::StringReplaceOneCharWithString"
	.align 8
.LC781:
	.string	"Runtime::StringCompareSequence"
	.section	.rodata.str1.1
.LC782:
	.string	"Runtime::StringSubstring"
.LC783:
	.string	"Runtime::StringToArray"
.LC784:
	.string	"Runtime::StringTrim"
	.section	.rodata.str1.8
	.align 8
.LC785:
	.string	"Runtime::CreatePrivateNameSymbol"
	.section	.rodata.str1.1
.LC786:
	.string	"Runtime::CreatePrivateSymbol"
	.section	.rodata.str1.8
	.align 8
.LC787:
	.string	"Runtime::SymbolDescriptiveString"
	.section	.rodata.str1.1
.LC788:
	.string	"Runtime::SymbolIsPrivate"
.LC789:
	.string	"Runtime::Abort"
.LC790:
	.string	"Runtime::AbortJS"
.LC791:
	.string	"Runtime::AbortCSAAssert"
	.section	.rodata.str1.8
	.align 8
.LC792:
	.string	"Runtime::ArraySpeciesProtector"
	.align 8
.LC793:
	.string	"Runtime::ClearFunctionFeedback"
	.align 8
.LC794:
	.string	"Runtime::ClearMegamorphicStubCache"
	.section	.rodata.str1.1
.LC795:
	.string	"Runtime::CloneWasmModule"
	.section	.rodata.str1.8
	.align 8
.LC796:
	.string	"Runtime::CompleteInobjectSlackTracking"
	.section	.rodata.str1.1
.LC797:
	.string	"Runtime::ConstructConsString"
.LC798:
	.string	"Runtime::ConstructDouble"
	.section	.rodata.str1.8
	.align 8
.LC799:
	.string	"Runtime::ConstructSlicedString"
	.section	.rodata.str1.1
.LC800:
	.string	"Runtime::DebugPrint"
.LC801:
	.string	"Runtime::DebugTrace"
	.section	.rodata.str1.8
	.align 8
.LC802:
	.string	"Runtime::DebugTrackRetainingPath"
	.section	.rodata.str1.1
.LC803:
	.string	"Runtime::DeoptimizeFunction"
	.section	.rodata.str1.8
	.align 8
.LC804:
	.string	"Runtime::DeserializeWasmModule"
	.align 8
.LC805:
	.string	"Runtime::DisallowCodegenFromStrings"
	.section	.rodata.str1.1
.LC806:
	.string	"Runtime::DisallowWasmCodegen"
.LC807:
	.string	"Runtime::DisassembleFunction"
	.section	.rodata.str1.8
	.align 8
.LC808:
	.string	"Runtime::EnableCodeLoggingForTesting"
	.align 8
.LC809:
	.string	"Runtime::EnsureFeedbackVectorForFunction"
	.align 8
.LC810:
	.string	"Runtime::FreezeWasmLazyCompilation"
	.section	.rodata.str1.1
.LC811:
	.string	"Runtime::GetCallable"
	.section	.rodata.str1.8
	.align 8
.LC812:
	.string	"Runtime::GetInitializerFunction"
	.align 8
.LC813:
	.string	"Runtime::GetOptimizationStatus"
	.section	.rodata.str1.1
.LC814:
	.string	"Runtime::GetUndetectable"
.LC815:
	.string	"Runtime::GetWasmExceptionId"
	.section	.rodata.str1.8
	.align 8
.LC816:
	.string	"Runtime::GetWasmExceptionValues"
	.align 8
.LC817:
	.string	"Runtime::GetWasmRecoveredTrapCount"
	.section	.rodata.str1.1
.LC818:
	.string	"Runtime::GlobalPrint"
	.section	.rodata.str1.8
	.align 8
.LC819:
	.string	"Runtime::HasDictionaryElements"
	.section	.rodata.str1.1
.LC820:
	.string	"Runtime::HasDoubleElements"
	.section	.rodata.str1.8
	.align 8
.LC821:
	.string	"Runtime::HasElementsInALargeObjectSpace"
	.section	.rodata.str1.1
.LC822:
	.string	"Runtime::HasFastElements"
.LC823:
	.string	"Runtime::HasFastProperties"
	.section	.rodata.str1.8
	.align 8
.LC824:
	.string	"Runtime::HasFixedBigInt64Elements"
	.align 8
.LC825:
	.string	"Runtime::HasFixedBigUint64Elements"
	.align 8
.LC826:
	.string	"Runtime::HasFixedFloat32Elements"
	.align 8
.LC827:
	.string	"Runtime::HasFixedFloat64Elements"
	.align 8
.LC828:
	.string	"Runtime::HasFixedInt16Elements"
	.align 8
.LC829:
	.string	"Runtime::HasFixedInt32Elements"
	.section	.rodata.str1.1
.LC830:
	.string	"Runtime::HasFixedInt8Elements"
	.section	.rodata.str1.8
	.align 8
.LC831:
	.string	"Runtime::HasFixedUint16Elements"
	.align 8
.LC832:
	.string	"Runtime::HasFixedUint32Elements"
	.align 8
.LC833:
	.string	"Runtime::HasFixedUint8ClampedElements"
	.align 8
.LC834:
	.string	"Runtime::HasFixedUint8Elements"
	.section	.rodata.str1.1
.LC835:
	.string	"Runtime::HasHoleyElements"
.LC836:
	.string	"Runtime::HasObjectElements"
.LC837:
	.string	"Runtime::HasPackedElements"
	.section	.rodata.str1.8
	.align 8
.LC838:
	.string	"Runtime::HasSloppyArgumentsElements"
	.section	.rodata.str1.1
.LC839:
	.string	"Runtime::HasSmiElements"
	.section	.rodata.str1.8
	.align 8
.LC840:
	.string	"Runtime::HasSmiOrObjectElements"
	.section	.rodata.str1.1
.LC841:
	.string	"Runtime::HaveSameMap"
.LC842:
	.string	"Runtime::HeapObjectVerify"
.LC843:
	.string	"Runtime::ICsAreEnabled"
.LC844:
	.string	"Runtime::InYoungGeneration"
.LC845:
	.string	"Runtime::IsAsmWasmCode"
	.section	.rodata.str1.8
	.align 8
.LC846:
	.string	"Runtime::IsConcurrentRecompilationSupported"
	.section	.rodata.str1.1
.LC847:
	.string	"Runtime::IsLiftoffFunction"
.LC848:
	.string	"Runtime::IsThreadInWasm"
.LC849:
	.string	"Runtime::IsWasmCode"
	.section	.rodata.str1.8
	.align 8
.LC850:
	.string	"Runtime::IsWasmTrapHandlerEnabled"
	.section	.rodata.str1.1
.LC851:
	.string	"Runtime::RegexpHasBytecode"
.LC852:
	.string	"Runtime::RegexpHasNativeCode"
.LC853:
	.string	"Runtime::MapIteratorProtector"
	.section	.rodata.str1.8
	.align 8
.LC854:
	.string	"Runtime::NeverOptimizeFunction"
	.align 8
.LC855:
	.string	"Runtime::NotifyContextDisposed"
	.align 8
.LC856:
	.string	"Runtime::OptimizeFunctionOnNextCall"
	.section	.rodata.str1.1
.LC857:
	.string	"Runtime::OptimizeOsr"
	.section	.rodata.str1.8
	.align 8
.LC858:
	.string	"Runtime::PrepareFunctionForOptimization"
	.align 8
.LC859:
	.string	"Runtime::PrintWithNameForAssert"
	.align 8
.LC860:
	.string	"Runtime::RedirectToWasmInterpreter"
	.section	.rodata.str1.1
.LC861:
	.string	"Runtime::RunningInSimulator"
.LC862:
	.string	"Runtime::SerializeWasmModule"
.LC863:
	.string	"Runtime::SetAllocationTimeout"
.LC864:
	.string	"Runtime::SetForceSlowPath"
.LC865:
	.string	"Runtime::SetIteratorProtector"
	.section	.rodata.str1.8
	.align 8
.LC866:
	.string	"Runtime::SetWasmCompileControls"
	.align 8
.LC867:
	.string	"Runtime::SetWasmInstantiateControls"
	.align 8
.LC868:
	.string	"Runtime::SetWasmThreadsEnabled"
	.align 8
.LC869:
	.string	"Runtime::StringIteratorProtector"
	.section	.rodata.str1.1
.LC870:
	.string	"Runtime::SystemBreak"
.LC871:
	.string	"Runtime::TraceEnter"
.LC872:
	.string	"Runtime::TraceExit"
.LC873:
	.string	"Runtime::TurbofanStaticAssert"
	.section	.rodata.str1.8
	.align 8
.LC874:
	.string	"Runtime::UnblockConcurrentRecompilation"
	.align 8
.LC875:
	.string	"Runtime::WasmGetNumberOfInstances"
	.align 8
.LC876:
	.string	"Runtime::WasmNumInterpretedCalls"
	.section	.rodata.str1.1
.LC877:
	.string	"Runtime::WasmTierUpFunction"
.LC878:
	.string	"Runtime::WasmTraceMemory"
.LC879:
	.string	"Runtime::DeoptimizeNow"
.LC880:
	.string	"Runtime::ArrayBufferDetach"
	.section	.rodata.str1.8
	.align 8
.LC881:
	.string	"Runtime::TypedArrayCopyElements"
	.section	.rodata.str1.1
.LC882:
	.string	"Runtime::TypedArrayGetBuffer"
.LC883:
	.string	"Runtime::TypedArraySet"
.LC884:
	.string	"Runtime::TypedArraySortFast"
.LC885:
	.string	"Runtime::ThrowWasmError"
	.section	.rodata.str1.8
	.align 8
.LC886:
	.string	"Runtime::ThrowWasmStackOverflow"
	.section	.rodata.str1.1
.LC887:
	.string	"Runtime::WasmI32AtomicWait"
.LC888:
	.string	"Runtime::WasmI64AtomicWait"
.LC889:
	.string	"Runtime::WasmAtomicNotify"
	.section	.rodata.str1.8
	.align 8
.LC890:
	.string	"Runtime::WasmExceptionGetValues"
	.section	.rodata.str1.1
.LC891:
	.string	"Runtime::WasmExceptionGetTag"
.LC892:
	.string	"Runtime::WasmMemoryGrow"
.LC893:
	.string	"Runtime::WasmRunInterpreter"
.LC894:
	.string	"Runtime::WasmStackGuard"
.LC895:
	.string	"Runtime::WasmThrowCreate"
.LC896:
	.string	"Runtime::WasmThrowTypeError"
.LC897:
	.string	"Runtime::WasmRefFunc"
.LC898:
	.string	"Runtime::WasmFunctionTableGet"
.LC899:
	.string	"Runtime::WasmFunctionTableSet"
.LC900:
	.string	"Runtime::WasmTableInit"
.LC901:
	.string	"Runtime::WasmTableCopy"
.LC902:
	.string	"Runtime::WasmTableGrow"
.LC903:
	.string	"Runtime::WasmTableFill"
	.section	.rodata.str1.8
	.align 8
.LC904:
	.string	"Runtime::WasmIsValidFuncRefValue"
	.section	.rodata.str1.1
.LC905:
	.string	"Runtime::WasmCompileLazy"
	.section	.rodata.str1.8
	.align 8
.LC906:
	.string	"Runtime::WasmNewMultiReturnFixedArray"
	.align 8
.LC907:
	.string	"Runtime::WasmNewMultiReturnJSArray"
	.section	.rodata.str1.1
.LC908:
	.string	"Isolate::handler_address"
.LC909:
	.string	"Isolate::c_entry_fp_address"
.LC910:
	.string	"Isolate::c_function_address"
.LC911:
	.string	"Isolate::context_address"
	.section	.rodata.str1.8
	.align 8
.LC912:
	.string	"Isolate::pending_exception_address"
	.align 8
.LC913:
	.string	"Isolate::pending_handler_context_address"
	.align 8
.LC914:
	.string	"Isolate::pending_handler_entrypoint_address"
	.align 8
.LC915:
	.string	"Isolate::pending_handler_constant_pool_address"
	.align 8
.LC916:
	.string	"Isolate::pending_handler_fp_address"
	.align 8
.LC917:
	.string	"Isolate::pending_handler_sp_address"
	.align 8
.LC918:
	.string	"Isolate::external_caught_exception_address"
	.section	.rodata.str1.1
.LC919:
	.string	"Isolate::js_entry_sp_address"
	.section	.rodata.str1.8
	.align 8
.LC920:
	.string	"Accessors::ArgumentsIteratorGetter"
	.section	.rodata.str1.1
.LC921:
	.string	"Accessors::ArrayLengthGetter"
	.section	.rodata.str1.8
	.align 8
.LC922:
	.string	"Accessors::BoundFunctionLengthGetter"
	.align 8
.LC923:
	.string	"Accessors::BoundFunctionNameGetter"
	.section	.rodata.str1.1
.LC924:
	.string	"Accessors::ErrorStackGetter"
	.section	.rodata.str1.8
	.align 8
.LC925:
	.string	"Accessors::FunctionArgumentsGetter"
	.align 8
.LC926:
	.string	"Accessors::FunctionCallerGetter"
	.section	.rodata.str1.1
.LC927:
	.string	"Accessors::FunctionNameGetter"
	.section	.rodata.str1.8
	.align 8
.LC928:
	.string	"Accessors::FunctionLengthGetter"
	.align 8
.LC929:
	.string	"Accessors::FunctionPrototypeGetter"
	.section	.rodata.str1.1
.LC930:
	.string	"Accessors::StringLengthGetter"
.LC931:
	.string	"Accessors::ArrayLengthSetter"
.LC932:
	.string	"Accessors::ErrorStackSetter"
	.section	.rodata.str1.8
	.align 8
.LC933:
	.string	"Accessors::FunctionPrototypeSetter"
	.align 8
.LC934:
	.string	"Accessors::ModuleNamespaceEntrySetter"
	.align 8
.LC935:
	.string	"Accessors::ReconfigureToDataProperty"
	.section	.rodata.str1.1
.LC936:
	.string	"Load StubCache::primary_->key"
	.section	.rodata.str1.8
	.align 8
.LC937:
	.string	"Load StubCache::primary_->value"
	.section	.rodata.str1.1
.LC938:
	.string	"Load StubCache::primary_->map"
	.section	.rodata.str1.8
	.align 8
.LC939:
	.string	"Load StubCache::secondary_->key"
	.align 8
.LC940:
	.string	"Load StubCache::secondary_->value"
	.align 8
.LC941:
	.string	"Load StubCache::secondary_->map"
	.align 8
.LC942:
	.string	"Store StubCache::primary_->key"
	.align 8
.LC943:
	.string	"Store StubCache::primary_->value"
	.align 8
.LC944:
	.string	"Store StubCache::primary_->map"
	.align 8
.LC945:
	.string	"Store StubCache::secondary_->key"
	.align 8
.LC946:
	.string	"Store StubCache::secondary_->value"
	.align 8
.LC947:
	.string	"Store StubCache::secondary_->map"
	.section	.rodata.str1.1
.LC948:
	.string	"StatsCounter::write_barriers"
	.section	.rodata.str1.8
	.align 8
.LC949:
	.string	"StatsCounter::constructed_objects"
	.align 8
.LC950:
	.string	"StatsCounter::fast_new_closure_total"
	.align 8
.LC951:
	.string	"StatsCounter::regexp_entry_native"
	.align 8
.LC952:
	.string	"StatsCounter::string_add_native"
	.align 8
.LC953:
	.string	"StatsCounter::sub_string_native"
	.align 8
.LC954:
	.string	"StatsCounter::ic_keyed_load_generic_smi"
	.align 8
.LC955:
	.string	"StatsCounter::ic_keyed_load_generic_symbol"
	.align 8
.LC956:
	.string	"StatsCounter::ic_keyed_load_generic_slow"
	.align 8
.LC957:
	.string	"StatsCounter::megamorphic_stub_cache_probes"
	.align 8
.LC958:
	.string	"StatsCounter::megamorphic_stub_cache_misses"
	.section	.data.rel.ro.local._ZN2v88internal22ExternalReferenceTable9ref_name_E,"aw"
	.align 32
	.type	_ZN2v88internal22ExternalReferenceTable9ref_name_E, @object
	.size	_ZN2v88internal22ExternalReferenceTable9ref_name_E, 7584
_ZN2v88internal22ExternalReferenceTable9ref_name_E:
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.quad	.LC85
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.quad	.LC113
	.quad	.LC114
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.quad	.LC121
	.quad	.LC122
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.quad	.LC129
	.quad	.LC130
	.quad	.LC131
	.quad	.LC132
	.quad	.LC133
	.quad	.LC134
	.quad	.LC135
	.quad	.LC136
	.quad	.LC137
	.quad	.LC138
	.quad	.LC139
	.quad	.LC140
	.quad	.LC141
	.quad	.LC142
	.quad	.LC143
	.quad	.LC144
	.quad	.LC145
	.quad	.LC146
	.quad	.LC147
	.quad	.LC148
	.quad	.LC149
	.quad	.LC150
	.quad	.LC151
	.quad	.LC152
	.quad	.LC153
	.quad	.LC154
	.quad	.LC155
	.quad	.LC156
	.quad	.LC157
	.quad	.LC158
	.quad	.LC159
	.quad	.LC160
	.quad	.LC161
	.quad	.LC162
	.quad	.LC163
	.quad	.LC164
	.quad	.LC165
	.quad	.LC166
	.quad	.LC167
	.quad	.LC168
	.quad	.LC169
	.quad	.LC170
	.quad	.LC171
	.quad	.LC172
	.quad	.LC173
	.quad	.LC174
	.quad	.LC175
	.quad	.LC176
	.quad	.LC177
	.quad	.LC178
	.quad	.LC179
	.quad	.LC180
	.quad	.LC181
	.quad	.LC182
	.quad	.LC183
	.quad	.LC184
	.quad	.LC185
	.quad	.LC186
	.quad	.LC187
	.quad	.LC188
	.quad	.LC189
	.quad	.LC190
	.quad	.LC191
	.quad	.LC192
	.quad	.LC193
	.quad	.LC194
	.quad	.LC195
	.quad	.LC196
	.quad	.LC197
	.quad	.LC198
	.quad	.LC199
	.quad	.LC200
	.quad	.LC201
	.quad	.LC202
	.quad	.LC203
	.quad	.LC204
	.quad	.LC205
	.quad	.LC206
	.quad	.LC207
	.quad	.LC208
	.quad	.LC209
	.quad	.LC210
	.quad	.LC211
	.quad	.LC212
	.quad	.LC213
	.quad	.LC214
	.quad	.LC215
	.quad	.LC216
	.quad	.LC217
	.quad	.LC218
	.quad	.LC219
	.quad	.LC220
	.quad	.LC221
	.quad	.LC222
	.quad	.LC223
	.quad	.LC224
	.quad	.LC225
	.quad	.LC226
	.quad	.LC227
	.quad	.LC228
	.quad	.LC229
	.quad	.LC230
	.quad	.LC231
	.quad	.LC232
	.quad	.LC233
	.quad	.LC234
	.quad	.LC235
	.quad	.LC236
	.quad	.LC237
	.quad	.LC238
	.quad	.LC239
	.quad	.LC240
	.quad	.LC241
	.quad	.LC242
	.quad	.LC243
	.quad	.LC244
	.quad	.LC245
	.quad	.LC246
	.quad	.LC247
	.quad	.LC248
	.quad	.LC249
	.quad	.LC250
	.quad	.LC251
	.quad	.LC252
	.quad	.LC253
	.quad	.LC254
	.quad	.LC255
	.quad	.LC256
	.quad	.LC257
	.quad	.LC258
	.quad	.LC259
	.quad	.LC260
	.quad	.LC261
	.quad	.LC262
	.quad	.LC263
	.quad	.LC264
	.quad	.LC265
	.quad	.LC266
	.quad	.LC267
	.quad	.LC268
	.quad	.LC269
	.quad	.LC270
	.quad	.LC271
	.quad	.LC272
	.quad	.LC273
	.quad	.LC274
	.quad	.LC275
	.quad	.LC276
	.quad	.LC277
	.quad	.LC278
	.quad	.LC279
	.quad	.LC280
	.quad	.LC281
	.quad	.LC282
	.quad	.LC283
	.quad	.LC284
	.quad	.LC285
	.quad	.LC286
	.quad	.LC287
	.quad	.LC288
	.quad	.LC289
	.quad	.LC290
	.quad	.LC291
	.quad	.LC292
	.quad	.LC293
	.quad	.LC294
	.quad	.LC295
	.quad	.LC296
	.quad	.LC297
	.quad	.LC298
	.quad	.LC299
	.quad	.LC300
	.quad	.LC301
	.quad	.LC302
	.quad	.LC303
	.quad	.LC304
	.quad	.LC305
	.quad	.LC306
	.quad	.LC307
	.quad	.LC308
	.quad	.LC309
	.quad	.LC310
	.quad	.LC311
	.quad	.LC312
	.quad	.LC313
	.quad	.LC314
	.quad	.LC315
	.quad	.LC316
	.quad	.LC317
	.quad	.LC318
	.quad	.LC319
	.quad	.LC320
	.quad	.LC321
	.quad	.LC322
	.quad	.LC323
	.quad	.LC324
	.quad	.LC325
	.quad	.LC326
	.quad	.LC327
	.quad	.LC328
	.quad	.LC329
	.quad	.LC330
	.quad	.LC331
	.quad	.LC332
	.quad	.LC333
	.quad	.LC334
	.quad	.LC335
	.quad	.LC336
	.quad	.LC337
	.quad	.LC338
	.quad	.LC339
	.quad	.LC340
	.quad	.LC341
	.quad	.LC342
	.quad	.LC343
	.quad	.LC344
	.quad	.LC345
	.quad	.LC346
	.quad	.LC347
	.quad	.LC348
	.quad	.LC349
	.quad	.LC350
	.quad	.LC351
	.quad	.LC352
	.quad	.LC353
	.quad	.LC354
	.quad	.LC355
	.quad	.LC356
	.quad	.LC357
	.quad	.LC358
	.quad	.LC359
	.quad	.LC360
	.quad	.LC361
	.quad	.LC362
	.quad	.LC363
	.quad	.LC364
	.quad	.LC365
	.quad	.LC366
	.quad	.LC367
	.quad	.LC368
	.quad	.LC369
	.quad	.LC370
	.quad	.LC371
	.quad	.LC372
	.quad	.LC373
	.quad	.LC374
	.quad	.LC375
	.quad	.LC376
	.quad	.LC377
	.quad	.LC378
	.quad	.LC379
	.quad	.LC380
	.quad	.LC381
	.quad	.LC382
	.quad	.LC383
	.quad	.LC384
	.quad	.LC385
	.quad	.LC386
	.quad	.LC387
	.quad	.LC388
	.quad	.LC389
	.quad	.LC390
	.quad	.LC391
	.quad	.LC392
	.quad	.LC393
	.quad	.LC394
	.quad	.LC395
	.quad	.LC396
	.quad	.LC397
	.quad	.LC398
	.quad	.LC399
	.quad	.LC400
	.quad	.LC401
	.quad	.LC402
	.quad	.LC403
	.quad	.LC404
	.quad	.LC405
	.quad	.LC406
	.quad	.LC407
	.quad	.LC408
	.quad	.LC409
	.quad	.LC410
	.quad	.LC411
	.quad	.LC412
	.quad	.LC413
	.quad	.LC414
	.quad	.LC415
	.quad	.LC416
	.quad	.LC417
	.quad	.LC418
	.quad	.LC419
	.quad	.LC420
	.quad	.LC421
	.quad	.LC422
	.quad	.LC423
	.quad	.LC424
	.quad	.LC425
	.quad	.LC426
	.quad	.LC427
	.quad	.LC428
	.quad	.LC429
	.quad	.LC430
	.quad	.LC431
	.quad	.LC432
	.quad	.LC433
	.quad	.LC434
	.quad	.LC435
	.quad	.LC436
	.quad	.LC437
	.quad	.LC438
	.quad	.LC439
	.quad	.LC440
	.quad	.LC441
	.quad	.LC442
	.quad	.LC443
	.quad	.LC444
	.quad	.LC445
	.quad	.LC446
	.quad	.LC447
	.quad	.LC448
	.quad	.LC449
	.quad	.LC450
	.quad	.LC451
	.quad	.LC452
	.quad	.LC453
	.quad	.LC454
	.quad	.LC455
	.quad	.LC456
	.quad	.LC457
	.quad	.LC458
	.quad	.LC459
	.quad	.LC460
	.quad	.LC461
	.quad	.LC462
	.quad	.LC463
	.quad	.LC464
	.quad	.LC465
	.quad	.LC466
	.quad	.LC467
	.quad	.LC468
	.quad	.LC469
	.quad	.LC470
	.quad	.LC471
	.quad	.LC472
	.quad	.LC473
	.quad	.LC474
	.quad	.LC475
	.quad	.LC476
	.quad	.LC477
	.quad	.LC478
	.quad	.LC479
	.quad	.LC480
	.quad	.LC481
	.quad	.LC482
	.quad	.LC483
	.quad	.LC484
	.quad	.LC485
	.quad	.LC486
	.quad	.LC487
	.quad	.LC488
	.quad	.LC489
	.quad	.LC490
	.quad	.LC491
	.quad	.LC492
	.quad	.LC493
	.quad	.LC494
	.quad	.LC495
	.quad	.LC496
	.quad	.LC497
	.quad	.LC498
	.quad	.LC499
	.quad	.LC500
	.quad	.LC501
	.quad	.LC502
	.quad	.LC503
	.quad	.LC504
	.quad	.LC505
	.quad	.LC506
	.quad	.LC507
	.quad	.LC508
	.quad	.LC509
	.quad	.LC510
	.quad	.LC511
	.quad	.LC512
	.quad	.LC513
	.quad	.LC514
	.quad	.LC515
	.quad	.LC516
	.quad	.LC517
	.quad	.LC518
	.quad	.LC519
	.quad	.LC520
	.quad	.LC521
	.quad	.LC522
	.quad	.LC523
	.quad	.LC524
	.quad	.LC525
	.quad	.LC526
	.quad	.LC527
	.quad	.LC528
	.quad	.LC529
	.quad	.LC530
	.quad	.LC531
	.quad	.LC532
	.quad	.LC533
	.quad	.LC534
	.quad	.LC535
	.quad	.LC536
	.quad	.LC537
	.quad	.LC538
	.quad	.LC539
	.quad	.LC540
	.quad	.LC541
	.quad	.LC542
	.quad	.LC543
	.quad	.LC544
	.quad	.LC545
	.quad	.LC546
	.quad	.LC547
	.quad	.LC548
	.quad	.LC549
	.quad	.LC550
	.quad	.LC551
	.quad	.LC552
	.quad	.LC553
	.quad	.LC554
	.quad	.LC555
	.quad	.LC556
	.quad	.LC557
	.quad	.LC558
	.quad	.LC559
	.quad	.LC560
	.quad	.LC561
	.quad	.LC562
	.quad	.LC563
	.quad	.LC564
	.quad	.LC565
	.quad	.LC566
	.quad	.LC567
	.quad	.LC568
	.quad	.LC569
	.quad	.LC570
	.quad	.LC571
	.quad	.LC572
	.quad	.LC573
	.quad	.LC574
	.quad	.LC575
	.quad	.LC576
	.quad	.LC577
	.quad	.LC578
	.quad	.LC579
	.quad	.LC580
	.quad	.LC581
	.quad	.LC582
	.quad	.LC583
	.quad	.LC584
	.quad	.LC585
	.quad	.LC586
	.quad	.LC587
	.quad	.LC588
	.quad	.LC589
	.quad	.LC590
	.quad	.LC591
	.quad	.LC592
	.quad	.LC593
	.quad	.LC594
	.quad	.LC595
	.quad	.LC596
	.quad	.LC597
	.quad	.LC598
	.quad	.LC599
	.quad	.LC600
	.quad	.LC601
	.quad	.LC602
	.quad	.LC603
	.quad	.LC604
	.quad	.LC605
	.quad	.LC606
	.quad	.LC607
	.quad	.LC608
	.quad	.LC609
	.quad	.LC610
	.quad	.LC611
	.quad	.LC612
	.quad	.LC613
	.quad	.LC614
	.quad	.LC615
	.quad	.LC616
	.quad	.LC617
	.quad	.LC618
	.quad	.LC619
	.quad	.LC620
	.quad	.LC621
	.quad	.LC622
	.quad	.LC623
	.quad	.LC624
	.quad	.LC625
	.quad	.LC626
	.quad	.LC627
	.quad	.LC628
	.quad	.LC629
	.quad	.LC630
	.quad	.LC631
	.quad	.LC632
	.quad	.LC633
	.quad	.LC634
	.quad	.LC635
	.quad	.LC636
	.quad	.LC637
	.quad	.LC638
	.quad	.LC639
	.quad	.LC640
	.quad	.LC641
	.quad	.LC642
	.quad	.LC643
	.quad	.LC644
	.quad	.LC645
	.quad	.LC646
	.quad	.LC647
	.quad	.LC648
	.quad	.LC649
	.quad	.LC650
	.quad	.LC651
	.quad	.LC652
	.quad	.LC653
	.quad	.LC654
	.quad	.LC655
	.quad	.LC656
	.quad	.LC657
	.quad	.LC658
	.quad	.LC659
	.quad	.LC660
	.quad	.LC661
	.quad	.LC662
	.quad	.LC663
	.quad	.LC664
	.quad	.LC665
	.quad	.LC666
	.quad	.LC667
	.quad	.LC668
	.quad	.LC669
	.quad	.LC670
	.quad	.LC671
	.quad	.LC672
	.quad	.LC673
	.quad	.LC674
	.quad	.LC675
	.quad	.LC676
	.quad	.LC677
	.quad	.LC678
	.quad	.LC679
	.quad	.LC680
	.quad	.LC681
	.quad	.LC682
	.quad	.LC683
	.quad	.LC684
	.quad	.LC685
	.quad	.LC686
	.quad	.LC687
	.quad	.LC688
	.quad	.LC689
	.quad	.LC690
	.quad	.LC691
	.quad	.LC692
	.quad	.LC693
	.quad	.LC694
	.quad	.LC695
	.quad	.LC696
	.quad	.LC697
	.quad	.LC698
	.quad	.LC699
	.quad	.LC700
	.quad	.LC701
	.quad	.LC702
	.quad	.LC703
	.quad	.LC704
	.quad	.LC705
	.quad	.LC706
	.quad	.LC707
	.quad	.LC708
	.quad	.LC709
	.quad	.LC710
	.quad	.LC711
	.quad	.LC712
	.quad	.LC713
	.quad	.LC714
	.quad	.LC715
	.quad	.LC716
	.quad	.LC717
	.quad	.LC718
	.quad	.LC719
	.quad	.LC720
	.quad	.LC721
	.quad	.LC722
	.quad	.LC723
	.quad	.LC724
	.quad	.LC725
	.quad	.LC726
	.quad	.LC727
	.quad	.LC728
	.quad	.LC729
	.quad	.LC730
	.quad	.LC731
	.quad	.LC732
	.quad	.LC733
	.quad	.LC734
	.quad	.LC735
	.quad	.LC736
	.quad	.LC737
	.quad	.LC738
	.quad	.LC739
	.quad	.LC740
	.quad	.LC741
	.quad	.LC742
	.quad	.LC743
	.quad	.LC744
	.quad	.LC745
	.quad	.LC746
	.quad	.LC747
	.quad	.LC748
	.quad	.LC749
	.quad	.LC750
	.quad	.LC751
	.quad	.LC752
	.quad	.LC753
	.quad	.LC754
	.quad	.LC755
	.quad	.LC756
	.quad	.LC757
	.quad	.LC758
	.quad	.LC759
	.quad	.LC760
	.quad	.LC761
	.quad	.LC762
	.quad	.LC763
	.quad	.LC764
	.quad	.LC765
	.quad	.LC766
	.quad	.LC767
	.quad	.LC768
	.quad	.LC769
	.quad	.LC770
	.quad	.LC771
	.quad	.LC772
	.quad	.LC773
	.quad	.LC774
	.quad	.LC775
	.quad	.LC776
	.quad	.LC777
	.quad	.LC778
	.quad	.LC779
	.quad	.LC780
	.quad	.LC781
	.quad	.LC782
	.quad	.LC783
	.quad	.LC784
	.quad	.LC785
	.quad	.LC786
	.quad	.LC787
	.quad	.LC788
	.quad	.LC789
	.quad	.LC790
	.quad	.LC791
	.quad	.LC792
	.quad	.LC793
	.quad	.LC794
	.quad	.LC795
	.quad	.LC796
	.quad	.LC797
	.quad	.LC798
	.quad	.LC799
	.quad	.LC800
	.quad	.LC801
	.quad	.LC802
	.quad	.LC803
	.quad	.LC804
	.quad	.LC805
	.quad	.LC806
	.quad	.LC807
	.quad	.LC808
	.quad	.LC809
	.quad	.LC810
	.quad	.LC811
	.quad	.LC812
	.quad	.LC813
	.quad	.LC814
	.quad	.LC815
	.quad	.LC816
	.quad	.LC817
	.quad	.LC818
	.quad	.LC819
	.quad	.LC820
	.quad	.LC821
	.quad	.LC822
	.quad	.LC823
	.quad	.LC824
	.quad	.LC825
	.quad	.LC826
	.quad	.LC827
	.quad	.LC828
	.quad	.LC829
	.quad	.LC830
	.quad	.LC831
	.quad	.LC832
	.quad	.LC833
	.quad	.LC834
	.quad	.LC835
	.quad	.LC836
	.quad	.LC837
	.quad	.LC838
	.quad	.LC839
	.quad	.LC840
	.quad	.LC841
	.quad	.LC842
	.quad	.LC843
	.quad	.LC844
	.quad	.LC845
	.quad	.LC846
	.quad	.LC847
	.quad	.LC848
	.quad	.LC849
	.quad	.LC850
	.quad	.LC851
	.quad	.LC852
	.quad	.LC853
	.quad	.LC854
	.quad	.LC855
	.quad	.LC856
	.quad	.LC857
	.quad	.LC858
	.quad	.LC859
	.quad	.LC860
	.quad	.LC861
	.quad	.LC862
	.quad	.LC863
	.quad	.LC864
	.quad	.LC865
	.quad	.LC866
	.quad	.LC867
	.quad	.LC868
	.quad	.LC869
	.quad	.LC870
	.quad	.LC871
	.quad	.LC872
	.quad	.LC873
	.quad	.LC874
	.quad	.LC875
	.quad	.LC876
	.quad	.LC877
	.quad	.LC878
	.quad	.LC879
	.quad	.LC880
	.quad	.LC881
	.quad	.LC882
	.quad	.LC883
	.quad	.LC884
	.quad	.LC885
	.quad	.LC886
	.quad	.LC887
	.quad	.LC888
	.quad	.LC889
	.quad	.LC890
	.quad	.LC891
	.quad	.LC892
	.quad	.LC893
	.quad	.LC894
	.quad	.LC895
	.quad	.LC896
	.quad	.LC897
	.quad	.LC898
	.quad	.LC899
	.quad	.LC900
	.quad	.LC901
	.quad	.LC902
	.quad	.LC903
	.quad	.LC904
	.quad	.LC905
	.quad	.LC906
	.quad	.LC907
	.quad	.LC908
	.quad	.LC909
	.quad	.LC910
	.quad	.LC911
	.quad	.LC912
	.quad	.LC913
	.quad	.LC914
	.quad	.LC915
	.quad	.LC916
	.quad	.LC917
	.quad	.LC918
	.quad	.LC919
	.quad	.LC920
	.quad	.LC921
	.quad	.LC922
	.quad	.LC923
	.quad	.LC924
	.quad	.LC925
	.quad	.LC926
	.quad	.LC927
	.quad	.LC928
	.quad	.LC929
	.quad	.LC930
	.quad	.LC931
	.quad	.LC932
	.quad	.LC933
	.quad	.LC934
	.quad	.LC935
	.quad	.LC936
	.quad	.LC937
	.quad	.LC938
	.quad	.LC939
	.quad	.LC940
	.quad	.LC941
	.quad	.LC942
	.quad	.LC943
	.quad	.LC944
	.quad	.LC945
	.quad	.LC946
	.quad	.LC947
	.quad	.LC948
	.quad	.LC949
	.quad	.LC950
	.quad	.LC951
	.quad	.LC952
	.quad	.LC953
	.quad	.LC954
	.quad	.LC955
	.quad	.LC956
	.quad	.LC957
	.quad	.LC958
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
