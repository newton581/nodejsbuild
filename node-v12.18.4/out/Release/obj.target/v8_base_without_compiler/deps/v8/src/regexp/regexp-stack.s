	.file	"regexp-stack.cc"
	.text
	.section	.rodata._ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NewArray"
	.section	.text._ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE
	.type	_ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE, @function
_ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE:
.LFB8630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	41208(%rsi), %rbx
	cmpq	$1023, 16(%rbx)
	movq	%rbx, (%rdi)
	jbe	.L10
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$1024, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L11
.L3:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L12
.L4:
	movq	%r12, (%rbx)
	leaq	1024(%r12), %rax
	addq	$256, %r12
	movq	%rax, 8(%rbx)
	movq	$1024, 16(%rbx)
	movq	%r12, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	(%rbx), %r13
	movq	%r12, %rax
	subq	%rdx, %rax
	leaq	1024(%rax), %rdi
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	jmp	.L4
.L11:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$1024, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L3
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE8630:
	.size	_ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE, .-_ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE
	.globl	_ZN2v88internal16RegExpStackScopeC1EPNS0_7IsolateE
	.set	_ZN2v88internal16RegExpStackScopeC1EPNS0_7IsolateE,_ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal16RegExpStackScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpStackScopeD2Ev
	.type	_ZN2v88internal16RegExpStackScopeD2Ev, @function
_ZN2v88internal16RegExpStackScopeD2Ev:
.LFB8633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	cmpq	$1024, 16(%rbx)
	jbe	.L13
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L15
	call	_ZdaPv@PLT
.L15:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movdqa	.LC1(%rip), %xmm0
	movups	%xmm0, 16(%rbx)
.L13:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8633:
	.size	_ZN2v88internal16RegExpStackScopeD2Ev, .-_ZN2v88internal16RegExpStackScopeD2Ev
	.globl	_ZN2v88internal16RegExpStackScopeD1Ev
	.set	_ZN2v88internal16RegExpStackScopeD1Ev,_ZN2v88internal16RegExpStackScopeD2Ev
	.section	.text._ZN2v88internal11RegExpStackC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpStackC2Ev
	.type	_ZN2v88internal11RegExpStackC2Ev, @function
_ZN2v88internal11RegExpStackC2Ev:
.LFB8636:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rdi)
	movups	%xmm0, (%rdi)
	movdqa	.LC1(%rip), %xmm0
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE8636:
	.size	_ZN2v88internal11RegExpStackC2Ev, .-_ZN2v88internal11RegExpStackC2Ev
	.globl	_ZN2v88internal11RegExpStackC1Ev
	.set	_ZN2v88internal11RegExpStackC1Ev,_ZN2v88internal11RegExpStackC2Ev
	.section	.text._ZN2v88internal11RegExpStackD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpStackD2Ev
	.type	_ZN2v88internal11RegExpStackD2Ev, @function
_ZN2v88internal11RegExpStackD2Ev:
.LFB8639:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rdi)
	je	.L21
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L21
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	ret
	.cfi_endproc
.LFE8639:
	.size	_ZN2v88internal11RegExpStackD2Ev, .-_ZN2v88internal11RegExpStackD2Ev
	.globl	_ZN2v88internal11RegExpStackD1Ev
	.set	_ZN2v88internal11RegExpStackD1Ev,_ZN2v88internal11RegExpStackD2Ev
	.section	.text._ZN2v88internal11RegExpStack12ArchiveStackEPc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpStack12ArchiveStackEPc
	.type	_ZN2v88internal11RegExpStack12ArchiveStackEPc, @function
_ZN2v88internal11RegExpStack12ArchiveStackEPc:
.LFB8641:
	.cfi_startproc
	endbr64
	movdqu	(%rdi), %xmm1
	pxor	%xmm0, %xmm0
	leaq	32(%rsi), %rax
	movups	%xmm1, (%rsi)
	movdqu	16(%rdi), %xmm2
	movups	%xmm2, 16(%rsi)
	movups	%xmm0, (%rdi)
	movdqa	.LC1(%rip), %xmm0
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE8641:
	.size	_ZN2v88internal11RegExpStack12ArchiveStackEPc, .-_ZN2v88internal11RegExpStack12ArchiveStackEPc
	.section	.text._ZN2v88internal11RegExpStack12RestoreStackEPc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpStack12RestoreStackEPc
	.type	_ZN2v88internal11RegExpStack12RestoreStackEPc, @function
_ZN2v88internal11RegExpStack12RestoreStackEPc:
.LFB8642:
	.cfi_startproc
	endbr64
	movdqu	(%rsi), %xmm0
	leaq	32(%rsi), %rax
	movups	%xmm0, (%rdi)
	movdqu	16(%rsi), %xmm1
	movups	%xmm1, 16(%rdi)
	ret
	.cfi_endproc
.LFE8642:
	.size	_ZN2v88internal11RegExpStack12RestoreStackEPc, .-_ZN2v88internal11RegExpStack12RestoreStackEPc
	.section	.text._ZN2v88internal11RegExpStack5ResetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpStack5ResetEv
	.type	_ZN2v88internal11RegExpStack5ResetEv, @function
_ZN2v88internal11RegExpStack5ResetEv:
.LFB8643:
	.cfi_startproc
	endbr64
	cmpq	$1024, 16(%rdi)
	jbe	.L35
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	_ZdaPv@PLT
.L30:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movdqa	.LC1(%rip), %xmm0
	movups	%xmm0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE8643:
	.size	_ZN2v88internal11RegExpStack5ResetEv, .-_ZN2v88internal11RegExpStack5ResetEv
	.section	.text._ZN2v88internal11RegExpStack11ThreadLocal4FreeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpStack11ThreadLocal4FreeEv
	.type	_ZN2v88internal11RegExpStack11ThreadLocal4FreeEv, @function
_ZN2v88internal11RegExpStack11ThreadLocal4FreeEv:
.LFB8644:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rdi)
	je	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L40
	call	_ZdaPv@PLT
.L40:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movdqa	.LC1(%rip), %xmm0
	movups	%xmm0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE8644:
	.size	_ZN2v88internal11RegExpStack11ThreadLocal4FreeEv, .-_ZN2v88internal11RegExpStack11ThreadLocal4FreeEv
	.section	.text._ZN2v88internal11RegExpStack14EnsureCapacityEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpStack14EnsureCapacityEm
	.type	_ZN2v88internal11RegExpStack14EnsureCapacityEm, @function
_ZN2v88internal11RegExpStack14EnsureCapacityEm:
.LFB8645:
	.cfi_startproc
	endbr64
	cmpq	$67108864, %rsi
	ja	.L54
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpq	$1024, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	$1024, %ebx
	cmovnb	%rsi, %rbx
	cmpq	16(%rdi), %rbx
	ja	.L50
	popq	%rbx
	movq	8(%rdi), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L62
.L52:
	movq	16(%r12), %rdx
	testq	%rdx, %rdx
	jne	.L63
.L53:
	movq	%r13, (%r12)
	leaq	0(%r13,%rbx), %rax
	addq	$256, %r13
	movq	%rax, 8(%r12)
	movq	%rbx, 16(%r12)
	movq	%r13, 24(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	(%r12), %r14
	movq	%rbx, %rdi
	subq	%rdx, %rdi
	addq	%r13, %rdi
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	jmp	.L53
.L62:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L52
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE8645:
	.size	_ZN2v88internal11RegExpStack14EnsureCapacityEm, .-_ZN2v88internal11RegExpStack14EnsureCapacityEm
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE:
.LFB9901:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE9901:
	.size	_GLOBAL__sub_I__ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16RegExpStackScopeC2EPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	0
	.quad	-1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
