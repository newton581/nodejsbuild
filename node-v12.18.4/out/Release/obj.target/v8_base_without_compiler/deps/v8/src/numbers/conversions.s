	.file	"conversions.cc"
	.text
	.section	.text._ZN2v88internal17StringToIntHelper18HandleSpecialCasesEv,"axG",@progbits,_ZN2v88internal17StringToIntHelper18HandleSpecialCasesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17StringToIntHelper18HandleSpecialCasesEv
	.type	_ZN2v88internal17StringToIntHelper18HandleSpecialCasesEv, @function
_ZN2v88internal17StringToIntHelper18HandleSpecialCasesEv:
.LFB17798:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE17798:
	.size	_ZN2v88internal17StringToIntHelper18HandleSpecialCasesEv, .-_ZN2v88internal17StringToIntHelper18HandleSpecialCasesEv
	.section	.text._ZN2v88internal20NumberParseIntHelper14AllocateResultEv,"axG",@progbits,_ZN2v88internal20NumberParseIntHelper14AllocateResultEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20NumberParseIntHelper14AllocateResultEv
	.type	_ZN2v88internal20NumberParseIntHelper14AllocateResultEv, @function
_ZN2v88internal20NumberParseIntHelper14AllocateResultEv:
.LFB17825:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE17825:
	.size	_ZN2v88internal20NumberParseIntHelper14AllocateResultEv, .-_ZN2v88internal20NumberParseIntHelper14AllocateResultEv
	.section	.text._ZN2v88internal20NumberParseIntHelper17ResultMultiplyAddEjj,"axG",@progbits,_ZN2v88internal20NumberParseIntHelper17ResultMultiplyAddEjj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20NumberParseIntHelper17ResultMultiplyAddEjj
	.type	_ZN2v88internal20NumberParseIntHelper17ResultMultiplyAddEjj, @function
_ZN2v88internal20NumberParseIntHelper17ResultMultiplyAddEjj:
.LFB17826:
	.cfi_startproc
	endbr64
	movl	%esi, %esi
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movl	%edx, %edx
	cvtsi2sdq	%rsi, %xmm0
	mulsd	56(%rdi), %xmm0
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 56(%rdi)
	ret
	.cfi_endproc
.LFE17826:
	.size	_ZN2v88internal20NumberParseIntHelper17ResultMultiplyAddEjj, .-_ZN2v88internal20NumberParseIntHelper17ResultMultiplyAddEjj
	.section	.text._ZN2v88internal20StringToBigIntHelperD2Ev,"axG",@progbits,_ZN2v88internal20StringToBigIntHelperD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20StringToBigIntHelperD2Ev
	.type	_ZN2v88internal20StringToBigIntHelperD2Ev, @function
_ZN2v88internal20StringToBigIntHelperD2Ev:
.LFB21537:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21537:
	.size	_ZN2v88internal20StringToBigIntHelperD2Ev, .-_ZN2v88internal20StringToBigIntHelperD2Ev
	.weak	_ZN2v88internal20StringToBigIntHelperD1Ev
	.set	_ZN2v88internal20StringToBigIntHelperD1Ev,_ZN2v88internal20StringToBigIntHelperD2Ev
	.section	.text._ZN2v88internal20NumberParseIntHelperD2Ev,"axG",@progbits,_ZN2v88internal20NumberParseIntHelperD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20NumberParseIntHelperD2Ev
	.type	_ZN2v88internal20NumberParseIntHelperD2Ev, @function
_ZN2v88internal20NumberParseIntHelperD2Ev:
.LFB21541:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21541:
	.size	_ZN2v88internal20NumberParseIntHelperD2Ev, .-_ZN2v88internal20NumberParseIntHelperD2Ev
	.weak	_ZN2v88internal20NumberParseIntHelperD1Ev
	.set	_ZN2v88internal20NumberParseIntHelperD1Ev,_ZN2v88internal20NumberParseIntHelperD2Ev
	.section	.text._ZN2v88internal20StringToBigIntHelper14AllocateResultEv,"axG",@progbits,_ZN2v88internal20StringToBigIntHelper14AllocateResultEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20StringToBigIntHelper14AllocateResultEv
	.type	_ZN2v88internal20StringToBigIntHelper14AllocateResultEv, @function
_ZN2v88internal20StringToBigIntHelper14AllocateResultEv:
.LFB17846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$1, 64(%rdi)
	movl	40(%rdi), %edx
	sete	%r8b
	movl	32(%rdi), %esi
	subl	36(%rdi), %edx
	movq	8(%rdi), %rdi
	call	_ZN2v88internal6BigInt11AllocateForEPNS0_7IsolateEiiNS0_11ShouldThrowENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L14
	movq	%rax, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	$1, 52(%rbx)
	movq	%rax, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17846:
	.size	_ZN2v88internal20StringToBigIntHelper14AllocateResultEv, .-_ZN2v88internal20StringToBigIntHelper14AllocateResultEv
	.section	.text._ZN2v88internal20StringToBigIntHelper17ResultMultiplyAddEjj,"axG",@progbits,_ZN2v88internal20StringToBigIntHelper17ResultMultiplyAddEjj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20StringToBigIntHelper17ResultMultiplyAddEjj
	.type	_ZN2v88internal20StringToBigIntHelper17ResultMultiplyAddEjj, @function
_ZN2v88internal20StringToBigIntHelper17ResultMultiplyAddEjj:
.LFB17847:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	movl	%edx, %edx
	movl	%esi, %esi
	jmp	_ZN2v88internal6BigInt18InplaceMultiplyAddENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEmm@PLT
	.cfi_endproc
.LFE17847:
	.size	_ZN2v88internal20StringToBigIntHelper17ResultMultiplyAddEjj, .-_ZN2v88internal20StringToBigIntHelper17ResultMultiplyAddEjj
	.section	.text._ZN2v88internal20NumberParseIntHelperD0Ev,"axG",@progbits,_ZN2v88internal20NumberParseIntHelperD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20NumberParseIntHelperD0Ev
	.type	_ZN2v88internal20NumberParseIntHelperD0Ev, @function
_ZN2v88internal20NumberParseIntHelperD0Ev:
.LFB21543:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21543:
	.size	_ZN2v88internal20NumberParseIntHelperD0Ev, .-_ZN2v88internal20NumberParseIntHelperD0Ev
	.section	.text._ZN2v88internal20StringToBigIntHelperD0Ev,"axG",@progbits,_ZN2v88internal20StringToBigIntHelperD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20StringToBigIntHelperD0Ev
	.type	_ZN2v88internal20StringToBigIntHelperD0Ev, @function
_ZN2v88internal20StringToBigIntHelperD0Ev:
.LFB21539:
	.cfi_startproc
	endbr64
	movl	$72, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21539:
	.size	_ZN2v88internal20StringToBigIntHelperD0Ev, .-_ZN2v88internal20StringToBigIntHelperD0Ev
	.section	.text._ZN2v88internalL31CreateExponentialRepresentationEPcibi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31CreateExponentialRepresentationEPcibi, @function
_ZN2v88internalL31CreateExponentialRepresentationEPcibi:
.LFB17858:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jns	.L19
	negl	%r12d
	movl	$1, %r13d
.L19:
	leaq	-80(%rbp), %r15
	leal	8(%rbx), %esi
	movl	%edx, -84(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal19SimpleStringBuilderC1Ei@PLT
	movl	-84(%rbp), %edx
	testb	%dl, %dl
	je	.L20
	movslq	-64(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movq	-80(%rbp), %rdx
	movb	$45, (%rdx,%rax)
.L20:
	movslq	-64(%rbp), %rax
	movzbl	(%r14), %ecx
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movq	-80(%rbp), %rdx
	movb	%cl, (%rdx,%rax)
	cmpl	$1, %ebx
	jne	.L36
.L21:
	movslq	-64(%rbp), %rax
	cmpb	$1, %r13b
	movl	%r12d, %esi
	movq	%r15, %rdi
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movq	-80(%rbp), %rdx
	movb	$101, (%rdx,%rax)
	movslq	-64(%rbp), %rdx
	sbbl	%eax, %eax
	andl	$-2, %eax
	leal	1(%rdx), %ecx
	addl	$45, %eax
	movl	%ecx, -64(%rbp)
	movq	-80(%rbp), %rcx
	movb	%al, (%rcx,%rdx)
	call	_ZN2v88internal19SimpleStringBuilder17AddDecimalIntegerEi@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal19SimpleStringBuilder8FinalizeEv@PLT
	movq	%rax, %r12
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	js	.L18
	movq	%r15, %rdi
	call	_ZN2v88internal19SimpleStringBuilder8FinalizeEv@PLT
.L18:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movslq	-64(%rbp), %rax
	leaq	1(%r14), %rsi
	movq	%r15, %rdi
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movq	-80(%rbp), %rdx
	movb	$46, (%rdx,%rax)
	call	_ZN2v88internal19SimpleStringBuilder9AddStringEPKc@PLT
	movq	%r14, %rsi
.L22:
	movl	(%rsi), %edx
	addq	$4, %rsi
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L22
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rsi), %rdx
	cmove	%rdx, %rsi
	movl	%eax, %edi
	addb	%al, %dil
	movq	%r15, %rdi
	sbbq	$3, %rsi
	subq	%r14, %rsi
	subl	%esi, %ebx
	movl	$48, %esi
	movl	%ebx, %edx
	call	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci@PLT
	jmp	.L21
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17858:
	.size	_ZN2v88internalL31CreateExponentialRepresentationEPcibi, .-_ZN2v88internalL31CreateExponentialRepresentationEPcibi
	.section	.rodata._ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"0"
.LC1:
	.string	"-Infinity"
.LC2:
	.string	"NaN"
.LC3:
	.string	"Infinity"
.LC10:
	.string	"0."
	.section	.text._ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE
	.type	_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE, @function
_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE:
.LFB17851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	.LC2(%rip), %r12
	subq	$80, %rsp
	andpd	.LC4(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	ucomisd	%xmm1, %xmm1
	jp	.L38
	ucomisd	.LC5(%rip), %xmm1
	ja	.L81
	comisd	.LC6(%rip), %xmm1
	jnb	.L42
	ucomisd	.LC7(%rip), %xmm0
	jp	.L42
	jne	.L42
	leaq	.LC0(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	pxor	%xmm1, %xmm1
	leaq	.LC1(%rip), %r12
	leaq	.LC3(%rip), %rax
	comisd	%xmm0, %xmm1
	cmovbe	%rax, %r12
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L42:
	comisd	.LC8(%rip), %xmm0
	jb	.L44
	movsd	.LC9(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L44
	movabsq	$-9223372036854775808, %rax
	movq	%xmm0, %rcx
	cmpq	%rax, %rcx
	je	.L44
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L44
	jne	.L44
	movl	$1, %r9d
	testl	%ecx, %ecx
	js	.L48
	negl	%ecx
	xorl	%r9d, %r9d
.L48:
	subl	$1, %esi
	movl	$48, %r10d
	movslq	%esi, %rax
	movb	$0, (%rdi,%rax)
	leaq	-1(%rax,%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rax, %r12
	movslq	%ecx, %rax
	movl	%ecx, %edx
	movl	%esi, %r8d
	imulq	$1717986919, %rax, %rax
	sarl	$31, %edx
	subl	$1, %esi
	sarq	$34, %rax
	subl	%edx, %eax
	leal	(%rax,%rax,4), %edx
	leal	(%r10,%rdx,2), %edx
	subl	%ecx, %edx
	movl	%eax, %ecx
	leaq	-1(%r12), %rax
	movb	%dl, (%r12)
	testl	%ecx, %ecx
	jne	.L49
	testb	%r9b, %r9b
	je	.L38
	leal	-2(%r8), %r12d
	movslq	%r12d, %r12
	addq	%rdi, %r12
	movb	$45, (%r12)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L44:
	subq	$8, %rsp
	leaq	-92(%rbp), %rax
	movslq	%esi, %rsi
	movq	%rdi, -80(%rbp)
	pushq	%rax
	leaq	-48(%rbp), %r12
	movl	$18, %ecx
	leaq	-84(%rbp), %r9
	movq	%rsi, -72(%rbp)
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	-88(%rbp), %r8
	xorl	%edi, %edi
	movl	$0, -64(%rbp)
	call	_ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_@PLT
	movl	-88(%rbp), %esi
	popq	%rdx
	popq	%rcx
	testl	%esi, %esi
	je	.L52
	movslq	-64(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movq	-80(%rbp), %rdx
	movb	$45, (%rdx,%rax)
.L52:
	movl	-92(%rbp), %edx
	cmpl	%edx, -84(%rbp)
	jg	.L53
	cmpl	$21, %edx
	jle	.L84
.L53:
	leal	-1(%rdx), %eax
	cmpl	$20, %eax
	jbe	.L85
	addl	$5, %edx
	cmpl	$5, %edx
	jbe	.L86
	movslq	-64(%rbp), %rax
	movzbl	-48(%rbp), %ecx
	leaq	-80(%rbp), %r13
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movq	-80(%rbp), %rdx
	movb	%cl, (%rdx,%rax)
	cmpl	$1, -84(%rbp)
	jne	.L87
.L57:
	movslq	-64(%rbp), %rax
	movq	%r13, %rdi
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movq	-80(%rbp), %rdx
	movb	$101, (%rdx,%rax)
	movslq	-64(%rbp), %rdx
	movl	-92(%rbp), %eax
	leal	1(%rdx), %ecx
	sarl	$31, %eax
	movl	%ecx, -64(%rbp)
	movq	-80(%rbp), %rcx
	andl	$2, %eax
	addl	$43, %eax
	movb	%al, (%rcx,%rdx)
	movl	-92(%rbp), %esi
	movl	$1, %eax
	subl	%esi, %eax
	subl	$1, %esi
	cmovs	%eax, %esi
	call	_ZN2v88internal19SimpleStringBuilder17AddDecimalIntegerEi@PLT
.L54:
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder8FinalizeEv@PLT
	movq	%rax, %r12
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	js	.L38
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder8FinalizeEv@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	-80(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder12AddSubstringEPKci@PLT
	movslq	-64(%rbp), %rax
	movq	%r13, %rdi
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movq	-80(%rbp), %rdx
	movb	$46, (%rdx,%rax)
	movslq	-92(%rbp), %rsi
	addq	%r12, %rsi
	call	_ZN2v88internal19SimpleStringBuilder9AddStringEPKc@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L87:
	movslq	-64(%rbp), %rax
	leaq	-47(%rbp), %rsi
	movq	%r13, %rdi
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movq	-80(%rbp), %rdx
	movb	$46, (%rdx,%rax)
	call	_ZN2v88internal19SimpleStringBuilder9AddStringEPKc@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	-80(%rbp), %r13
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder9AddStringEPKc@PLT
	movl	-92(%rbp), %edx
	movq	%r13, %rdi
	movl	$48, %esi
	negl	%edx
	call	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder9AddStringEPKc@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	-80(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder9AddStringEPKc@PLT
	movl	-92(%rbp), %edx
	movl	$48, %esi
	subl	-84(%rbp), %edx
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci@PLT
	jmp	.L54
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17851:
	.size	_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE, .-_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE
	.section	.text._ZN2v88internal12IntToCStringEiNS0_6VectorIcEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12IntToCStringEiNS0_6VectorIcEE
	.type	_ZN2v88internal12IntToCStringEiNS0_6VectorIcEE, @function
_ZN2v88internal12IntToCStringEiNS0_6VectorIcEE:
.LFB17853:
	.cfi_startproc
	endbr64
	movl	$1, %r11d
	testl	%edi, %edi
	js	.L89
	negl	%edi
	xorl	%r11d, %r11d
.L89:
	leal	-1(%rdx), %ecx
	movl	$48, %r10d
	movslq	%ecx, %rax
	movb	$0, (%rsi,%rax)
	leaq	-1(%rax,%rsi), %rax
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%rax, %r8
	movslq	%edi, %rax
	movl	%edi, %edx
	movl	%ecx, %r9d
	imulq	$1717986919, %rax, %rax
	sarl	$31, %edx
	subl	$1, %ecx
	sarq	$34, %rax
	subl	%edx, %eax
	leal	(%rax,%rax,4), %edx
	leal	(%r10,%rdx,2), %edx
	subl	%edi, %edx
	movl	%eax, %edi
	leaq	-1(%r8), %rax
	movb	%dl, (%r8)
	testl	%edi, %edi
	jne	.L90
	testb	%r11b, %r11b
	je	.L88
	leal	-2(%r9), %r8d
	movslq	%r8d, %r8
	addq	%rsi, %r8
	movb	$45, (%r8)
.L88:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE17853:
	.size	_ZN2v88internal12IntToCStringEiNS0_6VectorIcEE, .-_ZN2v88internal12IntToCStringEiNS0_6VectorIcEE
	.section	.text._ZN2v88internal20DoubleToFixedCStringEdi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20DoubleToFixedCStringEdi
	.type	_ZN2v88internal20DoubleToFixedCStringEdi, @function
_ZN2v88internal20DoubleToFixedCStringEdi:
.LFB17854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	comisd	%xmm0, %xmm1
	movapd	%xmm0, %xmm1
	ja	.L124
	xorl	%ebx, %ebx
.L98:
	comisd	.LC12(%rip), %xmm1
	jb	.L122
	leaq	-192(%rbp), %rdi
	movl	$100, %esi
	call	_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE
	movq	%rax, %rdi
	call	_ZN2v88internal6StrDupEPKc@PLT
	movq	%rax, %r12
.L97:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	subq	$8, %rsp
	movl	$122, %ecx
	movl	%r12d, %esi
	xorl	%r15d, %r15d
	leaq	-268(%rbp), %rax
	leaq	-192(%rbp), %r14
	movl	$1, %edi
	pushq	%rax
	leaq	-260(%rbp), %r9
	leaq	-264(%rbp), %r8
	movq	%r14, %rdx
	call	_ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_@PLT
	movl	-268(%rbp), %eax
	popq	%rcx
	popq	%rsi
	testl	%eax, %eax
	jle	.L126
.L103:
	movl	-260(%rbp), %edx
	addl	%r12d, %eax
	xorl	%r8d, %r8d
	leal	(%rdx,%r15), %esi
	cmpl	%eax, %esi
	jge	.L104
	movl	%eax, %r8d
	movl	%eax, %esi
	subl	%edx, %r8d
	subl	%r15d, %r8d
.L104:
	leaq	-256(%rbp), %r13
	addl	$1, %esi
	movl	%r8d, -276(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilderC1Ei@PLT
	movl	%r15d, %edx
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-224(%rbp), %r14
	call	_ZN2v88internal19SimpleStringBuilder9AddStringEPKc@PLT
	movl	-276(%rbp), %r8d
	movl	$48, %esi
	movq	%r13, %rdi
	movl	%r8d, %edx
	call	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder8FinalizeEv@PLT
	movl	-268(%rbp), %esi
	movq	%r14, %rdi
	movq	%rax, %r15
	addl	%r12d, %esi
	addl	$3, %esi
	call	_ZN2v88internal19SimpleStringBuilderC1Ei@PLT
	testb	%bl, %bl
	je	.L105
	movslq	-208(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -208(%rbp)
	movq	-224(%rbp), %rdx
	movb	$45, (%rdx,%rax)
.L105:
	movl	-268(%rbp), %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal19SimpleStringBuilder12AddSubstringEPKci@PLT
	testl	%r12d, %r12d
	jg	.L127
	testq	%r15, %r15
	je	.L107
.L129:
	movq	%r15, %rdi
	call	_ZdaPv@PLT
.L107:
	movq	%r14, %rdi
	call	_ZN2v88internal19SimpleStringBuilder8FinalizeEv@PLT
	movl	-208(%rbp), %edx
	movq	%rax, %r12
	testl	%edx, %edx
	jns	.L128
	movl	-240(%rbp), %eax
	testl	%eax, %eax
	js	.L97
.L130:
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder8FinalizeEv@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L127:
	movslq	-208(%rbp), %rax
	movq	%r14, %rdi
	leal	1(%rax), %edx
	movl	%edx, -208(%rbp)
	movq	-224(%rbp), %rdx
	movb	$46, (%rdx,%rax)
	movslq	-268(%rbp), %rsi
	movl	%r12d, %edx
	addq	%r15, %rsi
	call	_ZN2v88internal19SimpleStringBuilder12AddSubstringEPKci@PLT
	testq	%r15, %r15
	jne	.L129
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r14, %rdi
	call	_ZN2v88internal19SimpleStringBuilder8FinalizeEv@PLT
	movl	-240(%rbp), %eax
	testl	%eax, %eax
	js	.L97
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$1, -268(%rbp)
	movl	$1, %r15d
	subl	%eax, %r15d
	movl	$1, %eax
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L124:
	xorpd	.LC11(%rip), %xmm1
	movl	$1, %ebx
	jmp	.L98
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17854:
	.size	_ZN2v88internal20DoubleToFixedCStringEdi, .-_ZN2v88internal20DoubleToFixedCStringEdi
	.section	.text._ZN2v88internal26DoubleToExponentialCStringEdi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26DoubleToExponentialCStringEdi
	.type	_ZN2v88internal26DoubleToExponentialCStringEdi, @function
_ZN2v88internal26DoubleToExponentialCStringEdi:
.LFB17859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	subq	$136, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	comisd	%xmm0, %xmm1
	jbe	.L132
	xorpd	.LC11(%rip), %xmm0
	movl	$1, %r14d
.L132:
	leaq	-144(%rbp), %r13
	cmpl	$-1, %edi
	je	.L141
	subq	$8, %rsp
	leal	1(%rdi), %r12d
	movq	%r13, %rdx
	movl	$102, %ecx
	leaq	-156(%rbp), %rax
	leaq	-148(%rbp), %r9
	movl	%r12d, %esi
	movl	$2, %edi
	pushq	%rax
	leaq	-152(%rbp), %r8
	call	_ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_@PLT
	popq	%rax
	popq	%rdx
.L135:
	movl	-156(%rbp), %eax
	movl	%r14d, %edx
	movl	%r12d, %ecx
	movq	%r13, %rdi
	leal	-1(%rax), %esi
	call	_ZN2v88internalL31CreateExponentialRepresentationEPcibi
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L142
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	subq	$8, %rsp
	leaq	-156(%rbp), %rax
	xorl	%esi, %esi
	movq	%r13, %rdx
	pushq	%rax
	movl	$102, %ecx
	leaq	-148(%rbp), %r9
	xorl	%edi, %edi
	leaq	-152(%rbp), %r8
	call	_ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_@PLT
	popq	%rcx
	movl	-148(%rbp), %r12d
	popq	%rsi
	jmp	.L135
.L142:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17859:
	.size	_ZN2v88internal26DoubleToExponentialCStringEdi, .-_ZN2v88internal26DoubleToExponentialCStringEdi
	.section	.text._ZN2v88internal24DoubleToPrecisionCStringEdi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24DoubleToPrecisionCStringEdi
	.type	_ZN2v88internal24DoubleToPrecisionCStringEdi, @function
_ZN2v88internal24DoubleToPrecisionCStringEdi:
.LFB17860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	comisd	%xmm0, %xmm1
	jbe	.L144
	xorpd	.LC11(%rip), %xmm0
	movl	$1, %r13d
	movl	$1, %ebx
.L144:
	subq	$8, %rsp
	leaq	-204(%rbp), %rax
	movl	$101, %ecx
	movl	%r12d, %esi
	pushq	%rax
	leaq	-160(%rbp), %r14
	leaq	-196(%rbp), %r9
	movl	$2, %edi
	movq	%r14, %rdx
	leaq	-200(%rbp), %r8
	call	_ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_@PLT
	movl	-204(%rbp), %eax
	popq	%rdx
	popq	%rcx
	leal	-1(%rax), %esi
	cmpl	$-6, %esi
	jl	.L161
	cmpl	%esi, %r12d
	jle	.L161
	movl	%r12d, %esi
	leaq	-192(%rbp), %r13
	subl	%eax, %esi
	testl	%eax, %eax
	movq	%r13, %rdi
	leal	3(%rsi), %edx
	leal	2(%r12), %esi
	cmovle	%edx, %esi
	addl	$1, %esi
	call	_ZN2v88internal19SimpleStringBuilderC1Ei@PLT
	testb	%bl, %bl
	je	.L151
	movslq	-176(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -176(%rbp)
	movq	-192(%rbp), %rdx
	movb	$45, (%rdx,%rax)
.L151:
	movl	-204(%rbp), %edx
	testl	%edx, %edx
	jle	.L167
	cmpl	%edx, -196(%rbp)
	cmovle	-196(%rbp), %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder12AddSubstringEPKci@PLT
	movl	-204(%rbp), %edx
	movl	$48, %esi
	movq	%r13, %rdi
	subl	-196(%rbp), %edx
	call	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci@PLT
	cmpl	%r12d, -204(%rbp)
	jge	.L153
	movslq	-176(%rbp), %rax
	andl	$1, %ebx
	addl	$1, %ebx
	leal	1(%rax), %edx
	movl	%edx, -176(%rbp)
	movq	-192(%rbp), %rdx
	movb	$46, (%rdx,%rax)
	movslq	-204(%rbp), %rsi
	movl	-176(%rbp), %r15d
	cmpl	%esi, -196(%rbp)
	jg	.L168
.L155:
	subl	%r15d, %r12d
	movl	$48, %esi
	movq	%r13, %rdi
	leal	(%r12,%rbx), %edx
	call	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci@PLT
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L167:
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder9AddStringEPKc@PLT
	movl	-204(%rbp), %edx
	movl	$48, %esi
	movq	%r13, %rdi
	negl	%edx
	call	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder9AddStringEPKc@PLT
	movl	%r12d, %edx
	movl	$48, %esi
	movq	%r13, %rdi
	subl	-196(%rbp), %edx
	call	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci@PLT
.L153:
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder8FinalizeEv@PLT
	movq	%rax, %r12
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	js	.L143
	movq	%r13, %rdi
	call	_ZN2v88internal19SimpleStringBuilder8FinalizeEv@PLT
.L143:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L169
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movl	%r12d, %ecx
	movl	%r13d, %edx
	movq	%r14, %rdi
	call	_ZN2v88internalL31CreateExponentialRepresentationEPcibi
	movq	%rax, %r12
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L168:
	addq	%rsi, %r14
	subl	%ebx, %r15d
	movq	%r14, %rdi
	call	strlen@PLT
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	subl	%r15d, %edx
	cmpl	%eax, %edx
	cmovg	%eax, %edx
	call	_ZN2v88internal19SimpleStringBuilder12AddSubstringEPKci@PLT
	movl	-176(%rbp), %r15d
	jmp	.L155
.L169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17860:
	.size	_ZN2v88internal24DoubleToPrecisionCStringEdi, .-_ZN2v88internal24DoubleToPrecisionCStringEdi
	.section	.rodata._ZN2v88internal20DoubleToRadixCStringEdi.str1.1,"aMS",@progbits,1
.LC18:
	.string	"NewArray"
	.section	.text._ZN2v88internal20DoubleToRadixCStringEdi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20DoubleToRadixCStringEdi
	.type	_ZN2v88internal20DoubleToRadixCStringEdi, @function
_ZN2v88internal20DoubleToRadixCStringEdi:
.LFB17861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm5, %xmm5
	movapd	%xmm0, %xmm7
	movapd	%xmm0, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2248, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movsd	%xmm0, -2288(%rbp)
	cmpltsd	%xmm5, %xmm0
	xorpd	.LC11(%rip), %xmm1
	movsd	.LC4(%rip), %xmm2
	movsd	.LC15(%rip), %xmm3
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andpd	%xmm0, %xmm1
	andnpd	%xmm7, %xmm0
	orpd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movapd	%xmm1, %xmm6
	andpd	%xmm2, %xmm0
	ucomisd	%xmm0, %xmm3
	jbe	.L173
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm0, %xmm0
	movsd	.LC16(%rip), %xmm4
	andnpd	%xmm1, %xmm2
	cvtsi2sdq	%rax, %xmm0
	movapd	%xmm0, %xmm3
	movapd	%xmm0, %xmm6
	cmpnlesd	%xmm1, %xmm3
	andpd	%xmm4, %xmm3
	subsd	%xmm3, %xmm6
	orpd	%xmm2, %xmm6
.L173:
	movapd	%xmm1, %xmm0
	movq	%xmm1, %rdx
	movapd	%xmm6, %xmm2
	movabsq	$9218868437227405312, %rax
	subsd	%xmm6, %xmm0
	movsd	.LC13(%rip), %xmm3
	cmpq	%rax, %rdx
	je	.L174
	leaq	1(%rdx), %rax
	movq	%rax, %xmm3
	testq	%rdx, %rdx
	js	.L226
.L174:
	subsd	%xmm1, %xmm3
	movsd	.LC17(%rip), %xmm5
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm5, %xmm3
	maxsd	.LC14(%rip), %xmm3
	comisd	%xmm3, %xmm0
	jb	.L223
	movb	$46, -1156(%rbp)
	movsd	.LC16(%rip), %xmm7
	leaq	-1155(%rbp), %rdx
	movl	$1101, %r15d
	leaq	_ZZN2v88internal20DoubleToRadixCStringEdiE5chars(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L188:
	mulsd	%xmm1, %xmm0
	pxor	%xmm4, %xmm4
	movl	%r15d, %ebx
	addl	$1, %r15d
	mulsd	%xmm1, %xmm3
	cvttsd2sil	%xmm0, %eax
	cvtsi2sdl	%eax, %xmm4
	movslq	%eax, %rcx
	movzbl	(%r12,%rcx), %ecx
	movb	%cl, (%rdx)
	subsd	%xmm4, %xmm0
	comisd	%xmm5, %xmm0
	ja	.L179
	ucomisd	%xmm5, %xmm0
	jp	.L180
	jne	.L180
	testb	$1, %al
	je	.L180
.L179:
	movapd	%xmm3, %xmm4
	addsd	%xmm0, %xmm4
	comisd	%xmm7, %xmm4
	ja	.L227
.L180:
	addq	$1, %rdx
	comisd	%xmm3, %xmm0
	jnb	.L188
	leaq	-2256(%rbp), %rax
	addl	$2, %ebx
	movq	%rax, -2280(%rbp)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	-2256(%rbp), %rax
	movl	$1101, %ebx
	movl	$1100, %r15d
	movq	%rax, -2280(%rbp)
	leaq	_ZZN2v88internal20DoubleToRadixCStringEdiE5chars(%rip), %r12
.L177:
	movabsq	$9218868437227405312, %rdi
	movapd	%xmm2, %xmm0
	divsd	%xmm1, %xmm0
	movq	%xmm0, %rax
	testq	%rdi, %rax
	je	.L204
	leaq	-1157(%rbp), %rdx
	movl	$1100, %ecx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L229:
	movapd	%xmm0, %xmm3
	movb	$48, (%rdx)
	movapd	%xmm0, %xmm2
	subq	$1, %rdx
	divsd	%xmm1, %xmm3
	movq	%xmm3, %rax
	testq	%rdi, %rax
	je	.L228
	movapd	%xmm3, %xmm0
.L190:
	shrq	$52, %rax
	movl	%ecx, %r13d
	leal	-1(%rcx), %ecx
	andl	$2047, %eax
	cmpl	$1075, %eax
	jg	.L229
.L189:
	movslq	%r13d, %r14
	addq	-2280(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L191:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -2272(%rbp)
	subq	$1, %r14
	movsd	%xmm2, -2264(%rbp)
	call	fmod@PLT
	movsd	-2264(%rbp), %xmm2
	pxor	%xmm6, %xmm6
	movl	%r13d, %edx
	movsd	-2272(%rbp), %xmm1
	cvttsd2sil	%xmm0, %eax
	subl	$1, %r13d
	subsd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	cltq
	movzbl	(%r12,%rax), %eax
	movb	%al, (%r14)
	comisd	%xmm6, %xmm2
	ja	.L191
	comisd	-2288(%rbp), %xmm6
	jbe	.L192
	leal	-2(%rdx), %r13d
	movslq	%r13d, %rax
	movb	$45, -2256(%rbp,%rax)
.L192:
	subl	%r13d, %ebx
	movslq	%r15d, %r15
	leaq	_ZSt7nothrow(%rip), %rsi
	movb	$0, -2256(%rbp,%r15)
	movslq	%ebx, %r14
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L230
.L194:
	movslq	%r13d, %r13
	addq	-2280(%rbp), %r13
	cmpl	$8, %ebx
	jnb	.L195
	testb	$4, %bl
	jne	.L231
	testl	%ebx, %ebx
	je	.L170
	movzbl	0(%r13), %edx
	movb	%dl, (%rax)
	testb	$2, %bl
	jne	.L232
.L170:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L233
	addq	$2248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	0(%r13), %rdx
	leaq	8(%rax), %rdi
	movq	%r13, %rsi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%ebx, %edx
	movq	-8(%r13,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	movq	%rax, %rcx
	subq	%rdi, %rcx
	addl	%ecx, %ebx
	subq	%rcx, %rsi
	shrl	$3, %ebx
	movl	%ebx, %ecx
	rep movsq
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L226:
	movq	%xmm1, %rcx
	pxor	%xmm3, %xmm3
	leaq	-1(%rdx), %rdx
	btrq	$63, %rcx
	movq	%xmm3, %rax
	testq	%rcx, %rcx
	cmovne	%rdx, %rax
	movq	%rax, %xmm3
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L227:
	leaq	-2256(%rbp), %rsi
	movslq	%ebx, %rax
	movq	%rsi, -2280(%rbp)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-2280(%rbp), %rsi
	movl	%r8d, %r15d
	movzbl	(%rsi,%rax), %ecx
.L183:
	movsbl	%cl, %edx
	movslq	%eax, %r8
	leal	-87(%rdx), %r9d
	subl	$48, %edx
	cmpb	$58, %cl
	cmovge	%r9d, %edx
	addl	$1, %edx
	cmpl	%edi, %edx
	jl	.L234
	subq	$1, %rax
	movl	%eax, %r15d
	cmpl	$1100, %eax
	jne	.L184
	addsd	%xmm7, %xmm6
	movl	$1101, %ebx
	movapd	%xmm6, %xmm2
	jmp	.L177
.L228:
	movl	%ecx, %r13d
	jmp	.L189
.L234:
	movslq	%edx, %rdx
	leal	1(%r15), %ebx
	movzbl	(%r12,%rdx), %eax
	movb	%al, -2256(%rbp,%r8)
	jmp	.L177
.L231:
	movl	0(%r13), %edx
	movl	%ebx, %ebx
	movl	%edx, (%rax)
	movl	-4(%r13,%rbx), %edx
	movl	%edx, -4(%rax,%rbx)
	jmp	.L170
.L232:
	movl	%ebx, %ebx
	movzwl	-2(%r13,%rbx), %edx
	movw	%dx, -2(%rax,%rbx)
	jmp	.L170
.L204:
	movl	$1100, %r13d
	jmp	.L189
.L230:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L194
	leaq	.LC18(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L233:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17861:
	.size	_ZN2v88internal20DoubleToRadixCStringEdi, .-_ZN2v88internal20DoubleToRadixCStringEdi
	.section	.text._ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKhEEvT_i,"axG",@progbits,_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKhEEvT_i,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKhEEvT_i
	.type	_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKhEEvT_i, @function
_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKhEEvT_i:
.LFB19706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, 40(%rdi)
	movslq	%edx, %rdx
	leaq	(%rsi,%rdx), %r12
	cmpq	%rsi, %r12
	je	.L236
	movq	%rsi, %r13
	movq	%rsi, %rbx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r15
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L237:
	movslq	%edi, %rdi
	testb	$8, (%r15,%rdi)
	je	.L238
.L240:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	je	.L236
.L239:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L237
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L240
.L238:
	movzbl	(%rbx), %eax
	cmpb	$43, %al
	je	.L279
	cmpb	$45, %al
	je	.L280
	movl	32(%r14), %eax
	testl	%eax, %eax
	jne	.L245
.L283:
	movl	$10, 32(%r14)
	movzbl	(%rbx), %eax
	cmpb	$48, %al
	je	.L281
.L254:
	cmpb	$0, 48(%r14)
	jne	.L256
	leal	-48(%rax), %ecx
	movl	32(%r14), %edx
	cmpl	$9, %ecx
	ja	.L257
	leal	47(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L256
.L257:
	cmpl	$10, %edx
	setg	%cl
	cmpl	$96, %eax
	jle	.L258
	testb	%cl, %cl
	je	.L258
	leal	86(%rdx), %ecx
	cmpl	%ecx, %eax
	jg	.L259
.L256:
	subq	%r13, %rbx
	movl	%ebx, 36(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movl	$3, 52(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	cmpl	$16, %eax
	je	.L282
	.p2align 4,,10
	.p2align 3
.L255:
	movzbl	(%rbx), %eax
	cmpb	$48, %al
	jne	.L254
	addq	$1, %rbx
	movb	$1, 48(%r14)
	cmpq	%r12, %rbx
	jne	.L255
.L252:
	movl	$4, 52(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	addq	$1, %rbx
	cmpq	%r12, %rbx
	je	.L244
	movl	32(%r14), %eax
	movl	$1, 44(%r14)
	testl	%eax, %eax
	jne	.L245
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L280:
	addq	$1, %rbx
	cmpq	%r12, %rbx
	je	.L244
	movl	32(%r14), %eax
	movl	$0, 44(%r14)
	testl	%eax, %eax
	jne	.L245
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L281:
	leaq	1(%rbx), %rdx
	cmpq	%r12, %rdx
	je	.L252
	movzbl	1(%rbx), %eax
	andl	$-33, %eax
	cmpb	$88, %al
	je	.L284
	cmpb	$0, 49(%r14)
	je	.L253
	cmpb	$79, %al
	jne	.L251
	addq	$2, %rbx
	movl	$8, 32(%r14)
	cmpq	%r12, %rbx
	jne	.L255
.L244:
	movl	$2, 52(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movzbl	(%rbx), %eax
	cmpb	$48, %al
	jne	.L254
	leaq	1(%rbx), %rdx
	cmpq	%r12, %rdx
	je	.L252
	movzbl	1(%rbx), %eax
	andl	$-33, %eax
	cmpb	$88, %al
	jne	.L253
	addq	$2, %rbx
	cmpq	%r12, %rbx
	jne	.L255
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L258:
	cmpl	$64, %eax
	jle	.L244
	testb	%cl, %cl
	je	.L244
.L259:
	addl	$54, %edx
	cmpl	%edx, %eax
	jg	.L244
	jmp	.L256
.L284:
	addq	$2, %rbx
	movl	$16, 32(%r14)
	cmpq	%r12, %rbx
	jne	.L255
	jmp	.L244
.L253:
	movb	$1, 48(%r14)
	movq	%rdx, %rbx
	jmp	.L255
.L251:
	cmpb	$66, %al
	jne	.L253
	addq	$2, %rbx
	movl	$2, 32(%r14)
	cmpq	%r12, %rbx
	jne	.L255
	jmp	.L244
	.cfi_endproc
.LFE19706:
	.size	_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKhEEvT_i, .-_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKhEEvT_i
	.section	.text._ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKtEEvT_i,"axG",@progbits,_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKtEEvT_i,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKtEEvT_i
	.type	_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKtEEvT_i, @function
_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKtEEvT_i:
.LFB19708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, 40(%rdi)
	movslq	%edx, %rdx
	leaq	(%rsi,%rdx,2), %r14
	movq	%rsi, -56(%rbp)
	cmpq	%rsi, %r14
	je	.L286
	movq	%rsi, %rbx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r13
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L287:
	movslq	%r12d, %r12
	testb	$8, 0(%r13,%r12)
	je	.L292
.L290:
	addq	$2, %rbx
	cmpq	%rbx, %r14
	je	.L286
.L289:
	movzwl	(%rbx), %r12d
	movl	%r12d, %eax
	cmpw	$127, %r12w
	jbe	.L287
	movl	%r12d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L290
	subl	$8232, %r12d
	cmpl	$1, %r12d
	jbe	.L290
	movzwl	(%rbx), %eax
.L292:
	cmpw	$43, %ax
	je	.L329
	cmpw	$45, %ax
	je	.L330
.L295:
	movl	32(%r15), %eax
	testl	%eax, %eax
	jne	.L297
.L332:
	movl	$10, 32(%r15)
	movzwl	(%rbx), %eax
	cmpw	$48, %ax
	je	.L331
.L306:
	cmpb	$0, 48(%r15)
	jne	.L308
	leal	-48(%rax), %ecx
	movl	32(%r15), %edx
	cmpl	$9, %ecx
	ja	.L309
	leal	47(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L308
.L309:
	cmpl	$10, %edx
	setg	%cl
	cmpl	$96, %eax
	jle	.L310
	testb	%cl, %cl
	je	.L310
	leal	86(%rdx), %ecx
	cmpl	%ecx, %eax
	jg	.L311
.L308:
	subq	-56(%rbp), %rbx
	sarq	%rbx
	movl	%ebx, 36(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	movl	$3, 52(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	addq	$2, %rbx
	cmpq	%r14, %rbx
	je	.L296
	movl	32(%r15), %eax
	movl	$1, 44(%r15)
	testl	%eax, %eax
	je	.L332
.L297:
	cmpl	$16, %eax
	je	.L333
	.p2align 4,,10
	.p2align 3
.L307:
	movzwl	(%rbx), %eax
	cmpw	$48, %ax
	jne	.L306
	addq	$2, %rbx
	movb	$1, 48(%r15)
	cmpq	%r14, %rbx
	jne	.L307
.L304:
	movl	$4, 52(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movzwl	(%rbx), %eax
	cmpw	$48, %ax
	jne	.L306
	leaq	2(%rbx), %rdx
	cmpq	%r14, %rdx
	je	.L304
	movzwl	2(%rbx), %eax
	andl	$-33, %eax
	cmpw	$88, %ax
	jne	.L305
	addq	$4, %rbx
	cmpq	%r14, %rbx
	jne	.L307
.L296:
	movl	$2, 52(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	leaq	2(%rbx), %rdx
	cmpq	%r14, %rdx
	je	.L304
	movzwl	2(%rbx), %eax
	andl	$-33, %eax
	cmpw	$88, %ax
	je	.L334
	cmpb	$0, 49(%r15)
	je	.L305
	cmpw	$79, %ax
	jne	.L303
	addq	$4, %rbx
	movl	$8, 32(%r15)
	cmpq	%r14, %rbx
	jne	.L307
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L330:
	addq	$2, %rbx
	cmpq	%r14, %rbx
	je	.L296
	movl	$0, 44(%r15)
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L310:
	cmpl	$64, %eax
	jle	.L296
	testb	%cl, %cl
	je	.L296
.L311:
	addl	$54, %edx
	cmpl	%edx, %eax
	jg	.L296
	jmp	.L308
.L334:
	addq	$4, %rbx
	movl	$16, 32(%r15)
	cmpq	%r14, %rbx
	jne	.L307
	jmp	.L296
.L305:
	movb	$1, 48(%r15)
	movq	%rdx, %rbx
	jmp	.L307
.L303:
	cmpw	$66, %ax
	jne	.L305
	addq	$4, %rbx
	movl	$2, 32(%r15)
	cmpq	%r14, %rbx
	jne	.L307
	jmp	.L296
	.cfi_endproc
.LFE19708:
	.size	_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKtEEvT_i, .-_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKtEEvT_i
	.section	.rodata._ZN2v88internal20StringToBigIntHelper9GetResultEv.constprop.0.str1.1,"aMS",@progbits,1
.LC19:
	.string	"unreachable code"
	.section	.text._ZN2v88internal20StringToBigIntHelper9GetResultEv.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal20StringToBigIntHelper9GetResultEv.constprop.0, @function
_ZN2v88internal20StringToBigIntHelper9GetResultEv.constprop.0:
.LFB21622:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L417
	movl	40(%rdi), %edx
.L342:
	movq	%rbx, %rdi
	call	_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKhEEvT_i
.L340:
	movl	52(%rbx), %edi
	movl	64(%rbx), %eax
	testl	%edi, %edi
	jne	.L343
	cmpl	$1, %eax
	movl	32(%rbx), %esi
	movq	8(%rbx), %rdi
	movl	$1, %ecx
	sete	%r8b
	movl	40(%rbx), %edx
	subl	36(%rbx), %edx
	call	_ZN2v88internal6BigInt11AllocateForEPNS0_7IsolateEiiNS0_11ShouldThrowENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L344
	movl	52(%rbx), %esi
	movq	%rax, 56(%rbx)
	testl	%esi, %esi
	jne	.L395
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L418
.L347:
	movl	32(%rbx), %r13d
	movslq	40(%rbx), %rax
	movl	$10, %r14d
	movslq	36(%rbx), %rcx
	leaq	(%r12,%rcx), %r15
	addq	%rax, %r12
	cmpl	$10, %r13d
	cmovle	%r13d, %r14d
	leal	48(%r14), %eax
	leal	87(%r13), %r14d
	addl	$55, %r13d
	movl	%eax, -84(%rbp)
.L393:
	xorl	%edx, %edx
	movl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L359:
	movzbl	(%r15), %edi
	cmpb	$47, %dil
	ja	.L419
.L352:
	movq	56(%rbx), %rdi
	call	_ZN2v88internal6BigInt18InplaceMultiplyAddENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEmm@PLT
	cmpb	$0, 50(%rbx)
	jne	.L361
	cmpq	%r15, %r12
	je	.L361
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r13
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L362:
	movslq	%edi, %rdi
	testb	$8, 0(%r13,%rdi)
	je	.L363
.L365:
	addq	$1, %r15
	cmpq	%r15, %r12
	je	.L361
.L360:
	movzbl	(%r15), %edi
	testb	%dil, %dil
	jns	.L362
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L365
.L363:
	movl	64(%rbx), %eax
	movl	$2, 52(%rbx)
	testl	%eax, %eax
	je	.L366
	.p2align 4,,10
	.p2align 3
.L388:
	xorl	%eax, %eax
.L385:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L420
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	movq	(%rdx), %rax
	leaq	-65(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movl	$10, %r14d
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movl	32(%rbx), %r13d
	movslq	36(%rbx), %rdx
	cmpl	$10, %r13d
	leaq	(%rax,%rdx,2), %r12
	movslq	40(%rbx), %rdx
	leal	87(%r13), %r8d
	cmovle	%r13d, %r14d
	addl	$55, %r13d
	leaq	(%rax,%rdx,2), %r15
	addl	$48, %r14d
.L351:
	xorl	%edi, %edi
	movl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L375:
	movzwl	(%r12), %ecx
	cmpw	$47, %cx
	ja	.L421
.L368:
	movl	%edi, %edx
	movq	56(%rbx), %rdi
	call	_ZN2v88internal6BigInt18InplaceMultiplyAddENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEmm@PLT
	cmpb	$0, 50(%rbx)
	je	.L422
	.p2align 4,,10
	.p2align 3
.L377:
	movl	$5, 52(%rbx)
	.p2align 4,,10
	.p2align 3
.L395:
	movl	64(%rbx), %eax
.L343:
	testl	%eax, %eax
	je	.L423
	movl	52(%rbx), %eax
	cmpl	$3, %eax
	je	.L389
.L391:
	cmpl	$2, %eax
	ja	.L387
	testl	%eax, %eax
	jne	.L388
.L389:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L423:
	cmpl	$2, 44(%rbx)
	jne	.L397
.L384:
	movl	52(%rbx), %eax
	cmpl	$3, %eax
	jne	.L391
	movl	$4, 52(%rbx)
.L386:
	movq	8(%rbx), %rdi
	call	_ZN2v88internal6BigInt4ZeroEPNS0_7IsolateE@PLT
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L419:
	movzbl	%dil, %eax
	cmpl	%eax, -84(%rbp)
	jle	.L353
	subl	$48, %eax
.L354:
	movl	32(%rbx), %r8d
	movl	%r8d, %edi
	imull	%esi, %edi
	cmpl	$119304647, %edi
	ja	.L357
	imull	%r8d, %edx
	addq	$1, %r15
	movl	%edi, %esi
	addl	%eax, %edx
	cmpq	%r15, %r12
	jne	.L359
	movq	56(%rbx), %rdi
	call	_ZN2v88internal6BigInt18InplaceMultiplyAddENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEmm@PLT
.L361:
	movl	64(%rbx), %ecx
	movl	$5, 52(%rbx)
	testl	%ecx, %ecx
	jne	.L367
.L366:
	cmpl	$2, 44(%rbx)
	je	.L424
.L397:
	cmpl	$10, 32(%rbx)
	jne	.L388
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L357:
	movq	56(%rbx), %rdi
	call	_ZN2v88internal6BigInt18InplaceMultiplyAddENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEmm@PLT
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L353:
	cmpb	$96, %dil
	jbe	.L355
	cmpl	%eax, %r14d
	jle	.L356
	subl	$87, %eax
	jmp	.L354
.L355:
	cmpb	$64, %dil
	jbe	.L352
.L356:
	cmpl	%eax, %r13d
	jle	.L352
	subl	$55, %eax
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L387:
	cmpl	$4, %eax
	je	.L386
	cmpl	$5, %eax
	jne	.L389
.L367:
	movl	44(%rbx), %edx
	xorl	%esi, %esi
	movq	56(%rbx), %rdi
	testl	%edx, %edx
	sete	%sil
	call	_ZN2v88internal6BigInt8FinalizeENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEb@PLT
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L417:
	movq	16(%rdi), %rdx
	movq	(%rdx), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L337
.L425:
	cmpw	$8, %ax
	je	.L338
	movq	15(%rcx), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L425
.L337:
	movq	(%rdx), %rax
	leaq	-65(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKtEEvT_i
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L338:
	movq	(%rdx), %rax
	leaq	-65(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%rax, %rsi
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L344:
	movl	$1, 52(%rbx)
	movl	64(%rbx), %eax
	movq	$0, 56(%rbx)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L418:
	movq	16(%rbx), %rdx
	movq	(%rdx), %rcx
.L350:
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L348
	cmpw	$8, %ax
	je	.L349
	movq	15(%rcx), %rcx
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L349:
	movq	(%rdx), %rax
	leaq	-65(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%rax, %r12
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L424:
	movl	52(%rbx), %eax
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L421:
	movzwl	%cx, %eax
	cmpl	%eax, %r14d
	jle	.L369
	subl	$48, %eax
.L370:
	movl	32(%rbx), %edx
	movl	%edx, %ecx
	imull	%esi, %ecx
	cmpl	$119304647, %ecx
	ja	.L373
	imull	%edx, %edi
	addq	$2, %r12
	addl	%eax, %edi
	cmpq	%r12, %r15
	je	.L374
	movl	%ecx, %esi
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L369:
	cmpw	$96, %cx
	jbe	.L371
	cmpl	%eax, %r8d
	jle	.L372
	subl	$87, %eax
	jmp	.L370
.L371:
	cmpw	$64, %cx
	jbe	.L368
.L372:
	cmpl	%eax, %r13d
	jle	.L368
	subl	$55, %eax
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L373:
	movl	%edi, %edx
	movq	56(%rbx), %rdi
	movl	%r8d, -84(%rbp)
	call	_ZN2v88internal6BigInt18InplaceMultiplyAddENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEmm@PLT
	movl	-84(%rbp), %r8d
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L374:
	movl	%edi, %edx
	movq	56(%rbx), %rdi
	movl	%ecx, %esi
	call	_ZN2v88internal6BigInt18InplaceMultiplyAddENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEmm@PLT
	jmp	.L377
.L422:
	cmpq	%r12, %r15
	je	.L377
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L378:
	movslq	%r13d, %r13
	testb	$8, (%r14,%r13)
	je	.L382
.L381:
	addq	$2, %r12
	cmpq	%r12, %r15
	je	.L377
.L376:
	movzwl	(%r12), %r13d
	cmpw	$127, %r13w
	jbe	.L378
	movl	%r13d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L381
	subl	$8232, %r13d
	cmpl	$1, %r13d
	jbe	.L381
.L382:
	movl	$2, 52(%rbx)
	jmp	.L395
.L420:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21622:
	.size	_ZN2v88internal20StringToBigIntHelper9GetResultEv.constprop.0, .-_ZN2v88internal20StringToBigIntHelper9GetResultEv.constprop.0
	.section	.text._ZN2v88internal14StringToBigIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14StringToBigIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal14StringToBigIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal14StringToBigIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEE:
.LFB17849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, %r12
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L444
.L428:
	movq	-1(%r12), %rax
	cmpw	$63, 11(%rax)
	jbe	.L445
.L436:
	movdqa	.LC20(%rip), %xmm0
	leaq	16+_ZTVN2v88internal20StringToBigIntHelperE(%rip), %rax
	leaq	-96(%rbp), %rdi
	movq	%rbx, -88(%rbp)
	movq	%rax, -96(%rbp)
	movl	$1, %eax
	movq	%rsi, -80(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -48(%rbp)
	movl	$0, -44(%rbp)
	movq	$0, -40(%rbp)
	movl	$0, -32(%rbp)
	movw	%ax, -47(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal20StringToBigIntHelper9GetResultEv.constprop.0
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L446
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	movq	-1(%r12), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L436
	movq	(%rsi), %rax
	movq	41112(%rbx), %rdi
	movq	15(%rax), %r12
	testq	%rdi, %rdi
	je	.L439
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L444:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L428
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L431
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L431
	xorl	%edx, %edx
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %rsi
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L439:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L447
.L441:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L431:
	movq	41112(%rbx), %rdi
	movq	15(%rax), %r12
	testq	%rdi, %rdi
	je	.L433
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %rsi
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L433:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L448
.L435:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L428
.L448:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L447:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L441
.L446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17849:
	.size	_ZN2v88internal14StringToBigIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal14StringToBigIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal13BigIntLiteralEPNS0_7IsolateEPKc,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal13BigIntLiteralEPNS0_7IsolateEPKc
	.type	_ZN2v88internal13BigIntLiteralEPNS0_7IsolateEPKc, @function
_ZN2v88internal13BigIntLiteralEPNS0_7IsolateEPKc:
.LFB17850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-96(%rbp), %rdi
	movq	%r12, -88(%rbp)
	movl	%eax, -56(%rbp)
	leaq	16+_ZTVN2v88internal20StringToBigIntHelperE(%rip), %rax
	movq	$0, -80(%rbp)
	movq	%rbx, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$2, -52(%rbp)
	movb	$0, -48(%rbp)
	movb	$1, -46(%rbp)
	movl	$0, -44(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -40(%rbp)
	movl	$1, -32(%rbp)
	movb	$1, -47(%rbp)
	call	_ZN2v88internal20StringToBigIntHelper9GetResultEv.constprop.0
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L452
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L452:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17850:
	.size	_ZN2v88internal13BigIntLiteralEPNS0_7IsolateEPKc, .-_ZN2v88internal13BigIntLiteralEPNS0_7IsolateEPKc
	.section	.text._ZN2v88internal17StringToIntHelper13ParseInternalIPKhEEvT_,"axG",@progbits,_ZN2v88internal17StringToIntHelper13ParseInternalIPKhEEvT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17StringToIntHelper13ParseInternalIPKhEEvT_
	.type	_ZN2v88internal17StringToIntHelper13ParseInternalIPKhEEvT_, @function
_ZN2v88internal17StringToIntHelper13ParseInternalIPKhEEvT_:
.LFB19709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$10, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	32(%rdi), %r14d
	movslq	36(%rdi), %rbx
	movslq	40(%rdi), %r13
	addq	%rsi, %rbx
	leal	87(%r14), %r8d
	addq	%rsi, %r13
	cmpl	$10, %r14d
	cmovle	%r14d, %r15d
	addl	$55, %r14d
	addl	$48, %r15d
.L470:
	movl	$1, %r10d
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L461:
	movzbl	(%rbx), %ecx
	cmpb	$47, %cl
	ja	.L477
.L454:
	movq	(%r12), %rax
	movl	%r9d, %edx
	movl	%r10d, %esi
	movq	%r12, %rdi
	call	*24(%rax)
	cmpb	$0, 50(%r12)
	jne	.L463
	cmpq	%rbx, %r13
	je	.L463
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L464:
	movslq	%edi, %rdi
	testb	$8, (%r14,%rdi)
	je	.L465
.L467:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L463
.L462:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L464
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L467
.L465:
	movl	$2, 52(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	movzbl	%cl, %eax
	cmpl	%r15d, %eax
	jge	.L455
	subl	$48, %eax
.L456:
	movl	32(%r12), %edx
	movl	%edx, %esi
	imull	%r10d, %esi
	cmpl	$119304647, %esi
	ja	.L459
	imull	%r9d, %edx
	addq	$1, %rbx
	leal	(%rdx,%rax), %r9d
	cmpq	%r13, %rbx
	je	.L460
	movl	%esi, %r10d
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L455:
	cmpb	$96, %cl
	jbe	.L457
	cmpl	%r8d, %eax
	jge	.L458
	subl	$87, %eax
	jmp	.L456
.L457:
	cmpb	$64, %cl
	jbe	.L454
.L458:
	cmpl	%eax, %r14d
	jle	.L454
	subl	$55, %eax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L459:
	movq	(%r12), %rax
	movl	%r8d, -52(%rbp)
	movl	%r9d, %edx
	movl	%r10d, %esi
	movq	%r12, %rdi
	call	*24(%rax)
	movl	-52(%rbp), %r8d
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L460:
	movq	(%r12), %rax
	movl	%r9d, %edx
	movq	%r12, %rdi
	call	*24(%rax)
.L463:
	movl	$5, 52(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19709:
	.size	_ZN2v88internal17StringToIntHelper13ParseInternalIPKhEEvT_, .-_ZN2v88internal17StringToIntHelper13ParseInternalIPKhEEvT_
	.section	.text._ZN2v88internal17StringToIntHelper13ParseInternalIPKtEEvT_,"axG",@progbits,_ZN2v88internal17StringToIntHelper13ParseInternalIPKtEEvT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17StringToIntHelper13ParseInternalIPKtEEvT_
	.type	_ZN2v88internal17StringToIntHelper13ParseInternalIPKtEEvT_, @function
_ZN2v88internal17StringToIntHelper13ParseInternalIPKtEEvT_:
.LFB19710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$10, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	32(%rdi), %r14d
	movslq	36(%rdi), %rax
	cmpl	$10, %r14d
	leaq	(%rsi,%rax,2), %rbx
	movslq	40(%rdi), %rax
	leal	87(%r14), %r8d
	cmovle	%r14d, %r15d
	addl	$55, %r14d
	leaq	(%rsi,%rax,2), %r13
	addl	$48, %r15d
.L496:
	movl	$1, %r10d
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L486:
	movzwl	(%rbx), %ecx
	cmpw	$47, %cx
	ja	.L500
.L479:
	movq	(%r12), %rax
	movl	%r9d, %edx
	movl	%r10d, %esi
	movq	%r12, %rdi
	call	*24(%rax)
	cmpb	$0, 50(%r12)
	jne	.L488
	cmpq	%rbx, %r13
	je	.L488
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r15
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L489:
	movslq	%r14d, %r14
	testb	$8, (%r15,%r14)
	je	.L493
.L492:
	addq	$2, %rbx
	cmpq	%rbx, %r13
	je	.L488
.L487:
	movzwl	(%rbx), %r14d
	cmpw	$127, %r14w
	jbe	.L489
	movl	%r14d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L492
	subl	$8232, %r14d
	cmpl	$1, %r14d
	jbe	.L492
.L493:
	movl	$2, 52(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L500:
	.cfi_restore_state
	movzwl	%cx, %eax
	cmpl	%r15d, %eax
	jge	.L480
	subl	$48, %eax
.L481:
	movl	32(%r12), %edx
	movl	%edx, %esi
	imull	%r10d, %esi
	cmpl	$119304647, %esi
	ja	.L484
	imull	%r9d, %edx
	addq	$2, %rbx
	leal	(%rdx,%rax), %r9d
	cmpq	%r13, %rbx
	je	.L485
	movl	%esi, %r10d
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L480:
	cmpw	$96, %cx
	jbe	.L482
	cmpl	%r8d, %eax
	jge	.L483
	subl	$87, %eax
	jmp	.L481
.L482:
	cmpw	$64, %cx
	jbe	.L479
.L483:
	cmpl	%eax, %r14d
	jle	.L479
	subl	$55, %eax
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L484:
	movq	(%r12), %rax
	movl	%r8d, -52(%rbp)
	movl	%r9d, %edx
	movl	%r10d, %esi
	movq	%r12, %rdi
	call	*24(%rax)
	movl	-52(%rbp), %r8d
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L485:
	movq	(%r12), %rax
	movl	%r9d, %edx
	movq	%r12, %rdi
	call	*24(%rax)
.L488:
	movl	$5, 52(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19710:
	.size	_ZN2v88internal17StringToIntHelper13ParseInternalIPKtEEvT_, .-_ZN2v88internal17StringToIntHelper13ParseInternalIPKtEEvT_
	.section	.text._ZN2v88internal17StringToIntHelper8ParseIntEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17StringToIntHelper8ParseIntEv
	.type	_ZN2v88internal17StringToIntHelper8ParseIntEv, @function
_ZN2v88internal17StringToIntHelper8ParseIntEv:
.LFB17812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L518
	movl	40(%rdi), %edx
.L508:
	movq	%r12, %rdi
	call	_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKhEEvT_i
.L506:
	movl	52(%r12), %edx
	testl	%edx, %edx
	jne	.L501
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*32(%rax)
	movl	52(%r12), %eax
	testl	%eax, %eax
	jne	.L501
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L519
.L512:
	movq	%r12, %rdi
	call	_ZN2v88internal17StringToIntHelper13ParseInternalIPKhEEvT_
.L501:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L520
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	movq	16(%rdi), %rdx
	movq	(%rdx), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L503
.L521:
	cmpw	$8, %ax
	je	.L504
	movq	15(%rcx), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L521
.L503:
	movq	(%rdx), %rax
	leaq	-33(%rbp), %rsi
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKtEEvT_i
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L504:
	movq	(%rdx), %rax
	leaq	-33(%rbp), %rsi
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%rax, %rsi
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L519:
	movq	16(%r12), %rdx
	movq	(%rdx), %rcx
.L515:
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L513
	cmpw	$8, %ax
	je	.L514
	movq	15(%rcx), %rcx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L514:
	movq	(%rdx), %rax
	leaq	-33(%rbp), %rsi
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%rax, %rsi
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L513:
	movq	(%rdx), %rax
	leaq	-33(%rbp), %rsi
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17StringToIntHelper13ParseInternalIPKtEEvT_
	jmp	.L501
.L520:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17812:
	.size	_ZN2v88internal17StringToIntHelper8ParseIntEv, .-_ZN2v88internal17StringToIntHelper8ParseIntEv
	.section	.text._ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKhEEdT_,"axG",@progbits,_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKhEEdT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKhEEdT_
	.type	_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKhEEdT_, @function
_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKhEEdT_:
.LFB19712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$320, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movslq	36(%rdi), %rax
	movslq	40(%rdi), %rdi
	addq	%rsi, %rax
	addq	%rsi, %rdi
	xorl	%esi, %esi
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L529:
	cmpl	$309, %esi
	jg	.L524
	movslq	%esi, %rcx
	addl	$1, %esi
	movb	%dl, -320(%rbp,%rcx)
.L524:
	addq	$1, %rax
	cmpq	%rax, %rdi
	je	.L523
.L525:
	movzbl	(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L529
.L523:
	movslq	%esi, %rsi
	xorl	%edx, %edx
	leaq	-320(%rbp), %rdi
	movb	$0, -320(%rbp,%rsi)
	call	_ZN2v88internal6StrtodENS0_6VectorIKcEEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L530
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L530:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19712:
	.size	_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKhEEdT_, .-_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKhEEdT_
	.section	.text._ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKtEEdT_,"axG",@progbits,_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKtEEdT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKtEEdT_
	.type	_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKtEEdT_, @function
_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKtEEdT_:
.LFB19714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$320, %rsp
	movslq	40(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movslq	36(%rdi), %rax
	leaq	(%rsi,%rdx,2), %rdi
	leaq	(%rsi,%rax,2), %rax
	xorl	%esi, %esi
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L538:
	cmpl	$309, %esi
	jg	.L533
	movslq	%esi, %rcx
	addl	$1, %esi
	movb	%dl, -320(%rbp,%rcx)
.L533:
	addq	$2, %rax
	cmpq	%rax, %rdi
	je	.L532
.L534:
	movzwl	(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpw	$9, %cx
	jbe	.L538
.L532:
	movslq	%esi, %rsi
	xorl	%edx, %edx
	leaq	-320(%rbp), %rdi
	movb	$0, -320(%rbp,%rsi)
	call	_ZN2v88internal6StrtodENS0_6VectorIKcEEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L539
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L539:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19714:
	.size	_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKtEEdT_, .-_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKtEEdT_
	.section	.text._ZN2v88internal25InternalStringToIntDoubleILi1EPKhS3_EEdT0_T1_bb,"axG",@progbits,_ZN2v88internal25InternalStringToIntDoubleILi1EPKhS3_EEdT0_T1_bb,comdat
	.p2align 4
	.weak	_ZN2v88internal25InternalStringToIntDoubleILi1EPKhS3_EEdT0_T1_bb
	.type	_ZN2v88internal25InternalStringToIntDoubleILi1EPKhS3_EEdT0_T1_bb, @function
_ZN2v88internal25InternalStringToIntDoubleILi1EPKhS3_EEdT0_T1_bb:
.LFB20726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	movl	%ecx, %edx
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L595:
	addq	$1, %rbx
	cmpq	%r13, %rbx
	je	.L594
.L542:
	movzbl	(%rbx), %eax
	cmpb	$48, %al
	je	.L595
	xorl	%r12d, %r12d
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L597:
	subl	$48, %eax
	addq	$1, %rbx
	cltq
	leaq	(%rax,%r12,2), %r12
	movq	%r12, %rax
	sarq	$53, %rax
	jne	.L596
	cmpq	%r13, %rbx
	je	.L547
	movzbl	(%rbx), %eax
.L541:
	leal	-48(%rax), %ecx
	cmpb	$1, %cl
	jbe	.L597
	testb	%dl, %dl
	je	.L598
.L547:
	testb	%r15b, %r15b
	je	.L566
	testq	%r12, %r12
	je	.L567
	negq	%r12
.L566:
	addq	$24, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	cvtsi2sdq	%r12, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L594:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	testb	%r15b, %r15b
	je	.L540
.L567:
	movsd	.LC21(%rip), %xmm0
.L540:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L598:
	.cfi_restore_state
	cmpq	%rbx, %r13
	je	.L547
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L548:
	movslq	%edi, %rdi
	testb	$8, (%r14,%rdi)
	je	.L551
.L549:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L547
.L550:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L548
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L549
.L551:
	movsd	.LC22(%rip), %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L596:
	.cfi_restore_state
	movl	%eax, %esi
	cmpq	$1, %rax
	je	.L573
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L553:
	sarl	%esi
	movl	%r8d, %r9d
	addl	$1, %r8d
	cmpl	$1, %esi
	jne	.L553
	movl	%r8d, %ecx
	sall	%cl, %esi
	leal	-1(%rsi), %r14d
.L552:
	andl	%r12d, %r14d
	movl	%r8d, %ecx
	movb	$1, -61(%rbp)
	movl	%r14d, -60(%rbp)
	sarq	%cl, %r12
	cmpq	%r13, %rbx
	je	.L556
.L558:
	movzbl	(%rbx), %eax
	leal	-48(%rax), %ecx
	cmpb	$1, %cl
	jbe	.L555
	testb	%dl, %dl
	je	.L599
.L556:
	movl	$1, %eax
	movl	%r9d, %ecx
	sall	%cl, %eax
	cmpl	%eax, -60(%rbp)
	jg	.L575
	je	.L600
.L563:
	btq	$53, %r12
	jnc	.L565
	addl	$1, %r8d
	sarq	%r12
.L565:
	testb	%r15b, %r15b
	je	.L601
	negq	%r12
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
.L568:
	addq	$24, %rsp
	movl	%r8d, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ldexp@PLT
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
	jmp	.L568
.L600:
	testb	$1, %r12b
	jne	.L575
	cmpb	$0, -61(%rbp)
	jne	.L563
.L575:
	addq	$1, %r12
	jmp	.L563
.L599:
	cmpq	%rbx, %r13
	je	.L556
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L559:
	movslq	%edi, %rdi
	testb	$8, (%r14,%rdi)
	je	.L551
.L561:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L556
.L560:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L559
	movl	%r8d, -56(%rbp)
	movl	%r9d, -52(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movl	-52(%rbp), %r9d
	movl	-56(%rbp), %r8d
	testb	%al, %al
	je	.L551
	jmp	.L561
.L555:
	cmpb	$48, %al
	sete	%al
	addq	$1, %rbx
	andb	%al, -61(%rbp)
	addl	$1, %r8d
	cmpq	%r13, %rbx
	jne	.L558
	jmp	.L556
.L573:
	xorl	%r9d, %r9d
	movl	$1, %r14d
	movl	$1, %r8d
	jmp	.L552
	.cfi_endproc
.LFE20726:
	.size	_ZN2v88internal25InternalStringToIntDoubleILi1EPKhS3_EEdT0_T1_bb, .-_ZN2v88internal25InternalStringToIntDoubleILi1EPKhS3_EEdT0_T1_bb
	.section	.text._ZN2v88internal25InternalStringToIntDoubleILi2EPKhS3_EEdT0_T1_bb,"axG",@progbits,_ZN2v88internal25InternalStringToIntDoubleILi2EPKhS3_EEdT0_T1_bb,comdat
	.p2align 4
	.weak	_ZN2v88internal25InternalStringToIntDoubleILi2EPKhS3_EEdT0_T1_bb
	.type	_ZN2v88internal25InternalStringToIntDoubleILi2EPKhS3_EEdT0_T1_bb, @function
_ZN2v88internal25InternalStringToIntDoubleILi2EPKhS3_EEdT0_T1_bb:
.LFB20727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	movl	%ecx, %edx
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L657:
	addq	$1, %rbx
	cmpq	%r13, %rbx
	je	.L656
.L604:
	movzbl	(%rbx), %eax
	cmpb	$48, %al
	je	.L657
	xorl	%r12d, %r12d
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L659:
	subl	$48, %eax
	addq	$1, %rbx
	cltq
	leaq	(%rax,%r12,4), %r12
	movq	%r12, %rax
	sarq	$53, %rax
	jne	.L658
	cmpq	%r13, %rbx
	je	.L609
	movzbl	(%rbx), %eax
.L603:
	leal	-48(%rax), %ecx
	cmpb	$3, %cl
	jbe	.L659
	testb	%dl, %dl
	je	.L660
.L609:
	testb	%r15b, %r15b
	je	.L628
	testq	%r12, %r12
	je	.L629
	negq	%r12
.L628:
	addq	$24, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	cvtsi2sdq	%r12, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	testb	%r15b, %r15b
	je	.L602
.L629:
	movsd	.LC21(%rip), %xmm0
.L602:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L660:
	.cfi_restore_state
	cmpq	%rbx, %r13
	je	.L609
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L610:
	movslq	%edi, %rdi
	testb	$8, (%r14,%rdi)
	je	.L613
.L611:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L609
.L612:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L610
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L611
.L613:
	movsd	.LC22(%rip), %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L658:
	.cfi_restore_state
	movl	%eax, %esi
	cmpq	$1, %rax
	je	.L635
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L615:
	sarl	%esi
	movl	%r8d, %r9d
	addl	$1, %r8d
	cmpl	$1, %esi
	jne	.L615
	movl	%r8d, %ecx
	sall	%cl, %esi
	leal	-1(%rsi), %r14d
.L614:
	andl	%r12d, %r14d
	movl	%r8d, %ecx
	movb	$1, -61(%rbp)
	movl	%r14d, -60(%rbp)
	sarq	%cl, %r12
	cmpq	%r13, %rbx
	je	.L618
.L620:
	movzbl	(%rbx), %eax
	movq	%rbx, %r14
	leal	-48(%rax), %edi
	cmpb	$3, %dil
	jbe	.L617
	testb	%dl, %dl
	je	.L661
.L618:
	movl	$1, %eax
	movl	%r9d, %ecx
	sall	%cl, %eax
	cmpl	%eax, -60(%rbp)
	jg	.L637
	je	.L662
.L625:
	btq	$53, %r12
	jnc	.L627
	addl	$1, %r8d
	sarq	%r12
.L627:
	testb	%r15b, %r15b
	je	.L663
	negq	%r12
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
.L630:
	addq	$24, %rsp
	movl	%r8d, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ldexp@PLT
	.p2align 4,,10
	.p2align 3
.L663:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
	jmp	.L630
.L662:
	testb	$1, %r12b
	jne	.L637
	cmpb	$0, -61(%rbp)
	jne	.L625
.L637:
	addq	$1, %r12
	jmp	.L625
.L661:
	cmpq	%rbx, %r13
	je	.L618
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rbx
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L621:
	movslq	%edi, %rdi
	testb	$8, (%rbx,%rdi)
	je	.L613
.L623:
	addq	$1, %r14
	cmpq	%r14, %r13
	je	.L618
.L622:
	movzbl	(%r14), %edi
	testb	%dil, %dil
	jns	.L621
	movl	%r8d, -56(%rbp)
	movl	%r9d, -52(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movl	-52(%rbp), %r9d
	movl	-56(%rbp), %r8d
	testb	%al, %al
	je	.L613
	jmp	.L623
.L617:
	cmpb	$48, %al
	sete	%al
	addq	$1, %rbx
	andb	%al, -61(%rbp)
	addl	$2, %r8d
	cmpq	%rbx, %r13
	jne	.L620
	jmp	.L618
.L635:
	xorl	%r9d, %r9d
	movl	$1, %r14d
	movl	$1, %r8d
	jmp	.L614
	.cfi_endproc
.LFE20727:
	.size	_ZN2v88internal25InternalStringToIntDoubleILi2EPKhS3_EEdT0_T1_bb, .-_ZN2v88internal25InternalStringToIntDoubleILi2EPKhS3_EEdT0_T1_bb
	.section	.text._ZN2v88internal25InternalStringToIntDoubleILi3EPKhS3_EEdT0_T1_bb,"axG",@progbits,_ZN2v88internal25InternalStringToIntDoubleILi3EPKhS3_EEdT0_T1_bb,comdat
	.p2align 4
	.weak	_ZN2v88internal25InternalStringToIntDoubleILi3EPKhS3_EEdT0_T1_bb
	.type	_ZN2v88internal25InternalStringToIntDoubleILi3EPKhS3_EEdT0_T1_bb, @function
_ZN2v88internal25InternalStringToIntDoubleILi3EPKhS3_EEdT0_T1_bb:
.LFB20728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	movl	%ecx, %edx
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L719:
	addq	$1, %rbx
	cmpq	%r13, %rbx
	je	.L718
.L666:
	movzbl	(%rbx), %eax
	cmpb	$48, %al
	je	.L719
	xorl	%r12d, %r12d
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L721:
	subl	$48, %eax
	addq	$1, %rbx
	cltq
	leaq	(%rax,%r12,8), %r12
	movq	%r12, %rax
	sarq	$53, %rax
	jne	.L720
	cmpq	%r13, %rbx
	je	.L671
	movzbl	(%rbx), %eax
.L665:
	leal	-48(%rax), %ecx
	cmpb	$7, %cl
	jbe	.L721
	testb	%dl, %dl
	je	.L722
.L671:
	testb	%r15b, %r15b
	je	.L690
	testq	%r12, %r12
	je	.L691
	negq	%r12
.L690:
	addq	$24, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	cvtsi2sdq	%r12, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L718:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	testb	%r15b, %r15b
	je	.L664
.L691:
	movsd	.LC21(%rip), %xmm0
.L664:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L722:
	.cfi_restore_state
	cmpq	%rbx, %r13
	je	.L671
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L672:
	movslq	%edi, %rdi
	testb	$8, (%r14,%rdi)
	je	.L675
.L673:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L671
.L674:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L672
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L673
.L675:
	movsd	.LC22(%rip), %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L720:
	.cfi_restore_state
	movl	%eax, %esi
	cmpq	$1, %rax
	je	.L697
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L677:
	sarl	%esi
	movl	%r8d, %r9d
	addl	$1, %r8d
	cmpl	$1, %esi
	jne	.L677
	movl	%r8d, %ecx
	sall	%cl, %esi
	leal	-1(%rsi), %r14d
.L676:
	andl	%r12d, %r14d
	movl	%r8d, %ecx
	movb	$1, -61(%rbp)
	movl	%r14d, -60(%rbp)
	sarq	%cl, %r12
	cmpq	%r13, %rbx
	je	.L680
.L682:
	movzbl	(%rbx), %eax
	movq	%rbx, %r14
	leal	-48(%rax), %edi
	cmpb	$7, %dil
	jbe	.L679
	testb	%dl, %dl
	je	.L723
.L680:
	movl	$1, %eax
	movl	%r9d, %ecx
	sall	%cl, %eax
	cmpl	%eax, -60(%rbp)
	jg	.L699
	je	.L724
.L687:
	btq	$53, %r12
	jnc	.L689
	addl	$1, %r8d
	sarq	%r12
.L689:
	testb	%r15b, %r15b
	je	.L725
	negq	%r12
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
.L692:
	addq	$24, %rsp
	movl	%r8d, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ldexp@PLT
	.p2align 4,,10
	.p2align 3
.L725:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
	jmp	.L692
.L724:
	testb	$1, %r12b
	jne	.L699
	cmpb	$0, -61(%rbp)
	jne	.L687
.L699:
	addq	$1, %r12
	jmp	.L687
.L723:
	cmpq	%rbx, %r13
	je	.L680
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rbx
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L683:
	movslq	%edi, %rdi
	testb	$8, (%rbx,%rdi)
	je	.L675
.L685:
	addq	$1, %r14
	cmpq	%r14, %r13
	je	.L680
.L684:
	movzbl	(%r14), %edi
	testb	%dil, %dil
	jns	.L683
	movl	%r8d, -56(%rbp)
	movl	%r9d, -52(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movl	-52(%rbp), %r9d
	movl	-56(%rbp), %r8d
	testb	%al, %al
	je	.L675
	jmp	.L685
.L679:
	cmpb	$48, %al
	sete	%al
	addq	$1, %rbx
	andb	%al, -61(%rbp)
	addl	$3, %r8d
	cmpq	%rbx, %r13
	jne	.L682
	jmp	.L680
.L697:
	xorl	%r9d, %r9d
	movl	$1, %r14d
	movl	$1, %r8d
	jmp	.L676
	.cfi_endproc
.LFE20728:
	.size	_ZN2v88internal25InternalStringToIntDoubleILi3EPKhS3_EEdT0_T1_bb, .-_ZN2v88internal25InternalStringToIntDoubleILi3EPKhS3_EEdT0_T1_bb
	.section	.text._ZN2v88internal25InternalStringToIntDoubleILi4EPKhS3_EEdT0_T1_bb,"axG",@progbits,_ZN2v88internal25InternalStringToIntDoubleILi4EPKhS3_EEdT0_T1_bb,comdat
	.p2align 4
	.weak	_ZN2v88internal25InternalStringToIntDoubleILi4EPKhS3_EEdT0_T1_bb
	.type	_ZN2v88internal25InternalStringToIntDoubleILi4EPKhS3_EEdT0_T1_bb, @function
_ZN2v88internal25InternalStringToIntDoubleILi4EPKhS3_EEdT0_T1_bb:
.LFB20729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	%ecx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L784:
	addq	$1, %rbx
	cmpq	%r13, %rbx
	je	.L783
.L728:
	movzbl	(%rbx), %eax
	cmpb	$48, %al
	je	.L784
	xorl	%r12d, %r12d
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L730:
	leal	-65(%rax), %ecx
	cmpb	$5, %cl
	ja	.L733
	leal	-55(%rax), %ecx
.L732:
	salq	$4, %r12
	movslq	%ecx, %rax
	addq	$1, %rbx
	addq	%rax, %r12
	movq	%r12, %rax
	sarq	$53, %rax
	jne	.L785
	cmpq	%rbx, %r13
	je	.L734
	movzbl	(%rbx), %eax
.L727:
	cmpb	$47, %al
	jbe	.L730
	leal	-48(%rax), %ecx
	cmpb	$57, %al
	jbe	.L732
	cmpb	$96, %al
	jbe	.L730
	cmpb	$102, %al
	jbe	.L786
.L733:
	testb	%dl, %dl
	je	.L787
.L734:
	testb	%r14b, %r14b
	je	.L756
	testq	%r12, %r12
	je	.L757
	negq	%r12
.L756:
	addq	$24, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	cvtsi2sdq	%r12, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L786:
	.cfi_restore_state
	leal	-87(%rax), %ecx
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L783:
	pxor	%xmm0, %xmm0
	testb	%r14b, %r14b
	je	.L726
.L757:
	movsd	.LC21(%rip), %xmm0
.L726:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L787:
	.cfi_restore_state
	cmpq	%rbx, %r13
	je	.L734
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r15
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L735:
	movslq	%edi, %rdi
	testb	$8, (%r15,%rdi)
	je	.L738
.L736:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L734
.L737:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L735
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L736
.L738:
	movsd	.LC22(%rip), %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L785:
	.cfi_restore_state
	movl	%eax, %esi
	cmpq	$1, %rax
	je	.L763
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L741:
	sarl	%esi
	movl	%r8d, %r9d
	addl	$1, %r8d
	cmpl	$1, %esi
	jne	.L741
	movl	%r8d, %ecx
	sall	%cl, %esi
	leal	-1(%rsi), %r15d
.L740:
	andl	%r12d, %r15d
	movl	%r8d, %ecx
	movl	%r15d, -60(%rbp)
	sarq	%cl, %r12
	cmpq	%rbx, %r13
	je	.L764
	movq	%rbx, %rdi
	movl	$1, %r15d
.L748:
	movzbl	(%rdi), %ecx
	movq	%rdi, %rbx
	leal	-48(%rcx), %r10d
	movl	%ecx, %eax
	cmpb	$9, %r10b
	jbe	.L743
	cmpl	$96, %ecx
	jle	.L744
	cmpl	$102, %ecx
	jle	.L743
.L745:
	testb	%dl, %dl
	je	.L788
.L746:
	movl	$1, %eax
	movl	%r9d, %ecx
	sall	%cl, %eax
	cmpl	%eax, -60(%rbp)
	jg	.L765
	je	.L789
.L753:
	btq	$53, %r12
	jnc	.L755
	addl	$1, %r8d
	sarq	%r12
.L755:
	testb	%r14b, %r14b
	je	.L790
	negq	%r12
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
.L758:
	addq	$24, %rsp
	movl	%r8d, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ldexp@PLT
	.p2align 4,,10
	.p2align 3
.L744:
	.cfi_restore_state
	leal	-65(%rcx), %ecx
	cmpb	$5, %cl
	ja	.L745
	.p2align 4,,10
	.p2align 3
.L743:
	cmpb	$48, %al
	sete	%al
	addq	$1, %rdi
	addl	$4, %r8d
	andl	%eax, %r15d
	cmpq	%rdi, %r13
	jne	.L748
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L790:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
	jmp	.L758
.L789:
	testb	$1, %r12b
	jne	.L765
	testb	%r15b, %r15b
	jne	.L753
.L765:
	addq	$1, %r12
	jmp	.L753
.L788:
	cmpq	%rdi, %r13
	je	.L746
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L749:
	movslq	%edi, %rdi
	testb	$8, (%rdx,%rdi)
	je	.L738
.L751:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L746
.L750:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L749
	movl	%r8d, -56(%rbp)
	movl	%r9d, -52(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movl	-52(%rbp), %r9d
	movl	-56(%rbp), %r8d
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	je	.L738
	jmp	.L751
.L763:
	xorl	%r9d, %r9d
	movl	$1, %r15d
	movl	$1, %r8d
	jmp	.L740
.L764:
	movl	$1, %r15d
	jmp	.L746
	.cfi_endproc
.LFE20729:
	.size	_ZN2v88internal25InternalStringToIntDoubleILi4EPKhS3_EEdT0_T1_bb, .-_ZN2v88internal25InternalStringToIntDoubleILi4EPKhS3_EEdT0_T1_bb
	.section	.text._ZN2v88internal25InternalStringToIntDoubleILi5EPKhS3_EEdT0_T1_bb,"axG",@progbits,_ZN2v88internal25InternalStringToIntDoubleILi5EPKhS3_EEdT0_T1_bb,comdat
	.p2align 4
	.weak	_ZN2v88internal25InternalStringToIntDoubleILi5EPKhS3_EEdT0_T1_bb
	.type	_ZN2v88internal25InternalStringToIntDoubleILi5EPKhS3_EEdT0_T1_bb, @function
_ZN2v88internal25InternalStringToIntDoubleILi5EPKhS3_EEdT0_T1_bb:
.LFB20730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	%ecx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L849:
	addq	$1, %rbx
	cmpq	%r13, %rbx
	je	.L848
.L793:
	movzbl	(%rbx), %eax
	cmpb	$48, %al
	je	.L849
	xorl	%r12d, %r12d
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L795:
	leal	-65(%rax), %ecx
	cmpb	$21, %cl
	ja	.L798
	leal	-55(%rax), %ecx
.L797:
	salq	$5, %r12
	movslq	%ecx, %rax
	addq	$1, %rbx
	addq	%rax, %r12
	movq	%r12, %rax
	sarq	$53, %rax
	jne	.L850
	cmpq	%rbx, %r13
	je	.L799
	movzbl	(%rbx), %eax
.L792:
	cmpb	$47, %al
	jbe	.L795
	leal	-48(%rax), %ecx
	cmpb	$57, %al
	jbe	.L797
	cmpb	$96, %al
	jbe	.L795
	cmpb	$118, %al
	jbe	.L851
.L798:
	testb	%dl, %dl
	je	.L852
.L799:
	testb	%r14b, %r14b
	je	.L821
	testq	%r12, %r12
	je	.L822
	negq	%r12
.L821:
	addq	$24, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	cvtsi2sdq	%r12, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L851:
	.cfi_restore_state
	leal	-87(%rax), %ecx
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L848:
	pxor	%xmm0, %xmm0
	testb	%r14b, %r14b
	je	.L791
.L822:
	movsd	.LC21(%rip), %xmm0
.L791:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L852:
	.cfi_restore_state
	cmpq	%rbx, %r13
	je	.L799
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r15
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L800:
	movslq	%edi, %rdi
	testb	$8, (%r15,%rdi)
	je	.L803
.L801:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L799
.L802:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L800
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L801
.L803:
	movsd	.LC22(%rip), %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L850:
	.cfi_restore_state
	movl	%eax, %esi
	cmpq	$1, %rax
	je	.L828
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L806:
	sarl	%esi
	movl	%r8d, %r9d
	addl	$1, %r8d
	cmpl	$1, %esi
	jne	.L806
	movl	%r8d, %ecx
	sall	%cl, %esi
	leal	-1(%rsi), %r15d
.L805:
	andl	%r12d, %r15d
	movl	%r8d, %ecx
	movl	%r15d, -60(%rbp)
	sarq	%cl, %r12
	cmpq	%rbx, %r13
	je	.L829
	movq	%rbx, %rdi
	movl	$1, %r15d
.L813:
	movzbl	(%rdi), %ecx
	movq	%rdi, %rbx
	leal	-48(%rcx), %r10d
	movl	%ecx, %eax
	cmpb	$9, %r10b
	jbe	.L808
	cmpl	$96, %ecx
	jle	.L809
	cmpl	$118, %ecx
	jle	.L808
.L810:
	testb	%dl, %dl
	je	.L853
.L811:
	movl	$1, %eax
	movl	%r9d, %ecx
	sall	%cl, %eax
	cmpl	%eax, -60(%rbp)
	jg	.L830
	je	.L854
.L818:
	btq	$53, %r12
	jnc	.L820
	addl	$1, %r8d
	sarq	%r12
.L820:
	testb	%r14b, %r14b
	je	.L855
	negq	%r12
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
.L823:
	addq	$24, %rsp
	movl	%r8d, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ldexp@PLT
	.p2align 4,,10
	.p2align 3
.L809:
	.cfi_restore_state
	leal	-65(%rcx), %ecx
	cmpb	$21, %cl
	ja	.L810
	.p2align 4,,10
	.p2align 3
.L808:
	cmpb	$48, %al
	sete	%al
	addq	$1, %rdi
	addl	$5, %r8d
	andl	%eax, %r15d
	cmpq	%rdi, %r13
	jne	.L813
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L855:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
	jmp	.L823
.L854:
	testb	$1, %r12b
	jne	.L830
	testb	%r15b, %r15b
	jne	.L818
.L830:
	addq	$1, %r12
	jmp	.L818
.L853:
	cmpq	%rdi, %r13
	je	.L811
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L814:
	movslq	%edi, %rdi
	testb	$8, (%rdx,%rdi)
	je	.L803
.L816:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L811
.L815:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L814
	movl	%r8d, -56(%rbp)
	movl	%r9d, -52(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movl	-52(%rbp), %r9d
	movl	-56(%rbp), %r8d
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	je	.L803
	jmp	.L816
.L828:
	xorl	%r9d, %r9d
	movl	$1, %r15d
	movl	$1, %r8d
	jmp	.L805
.L829:
	movl	$1, %r15d
	jmp	.L811
	.cfi_endproc
.LFE20730:
	.size	_ZN2v88internal25InternalStringToIntDoubleILi5EPKhS3_EEdT0_T1_bb, .-_ZN2v88internal25InternalStringToIntDoubleILi5EPKhS3_EEdT0_T1_bb
	.section	.text._ZN2v88internal25InternalStringToIntDoubleILi1EPKtS3_EEdT0_T1_bb,"axG",@progbits,_ZN2v88internal25InternalStringToIntDoubleILi1EPKtS3_EEdT0_T1_bb,comdat
	.p2align 4
	.weak	_ZN2v88internal25InternalStringToIntDoubleILi1EPKtS3_EEdT0_T1_bb
	.type	_ZN2v88internal25InternalStringToIntDoubleILi1EPKtS3_EEdT0_T1_bb, @function
_ZN2v88internal25InternalStringToIntDoubleILi1EPKtS3_EEdT0_T1_bb:
.LFB20731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	movl	%ecx, %edx
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L910:
	addq	$2, %rbx
	cmpq	%r13, %rbx
	je	.L909
.L858:
	movzwl	(%rbx), %eax
	cmpw	$48, %ax
	je	.L910
	xorl	%r12d, %r12d
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L912:
	subl	$48, %eax
	addq	$2, %rbx
	cltq
	leaq	(%rax,%r12,2), %r12
	movq	%r12, %rax
	sarq	$53, %rax
	jne	.L911
	cmpq	%r13, %rbx
	je	.L863
	movzwl	(%rbx), %eax
.L857:
	leal	-48(%rax), %ecx
	cmpw	$1, %cx
	jbe	.L912
	testb	%dl, %dl
	je	.L913
.L863:
	testb	%r15b, %r15b
	je	.L884
	testq	%r12, %r12
	je	.L885
	negq	%r12
.L884:
	addq	$40, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	cvtsi2sdq	%r12, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	testb	%r15b, %r15b
	je	.L856
.L885:
	movsd	.LC21(%rip), %xmm0
.L856:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L913:
	.cfi_restore_state
	cmpq	%rbx, %r13
	je	.L863
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L864:
	movslq	%r14d, %r14
	testb	$8, (%rdx,%r14)
	je	.L868
.L867:
	addq	$2, %rbx
	cmpq	%rbx, %r13
	je	.L863
.L866:
	movzwl	(%rbx), %r14d
	cmpw	$127, %r14w
	jbe	.L864
	movl	%r14d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	jne	.L867
	subl	$8232, %r14d
	cmpl	$1, %r14d
	jbe	.L867
.L868:
	movsd	.LC22(%rip), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L911:
	.cfi_restore_state
	movl	%eax, %esi
	cmpq	$1, %rax
	je	.L891
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L870:
	sarl	%esi
	movl	%r8d, %r9d
	addl	$1, %r8d
	cmpl	$1, %esi
	jne	.L870
	movl	%r8d, %ecx
	sall	%cl, %esi
	leal	-1(%rsi), %r14d
.L869:
	andl	%r12d, %r14d
	movl	%r8d, %ecx
	movb	$1, -69(%rbp)
	movl	%r14d, -68(%rbp)
	sarq	%cl, %r12
	cmpq	%r13, %rbx
	je	.L873
.L875:
	movzwl	(%rbx), %eax
	movq	%rbx, %rcx
	leal	-48(%rax), %edi
	cmpw	$1, %di
	jbe	.L872
	testb	%dl, %dl
	je	.L914
.L873:
	movl	$1, %eax
	movl	%r9d, %ecx
	sall	%cl, %eax
	cmpl	%eax, -68(%rbp)
	jg	.L893
	je	.L915
.L881:
	btq	$53, %r12
	jnc	.L883
	addl	$1, %r8d
	sarq	%r12
.L883:
	testb	%r15b, %r15b
	je	.L916
	negq	%r12
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
.L886:
	addq	$40, %rsp
	movl	%r8d, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ldexp@PLT
	.p2align 4,,10
	.p2align 3
.L916:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
	jmp	.L886
.L915:
	testb	$1, %r12b
	jne	.L893
	cmpb	$0, -69(%rbp)
	jne	.L881
.L893:
	addq	$1, %r12
	jmp	.L881
.L914:
	cmpq	%rbx, %r13
	je	.L873
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L876:
	movslq	%ebx, %rbx
	testb	$8, (%r14,%rbx)
	je	.L868
.L879:
	addq	$2, %rcx
	cmpq	%rcx, %r13
	je	.L873
.L878:
	movzwl	(%rcx), %ebx
	cmpw	$127, %bx
	jbe	.L876
	movl	%ebx, %edi
	movl	%r8d, -64(%rbp)
	movl	%r9d, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movq	-56(%rbp), %rcx
	movl	-60(%rbp), %r9d
	testb	%al, %al
	movl	-64(%rbp), %r8d
	jne	.L879
	subl	$8232, %ebx
	cmpl	$1, %ebx
	ja	.L868
	jmp	.L879
.L872:
	cmpw	$48, %ax
	sete	%al
	addq	$2, %rbx
	andb	%al, -69(%rbp)
	addl	$1, %r8d
	cmpq	%rbx, %r13
	jne	.L875
	jmp	.L873
.L891:
	xorl	%r9d, %r9d
	movl	$1, %r14d
	movl	$1, %r8d
	jmp	.L869
	.cfi_endproc
.LFE20731:
	.size	_ZN2v88internal25InternalStringToIntDoubleILi1EPKtS3_EEdT0_T1_bb, .-_ZN2v88internal25InternalStringToIntDoubleILi1EPKtS3_EEdT0_T1_bb
	.section	.text._ZN2v88internal25InternalStringToIntDoubleILi2EPKtS3_EEdT0_T1_bb,"axG",@progbits,_ZN2v88internal25InternalStringToIntDoubleILi2EPKtS3_EEdT0_T1_bb,comdat
	.p2align 4
	.weak	_ZN2v88internal25InternalStringToIntDoubleILi2EPKtS3_EEdT0_T1_bb
	.type	_ZN2v88internal25InternalStringToIntDoubleILi2EPKtS3_EEdT0_T1_bb, @function
_ZN2v88internal25InternalStringToIntDoubleILi2EPKtS3_EEdT0_T1_bb:
.LFB20732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	movl	%ecx, %edx
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L971:
	addq	$2, %rbx
	cmpq	%r13, %rbx
	je	.L970
.L919:
	movzwl	(%rbx), %eax
	cmpw	$48, %ax
	je	.L971
	xorl	%r12d, %r12d
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L973:
	subl	$48, %eax
	addq	$2, %rbx
	cltq
	leaq	(%rax,%r12,4), %r12
	movq	%r12, %rax
	sarq	$53, %rax
	jne	.L972
	cmpq	%r13, %rbx
	je	.L924
	movzwl	(%rbx), %eax
.L918:
	leal	-48(%rax), %ecx
	cmpw	$3, %cx
	jbe	.L973
	testb	%dl, %dl
	je	.L974
.L924:
	testb	%r15b, %r15b
	je	.L945
	testq	%r12, %r12
	je	.L946
	negq	%r12
.L945:
	addq	$40, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	cvtsi2sdq	%r12, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L970:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	testb	%r15b, %r15b
	je	.L917
.L946:
	movsd	.LC21(%rip), %xmm0
.L917:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L974:
	.cfi_restore_state
	cmpq	%rbx, %r13
	je	.L924
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L925:
	movslq	%r14d, %r14
	testb	$8, (%rdx,%r14)
	je	.L929
.L928:
	addq	$2, %rbx
	cmpq	%rbx, %r13
	je	.L924
.L927:
	movzwl	(%rbx), %r14d
	cmpw	$127, %r14w
	jbe	.L925
	movl	%r14d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	jne	.L928
	subl	$8232, %r14d
	cmpl	$1, %r14d
	jbe	.L928
.L929:
	movsd	.LC22(%rip), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L972:
	.cfi_restore_state
	movl	%eax, %esi
	cmpq	$1, %rax
	je	.L952
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L931:
	sarl	%esi
	movl	%r8d, %r9d
	addl	$1, %r8d
	cmpl	$1, %esi
	jne	.L931
	movl	%r8d, %ecx
	sall	%cl, %esi
	leal	-1(%rsi), %r14d
.L930:
	andl	%r12d, %r14d
	movl	%r8d, %ecx
	movb	$1, -65(%rbp)
	movl	%r14d, -64(%rbp)
	sarq	%cl, %r12
	cmpq	%r13, %rbx
	je	.L934
.L936:
	movzwl	(%rbx), %eax
	leal	-48(%rax), %ecx
	cmpw	$3, %cx
	jbe	.L933
	testb	%dl, %dl
	je	.L975
.L934:
	movl	$1, %eax
	movl	%r9d, %ecx
	sall	%cl, %eax
	cmpl	%eax, -64(%rbp)
	jg	.L954
	je	.L976
.L942:
	btq	$53, %r12
	jnc	.L944
	addl	$1, %r8d
	sarq	%r12
.L944:
	testb	%r15b, %r15b
	je	.L977
	negq	%r12
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
.L947:
	addq	$40, %rsp
	movl	%r8d, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ldexp@PLT
	.p2align 4,,10
	.p2align 3
.L977:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
	jmp	.L947
.L976:
	testb	$1, %r12b
	jne	.L954
	cmpb	$0, -65(%rbp)
	jne	.L942
.L954:
	addq	$1, %r12
	jmp	.L942
.L975:
	cmpq	%rbx, %r13
	je	.L934
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L937:
	movslq	%edx, %rdx
	testb	$8, (%r14,%rdx)
	je	.L929
.L940:
	addq	$2, %rbx
	cmpq	%rbx, %r13
	je	.L934
.L939:
	movzwl	(%rbx), %edx
	cmpw	$127, %dx
	jbe	.L937
	movl	%edx, %edi
	movl	%r9d, -60(%rbp)
	movl	%r8d, -56(%rbp)
	movl	%edx, -52(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movl	-52(%rbp), %edx
	movl	-56(%rbp), %r8d
	testb	%al, %al
	movl	-60(%rbp), %r9d
	jne	.L940
	subl	$8232, %edx
	cmpl	$1, %edx
	ja	.L929
	jmp	.L940
.L933:
	cmpw	$48, %ax
	sete	%al
	addq	$2, %rbx
	andb	%al, -65(%rbp)
	addl	$2, %r8d
	cmpq	%r13, %rbx
	jne	.L936
	jmp	.L934
.L952:
	xorl	%r9d, %r9d
	movl	$1, %r14d
	movl	$1, %r8d
	jmp	.L930
	.cfi_endproc
.LFE20732:
	.size	_ZN2v88internal25InternalStringToIntDoubleILi2EPKtS3_EEdT0_T1_bb, .-_ZN2v88internal25InternalStringToIntDoubleILi2EPKtS3_EEdT0_T1_bb
	.section	.text._ZN2v88internal25InternalStringToIntDoubleILi3EPKtS3_EEdT0_T1_bb,"axG",@progbits,_ZN2v88internal25InternalStringToIntDoubleILi3EPKtS3_EEdT0_T1_bb,comdat
	.p2align 4
	.weak	_ZN2v88internal25InternalStringToIntDoubleILi3EPKtS3_EEdT0_T1_bb
	.type	_ZN2v88internal25InternalStringToIntDoubleILi3EPKtS3_EEdT0_T1_bb, @function
_ZN2v88internal25InternalStringToIntDoubleILi3EPKtS3_EEdT0_T1_bb:
.LFB20733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	movl	%ecx, %edx
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L1032:
	addq	$2, %rbx
	cmpq	%r13, %rbx
	je	.L1031
.L980:
	movzwl	(%rbx), %eax
	cmpw	$48, %ax
	je	.L1032
	xorl	%r12d, %r12d
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1034:
	subl	$48, %eax
	addq	$2, %rbx
	cltq
	leaq	(%rax,%r12,8), %r12
	movq	%r12, %rax
	sarq	$53, %rax
	jne	.L1033
	cmpq	%r13, %rbx
	je	.L985
	movzwl	(%rbx), %eax
.L979:
	leal	-48(%rax), %ecx
	cmpw	$7, %cx
	jbe	.L1034
	testb	%dl, %dl
	je	.L1035
.L985:
	testb	%r15b, %r15b
	je	.L1006
	testq	%r12, %r12
	je	.L1007
	negq	%r12
.L1006:
	addq	$40, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	cvtsi2sdq	%r12, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1031:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	testb	%r15b, %r15b
	je	.L978
.L1007:
	movsd	.LC21(%rip), %xmm0
.L978:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1035:
	.cfi_restore_state
	cmpq	%rbx, %r13
	je	.L985
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L986:
	movslq	%r14d, %r14
	testb	$8, (%rdx,%r14)
	je	.L990
.L989:
	addq	$2, %rbx
	cmpq	%rbx, %r13
	je	.L985
.L988:
	movzwl	(%rbx), %r14d
	cmpw	$127, %r14w
	jbe	.L986
	movl	%r14d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	jne	.L989
	subl	$8232, %r14d
	cmpl	$1, %r14d
	jbe	.L989
.L990:
	movsd	.LC22(%rip), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1033:
	.cfi_restore_state
	movl	%eax, %esi
	cmpq	$1, %rax
	je	.L1013
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L992:
	sarl	%esi
	movl	%r8d, %r9d
	addl	$1, %r8d
	cmpl	$1, %esi
	jne	.L992
	movl	%r8d, %ecx
	sall	%cl, %esi
	leal	-1(%rsi), %r14d
.L991:
	andl	%r12d, %r14d
	movl	%r8d, %ecx
	movb	$1, -69(%rbp)
	movl	%r14d, -68(%rbp)
	sarq	%cl, %r12
	cmpq	%r13, %rbx
	je	.L995
.L997:
	movzwl	(%rbx), %eax
	movq	%rbx, %rcx
	leal	-48(%rax), %edi
	cmpw	$7, %di
	jbe	.L994
	testb	%dl, %dl
	je	.L1036
.L995:
	movl	$1, %eax
	movl	%r9d, %ecx
	sall	%cl, %eax
	cmpl	%eax, -68(%rbp)
	jg	.L1015
	je	.L1037
.L1003:
	btq	$53, %r12
	jnc	.L1005
	addl	$1, %r8d
	sarq	%r12
.L1005:
	testb	%r15b, %r15b
	je	.L1038
	negq	%r12
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
.L1008:
	addq	$40, %rsp
	movl	%r8d, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ldexp@PLT
	.p2align 4,,10
	.p2align 3
.L1038:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
	jmp	.L1008
.L1037:
	testb	$1, %r12b
	jne	.L1015
	cmpb	$0, -69(%rbp)
	jne	.L1003
.L1015:
	addq	$1, %r12
	jmp	.L1003
.L1036:
	cmpq	%rbx, %r13
	je	.L995
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L998:
	movslq	%ebx, %rbx
	testb	$8, (%r14,%rbx)
	je	.L990
.L1001:
	addq	$2, %rcx
	cmpq	%rcx, %r13
	je	.L995
.L1000:
	movzwl	(%rcx), %ebx
	cmpw	$127, %bx
	jbe	.L998
	movl	%ebx, %edi
	movl	%r8d, -64(%rbp)
	movl	%r9d, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movq	-56(%rbp), %rcx
	movl	-60(%rbp), %r9d
	testb	%al, %al
	movl	-64(%rbp), %r8d
	jne	.L1001
	subl	$8232, %ebx
	cmpl	$1, %ebx
	ja	.L990
	jmp	.L1001
.L994:
	cmpw	$48, %ax
	sete	%al
	addq	$2, %rbx
	andb	%al, -69(%rbp)
	addl	$3, %r8d
	cmpq	%rbx, %r13
	jne	.L997
	jmp	.L995
.L1013:
	xorl	%r9d, %r9d
	movl	$1, %r14d
	movl	$1, %r8d
	jmp	.L991
	.cfi_endproc
.LFE20733:
	.size	_ZN2v88internal25InternalStringToIntDoubleILi3EPKtS3_EEdT0_T1_bb, .-_ZN2v88internal25InternalStringToIntDoubleILi3EPKtS3_EEdT0_T1_bb
	.section	.text._ZN2v88internal25InternalStringToIntDoubleILi4EPKtS3_EEdT0_T1_bb,"axG",@progbits,_ZN2v88internal25InternalStringToIntDoubleILi4EPKtS3_EEdT0_T1_bb,comdat
	.p2align 4
	.weak	_ZN2v88internal25InternalStringToIntDoubleILi4EPKtS3_EEdT0_T1_bb
	.type	_ZN2v88internal25InternalStringToIntDoubleILi4EPKtS3_EEdT0_T1_bb, @function
_ZN2v88internal25InternalStringToIntDoubleILi4EPKtS3_EEdT0_T1_bb:
.LFB20734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	%ecx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1096:
	addq	$2, %rbx
	cmpq	%r13, %rbx
	je	.L1095
.L1041:
	movzwl	(%rbx), %eax
	cmpw	$48, %ax
	je	.L1096
	xorl	%r12d, %r12d
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1043:
	leal	-65(%rax), %ecx
	cmpw	$5, %cx
	ja	.L1046
	leal	-55(%rax), %ecx
.L1045:
	salq	$4, %r12
	movslq	%ecx, %rax
	addq	$2, %rbx
	addq	%rax, %r12
	movq	%r12, %rax
	sarq	$53, %rax
	jne	.L1097
	cmpq	%rbx, %r13
	je	.L1047
	movzwl	(%rbx), %eax
.L1040:
	cmpw	$47, %ax
	jbe	.L1043
	leal	-48(%rax), %ecx
	cmpw	$57, %ax
	jbe	.L1045
	cmpw	$96, %ax
	jbe	.L1043
	cmpw	$102, %ax
	jbe	.L1098
.L1046:
	testb	%dl, %dl
	je	.L1099
.L1047:
	testb	%r14b, %r14b
	je	.L1071
	testq	%r12, %r12
	je	.L1072
	negq	%r12
.L1071:
	addq	$40, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	cvtsi2sdq	%r12, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1098:
	.cfi_restore_state
	leal	-87(%rax), %ecx
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1095:
	pxor	%xmm0, %xmm0
	testb	%r14b, %r14b
	je	.L1039
.L1072:
	movsd	.LC21(%rip), %xmm0
.L1039:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1099:
	.cfi_restore_state
	cmpq	%rbx, %r13
	je	.L1047
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1048:
	movslq	%r15d, %r15
	testb	$8, (%rdx,%r15)
	je	.L1052
.L1051:
	addq	$2, %rbx
	cmpq	%rbx, %r13
	je	.L1047
.L1050:
	movzwl	(%rbx), %r15d
	cmpw	$127, %r15w
	jbe	.L1048
	movl	%r15d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	jne	.L1051
	subl	$8232, %r15d
	cmpl	$1, %r15d
	jbe	.L1051
.L1052:
	movsd	.LC22(%rip), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1097:
	.cfi_restore_state
	movl	%eax, %esi
	cmpq	$1, %rax
	je	.L1078
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L1055:
	sarl	%esi
	movl	%r8d, %r9d
	addl	$1, %r8d
	cmpl	$1, %esi
	jne	.L1055
	movl	%r8d, %ecx
	sall	%cl, %esi
	leal	-1(%rsi), %r15d
.L1054:
	andl	%r12d, %r15d
	movq	%r12, %rsi
	movl	%r8d, %ecx
	movl	%r15d, -68(%rbp)
	sarq	%cl, %rsi
	cmpq	%rbx, %r13
	je	.L1079
	movq	%rbx, %rdi
	movl	$1, %r15d
.L1062:
	movzwl	(%rdi), %ecx
	movq	%rdi, %rbx
	leal	-48(%rcx), %r11d
	movl	%ecx, %eax
	cmpw	$9, %r11w
	jbe	.L1057
	cmpl	$96, %ecx
	jle	.L1058
	cmpl	$102, %ecx
	jle	.L1057
.L1059:
	testb	%dl, %dl
	je	.L1100
.L1060:
	movl	$1, %eax
	movl	%r9d, %ecx
	sall	%cl, %eax
	cmpl	%eax, -68(%rbp)
	jg	.L1080
	je	.L1101
.L1068:
	btq	$53, %rsi
	jnc	.L1070
	addl	$1, %r8d
	sarq	%rsi
.L1070:
	testb	%r14b, %r14b
	je	.L1102
	negq	%rsi
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L1073:
	addq	$40, %rsp
	movl	%r8d, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ldexp@PLT
	.p2align 4,,10
	.p2align 3
.L1058:
	.cfi_restore_state
	leal	-65(%rcx), %ecx
	cmpw	$5, %cx
	ja	.L1059
	.p2align 4,,10
	.p2align 3
.L1057:
	cmpw	$48, %ax
	sete	%al
	addq	$2, %rdi
	addl	$4, %r8d
	andl	%eax, %r15d
	cmpq	%rdi, %r13
	jne	.L1062
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1102:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	jmp	.L1073
.L1101:
	testb	$1, %sil
	jne	.L1080
	testb	%r15b, %r15b
	jne	.L1068
.L1080:
	addq	$1, %rsi
	jmp	.L1068
.L1100:
	cmpq	%rdi, %r13
	je	.L1060
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1063:
	movslq	%r12d, %rax
	testb	$8, (%rdx,%rax)
	je	.L1052
.L1066:
	addq	$2, %rbx
	cmpq	%rbx, %r13
	je	.L1060
.L1065:
	movzwl	(%rbx), %r12d
	cmpw	$127, %r12w
	jbe	.L1063
	movl	%r12d, %edi
	movl	%r8d, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movl	%r9d, -52(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movl	-52(%rbp), %r9d
	movq	-64(%rbp), %rsi
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	movl	-56(%rbp), %r8d
	jne	.L1066
	subl	$8232, %r12d
	cmpl	$1, %r12d
	ja	.L1052
	jmp	.L1066
.L1078:
	xorl	%r9d, %r9d
	movl	$1, %r15d
	movl	$1, %r8d
	jmp	.L1054
.L1079:
	movl	$1, %r15d
	jmp	.L1060
	.cfi_endproc
.LFE20734:
	.size	_ZN2v88internal25InternalStringToIntDoubleILi4EPKtS3_EEdT0_T1_bb, .-_ZN2v88internal25InternalStringToIntDoubleILi4EPKtS3_EEdT0_T1_bb
	.section	.text._ZN2v88internal25InternalStringToIntDoubleILi5EPKtS3_EEdT0_T1_bb,"axG",@progbits,_ZN2v88internal25InternalStringToIntDoubleILi5EPKtS3_EEdT0_T1_bb,comdat
	.p2align 4
	.weak	_ZN2v88internal25InternalStringToIntDoubleILi5EPKtS3_EEdT0_T1_bb
	.type	_ZN2v88internal25InternalStringToIntDoubleILi5EPKtS3_EEdT0_T1_bb, @function
_ZN2v88internal25InternalStringToIntDoubleILi5EPKtS3_EEdT0_T1_bb:
.LFB20735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	%ecx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1160:
	addq	$2, %rbx
	cmpq	%r13, %rbx
	je	.L1159
.L1105:
	movzwl	(%rbx), %eax
	cmpw	$48, %ax
	je	.L1160
	xorl	%r12d, %r12d
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1107:
	leal	-65(%rax), %ecx
	cmpw	$21, %cx
	ja	.L1110
	leal	-55(%rax), %ecx
.L1109:
	salq	$5, %r12
	movslq	%ecx, %rax
	addq	$2, %rbx
	addq	%rax, %r12
	movq	%r12, %rax
	sarq	$53, %rax
	jne	.L1161
	cmpq	%rbx, %r13
	je	.L1111
	movzwl	(%rbx), %eax
.L1104:
	cmpw	$47, %ax
	jbe	.L1107
	leal	-48(%rax), %ecx
	cmpw	$57, %ax
	jbe	.L1109
	cmpw	$96, %ax
	jbe	.L1107
	cmpw	$118, %ax
	jbe	.L1162
.L1110:
	testb	%dl, %dl
	je	.L1163
.L1111:
	testb	%r14b, %r14b
	je	.L1135
	testq	%r12, %r12
	je	.L1136
	negq	%r12
.L1135:
	addq	$40, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	cvtsi2sdq	%r12, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1162:
	.cfi_restore_state
	leal	-87(%rax), %ecx
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1159:
	pxor	%xmm0, %xmm0
	testb	%r14b, %r14b
	je	.L1103
.L1136:
	movsd	.LC21(%rip), %xmm0
.L1103:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1163:
	.cfi_restore_state
	cmpq	%rbx, %r13
	je	.L1111
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1112:
	movslq	%r15d, %r15
	testb	$8, (%rdx,%r15)
	je	.L1116
.L1115:
	addq	$2, %rbx
	cmpq	%rbx, %r13
	je	.L1111
.L1114:
	movzwl	(%rbx), %r15d
	cmpw	$127, %r15w
	jbe	.L1112
	movl	%r15d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	jne	.L1115
	subl	$8232, %r15d
	cmpl	$1, %r15d
	jbe	.L1115
.L1116:
	movsd	.LC22(%rip), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1161:
	.cfi_restore_state
	movl	%eax, %esi
	cmpq	$1, %rax
	je	.L1142
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L1119:
	sarl	%esi
	movl	%r8d, %r9d
	addl	$1, %r8d
	cmpl	$1, %esi
	jne	.L1119
	movl	%r8d, %ecx
	sall	%cl, %esi
	leal	-1(%rsi), %r15d
.L1118:
	andl	%r12d, %r15d
	movq	%r12, %rsi
	movl	%r8d, %ecx
	movl	%r15d, -68(%rbp)
	sarq	%cl, %rsi
	cmpq	%rbx, %r13
	je	.L1143
	movq	%rbx, %rdi
	movl	$1, %r15d
.L1126:
	movzwl	(%rdi), %ecx
	movq	%rdi, %rbx
	leal	-48(%rcx), %r11d
	movl	%ecx, %eax
	cmpw	$9, %r11w
	jbe	.L1121
	cmpl	$96, %ecx
	jle	.L1122
	cmpl	$118, %ecx
	jle	.L1121
.L1123:
	testb	%dl, %dl
	je	.L1164
.L1124:
	movl	$1, %eax
	movl	%r9d, %ecx
	sall	%cl, %eax
	cmpl	%eax, -68(%rbp)
	jg	.L1144
	je	.L1165
.L1132:
	btq	$53, %rsi
	jnc	.L1134
	addl	$1, %r8d
	sarq	%rsi
.L1134:
	testb	%r14b, %r14b
	je	.L1166
	negq	%rsi
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L1137:
	addq	$40, %rsp
	movl	%r8d, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ldexp@PLT
	.p2align 4,,10
	.p2align 3
.L1122:
	.cfi_restore_state
	leal	-65(%rcx), %ecx
	cmpw	$21, %cx
	ja	.L1123
	.p2align 4,,10
	.p2align 3
.L1121:
	cmpw	$48, %ax
	sete	%al
	addq	$2, %rdi
	addl	$5, %r8d
	andl	%eax, %r15d
	cmpq	%rdi, %r13
	jne	.L1126
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1166:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	jmp	.L1137
.L1165:
	testb	$1, %sil
	jne	.L1144
	testb	%r15b, %r15b
	jne	.L1132
.L1144:
	addq	$1, %rsi
	jmp	.L1132
.L1164:
	cmpq	%rdi, %r13
	je	.L1124
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1127:
	movslq	%r12d, %rax
	testb	$8, (%rdx,%rax)
	je	.L1116
.L1130:
	addq	$2, %rbx
	cmpq	%rbx, %r13
	je	.L1124
.L1129:
	movzwl	(%rbx), %r12d
	cmpw	$127, %r12w
	jbe	.L1127
	movl	%r12d, %edi
	movl	%r8d, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movl	%r9d, -52(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movl	-52(%rbp), %r9d
	movq	-64(%rbp), %rsi
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	movl	-56(%rbp), %r8d
	jne	.L1130
	subl	$8232, %r12d
	cmpl	$1, %r12d
	ja	.L1116
	jmp	.L1130
.L1142:
	xorl	%r9d, %r9d
	movl	$1, %r15d
	movl	$1, %r8d
	jmp	.L1118
.L1143:
	movl	$1, %r15d
	jmp	.L1124
	.cfi_endproc
.LFE20735:
	.size	_ZN2v88internal25InternalStringToIntDoubleILi5EPKtS3_EEdT0_T1_bb, .-_ZN2v88internal25InternalStringToIntDoubleILi5EPKtS3_EEdT0_T1_bb
	.section	.text._ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv,"axG",@progbits,_ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv
	.type	_ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv, @function
_ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv:
.LFB17827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	32(%rdi), %eax
	testl	%eax, %eax
	jle	.L1167
	leal	-1(%rax), %edx
	movq	%rdi, %rbx
	testl	%eax, %edx
	je	.L1201
	cmpl	$10, %eax
	jne	.L1167
	movq	24(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L1202
.L1172:
	movq	%rbx, %rdi
	call	_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKhEEdT_
.L1194:
	movl	$5, 52(%rbx)
	movsd	%xmm0, 56(%rbx)
.L1167:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1203
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1201:
	.cfi_restore_state
	movq	24(%rdi), %rsi
	movl	$1, %r12d
	testq	%rsi, %rsi
	je	.L1171
.L1170:
	movslq	36(%rbx), %rdi
	movslq	40(%rbx), %rax
	addq	%rsi, %rdi
	addq	%rax, %rsi
	cmpl	$32, 32(%rbx)
	ja	.L1179
	movl	32(%rbx), %eax
	leaq	.L1181(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv,"aG",@progbits,_ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv,comdat
	.align 4
	.align 4
.L1181:
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1185-.L1181
	.long	.L1179-.L1181
	.long	.L1184-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1183-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1182-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1179-.L1181
	.long	.L1180-.L1181
	.section	.text._ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv,"axG",@progbits,_ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv,comdat
.L1180:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi5EPKhS3_EEdT0_T1_bb
	jmp	.L1194
.L1182:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi4EPKhS3_EEdT0_T1_bb
	jmp	.L1194
.L1183:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi3EPKhS3_EEdT0_T1_bb
	jmp	.L1194
.L1184:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi2EPKhS3_EEdT0_T1_bb
	jmp	.L1194
.L1185:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi1EPKhS3_EEdT0_T1_bb
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1202:
	xorl	%r12d, %r12d
.L1171:
	movq	16(%rbx), %rcx
	movq	(%rcx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L1173
.L1204:
	cmpw	$8, %ax
	je	.L1174
	movq	15(%rdx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L1204
.L1173:
	movq	(%rcx), %rax
	leaq	-33(%rbp), %rsi
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	testb	%r12b, %r12b
	je	.L1205
	movslq	36(%rbx), %rdx
	cmpl	$32, 32(%rbx)
	leaq	(%rax,%rdx,2), %rdi
	movslq	40(%rbx), %rdx
	leaq	(%rax,%rdx,2), %rsi
	ja	.L1179
	movl	32(%rbx), %eax
	leaq	.L1189(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv,"aG",@progbits,_ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv,comdat
	.align 4
	.align 4
.L1189:
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1193-.L1189
	.long	.L1179-.L1189
	.long	.L1192-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1191-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1190-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1179-.L1189
	.long	.L1188-.L1189
	.section	.text._ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv,"axG",@progbits,_ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv,comdat
	.p2align 4,,10
	.p2align 3
.L1174:
	movq	(%rcx), %rax
	leaq	-33(%rbp), %rsi
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%rax, %rsi
	testb	%r12b, %r12b
	jne	.L1170
	jmp	.L1172
.L1179:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1205:
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal20NumberParseIntHelper17HandleBaseTenCaseIPKtEEdT_
	jmp	.L1194
.L1188:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi5EPKtS3_EEdT0_T1_bb
	jmp	.L1194
.L1190:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi4EPKtS3_EEdT0_T1_bb
	jmp	.L1194
.L1191:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi3EPKtS3_EEdT0_T1_bb
	jmp	.L1194
.L1192:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi2EPKtS3_EEdT0_T1_bb
	jmp	.L1194
.L1193:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi1EPKtS3_EEdT0_T1_bb
	jmp	.L1194
.L1203:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17827:
	.size	_ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv, .-_ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv
	.section	.text._ZN2v88internal11StringToIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11StringToIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEEi
	.type	_ZN2v88internal11StringToIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEEi, @function
_ZN2v88internal11StringToIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEEi:
.LFB17835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$400, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -408(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -376(%rbp)
	leaq	16+_ZTVN2v88internal20NumberParseIntHelperE(%rip), %rax
	movq	%rsi, -400(%rbp)
	movq	$0, -392(%rbp)
	movl	%edx, -384(%rbp)
	movl	$0, -380(%rbp)
	movw	%cx, -368(%rbp)
	movb	$1, -366(%rbp)
	movl	$0, -364(%rbp)
	movq	$0x000000000, -360(%rbp)
	movq	%rax, -416(%rbp)
	movq	(%rsi), %rax
.L1209:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andw	$9, %dx
	je	.L1207
	cmpw	$8, %dx
	jne	.L1325
	movq	-392(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1211
	movl	-376(%rbp), %edx
.L1212:
	leaq	-416(%rbp), %rdi
	call	_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKhEEvT_i
.L1210:
	movl	-364(%rbp), %eax
	testl	%eax, %eax
	jne	.L1213
	movl	-384(%rbp), %esi
	testl	%esi, %esi
	jle	.L1214
	leal	-1(%rsi), %eax
	testl	%esi, %eax
	jne	.L1326
	movq	-392(%rbp), %r8
	movl	$1, %ebx
	testq	%r8, %r8
	je	.L1304
	movslq	-380(%rbp), %rdi
	movslq	-376(%rbp), %rsi
	addq	%r8, %rdi
	addq	%rsi, %r8
.L1302:
	cmpl	$32, -384(%rbp)
	ja	.L1224
	movl	-384(%rbp), %eax
	leaq	.L1226(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11StringToIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEEi,"a",@progbits
	.align 4
	.align 4
.L1226:
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1230-.L1226
	.long	.L1224-.L1226
	.long	.L1229-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1228-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1227-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1224-.L1226
	.long	.L1225-.L1226
	.section	.text._ZN2v88internal11StringToIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEEi
	.p2align 4,,10
	.p2align 3
.L1326:
	cmpl	$10, %esi
	jne	.L1214
	movq	-392(%rbp), %r8
	testq	%r8, %r8
	je	.L1327
	movslq	-380(%rbp), %rdi
	movslq	-376(%rbp), %rsi
	addq	%r8, %rdi
	addq	%rsi, %r8
.L1223:
	xorl	%esi, %esi
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1328:
	cmpl	$309, %esi
	jg	.L1233
	movslq	%esi, %rdx
	addl	$1, %esi
	movb	%al, -352(%rbp,%rdx)
.L1233:
	addq	$1, %rdi
	cmpq	%r8, %rdi
	je	.L1232
.L1234:
	movzbl	(%rdi), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L1328
.L1232:
	movslq	%esi, %rsi
	leaq	-352(%rbp), %rdi
	xorl	%edx, %edx
	movb	$0, -352(%rbp,%rsi)
	call	_ZN2v88internal6StrtodENS0_6VectorIKcEEi@PLT
.L1242:
	movsd	%xmm0, -360(%rbp)
.L1246:
	movsd	-360(%rbp), %xmm0
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1214:
	movq	-392(%rbp), %r12
	testq	%r12, %r12
	je	.L1329
.L1252:
	movslq	-380(%rbp), %rbx
	leal	87(%rsi), %r10d
	leal	55(%rsi), %r9d
	movslq	-376(%rbp), %rax
	movl	$10, %r8d
	movsd	-360(%rbp), %xmm1
	addq	%r12, %rbx
	addq	%rax, %r12
	cmpl	$10, %esi
	cmovle	%esi, %r8d
	addl	$48, %r8d
.L1288:
	xorl	%eax, %eax
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1260:
	movzbl	(%rbx), %ecx
	cmpb	$47, %cl
	ja	.L1330
.L1253:
	pxor	%xmm3, %xmm3
	pxor	%xmm0, %xmm0
	cmpb	$0, -366(%rbp)
	cvtsi2sdl	%edi, %xmm3
	cvtsi2sdq	%rax, %xmm0
	mulsd	%xmm3, %xmm1
	addsd	%xmm1, %xmm0
	movsd	%xmm0, -360(%rbp)
	jne	.L1267
	cmpq	%rbx, %r12
	je	.L1267
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r13
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1262:
	movslq	%edi, %rdi
	testb	$8, 0(%r13,%rdi)
	je	.L1282
.L1266:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	je	.L1246
.L1265:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L1262
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L1266
	movsd	.LC22(%rip), %xmm0
	.p2align 4,,10
	.p2align 3
.L1206:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1331
	addq	$400, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1330:
	.cfi_restore_state
	movzbl	%cl, %edx
	cmpl	%edx, %r8d
	jle	.L1254
	subl	$48, %edx
.L1255:
	movl	%esi, %ecx
	imull	%edi, %ecx
	cmpl	$119304647, %ecx
	ja	.L1258
.L1332:
	imull	%esi, %eax
	addq	$1, %rbx
	addl	%edx, %eax
	cmpq	%rbx, %r12
	je	.L1259
	movl	%ecx, %edi
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1254:
	cmpb	$96, %cl
	jbe	.L1256
	cmpl	%edx, %r10d
	jle	.L1257
	movl	%esi, %ecx
	subl	$87, %edx
	imull	%edi, %ecx
	cmpl	$119304647, %ecx
	jbe	.L1332
	.p2align 4,,10
	.p2align 3
.L1258:
	pxor	%xmm2, %xmm2
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm2
	cvtsi2sdq	%rax, %xmm0
	mulsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, -360(%rbp)
	jmp	.L1288
.L1256:
	cmpb	$64, %cl
	jbe	.L1253
.L1257:
	cmpl	%edx, %r9d
	jle	.L1253
	subl	$55, %edx
	jmp	.L1255
	.p2align 4,,10
	.p2align 3
.L1259:
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	mulsd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm1, %xmm0
.L1267:
	movl	-372(%rbp), %eax
	testl	%eax, %eax
	jne	.L1206
	xorpd	.LC11(%rip), %xmm0
	jmp	.L1206
	.p2align 4,,10
	.p2align 3
.L1325:
	movq	15(%rax), %rax
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1207:
	movq	-400(%rbp), %rax
	leaq	-425(%rbp), %rsi
	leaq	-424(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	leaq	-416(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17StringToIntHelper19DetectRadixInternalIPKtEEvT_i
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1213:
	cmpl	$4, %eax
	jne	.L1333
	movl	-372(%rbp), %edx
	movsd	.LC21(%rip), %xmm0
	testl	%edx, %edx
	je	.L1206
	pxor	%xmm0, %xmm0
	jmp	.L1206
	.p2align 4,,10
	.p2align 3
.L1333:
	ja	.L1284
	subl	$2, %eax
	cmpl	$1, %eax
	ja	.L1224
.L1282:
	movsd	.LC22(%rip), %xmm0
	jmp	.L1206
.L1284:
	cmpl	$5, %eax
	je	.L1246
.L1224:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1211:
	movq	-400(%rbp), %rax
	leaq	-425(%rbp), %rsi
	leaq	-424(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%rax, %rsi
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1329:
	movq	-400(%rbp), %rdx
	movq	(%rdx), %rcx
.L1249:
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L1247
	cmpw	$8, %ax
	je	.L1248
	movq	15(%rcx), %rcx
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1327:
	xorl	%ebx, %ebx
.L1304:
	movq	-400(%rbp), %rdx
	movq	(%rdx), %rcx
.L1219:
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L1217
	cmpw	$8, %ax
	je	.L1218
	movq	15(%rcx), %rcx
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	(%rdx), %rax
	leaq	-425(%rbp), %rsi
	leaq	-424(%rbp), %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movl	-384(%rbp), %esi
	movq	%rax, %r12
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1218:
	movq	(%rdx), %rax
	leaq	-424(%rbp), %rdi
	leaq	-425(%rbp), %rsi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movslq	-380(%rbp), %rdi
	movslq	-376(%rbp), %r8
	addq	%rax, %rdi
	addq	%rax, %r8
	testb	%bl, %bl
	jne	.L1302
	jmp	.L1223
.L1225:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal25InternalStringToIntDoubleILi5EPKhS3_EEdT0_T1_bb
	jmp	.L1242
.L1227:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal25InternalStringToIntDoubleILi4EPKhS3_EEdT0_T1_bb
	jmp	.L1242
.L1228:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal25InternalStringToIntDoubleILi3EPKhS3_EEdT0_T1_bb
	jmp	.L1242
.L1229:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal25InternalStringToIntDoubleILi2EPKhS3_EEdT0_T1_bb
	jmp	.L1242
.L1230:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal25InternalStringToIntDoubleILi1EPKhS3_EEdT0_T1_bb
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1247:
	movq	(%rdx), %rax
	leaq	-424(%rbp), %rdi
	leaq	-425(%rbp), %rsi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movl	-384(%rbp), %edi
	movslq	-380(%rbp), %rdx
	movl	$10, %r8d
	movsd	-360(%rbp), %xmm0
	cmpl	$10, %edi
	leaq	(%rax,%rdx,2), %rbx
	movslq	-376(%rbp), %rdx
	leal	87(%rdi), %r10d
	cmovle	%edi, %r8d
	leal	55(%rdi), %r9d
	leaq	(%rax,%rdx,2), %r12
	addl	$48, %r8d
.L1250:
	xorl	%ecx, %ecx
	movl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L1275:
	movzwl	(%rbx), %edx
	cmpw	$47, %dx
	ja	.L1334
.L1268:
	pxor	%xmm3, %xmm3
	cmpb	$0, -366(%rbp)
	cvtsi2sdl	%esi, %xmm3
	mulsd	%xmm0, %xmm3
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm3, %xmm0
	movsd	%xmm0, -360(%rbp)
	jne	.L1246
	cmpq	%rbx, %r12
	je	.L1246
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1278:
	movslq	%r13d, %r13
	testb	$8, (%r14,%r13)
	je	.L1282
.L1281:
	addq	$2, %rbx
	cmpq	%rbx, %r12
	je	.L1246
.L1276:
	movzwl	(%rbx), %r13d
	cmpw	$127, %r13w
	jbe	.L1278
	movl	%r13d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L1281
	subl	$8232, %r13d
	cmpl	$1, %r13d
	ja	.L1282
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1334:
	movzwl	%dx, %eax
	cmpl	%eax, %r8d
	jle	.L1269
	subl	$48, %eax
.L1270:
	movl	%edi, %edx
	imull	%esi, %edx
	cmpl	$119304647, %edx
	ja	.L1273
	imull	%edi, %ecx
	addq	$2, %rbx
	addl	%eax, %ecx
	cmpq	%rbx, %r12
	je	.L1274
	movl	%edx, %esi
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1269:
	cmpw	$96, %dx
	jbe	.L1271
	cmpl	%eax, %r10d
	jle	.L1272
	subl	$87, %eax
	jmp	.L1270
.L1271:
	cmpw	$64, %dx
	jbe	.L1268
.L1272:
	cmpl	%eax, %r9d
	jle	.L1268
	subl	$55, %eax
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1273:
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm0, %xmm2
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, -360(%rbp)
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1217:
	movq	(%rdx), %rax
	leaq	-425(%rbp), %rsi
	leaq	-424(%rbp), %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	testb	%bl, %bl
	je	.L1335
	movslq	-380(%rbp), %rdx
	cmpl	$32, -384(%rbp)
	leaq	(%rax,%rdx,2), %rdi
	movslq	-376(%rbp), %rdx
	leaq	(%rax,%rdx,2), %rsi
	ja	.L1224
	movl	-384(%rbp), %eax
	leaq	.L1237(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11StringToIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEEi
	.align 4
	.align 4
.L1237:
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1241-.L1237
	.long	.L1224-.L1237
	.long	.L1240-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1239-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1238-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1224-.L1237
	.long	.L1236-.L1237
	.section	.text._ZN2v88internal11StringToIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEEi
.L1236:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi5EPKtS3_EEdT0_T1_bb
	jmp	.L1242
.L1238:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi4EPKtS3_EEdT0_T1_bb
	jmp	.L1242
.L1239:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi3EPKtS3_EEdT0_T1_bb
	jmp	.L1242
.L1240:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi2EPKtS3_EEdT0_T1_bb
	jmp	.L1242
.L1241:
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi1EPKtS3_EEdT0_T1_bb
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1274:
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	mulsd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, -360(%rbp)
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1335:
	movslq	-380(%rbp), %rdx
	movslq	-376(%rbp), %rcx
	leaq	(%rax,%rdx,2), %rdx
	leaq	(%rax,%rcx,2), %rdi
	xorl	%eax, %eax
	jmp	.L1245
	.p2align 4,,10
	.p2align 3
.L1336:
	cmpl	$309, %eax
	jg	.L1244
	movslq	%eax, %rsi
	addl	$1, %eax
	movb	%cl, -352(%rbp,%rsi)
.L1244:
	addq	$2, %rdx
	cmpq	%rdx, %rdi
	je	.L1243
.L1245:
	movzwl	(%rdx), %ecx
	leal	-48(%rcx), %esi
	cmpw	$9, %si
	jbe	.L1336
.L1243:
	movslq	%eax, %rsi
	leaq	-352(%rbp), %rdi
	xorl	%edx, %edx
	movb	$0, -352(%rbp,%rsi)
	call	_ZN2v88internal6StrtodENS0_6VectorIKcEEi@PLT
	jmp	.L1242
.L1331:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17835:
	.size	_ZN2v88internal11StringToIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEEi, .-_ZN2v88internal11StringToIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEEi
	.section	.text._ZN2v88internal25InternalStringToIntDoubleILi3EPcS2_EEdT0_T1_bb,"axG",@progbits,_ZN2v88internal25InternalStringToIntDoubleILi3EPcS2_EEdT0_T1_bb,comdat
	.p2align 4
	.weak	_ZN2v88internal25InternalStringToIntDoubleILi3EPcS2_EEdT0_T1_bb
	.type	_ZN2v88internal25InternalStringToIntDoubleILi3EPcS2_EEdT0_T1_bb, @function
_ZN2v88internal25InternalStringToIntDoubleILi3EPcS2_EEdT0_T1_bb:
.LFB20737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	movl	%ecx, %edx
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	jmp	.L1339
	.p2align 4,,10
	.p2align 3
.L1392:
	addq	$1, %rbx
	cmpq	%r13, %rbx
	je	.L1391
.L1339:
	movzbl	(%rbx), %eax
	cmpb	$48, %al
	je	.L1392
	xorl	%r12d, %r12d
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1394:
	movsbl	%al, %ecx
	cmpb	$55, %al
	jg	.L1341
	subl	$48, %ecx
	addq	$1, %rbx
	movslq	%ecx, %rcx
	leaq	(%rcx,%r12,8), %r12
	movq	%r12, %rax
	sarq	$53, %rax
	jne	.L1393
	cmpq	%rbx, %r13
	je	.L1344
	movzbl	(%rbx), %eax
.L1338:
	cmpb	$47, %al
	jg	.L1394
.L1341:
	testb	%dl, %dl
	je	.L1395
.L1344:
	testb	%r15b, %r15b
	je	.L1363
	testq	%r12, %r12
	je	.L1364
	negq	%r12
.L1363:
	addq	$24, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	cvtsi2sdq	%r12, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1391:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	testb	%r15b, %r15b
	je	.L1337
.L1364:
	movsd	.LC21(%rip), %xmm0
.L1337:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1395:
	.cfi_restore_state
	cmpq	%rbx, %r13
	je	.L1344
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1345:
	testb	$8, (%r14,%rdi)
	je	.L1348
.L1346:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L1344
.L1347:
	movsbq	(%rbx), %rdi
	cmpl	$127, %edi
	jbe	.L1345
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L1346
.L1348:
	movsd	.LC22(%rip), %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1393:
	.cfi_restore_state
	movl	%eax, %esi
	cmpq	$1, %rax
	je	.L1370
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L1350:
	sarl	%esi
	movl	%r8d, %r9d
	addl	$1, %r8d
	cmpl	$1, %esi
	jne	.L1350
	movl	%r8d, %ecx
	sall	%cl, %esi
	leal	-1(%rsi), %r14d
.L1349:
	andl	%r12d, %r14d
	movl	%r8d, %ecx
	movb	$1, -61(%rbp)
	movl	%r14d, -60(%rbp)
	sarq	%cl, %r12
	cmpq	%rbx, %r13
	je	.L1353
.L1355:
	movzbl	(%rbx), %eax
	movq	%rbx, %r14
	leal	-48(%rax), %edi
	cmpb	$7, %dil
	jbe	.L1352
	testb	%dl, %dl
	je	.L1396
.L1353:
	movl	$1, %eax
	movl	%r9d, %ecx
	sall	%cl, %eax
	cmpl	%eax, -60(%rbp)
	jg	.L1372
	je	.L1397
.L1360:
	btq	$53, %r12
	jnc	.L1362
	addl	$1, %r8d
	sarq	%r12
.L1362:
	testb	%r15b, %r15b
	je	.L1398
	negq	%r12
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
.L1365:
	addq	$24, %rsp
	movl	%r8d, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ldexp@PLT
	.p2align 4,,10
	.p2align 3
.L1398:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
	jmp	.L1365
.L1397:
	testb	$1, %r12b
	jne	.L1372
	cmpb	$0, -61(%rbp)
	jne	.L1360
.L1372:
	addq	$1, %r12
	jmp	.L1360
.L1396:
	cmpq	%rbx, %r13
	je	.L1353
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rbx
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1356:
	testb	$8, (%rbx,%rdi)
	je	.L1348
.L1358:
	addq	$1, %r14
	cmpq	%r14, %r13
	je	.L1353
.L1357:
	movsbq	(%r14), %rdi
	cmpl	$127, %edi
	jbe	.L1356
	movl	%r8d, -56(%rbp)
	movl	%r9d, -52(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movl	-52(%rbp), %r9d
	movl	-56(%rbp), %r8d
	testb	%al, %al
	je	.L1348
	jmp	.L1358
.L1352:
	cmpb	$48, %al
	sete	%al
	addq	$1, %rbx
	andb	%al, -61(%rbp)
	addl	$3, %r8d
	cmpq	%rbx, %r13
	jne	.L1355
	jmp	.L1353
.L1370:
	xorl	%r9d, %r9d
	movl	$1, %r14d
	movl	$1, %r8d
	jmp	.L1349
	.cfi_endproc
.LFE20737:
	.size	_ZN2v88internal25InternalStringToIntDoubleILi3EPcS2_EEdT0_T1_bb, .-_ZN2v88internal25InternalStringToIntDoubleILi3EPcS2_EEdT0_T1_bb
	.section	.text._ZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_id,"axG",@progbits,_ZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_id,comdat
	.p2align 4
	.weak	_ZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_id
	.type	_ZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_id, @function
_ZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_id:
.LFB19715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%xmm0, %r13
	pushq	%r12
	pushq	%rbx
	subq	$824, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rdi, %rsi
	je	.L1404
	movq	%rdi, %rbx
	movq	%rsi, %r15
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	movl	%edx, %r12d
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1402:
	movslq	%edi, %rdi
	testb	$8, (%r14,%rdi)
	je	.L1403
.L1405:
	addq	$1, %rbx
	cmpq	%rbx, %r15
	je	.L1404
.L1400:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L1402
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L1405
.L1403:
	movzbl	(%rbx), %eax
	cmpb	$43, %al
	je	.L1558
	cmpb	$45, %al
	je	.L1559
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
.L1407:
	movl	%r12d, %ecx
	andl	$16, %ecx
	cmpb	$73, %al
	je	.L1560
.L1409:
	testl	%ecx, %ecx
	setne	%sil
	cmpb	$48, %al
	je	.L1561
	movl	$1, %r11d
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
.L1417:
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1562:
	movslq	%r13d, %rdi
	addl	$1, %r13d
	movb	%al, -848(%rbp,%rdi)
.L1430:
	cmpb	$55, %al
	setbe	%al
	addq	$1, %rbx
	andl	%eax, %r10d
	cmpq	%r15, %rbx
	je	.L1431
.L1432:
	movzbl	(%rbx), %eax
	leal	-48(%rax), %edi
	cmpb	$9, %dil
	ja	.L1428
	cmpl	$771, %r13d
	jle	.L1562
	addl	$1, %edx
	cmpb	$48, %al
	setne	%dil
	orl	%edi, %r12d
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	%r13, %xmm0
.L1399:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1563
	addq	$824, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1558:
	.cfi_restore_state
	leaq	1(%rbx), %rdx
	cmpq	%r15, %rdx
	je	.L1408
	movzbl	1(%rbx), %eax
	movl	%r12d, %ecx
	movq	%rdx, %rbx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movl	$2, %r14d
	andl	$16, %ecx
	cmpb	$73, %al
	jne	.L1409
.L1560:
	addq	$1, %rbx
	cmpq	%rbx, %r15
	je	.L1408
	leaq	1+_ZZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_idE15kInfinityString(%rip), %rdx
	movl	$110, %eax
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1565:
	movsbl	1(%rdx), %eax
	addq	$1, %rdx
	addq	$1, %rbx
	testb	%al, %al
	je	.L1564
	cmpq	%r15, %rbx
	je	.L1408
.L1410:
	movzbl	(%rbx), %esi
	cmpl	%eax, %esi
	je	.L1565
	.p2align 4,,10
	.p2align 3
.L1408:
	movsd	.LC22(%rip), %xmm0
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1559:
	leaq	1(%rbx), %rdx
	cmpq	%r15, %rdx
	je	.L1408
	movzbl	1(%rbx), %eax
	movl	$1, %r8d
	movq	%rdx, %rbx
	movl	$1, %r14d
	movl	$1, %edx
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1428:
	testl	%r13d, %r13d
	jne	.L1433
	cmpb	$46, %al
	je	.L1566
	testb	%r9b, %r9b
	je	.L1408
	xorl	%r9d, %r9d
.L1451:
	andl	$-33, %eax
	xorl	%r10d, %r10d
	cmpb	$69, %al
	je	.L1475
.L1471:
	testl	%ecx, %ecx
	je	.L1462
.L1465:
	addl	%r9d, %edx
	.p2align 4,,10
	.p2align 3
.L1431:
	testb	%r10b, %r10b
	jne	.L1439
.L1441:
	testb	%r12b, %r12b
	je	.L1468
	movslq	%r13d, %rax
	subl	$1, %edx
	addl	$1, %r13d
	movb	$49, -848(%rbp,%rax)
.L1468:
	movslq	%r13d, %rsi
	leaq	-848(%rbp), %rdi
	movb	$0, -848(%rbp,%rsi)
	call	_ZN2v88internal6StrtodENS0_6VectorIKcEEi@PLT
	cmpl	$1, %r14d
	jne	.L1399
	xorpd	.LC11(%rip), %xmm0
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1561:
	leaq	1(%rbx), %rax
	cmpq	%r15, %rax
	je	.L1552
	testb	$1, %r12b
	je	.L1420
	movzbl	1(%rbx), %edi
	andl	$-33, %edi
	cmpb	$88, %dil
	jne	.L1420
	leaq	2(%rbx), %rdi
	cmpq	%r15, %rdi
	je	.L1408
	movzbl	2(%rbx), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L1421
	movzbl	%al, %edx
	cmpl	$96, %edx
	jle	.L1422
	cmpl	$102, %edx
	jg	.L1408
.L1421:
	testl	%r14d, %r14d
	jne	.L1408
	movzbl	%sil, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal25InternalStringToIntDoubleILi4EPKhS3_EEdT0_T1_bb
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1420:
	testb	$2, %r12b
	jne	.L1567
.L1423:
	testb	$8, %r12b
	je	.L1426
	movzbl	1(%rbx), %edi
	andl	$-33, %edi
	cmpb	$66, %dil
	jne	.L1426
	leaq	2(%rbx), %rdi
	cmpq	%r15, %rdi
	je	.L1408
	movzbl	2(%rbx), %eax
	subl	$48, %eax
	cmpl	$1, %eax
	ja	.L1408
	testb	%dl, %dl
	jne	.L1408
	movzbl	%sil, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal25InternalStringToIntDoubleILi1EPKhS3_EEdT0_T1_bb
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1433:
	cmpb	$46, %al
	je	.L1568
	andl	$-33, %eax
	xorl	%r9d, %r9d
	cmpb	$69, %al
	jne	.L1471
	testb	%r10b, %r10b
	jne	.L1408
	xorl	%r9d, %r9d
.L1475:
	leaq	1(%rbx), %rax
	cmpq	%r15, %rax
	je	.L1469
	movzbl	1(%rbx), %edi
	leal	-43(%rdi), %r10d
	andl	$253, %r10d
	jne	.L1481
	leaq	2(%rbx), %rax
	cmpq	%r15, %rax
	je	.L1469
	movl	%edi, %r11d
	movzbl	2(%rbx), %edi
.L1453:
	subl	$48, %edi
	cmpb	$9, %dil
	jbe	.L1569
.L1469:
	testl	%ecx, %ecx
	je	.L1408
.L1474:
	addl	%r9d, %edx
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1564:
	testl	%ecx, %ecx
	je	.L1412
.L1413:
	movsd	.LC23(%rip), %xmm0
	cmpl	$1, %r14d
	je	.L1399
	movsd	.LC13(%rip), %xmm0
	jmp	.L1399
.L1485:
	testb	%r10b, %r10b
	je	.L1546
	.p2align 4,,10
	.p2align 3
.L1439:
	leaq	-848(%rbp), %rdi
	movslq	%r13d, %r13
	movzbl	%sil, %ecx
	movl	%r8d, %edx
	leaq	(%rdi,%r13), %rsi
	call	_ZN2v88internal25InternalStringToIntDoubleILi3EPcS2_EEdT0_T1_bb
	jmp	.L1399
.L1572:
	cmpb	$48, 1(%rbx)
	jne	.L1480
	addq	$2, %rbx
	cmpq	%rbx, %r15
	je	.L1552
	leal	-1(%rbx), %eax
.L1444:
	movl	%eax, %r9d
	subl	%ebx, %r9d
	cmpb	$48, (%rbx)
	jne	.L1450
	addq	$1, %rbx
	cmpq	%r15, %rbx
	jne	.L1444
	.p2align 4,,10
	.p2align 3
.L1552:
	pxor	%xmm0, %xmm0
	cmpl	$1, %r14d
	jne	.L1399
	movsd	.LC21(%rip), %xmm0
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1570:
	addq	$1, %rax
	cmpq	%r15, %rax
	je	.L1552
.L1426:
	cmpb	$48, (%rax)
	je	.L1570
	shrl	$2, %r12d
	movq	%rax, %rbx
	xorl	%r11d, %r11d
	movl	$1, %r9d
	movl	%r12d, %r10d
	andl	$1, %r10d
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1567:
	movzbl	1(%rbx), %edi
	andl	$-33, %edi
	cmpb	$79, %dil
	jne	.L1423
	leaq	2(%rbx), %rdi
	cmpq	%r15, %rdi
	je	.L1408
	movzbl	2(%rbx), %eax
	subl	$48, %eax
	cmpb	$7, %al
	ja	.L1408
	testb	%dl, %dl
	jne	.L1408
	movzbl	%sil, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal25InternalStringToIntDoubleILi3EPKhS3_EEdT0_T1_bb
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1568:
	testl	%ecx, %ecx
	jne	.L1485
	testb	%r10b, %r10b
	jne	.L1408
.L1546:
	addq	$1, %rbx
	cmpq	%rbx, %r15
	je	.L1441
.L1554:
	xorl	%r9d, %r9d
	jmp	.L1450
	.p2align 4,,10
	.p2align 3
.L1571:
	movslq	%r13d, %rdi
	subl	$1, %r9d
	addl	$1, %r13d
	movb	%al, -848(%rbp,%rdi)
.L1448:
	addq	$1, %rbx
	cmpq	%r15, %rbx
	je	.L1474
.L1450:
	movzbl	(%rbx), %eax
	leal	-48(%rax), %edi
	cmpb	$9, %dil
	ja	.L1446
	cmpl	$771, %r13d
	jle	.L1571
	cmpb	$48, %al
	setne	%al
	orl	%eax, %r12d
	jmp	.L1448
.L1566:
	leaq	1(%rbx), %rax
	cmpq	%rax, %r15
	jne	.L1572
	testb	%r9b, %r9b
	je	.L1408
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1412:
	cmpq	%r15, %rbx
	je	.L1413
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r12
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1414:
	movslq	%edi, %rdi
	testb	$8, (%r12,%rdi)
	je	.L1408
.L1416:
	addq	$1, %rbx
	cmpq	%rbx, %r15
	je	.L1413
.L1415:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L1414
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	je	.L1408
	jmp	.L1416
.L1462:
	cmpq	%rbx, %r15
	je	.L1465
.L1463:
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rcx
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1464:
	movslq	%edi, %rdi
	testb	$8, (%rcx,%rdi)
	je	.L1408
.L1467:
	addq	$1, %rbx
	cmpq	%rbx, %r15
	je	.L1465
.L1466:
	movzbl	(%rbx), %edi
	testb	%dil, %dil
	jns	.L1464
	movl	%r8d, -864(%rbp)
	movb	%sil, -858(%rbp)
	movb	%r10b, -857(%rbp)
	movl	%edx, -856(%rbp)
	movl	%r9d, -852(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movl	-852(%rbp), %r9d
	movl	-856(%rbp), %edx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rcx
	testb	%al, %al
	movzbl	-857(%rbp), %r10d
	movzbl	-858(%rbp), %esi
	movl	-864(%rbp), %r8d
	je	.L1408
	jmp	.L1467
.L1422:
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L1408
	jmp	.L1421
.L1481:
	movl	$43, %r11d
	jmp	.L1453
.L1446:
	testl	%r9d, %r9d
	jne	.L1451
	testb	%r11b, %r11b
	je	.L1451
	testl	%r13d, %r13d
	je	.L1408
	jmp	.L1451
.L1569:
	xorl	%edi, %edi
	jmp	.L1458
.L1573:
	cmpl	$3, %r10d
	jg	.L1482
.L1455:
	leal	(%rdi,%rdi,4), %edi
	leal	(%r10,%rdi,2), %edi
.L1456:
	addq	$1, %rax
	cmpq	%r15, %rax
	je	.L1457
	movzbl	(%rax), %ebx
	leal	-48(%rbx), %r10d
	cmpb	$9, %r10b
	ja	.L1457
.L1458:
	movzbl	(%rax), %r10d
	subl	$48, %r10d
	cmpl	$107374181, %edi
	jle	.L1455
	cmpl	$107374182, %edi
	je	.L1573
.L1482:
	movl	$1073741823, %edi
	jmp	.L1456
.L1457:
	movl	%edi, %r10d
	negl	%r10d
	cmpb	$45, %r11b
	cmove	%r10d, %edi
	addl	%edi, %r9d
	testl	%ecx, %ecx
	jne	.L1474
	cmpq	%rax, %r15
	je	.L1474
	movq	%rax, %rbx
	xorl	%r10d, %r10d
	jmp	.L1463
.L1563:
	call	__stack_chk_fail@PLT
.L1480:
	movq	%rax, %rbx
	jmp	.L1554
	.cfi_endproc
.LFE19715:
	.size	_ZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_id, .-_ZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_id
	.section	.text._ZN2v88internal14StringToDoubleENS0_6VectorIKhEEid,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14StringToDoubleENS0_6VectorIKhEEid
	.type	_ZN2v88internal14StringToDoubleENS0_6VectorIKhEEid, @function
_ZN2v88internal14StringToDoubleENS0_6VectorIKhEEid:
.LFB17832:
	.cfi_startproc
	endbr64
	addq	%rdi, %rsi
	jmp	_ZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_id
	.cfi_endproc
.LFE17832:
	.size	_ZN2v88internal14StringToDoubleENS0_6VectorIKhEEid, .-_ZN2v88internal14StringToDoubleENS0_6VectorIKhEEid
	.section	.text._ZN2v88internal14StringToDoubleEPKcid,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14StringToDoubleEPKcid
	.type	_ZN2v88internal14StringToDoubleEPKcid, @function
_ZN2v88internal14StringToDoubleEPKcid:
.LFB17831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movsd	%xmm0, -24(%rbp)
	call	strlen@PLT
	movsd	-24(%rbp), %xmm0
	movl	%r13d, %edx
	movq	%r12, %rdi
	addq	$16, %rsp
	leaq	(%r12,%rax), %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_id
	.cfi_endproc
.LFE17831:
	.size	_ZN2v88internal14StringToDoubleEPKcid, .-_ZN2v88internal14StringToDoubleEPKcid
	.section	.text._ZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_id,"axG",@progbits,_ZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_id,comdat
	.p2align 4
	.weak	_ZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_id
	.type	_ZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_id, @function
_ZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_id:
.LFB19716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%xmm0, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$840, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rdi, %rsi
	jne	.L1578
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1580:
	movslq	%r12d, %r12
	testb	$8, (%r15,%r12)
	je	.L1585
.L1583:
	addq	$2, %rbx
	cmpq	%rbx, %rsi
	je	.L1582
.L1578:
	movzwl	(%rbx), %r12d
	movl	%r12d, %eax
	cmpw	$127, %r12w
	jbe	.L1580
	movl	%r12d, %edi
	movq	%rsi, -856(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movq	-856(%rbp), %rsi
	testb	%al, %al
	jne	.L1583
	subl	$8232, %r12d
	cmpl	$1, %r12d
	jbe	.L1583
	movzwl	(%rbx), %eax
.L1585:
	movl	%r13d, %ecx
	andl	$16, %ecx
	cmpw	$43, %ax
	je	.L1734
	cmpw	$45, %ax
	je	.L1735
	xorl	%edi, %edi
	xorl	%edx, %edx
	xorl	%r12d, %r12d
.L1587:
	cmpw	$73, %ax
	je	.L1736
.L1589:
	testl	%ecx, %ecx
	setne	%r8b
	cmpw	$48, %ax
	je	.L1737
	movl	$1, %r11d
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
.L1598:
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1738:
	movslq	%r14d, %rdi
	addl	$1, %r14d
	movb	%al, -848(%rbp,%rdi)
.L1611:
	cmpw	$55, %ax
	setbe	%al
	addq	$2, %rbx
	andl	%eax, %r10d
	cmpq	%rsi, %rbx
	je	.L1612
.L1613:
	movzwl	(%rbx), %eax
	leal	-48(%rax), %edi
	cmpw	$9, %di
	ja	.L1609
	cmpl	$771, %r14d
	jle	.L1738
	addl	$1, %r15d
	cmpw	$48, %ax
	setne	%dil
	orl	%edi, %r13d
	jmp	.L1611
	.p2align 4,,10
	.p2align 3
.L1582:
	movq	%r14, %xmm0
.L1577:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1739
	addq	$840, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1734:
	.cfi_restore_state
	leaq	2(%rbx), %rdx
	cmpq	%rsi, %rdx
	je	.L1588
	movzwl	2(%rbx), %eax
	movl	$1, %edi
	movq	%rdx, %rbx
	movl	$2, %r12d
	xorl	%edx, %edx
	cmpw	$73, %ax
	jne	.L1589
.L1736:
	addq	$2, %rbx
	cmpq	%rbx, %rsi
	je	.L1588
	leaq	1+_ZZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_idE15kInfinityString(%rip), %rdx
	movl	$110, %eax
	jmp	.L1590
	.p2align 4,,10
	.p2align 3
.L1741:
	movsbl	1(%rdx), %eax
	addq	$1, %rdx
	addq	$2, %rbx
	testb	%al, %al
	je	.L1740
	cmpq	%rbx, %rsi
	je	.L1588
.L1590:
	movzwl	(%rbx), %edi
	cmpl	%eax, %edi
	je	.L1741
	.p2align 4,,10
	.p2align 3
.L1588:
	movsd	.LC22(%rip), %xmm0
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1609:
	testl	%r14d, %r14d
	jne	.L1614
	cmpw	$46, %ax
	je	.L1742
	testb	%r9b, %r9b
	je	.L1588
	xorl	%r9d, %r9d
.L1633:
	andl	$-33, %eax
	xorl	%r10d, %r10d
	cmpw	$69, %ax
	je	.L1658
.L1654:
	testl	%ecx, %ecx
	je	.L1644
.L1648:
	addl	%r9d, %r15d
	.p2align 4,,10
	.p2align 3
.L1612:
	testb	%r10b, %r10b
	jne	.L1620
.L1622:
	testb	%r13b, %r13b
	je	.L1651
	movslq	%r14d, %rax
	subl	$1, %r15d
	addl	$1, %r14d
	movb	$49, -848(%rbp,%rax)
.L1651:
	movslq	%r14d, %rsi
	leaq	-848(%rbp), %rdi
	movl	%r15d, %edx
	movb	$0, -848(%rbp,%rsi)
	call	_ZN2v88internal6StrtodENS0_6VectorIKcEEi@PLT
	cmpl	$1, %r12d
	jne	.L1577
	xorpd	.LC11(%rip), %xmm0
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1735:
	leaq	2(%rbx), %rdx
	cmpq	%rsi, %rdx
	je	.L1588
	movzwl	2(%rbx), %eax
	movl	$1, %edi
	movq	%rdx, %rbx
	movl	$1, %r12d
	movl	$1, %edx
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1737:
	leaq	2(%rbx), %rax
	cmpq	%rsi, %rax
	je	.L1728
	testb	$1, %r13b
	je	.L1601
	movzwl	2(%rbx), %r9d
	andl	$-33, %r9d
	cmpw	$88, %r9w
	jne	.L1601
	leaq	4(%rbx), %rdi
	cmpq	%rsi, %rdi
	je	.L1588
	movzwl	4(%rbx), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L1602
	movzwl	%ax, %edx
	cmpl	$96, %edx
	jle	.L1603
	cmpl	$102, %edx
	jg	.L1588
.L1602:
	testl	%r12d, %r12d
	jne	.L1588
	movzbl	%r8b, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal25InternalStringToIntDoubleILi4EPKtS3_EEdT0_T1_bb
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1601:
	testb	$2, %r13b
	jne	.L1743
.L1604:
	testb	$8, %r13b
	je	.L1607
	movzwl	2(%rbx), %r9d
	andl	$-33, %r9d
	cmpw	$66, %r9w
	jne	.L1607
	leaq	4(%rbx), %r9
	cmpq	%rsi, %r9
	je	.L1588
	movzwl	4(%rbx), %eax
	subl	$48, %eax
	cmpl	$1, %eax
	ja	.L1588
	testb	%dil, %dil
	jne	.L1588
	movzbl	%r8b, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal25InternalStringToIntDoubleILi1EPKtS3_EEdT0_T1_bb
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1614:
	cmpw	$46, %ax
	je	.L1744
	andl	$-33, %eax
	xorl	%r9d, %r9d
	cmpw	$69, %ax
	jne	.L1654
	testb	%r10b, %r10b
	jne	.L1588
	xorl	%r9d, %r9d
.L1658:
	leaq	2(%rbx), %rax
	cmpq	%rsi, %rax
	je	.L1652
	movzwl	2(%rbx), %edi
	leal	-43(%rdi), %r10d
	testw	$-3, %r10w
	jne	.L1664
	leaq	4(%rbx), %rax
	cmpq	%rsi, %rax
	je	.L1652
	movl	%edi, %r11d
	movzwl	4(%rbx), %edi
.L1635:
	subl	$48, %edi
	cmpw	$9, %di
	jbe	.L1745
.L1652:
	testl	%ecx, %ecx
	je	.L1588
.L1657:
	addl	%r9d, %r15d
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1740:
	testl	%ecx, %ecx
	je	.L1592
.L1593:
	movsd	.LC23(%rip), %xmm0
	cmpl	$1, %r12d
	je	.L1577
	movsd	.LC13(%rip), %xmm0
	jmp	.L1577
.L1668:
	testb	%r10b, %r10b
	je	.L1721
	.p2align 4,,10
	.p2align 3
.L1620:
	leaq	-848(%rbp), %rdi
	movslq	%r14d, %rsi
	movzbl	%r8b, %ecx
	addq	%rdi, %rsi
	call	_ZN2v88internal25InternalStringToIntDoubleILi3EPcS2_EEdT0_T1_bb
	jmp	.L1577
.L1748:
	cmpw	$48, 2(%rbx)
	jne	.L1663
	leaq	4(%rbx), %rax
	xorl	%r9d, %r9d
	cmpq	%rax, %rsi
	je	.L1728
.L1626:
	subl	$1, %r9d
	cmpw	$48, (%rax)
	movq	%rax, %rbx
	jne	.L1632
	addq	$2, %rax
	cmpq	%rax, %rsi
	jne	.L1626
	.p2align 4,,10
	.p2align 3
.L1728:
	pxor	%xmm0, %xmm0
	cmpl	$1, %r12d
	jne	.L1577
	movsd	.LC21(%rip), %xmm0
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1746:
	addq	$2, %rax
	cmpq	%rsi, %rax
	je	.L1728
.L1607:
	cmpw	$48, (%rax)
	je	.L1746
	shrl	$2, %r13d
	movq	%rax, %rbx
	xorl	%r11d, %r11d
	movl	$1, %r9d
	movl	%r13d, %r10d
	andl	$1, %r10d
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1743:
	movzwl	2(%rbx), %r9d
	andl	$-33, %r9d
	cmpw	$79, %r9w
	jne	.L1604
	leaq	4(%rbx), %r9
	cmpq	%rsi, %r9
	je	.L1588
	movzwl	4(%rbx), %eax
	subl	$48, %eax
	cmpw	$7, %ax
	ja	.L1588
	testb	%dil, %dil
	jne	.L1588
	movzbl	%r8b, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal25InternalStringToIntDoubleILi3EPKtS3_EEdT0_T1_bb
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1744:
	testl	%ecx, %ecx
	jne	.L1668
	testb	%r10b, %r10b
	jne	.L1588
.L1721:
	addq	$2, %rbx
	cmpq	%rbx, %rsi
	je	.L1622
.L1730:
	xorl	%r9d, %r9d
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1747:
	movslq	%r14d, %rdi
	subl	$1, %r9d
	addl	$1, %r14d
	movb	%al, -848(%rbp,%rdi)
.L1630:
	addq	$2, %rbx
	cmpq	%rsi, %rbx
	je	.L1657
.L1632:
	movzwl	(%rbx), %eax
	leal	-48(%rax), %edi
	cmpw	$9, %di
	ja	.L1628
	cmpl	$771, %r14d
	jle	.L1747
	cmpw	$48, %ax
	setne	%al
	orl	%eax, %r13d
	jmp	.L1630
.L1742:
	leaq	2(%rbx), %rax
	cmpq	%rax, %rsi
	jne	.L1748
	testb	%r9b, %r9b
	je	.L1588
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1592:
	cmpq	%rbx, %rsi
	je	.L1593
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1594:
	movslq	%r13d, %r13
	testb	$8, (%r14,%r13)
	je	.L1588
.L1597:
	addq	$2, %rbx
	cmpq	%rbx, %rsi
	je	.L1593
.L1596:
	movzwl	(%rbx), %r13d
	cmpw	$127, %r13w
	jbe	.L1594
	movl	%r13d, %edi
	movq	%rsi, -856(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movq	-856(%rbp), %rsi
	testb	%al, %al
	jne	.L1597
	subl	$8232, %r13d
	cmpl	$1, %r13d
	ja	.L1588
	jmp	.L1597
.L1644:
	cmpq	%rsi, %rbx
	je	.L1648
.L1645:
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r11
	jmp	.L1649
	.p2align 4,,10
	.p2align 3
.L1646:
	movslq	%ecx, %rcx
	testb	$8, (%r11,%rcx)
	je	.L1588
.L1650:
	addq	$2, %rbx
	cmpq	%rbx, %rsi
	je	.L1648
.L1649:
	movzwl	(%rbx), %ecx
	cmpw	$127, %cx
	jbe	.L1646
	movl	%ecx, %edi
	movq	%rsi, -880(%rbp)
	movb	%r8b, -862(%rbp)
	movl	%edx, -868(%rbp)
	movb	%r10b, -861(%rbp)
	movl	%r9d, -860(%rbp)
	movl	%ecx, -856(%rbp)
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movl	-856(%rbp), %ecx
	movl	-860(%rbp), %r9d
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r11
	testb	%al, %al
	movzbl	-861(%rbp), %r10d
	movl	-868(%rbp), %edx
	movzbl	-862(%rbp), %r8d
	movq	-880(%rbp), %rsi
	jne	.L1650
	subl	$8232, %ecx
	cmpl	$1, %ecx
	ja	.L1588
	jmp	.L1650
.L1603:
	subl	$65, %eax
	cmpw	$5, %ax
	ja	.L1588
	jmp	.L1602
.L1664:
	movl	$43, %r11d
	jmp	.L1635
.L1628:
	testl	%r9d, %r9d
	jne	.L1633
	testb	%r11b, %r11b
	je	.L1633
	testl	%r14d, %r14d
	je	.L1588
	jmp	.L1633
.L1745:
	xorl	%edi, %edi
	jmp	.L1640
.L1749:
	cmpl	$3, %r10d
	jg	.L1665
.L1637:
	leal	(%rdi,%rdi,4), %edi
	leal	(%r10,%rdi,2), %edi
.L1638:
	addq	$2, %rax
	cmpq	%rsi, %rax
	je	.L1639
	movzwl	(%rax), %ebx
	leal	-48(%rbx), %r10d
	cmpw	$9, %r10w
	ja	.L1639
.L1640:
	movzwl	(%rax), %r10d
	subl	$48, %r10d
	cmpl	$107374181, %edi
	jle	.L1637
	cmpl	$107374182, %edi
	je	.L1749
.L1665:
	movl	$1073741823, %edi
	jmp	.L1638
.L1639:
	movl	%edi, %r10d
	negl	%r10d
	cmpb	$45, %r11b
	cmove	%r10d, %edi
	addl	%edi, %r9d
	testl	%ecx, %ecx
	jne	.L1657
	cmpq	%rsi, %rax
	je	.L1657
	movq	%rax, %rbx
	xorl	%r10d, %r10d
	jmp	.L1645
.L1739:
	call	__stack_chk_fail@PLT
.L1663:
	movq	%rax, %rbx
	jmp	.L1730
	.cfi_endproc
.LFE19716:
	.size	_ZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_id, .-_ZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_id
	.section	.text._ZN2v88internal14StringToDoubleENS0_6VectorIKtEEid,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14StringToDoubleENS0_6VectorIKtEEid
	.type	_ZN2v88internal14StringToDoubleENS0_6VectorIKtEEid, @function
_ZN2v88internal14StringToDoubleENS0_6VectorIKtEEid:
.LFB17834:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	_ZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_id
	.cfi_endproc
.LFE17834:
	.size	_ZN2v88internal14StringToDoubleENS0_6VectorIKtEEid, .-_ZN2v88internal14StringToDoubleENS0_6VectorIKtEEid
	.section	.text._ZN2v88internal14IsSpecialIndexENS0_6StringE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14IsSpecialIndexENS0_6StringE
	.type	_ZN2v88internal14IsSpecialIndexENS0_6StringE, @function
_ZN2v88internal14IsSpecialIndexENS0_6StringE:
.LFB17863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	11(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.L1752
	cmpl	$24, %ebx
	jg	.L1752
	leaq	-112(%rbp), %r12
	movl	%ebx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	movzwl	-112(%rbp), %eax
	movl	%eax, %edi
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L1770
	cmpw	$45, %di
	je	.L1793
	cmpl	$8, %ebx
	jne	.L1773
	cmpw	$73, %di
	je	.L1772
.L1773:
	cmpw	$78, %di
	jne	.L1752
	cmpl	$3, %ebx
	je	.L1794
	.p2align 4,,10
	.p2align 3
.L1752:
	xorl	%eax, %eax
.L1759:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L1795
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1770:
	.cfi_restore_state
	movl	%ebx, %eax
	xorl	%r8d, %r8d
.L1753:
	cmpl	$15, %eax
	jg	.L1760
	cmpl	%ebx, %r8d
	jge	.L1761
.L1768:
	leal	-1(%rbx), %edx
	movslq	%r8d, %rcx
	subl	%r8d, %edx
	leaq	(%r12,%rcx,2), %rax
	addq	%rcx, %rdx
	movl	$1, %ecx
	leaq	-110(%rbp,%rdx,2), %rsi
	.p2align 4,,10
	.p2align 3
.L1762:
	movzwl	(%rax), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	setbe	%dl
	addq	$2, %rax
	andl	%edx, %ecx
	cmpq	%rax, %rsi
	jne	.L1762
	testb	%cl, %cl
	jne	.L1761
	movslq	%ebx, %rax
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	leaq	(%r12,%rax,2), %rsi
	call	_ZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_id
	ucomisd	%xmm0, %xmm0
	jp	.L1752
	leaq	-64(%rbp), %rdi
	movl	$25, %esi
	call	_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE
.L1766:
	xorl	%edx, %edx
	jmp	.L1765
	.p2align 4,,10
	.p2align 3
.L1796:
	addq	$1, %rdx
	cmpl	%edx, %ebx
	jle	.L1764
.L1765:
	movsbw	(%rax,%rdx), %cx
	cmpw	(%r12,%rdx,2), %cx
	je	.L1796
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1760:
	movslq	%ebx, %rax
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	leaq	(%r12,%rax,2), %rsi
	call	_ZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_id
	ucomisd	%xmm0, %xmm0
	jp	.L1752
	leaq	-64(%rbp), %rdi
	movl	$25, %esi
	call	_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE
	testl	%ebx, %ebx
	jg	.L1766
	.p2align 4,,10
	.p2align 3
.L1764:
	movl	$1, %eax
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1793:
	cmpl	$1, %ebx
	je	.L1752
	movzwl	-110(%rbp), %eax
	movl	%eax, %edi
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L1797
	cmpl	$9, %ebx
	jne	.L1752
	cmpw	$73, %di
	jne	.L1752
	movl	$1, %r8d
.L1756:
	cmpl	%r8d, %ebx
	jle	.L1764
	movl	$73, %edi
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1761:
	cmpw	$48, %di
	jne	.L1764
	subl	$1, %ebx
	cmpl	%r8d, %ebx
	sete	%al
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1797:
	leal	-1(%rbx), %eax
	movl	$1, %r8d
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1794:
	cmpw	$97, -110(%rbp)
	jne	.L1752
	cmpw	$78, -108(%rbp)
	jne	.L1752
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1772:
	xorl	%r8d, %r8d
	jmp	.L1756
.L1795:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17863:
	.size	_ZN2v88internal14IsSpecialIndexENS0_6StringE, .-_ZN2v88internal14IsSpecialIndexENS0_6StringE
	.section	.text._ZN2v88internal14StringToDoubleEPNS0_7IsolateENS0_6HandleINS0_6StringEEEid,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14StringToDoubleEPNS0_7IsolateENS0_6HandleINS0_6StringEEEid
	.type	_ZN2v88internal14StringToDoubleEPNS0_7IsolateENS0_6HandleINS0_6StringEEEid, @function
_ZN2v88internal14StringToDoubleEPNS0_7IsolateENS0_6HandleINS0_6StringEEEid:
.LFB17862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edx, %r12d
	subq	$56, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	%rdx, %r13
	movq	-1(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L1820
.L1800:
	movq	-1(%r13), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1809
.L1819:
	movq	(%rax), %rsi
.L1808:
	movq	%rsi, -48(%rbp)
	leaq	-48(%rbp), %rdi
	leaq	-49(%rbp), %rsi
	movsd	%xmm0, -72(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movsd	-72(%rbp), %xmm0
	movq	%rax, %rdi
	movq	%rdx, %rax
	movslq	%edx, %rdx
	shrq	$32, %rax
	cmpl	$1, %eax
	je	.L1821
	leaq	(%rdi,%rdx,2), %rsi
	movl	%r12d, %edx
	call	_ZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_id
.L1816:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1822
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1809:
	.cfi_restore_state
	movq	-1(%r13), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L1819
	movq	(%rax), %rax
	movq	41112(%r14), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1812
	movsd	%xmm0, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movsd	-72(%rbp), %xmm0
	movq	(%rax), %rsi
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1820:
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpw	$1, %cx
	jne	.L1800
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L1803
	movq	23(%rdx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L1803
	xorl	%edx, %edx
	movsd	%xmm0, -72(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movsd	-72(%rbp), %xmm0
	movq	(%rax), %rsi
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1821:
	leaq	(%rdi,%rdx), %rsi
	movl	%r12d, %edx
	call	_ZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_id
	jmp	.L1816
	.p2align 4,,10
	.p2align 3
.L1812:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1823
.L1814:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1803:
	movq	41112(%r14), %rdi
	movq	15(%rdx), %r13
	testq	%rdi, %rdi
	je	.L1805
	movq	%r13, %rsi
	movsd	%xmm0, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movsd	-72(%rbp), %xmm0
	movq	(%rax), %r13
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1805:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1824
.L1807:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%r13, (%rax)
	jmp	.L1800
.L1824:
	movq	%r14, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movsd	-72(%rbp), %xmm0
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1823:
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	movsd	%xmm0, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movsd	-80(%rbp), %xmm0
	movq	-72(%rbp), %rsi
	jmp	.L1814
.L1822:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17862:
	.size	_ZN2v88internal14StringToDoubleEPNS0_7IsolateENS0_6HandleINS0_6StringEEEid, .-_ZN2v88internal14StringToDoubleEPNS0_7IsolateENS0_6HandleINS0_6StringEEEid
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal17StringToIntHelper8ParseIntEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal17StringToIntHelper8ParseIntEv, @function
_GLOBAL__sub_I__ZN2v88internal17StringToIntHelper8ParseIntEv:
.LFB21569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21569:
	.size	_GLOBAL__sub_I__ZN2v88internal17StringToIntHelper8ParseIntEv, .-_GLOBAL__sub_I__ZN2v88internal17StringToIntHelper8ParseIntEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal17StringToIntHelper8ParseIntEv
	.weak	_ZTVN2v88internal20NumberParseIntHelperE
	.section	.data.rel.ro.local._ZTVN2v88internal20NumberParseIntHelperE,"awG",@progbits,_ZTVN2v88internal20NumberParseIntHelperE,comdat
	.align 8
	.type	_ZTVN2v88internal20NumberParseIntHelperE, @object
	.size	_ZTVN2v88internal20NumberParseIntHelperE, 56
_ZTVN2v88internal20NumberParseIntHelperE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal20NumberParseIntHelperD1Ev
	.quad	_ZN2v88internal20NumberParseIntHelperD0Ev
	.quad	_ZN2v88internal20NumberParseIntHelper14AllocateResultEv
	.quad	_ZN2v88internal20NumberParseIntHelper17ResultMultiplyAddEjj
	.quad	_ZN2v88internal20NumberParseIntHelper18HandleSpecialCasesEv
	.weak	_ZTVN2v88internal20StringToBigIntHelperE
	.section	.data.rel.ro.local._ZTVN2v88internal20StringToBigIntHelperE,"awG",@progbits,_ZTVN2v88internal20StringToBigIntHelperE,comdat
	.align 8
	.type	_ZTVN2v88internal20StringToBigIntHelperE, @object
	.size	_ZTVN2v88internal20StringToBigIntHelperE, 56
_ZTVN2v88internal20StringToBigIntHelperE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal20StringToBigIntHelperD1Ev
	.quad	_ZN2v88internal20StringToBigIntHelperD0Ev
	.quad	_ZN2v88internal20StringToBigIntHelper14AllocateResultEv
	.quad	_ZN2v88internal20StringToBigIntHelper17ResultMultiplyAddEjj
	.quad	_ZN2v88internal17StringToIntHelper18HandleSpecialCasesEv
	.weak	_ZZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_idE15kInfinityString
	.section	.rodata._ZZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_idE15kInfinityString,"aG",@progbits,_ZZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_idE15kInfinityString,comdat
	.align 8
	.type	_ZZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_idE15kInfinityString, @gnu_unique_object
	.size	_ZZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_idE15kInfinityString, 9
_ZZN2v88internal22InternalStringToDoubleIPKtS3_EEdT_T0_idE15kInfinityString:
	.string	"Infinity"
	.weak	_ZZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_idE15kInfinityString
	.section	.rodata._ZZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_idE15kInfinityString,"aG",@progbits,_ZZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_idE15kInfinityString,comdat
	.align 8
	.type	_ZZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_idE15kInfinityString, @gnu_unique_object
	.size	_ZZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_idE15kInfinityString, 9
_ZZN2v88internal22InternalStringToDoubleIPKhS3_EEdT_T0_idE15kInfinityString:
	.string	"Infinity"
	.section	.rodata._ZZN2v88internal20DoubleToRadixCStringEdiE5chars,"a"
	.align 32
	.type	_ZZN2v88internal20DoubleToRadixCStringEdiE5chars, @object
	.size	_ZZN2v88internal20DoubleToRadixCStringEdiE5chars, 37
_ZZN2v88internal20DoubleToRadixCStringEdiE5chars:
	.string	"0123456789abcdefghijklmnopqrstuvwxyz"
	.section	.rodata._ZN2v88internalL15kAsciiCharFlagsE,"a"
	.align 32
	.type	_ZN2v88internalL15kAsciiCharFlagsE, @object
	.size	_ZN2v88internalL15kAsciiCharFlagsE, 128
_ZN2v88internalL15kAsciiCharFlagsE:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\f\b\f\f\b"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\f"
	.string	""
	.string	""
	.string	"\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\002\002\002\002\002\002\002\002\002\002"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	"\003"
	.string	""
	.string	"\003"
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC5:
	.long	4294967295
	.long	2146435071
	.align 8
.LC6:
	.long	0
	.long	1048576
	.align 8
.LC7:
	.long	0
	.long	0
	.align 8
.LC8:
	.long	0
	.long	-1042284544
	.align 8
.LC9:
	.long	4290772992
	.long	1105199103
	.section	.rodata.cst16
	.align 16
.LC11:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC12:
	.long	3605196624
	.long	1145772772
	.align 8
.LC13:
	.long	0
	.long	2146435072
	.align 8
.LC14:
	.long	1
	.long	0
	.align 8
.LC15:
	.long	0
	.long	1127219200
	.align 8
.LC16:
	.long	0
	.long	1072693248
	.align 8
.LC17:
	.long	0
	.long	1071644672
	.section	.rodata.cst16
	.align 16
.LC20:
	.long	0
	.long	0
	.long	0
	.long	2
	.section	.rodata.cst8
	.align 8
.LC21:
	.long	0
	.long	-2147483648
	.align 8
.LC22:
	.long	0
	.long	2146959360
	.align 8
.LC23:
	.long	0
	.long	-1048576
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
