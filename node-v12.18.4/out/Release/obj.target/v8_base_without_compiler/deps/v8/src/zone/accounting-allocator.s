	.file	"accounting-allocator.cc"
	.text
	.section	.text._ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE,"axG",@progbits,_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE
	.type	_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE, @function
_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE:
.LFB2980:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2980:
	.size	_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE, .-_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE
	.section	.text._ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE,"axG",@progbits,_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE
	.type	_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE, @function
_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE:
.LFB2981:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2981:
	.size	_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE, .-_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE
	.section	.text._ZN2v88internal19AccountingAllocatorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AccountingAllocatorD2Ev
	.type	_ZN2v88internal19AccountingAllocatorD2Ev, @function
_ZN2v88internal19AccountingAllocatorD2Ev:
.LFB3671:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3671:
	.size	_ZN2v88internal19AccountingAllocatorD2Ev, .-_ZN2v88internal19AccountingAllocatorD2Ev
	.globl	_ZN2v88internal19AccountingAllocatorD1Ev
	.set	_ZN2v88internal19AccountingAllocatorD1Ev,_ZN2v88internal19AccountingAllocatorD2Ev
	.section	.text._ZN2v88internal19AccountingAllocatorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AccountingAllocatorD0Ev
	.type	_ZN2v88internal19AccountingAllocatorD0Ev, @function
_ZN2v88internal19AccountingAllocatorD0Ev:
.LFB3673:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3673:
	.size	_ZN2v88internal19AccountingAllocatorD0Ev, .-_ZN2v88internal19AccountingAllocatorD0Ev
	.section	.text._ZN2v88internal19AccountingAllocator15AllocateSegmentEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AccountingAllocator15AllocateSegmentEm
	.type	_ZN2v88internal19AccountingAllocator15AllocateSegmentEm, @function
_ZN2v88internal19AccountingAllocator15AllocateSegmentEm:
.LFB3674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	_ZN2v88internal14AllocWithRetryEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L6
	movq	%r12, %rdx
	lock xaddq	%rdx, 8(%rbx)
	movq	16(%rbx), %rax
	addq	%r12, %rdx
	leaq	16(%rbx), %rcx
.L9:
	cmpq	%rax, %rdx
	jbe	.L8
	lock cmpxchgq	%rdx, (%rcx)
	jne	.L9
.L8:
	movq	%r12, 16(%r8)
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%r8)
.L6:
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3674:
	.size	_ZN2v88internal19AccountingAllocator15AllocateSegmentEm, .-_ZN2v88internal19AccountingAllocator15AllocateSegmentEm
	.section	.text._ZN2v88internal19AccountingAllocator13ReturnSegmentEPNS0_7SegmentE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AccountingAllocator13ReturnSegmentEPNS0_7SegmentE
	.type	_ZN2v88internal19AccountingAllocator13ReturnSegmentEPNS0_7SegmentE, @function
_ZN2v88internal19AccountingAllocator13ReturnSegmentEPNS0_7SegmentE:
.LFB3675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	_ZN2v88internal7Segment11ZapContentsEv@PLT
	movq	16(%r12), %rax
	lock subq	%rax, 8(%rbx)
	movq	%r12, %rdi
	call	_ZN2v88internal7Segment9ZapHeaderEv@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.cfi_endproc
.LFE3675:
	.size	_ZN2v88internal19AccountingAllocator13ReturnSegmentEPNS0_7SegmentE, .-_ZN2v88internal19AccountingAllocator13ReturnSegmentEPNS0_7SegmentE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19AccountingAllocatorD2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19AccountingAllocatorD2Ev, @function
_GLOBAL__sub_I__ZN2v88internal19AccountingAllocatorD2Ev:
.LFB4264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE4264:
	.size	_GLOBAL__sub_I__ZN2v88internal19AccountingAllocatorD2Ev, .-_GLOBAL__sub_I__ZN2v88internal19AccountingAllocatorD2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19AccountingAllocatorD2Ev
	.weak	_ZTVN2v88internal19AccountingAllocatorE
	.section	.data.rel.ro.local._ZTVN2v88internal19AccountingAllocatorE,"awG",@progbits,_ZTVN2v88internal19AccountingAllocatorE,comdat
	.align 8
	.type	_ZTVN2v88internal19AccountingAllocatorE, @object
	.size	_ZTVN2v88internal19AccountingAllocatorE, 64
_ZTVN2v88internal19AccountingAllocatorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19AccountingAllocatorD1Ev
	.quad	_ZN2v88internal19AccountingAllocatorD0Ev
	.quad	_ZN2v88internal19AccountingAllocator15AllocateSegmentEm
	.quad	_ZN2v88internal19AccountingAllocator13ReturnSegmentEPNS0_7SegmentE
	.quad	_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE
	.quad	_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
