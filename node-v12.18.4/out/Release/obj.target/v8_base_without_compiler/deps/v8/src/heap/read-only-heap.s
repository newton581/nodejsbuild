	.file	"read-only-heap.cc"
	.text
	.section	.text._ZN2v88internal5Space15CommittedMemoryEv,"axG",@progbits,_ZN2v88internal5Space15CommittedMemoryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Space15CommittedMemoryEv
	.type	_ZN2v88internal5Space15CommittedMemoryEv, @function
_ZN2v88internal5Space15CommittedMemoryEv:
.LFB9213:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE9213:
	.size	_ZN2v88internal5Space15CommittedMemoryEv, .-_ZN2v88internal5Space15CommittedMemoryEv
	.section	.text._ZN2v88internal5Space22MaximumCommittedMemoryEv,"axG",@progbits,_ZN2v88internal5Space22MaximumCommittedMemoryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.type	_ZN2v88internal5Space22MaximumCommittedMemoryEv, @function
_ZN2v88internal5Space22MaximumCommittedMemoryEv:
.LFB9214:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	ret
	.cfi_endproc
.LFE9214:
	.size	_ZN2v88internal5Space22MaximumCommittedMemoryEv, .-_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.section	.text._ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi,"axG",@progbits,_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.type	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi, @function
_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi:
.LFB9216:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	andl	$-8, %eax
	cmpl	$3, 72(%rdi)
	je	.L7
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%esi, %eax
	andl	$-32, %eax
	ret
	.cfi_endproc
.LFE9216:
	.size	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi, .-_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.section	.text._ZN2v88internal10PagedSpace4SizeEv,"axG",@progbits,_ZN2v88internal10PagedSpace4SizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace4SizeEv
	.type	_ZN2v88internal10PagedSpace4SizeEv, @function
_ZN2v88internal10PagedSpace4SizeEv:
.LFB9392:
	.cfi_startproc
	endbr64
	movq	184(%rdi), %rax
	ret
	.cfi_endproc
.LFE9392:
	.size	_ZN2v88internal10PagedSpace4SizeEv, .-_ZN2v88internal10PagedSpace4SizeEv
	.section	.text._ZN2v88internal10PagedSpace8is_localEv,"axG",@progbits,_ZN2v88internal10PagedSpace8is_localEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace8is_localEv
	.type	_ZN2v88internal10PagedSpace8is_localEv, @function
_ZN2v88internal10PagedSpace8is_localEv:
.LFB9403:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE9403:
	.size	_ZN2v88internal10PagedSpace8is_localEv, .-_ZN2v88internal10PagedSpace8is_localEv
	.section	.text._ZN2v88internal10PagedSpace12snapshotableEv,"axG",@progbits,_ZN2v88internal10PagedSpace12snapshotableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace12snapshotableEv
	.type	_ZN2v88internal10PagedSpace12snapshotableEv, @function
_ZN2v88internal10PagedSpace12snapshotableEv:
.LFB9410:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE9410:
	.size	_ZN2v88internal10PagedSpace12snapshotableEv, .-_ZN2v88internal10PagedSpace12snapshotableEv
	.section	.text._ZN2v88internal13ReadOnlySpace9AvailableEv,"axG",@progbits,_ZN2v88internal13ReadOnlySpace9AvailableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ReadOnlySpace9AvailableEv
	.type	_ZN2v88internal13ReadOnlySpace9AvailableEv, @function
_ZN2v88internal13ReadOnlySpace9AvailableEv:
.LFB9507:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE9507:
	.size	_ZN2v88internal13ReadOnlySpace9AvailableEv, .-_ZN2v88internal13ReadOnlySpace9AvailableEv
	.section	.text._ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE,"axG",@progbits,_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.type	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE, @function
_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE:
.LFB9219:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE9219:
	.size	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE, .-_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.section	.text._ZN2v88internal10PagedSpace5WasteEv,"axG",@progbits,_ZN2v88internal10PagedSpace5WasteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace5WasteEv
	.type	_ZN2v88internal10PagedSpace5WasteEv, @function
_ZN2v88internal10PagedSpace5WasteEv:
.LFB9393:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	movq	24(%rax), %rax
	ret
	.cfi_endproc
.LFE9393:
	.size	_ZN2v88internal10PagedSpace5WasteEv, .-_ZN2v88internal10PagedSpace5WasteEv
	.section	.text._ZN2v88internal10PagedSpace24SupportsInlineAllocationEv,"axG",@progbits,_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.type	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv, @function
_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv:
.LFB9409:
	.cfi_startproc
	endbr64
	cmpl	$2, 72(%rdi)
	je	.L22
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*152(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE9409:
	.size	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv, .-_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.section	.text._ZN2v88internal13ReadOnlySpaceD2Ev,"axG",@progbits,_ZN2v88internal13ReadOnlySpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ReadOnlySpaceD2Ev
	.type	_ZN2v88internal13ReadOnlySpaceD2Ev, @function
_ZN2v88internal13ReadOnlySpaceD2Ev:
.LFB9503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal13ReadOnlySpaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal13ReadOnlySpace6UnsealEv@PLT
	leaq	16+_ZTVN2v88internal10PagedSpaceE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	call	_ZN2v88internal10PagedSpace8TearDownEv@PLT
	leaq	192(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	48(%rbx), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L24
	call	_ZdaPv@PLT
.L24:
	movq	96(%rbx), %rdi
	movq	$0, 48(%rbx)
	testq	%rdi, %rdi
	je	.L25
	movq	(%rdi), %rax
	call	*8(%rax)
.L25:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L23
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9503:
	.size	_ZN2v88internal13ReadOnlySpaceD2Ev, .-_ZN2v88internal13ReadOnlySpaceD2Ev
	.weak	_ZN2v88internal13ReadOnlySpaceD1Ev
	.set	_ZN2v88internal13ReadOnlySpaceD1Ev,_ZN2v88internal13ReadOnlySpaceD2Ev
	.section	.text._ZN2v88internal13ReadOnlySpaceD0Ev,"axG",@progbits,_ZN2v88internal13ReadOnlySpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ReadOnlySpaceD0Ev
	.type	_ZN2v88internal13ReadOnlySpaceD0Ev, @function
_ZN2v88internal13ReadOnlySpaceD0Ev:
.LFB9505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal13ReadOnlySpaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal13ReadOnlySpace6UnsealEv@PLT
	leaq	16+_ZTVN2v88internal10PagedSpaceE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN2v88internal10PagedSpace8TearDownEv@PLT
	leaq	192(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	48(%r12), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L35
	call	_ZdaPv@PLT
.L35:
	movq	$0, 48(%r12)
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L36
	movq	(%rdi), %rax
	call	*8(%rax)
.L36:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE9505:
	.size	_ZN2v88internal13ReadOnlySpaceD0Ev, .-_ZN2v88internal13ReadOnlySpaceD0Ev
	.section	.text._ZN2v88internal12ReadOnlyHeap5SetUpEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12ReadOnlyHeap5SetUpEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE
	.type	_ZN2v88internal12ReadOnlyHeap5SetUpEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE, @function
_ZN2v88internal12ReadOnlyHeap5SetUpEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE:
.LFB19986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$240, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZN2v88internal8MallocednwEm@PLT
	leaq	37592(%r12), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal13ReadOnlySpaceC1EPNS0_4HeapE@PLT
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%r12, %rdi
	movb	$0, (%rax)
	movq	%rax, %rsi
	movq	%rax, %rbx
	movq	%r13, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	call	_ZN2v88internal7Isolate21SetUpFromReadOnlyHeapEPNS0_12ReadOnlyHeapE@PLT
	testq	%r14, %r14
	je	.L48
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE@PLT
	movq	8(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v88internal13ReadOnlySpace4SealENS1_8SealModeE@PLT
	movb	$1, (%rbx)
.L48:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19986:
	.size	_ZN2v88internal12ReadOnlyHeap5SetUpEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE, .-_ZN2v88internal12ReadOnlyHeap5SetUpEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE
	.section	.text._ZN2v88internal12ReadOnlyHeap21DeseralizeIntoIsolateEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12ReadOnlyHeap21DeseralizeIntoIsolateEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE
	.type	_ZN2v88internal12ReadOnlyHeap21DeseralizeIntoIsolateEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE, @function
_ZN2v88internal12ReadOnlyHeap21DeseralizeIntoIsolateEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE:
.LFB19987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal20ReadOnlyDeserializer15DeserializeIntoEPNS0_7IsolateE@PLT
	movq	8(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v88internal13ReadOnlySpace4SealENS1_8SealModeE@PLT
	movb	$1, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19987:
	.size	_ZN2v88internal12ReadOnlyHeap21DeseralizeIntoIsolateEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE, .-_ZN2v88internal12ReadOnlyHeap21DeseralizeIntoIsolateEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE
	.section	.text._ZN2v88internal12ReadOnlyHeap27OnCreateHeapObjectsCompleteEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12ReadOnlyHeap27OnCreateHeapObjectsCompleteEPNS0_7IsolateE
	.type	_ZN2v88internal12ReadOnlyHeap27OnCreateHeapObjectsCompleteEPNS0_7IsolateE, @function
_ZN2v88internal12ReadOnlyHeap27OnCreateHeapObjectsCompleteEPNS0_7IsolateE:
.LFB19988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	_ZN2v88internal13ReadOnlySpace4SealENS1_8SealModeE@PLT
	movb	$1, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19988:
	.size	_ZN2v88internal12ReadOnlyHeap27OnCreateHeapObjectsCompleteEPNS0_7IsolateE, .-_ZN2v88internal12ReadOnlyHeap27OnCreateHeapObjectsCompleteEPNS0_7IsolateE
	.section	.text._ZN2v88internal12ReadOnlyHeap24CreateAndAttachToIsolateEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12ReadOnlyHeap24CreateAndAttachToIsolateEPNS0_7IsolateE
	.type	_ZN2v88internal12ReadOnlyHeap24CreateAndAttachToIsolateEPNS0_7IsolateE, @function
_ZN2v88internal12ReadOnlyHeap24CreateAndAttachToIsolateEPNS0_7IsolateE:
.LFB19989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$240, %edi
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	_ZN2v88internal8MallocednwEm@PLT
	leaq	37592(%r13), %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal13ReadOnlySpaceC1EPNS0_4HeapE@PLT
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%rbx, 8(%rax)
	movq	%rax, %rsi
	movq	%rax, %r12
	movb	$0, (%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	call	_ZN2v88internal7Isolate21SetUpFromReadOnlyHeapEPNS0_12ReadOnlyHeapE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19989:
	.size	_ZN2v88internal12ReadOnlyHeap24CreateAndAttachToIsolateEPNS0_7IsolateE, .-_ZN2v88internal12ReadOnlyHeap24CreateAndAttachToIsolateEPNS0_7IsolateE
	.section	.text._ZN2v88internal12ReadOnlyHeap15InitFromIsolateEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12ReadOnlyHeap15InitFromIsolateEPNS0_7IsolateE
	.type	_ZN2v88internal12ReadOnlyHeap15InitFromIsolateEPNS0_7IsolateE, @function
_ZN2v88internal12ReadOnlyHeap15InitFromIsolateEPNS0_7IsolateE:
.LFB25872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	_ZN2v88internal13ReadOnlySpace4SealENS1_8SealModeE@PLT
	movb	$1, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25872:
	.size	_ZN2v88internal12ReadOnlyHeap15InitFromIsolateEPNS0_7IsolateE, .-_ZN2v88internal12ReadOnlyHeap15InitFromIsolateEPNS0_7IsolateE
	.section	.text._ZN2v88internal12ReadOnlyHeap14OnHeapTearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12ReadOnlyHeap14OnHeapTearDownEv
	.type	_ZN2v88internal12ReadOnlyHeap14OnHeapTearDownEv, @function
_ZN2v88internal12ReadOnlyHeap14OnHeapTearDownEv:
.LFB19991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	8(%rdi), %r12
	testq	%r12, %r12
	je	.L63
	movq	(%r12), %rax
	leaq	_ZN2v88internal13ReadOnlySpaceD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L64
	leaq	16+_ZTVN2v88internal13ReadOnlySpaceE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN2v88internal13ReadOnlySpace6UnsealEv@PLT
	leaq	16+_ZTVN2v88internal10PagedSpaceE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN2v88internal10PagedSpace8TearDownEv@PLT
	leaq	192(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	48(%r12), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L65
	call	_ZdaPv@PLT
.L65:
	movq	$0, 48(%r12)
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L66
	movq	(%rdi), %rax
	call	*8(%rax)
.L66:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	%r12, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L63:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	_ZdlPv@PLT
.L68:
	popq	%r12
	movq	%r13, %rdi
	movl	$40, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L63
	.cfi_endproc
.LFE19991:
	.size	_ZN2v88internal12ReadOnlyHeap14OnHeapTearDownEv, .-_ZN2v88internal12ReadOnlyHeap14OnHeapTearDownEv
	.section	.text._ZN2v88internal12ReadOnlyHeap22ClearSharedHeapForTestEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12ReadOnlyHeap22ClearSharedHeapForTestEv
	.type	_ZN2v88internal12ReadOnlyHeap22ClearSharedHeapForTestEv, @function
_ZN2v88internal12ReadOnlyHeap22ClearSharedHeapForTestEv:
.LFB19995:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE19995:
	.size	_ZN2v88internal12ReadOnlyHeap22ClearSharedHeapForTestEv, .-_ZN2v88internal12ReadOnlyHeap22ClearSharedHeapForTestEv
	.section	.text._ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE
	.type	_ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE, @function
_ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE:
.LFB19996:
	.cfi_startproc
	endbr64
	andq	$-262144, %rdi
	movq	8(%rdi), %rax
	shrq	$21, %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE19996:
	.size	_ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE, .-_ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE
	.section	.rodata._ZN2v88internal12ReadOnlyHeap25ExtendReadOnlyObjectCacheEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal12ReadOnlyHeap25ExtendReadOnlyObjectCacheEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12ReadOnlyHeap25ExtendReadOnlyObjectCacheEv
	.type	_ZN2v88internal12ReadOnlyHeap25ExtendReadOnlyObjectCacheEv, @function
_ZN2v88internal12ReadOnlyHeap25ExtendReadOnlyObjectCacheEv:
.LFB19997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	cmpq	32(%rdi), %r12
	je	.L88
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	movq	%rax, (%r12)
	movq	24(%rdi), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 24(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	16(%rdi), %r14
	movq	%r12, %rcx
	movabsq	$1152921504606846975, %rdx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L115
	testq	%rax, %rax
	je	.L99
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L116
.L91:
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rdx
.L92:
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	movq	%rax, 0(%r13,%rcx)
	cmpq	%r14, %r12
	je	.L102
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L103
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L103
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L95:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L95
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L97
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L97:
	leaq	8(%r13,%rsi), %rax
	leaq	8(%rax), %rdx
.L93:
	testq	%r14, %r14
	je	.L98
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rdx
.L98:
	movq	%r13, %xmm0
	movq	%rdx, %xmm2
	movq	%r15, 32(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L117
	movl	$8, %edx
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$8, %r15d
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L94:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L94
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%r13, %rax
	jmp	.L93
.L115:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L117:
	cmpq	%rdx, %rsi
	cmovbe	%rsi, %rdx
	movq	%rdx, %r15
	salq	$3, %r15
	jmp	.L91
	.cfi_endproc
.LFE19997:
	.size	_ZN2v88internal12ReadOnlyHeap25ExtendReadOnlyObjectCacheEv, .-_ZN2v88internal12ReadOnlyHeap25ExtendReadOnlyObjectCacheEv
	.section	.text._ZNK2v88internal12ReadOnlyHeap23cached_read_only_objectEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12ReadOnlyHeap23cached_read_only_objectEm
	.type	_ZNK2v88internal12ReadOnlyHeap23cached_read_only_objectEm, @function
_ZNK2v88internal12ReadOnlyHeap23cached_read_only_objectEm:
.LFB19998:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE19998:
	.size	_ZNK2v88internal12ReadOnlyHeap23cached_read_only_objectEm, .-_ZNK2v88internal12ReadOnlyHeap23cached_read_only_objectEm
	.section	.text._ZNK2v88internal12ReadOnlyHeap37read_only_object_cache_is_initializedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12ReadOnlyHeap37read_only_object_cache_is_initializedEv
	.type	_ZNK2v88internal12ReadOnlyHeap37read_only_object_cache_is_initializedEv, @function
_ZNK2v88internal12ReadOnlyHeap37read_only_object_cache_is_initializedEv:
.LFB19999:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	cmpq	%rax, 16(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE19999:
	.size	_ZNK2v88internal12ReadOnlyHeap37read_only_object_cache_is_initializedEv, .-_ZNK2v88internal12ReadOnlyHeap37read_only_object_cache_is_initializedEv
	.section	.text._ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_12ReadOnlyHeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_12ReadOnlyHeapE
	.type	_ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_12ReadOnlyHeapE, @function
_ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_12ReadOnlyHeapE:
.LFB20001:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	movq	%rax, (%rdi)
	movq	32(%rax), %rax
	movq	%rax, 8(%rdi)
	movq	40(%rax), %rax
	movq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE20001:
	.size	_ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_12ReadOnlyHeapE, .-_ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_12ReadOnlyHeapE
	.globl	_ZN2v88internal26ReadOnlyHeapObjectIteratorC1EPNS0_12ReadOnlyHeapE
	.set	_ZN2v88internal26ReadOnlyHeapObjectIteratorC1EPNS0_12ReadOnlyHeapE,_ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_12ReadOnlyHeapE
	.section	.text._ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_13ReadOnlySpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_13ReadOnlySpaceE
	.type	_ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_13ReadOnlySpaceE, @function
_ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_13ReadOnlySpaceE:
.LFB20004:
	.cfi_startproc
	endbr64
	movq	32(%rsi), %rax
	movq	%rsi, (%rdi)
	movq	%rax, 8(%rdi)
	movq	40(%rax), %rax
	movq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE20004:
	.size	_ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_13ReadOnlySpaceE, .-_ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_13ReadOnlySpaceE
	.globl	_ZN2v88internal26ReadOnlyHeapObjectIteratorC1EPNS0_13ReadOnlySpaceE
	.set	_ZN2v88internal26ReadOnlyHeapObjectIteratorC1EPNS0_13ReadOnlySpaceE,_ZN2v88internal26ReadOnlyHeapObjectIteratorC2EPNS0_13ReadOnlySpaceE
	.section	.text._ZN2v88internal26ReadOnlyHeapObjectIterator4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ReadOnlyHeapObjectIterator4NextEv
	.type	_ZN2v88internal26ReadOnlyHeapObjectIterator4NextEv, @function
_ZN2v88internal26ReadOnlyHeapObjectIterator4NextEv:
.LFB20006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L125
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	leaq	-32(%rbp), %r12
	cmpq	%rax, 48(%rdx)
	je	.L135
.L126:
	movq	(%rbx), %rdx
	cmpq	%rax, 104(%rdx)
	jne	.L129
	movq	112(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L136
.L129:
	leaq	1(%rax), %rdx
	movq	%r12, %rdi
	movq	%rdx, -32(%rbp)
	movq	(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, 16(%rbx)
	movq	-32(%rbp), %rax
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	cmpw	$76, %dx
	je	.L131
	cmpw	$73, %dx
	je	.L131
.L125:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L137
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movq	%rdx, 16(%rbx)
	movq	%rdx, %rax
.L130:
	movq	8(%rbx), %rdx
	cmpq	%rax, 48(%rdx)
	jne	.L126
.L135:
	movq	224(%rdx), %rax
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.L125
	movq	40(%rax), %rax
	movq	%rax, 16(%rbx)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L131:
	movq	16(%rbx), %rax
	jmp	.L130
.L137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20006:
	.size	_ZN2v88internal26ReadOnlyHeapObjectIterator4NextEv, .-_ZN2v88internal26ReadOnlyHeapObjectIterator4NextEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal12ReadOnlyHeap5SetUpEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal12ReadOnlyHeap5SetUpEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE, @function
_GLOBAL__sub_I__ZN2v88internal12ReadOnlyHeap5SetUpEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE:
.LFB25819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25819:
	.size	_GLOBAL__sub_I__ZN2v88internal12ReadOnlyHeap5SetUpEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE, .-_GLOBAL__sub_I__ZN2v88internal12ReadOnlyHeap5SetUpEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal12ReadOnlyHeap5SetUpEPNS0_7IsolateEPNS0_20ReadOnlyDeserializerE
	.weak	_ZTVN2v88internal13ReadOnlySpaceE
	.section	.data.rel.ro._ZTVN2v88internal13ReadOnlySpaceE,"awG",@progbits,_ZTVN2v88internal13ReadOnlySpaceE,comdat
	.align 8
	.type	_ZTVN2v88internal13ReadOnlySpaceE, @object
	.size	_ZTVN2v88internal13ReadOnlySpaceE, 208
_ZTVN2v88internal13ReadOnlySpaceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal13ReadOnlySpaceD1Ev
	.quad	_ZN2v88internal13ReadOnlySpaceD0Ev
	.quad	_ZN2v88internal19SpaceWithLinearArea21AddAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal19SpaceWithLinearArea24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal19SpaceWithLinearArea24PauseAllocationObserversEv
	.quad	_ZN2v88internal19SpaceWithLinearArea25ResumeAllocationObserversEv
	.quad	_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv
	.quad	_ZN2v88internal5Space15CommittedMemoryEv
	.quad	_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.quad	_ZN2v88internal10PagedSpace4SizeEv
	.quad	_ZN2v88internal10PagedSpace13SizeOfObjectsEv
	.quad	_ZN2v88internal10PagedSpace23CommittedPhysicalMemoryEv
	.quad	_ZN2v88internal13ReadOnlySpace9AvailableEv
	.quad	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.quad	_ZN2v88internal10PagedSpace17GetObjectIteratorEv
	.quad	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.quad	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.quad	_ZN2v88internal10PagedSpace27UpdateInlineAllocationLimitEm
	.quad	_ZN2v88internal10PagedSpace5WasteEv
	.quad	_ZN2v88internal10PagedSpace8is_localEv
	.quad	_ZN2v88internal10PagedSpace14RefillFreeListEv
	.quad	_ZN2v88internal10PagedSpace12snapshotableEv
	.quad	_ZN2v88internal10PagedSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE
	.quad	_ZN2v88internal10PagedSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
