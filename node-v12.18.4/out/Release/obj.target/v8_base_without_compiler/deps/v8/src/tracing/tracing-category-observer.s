	.file	"tracing-category-observer.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB1935:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE1935:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE,"axG",@progbits,_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE
	.type	_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE, @function
_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE:
.LFB1939:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE1939:
	.size	_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE, .-_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE
	.section	.text._ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE,"axG",@progbits,_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE
	.type	_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE, @function
_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE:
.LFB1940:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE1940:
	.size	_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE, .-_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE
	.section	.text._ZN2v87tracing23TracingCategoryObserverD2Ev,"axG",@progbits,_ZN2v87tracing23TracingCategoryObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v87tracing23TracingCategoryObserverD2Ev
	.type	_ZN2v87tracing23TracingCategoryObserverD2Ev, @function
_ZN2v87tracing23TracingCategoryObserverD2Ev:
.LFB10454:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10454:
	.size	_ZN2v87tracing23TracingCategoryObserverD2Ev, .-_ZN2v87tracing23TracingCategoryObserverD2Ev
	.weak	_ZN2v87tracing23TracingCategoryObserverD1Ev
	.set	_ZN2v87tracing23TracingCategoryObserverD1Ev,_ZN2v87tracing23TracingCategoryObserverD2Ev
	.section	.text._ZN2v87tracing23TracingCategoryObserverD0Ev,"axG",@progbits,_ZN2v87tracing23TracingCategoryObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v87tracing23TracingCategoryObserverD0Ev
	.type	_ZN2v87tracing23TracingCategoryObserverD0Ev, @function
_ZN2v87tracing23TracingCategoryObserverD0Ev:
.LFB10456:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10456:
	.size	_ZN2v87tracing23TracingCategoryObserverD0Ev, .-_ZN2v87tracing23TracingCategoryObserverD0Ev
	.section	.text._ZN2v87tracing23TracingCategoryObserver15OnTraceDisabledEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing23TracingCategoryObserver15OnTraceDisabledEv
	.type	_ZN2v87tracing23TracingCategoryObserver15OnTraceDisabledEv, @function
_ZN2v87tracing23TracingCategoryObserver15OnTraceDisabledEv:
.LFB9163:
	.cfi_startproc
	endbr64
	lock andl	$-7, _ZN2v88internal12TracingFlags13runtime_statsE(%rip)
	lock andl	$-3, _ZN2v88internal12TracingFlags2gcE(%rip)
	lock andl	$-3, _ZN2v88internal12TracingFlags8gc_statsE(%rip)
	lock andl	$-3, _ZN2v88internal12TracingFlags8ic_statsE(%rip)
	ret
	.cfi_endproc
.LFE9163:
	.size	_ZN2v87tracing23TracingCategoryObserver15OnTraceDisabledEv, .-_ZN2v87tracing23TracingCategoryObserver15OnTraceDisabledEv
	.section	.rodata._ZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime_stats"
	.align 8
.LC1:
	.string	"disabled-by-default-v8.runtime_stats_sampling"
	.section	.rodata._ZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"disabled-by-default-v8.gc"
	.section	.rodata._ZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEv.str1.8
	.align 8
.LC3:
	.string	"disabled-by-default-v8.gc_stats"
	.align 8
.LC4:
	.string	"disabled-by-default-v8.ic_stats"
	.section	.text._ZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEv
	.type	_ZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEv, @function
_ZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEv:
.LFB9162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic31(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rax, %rax
	je	.L50
.L10:
	movzbl	(%rax), %eax
	testb	$5, %al
	jne	.L51
.L12:
	movq	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic37(%rip), %rax
	testq	%rax, %rax
	je	.L52
.L14:
	movzbl	(%rax), %eax
	testb	$5, %al
	jne	.L53
.L16:
	movq	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic43(%rip), %rax
	testq	%rax, %rax
	je	.L54
.L18:
	movzbl	(%rax), %eax
	testb	$5, %al
	jne	.L55
.L20:
	movq	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic48(%rip), %rax
	testq	%rax, %rax
	je	.L56
.L22:
	movzbl	(%rax), %eax
	testb	$5, %al
	jne	.L57
.L24:
	movq	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic54(%rip), %rax
	testq	%rax, %rax
	je	.L58
.L26:
	movzbl	(%rax), %eax
	testb	$5, %al
	jne	.L59
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	lock orl	$2, _ZN2v88internal12TracingFlags8ic_statsE(%rip)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L60
.L27:
	movq	%rax, _ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic54(%rip)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L57:
	lock orl	$2, _ZN2v88internal12TracingFlags8gc_statsE(%rip)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L56:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L61
.L23:
	movq	%rax, _ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic48(%rip)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L55:
	lock orl	$2, _ZN2v88internal12TracingFlags2gcE(%rip)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L54:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L62
.L19:
	movq	%rax, _ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic43(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L53:
	lock orl	$4, _ZN2v88internal12TracingFlags13runtime_statsE(%rip)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L52:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L63
.L15:
	movq	%rax, _ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic37(%rip)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L51:
	lock orl	$2, _ZN2v88internal12TracingFlags13runtime_statsE(%rip)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L50:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L64
.L11:
	movq	%rax, _ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic31(%rip)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	.LC4(%rip), %rsi
	call	*%rdx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	.LC0(%rip), %rsi
	call	*%rdx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	.LC1(%rip), %rsi
	call	*%rdx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	.LC2(%rip), %rsi
	call	*%rdx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	.LC3(%rip), %rsi
	call	*%rdx
	jmp	.L23
	.cfi_endproc
.LFE9162:
	.size	_ZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEv, .-_ZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEv
	.section	.text._ZN2v87tracing23TracingCategoryObserver5SetUpEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing23TracingCategoryObserver5SetUpEv
	.type	_ZN2v87tracing23TracingCategoryObserver5SetUpEv, @function
_ZN2v87tracing23TracingCategoryObserver5SetUpEv:
.LFB9150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Znwm@PLT
	leaq	16+_ZTVN2v87tracing23TracingCategoryObserverE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rax, _ZN2v87tracing23TracingCategoryObserver9instance_E(%rip)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*144(%rax)
	leaq	_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L68
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	_ZN2v87tracing23TracingCategoryObserver9instance_E(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE9150:
	.size	_ZN2v87tracing23TracingCategoryObserver5SetUpEv, .-_ZN2v87tracing23TracingCategoryObserver5SetUpEv
	.section	.text._ZN2v87tracing23TracingCategoryObserver8TearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing23TracingCategoryObserver8TearDownEv
	.type	_ZN2v87tracing23TracingCategoryObserver8TearDownEv, @function
_ZN2v87tracing23TracingCategoryObserver8TearDownEv:
.LFB9161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*144(%rax)
	leaq	_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE(%rip), %rdx
	movq	_ZN2v87tracing23TracingCategoryObserver9instance_E(%rip), %r8
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L73
.L70:
	testq	%r8, %r8
	je	.L69
	movq	(%r8), %rax
	movq	%r8, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	8(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	%r8, %rsi
	call	*%rax
	movq	_ZN2v87tracing23TracingCategoryObserver9instance_E(%rip), %r8
	jmp	.L70
	.cfi_endproc
.LFE9161:
	.size	_ZN2v87tracing23TracingCategoryObserver8TearDownEv, .-_ZN2v87tracing23TracingCategoryObserver8TearDownEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v87tracing23TracingCategoryObserver9instance_E,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v87tracing23TracingCategoryObserver9instance_E, @function
_GLOBAL__sub_I__ZN2v87tracing23TracingCategoryObserver9instance_E:
.LFB10474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10474:
	.size	_GLOBAL__sub_I__ZN2v87tracing23TracingCategoryObserver9instance_E, .-_GLOBAL__sub_I__ZN2v87tracing23TracingCategoryObserver9instance_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v87tracing23TracingCategoryObserver9instance_E
	.weak	_ZTVN2v87tracing23TracingCategoryObserverE
	.section	.data.rel.ro.local._ZTVN2v87tracing23TracingCategoryObserverE,"awG",@progbits,_ZTVN2v87tracing23TracingCategoryObserverE,comdat
	.align 8
	.type	_ZTVN2v87tracing23TracingCategoryObserverE, @object
	.size	_ZTVN2v87tracing23TracingCategoryObserverE, 48
_ZTVN2v87tracing23TracingCategoryObserverE:
	.quad	0
	.quad	0
	.quad	_ZN2v87tracing23TracingCategoryObserverD1Ev
	.quad	_ZN2v87tracing23TracingCategoryObserverD0Ev
	.quad	_ZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEv
	.quad	_ZN2v87tracing23TracingCategoryObserver15OnTraceDisabledEv
	.section	.bss._ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic54,"aw",@nobits
	.align 8
	.type	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic54, @object
	.size	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic54, 8
_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic54:
	.zero	8
	.section	.bss._ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic48,"aw",@nobits
	.align 8
	.type	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic48, @object
	.size	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic48, 8
_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic48:
	.zero	8
	.section	.bss._ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic43,"aw",@nobits
	.align 8
	.type	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic43, @object
	.size	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic43, 8
_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic43:
	.zero	8
	.section	.bss._ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic37,"aw",@nobits
	.align 8
	.type	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic37, @object
	.size	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic37, 8
_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic37:
	.zero	8
	.section	.bss._ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic31,"aw",@nobits
	.align 8
	.type	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic31, @object
	.size	_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic31, 8
_ZZN2v87tracing23TracingCategoryObserver14OnTraceEnabledEvE27trace_event_unique_atomic31:
	.zero	8
	.globl	_ZN2v87tracing23TracingCategoryObserver9instance_E
	.section	.bss._ZN2v87tracing23TracingCategoryObserver9instance_E,"aw",@nobits
	.align 8
	.type	_ZN2v87tracing23TracingCategoryObserver9instance_E, @object
	.size	_ZN2v87tracing23TracingCategoryObserver9instance_E, 8
_ZN2v87tracing23TracingCategoryObserver9instance_E:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
