	.file	"builtins-string.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internal12_GLOBAL__N_113NextCodePointEPNS0_7IsolateENS0_16BuiltinArgumentsEi.isra.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"(location_) != nullptr"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12_GLOBAL__N_113NextCodePointEPNS0_7IsolateENS0_16BuiltinArgumentsEi.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_113NextCodePointEPNS0_7IsolateENS0_16BuiltinArgumentsEi.isra.0, @function
_ZN2v88internal12_GLOBAL__N_113NextCodePointEPNS0_7IsolateENS0_16BuiltinArgumentsEi.isra.0:
.LFB25643:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	8(,%rdx,8), %edx
	movslq	%edx, %rdx
	subq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L47
.L12:
	movq	%rdx, %rcx
	pxor	%xmm0, %xmm0
	sarq	$32, %rcx
	cvtsi2sdl	%ecx, %xmm0
.L19:
	sarq	$32, %rdx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
.L24:
	ucomisd	%xmm0, %xmm1
	jp	.L17
	je	.L48
.L17:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$191, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movl	$-1, %eax
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	comisd	%xmm1, %xmm0
	ja	.L17
	comisd	.LC3(%rip), %xmm1
	ja	.L17
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L49
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L27:
	movsd	.LC5(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC4(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L28
	movsd	.LC6(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L50
.L28:
	movabsq	$9218868437227405312, %rax
	movq	%xmm0, %rdx
	testq	%rax, %rdx
	je	.L36
	movq	%xmm0, %rsi
	xorl	%eax, %eax
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L51
	cmpl	$31, %ecx
	jg	.L5
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %rax
	addq	%rsi, %rax
	salq	%cl, %rax
	movl	%eax, %eax
.L33:
	sarq	$63, %rdx
	addq	$8, %rsp
	orl	$1, %edx
	popq	%rbx
	popq	%r12
	imull	%edx, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L8
	xorl	%eax, %eax
.L9:
	testb	%al, %al
	jne	.L12
	movq	-1(%rdx), %rax
	movq	(%r12), %rdx
	cmpw	$65, 11(%rax)
	jne	.L13
	movq	%rdx, %rax
	movq	%r12, %rbx
	notq	%rax
	andl	$1, %eax
.L14:
	testb	%al, %al
	jne	.L12
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L52
	movq	(%rbx), %rdx
	movq	(%rax), %rcx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	testb	$1, %cl
	je	.L53
	movsd	7(%rcx), %xmm0
.L22:
	testb	%al, %al
	jne	.L19
	movsd	7(%rdx), %xmm1
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L49:
	movsd	7(%rax), %xmm0
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L51:
	cmpl	$-52, %ecx
	jl	.L5
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rax
	addq	%rcx, %rax
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L50:
	comisd	.LC7(%rip), %xmm0
	jb	.L28
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L28
	je	.L5
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%edx, %edx
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L54
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	testb	$1, %dl
	je	.L12
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L16
	xorl	%eax, %eax
	movq	%r12, %rbx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L17
	movq	(%rax), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%eax, %eax
	jmp	.L5
.L54:
	movq	(%rax), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L9
.L53:
	sarq	$32, %rcx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	jmp	.L22
	.cfi_endproc
.LFE25643:
	.size	_ZN2v88internal12_GLOBAL__N_113NextCodePointEPNS0_7IsolateENS0_16BuiltinArgumentsEi.isra.0, .-_ZN2v88internal12_GLOBAL__N_113NextCodePointEPNS0_7IsolateENS0_16BuiltinArgumentsEi.isra.0
	.section	.rodata._ZN2v88internalL41Builtin_Impl_StringPrototypeLocaleCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"String.prototype.localeCompare"
	.section	.text._ZN2v88internalL41Builtin_Impl_StringPrototypeLocaleCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_StringPrototypeLocaleCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_StringPrototypeLocaleCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20416:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$62, %esi
	subq	$56, %rsp
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	(%rbx), %rax
	cmpq	%rax, 104(%r12)
	jne	.L80
.L56:
	leaq	.LC8(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$30, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L81
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$27, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r15
.L62:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L75
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L75:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	cmpq	%rax, 88(%r12)
	je	.L56
	testb	$1, %al
	jne	.L57
.L60:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L79
.L59:
	leaq	88(%r12), %rax
	leaq	-8(%rbx), %rsi
	cmpl	$5, %r15d
	cmovle	%rax, %rsi
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L65
.L68:
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L79
.L67:
	cmpl	$7, %r15d
	jg	.L69
	leaq	88(%r12), %rcx
	movq	%rcx, %r8
	je	.L72
.L70:
	movq	%rsi, %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal4Intl19StringLocaleCompareEPNS0_7IsolateENS0_6HandleINS0_6StringEEES6_NS4_INS0_6ObjectEEES8_@PLT
	testq	%rax, %rax
	je	.L79
	movq	(%rax), %r15
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L81:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L79:
	movq	312(%r12), %r15
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	-24(%rbx), %r8
.L72:
	leaq	-16(%rbx), %rcx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L57:
	movq	-1(%rax), %rax
	movq	%rbx, %r9
	cmpw	$63, 11(%rax)
	jbe	.L59
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L65:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L68
	jmp	.L67
.L82:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20416:
	.size	_ZN2v88internalL41Builtin_Impl_StringPrototypeLocaleCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_StringPrototypeLocaleCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL22Builtin_Impl_StringRawENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"raw"
	.section	.text._ZN2v88internalL22Builtin_Impl_StringRawENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22Builtin_Impl_StringRawENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL22Builtin_Impl_StringRawENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20419:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-8(%rsi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-144(%rbp), %rbx
	subq	$200, %rsp
	movq	%rsi, -216(%rbp)
	movq	%rbx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	cmpl	$5, %edi
	movq	$3, -136(%rbp)
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -200(%rbp)
	leaq	88(%rdx), %rax
	cmovle	%rax, %r12
	leaq	.LC9(%rip), %rax
	xorl	%edx, %edx
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L165
	movq	%rax, %r15
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L87
.L90:
	movq	%r12, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L161
.L89:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L92
.L94:
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rdx
.L93:
	movq	(%r15), %rcx
	movl	$3, %eax
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	je	.L166
.L95:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%r13, -120(%rbp)
	movq	(%r15), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L167
.L96:
	movq	%rbx, %rdi
	movq	%r15, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L97
	movq	-120(%rbp), %rax
	leaq	88(%rax), %r12
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L99
.L102:
	movq	%r12, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L161
	movq	(%r12), %rax
	leaq	2768(%r13), %r9
	testb	$1, %al
	jne	.L103
.L105:
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-224(%rbp), %r9
	movq	%rax, %r15
.L104:
	movq	2768(%r13), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L106
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L106:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	2768(%r13), %rax
	movq	%r13, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L168
.L107:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r15, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L108
	movq	-120(%rbp), %rax
	movq	88(%rax), %rsi
	addq	$88, %rax
	testb	$1, %sil
	jne	.L110
.L170:
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r13), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L111
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L112:
	testq	%rax, %rax
	je	.L161
.L114:
	leaq	-192(%rbp), %r15
	movq	%r13, %rsi
	movq	%rax, -224(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rax
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L115
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L116:
	comisd	.LC10(%rip), %xmm0
	ja	.L154
	cvttsd2siq	%xmm0, %rax
	movl	%eax, -224(%rbp)
	testl	%eax, %eax
	jne	.L117
.L118:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	je	.L161
	movq	(%rax), %r12
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L123
	.p2align 4,,10
	.p2align 3
.L161:
	movq	312(%r13), %r12
.L91:
	movq	-208(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-200(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L148
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L148:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L169
	addq	$200, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L161
	movq	(%r12), %rax
	testb	$1, %al
	je	.L102
.L99:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L102
	movq	(%r12), %rax
	leaq	2768(%r13), %r9
	testb	$1, %al
	je	.L105
	.p2align 4,,10
	.p2align 3
.L103:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L105
	movq	%r12, %r15
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L166:
	movl	11(%rcx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L92:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L94
	movq	%r12, %rdx
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L87:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L90
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L161
	movq	(%rax), %rsi
	testb	$1, %sil
	je	.L170
.L110:
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L111:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L171
.L113:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L154:
	movl	$-1, -224(%rbp)
.L117:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L119
.L121:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
.L120:
	movq	%rax, -80(%rbp)
	movq	%rbx, %rdi
	movabsq	$824633720832, %rcx
	movabsq	$-4294967296, %rax
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	%r13, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L122
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rsi
.L123:
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L125
.L128:
	movq	%r13, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L161
.L127:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	cmpl	$1, -224(%rbp)
	je	.L118
	leal	-4(%r14), %eax
	movl	$2, %r14d
	movq	%r15, -232(%rbp)
	movl	%eax, -236(%rbp)
	leal	-1(%r14), %r15d
	cmpl	%r14d, -236(%rbp)
	jbe	.L130
.L172:
	movq	-216(%rbp), %rsi
	leal	0(,%r14,8), %eax
	cltq
	subq	%rax, %rsi
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L131
.L134:
	movq	%r13, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L161
.L133:
	movq	-232(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
.L130:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L136
.L138:
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
.L137:
	movabsq	$824633720832, %rcx
	movq	%rbx, %rdi
	movq	%r13, -120(%rbp)
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%r15d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L139
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rsi
.L140:
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L141
.L144:
	movq	%r13, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L161
.L143:
	movq	-232(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	leal	1(%r14), %eax
	cmpl	%r14d, -224(%rbp)
	je	.L159
	movl	%eax, %r14d
	leal	-1(%r14), %r15d
	cmpl	%r14d, -236(%rbp)
	ja	.L172
	movq	(%r12), %rax
	testb	$1, %al
	je	.L138
.L136:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L138
	movq	%r12, %rax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L115:
	movsd	7(%rax), %xmm0
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L139:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L140
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rdx, -224(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-224(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r9, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r9
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L141:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L144
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L119:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L121
	movq	%r12, %rax
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L131:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L134
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L125:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L128
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%r13, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	jmp	.L113
.L159:
	movq	-232(%rbp), %r15
	jmp	.L118
.L169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20419:
	.size	_ZN2v88internalL22Builtin_Impl_StringRawENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL22Builtin_Impl_StringRawENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L179
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L182
.L173:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L173
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL45Builtin_Impl_Stats_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC12:
	.string	"V8.Builtin_StringPrototypeLastIndexOf"
	.section	.text._ZN2v88internalL45Builtin_Impl_Stats_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_Stats_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_Stats_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE:
.LFB20411:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L218
.L184:
	movq	_ZZN2v88internalL45Builtin_Impl_Stats_StringPrototypeLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic124(%rip), %rbx
	testq	%rbx, %rbx
	je	.L219
.L186:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L220
.L188:
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	cmpl	$6, %r14d
	jg	.L192
	leaq	88(%r12), %rdx
	movq	%rdx, %rcx
	je	.L195
.L193:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String11LastIndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_@PLT
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L198
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L198:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L221
.L183:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L222
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	leaq	-16(%r13), %rcx
.L195:
	leaq	-8(%r13), %rdx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L220:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L223
.L189:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L190
	movq	(%rdi), %rax
	call	*8(%rax)
.L190:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L191
	movq	(%rdi), %rax
	call	*8(%rax)
.L191:
	leaq	.LC12(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L219:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L224
.L187:
	movq	%rbx, _ZZN2v88internalL45Builtin_Impl_Stats_StringPrototypeLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic124(%rip)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L218:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$837, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	.LC11(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L223:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L189
.L222:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20411:
	.size	_ZN2v88internalL45Builtin_Impl_Stats_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_Stats_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8.Builtin_StringPrototypeLocaleCompare"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE:
.LFB20414:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L254
.L226:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_StringPrototypeLocaleCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic135(%rip), %rbx
	testq	%rbx, %rbx
	je	.L255
.L228:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L256
.L230:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL41Builtin_Impl_StringPrototypeLocaleCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L257
.L234:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L258
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L259
.L229:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_StringPrototypeLocaleCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic135(%rip)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L256:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L260
.L231:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L232
	movq	(%rdi), %rax
	call	*8(%rax)
.L232:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L233
	movq	(%rdi), %rax
	call	*8(%rax)
.L233:
	leaq	.LC13(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L257:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L254:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$838, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L260:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L259:
	leaq	.LC11(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L229
.L258:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20414:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL28Builtin_Impl_Stats_StringRawEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"V8.Builtin_StringRaw"
	.section	.text._ZN2v88internalL28Builtin_Impl_Stats_StringRawEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Builtin_Impl_Stats_StringRawEiPmPNS0_7IsolateE, @function
_ZN2v88internalL28Builtin_Impl_Stats_StringRawEiPmPNS0_7IsolateE:
.LFB20417:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L290
.L262:
	movq	_ZZN2v88internalL28Builtin_Impl_Stats_StringRawEiPmPNS0_7IsolateEE28trace_event_unique_atomic422(%rip), %rbx
	testq	%rbx, %rbx
	je	.L291
.L264:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L292
.L266:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL22Builtin_Impl_StringRawENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L293
.L270:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L294
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L295
.L265:
	movq	%rbx, _ZZN2v88internalL28Builtin_Impl_Stats_StringRawEiPmPNS0_7IsolateEE28trace_event_unique_atomic422(%rip)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L292:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L296
.L267:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L268
	movq	(%rdi), %rax
	call	*8(%rax)
.L268:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L269
	movq	(%rdi), %rax
	call	*8(%rax)
.L269:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L293:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L290:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$839, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L296:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L295:
	leaq	.LC11(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L265
.L294:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20417:
	.size	_ZN2v88internalL28Builtin_Impl_Stats_StringRawEiPmPNS0_7IsolateE, .-_ZN2v88internalL28Builtin_Impl_Stats_StringRawEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Builtin_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Builtin_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Builtin_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Builtin_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE:
.LFB20412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L307
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	cmpl	$6, %edi
	jle	.L308
	leaq	-16(%rsi), %rcx
.L302:
	leaq	-8(%rsi), %rdx
.L300:
	movq	%r12, %rdi
	call	_ZN2v88internal6String11LastIndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L305
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L305:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	leaq	88(%rdx), %rdx
	movq	%rdx, %rcx
	jne	.L300
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L307:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL45Builtin_Impl_Stats_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20412:
	.size	_ZN2v88internal34Builtin_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE, .-_ZN2v88internal34Builtin_StringPrototypeLastIndexOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE:
.LFB20415:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L313
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL41Builtin_Impl_StringPrototypeLocaleCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore 6
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20415:
	.size	_ZN2v88internal36Builtin_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_StringPrototypeLocaleCompareEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal17Builtin_StringRawEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Builtin_StringRawEiPmPNS0_7IsolateE
	.type	_ZN2v88internal17Builtin_StringRawEiPmPNS0_7IsolateE, @function
_ZN2v88internal17Builtin_StringRawEiPmPNS0_7IsolateE:
.LFB20418:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L318
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL22Builtin_Impl_StringRawENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore 6
	jmp	_ZN2v88internalL28Builtin_Impl_Stats_StringRawEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20418:
	.size	_ZN2v88internal17Builtin_StringRawEiPmPNS0_7IsolateE, .-_ZN2v88internal17Builtin_StringRawEiPmPNS0_7IsolateE
	.section	.rodata._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC15:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.type	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, @function
_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_:
.LFB24704:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L333
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L328
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L334
.L330:
	movq	%rcx, %r14
.L321:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L327:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L335
	testq	%rsi, %rsi
	jg	.L323
	testq	%r15, %r15
	jne	.L326
.L324:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L323
.L326:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L334:
	testq	%r14, %r14
	js	.L330
	jne	.L321
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L324
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L328:
	movl	$1, %r14d
	jmp	.L321
.L333:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24704:
	.size	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, .-_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.section	.text._ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.type	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, @function
_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_:
.LFB24721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movabsq	$4611686018427387903, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%r9, %rcx
	subq	%r15, %rcx
	movq	%rcx, %rax
	sarq	%rax
	cmpq	%rsi, %rax
	je	.L350
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L346
	movabsq	$9223372036854775806, %r14
	cmpq	%rcx, %rax
	jbe	.L351
.L338:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L345:
	movzwl	0(%r13), %eax
	subq	%r8, %r9
	leaq	2(%rbx,%rdx), %r10
	movq	%r9, %r13
	movw	%ax, (%rbx,%rdx)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L352
	testq	%r9, %r9
	jg	.L341
	testq	%r15, %r15
	jne	.L344
.L342:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L341
.L344:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L351:
	testq	%rcx, %rcx
	jne	.L339
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L341:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L342
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L346:
	movl	$2, %r14d
	jmp	.L338
.L350:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L339:
	cmpq	%rsi, %rcx
	cmovbe	%rcx, %rsi
	movq	%rsi, %r14
	addq	%rsi, %r14
	jmp	.L338
	.cfi_endproc
.LFE24721:
	.size	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, .-_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.section	.rodata._ZN2v88internalL32Builtin_Impl_StringFromCodePointENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"vector::reserve"
	.section	.text._ZN2v88internalL32Builtin_Impl_StringFromCodePointENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Builtin_Impl_StringFromCodePointENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL32Builtin_Impl_StringFromCodePointENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20392:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	movq	%rax, -136(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -144(%rbp)
	movl	41104(%rdx), %eax
	leal	1(%rax), %edx
	movl	%edx, 41104(%r14)
	subl	$5, %ebx
	jne	.L354
	movl	%eax, 41104(%r14)
	movq	128(%r14), %r12
.L355:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L426
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	js	.L368
	movslq	%ebx, %r13
	movq	%rsi, %r12
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rdx
	movq	%rax, %r15
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L427
	testq	%r8, %r8
	jne	.L358
.L359:
	movq	%r15, %xmm0
	addq	%r13, %r15
	xorl	%r13d, %r13d
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, -96(%rbp)
	leaq	-80(%rbp), %r15
	movaps	%xmm0, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L365:
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_113NextCodePointEPNS0_7IsolateENS0_16BuiltinArgumentsEi.isra.0
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L425
	cmpl	$255, %eax
	jg	.L362
	movb	%al, -80(%rbp)
	movq	-104(%rbp), %rsi
	cmpq	-96(%rbp), %rsi
	je	.L363
	addl	$1, %r13d
	movb	%al, (%rsi)
	addq	$1, -104(%rbp)
	cmpl	%r13d, %ebx
	jg	.L365
.L362:
	cmpl	%r13d, %ebx
	je	.L428
	movl	%ebx, %eax
	pxor	%xmm0, %xmm0
	movabsq	$4611686018427387903, %rdx
	movl	%ecx, -152(%rbp)
	subl	%r13d, %eax
	movq	$0, -64(%rbp)
	cltq
	movaps	%xmm0, -80(%rbp)
	cmpq	%rdx, %rax
	ja	.L368
	leaq	(%rax,%rax), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	-80(%rbp), %r11
	movq	-72(%rbp), %rdx
	movl	-152(%rbp), %ecx
	movq	%rax, %r10
	subq	%r11, %rdx
	testq	%rdx, %rdx
	jg	.L429
	testq	%r11, %r11
	jne	.L370
.L371:
	movq	%r10, %xmm0
	leaq	(%r10,%r15), %rdx
	leaq	-114(%rbp), %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L430:
	movw	%cx, -114(%rbp)
	cmpq	%rdx, %r10
	je	.L373
	movw	%cx, (%r10)
	addq	$2, -72(%rbp)
.L375:
	addl	$1, %r13d
	cmpl	%r13d, %ebx
	je	.L380
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_113NextCodePointEPNS0_7IsolateENS0_16BuiltinArgumentsEi.isra.0
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L424
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rdx
.L382:
	movl	%ecx, %r11d
	cmpl	$65535, %ecx
	jle	.L430
	leal	-65536(%rcx), %eax
	shrl	$10, %eax
	andw	$1023, %ax
	subw	$10240, %ax
	movw	%ax, -114(%rbp)
	cmpq	%rdx, %r10
	je	.L376
	movw	%ax, (%r10)
	movq	-72(%rbp), %rax
	leaq	2(%rax), %rsi
	movq	%rsi, -72(%rbp)
.L377:
	andw	$1023, %r11w
	subw	$9216, %r11w
	movw	%r11w, -114(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L378
	movw	%r11w, (%rsi)
	addq	$2, -72(%rbp)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L363:
	leaq	-112(%rbp), %rdi
	movq	%r15, %rdx
	addl	$1, %r13d
	movl	%eax, -152(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	cmpl	%r13d, %ebx
	movl	-152(%rbp), %ecx
	jg	.L365
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L425:
	movq	312(%r14), %r12
.L361:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L397
	call	_ZdlPv@PLT
.L397:
	movq	-136(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-144(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L355
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L428:
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rax
	leaq	-80(%rbp), %rsi
	movq	%r14, %rdi
	subq	%rdx, %rax
	movq	%rdx, -80(%rbp)
	xorl	%edx, %edx
	cltq
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L425
	movq	(%rax), %r12
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L427:
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -152(%rbp)
	call	memmove@PLT
	movq	-152(%rbp), %r8
.L358:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L359
.L429:
	movq	%r11, %rsi
	movq	%rax, %rdi
	movl	%ecx, -160(%rbp)
	movq	%r11, -152(%rbp)
	call	memmove@PLT
	movq	-152(%rbp), %r11
	movl	-160(%rbp), %ecx
	movq	%rax, %r10
.L370:
	movq	%r11, %rdi
	movq	%r10, -160(%rbp)
	movl	%ecx, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %r10
	movl	-152(%rbp), %ecx
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L373:
	leaq	-80(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L378:
	leaq	-80(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L376:
	movq	%r10, %rsi
	leaq	-80(%rbp), %rdi
	movq	%r15, %rdx
	movl	%r11d, -152(%rbp)
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movl	-152(%rbp), %r11d
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L424:
	movq	312(%r14), %r12
	movq	-80(%rbp), %rsi
.L383:
	testq	%rsi, %rsi
	je	.L361
	movq	%rsi, %rdi
	call	_ZdlPv@PLT
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L380:
	movq	-72(%rbp), %rsi
	subq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rsi, %rax
	movq	-104(%rbp), %rsi
	subq	-112(%rbp), %rsi
	sarq	%rax
	addl	%eax, %esi
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L424
	movq	(%rax), %r12
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rax
	leaq	15(%r12), %rsi
	subq	%rcx, %rax
	leaq	(%rsi,%rax,2), %rdi
	cmpq	%rsi, %rdi
	jbe	.L385
	movq	%rdi, %rax
	leaq	16(%r12), %rdx
	movl	$1, %r8d
	movl	$2, %r11d
	subq	%r12, %rax
	leaq	-16(%rax), %r10
	movq	%r10, %rax
	shrq	%rax
	addq	$1, %rax
	cmpq	%rdx, %rdi
	cmovnb	%rax, %r8
	addq	%rax, %rax
	cmpq	%rdx, %rdi
	cmovb	%r11, %rax
	addq	%rsi, %rax
	cmpq	%rax, %rcx
	leaq	(%rcx,%r8), %rax
	setnb	%r11b
	cmpq	%rax, %rsi
	setnb	%al
	orb	%al, %r11b
	je	.L413
	cmpq	$29, %r10
	seta	%r10b
	cmpq	%rdx, %rdi
	setnb	%al
	testb	%al, %r10b
	je	.L413
	movq	%r8, %r10
	movq	%rcx, %rdx
	pxor	%xmm1, %xmm1
	movq	%rsi, %rax
	andq	$-16, %r10
	addq	%rcx, %r10
	.p2align 4,,10
	.p2align 3
.L387:
	movdqu	(%rdx), %xmm0
	addq	$16, %rdx
	addq	$32, %rax
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	movups	%xmm0, -16(%rax)
	movups	%xmm2, -32(%rax)
	cmpq	%r10, %rdx
	jne	.L387
	movq	%r8, %rdx
	andq	$-16, %rdx
	leaq	(%rsi,%rdx,2), %rax
	addq	%rdx, %rcx
	cmpq	%r8, %rdx
	je	.L389
	movzbl	(%rcx), %edx
	movw	%dx, (%rax)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	1(%rcx), %edx
	movw	%dx, 2(%rax)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	2(%rcx), %edx
	movw	%dx, 4(%rax)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	3(%rcx), %edx
	movw	%dx, 6(%rax)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	4(%rcx), %edx
	movw	%dx, 8(%rax)
	leaq	10(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	5(%rcx), %edx
	movw	%dx, 10(%rax)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	6(%rcx), %edx
	movw	%dx, 12(%rax)
	leaq	14(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	7(%rcx), %edx
	movw	%dx, 14(%rax)
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	8(%rcx), %edx
	movw	%dx, 16(%rax)
	leaq	18(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	9(%rcx), %edx
	movw	%dx, 18(%rax)
	leaq	20(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	10(%rcx), %edx
	movw	%dx, 20(%rax)
	leaq	22(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	11(%rcx), %edx
	movw	%dx, 22(%rax)
	leaq	24(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	12(%rcx), %edx
	movw	%dx, 24(%rax)
	leaq	26(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	13(%rcx), %edx
	movw	%dx, 26(%rax)
	leaq	28(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L389
	movzbl	14(%rcx), %edx
	movw	%dx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L389:
	movq	(%rbx), %r12
	movq	-104(%rbp), %rax
	subq	-112(%rbp), %rax
	leaq	15(%r12,%rax,2), %rdi
.L385:
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	subq	%rsi, %rdx
	cmpq	$7, %rdx
	ja	.L431
	leaq	(%rdi,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L383
	leaq	15(%rsi), %rax
	subq	$1, %rdx
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L414
	cmpq	$13, %rdx
	jbe	.L414
	shrq	%rdx
	xorl	%eax, %eax
	addq	$1, %rdx
	movq	%rdx, %r8
	shrq	$3, %r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L393:
	movdqu	(%rsi,%rax), %xmm3
	movups	%xmm3, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %r8
	jne	.L393
	movq	%rdx, %rax
	andq	$-8, %rax
	leaq	(%rax,%rax), %r8
	addq	%r8, %rdi
	addq	%r8, %rsi
	cmpq	%rdx, %rax
	je	.L395
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	leaq	2(%rdi), %rax
	cmpq	%rax, %rcx
	jbe	.L395
	movzwl	2(%rsi), %eax
	movw	%ax, 2(%rdi)
	leaq	4(%rdi), %rax
	cmpq	%rax, %rcx
	jbe	.L395
	movzwl	4(%rsi), %eax
	movw	%ax, 4(%rdi)
	leaq	6(%rdi), %rax
	cmpq	%rax, %rcx
	jbe	.L395
	movzwl	6(%rsi), %eax
	movw	%ax, 6(%rdi)
	leaq	8(%rdi), %rax
	cmpq	%rax, %rcx
	jbe	.L395
	movzwl	8(%rsi), %eax
	movw	%ax, 8(%rdi)
	leaq	10(%rdi), %rax
	cmpq	%rax, %rcx
	jbe	.L395
	movzwl	10(%rsi), %eax
	movw	%ax, 10(%rdi)
	leaq	12(%rdi), %rax
	cmpq	%rax, %rcx
	jbe	.L395
	movzwl	12(%rsi), %eax
	movw	%ax, 12(%rdi)
.L395:
	movq	(%rbx), %r12
	movq	-80(%rbp), %rsi
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L413:
	movzbl	(%rcx), %edx
	movq	%rsi, %rax
	addq	$2, %rsi
	addq	$1, %rcx
	movw	%dx, (%rax)
	cmpq	%rsi, %rdi
	ja	.L413
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L414:
	movsw
	cmpq	%rdi, %rcx
	ja	.L414
	jmp	.L395
.L431:
	call	memcpy@PLT
	movq	(%rbx), %r12
	movq	-80(%rbp), %rsi
	jmp	.L383
.L426:
	call	__stack_chk_fail@PLT
.L368:
	leaq	.LC16(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20392:
	.size	_ZN2v88internalL32Builtin_Impl_StringFromCodePointENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL32Builtin_Impl_StringFromCodePointENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Builtin_Impl_Stats_StringFromCodePointEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Builtin_StringFromCodePoint"
	.section	.text._ZN2v88internalL38Builtin_Impl_Stats_StringFromCodePointEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_Stats_StringFromCodePointEiPmPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_Stats_StringFromCodePointEiPmPNS0_7IsolateE:
.LFB20390:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L461
.L433:
	movq	_ZZN2v88internalL38Builtin_Impl_Stats_StringFromCodePointEiPmPNS0_7IsolateEE27trace_event_unique_atomic58(%rip), %rbx
	testq	%rbx, %rbx
	je	.L462
.L435:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L463
.L437:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL32Builtin_Impl_StringFromCodePointENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L464
.L441:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L465
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L466
.L436:
	movq	%rbx, _ZZN2v88internalL38Builtin_Impl_Stats_StringFromCodePointEiPmPNS0_7IsolateEE27trace_event_unique_atomic58(%rip)
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L463:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L467
.L438:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L439
	movq	(%rdi), %rax
	call	*8(%rax)
.L439:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L440
	movq	(%rdi), %rax
	call	*8(%rax)
.L440:
	leaq	.LC17(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L464:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L461:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$836, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L467:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L466:
	leaq	.LC11(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L436
.L465:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20390:
	.size	_ZN2v88internalL38Builtin_Impl_Stats_StringFromCodePointEiPmPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_Stats_StringFromCodePointEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Builtin_StringFromCodePointEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Builtin_StringFromCodePointEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Builtin_StringFromCodePointEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Builtin_StringFromCodePointEiPmPNS0_7IsolateE:
.LFB20391:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L472
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL32Builtin_Impl_StringFromCodePointENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore 6
	jmp	_ZN2v88internalL38Builtin_Impl_Stats_StringFromCodePointEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20391:
	.size	_ZN2v88internal27Builtin_StringFromCodePointEiPmPNS0_7IsolateE, .-_ZN2v88internal27Builtin_StringFromCodePointEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal27Builtin_StringFromCodePointEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal27Builtin_StringFromCodePointEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal27Builtin_StringFromCodePointEiPmPNS0_7IsolateE:
.LFB25623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25623:
	.size	_GLOBAL__sub_I__ZN2v88internal27Builtin_StringFromCodePointEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal27Builtin_StringFromCodePointEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal27Builtin_StringFromCodePointEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL28Builtin_Impl_Stats_StringRawEiPmPNS0_7IsolateEE28trace_event_unique_atomic422,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Builtin_Impl_Stats_StringRawEiPmPNS0_7IsolateEE28trace_event_unique_atomic422, @object
	.size	_ZZN2v88internalL28Builtin_Impl_Stats_StringRawEiPmPNS0_7IsolateEE28trace_event_unique_atomic422, 8
_ZZN2v88internalL28Builtin_Impl_Stats_StringRawEiPmPNS0_7IsolateEE28trace_event_unique_atomic422:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_StringPrototypeLocaleCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic135,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_StringPrototypeLocaleCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic135, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_StringPrototypeLocaleCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic135, 8
_ZZN2v88internalL47Builtin_Impl_Stats_StringPrototypeLocaleCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic135:
	.zero	8
	.section	.bss._ZZN2v88internalL45Builtin_Impl_Stats_StringPrototypeLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic124,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Builtin_Impl_Stats_StringPrototypeLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic124, @object
	.size	_ZZN2v88internalL45Builtin_Impl_Stats_StringPrototypeLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic124, 8
_ZZN2v88internalL45Builtin_Impl_Stats_StringPrototypeLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic124:
	.zero	8
	.section	.bss._ZZN2v88internalL38Builtin_Impl_Stats_StringFromCodePointEiPmPNS0_7IsolateEE27trace_event_unique_atomic58,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Builtin_Impl_Stats_StringFromCodePointEiPmPNS0_7IsolateEE27trace_event_unique_atomic58, @object
	.size	_ZZN2v88internalL38Builtin_Impl_Stats_StringFromCodePointEiPmPNS0_7IsolateEE27trace_event_unique_atomic58, 8
_ZZN2v88internalL38Builtin_Impl_Stats_StringFromCodePointEiPmPNS0_7IsolateEE27trace_event_unique_atomic58:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	0
	.long	1093730303
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	4294967295
	.long	2146435071
	.align 8
.LC6:
	.long	4290772992
	.long	1105199103
	.align 8
.LC7:
	.long	0
	.long	-1042284544
	.align 8
.LC10:
	.long	4292870144
	.long	1106247679
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
