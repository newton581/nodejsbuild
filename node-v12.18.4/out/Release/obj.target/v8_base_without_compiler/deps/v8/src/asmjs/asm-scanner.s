	.file	"asm-scanner.cc"
	.text
	.section	.text._ZN2v88internal12AsmJsScanner6RewindEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScanner6RewindEv
	.type	_ZN2v88internal12AsmJsScanner6RewindEv, @function
_ZN2v88internal12AsmJsScanner6RewindEv:
.LFB5674:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	movb	$1, 48(%rdi)
	movq	$0, 64(%rdi)
	movl	%eax, 16(%rdi)
	movq	24(%rdi), %rax
	movq	%rax, 40(%rdi)
	movl	12(%rdi), %eax
	movl	$0, 12(%rdi)
	movl	%eax, 8(%rdi)
	movq	32(%rdi), %rax
	movq	$0, 32(%rdi)
	movq	%rax, 24(%rdi)
	movq	56(%rdi), %rax
	movb	$0, (%rax)
	ret
	.cfi_endproc
.LFE5674:
	.size	_ZN2v88internal12AsmJsScanner6RewindEv, .-_ZN2v88internal12AsmJsScanner6RewindEv
	.section	.text._ZN2v88internal12AsmJsScanner11ResetLocalsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScanner11ResetLocalsEv
	.type	_ZN2v88internal12AsmJsScanner11ResetLocalsEv, @function
_ZN2v88internal12AsmJsScanner11ResetLocalsEv:
.LFB5675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	112(%rdi), %r12
	testq	%r12, %r12
	jne	.L7
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L18:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L4
.L6:
	movq	%r13, %r12
.L7:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	(%r12), %r13
	cmpq	%rax, %rdi
	jne	.L18
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L6
.L4:
	movq	104(%rbx), %rax
	movq	96(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, 120(%rbx)
	movq	$0, 112(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5675:
	.size	_ZN2v88internal12AsmJsScanner11ResetLocalsEv, .-_ZN2v88internal12AsmJsScanner11ResetLocalsEv
	.section	.text._ZN2v88internal12AsmJsScanner13ConsumeNumberEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScanner13ConsumeNumberEi
	.type	_ZN2v88internal12AsmJsScanner13ConsumeNumberEi, @function
_ZN2v88internal12AsmJsScanner13ConsumeNumberEi:
.LFB5678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsbl	%sil, %r8d
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	xorl	%esi, %esi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%r15, -96(%rbp)
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	cmpl	$46, %ebx
	sete	-105(%rbp)
	xorl	%r13d, %r13d
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L24:
	movq	-88(%rbp), %r12
	movq	-96(%rbp), %rcx
	cmpl	$46, %edx
	je	.L55
	cmpl	$98, %edx
	sete	%al
	cmpl	$111, %edx
	sete	%dil
	orb	%dil, %al
	je	.L70
.L56:
	movl	%eax, %r13d
.L27:
	movl	%edx, %eax
	cmpq	%r15, %rcx
	leaq	1(%r12), %rbx
	movl	$15, %edx
	cmovne	-80(%rbp), %rdx
	cmpq	%rdx, %rbx
	ja	.L71
.L35:
	movb	%al, (%rcx,%r12)
	movq	-96(%rbp), %rax
	movq	%rbx, -88(%rbp)
	movb	$0, (%rax,%rbx)
.L36:
	movq	(%r14), %rcx
	movq	16(%rcx), %rax
	cmpq	%rax, 24(%rcx)
	jbe	.L20
.L23:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	leal	-48(%rdx), %eax
	cmpl	$9, %eax
	jbe	.L24
	movl	%edx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$5, %eax
	jbe	.L24
	cmpl	$46, %edx
	sete	%al
	cmpl	$98, %edx
	sete	%cl
	orb	%cl, %al
	jne	.L24
	cmpl	$111, %edx
	sete	%cl
	movl	%ecx, %edi
	cmpl	$120, %edx
	je	.L25
	testb	%cl, %cl
	je	.L72
.L25:
	movq	-88(%rbp), %r12
	movq	-96(%rbp), %rcx
	orb	%dil, %al
	jne	.L56
	.p2align 4,,10
	.p2align 3
.L70:
	cmpl	$120, %edx
	movl	$1, %eax
	cmove	%eax, %r13d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L55:
	movb	$1, -105(%rbp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L71:
	movq	-120(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movb	%al, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-96(%rbp), %rcx
	movzbl	-104(%rbp), %eax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L20:
	cmpb	$0, 48(%rcx)
	jne	.L22
	movq	(%rcx), %rax
	movq	%rcx, -104(%rbp)
	movq	%rcx, %rdi
	call	*40(%rax)
	movq	-104(%rbp), %rcx
	testb	%al, %al
	movq	16(%rcx), %rax
	jne	.L23
.L22:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
.L28:
	movq	(%r14), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdx
	cmpq	%rdx, %rax
	jbe	.L30
.L77:
	subq	$2, %rax
	movq	%rax, 16(%rdi)
.L31:
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rdi
	cmpq	$1, %rax
	je	.L73
.L33:
	movslq	%eax, %rsi
	pxor	%xmm0, %xmm0
	movl	$15, %edx
	call	_ZN2v88internal14StringToDoubleENS0_6VectorIKhEEid@PLT
	movq	-96(%rbp), %rdi
	movsd	%xmm0, 272(%r14)
	ucomisd	%xmm0, %xmm0
	jp	.L74
	cmpb	$0, -105(%rbp)
	jne	.L75
	comisd	.LC1(%rip), %xmm0
	ja	.L67
	cvttsd2siq	%xmm0, %rax
	movl	$-3, 8(%r14)
	movl	%eax, 280(%r14)
.L38:
	cmpq	%r15, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L72:
	.cfi_restore_state
	leal	-43(%rdx), %eax
	andl	$-3, %eax
	setne	%al
	orb	%al, %r13b
	jne	.L28
	movq	-88(%rbp), %r12
	movq	-96(%rbp), %rcx
	movzbl	-1(%rcx,%r12), %eax
	andl	$-33, %eax
	cmpb	$69, %al
	je	.L27
	movq	(%r14), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdx
	cmpq	%rdx, %rax
	ja	.L77
.L30:
	movq	32(%rdi), %rcx
	subq	%rdx, %rax
	movq	%rdx, 16(%rdi)
	sarq	%rax
	cmpb	$0, 48(%rdi)
	leaq	-1(%rax,%rcx), %rax
	movq	%rax, 32(%rdi)
	jne	.L31
	movq	(%rdi), %rax
	call	*40(%rax)
	jmp	.L31
.L75:
	movl	$-4, 8(%r14)
	jmp	.L38
.L73:
	movzbl	(%rdi), %edx
	cmpb	$48, %dl
	je	.L78
	cmpb	$46, %dl
	jne	.L33
.L43:
	movl	$46, 8(%r14)
	jmp	.L38
.L74:
	cmpb	$46, (%rdi)
	je	.L79
.L67:
	movl	$-2, 8(%r14)
	jmp	.L38
.L78:
	movl	$0, 280(%r14)
	movl	$-3, 8(%r14)
	jmp	.L38
.L79:
	cmpq	$1, -88(%rbp)
	jbe	.L43
	movl	$1, %ebx
.L46:
	movq	(%r14), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdx
	cmpq	%rdx, %rax
	jbe	.L44
	subq	$2, %rax
	movq	%rax, 16(%rdi)
.L45:
	addq	$1, %rbx
	cmpq	-88(%rbp), %rbx
	jb	.L46
	movq	-96(%rbp), %rdi
	jmp	.L43
.L44:
	movq	32(%rdi), %rcx
	subq	%rdx, %rax
	movq	%rdx, 16(%rdi)
	sarq	%rax
	cmpb	$0, 48(%rdi)
	leaq	-1(%rax,%rcx), %rax
	movq	%rax, 32(%rdi)
	jne	.L45
	movq	(%rdi), %rax
	call	*40(%rax)
	jmp	.L45
.L76:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5678:
	.size	_ZN2v88internal12AsmJsScanner13ConsumeNumberEi, .-_ZN2v88internal12AsmJsScanner13ConsumeNumberEi
	.section	.text._ZN2v88internal12AsmJsScanner15ConsumeCCommentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScanner15ConsumeCCommentEv
	.type	_ZN2v88internal12AsmJsScanner15ConsumeCCommentEv, @function
_ZN2v88internal12AsmJsScanner15ConsumeCCommentEv:
.LFB5681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
.L81:
	movq	(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L82
.L100:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	cmpl	$42, %edx
	je	.L89
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L101:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	cmpl	$47, %edx
	je	.L102
	cmpl	$42, %edx
	jne	.L88
.L89:
	movq	(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jb	.L101
	cmpb	$0, 48(%rbx)
	je	.L92
	addq	$2, %rax
	movq	%rax, 16(%rbx)
.L87:
	xorl	%eax, %eax
.L80:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L93
	movq	16(%rbx), %rax
	jmp	.L101
.L84:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L86
	.p2align 4,,10
	.p2align 3
.L93:
	addq	$2, 16(%rbx)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L82:
	cmpb	$0, 48(%rbx)
	je	.L84
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	xorl	%eax, %eax
	jmp	.L80
.L88:
	cmpl	$10, %edx
	jne	.L81
	movb	$1, 284(%r12)
	jmp	.L81
.L86:
	movq	16(%rbx), %rax
	jmp	.L100
	.cfi_endproc
.LFE5681:
	.size	_ZN2v88internal12AsmJsScanner15ConsumeCCommentEv, .-_ZN2v88internal12AsmJsScanner15ConsumeCCommentEv
	.section	.text._ZN2v88internal12AsmJsScanner17ConsumeCPPCommentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScanner17ConsumeCPPCommentEv
	.type	_ZN2v88internal12AsmJsScanner17ConsumeCPPCommentEv, @function
_ZN2v88internal12AsmJsScanner17ConsumeCPPCommentEv:
.LFB5682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L112:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	cmpl	$10, %edx
	je	.L113
.L110:
	movq	(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jb	.L112
	cmpb	$0, 48(%rbx)
	jne	.L114
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L115
	addq	$2, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movb	$1, 284(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movq	16(%rbx), %rax
	jmp	.L112
	.cfi_endproc
.LFE5682:
	.size	_ZN2v88internal12AsmJsScanner17ConsumeCPPCommentEv, .-_ZN2v88internal12AsmJsScanner17ConsumeCPPCommentEv
	.section	.rodata._ZN2v88internal12AsmJsScanner13ConsumeStringEi.str1.1,"aMS",@progbits,1
.LC2:
	.string	"use asm"
	.section	.text._ZN2v88internal12AsmJsScanner13ConsumeStringEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScanner13ConsumeStringEi
	.type	_ZN2v88internal12AsmJsScanner13ConsumeStringEi, @function
_ZN2v88internal12AsmJsScanner13ConsumeStringEi:
.LFB5683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	.LC2(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$117, %ebx
	subq	$24, %rsp
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L119:
	movzwl	(%rax), %r14d
.L118:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	cmpl	%r14d, %ebx
	jne	.L126
.L120:
	movsbl	1(%r13), %ebx
	addq	$1, %r13
	testb	%bl, %bl
	je	.L131
.L122:
	movq	(%r15), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jb	.L119
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L118
	movq	(%r12), %rax
	movl	%esi, -52(%rbp)
	movq	%r12, %rdi
	call	*40(%rax)
	movl	-52(%rbp), %esi
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L119
	addq	$2, %rax
	movq	%rax, 16(%r12)
	cmpl	%r14d, %ebx
	je	.L120
.L126:
	movl	$-2, 8(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movq	(%r15), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L123
.L125:
	movzwl	(%rax), %r12d
.L124:
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	cmpl	%r12d, %esi
	jne	.L126
	movl	$-9937, 8(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L123:
	.cfi_restore_state
	cmpb	$0, 48(%rbx)
	movl	$-1, %r12d
	jne	.L124
	movq	(%rbx), %rax
	movl	%esi, -52(%rbp)
	movq	%rbx, %rdi
	call	*40(%rax)
	movl	-52(%rbp), %esi
	testb	%al, %al
	movq	16(%rbx), %rax
	jne	.L125
	jmp	.L124
	.cfi_endproc
.LFE5683:
	.size	_ZN2v88internal12AsmJsScanner13ConsumeStringEi, .-_ZN2v88internal12AsmJsScanner13ConsumeStringEi
	.section	.rodata._ZN2v88internal12AsmJsScanner21ConsumeCompareOrShiftEi.str1.1,"aMS",@progbits,1
.LC3:
	.string	"unreachable code"
	.section	.text._ZN2v88internal12AsmJsScanner21ConsumeCompareOrShiftEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScanner21ConsumeCompareOrShiftEi
	.type	_ZN2v88internal12AsmJsScanner21ConsumeCompareOrShiftEi, @function
_ZN2v88internal12AsmJsScanner21ConsumeCompareOrShiftEi:
.LFB5684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L133
.L169:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	cmpl	$61, %edx
	je	.L171
	cmpl	$60, %esi
	jne	.L146
	cmpl	$60, %edx
	jne	.L146
	movl	$-9940, 8(%rbx)
.L132:
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movq	(%rbx), %r13
	movq	16(%r13), %rax
	cmpl	$62, %esi
	jne	.L147
	cmpl	$62, %edx
	jne	.L147
	cmpq	%rax, 24(%r13)
	jbe	.L148
.L170:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	cmpl	$62, %edx
	jne	.L151
	movl	$-9938, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	cmpl	$61, %esi
	je	.L139
	jg	.L140
	cmpl	$33, %esi
	je	.L141
	cmpl	$60, %esi
	jne	.L143
	movl	$-9944, 8(%rbx)
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L140:
	cmpl	$62, %esi
	jne	.L143
	movl	$-9943, 8(%rbx)
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L141:
	movl	$-9941, 8(%rbx)
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L148:
	cmpb	$0, 48(%r13)
	je	.L150
	addq	$2, %rax
	movq	%rax, 16(%r13)
	.p2align 4,,10
	.p2align 3
.L151:
	movq	(%rbx), %rdi
	movl	$-9939, 8(%rbx)
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdx
	cmpq	%rdx, %rax
	jbe	.L153
	subq	$2, %rax
	movq	%rax, 16(%rdi)
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L133:
	cmpb	$0, 48(%r13)
	je	.L135
	addq	$2, %rax
	movq	%rax, 16(%r13)
.L136:
	movq	(%rbx), %r13
	movq	16(%r13), %rax
.L147:
	movq	8(%r13), %rdx
	cmpq	%rdx, %rax
	jbe	.L154
	subq	$2, %rax
	movq	%rax, 16(%r13)
.L155:
	movl	%esi, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movl	$-9942, 8(%rbx)
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L135:
	movq	0(%r13), %rax
	movl	%esi, -20(%rbp)
	movq	%r13, %rdi
	call	*40(%rax)
	movl	-20(%rbp), %esi
	testb	%al, %al
	je	.L137
	movq	16(%r13), %rax
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L137:
	addq	$2, 16(%r13)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L150:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L152
	addq	$2, 16(%r13)
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L154:
	movq	32(%r13), %rcx
	subq	%rdx, %rax
	movq	%rdx, 16(%r13)
	sarq	%rax
	cmpb	$0, 48(%r13)
	leaq	-1(%rax,%rcx), %rax
	movq	%rax, 32(%r13)
	jne	.L155
	movq	0(%r13), %rax
	movl	%esi, -20(%rbp)
	movq	%r13, %rdi
	call	*40(%rax)
	movl	-20(%rbp), %esi
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L153:
	movq	32(%rdi), %rcx
	subq	%rdx, %rax
	movq	%rdx, 16(%rdi)
	sarq	%rax
	cmpb	$0, 48(%rdi)
	leaq	-1(%rax,%rcx), %rax
	movq	%rax, 32(%rdi)
	jne	.L132
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L152:
	.cfi_restore_state
	movq	16(%r13), %rax
	jmp	.L170
.L143:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5684:
	.size	_ZN2v88internal12AsmJsScanner21ConsumeCompareOrShiftEi, .-_ZN2v88internal12AsmJsScanner21ConsumeCompareOrShiftEi
	.section	.text._ZN2v88internal12AsmJsScanner17IsIdentifierStartEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScanner17IsIdentifierStartEi
	.type	_ZN2v88internal12AsmJsScanner17IsIdentifierStartEi, @function
_ZN2v88internal12AsmJsScanner17IsIdentifierStartEi:
.LFB5685:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	movl	$1, %eax
	orl	$32, %edx
	subl	$97, %edx
	cmpl	$25, %edx
	jbe	.L172
	cmpl	$95, %esi
	sete	%al
	cmpl	$36, %esi
	sete	%dl
	orl	%edx, %eax
.L172:
	ret
	.cfi_endproc
.LFE5685:
	.size	_ZN2v88internal12AsmJsScanner17IsIdentifierStartEi, .-_ZN2v88internal12AsmJsScanner17IsIdentifierStartEi
	.section	.text._ZN2v88internal12AsmJsScanner16IsIdentifierPartEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScanner16IsIdentifierPartEi
	.type	_ZN2v88internal12AsmJsScanner16IsIdentifierPartEi, @function
_ZN2v88internal12AsmJsScanner16IsIdentifierPartEi:
.LFB5686:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	movl	$1, %eax
	orl	$32, %edx
	subl	$97, %edx
	cmpl	$25, %edx
	jbe	.L175
	leal	-48(%rsi), %edx
	cmpl	$9, %edx
	ja	.L179
.L175:
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	cmpl	$36, %esi
	sete	%al
	cmpl	$95, %esi
	sete	%dl
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE5686:
	.size	_ZN2v88internal12AsmJsScanner16IsIdentifierPartEi, .-_ZN2v88internal12AsmJsScanner16IsIdentifierPartEi
	.section	.text._ZN2v88internal12AsmJsScanner13IsNumberStartEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScanner13IsNumberStartEi
	.type	_ZN2v88internal12AsmJsScanner13IsNumberStartEi, @function
_ZN2v88internal12AsmJsScanner13IsNumberStartEi:
.LFB5687:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpl	$46, %esi
	je	.L180
	subl	$48, %esi
	cmpl	$9, %esi
	setbe	%al
.L180:
	ret
	.cfi_endproc
.LFE5687:
	.size	_ZN2v88internal12AsmJsScanner13IsNumberStartEi, .-_ZN2v88internal12AsmJsScanner13IsNumberStartEi
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_:
.LFB6324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%rbx), %r14
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r14
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L185
	movq	(%rax), %rbx
	movq	%rdx, %r15
	movq	48(%rbx), %rcx
.L188:
	cmpq	%rcx, %r13
	je	.L205
.L186:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L185
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	cmpq	%rdx, %r15
	je	.L188
.L185:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L186
	testq	%rdx, %rdx
	je	.L187
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L186
.L187:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6324:
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm:
.LFB6468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L230
.L207:
	movq	%r14, 48(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L216
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L217:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L231
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L232
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L209:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L211
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L213:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L214:
	testq	%rsi, %rsi
	je	.L211
.L212:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	48(%rcx), %rax
	divq	%r13
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L213
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L219
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L212
	.p2align 4,,10
	.p2align 3
.L211:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r15, (%rbx)
	divq	%r13
	movq	%r13, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L216:
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L218
	movq	48(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L218:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%rdx, %rdi
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L231:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L209
.L232:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6468:
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm
	.section	.text._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_,"axG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	.type	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_, @function
_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_:
.LFB6318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%r14), %r15
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r15
	movq	(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L234
	movq	(%rax), %rbx
	movq	48(%rbx), %rcx
.L237:
	cmpq	%rcx, %r13
	je	.L255
.L235:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L234
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r9
	je	.L237
.L234:
	movl	$56, %edi
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movq	-56(%rbp), %r9
	movq	$0, (%rax)
	movq	%rax, %rcx
	leaq	24(%rax), %rax
	movq	%rax, 8(%rcx)
	leaq	16(%r12), %rax
	cmpq	%rax, %rdx
	je	.L256
	movq	%rdx, 8(%rcx)
	movq	16(%r12), %rdx
	movq	%rdx, 24(%rcx)
.L239:
	movq	8(%r12), %rdx
	movq	%rax, (%r12)
	movq	%r14, %rdi
	movq	%r9, %rsi
	movq	$0, 8(%r12)
	movl	$1, %r8d
	movq	%rdx, 16(%rcx)
	movq	%r13, %rdx
	movb	$0, 16(%r12)
	movl	$0, 40(%rcx)
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm
	addq	$24, %rsp
	popq	%rbx
	addq	$40, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L235
	movq	%r9, -56(%rbp)
	testq	%rdx, %rdx
	je	.L236
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	jne	.L235
.L236:
	addq	$24, %rsp
	leaq	40(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movdqu	16(%r12), %xmm0
	movups	%xmm0, 24(%rcx)
	jmp	.L239
	.cfi_endproc
.LFE6318:
	.size	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_, .-_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	.section	.rodata._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,"axG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.type	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, @function
_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_:
.LFB6327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%r14), %r15
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r15
	movq	(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L258
	movq	(%rax), %rbx
	movq	48(%rbx), %rcx
.L261:
	cmpq	%rcx, %r13
	je	.L292
.L259:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L258
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r9
	je	.L261
.L258:
	movl	$56, %edi
	movq	%r9, -72(%rbp)
	call	_Znwm@PLT
	movq	(%r12), %rbx
	movq	8(%r12), %r12
	leaq	24(%rax), %rdi
	movq	$0, (%rax)
	movq	%rax, %r15
	movq	-72(%rbp), %r9
	movq	%rdi, 8(%rax)
	movq	%rbx, %rax
	addq	%r12, %rax
	je	.L262
	testq	%rbx, %rbx
	je	.L293
.L262:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	ja	.L294
	cmpq	$1, %r12
	jne	.L265
	movzbl	(%rbx), %eax
	movb	%al, 24(%r15)
.L266:
	movq	%r12, 16(%r15)
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movb	$0, (%rdi,%r12)
	movq	%r9, %rsi
	movq	%r14, %rdi
	movl	$0, 40(%r15)
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm
	addq	$40, %rax
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L292:
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L259
	movq	%r9, -72(%rbp)
	testq	%rdx, %rdx
	je	.L260
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	movq	-72(%rbp), %r9
	testl	%eax, %eax
	jne	.L259
.L260:
	leaq	40(%rbx), %rax
.L257:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L295
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L266
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L294:
	leaq	8(%r15), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r9
	movq	%rax, 8(%r15)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 24(%r15)
.L264:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r9, -72(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r12
	movq	8(%r15), %rdi
	movq	-72(%rbp), %r9
	jmp	.L266
.L295:
	call	__stack_chk_fail@PLT
.L293:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE6327:
	.size	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, .-_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.section	.rodata._ZN2v88internal12AsmJsScanner17ConsumeIdentifierEi.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"global_count_ < kMaxIdentifierCount"
	.section	.rodata._ZN2v88internal12AsmJsScanner17ConsumeIdentifierEi.str1.1,"aMS",@progbits,1
.LC6:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal12AsmJsScanner17ConsumeIdentifierEi.str1.8
	.align 8
.LC7:
	.string	"local_names_.size() < kMaxIdentifierCount"
	.section	.text._ZN2v88internal12AsmJsScanner17ConsumeIdentifierEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScanner17ConsumeIdentifierEi
	.type	_ZN2v88internal12AsmJsScanner17ConsumeIdentifierEi, @function
_ZN2v88internal12AsmJsScanner17ConsumeIdentifierEi:
.LFB5677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	56(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	56(%rdi), %rax
	movq	$0, 64(%rdi)
	movb	$0, (%rax)
	.p2align 4,,10
	.p2align 3
.L306:
	movl	%r9d, %eax
	orl	$32, %eax
	subl	$97, %eax
	cmpl	$25, %eax
	jbe	.L297
	leal	-36(%r9), %eax
	cmpl	$59, %eax
	ja	.L298
	movabsq	$576460752307613697, %rcx
	btq	%rax, %rcx
	jc	.L297
	.p2align 4,,10
	.p2align 3
.L298:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdx
	cmpq	%rdx, %rax
	jbe	.L335
	subq	$2, %rax
	movq	%rax, 16(%rdi)
.L307:
	cmpl	$46, 12(%rbx)
	je	.L336
	leaq	96(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L311
.L334:
	movl	40(%rax), %eax
	movl	%eax, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movq	64(%rbx), %r15
	movq	56(%rbx), %rax
	leaq	72(%rbx), %rdx
	movl	%r9d, %r13d
	leaq	1(%r15), %r14
	cmpq	%rdx, %rax
	je	.L320
	movq	72(%rbx), %rdx
.L301:
	cmpq	%rdx, %r14
	ja	.L337
.L302:
	movb	%r13b, (%rax,%r15)
	movq	56(%rbx), %rax
	movq	%r14, 64(%rbx)
	movb	$0, 1(%rax,%r15)
	movq	(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L303
.L305:
	movzwl	(%rax), %r9d
.L304:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L337:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	56(%rbx), %rax
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L320:
	movl	$15, %edx
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L303:
	cmpb	$0, 48(%r15)
	movl	$-1, %r9d
	jne	.L304
	movq	(%r15), %rax
	movl	%r9d, -52(%rbp)
	movq	%r15, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r15), %rax
	jne	.L305
	movl	-52(%rbp), %r9d
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L311:
	cmpb	$0, 88(%rbx)
	je	.L338
	cmpl	$46, 12(%rbx)
	je	.L318
.L319:
	movq	120(%rbx), %rdx
	cmpq	$251658239, %rdx
	jbe	.L316
	leaq	.LC7(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L336:
	leaq	208(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	jne	.L334
.L309:
	cmpl	$46, 12(%rbx)
	je	.L318
	cmpb	$0, 88(%rbx)
	jne	.L319
	movl	264(%rbx), %eax
	cmpl	$251658239, %eax
	jg	.L317
	leal	1(%rax), %edx
	addl	$256, %eax
	leaq	152(%rbx), %rdi
	movq	%r12, %rsi
	movl	%edx, 264(%rbx)
	movl	%eax, 8(%rbx)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movl	8(%rbx), %edx
	movl	%edx, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L318:
	.cfi_restore_state
	movl	264(%rbx), %eax
	cmpl	$251658239, %eax
	jg	.L317
	leal	1(%rax), %edx
	addl	$256, %eax
	leaq	208(%rbx), %rdi
	movq	%r12, %rsi
	movl	%edx, 264(%rbx)
	movl	%eax, 8(%rbx)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movl	8(%rbx), %edx
	movl	%edx, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L316:
	.cfi_restore_state
	movl	$-10000, %eax
	leaq	96(%rbx), %rdi
	movq	%r12, %rsi
	subl	%edx, %eax
	movl	%eax, 8(%rbx)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movl	8(%rbx), %edx
	movl	%edx, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movq	32(%rdi), %rcx
	subq	%rdx, %rax
	movq	%rdx, 16(%rdi)
	sarq	%rax
	cmpb	$0, 48(%rdi)
	leaq	-1(%rax,%rcx), %rax
	movq	%rax, 32(%rdi)
	jne	.L307
	movq	(%rdi), %rax
	call	*40(%rax)
	jmp	.L307
.L338:
	leaq	152(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	jne	.L334
	jmp	.L309
.L317:
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5677:
	.size	_ZN2v88internal12AsmJsScanner17ConsumeIdentifierEi, .-_ZN2v88internal12AsmJsScanner17ConsumeIdentifierEi
	.section	.text._ZN2v88internal12AsmJsScanner4SeekEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScanner4SeekEm
	.type	_ZN2v88internal12AsmJsScanner4SeekEm, @function
_ZN2v88internal12AsmJsScanner4SeekEm:
.LFB5676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	8(%rdi), %rcx
	cmpq	%rdx, %rsi
	jb	.L340
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	sarq	%rax
	addq	%rdx, %rax
	cmpq	%rax, %rsi
	jnb	.L340
	subq	%rdx, %rsi
	leaq	(%rcx,%rsi,2), %rax
	movq	%rax, 16(%rdi)
.L341:
	movq	$0, 8(%r12)
	pxor	%xmm0, %xmm0
	leaq	.L351(%rip), %r13
	movl	$0, 16(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movb	$0, 284(%r12)
	movups	%xmm0, 24(%r12)
.L343:
	movq	(%r12), %rbx
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	sarq	%rax
	addq	32(%rbx), %rax
	movq	%rax, 24(%r12)
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L344
.L382:
	movzwl	(%rax), %esi
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	leal	-9(%rsi), %eax
	cmpl	$117, %eax
	ja	.L349
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12AsmJsScanner4SeekEm,"a",@progbits
	.align 4
	.align 4
.L351:
	.long	.L343-.L351
	.long	.L374-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L343-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L343-.L351
	.long	.L352-.L351
	.long	.L354-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L354-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L349-.L351
	.long	.L353-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L352-.L351
	.long	.L352-.L351
	.long	.L352-.L351
	.long	.L350-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L350-.L351
	.long	.L349-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.section	.text._ZN2v88internal12AsmJsScanner4SeekEm
	.p2align 4,,10
	.p2align 3
.L349:
	movl	%esi, %eax
	orl	$32, %eax
	subl	$97, %eax
	cmpl	$25, %eax
	jbe	.L370
	cmpl	$95, %esi
	je	.L370
	cmpl	$36, %esi
	je	.L370
	cmpl	$46, %esi
	je	.L371
	leal	-48(%rsi), %eax
	cmpl	$9, %eax
	jbe	.L371
.L373:
	movl	$-2, 8(%r12)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L350:
	movl	%esi, 8(%r12)
.L339:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner17ConsumeIdentifierEi
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner21ConsumeCompareOrShiftEi
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner13ConsumeStringEi
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	movb	$1, 284(%r12)
	jmp	.L343
.L353:
	movq	(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L358
.L383:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	cmpl	$47, %edx
	je	.L375
	cmpl	$42, %edx
	jne	.L361
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner15ConsumeCCommentEv
	testb	%al, %al
	jne	.L343
	jmp	.L373
.L367:
	movq	16(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L384:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	cmpl	$10, %edx
	je	.L374
.L375:
	movq	(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jb	.L384
	cmpb	$0, 48(%rbx)
	je	.L366
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L340:
	cmpb	$0, 48(%rdi)
	movq	%rsi, 32(%rdi)
	movq	%rcx, 16(%rdi)
	jne	.L341
	movq	(%rdi), %rax
	call	*40(%rax)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L344:
	cmpb	$0, 48(%rbx)
	jne	.L385
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L386
	addq	$2, 16(%rbx)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L385:
	addq	$2, %rax
	movq	%rax, 16(%rbx)
.L347:
	movl	$-1, 8(%r12)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L386:
	movq	16(%rbx), %rax
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L371:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner13ConsumeNumberEi
.L358:
	.cfi_restore_state
	cmpb	$0, 48(%rbx)
	je	.L360
	addq	$2, %rax
	movq	%rax, 16(%rbx)
.L361:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdx
	cmpq	%rdx, %rax
	jbe	.L368
	subq	$2, %rax
	movq	%rax, 16(%rdi)
.L369:
	movl	$47, 8(%r12)
	jmp	.L339
.L366:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L367
	addq	$2, 16(%rbx)
	jmp	.L343
.L360:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L362
	addq	$2, 16(%rbx)
	jmp	.L361
.L362:
	movq	16(%rbx), %rax
	jmp	.L383
.L368:
	movq	32(%rdi), %rcx
	subq	%rdx, %rax
	movq	%rdx, 16(%rdi)
	sarq	%rax
	cmpb	$0, 48(%rdi)
	leaq	-1(%rax,%rcx), %rax
	movq	%rax, 32(%rdi)
	jne	.L369
	movq	(%rdi), %rax
	call	*40(%rax)
	jmp	.L369
	.cfi_endproc
.LFE5676:
	.size	_ZN2v88internal12AsmJsScanner4SeekEm, .-_ZN2v88internal12AsmJsScanner4SeekEm
	.section	.text._ZN2v88internal12AsmJsScanner4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScanner4NextEv
	.type	_ZN2v88internal12AsmJsScanner4NextEv, @function
_ZN2v88internal12AsmJsScanner4NextEv:
.LFB5673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, 48(%rdi)
	movl	8(%rdi), %eax
	jne	.L432
	leal	2(%rax), %edx
	cmpl	$1, %edx
	jbe	.L387
	movl	%eax, 12(%rdi)
	movq	24(%rdi), %rax
	leaq	.L399(%rip), %r13
	movb	$0, 284(%rdi)
	movq	%rax, 32(%rdi)
.L391:
	movq	(%r12), %rbx
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	sarq	%rax
	addq	32(%rbx), %rax
	movq	%rax, 24(%r12)
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L392
.L429:
	movzwl	(%rax), %esi
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	leal	-9(%rsi), %eax
	cmpl	$117, %eax
	ja	.L397
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12AsmJsScanner4NextEv,"a",@progbits
	.align 4
	.align 4
.L399:
	.long	.L391-.L399
	.long	.L421-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L391-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L391-.L399
	.long	.L400-.L399
	.long	.L402-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L398-.L399
	.long	.L398-.L399
	.long	.L402-.L399
	.long	.L398-.L399
	.long	.L398-.L399
	.long	.L398-.L399
	.long	.L398-.L399
	.long	.L398-.L399
	.long	.L398-.L399
	.long	.L397-.L399
	.long	.L401-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L398-.L399
	.long	.L398-.L399
	.long	.L400-.L399
	.long	.L400-.L399
	.long	.L400-.L399
	.long	.L398-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L398-.L399
	.long	.L397-.L399
	.long	.L398-.L399
	.long	.L398-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L397-.L399
	.long	.L398-.L399
	.long	.L398-.L399
	.long	.L398-.L399
	.long	.L398-.L399
	.section	.text._ZN2v88internal12AsmJsScanner4NextEv
.L405:
	cmpb	$0, 48(%rbx)
	je	.L407
	addq	$2, %rax
	movq	%rax, 16(%rbx)
.L408:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdx
	cmpq	%rdx, %rax
	jbe	.L415
	subq	$2, %rax
	movq	%rax, 16(%rdi)
.L416:
	movl	$47, 8(%r12)
.L387:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movl	%eax, 12(%rdi)
	movq	24(%rdi), %rax
	movb	$0, 48(%rdi)
	movq	%rax, 32(%rdi)
	movl	16(%rdi), %eax
	movl	$0, 16(%rdi)
	movl	%eax, 8(%rdi)
	movq	40(%rdi), %rax
	movq	$0, 40(%rdi)
	movq	%rax, 24(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L397:
	.cfi_restore_state
	movl	%esi, %eax
	orl	$32, %eax
	subl	$97, %eax
	cmpl	$25, %eax
	jbe	.L417
	cmpl	$95, %esi
	je	.L417
	cmpl	$36, %esi
	je	.L417
	cmpl	$46, %esi
	je	.L418
	leal	-48(%rsi), %eax
	cmpl	$9, %eax
	jbe	.L418
.L420:
	movl	$-2, 8(%r12)
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L398:
	movl	%esi, 8(%r12)
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L417:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner17ConsumeIdentifierEi
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner21ConsumeCompareOrShiftEi
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner13ConsumeStringEi
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movb	$1, 284(%r12)
	jmp	.L391
.L401:
	movq	(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L405
.L430:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	cmpl	$47, %edx
	je	.L422
	cmpl	$42, %edx
	jne	.L408
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner15ConsumeCCommentEv
	testb	%al, %al
	jne	.L391
	jmp	.L420
.L414:
	movq	16(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L431:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	cmpl	$10, %edx
	je	.L421
.L422:
	movq	(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jb	.L431
	cmpb	$0, 48(%rbx)
	je	.L413
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L392:
	cmpb	$0, 48(%rbx)
	jne	.L433
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L434
	addq	$2, 16(%rbx)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L433:
	addq	$2, %rax
	movq	%rax, 16(%rbx)
.L395:
	movl	$-1, 8(%r12)
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L434:
	movq	16(%rbx), %rax
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L418:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner13ConsumeNumberEi
.L413:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L414
	addq	$2, 16(%rbx)
	jmp	.L391
.L407:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L409
	addq	$2, 16(%rbx)
	jmp	.L408
.L409:
	movq	16(%rbx), %rax
	jmp	.L430
.L415:
	movq	32(%rdi), %rcx
	subq	%rdx, %rax
	movq	%rdx, 16(%rdi)
	sarq	%rax
	cmpb	$0, 48(%rdi)
	leaq	-1(%rcx,%rax), %rax
	movq	%rax, 32(%rdi)
	jne	.L416
	movq	(%rdi), %rax
	call	*40(%rax)
	jmp	.L416
	.cfi_endproc
.LFE5673:
	.size	_ZN2v88internal12AsmJsScanner4NextEv, .-_ZN2v88internal12AsmJsScanner4NextEv
	.section	.text._ZN2v88internal12AsmJsScannerC2EPNS0_20Utf16CharacterStreamE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AsmJsScannerC2EPNS0_20Utf16CharacterStreamE
	.type	_ZN2v88internal12AsmJsScannerC2EPNS0_20Utf16CharacterStreamE, @function
_ZN2v88internal12AsmJsScannerC2EPNS0_20Utf16CharacterStreamE:
.LFB5671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movl	$26989, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	208(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-64(%rbp), %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	72(%rdi), %rax
	movups	%xmm0, 24(%rdi)
	movss	.LC8(%rip), %xmm0
	movq	%rax, 56(%rdi)
	leaq	144(%rdi), %rax
	movq	%rax, 96(%rdi)
	leaq	200(%rdi), %rax
	movq	%rax, 152(%rdi)
	leaq	256(%rdi), %rax
	movq	%rsi, (%rdi)
	movq	%r12, %rsi
	movss	%xmm0, 128(%rdi)
	movss	%xmm0, 184(%rdi)
	movq	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movq	$0, 64(%rdi)
	movb	$0, 72(%rdi)
	movb	$0, 88(%rdi)
	movq	$1, 104(%rdi)
	movq	$0, 112(%rdi)
	movq	$0, 120(%rdi)
	movq	$0, 136(%rdi)
	movq	$0, 144(%rdi)
	movq	$1, 160(%rdi)
	movq	$0, 168(%rdi)
	movq	$0, 176(%rdi)
	movq	$0, 192(%rdi)
	movq	$0, 200(%rdi)
	movq	%rax, 208(%rdi)
	movq	$1, 216(%rdi)
	movq	$0, 224(%rdi)
	movq	$0, 232(%rdi)
	movq	$0, 248(%rdi)
	movq	$0, 256(%rdi)
	movl	$0, 264(%rdi)
	movq	$0x000000000, 272(%rdi)
	movl	$0, 280(%rdi)
	movb	$0, 284(%rdi)
	movss	%xmm0, 240(%rdi)
	movq	%r14, %rdi
	movq	%rbx, -80(%rbp)
	movw	%cx, -64(%rbp)
	movb	$110, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9999, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L436
	call	_ZdlPv@PLT
.L436:
	movl	$24941, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%dx, -64(%rbp)
	movb	$120, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9998, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L437
	call	_ZdlPv@PLT
.L437:
	movl	$25185, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%ax, -64(%rbp)
	movb	$115, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9997, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L438
	call	_ZdlPv@PLT
.L438:
	movl	$25710, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1970238054, -64(%rbp)
	movw	%ax, -60(%rbp)
	movq	$6, -72(%rbp)
	movb	$0, -58(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9996, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L439
	call	_ZdlPv@PLT
.L439:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1936679777, -64(%rbp)
	movq	$4, -72(%rbp)
	movb	$0, -60(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9995, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L440
	call	_ZdlPv@PLT
.L440:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1852404577, -64(%rbp)
	movq	$4, -72(%rbp)
	movb	$0, -60(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9994, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L441
	call	_ZdlPv@PLT
.L441:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1851880545, -64(%rbp)
	movq	$4, -72(%rbp)
	movb	$0, -60(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9993, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L442
	call	_ZdlPv@PLT
.L442:
	movl	$28515, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%ax, -64(%rbp)
	movb	$115, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9992, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L443
	call	_ZdlPv@PLT
.L443:
	movl	$26995, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%ax, -64(%rbp)
	movb	$110, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9991, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L444
	call	_ZdlPv@PLT
.L444:
	movl	$24948, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%ax, -64(%rbp)
	movb	$110, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9990, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L445
	call	_ZdlPv@PLT
.L445:
	movl	$30821, %r11d
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%r11w, -64(%rbp)
	movb	$112, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9989, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L446
	call	_ZdlPv@PLT
.L446:
	movl	$28524, %r10d
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%r10w, -64(%rbp)
	movb	$103, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9988, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L447
	call	_ZdlPv@PLT
.L447:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1851880545, -64(%rbp)
	movb	$50, -60(%rbp)
	movq	$5, -72(%rbp)
	movb	$0, -59(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9987, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L448
	call	_ZdlPv@PLT
.L448:
	movl	$28528, %r9d
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%r9w, -64(%rbp)
	movb	$119, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9986, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L449
	call	_ZdlPv@PLT
.L449:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1819635049, -64(%rbp)
	movq	$4, -72(%rbp)
	movb	$0, -60(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9985, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L450
	call	_ZdlPv@PLT
.L450:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$863661155, -64(%rbp)
	movb	$50, -60(%rbp)
	movq	$5, -72(%rbp)
	movb	$0, -59(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9984, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1818846563, -64(%rbp)
	movq	$4, -72(%rbp)
	movb	$0, -60(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9983, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L452
	call	_ZdlPv@PLT
.L452:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1869573222, -64(%rbp)
	movb	$114, -60(%rbp)
	movq	$5, -72(%rbp)
	movb	$0, -59(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9982, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L453
	call	_ZdlPv@PLT
.L453:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1953657203, -64(%rbp)
	movq	$4, -72(%rbp)
	movb	$0, -60(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9981, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L454
	call	_ZdlPv@PLT
.L454:
	movabsq	$7021800393469619785, %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movq	%rax, -64(%rbp)
	movb	$121, -56(%rbp)
	movq	$9, -72(%rbp)
	movb	$0, -55(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9980, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L455
	call	_ZdlPv@PLT
.L455:
	movl	$31073, %r8d
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movabsq	$8246725578396166485, %rax
	movw	%r8w, -56(%rbp)
	movq	%rax, -64(%rbp)
	movq	$10, -72(%rbp)
	movb	$0, -54(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9979, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L456
	call	_ZdlPv@PLT
.L456:
	movl	$31073, %edi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movabsq	$8246725568682552905, %rax
	movw	%di, -56(%rbp)
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	movq	$10, -72(%rbp)
	movb	$0, -54(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9978, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L457
	call	_ZdlPv@PLT
.L457:
	movl	$24946, %esi
	movq	%r14, %rdi
	movq	%rbx, -80(%rbp)
	movabsq	$8232921179844667733, %rax
	movw	%si, -56(%rbp)
	movq	%r12, %rsi
	movq	%rax, -64(%rbp)
	movb	$121, -54(%rbp)
	movq	$11, -72(%rbp)
	movb	$0, -53(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9977, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L458
	call	_ZdlPv@PLT
.L458:
	movl	$31073, %ecx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movabsq	$8246725551536238153, %rax
	movw	%cx, -56(%rbp)
	movq	%rax, -64(%rbp)
	movq	$10, -72(%rbp)
	movb	$0, -54(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9976, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L459
	call	_ZdlPv@PLT
.L459:
	movl	$24946, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movabsq	$8232916790388091221, %rax
	movw	%dx, -56(%rbp)
	movq	%rax, -64(%rbp)
	movb	$121, -54(%rbp)
	movq	$11, -72(%rbp)
	movb	$0, -53(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9975, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L460
	call	_ZdlPv@PLT
.L460:
	movabsq	$4697873936244763718, %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movq	%rax, -64(%rbp)
	movl	$2036429426, -56(%rbp)
	movq	$12, -72(%rbp)
	movb	$0, -52(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9974, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L461
	call	_ZdlPv@PLT
.L461:
	movabsq	$4698440184733068358, %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movq	%rax, -64(%rbp)
	movl	$2036429426, -56(%rbp)
	movq	$12, -72(%rbp)
	movb	$0, -52(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9973, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L462
	call	_ZdlPv@PLT
.L462:
	movl	$69, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movq	$1, -72(%rbp)
	movw	%ax, -64(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9972, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L463
	call	_ZdlPv@PLT
.L463:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$808537676, -64(%rbp)
	movq	$4, -72(%rbp)
	movb	$0, -60(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9971, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L464
	call	_ZdlPv@PLT
.L464:
	movl	$20044, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%ax, -64(%rbp)
	movb	$50, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9970, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L465
	call	_ZdlPv@PLT
.L465:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$843534156, -64(%rbp)
	movb	$69, -60(%rbp)
	movq	$5, -72(%rbp)
	movb	$0, -59(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9969, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L466
	call	_ZdlPv@PLT
.L466:
	movl	$17712, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$826756940, -64(%rbp)
	movw	%ax, -60(%rbp)
	movq	$6, -72(%rbp)
	movb	$0, -58(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9968, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L467
	call	_ZdlPv@PLT
.L467:
	movl	$18768, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%ax, -64(%rbp)
	movq	$2, -72(%rbp)
	movb	$0, -62(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9967, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L468
	call	_ZdlPv@PLT
.L468:
	movl	$24369, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1414680915, -64(%rbp)
	movw	%ax, -60(%rbp)
	movb	$50, -58(%rbp)
	movq	$7, -72(%rbp)
	movb	$0, -57(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9966, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L469
	call	_ZdlPv@PLT
.L469:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1414680915, -64(%rbp)
	movb	$50, -60(%rbp)
	movq	$5, -72(%rbp)
	movb	$0, -59(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9965, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L470
	call	_ZdlPv@PLT
.L470:
	movabsq	$8751735898823355977, %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movq	%rax, -64(%rbp)
	movq	$8, -72(%rbp)
	movb	$0, -56(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9964, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L471
	call	_ZdlPv@PLT
.L471:
	movl	$24910, %r11d
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%r11w, -64(%rbp)
	movb	$78, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9963, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L472
	call	_ZdlPv@PLT
.L472:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1752457549, -64(%rbp)
	movq	$4, -72(%rbp)
	movb	$0, -60(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9962, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L473
	call	_ZdlPv@PLT
.L473:
	leaq	152(%r13), %r14
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movabsq	$8389754676633367137, %rax
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	movb	$115, -56(%rbp)
	movq	$9, -72(%rbp)
	movb	$0, -55(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9961, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L474
	call	_ZdlPv@PLT
.L474:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1634038370, -64(%rbp)
	movb	$107, -60(%rbp)
	movq	$5, -72(%rbp)
	movb	$0, -59(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9960, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L475
	call	_ZdlPv@PLT
.L475:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1702060387, -64(%rbp)
	movq	$4, -72(%rbp)
	movb	$0, -60(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9959, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L476
	call	_ZdlPv@PLT
.L476:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1936617315, -64(%rbp)
	movb	$116, -60(%rbp)
	movq	$5, -72(%rbp)
	movb	$0, -59(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9958, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L477
	call	_ZdlPv@PLT
.L477:
	movabsq	$7310870969309884259, %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movq	%rax, -64(%rbp)
	movq	$8, -72(%rbp)
	movb	$0, -56(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9957, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L478
	call	_ZdlPv@PLT
.L478:
	movl	$27765, %r10d
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1634100580, -64(%rbp)
	movw	%r10w, -60(%rbp)
	movb	$116, -58(%rbp)
	movq	$7, -72(%rbp)
	movb	$0, -57(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9956, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L479
	call	_ZdlPv@PLT
.L479:
	movl	$28516, %r9d
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%r9w, -64(%rbp)
	movq	$2, -72(%rbp)
	movb	$0, -62(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9955, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L480
	call	_ZdlPv@PLT
.L480:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1702063205, -64(%rbp)
	movq	$4, -72(%rbp)
	movb	$0, -60(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9954, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L481
	call	_ZdlPv@PLT
.L481:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1818326629, -64(%rbp)
	movq	$4, -72(%rbp)
	movb	$0, -60(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9953, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L482
	call	_ZdlPv@PLT
.L482:
	movl	$28518, %r8d
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%r8w, -64(%rbp)
	movb	$114, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9952, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L483
	call	_ZdlPv@PLT
.L483:
	movabsq	$7957695015192261990, %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movq	%rax, -64(%rbp)
	movq	$8, -72(%rbp)
	movb	$0, -56(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9951, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L484
	call	_ZdlPv@PLT
.L484:
	movl	$26217, %edi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%di, -64(%rbp)
	movq	%r14, %rdi
	movq	$2, -72(%rbp)
	movb	$0, -62(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9950, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L485
	call	_ZdlPv@PLT
.L485:
	movl	$25966, %esi
	movq	%r14, %rdi
	movq	%rbx, -80(%rbp)
	movw	%si, -64(%rbp)
	movq	%r12, %rsi
	movb	$119, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9949, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L486
	call	_ZdlPv@PLT
.L486:
	movl	$28274, %ecx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1970562418, -64(%rbp)
	movw	%cx, -60(%rbp)
	movq	$6, -72(%rbp)
	movb	$0, -58(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9948, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L487
	call	_ZdlPv@PLT
.L487:
	movl	$26723, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1953068915, -64(%rbp)
	movw	%dx, -60(%rbp)
	movq	$6, -72(%rbp)
	movb	$0, -58(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9947, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L488
	call	_ZdlPv@PLT
.L488:
	movl	$24950, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movw	%ax, -64(%rbp)
	movb	$114, -62(%rbp)
	movq	$3, -72(%rbp)
	movb	$0, -61(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9946, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L489
	call	_ZdlPv@PLT
.L489:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movl	$1818847351, -64(%rbp)
	movb	$101, -60(%rbp)
	movq	$5, -72(%rbp)
	movb	$0, -59(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_iESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movl	$-9945, (%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L490
	call	_ZdlPv@PLT
.L490:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L493
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L493:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5671:
	.size	_ZN2v88internal12AsmJsScannerC2EPNS0_20Utf16CharacterStreamE, .-_ZN2v88internal12AsmJsScannerC2EPNS0_20Utf16CharacterStreamE
	.globl	_ZN2v88internal12AsmJsScannerC1EPNS0_20Utf16CharacterStreamE
	.set	_ZN2v88internal12AsmJsScannerC1EPNS0_20Utf16CharacterStreamE,_ZN2v88internal12AsmJsScannerC2EPNS0_20Utf16CharacterStreamE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal12AsmJsScannerC2EPNS0_20Utf16CharacterStreamE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal12AsmJsScannerC2EPNS0_20Utf16CharacterStreamE, @function
_GLOBAL__sub_I__ZN2v88internal12AsmJsScannerC2EPNS0_20Utf16CharacterStreamE:
.LFB6734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE6734:
	.size	_GLOBAL__sub_I__ZN2v88internal12AsmJsScannerC2EPNS0_20Utf16CharacterStreamE, .-_GLOBAL__sub_I__ZN2v88internal12AsmJsScannerC2EPNS0_20Utf16CharacterStreamE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal12AsmJsScannerC2EPNS0_20Utf16CharacterStreamE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4292870144
	.long	1106247679
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC8:
	.long	1065353216
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
