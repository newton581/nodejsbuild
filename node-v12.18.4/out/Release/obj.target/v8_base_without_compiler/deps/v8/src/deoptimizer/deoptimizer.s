	.file	"deoptimizer.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5077:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5077:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5078:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5078:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5080:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5080:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,"axG",@progbits,_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.type	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, @function
_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv:
.LFB10870:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10870:
	.size	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, .-_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.section	.text._ZN2v88internal12_GLOBAL__N_117ActivationsFinderD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117ActivationsFinderD2Ev, @function
_ZN2v88internal12_GLOBAL__N_117ActivationsFinderD2Ev:
.LFB32900:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE32900:
	.size	_ZN2v88internal12_GLOBAL__N_117ActivationsFinderD2Ev, .-_ZN2v88internal12_GLOBAL__N_117ActivationsFinderD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_117ActivationsFinderD1Ev,_ZN2v88internal12_GLOBAL__N_117ActivationsFinderD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_117ActivationsFinderD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117ActivationsFinderD0Ev, @function
_ZN2v88internal12_GLOBAL__N_117ActivationsFinderD0Ev:
.LFB32902:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE32902:
	.size	_ZN2v88internal12_GLOBAL__N_117ActivationsFinderD0Ev, .-_ZN2v88internal12_GLOBAL__N_117ActivationsFinderD0Ev
	.section	.rodata._ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"1U == uint32_value()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0, @function
_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0:
.LFB33938:
	.cfi_startproc
	cmpb	$5, (%rdi)
	ja	.L9
	movzbl	(%rdi), %eax
	leaq	.L11(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0,"a",@progbits
	.align 4
	.align 4
.L11:
	.long	.L9-.L11
	.long	.L15-.L11
	.long	.L14-.L11
	.long	.L13-.L11
	.long	.L12-.L11
	.long	.L10-.L11
	.section	.text._ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	.p2align 4,,10
	.p2align 3
.L13:
	movq	24(%rdi), %rax
	movl	$2147483648, %edx
	movl	$4294967295, %ecx
	addq	%rax, %rdx
	cmpq	%rcx, %rdx
	jbe	.L22
.L9:
	movq	8(%rdi), %rax
	movq	24(%rax), %rax
	movq	304(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jne	.L17
	movq	8(%rdi), %rax
	movq	24(%rax), %rax
	movq	120(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	24(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	24(%rdi), %eax
	testl	%eax, %eax
	js	.L9
.L22:
	salq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movslq	24(%rdi), %rax
	salq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	cmpl	$1, %eax
	jne	.L23
	movq	8(%rdi), %rax
	movq	24(%rax), %rax
	movq	112(%rax), %rax
	ret
.L23:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE33938:
	.size	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0, .-_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	.section	.rodata._ZN2v88internal15TranslatedValue17MaterializeSimpleEv.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"internal error: unexpected materialization."
	.section	.text._ZN2v88internal15TranslatedValue17MaterializeSimpleEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal15TranslatedValue17MaterializeSimpleEv.part.0, @function
_ZN2v88internal15TranslatedValue17MaterializeSimpleEv.part.0:
.LFB33939:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	%rax, %rsi
	movq	8(%rbx), %rax
	movq	24(%rax), %rdi
	cmpq	304(%rdi), %rsi
	je	.L25
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L26
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L40:
	movq	%rax, 16(%rbx)
	movb	$2, 1(%rbx)
.L24:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	cmpb	$9, (%rbx)
	ja	.L24
	movzbl	(%rbx), %eax
	leaq	.L31(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal15TranslatedValue17MaterializeSimpleEv.part.0,"a",@progbits
	.align 4
	.align 4
.L31:
	.long	.L30-.L31
	.long	.L30-.L31
	.long	.L36-.L31
	.long	.L35-.L31
	.long	.L34-.L31
	.long	.L30-.L31
	.long	.L33-.L31
	.long	.L32-.L31
	.long	.L30-.L31
	.long	.L30-.L31
	.section	.text._ZN2v88internal15TranslatedValue17MaterializeSimpleEv.part.0
	.p2align 4,,10
	.p2align 3
.L26:
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L41
.L28:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%rsi, (%rax)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L36:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	cvtsi2sdl	24(%rbx), %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movb	$2, 1(%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L33:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	cvtss2sd	24(%rbx), %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L35:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	cvtsi2sdq	24(%rbx), %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movb	$2, 1(%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L34:
	movl	24(%rbx), %eax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movb	$2, 1(%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L32:
	movsd	24(%rbx), %xmm0
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movb	$2, 1(%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L24
.L30:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rsi, -32(%rbp)
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-32(%rbp), %rsi
	movq	-24(%rbp), %rdi
	jmp	.L28
	.cfi_endproc
.LFE33939:
	.size	_ZN2v88internal15TranslatedValue17MaterializeSimpleEv.part.0, .-_ZN2v88internal15TranslatedValue17MaterializeSimpleEv.part.0
	.section	.text._ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0, @function
_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0:
.LFB34270:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	$8, 8(%rdi)
	movl	$64, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	movl	$512, %edi
	movq	%rax, (%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	call	_Znwm@PLT
	movq	%r12, 40(%rbx)
	movq	%rax, %xmm0
	leaq	512(%rax), %rdx
	movq	%rax, (%r12)
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 72(%rbx)
	movq	%rdx, 32(%rbx)
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	movups	%xmm0, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE34270:
	.size	_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0, .-_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0
	.section	.text._ZN2v88internal10FixedArray3setEiNS0_6ObjectE,"axG",@progbits,_ZN2v88internal10FixedArray3setEiNS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	.type	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE, @function
_ZN2v88internal10FixedArray3setEiNS0_6ObjectE:
.LFB14044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leal	16(,%rsi,8), %ebx
	movq	(%rdi), %rax
	movslq	%ebx, %rbx
	movq	%rdx, -1(%rbx,%rax)
	testb	$1, %dl
	je	.L44
	movq	%rdx, %r14
	movq	%rdi, %r13
	movq	(%rdi), %rdi
	movq	%rdx, %r12
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testl	$262144, %eax
	jne	.L56
	testb	$24, %al
	je	.L44
.L58:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L57
.L44:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testb	$24, %al
	jne	.L58
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L57:
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE14044:
	.size	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE, .-_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	.section	.text._ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv,"axG",@progbits,_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv
	.type	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv, @function
_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv:
.LFB18168:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L65
.L60:
	movq	7(%rdx), %rax
	testb	$1, %al
	jne	.L66
.L64:
	movq	(%rdi), %rax
	movq	7(%rax), %rax
	movq	7(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L60
	movq	39(%rax), %rcx
	testb	$1, %cl
	je	.L60
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L60
	movq	31(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L64
	movq	(%rdi), %rax
	movq	7(%rax), %rax
	ret
	.cfi_endproc
.LFE18168:
	.size	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv, .-_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB25289:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L73
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L76
.L67:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L67
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE25289:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internal11FrameWriter12PushRawValueElPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"    0x%012lx: [top + %3d] <- 0x%012lx ;  %s"
	.section	.text._ZN2v88internal11FrameWriter12PushRawValueElPKc,"axG",@progbits,_ZN2v88internal11FrameWriter12PushRawValueElPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11FrameWriter12PushRawValueElPKc
	.type	_ZN2v88internal11FrameWriter12PushRawValueElPKc, @function
_ZN2v88internal11FrameWriter12PushRawValueElPKc:
.LFB25746:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	movq	%rdx, %r9
	movq	8(%rdi), %rdx
	movq	%rsi, %r8
	subl	$8, %eax
	movl	%eax, 24(%rdi)
	movq	%rsi, 320(%rdx,%rax)
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L77
	movq	(%rax), %rax
	movl	24(%rdi), %edx
	movq	8(%rdi), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	jmp	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	ret
	.cfi_endproc
.LFE25746:
	.size	_ZN2v88internal11FrameWriter12PushRawValueElPKc, .-_ZN2v88internal11FrameWriter12PushRawValueElPKc
	.section	.rodata._ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc.str1.1,"aMS",@progbits,1
.LC4:
	.string	"    0x%012lx: [top + %3d] <- "
.LC5:
	.string	"0x%012lx <Smi %d>"
.LC6:
	.string	" ;  %s"
	.section	.text._ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc,"axG",@progbits,_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	.type	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc, @function
_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc:
.LFB25747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	24(%rdi), %eax
	subl	$8, %eax
	movl	%eax, 24(%rdi)
	movq	%rsi, 320(%rdx,%rax)
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L79
	movq	%rsi, -32(%rbp)
	movq	(%rax), %rax
	movq	%rdi, %rbx
	movl	24(%rdi), %edx
	movq	8(%rdi), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	movq	%rdx, %rcx
	addq	272(%rsi), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-32(%rbp), %rsi
	testb	$1, %sil
	je	.L88
	movq	16(%rbx), %rax
	leaq	-32(%rbp), %rdi
	movq	(%rax), %rax
	movq	144(%rax), %rsi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
.L82:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	leaq	.LC6(%rip), %rsi
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L79:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	%rsi, %rdx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	sarq	$32, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L82
.L89:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25747:
	.size	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc, .-_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	.section	.text._ZN2v88internal15DeoptimizerDataC2EPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15DeoptimizerDataC2EPNS0_4HeapE
	.type	_ZN2v88internal15DeoptimizerDataC2EPNS0_4HeapE, @function
_ZN2v88internal15DeoptimizerDataC2EPNS0_4HeapE:
.LFB25758:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	%rsi, %r8
	pxor	%xmm0, %xmm0
	leaq	32(%rdi), %rdx
	movq	$0, 24(%rdi)
	leaq	8(%rdi), %rsi
	movq	$0, 32(%rdi)
	movups	%xmm0, 8(%rdi)
	movq	%r8, %rdi
	jmp	_ZN2v88internal4Heap19RegisterStrongRootsENS0_14FullObjectSlotES2_@PLT
	.cfi_endproc
.LFE25758:
	.size	_ZN2v88internal15DeoptimizerDataC2EPNS0_4HeapE, .-_ZN2v88internal15DeoptimizerDataC2EPNS0_4HeapE
	.globl	_ZN2v88internal15DeoptimizerDataC1EPNS0_4HeapE
	.set	_ZN2v88internal15DeoptimizerDataC1EPNS0_4HeapE,_ZN2v88internal15DeoptimizerDataC2EPNS0_4HeapE
	.section	.text._ZN2v88internal15DeoptimizerDataD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15DeoptimizerDataD2Ev
	.type	_ZN2v88internal15DeoptimizerDataD2Ev, @function
_ZN2v88internal15DeoptimizerDataD2Ev:
.LFB25761:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	leaq	8(%rdi), %rsi
	movq	%r8, %rdi
	jmp	_ZN2v88internal4Heap21UnregisterStrongRootsENS0_14FullObjectSlotE@PLT
	.cfi_endproc
.LFE25761:
	.size	_ZN2v88internal15DeoptimizerDataD2Ev, .-_ZN2v88internal15DeoptimizerDataD2Ev
	.globl	_ZN2v88internal15DeoptimizerDataD1Ev
	.set	_ZN2v88internal15DeoptimizerDataD1Ev,_ZN2v88internal15DeoptimizerDataD2Ev
	.section	.text._ZN2v88internal15DeoptimizerData16deopt_entry_codeENS0_14DeoptimizeKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15DeoptimizerData16deopt_entry_codeENS0_14DeoptimizeKindE
	.type	_ZN2v88internal15DeoptimizerData16deopt_entry_codeENS0_14DeoptimizeKindE, @function
_ZN2v88internal15DeoptimizerData16deopt_entry_codeENS0_14DeoptimizeKindE:
.LFB25763:
	.cfi_startproc
	endbr64
	movzbl	%sil, %esi
	movq	8(%rdi,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE25763:
	.size	_ZN2v88internal15DeoptimizerData16deopt_entry_codeENS0_14DeoptimizeKindE, .-_ZN2v88internal15DeoptimizerData16deopt_entry_codeENS0_14DeoptimizeKindE
	.section	.text._ZN2v88internal15DeoptimizerData20set_deopt_entry_codeENS0_14DeoptimizeKindENS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15DeoptimizerData20set_deopt_entry_codeENS0_14DeoptimizeKindENS0_4CodeE
	.type	_ZN2v88internal15DeoptimizerData20set_deopt_entry_codeENS0_14DeoptimizeKindENS0_4CodeE, @function
_ZN2v88internal15DeoptimizerData20set_deopt_entry_codeENS0_14DeoptimizeKindENS0_4CodeE:
.LFB25764:
	.cfi_startproc
	endbr64
	movzbl	%sil, %esi
	movq	%rdx, 8(%rdi,%rsi,8)
	ret
	.cfi_endproc
.LFE25764:
	.size	_ZN2v88internal15DeoptimizerData20set_deopt_entry_codeENS0_14DeoptimizeKindENS0_4CodeE, .-_ZN2v88internal15DeoptimizerData20set_deopt_entry_codeENS0_14DeoptimizeKindENS0_4CodeE
	.section	.rodata._ZN2v88internal11Deoptimizer20FindDeoptimizingCodeEm.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"code.kind() == Code::OPTIMIZED_FUNCTION"
	.section	.text._ZN2v88internal11Deoptimizer20FindDeoptimizingCodeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer20FindDeoptimizingCodeEm
	.type	_ZN2v88internal11Deoptimizer20FindDeoptimizingCodeEm, @function
_ZN2v88internal11Deoptimizer20FindDeoptimizingCodeEm:
.LFB25765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	testb	$1, %al
	jne	.L109
.L95:
	xorl	%eax, %eax
.L102:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L110
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	31(%rax), %rax
	movq	(%rdi), %r12
	leaq	-56(%rbp), %rdi
	movq	%rsi, %rbx
	leaq	-48(%rbp), %r13
	movq	39(%rax), %rax
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal13NativeContext23DeoptimizedCodeListHeadEv@PLT
	cmpq	%rax, 88(%r12)
	jne	.L96
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L101:
	movq	31(%rax), %rax
	movq	7(%rax), %rax
	cmpq	%rax, 88(%r12)
	je	.L95
.L96:
	movq	%rax, -48(%rbp)
	movl	43(%rax), %edx
	testb	$62, %dl
	jne	.L111
	testl	%edx, %edx
	js	.L112
.L98:
	leaq	-1(%rax), %r14
	cmpq	%r14, %rbx
	jb	.L101
	movq	-1(%rax), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r14
	cmpq	%r14, %rbx
	jb	.L100
	movq	-48(%rbp), %rax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L112:
	movq	%r13, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	cmpq	%rax, %rbx
	jnb	.L99
.L108:
	movq	-48(%rbp), %rax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%r13, %rdi
	call	_ZNK2v88internal4Code21OffHeapInstructionEndEv@PLT
	cmpq	%rax, %rbx
	jnb	.L108
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-48(%rbp), %rax
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25765:
	.size	_ZN2v88internal11Deoptimizer20FindDeoptimizingCodeEm, .-_ZN2v88internal11Deoptimizer20FindDeoptimizingCodeEm
	.section	.rodata._ZN2v88internal11Deoptimizer4GrabEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"(result) != nullptr"
	.section	.text._ZN2v88internal11Deoptimizer4GrabEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer4GrabEPNS0_7IsolateE
	.type	_ZN2v88internal11Deoptimizer4GrabEPNS0_7IsolateE, @function
_ZN2v88internal11Deoptimizer4GrabEPNS0_7IsolateE:
.LFB25767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	41040(%rdi), %rax
	movq	32(%rax), %r12
	testq	%r12, %r12
	je	.L135
	movq	%rdi, %r13
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L115
	call	free@PLT
.L115:
	movl	64(%r12), %eax
	movq	72(%r12), %rdi
	testl	%eax, %eax
	jle	.L116
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L120:
	movq	(%rdi,%rbx,8), %r8
	cmpq	%r8, 56(%r12)
	je	.L117
	testq	%r8, %r8
	je	.L117
	movq	%r8, %rdi
	addq	$1, %rbx
	call	free@PLT
	movl	64(%r12), %eax
	movq	72(%r12), %rdi
	cmpl	%ebx, %eax
	jg	.L120
.L116:
	testq	%rdi, %rdi
	je	.L121
.L136:
	call	_ZdaPv@PLT
.L121:
	movq	$0, 56(%r12)
	movq	$0, 72(%r12)
	movq	41040(%r13), %rax
	movq	$0, 32(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L120
	testq	%rdi, %rdi
	jne	.L136
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L135:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25767:
	.size	_ZN2v88internal11Deoptimizer4GrabEPNS0_7IsolateE, .-_ZN2v88internal11Deoptimizer4GrabEPNS0_7IsolateE
	.section	.text._ZN2v88internal11Deoptimizer21MarkAllCodeForContextENS0_13NativeContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer21MarkAllCodeForContextENS0_13NativeContextE
	.type	_ZN2v88internal11Deoptimizer21MarkAllCodeForContextENS0_13NativeContextE, @function
_ZN2v88internal11Deoptimizer21MarkAllCodeForContextENS0_13NativeContextE:
.LFB25798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	leaq	-8(%rbp), %rdi
	call	_ZN2v88internal13NativeContext21OptimizedCodeListHeadEv@PLT
	movq	-8(%rbp), %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	je	.L137
	leaq	-37592(%rdx), %rsi
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L139:
	movq	31(%rax), %rdx
	movl	15(%rdx), %edx
	movq	31(%rax), %rcx
	orl	$1, %edx
	movl	%edx, 15(%rcx)
	movq	31(%rax), %rax
	movq	7(%rax), %rax
	cmpq	%rax, 88(%rsi)
	je	.L137
.L140:
	testb	$62, 43(%rax)
	je	.L139
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L137:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25798:
	.size	_ZN2v88internal11Deoptimizer21MarkAllCodeForContextENS0_13NativeContextE, .-_ZN2v88internal11Deoptimizer21MarkAllCodeForContextENS0_13NativeContextE
	.section	.rodata._ZN2v88internal11Deoptimizer10MessageForENS0_14DeoptimizeKindE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"soft"
.LC10:
	.string	"lazy"
.LC11:
	.string	"eager"
.LC12:
	.string	"Unsupported deopt kind"
	.section	.text._ZN2v88internal11Deoptimizer10MessageForENS0_14DeoptimizeKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer10MessageForENS0_14DeoptimizeKindE
	.type	_ZN2v88internal11Deoptimizer10MessageForENS0_14DeoptimizeKindE, @function
_ZN2v88internal11Deoptimizer10MessageForENS0_14DeoptimizeKindE:
.LFB25801:
	.cfi_startproc
	endbr64
	cmpb	$1, %dil
	je	.L145
	cmpb	$2, %dil
	je	.L146
	testb	%dil, %dil
	je	.L147
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	.LC9(%rip), %rax
	ret
	.cfi_endproc
.LFE25801:
	.size	_ZN2v88internal11Deoptimizer10MessageForENS0_14DeoptimizeKindE, .-_ZN2v88internal11Deoptimizer10MessageForENS0_14DeoptimizeKindE
	.section	.text._ZN2v88internal11Deoptimizer17FindOptimizedCodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer17FindOptimizedCodeEv
	.type	_ZN2v88internal11Deoptimizer17FindOptimizedCodeEv, @function
_ZN2v88internal11Deoptimizer17FindOptimizedCodeEv:
.LFB25827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rsi
	call	_ZN2v88internal11Deoptimizer20FindDeoptimizingCodeEm
	testq	%rax, %rax
	je	.L156
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movq	32(%rbx), %rsi
	movq	(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate14FindCodeObjectEm@PLT
	.cfi_endproc
.LFE25827:
	.size	_ZN2v88internal11Deoptimizer17FindOptimizedCodeEv, .-_ZN2v88internal11Deoptimizer17FindOptimizedCodeEv
	.section	.rodata._ZN2v88internal11Deoptimizer17PrintFunctionNameEv.str1.1,"aMS",@progbits,1
.LC13:
	.string	"%s"
	.section	.text._ZN2v88internal11Deoptimizer17PrintFunctionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer17PrintFunctionNameEv
	.type	_ZN2v88internal11Deoptimizer17PrintFunctionNameEv, @function
_ZN2v88internal11Deoptimizer17PrintFunctionNameEv:
.LFB25828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	testb	$1, %al
	je	.L158
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	je	.L160
.L158:
	movq	16(%rbx), %rax
	movl	43(%rax), %edi
	shrl	%edi
	andl	$31, %edi
	call	_ZN2v88internal4Code11Kind2StringENS1_4KindE@PLT
	leaq	.LC13(%rip), %rsi
	movq	%rax, %rdx
	movq	304(%rbx), %rax
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movq	304(%rdi), %rax
	leaq	8(%rdi), %rdi
	movq	(%rax), %rax
	movq	144(%rax), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	.cfi_endproc
.LFE25828:
	.size	_ZN2v88internal11Deoptimizer17PrintFunctionNameEv, .-_ZN2v88internal11Deoptimizer17PrintFunctionNameEv
	.section	.text._ZNK2v88internal11Deoptimizer8functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11Deoptimizer8functionEv
	.type	_ZNK2v88internal11Deoptimizer8functionEv, @function
_ZNK2v88internal11Deoptimizer8functionEv:
.LFB25829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	movq	8(%rdi), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L162
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L166
.L164:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L164
	.cfi_endproc
.LFE25829:
	.size	_ZNK2v88internal11Deoptimizer8functionEv, .-_ZNK2v88internal11Deoptimizer8functionEv
	.section	.text._ZNK2v88internal11Deoptimizer13compiled_codeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11Deoptimizer13compiled_codeEv
	.type	_ZNK2v88internal11Deoptimizer13compiled_codeEv, @function
_ZNK2v88internal11Deoptimizer13compiled_codeEv:
.LFB25830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	movq	16(%rdi), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L168
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L172
.L170:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L170
	.cfi_endproc
.LFE25830:
	.size	_ZNK2v88internal11Deoptimizer13compiled_codeEv, .-_ZNK2v88internal11Deoptimizer13compiled_codeEv
	.section	.text._ZN2v88internal11DeoptimizerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11DeoptimizerD2Ev
	.type	_ZN2v88internal11DeoptimizerD2Ev, @function
_ZN2v88internal11DeoptimizerD2Ev:
.LFB25832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	304(%rdi), %r12
	testq	%r12, %r12
	je	.L174
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L176
	movq	(%r12), %r13
	subl	$1, 152(%r13)
	je	.L204
.L176:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L174:
	movq	280(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L178
	call	_ZdlPv@PLT
.L178:
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L179
	movq	248(%rbx), %rax
	movq	216(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L180
	.p2align 4,,10
	.p2align 3
.L181:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L181
	movq	176(%rbx), %rdi
.L180:
	call	_ZdlPv@PLT
.L179:
	movq	136(%rbx), %r15
	movq	128(%rbx), %r12
	cmpq	%r12, %r15
	je	.L182
	.p2align 4,,10
	.p2align 3
.L186:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L183
	movq	112(%r12), %rax
	movq	80(%r12), %r13
	leaq	8(%rax), %r14
	cmpq	%r13, %r14
	jbe	.L184
	.p2align 4,,10
	.p2align 3
.L185:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %r14
	ja	.L185
	movq	40(%r12), %rdi
.L184:
	call	_ZdlPv@PLT
.L183:
	addq	$120, %r12
	cmpq	%r12, %r15
	jne	.L186
	movq	128(%rbx), %r12
.L182:
	testq	%r12, %r12
	je	.L173
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	movq	144(%r13), %rdi
	call	fclose@PLT
	movq	$0, 144(%r13)
	jmp	.L176
	.cfi_endproc
.LFE25832:
	.size	_ZN2v88internal11DeoptimizerD2Ev, .-_ZN2v88internal11DeoptimizerD2Ev
	.globl	_ZN2v88internal11DeoptimizerD1Ev
	.set	_ZN2v88internal11DeoptimizerD1Ev,_ZN2v88internal11DeoptimizerD2Ev
	.section	.text._ZN2v88internal11Deoptimizer23DeleteFrameDescriptionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer23DeleteFrameDescriptionsEv
	.type	_ZN2v88internal11Deoptimizer23DeleteFrameDescriptionsEv, @function
_ZN2v88internal11Deoptimizer23DeleteFrameDescriptionsEv:
.LFB25834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L206
	call	free@PLT
.L206:
	movl	64(%r12), %eax
	movq	72(%r12), %r8
	testl	%eax, %eax
	jle	.L207
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L211:
	movq	(%r8,%rbx,8), %rdi
	cmpq	%rdi, 56(%r12)
	je	.L208
	testq	%rdi, %rdi
	je	.L208
	call	free@PLT
	movl	64(%r12), %eax
	addq	$1, %rbx
	movq	72(%r12), %r8
	cmpl	%ebx, %eax
	jg	.L211
.L207:
	testq	%r8, %r8
	je	.L212
	movq	%r8, %rdi
	call	_ZdaPv@PLT
.L212:
	movq	$0, 56(%r12)
	popq	%rbx
	movq	$0, 72(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L211
	jmp	.L207
	.cfi_endproc
.LFE25834:
	.size	_ZN2v88internal11Deoptimizer23DeleteFrameDescriptionsEv, .-_ZN2v88internal11Deoptimizer23DeleteFrameDescriptionsEv
	.section	.rodata._ZN2v88internal11Deoptimizer22GetDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"kind <= DeoptimizerData::kLastDeoptimizeKind"
	.align 8
.LC15:
	.string	"!data->deopt_entry_code(kind).is_null()"
	.section	.text._ZN2v88internal11Deoptimizer22GetDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer22GetDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE
	.type	_ZN2v88internal11Deoptimizer22GetDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE, @function
_ZN2v88internal11Deoptimizer22GetDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE:
.LFB25835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	41040(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpb	$2, %sil
	jbe	.L232
	leaq	.LC14(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L232:
	movzbl	%sil, %eax
	movq	8(%rdx,%rax,8), %rax
	testq	%rax, %rax
	je	.L233
	addq	$63, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	leaq	.LC15(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25835:
	.size	_ZN2v88internal11Deoptimizer22GetDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE, .-_ZN2v88internal11Deoptimizer22GetDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE
	.section	.rodata._ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmNS0_14DeoptimizeKindE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"type <= DeoptimizerData::kLastDeoptimizeKind"
	.section	.text._ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmNS0_14DeoptimizeKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmNS0_14DeoptimizeKindE
	.type	_ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmNS0_14DeoptimizeKindE, @function
_ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmNS0_14DeoptimizeKindE:
.LFB25836:
	.cfi_startproc
	endbr64
	movq	41040(%rdi), %rcx
	cmpb	$2, %dl
	jbe	.L243
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC16(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movzbl	%dl, %eax
	xorl	%r8d, %r8d
	movq	8(%rcx,%rax,8), %rax
	testq	%rax, %rax
	je	.L234
	addq	$63, %rax
	cmpq	%rsi, %rax
	sete	%r8b
.L234:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE25836:
	.size	_ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmNS0_14DeoptimizeKindE, .-_ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmNS0_14DeoptimizeKindE
	.section	.text._ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmPNS0_14DeoptimizeKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmPNS0_14DeoptimizeKindE
	.type	_ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmPNS0_14DeoptimizeKindE, @function
_ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmPNS0_14DeoptimizeKindE:
.LFB25837:
	.cfi_startproc
	endbr64
	movq	41040(%rdi), %rcx
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L245
	addq	$63, %rax
	cmpq	%rax, %rsi
	je	.L257
.L245:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L247
	addq	$63, %rax
	cmpq	%rax, %rsi
	je	.L258
.L247:
	movq	24(%rcx), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L244
	addq	$63, %rax
	cmpq	%rax, %rsi
	je	.L259
.L244:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	movl	$1, %r8d
	movb	$0, (%rdx)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	movl	$1, %r8d
	movb	$1, (%rdx)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	movl	$1, %r8d
	movb	$2, (%rdx)
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE25837:
	.size	_ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmPNS0_14DeoptimizeKindE, .-_ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmPNS0_14DeoptimizeKindE
	.section	.text._ZN2v88internal11Deoptimizer23GetDeoptimizedCodeCountEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer23GetDeoptimizedCodeCountEPNS0_7IsolateE
	.type	_ZN2v88internal11Deoptimizer23GetDeoptimizedCodeCountEPNS0_7IsolateE, @function
_ZN2v88internal11Deoptimizer23GetDeoptimizedCodeCountEPNS0_7IsolateE:
.LFB25838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	39120(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	88(%rdi), %r14
	je	.L267
	movq	%rdi, %r13
	xorl	%r12d, %r12d
	leaq	-48(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%rbx, %rdi
	movq	%r14, -48(%rbp)
	call	_ZN2v88internal13NativeContext23DeoptimizedCodeListHeadEv@PLT
	cmpq	88(%r13), %rax
	je	.L262
	.p2align 4,,10
	.p2align 3
.L264:
	movq	31(%rax), %rdx
	movl	15(%rdx), %edx
	movq	31(%rax), %rax
	andl	$1, %edx
	cmpb	$1, %dl
	adcl	$0, %r12d
	movq	7(%rax), %rax
	cmpq	%rax, 88(%r13)
	jne	.L264
.L262:
	movq	1959(%r14), %r14
	cmpq	%r14, 88(%r13)
	jne	.L265
.L260:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L271
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L267:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L260
.L271:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25838:
	.size	_ZN2v88internal11Deoptimizer23GetDeoptimizedCodeCountEPNS0_7IsolateE, .-_ZN2v88internal11Deoptimizer23GetDeoptimizedCodeCountEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal11Deoptimizer32TrampolineForBuiltinContinuationENS0_23BuiltinContinuationModeEb.str1.1,"aMS",@progbits,1
.LC17:
	.string	"unreachable code"
	.section	.text._ZN2v88internal11Deoptimizer32TrampolineForBuiltinContinuationENS0_23BuiltinContinuationModeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer32TrampolineForBuiltinContinuationENS0_23BuiltinContinuationModeEb
	.type	_ZN2v88internal11Deoptimizer32TrampolineForBuiltinContinuationENS0_23BuiltinContinuationModeEb, @function
_ZN2v88internal11Deoptimizer32TrampolineForBuiltinContinuationENS0_23BuiltinContinuationModeEb:
.LFB25849:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	je	.L273
	subl	$1, %edi
	cmpl	$2, %edi
	ja	.L274
	xorl	%eax, %eax
	testb	%sil, %sil
	setne	%al
	addl	$73, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	xorl	%eax, %eax
	testb	%sil, %sil
	setne	%al
	addl	$71, %eax
	ret
.L274:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25849:
	.size	_ZN2v88internal11Deoptimizer32TrampolineForBuiltinContinuationENS0_23BuiltinContinuationModeEb, .-_ZN2v88internal11Deoptimizer32TrampolineForBuiltinContinuationENS0_23BuiltinContinuationModeEb
	.section	.text._ZNK2v88internal11Deoptimizer33ComputeInputFrameAboveFpFixedSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11Deoptimizer33ComputeInputFrameAboveFpFixedSizeEv
	.type	_ZNK2v88internal11Deoptimizer33ComputeInputFrameAboveFpFixedSizeEv, @function
_ZNK2v88internal11Deoptimizer33ComputeInputFrameAboveFpFixedSizeEv:
.LFB25863:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	testb	$1, %dl
	jne	.L291
	movl	$16, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	23(%rdx), %rax
	movzwl	41(%rax), %ebx
	addl	$1, %ebx
	movzwl	%bx, %ebx
	movl	%ebx, %edi
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	cmpb	$1, %al
	sbbl	$-1, %ebx
	addq	$8, %rsp
	leal	16(,%rbx,8), %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25863:
	.size	_ZNK2v88internal11Deoptimizer33ComputeInputFrameAboveFpFixedSizeEv, .-_ZNK2v88internal11Deoptimizer33ComputeInputFrameAboveFpFixedSizeEv
	.section	.rodata._ZNK2v88internal11Deoptimizer21ComputeInputFrameSizeEv.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"fixed_size_above_fp + (stack_slots * kSystemPointerSize) - CommonFrameConstants::kFixedFrameSizeAboveFp + outgoing_size == result"
	.section	.text._ZNK2v88internal11Deoptimizer21ComputeInputFrameSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11Deoptimizer21ComputeInputFrameSizeEv
	.type	_ZNK2v88internal11Deoptimizer21ComputeInputFrameSizeEv, @function
_ZNK2v88internal11Deoptimizer21ComputeInputFrameSizeEv:
.LFB25864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rdx
	movq	%rdi, %rbx
	testb	$1, %dl
	je	.L293
	movq	23(%rdx), %rax
	movzwl	41(%rax), %r12d
	addl	$1, %r12d
	movzwl	%r12w, %r12d
	movl	%r12d, %edi
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	cmpb	$1, %al
	sbbl	$-1, %r12d
	leal	16(,%r12,8), %eax
.L293:
	movl	40(%rbx), %edx
	leal	(%rdx,%rax), %r8d
	movq	16(%rbx), %rax
	movl	43(%rax), %eax
	testb	$62, %al
	jne	.L292
	shrl	$4, %eax
	andl	$134217720, %eax
	subl	$16, %eax
	cmpl	%eax, %edx
	jne	.L301
.L292:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	leaq	.LC18(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25864:
	.size	_ZNK2v88internal11Deoptimizer21ComputeInputFrameSizeEv, .-_ZNK2v88internal11Deoptimizer21ComputeInputFrameSizeEv
	.section	.text._ZN2v88internal11Deoptimizer27ComputeIncomingArgumentSizeENS0_18SharedFunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer27ComputeIncomingArgumentSizeENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal11Deoptimizer27ComputeIncomingArgumentSizeENS0_18SharedFunctionInfoE, @function
_ZN2v88internal11Deoptimizer27ComputeIncomingArgumentSizeENS0_18SharedFunctionInfoE:
.LFB25865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movzwl	41(%rdi), %ebx
	addl	$1, %ebx
	movzwl	%bx, %ebx
	movl	%ebx, %edi
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	cmpb	$1, %al
	sbbl	$-1, %ebx
	addq	$8, %rsp
	leal	0(,%rbx,8), %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25865:
	.size	_ZN2v88internal11Deoptimizer27ComputeIncomingArgumentSizeENS0_18SharedFunctionInfoE, .-_ZN2v88internal11Deoptimizer27ComputeIncomingArgumentSizeENS0_18SharedFunctionInfoE
	.section	.text._ZN2v88internal16FrameDescriptionC2Eji,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FrameDescriptionC2Eji
	.type	_ZN2v88internal16FrameDescriptionC2Eji, @function
_ZN2v88internal16FrameDescriptionC2Eji:
.LFB25902:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movl	%edx, 8(%rdi)
	movq	%rdi, %r8
	movdqa	.LC19(%rip), %xmm0
	movq	%rax, (%rdi)
	movl	$16, %ecx
	xorl	%eax, %eax
	leaq	144(%rdi), %rdi
	rep stosq
	movl	$3203260077, %eax
	movups	%xmm0, 16(%r8)
	addq	$320, %r8
	movq	%rax, -16(%r8)
	xorl	%eax, %eax
	movups	%xmm0, -48(%r8)
	movups	%xmm0, -32(%r8)
	movups	%xmm0, -288(%r8)
	movups	%xmm0, -272(%r8)
	movups	%xmm0, -256(%r8)
	movups	%xmm0, -240(%r8)
	movups	%xmm0, -224(%r8)
	movups	%xmm0, -208(%r8)
	movups	%xmm0, -192(%r8)
	testl	%esi, %esi
	je	.L307
	.p2align 4,,10
	.p2align 3
.L310:
	movl	%eax, %edx
	movl	$3203260077, %ecx
	addl	$8, %eax
	movq	%rcx, (%rdx,%r8)
	cmpl	%eax, %esi
	ja	.L310
.L307:
	ret
	.cfi_endproc
.LFE25902:
	.size	_ZN2v88internal16FrameDescriptionC2Eji, .-_ZN2v88internal16FrameDescriptionC2Eji
	.globl	_ZN2v88internal16FrameDescriptionC1Eji
	.set	_ZN2v88internal16FrameDescriptionC1Eji,_ZN2v88internal16FrameDescriptionC2Eji
	.section	.text._ZN2v88internal19TranslationIteratorC2ENS0_9ByteArrayEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TranslationIteratorC2ENS0_9ByteArrayEi
	.type	_ZN2v88internal19TranslationIteratorC2ENS0_9ByteArrayEi, @function
_ZN2v88internal19TranslationIteratorC2ENS0_9ByteArrayEi:
.LFB25906:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	ret
	.cfi_endproc
.LFE25906:
	.size	_ZN2v88internal19TranslationIteratorC2ENS0_9ByteArrayEi, .-_ZN2v88internal19TranslationIteratorC2ENS0_9ByteArrayEi
	.globl	_ZN2v88internal19TranslationIteratorC1ENS0_9ByteArrayEi
	.set	_ZN2v88internal19TranslationIteratorC1ENS0_9ByteArrayEi,_ZN2v88internal19TranslationIteratorC2ENS0_9ByteArrayEi
	.section	.text._ZN2v88internal19TranslationIterator4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TranslationIterator4NextEv
	.type	_ZN2v88internal19TranslationIterator4NextEv, @function
_ZN2v88internal19TranslationIterator4NextEv:
.LFB25908:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %edx
	movq	(%rdi), %r9
	leal	1(%rdx), %eax
	movl	%eax, 8(%rdi)
	leal	16(%rdx), %eax
	cltq
	movzbl	-1(%r9,%rax), %r8d
	movl	%r8d, %eax
	sarl	%r8d
	testb	$1, %al
	je	.L314
	addl	$2, %edx
	xorl	%ecx, %ecx
	movslq	%edx, %rdx
	.p2align 4,,10
	.p2align 3
.L315:
	movl	%edx, 8(%rdi)
	movzbl	14(%r9,%rdx), %eax
	addl	$7, %ecx
	addq	$1, %rdx
	movl	%eax, %esi
	sarl	%eax
	sall	%cl, %eax
	orl	%eax, %r8d
	andl	$1, %esi
	jne	.L315
.L314:
	movl	%r8d, %eax
	shrl	%eax
	movl	%eax, %edx
	negl	%edx
	andl	$1, %r8d
	cmovne	%edx, %eax
	ret
	.cfi_endproc
.LFE25908:
	.size	_ZN2v88internal19TranslationIterator4NextEv, .-_ZN2v88internal19TranslationIterator4NextEv
	.section	.text._ZNK2v88internal19TranslationIterator7HasNextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal19TranslationIterator7HasNextEv
	.type	_ZNK2v88internal19TranslationIterator7HasNextEv, @function
_ZNK2v88internal19TranslationIterator7HasNextEv:
.LFB25909:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	cmpl	%edx, 11(%rax)
	setg	%al
	ret
	.cfi_endproc
.LFE25909:
	.size	_ZNK2v88internal19TranslationIterator7HasNextEv, .-_ZNK2v88internal19TranslationIterator7HasNextEv
	.section	.text._ZN2v88internal17TranslationBuffer15CreateByteArrayEPNS0_7FactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17TranslationBuffer15CreateByteArrayEPNS0_7FactoryE
	.type	_ZN2v88internal17TranslationBuffer15CreateByteArrayEPNS0_7FactoryE, @function
_ZN2v88internal17TranslationBuffer15CreateByteArrayEPNS0_7FactoryE:
.LFB25910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	8(%rbx), %esi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	16(%rbx), %rbx
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rbx, %rbx
	je	.L325
	leaq	15(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L326:
	movl	4(%rbx), %edx
	leaq	24(%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
	movl	4(%rbx), %eax
	movq	8(%rbx), %rbx
	addq	%rax, %rcx
	testq	%rbx, %rbx
	jne	.L326
.L325:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25910:
	.size	_ZN2v88internal17TranslationBuffer15CreateByteArrayEPNS0_7FactoryE, .-_ZN2v88internal17TranslationBuffer15CreateByteArrayEPNS0_7FactoryE
	.section	.rodata._ZN2v88internal11Translation19NumberOfOperandsForENS1_6OpcodeE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"Unexpected translation type"
	.section	.text._ZN2v88internal11Translation19NumberOfOperandsForENS1_6OpcodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation19NumberOfOperandsForENS1_6OpcodeE
	.type	_ZN2v88internal11Translation19NumberOfOperandsForENS1_6OpcodeE, @function
_ZN2v88internal11Translation19NumberOfOperandsForENS1_6OpcodeE:
.LFB25938:
	.cfi_startproc
	endbr64
	cmpl	$26, %edi
	ja	.L333
	movl	%edi, %edi
	leaq	CSWTCH.1161(%rip), %rax
	movl	(%rax,%rdi,4), %eax
	ret
.L333:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC20(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25938:
	.size	_ZN2v88internal11Translation19NumberOfOperandsForENS1_6OpcodeE, .-_ZN2v88internal11Translation19NumberOfOperandsForENS1_6OpcodeE
	.section	.rodata._ZN2v88internal11Translation9StringForENS1_6OpcodeE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"UPDATE_FEEDBACK"
.LC22:
	.string	"BEGIN"
.LC23:
	.string	"BUILTIN_CONTINUATION_FRAME"
	.section	.rodata._ZN2v88internal11Translation9StringForENS1_6OpcodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"JAVA_SCRIPT_BUILTIN_CONTINUATION_FRAME"
	.align 8
.LC25:
	.string	"JAVA_SCRIPT_BUILTIN_CONTINUATION_WITH_CATCH_FRAME"
	.section	.rodata._ZN2v88internal11Translation9StringForENS1_6OpcodeE.str1.1
.LC26:
	.string	"CONSTRUCT_STUB_FRAME"
.LC27:
	.string	"ARGUMENTS_ADAPTOR_FRAME"
.LC28:
	.string	"DUPLICATED_OBJECT"
.LC29:
	.string	"ARGUMENTS_ELEMENTS"
.LC30:
	.string	"ARGUMENTS_LENGTH"
.LC31:
	.string	"CAPTURED_OBJECT"
.LC32:
	.string	"REGISTER"
.LC33:
	.string	"INT32_REGISTER"
.LC34:
	.string	"INT64_REGISTER"
.LC35:
	.string	"UINT32_REGISTER"
.LC36:
	.string	"BOOL_REGISTER"
.LC37:
	.string	"FLOAT_REGISTER"
.LC38:
	.string	"DOUBLE_REGISTER"
.LC39:
	.string	"STACK_SLOT"
.LC40:
	.string	"INT32_STACK_SLOT"
.LC41:
	.string	"INT64_STACK_SLOT"
.LC42:
	.string	"UINT32_STACK_SLOT"
.LC43:
	.string	"BOOL_STACK_SLOT"
.LC44:
	.string	"FLOAT_STACK_SLOT"
.LC45:
	.string	"DOUBLE_STACK_SLOT"
.LC46:
	.string	"LITERAL"
.LC47:
	.string	"INTERPRETED_FRAME"
	.section	.text._ZN2v88internal11Translation9StringForENS1_6OpcodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation9StringForENS1_6OpcodeE
	.type	_ZN2v88internal11Translation9StringForENS1_6OpcodeE, @function
_ZN2v88internal11Translation9StringForENS1_6OpcodeE:
.LFB25939:
	.cfi_startproc
	endbr64
	cmpl	$26, %edi
	ja	.L338
	leaq	.L340(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11Translation9StringForENS1_6OpcodeE,"a",@progbits
	.align 4
	.align 4
.L340:
	.long	.L366-.L340
	.long	.L365-.L340
	.long	.L367-.L340
	.long	.L363-.L340
	.long	.L362-.L340
	.long	.L361-.L340
	.long	.L360-.L340
	.long	.L359-.L340
	.long	.L358-.L340
	.long	.L357-.L340
	.long	.L356-.L340
	.long	.L355-.L340
	.long	.L354-.L340
	.long	.L353-.L340
	.long	.L352-.L340
	.long	.L351-.L340
	.long	.L350-.L340
	.long	.L349-.L340
	.long	.L348-.L340
	.long	.L347-.L340
	.long	.L346-.L340
	.long	.L345-.L340
	.long	.L344-.L340
	.long	.L343-.L340
	.long	.L342-.L340
	.long	.L341-.L340
	.long	.L339-.L340
	.section	.text._ZN2v88internal11Translation9StringForENS1_6OpcodeE
	.p2align 4,,10
	.p2align 3
.L341:
	leaq	.LC46(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	leaq	.LC45(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	leaq	.LC44(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	leaq	.LC43(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	leaq	.LC42(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	leaq	.LC41(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	leaq	.LC40(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	.LC39(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	leaq	.LC38(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	leaq	.LC37(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	leaq	.LC36(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	leaq	.LC35(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	leaq	.LC34(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	leaq	.LC33(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	leaq	.LC32(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	leaq	.LC31(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	leaq	.LC30(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	leaq	.LC29(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	leaq	.LC28(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	leaq	.LC27(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	leaq	.LC26(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	leaq	.LC25(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	leaq	.LC24(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	leaq	.LC23(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	leaq	.LC47(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	leaq	.LC22(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	leaq	.LC21(%rip), %rax
	ret
.L338:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25939:
	.size	_ZN2v88internal11Translation9StringForENS1_6OpcodeE, .-_ZN2v88internal11Translation9StringForENS1_6OpcodeE
	.section	.text._ZN2v88internal23MaterializedObjectStore15GetStackEntriesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23MaterializedObjectStore15GetStackEntriesEv
	.type	_ZN2v88internal23MaterializedObjectStore15GetStackEntriesEv, @function
_ZN2v88internal23MaterializedObjectStore15GetStackEntriesEv:
.LFB25944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	movq	41112(%rbx), %rdi
	movq	4688(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L371
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L375
.L373:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L373
	.cfi_endproc
.LFE25944:
	.size	_ZN2v88internal23MaterializedObjectStore15GetStackEntriesEv, .-_ZN2v88internal23MaterializedObjectStore15GetStackEntriesEv
	.section	.text._ZN2v88internal23MaterializedObjectStore18EnsureStackEntriesEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23MaterializedObjectStore18EnsureStackEntriesEi
	.type	_ZN2v88internal23MaterializedObjectStore18EnsureStackEntriesEi, @function
_ZN2v88internal23MaterializedObjectStore18EnsureStackEntriesEi:
.LFB25945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%rdi, -88(%rbp)
	movl	%esi, -64(%rbp)
	movq	41112(%rbx), %rdi
	movq	4688(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L377
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L378:
	movslq	11(%rsi), %rax
	movl	-64(%rbp), %ecx
	cmpl	%ecx, 11(%rsi)
	jl	.L380
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	cmpl	$10, %ecx
	movl	$10, %esi
	movl	$1, %edx
	cmovge	%ecx, %esi
	addl	%eax, %eax
	cmpl	%esi, %eax
	cmovge	%eax, %esi
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r15
	movslq	11(%rdx), %rcx
	movslq	%ecx, %rax
	testq	%rcx, %rcx
	jle	.L382
	movl	$15, %ebx
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L386:
	movq	(%r15), %rdi
	movq	(%rdx,%rbx), %r12
	leaq	(%rdi,%rbx), %rsi
	movq	%r12, (%rsi)
	testb	$1, %r12b
	je	.L396
	movq	%r12, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -56(%rbp)
	testl	$262144, %eax
	je	.L384
	movq	%r12, %rdx
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r9
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	8(%r9), %rax
.L384:
	testb	$24, %al
	je	.L396
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L396
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L396:
	movq	0(%r13), %rdx
	addl	$1, %r14d
	addq	$8, %rbx
	movslq	11(%rdx), %rsi
	movslq	%esi, %rax
	cmpl	%esi, %r14d
	jl	.L386
.L382:
	movq	-88(%rbp), %rcx
	movl	-64(%rbp), %edi
	movq	(%rcx), %rcx
	movq	88(%rcx), %rdx
	cmpl	%eax, %edi
	jle	.L387
	testb	$1, %dl
	je	.L388
	subl	$1, %edi
	leal	16(,%rax,8), %ebx
	movl	%edi, %r13d
	movslq	%ebx, %rbx
	subl	%eax, %r13d
	leaq	3(%r13,%rax), %r12
	leaq	0(,%r12,8), %rax
	movq	%rdx, %r12
	movq	%rax, -56(%rbp)
	andq	$-262144, %r12
	.p2align 4,,10
	.p2align 3
.L392:
	movq	(%r15), %r13
	leaq	-1(%rbx,%r13), %r14
	movq	%rdx, (%r14)
	movq	8(%r12), %rax
	testl	$262144, %eax
	je	.L394
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-64(%rbp), %rdx
.L394:
	testb	$24, %al
	je	.L393
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L393
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdx
.L393:
	addq	$8, %rbx
	cmpq	-56(%rbp), %rbx
	jne	.L392
.L391:
	movq	-88(%rbp), %rax
	movq	(%rax), %rcx
.L387:
	movq	(%r15), %rax
	movq	%rax, 4688(%rcx)
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L406
.L379:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L388:
	movl	%edi, %r13d
	leal	16(,%rax,8), %ecx
	subl	$1, %r13d
	movslq	%ecx, %rcx
	subl	%eax, %r13d
	leaq	3(%r13,%rax), %rsi
	salq	$3, %rsi
	.p2align 4,,10
	.p2align 3
.L395:
	movq	(%r15), %rax
	addq	$8, %rcx
	movq	%rdx, -9(%rcx,%rax)
	cmpq	%rcx, %rsi
	jne	.L395
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L379
	.cfi_endproc
.LFE25945:
	.size	_ZN2v88internal23MaterializedObjectStore18EnsureStackEntriesEi, .-_ZN2v88internal23MaterializedObjectStore18EnsureStackEntriesEi
	.section	.rodata._ZN2v88internal11Deoptimizer12GetDeoptInfoENS0_4CodeEm.str1.8,"aMS",@progbits,1
	.align 8
.LC48:
	.string	"code.InstructionStart() <= pc && pc <= code.InstructionEnd()"
	.section	.text._ZN2v88internal11Deoptimizer12GetDeoptInfoENS0_4CodeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer12GetDeoptInfoENS0_4CodeEm
	.type	_ZN2v88internal11Deoptimizer12GetDeoptInfoENS0_4CodeEm, @function
_ZN2v88internal11Deoptimizer12GetDeoptInfoENS0_4CodeEm:
.LFB25959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -136(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movl	43(%rdi), %ecx
	testl	%ecx, %ecx
	js	.L423
	leaq	63(%rdi), %rdx
	movq	%rdi, %rax
	cmpq	%rdx, %rsi
	jb	.L410
.L411:
	movslq	39(%rax), %rax
	addq	%rdx, %rax
.L413:
	cmpq	%rax, %r13
	ja	.L410
	movq	-136(%rbp), %rsi
	leaq	-128(%rbp), %r14
	movl	$122880, %edx
	xorl	%r12d, %r12d
	movq	%r14, %rdi
	movl	$-1, %r15d
	movl	$32, %ebx
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	cmpb	$0, -72(%rbp)
	je	.L418
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L417:
	movzbl	-96(%rbp), %edx
	cmpb	$15, %al
	cmove	%edx, %ebx
.L416:
	movq	%r14, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	jne	.L414
.L418:
	cmpq	-112(%rbp), %r13
	jbe	.L414
	movzbl	-104(%rbp), %eax
	cmpb	$13, %al
	je	.L424
	cmpb	$16, %al
	jne	.L417
	movq	%r14, %rdi
	movl	-96(%rbp), %r15d
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	je	.L418
	.p2align 4,,10
	.p2align 3
.L414:
	salq	$32, %r15
	movzbl	%bl, %edx
	orq	%r15, %rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L425
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	movq	-96(%rbp), %r12
	movq	%r14, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	movabsq	$-140735340871681, %rax
	addl	$1, %r12d
	movslq	%r12d, %r12
	addq	%r12, %r12
	andq	%rax, %r12
	movl	-96(%rbp), %eax
	addl	$1, %eax
	cltq
	salq	$31, %rax
	orq	%rax, %r12
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L423:
	leaq	-136(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	cmpq	%rax, %r13
	jb	.L410
	movq	-136(%rbp), %rax
	movl	43(%rax), %edx
	testl	%edx, %edx
	jns	.L412
	movq	%r12, %rdi
	call	_ZNK2v88internal4Code21OffHeapInstructionEndEv@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	.LC48(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L412:
	leaq	63(%rax), %rdx
	jmp	.L411
.L425:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25959:
	.size	_ZN2v88internal11Deoptimizer12GetDeoptInfoENS0_4CodeEm, .-_ZN2v88internal11Deoptimizer12GetDeoptInfoENS0_4CodeEm
	.section	.text._ZN2v88internal11Deoptimizer38ComputeSourcePositionFromBytecodeArrayENS0_18SharedFunctionInfoENS0_9BailoutIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer38ComputeSourcePositionFromBytecodeArrayENS0_18SharedFunctionInfoENS0_9BailoutIdE
	.type	_ZN2v88internal11Deoptimizer38ComputeSourcePositionFromBytecodeArrayENS0_18SharedFunctionInfoENS0_9BailoutIdE, @function
_ZN2v88internal11Deoptimizer38ComputeSourcePositionFromBytecodeArrayENS0_18SharedFunctionInfoENS0_9BailoutIdE:
.LFB25960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	31(%rdi), %rax
	testb	$1, %al
	jne	.L432
.L427:
	movq	7(%rdi), %rax
	testb	$1, %al
	jne	.L433
.L429:
	movq	7(%rdi), %rax
	movq	7(%rax), %rax
.L428:
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L434
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L427
	movq	39(%rax), %rdx
	testb	$1, %dl
	je	.L427
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L427
	movq	31(%rax), %rax
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L433:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L429
	movq	7(%rdi), %rax
	jmp	.L428
.L434:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25960:
	.size	_ZN2v88internal11Deoptimizer38ComputeSourcePositionFromBytecodeArrayENS0_18SharedFunctionInfoENS0_9BailoutIdE, .-_ZN2v88internal11Deoptimizer38ComputeSourcePositionFromBytecodeArrayENS0_18SharedFunctionInfoENS0_9BailoutIdE
	.section	.text._ZN2v88internal15TranslatedValue17NewDeferredObjectEPNS0_15TranslatedStateEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue17NewDeferredObjectEPNS0_15TranslatedStateEii
	.type	_ZN2v88internal15TranslatedValue17NewDeferredObjectEPNS0_15TranslatedStateEii, @function
_ZN2v88internal15TranslatedValue17NewDeferredObjectEPNS0_15TranslatedStateEii:
.LFB25961:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movl	$8, %edi
	movw	%di, (%rax)
	movq	%rsi, 8(%rax)
	movq	$0, 16(%rax)
	movl	%ecx, 24(%rax)
	movl	%edx, 28(%rax)
	ret
	.cfi_endproc
.LFE25961:
	.size	_ZN2v88internal15TranslatedValue17NewDeferredObjectEPNS0_15TranslatedStateEii, .-_ZN2v88internal15TranslatedValue17NewDeferredObjectEPNS0_15TranslatedStateEii
	.section	.text._ZN2v88internal15TranslatedValue18NewDuplicateObjectEPNS0_15TranslatedStateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue18NewDuplicateObjectEPNS0_15TranslatedStateEi
	.type	_ZN2v88internal15TranslatedValue18NewDuplicateObjectEPNS0_15TranslatedStateEi, @function
_ZN2v88internal15TranslatedValue18NewDuplicateObjectEPNS0_15TranslatedStateEi:
.LFB25962:
	.cfi_startproc
	endbr64
	movl	$9, %ecx
	movq	%rsi, 8(%rdi)
	movq	%rdi, %rax
	movw	%cx, (%rdi)
	movq	$0, 16(%rdi)
	movl	%edx, 24(%rdi)
	movl	$-1, 28(%rdi)
	ret
	.cfi_endproc
.LFE25962:
	.size	_ZN2v88internal15TranslatedValue18NewDuplicateObjectEPNS0_15TranslatedStateEi, .-_ZN2v88internal15TranslatedValue18NewDuplicateObjectEPNS0_15TranslatedStateEi
	.section	.text._ZN2v88internal15TranslatedValue8NewFloatEPNS0_15TranslatedStateENS0_7Float32E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue8NewFloatEPNS0_15TranslatedStateENS0_7Float32E
	.type	_ZN2v88internal15TranslatedValue8NewFloatEPNS0_15TranslatedStateENS0_7Float32E, @function
_ZN2v88internal15TranslatedValue8NewFloatEPNS0_15TranslatedStateENS0_7Float32E:
.LFB25963:
	.cfi_startproc
	endbr64
	movl	$6, %ecx
	movq	%rsi, 8(%rdi)
	movq	%rdi, %rax
	movw	%cx, (%rdi)
	movq	$0, 16(%rdi)
	movl	%edx, 24(%rdi)
	ret
	.cfi_endproc
.LFE25963:
	.size	_ZN2v88internal15TranslatedValue8NewFloatEPNS0_15TranslatedStateENS0_7Float32E, .-_ZN2v88internal15TranslatedValue8NewFloatEPNS0_15TranslatedStateENS0_7Float32E
	.section	.text._ZN2v88internal15TranslatedValue9NewDoubleEPNS0_15TranslatedStateENS0_7Float64E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue9NewDoubleEPNS0_15TranslatedStateENS0_7Float64E
	.type	_ZN2v88internal15TranslatedValue9NewDoubleEPNS0_15TranslatedStateENS0_7Float64E, @function
_ZN2v88internal15TranslatedValue9NewDoubleEPNS0_15TranslatedStateENS0_7Float64E:
.LFB25964:
	.cfi_startproc
	endbr64
	movl	$7, %ecx
	movq	%rsi, 8(%rdi)
	movq	%rdi, %rax
	movw	%cx, (%rdi)
	movq	$0, 16(%rdi)
	movq	%rdx, 24(%rdi)
	ret
	.cfi_endproc
.LFE25964:
	.size	_ZN2v88internal15TranslatedValue9NewDoubleEPNS0_15TranslatedStateENS0_7Float64E, .-_ZN2v88internal15TranslatedValue9NewDoubleEPNS0_15TranslatedStateENS0_7Float64E
	.section	.text._ZN2v88internal15TranslatedValue8NewInt32EPNS0_15TranslatedStateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue8NewInt32EPNS0_15TranslatedStateEi
	.type	_ZN2v88internal15TranslatedValue8NewInt32EPNS0_15TranslatedStateEi, @function
_ZN2v88internal15TranslatedValue8NewInt32EPNS0_15TranslatedStateEi:
.LFB25965:
	.cfi_startproc
	endbr64
	movl	$2, %ecx
	movq	%rsi, 8(%rdi)
	movq	%rdi, %rax
	movw	%cx, (%rdi)
	movq	$0, 16(%rdi)
	movl	%edx, 24(%rdi)
	ret
	.cfi_endproc
.LFE25965:
	.size	_ZN2v88internal15TranslatedValue8NewInt32EPNS0_15TranslatedStateEi, .-_ZN2v88internal15TranslatedValue8NewInt32EPNS0_15TranslatedStateEi
	.section	.text._ZN2v88internal15TranslatedValue8NewInt64EPNS0_15TranslatedStateEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue8NewInt64EPNS0_15TranslatedStateEl
	.type	_ZN2v88internal15TranslatedValue8NewInt64EPNS0_15TranslatedStateEl, @function
_ZN2v88internal15TranslatedValue8NewInt64EPNS0_15TranslatedStateEl:
.LFB25966:
	.cfi_startproc
	endbr64
	movl	$3, %ecx
	movq	%rsi, 8(%rdi)
	movq	%rdi, %rax
	movw	%cx, (%rdi)
	movq	$0, 16(%rdi)
	movq	%rdx, 24(%rdi)
	ret
	.cfi_endproc
.LFE25966:
	.size	_ZN2v88internal15TranslatedValue8NewInt64EPNS0_15TranslatedStateEl, .-_ZN2v88internal15TranslatedValue8NewInt64EPNS0_15TranslatedStateEl
	.section	.text._ZN2v88internal15TranslatedValue9NewUInt32EPNS0_15TranslatedStateEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue9NewUInt32EPNS0_15TranslatedStateEj
	.type	_ZN2v88internal15TranslatedValue9NewUInt32EPNS0_15TranslatedStateEj, @function
_ZN2v88internal15TranslatedValue9NewUInt32EPNS0_15TranslatedStateEj:
.LFB25967:
	.cfi_startproc
	endbr64
	movl	$4, %ecx
	movq	%rsi, 8(%rdi)
	movq	%rdi, %rax
	movw	%cx, (%rdi)
	movq	$0, 16(%rdi)
	movl	%edx, 24(%rdi)
	ret
	.cfi_endproc
.LFE25967:
	.size	_ZN2v88internal15TranslatedValue9NewUInt32EPNS0_15TranslatedStateEj, .-_ZN2v88internal15TranslatedValue9NewUInt32EPNS0_15TranslatedStateEj
	.section	.text._ZN2v88internal15TranslatedValue7NewBoolEPNS0_15TranslatedStateEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue7NewBoolEPNS0_15TranslatedStateEj
	.type	_ZN2v88internal15TranslatedValue7NewBoolEPNS0_15TranslatedStateEj, @function
_ZN2v88internal15TranslatedValue7NewBoolEPNS0_15TranslatedStateEj:
.LFB25968:
	.cfi_startproc
	endbr64
	movl	$5, %ecx
	movq	%rsi, 8(%rdi)
	movq	%rdi, %rax
	movw	%cx, (%rdi)
	movq	$0, 16(%rdi)
	movl	%edx, 24(%rdi)
	ret
	.cfi_endproc
.LFE25968:
	.size	_ZN2v88internal15TranslatedValue7NewBoolEPNS0_15TranslatedStateEj, .-_ZN2v88internal15TranslatedValue7NewBoolEPNS0_15TranslatedStateEj
	.section	.text._ZN2v88internal15TranslatedValue9NewTaggedEPNS0_15TranslatedStateENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue9NewTaggedEPNS0_15TranslatedStateENS0_6ObjectE
	.type	_ZN2v88internal15TranslatedValue9NewTaggedEPNS0_15TranslatedStateENS0_6ObjectE, @function
_ZN2v88internal15TranslatedValue9NewTaggedEPNS0_15TranslatedStateENS0_6ObjectE:
.LFB25969:
	.cfi_startproc
	endbr64
	movl	$1, %ecx
	movq	%rsi, 8(%rdi)
	movq	%rdi, %rax
	movw	%cx, (%rdi)
	movq	$0, 16(%rdi)
	movq	%rdx, 24(%rdi)
	ret
	.cfi_endproc
.LFE25969:
	.size	_ZN2v88internal15TranslatedValue9NewTaggedEPNS0_15TranslatedStateENS0_6ObjectE, .-_ZN2v88internal15TranslatedValue9NewTaggedEPNS0_15TranslatedStateENS0_6ObjectE
	.section	.text._ZN2v88internal15TranslatedValue10NewInvalidEPNS0_15TranslatedStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue10NewInvalidEPNS0_15TranslatedStateE
	.type	_ZN2v88internal15TranslatedValue10NewInvalidEPNS0_15TranslatedStateE, @function
_ZN2v88internal15TranslatedValue10NewInvalidEPNS0_15TranslatedStateE:
.LFB25970:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	movq	%rsi, 8(%rdi)
	movq	%rdi, %rax
	movw	%dx, (%rdi)
	movq	$0, 16(%rdi)
	ret
	.cfi_endproc
.LFE25970:
	.size	_ZN2v88internal15TranslatedValue10NewInvalidEPNS0_15TranslatedStateE, .-_ZN2v88internal15TranslatedValue10NewInvalidEPNS0_15TranslatedStateE
	.section	.text._ZNK2v88internal15TranslatedValue7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15TranslatedValue7isolateEv
	.type	_ZNK2v88internal15TranslatedValue7isolateEv, @function
_ZNK2v88internal15TranslatedValue7isolateEv:
.LFB25971:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	24(%rax), %rax
	ret
	.cfi_endproc
.LFE25971:
	.size	_ZNK2v88internal15TranslatedValue7isolateEv, .-_ZNK2v88internal15TranslatedValue7isolateEv
	.section	.text._ZNK2v88internal15TranslatedValue11raw_literalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15TranslatedValue11raw_literalEv
	.type	_ZNK2v88internal15TranslatedValue11raw_literalEv, @function
_ZNK2v88internal15TranslatedValue11raw_literalEv:
.LFB25972:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE25972:
	.size	_ZNK2v88internal15TranslatedValue11raw_literalEv, .-_ZNK2v88internal15TranslatedValue11raw_literalEv
	.section	.text._ZNK2v88internal15TranslatedValue11int32_valueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15TranslatedValue11int32_valueEv
	.type	_ZNK2v88internal15TranslatedValue11int32_valueEv, @function
_ZNK2v88internal15TranslatedValue11int32_valueEv:
.LFB25973:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE25973:
	.size	_ZNK2v88internal15TranslatedValue11int32_valueEv, .-_ZNK2v88internal15TranslatedValue11int32_valueEv
	.section	.text._ZNK2v88internal15TranslatedValue11int64_valueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15TranslatedValue11int64_valueEv
	.type	_ZNK2v88internal15TranslatedValue11int64_valueEv, @function
_ZNK2v88internal15TranslatedValue11int64_valueEv:
.LFB25974:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE25974:
	.size	_ZNK2v88internal15TranslatedValue11int64_valueEv, .-_ZNK2v88internal15TranslatedValue11int64_valueEv
	.section	.text._ZNK2v88internal15TranslatedValue12uint32_valueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15TranslatedValue12uint32_valueEv
	.type	_ZNK2v88internal15TranslatedValue12uint32_valueEv, @function
_ZNK2v88internal15TranslatedValue12uint32_valueEv:
.LFB25975:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE25975:
	.size	_ZNK2v88internal15TranslatedValue12uint32_valueEv, .-_ZNK2v88internal15TranslatedValue12uint32_valueEv
	.section	.text._ZNK2v88internal15TranslatedValue11float_valueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15TranslatedValue11float_valueEv
	.type	_ZNK2v88internal15TranslatedValue11float_valueEv, @function
_ZNK2v88internal15TranslatedValue11float_valueEv:
.LFB25976:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE25976:
	.size	_ZNK2v88internal15TranslatedValue11float_valueEv, .-_ZNK2v88internal15TranslatedValue11float_valueEv
	.section	.text._ZNK2v88internal15TranslatedValue12double_valueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15TranslatedValue12double_valueEv
	.type	_ZNK2v88internal15TranslatedValue12double_valueEv, @function
_ZNK2v88internal15TranslatedValue12double_valueEv:
.LFB25977:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE25977:
	.size	_ZNK2v88internal15TranslatedValue12double_valueEv, .-_ZNK2v88internal15TranslatedValue12double_valueEv
	.section	.text._ZNK2v88internal15TranslatedValue13object_lengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15TranslatedValue13object_lengthEv
	.type	_ZNK2v88internal15TranslatedValue13object_lengthEv, @function
_ZNK2v88internal15TranslatedValue13object_lengthEv:
.LFB25978:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %eax
	ret
	.cfi_endproc
.LFE25978:
	.size	_ZNK2v88internal15TranslatedValue13object_lengthEv, .-_ZNK2v88internal15TranslatedValue13object_lengthEv
	.section	.text._ZNK2v88internal15TranslatedValue12object_indexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15TranslatedValue12object_indexEv
	.type	_ZNK2v88internal15TranslatedValue12object_indexEv, @function
_ZNK2v88internal15TranslatedValue12object_indexEv:
.LFB25979:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE25979:
	.size	_ZNK2v88internal15TranslatedValue12object_indexEv, .-_ZNK2v88internal15TranslatedValue12object_indexEv
	.section	.text._ZNK2v88internal15TranslatedValue11GetRawValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15TranslatedValue11GetRawValueEv
	.type	_ZNK2v88internal15TranslatedValue11GetRawValueEv, @function
_ZNK2v88internal15TranslatedValue11GetRawValueEv:
.LFB25980:
	.cfi_startproc
	endbr64
	cmpb	$2, 1(%rdi)
	je	.L458
	jmp	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	.p2align 4,,10
	.p2align 3
.L458:
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE25980:
	.size	_ZNK2v88internal15TranslatedValue11GetRawValueEv, .-_ZNK2v88internal15TranslatedValue11GetRawValueEv
	.section	.text._ZN2v88internal15TranslatedValue23set_initialized_storageENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue23set_initialized_storageENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal15TranslatedValue23set_initialized_storageENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal15TranslatedValue23set_initialized_storageENS0_6HandleINS0_6ObjectEEE:
.LFB25981:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	movb	$2, 1(%rdi)
	ret
	.cfi_endproc
.LFE25981:
	.size	_ZN2v88internal15TranslatedValue23set_initialized_storageENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal15TranslatedValue23set_initialized_storageENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal15TranslatedValue17MaterializeSimpleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue17MaterializeSimpleEv
	.type	_ZN2v88internal15TranslatedValue17MaterializeSimpleEv, @function
_ZN2v88internal15TranslatedValue17MaterializeSimpleEv:
.LFB25983:
	.cfi_startproc
	endbr64
	cmpb	$2, 1(%rdi)
	je	.L460
	jmp	_ZN2v88internal15TranslatedValue17MaterializeSimpleEv.part.0
	.p2align 4,,10
	.p2align 3
.L460:
	ret
	.cfi_endproc
.LFE25983:
	.size	_ZN2v88internal15TranslatedValue17MaterializeSimpleEv, .-_ZN2v88internal15TranslatedValue17MaterializeSimpleEv
	.section	.text._ZNK2v88internal15TranslatedValue20IsMaterializedObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15TranslatedValue20IsMaterializedObjectEv
	.type	_ZNK2v88internal15TranslatedValue20IsMaterializedObjectEv, @function
_ZNK2v88internal15TranslatedValue20IsMaterializedObjectEv:
.LFB25984:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	subl	$8, %eax
	cmpl	$1, %eax
	setbe	%al
	ret
	.cfi_endproc
.LFE25984:
	.size	_ZNK2v88internal15TranslatedValue20IsMaterializedObjectEv, .-_ZNK2v88internal15TranslatedValue20IsMaterializedObjectEv
	.section	.text._ZNK2v88internal15TranslatedValue26IsMaterializableByDebuggerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15TranslatedValue26IsMaterializableByDebuggerEv
	.type	_ZNK2v88internal15TranslatedValue26IsMaterializableByDebuggerEv, @function
_ZNK2v88internal15TranslatedValue26IsMaterializableByDebuggerEv:
.LFB25985:
	.cfi_startproc
	endbr64
	cmpb	$7, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE25985:
	.size	_ZNK2v88internal15TranslatedValue26IsMaterializableByDebuggerEv, .-_ZNK2v88internal15TranslatedValue26IsMaterializableByDebuggerEv
	.section	.text._ZNK2v88internal15TranslatedValue16GetChildrenCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15TranslatedValue16GetChildrenCountEv
	.type	_ZNK2v88internal15TranslatedValue16GetChildrenCountEv, @function
_ZNK2v88internal15TranslatedValue16GetChildrenCountEv:
.LFB25986:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$8, (%rdi)
	jne	.L464
	movl	28(%rdi), %eax
.L464:
	ret
	.cfi_endproc
.LFE25986:
	.size	_ZNK2v88internal15TranslatedValue16GetChildrenCountEv, .-_ZNK2v88internal15TranslatedValue16GetChildrenCountEv
	.section	.text._ZN2v88internal15TranslatedState13GetUInt64SlotEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState13GetUInt64SlotEmi
	.type	_ZN2v88internal15TranslatedState13GetUInt64SlotEmi, @function
_ZN2v88internal15TranslatedState13GetUInt64SlotEmi:
.LFB25987:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movq	(%rsi,%rdi), %rax
	ret
	.cfi_endproc
.LFE25987:
	.size	_ZN2v88internal15TranslatedState13GetUInt64SlotEmi, .-_ZN2v88internal15TranslatedState13GetUInt64SlotEmi
	.section	.text._ZN2v88internal15TranslatedState13GetUInt32SlotEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState13GetUInt32SlotEmi
	.type	_ZN2v88internal15TranslatedState13GetUInt32SlotEmi, @function
_ZN2v88internal15TranslatedState13GetUInt32SlotEmi:
.LFB25988:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movl	(%rsi,%rdi), %eax
	ret
	.cfi_endproc
.LFE25988:
	.size	_ZN2v88internal15TranslatedState13GetUInt32SlotEmi, .-_ZN2v88internal15TranslatedState13GetUInt32SlotEmi
	.section	.text._ZN2v88internal15TranslatedState12GetFloatSlotEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState12GetFloatSlotEmi
	.type	_ZN2v88internal15TranslatedState12GetFloatSlotEmi, @function
_ZN2v88internal15TranslatedState12GetFloatSlotEmi:
.LFB25989:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movl	(%rsi,%rdi), %eax
	ret
	.cfi_endproc
.LFE25989:
	.size	_ZN2v88internal15TranslatedState12GetFloatSlotEmi, .-_ZN2v88internal15TranslatedState12GetFloatSlotEmi
	.section	.text._ZN2v88internal15TranslatedState13GetDoubleSlotEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState13GetDoubleSlotEmi
	.type	_ZN2v88internal15TranslatedState13GetDoubleSlotEmi, @function
_ZN2v88internal15TranslatedState13GetDoubleSlotEmi:
.LFB25990:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movq	(%rsi,%rdi), %rax
	ret
	.cfi_endproc
.LFE25990:
	.size	_ZN2v88internal15TranslatedState13GetDoubleSlotEmi, .-_ZN2v88internal15TranslatedState13GetDoubleSlotEmi
	.section	.text._ZN2v88internal15TranslatedValue8HandlifyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue8HandlifyEv
	.type	_ZN2v88internal15TranslatedValue8HandlifyEv, @function
_ZN2v88internal15TranslatedValue8HandlifyEv:
.LFB25991:
	.cfi_startproc
	endbr64
	cmpb	$1, (%rdi)
	je	.L480
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	8(%rdi), %rax
	movq	24(%rdi), %rsi
	movq	24(%rax), %r12
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L473
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L474:
	movq	%rax, 16(%rbx)
	movb	$2, 1(%rbx)
	movq	$0, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L481
.L475:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L481:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L475
	.cfi_endproc
.LFE25991:
	.size	_ZN2v88internal15TranslatedValue8HandlifyEv, .-_ZN2v88internal15TranslatedValue8HandlifyEv
	.section	.text._ZN2v88internal15TranslatedFrame16InterpretedFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEiii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedFrame16InterpretedFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEiii
	.type	_ZN2v88internal15TranslatedFrame16InterpretedFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEiii, @function
_ZN2v88internal15TranslatedFrame16InterpretedFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEiii:
.LFB25992:
	.cfi_startproc
	endbr64
	movabsq	$-4294967296, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	40(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	movq	%rax, -40(%rdi)
	movq	%rdx, -32(%rdi)
	movq	$0, -24(%rdi)
	movl	%ecx, -16(%rdi)
	movl	%r8d, -12(%rdi)
	movl	%r9d, -8(%rdi)
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	call	_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0
	movl	%ebx, 4(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25992:
	.size	_ZN2v88internal15TranslatedFrame16InterpretedFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEiii, .-_ZN2v88internal15TranslatedFrame16InterpretedFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEiii
	.section	.text._ZN2v88internal15TranslatedFrame21ArgumentsAdaptorFrameENS0_18SharedFunctionInfoEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedFrame21ArgumentsAdaptorFrameENS0_18SharedFunctionInfoEi
	.type	_ZN2v88internal15TranslatedFrame21ArgumentsAdaptorFrameENS0_18SharedFunctionInfoEi, @function
_ZN2v88internal15TranslatedFrame21ArgumentsAdaptorFrameENS0_18SharedFunctionInfoEi:
.LFB25993:
	.cfi_startproc
	endbr64
	movabsq	$-4294967295, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	40(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	movq	%rsi, -32(%rdi)
	movq	$0, -24(%rdi)
	movl	%edx, -16(%rdi)
	movq	$0, -12(%rdi)
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	call	_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25993:
	.size	_ZN2v88internal15TranslatedFrame21ArgumentsAdaptorFrameENS0_18SharedFunctionInfoEi, .-_ZN2v88internal15TranslatedFrame21ArgumentsAdaptorFrameENS0_18SharedFunctionInfoEi
	.section	.text._ZN2v88internal15TranslatedFrame18ConstructStubFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedFrame18ConstructStubFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi
	.type	_ZN2v88internal15TranslatedFrame18ConstructStubFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi, @function
_ZN2v88internal15TranslatedFrame18ConstructStubFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi:
.LFB25994:
	.cfi_startproc
	endbr64
	movabsq	$-4294967294, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	40(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	movq	%rax, -40(%rdi)
	movq	%rdx, -32(%rdi)
	movq	$0, -24(%rdi)
	movl	%ecx, -16(%rdi)
	movq	$0, -12(%rdi)
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	call	_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0
	movl	%ebx, 4(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25994:
	.size	_ZN2v88internal15TranslatedFrame18ConstructStubFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi, .-_ZN2v88internal15TranslatedFrame18ConstructStubFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi
	.section	.text._ZN2v88internal15TranslatedFrame24BuiltinContinuationFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedFrame24BuiltinContinuationFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi
	.type	_ZN2v88internal15TranslatedFrame24BuiltinContinuationFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi, @function
_ZN2v88internal15TranslatedFrame24BuiltinContinuationFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi:
.LFB25995:
	.cfi_startproc
	endbr64
	movabsq	$-4294967293, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	40(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	movq	%rax, -40(%rdi)
	movq	%rdx, -32(%rdi)
	movq	$0, -24(%rdi)
	movl	%ecx, -16(%rdi)
	movq	$0, -12(%rdi)
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	call	_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0
	movl	%ebx, 4(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25995:
	.size	_ZN2v88internal15TranslatedFrame24BuiltinContinuationFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi, .-_ZN2v88internal15TranslatedFrame24BuiltinContinuationFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi
	.section	.text._ZN2v88internal15TranslatedFrame34JavaScriptBuiltinContinuationFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedFrame34JavaScriptBuiltinContinuationFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi
	.type	_ZN2v88internal15TranslatedFrame34JavaScriptBuiltinContinuationFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi, @function
_ZN2v88internal15TranslatedFrame34JavaScriptBuiltinContinuationFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi:
.LFB25996:
	.cfi_startproc
	endbr64
	movabsq	$-4294967292, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	40(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	movq	%rax, -40(%rdi)
	movq	%rdx, -32(%rdi)
	movq	$0, -24(%rdi)
	movl	%ecx, -16(%rdi)
	movq	$0, -12(%rdi)
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	call	_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0
	movl	%ebx, 4(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25996:
	.size	_ZN2v88internal15TranslatedFrame34JavaScriptBuiltinContinuationFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi, .-_ZN2v88internal15TranslatedFrame34JavaScriptBuiltinContinuationFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi
	.section	.text._ZN2v88internal15TranslatedFrame43JavaScriptBuiltinContinuationWithCatchFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedFrame43JavaScriptBuiltinContinuationWithCatchFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi
	.type	_ZN2v88internal15TranslatedFrame43JavaScriptBuiltinContinuationWithCatchFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi, @function
_ZN2v88internal15TranslatedFrame43JavaScriptBuiltinContinuationWithCatchFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi:
.LFB25997:
	.cfi_startproc
	endbr64
	movabsq	$-4294967291, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	40(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	movq	%rax, -40(%rdi)
	movq	%rdx, -32(%rdi)
	movq	$0, -24(%rdi)
	movl	%ecx, -16(%rdi)
	movq	$0, -12(%rdi)
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	call	_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0
	movl	%ebx, 4(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25997:
	.size	_ZN2v88internal15TranslatedFrame43JavaScriptBuiltinContinuationWithCatchFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi, .-_ZN2v88internal15TranslatedFrame43JavaScriptBuiltinContinuationWithCatchFrameENS0_9BailoutIdENS0_18SharedFunctionInfoEi
	.section	.text._ZN2v88internal15TranslatedFrame13GetValueCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedFrame13GetValueCountEv
	.type	_ZN2v88internal15TranslatedFrame13GetValueCountEv, @function
_ZN2v88internal15TranslatedFrame13GetValueCountEv:
.LFB25998:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L495
	ja	.L504
	movq	8(%rdi), %rax
	movzwl	41(%rax), %eax
	addl	$1, %eax
	movzwl	%ax, %eax
	addl	24(%rdi), %eax
	addl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	subl	$2, %eax
	cmpl	$3, %eax
	ja	.L505
	movl	24(%rdi), %eax
	addl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	movl	24(%rdi), %eax
	addl	$1, %eax
	ret
.L505:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25998:
	.size	_ZN2v88internal15TranslatedFrame13GetValueCountEv, .-_ZN2v88internal15TranslatedFrame13GetValueCountEv
	.section	.text._ZN2v88internal15TranslatedFrame8HandlifyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedFrame8HandlifyEv
	.type	_ZN2v88internal15TranslatedFrame8HandlifyEv, @function
_ZN2v88internal15TranslatedFrame8HandlifyEv:
.LFB25999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L507
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L508
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L509:
	movq	%rax, 16(%r13)
	movq	$0, 8(%r13)
.L507:
	movq	80(%r13), %r15
	movq	56(%r13), %rbx
	movq	72(%r13), %r12
	movq	88(%r13), %r14
	addq	$8, %r15
	.p2align 4,,10
	.p2align 3
.L516:
	cmpq	%rbx, %r14
	je	.L506
.L524:
	cmpb	$1, (%rbx)
	je	.L523
.L512:
	addq	$32, %rbx
	cmpq	%rbx, %r12
	jne	.L516
	movq	(%r15), %rbx
	addq	$8, %r15
	leaq	512(%rbx), %r12
	cmpq	%rbx, %r14
	jne	.L524
.L506:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	24(%rbx), %rsi
	movq	24(%rax), %r13
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L513
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L514:
	movq	%rax, 16(%rbx)
	movb	$2, 1(%rbx)
	movq	$0, 24(%rbx)
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L513:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L525
.L515:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L508:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L526
.L510:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L525:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L515
.L526:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L510
	.cfi_endproc
.LFE25999:
	.size	_ZN2v88internal15TranslatedFrame8HandlifyEv, .-_ZN2v88internal15TranslatedFrame8HandlifyEv
	.section	.rodata._ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE.str1.1,"aMS",@progbits,1
.LC49:
	.string	"  reading input frame %s"
	.section	.rodata._ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE.str1.8,"aMS",@progbits,1
	.align 8
.LC50:
	.string	" => bytecode_offset=%d, args=%d, height=%d, retval=%i(#%i); inputs:\n"
	.align 8
.LC51:
	.string	"  reading arguments adaptor frame %s"
	.section	.rodata._ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE.str1.1
.LC52:
	.string	" => height=%d; inputs:\n"
	.section	.rodata._ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE.str1.8
	.align 8
.LC53:
	.string	"  reading construct stub frame %s"
	.align 8
.LC54:
	.string	" => bailout_id=%d, height=%d; inputs:\n"
	.align 8
.LC55:
	.string	"  reading builtin continuation frame %s"
	.align 8
.LC56:
	.string	"  reading JavaScript builtin continuation frame %s"
	.align 8
.LC57:
	.string	"  reading JavaScript builtin continuation frame with catch %s"
	.align 8
.LC58:
	.string	"We should never get here - unexpected deopt info."
	.section	.text._ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE
	.type	_ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE, @function
_ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE:
.LFB26000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rdx, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal19TranslationIterator4NextEv
	cmpl	$6, %eax
	ja	.L528
	leaq	.L530(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE,"a",@progbits
	.align 4
	.align 4
.L530:
	.long	.L528-.L530
	.long	.L535-.L530
	.long	.L534-.L530
	.long	.L533-.L530
	.long	.L532-.L530
	.long	.L531-.L530
	.long	.L529-.L530
	.section	.text._ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE
	.p2align 4,,10
	.p2align 3
.L529:
	call	_ZN2v88internal19TranslationIterator4NextEv
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%r11,%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %ebx
	testq	%r12, %r12
	je	.L539
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	xorl	%r8d, %r8d
	leaq	-72(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC51(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	%r12, %rdi
	movl	%ebx, %edx
	xorl	%eax, %eax
	leaq	.LC52(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L539
	call	_ZdaPv@PLT
.L539:
	movq	$0, 16(%r14)
	pxor	%xmm0, %xmm0
	leaq	40(%r14), %rdi
	movabsq	$-4294967295, %rax
	movq	%rax, (%r14)
	movq	-80(%rbp), %rax
	movl	%ebx, 24(%r14)
	movq	%rax, 8(%r14)
	movq	$0, 28(%r14)
	movq	$0, 40(%r14)
	movq	$0, 48(%r14)
	movups	%xmm0, 56(%r14)
	movups	%xmm0, 72(%r14)
	movups	%xmm0, 88(%r14)
	movups	%xmm0, 104(%r14)
	call	_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L535:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %ebx
	call	_ZN2v88internal19TranslationIterator4NextEv
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%r11,%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %r15d
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %r13d
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, -84(%rbp)
	testq	%r12, %r12
	je	.L536
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	xorl	%r8d, %r8d
	leaq	-72(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC49(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-80(%rbp), %rax
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r12, %rdi
	movl	%r13d, %r9d
	leaq	.LC50(%rip), %rsi
	movl	%r15d, %r8d
	movzwl	41(%rax), %ecx
	movl	-84(%rbp), %eax
	pushq	%rax
	addl	$1, %ecx
	xorl	%eax, %eax
	movzwl	%cx, %ecx
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-72(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L536
	call	_ZdaPv@PLT
.L536:
	movabsq	$-4294967296, %rax
	movq	$0, 16(%r14)
	movq	%rax, (%r14)
	movq	-80(%rbp), %rax
	movl	%r15d, 24(%r14)
	movq	%rax, 8(%r14)
	movl	-84(%rbp), %eax
	movl	%r13d, 28(%r14)
	movl	%eax, 32(%r14)
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L534:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %ebx
	call	_ZN2v88internal19TranslationIterator4NextEv
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%r11,%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %r13d
	testq	%r12, %r12
	je	.L543
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	xorl	%r8d, %r8d
	leaq	-72(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC55(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	%r12, %rdi
	movl	%r13d, %ecx
	movl	%ebx, %edx
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L543
	call	_ZdaPv@PLT
.L543:
	movabsq	$-4294967293, %rax
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L532:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %ebx
	call	_ZN2v88internal19TranslationIterator4NextEv
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%r11,%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %r13d
	testq	%r12, %r12
	je	.L547
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	xorl	%r8d, %r8d
	leaq	-72(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC57(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	%r12, %rdi
	movl	%r13d, %ecx
	movl	%ebx, %edx
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L547
	call	_ZdaPv@PLT
.L547:
	movabsq	$-4294967291, %rax
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L533:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %ebx
	call	_ZN2v88internal19TranslationIterator4NextEv
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%r11,%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %r13d
	testq	%r12, %r12
	je	.L545
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	xorl	%r8d, %r8d
	leaq	-72(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC56(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	%r12, %rdi
	movl	%r13d, %ecx
	movl	%ebx, %edx
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L545
	call	_ZdaPv@PLT
.L545:
	movabsq	$-4294967292, %rax
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L531:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %ebx
	call	_ZN2v88internal19TranslationIterator4NextEv
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%r11,%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %r13d
	testq	%r12, %r12
	je	.L541
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	xorl	%r8d, %r8d
	leaq	-72(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC53(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	%r12, %rdi
	movl	%r13d, %ecx
	movl	%ebx, %edx
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L541
	call	_ZdaPv@PLT
.L541:
	movabsq	$-4294967294, %rax
.L587:
	movq	%rax, (%r14)
	movq	-80(%rbp), %rax
	movq	$0, 16(%r14)
	movq	%rax, 8(%r14)
	movl	%r13d, 24(%r14)
	movq	$0, 28(%r14)
.L588:
	movq	$0, 40(%r14)
	pxor	%xmm0, %xmm0
	leaq	40(%r14), %rdi
	movq	$0, 48(%r14)
	movups	%xmm0, 56(%r14)
	movups	%xmm0, 72(%r14)
	movups	%xmm0, 88(%r14)
	movups	%xmm0, 104(%r14)
	call	_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0
	movl	%ebx, 4(%r14)
.L527:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L589
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L528:
	.cfi_restore_state
	leaq	.LC58(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L589:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26000:
	.size	_ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE, .-_ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE
	.section	.text._ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	.type	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E, @function
_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E:
.LFB26001:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	16(%rdi), %rcx
	movl	$1, %edx
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L596:
	testl	%edx, %edx
	jle	.L597
.L591:
	subl	$1, %edx
	cmpb	$8, (%rax)
	jne	.L593
	addl	28(%rax), %edx
.L593:
	addq	$32, %rax
	movq	%rax, (%rdi)
	cmpq	%rcx, %rax
	jne	.L596
	movq	24(%rdi), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, 24(%rdi)
	movq	8(%rax), %rax
	leaq	512(%rax), %rcx
	movq	%rax, 8(%rdi)
	movq	%rcx, 16(%rdi)
	movq	%rax, (%rdi)
	testl	%edx, %edx
	jg	.L591
.L597:
	ret
	.cfi_endproc
.LFE26001:
	.size	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E, .-_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	.section	.text._ZN2v88internal15TranslatedState24ComputeArgumentsPositionEmNS0_19CreateArgumentsTypeEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState24ComputeArgumentsPositionEmNS0_19CreateArgumentsTypeEPi
	.type	_ZN2v88internal15TranslatedState24ComputeArgumentsPositionEmNS0_19CreateArgumentsTypeEPi, @function
_ZN2v88internal15TranslatedState24ComputeArgumentsPositionEmNS0_19CreateArgumentsTypeEPi:
.LFB26002:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	cmpq	$38, -8(%rax)
	je	.L608
	movq	%rsi, %rax
	testq	%rcx, %rcx
	je	.L598
	movl	40(%rdi), %esi
	movl	%esi, (%rcx)
.L601:
	cmpb	$2, %dl
	jne	.L598
	subl	40(%rdi), %esi
	movl	$0, %edx
	cmovs	%edx, %esi
	movl	%esi, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L598:
	ret
	.p2align 4,,10
	.p2align 3
.L608:
	testq	%rcx, %rcx
	je	.L598
	movslq	-20(%rax), %r8
	movl	%r8d, (%rcx)
	movl	%r8d, %esi
	jmp	.L601
	.cfi_endproc
.LFE26002:
	.size	_ZN2v88internal15TranslatedState24ComputeArgumentsPositionEmNS0_19CreateArgumentsTypeEPi, .-_ZN2v88internal15TranslatedState24ComputeArgumentsPositionEmNS0_19CreateArgumentsTypeEPi
	.section	.text._ZN2v88internal15TranslatedState18DecompressIfNeededEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState18DecompressIfNeededEl
	.type	_ZN2v88internal15TranslatedState18DecompressIfNeededEl, @function
_ZN2v88internal15TranslatedState18DecompressIfNeededEl:
.LFB26008:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE26008:
	.size	_ZN2v88internal15TranslatedState18DecompressIfNeededEl, .-_ZN2v88internal15TranslatedState18DecompressIfNeededEl
	.section	.rodata._ZN2v88internal15TranslatedState21GetValueByObjectIndexEi.str1.8,"aMS",@progbits,1
	.align 8
.LC59:
	.string	"static_cast<size_t>(object_index) < object_positions_.size()"
	.section	.text._ZN2v88internal15TranslatedState21GetValueByObjectIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState21GetValueByObjectIndexEi
	.type	_ZN2v88internal15TranslatedState21GetValueByObjectIndexEi, @function
_ZN2v88internal15TranslatedState21GetValueByObjectIndexEi:
.LFB26017:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %r8
	movq	120(%rdi), %rdx
	movslq	%esi, %rsi
	movq	96(%rdi), %rax
	subq	104(%rdi), %rax
	subq	%r8, %rdx
	sarq	$3, %rax
	movq	64(%rdi), %r9
	sarq	$3, %rdx
	subq	$1, %rdx
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	80(%rdi), %rax
	subq	%r9, %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rax, %rsi
	jnb	.L623
	movq	%r9, %rax
	subq	72(%rdi), %rax
	sarq	$3, %rax
	addq	%rsi, %rax
	js	.L612
	cmpq	$63, %rax
	jle	.L624
	movq	%rax, %rdx
	sarq	$6, %rdx
.L615:
	movq	%rdx, %rcx
	movq	(%r8,%rdx,8), %rdx
	salq	$6, %rcx
	subq	%rcx, %rax
	leaq	(%rdx,%rax,8), %rsi
.L614:
	movslq	(%rsi), %rdx
	movq	%rdx, %rax
	salq	$4, %rax
	subq	%rdx, %rax
	movq	(%rdi), %rdx
	leaq	(%rdx,%rax,8), %rcx
	movslq	4(%rsi), %rax
	movq	56(%rcx), %rdi
	movq	%rdi, %rdx
	subq	64(%rcx), %rdx
	sarq	$5, %rdx
	addq	%rax, %rdx
	js	.L616
	cmpq	$15, %rdx
	jg	.L617
	salq	$5, %rax
	addq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	leaq	(%r9,%rsi,8), %rsi
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L617:
	movq	%rdx, %rax
	sarq	$4, %rax
.L619:
	movq	%rax, %rsi
	movq	80(%rcx), %rcx
	salq	$4, %rsi
	subq	%rsi, %rdx
	salq	$5, %rdx
	addq	(%rcx,%rax,8), %rdx
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L616:
	movq	%rdx, %rax
	notq	%rax
	shrq	$4, %rax
	notq	%rax
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L612:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L623:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC59(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE26017:
	.size	_ZN2v88internal15TranslatedState21GetValueByObjectIndexEi, .-_ZN2v88internal15TranslatedState21GetValueByObjectIndexEi
	.section	.text._ZN2v88internal15TranslatedState9SkipSlotsEiPNS0_15TranslatedFrameEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState9SkipSlotsEiPNS0_15TranslatedFrameEPi
	.type	_ZN2v88internal15TranslatedState9SkipSlotsEiPNS0_15TranslatedFrameEPi, @function
_ZN2v88internal15TranslatedState9SkipSlotsEiPNS0_15TranslatedFrameEPi:
.LFB26023:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L625
	movslq	(%rcx), %rdi
	movl	$1, %r9d
	.p2align 4,,10
	.p2align 3
.L627:
	movq	56(%rdx), %r8
	movq	%r8, %rax
	subq	64(%rdx), %rax
	sarq	$5, %rax
	addq	%rdi, %rax
	js	.L628
	cmpq	$15, %rax
	jg	.L629
	movq	%rdi, %rax
	subl	$1, %esi
	salq	$5, %rax
	addq	%r8, %rax
	leal	(%r9,%rdi), %r8d
	movl	%r8d, (%rcx)
	cmpb	$8, (%rax)
	je	.L637
.L632:
	addq	$1, %rdi
	testl	%esi, %esi
	jne	.L627
.L625:
	ret
	.p2align 4,,10
	.p2align 3
.L629:
	movq	%rax, %r8
	sarq	$4, %r8
.L631:
	movq	%r8, %r11
	movq	80(%rdx), %r10
	subl	$1, %esi
	salq	$4, %r11
	subq	%r11, %rax
	salq	$5, %rax
	addq	(%r10,%r8,8), %rax
	leal	(%r9,%rdi), %r8d
	movl	%r8d, (%rcx)
	cmpb	$8, (%rax)
	jne	.L632
.L637:
	addl	28(%rax), %esi
	addq	$1, %rdi
	testl	%esi, %esi
	jg	.L627
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L628:
	movq	%rax, %r8
	notq	%r8
	shrq	$4, %r8
	notq	%r8
	jmp	.L631
	.cfi_endproc
.LFE26023:
	.size	_ZN2v88internal15TranslatedState9SkipSlotsEiPNS0_15TranslatedFrameEPi, .-_ZN2v88internal15TranslatedState9SkipSlotsEiPNS0_15TranslatedFrameEPi
	.section	.rodata._ZN2v88internal15TranslatedState34EnsurePropertiesAllocatedAndMarkedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC60:
	.string	"TranslatedValue::kUninitialized == properties_slot->materialization_state()"
	.section	.text._ZN2v88internal15TranslatedState34EnsurePropertiesAllocatedAndMarkedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState34EnsurePropertiesAllocatedAndMarkedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE
	.type	_ZN2v88internal15TranslatedState34EnsurePropertiesAllocatedAndMarkedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal15TranslatedState34EnsurePropertiesAllocatedAndMarkedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE:
.LFB26026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpb	$0, 1(%rsi)
	jne	.L667
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	%rdx, %rbx
	movl	$-16, %esi
	cmpb	$8, 0(%r13)
	je	.L668
.L640:
	movq	24(%r14), %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%rax, %r12
	xorl	%eax, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L644
	.p2align 4,,10
	.p2align 3
.L641:
	movb	$0, 15(%rdx,%rax)
	movq	(%r12), %rdx
	addq	$1, %rax
	cmpl	%eax, 11(%rdx)
	jg	.L641
.L644:
	movb	$1, 1(%r13)
	movq	%r12, 16(%r13)
	movq	24(%r14), %r13
	movq	(%rbx), %rax
	movq	41112(%r13), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L669
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L645:
	movq	(%rbx), %r11
	movl	15(%r11), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	je	.L638
	subl	$1, %edx
	movl	$31, %esi
	leaq	(%rdx,%rdx,2), %rdx
	leaq	55(,%rdx,8), %r9
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L658:
	movl	$32768, %r11d
.L651:
	movq	(%rax), %rdx
	movq	(%rdx,%rsi), %rdx
	shrq	$38, %rdx
	andl	$7, %edx
	cmpl	$2, %edx
	je	.L670
.L655:
	addq	$24, %rsi
	cmpq	%r9, %rsi
	je	.L638
.L673:
	movq	(%rbx), %r11
.L648:
	movq	39(%r11), %rdx
	movq	(%rdx,%rsi), %rdx
	movzbl	7(%r11), %ecx
	movq	%rdx, %rdi
	shrq	$38, %rdx
	shrq	$51, %rdi
	andl	$7, %edx
	movq	%rdi, %r8
	movzbl	8(%r11), %edi
	andl	$1023, %r8d
	subl	%edi, %ecx
	cmpl	%ecx, %r8d
	setl	%r10b
	jl	.L671
	subl	%ecx, %r8d
	movl	$16, %edi
	leal	16(,%r8,8), %r8d
.L650:
	cmpl	$2, %edx
	je	.L658
	cmpb	$2, %dl
	jg	.L652
	je	.L653
.L659:
	xorl	%r11d, %r11d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L668:
	movl	28(%r13), %eax
	leal	-16(,%rax,8), %esi
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L669:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L672
.L646:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L652:
	subl	$3, %edx
	cmpb	$1, %dl
	jbe	.L659
.L653:
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L670:
	movzbl	%r10b, %r10d
	movslq	%ecx, %rcx
	movslq	%r8d, %r8
	movslq	%edi, %rdi
	salq	$14, %r10
	salq	$17, %rcx
	orq	%r10, %rcx
	salq	$27, %rdi
	orq	%r8, %rcx
	orq	%rdi, %rcx
	orq	%rcx, %r11
	andb	$64, %ch
	jne	.L655
	movq	%r11, %rdx
	sarl	$3, %r11d
	movq	(%r12), %rcx
	addq	$24, %rsi
	shrq	$30, %rdx
	andl	$2047, %r11d
	andl	$15, %edx
	subl	%edx, %r11d
	leal	16(,%r11,8), %edx
	movslq	%edx, %rdx
	movb	$2, -1(%rdx,%rcx)
	cmpq	%r9, %rsi
	jne	.L673
.L638:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	movzbl	8(%r11), %edi
	movzbl	8(%r11), %r11d
	addl	%r11d, %r8d
	sall	$3, %edi
	sall	$3, %r8d
	jmp	.L650
.L667:
	leaq	.LC60(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L672:
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L646
	.cfi_endproc
.LFE26026:
	.size	_ZN2v88internal15TranslatedState34EnsurePropertiesAllocatedAndMarkedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal15TranslatedState34EnsurePropertiesAllocatedAndMarkedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE
	.section	.text._ZN2v88internal15TranslatedState18AllocateStorageForEPNS0_15TranslatedValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState18AllocateStorageForEPNS0_15TranslatedValueE
	.type	_ZN2v88internal15TranslatedState18AllocateStorageForEPNS0_15TranslatedValueE, @function
_ZN2v88internal15TranslatedState18AllocateStorageForEPNS0_15TranslatedValueE:
.LFB26027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$8, (%rsi)
	movl	$-16, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L675
	movl	28(%rsi), %eax
	leal	-16(,%rax,8), %r8d
.L675:
	movq	24(%rdi), %rdi
	movl	$1, %edx
	movl	%r8d, %esi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	movq	(%rax), %rcx
	movl	11(%rcx), %esi
	testl	%esi, %esi
	jle	.L682
	.p2align 4,,10
	.p2align 3
.L676:
	movb	$0, 15(%rcx,%rdx)
	movq	(%rax), %rcx
	addq	$1, %rdx
	cmpl	%edx, 11(%rcx)
	jg	.L676
.L682:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26027:
	.size	_ZN2v88internal15TranslatedState18AllocateStorageForEPNS0_15TranslatedValueE, .-_ZN2v88internal15TranslatedState18AllocateStorageForEPNS0_15TranslatedValueE
	.section	.rodata._ZN2v88internal15TranslatedState23EnsureJSObjectAllocatedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC61:
	.string	"map->instance_size() == slot->GetChildrenCount() * kTaggedSize"
	.align 8
.LC62:
	.string	"index.index() >= FixedArray::kHeaderSize / kTaggedSize"
	.align 8
.LC63:
	.string	"(!IsSmi() && (*layout_word_index < length())) || (IsSmi() && (*layout_word_index < 1))"
	.section	.text._ZN2v88internal15TranslatedState23EnsureJSObjectAllocatedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState23EnsureJSObjectAllocatedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE
	.type	_ZN2v88internal15TranslatedState23EnsureJSObjectAllocatedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal15TranslatedState23EnsureJSObjectAllocatedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE:
.LFB26028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$24, %rsp
	cmpb	$8, (%rsi)
	jne	.L686
	movl	28(%rsi), %eax
	leal	0(,%rax,8), %edx
.L686:
	movq	(%rbx), %rax
	movzbl	7(%rax), %eax
	sall	$3, %eax
	cmpl	%edx, %eax
	jne	.L741
	cmpb	$8, (%r12)
	movl	$-16, %esi
	je	.L742
.L688:
	movq	24(%r14), %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%rax, %r13
	xorl	%eax, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L692
	.p2align 4,,10
	.p2align 3
.L689:
	movb	$0, 15(%rdx,%rax)
	movq	0(%r13), %rdx
	addq	$1, %rax
	cmpl	%eax, 11(%rdx)
	jg	.L689
.L692:
	movq	24(%r14), %r14
	movq	(%rbx), %rax
	movq	41112(%r14), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L743
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L693:
	movq	(%rbx), %r8
	movl	15(%r8), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	je	.L695
	subl	$1, %edx
	movl	$31, %edi
	movl	$1, %r11d
	leaq	(%rdx,%rdx,2), %rdx
	leaq	55(,%rdx,8), %r10
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L719:
	movl	$32768, %r15d
.L699:
	movq	(%rax), %rdx
	movq	(%rdx,%rdi), %rdx
	shrq	$38, %rdx
	andl	$7, %edx
	cmpl	$2, %edx
	je	.L744
.L705:
	addq	$24, %rdi
	cmpq	%rdi, %r10
	je	.L695
.L749:
	movq	(%rbx), %r8
.L696:
	movq	39(%r8), %rdx
	movq	(%rdx,%rdi), %rdx
	movzbl	7(%r8), %ecx
	movzbl	8(%r8), %r9d
	movq	%rdx, %rsi
	shrq	$38, %rdx
	shrq	$51, %rsi
	subl	%r9d, %ecx
	andl	$7, %edx
	andl	$1023, %esi
	cmpl	%ecx, %esi
	jl	.L745
	movl	%esi, %r9d
	subl	%ecx, %r9d
	leal	16(,%r9,8), %r14d
	movl	$16, %r9d
.L698:
	cmpl	$2, %edx
	je	.L719
	cmpb	$2, %dl
	jg	.L700
	je	.L701
.L720:
	xorl	%r15d, %r15d
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L742:
	movl	28(%r12), %eax
	leal	-16(,%rax,8), %esi
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L743:
	movq	41088(%r14), %rax
	cmpq	%rax, 41096(%r14)
	je	.L746
.L694:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L700:
	subl	$3, %edx
	cmpb	$1, %dl
	jbe	.L720
.L701:
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L744:
	xorl	%edx, %edx
	cmpl	%ecx, %esi
	movslq	%ecx, %rcx
	movslq	%r14d, %r14
	setl	%dl
	salq	$17, %rcx
	movslq	%r9d, %r9
	salq	$14, %rdx
	salq	$27, %r9
	orq	%rdx, %rcx
	orq	%r14, %rcx
	orq	%r9, %rcx
	orq	%rcx, %r15
	andb	$64, %ch
	je	.L705
	movl	%r15d, %edx
	sarl	$3, %edx
	movl	%edx, %ecx
	andl	$2047, %ecx
	andl	$2046, %edx
	je	.L747
	movq	47(%r8), %rsi
	leal	-16(,%rcx,8), %edx
	testq	%rsi, %rsi
	je	.L706
	movq	%rsi, %r8
	movl	$32, %r14d
	notq	%r8
	movl	%r8d, %r9d
	andl	$1, %r9d
	jne	.L707
	movslq	11(%rsi), %r14
	sall	$3, %r14d
.L707:
	shrq	$30, %r15
	andl	$15, %r15d
	subl	%r15d, %ecx
	cmpl	%r14d, %ecx
	jnb	.L706
	testl	%ecx, %ecx
	leal	31(%rcx), %r14d
	cmovns	%ecx, %r14d
	sarl	$5, %r14d
	andl	$1, %r8d
	jne	.L708
	cmpl	%r14d, 11(%rsi)
	jle	.L708
	movl	%ecx, %r8d
	sarl	$31, %r8d
	shrl	$27, %r8d
	addl	%r8d, %ecx
	andl	$31, %ecx
	subl	%r8d, %ecx
	movl	%r11d, %r8d
	sall	%cl, %r8d
	testb	%r9b, %r9b
	je	.L748
.L712:
	sarq	$32, %rsi
	testl	%esi, %r8d
	sete	%cl
.L714:
	addl	$1, %ecx
.L715:
	movq	0(%r13), %rsi
	addl	$16, %edx
	addq	$24, %rdi
	movslq	%edx, %rdx
	movb	%cl, -1(%rdx,%rsi)
	cmpq	%rdi, %r10
	jne	.L749
.L695:
	movq	%r13, 16(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L745:
	.cfi_restore_state
	movzbl	8(%r8), %r9d
	movzbl	8(%r8), %r14d
	addl	%esi, %r14d
	sall	$3, %r9d
	sall	$3, %r14d
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L706:
	movl	$2, %ecx
	jmp	.L715
.L748:
	leal	0(,%r14,4), %ecx
	movslq	%ecx, %rcx
	movl	15(%rsi,%rcx), %ecx
	testl	%ecx, %r8d
	sete	%cl
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L747:
	leaq	.LC62(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L741:
	leaq	.LC61(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L746:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L694
.L708:
	testb	%r9b, %r9b
	je	.L710
	cmpl	$31, %ecx
	jg	.L710
	movl	%r11d, %r8d
	sall	%cl, %r8d
	jmp	.L712
.L710:
	leaq	.LC63(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE26028:
	.size	_ZN2v88internal15TranslatedState23EnsureJSObjectAllocatedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal15TranslatedState23EnsureJSObjectAllocatedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE
	.section	.rodata._ZN2v88internal15TranslatedState21ResolveCapturedObjectEPNS0_15TranslatedValueE.str1.8,"aMS",@progbits,1
	.align 8
.LC64:
	.string	"TranslatedValue::kCapturedObject == slot->kind()"
	.section	.text._ZN2v88internal15TranslatedState21ResolveCapturedObjectEPNS0_15TranslatedValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState21ResolveCapturedObjectEPNS0_15TranslatedValueE
	.type	_ZN2v88internal15TranslatedState21ResolveCapturedObjectEPNS0_15TranslatedValueE, @function
_ZN2v88internal15TranslatedState21ResolveCapturedObjectEPNS0_15TranslatedValueE:
.LFB26032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	(%rsi), %edx
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpb	$9, %dl
	jne	.L751
	movq	88(%rdi), %r9
	movq	120(%rdi), %rdx
	movq	96(%rdi), %rcx
	subq	104(%rdi), %rcx
	subq	%r9, %rdx
	sarq	$3, %rcx
	movq	64(%rdi), %r8
	sarq	$3, %rdx
	subq	$1, %rdx
	movq	%rdx, %rsi
	salq	$6, %rsi
	leaq	(%rsi,%rcx), %rdx
	movq	80(%rdi), %rcx
	subq	%r8, %rcx
	sarq	$3, %rcx
	addq	%rdx, %rcx
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L767:
	cmpq	$63, %rax
	jg	.L754
	leaq	(%r8,%rdx,8), %r11
.L755:
	movslq	(%r11), %rdx
	movq	%rdx, %rax
	salq	$4, %rax
	subq	%rdx, %rax
	movq	(%rdi), %rdx
	leaq	(%rdx,%rax,8), %r10
	movslq	4(%r11), %rax
	movq	56(%r10), %rsi
	movq	%rsi, %rdx
	subq	64(%r10), %rdx
	sarq	$5, %rdx
	addq	%rax, %rdx
	js	.L757
	cmpq	$15, %rdx
	jg	.L758
	salq	$5, %rax
	addq	%rsi, %rax
	movzbl	(%rax), %edx
	cmpb	$9, %dl
	jne	.L751
.L761:
	movslq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jnb	.L766
	movq	%r8, %rax
	subq	72(%rdi), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	jns	.L767
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
.L756:
	movq	%rdx, %rsi
	movq	(%r9,%rdx,8), %rdx
	salq	$6, %rsi
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %r11
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L758:
	movq	%rdx, %rax
	sarq	$4, %rax
.L760:
	movq	80(%r10), %rsi
	movq	%rax, %r10
	salq	$4, %r10
	subq	%r10, %rdx
	salq	$5, %rdx
	addq	(%rsi,%rax,8), %rdx
	movq	%rdx, %rax
	movzbl	(%rax), %edx
	cmpb	$9, %dl
	je	.L761
.L751:
	cmpb	$8, %dl
	jne	.L768
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L754:
	.cfi_restore_state
	movq	%rax, %rdx
	sarq	$6, %rdx
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L757:
	movq	%rdx, %rax
	notq	%rax
	shrq	$4, %rax
	notq	%rax
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L766:
	leaq	.LC59(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L768:
	leaq	.LC64(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE26032:
	.size	_ZN2v88internal15TranslatedState21ResolveCapturedObjectEPNS0_15TranslatedValueE, .-_ZN2v88internal15TranslatedState21ResolveCapturedObjectEPNS0_15TranslatedValueE
	.section	.rodata._ZN2v88internal15TranslatedState18GetValueAndAdvanceEPNS0_15TranslatedFrameEPi.str1.8,"aMS",@progbits,1
	.align 8
.LC65:
	.string	"TranslatedValue::kUninitialized != slot->materialization_state()"
	.section	.text._ZN2v88internal15TranslatedState18GetValueAndAdvanceEPNS0_15TranslatedFrameEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState18GetValueAndAdvanceEPNS0_15TranslatedFrameEPi
	.type	_ZN2v88internal15TranslatedState18GetValueAndAdvanceEPNS0_15TranslatedFrameEPi, @function
_ZN2v88internal15TranslatedState18GetValueAndAdvanceEPNS0_15TranslatedFrameEPi:
.LFB26029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	(%rdx), %r9
	movq	%rsi, %r8
	movq	%r9, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	56(%rsi), %rbx
	movq	64(%rsi), %rcx
	movq	80(%rsi), %r11
	movq	%rbx, %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	addq	%r9, %rax
	js	.L770
	cmpq	$15, %rax
	jle	.L788
	movq	%rax, %r12
	sarq	$4, %r12
.L773:
	movq	%r12, %rsi
	salq	$4, %rsi
	subq	%rsi, %rax
	movq	%rax, %rsi
	salq	$5, %rsi
	addq	(%r11,%r12,8), %rsi
.L772:
	movq	%r9, %rax
	addl	$1, %r10d
	movl	$1, %r9d
	subl	%eax, %r10d
	jmp	.L774
	.p2align 4,,10
	.p2align 3
.L790:
	cmpq	$15, %rcx
	jg	.L777
	movq	%rax, %rcx
	leal	(%r10,%rax), %r11d
	subl	$1, %r9d
	salq	$5, %rcx
	movl	%r11d, (%rdx)
	addq	%rbx, %rcx
	cmpb	$8, (%rcx)
	je	.L789
.L780:
	addq	$1, %rax
	testl	%r9d, %r9d
	je	.L775
.L781:
	movq	56(%r8), %rbx
	movq	64(%r8), %rcx
	movq	80(%r8), %r11
.L774:
	movq	%rbx, %r12
	subq	%rcx, %r12
	movq	%r12, %rcx
	sarq	$5, %rcx
	addq	%rax, %rcx
	jns	.L790
	movq	%rcx, %rbx
	notq	%rbx
	shrq	$4, %rbx
	notq	%rbx
.L779:
	movq	%rbx, %r12
	subl	$1, %r9d
	salq	$4, %r12
	subq	%r12, %rcx
	salq	$5, %rcx
	addq	(%r11,%rbx,8), %rcx
	leal	(%r10,%rax), %r11d
	movl	%r11d, (%rdx)
	cmpb	$8, (%rcx)
	jne	.L780
.L789:
	addl	28(%rcx), %r9d
	addq	$1, %rax
	testl	%r9d, %r9d
	jg	.L781
.L775:
	cmpb	$9, (%rsi)
	je	.L791
	cmpb	$0, 1(%rsi)
	je	.L792
.L783:
	popq	%rbx
	movq	16(%rsi), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L788:
	.cfi_restore_state
	movq	%r9, %rsi
	salq	$5, %rsi
	addq	%rbx, %rsi
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L777:
	movq	%rcx, %rbx
	sarq	$4, %rbx
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L770:
	movq	%rax, %r12
	notq	%r12
	shrq	$4, %r12
	notq	%r12
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L791:
	call	_ZN2v88internal15TranslatedState21ResolveCapturedObjectEPNS0_15TranslatedValueE
	movq	%rax, %rsi
	cmpb	$0, 1(%rsi)
	jne	.L783
.L792:
	leaq	.LC65(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE26029:
	.size	_ZN2v88internal15TranslatedState18GetValueAndAdvanceEPNS0_15TranslatedFrameEPi, .-_ZN2v88internal15TranslatedState18GetValueAndAdvanceEPNS0_15TranslatedFrameEPi
	.section	.rodata._ZN2v88internal15TranslatedState20InitializeJSObjectAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE.str1.1,"aMS",@progbits,1
.LC66:
	.string	"slot->GetChildrenCount() >= 2"
.LC67:
	.string	"field_value->IsHeapNumber()"
.LC68:
	.string	"kStoreTagged == marker"
	.section	.text._ZN2v88internal15TranslatedState20InitializeJSObjectAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState20InitializeJSObjectAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	.type	_ZN2v88internal15TranslatedState20InitializeJSObjectAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE, @function
_ZN2v88internal15TranslatedState20InitializeJSObjectAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE:
.LFB26030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	cmpb	$8, (%rcx)
	movq	16(%rcx), %r15
	movq	%rsi, -56(%rbp)
	movq	%r8, -64(%rbp)
	je	.L845
.L794:
	leaq	.LC66(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L845:
	movq	%rdx, %r14
	movl	28(%rcx), %edx
	cmpl	$1, %edx
	jle	.L794
	movq	24(%rdi), %rax
	movq	(%r15), %rsi
	movq	%r9, %rcx
	movq	%rdi, %r12
	sall	$3, %edx
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap24NotifyObjectLayoutChangeENS0_10HeapObjectEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	-56(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState18GetValueAndAdvanceEPNS0_15TranslatedFrameEPi
	movq	(%r15), %rdx
	movq	(%rax), %rcx
	movq	%rax, %r13
	movq	%rcx, 7(%rdx)
	movq	(%r15), %rdi
	movq	(%rax), %rdx
	leaq	7(%rdi), %rsi
	testb	$1, %dl
	je	.L799
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	testb	$4, 10(%rcx)
	jne	.L846
.L798:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L799
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L847
.L799:
	cmpb	$8, (%rbx)
	jne	.L804
.L826:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L801:
	leal	2(%r13), %eax
	cmpl	%eax, 28(%rbx)
	jle	.L804
	movq	-56(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState18GetValueAndAdvanceEPNS0_15TranslatedFrameEPi
	movq	(%r15), %rdi
	leaq	16(,%r13,8), %r9
	leaq	-1(%r9,%rdi), %rsi
	movzbl	(%rsi), %edx
	cmpb	$1, %dl
	je	.L848
	cmpb	$2, %dl
	je	.L849
	testb	%dl, %dl
	jne	.L850
	movq	(%rax), %rdx
	movq	%rdx, (%rsi)
	movq	%rax, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	(%r15), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%r9), %rsi
	testb	$1, %dl
	je	.L809
	movq	%rdx, %r10
	andq	$-262144, %r10
	testb	$4, 10(%r10)
	je	.L821
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rax
	movq	(%r15), %rdi
	movq	-72(%rbp), %r9
	movq	(%rax), %rdx
	leaq	-1(%rdi,%r9), %rsi
	testb	$1, %dl
	jne	.L821
	.p2align 4,,10
	.p2align 3
.L809:
	addq	$1, %r13
	cmpb	$8, (%rbx)
	je	.L801
.L804:
	movq	-64(%rbp), %rax
	movq	(%r15), %rdi
	movq	(%rax), %rdx
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	jne	.L851
.L793:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L846:
	.cfi_restore_state
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r15), %rdi
	movq	0(%r13), %rdx
	leaq	7(%rdi), %rsi
	testb	$1, %dl
	jne	.L798
	cmpb	$8, (%rbx)
	je	.L826
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L848:
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L806
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L807:
	movq	%xmm0, (%rsi)
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L849:
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L852
.L811:
	leaq	.LC67(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L821:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L809
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L809
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L806:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L811
	movq	7(%rax), %xmm0
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L852:
	movq	-1(%rdx), %rsi
	cmpw	$65, 11(%rsi)
	jne	.L811
	movq	%rdx, -1(%r9,%rdi)
	movq	%rax, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	(%r15), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%r9), %r10
	testb	$1, %dl
	je	.L809
	movq	%rdx, %rsi
	andq	$-262144, %rsi
	testb	$4, 10(%rsi)
	je	.L815
	movq	%r10, %rsi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rax
	movq	(%r15), %rdi
	movq	-72(%rbp), %r9
	movq	(%rax), %rdx
	leaq	-1(%rdi,%r9), %r10
	testb	$1, %dl
	je	.L809
.L815:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L809
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L809
	movq	%r10, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L850:
	leaq	.LC68(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L847:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	cmpb	$8, (%rbx)
	je	.L826
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L851:
	testb	$1, %dl
	je	.L793
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L793
	addq	$40, %rsp
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE26030:
	.size	_ZN2v88internal15TranslatedState20InitializeJSObjectAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE, .-_ZN2v88internal15TranslatedState20InitializeJSObjectAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	.section	.rodata._ZN2v88internal15TranslatedState34InitializeObjectWithTaggedFieldsAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE.str1.1,"aMS",@progbits,1
.LC69:
	.string	"2 == slot->GetChildrenCount()"
	.section	.rodata._ZN2v88internal15TranslatedState34InitializeObjectWithTaggedFieldsAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE.str1.8,"aMS",@progbits,1
	.align 8
.LC70:
	.string	"*length_value == Smi::FromInt(0)"
	.align 8
.LC71:
	.string	"marker == kStoreTagged || i == 1"
	.section	.text._ZN2v88internal15TranslatedState34InitializeObjectWithTaggedFieldsAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState34InitializeObjectWithTaggedFieldsAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	.type	_ZN2v88internal15TranslatedState34InitializeObjectWithTaggedFieldsAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE, @function
_ZN2v88internal15TranslatedState34InitializeObjectWithTaggedFieldsAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE:
.LFB26031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	16(%rcx), %r15
	movq	24(%rdi), %rdi
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movzbl	(%rcx), %eax
	movq	%r8, -80(%rbp)
	movq	(%r15), %rsi
	cmpq	%rsi, 288(%rdi)
	je	.L898
	addq	$37592, %rdi
	xorl	%edx, %edx
	cmpb	$8, %al
	jne	.L859
	movl	28(%rcx), %eax
	leal	0(,%rax,8), %edx
.L859:
	movq	%r9, %rcx
	call	_ZN2v88internal4Heap24NotifyObjectLayoutChangeENS0_10HeapObjectEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	cmpb	$8, (%rbx)
	jne	.L863
	movl	$8, %r14d
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L860:
	cmpl	28(%rbx), %r13d
	jge	.L863
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState18GetValueAndAdvanceEPNS0_15TranslatedFrameEPi
	movq	(%r15), %rdx
	leaq	-1(%r14,%rdx), %rdx
	movzbl	(%rdx), %esi
	cmpl	$1, %r13d
	jle	.L864
	cmpb	$2, %sil
	jne	.L864
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L899
.L865:
	leaq	.LC67(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L864:
	testb	%sil, %sil
	je	.L897
	cmpl	$1, %r13d
	jne	.L895
.L897:
	movq	(%rax), %rsi
.L867:
	movq	%rsi, (%rdx)
	movq	%rax, -72(%rbp)
	movq	(%r15), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%r14), %rsi
	testb	$1, %dl
	je	.L871
	movq	%rdx, %r9
	andq	$-262144, %r9
	testb	$4, 10(%r9)
	je	.L870
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rax
	movq	(%r15), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%r14), %rsi
	testb	$1, %dl
	jne	.L870
	.p2align 4,,10
	.p2align 3
.L871:
	addl	$1, %r13d
	addq	$8, %r14
	cmpb	$8, (%rbx)
	je	.L860
.L863:
	movq	-80(%rbp), %rax
	movq	(%r15), %rdi
	movq	(%rax), %rdx
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	jne	.L900
.L853:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L870:
	.cfi_restore_state
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L871
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L871
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L899:
	movq	-1(%rsi), %rdi
	cmpw	$65, 11(%rdi)
	jne	.L865
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L898:
	cmpb	$8, %al
	jne	.L855
	cmpl	$2, 28(%rcx)
	jne	.L855
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState18GetValueAndAdvanceEPNS0_15TranslatedFrameEPi
	cmpq	$0, (%rax)
	je	.L853
	leaq	.LC70(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L895:
	leaq	.LC71(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L900:
	testb	$1, %dl
	je	.L853
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L853
	addq	$40, %rsp
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L855:
	.cfi_restore_state
	leaq	.LC69(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE26031:
	.size	_ZN2v88internal15TranslatedState34InitializeObjectWithTaggedFieldsAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE, .-_ZN2v88internal15TranslatedState34InitializeObjectWithTaggedFieldsAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	.section	.text._ZN2v88internal15TranslatedState24GetFrameFromJSFrameIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState24GetFrameFromJSFrameIndexEi
	.type	_ZN2v88internal15TranslatedState24GetFrameFromJSFrameIndexEi, @function
_ZN2v88internal15TranslatedState24GetFrameFromJSFrameIndexEi:
.LFB26033:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rdi), %r8
	movabsq	$-1229782938247303441, %rdx
	subq	%rax, %r8
	sarq	$3, %r8
	imulq	%rdx, %r8
	testq	%r8, %r8
	je	.L906
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L905:
	movl	(%rax), %ecx
	movq	%rax, %r9
	leal	-4(%rcx), %edi
	cmpl	$1, %edi
	jbe	.L907
	testl	%ecx, %ecx
	jne	.L903
.L907:
	testl	%esi, %esi
	jle	.L901
	subl	$1, %esi
.L903:
	addq	$1, %rdx
	addq	$120, %rax
	cmpq	%r8, %rdx
	jne	.L905
.L906:
	xorl	%r9d, %r9d
.L901:
	movq	%r9, %rax
	ret
	.cfi_endproc
.LFE26033:
	.size	_ZN2v88internal15TranslatedState24GetFrameFromJSFrameIndexEi, .-_ZN2v88internal15TranslatedState24GetFrameFromJSFrameIndexEi
	.section	.text._ZN2v88internal15TranslatedState25VerifyMaterializedObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState25VerifyMaterializedObjectsEv
	.type	_ZN2v88internal15TranslatedState25VerifyMaterializedObjectsEv, @function
_ZN2v88internal15TranslatedState25VerifyMaterializedObjectsEv:
.LFB26037:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE26037:
	.size	_ZN2v88internal15TranslatedState25VerifyMaterializedObjectsEv, .-_ZN2v88internal15TranslatedState25VerifyMaterializedObjectsEv
	.section	.rodata._ZN2v88internal15TranslatedState16DoUpdateFeedbackEv.str1.1,"aMS",@progbits,1
.LC72:
	.string	"!feedback_slot_.IsInvalid()"
	.section	.text._ZN2v88internal15TranslatedState16DoUpdateFeedbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState16DoUpdateFeedbackEv
	.type	_ZN2v88internal15TranslatedState16DoUpdateFeedbackEv, @function
_ZN2v88internal15TranslatedState16DoUpdateFeedbackEv:
.LFB26038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 128(%rdi)
	je	.L918
	cmpl	$-1, 144(%rdi)
	movq	%rdi, %rbx
	je	.L922
	movq	24(%rdi), %rdi
	movl	$47, %esi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	128(%rbx), %rdx
	movl	144(%rbx), %esi
	xorl	%eax, %eax
	movq	$0, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movl	%esi, -32(%rbp)
	testq	%rdx, %rdx
	je	.L916
	movq	(%rdx), %rax
	leaq	-56(%rbp), %rdi
	movq	%rax, -56(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
.L916:
	leaq	-48(%rbp), %rdi
	movl	$1, %esi
	movl	%eax, -28(%rbp)
	call	_ZN2v88internal13FeedbackNexus18SetSpeculationModeENS0_15SpeculationModeE@PLT
	movl	$1, %eax
.L913:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L923
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L918:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L922:
	leaq	.LC72(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L923:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26038:
	.size	_ZN2v88internal15TranslatedState16DoUpdateFeedbackEv, .-_ZN2v88internal15TranslatedState16DoUpdateFeedbackEv
	.section	.rodata._ZN2v88internal15TranslatedState18ReadUpdateFeedbackEPNS0_19TranslationIteratorENS0_10FixedArrayEP8_IO_FILE.str1.8,"aMS",@progbits,1
	.align 8
.LC73:
	.string	"Translation::UPDATE_FEEDBACK == iterator->Next()"
	.align 8
.LC74:
	.string	"  reading FeedbackVector (slot %d)\n"
	.section	.text._ZN2v88internal15TranslatedState18ReadUpdateFeedbackEPNS0_19TranslationIteratorENS0_10FixedArrayEP8_IO_FILE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState18ReadUpdateFeedbackEPNS0_19TranslationIteratorENS0_10FixedArrayEP8_IO_FILE
	.type	_ZN2v88internal15TranslatedState18ReadUpdateFeedbackEPNS0_19TranslationIteratorENS0_10FixedArrayEP8_IO_FILE, @function
_ZN2v88internal15TranslatedState18ReadUpdateFeedbackEPNS0_19TranslationIteratorENS0_10FixedArrayEP8_IO_FILE:
.LFB26039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsi, %rdi
	movq	%rcx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal19TranslationIterator4NextEv
	cmpl	$26, %eax
	jne	.L928
	call	_ZN2v88internal19TranslationIterator4NextEv
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rbx,%rax), %rax
	movq	%rax, 136(%r10)
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, 144(%r10)
	movl	%eax, %edx
	testq	%r11, %r11
	je	.L924
	addq	$8, %rsp
	leaq	.LC74(%rip), %rsi
	movq	%r11, %rdi
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L924:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L928:
	.cfi_restore_state
	leaq	.LC73(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE26039:
	.size	_ZN2v88internal15TranslatedState18ReadUpdateFeedbackEPNS0_19TranslationIteratorENS0_10FixedArrayEP8_IO_FILE, .-_ZN2v88internal15TranslatedState18ReadUpdateFeedbackEPNS0_19TranslationIteratorENS0_10FixedArrayEP8_IO_FILE
	.section	.text._ZN2v88internal13ZoneChunkListIhE9push_backERKh,"axG",@progbits,_ZN2v88internal13ZoneChunkListIhE9push_backERKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	.type	_ZN2v88internal13ZoneChunkListIhE9push_backERKh, @function
_ZN2v88internal13ZoneChunkListIhE9push_backERKh:
.LFB28562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L938
.L930:
	movl	4(%rax), %edx
	cmpl	(%rax), %edx
	je	.L939
.L933:
	movzbl	0(%r13), %ecx
	movb	%cl, 24(%rax,%rdx)
	movq	24(%rbx), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L939:
	.cfi_restore_state
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L940
.L934:
	movq	%rax, 24(%rbx)
	movl	4(%rax), %edx
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L938:
	movq	(%rdi), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L941
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L932:
	pxor	%xmm0, %xmm0
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rax, 24(%rbx)
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L940:
	leal	(%rdx,%rdx), %r12d
	movl	$256, %eax
	movq	(%rbx), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L942
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L936:
	pxor	%xmm0, %xmm0
	movl	$0, 4(%rax)
	movups	%xmm0, 8(%rax)
	movl	%r12d, (%rax)
	movq	24(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rbx), %rax
	movq	8(%rax), %rax
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L941:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L942:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L936
	.cfi_endproc
.LFE28562:
	.size	_ZN2v88internal13ZoneChunkListIhE9push_backERKh, .-_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	.section	.text._ZN2v88internal17TranslationBuffer3AddEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17TranslationBuffer3AddEi
	.type	_ZN2v88internal17TranslationBuffer3AddEi, @function
_ZN2v88internal17TranslationBuffer3AddEi:
.LFB25904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-41(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	negl	%ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	leal	(%rbx,%rbx), %eax
	leal	(%rsi,%rsi), %ebx
	cmovs	%eax, %ebx
	shrl	$31, %esi
	orl	%esi, %ebx
	.p2align 4,,10
	.p2align 3
.L946:
	movl	%ebx, %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	addl	%eax, %eax
	shrl	$7, %ebx
	setne	%dl
	orl	%edx, %eax
	movb	%al, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	testl	%ebx, %ebx
	jne	.L946
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L950
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L950:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25904:
	.size	_ZN2v88internal17TranslationBuffer3AddEi, .-_ZN2v88internal17TranslationBuffer3AddEi
	.section	.text._ZN2v88internal11Translation29BeginBuiltinContinuationFrameENS0_9BailoutIdEij,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation29BeginBuiltinContinuationFrameENS0_9BailoutIdEij
	.type	_ZN2v88internal11Translation29BeginBuiltinContinuationFrameENS0_9BailoutIdEij, @function
_ZN2v88internal11Translation29BeginBuiltinContinuationFrameENS0_9BailoutIdEij:
.LFB25911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	leaq	-41(%rbp), %rsi
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$8, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movq	(%rbx), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L954
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L954:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25911:
	.size	_ZN2v88internal11Translation29BeginBuiltinContinuationFrameENS0_9BailoutIdEij, .-_ZN2v88internal11Translation29BeginBuiltinContinuationFrameENS0_9BailoutIdEij
	.section	.text._ZN2v88internal11Translation39BeginJavaScriptBuiltinContinuationFrameENS0_9BailoutIdEij,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation39BeginJavaScriptBuiltinContinuationFrameENS0_9BailoutIdEij
	.type	_ZN2v88internal11Translation39BeginJavaScriptBuiltinContinuationFrameENS0_9BailoutIdEij, @function
_ZN2v88internal11Translation39BeginJavaScriptBuiltinContinuationFrameENS0_9BailoutIdEij:
.LFB25912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	leaq	-41(%rbp), %rsi
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$12, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movq	(%rbx), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L958
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L958:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25912:
	.size	_ZN2v88internal11Translation39BeginJavaScriptBuiltinContinuationFrameENS0_9BailoutIdEij, .-_ZN2v88internal11Translation39BeginJavaScriptBuiltinContinuationFrameENS0_9BailoutIdEij
	.section	.text._ZN2v88internal11Translation48BeginJavaScriptBuiltinContinuationWithCatchFrameENS0_9BailoutIdEij,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation48BeginJavaScriptBuiltinContinuationWithCatchFrameENS0_9BailoutIdEij
	.type	_ZN2v88internal11Translation48BeginJavaScriptBuiltinContinuationWithCatchFrameENS0_9BailoutIdEij, @function
_ZN2v88internal11Translation48BeginJavaScriptBuiltinContinuationWithCatchFrameENS0_9BailoutIdEij:
.LFB25913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	leaq	-41(%rbp), %rsi
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$16, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movq	(%rbx), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L962
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L962:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25913:
	.size	_ZN2v88internal11Translation48BeginJavaScriptBuiltinContinuationWithCatchFrameENS0_9BailoutIdEij, .-_ZN2v88internal11Translation48BeginJavaScriptBuiltinContinuationWithCatchFrameENS0_9BailoutIdEij
	.section	.text._ZN2v88internal11Translation23BeginConstructStubFrameENS0_9BailoutIdEij,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation23BeginConstructStubFrameENS0_9BailoutIdEij
	.type	_ZN2v88internal11Translation23BeginConstructStubFrameENS0_9BailoutIdEij, @function
_ZN2v88internal11Translation23BeginConstructStubFrameENS0_9BailoutIdEij:
.LFB25914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	leaq	-41(%rbp), %rsi
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$20, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movq	(%rbx), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L966
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L966:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25914:
	.size	_ZN2v88internal11Translation23BeginConstructStubFrameENS0_9BailoutIdEij, .-_ZN2v88internal11Translation23BeginConstructStubFrameENS0_9BailoutIdEij
	.section	.text._ZN2v88internal11Translation21BeginInterpretedFrameENS0_9BailoutIdEijii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation21BeginInterpretedFrameENS0_9BailoutIdEijii
	.type	_ZN2v88internal11Translation21BeginInterpretedFrameENS0_9BailoutIdEijii, @function
_ZN2v88internal11Translation21BeginInterpretedFrameENS0_9BailoutIdEijii:
.LFB25916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	leaq	-57(%rbp), %rsi
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	%edx, -68(%rbp)
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$4, -57(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movq	(%rbx), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movl	-68(%rbp), %r10d
	movq	(%rbx), %rdi
	movl	%r10d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	(%rbx), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal17TranslationBuffer3AddEi
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L970
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L970:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25916:
	.size	_ZN2v88internal11Translation21BeginInterpretedFrameENS0_9BailoutIdEijii, .-_ZN2v88internal11Translation21BeginInterpretedFrameENS0_9BailoutIdEijii
	.section	.text._ZN2v88internal11Translation17ArgumentsElementsENS0_19CreateArgumentsTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation17ArgumentsElementsENS0_19CreateArgumentsTypeE
	.type	_ZN2v88internal11Translation17ArgumentsElementsENS0_19CreateArgumentsTypeE, @function
_ZN2v88internal11Translation17ArgumentsElementsENS0_19CreateArgumentsTypeE:
.LFB25917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	leaq	-41(%rbp), %rsi
	pushq	%r12
	movzbl	%r13b, %r13d
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addl	%r13d, %r13d
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$32, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movq	(%rbx), %rbx
	pxor	%xmm0, %xmm0
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L975:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%rbx), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%rbx)
	testl	%r13d, %r13d
	je	.L983
.L979:
	movl	%r13d, %r14d
	addl	%r14d, %r14d
	shrl	$7, %r13d
	setne	%al
	orl	%eax, %r14d
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L984
.L972:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L975
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L985
.L976:
	movq	%rax, 24(%rbx)
	movl	4(%rax), %r12d
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L984:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L986
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L974:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rax, 24(%rbx)
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L983:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L987
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L985:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	(%rbx), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L988
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L978:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rbx), %rax
	movq	8(%rax), %rax
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L986:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L988:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L978
.L987:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25917:
	.size	_ZN2v88internal11Translation17ArgumentsElementsENS0_19CreateArgumentsTypeE, .-_ZN2v88internal11Translation17ArgumentsElementsENS0_19CreateArgumentsTypeE
	.section	.text._ZN2v88internal11Translation15ArgumentsLengthENS0_19CreateArgumentsTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation15ArgumentsLengthENS0_19CreateArgumentsTypeE
	.type	_ZN2v88internal11Translation15ArgumentsLengthENS0_19CreateArgumentsTypeE, @function
_ZN2v88internal11Translation15ArgumentsLengthENS0_19CreateArgumentsTypeE:
.LFB25918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	leaq	-41(%rbp), %rsi
	pushq	%r12
	movzbl	%r13b, %r13d
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addl	%r13d, %r13d
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$36, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movq	(%rbx), %rbx
	pxor	%xmm0, %xmm0
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L993:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%rbx), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%rbx)
	testl	%r13d, %r13d
	je	.L1001
.L997:
	movl	%r13d, %r14d
	addl	%r14d, %r14d
	shrl	$7, %r13d
	setne	%al
	orl	%eax, %r14d
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L1002
.L990:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L993
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1003
.L994:
	movq	%rax, 24(%rbx)
	movl	4(%rax), %r12d
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1004
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L992:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rax, 24(%rbx)
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1005
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1003:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	(%rbx), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1006
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L996:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rbx), %rax
	movq	8(%rax), %rax
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1004:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1006:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L996
.L1005:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25918:
	.size	_ZN2v88internal11Translation15ArgumentsLengthENS0_19CreateArgumentsTypeE, .-_ZN2v88internal11Translation15ArgumentsLengthENS0_19CreateArgumentsTypeE
	.section	.text._ZN2v88internal11Translation19BeginCapturedObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation19BeginCapturedObjectEi
	.type	_ZN2v88internal11Translation19BeginCapturedObjectEi, @function
_ZN2v88internal11Translation19BeginCapturedObjectEi:
.LFB25919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$40, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1013:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1021
.L1017:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1022
.L1010:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1013
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1023
.L1014:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1022:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1024
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1012:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1025
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1023:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1026
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1016:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1024:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1026:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1016
.L1025:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25919:
	.size	_ZN2v88internal11Translation19BeginCapturedObjectEi, .-_ZN2v88internal11Translation19BeginCapturedObjectEi
	.section	.text._ZN2v88internal11Translation15DuplicateObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation15DuplicateObjectEi
	.type	_ZN2v88internal11Translation15DuplicateObjectEi, @function
_ZN2v88internal11Translation15DuplicateObjectEi:
.LFB25920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$28, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1033:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1041
.L1037:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1042
.L1030:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1033
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1043
.L1034:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1044
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1032:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1041:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1045
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1043:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1046
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1036:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1044:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1046:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1036
.L1045:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25920:
	.size	_ZN2v88internal11Translation15DuplicateObjectEi, .-_ZN2v88internal11Translation15DuplicateObjectEi
	.section	.text._ZN2v88internal11Translation13StoreRegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation13StoreRegisterENS0_8RegisterE
	.type	_ZN2v88internal11Translation13StoreRegisterENS0_8RegisterE, @function
_ZN2v88internal11Translation13StoreRegisterENS0_8RegisterE:
.LFB25921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$44, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1053:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1061
.L1057:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1062
.L1050:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1053
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1063
.L1054:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1064
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1052:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1065
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1063:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1066
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1056:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1064:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1066:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1056
.L1065:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25921:
	.size	_ZN2v88internal11Translation13StoreRegisterENS0_8RegisterE, .-_ZN2v88internal11Translation13StoreRegisterENS0_8RegisterE
	.section	.text._ZN2v88internal11Translation18StoreInt32RegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation18StoreInt32RegisterENS0_8RegisterE
	.type	_ZN2v88internal11Translation18StoreInt32RegisterENS0_8RegisterE, @function
_ZN2v88internal11Translation18StoreInt32RegisterENS0_8RegisterE:
.LFB25922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$48, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1073:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1081
.L1077:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1082
.L1070:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1073
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1083
.L1074:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1084
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1072:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1070
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1085
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1083:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1086
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1076:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1084:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1086:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1076
.L1085:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25922:
	.size	_ZN2v88internal11Translation18StoreInt32RegisterENS0_8RegisterE, .-_ZN2v88internal11Translation18StoreInt32RegisterENS0_8RegisterE
	.section	.text._ZN2v88internal11Translation18StoreInt64RegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation18StoreInt64RegisterENS0_8RegisterE
	.type	_ZN2v88internal11Translation18StoreInt64RegisterENS0_8RegisterE, @function
_ZN2v88internal11Translation18StoreInt64RegisterENS0_8RegisterE:
.LFB25923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$52, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1093:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1101
.L1097:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1102
.L1090:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1093
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1103
.L1094:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1102:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1104
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1092:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1101:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1105
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1103:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1106
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1096:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1104:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L1106:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1096
.L1105:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25923:
	.size	_ZN2v88internal11Translation18StoreInt64RegisterENS0_8RegisterE, .-_ZN2v88internal11Translation18StoreInt64RegisterENS0_8RegisterE
	.section	.text._ZN2v88internal11Translation19StoreUint32RegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation19StoreUint32RegisterENS0_8RegisterE
	.type	_ZN2v88internal11Translation19StoreUint32RegisterENS0_8RegisterE, @function
_ZN2v88internal11Translation19StoreUint32RegisterENS0_8RegisterE:
.LFB25924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$56, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1113:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1121
.L1117:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1122
.L1110:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1113
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1123
.L1114:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1124
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1112:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1121:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1125
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1123:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1126
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1116:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1124:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1126:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1116
.L1125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25924:
	.size	_ZN2v88internal11Translation19StoreUint32RegisterENS0_8RegisterE, .-_ZN2v88internal11Translation19StoreUint32RegisterENS0_8RegisterE
	.section	.text._ZN2v88internal11Translation17StoreBoolRegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation17StoreBoolRegisterENS0_8RegisterE
	.type	_ZN2v88internal11Translation17StoreBoolRegisterENS0_8RegisterE, @function
_ZN2v88internal11Translation17StoreBoolRegisterENS0_8RegisterE:
.LFB25925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$60, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1133:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1141
.L1137:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1142
.L1130:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1133
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1143
.L1134:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1133
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1144
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1132:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1141:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1145
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1143:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1146
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1136:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1144:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1132
	.p2align 4,,10
	.p2align 3
.L1146:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1136
.L1145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25925:
	.size	_ZN2v88internal11Translation17StoreBoolRegisterENS0_8RegisterE, .-_ZN2v88internal11Translation17StoreBoolRegisterENS0_8RegisterE
	.section	.text._ZN2v88internal11Translation18StoreFloatRegisterENS0_11XMMRegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation18StoreFloatRegisterENS0_11XMMRegisterE
	.type	_ZN2v88internal11Translation18StoreFloatRegisterENS0_11XMMRegisterE, @function
_ZN2v88internal11Translation18StoreFloatRegisterENS0_11XMMRegisterE:
.LFB25926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$64, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1153:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1161
.L1157:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1162
.L1150:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1153
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1163
.L1154:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1164
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1152:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1165
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1163:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1166
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1156:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1164:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1166:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1156
.L1165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25926:
	.size	_ZN2v88internal11Translation18StoreFloatRegisterENS0_11XMMRegisterE, .-_ZN2v88internal11Translation18StoreFloatRegisterENS0_11XMMRegisterE
	.section	.text._ZN2v88internal11Translation19StoreDoubleRegisterENS0_11XMMRegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation19StoreDoubleRegisterENS0_11XMMRegisterE
	.type	_ZN2v88internal11Translation19StoreDoubleRegisterENS0_11XMMRegisterE, @function
_ZN2v88internal11Translation19StoreDoubleRegisterENS0_11XMMRegisterE:
.LFB25927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$68, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1173:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1181
.L1177:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1182
.L1170:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1173
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1183
.L1174:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1184
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1172:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1181:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1185
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1183:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1186
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1176:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1184:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1186:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1176
.L1185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25927:
	.size	_ZN2v88internal11Translation19StoreDoubleRegisterENS0_11XMMRegisterE, .-_ZN2v88internal11Translation19StoreDoubleRegisterENS0_11XMMRegisterE
	.section	.text._ZN2v88internal11Translation14StoreStackSlotEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation14StoreStackSlotEi
	.type	_ZN2v88internal11Translation14StoreStackSlotEi, @function
_ZN2v88internal11Translation14StoreStackSlotEi:
.LFB25928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$72, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1193:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1201
.L1197:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1202
.L1190:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1193
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1203
.L1194:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1202:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1204
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1192:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1201:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1205
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1203:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1206
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1196:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1204:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1206:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1196
.L1205:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25928:
	.size	_ZN2v88internal11Translation14StoreStackSlotEi, .-_ZN2v88internal11Translation14StoreStackSlotEi
	.section	.text._ZN2v88internal11Translation19StoreInt32StackSlotEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation19StoreInt32StackSlotEi
	.type	_ZN2v88internal11Translation19StoreInt32StackSlotEi, @function
_ZN2v88internal11Translation19StoreInt32StackSlotEi:
.LFB25929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$76, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1213:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1221
.L1217:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1222
.L1210:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1213
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1223
.L1214:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1222:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1224
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1212:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1225
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1223:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1226
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1216:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1214
	.p2align 4,,10
	.p2align 3
.L1224:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1226:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1216
.L1225:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25929:
	.size	_ZN2v88internal11Translation19StoreInt32StackSlotEi, .-_ZN2v88internal11Translation19StoreInt32StackSlotEi
	.section	.text._ZN2v88internal11Translation19StoreInt64StackSlotEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation19StoreInt64StackSlotEi
	.type	_ZN2v88internal11Translation19StoreInt64StackSlotEi, @function
_ZN2v88internal11Translation19StoreInt64StackSlotEi:
.LFB25930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$80, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1233:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1241
.L1237:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1242
.L1230:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1233
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1243
.L1234:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1242:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1244
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1232:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1241:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1245
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1243:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1246
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1236:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1244:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1246:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1236
.L1245:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25930:
	.size	_ZN2v88internal11Translation19StoreInt64StackSlotEi, .-_ZN2v88internal11Translation19StoreInt64StackSlotEi
	.section	.text._ZN2v88internal11Translation20StoreUint32StackSlotEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation20StoreUint32StackSlotEi
	.type	_ZN2v88internal11Translation20StoreUint32StackSlotEi, @function
_ZN2v88internal11Translation20StoreUint32StackSlotEi:
.LFB25931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$84, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1253:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1261
.L1257:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1262
.L1250:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1253
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1263
.L1254:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1262:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1264
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1252:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1265
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1263:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1266
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1256:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1264:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1266:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1256
.L1265:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25931:
	.size	_ZN2v88internal11Translation20StoreUint32StackSlotEi, .-_ZN2v88internal11Translation20StoreUint32StackSlotEi
	.section	.text._ZN2v88internal11Translation18StoreBoolStackSlotEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation18StoreBoolStackSlotEi
	.type	_ZN2v88internal11Translation18StoreBoolStackSlotEi, @function
_ZN2v88internal11Translation18StoreBoolStackSlotEi:
.LFB25932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$88, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1273:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1281
.L1277:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1282
.L1270:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1273
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1283
.L1274:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1282:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1284
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1272:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1285
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1283:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1286
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1276:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1284:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1286:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1276
.L1285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25932:
	.size	_ZN2v88internal11Translation18StoreBoolStackSlotEi, .-_ZN2v88internal11Translation18StoreBoolStackSlotEi
	.section	.text._ZN2v88internal11Translation19StoreFloatStackSlotEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation19StoreFloatStackSlotEi
	.type	_ZN2v88internal11Translation19StoreFloatStackSlotEi, @function
_ZN2v88internal11Translation19StoreFloatStackSlotEi:
.LFB25933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$92, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1297
	.p2align 4,,10
	.p2align 3
.L1293:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1301
.L1297:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1302
.L1290:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1293
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1303
.L1294:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1302:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1304
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1292:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1290
	.p2align 4,,10
	.p2align 3
.L1301:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1305
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1303:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1306
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1296:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1304:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1306:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1296
.L1305:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25933:
	.size	_ZN2v88internal11Translation19StoreFloatStackSlotEi, .-_ZN2v88internal11Translation19StoreFloatStackSlotEi
	.section	.text._ZN2v88internal11Translation20StoreDoubleStackSlotEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation20StoreDoubleStackSlotEi
	.type	_ZN2v88internal11Translation20StoreDoubleStackSlotEi, @function
_ZN2v88internal11Translation20StoreDoubleStackSlotEi:
.LFB25934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$96, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1313:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1321
.L1317:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1322
.L1310:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1313
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1323
.L1314:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1324
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1312:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1321:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1325
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1323:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1326
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1316:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1324:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1326:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1316
.L1325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25934:
	.size	_ZN2v88internal11Translation20StoreDoubleStackSlotEi, .-_ZN2v88internal11Translation20StoreDoubleStackSlotEi
	.section	.text._ZN2v88internal11Translation12StoreLiteralEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation12StoreLiteralEi
	.type	_ZN2v88internal11Translation12StoreLiteralEi, @function
_ZN2v88internal11Translation12StoreLiteralEi:
.LFB25935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	-41(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$100, -41(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	(%r12), %r13
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1333:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1341
.L1337:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1342
.L1330:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1333
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1343
.L1334:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1342:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1344
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1332:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1341:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1345
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1343:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1346
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1336:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1344:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1346:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1336
.L1345:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25935:
	.size	_ZN2v88internal11Translation12StoreLiteralEi, .-_ZN2v88internal11Translation12StoreLiteralEi
	.section	.text._ZN2v88internal11Translation17AddUpdateFeedbackEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation17AddUpdateFeedbackEii
	.type	_ZN2v88internal11Translation17AddUpdateFeedbackEii, @function
_ZN2v88internal11Translation17AddUpdateFeedbackEii:
.LFB25936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	leaq	-57(%rbp), %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$104, -57(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%r12d, %esi
	movq	(%r14), %r15
	pxor	%xmm0, %xmm0
	negl	%esi
	testl	%r12d, %r12d
	leal	(%rsi,%rsi), %eax
	leal	(%r12,%r12), %esi
	cmovs	%eax, %esi
	shrl	$31, %r12d
	orl	%esi, %r12d
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1353:
	movb	%dl, 24(%rax,%r13)
	movq	24(%r15), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r15)
	testl	%r12d, %r12d
	je	.L1372
.L1357:
	movl	%r12d, %edx
	addl	%edx, %edx
	shrl	$7, %r12d
	setne	%al
	orl	%eax, %edx
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L1373
.L1350:
	movl	4(%rax), %r13d
	cmpl	(%rax), %r13d
	jne	.L1353
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1374
.L1354:
	movq	%rax, 24(%r15)
	movl	4(%rax), %r13d
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1373:
	movq	(%r15), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$31, %rcx
	jbe	.L1375
	leaq	32(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L1352:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r15)
	movq	%rax, 24(%r15)
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1372:
	movl	%ebx, %edx
	movq	(%r14), %r13
	pxor	%xmm0, %xmm0
	negl	%edx
	testl	%ebx, %ebx
	leal	(%rdx,%rdx), %eax
	leal	(%rbx,%rbx), %edx
	cmovs	%eax, %edx
	shrl	$31, %ebx
	orl	%edx, %ebx
	jmp	.L1367
	.p2align 4,,10
	.p2align 3
.L1363:
	movb	%r14b, 24(%rax,%r12)
	movq	24(%r13), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%r13)
	testl	%ebx, %ebx
	je	.L1376
.L1367:
	movl	%ebx, %r14d
	addl	%r14d, %r14d
	shrl	$7, %ebx
	setne	%al
	orl	%eax, %r14d
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1377
.L1360:
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1363
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1378
.L1364:
	movq	%rax, 24(%r13)
	movl	4(%rax), %r12d
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1377:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1379
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1362:
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1376:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1380
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1378:
	.cfi_restore_state
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	0(%r13), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1381
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1366:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r12d, (%rax)
	movq	24(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r13), %rax
	movq	8(%rax), %rax
	jmp	.L1364
	.p2align 4,,10
	.p2align 3
.L1374:
	addl	%r13d, %r13d
	movl	$256, %eax
	movq	(%r15), %rdi
	cmpl	$256, %r13d
	cmova	%eax, %r13d
	movq	24(%rdi), %rcx
	movq	16(%rdi), %rax
	movl	%r13d, %esi
	subq	%rax, %rcx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rcx, %rsi
	ja	.L1382
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1356:
	movups	%xmm0, 8(%rax)
	movl	$0, 4(%rax)
	movl	%r13d, (%rax)
	movq	24(%r15), %rcx
	movq	%rax, 8(%rcx)
	movq	24(%r15), %rcx
	movq	%rcx, 16(%rax)
	movq	24(%r15), %rax
	movq	8(%rax), %rax
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1375:
	movl	$32, %esi
	movb	%dl, -65(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-65(%rbp), %edx
	pxor	%xmm0, %xmm0
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1379:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1382:
	movb	%dl, -65(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-65(%rbp), %edx
	pxor	%xmm0, %xmm0
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L1381:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	pxor	%xmm0, %xmm0
	jmp	.L1366
.L1380:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25936:
	.size	_ZN2v88internal11Translation17AddUpdateFeedbackEii, .-_ZN2v88internal11Translation17AddUpdateFeedbackEii
	.section	.text._ZN2v88internal11Translation20StoreJSFrameFunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation20StoreJSFrameFunctionEv
	.type	_ZN2v88internal11Translation20StoreJSFrameFunctionEv, @function
_ZN2v88internal11Translation20StoreJSFrameFunctionEv:
.LFB25937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-25(%rbp), %rsi
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$72, -25(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movq	(%rbx), %rbx
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L1393
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	je	.L1394
.L1387:
	movb	$12, 24(%rax,%r12)
	movq	24(%rbx), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1395
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1393:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1396
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1386:
	pxor	%xmm0, %xmm0
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rax, 24(%rbx)
	movl	4(%rax), %r12d
	cmpl	(%rax), %r12d
	jne	.L1387
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1397
.L1388:
	movq	%rax, 24(%rbx)
	movl	4(%rax), %r12d
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1397:
	addl	%r12d, %r12d
	movl	$256, %eax
	movq	(%rbx), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	%r12d, %esi
	subq	%rax, %rdx
	addq	$31, %rsi
	andq	$-8, %rsi
	cmpq	%rdx, %rsi
	ja	.L1398
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1390:
	pxor	%xmm0, %xmm0
	movl	$0, 4(%rax)
	movups	%xmm0, 8(%rax)
	movl	%r12d, (%rax)
	movq	24(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rbx), %rax
	movq	8(%rax), %rax
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1396:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1398:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1390
.L1395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25937:
	.size	_ZN2v88internal11Translation20StoreJSFrameFunctionEv, .-_ZN2v88internal11Translation20StoreJSFrameFunctionEv
	.section	.text._ZN2v88internal11Translation26BeginArgumentsAdaptorFrameEij,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Translation26BeginArgumentsAdaptorFrameEij
	.type	_ZN2v88internal11Translation26BeginArgumentsAdaptorFrameEij, @function
_ZN2v88internal11Translation26BeginArgumentsAdaptorFrameEij:
.LFB25915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-57(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movq	%r14, %rsi
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$24, -57(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	movl	%ebx, %esi
	movq	0(%r13), %r15
	negl	%esi
	testl	%ebx, %ebx
	leal	(%rsi,%rsi), %eax
	leal	(%rbx,%rbx), %esi
	cmovs	%eax, %esi
	shrl	$31, %ebx
	orl	%esi, %ebx
	.p2align 4,,10
	.p2align 3
.L1402:
	movl	%ebx, %eax
	movq	%r14, %rsi
	movq	%r15, %rdi
	addl	%eax, %eax
	shrl	$7, %ebx
	setne	%dl
	orl	%edx, %eax
	movb	%al, -57(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	testl	%ebx, %ebx
	jne	.L1402
	movl	%r12d, %edx
	leal	(%r12,%r12), %ebx
	movq	0(%r13), %r13
	negl	%edx
	addl	%edx, %edx
	testl	%r12d, %r12d
	cmovns	%ebx, %edx
	movl	%r12d, %ebx
	shrl	$31, %ebx
	orl	%edx, %ebx
	.p2align 4,,10
	.p2align 3
.L1405:
	movl	%ebx, %eax
	movq	%r14, %rsi
	movq	%r13, %rdi
	addl	%eax, %eax
	shrl	$7, %ebx
	setne	%dl
	orl	%edx, %eax
	movb	%al, -57(%rbp)
	call	_ZN2v88internal13ZoneChunkListIhE9push_backERKh
	testl	%ebx, %ebx
	jne	.L1405
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1410
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1410:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25915:
	.size	_ZN2v88internal11Translation26BeginArgumentsAdaptorFrameEij, .-_ZN2v88internal11Translation26BeginArgumentsAdaptorFrameEij
	.section	.rodata._ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC75:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_:
.LFB28951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$5, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$4, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	sarq	$5, %rax
	addq	%rdx, %rax
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	je	.L1420
	movq	(%rbx), %rcx
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L1421
.L1413:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movdqu	(%r12), %xmm1
	movq	48(%rbx), %rax
	movups	%xmm1, (%rax)
	movdqu	16(%r12), %xmm2
	movups	%xmm2, 16(%rax)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1421:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L1422
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r14
	ja	.L1423
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1418
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L1418:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L1416:
	movq	%r15, 40(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r13), %r14
	movq	(%r15), %xmm0
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1422:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L1415
	cmpq	%r14, %rsi
	je	.L1416
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1415:
	cmpq	%r14, %rsi
	je	.L1416
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1416
.L1420:
	leaq	.LC75(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1423:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE28951:
	.size	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.section	.text._ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE9push_backERKS2_,"axG",@progbits,_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE9push_backERKS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE9push_backERKS2_
	.type	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE9push_backERKS2_, @function
_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE9push_backERKS2_:
.LFB26753:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	movq	48(%rdi), %rdx
	subq	$32, %rax
	cmpq	%rax, %rdx
	je	.L1425
	movdqu	(%rsi), %xmm0
	movups	%xmm0, (%rdx)
	movdqu	16(%rsi), %xmm1
	movups	%xmm1, 16(%rdx)
	addq	$32, 48(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1425:
	jmp	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.cfi_endproc
.LFE26753:
	.size	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE9push_backERKS2_, .-_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE9push_backERKS2_
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB29108:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1436
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1430:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1430
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1436:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE29108:
	.size	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.rodata._ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC76:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.type	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, @function
_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_:
.LFB29113:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1453
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1449
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1454
.L1441:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1448:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1455
	testq	%r13, %r13
	jg	.L1444
	testq	%r9, %r9
	jne	.L1447
.L1445:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1455:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1444
.L1447:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1454:
	testq	%rsi, %rsi
	jne	.L1442
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1445
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1449:
	movl	$8, %r14d
	jmp	.L1441
.L1453:
	leaq	.LC76(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1442:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1441
	.cfi_endproc
.LFE29113:
	.size	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, .-_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB30096:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1464
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1458:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1458
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1464:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE30096:
	.size	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE5eraseERKS2_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE5eraseERKS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE5eraseERKS2_
	.type	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE5eraseERKS2_, @function
_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE5eraseERKS2_:
.LFB30082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	testq	%r15, %r15
	je	.L1468
	movq	(%rsi), %rcx
	movq	%r13, %r14
	movq	%r15, %rbx
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1512:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L1511
.L1489:
	movq	%rax, %rbx
.L1469:
	cmpq	%rcx, 32(%rbx)
	jb	.L1512
	movq	16(%rbx), %rax
	jbe	.L1513
	movq	%rbx, %r14
	testq	%rax, %rax
	jne	.L1489
.L1511:
	movq	40(%r12), %r8
	cmpq	%r14, 24(%r12)
	jne	.L1472
	cmpq	%r14, %r13
	je	.L1486
.L1472:
	xorl	%r8d, %r8d
.L1467:
	addq	$24, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1513:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L1476
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1514:
	movq	%rdx, %r14
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L1482
.L1476:
	cmpq	32(%rdx), %rcx
	jb	.L1514
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1476
	.p2align 4,,10
	.p2align 3
.L1482:
	testq	%rax, %rax
	je	.L1477
.L1515:
	cmpq	32(%rax), %rcx
	ja	.L1481
	movq	%rax, %rbx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1515
.L1477:
	movq	40(%r12), %r8
	cmpq	%rbx, 24(%r12)
	jne	.L1484
	cmpq	%r14, %r13
	je	.L1486
.L1484:
	cmpq	%rbx, %r14
	je	.L1472
	.p2align 4,,10
	.p2align 3
.L1488:
	movq	%rbx, %rdi
	movq	%rbx, %r15
	movq	%r8, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	movq	40(%r12), %rax
	movq	-56(%rbp), %r8
	subq	$1, %rax
	cmpq	%r14, %rbx
	movq	%rax, 40(%r12)
	jne	.L1488
	subq	%rax, %r8
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1481:
	movq	24(%rax), %rax
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	24(%r15), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r15, %rdi
	movq	16(%r15), %r15
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
	testq	%r15, %r15
	jne	.L1486
.L1485:
	movq	$0, 16(%r12)
	movq	%r13, 24(%r12)
	movq	%r13, 32(%r12)
	movq	$0, 40(%r12)
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1468:
	movq	40(%rdi), %r8
	cmpq	%r13, 24(%rdi)
	jne	.L1472
	jmp	.L1485
	.cfi_endproc
.LFE30082:
	.size	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE5eraseERKS2_, .-_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE5eraseERKS2_
	.section	.text._ZN2v88internal12_GLOBAL__N_117ActivationsFinder11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117ActivationsFinder11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE, @function
_ZN2v88internal12_GLOBAL__N_117ActivationsFinder11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE:
.LFB25782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-1488(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$1512, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1516
	leaq	-1528(%rbp), %r13
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	%rbx, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1516
.L1522:
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$4, %eax
	jne	.L1518
	movq	-72(%rbp), %rdi
	call	_ZNK2v88internal10StackFrame10LookupCodeEv@PLT
	movq	%rax, -1528(%rbp)
	testb	$62, 43(%rax)
	jne	.L1518
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L1518
	movq	8(%r12), %rdi
	movq	%r13, %rsi
	call	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE5eraseERKS2_
	movq	-72(%rbp), %rax
	leaq	-1520(%rbp), %rdi
	movq	%r13, %rsi
	movq	40(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal4Code17GetSafepointEntryEm@PLT
	movq	-72(%rbp), %rax
	movq	-1528(%rbp), %rcx
	movslq	-1504(%rbp), %rdx
	movq	40(%rax), %rax
	leaq	63(%rcx,%rdx), %rdx
	movq	%rdx, (%rax)
	jmp	.L1518
	.p2align 4,,10
	.p2align 3
.L1516:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1532
	addq	$1512, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1532:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25782:
	.size	_ZN2v88internal12_GLOBAL__N_117ActivationsFinder11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE, .-_ZN2v88internal12_GLOBAL__N_117ActivationsFinder11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE
	.section	.text._ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_
	.type	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_, @function
_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_:
.LFB30098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L1534
	movq	(%rsi), %r13
	jmp	.L1535
	.p2align 4,,10
	.p2align 3
.L1551:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L1536
.L1552:
	movq	%rax, %r12
.L1535:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r13
	jb	.L1551
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L1552
.L1536:
	testb	%dl, %dl
	jne	.L1553
	cmpq	%rcx, %r13
	jbe	.L1544
.L1543:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L1554
.L1541:
	movl	$40, %edi
	movl	%r8d, -52(%rbp)
	call	_Znwm@PLT
	movl	-52(%rbp), %r8d
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rax, %r13
	movq	(%r14), %rax
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1553:
	.cfi_restore_state
	cmpq	24(%rbx), %r12
	je	.L1543
.L1545:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	32(%rax), %r13
	ja	.L1543
	movq	%rax, %r12
.L1544:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1554:
	.cfi_restore_state
	xorl	%r8d, %r8d
	cmpq	32(%r12), %r13
	setb	%r8b
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1534:
	movq	%r15, %r12
	cmpq	24(%rdi), %r15
	je	.L1547
	movq	(%rsi), %r13
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1547:
	movl	$1, %r8d
	jmp	.L1541
	.cfi_endproc
.LFE30098:
	.size	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_, .-_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_
	.section	.text._ZN2v88internal11Deoptimizer30DeoptimizeMarkedCodeForContextENS0_13NativeContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer30DeoptimizeMarkedCodeForContextENS0_13NativeContextE
	.type	_ZN2v88internal11Deoptimizer30DeoptimizeMarkedCodeForContextENS0_13NativeContextE, @function
_ZN2v88internal11Deoptimizer30DeoptimizeMarkedCodeForContextENS0_13NativeContextE:
.LFB25783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	andq	$-262144, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-136(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -136(%rbp)
	movq	%r13, %rdi
	movq	24(%rax), %r15
	leaq	-104(%rbp), %rax
	leaq	-37592(%r15), %rbx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movl	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, -144(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal13NativeContext21OptimizedCodeListHeadEv@PLT
	movq	%rax, %r14
	leaq	-128(%rbp), %rax
	movq	%rax, -152(%rbp)
	cmpq	88(%rbx), %r14
	jne	.L1556
	jmp	.L1570
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	-128(%rbp), %r12
	cmpq	%r14, 88(%rbx)
	je	.L1570
.L1556:
	movq	%r14, -128(%rbp)
	testb	$62, 43(%r14)
	jne	.L1601
	movq	31(%r14), %rax
	movq	7(%rax), %r14
	movq	-128(%rbp), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L1560
	movq	-152(%rbp), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_
	testq	%r12, %r12
	je	.L1561
	movq	31(%r12), %rdi
	movq	%r14, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %r14b
	je	.L1565
	movq	%r14, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -160(%rbp)
	testl	$262144, %eax
	jne	.L1602
	testb	$24, %al
	je	.L1565
.L1606:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1603
	.p2align 4,,10
	.p2align 3
.L1565:
	movq	%r13, %rdi
	call	_ZN2v88internal13NativeContext23DeoptimizedCodeListHeadEv@PLT
	movq	%rax, %rdx
	movq	-128(%rbp), %rax
	movq	31(%rax), %rdi
	movq	%rdx, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %dl
	je	.L1573
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -160(%rbp)
	testl	$262144, %eax
	je	.L1567
	movq	%rdx, -184(%rbp)
	movq	%rsi, -176(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-160(%rbp), %r8
	movq	-184(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	-168(%rbp), %rdi
	movq	8(%r8), %rax
.L1567:
	testb	$24, %al
	je	.L1573
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1604
.L1573:
	movq	-128(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13NativeContext26SetDeoptimizedCodeListHeadENS0_6ObjectE@PLT
.L1607:
	cmpq	%r14, 88(%rbx)
	jne	.L1556
.L1570:
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_117ActivationsFinderE(%rip), %rax
	leaq	-112(%rbp), %r13
	movq	%rbx, %rsi
	movq	%rax, %xmm0
	movq	%r13, %xmm1
	leaq	-128(%rbp), %r12
	punpcklqdq	%xmm1, %xmm0
	movq	%r12, %rdi
	leaq	12448(%rbx), %rdx
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_117ActivationsFinder11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE
	movq	41168(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal13ThreadManager22IterateArchivedThreadsEPNS0_13ThreadVisitorE@PLT
	movq	-88(%rbp), %r12
	cmpq	-144(%rbp), %r12
	je	.L1558
	.p2align 4,,10
	.p2align 3
.L1557:
	movq	32(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4Heap32InvalidateCodeDeoptimizationDataENS0_4CodeE@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	-144(%rbp), %rax
	jne	.L1557
.L1558:
	movq	-96(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1555
.L1572:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal4CodeES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1572
.L1555:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1605
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1561:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13NativeContext24SetOptimizedCodeListHeadENS0_6ObjectE@PLT
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1601:
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1602:
	movq	%r14, %rdx
	movq	%rsi, -176(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-160(%rbp), %r8
	movq	-176(%rbp), %rsi
	movq	-168(%rbp), %rdi
	movq	8(%r8), %rax
	testb	$24, %al
	jne	.L1606
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1604:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13NativeContext26SetDeoptimizedCodeListHeadENS0_6ObjectE@PLT
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1603:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1565
.L1605:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25783:
	.size	_ZN2v88internal11Deoptimizer30DeoptimizeMarkedCodeForContextENS0_13NativeContextE, .-_ZN2v88internal11Deoptimizer30DeoptimizeMarkedCodeForContextENS0_13NativeContextE
	.section	.rodata._ZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC77:
	.string	"v8"
.LC78:
	.string	"V8.DeoptimizeCode"
.LC79:
	.string	"ab"
	.section	.rodata._ZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC80:
	.string	"[deoptimize all code in all contexts]\n"
	.section	.text._ZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateE
	.type	_ZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateE, @function
_ZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateE:
.LFB25796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1646
.L1609:
	leaq	-152(%rbp), %r13
	xorl	%esi, %esi
	movq	%r12, -152(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	_ZZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateEE28trace_event_unique_atomic366(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1647
.L1611:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1648
.L1613:
	cmpb	$0, _ZN2v88internal16FLAG_trace_deoptE(%rip)
	jne	.L1649
.L1617:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27AbortConcurrentOptimizationENS0_16BlockingBehaviorE@PLT
	movq	39120(%r12), %rbx
	cmpq	88(%r12), %rbx
	je	.L1627
	.p2align 4,,10
	.p2align 3
.L1624:
	movq	%rbx, %rdi
	call	_ZN2v88internal11Deoptimizer21MarkAllCodeForContextENS0_13NativeContextE
	movq	%rbx, %rdi
	call	_ZN2v88internal11Deoptimizer30DeoptimizeMarkedCodeForContextENS0_13NativeContextE
	movq	1959(%rbx), %rbx
	cmpq	%rbx, 88(%r12)
	jne	.L1624
.L1627:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1650
.L1608:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1651
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1647:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1652
.L1612:
	movq	%rbx, _ZZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateEE28trace_event_unique_atomic366(%rip)
	jmp	.L1611
	.p2align 4,,10
	.p2align 3
.L1649:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rax), %rdi
	movq	%rax, %rbx
	je	.L1619
	testq	%rdi, %rdi
	je	.L1653
.L1620:
	addl	$1, 152(%rbx)
.L1619:
	xorl	%eax, %eax
	leaq	.LC80(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L1617
	subl	$1, 152(%rbx)
	jne	.L1617
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1648:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1654
.L1614:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1615
	movq	(%rdi), %rax
	call	*8(%rax)
.L1615:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1616
	movq	(%rdi), %rax
	call	*8(%rax)
.L1616:
	leaq	.LC78(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1650:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1646:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$140, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1609
.L1654:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC78(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1614
.L1652:
	leaq	.LC77(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1612
.L1653:
	movq	(%rax), %rdi
	leaq	.LC79(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rdi
	jmp	.L1620
.L1651:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25796:
	.size	_ZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateE, .-_ZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC81:
	.string	"[deoptimize marked code in all contexts]\n"
	.section	.text._ZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateE
	.type	_ZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateE, @function
_ZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateE:
.LFB25797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1693
.L1656:
	leaq	-152(%rbp), %r13
	xorl	%esi, %esi
	movq	%r12, -152(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	_ZZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateEE28trace_event_unique_atomic387(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1694
.L1658:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1695
.L1660:
	cmpb	$0, _ZN2v88internal16FLAG_trace_deoptE(%rip)
	jne	.L1696
.L1664:
	movq	39120(%r12), %rbx
	cmpq	88(%r12), %rbx
	je	.L1674
	.p2align 4,,10
	.p2align 3
.L1671:
	movq	%rbx, %rdi
	call	_ZN2v88internal11Deoptimizer30DeoptimizeMarkedCodeForContextENS0_13NativeContextE
	movq	1959(%rbx), %rbx
	cmpq	%rbx, 88(%r12)
	jne	.L1671
.L1674:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1697
.L1655:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1698
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1694:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1699
.L1659:
	movq	%rbx, _ZZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateEE28trace_event_unique_atomic387(%rip)
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rax), %rdi
	movq	%rax, %rbx
	je	.L1666
	testq	%rdi, %rdi
	je	.L1700
.L1667:
	addl	$1, 152(%rbx)
.L1666:
	xorl	%eax, %eax
	leaq	.LC81(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L1664
	subl	$1, 152(%rbx)
	jne	.L1664
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1695:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1701
.L1661:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1662
	movq	(%rdi), %rax
	call	*8(%rax)
.L1662:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1663
	movq	(%rdi), %rax
	call	*8(%rax)
.L1663:
	leaq	.LC78(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1697:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1693:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$140, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1656
.L1701:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC78(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1661
.L1699:
	leaq	.LC77(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1659
.L1700:
	movq	(%rax), %rdi
	leaq	.LC79(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rdi
	jmp	.L1667
.L1698:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25797:
	.size	_ZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateE, .-_ZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC82:
	.string	"unlinking code marked for deopt"
	.section	.text._ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE
	.type	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE, @function
_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE:
.LFB25799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	andq	$-262144, %r15
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%r15), %rax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	leaq	-37592(%rax), %r14
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1759
.L1703:
	movq	%r14, -176(%rbp)
	leaq	-176(%rbp), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	_ZZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeEE28trace_event_unique_atomic418(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1760
.L1705:
	movq	$0, -160(%rbp)
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L1761
.L1707:
	cmpb	$0, _ZN2v88internal19FLAG_flush_bytecodeE(%rip)
	jne	.L1762
.L1711:
	testq	%r12, %r12
	je	.L1763
.L1726:
	testb	$62, 43(%r13)
	je	.L1764
.L1727:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1765
.L1702:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1766
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1760:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1767
.L1706:
	movq	%rdx, _ZZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeEE28trace_event_unique_atomic418(%rip)
	jmp	.L1705
	.p2align 4,,10
	.p2align 3
.L1764:
	movq	31(%r13), %rax
	leaq	-168(%rbp), %rdi
	movl	15(%rax), %eax
	movq	31(%r13), %rdx
	orl	$1, %eax
	movl	%eax, 15(%rdx)
	leaq	.LC82(%rip), %rdx
	movq	39(%rbx), %rax
	movq	7(%rax), %rax
	movq	%rax, -168(%rbp)
	movq	23(%rbx), %rsi
	call	_ZN2v88internal14FeedbackVector41EvictOptimizedCodeMarkedForDeoptimizationENS0_18SharedFunctionInfoEPKc@PLT
	movq	31(%r13), %rax
	movl	15(%rax), %eax
	testb	$4, %al
	je	.L1768
.L1728:
	movq	31(%rbx), %rax
	movq	39(%rax), %rdi
	call	_ZN2v88internal11Deoptimizer30DeoptimizeMarkedCodeForContextENS0_13NativeContextE
	jmp	.L1727
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	47(%rbx), %r13
	jmp	.L1726
	.p2align 4,,10
	.p2align 3
.L1762:
	movq	23(%rbx), %rax
	leaq	47(%rbx), %r8
	movq	47(%rbx), %rdx
	testb	$1, %al
	je	.L1711
	movq	-1(%rax), %rcx
	cmpw	$160, 11(%rcx)
	jne	.L1711
	testb	$1, %dl
	je	.L1711
	movq	-1(%rdx), %rcx
	cmpw	$69, 11(%rcx)
	jne	.L1711
	movabsq	$287762808832, %rcx
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1717
	testb	$1, %al
	je	.L1711
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L1717
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L1711
.L1717:
	cmpl	$67, 59(%rdx)
	je	.L1711
	movq	24(%r15), %rdi
	movl	$67, %esi
	movq	%r8, -184(%rbp)
	addq	$3592, %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, 47(%rbx)
	movq	%rax, %rdx
	testb	$1, %al
	je	.L1718
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L1718
	movq	-184(%rbp), %r8
	movq	%rbx, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L1718:
	movq	39(%rbx), %r15
	movq	%r15, %rax
	leaq	7(%r15), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	-37504(%rdx), %rdx
	movq	%rdx, 7(%r15)
	testb	$1, %dl
	je	.L1730
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rcx
	movq	%r8, -184(%rbp)
	testl	$262144, %ecx
	je	.L1722
	movq	%r15, %rdi
	movq	%rax, -208(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %r8
	movq	-208(%rbp), %rax
	movq	-200(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	8(%r8), %rcx
.L1722:
	andl	$24, %ecx
	je	.L1730
	testb	$24, 8(%rax)
	jne	.L1730
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1730:
	cmpb	$0, _ZN2v88internal29FLAG_lazy_feedback_allocationE(%rip)
	movl	_ZN2v88internal21FLAG_interrupt_budgetE(%rip), %eax
	je	.L1725
	movl	_ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE(%rip), %eax
.L1725:
	movl	%eax, 15(%r15)
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1761:
	pxor	%xmm0, %xmm0
	movq	%rdx, -192(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	$0, -184(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1769
.L1708:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1709
	movq	(%rdi), %rax
	movq	%rdx, -192(%rbp)
	call	*8(%rax)
	movq	-192(%rbp), %rdx
.L1709:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1710
	movq	(%rdi), %rax
	movq	%rdx, -192(%rbp)
	call	*8(%rax)
	movq	-192(%rbp), %rdx
.L1710:
	leaq	.LC78(%rip), %rax
	movq	%rdx, -152(%rbp)
	movq	%rax, -144(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -136(%rbp)
	leaq	-152(%rbp), %rax
	movq	%rax, -160(%rbp)
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1768:
	movq	31(%r13), %rax
	movl	15(%rax), %eax
	movq	31(%r13), %rdx
	orl	$4, %eax
	movl	%eax, 15(%rdx)
	jmp	.L1728
	.p2align 4,,10
	.p2align 3
.L1765:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1702
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	40960(%r14), %rax
	leaq	-120(%rbp), %rsi
	movl	$140, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1703
	.p2align 4,,10
	.p2align 3
.L1769:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movl	$88, %esi
	pushq	%rcx
	leaq	.LC78(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	-192(%rbp), %rdx
	movq	%rax, -184(%rbp)
	addq	$64, %rsp
	jmp	.L1708
	.p2align 4,,10
	.p2align 3
.L1767:
	leaq	.LC77(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1706
.L1766:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25799:
	.size	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE, .-_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE
	.section	.rodata._ZNSt6vectorIN2v88internal15TranslatedFrame8iteratorESaIS3_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S5_EEmRKS3_.str1.1,"aMS",@progbits,1
.LC83:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIN2v88internal15TranslatedFrame8iteratorESaIS3_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S5_EEmRKS3_,"axG",@progbits,_ZNSt6vectorIN2v88internal15TranslatedFrame8iteratorESaIS3_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S5_EEmRKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal15TranslatedFrame8iteratorESaIS3_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S5_EEmRKS3_
	.type	_ZNSt6vectorIN2v88internal15TranslatedFrame8iteratorESaIS3_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S5_EEmRKS3_, @function
_ZNSt6vectorIN2v88internal15TranslatedFrame8iteratorESaIS3_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S5_EEmRKS3_:
.LFB30165:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L1819
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	movabsq	$-3689348814741910323, %rcx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rdi
	movq	16(%r12), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	imulq	%rcx, %rdx
	cmpq	%r14, %rdx
	jb	.L1773
	movq	%rdi, %r9
	movq	8(%r15), %rsi
	movq	(%r15), %xmm3
	subq	%rbx, %r9
	movq	16(%r15), %xmm2
	movl	32(%r15), %r8d
	movq	%r9, %rdx
	movq	%rsi, -56(%rbp)
	movq	24(%r15), %rsi
	sarq	$3, %rdx
	imulq	%rcx, %rdx
	movq	%rsi, -64(%rbp)
	cmpq	%rdx, %r14
	jnb	.L1774
	leaq	(%r14,%r14,4), %rdx
	movq	%rdi, %rsi
	leaq	0(,%rdx,8), %r10
	subq	%r10, %rsi
	cmpq	%rsi, %rdi
	je	.L1799
	movq	%rsi, %rdx
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L1776:
	movdqu	16(%rdx), %xmm0
	movdqu	(%rdx), %xmm4
	addq	$40, %rdx
	addq	$40, %rcx
	movups	%xmm4, -40(%rcx)
	movups	%xmm0, -24(%rcx)
	movl	-8(%rdx), %r9d
	movl	%r9d, -8(%rcx)
	cmpq	%rdx, %rdi
	jne	.L1776
	movq	8(%r12), %rdx
.L1775:
	movq	%rsi, %r9
	addq	%r10, %rdx
	subq	%rbx, %r9
	movq	%rdx, 8(%r12)
	movabsq	$-3689348814741910323, %rdx
	movq	%r9, %rcx
	sarq	$3, %rcx
	imulq	%rdx, %rcx
	movq	$-40, %rdx
	testq	%r9, %r9
	jle	.L1780
	.p2align 4,,10
	.p2align 3
.L1777:
	movdqu	(%rsi,%rdx), %xmm5
	movups	%xmm5, (%rdi,%rdx)
	movdqu	16(%rsi,%rdx), %xmm6
	movups	%xmm6, 16(%rdi,%rdx)
	movl	32(%rsi,%rdx), %r9d
	movl	%r9d, 32(%rdi,%rdx)
	subq	$40, %rdx
	subq	$1, %rcx
	jne	.L1777
.L1780:
	leaq	(%rbx,%r10), %rdx
	cmpq	%rdx, %rbx
	je	.L1770
	movdqa	%xmm3, %xmm1
	movdqa	%xmm2, %xmm0
	movhps	-56(%rbp), %xmm1
	movhps	-64(%rbp), %xmm0
	.p2align 4,,10
	.p2align 3
.L1779:
	movl	%r8d, 32(%rax)
	addq	$40, %rax
	movups	%xmm1, -40(%rax)
	movups	%xmm0, -24(%rax)
	cmpq	%rax, %rdx
	jne	.L1779
.L1770:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1819:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1774:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	subq	%rdx, %r14
	je	.L1801
	movdqa	%xmm3, %xmm1
	movdqa	%xmm2, %xmm0
	movq	%rdi, %rcx
	movq	%r14, %rsi
	movhps	-56(%rbp), %xmm1
	movhps	-64(%rbp), %xmm0
	.p2align 4,,10
	.p2align 3
.L1782:
	movl	%r8d, 32(%rcx)
	addq	$40, %rcx
	movups	%xmm1, -40(%rcx)
	movups	%xmm0, -24(%rcx)
	subq	$1, %rsi
	jne	.L1782
	leaq	(%r14,%r14,4), %rdx
	leaq	(%rdi,%rdx,8), %rdx
.L1781:
	movq	%rdx, 8(%r12)
	cmpq	%rbx, %rdi
	je	.L1783
	movq	%rbx, %rsi
	.p2align 4,,10
	.p2align 3
.L1784:
	movdqu	16(%rsi), %xmm0
	movdqu	(%rsi), %xmm7
	addq	$40, %rsi
	addq	$40, %rdx
	movups	%xmm7, -40(%rdx)
	movups	%xmm0, -24(%rdx)
	movl	-8(%rsi), %ecx
	movl	%ecx, -8(%rdx)
	cmpq	%rsi, %rdi
	jne	.L1784
	addq	%r9, 8(%r12)
	movdqa	%xmm3, %xmm1
	movdqa	%xmm2, %xmm0
	movhps	-56(%rbp), %xmm1
	movhps	-64(%rbp), %xmm0
	.p2align 4,,10
	.p2align 3
.L1786:
	movl	%r8d, 32(%rax)
	addq	$40, %rax
	movups	%xmm1, -40(%rax)
	movups	%xmm0, -24(%rax)
	cmpq	%rax, %rdi
	jne	.L1786
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1773:
	.cfi_restore_state
	movq	(%r12), %r9
	movq	%rdi, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$230584300921369395, %rcx
	movq	%rcx, %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %r14
	ja	.L1822
	cmpq	%rax, %r14
	movq	%rax, %rdx
	movq	%rsi, %r13
	cmovnb	%r14, %rdx
	addq	%rdx, %rax
	setc	%dl
	subq	%r9, %r13
	movzbl	%dl, %edx
	testq	%rdx, %rdx
	jne	.L1802
	testq	%rax, %rax
	jne	.L1791
	xorl	%r8d, %r8d
.L1798:
	movq	(%r15), %xmm1
	movl	32(%r15), %ecx
	leaq	(%rax,%r13), %rsi
	movq	%r14, %rdx
	movq	16(%r15), %xmm0
	movhps	8(%r15), %xmm1
	movhps	24(%r15), %xmm0
	.p2align 4,,10
	.p2align 3
.L1792:
	movl	%ecx, 32(%rsi)
	addq	$40, %rsi
	movups	%xmm1, -40(%rsi)
	movups	%xmm0, -24(%rsi)
	subq	$1, %rdx
	jne	.L1792
	cmpq	%r9, %rbx
	je	.L1803
	movq	%r9, %rcx
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1794:
	movdqu	16(%rcx), %xmm0
	movdqu	(%rcx), %xmm2
	addq	$40, %rcx
	addq	$40, %rdx
	movups	%xmm2, -40(%rdx)
	movups	%xmm0, -24(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	cmpq	%rcx, %rbx
	jne	.L1794
	leaq	-40(%rbx), %rdx
	subq	%r9, %rdx
	shrq	$3, %rdx
	leaq	40(%rax,%rdx,8), %rcx
.L1793:
	leaq	(%r14,%r14,4), %rdx
	leaq	(%rcx,%rdx,8), %r13
	cmpq	%rdi, %rbx
	je	.L1795
	movq	%rbx, %rdx
	movq	%r13, %rcx
	.p2align 4,,10
	.p2align 3
.L1796:
	movdqu	(%rdx), %xmm3
	movl	32(%rdx), %esi
	addq	$40, %rdx
	addq	$40, %rcx
	movdqu	-24(%rdx), %xmm4
	movups	%xmm3, -40(%rcx)
	movups	%xmm4, -24(%rcx)
	movl	%esi, -8(%rcx)
	cmpq	%rdx, %rdi
	jne	.L1796
	subq	%rbx, %rdi
	leaq	-40(%rdi), %rdx
	shrq	$3, %rdx
	leaq	40(%r13,%rdx,8), %r13
.L1795:
	testq	%r9, %r9
	je	.L1797
	movq	%r9, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L1797:
	movq	%r13, %xmm5
	movq	%rax, %xmm0
	movq	%r8, 16(%r12)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1802:
	.cfi_restore_state
	movabsq	$9223372036854775800, %r8
.L1790:
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	(%r12), %r9
	movq	8(%r12), %rdi
	addq	%rax, %r8
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1791:
	cmpq	%rcx, %rax
	cmova	%rcx, %rax
	leaq	(%rax,%rax,4), %r8
	salq	$3, %r8
	jmp	.L1790
	.p2align 4,,10
	.p2align 3
.L1799:
	movq	%rdi, %rdx
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L1801:
	movq	%rdi, %rdx
	jmp	.L1781
	.p2align 4,,10
	.p2align 3
.L1783:
	addq	%r9, %rdx
	movq	%rdx, 8(%r12)
	jmp	.L1770
	.p2align 4,,10
	.p2align 3
.L1803:
	movq	%rax, %rcx
	jmp	.L1793
.L1822:
	leaq	.LC83(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30165:
	.size	_ZNSt6vectorIN2v88internal15TranslatedFrame8iteratorESaIS3_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S5_EEmRKS3_, .-_ZNSt6vectorIN2v88internal15TranslatedFrame8iteratorESaIS3_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S5_EEmRKS3_
	.section	.text._ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.type	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, @function
_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E:
.LFB30192:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1838
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L1827:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L1825
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1823
.L1826:
	movq	%rbx, %r12
	jmp	.L1827
	.p2align 4,,10
	.p2align 3
.L1825:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1826
.L1823:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1838:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE30192:
	.size	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, .-_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.section	.text._ZN2v88internal9AssemblerD2Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD2Ev
	.type	_ZN2v88internal9AssemblerD2Ev, @function
_ZN2v88internal9AssemblerD2Ev:
.LFB25877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L1845
.L1842:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1842
.L1845:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L1843
.L1844:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L1848
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1843
.L1849:
	movq	%rbx, %r13
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1848:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1849
.L1843:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1847
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L1850
	.p2align 4,,10
	.p2align 3
.L1851:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L1851
	movq	328(%r12), %rdi
.L1850:
	call	_ZdlPv@PLT
.L1847:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1852
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L1853
	.p2align 4,,10
	.p2align 3
.L1854:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L1854
	movq	240(%r12), %rdi
.L1853:
	call	_ZdlPv@PLT
.L1852:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.cfi_endproc
.LFE25877:
	.size	_ZN2v88internal9AssemblerD2Ev, .-_ZN2v88internal9AssemblerD2Ev
	.weak	_ZN2v88internal9AssemblerD1Ev
	.set	_ZN2v88internal9AssemblerD1Ev,_ZN2v88internal9AssemblerD2Ev
	.section	.text._ZN2v88internal14MacroAssemblerD2Ev,"axG",@progbits,_ZN2v88internal14MacroAssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MacroAssemblerD2Ev
	.type	_ZN2v88internal14MacroAssemblerD2Ev, @function
_ZN2v88internal14MacroAssemblerD2Ev:
.LFB32920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L1879
.L1876:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1876
.L1879:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L1877
.L1878:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L1882
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1877
.L1883:
	movq	%rbx, %r13
	jmp	.L1878
	.p2align 4,,10
	.p2align 3
.L1882:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1883
.L1877:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1881
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L1884
	.p2align 4,,10
	.p2align 3
.L1885:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L1885
	movq	328(%r12), %rdi
.L1884:
	call	_ZdlPv@PLT
.L1881:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1886
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L1887
	.p2align 4,,10
	.p2align 3
.L1888:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L1888
	movq	240(%r12), %rdi
.L1887:
	call	_ZdlPv@PLT
.L1886:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.cfi_endproc
.LFE32920:
	.size	_ZN2v88internal14MacroAssemblerD2Ev, .-_ZN2v88internal14MacroAssemblerD2Ev
	.weak	_ZN2v88internal14MacroAssemblerD1Ev
	.set	_ZN2v88internal14MacroAssemblerD1Ev,_ZN2v88internal14MacroAssemblerD2Ev
	.section	.text._ZN2v88internal9AssemblerD0Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD0Ev
	.type	_ZN2v88internal9AssemblerD0Ev, @function
_ZN2v88internal9AssemblerD0Ev:
.LFB25879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L1913
.L1910:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1910
.L1913:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L1911
.L1912:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L1916
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1911
.L1917:
	movq	%rbx, %r13
	jmp	.L1912
	.p2align 4,,10
	.p2align 3
.L1916:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1917
.L1911:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1915
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L1918
	.p2align 4,,10
	.p2align 3
.L1919:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L1919
	movq	328(%r12), %rdi
.L1918:
	call	_ZdlPv@PLT
.L1915:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1920
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L1921
	.p2align 4,,10
	.p2align 3
.L1922:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L1922
	movq	240(%r12), %rdi
.L1921:
	call	_ZdlPv@PLT
.L1920:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE25879:
	.size	_ZN2v88internal9AssemblerD0Ev, .-_ZN2v88internal9AssemblerD0Ev
	.section	.text._ZN2v88internal14MacroAssemblerD0Ev,"axG",@progbits,_ZN2v88internal14MacroAssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MacroAssemblerD0Ev
	.type	_ZN2v88internal14MacroAssemblerD0Ev, @function
_ZN2v88internal14MacroAssemblerD0Ev:
.LFB32922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L1947
.L1944:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1944
.L1947:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L1945
.L1946:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L1950
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1945
.L1951:
	movq	%rbx, %r13
	jmp	.L1946
	.p2align 4,,10
	.p2align 3
.L1950:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1951
.L1945:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1949
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L1952
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L1953
	movq	328(%r12), %rdi
.L1952:
	call	_ZdlPv@PLT
.L1949:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1954
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L1955
	.p2align 4,,10
	.p2align 3
.L1956:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L1956
	movq	240(%r12), %rdi
.L1955:
	call	_ZdlPv@PLT
.L1954:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE32922:
	.size	_ZN2v88internal14MacroAssemblerD0Ev, .-_ZN2v88internal14MacroAssemblerD0Ev
	.section	.rodata._ZN2v88internal11Deoptimizer32EnsureCodeForDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE.str1.8,"aMS",@progbits,1
	.align 8
.LC84:
	.string	"kind == DeoptimizeKind::kEager || kind == DeoptimizeKind::kSoft || kind == DeoptimizeKind::kLazy"
	.align 8
.LC85:
	.string	"isolate->heap()->IsImmovable(*code)"
	.align 8
.LC86:
	.string	"data->deopt_entry_code(kind).is_null()"
	.section	.text._ZN2v88internal11Deoptimizer32EnsureCodeForDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer32EnsureCodeForDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE
	.type	_ZN2v88internal11Deoptimizer32EnsureCodeForDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE, @function
_ZN2v88internal11Deoptimizer32EnsureCodeForDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE:
.LFB25866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$744, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpb	$2, %sil
	ja	.L2025
	movq	41040(%rdi), %rcx
	movzbl	%sil, %edx
	movl	%esi, -772(%rbp)
	movq	%rdi, %rbx
	leaq	8(%rcx,%rdx,8), %r12
	cmpq	$0, (%r12)
	je	.L2026
.L1977:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2027
	addq	$744, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2026:
	.cfi_restore_state
	leaq	-760(%rbp), %rdi
	movl	$16384, %esi
	leaq	-688(%rbp), %r14
	call	_ZN2v88internal18NewAssemblerBufferEi@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-760(%rbp), %rdx
	leaq	-752(%rbp), %r15
	leaq	-608(%rbp), %r13
	movq	$0, -760(%rbp)
	movq	%rdx, -752(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb@PLT
	movq	%r13, %rdi
	movq	%r15, %r8
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal18TurboAssemblerBaseC1EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE@PLT
	movq	-752(%rbp), %rdi
	movl	-772(%rbp), %eax
	testq	%rdi, %rdi
	je	.L1981
	movq	(%rdi), %rdx
	call	*8(%rdx)
	movl	-772(%rbp), %eax
.L1981:
	movq	-760(%rbp), %rdi
	leaq	16+_ZTVN2v88internal14MacroAssemblerE(%rip), %rcx
	movq	%rcx, -608(%rbp)
	testq	%rdi, %rdi
	je	.L1982
	movq	(%rdi), %rdx
	movl	%eax, -772(%rbp)
	call	*8(%rdx)
	movl	-772(%rbp), %eax
.L1982:
	movq	-96(%rbp), %rsi
	movl	%eax, %edx
	movq	%r13, %rdi
	movb	$0, -400(%rbp)
	call	_ZN2v88internal11Deoptimizer29GenerateDeoptimizationEntriesEPNS0_14MacroAssemblerEPNS0_7IsolateENS0_14DeoptimizeKindE@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -688(%rbp)
	movaps	%xmm0, -672(%rbp)
	movaps	%xmm0, -656(%rbp)
	movaps	%xmm0, -640(%rbp)
	movaps	%xmm0, -624(%rbp)
	call	_ZN2v88internal9Assembler7GetCodeEPNS0_7IsolateEPNS0_8CodeDescEPNS0_21SafepointTableBuilderEi@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movl	$2, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory11CodeBuilderC1EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE@PLT
	movq	%r15, %rdi
	movb	$0, -695(%rbp)
	call	_ZN2v88internal7Factory11CodeBuilder5BuildEv@PLT
	leaq	37592(%rbx), %rdi
	movq	(%rax), %rsi
	movq	%rax, %r14
	call	_ZN2v88internal4Heap11IsImmovableENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L2028
	cmpq	$0, (%r12)
	jne	.L2029
	movq	(%r14), %rax
	movq	%rax, (%r12)
	movq	-128(%rbp), %rbx
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	leaq	-144(%rbp), %r12
	movq	%rax, -608(%rbp)
	testq	%rbx, %rbx
	je	.L1988
.L1985:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1985
.L1988:
	movq	-184(%rbp), %r12
	leaq	-200(%rbp), %r14
	testq	%r12, %r12
	je	.L1986
.L1987:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L1991
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1986
.L1992:
	movq	%rbx, %r12
	jmp	.L1987
	.p2align 4,,10
	.p2align 3
.L1991:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1992
.L1986:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1990
	movq	-208(%rbp), %rax
	movq	-240(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L1993
	.p2align 4,,10
	.p2align 3
.L1994:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L1994
	movq	-280(%rbp), %rdi
.L1993:
	call	_ZdlPv@PLT
.L1990:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1995
	movq	-296(%rbp), %rax
	movq	-328(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L1996
	.p2align 4,,10
	.p2align 3
.L1997:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L1997
	movq	-368(%rbp), %rdi
.L1996:
	call	_ZdlPv@PLT
.L1995:
	movq	%r13, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L2025:
	leaq	.LC84(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2028:
	leaq	.LC85(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2029:
	leaq	.LC86(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2027:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25866:
	.size	_ZN2v88internal11Deoptimizer32EnsureCodeForDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE, .-_ZN2v88internal11Deoptimizer32EnsureCodeForDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE
	.section	.text._ZN2v88internal11Deoptimizer34EnsureCodeForDeoptimizationEntriesEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer34EnsureCodeForDeoptimizationEntriesEPNS0_7IsolateE
	.type	_ZN2v88internal11Deoptimizer34EnsureCodeForDeoptimizationEntriesEPNS0_7IsolateE, @function
_ZN2v88internal11Deoptimizer34EnsureCodeForDeoptimizationEntriesEPNS0_7IsolateE:
.LFB25894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal11Deoptimizer32EnsureCodeForDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal11Deoptimizer32EnsureCodeForDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11Deoptimizer32EnsureCodeForDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE
	.cfi_endproc
.LFE25894:
	.size	_ZN2v88internal11Deoptimizer34EnsureCodeForDeoptimizationEntriesEPNS0_7IsolateE, .-_ZN2v88internal11Deoptimizer34EnsureCodeForDeoptimizationEntriesEPNS0_7IsolateE
	.section	.rodata._ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC87:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm, @function
_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm:
.LFB30228:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L2071
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	%rcx, %r13
	subq	(%rdi), %r13
	subq	%rcx, %rax
	movq	%r13, %r15
	sarq	$3, %rax
	sarq	$3, %r15
	subq	%r15, %rsi
	cmpq	%rbx, %rax
	jb	.L2034
	cmpq	$1, %rbx
	je	.L2053
	leaq	-2(%rbx), %rdx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	addq	$1, %rdx
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	%rax, %rsi
	addq	$1, %rax
	salq	$4, %rsi
	movups	%xmm0, (%rcx,%rsi)
	cmpq	%rax, %rdx
	ja	.L2036
	leaq	(%rdx,%rdx), %rax
	salq	$4, %rdx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L2037
.L2035:
	movq	$0, (%rdx)
.L2037:
	leaq	(%rcx,%rbx,8), %rax
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2071:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L2034:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rsi
	jb	.L2074
	cmpq	%rbx, %r15
	movq	%rbx, %rsi
	cmovnb	%r15, %rsi
	addq	%r15, %rsi
	cmpq	%rdx, %rsi
	cmova	%rdx, %rsi
	salq	$3, %rsi
	movq	%rsi, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	cmpq	$1, %rbx
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	leaq	(%rax,%r13), %rcx
	je	.L2052
	leaq	-2(%rbx), %rax
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L2043:
	movq	%rdx, %rdi
	addq	$1, %rdx
	salq	$4, %rdi
	movups	%xmm0, (%rcx,%rdi)
	cmpq	%rdx, %rax
	ja	.L2043
	leaq	(%rax,%rax), %rdx
	salq	$4, %rax
	addq	%rax, %rcx
	cmpq	%rdx, %rbx
	je	.L2041
.L2052:
	movq	$0, (%rcx)
.L2041:
	movq	8(%r12), %rcx
	movq	(%r12), %rdi
	cmpq	%rdi, %rcx
	je	.L2051
	leaq	-8(%rcx), %rax
	leaq	15(%rdi), %rdx
	subq	%rdi, %rax
	subq	%r14, %rdx
	shrq	$3, %rax
	cmpq	$30, %rdx
	jbe	.L2055
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rax
	je	.L2055
	addq	$1, %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2049:
	movdqu	(%rdi,%rdx), %xmm1
	movups	%xmm1, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L2049
	movq	%rax, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%rdi,%rdx), %rcx
	addq	%r14, %rdx
	cmpq	%r8, %rax
	je	.L2045
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
.L2045:
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rsi
.L2046:
	addq	%r15, %rbx
	movq	%r14, (%r12)
	leaq	(%r14,%rbx,8), %rax
	addq	%rsi, %r14
	movq	%rax, 8(%r12)
	movq	%r14, 16(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2055:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L2047:
	movq	(%rax), %r8
	addq	$8, %rax
	addq	$8, %rdx
	movq	%r8, -8(%rdx)
	cmpq	%rax, %rcx
	jne	.L2047
.L2051:
	testq	%rdi, %rdi
	je	.L2046
	jmp	.L2045
	.p2align 4,,10
	.p2align 3
.L2053:
	movq	%rcx, %rdx
	jmp	.L2035
.L2074:
	leaq	.LC87(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30228:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm, .-_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm
	.section	.text._ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl,"axG",@progbits,_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl
	.type	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl, @function
_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl:
.LFB30491:
	.cfi_startproc
	endbr64
	movq	(%rsi), %r9
	movq	%rdi, %rax
	movq	8(%rsi), %rdi
	movq	16(%rsi), %r8
	movq	24(%rsi), %rsi
	movq	%r9, %rcx
	subq	%rdi, %rcx
	sarq	$5, %rcx
	addq	%rdx, %rcx
	js	.L2076
	cmpq	$15, %rcx
	jle	.L2080
	movq	%rcx, %rdx
	sarq	$4, %rdx
.L2079:
	leaq	(%rsi,%rdx,8), %rsi
	salq	$4, %rdx
	movq	(%rsi), %rdi
	subq	%rdx, %rcx
	salq	$5, %rcx
	leaq	(%rdi,%rcx), %rdx
	leaq	512(%rdi), %r8
	movq	%rdx, %xmm0
.L2078:
	movq	%rdi, %xmm1
	movq	%rsi, %xmm2
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	movq	%r8, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L2080:
	salq	$5, %rdx
	leaq	(%r9,%rdx), %rcx
	movq	%rcx, %xmm0
	jmp	.L2078
	.p2align 4,,10
	.p2align 3
.L2076:
	movq	%rcx, %rdx
	notq	%rdx
	shrq	$4, %rdx
	notq	%rdx
	jmp	.L2079
	.cfi_endproc
.LFE30491:
	.size	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl, .-_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl
	.section	.rodata._ZN2v88internal15TranslatedState32GetArgumentsInfoFromJSFrameIndexEiPi.str1.1,"aMS",@progbits,1
.LC88:
	.string	"argc_object.IsSmi()"
	.section	.text._ZN2v88internal15TranslatedState32GetArgumentsInfoFromJSFrameIndexEiPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState32GetArgumentsInfoFromJSFrameIndexEiPi
	.type	_ZN2v88internal15TranslatedState32GetArgumentsInfoFromJSFrameIndexEiPi, @function
_ZN2v88internal15TranslatedState32GetArgumentsInfoFromJSFrameIndexEiPi:
.LFB26034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rcx
	movq	8(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movabsq	$-1229782938247303441, %rax
	subq	%rcx, %r8
	sarq	$3, %r8
	imulq	%rax, %r8
	testq	%r8, %r8
	je	.L2097
	movq	%rdi, %r12
	movq	%rdx, %r13
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2095:
	movl	(%rcx,%rbx), %edx
	leaq	(%rcx,%rbx), %r9
	leal	-4(%rdx), %edi
	cmpl	$1, %edi
	jbe	.L2098
	testl	%edx, %edx
	jne	.L2083
.L2098:
	testl	%esi, %esi
	jle	.L2085
	subl	$1, %esi
.L2083:
	addq	$1, %rax
	addq	$120, %rbx
	cmpq	%r8, %rax
	jne	.L2095
.L2097:
	xorl	%eax, %eax
.L2081:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L2107
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2085:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L2086
	subq	$1, %rax
	movq	%rax, %rsi
	salq	$4, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
	salq	$3, %rax
	addq	%rax, %rcx
	cmpl	$1, (%rcx)
	je	.L2087
.L2086:
	movq	16(%r9), %rax
	movq	(%rax), %rax
	movzwl	41(%rax), %eax
	cmpl	$4, %edx
	je	.L2108
.L2089:
	addl	$1, %eax
	movzwl	%ax, %eax
	movl	%eax, 0(%r13)
.L2091:
	movq	(%r12), %rax
	addq	%rbx, %rax
	jmp	.L2081
.L2087:
	movl	24(%rcx), %edx
	movl	%edx, 0(%r13)
	addq	(%r12), %rax
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2108:
	cmpw	$-1, %ax
	jne	.L2089
	movslq	24(%r9), %rdx
	leaq	-80(%rbp), %rdi
	leaq	56(%r9), %rsi
	call	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl
	movq	-80(%rbp), %rdi
	cmpb	$2, 1(%rdi)
	je	.L2109
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
.L2093:
	testb	$1, %al
	jne	.L2110
	sarq	$32, %rax
	movl	%eax, 0(%r13)
	jmp	.L2091
.L2109:
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	jmp	.L2093
.L2110:
	leaq	.LC88(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26034:
	.size	_ZN2v88internal15TranslatedState32GetArgumentsInfoFromJSFrameIndexEiPi, .-_ZN2v88internal15TranslatedState32GetArgumentsInfoFromJSFrameIndexEiPi
	.section	.text._ZNSt11_Deque_baseIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_initialize_mapEm:
.LFB31288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	shrq	$6, %rdi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	1(%rdi), %rbx
	addq	$3, %rdi
	subq	$8, %rsp
	cmpq	$8, %rdi
	ja	.L2112
	movq	$8, 8(%r13)
	movl	$64, %edi
.L2113:
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	movq	%rax, 0(%r13)
	subq	%rbx, %rdx
	shrq	%rdx
	leaq	(%rax,%rdx,8), %r15
	leaq	(%r15,%rbx,8), %r12
	movq	%r15, %rbx
	cmpq	%r12, %r15
	jnb	.L2115
	.p2align 4,,10
	.p2align 3
.L2114:
	movl	$512, %edi
	addq	$8, %rbx
	call	_Znwm@PLT
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r12
	ja	.L2114
.L2115:
	movq	(%r15), %rdx
	andl	$63, %r14d
	subq	$8, %r12
	movq	%r15, 40(%r13)
	leaq	512(%rdx), %rax
	movq	%rdx, %xmm0
	movq	%rax, 32(%r13)
	movq	(%r12), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, %xmm2
	movups	%xmm0, 16(%r13)
	leaq	(%rax,%r14,8), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 48(%r13)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 64(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2112:
	.cfi_restore_state
	movq	%rdi, 8(%r13)
	salq	$3, %rdi
	jmp	.L2113
	.cfi_endproc
.LFE31288:
	.size	_ZNSt11_Deque_baseIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_initialize_mapEm
	.section	.text._ZN2v88internal11DeoptimizerC2EPNS0_7IsolateENS0_10JSFunctionENS0_14DeoptimizeKindEjmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11DeoptimizerC2EPNS0_7IsolateENS0_10JSFunctionENS0_14DeoptimizeKindEjmi
	.type	_ZN2v88internal11DeoptimizerC2EPNS0_7IsolateENS0_10JSFunctionENS0_14DeoptimizeKindEjmi, @function
_ZN2v88internal11DeoptimizerC2EPNS0_7IsolateENS0_10JSFunctionENS0_14DeoptimizeKindEjmi:
.LFB25825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	addq	$176, %rdi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movl	16(%rbp), %eax
	movups	%xmm0, -96(%rdi)
	movq	%rsi, -176(%rdi)
	xorl	%esi, %esi
	movq	%rdx, -168(%rdi)
	movq	$0, -160(%rdi)
	movl	%r8d, -152(%rdi)
	movb	%cl, -148(%rdi)
	movq	%r9, -144(%rdi)
	movl	%eax, -136(%rdi)
	movb	$0, -132(%rdi)
	movq	$-1, -128(%rdi)
	movq	$0, -120(%rdi)
	movq	$0, -112(%rdi)
	movq	$0, -104(%rdi)
	movq	$0, -16(%rdi)
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, -80(%rdi)
	movups	%xmm0, -64(%rdi)
	movups	%xmm0, -48(%rdi)
	movups	%xmm0, -32(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	call	_ZNSt11_Deque_baseIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_initialize_mapEm
	pxor	%xmm0, %xmm0
	movq	$0, 256(%r15)
	movq	$0, 264(%r15)
	movl	$-1, 272(%r15)
	movups	%xmm0, 280(%r15)
	movups	%xmm0, 296(%r15)
	cmpb	$0, 41048(%r13)
	je	.L2121
	movb	$0, 41048(%r13)
	movb	$1, 44(%r15)
.L2121:
	movq	32(%r15), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11Deoptimizer20FindDeoptimizingCodeEm
	testq	%rax, %rax
	je	.L2158
.L2122:
	xorl	%r14d, %r14d
	cmpb	$0, _ZN2v88internal16FLAG_trace_deoptE(%rip)
	movq	%rax, 16(%r15)
	jne	.L2159
.L2123:
	movq	%r14, 304(%r15)
	testb	$62, 43(%rax)
	je	.L2160
	cmpb	$1, 28(%r15)
	je	.L2144
.L2133:
	movq	8(%r15), %rax
	movl	$16, %r12d
	testb	$1, %al
	je	.L2136
	movq	23(%rax), %rax
	movzwl	41(%rax), %r12d
	addl	$1, %r12d
	movzwl	%r12w, %r12d
	movl	%r12d, %edi
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	cmpb	$1, %al
	sbbl	$-1, %r12d
	leal	16(,%r12,8), %r12d
.L2136:
	movq	16(%r15), %rax
	movl	40(%r15), %edx
	movl	43(%rax), %eax
	addl	%edx, %r12d
	testb	$62, %al
	jne	.L2138
	shrl	$4, %eax
	andl	$134217720, %eax
	subl	$16, %eax
	cmpl	%eax, %edx
	jne	.L2161
.L2138:
	movq	23(%rbx), %rax
	movl	%r12d, %r13d
	leaq	320(%r13), %rdi
	movzwl	41(%rax), %ebx
	call	malloc@PLT
	movdqa	.LC19(%rip), %xmm0
	movl	$16, %ecx
	addl	$1, %ebx
	movq	%r13, (%rax)
	movq	%rax, %rdx
	leaq	144(%rax), %rdi
	movzwl	%bx, %ebx
	leaq	320(%rdx), %rsi
	movl	%ebx, 8(%rax)
	xorl	%eax, %eax
	rep stosq
	movl	$3203260077, %eax
	movups	%xmm0, 272(%rdx)
	movq	%rax, 304(%rdx)
	xorl	%eax, %eax
	movups	%xmm0, 288(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 64(%rdx)
	movups	%xmm0, 80(%rdx)
	movups	%xmm0, 96(%rdx)
	movups	%xmm0, 112(%rdx)
	movups	%xmm0, 128(%rdx)
	testl	%r12d, %r12d
	je	.L2142
	.p2align 4,,10
	.p2align 3
.L2143:
	movl	%eax, %ecx
	movl	$3203260077, %ebx
	addl	$8, %eax
	movq	%rbx, (%rcx,%rsi)
	cmpl	%eax, %r12d
	ja	.L2143
.L2142:
	cmpb	$0, _ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE(%rip)
	movq	%rdx, 56(%r15)
	jne	.L2162
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2160:
	.cfi_restore_state
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$4, %al
	je	.L2163
.L2128:
	movq	16(%r15), %rax
	testb	$62, 43(%rax)
	jne	.L2133
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	movq	16(%r15), %rdx
	movq	31(%rdx), %rdx
	orl	$4, %eax
	movl	%eax, 15(%rdx)
	movq	(%r15), %rax
	movq	32(%r15), %r14
	movq	41488(%rax), %r13
	movl	40(%r15), %eax
	movl	%eax, -56(%rbp)
	movq	16(%r15), %rax
	movq	%rax, -64(%rbp)
	leaq	56(%r13), %rax
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%r13), %r13
	testq	%r13, %r13
	je	.L2135
	.p2align 4,,10
	.p2align 3
.L2134:
	movq	8(%r13), %rdi
	movl	-56(%rbp), %r8d
	movq	%r14, %rcx
	movl	%r12d, %edx
	movq	-64(%rbp), %rsi
	movq	(%rdi), %r9
	call	*128(%r9)
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L2134
.L2135:
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L2133
	.p2align 4,,10
	.p2align 3
.L2159:
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movl	$8, %edi
	movq	%rax, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	%rax, %r14
	movq	%rdx, (%rax)
	je	.L2157
	cmpq	$0, 144(%rdx)
	je	.L2164
.L2125:
	addl	$1, 152(%rdx)
.L2157:
	movq	16(%r15), %rax
	jmp	.L2123
	.p2align 4,,10
	.p2align 3
.L2158:
	movq	32(%r15), %rsi
	movq	(%r15), %rdi
	call	_ZN2v88internal7Isolate14FindCodeObjectEm@PLT
	jmp	.L2122
	.p2align 4,,10
	.p2align 3
.L2162:
	movq	16(%r15), %rsi
	movq	15(%rsi), %rax
	movq	79(%rax), %rdx
	movl	_ZN2v88internal11Deoptimizer14kDeoptExitSizeE(%rip), %ecx
	movl	32(%r15), %eax
	sarq	$32, %rdx
	leaq	63(%rsi,%rdx), %rdx
	subl	%ecx, %eax
	subl	%edx, %eax
	cltd
	idivl	%ecx
	movl	%eax, 24(%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2163:
	.cfi_restore_state
	cmpb	$1, 28(%r15)
	jne	.L2128
	.p2align 4,,10
	.p2align 3
.L2144:
	movq	40960(%r13), %r13
	cmpb	$0, 7232(%r13)
	je	.L2130
	movq	7224(%r13), %rax
.L2131:
	testq	%rax, %rax
	je	.L2128
	addl	$1, (%rax)
	jmp	.L2128
	.p2align 4,,10
	.p2align 3
.L2130:
	movb	$1, 7232(%r13)
	leaq	7208(%r13), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7224(%r13)
	jmp	.L2131
	.p2align 4,,10
	.p2align 3
.L2161:
	leaq	.LC18(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2164:
	movq	(%rdx), %rdi
	leaq	.LC79(%rip), %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, 144(%rdx)
	jmp	.L2125
	.cfi_endproc
.LFE25825:
	.size	_ZN2v88internal11DeoptimizerC2EPNS0_7IsolateENS0_10JSFunctionENS0_14DeoptimizeKindEjmi, .-_ZN2v88internal11DeoptimizerC2EPNS0_7IsolateENS0_10JSFunctionENS0_14DeoptimizeKindEjmi
	.globl	_ZN2v88internal11DeoptimizerC1EPNS0_7IsolateENS0_10JSFunctionENS0_14DeoptimizeKindEjmi
	.set	_ZN2v88internal11DeoptimizerC1EPNS0_7IsolateENS0_10JSFunctionENS0_14DeoptimizeKindEjmi,_ZN2v88internal11DeoptimizerC2EPNS0_7IsolateENS0_10JSFunctionENS0_14DeoptimizeKindEjmi
	.section	.rodata._ZN2v88internal11Deoptimizer3NewEmNS0_14DeoptimizeKindEjmiPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC89:
	.string	"(isolate->deoptimizer_data()->current_) == nullptr"
	.section	.text._ZN2v88internal11Deoptimizer3NewEmNS0_14DeoptimizeKindEjmiPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer3NewEmNS0_14DeoptimizeKindEjmiPNS0_7IsolateE
	.type	_ZN2v88internal11Deoptimizer3NewEmNS0_14DeoptimizeKindEjmiPNS0_7IsolateE, @function
_ZN2v88internal11Deoptimizer3NewEmNS0_14DeoptimizeKindEjmiPNS0_7IsolateE:
.LFB25766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$312, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$24, %rsp
	movq	%rcx, -64(%rbp)
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal8MallocednwEm@PLT
	movl	-52(%rbp), %r8d
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	-64(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%rax, %r12
	pushq	%r8
	movl	%r15d, %r8d
	movq	%rcx, %r9
	movl	%r14d, %ecx
	call	_ZN2v88internal11DeoptimizerC1EPNS0_7IsolateENS0_10JSFunctionENS0_14DeoptimizeKindEjmi
	movq	41040(%rbx), %rax
	popq	%rdx
	popq	%rcx
	cmpq	$0, 32(%rax)
	jne	.L2168
	movq	%r12, 32(%rax)
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2168:
	.cfi_restore_state
	leaq	.LC89(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25766:
	.size	_ZN2v88internal11Deoptimizer3NewEmNS0_14DeoptimizeKindEjmiPNS0_7IsolateE, .-_ZN2v88internal11Deoptimizer3NewEmNS0_14DeoptimizeKindEjmiPNS0_7IsolateE
	.section	.text._ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB31343:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$192153584101141162, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	imulq	%rcx, %rax
	cmpq	%rdi, %rax
	je	.L2188
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L2179
	movabsq	$9223372036854775776, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L2189
.L2171:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rax, %r14
	leaq	48(%rax), %r8
	addq	%rax, %rsi
.L2178:
	movq	(%rdx), %rax
	movdqu	8(%rdx), %xmm4
	addq	%r14, %rcx
	movdqu	24(%rdx), %xmm5
	movl	40(%rdx), %edx
	movq	%rax, (%rcx)
	movl	%edx, 40(%rcx)
	movups	%xmm4, 8(%rcx)
	movups	%xmm5, 24(%rcx)
	cmpq	%r15, %rbx
	je	.L2173
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L2174:
	movq	(%rdx), %rax
	addq	$48, %rdx
	addq	$48, %rcx
	movq	%rax, -48(%rcx)
	movdqu	-24(%rdx), %xmm0
	movdqu	-40(%rdx), %xmm1
	movups	%xmm0, -24(%rcx)
	movups	%xmm1, -40(%rcx)
	movl	-8(%rdx), %eax
	movl	%eax, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L2174
	movabsq	$768614336404564651, %rcx
	leaq	-48(%rbx), %rdx
	subq	%r15, %rdx
	shrq	$4, %rdx
	imulq	%rcx, %rdx
	movabsq	$1152921504606846975, %rcx
	andq	%rcx, %rdx
	leaq	6(%rdx,%rdx,2), %r8
	salq	$4, %r8
	addq	%r14, %r8
.L2173:
	cmpq	%r12, %rbx
	je	.L2175
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L2176:
	movq	(%rdx), %rax
	movdqu	8(%rdx), %xmm2
	addq	$48, %rdx
	addq	$48, %rcx
	movdqu	-24(%rdx), %xmm3
	movq	%rax, -48(%rcx)
	movl	-8(%rdx), %eax
	movups	%xmm2, -40(%rcx)
	movups	%xmm3, -24(%rcx)
	movl	%eax, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L2176
	movabsq	$768614336404564651, %rcx
	subq	%rbx, %rdx
	subq	$48, %rdx
	shrq	$4, %rdx
	imulq	%rcx, %rdx
	movabsq	$1152921504606846975, %rcx
	andq	%rcx, %rdx
	leaq	3(%rdx,%rdx,2), %rdx
	salq	$4, %rdx
	addq	%rdx, %r8
.L2175:
	testq	%r15, %r15
	je	.L2177
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %r8
.L2177:
	movq	%r14, %xmm0
	movq	%r8, %xmm6
	movq	%rsi, 16(%r13)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2189:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L2172
	movl	$48, %r8d
	xorl	%esi, %esi
	xorl	%r14d, %r14d
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2179:
	movl	$48, %esi
	jmp	.L2171
.L2172:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	imulq	$48, %rax, %rsi
	jmp	.L2171
.L2188:
	leaq	.LC76(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE31343:
	.size	_ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.rodata._ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc.str1.1,"aMS",@progbits,1
.LC90:
	.string	" (input #%d)\n"
	.section	.text._ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc,"axG",@progbits,_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc
	.type	_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc, @function
_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc:
.LFB25751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$2, 1(%rdi)
	je	.L2206
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	%rax, %r13
.L2192:
	movl	24(%rbx), %eax
	movq	8(%rbx), %rdx
	subl	$8, %eax
	movl	%eax, 24(%rbx)
	movq	%r13, 320(%rdx,%rax)
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L2194
	movl	24(%rbx), %edx
	movq	8(%rbx), %rsi
	movq	%r13, -96(%rbp)
	movq	(%rax), %rax
	movq	%rdx, %rcx
	addq	272(%rsi), %rdx
	leaq	.LC4(%rip), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-96(%rbp), %rsi
	testb	$1, %sil
	je	.L2207
	movq	16(%rbx), %rax
	leaq	-96(%rbp), %rdi
	movq	(%rax), %rax
	movq	144(%rax), %rsi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
.L2196:
	movq	16(%rbx), %rax
	movq	%r14, %rdx
	leaq	.LC6(%rip), %rsi
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L2194
	movq	(%rax), %rax
	movl	32(%r12), %edx
	leaq	.LC90(%rip), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2194:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	cmpq	304(%rax), %r13
	je	.L2208
.L2190:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2209
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2206:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movq	(%rax), %r13
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2208:
	movq	8(%rbx), %rdx
	movl	24(%rbx), %eax
	movdqu	(%r12), %xmm0
	movdqu	16(%r12), %xmm1
	addq	272(%rdx), %rax
	movl	32(%r12), %edx
	movq	%rax, -96(%rbp)
	movl	%edx, -56(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm1, -72(%rbp)
	movq	288(%rdi), %rsi
	cmpq	296(%rdi), %rsi
	je	.L2199
	movq	%rax, (%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-72(%rbp), %rax
	movq	%rax, 24(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 32(%rsi)
	movl	-56(%rbp), %eax
	movl	%eax, 40(%rsi)
	addq	$48, 288(%rdi)
	jmp	.L2190
	.p2align 4,,10
	.p2align 3
.L2207:
	movq	%rsi, %rdx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	sarq	$32, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2196
	.p2align 4,,10
	.p2align 3
.L2199:
	leaq	-96(%rbp), %rdx
	addq	$280, %rdi
	call	_ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2190
.L2209:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25751:
	.size	_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc, .-_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc
	.section	.text._ZN2v88internal11Deoptimizer28QueueValueForMaterializationEmNS0_6ObjectERKNS0_15TranslatedFrame8iteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer28QueueValueForMaterializationEmNS0_6ObjectERKNS0_15TranslatedFrame8iteratorE
	.type	_ZN2v88internal11Deoptimizer28QueueValueForMaterializationEmNS0_6ObjectERKNS0_15TranslatedFrame8iteratorE, @function
_ZN2v88internal11Deoptimizer28QueueValueForMaterializationEmNS0_6ObjectERKNS0_15TranslatedFrame8iteratorE:
.LFB25862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	cmpq	304(%rax), %rdx
	je	.L2216
.L2210:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2217
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2216:
	.cfi_restore_state
	movdqu	(%rcx), %xmm0
	movdqu	16(%rcx), %xmm1
	movq	%rsi, -64(%rbp)
	movl	32(%rcx), %eax
	movq	288(%rdi), %r8
	movups	%xmm0, -56(%rbp)
	movl	%eax, -24(%rbp)
	movups	%xmm1, -40(%rbp)
	cmpq	296(%rdi), %r8
	je	.L2212
	movq	%rsi, (%r8)
	movq	-56(%rbp), %rax
	movq	%rax, 8(%r8)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r8)
	movq	-40(%rbp), %rax
	movq	%rax, 24(%r8)
	movq	-32(%rbp), %rax
	movq	%rax, 32(%r8)
	movl	-24(%rbp), %eax
	movl	%eax, 40(%r8)
	addq	$48, 288(%rdi)
	jmp	.L2210
	.p2align 4,,10
	.p2align 3
.L2212:
	leaq	-64(%rbp), %rdx
	addq	$280, %rdi
	movq	%r8, %rsi
	call	_ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2210
.L2217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25862:
	.size	_ZN2v88internal11Deoptimizer28QueueValueForMaterializationEmNS0_6ObjectERKNS0_15TranslatedFrame8iteratorE, .-_ZN2v88internal11Deoptimizer28QueueValueForMaterializationEmNS0_6ObjectERKNS0_15TranslatedFrame8iteratorE
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.1,"aMS",@progbits,1
.LC91:
	.string	"invalid"
.LC92:
	.string	""
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC93:
	.string	"code == kJavaScriptCallArgCountRegister.code()"
	.align 8
.LC94:
	.string	"IsAnyTagged(type.representation())"
	.align 8
.LC95:
	.string	"BuiltinContinuationModeIsJavaScript(mode) == has_argc"
	.align 8
.LC96:
	.string	"  translating BuiltinContinuation to %s, => register_param_count=%d, stack_param_count=%d, frame_size=%d\n"
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.1
.LC97:
	.string	"padding\n"
.LC98:
	.string	"stack parameter"
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.8
	.align 8
.LC99:
	.string	"placeholder for exception on lazy deopt\n"
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.1
.LC100:
	.string	"exception (from accumulator)\n"
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.8
	.align 8
.LC101:
	.string	"placeholder for return result on lazy deopt\n"
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.1
.LC102:
	.string	"caller's pc\n"
.LC103:
	.string	"caller's fp\n"
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.8
	.align 8
.LC104:
	.string	"context (builtin continuation sentinel)\n"
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.1
.LC105:
	.string	"JSFunction\n"
.LC106:
	.string	"unused\n"
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.8
	.align 8
.LC107:
	.string	"frame height at deoptimization\n"
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.1
.LC108:
	.string	"builtin JavaScript context\n"
.LC109:
	.string	"builtin index\n"
.LC110:
	.string	"NewArray"
.LC111:
	.string	"rax"
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.8
	.align 8
.LC112:
	.string	"tagged argument count %s (will be untagged by continuation)\n"
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.1
.LC113:
	.string	"builtin register argument %s\n"
.LC114:
	.string	"callback result\n"
	.section	.rodata._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE.str1.8
	.align 8
.LC115:
	.string	"translated_frame->end() == value_iterator"
	.align 8
.LC116:
	.string	"0u == frame_writer.top_offset()"
	.section	.text._ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE
	.type	_ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE, @function
_ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE:
.LFB25850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-288(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$360, %rsp
	movl	%edx, -332(%rbp)
	movl	4(%rsi), %edi
	movdqu	56(%rsi), %xmm3
	movdqu	72(%rsi), %xmm4
	movl	%ecx, -312(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movl	$0, -128(%rbp)
	call	_ZN2v88internal8Builtins23GetBuiltinFromBailoutIdENS0_9BailoutIdE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	%eax, -352(%rbp)
	call	_ZN2v88internal8Builtins26CallInterfaceDescriptorForENS1_4NameE@PLT
	call	_ZN2v88internal21RegisterConfiguration7DefaultEv@PLT
	movl	64(%rbx), %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	leaq	-256(%rbp), %rdi
	movq	%rax, -328(%rbp)
	leal	-1(%rcx), %esi
	movq	%rax, %rcx
	cmpl	%r14d, %esi
	movl	%esi, -336(%rbp)
	movl	24(%r13), %esi
	pushq	$0
	sete	%r8b
	pushq	%r15
	movzbl	28(%rbx), %r9d
	call	_ZN2v88internal28BuiltinContinuationFrameInfoC1EiRKNS0_23CallInterfaceDescriptorEPKNS0_21RegisterConfigurationEbNS0_14DeoptimizeKindENS0_23BuiltinContinuationModeENS0_13FrameInfoKindE@PLT
	movq	-280(%rbp), %rcx
	popq	%r14
	popq	%r15
	movl	(%rcx), %edi
	movl	%edi, -320(%rbp)
	testl	%edi, %edi
	jle	.L2219
	movslq	4(%rcx), %rdx
	movq	32(%rcx), %rsi
	subl	$1, %edi
	xorl	%r8d, %r8d
	movq	24(%rcx), %rcx
	leaq	1(%rdx,%rdi), %rdi
	leaq	(%rdx,%rdx), %rax
	negq	%rdx
	addq	%rdi, %rdi
	leaq	(%rcx,%rdx,4), %r9
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2221:
	addq	$2, %rax
	cmpq	%rax, %rdi
	je	.L2338
.L2222:
	movzbl	(%rsi,%rax), %edx
	cmpb	$2, 1(%rsi,%rax)
	sete	%cl
	cmpb	$4, %dl
	sete	%r10b
	andb	%r10b, %cl
	jne	.L2339
	subl	$6, %edx
	cmpb	$2, %dl
	jbe	.L2221
	leaq	.LC94(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2339:
	movl	(%r9,%rax,2), %r12d
	testl	%r12d, %r12d
	jne	.L2340
	addq	$2, %rax
	movl	%ecx, %r8d
	cmpq	%rax, %rdi
	jne	.L2222
	.p2align 4,,10
	.p2align 3
.L2338:
	movl	-312(%rbp), %eax
	testl	%eax, %eax
	je	.L2223
	subl	$1, %eax
	cmpl	$2, %eax
	ja	.L2261
	movl	$1, %eax
.L2224:
	cmpb	%al, %r8b
	jne	.L2290
	movl	-240(%rbp), %eax
	cmpq	$0, 304(%rbx)
	movl	-244(%rbp), %r12d
	movl	-248(%rbp), %r14d
	movl	%eax, -392(%rbp)
	je	.L2226
	movl	-352(%rbp), %edi
	call	_ZN2v88internal8Builtins4nameEi@PLT
	movl	-320(%rbp), %ecx
	movl	%r14d, %r8d
	movl	%r12d, %r9d
	movq	%rax, %rdx
	movq	304(%rbx), %rax
	leaq	.LC96(%rip), %rsi
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movl	-248(%rbp), %r14d
.L2226:
	movl	%r12d, %r15d
	leaq	320(%r15), %rdi
	call	malloc@PLT
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%rax, %rdi
	movq	%rax, %r11
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal16FrameDescriptionC1Eji
	movslq	-332(%rbp), %rax
	movq	72(%rbx), %rdx
	movq	%rbx, %xmm0
	movq	%r11, %xmm5
	movq	%r11, (%rdx,%rax,8)
	leaq	0(,%rax,8), %rsi
	movq	%rax, %rcx
	punpcklqdq	%xmm5, %xmm0
	movq	304(%rbx), %rax
	movq	%rsi, -376(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	%rax, -176(%rbp)
	movq	(%r11), %rax
	movl	%eax, -168(%rbp)
	testl	%ecx, %ecx
	je	.L2341
	movq	72(%rbx), %rax
	movq	-8(%rax,%rsi), %rax
	movq	272(%rax), %rax
	subq	%r15, %rax
	movq	%rax, -360(%rbp)
	movq	%rax, %rcx
.L2228:
	movq	-344(%rbp), %rax
	movq	-160(%rbp), %rdi
	movq	%rcx, 272(%rax)
	cmpb	$2, 1(%rdi)
	je	.L2342
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	%rax, -400(%rbp)
.L2230:
	leaq	-160(%rbp), %r15
	addl	$1, -128(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	movq	(%rbx), %rax
	movl	-248(%rbp), %edi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	testb	%al, %al
	jne	.L2343
.L2231:
	movl	-252(%rbp), %r11d
	leaq	-112(%rbp), %rax
	xorl	%r14d, %r14d
	movq	%rax, -384(%rbp)
	testl	%r11d, %r11d
	jne	.L2232
	jmp	.L2246
	.p2align 4,,10
	.p2align 3
.L2236:
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	%rax, %r12
.L2237:
	movl	-168(%rbp), %eax
	movq	-184(%rbp), %rdx
	subl	$8, %eax
	movl	%eax, -168(%rbp)
	movq	%r12, 320(%rdx,%rax)
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L2239
	movq	%r12, -112(%rbp)
	movq	(%rax), %rax
	movl	-168(%rbp), %edx
	movq	-184(%rbp), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	movq	%rdx, %rcx
	addq	272(%rsi), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-112(%rbp), %rsi
	testb	$1, %sil
	jne	.L2240
	movq	%rsi, %rdx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	sarq	$32, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L2241:
	movq	-176(%rbp), %rax
	leaq	.LC98(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L2239
	movq	(%rax), %rax
	movl	-128(%rbp), %edx
	leaq	.LC90(%rip), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2239:
	movq	-192(%rbp), %rdx
	movl	-128(%rbp), %eax
	movq	(%rdx), %rcx
	cmpq	304(%rcx), %r12
	je	.L2344
.L2243:
	addl	$1, %eax
	movq	%r15, %rdi
	addl	$1, %r14d
	movl	%eax, -128(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	cmpl	-252(%rbp), %r14d
	jnb	.L2246
.L2232:
	movq	-160(%rbp), %rdi
	cmpb	$2, 1(%rdi)
	jne	.L2236
	movq	16(%rdi), %rax
	movq	(%rax), %r12
	jmp	.L2237
	.p2align 4,,10
	.p2align 3
.L2341:
	movq	80(%rbx), %rax
	subq	%r15, %rax
	movq	%rax, -360(%rbp)
	movq	%rax, %rcx
	jmp	.L2228
.L2219:
	movl	-312(%rbp), %eax
	testl	%eax, %eax
	jne	.L2345
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L2223:
	xorl	%eax, %eax
	jmp	.L2224
	.p2align 4,,10
	.p2align 3
.L2344:
	movq	-184(%rbp), %rsi
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm6
	movl	-168(%rbp), %ecx
	addq	272(%rsi), %rcx
	movl	%eax, -72(%rbp)
	movq	%rcx, -112(%rbp)
	movups	%xmm5, -104(%rbp)
	movups	%xmm6, -88(%rbp)
	movq	288(%rdx), %rsi
	cmpq	296(%rdx), %rsi
	je	.L2244
	movq	%rcx, (%rsi)
	movq	-104(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 32(%rsi)
	movl	-72(%rbp), %eax
	movl	%eax, 40(%rsi)
	addq	$48, 288(%rdx)
.L2245:
	movl	-128(%rbp), %eax
	jmp	.L2243
	.p2align 4,,10
	.p2align 3
.L2240:
	movq	-176(%rbp), %rax
	movq	-384(%rbp), %rdi
	movq	(%rax), %rax
	movq	144(%rax), %rsi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	jmp	.L2241
	.p2align 4,,10
	.p2align 3
.L2246:
	movl	-312(%rbp), %eax
	cmpl	$2, %eax
	je	.L2233
	cmpl	$3, %eax
	jne	.L2235
	movq	56(%rbx), %rax
	leaq	-192(%rbp), %rdi
	leaq	.LC100(%rip), %rdx
	movq	16(%rax), %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
.L2235:
	cmpb	$0, -256(%rbp)
	jne	.L2346
.L2247:
	movq	-328(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movslq	8(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L2347
.L2248:
	movl	-320(%rbp), %r10d
	testl	%r10d, %r10d
	jle	.L2252
	movl	-320(%rbp), %eax
	xorl	%esi, %esi
	subl	$1, %eax
	leaq	4(,%rax,4), %r8
	.p2align 4,,10
	.p2align 3
.L2253:
	movq	-280(%rbp), %rax
	movq	-224(%rbp), %r9
	movq	%r15, %rdi
	movdqa	-160(%rbp), %xmm1
	movq	24(%rax), %rax
	movslq	(%rax,%rsi), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%r9,%rax,8), %rax
	movups	%xmm1, (%rax)
	movdqa	-144(%rbp), %xmm2
	movups	%xmm2, 16(%rax)
	movl	-128(%rbp), %edx
	movl	%edx, 32(%rax)
	addl	$1, -128(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	addq	$4, %rsi
	cmpq	%rsi, %r8
	jne	.L2253
.L2252:
	movq	-160(%rbp), %rdi
	cmpb	$2, 1(%rdi)
	je	.L2348
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	-160(%rbp), %rdi
	movq	%rax, %rsi
.L2254:
	movq	-152(%rbp), %rax
	movl	-128(%rbp), %edx
	movq	%rdi, -112(%rbp)
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	movq	-144(%rbp), %rax
	movl	%edx, -80(%rbp)
	addl	$1, %edx
	movq	%rax, -96(%rbp)
	movq	-136(%rbp), %rax
	movl	%edx, -128(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	movq	-224(%rbp), %rax
	movdqa	-112(%rbp), %xmm7
	movl	-332(%rbp), %r8d
	movups	%xmm7, 240(%rax)
	movdqa	-96(%rbp), %xmm7
	movups	%xmm7, 256(%rax)
	movl	-80(%rbp), %edx
	movl	%edx, 272(%rax)
	movq	-344(%rbp), %rax
	movq	%rsi, 296(%rax)
	movq	%rsi, 64(%rax)
	testl	%r8d, %r8d
	jne	.L2255
	movq	96(%rbx), %r12
.L2256:
	movl	-168(%rbp), %eax
	movq	-184(%rbp), %rdi
	movq	%r12, %rdx
	leal	-8(%rax), %esi
	movl	%esi, -168(%rbp)
	call	_ZN2v88internal16FrameDescription11SetCallerPcEjl@PLT
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L2257
	movq	(%rax), %rax
	movl	-168(%rbp), %edx
	movq	%r12, %r8
	leaq	.LC102(%rip), %r9
	movq	-184(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2257:
	movl	-332(%rbp), %edi
	testl	%edi, %edi
	jne	.L2258
	movq	88(%rbx), %r12
.L2259:
	movl	-168(%rbp), %eax
	movq	-184(%rbp), %rdi
	movq	%r12, %rdx
	leal	-8(%rax), %esi
	movl	%esi, -168(%rbp)
	call	_ZN2v88internal16FrameDescription11SetCallerFpEjl@PLT
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L2260
	movq	(%rax), %rax
	movl	-168(%rbp), %edx
	movq	%r12, %r8
	leaq	.LC103(%rip), %r9
	movq	-184(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2260:
	movl	-168(%rbp), %edx
	movq	-344(%rbp), %rcx
	movq	%rdx, %rax
	addq	-360(%rbp), %rdx
	movq	%rdx, 288(%rcx)
	movl	-312(%rbp), %ecx
	movq	%rdx, -376(%rbp)
	cmpl	$3, %ecx
	ja	.L2261
	movl	%ecx, %edx
	leaq	CSWTCH.551(%rip), %rcx
	subl	$8, %eax
	movl	(%rcx,%rdx,4), %r8d
	movq	-184(%rbp), %rdx
	movl	%eax, -168(%rbp)
	addl	%r8d, %r8d
	movslq	%r8d, %r8
	movq	%r8, 320(%rdx,%rax)
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L2262
	movq	(%rax), %rax
	movl	-168(%rbp), %edx
	leaq	.LC104(%rip), %r9
	movq	-184(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2262:
	movl	-312(%rbp), %eax
	leaq	-192(%rbp), %r14
	subl	$1, %eax
	movl	%eax, -360(%rbp)
	cmpl	$2, %eax
	ja	.L2263
	movq	-400(%rbp), %rsi
	leaq	.LC105(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal11FrameWriter12PushRawValueElPKc
.L2264:
	movq	-392(%rbp), %rsi
	leaq	.LC107(%rip), %rdx
	movq	%r14, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	leaq	-112(%rbp), %rsi
	leaq	.LC108(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc
	movq	-352(%rbp), %rsi
	leaq	.LC109(%rip), %rdx
	movq	%r14, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	movq	-328(%rbp), %rcx
	movl	24(%rcx), %eax
	movl	%eax, -352(%rbp)
	testl	%eax, %eax
	jle	.L2265
	subl	$1, %eax
	movq	%r13, -384(%rbp)
	xorl	%r12d, %r12d
	movq	%rcx, %r13
	leaq	4(,%rax,4), %rax
	movq	%r14, -312(%rbp)
	movq	%rax, -328(%rbp)
	jmp	.L2289
	.p2align 4,,10
	.p2align 3
.L2351:
	testl	%r14d, %r14d
	jne	.L2300
	cmpl	$2, -360(%rbp)
	jbe	.L2268
.L2300:
	leaq	.LC91(%rip), %rcx
	movq	$-1, %r9
	cmpl	$-1, %r14d
	je	.L2271
	movslq	%r14d, %r9
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rax
	movq	(%rax,%r9,8), %rcx
.L2271:
	movq	-264(%rbp), %rsi
	leaq	.LC113(%rip), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	movq	%r9, -320(%rbp)
	movq	%r15, -272(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-320(%rbp), %r9
.L2272:
	cmpq	$0, 304(%rbx)
	je	.L2267
	movq	%r15, %r10
.L2273:
	movq	-224(%rbp), %rax
	leaq	(%r9,%r9,4), %rdx
	movq	-312(%rbp), %rdi
	addq	$4, %r12
	leaq	(%rax,%rdx,8), %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc
	movq	%r15, %rdi
	call	_ZdaPv@PLT
	cmpq	-328(%rbp), %r12
	je	.L2349
.L2289:
	movq	56(%r13), %rax
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movl	(%rax,%r12), %r14d
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2350
.L2266:
	cmpq	$0, 304(%rbx)
	movslq	%r14d, %r9
	movq	$128, -264(%rbp)
	jne	.L2351
.L2267:
	leaq	.LC92(%rip), %r10
	jmp	.L2273
	.p2align 4,,10
	.p2align 3
.L2349:
	movq	-384(%rbp), %r13
	movq	-312(%rbp), %r14
.L2265:
	movl	-352(%rbp), %edi
	xorl	%r12d, %r12d
	leaq	.LC97(%rip), %r15
	call	_ZN2v88internal33BuiltinContinuationFrameConstants16PaddingSlotCountEi@PLT
	testl	%eax, %eax
	jle	.L2277
	movq	%r13, -320(%rbp)
	movl	%r12d, %r13d
	movq	-368(%rbp), %r12
	movq	%rbx, -312(%rbp)
	movl	%eax, %ebx
	.p2align 4,,10
	.p2align 3
.L2274:
	movq	96(%r12), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	addl	$1, %r13d
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	cmpl	%r13d, %ebx
	jne	.L2274
	movq	-312(%rbp), %rbx
	movq	-320(%rbp), %r13
.L2277:
	movl	-332(%rbp), %ecx
	cmpl	%ecx, -336(%rbp)
	je	.L2352
.L2276:
	movq	-160(%rbp), %rax
	cmpq	%rax, 88(%r13)
	jne	.L2353
	movl	-168(%rbp), %esi
	testl	%esi, %esi
	jne	.L2354
	movl	-332(%rbp), %ecx
	cmpl	%ecx, -336(%rbp)
	je	.L2355
.L2281:
	call	_ZN2v88internal15JavaScriptFrame11fp_registerEv@PLT
	movq	-376(%rbp), %rsi
	movq	-344(%rbp), %rcx
	movl	%eax, %eax
	movq	%rsi, 16(%rcx,%rax,8)
	movq	(%rbx), %rax
	xorl	%esi, %esi
	leaq	41184(%rax), %rdi
	movzbl	-256(%rbp), %eax
	testb	%al, %al
	setne	%sil
	cmpl	$2, -360(%rbp)
	jbe	.L2282
	addl	$71, %esi
.L2283:
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, -296(%rbp)
	movl	43(%rax), %ecx
	leaq	63(%rax), %rdx
	testl	%ecx, %ecx
	js	.L2356
.L2285:
	movq	-344(%rbp), %rax
	movq	(%rbx), %rdi
	movl	$70, %esi
	movq	%rdx, 280(%rax)
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, -272(%rbp)
	leaq	63(%rax), %rdx
	movl	43(%rax), %eax
	testl	%eax, %eax
	js	.L2357
.L2287:
	movq	-344(%rbp), %rax
	movq	-224(%rbp), %rdi
	movq	%rdx, 312(%rax)
	testq	%rdi, %rdi
	je	.L2218
	call	_ZdlPv@PLT
.L2218:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2358
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2268:
	.cfi_restore_state
	leaq	.LC111(%rip), %rcx
	movq	%r15, %rdi
	movl	$128, %esi
	xorl	%eax, %eax
	leaq	.LC112(%rip), %rdx
	movq	%r15, -272(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movslq	%r14d, %r9
	jmp	.L2272
	.p2align 4,,10
	.p2align 3
.L2258:
	movq	72(%rbx), %rax
	movq	-376(%rbp), %rcx
	movq	-8(%rax,%rcx), %rax
	movq	288(%rax), %r12
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L2255:
	movq	72(%rbx), %rax
	movq	-376(%rbp), %rcx
	movq	-8(%rax,%rcx), %rax
	movq	280(%rax), %r12
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2263:
	leaq	.LC106(%rip), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11FrameWriter12PushRawValueElPKc
	jmp	.L2264
	.p2align 4,,10
	.p2align 3
.L2282:
	addl	$73, %esi
	jmp	.L2283
	.p2align 4,,10
	.p2align 3
.L2348:
	movq	16(%rdi), %rax
	movq	(%rax), %rsi
	jmp	.L2254
	.p2align 4,,10
	.p2align 3
.L2352:
	cmpb	$0, -256(%rbp)
	je	.L2278
	movq	56(%rbx), %rax
	leaq	.LC114(%rip), %rdx
	movq	%r14, %rdi
	movq	16(%rax), %rsi
	call	_ZN2v88internal11FrameWriter12PushRawValueElPKc
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2342:
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, -400(%rbp)
	jmp	.L2230
	.p2align 4,,10
	.p2align 3
.L2340:
	leaq	.LC93(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2233:
	movq	-368(%rbp), %rax
	leaq	-192(%rbp), %rdi
	leaq	.LC99(%rip), %rdx
	movq	96(%rax), %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	jmp	.L2235
	.p2align 4,,10
	.p2align 3
.L2343:
	movq	-368(%rbp), %rax
	leaq	-192(%rbp), %rdi
	leaq	.LC97(%rip), %rdx
	movq	96(%rax), %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	jmp	.L2231
	.p2align 4,,10
	.p2align 3
.L2347:
	leaq	-224(%rbp), %rdi
	movq	%r15, %rcx
	xorl	%esi, %esi
	call	_ZNSt6vectorIN2v88internal15TranslatedFrame8iteratorESaIS3_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S5_EEmRKS3_
	jmp	.L2248
	.p2align 4,,10
	.p2align 3
.L2346:
	movq	-368(%rbp), %rax
	leaq	-192(%rbp), %rdi
	leaq	.LC101(%rip), %rdx
	movq	96(%rax), %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	jmp	.L2247
.L2244:
	leaq	280(%rdx), %rdi
	movq	-384(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2245
.L2357:
	leaq	-272(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rdx
	jmp	.L2287
.L2356:
	leaq	-296(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rdx
	jmp	.L2285
.L2278:
	movq	-368(%rbp), %rax
	leaq	.LC114(%rip), %rdx
	movq	%r14, %rdi
	movq	88(%rax), %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	jmp	.L2276
.L2355:
	call	_ZN2v88internal15JavaScriptFrame16context_registerEv@PLT
	movq	-344(%rbp), %rcx
	movl	%eax, %eax
	movq	$0, 16(%rcx,%rax,8)
	jmp	.L2281
.L2345:
	subl	$1, %eax
	cmpl	$2, %eax
	ja	.L2261
.L2290:
	leaq	.LC95(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2353:
	leaq	.LC115(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2354:
	leaq	.LC116(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2350:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L2266
	leaq	.LC110(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L2358:
	call	__stack_chk_fail@PLT
.L2261:
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25850:
	.size	_ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE, .-_ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE
	.section	.rodata._ZN2v88internal11Deoptimizer30DoComputeArgumentsAdaptorFrameEPNS0_15TranslatedFrameEi.str1.8,"aMS",@progbits,1
	.align 8
.LC117:
	.string	"  translating arguments adaptor => variable_frame_size=%d, frame_size=%d\n"
	.align 8
.LC118:
	.string	"frame_index < output_count_ - 1"
	.align 8
.LC119:
	.string	"(output_[frame_index]) == nullptr"
	.section	.rodata._ZN2v88internal11Deoptimizer30DoComputeArgumentsAdaptorFrameEPNS0_15TranslatedFrameEi.str1.1,"aMS",@progbits,1
.LC120:
	.string	"context (adaptor sentinel)\n"
.LC121:
	.string	"function\n"
.LC122:
	.string	"argc\n"
	.section	.text._ZN2v88internal11Deoptimizer30DoComputeArgumentsAdaptorFrameEPNS0_15TranslatedFrameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer30DoComputeArgumentsAdaptorFrameEPNS0_15TranslatedFrameEi
	.type	_ZN2v88internal11Deoptimizer30DoComputeArgumentsAdaptorFrameEPNS0_15TranslatedFrameEi, @function
_ZN2v88internal11Deoptimizer30DoComputeArgumentsAdaptorFrameEPNS0_15TranslatedFrameEi:
.LFB25842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-112(%rbp), %rax
	pushq	%r13
	movq	%rax, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	24(%rsi), %r14d
	movq	%rsi, -312(%rbp)
	movdqu	72(%rsi), %xmm1
	movdqu	56(%rsi), %xmm2
	movl	%edx, -244(%rbp)
	movl	%r14d, %esi
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movaps	%xmm2, -288(%rbp)
	movaps	%xmm1, -272(%rbp)
	movq	%rax, -288(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm1, -192(%rbp)
	movl	$0, -176(%rbp)
	call	_ZN2v88internal25ArgumentsAdaptorFrameInfoC1Ei@PLT
	movl	-176(%rbp), %eax
	movl	-112(%rbp), %r8d
	leaq	-208(%rbp), %rdi
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm6
	movl	%eax, -128(%rbp)
	addl	$1, %eax
	movl	-108(%rbp), %ebx
	movl	%eax, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	movq	304(%r15), %rax
	testq	%rax, %rax
	je	.L2360
	movq	(%rax), %rax
	movl	%ebx, %ecx
	movl	%r8d, %edx
	leaq	.LC117(%rip), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2360:
	movl	%ebx, %r13d
	leaq	320(%r13), %rdi
	call	malloc@PLT
	movdqa	.LC19(%rip), %xmm0
	movl	$16, %ecx
	movq	%r13, (%rax)
	movq	%rax, %r12
	leaq	144(%rax), %rdi
	movl	%r14d, 8(%rax)
	xorl	%eax, %eax
	rep stosq
	movl	$3203260077, %eax
	movups	%xmm0, 16(%r12)
	movq	%rax, 304(%r12)
	movups	%xmm0, 272(%r12)
	movups	%xmm0, 288(%r12)
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movups	%xmm0, 112(%r12)
	movups	%xmm0, 128(%r12)
	testl	%ebx, %ebx
	je	.L2396
	leaq	320(%r12), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2362:
	movl	%eax, %edx
	movl	$3203260077, %esi
	addl	$8, %eax
	movq	%rsi, (%rdx,%rcx)
	cmpl	%eax, %ebx
	ja	.L2362
	movl	(%r12), %edx
.L2361:
	movq	%r15, %xmm0
	movq	304(%r15), %rax
	movq	%r12, %xmm7
	movl	%edx, -216(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movq	%rax, -224(%rbp)
	movl	64(%r15), %eax
	movaps	%xmm0, -240(%rbp)
	subl	$1, %eax
	cmpl	-244(%rbp), %eax
	jle	.L2420
	movslq	-244(%rbp), %rax
	salq	$3, %rax
	movq	%rax, -256(%rbp)
	addq	72(%r15), %rax
	cmpq	$0, (%rax)
	jne	.L2421
	movl	-244(%rbp), %esi
	movq	%r12, (%rax)
	testl	%esi, %esi
	je	.L2422
	movq	72(%r15), %rax
	movq	-256(%rbp), %rbx
	movq	-8(%rax,%rbx), %rax
	movq	272(%rax), %rax
	subq	%r13, %rax
	movq	%rax, -272(%rbp)
.L2366:
	movq	%rax, 272(%r12)
	movq	(%r15), %rax
	movl	%r14d, %edi
	leaq	-240(%rbp), %rbx
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	movq	%rbx, -304(%rbp)
	testb	%al, %al
	jne	.L2423
.L2367:
	testl	%r14d, %r14d
	jle	.L2368
	movq	-208(%rbp), %rdi
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L2369:
	cmpb	$2, 1(%rdi)
	je	.L2424
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	%rax, %rbx
.L2371:
	movl	-216(%rbp), %eax
	movq	-232(%rbp), %rdx
	subl	$8, %eax
	movl	%eax, -216(%rbp)
	movq	%rbx, 320(%rdx,%rax)
	movq	-224(%rbp), %rax
	testq	%rax, %rax
	je	.L2373
	movq	%rbx, -112(%rbp)
	movq	(%rax), %rax
	movl	-216(%rbp), %edx
	movq	-232(%rbp), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	movq	%rdx, %rcx
	addq	272(%rsi), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-112(%rbp), %rsi
	testb	$1, %sil
	jne	.L2374
	movq	%rsi, %rdx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	sarq	$32, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L2375:
	movq	-224(%rbp), %rax
	leaq	.LC98(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-224(%rbp), %rax
	testq	%rax, %rax
	je	.L2373
	movq	(%rax), %rax
	movl	-176(%rbp), %edx
	leaq	.LC90(%rip), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2373:
	movq	-240(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movq	-192(%rbp), %rdx
	movl	-176(%rbp), %eax
	movq	(%rcx), %rsi
	cmpq	304(%rsi), %rbx
	je	.L2425
.L2377:
	addl	$1, %eax
	addl	$1, %r13d
	movl	%eax, -176(%rbp)
	movl	$1, %eax
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2419:
	testl	%eax, %eax
	jle	.L2426
.L2380:
	subl	$1, %eax
	cmpb	$8, (%rdi)
	jne	.L2382
	addl	28(%rdi), %eax
.L2382:
	addq	$32, %rdi
	movq	%rdi, -208(%rbp)
	cmpq	%rdx, %rdi
	jne	.L2419
	movq	-184(%rbp), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, -184(%rbp)
	movq	8(%rdx), %rdi
	leaq	512(%rdi), %rdx
	movq	%rdi, -200(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%rdi, -208(%rbp)
	testl	%eax, %eax
	jg	.L2380
.L2426:
	cmpl	%r14d, %r13d
	jne	.L2369
.L2368:
	movl	-244(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L2385
	movq	96(%r15), %rbx
.L2386:
	movl	-216(%rbp), %eax
	movq	-232(%rbp), %rdi
	movq	%rbx, %rdx
	leal	-8(%rax), %esi
	movl	%esi, -216(%rbp)
	call	_ZN2v88internal16FrameDescription11SetCallerPcEjl@PLT
	movq	-224(%rbp), %rax
	testq	%rax, %rax
	je	.L2387
	movq	(%rax), %rax
	movl	-216(%rbp), %edx
	movq	%rbx, %r8
	leaq	.LC102(%rip), %r9
	movq	-232(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2387:
	movl	-244(%rbp), %edx
	testl	%edx, %edx
	jne	.L2388
	movq	88(%r15), %rbx
.L2389:
	movl	-216(%rbp), %eax
	movq	-232(%rbp), %rdi
	movq	%rbx, %rdx
	leal	-8(%rax), %esi
	movl	%esi, -216(%rbp)
	call	_ZN2v88internal16FrameDescription11SetCallerFpEjl@PLT
	movq	-224(%rbp), %rax
	testq	%rax, %rax
	je	.L2390
	movq	(%rax), %rax
	movl	-216(%rbp), %edx
	movq	%rbx, %r8
	leaq	.LC103(%rip), %r9
	movq	-232(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2390:
	movl	-216(%rbp), %edx
	movq	%rdx, %rax
	addq	-272(%rbp), %rdx
	movq	%rdx, 288(%r12)
	movq	-232(%rbp), %rdx
	subl	$8, %eax
	movl	%eax, -216(%rbp)
	movq	$38, 320(%rdx,%rax)
	movq	-224(%rbp), %rax
	testq	%rax, %rax
	je	.L2391
	movq	(%rax), %rax
	movl	-216(%rbp), %edx
	leaq	.LC120(%rip), %r9
	movl	$38, %r8d
	movq	-232(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2391:
	movq	-304(%rbp), %rbx
	leaq	-160(%rbp), %rsi
	leaq	.LC121(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc
	leal	-1(%r14), %esi
	movq	%rbx, %rdi
	leaq	.LC122(%rip), %rdx
	salq	$32, %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	movq	-296(%rbp), %rax
	movq	%rbx, %rdi
	leaq	.LC97(%rip), %rdx
	movq	96(%rax), %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	movq	-312(%rbp), %rax
	movq	-208(%rbp), %rbx
	cmpq	%rbx, 88(%rax)
	jne	.L2427
	movq	(%r15), %rax
	movl	$3, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, -112(%rbp)
	leaq	63(%rax), %rdx
	movl	43(%rax), %eax
	testl	%eax, %eax
	js	.L2428
.L2394:
	movq	(%r15), %rax
	movslq	4828(%rax), %rax
	addq	%rdx, %rax
	movq	%rax, 280(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2429
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2422:
	.cfi_restore_state
	movq	80(%r15), %rax
	subq	%r13, %rax
	movq	%rax, -272(%rbp)
	jmp	.L2366
	.p2align 4,,10
	.p2align 3
.L2424:
	movq	16(%rdi), %rax
	movq	(%rax), %rbx
	jmp	.L2371
	.p2align 4,,10
	.p2align 3
.L2425:
	movq	-232(%rbp), %rsi
	movl	-216(%rbp), %r8d
	addq	272(%rsi), %r8
	movq	%rdx, -88(%rbp)
	movq	-200(%rbp), %rsi
	movq	-184(%rbp), %rdx
	movq	%r8, -112(%rbp)
	movq	%rdi, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	%eax, -72(%rbp)
	movq	288(%rcx), %rsi
	cmpq	296(%rcx), %rsi
	je	.L2378
	movq	%r8, (%rsi)
	movq	-104(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 32(%rsi)
	movl	-72(%rbp), %eax
	movl	%eax, 40(%rsi)
	addq	$48, 288(%rcx)
.L2379:
	movl	-176(%rbp), %eax
	movq	-208(%rbp), %rdi
	movq	-192(%rbp), %rdx
	jmp	.L2377
	.p2align 4,,10
	.p2align 3
.L2374:
	movq	-224(%rbp), %rax
	movq	-288(%rbp), %rdi
	movq	(%rax), %rax
	movq	144(%rax), %rsi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	jmp	.L2375
	.p2align 4,,10
	.p2align 3
.L2388:
	movq	-256(%rbp), %rbx
	movq	72(%r15), %rax
	movq	-8(%rax,%rbx), %rax
	movq	288(%rax), %rbx
	jmp	.L2389
	.p2align 4,,10
	.p2align 3
.L2385:
	movq	-256(%rbp), %rbx
	movq	72(%r15), %rax
	movq	-8(%rax,%rbx), %rax
	movq	280(%rax), %rbx
	jmp	.L2386
	.p2align 4,,10
	.p2align 3
.L2423:
	movq	-296(%rbp), %rax
	leaq	.LC97(%rip), %rdx
	movq	%rbx, %rdi
	movq	96(%rax), %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	jmp	.L2367
	.p2align 4,,10
	.p2align 3
.L2378:
	movq	-288(%rbp), %rdx
	leaq	280(%rcx), %rdi
	call	_ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2379
	.p2align 4,,10
	.p2align 3
.L2428:
	movq	-288(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rdx
	jmp	.L2394
	.p2align 4,,10
	.p2align 3
.L2396:
	xorl	%edx, %edx
	jmp	.L2361
	.p2align 4,,10
	.p2align 3
.L2420:
	leaq	.LC118(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2421:
	leaq	.LC119(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2427:
	leaq	.LC115(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2429:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25842:
	.size	_ZN2v88internal11Deoptimizer30DoComputeArgumentsAdaptorFrameEPNS0_15TranslatedFrameEi, .-_ZN2v88internal11Deoptimizer30DoComputeArgumentsAdaptorFrameEPNS0_15TranslatedFrameEi
	.section	.rodata._ZN2v88internal11Deoptimizer27DoComputeConstructStubFrameEPNS0_15TranslatedFrameEi.str1.1,"aMS",@progbits,1
.LC123:
	.string	"create"
.LC124:
	.string	"invoke"
.LC125:
	.string	"new target\n"
.LC126:
	.string	"allocated receiver\n"
	.section	.rodata._ZN2v88internal11Deoptimizer27DoComputeConstructStubFrameEPNS0_15TranslatedFrameEi.str1.8,"aMS",@progbits,1
	.align 8
.LC127:
	.string	"!is_topmost || deopt_kind_ == DeoptimizeKind::kLazy"
	.align 8
.LC128:
	.string	"  translating construct stub => bailout_id=%d (%s), variable_frame_size=%d, frame_size=%d\n"
	.align 8
.LC129:
	.string	"context (construct stub sentinel)\n"
	.section	.rodata._ZN2v88internal11Deoptimizer27DoComputeConstructStubFrameEPNS0_15TranslatedFrameEi.str1.1
.LC130:
	.string	"context"
.LC131:
	.string	"constructor function\n"
	.section	.rodata._ZN2v88internal11Deoptimizer27DoComputeConstructStubFrameEPNS0_15TranslatedFrameEi.str1.8
	.align 8
.LC132:
	.string	"bailout_id == BailoutId::ConstructStubCreate() || bailout_id == BailoutId::ConstructStubInvoke()"
	.section	.rodata._ZN2v88internal11Deoptimizer27DoComputeConstructStubFrameEPNS0_15TranslatedFrameEi.str1.1
.LC133:
	.string	"subcall result\n"
	.section	.text._ZN2v88internal11Deoptimizer27DoComputeConstructStubFrameEPNS0_15TranslatedFrameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer27DoComputeConstructStubFrameEPNS0_15TranslatedFrameEi
	.type	_ZN2v88internal11Deoptimizer27DoComputeConstructStubFrameEPNS0_15TranslatedFrameEi, @function
_ZN2v88internal11Deoptimizer27DoComputeConstructStubFrameEPNS0_15TranslatedFrameEi:
.LFB25843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -308(%rbp)
	movl	64(%rdi), %eax
	movq	%rsi, -368(%rbp)
	movdqu	72(%rsi), %xmm1
	movdqu	56(%rsi), %xmm2
	subl	$1, %eax
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	cmpl	%edx, %eax
	movaps	%xmm1, -336(%rbp)
	movl	$0, -224(%rbp)
	sete	%bl
	movl	%eax, -336(%rbp)
	movaps	%xmm2, -352(%rbp)
	movaps	%xmm2, -256(%rbp)
	movaps	%xmm1, -240(%rbp)
	je	.L2504
.L2431:
	movq	(%r15), %rax
	movl	$29, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movzbl	%bl, %edx
	xorl	%ecx, %ecx
	movq	%rax, -296(%rbp)
	movq	-368(%rbp), %rax
	movl	24(%rax), %r14d
	movl	4(%rax), %r12d
	leaq	-112(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -352(%rbp)
	movl	%r14d, %esi
	movl	%r12d, -312(%rbp)
	call	_ZN2v88internal22ConstructStubFrameInfoC1EibNS0_13FrameInfoKindE@PLT
	movl	-224(%rbp), %eax
	movl	-112(%rbp), %r8d
	movdqa	-256(%rbp), %xmm5
	movdqa	-240(%rbp), %xmm6
	movl	%eax, -176(%rbp)
	addl	$1, %eax
	movl	-108(%rbp), %ebx
	movl	%eax, -224(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -392(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	movq	304(%r15), %rax
	testq	%rax, %rax
	je	.L2432
	movq	(%rax), %rax
	cmpl	$1, %r12d
	movl	%ebx, %r9d
	leaq	.LC124(%rip), %rdx
	leaq	.LC123(%rip), %rcx
	leaq	.LC128(%rip), %rsi
	cmovne	%rdx, %rcx
	movq	144(%rax), %rdi
	movl	%r12d, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2432:
	movl	%ebx, %r12d
	leaq	320(%r12), %rdi
	call	malloc@PLT
	movdqa	.LC19(%rip), %xmm0
	movl	$16, %ecx
	movq	%r12, (%rax)
	movq	%rax, %r13
	leaq	144(%rax), %rdi
	movl	%r14d, 8(%rax)
	xorl	%eax, %eax
	rep stosq
	movl	$3203260077, %eax
	movups	%xmm0, 272(%r13)
	movq	%rax, 304(%r13)
	movups	%xmm0, 288(%r13)
	movups	%xmm0, 16(%r13)
	movups	%xmm0, 32(%r13)
	movups	%xmm0, 48(%r13)
	movups	%xmm0, 64(%r13)
	movups	%xmm0, 80(%r13)
	movups	%xmm0, 96(%r13)
	movups	%xmm0, 112(%r13)
	movups	%xmm0, 128(%r13)
	testl	%ebx, %ebx
	je	.L2473
	leaq	320(%r13), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2435:
	movl	%eax, %edx
	movl	$3203260077, %esi
	addl	$8, %eax
	movq	%rsi, (%rdx,%rcx)
	cmpl	%eax, %ebx
	ja	.L2435
	movl	0(%r13), %edx
.L2434:
	movq	304(%r15), %rax
	movl	%edx, -264(%rbp)
	movq	%r13, %xmm7
	movl	%r14d, %edi
	movq	72(%r15), %rdx
	movq	%r15, %xmm0
	movq	%rax, -272(%rbp)
	movslq	-308(%rbp), %rax
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -288(%rbp)
	leaq	0(,%rax,8), %rbx
	movq	%r13, (%rdx,%rax,8)
	movq	72(%r15), %rax
	movq	%rbx, -360(%rbp)
	movq	-8(%rax,%rbx), %rax
	leaq	-288(%rbp), %rbx
	movq	272(%rax), %rax
	subq	%r12, %rax
	movq	%rax, -384(%rbp)
	movq	%rax, 272(%r13)
	movq	(%r15), %rax
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	movq	%rbx, -320(%rbp)
	testb	%al, %al
	jne	.L2505
.L2436:
	movdqa	-256(%rbp), %xmm5
	movdqa	-240(%rbp), %xmm6
	movl	-224(%rbp), %eax
	movq	-256(%rbp), %rdi
	movaps	%xmm5, -160(%rbp)
	movl	%eax, -128(%rbp)
	movaps	%xmm6, -144(%rbp)
	testl	%r14d, %r14d
	jle	.L2437
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L2438:
	cmpb	$2, 1(%rdi)
	je	.L2506
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	%rax, %rbx
.L2440:
	movl	-264(%rbp), %eax
	movq	-280(%rbp), %rdx
	subl	$8, %eax
	movl	%eax, -264(%rbp)
	movq	%rbx, 320(%rdx,%rax)
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2442
	movq	%rbx, -112(%rbp)
	movq	(%rax), %rax
	movl	-264(%rbp), %edx
	movq	-280(%rbp), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	movq	%rdx, %rcx
	addq	272(%rsi), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-112(%rbp), %rsi
	testb	$1, %sil
	jne	.L2443
	movq	%rsi, %rdx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	sarq	$32, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L2444:
	movq	-272(%rbp), %rax
	leaq	.LC98(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2442
	movq	(%rax), %rax
	movl	-224(%rbp), %edx
	leaq	.LC90(%rip), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2442:
	movq	-288(%rbp), %rcx
	movq	-256(%rbp), %rdi
	movq	-240(%rbp), %rdx
	movl	-224(%rbp), %eax
	movq	(%rcx), %rsi
	cmpq	304(%rsi), %rbx
	je	.L2507
.L2446:
	addl	$1, %eax
	addl	$1, %r12d
	movl	%eax, -224(%rbp)
	movl	$1, %eax
	jmp	.L2449
	.p2align 4,,10
	.p2align 3
.L2503:
	testl	%eax, %eax
	jle	.L2508
.L2449:
	subl	$1, %eax
	cmpb	$8, (%rdi)
	jne	.L2451
	addl	28(%rdi), %eax
.L2451:
	addq	$32, %rdi
	movq	%rdi, -256(%rbp)
	cmpq	%rdx, %rdi
	jne	.L2503
	movq	-232(%rbp), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, -232(%rbp)
	movq	8(%rdx), %rdi
	leaq	512(%rdi), %rdx
	movq	%rdi, -248(%rbp)
	movq	%rdx, -240(%rbp)
	movq	%rdi, -256(%rbp)
	testl	%eax, %eax
	jg	.L2449
.L2508:
	cmpl	%r14d, %r12d
	jne	.L2438
.L2437:
	movq	-360(%rbp), %rbx
	movq	72(%r15), %rax
	movq	-280(%rbp), %rdi
	movq	-8(%rax,%rbx), %rax
	movq	280(%rax), %rbx
	movl	-264(%rbp), %eax
	movq	%rbx, %rdx
	leal	-8(%rax), %esi
	movl	%esi, -264(%rbp)
	call	_ZN2v88internal16FrameDescription11SetCallerPcEjl@PLT
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2454
	movq	(%rax), %rax
	movl	-264(%rbp), %edx
	movq	%rbx, %r8
	leaq	.LC102(%rip), %r9
	movq	-280(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2454:
	movq	-360(%rbp), %rbx
	movq	72(%r15), %rax
	movq	-280(%rbp), %rdi
	movq	-8(%rax,%rbx), %rax
	movq	288(%rax), %rbx
	movl	-264(%rbp), %eax
	movq	%rbx, %rdx
	leal	-8(%rax), %esi
	movl	%esi, -264(%rbp)
	call	_ZN2v88internal16FrameDescription11SetCallerFpEjl@PLT
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2455
	movq	(%rax), %rax
	movl	-264(%rbp), %edx
	movq	%rbx, %r8
	leaq	.LC103(%rip), %r9
	movq	-280(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2455:
	movl	-264(%rbp), %ebx
	movl	-308(%rbp), %esi
	movq	%rbx, %rax
	addq	-384(%rbp), %rbx
	movq	%rbx, 288(%r13)
	cmpl	%esi, -336(%rbp)
	je	.L2509
.L2456:
	movq	-280(%rbp), %rdx
	subl	$8, %eax
	movl	%eax, -264(%rbp)
	movq	$36, 320(%rdx,%rax)
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2457
	movq	(%rax), %rax
	movl	-264(%rbp), %edx
	leaq	.LC129(%rip), %r9
	movl	$36, %r8d
	movq	-280(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2457:
	movl	-224(%rbp), %eax
	movdqa	-256(%rbp), %xmm7
	movdqa	-240(%rbp), %xmm1
	movq	-392(%rbp), %rdi
	movl	%eax, -80(%rbp)
	addl	$1, %eax
	movl	%eax, -224(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	movq	-320(%rbp), %rbx
	movq	-352(%rbp), %rsi
	leaq	.LC130(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc
	leal	-1(%r14), %esi
	leaq	.LC122(%rip), %rdx
	movq	%rbx, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	leaq	-208(%rbp), %rsi
	leaq	.LC131(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc
	movq	-376(%rbp), %rax
	leaq	.LC97(%rip), %rdx
	movq	%rbx, %rdi
	movq	96(%rax), %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	cmpl	$1, -312(%rbp)
	leaq	.LC125(%rip), %rdx
	jne	.L2510
.L2458:
	movq	-320(%rbp), %rdi
	leaq	-160(%rbp), %rsi
	call	_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc
	movl	-308(%rbp), %ebx
	cmpl	%ebx, -336(%rbp)
	je	.L2511
.L2460:
	movq	-368(%rbp), %rax
	movq	-256(%rbp), %rbx
	cmpq	%rbx, 88(%rax)
	jne	.L2512
	movl	-264(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L2513
	movq	-296(%rbp), %rdx
	leaq	63(%rdx), %rcx
	movl	43(%rdx), %edx
	testl	%edx, %edx
	js	.L2514
.L2465:
	cmpl	$1, -312(%rbp)
	movq	(%r15), %rax
	je	.L2515
	movslq	4844(%rax), %rax
.L2467:
	addq	%rcx, %rax
	movl	-308(%rbp), %ebx
	movq	%rax, 280(%r13)
	cmpl	%ebx, -336(%rbp)
	je	.L2516
.L2430:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2517
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2506:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movq	(%rax), %rbx
	jmp	.L2440
	.p2align 4,,10
	.p2align 3
.L2507:
	movq	-280(%rbp), %rsi
	movl	-264(%rbp), %r8d
	addq	272(%rsi), %r8
	movq	%rdx, -88(%rbp)
	movq	-248(%rbp), %rsi
	movq	-232(%rbp), %rdx
	movq	%r8, -112(%rbp)
	movq	%rdi, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	%eax, -72(%rbp)
	movq	288(%rcx), %rsi
	cmpq	296(%rcx), %rsi
	je	.L2447
	movq	%r8, (%rsi)
	movq	-104(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 32(%rsi)
	movl	-72(%rbp), %eax
	movl	%eax, 40(%rsi)
	addq	$48, 288(%rcx)
.L2448:
	movl	-224(%rbp), %eax
	movq	-256(%rbp), %rdi
	movq	-240(%rbp), %rdx
	jmp	.L2446
	.p2align 4,,10
	.p2align 3
.L2443:
	movq	-272(%rbp), %rax
	movq	-352(%rbp), %rdi
	movq	(%rax), %rax
	movq	144(%rax), %rsi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	jmp	.L2444
	.p2align 4,,10
	.p2align 3
.L2505:
	movq	-376(%rbp), %rax
	leaq	.LC97(%rip), %rdx
	movq	%rbx, %rdi
	movq	96(%rax), %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	jmp	.L2436
	.p2align 4,,10
	.p2align 3
.L2447:
	movq	-352(%rbp), %rdx
	leaq	280(%rcx), %rdi
	call	_ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2448
	.p2align 4,,10
	.p2align 3
.L2515:
	movslq	4836(%rax), %rax
	jmp	.L2467
	.p2align 4,,10
	.p2align 3
.L2514:
	leaq	-296(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rcx
	jmp	.L2465
	.p2align 4,,10
	.p2align 3
.L2509:
	call	_ZN2v88internal15JavaScriptFrame11fp_registerEv@PLT
	movl	%eax, %eax
	movq	%rbx, 16(%r13,%rax,8)
	movl	-264(%rbp), %eax
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L2511:
	movq	56(%r15), %rax
	movq	-280(%rbp), %rdx
	movq	16(%rax), %r8
	movl	-264(%rbp), %eax
	subl	$8, %eax
	movl	%eax, -264(%rbp)
	movq	%r8, 320(%rdx,%rax)
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2460
	movq	(%rax), %rax
	movl	-264(%rbp), %edx
	leaq	.LC133(%rip), %r9
	movq	-280(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L2460
	.p2align 4,,10
	.p2align 3
.L2516:
	call	_ZN2v88internal15JavaScriptFrame16context_registerEv@PLT
	movq	(%r15), %rdi
	movl	$70, %esi
	movl	%eax, %eax
	movq	$0, 16(%r13,%rax,8)
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, -112(%rbp)
	leaq	63(%rax), %rdx
	movl	43(%rax), %eax
	testl	%eax, %eax
	js	.L2518
.L2470:
	movq	%rdx, 312(%r13)
	jmp	.L2430
	.p2align 4,,10
	.p2align 3
.L2473:
	xorl	%edx, %edx
	jmp	.L2434
	.p2align 4,,10
	.p2align 3
.L2504:
	cmpb	$2, 28(%rdi)
	je	.L2431
	leaq	.LC127(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2510:
	cmpl	$2, -312(%rbp)
	leaq	.LC126(%rip), %rdx
	je	.L2458
	leaq	.LC132(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2512:
	leaq	.LC115(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2513:
	leaq	.LC116(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2518:
	movq	-352(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rdx
	jmp	.L2470
.L2517:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25843:
	.size	_ZN2v88internal11Deoptimizer27DoComputeConstructStubFrameEPNS0_15TranslatedFrameEi, .-_ZN2v88internal11Deoptimizer27DoComputeConstructStubFrameEPNS0_15TranslatedFrameEi
	.section	.rodata._ZN2v88internal11Deoptimizer25DoComputeInterpretedFrameEPNS0_15TranslatedFrameEib.str1.1,"aMS",@progbits,1
.LC134:
	.string	" (throw)"
.LC135:
	.string	"!raw_shared_info_.is_null()"
	.section	.rodata._ZN2v88internal11Deoptimizer25DoComputeInterpretedFrameEPNS0_15TranslatedFrameEib.str1.8,"aMS",@progbits,1
	.align 8
.LC136:
	.string	"  translating interpreted frame "
	.align 8
.LC137:
	.string	" => bytecode_offset=%d, variable_frame_size=%d, frame_size=%d%s\n"
	.align 8
.LC138:
	.string	"frame_index >= 0 && frame_index < output_count_"
	.align 8
.LC139:
	.string	"    -------------------------\n"
	.section	.rodata._ZN2v88internal11Deoptimizer25DoComputeInterpretedFrameEPNS0_15TranslatedFrameEib.str1.1
.LC140:
	.string	"function"
.LC141:
	.string	"bytecode array\n"
.LC142:
	.string	"bytecode offset\n"
.LC143:
	.string	"return value 0\n"
	.section	.rodata._ZN2v88internal11Deoptimizer25DoComputeInterpretedFrameEPNS0_15TranslatedFrameEib.str1.8
	.align 8
.LC144:
	.string	"return_value_first_reg + return_value_count <= locals_count"
	.section	.rodata._ZN2v88internal11Deoptimizer25DoComputeInterpretedFrameEPNS0_15TranslatedFrameEib.str1.1
.LC145:
	.string	"return_index == 1"
.LC146:
	.string	"return value 1\n"
.LC147:
	.string	"accumulator\n"
.LC148:
	.string	"accumulator"
	.section	.rodata._ZN2v88internal11Deoptimizer25DoComputeInterpretedFrameEPNS0_15TranslatedFrameEib.str1.8
	.align 8
.LC149:
	.string	"translated_frame->return_value_count() == 1"
	.section	.text._ZN2v88internal11Deoptimizer25DoComputeInterpretedFrameEPNS0_15TranslatedFrameEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer25DoComputeInterpretedFrameEPNS0_15TranslatedFrameEib
	.type	_ZN2v88internal11Deoptimizer25DoComputeInterpretedFrameEPNS0_15TranslatedFrameEib, @function
_ZN2v88internal11Deoptimizer25DoComputeInterpretedFrameEPNS0_15TranslatedFrameEib:
.LFB25841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -368(%rbp)
	movl	%edx, -340(%rbp)
	movl	%ecx, -344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L2669
	movl	64(%rdi), %ecx
	movdqu	72(%rsi), %xmm3
	movq	%rdi, %r13
	movq	%rax, -304(%rbp)
	movdqu	56(%rsi), %xmm4
	movl	4(%rsi), %r12d
	movl	$0, -224(%rbp)
	subl	$1, %ecx
	cmpl	-340(%rbp), %ecx
	movaps	%xmm3, -320(%rbp)
	sete	-376(%rbp)
	cmpb	$0, -344(%rbp)
	movaps	%xmm4, -336(%rbp)
	movl	%ecx, -372(%rbp)
	movl	%r12d, -336(%rbp)
	movaps	%xmm4, -256(%rbp)
	movaps	%xmm3, -240(%rbp)
	je	.L2521
	movl	52(%rdi), %ebx
	movl	%ebx, -336(%rbp)
.L2521:
	movzwl	41(%rax), %ebx
	movzbl	-376(%rbp), %ecx
	xorl	%r8d, %r8d
	leal	1(%rbx), %eax
	movw	%ax, -424(%rbp)
	movzwl	%ax, %r14d
	movq	-368(%rbp), %rax
	movl	%r14d, %esi
	movl	24(%rax), %edx
	leaq	-112(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -360(%rbp)
	movl	%edx, -320(%rbp)
	call	_ZN2v88internal20InterpretedFrameInfoC1EiibNS0_13FrameInfoKindE@PLT
	movl	-112(%rbp), %ebx
	movl	-224(%rbp), %eax
	leaq	-256(%rbp), %rsi
	movdqa	-256(%rbp), %xmm7
	movdqa	-240(%rbp), %xmm3
	movq	%rsi, %rdi
	movq	%rsi, -384(%rbp)
	movl	%ebx, -404(%rbp)
	movl	-108(%rbp), %ebx
	movl	%eax, -176(%rbp)
	addl	$1, %eax
	movl	%ebx, -352(%rbp)
	movl	-104(%rbp), %ebx
	movl	%eax, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	movq	304(%r13), %rax
	testq	%rax, %rax
	je	.L2522
	movq	(%rax), %rax
	leaq	.LC136(%rip), %rsi
	leaq	-160(%rbp), %r15
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	leaq	-304(%rbp), %rdi
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	-360(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	304(%r13), %rax
	movq	-160(%rbp), %rdx
	leaq	.LC13(%rip), %rsi
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	304(%r13), %rax
	movl	%ebx, %r8d
	cmpb	$0, -344(%rbp)
	leaq	.LC134(%rip), %r9
	movl	-352(%rbp), %ecx
	movl	%r12d, %edx
	leaq	.LC137(%rip), %rsi
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	leaq	.LC92(%rip), %rax
	cmove	%rax, %r9
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2522
	call	_ZdaPv@PLT
.L2522:
	movl	%ebx, %r12d
	leaq	320(%r12), %rdi
	call	malloc@PLT
	movdqa	.LC19(%rip), %xmm0
	movl	$16, %ecx
	movq	%r12, (%rax)
	movq	%rax, %rsi
	leaq	144(%rax), %rdi
	movl	%r14d, 8(%rax)
	movq	%rax, -352(%rbp)
	xorl	%eax, %eax
	rep stosq
	movl	$3203260077, %eax
	movups	%xmm0, 272(%rsi)
	movq	%rax, 304(%rsi)
	movups	%xmm0, 288(%rsi)
	movups	%xmm0, 16(%rsi)
	movups	%xmm0, 32(%rsi)
	movups	%xmm0, 48(%rsi)
	movups	%xmm0, 64(%rsi)
	movups	%xmm0, 80(%rsi)
	movups	%xmm0, 96(%rsi)
	movups	%xmm0, 112(%rsi)
	movups	%xmm0, 128(%rsi)
	testl	%ebx, %ebx
	je	.L2612
	leaq	320(%rsi), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2526:
	movl	%eax, %edx
	movl	$3203260077, %esi
	addl	$8, %eax
	movq	%rsi, (%rdx,%rcx)
	cmpl	%eax, %ebx
	ja	.L2526
	movq	-352(%rbp), %rax
	movl	(%rax), %eax
.L2525:
	movl	%eax, -264(%rbp)
	movq	304(%r13), %rdx
	movq	%r13, %xmm0
	movslq	-340(%rbp), %rax
	movhps	-352(%rbp), %xmm0
	movq	%rdx, -272(%rbp)
	movaps	%xmm0, -288(%rbp)
	testl	%eax, %eax
	js	.L2527
	cmpl	%eax, 64(%r13)
	jle	.L2527
	salq	$3, %rax
	movq	%rax, -416(%rbp)
	addq	72(%r13), %rax
	cmpq	$0, (%rax)
	jne	.L2670
	movq	-352(%rbp), %rbx
	movl	-340(%rbp), %r11d
	movq	%rbx, (%rax)
	testl	%r11d, %r11d
	je	.L2671
	movq	-416(%rbp), %rbx
	movq	72(%r13), %rax
	movq	-8(%rax,%rbx), %rax
	movq	272(%rax), %rax
	subq	%r12, %rax
	movq	%rax, -400(%rbp)
	movq	%rax, %rbx
.L2531:
	movq	-352(%rbp), %rax
	movl	%r14d, %edi
	movq	%rbx, 272(%rax)
	movq	0(%r13), %rax
	leaq	-288(%rbp), %rbx
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	movq	%rbx, -392(%rbp)
	testb	%al, %al
	jne	.L2672
.L2532:
	xorl	%r12d, %r12d
	cmpw	$0, -424(%rbp)
	leaq	.LC4(%rip), %r15
	je	.L2546
	movq	%r13, -424(%rbp)
	movq	-384(%rbp), %r13
	jmp	.L2533
	.p2align 4,,10
	.p2align 3
.L2536:
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	%rax, %rbx
.L2537:
	movl	-264(%rbp), %eax
	movq	-280(%rbp), %rdx
	subl	$8, %eax
	movl	%eax, -264(%rbp)
	movq	%rbx, 320(%rdx,%rax)
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2539
	movq	%rbx, -112(%rbp)
	movq	(%rax), %rax
	movl	-264(%rbp), %edx
	movq	-280(%rbp), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	movq	%rdx, %rcx
	addq	272(%rsi), %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-112(%rbp), %rsi
	testb	$1, %sil
	jne	.L2540
	movq	%rsi, %rdx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	sarq	$32, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L2541:
	movq	-272(%rbp), %rax
	leaq	.LC98(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2539
	movq	(%rax), %rax
	movl	-224(%rbp), %edx
	leaq	.LC90(%rip), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2539:
	movq	-288(%rbp), %rdx
	movl	-224(%rbp), %eax
	movq	(%rdx), %rcx
	cmpq	304(%rcx), %rbx
	je	.L2673
.L2543:
	addl	$1, %eax
	addl	$1, %r12d
	movq	%r13, %rdi
	movl	%eax, -224(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	cmpl	%r12d, %r14d
	je	.L2674
.L2533:
	movq	-256(%rbp), %rdi
	cmpb	$2, 1(%rdi)
	jne	.L2536
	movq	16(%rdi), %rax
	movq	(%rax), %rbx
	jmp	.L2537
	.p2align 4,,10
	.p2align 3
.L2671:
	movq	80(%r13), %rax
	subq	%r12, %rax
	movq	%rax, -400(%rbp)
	movq	%rax, %rbx
	jmp	.L2531
	.p2align 4,,10
	.p2align 3
.L2673:
	movq	-280(%rbp), %rsi
	movdqa	-256(%rbp), %xmm1
	movdqa	-240(%rbp), %xmm2
	movl	-264(%rbp), %ecx
	addq	272(%rsi), %rcx
	movl	%eax, -72(%rbp)
	movq	%rcx, -112(%rbp)
	movups	%xmm1, -104(%rbp)
	movups	%xmm2, -88(%rbp)
	movq	288(%rdx), %rsi
	cmpq	296(%rdx), %rsi
	je	.L2544
	movq	%rcx, (%rsi)
	movq	-104(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 32(%rsi)
	movl	-72(%rbp), %eax
	movl	%eax, 40(%rsi)
	addq	$48, 288(%rdx)
.L2545:
	movl	-224(%rbp), %eax
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2540:
	movq	-272(%rbp), %rax
	movq	-360(%rbp), %rdi
	movq	(%rax), %rax
	movq	144(%rax), %rsi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	jmp	.L2541
	.p2align 4,,10
	.p2align 3
.L2674:
	movq	-424(%rbp), %r13
.L2546:
	movq	304(%r13), %rax
	testq	%rax, %rax
	je	.L2535
	movq	(%rax), %rax
	leaq	.LC139(%rip), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2535:
	movl	-340(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L2547
	movq	96(%r13), %rbx
.L2548:
	movl	-264(%rbp), %eax
	movq	-280(%rbp), %rdi
	movq	%rbx, %rdx
	leal	-8(%rax), %esi
	movl	%esi, -264(%rbp)
	call	_ZN2v88internal16FrameDescription11SetCallerPcEjl@PLT
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2549
	movq	(%rax), %rax
	movl	-264(%rbp), %edx
	movq	%rbx, %r8
	leaq	.LC102(%rip), %r9
	movq	-280(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2549:
	movl	-340(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L2550
	movq	88(%r13), %rbx
.L2551:
	movl	-264(%rbp), %eax
	movq	-280(%rbp), %rdi
	movq	%rbx, %rdx
	leal	-8(%rax), %esi
	movl	%esi, -264(%rbp)
	call	_ZN2v88internal16FrameDescription11SetCallerFpEjl@PLT
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2552
	movq	(%rax), %rax
	movl	-264(%rbp), %edx
	movq	%rbx, %r8
	leaq	.LC103(%rip), %r9
	movq	-280(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2552:
	movq	-352(%rbp), %rax
	movl	-264(%rbp), %ebx
	addq	-400(%rbp), %rbx
	movl	-340(%rbp), %ecx
	movq	%rbx, 288(%rax)
	cmpl	%ecx, -372(%rbp)
	je	.L2675
.L2553:
	movl	-224(%rbp), %eax
	movdqa	-256(%rbp), %xmm4
	movdqa	-240(%rbp), %xmm5
	movq	-384(%rbp), %rdi
	movl	%eax, -128(%rbp)
	addl	$1, %eax
	movl	%eax, -224(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	cmpb	$0, -344(%rbp)
	je	.L2667
	movl	48(%r13), %r8d
	xorl	%esi, %esi
	leaq	-160(%rbp), %r15
	testl	%r8d, %r8d
	js	.L2667
	.p2align 4,,10
	.p2align 3
.L2558:
	movq	%r15, %rdi
	addl	$1, -128(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	addl	$1, %esi
	cmpl	%esi, 48(%r13)
	jge	.L2558
	movq	-160(%rbp), %rdi
	cmpb	$2, 1(%rdi)
	je	.L2676
.L2659:
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
.L2559:
	movq	-352(%rbp), %rbx
	leaq	.LC130(%rip), %rdx
	movq	%r15, %rsi
	leaq	-304(%rbp), %r12
	movq	%rax, 296(%rbx)
	movq	-392(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc
	movq	%rbx, %rdi
	leaq	-208(%rbp), %rsi
	leaq	.LC140(%rip), %rdx
	call	_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc
	movq	%r12, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo12HasBreakInfoEv@PLT
	testb	%al, %al
	je	.L2560
	movq	-304(%rbp), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %rsi
.L2561:
	movq	-392(%rbp), %rbx
	leaq	.LC141(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	movl	-336(%rbp), %esi
	leaq	.LC142(%rip), %rdx
	movq	%rbx, %rdi
	addl	$53, %esi
	salq	$32, %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	movq	304(%r13), %rax
	testq	%rax, %rax
	je	.L2562
	movq	(%rax), %rax
	leaq	.LC139(%rip), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2562:
	movq	-368(%rbp), %rax
	movl	-320(%rbp), %ecx
	movl	%ecx, %ebx
	subl	28(%rax), %ebx
	movl	32(%rax), %eax
	movl	%ebx, -336(%rbp)
	testl	%ecx, %ecx
	jle	.L2563
	movzbl	-344(%rbp), %r14d
	addl	%ebx, %eax
	xorl	%r12d, %r12d
	leaq	.LC4(%rip), %r15
	xorl	$1, %r14d
	andb	-376(%rbp), %r14b
	movl	%eax, -376(%rbp)
	.p2align 4,,10
	.p2align 3
.L2564:
	testb	%r14b, %r14b
	je	.L2565
	cmpb	$2, 28(%r13)
	jne	.L2565
	cmpl	-336(%rbp), %r12d
	jge	.L2677
.L2565:
	movq	-256(%rbp), %rdi
	cmpb	$2, 1(%rdi)
	je	.L2678
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	%rax, %rbx
.L2573:
	movl	-264(%rbp), %eax
	movq	-280(%rbp), %rdx
	subl	$8, %eax
	movl	%eax, -264(%rbp)
	movq	%rbx, 320(%rdx,%rax)
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2575
	movq	%rbx, -112(%rbp)
	movq	(%rax), %rax
	movl	-264(%rbp), %edx
	movq	-280(%rbp), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	movq	%rdx, %rcx
	addq	272(%rsi), %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-112(%rbp), %rsi
	testb	$1, %sil
	jne	.L2576
	movq	%rsi, %rdx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	sarq	$32, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L2577:
	movq	-272(%rbp), %rax
	leaq	.LC98(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2575
	movq	(%rax), %rax
	movl	-224(%rbp), %edx
	leaq	.LC90(%rip), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2575:
	movq	-288(%rbp), %rdi
	movq	-256(%rbp), %rax
	movq	-240(%rbp), %rcx
	movl	-224(%rbp), %edx
	movq	(%rdi), %rsi
	cmpq	304(%rsi), %rbx
	je	.L2679
.L2569:
	addl	$1, %edx
	addl	$1, %r12d
	movl	%edx, -224(%rbp)
	movl	$1, %edx
	jmp	.L2581
	.p2align 4,,10
	.p2align 3
.L2668:
	testl	%edx, %edx
	jle	.L2680
.L2581:
	subl	$1, %edx
	cmpb	$8, (%rax)
	jne	.L2583
	addl	28(%rax), %edx
.L2583:
	addq	$32, %rax
	movq	%rax, -256(%rbp)
	cmpq	%rcx, %rax
	jne	.L2668
	movq	-232(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, -232(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rcx
	movq	%rax, -248(%rbp)
	movq	%rcx, -240(%rbp)
	movq	%rax, -256(%rbp)
	testl	%edx, %edx
	jg	.L2581
.L2680:
	cmpl	-320(%rbp), %r12d
	jne	.L2564
.L2563:
	movl	-320(%rbp), %r12d
	leaq	.LC4(%rip), %rbx
	cmpl	-404(%rbp), %r12d
	jnb	.L2592
	movq	-432(%rbp), %r14
	movl	-404(%rbp), %r15d
	jmp	.L2586
	.p2align 4,,10
	.p2align 3
.L2681:
	movq	%rsi, %rdx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	sarq	$32, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L2591:
	movq	-272(%rbp), %rax
	leaq	.LC97(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2589:
	cmpl	%r15d, %r12d
	je	.L2592
.L2586:
	movl	-264(%rbp), %eax
	movq	96(%r14), %rdx
	addl	$1, %r12d
	movq	-280(%rbp), %rcx
	subl	$8, %eax
	movl	%eax, -264(%rbp)
	movq	%rdx, 320(%rcx,%rax)
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2589
	movq	%rdx, -112(%rbp)
	movq	(%rax), %rax
	movq	-280(%rbp), %rsi
	movl	-264(%rbp), %ecx
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	movl	%ecx, %edx
	addq	272(%rsi), %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-112(%rbp), %rsi
	testb	$1, %sil
	je	.L2681
	movq	-272(%rbp), %rax
	movq	-360(%rbp), %rdi
	movq	(%rax), %rax
	movq	144(%rax), %rsi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	jmp	.L2591
	.p2align 4,,10
	.p2align 3
.L2678:
	movq	16(%rdi), %rax
	movq	(%rax), %rbx
	jmp	.L2573
	.p2align 4,,10
	.p2align 3
.L2679:
	movq	-280(%rbp), %rsi
	movl	-264(%rbp), %r8d
	addq	272(%rsi), %r8
	movq	%rax, -104(%rbp)
	movq	-248(%rbp), %rax
	movq	%r8, -112(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	-232(%rbp), %rax
	movl	%edx, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	288(%rdi), %rsi
	cmpq	296(%rdi), %rsi
	je	.L2579
	movq	%r8, (%rsi)
	movq	-104(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 32(%rsi)
	movl	-72(%rbp), %eax
	movl	%eax, 40(%rsi)
	addq	$48, 288(%rdi)
.L2580:
	movl	-224(%rbp), %edx
	movq	-256(%rbp), %rax
	movq	-240(%rbp), %rcx
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2576:
	movq	-272(%rbp), %rax
	movq	-360(%rbp), %rdi
	movq	(%rax), %rax
	movq	144(%rax), %rsi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	jmp	.L2577
	.p2align 4,,10
	.p2align 3
.L2677:
	cmpl	%r12d, -376(%rbp)
	jle	.L2565
	movl	%r12d, %eax
	subl	-336(%rbp), %eax
	je	.L2682
	cmpl	$1, %eax
	jne	.L2683
	movq	56(%r13), %rax
	movq	-280(%rbp), %rdx
	movq	32(%rax), %r8
	movl	-264(%rbp), %eax
	subl	$8, %eax
	movl	%eax, -264(%rbp)
	movq	%r8, 320(%rdx,%rax)
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2580
	movq	(%rax), %rax
	movl	-264(%rbp), %edx
	leaq	.LC146(%rip), %r9
	movq	-280(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movl	-224(%rbp), %edx
	movq	-256(%rbp), %rax
	movq	-240(%rbp), %rcx
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2592:
	movl	-340(%rbp), %ebx
	cmpl	%ebx, -372(%rbp)
	jne	.L2661
	cmpb	$0, -344(%rbp)
	jne	.L2684
	cmpb	$2, 28(%r13)
	jne	.L2595
	movq	-368(%rbp), %rax
	movl	28(%rax), %edi
	testl	%edi, %edi
	jne	.L2595
	movl	32(%rax), %eax
	testl	%eax, %eax
	jle	.L2595
	cmpl	$1, %eax
	jne	.L2685
	movq	56(%r13), %rax
	movq	-392(%rbp), %rdi
	leaq	.LC143(%rip), %rdx
	movq	16(%rax), %rsi
	call	_ZN2v88internal11FrameWriter12PushRawValueElPKc
	.p2align 4,,10
	.p2align 3
.L2661:
	movq	-384(%rbp), %rdi
	addl	$1, -224(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	movq	-368(%rbp), %rax
	movq	-256(%rbp), %rbx
	cmpq	%rbx, 88(%rax)
	jne	.L2686
	movl	-264(%rbp), %esi
	testl	%esi, %esi
	jne	.L2687
	movq	0(%r13), %rax
	movl	-340(%rbp), %ebx
	leaq	41184(%rax), %r12
	cmpl	%ebx, -372(%rbp)
	je	.L2688
.L2601:
	cmpb	$0, -344(%rbp)
	je	.L2689
.L2602:
	movl	$65, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, -296(%rbp)
.L2603:
	movq	-296(%rbp), %rax
	movl	43(%rax), %ecx
	leaq	63(%rax), %rdx
	testl	%ecx, %ecx
	js	.L2690
.L2605:
	movq	-352(%rbp), %rax
	movl	-340(%rbp), %ebx
	movq	%rdx, 280(%rax)
	cmpl	%ebx, -372(%rbp)
	je	.L2691
.L2519:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2692
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2579:
	.cfi_restore_state
	movq	-360(%rbp), %rdx
	addq	$280, %rdi
	call	_ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2667:
	movq	-160(%rbp), %rdi
	leaq	-160(%rbp), %r15
	cmpb	$2, 1(%rdi)
	jne	.L2659
.L2676:
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	jmp	.L2559
	.p2align 4,,10
	.p2align 3
.L2550:
	movq	-416(%rbp), %rbx
	movq	72(%r13), %rax
	movq	-8(%rax,%rbx), %rax
	movq	288(%rax), %rbx
	jmp	.L2551
	.p2align 4,,10
	.p2align 3
.L2547:
	movq	-416(%rbp), %rbx
	movq	72(%r13), %rax
	movq	-8(%rax,%rbx), %rax
	movq	280(%rax), %rbx
	jmp	.L2548
	.p2align 4,,10
	.p2align 3
.L2682:
	movq	56(%r13), %rax
	movq	-280(%rbp), %rdx
	movq	16(%rax), %r8
	movl	-264(%rbp), %eax
	subl	$8, %eax
	movl	%eax, -264(%rbp)
	movq	%r8, 320(%rdx,%rax)
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L2567
	movq	(%rax), %rax
	movl	-264(%rbp), %edx
	leaq	.LC143(%rip), %r9
	movq	-280(%rbp), %rsi
	movq	144(%rax), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	addq	272(%rsi), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2567:
	movl	-320(%rbp), %ebx
	cmpl	%ebx, -376(%rbp)
	jle	.L2580
	leaq	.LC144(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2560:
	movq	%r12, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv
	movq	%rax, %rsi
	jmp	.L2561
	.p2align 4,,10
	.p2align 3
.L2672:
	movq	-432(%rbp), %rax
	leaq	.LC97(%rip), %rdx
	movq	%rbx, %rdi
	movq	96(%rax), %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	jmp	.L2532
.L2684:
	movq	56(%r13), %rax
	movq	-392(%rbp), %rdi
	leaq	.LC147(%rip), %rdx
	movq	16(%rax), %rsi
	call	_ZN2v88internal11FrameWriter13PushRawObjectENS0_6ObjectEPKc
	jmp	.L2661
	.p2align 4,,10
	.p2align 3
.L2595:
	movq	-384(%rbp), %rsi
	movq	-392(%rbp), %rdi
	leaq	.LC148(%rip), %rdx
	call	_ZN2v88internal11FrameWriter19PushTranslatedValueERKNS0_15TranslatedFrame8iteratorEPKc
	jmp	.L2661
	.p2align 4,,10
	.p2align 3
.L2544:
	leaq	280(%rdx), %rdi
	movq	-360(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal11Deoptimizer18ValueToMaterializeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2545
	.p2align 4,,10
	.p2align 3
.L2527:
	leaq	.LC138(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2688:
	cmpb	$2, 28(%r13)
	jne	.L2602
	jmp	.L2601
.L2675:
	call	_ZN2v88internal15JavaScriptFrame11fp_registerEv@PLT
	movq	-352(%rbp), %rcx
	movl	%eax, %eax
	movq	%rbx, 16(%rcx,%rax,8)
	jmp	.L2553
.L2689:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, -296(%rbp)
	jmp	.L2603
.L2690:
	leaq	-296(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rdx
	jmp	.L2605
.L2669:
	leaq	.LC135(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L2612:
	xorl	%eax, %eax
	jmp	.L2525
.L2691:
	call	_ZN2v88internal15JavaScriptFrame16context_registerEv@PLT
	movq	-352(%rbp), %rbx
	movl	$70, %esi
	movq	%r12, %rdi
	movl	%eax, %eax
	movq	$0, 16(%rbx,%rax,8)
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, -112(%rbp)
	leaq	63(%rax), %rdx
	movl	43(%rax), %eax
	testl	%eax, %eax
	js	.L2693
.L2608:
	movq	-352(%rbp), %rax
	movq	%rdx, 312(%rax)
	jmp	.L2519
.L2670:
	leaq	.LC119(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2683:
	leaq	.LC145(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2686:
	leaq	.LC115(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2687:
	leaq	.LC116(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2693:
	movq	-360(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rdx
	jmp	.L2608
.L2685:
	leaq	.LC149(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2692:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25841:
	.size	_ZN2v88internal11Deoptimizer25DoComputeInterpretedFrameEPNS0_15TranslatedFrameEib, .-_ZN2v88internal11Deoptimizer25DoComputeInterpretedFrameEPNS0_15TranslatedFrameEib
	.section	.text._ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPmSt6vectorImSaImEEEENS0_5__ops16_Iter_equals_valIKmEEET_SB_SB_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPmSt6vectorImSaImEEEENS0_5__ops16_Iter_equals_valIKmEEET_SB_SB_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPmSt6vectorImSaImEEEENS0_5__ops16_Iter_equals_valIKmEEET_SB_SB_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPmSt6vectorImSaImEEEENS0_5__ops16_Iter_equals_valIKmEEET_SB_SB_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPmSt6vectorImSaImEEEENS0_5__ops16_Iter_equals_valIKmEEET_SB_SB_T0_St26random_access_iterator_tag:
.LFB31361:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	subq	%rdi, %r8
	movq	%r8, %rax
	sarq	$5, %r8
	sarq	$3, %rax
	testq	%r8, %r8
	jle	.L2695
	salq	$5, %r8
	movq	(%rdx), %rcx
	addq	%rdi, %r8
	jmp	.L2701
	.p2align 4,,10
	.p2align 3
.L2696:
	cmpq	8(%rdi), %rcx
	je	.L2713
	cmpq	16(%rdi), %rcx
	je	.L2714
	cmpq	24(%rdi), %rcx
	je	.L2715
	addq	$32, %rdi
	cmpq	%r8, %rdi
	je	.L2716
.L2701:
	cmpq	(%rdi), %rcx
	jne	.L2696
.L2712:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2716:
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
.L2695:
	cmpq	$2, %rax
	je	.L2702
	cmpq	$3, %rax
	je	.L2703
	cmpq	$1, %rax
	je	.L2717
.L2705:
	movq	%rsi, %rax
	ret
.L2703:
	movq	(%rdx), %rax
	cmpq	%rax, (%rdi)
	je	.L2712
	addq	$8, %rdi
	jmp	.L2706
.L2717:
	movq	(%rdx), %rax
.L2707:
	cmpq	(%rdi), %rax
	jne	.L2705
	jmp	.L2712
.L2702:
	movq	(%rdx), %rax
.L2706:
	cmpq	%rax, (%rdi)
	je	.L2712
	addq	$8, %rdi
	jmp	.L2707
	.p2align 4,,10
	.p2align 3
.L2713:
	leaq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2714:
	leaq	16(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2715:
	leaq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE31361:
	.size	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPmSt6vectorImSaImEEEENS0_5__ops16_Iter_equals_valIKmEEET_SB_SB_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPmSt6vectorImSaImEEEENS0_5__ops16_Iter_equals_valIKmEEET_SB_SB_T0_St26random_access_iterator_tag
	.section	.rodata._ZN2v88internal23MaterializedObjectStore6RemoveEm.str1.1,"aMS",@progbits,1
.LC150:
	.string	"index < array.length()"
	.section	.text._ZN2v88internal23MaterializedObjectStore6RemoveEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23MaterializedObjectStore6RemoveEm
	.type	_ZN2v88internal23MaterializedObjectStore6RemoveEm, @function
_ZN2v88internal23MaterializedObjectStore6RemoveEm:
.LFB25942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-56(%rbp), %rdx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	16(%rdi), %rsi
	movq	8(%rdi), %rdi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPmSt6vectorImSaImEEEENS0_5__ops16_Iter_equals_valIKmEEET_SB_SB_T0_St26random_access_iterator_tag
	movq	16(%r15), %rdx
	cmpq	%rdx, %rax
	je	.L2732
	movq	%rax, %r13
	leaq	8(%rax), %rsi
	subq	8(%r15), %r13
	sarq	$3, %r13
	cmpq	%rsi, %rdx
	je	.L2720
	subq	%rsi, %rdx
	movq	%rax, %rdi
	call	memmove@PLT
	movq	16(%r15), %rsi
.L2720:
	movq	(%r15), %rax
	leaq	-8(%rsi), %r12
	movq	%r12, 16(%r15)
	movq	4688(%rax), %r14
	cmpl	%r13d, 11(%r14)
	jle	.L2747
	subq	8(%r15), %r12
	leaq	-1(%r14), %rdi
	movq	%r12, %rcx
	movq	%rdi, -88(%rbp)
	sarq	$3, %rcx
	movq	%rcx, -96(%rbp)
	cmpl	%ecx, %r13d
	jge	.L2722
	movl	%r13d, %eax
	leal	16(,%r13,8), %ebx
	movslq	%r13d, %r13
	notl	%eax
	movslq	%ebx, %rbx
	addl	%ecx, %eax
	addq	%rdi, %rbx
	addq	%rax, %r13
	movq	%r14, %rax
	andq	$-262144, %rax
	leaq	23(%r14,%r13,8), %r13
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L2726:
	movq	%rbx, %r12
	movq	8(%rbx), %rdx
	addq	$8, %rbx
	movq	%rdx, (%r12)
	testb	$1, %dl
	je	.L2731
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -64(%rbp)
	testl	$262144, %eax
	je	.L2724
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rdx
	movq	8(%r8), %rax
.L2724:
	testb	$24, %al
	je	.L2731
	movq	-80(%rbp), %rax
	testb	$24, 8(%rax)
	jne	.L2731
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2731:
	cmpq	%rbx, %r13
	jne	.L2726
	movq	(%r15), %rax
.L2722:
	movq	88(%rax), %r13
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %r15
	leal	16(,%rax,8), %esi
	movslq	%esi, %rsi
	addq	%rsi, %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L2730
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L2728
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L2728:
	testb	$24, %al
	je	.L2730
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2748
.L2730:
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2732:
	.cfi_restore_state
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2748:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2730
	.p2align 4,,10
	.p2align 3
.L2747:
	leaq	.LC150(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25942:
	.size	_ZN2v88internal23MaterializedObjectStore6RemoveEm, .-_ZN2v88internal23MaterializedObjectStore6RemoveEm
	.section	.text._ZN2v88internal23MaterializedObjectStore14StackIdToIndexEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23MaterializedObjectStore14StackIdToIndexEm
	.type	_ZN2v88internal23MaterializedObjectStore14StackIdToIndexEm, @function
_ZN2v88internal23MaterializedObjectStore14StackIdToIndexEm:
.LFB25943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-24(%rbp), %rdx
	subq	$24, %rsp
	movq	%rsi, -24(%rbp)
	movq	16(%rdi), %rsi
	movq	8(%rdi), %rdi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPmSt6vectorImSaImEEEENS0_5__ops16_Iter_equals_valIKmEEET_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 16(%rbx)
	je	.L2751
	subq	8(%rbx), %rax
	sarq	$3, %rax
.L2749:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2751:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L2749
	.cfi_endproc
.LFE25943:
	.size	_ZN2v88internal23MaterializedObjectStore14StackIdToIndexEm, .-_ZN2v88internal23MaterializedObjectStore14StackIdToIndexEm
	.section	.text._ZN2v88internal23MaterializedObjectStore3SetEmNS0_6HandleINS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23MaterializedObjectStore3SetEmNS0_6HandleINS0_10FixedArrayEEE
	.type	_ZN2v88internal23MaterializedObjectStore3SetEmNS0_6HandleINS0_10FixedArrayEEE, @function
_ZN2v88internal23MaterializedObjectStore3SetEmNS0_6HandleINS0_10FixedArrayEEE:
.LFB25941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	leaq	-48(%rbp), %rdx
	subq	$32, %rsp
	movq	%rsi, -56(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -48(%rbp)
	movq	16(%rdi), %rsi
	movq	8(%rdi), %rdi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPmSt6vectorImSaImEEEENS0_5__ops16_Iter_equals_valIKmEEET_SB_SB_T0_St26random_access_iterator_tag
	movq	16(%r12), %rsi
	movq	8(%r12), %rdx
	cmpq	%rax, %rsi
	je	.L2755
	subq	%rdx, %rax
	sarq	$3, %rax
	movl	%eax, %r13d
	cmpl	$-1, %eax
	je	.L2755
.L2756:
	leal	1(%r13), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal23MaterializedObjectStore18EnsureStackEntriesEi
	movq	(%rbx), %r12
	movq	(%rax), %r14
	leal	16(,%r13,8), %eax
	cltq
	leaq	-1(%r14,%rax), %r13
	movq	%r12, 0(%r13)
	testb	$1, %r12b
	je	.L2753
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2770
	testb	$24, %al
	je	.L2753
.L2773:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2771
	.p2align 4,,10
	.p2align 3
.L2753:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2772
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2770:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L2773
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2755:
	movq	%rsi, %r13
	subq	%rdx, %r13
	sarq	$3, %r13
	cmpq	24(%r12), %rsi
	je	.L2757
	movq	-56(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
	jmp	.L2756
	.p2align 4,,10
	.p2align 3
.L2771:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2757:
	leaq	-56(%rbp), %rdx
	leaq	8(%r12), %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L2756
.L2772:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25941:
	.size	_ZN2v88internal23MaterializedObjectStore3SetEmNS0_6HandleINS0_10FixedArrayEEE, .-_ZN2v88internal23MaterializedObjectStore3SetEmNS0_6HandleINS0_10FixedArrayEEE
	.section	.rodata._ZN2v88internal23MaterializedObjectStore3GetEm.str1.1,"aMS",@progbits,1
.LC151:
	.string	"array->length() > index"
	.section	.text._ZN2v88internal23MaterializedObjectStore3GetEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23MaterializedObjectStore3GetEm
	.type	_ZN2v88internal23MaterializedObjectStore3GetEm, @function
_ZN2v88internal23MaterializedObjectStore3GetEm:
.LFB25940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-48(%rbp), %rdx
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -48(%rbp)
	movq	16(%rdi), %rsi
	movq	8(%rdi), %rdi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPmSt6vectorImSaImEEEENS0_5__ops16_Iter_equals_valIKmEEET_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 16(%r12)
	je	.L2776
	subq	8(%r12), %rax
	sarq	$3, %rax
	movq	%rax, %rbx
	cmpl	$-1, %eax
	jne	.L2777
.L2776:
	xorl	%eax, %eax
.L2778:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2788
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2777:
	.cfi_restore_state
	movq	(%r12), %r13
	movq	41112(%r13), %rdi
	movq	4688(%r13), %rsi
	testq	%rdi, %rdi
	je	.L2779
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2780:
	cmpl	%ebx, 11(%rsi)
	jle	.L2789
	movq	(%r12), %r12
	leal	16(,%rbx,8), %eax
	cltq
	movq	-1(%rsi,%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2783
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2779:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L2790
.L2781:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L2780
	.p2align 4,,10
	.p2align 3
.L2783:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2791
.L2785:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2790:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L2781
	.p2align 4,,10
	.p2align 3
.L2791:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L2785
	.p2align 4,,10
	.p2align 3
.L2789:
	leaq	.LC151(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2788:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25940:
	.size	_ZN2v88internal23MaterializedObjectStore3GetEm, .-_ZN2v88internal23MaterializedObjectStore3GetEm
	.section	.rodata._ZN2v88internal15TranslatedState39UpdateFromPreviouslyMaterializedObjectsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC152:
	.string	"length == previously_materialized_objects->length()"
	.align 8
.LC153:
	.string	"value_info->IsMaterializedObject()"
	.section	.text._ZN2v88internal15TranslatedState39UpdateFromPreviouslyMaterializedObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState39UpdateFromPreviouslyMaterializedObjectsEv
	.type	_ZN2v88internal15TranslatedState39UpdateFromPreviouslyMaterializedObjectsEv, @function
_ZN2v88internal15TranslatedState39UpdateFromPreviouslyMaterializedObjectsEv:
.LFB26036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rax
	movq	32(%rdi), %rsi
	movq	41056(%rax), %rdi
	call	_ZN2v88internal23MaterializedObjectStore3GetEm
	testq	%rax, %rax
	je	.L2792
	movq	120(%r13), %rdx
	subq	88(%r13), %rdx
	movq	%rax, %r14
	sarq	$3, %rdx
	movq	96(%r13), %rax
	subq	104(%r13), %rax
	subq	$1, %rdx
	sarq	$3, %rax
	movq	(%r14), %rsi
	movq	24(%r13), %r15
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	80(%r13), %rax
	subq	64(%r13), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpl	%eax, 11(%rsi)
	jne	.L2816
	testl	%eax, %eax
	jle	.L2792
	leal	-1(%rax), %ecx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L2811:
	leaq	0(,%r12,8), %rdx
	movq	15(%rsi,%rdx), %rax
	leaq	16(%rdx), %rdi
	cmpq	%rax, 304(%r15)
	je	.L2796
	movq	64(%r13), %r8
	movq	%r8, %rax
	subq	72(%r13), %rax
	sarq	$3, %rax
	addq	%r12, %rax
	js	.L2797
	addq	%r8, %rdx
	cmpq	$63, %rax
	jle	.L2799
	movq	%rax, %rdx
	sarq	$6, %rdx
.L2800:
	movq	88(%r13), %r8
	movq	%rdx, %r9
	salq	$6, %r9
	movq	(%r8,%rdx,8), %rdx
	subq	%r9, %rax
	leaq	(%rdx,%rax,8), %rdx
.L2799:
	movslq	(%rdx), %r8
	movslq	4(%rdx), %rbx
	movq	%r8, %rax
	salq	$4, %rax
	subq	%r8, %rax
	movq	0(%r13), %r8
	leaq	(%r8,%rax,8), %r9
	movq	56(%r9), %r8
	movq	%r8, %rax
	subq	64(%r9), %rax
	sarq	$5, %rax
	addq	%rbx, %rax
	js	.L2801
	cmpq	$15, %rax
	jg	.L2802
	salq	$5, %rbx
	addq	%r8, %rbx
.L2803:
	movzbl	(%rbx), %eax
	movl	%eax, %edx
	subl	$8, %eax
	cmpl	$1, %eax
	jbe	.L2817
	leaq	.LC153(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2817:
	cmpb	$8, %dl
	jne	.L2796
	movq	24(%r13), %rdx
	movq	-1(%rdi,%rsi), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2808
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rcx
.L2809:
	movq	%rax, 16(%rbx)
	movb	$2, 1(%rbx)
	.p2align 4,,10
	.p2align 3
.L2796:
	leaq	1(%r12), %rax
	cmpq	%rcx, %r12
	je	.L2792
	movq	(%r14), %rsi
	movq	%rax, %r12
	jmp	.L2811
	.p2align 4,,10
	.p2align 3
.L2802:
	movq	%rax, %rdx
	sarq	$4, %rdx
.L2804:
	movq	80(%r9), %r8
	movq	%rdx, %r9
	salq	$4, %r9
	subq	%r9, %rax
	salq	$5, %rax
	addq	(%r8,%rdx,8), %rax
	movq	%rax, %rbx
	jmp	.L2803
	.p2align 4,,10
	.p2align 3
.L2801:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$4, %rdx
	notq	%rdx
	jmp	.L2804
	.p2align 4,,10
	.p2align 3
.L2797:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L2800
	.p2align 4,,10
	.p2align 3
.L2792:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2808:
	.cfi_restore_state
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L2818
.L2810:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L2809
	.p2align 4,,10
	.p2align 3
.L2816:
	leaq	.LC152(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2818:
	movq	%rdx, %rdi
	movq	%rcx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdx
	jmp	.L2810
	.cfi_endproc
.LFE26036:
	.size	_ZN2v88internal15TranslatedState39UpdateFromPreviouslyMaterializedObjectsEv, .-_ZN2v88internal15TranslatedState39UpdateFromPreviouslyMaterializedObjectsEv
	.section	.text._ZN2v88internal15TranslatedState7PrepareEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState7PrepareEm
	.type	_ZN2v88internal15TranslatedState7PrepareEm, @function
_ZN2v88internal15TranslatedState7PrepareEm:
.LFB26016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rbx
	movq	8(%rdi), %r12
	cmpq	%rbx, %r12
	je	.L2820
	.p2align 4,,10
	.p2align 3
.L2821:
	movq	%rbx, %rdi
	addq	$120, %rbx
	call	_ZN2v88internal15TranslatedFrame8HandlifyEv
	cmpq	%rbx, %r12
	jne	.L2821
.L2820:
	movq	136(%r13), %rsi
	testq	%rsi, %rsi
	je	.L2822
	movq	24(%r13), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2823
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2824:
	movq	%rax, 128(%r13)
	movq	$0, 136(%r13)
.L2822:
	movq	%r14, 32(%r13)
	addq	$16, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15TranslatedState39UpdateFromPreviouslyMaterializedObjectsEv
	.p2align 4,,10
	.p2align 3
.L2823:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2831
.L2825:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L2824
.L2831:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L2825
	.cfi_endproc
.LFE26016:
	.size	_ZN2v88internal15TranslatedState7PrepareEm, .-_ZN2v88internal15TranslatedState7PrepareEm
	.section	.rodata._ZN2v88internal15TranslatedState31StoreMaterializedValuesAndDeoptEPNS0_15JavaScriptFrameE.str1.8,"aMS",@progbits,1
	.align 8
.LC154:
	.string	"previously_materialized_objects->get(i) == *value"
	.align 8
.LC155:
	.string	"frames_[0].kind() == TranslatedFrame::kInterpretedFunction"
	.align 8
.LC156:
	.string	"frame->function() == frames_[0].front().GetRawValue()"
	.section	.text._ZN2v88internal15TranslatedState31StoreMaterializedValuesAndDeoptEPNS0_15JavaScriptFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState31StoreMaterializedValuesAndDeoptEPNS0_15JavaScriptFrameE
	.type	_ZN2v88internal15TranslatedState31StoreMaterializedValuesAndDeoptEPNS0_15JavaScriptFrameE, @function
_ZN2v88internal15TranslatedState31StoreMaterializedValuesAndDeoptEPNS0_15JavaScriptFrameE:
.LFB26035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -120(%rbp)
	movq	32(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	41056(%rax), %rax
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal23MaterializedObjectStore3GetEm
	movq	96(%r15), %rcx
	subq	104(%r15), %rcx
	movq	%rax, %r12
	movq	24(%r15), %rax
	sarq	$3, %rcx
	movq	%rax, -88(%rbp)
	addq	$304, %rax
	movq	%rax, -80(%rbp)
	movq	120(%r15), %rax
	subq	88(%r15), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$6, %rax
	addq	%rcx, %rax
	movq	80(%r15), %rcx
	subq	64(%r15), %rcx
	sarq	$3, %rcx
	leaq	(%rax,%rcx), %r13
	movl	%r13d, -68(%rbp)
	testq	%r12, %r12
	je	.L2892
	movq	(%r12), %rax
	cmpl	%r13d, 11(%rax)
	jne	.L2869
	movb	$0, -136(%rbp)
	testl	%r13d, %r13d
	jle	.L2832
.L2841:
	leaq	-64(%rbp), %rax
	movb	$0, -104(%rbp)
	xorl	%ebx, %ebx
	movq	%rax, -112(%rbp)
	movq	%r12, -96(%rbp)
	movq	%rbx, %r12
	movq	%r15, %rbx
	.p2align 4,,10
	.p2align 3
.L2862:
	movq	64(%rbx), %rsi
	movl	%r12d, %r15d
	movl	%r12d, %r13d
	movq	%rsi, %rax
	subq	72(%rbx), %rax
	sarq	$3, %rax
	addq	%r12, %rax
	js	.L2843
	cmpq	$63, %rax
	jg	.L2844
	leaq	(%rsi,%r12,8), %rsi
.L2845:
	movslq	(%rsi), %rdi
	movslq	4(%rsi), %rsi
	movq	%rdi, %rax
	salq	$4, %rax
	subq	%rdi, %rax
	movq	(%rbx), %rdi
	leaq	(%rdi,%rax,8), %rax
	movq	56(%rax), %r9
	movq	%r9, %rdi
	subq	64(%rax), %rdi
	sarq	$5, %rdi
	addq	%rsi, %rdi
	js	.L2847
	cmpq	$15, %rdi
	jg	.L2848
	salq	$5, %rsi
	leaq	(%r9,%rsi), %rdi
.L2849:
	movzbl	(%rdi), %eax
	subl	$8, %eax
	cmpl	$1, %eax
	jbe	.L2893
	leaq	.LC153(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2893:
	cmpl	%r12d, 24(%rdi)
	jne	.L2852
	cmpb	$2, 1(%rdi)
	movq	24(%rbx), %r14
	je	.L2894
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	%rax, %rsi
.L2855:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2856
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpq	-80(%rbp), %rax
	je	.L2852
	movq	-88(%rbp), %rdx
	movq	304(%rdx), %rdi
	testq	%rax, %rax
	je	.L2860
	movq	(%rax), %rsi
.L2859:
	cmpq	%rdi, %rsi
	je	.L2852
.L2860:
	movq	-96(%rbp), %rcx
	leal	16(,%r15,8), %edx
	movslq	%edx, %rdx
	movq	(%rcx), %rsi
	movq	-1(%rdx,%rsi), %r9
	cmpq	%rdi, %r9
	je	.L2895
	movq	-1(%rdx,%rsi), %rdx
	cmpq	%rdx, (%rax)
	jne	.L2896
	.p2align 4,,10
	.p2align 3
.L2852:
	addq	$1, %r12
	cmpl	%r12d, -68(%rbp)
	jg	.L2862
	cmpb	$0, -104(%rbp)
	movq	-96(%rbp), %r12
	je	.L2832
	cmpb	$0, -136(%rbp)
	je	.L2832
	movq	32(%rbx), %rsi
	movq	-128(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal23MaterializedObjectStore3SetEmNS0_6HandleINS0_10FixedArrayEEE
	movq	(%rbx), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L2897
	movq	56(%rax), %rdi
	cmpb	$2, 1(%rdi)
	je	.L2898
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	%rax, %rbx
.L2866:
	movq	-120(%rbp), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	cmpq	%rax, %rbx
	jne	.L2899
	movq	-120(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZNK2v88internal10StackFrame10LookupCodeEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	movq	(%rbx), %rax
	call	*152(%rax)
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE
.L2832:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2900
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2848:
	.cfi_restore_state
	movq	%rdi, %rsi
	sarq	$4, %rsi
.L2850:
	movq	%rsi, %r9
	movq	80(%rax), %rax
	salq	$4, %r9
	subq	%r9, %rdi
	salq	$5, %rdi
	addq	(%rax,%rsi,8), %rdi
	jmp	.L2849
	.p2align 4,,10
	.p2align 3
.L2844:
	movq	%rax, %rsi
	sarq	$6, %rsi
.L2846:
	movq	88(%rbx), %rdi
	movq	%rsi, %r9
	salq	$6, %r9
	movq	(%rdi,%rsi,8), %rsi
	subq	%r9, %rax
	leaq	(%rsi,%rax,8), %rsi
	jmp	.L2845
	.p2align 4,,10
	.p2align 3
.L2847:
	movq	%rdi, %rsi
	notq	%rsi
	shrq	$4, %rsi
	notq	%rsi
	jmp	.L2850
	.p2align 4,,10
	.p2align 3
.L2843:
	movq	%rax, %rsi
	notq	%rsi
	shrq	$6, %rsi
	notq	%rsi
	jmp	.L2846
	.p2align 4,,10
	.p2align 3
.L2856:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L2901
.L2858:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r14)
	movq	%rsi, (%rax)
	cmpq	-80(%rbp), %rax
	je	.L2852
	movq	-88(%rbp), %rdx
	movq	304(%rdx), %rdi
	jmp	.L2859
	.p2align 4,,10
	.p2align 3
.L2894:
	movq	16(%rdi), %rax
	movq	(%rax), %rsi
	jmp	.L2855
	.p2align 4,,10
	.p2align 3
.L2895:
	movq	%rsi, -64(%rbp)
	movq	-112(%rbp), %rdi
	movl	%r13d, %esi
	movq	(%rax), %rdx
	call	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	movb	$1, -104(%rbp)
	jmp	.L2852
	.p2align 4,,10
	.p2align 3
.L2901:
	movq	%r14, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	jmp	.L2858
	.p2align 4,,10
	.p2align 3
.L2892:
	movq	-88(%rbp), %r14
	movl	$1, %edx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testl	%r13d, %r13d
	jle	.L2834
	leal	-1(%r13), %eax
	movl	$16, %ebx
	movq	%r13, -136(%rbp)
	movq	%r12, %r13
	leaq	24(,%rax,8), %rax
	movq	%r15, -144(%rbp)
	movq	%rbx, %r15
	movq	%r14, %rbx
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L2839:
	movq	0(%r13), %rdi
	movq	304(%rbx), %r14
	leaq	-1(%rdi,%r15), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L2868
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -96(%rbp)
	testl	$262144, %edx
	je	.L2836
	movq	%r14, %rdx
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%rcx), %rdx
.L2836:
	andl	$24, %edx
	je	.L2868
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L2868
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2868:
	addq	$8, %r15
	cmpq	%r15, %r12
	jne	.L2839
	movq	%r13, %r12
	movq	-144(%rbp), %r15
	movq	-136(%rbp), %r13
	movq	(%r12), %rax
	cmpl	%r13d, 11(%rax)
	jne	.L2869
	movb	$1, -136(%rbp)
	jmp	.L2841
.L2834:
	movq	(%rax), %rax
	cmpl	%r13d, 11(%rax)
	je	.L2832
	.p2align 4,,10
	.p2align 3
.L2869:
	leaq	.LC152(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2896:
	leaq	.LC154(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2898:
	movq	16(%rdi), %rax
	movq	(%rax), %rbx
	jmp	.L2866
.L2897:
	leaq	.LC155(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2899:
	leaq	.LC156(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2900:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26035:
	.size	_ZN2v88internal15TranslatedState31StoreMaterializedValuesAndDeoptEPNS0_15JavaScriptFrameE, .-_ZN2v88internal15TranslatedState31StoreMaterializedValuesAndDeoptEPNS0_15JavaScriptFrameE
	.section	.text._ZNKSt15_Deque_iteratorIN2v88internal15TranslatedState14ObjectPositionERS3_PS3_EplEl,"axG",@progbits,_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedState14ObjectPositionERS3_PS3_EplEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedState14ObjectPositionERS3_PS3_EplEl
	.type	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedState14ObjectPositionERS3_PS3_EplEl, @function
_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedState14ObjectPositionERS3_PS3_EplEl:
.LFB31379:
	.cfi_startproc
	endbr64
	movq	(%rsi), %r9
	movq	%rdi, %rax
	movq	8(%rsi), %rdi
	movq	16(%rsi), %r8
	movq	24(%rsi), %rsi
	movq	%r9, %rcx
	subq	%rdi, %rcx
	sarq	$3, %rcx
	addq	%rdx, %rcx
	js	.L2903
	cmpq	$63, %rcx
	jle	.L2907
	movq	%rcx, %rdx
	sarq	$6, %rdx
.L2906:
	leaq	(%rsi,%rdx,8), %rsi
	salq	$6, %rdx
	movq	(%rsi), %rdi
	subq	%rdx, %rcx
	leaq	(%rdi,%rcx,8), %rdx
	leaq	512(%rdi), %r8
	movq	%rdx, %xmm0
.L2905:
	movq	%rdi, %xmm1
	movq	%rsi, %xmm2
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	movq	%r8, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L2907:
	leaq	(%r9,%rdx,8), %rcx
	movq	%rcx, %xmm0
	jmp	.L2905
	.p2align 4,,10
	.p2align 3
.L2903:
	movq	%rcx, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L2906
	.cfi_endproc
.LFE31379:
	.size	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedState14ObjectPositionERS3_PS3_EplEl, .-_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedState14ObjectPositionERS3_PS3_EplEl
	.section	.text._ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_reallocate_mapEmb,"axG",@progbits,_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_reallocate_mapEmb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_reallocate_mapEmb
	.type	_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_reallocate_mapEmb, @function
_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_reallocate_mapEmb:
.LFB31974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	72(%rdi), %rax
	movq	40(%rdi), %r8
	movq	8(%rdi), %rbx
	movq	%rax, %r12
	subq	%r8, %r12
	movq	%r12, %rcx
	sarq	$3, %rcx
	leaq	1(%rsi,%rcx), %rcx
	leaq	(%rcx,%rcx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L2909
	subq	%rcx, %rbx
	shrq	%rbx
	salq	$3, %rbx
	testb	%dl, %dl
	leaq	(%rbx,%rsi,8), %rcx
	cmovne	%rcx, %rbx
	addq	$8, %rax
	addq	0(%r13), %rbx
	movq	%rax, %rdx
	subq	%r8, %rdx
	cmpq	%rbx, %r8
	jbe	.L2911
	cmpq	%rax, %r8
	je	.L2912
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L2912
	.p2align 4,,10
	.p2align 3
.L2909:
	cmpq	%rsi, %rbx
	movq	%rsi, %rax
	cmovnb	%rbx, %rax
	leaq	2(%rbx,%rax), %r14
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r14
	ja	.L2917
	leaq	0(,%r14,8), %rdi
	movl	%edx, -68(%rbp)
	movq	%r14, %rbx
	movq	%rsi, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movl	-68(%rbp), %edx
	movq	%rax, %r15
	subq	%rcx, %rbx
	shrq	%rbx
	salq	$3, %rbx
	testb	%dl, %dl
	leaq	(%rbx,%rsi,8), %rax
	movq	40(%r13), %rsi
	cmovne	%rax, %rbx
	movq	72(%r13), %rax
	leaq	8(%rax), %rdx
	addq	%r15, %rbx
	cmpq	%rsi, %rdx
	je	.L2915
	subq	%rsi, %rdx
	movq	%rbx, %rdi
	call	memmove@PLT
.L2915:
	movq	0(%r13), %rdi
	call	_ZdlPv@PLT
	movq	%r15, 0(%r13)
	movq	%r14, 8(%r13)
.L2912:
	movq	%rbx, 40(%r13)
	movq	(%rbx), %rax
	movq	(%rbx), %xmm0
	addq	%r12, %rbx
	addq	$512, %rax
	movq	%rbx, 72(%r13)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%r13)
	movq	(%rbx), %rax
	movq	%rax, 56(%r13)
	addq	$512, %rax
	movq	%rax, 64(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2911:
	.cfi_restore_state
	cmpq	%rax, %r8
	je	.L2912
	leaq	8(%r12), %rdi
	movq	%r8, %rsi
	subq	%rdx, %rdi
	addq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L2912
.L2917:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE31974:
	.size	_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_reallocate_mapEmb, .-_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_reallocate_mapEmb
	.section	.text._ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE12emplace_backIJS3_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE12emplace_backIJS3_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE12emplace_backIJS3_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE12emplace_backIJS3_EEEvDpOT_:
.LFB30235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %rax
	movq	48(%rdi), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2919
	movq	(%rsi), %rax
	movq	%rax, (%rdx)
	addq	$8, 48(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2919:
	.cfi_restore_state
	movq	72(%rdi), %r13
	subq	56(%rdi), %rdx
	sarq	$3, %rdx
	movq	%r13, %rax
	subq	40(%rdi), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$6, %rax
	addq	%rdx, %rax
	movq	32(%rdi), %rdx
	subq	16(%rdi), %rdx
	sarq	$3, %rdx
	addq	%rax, %rdx
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	je	.L2924
	movq	8(%rdi), %rcx
	movq	%r13, %rax
	subq	(%rdi), %rax
	sarq	$3, %rax
	subq	%rax, %rcx
	cmpq	$1, %rcx
	jbe	.L2925
.L2922:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	48(%rbx), %rax
	movq	%rdx, (%rax)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2925:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_reallocate_mapEmb
	movq	72(%rbx), %r13
	jmp	.L2922
.L2924:
	leaq	.LC75(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30235:
	.size	_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE12emplace_backIJS3_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	.section	.rodata._ZN2v88internal15TranslatedState39CreateArgumentsElementsTranslatedValuesEimNS0_19CreateArgumentsTypeEP8_IO_FILE.str1.8,"aMS",@progbits,1
	.align 8
.LC157:
	.string	"arguments elements object #%d (type = %d, length = %d)"
	.section	.text._ZN2v88internal15TranslatedState39CreateArgumentsElementsTranslatedValuesEimNS0_19CreateArgumentsTypeEP8_IO_FILE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState39CreateArgumentsElementsTranslatedValuesEimNS0_19CreateArgumentsTypeEP8_IO_FILE
	.type	_ZN2v88internal15TranslatedState39CreateArgumentsElementsTranslatedValuesEimNS0_19CreateArgumentsTypeEP8_IO_FILE, @function
_ZN2v88internal15TranslatedState39CreateArgumentsElementsTranslatedValuesEimNS0_19CreateArgumentsTypeEP8_IO_FILE:
.LFB26003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r10d
	movq	%rdx, %rsi
	movq	%rdi, %r9
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -128(%rbp)
	movslq	%r10d, %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	salq	$4, %rax
	subq	%rdx, %rax
	movq	(%r9), %rdx
	leaq	(%rdx,%rax,8), %rbx
	movq	(%rsi), %rax
	cmpq	$38, -8(%rax)
	je	.L2951
	movl	40(%r9), %r13d
.L2928:
	cmpb	$2, %r14b
	jne	.L2929
	subl	40(%r9), %r13d
	movl	$0, %eax
	cmovs	%eax, %r13d
.L2929:
	movq	120(%r9), %rax
	subq	88(%r9), %rax
	sarq	$3, %rax
	movq	96(%r9), %r12
	subq	104(%r9), %r12
	subq	$1, %rax
	sarq	$3, %r12
	movq	88(%rbx), %r15
	subq	96(%rbx), %r15
	salq	$6, %rax
	sarq	$5, %r15
	leaq	(%rax,%r12), %rax
	movq	80(%r9), %r12
	subq	64(%r9), %r12
	sarq	$3, %r12
	addq	%rax, %r12
	movq	112(%rbx), %rax
	subq	80(%rbx), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$4, %rax
	leaq	(%rax,%r15), %rax
	movq	72(%rbx), %r15
	subq	56(%rbx), %r15
	sarq	$5, %r15
	addq	%rax, %r15
	testq	%rdi, %rdi
	je	.L2930
	movzbl	%r14b, %ecx
	movl	%r13d, %r8d
	movl	%r12d, %edx
	xorl	%eax, %eax
	leaq	.LC157(%rip), %rsi
	movl	%r10d, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movl	-112(%rbp), %r10d
	movq	-104(%rbp), %r9
.L2930:
	movl	%r15d, -92(%rbp)
	leaq	-96(%rbp), %r15
	leaq	48(%r9), %rdi
	movq	%r15, %rsi
	movq	%r9, -104(%rbp)
	movl	%r10d, -96(%rbp)
	call	_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	movl	$8, %r9d
	leal	2(%r13), %eax
	movl	%r12d, -72(%rbp)
	movw	%r9w, -96(%rbp)
	movq	-104(%rbp), %r9
	leaq	40(%rbx), %r12
	movq	$0, -80(%rbp)
	movq	%r9, -88(%rbp)
	movl	%eax, -68(%rbp)
	movq	104(%rbx), %rax
	movq	88(%rbx), %rdx
	subq	$32, %rax
	cmpq	%rax, %rdx
	je	.L2931
	movdqa	-96(%rbp), %xmm4
	movups	%xmm4, (%rdx)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm5, 16(%rdx)
	movq	88(%rbx), %rax
	addq	$32, %rax
	movq	%rax, 88(%rbx)
.L2932:
	movq	24(%r9), %rcx
	movl	$1, %r8d
	movq	152(%rcx), %rdx
	movw	%r8w, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%rdx, -72(%rbp)
	movq	$0, -80(%rbp)
	movq	104(%rbx), %rsi
	leaq	-32(%rsi), %rdx
	cmpq	%rdx, %rax
	je	.L2933
	movdqa	-96(%rbp), %xmm6
	movups	%xmm6, (%rax)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm7, 16(%rax)
	movq	88(%rbx), %rax
	addq	$32, %rax
	movq	%rax, 88(%rbx)
.L2934:
	movl	$2, %edi
	movq	%r9, -88(%rbp)
	movw	%di, -96(%rbp)
	movq	$0, -80(%rbp)
	movl	%r13d, -72(%rbp)
	movq	104(%rbx), %rsi
	leaq	-32(%rsi), %rdx
	cmpq	%rdx, %rax
	je	.L2935
	movdqa	-96(%rbp), %xmm4
	movups	%xmm4, (%rax)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm5, 16(%rax)
	addq	$32, 88(%rbx)
.L2936:
	testb	%r14b, %r14b
	jne	.L2937
	cmpl	%r13d, 40(%r9)
	movl	%r13d, %r10d
	cmovle	40(%r9), %r10d
	subl	%r10d, %r13d
	testl	%r10d, %r10d
	jle	.L2937
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L2940:
	movq	96(%rcx), %rax
	movl	$1, %esi
	movq	%r9, -88(%rbp)
	movw	%si, -96(%rbp)
	movq	$0, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	104(%rbx), %rax
	movq	88(%rbx), %rdx
	subq	$32, %rax
	cmpq	%rax, %rdx
	je	.L2938
	movdqa	-96(%rbp), %xmm0
	addl	$1, %r14d
	movups	%xmm0, (%rdx)
	movdqa	-80(%rbp), %xmm1
	movups	%xmm1, 16(%rdx)
	addq	$32, 88(%rbx)
	cmpl	%r14d, %r10d
	jne	.L2940
.L2937:
	movl	%r13d, %r8d
	subl	$1, %r8d
	js	.L2926
	movq	-128(%rbp), %rdi
	leal	0(,%r8,8), %eax
	movl	%r8d, %r8d
	cltq
	salq	$3, %r8
	leaq	8(%rax,%rdi), %r13
	leaq	16(%rax,%rdi), %r14
	subq	%r8, %r13
	.p2align 4,,10
	.p2align 3
.L2945:
	movq	(%r14), %rax
	movl	$1, %edx
	movq	%r9, -88(%rbp)
	movw	%dx, -96(%rbp)
	movq	$0, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	104(%rbx), %rax
	movq	88(%rbx), %rdx
	subq	$32, %rax
	cmpq	%rax, %rdx
	je	.L2942
	movdqa	-96(%rbp), %xmm2
	subq	$8, %r14
	movups	%xmm2, (%rdx)
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, 16(%rdx)
	addq	$32, 88(%rbx)
	cmpq	%r13, %r14
	jne	.L2945
.L2926:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2952
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2942:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	subq	$8, %r14
	movq	%r9, -104(%rbp)
	call	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	cmpq	%r14, %r13
	movq	-104(%rbp), %r9
	jne	.L2945
	jmp	.L2926
	.p2align 4,,10
	.p2align 3
.L2938:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r9, -120(%rbp)
	addl	$1, %r14d
	movl	%r10d, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	movl	-112(%rbp), %r10d
	movq	-104(%rbp), %rcx
	movq	-120(%rbp), %r9
	cmpl	%r10d, %r14d
	jne	.L2940
	jmp	.L2937
	.p2align 4,,10
	.p2align 3
.L2951:
	movq	%rax, -128(%rbp)
	movl	-20(%rax), %r13d
	jmp	.L2928
	.p2align 4,,10
	.p2align 3
.L2935:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r9, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %rcx
	jmp	.L2936
	.p2align 4,,10
	.p2align 3
.L2931:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r9, -104(%rbp)
	call	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	movq	88(%rbx), %rax
	movq	-104(%rbp), %r9
	jmp	.L2932
	.p2align 4,,10
	.p2align 3
.L2933:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r9, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	movq	88(%rbx), %rax
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %rcx
	jmp	.L2934
.L2952:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26003:
	.size	_ZN2v88internal15TranslatedState39CreateArgumentsElementsTranslatedValuesEimNS0_19CreateArgumentsTypeEP8_IO_FILE, .-_ZN2v88internal15TranslatedState39CreateArgumentsElementsTranslatedValuesEimNS0_19CreateArgumentsTypeEP8_IO_FILE
	.section	.rodata._ZN2v88internal15TranslatedState25CreateNextTranslatedValueEiPNS0_19TranslationIteratorENS0_10FixedArrayEmPNS0_14RegisterValuesEP8_IO_FILE.str1.1,"aMS",@progbits,1
.LC158:
	.string	"duplicated object #%d"
	.section	.rodata._ZN2v88internal15TranslatedState25CreateNextTranslatedValueEiPNS0_19TranslationIteratorENS0_10FixedArrayEmPNS0_14RegisterValuesEP8_IO_FILE.str1.8,"aMS",@progbits,1
	.align 8
.LC159:
	.string	"arguments length field (type = %d, length = %d)"
	.align 8
.LC160:
	.string	"captured object #%d (length = %d)"
	.section	.rodata._ZN2v88internal15TranslatedState25CreateNextTranslatedValueEiPNS0_19TranslationIteratorENS0_10FixedArrayEmPNS0_14RegisterValuesEP8_IO_FILE.str1.1
.LC161:
	.string	"0x%012lx ; %s "
.LC162:
	.string	"%ld ; %s (int32)"
.LC163:
	.string	"%ld ; %s (int64)"
.LC164:
	.string	"%lu ; %s (uint32)"
.LC165:
	.string	"%ld ; %s (bool)"
.LC166:
	.string	"%e ; %s (float)"
.LC167:
	.string	"%e ; %s (double)"
.LC168:
	.string	"0x%012lx ;  [fp %c %3d]  "
.LC169:
	.string	"%d ; (int32) [fp %c %3d] "
.LC170:
	.string	"%ld ; (int64) [fp %c %3d] "
.LC171:
	.string	"%u ; (uint32) [fp %c %3d] "
.LC172:
	.string	"%u ; (bool) [fp %c %3d] "
.LC173:
	.string	"%e ; (float) [fp %c %3d] "
.LC174:
	.string	"%e ; (double) [fp %c %d] "
.LC175:
	.string	"0x%012lx ; (literal %2d) "
	.section	.text._ZN2v88internal15TranslatedState25CreateNextTranslatedValueEiPNS0_19TranslationIteratorENS0_10FixedArrayEmPNS0_14RegisterValuesEP8_IO_FILE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState25CreateNextTranslatedValueEiPNS0_19TranslationIteratorENS0_10FixedArrayEmPNS0_14RegisterValuesEP8_IO_FILE
	.type	_ZN2v88internal15TranslatedState25CreateNextTranslatedValueEiPNS0_19TranslationIteratorENS0_10FixedArrayEmPNS0_14RegisterValuesEP8_IO_FILE, @function
_ZN2v88internal15TranslatedState25CreateNextTranslatedValueEiPNS0_19TranslationIteratorENS0_10FixedArrayEmPNS0_14RegisterValuesEP8_IO_FILE:
.LFB26004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r11
	leaq	16+_ZTVN6disasm13NameConverterE(%rip), %rcx
	movq	%r9, %r10
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movslq	%esi, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$280, %rsp
	movq	16(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-184(%rbp), %rax
	movq	$128, -192(%rbp)
	movq	%rax, %xmm1
	movq	%rdx, %rax
	salq	$4, %rax
	punpcklqdq	%xmm1, %xmm0
	subq	%rdx, %rax
	movq	(%r12), %rdx
	movaps	%xmm0, -208(%rbp)
	leaq	(%rdx,%rax,8), %r14
	movq	112(%r14), %rax
	movq	%rax, -304(%rbp)
	movq	80(%r14), %rax
	movq	%rax, -296(%rbp)
	movq	88(%r14), %rax
	movq	%rax, -272(%rbp)
	movq	96(%r14), %rax
	movq	%rax, -264(%rbp)
	movq	72(%r14), %rax
	movq	%rax, -280(%rbp)
	movq	56(%r14), %rax
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal19TranslationIterator4NextEv
	subl	$7, %eax
	cmpl	$18, %eax
	ja	.L2954
	leaq	.L2956(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal15TranslatedState25CreateNextTranslatedValueEiPNS0_19TranslationIteratorENS0_10FixedArrayEmPNS0_14RegisterValuesEP8_IO_FILE,"a",@progbits
	.align 4
	.align 4
.L2956:
	.long	.L2974-.L2956
	.long	.L2973-.L2956
	.long	.L2972-.L2956
	.long	.L2971-.L2956
	.long	.L2970-.L2956
	.long	.L2969-.L2956
	.long	.L2968-.L2956
	.long	.L2967-.L2956
	.long	.L2966-.L2956
	.long	.L2965-.L2956
	.long	.L2964-.L2956
	.long	.L2963-.L2956
	.long	.L2962-.L2956
	.long	.L2961-.L2956
	.long	.L2960-.L2956
	.long	.L2959-.L2956
	.long	.L2958-.L2956
	.long	.L2957-.L2956
	.long	.L2955-.L2956
	.section	.text._ZN2v88internal15TranslatedState25CreateNextTranslatedValueEiPNS0_19TranslationIteratorENS0_10FixedArrayEmPNS0_14RegisterValuesEP8_IO_FILE
	.p2align 4,,10
	.p2align 3
.L2957:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movslq	%eax, %rdx
	movq	(%rdx,%rbx), %rbx
	testq	%r15, %r15
	je	.L3035
	cltd
	movq	%rbx, %xmm0
	leaq	.LC174(%rip), %rsi
	movq	%r15, %rdi
	xorl	%edx, %eax
	movl	%eax, %ecx
	movl	$1, %eax
	subl	%edx, %ecx
	andl	$2, %edx
	addl	$43, %edx
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L3035:
	movl	$7, %edx
	movq	%r12, -232(%rbp)
	movw	%dx, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	%rbx, -216(%rbp)
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L2955:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %ecx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%r11,%rax), %rdx
	movq	%rdx, -248(%rbp)
	testq	%r15, %r15
	je	.L3038
	leaq	.LC175(%rip), %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	leaq	-248(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movq	-248(%rbp), %rdx
.L3038:
	movl	$1, %eax
	movq	%r12, -232(%rbp)
	movw	%ax, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	%rdx, -216(%rbp)
	.p2align 4,,10
	.p2align 3
.L3127:
	leaq	-240(%rbp), %rsi
	leaq	40(%r14), %rdi
	call	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE9push_backERKS2_
	xorl	%eax, %eax
	cmpb	$8, -240(%rbp)
	jne	.L2953
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2974:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %ebx
	testq	%r15, %r15
	je	.L2975
	movl	%eax, %edx
	leaq	.LC158(%rip), %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2975:
	leaq	-240(%rbp), %r13
	leaq	48(%r12), %rax
	movslq	%ebx, %rdx
	leaq	64(%r12), %rsi
	movq	%r13, %rdi
	movq	%rax, -264(%rbp)
	call	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedState14ObjectPositionERS3_PS3_EplEl
	movq	112(%r12), %rax
	movq	96(%r12), %rdx
	movq	-240(%rbp), %r15
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2976
	movq	(%r15), %rax
	movq	%rax, (%rdx)
	addq	$8, 96(%r12)
.L2977:
	movl	$9, %ecx
	movq	%r12, -232(%rbp)
	movw	%cx, -240(%rbp)
	movq	$0, -224(%rbp)
	movl	%ebx, -216(%rbp)
	movl	$-1, -212(%rbp)
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L2973:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movq	%r15, %r8
	movq	%rbx, %rdx
	movl	%r13d, %esi
	movl	%eax, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState39CreateArgumentsElementsTranslatedValuesEimNS0_19CreateArgumentsTypeEP8_IO_FILE
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2953:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3154
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2965:
	.cfi_restore_state
	call	_ZN2v88internal19TranslationIterator4NextEv
	addq	$40, %r14
	movslq	%eax, %rbx
	testq	%r10, %r10
	je	.L3142
	movl	%ebx, %esi
	movq	%r10, %rdi
	call	_ZNK2v88internal14RegisterValues16GetFloatRegisterEj@PLT
	movl	%eax, %r13d
	testq	%r15, %r15
	je	.L3008
	leaq	.LC91(%rip), %rdx
	cmpl	$-1, %ebx
	je	.L3009
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%rbx,8), %rdx
.L3009:
	movd	%r13d, %xmm3
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movl	$1, %eax
	leaq	.LC166(%rip), %rsi
	cvtss2sd	%xmm3, %xmm0
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L3008:
	movl	$6, %ebx
	movq	%r12, -232(%rbp)
	movw	%bx, -240(%rbp)
	movq	$0, -224(%rbp)
	movl	%r13d, -216(%rbp)
	jmp	.L3140
	.p2align 4,,10
	.p2align 3
.L2964:
	call	_ZN2v88internal19TranslationIterator4NextEv
	addq	$40, %r14
	testq	%r10, %r10
	je	.L3142
	movl	%eax, %edx
	movq	128(%r10,%rdx,8), %rbx
	testq	%r15, %r15
	je	.L3013
	leaq	.LC91(%rip), %rdx
	cmpl	$-1, %eax
	je	.L3014
	cltq
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
.L3014:
	movq	%rbx, %xmm0
	leaq	.LC167(%rip), %rsi
	movq	%r15, %rdi
	movl	$1, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L3013:
	movl	$7, %r11d
	movq	%r12, -232(%rbp)
	movw	%r11w, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	%rbx, -216(%rbp)
	jmp	.L3140
	.p2align 4,,10
	.p2align 3
.L2969:
	call	_ZN2v88internal19TranslationIterator4NextEv
	addq	$40, %r14
	movl	%eax, %esi
	testq	%r10, %r10
	je	.L3142
	movl	%eax, %eax
	movq	(%r10,%rax,8), %rbx
	testq	%r15, %r15
	je	.L2992
	leaq	-208(%rbp), %rdi
	call	_ZNK6disasm13NameConverter17NameOfCPURegisterEi@PLT
	movq	%rbx, %rdx
	leaq	.LC162(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2992:
	movl	$2, %eax
	movq	%r12, -232(%rbp)
	movw	%ax, -240(%rbp)
	movq	$0, -224(%rbp)
	movl	%ebx, -216(%rbp)
	jmp	.L3140
	.p2align 4,,10
	.p2align 3
.L2968:
	call	_ZN2v88internal19TranslationIterator4NextEv
	addq	$40, %r14
	movl	%eax, %esi
	testq	%r10, %r10
	je	.L3142
	movl	%eax, %eax
	movq	(%r10,%rax,8), %rbx
	testq	%r15, %r15
	je	.L2996
	leaq	-208(%rbp), %rdi
	call	_ZNK6disasm13NameConverter17NameOfCPURegisterEi@PLT
	movq	%rbx, %rdx
	leaq	.LC163(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2996:
	movl	$3, %eax
	movq	%r12, -232(%rbp)
	movw	%ax, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	%rbx, -216(%rbp)
	jmp	.L3140
	.p2align 4,,10
	.p2align 3
.L2967:
	call	_ZN2v88internal19TranslationIterator4NextEv
	addq	$40, %r14
	movl	%eax, %esi
	testq	%r10, %r10
	je	.L3142
	movl	%eax, %eax
	movq	(%r10,%rax,8), %rbx
	testq	%r15, %r15
	je	.L3000
	leaq	-208(%rbp), %rdi
	call	_ZNK6disasm13NameConverter17NameOfCPURegisterEi@PLT
	movq	%rbx, %rdx
	leaq	.LC164(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L3000:
	movl	$4, %r15d
	movq	%r12, -232(%rbp)
	movw	%r15w, -240(%rbp)
	movq	$0, -224(%rbp)
	movl	%ebx, -216(%rbp)
	jmp	.L3140
	.p2align 4,,10
	.p2align 3
.L2966:
	call	_ZN2v88internal19TranslationIterator4NextEv
	addq	$40, %r14
	movl	%eax, %esi
	testq	%r10, %r10
	je	.L3142
	movl	%eax, %eax
	movq	(%r10,%rax,8), %rbx
	testq	%r15, %r15
	je	.L3004
	leaq	-208(%rbp), %rdi
	call	_ZNK6disasm13NameConverter17NameOfCPURegisterEi@PLT
	movq	%rbx, %rdx
	leaq	.LC165(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L3004:
	movl	$5, %r13d
	movq	%r12, -232(%rbp)
	movw	%r13w, -240(%rbp)
	movq	$0, -224(%rbp)
	movl	%ebx, -216(%rbp)
	jmp	.L3140
	.p2align 4,,10
	.p2align 3
.L2971:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, -308(%rbp)
	movl	%eax, %ecx
	movq	120(%r12), %rax
	subq	88(%r12), %rax
	sarq	$3, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	movq	96(%r12), %rax
	subq	104(%r12), %rax
	salq	$6, %rdx
	sarq	$3, %rax
	addq	%rdx, %rax
	movq	80(%r12), %rdx
	subq	64(%r12), %rdx
	sarq	$3, %rdx
	leaq	(%rax,%rdx), %rbx
	movq	%rbx, -320(%rbp)
	testq	%r15, %r15
	je	.L2983
	movl	%ebx, %edx
	leaq	.LC160(%rip), %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2983:
	movq	-304(%rbp), %rbx
	subq	-296(%rbp), %rbx
	leaq	48(%r12), %rdi
	movl	%r13d, -240(%rbp)
	sarq	$3, %rbx
	movq	-272(%rbp), %rax
	subq	-264(%rbp), %rax
	leaq	-240(%rbp), %r13
	leaq	-1(%rbx), %rdx
	sarq	$5, %rax
	movq	%r13, %rsi
	movq	%rdx, %rcx
	salq	$4, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	-280(%rbp), %rax
	subq	-288(%rbp), %rax
	sarq	$5, %rax
	addq	%rdx, %rax
	movl	%eax, -236(%rbp)
	call	_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	movl	$8, %eax
	movq	%r12, -232(%rbp)
	movw	%ax, -240(%rbp)
	movl	-320(%rbp), %eax
	movq	$0, -224(%rbp)
	movl	%eax, -216(%rbp)
	movl	-308(%rbp), %eax
	movl	%eax, -212(%rbp)
.L3131:
	leaq	40(%r14), %rdi
	movq	%r13, %rsi
	call	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE9push_backERKS2_
	xorl	%eax, %eax
	cmpb	$8, -240(%rbp)
	jne	.L2953
	.p2align 4,,10
	.p2align 3
.L3125:
	movl	-212(%rbp), %eax
	jmp	.L2953
	.p2align 4,,10
	.p2align 3
.L2970:
	call	_ZN2v88internal19TranslationIterator4NextEv
	addq	$40, %r14
	movl	%eax, %esi
	testq	%r10, %r10
	je	.L3142
	movl	%eax, %eax
	leaq	-240(%rbp), %r13
	movq	(%r10,%rax,8), %rbx
	testq	%r15, %r15
	je	.L2988
	leaq	-208(%rbp), %rdi
	call	_ZNK6disasm13NameConverter17NameOfCPURegisterEi@PLT
	leaq	.LC161(%rip), %rsi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rbx, -240(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
.L2988:
	movl	$1, %eax
	movq	%r12, -232(%rbp)
	movq	%r13, %rsi
	movw	%ax, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	%rbx, -216(%rbp)
	jmp	.L3137
	.p2align 4,,10
	.p2align 3
.L2972:
	call	_ZN2v88internal19TranslationIterator4NextEv
	leaq	-248(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	movl	%eax, %r10d
	call	_ZN2v88internal15TranslatedState24ComputeArgumentsPositionEmNS0_19CreateArgumentsTypeEPi
	testq	%r15, %r15
	je	.L2982
	movl	-248(%rbp), %ecx
	movzbl	%r10b, %edx
	movq	%r15, %rdi
	xorl	%eax, %eax
	leaq	.LC159(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L2982:
	movl	-248(%rbp), %eax
	movl	$2, %edx
	leaq	40(%r14), %rdi
	leaq	-240(%rbp), %rsi
	movw	%dx, -240(%rbp)
	movl	%eax, -216(%rbp)
	movq	%r12, -232(%rbp)
	movq	$0, -224(%rbp)
	call	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE9push_backERKS2_
	xorl	%eax, %eax
	jmp	.L2953
	.p2align 4,,10
	.p2align 3
.L2961:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movl	%eax, %r8d
	cltq
	movq	(%rax,%rbx), %rbx
	testq	%r15, %r15
	je	.L3023
	movl	%r8d, %eax
	movq	%rbx, %rdx
	leaq	.LC170(%rip), %rsi
	movq	%r15, %rdi
	sarl	$31, %eax
	movl	%eax, %ecx
	xorl	%eax, %r8d
	andl	$2, %ecx
	subl	%eax, %r8d
	xorl	%eax, %eax
	addl	$43, %ecx
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L3023:
	movl	$3, %r8d
	movq	%r12, -232(%rbp)
	movw	%r8w, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	%rbx, -216(%rbp)
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L2963:
	call	_ZN2v88internal19TranslationIterator4NextEv
	leaq	-240(%rbp), %r13
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movl	%eax, %r8d
	cltq
	movq	(%rax,%rbx), %rbx
	testq	%r15, %r15
	je	.L3015
	movl	%r8d, %eax
	leaq	.LC168(%rip), %rsi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	sarl	$31, %eax
	movl	%eax, %ecx
	xorl	%eax, %r8d
	andl	$2, %ecx
	subl	%eax, %r8d
	xorl	%eax, %eax
	addl	$43, %ecx
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rbx, -240(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
.L3015:
	movl	$1, %r10d
	movq	%r12, -232(%rbp)
	movw	%r10w, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	%rbx, -216(%rbp)
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L2959:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movl	%eax, %r8d
	cltq
	movl	(%rax,%rbx), %ebx
	testq	%r15, %r15
	je	.L3029
	movl	%r8d, %eax
	movl	%ebx, %edx
	leaq	.LC172(%rip), %rsi
	movq	%r15, %rdi
	sarl	$31, %eax
	movl	%eax, %ecx
	xorl	%eax, %r8d
	andl	$2, %ecx
	subl	%eax, %r8d
	xorl	%eax, %eax
	addl	$43, %ecx
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L3029:
	movl	$5, %esi
	movq	%r12, -232(%rbp)
	movw	%si, -240(%rbp)
	movq	$0, -224(%rbp)
	movl	%ebx, -216(%rbp)
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L2962:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movl	%eax, %r8d
	cltq
	movl	(%rax,%rbx), %ebx
	testq	%r15, %r15
	je	.L3019
	movl	%r8d, %eax
	movl	%ebx, %edx
	leaq	.LC169(%rip), %rsi
	movq	%r15, %rdi
	sarl	$31, %eax
	movl	%eax, %ecx
	xorl	%eax, %r8d
	andl	$2, %ecx
	subl	%eax, %r8d
	xorl	%eax, %eax
	addl	$43, %ecx
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L3019:
	movl	$2, %r9d
	movq	%r12, -232(%rbp)
	movw	%r9w, -240(%rbp)
	movq	$0, -224(%rbp)
	movl	%ebx, -216(%rbp)
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L2960:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movl	%eax, %r8d
	cltq
	movl	(%rax,%rbx), %ebx
	testq	%r15, %r15
	je	.L3026
	movl	%r8d, %eax
	movl	%ebx, %edx
	leaq	.LC171(%rip), %rsi
	movq	%r15, %rdi
	sarl	$31, %eax
	movl	%eax, %ecx
	xorl	%eax, %r8d
	andl	$2, %ecx
	subl	%eax, %r8d
	xorl	%eax, %eax
	addl	$43, %ecx
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L3026:
	movl	$4, %edi
	movq	%r12, -232(%rbp)
	movw	%di, -240(%rbp)
	movq	$0, -224(%rbp)
	movl	%ebx, -216(%rbp)
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L2958:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movslq	%eax, %rdx
	movl	(%rdx,%rbx), %ebx
	testq	%r15, %r15
	je	.L3032
	cltd
	movd	%ebx, %xmm2
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	xorl	%edx, %eax
	leaq	.LC173(%rip), %rsi
	cvtss2sd	%xmm2, %xmm0
	movl	%eax, %ecx
	movl	$1, %eax
	subl	%edx, %ecx
	andl	$2, %edx
	addl	$43, %edx
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L3032:
	movl	$6, %ecx
	movq	%r12, -232(%rbp)
	movw	%cx, -240(%rbp)
	movq	$0, -224(%rbp)
	movl	%ebx, -216(%rbp)
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L3142:
	xorl	%eax, %eax
	movq	%r12, -232(%rbp)
	movw	%ax, -240(%rbp)
	movq	$0, -224(%rbp)
.L3140:
	leaq	-240(%rbp), %rsi
.L3137:
	movq	%r14, %rdi
	call	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE9push_backERKS2_
	xorl	%eax, %eax
	cmpb	$8, -240(%rbp)
	jne	.L2953
	jmp	.L3125
.L2976:
	movq	120(%r12), %rcx
	subq	104(%r12), %rdx
	sarq	$3, %rdx
	movq	%rcx, %rax
	subq	88(%r12), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$6, %rax
	addq	%rax, %rdx
	movq	80(%r12), %rax
	subq	64(%r12), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	je	.L3155
	movq	56(%r12), %rsi
	movq	%rcx, %rax
	subq	48(%r12), %rax
	sarq	$3, %rax
	subq	%rax, %rsi
	cmpq	$1, %rsi
	jbe	.L3156
.L2979:
	movl	$512, %edi
	movq	%rcx, -264(%rbp)
	call	_Znwm@PLT
	movq	-264(%rbp), %rcx
	movq	%rax, 8(%rcx)
	movq	(%r15), %rdx
	movq	96(%r12), %rax
	movq	%rdx, (%rax)
	movq	120(%r12), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 120(%r12)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 104(%r12)
	movq	%rdx, 112(%r12)
	movq	%rax, 96(%r12)
	jmp	.L2977
.L3156:
	xorl	%edx, %edx
	movl	$1, %esi
	leaq	48(%r12), %rdi
	call	_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_reallocate_mapEmb
	movq	120(%r12), %rcx
	jmp	.L2979
.L2954:
	leaq	.LC58(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3154:
	call	__stack_chk_fail@PLT
.L3155:
	leaq	.LC75(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26004:
	.size	_ZN2v88internal15TranslatedState25CreateNextTranslatedValueEiPNS0_19TranslationIteratorENS0_10FixedArrayEmPNS0_14RegisterValuesEP8_IO_FILE, .-_ZN2v88internal15TranslatedState25CreateNextTranslatedValueEiPNS0_19TranslationIteratorENS0_10FixedArrayEmPNS0_14RegisterValuesEP8_IO_FILE
	.section	.text._ZNSt11_Deque_baseIiSaIiEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIiSaIiEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIiSaIiEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIiSaIiEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIiSaIiEE17_M_initialize_mapEm:
.LFB31983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	shrq	$7, %rdi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	1(%rdi), %rbx
	addq	$3, %rdi
	subq	$8, %rsp
	cmpq	$8, %rdi
	ja	.L3158
	movq	$8, 8(%r13)
	movl	$64, %edi
.L3159:
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	movq	%rax, 0(%r13)
	subq	%rbx, %rdx
	shrq	%rdx
	leaq	(%rax,%rdx,8), %r15
	leaq	(%r15,%rbx,8), %r12
	movq	%r15, %rbx
	cmpq	%r12, %r15
	jnb	.L3161
	.p2align 4,,10
	.p2align 3
.L3160:
	movl	$512, %edi
	addq	$8, %rbx
	call	_Znwm@PLT
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r12
	ja	.L3160
.L3161:
	movq	(%r15), %rdx
	andl	$127, %r14d
	subq	$8, %r12
	movq	%r15, 40(%r13)
	leaq	512(%rdx), %rax
	movq	%rdx, %xmm0
	movq	%rax, 32(%r13)
	movq	(%r12), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, %xmm2
	movups	%xmm0, 16(%r13)
	leaq	(%rax,%r14,4), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 48(%r13)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 64(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3158:
	.cfi_restore_state
	movq	%rdi, 8(%r13)
	salq	$3, %rdi
	jmp	.L3159
	.cfi_endproc
.LFE31983:
	.size	_ZNSt11_Deque_baseIiSaIiEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIiSaIiEE17_M_initialize_mapEm
	.section	.text._ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPKN2v88internal15TranslatedFrameEPS4_EET0_T_S9_S8_,"axG",@progbits,_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPKN2v88internal15TranslatedFrameEPS4_EET0_T_S9_S8_,comdat
	.p2align 4
	.weak	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPKN2v88internal15TranslatedFrameEPS4_EET0_T_S9_S8_
	.type	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPKN2v88internal15TranslatedFrameEPS4_EET0_T_S9_S8_, @function
_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPKN2v88internal15TranslatedFrameEPS4_EET0_T_S9_S8_:
.LFB32383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	cmpq	%rsi, %rdi
	je	.L3167
	movq	%rdi, %r15
	.p2align 4,,10
	.p2align 3
.L3179:
	movl	(%r15), %eax
	pxor	%xmm0, %xmm0
	movl	%eax, (%r12)
	movl	4(%r15), %eax
	movl	%eax, 4(%r12)
	movq	8(%r15), %rax
	movq	%rax, 8(%r12)
	movq	16(%r15), %rax
	movq	%rax, 16(%r12)
	movl	24(%r15), %eax
	movl	%eax, 24(%r12)
	movl	28(%r15), %eax
	movl	%eax, 28(%r12)
	movl	32(%r15), %eax
	movl	%eax, 32(%r12)
	movq	112(%r15), %rax
	subq	80(%r15), %rax
	movq	88(%r15), %rbx
	sarq	$3, %rax
	subq	96(%r15), %rbx
	subq	$1, %rax
	sarq	$5, %rbx
	salq	$4, %rax
	leaq	(%rax,%rbx), %rax
	movq	72(%r15), %rbx
	subq	56(%r15), %rbx
	movq	$0, 40(%r12)
	sarq	$5, %rbx
	movups	%xmm0, 56(%r12)
	addq	%rax, %rbx
	movups	%xmm0, 72(%r12)
	movq	%rbx, %rdi
	movups	%xmm0, 88(%r12)
	shrq	$4, %rdi
	movups	%xmm0, 104(%r12)
	leaq	1(%rdi), %r13
	addq	$3, %rdi
	cmpq	$8, %rdi
	ja	.L3168
	movq	$8, 48(%r12)
	movl	$64, %edi
.L3169:
	call	_Znwm@PLT
	movq	48(%r12), %rdx
	movq	%rax, 40(%r12)
	subq	%r13, %rdx
	shrq	%rdx
	leaq	(%rax,%rdx,8), %r14
	leaq	(%r14,%r13,8), %r13
	movq	%r14, %rsi
	cmpq	%r13, %r14
	jnb	.L3173
	.p2align 4,,10
	.p2align 3
.L3171:
	movl	$512, %edi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, (%rsi)
	addq	$8, %rsi
	cmpq	%rsi, %r13
	ja	.L3171
.L3173:
	movq	(%r14), %rax
	andl	$15, %ebx
	movq	-8(%r13), %rcx
	leaq	-8(%r13), %rdx
	salq	$5, %rbx
	addq	-8(%r13), %rbx
	movq	%rdx, %xmm3
	movq	%r14, 80(%r12)
	movq	%rax, %xmm0
	addq	$512, %rcx
	leaq	512(%rax), %rsi
	punpcklqdq	%xmm0, %xmm0
	movq	%rsi, 72(%r12)
	movups	%xmm0, 56(%r12)
	movq	%rbx, %xmm0
	movhps	-8(%r13), %xmm0
	movups	%xmm0, 88(%r12)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 104(%r12)
	movq	88(%r15), %r10
	movq	56(%r15), %rdx
	movq	72(%r15), %r9
	movq	80(%r15), %rdi
	.p2align 4,,10
	.p2align 3
.L3172:
	movq	%rdi, %r8
.L3178:
	cmpq	%rdx, %r10
	je	.L3174
	movdqu	(%rdx), %xmm1
	addq	$32, %rdx
	movups	%xmm1, (%rax)
	movdqu	-16(%rdx), %xmm2
	movups	%xmm2, 16(%rax)
	cmpq	%r9, %rdx
	je	.L3186
	addq	$32, %rax
	cmpq	%rax, %rsi
	jne	.L3178
	movq	8(%r14), %rax
	addq	$8, %r14
	leaq	512(%rax), %rsi
	jmp	.L3172
	.p2align 4,,10
	.p2align 3
.L3186:
	movq	8(%r8), %rdx
	addq	$32, %rax
	addq	$8, %rdi
	leaq	512(%rdx), %r9
	cmpq	%rax, %rsi
	jne	.L3172
	movq	8(%r14), %rax
	addq	$8, %r14
	leaq	512(%rax), %rsi
	jmp	.L3172
	.p2align 4,,10
	.p2align 3
.L3174:
	addq	$120, %r15
	addq	$120, %r12
	cmpq	%r15, -64(%rbp)
	jne	.L3179
.L3167:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3168:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rax
	movq	%rdi, 48(%r12)
	cmpq	%rax, %rdi
	ja	.L3187
	salq	$3, %rdi
	jmp	.L3169
.L3187:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE32383:
	.size	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPKN2v88internal15TranslatedFrameEPS4_EET0_T_S9_S8_, .-_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPKN2v88internal15TranslatedFrameEPS4_EET0_T_S9_S8_
	.section	.rodata._ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE7reserveEm.str1.1,"aMS",@progbits,1
.LC176:
	.string	"vector::reserve"
	.section	.text._ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE7reserveEm,"axG",@progbits,_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE7reserveEm
	.type	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE7reserveEm, @function
_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE7reserveEm:
.LFB28613:
	.cfi_startproc
	endbr64
	movabsq	$76861433640456465, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	ja	.L3209
	movq	(%rdi), %r13
	movq	16(%rdi), %rax
	movq	%rdi, %r12
	movabsq	$-1229782938247303441, %rdx
	subq	%r13, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L3210
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3210:
	.cfi_restore_state
	movq	8(%rdi), %rbx
	movq	%rsi, %r15
	movq	$0, -56(%rbp)
	salq	$4, %r15
	movq	%rbx, %rax
	subq	%rsi, %r15
	subq	%r13, %rax
	movq	%rax, -64(%rbp)
	leaq	0(,%r15,8), %rax
	movq	%rax, -72(%rbp)
	testq	%rsi, %rsi
	je	.L3191
	movq	%rax, %rdi
	call	_Znwm@PLT
	movq	%rax, -56(%rbp)
.L3191:
	movq	-56(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPKN2v88internal15TranslatedFrameEPS4_EET0_T_S9_S8_
	movq	8(%r12), %r13
	movq	(%r12), %r15
	cmpq	%r15, %r13
	je	.L3192
	.p2align 4,,10
	.p2align 3
.L3196:
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3193
	movq	112(%r15), %rax
	movq	80(%r15), %r14
	leaq	8(%rax), %rbx
	cmpq	%r14, %rbx
	jbe	.L3194
	.p2align 4,,10
	.p2align 3
.L3195:
	movq	(%r14), %rdi
	addq	$8, %r14
	call	_ZdlPv@PLT
	cmpq	%r14, %rbx
	ja	.L3195
	movq	40(%r15), %rdi
.L3194:
	call	_ZdlPv@PLT
.L3193:
	addq	$120, %r15
	cmpq	%r15, %r13
	jne	.L3196
	movq	(%r12), %r15
.L3192:
	testq	%r15, %r15
	je	.L3197
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3197:
	movq	-56(%rbp), %r14
	movq	-64(%rbp), %rax
	movq	%r14, (%r12)
	addq	%r14, %rax
	addq	-72(%rbp), %r14
	movq	%rax, 8(%r12)
	movq	%r14, 16(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3209:
	.cfi_restore_state
	leaq	.LC176(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE28613:
	.size	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE7reserveEm, .-_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE7reserveEm
	.section	.text._ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB31396:
	.cfi_startproc
	endbr64
	movabsq	$-1229782938247303441, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$76861433640456465, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%rdi, -72(%rbp)
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rsi, %rax
	je	.L3237
	movq	%rbx, %r8
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L3224
	movabsq	$9223372036854775800, %rcx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L3238
.L3213:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	%rax, -56(%rbp)
	movq	-88(%rbp), %rdx
	addq	%rax, %rcx
	movq	%rcx, -64(%rbp)
.L3223:
	movq	-56(%rbp), %rax
	movl	(%rdx), %ecx
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movq	24(%rdx), %rsi
	leaq	(%rax,%r8), %r13
	movl	%ecx, 0(%r13)
	movl	4(%rdx), %ecx
	leaq	40(%r13), %rdi
	movq	%rsi, 24(%r13)
	movl	%ecx, 4(%r13)
	movq	8(%rdx), %rcx
	movq	$0, 40(%r13)
	movq	%rcx, 8(%r13)
	movq	16(%rdx), %rcx
	movq	$0, 48(%r13)
	movq	%rcx, 16(%r13)
	movl	32(%rdx), %ecx
	movups	%xmm0, 56(%r13)
	movl	%ecx, 32(%r13)
	movups	%xmm0, 72(%r13)
	movups	%xmm0, 88(%r13)
	movups	%xmm0, 104(%r13)
	call	_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0
	movq	-80(%rbp), %rdx
	movq	40(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L3215
	movq	80(%r13), %rsi
	movdqu	72(%rdx), %xmm3
	movq	64(%r13), %rdi
	movq	56(%r13), %xmm1
	movq	%rsi, %xmm5
	movq	112(%r13), %rsi
	movq	72(%r13), %xmm0
	movups	%xmm3, 72(%r13)
	movq	%rdi, %xmm4
	movdqu	56(%rdx), %xmm2
	movq	96(%r13), %rdi
	movq	%rsi, %xmm3
	movq	40(%r13), %rsi
	punpcklqdq	%xmm4, %xmm1
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm1, 56(%rdx)
	movq	88(%r13), %xmm1
	movdqu	88(%rdx), %xmm6
	movq	%rcx, 40(%r13)
	movq	48(%r13), %rcx
	movq	%rsi, 40(%rdx)
	movq	48(%rdx), %rsi
	movups	%xmm0, 72(%rdx)
	movq	104(%r13), %xmm0
	movdqu	104(%rdx), %xmm7
	movups	%xmm2, 56(%r13)
	movq	%rdi, %xmm2
	movq	%rsi, 48(%r13)
	punpcklqdq	%xmm2, %xmm1
	punpcklqdq	%xmm3, %xmm0
	movq	%rcx, 48(%rdx)
	movups	%xmm6, 88(%r13)
	movups	%xmm7, 104(%r13)
	movups	%xmm1, 88(%rdx)
	movups	%xmm0, 104(%rdx)
.L3215:
	movq	-56(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r14, %r13
	call	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPKN2v88internal15TranslatedFrameEPS4_EET0_T_S9_S8_
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	120(%rax), %rdx
	call	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPKN2v88internal15TranslatedFrameEPS4_EET0_T_S9_S8_
	movq	%rax, -80(%rbp)
	cmpq	%r12, %r14
	je	.L3222
	.p2align 4,,10
	.p2align 3
.L3216:
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3219
	movq	112(%r13), %rax
	movq	80(%r13), %r15
	leaq	8(%rax), %rbx
	cmpq	%r15, %rbx
	jbe	.L3220
	.p2align 4,,10
	.p2align 3
.L3221:
	movq	(%r15), %rdi
	addq	$8, %r15
	call	_ZdlPv@PLT
	cmpq	%r15, %rbx
	ja	.L3221
	movq	40(%r13), %rdi
.L3220:
	call	_ZdlPv@PLT
.L3219:
	addq	$120, %r13
	cmpq	%r12, %r13
	jne	.L3216
.L3222:
	testq	%r14, %r14
	je	.L3218
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3218:
	movq	-56(%rbp), %xmm0
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rsi
	movhps	-80(%rbp), %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3238:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L3214
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	jmp	.L3223
	.p2align 4,,10
	.p2align 3
.L3224:
	movl	$120, %ecx
	jmp	.L3213
.L3214:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	imulq	$120, %rsi, %rcx
	jmp	.L3213
.L3237:
	leaq	.LC76(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE31396:
	.size	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE12emplace_backIJS2_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE12emplace_backIJS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE12emplace_backIJS2_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE12emplace_backIJS2_EEEvDpOT_:
.LFB30250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L3240
	movl	(%rsi), %eax
	movq	24(%rsi), %rdx
	pxor	%xmm0, %xmm0
	leaq	40(%r12), %rdi
	movl	%eax, (%r12)
	movl	4(%rsi), %eax
	movq	%rdx, 24(%r12)
	movl	%eax, 4(%r12)
	movq	8(%rsi), %rax
	movq	%rax, 8(%r12)
	movq	16(%rsi), %rax
	movq	%rax, 16(%r12)
	movl	32(%rsi), %eax
	movq	$0, 40(%r12)
	movl	%eax, 32(%r12)
	movq	$0, 48(%r12)
	movups	%xmm0, 56(%r12)
	movups	%xmm0, 72(%r12)
	movups	%xmm0, 88(%r12)
	movups	%xmm0, 104(%r12)
	call	_ZNSt11_Deque_baseIN2v88internal15TranslatedValueESaIS2_EE17_M_initialize_mapEm.constprop.0
	movq	40(%r13), %rax
	testq	%rax, %rax
	je	.L3241
	movq	64(%r12), %rcx
	movq	80(%r12), %rdx
	movq	56(%r12), %xmm1
	movq	72(%r12), %xmm0
	movq	%rcx, %xmm4
	movq	%rdx, %xmm5
	movdqu	56(%r13), %xmm2
	movdqu	72(%r13), %xmm3
	punpcklqdq	%xmm4, %xmm1
	punpcklqdq	%xmm5, %xmm0
	movdqu	88(%r13), %xmm6
	movdqu	104(%r13), %xmm7
	movups	%xmm1, 56(%r13)
	movups	%xmm0, 72(%r13)
	movq	112(%r12), %rdx
	movq	88(%r12), %xmm1
	movups	%xmm3, 72(%r12)
	movq	96(%r12), %rcx
	movq	104(%r12), %xmm0
	movq	%rdx, %xmm3
	movq	40(%r12), %rdx
	movq	%rax, 40(%r12)
	movq	48(%r12), %rax
	movups	%xmm2, 56(%r12)
	punpcklqdq	%xmm3, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, 40(%r13)
	movq	48(%r13), %rdx
	punpcklqdq	%xmm2, %xmm1
	movq	%rax, 48(%r13)
	movq	%rdx, 48(%r12)
	movups	%xmm6, 88(%r12)
	movups	%xmm7, 104(%r12)
	movups	%xmm1, 88(%r13)
	movups	%xmm0, 104(%r13)
.L3241:
	addq	$120, 8(%r14)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3240:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rdx
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.cfi_endproc
.LFE30250:
	.size	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE12emplace_backIJS2_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	.section	.text._ZNSt5dequeIiSaIiEE17_M_reallocate_mapEmb,"axG",@progbits,_ZNSt5dequeIiSaIiEE17_M_reallocate_mapEmb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIiSaIiEE17_M_reallocate_mapEmb
	.type	_ZNSt5dequeIiSaIiEE17_M_reallocate_mapEmb, @function
_ZNSt5dequeIiSaIiEE17_M_reallocate_mapEmb:
.LFB32394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	72(%rdi), %rax
	movq	40(%rdi), %r8
	movq	8(%rdi), %rbx
	movq	%rax, %r12
	subq	%r8, %r12
	movq	%r12, %rcx
	sarq	$3, %rcx
	leaq	1(%rsi,%rcx), %rcx
	leaq	(%rcx,%rcx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L3249
	subq	%rcx, %rbx
	shrq	%rbx
	salq	$3, %rbx
	testb	%dl, %dl
	leaq	(%rbx,%rsi,8), %rcx
	cmovne	%rcx, %rbx
	addq	$8, %rax
	addq	0(%r13), %rbx
	movq	%rax, %rdx
	subq	%r8, %rdx
	cmpq	%rbx, %r8
	jbe	.L3251
	cmpq	%rax, %r8
	je	.L3252
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L3252
	.p2align 4,,10
	.p2align 3
.L3249:
	cmpq	%rsi, %rbx
	movq	%rsi, %rax
	cmovnb	%rbx, %rax
	leaq	2(%rbx,%rax), %r14
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r14
	ja	.L3257
	leaq	0(,%r14,8), %rdi
	movl	%edx, -68(%rbp)
	movq	%r14, %rbx
	movq	%rsi, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movl	-68(%rbp), %edx
	movq	%rax, %r15
	subq	%rcx, %rbx
	shrq	%rbx
	salq	$3, %rbx
	testb	%dl, %dl
	leaq	(%rbx,%rsi,8), %rax
	movq	40(%r13), %rsi
	cmovne	%rax, %rbx
	movq	72(%r13), %rax
	leaq	8(%rax), %rdx
	addq	%r15, %rbx
	cmpq	%rsi, %rdx
	je	.L3255
	subq	%rsi, %rdx
	movq	%rbx, %rdi
	call	memmove@PLT
.L3255:
	movq	0(%r13), %rdi
	call	_ZdlPv@PLT
	movq	%r15, 0(%r13)
	movq	%r14, 8(%r13)
.L3252:
	movq	%rbx, 40(%r13)
	movq	(%rbx), %rax
	movq	(%rbx), %xmm0
	addq	%r12, %rbx
	addq	$512, %rax
	movq	%rbx, 72(%r13)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%r13)
	movq	(%rbx), %rax
	movq	%rax, 56(%r13)
	addq	$512, %rax
	movq	%rax, 64(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3251:
	.cfi_restore_state
	cmpq	%rax, %r8
	je	.L3252
	leaq	8(%r12), %rdi
	movq	%r8, %rsi
	subq	%rdx, %rdi
	addq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L3252
.L3257:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE32394:
	.size	_ZNSt5dequeIiSaIiEE17_M_reallocate_mapEmb, .-_ZNSt5dequeIiSaIiEE17_M_reallocate_mapEmb
	.section	.text._ZNSt5dequeIiSaIiEE12emplace_backIJiEEEvDpOT_,"axG",@progbits,_ZNSt5dequeIiSaIiEE12emplace_backIJiEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIiSaIiEE12emplace_backIJiEEEvDpOT_
	.type	_ZNSt5dequeIiSaIiEE12emplace_backIJiEEEvDpOT_, @function
_ZNSt5dequeIiSaIiEE12emplace_backIJiEEEvDpOT_:
.LFB31406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %rax
	movq	48(%rdi), %rdx
	subq	$4, %rax
	cmpq	%rax, %rdx
	je	.L3259
	movl	(%rsi), %eax
	movl	%eax, (%rdx)
	addq	$4, 48(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3259:
	.cfi_restore_state
	movq	72(%rdi), %r13
	subq	56(%rdi), %rdx
	sarq	$2, %rdx
	movq	%r13, %rax
	subq	40(%rdi), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$7, %rax
	addq	%rdx, %rax
	movq	32(%rdi), %rdx
	subq	16(%rdi), %rdx
	sarq	$2, %rdx
	addq	%rax, %rdx
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rdx
	je	.L3264
	movq	8(%rdi), %rcx
	movq	%r13, %rax
	subq	(%rdi), %rax
	sarq	$3, %rax
	subq	%rax, %rcx
	cmpq	$1, %rcx
	jbe	.L3265
.L3262:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r13)
	movl	(%r12), %edx
	movq	48(%rbx), %rax
	movl	%edx, (%rax)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3265:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIiSaIiEE17_M_reallocate_mapEmb
	movq	72(%rbx), %r13
	jmp	.L3262
.L3264:
	leaq	.LC75(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE31406:
	.size	_ZNSt5dequeIiSaIiEE12emplace_backIJiEEEvDpOT_, .-_ZNSt5dequeIiSaIiEE12emplace_backIJiEEEvDpOT_
	.section	.text._ZN2v88internal15TranslatedState23EnsureChildrenAllocatedEiPNS0_15TranslatedFrameEPiPSt5stackIiSt5dequeIiSaIiEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState23EnsureChildrenAllocatedEiPNS0_15TranslatedFrameEPiPSt5stackIiSt5dequeIiSaIiEEE
	.type	_ZN2v88internal15TranslatedState23EnsureChildrenAllocatedEiPNS0_15TranslatedFrameEPiPSt5stackIiSt5dequeIiSaIiEEE, @function
_ZN2v88internal15TranslatedState23EnsureChildrenAllocatedEiPNS0_15TranslatedFrameEPiPSt5stackIiSt5dequeIiSaIiEEE:
.LFB26025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -88(%rbp)
	movl	%esi, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r8, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L3266
	leaq	-60(%rbp), %rax
	movl	$0, -68(%rbp)
	movl	(%rcx), %r15d
	movq	%rdx, %r14
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L3287:
	movq	56(%r14), %r12
	movq	64(%r14), %rbx
	movslq	%r15d, %rcx
	movq	80(%r14), %r13
	movq	%r12, %rax
	subq	%rbx, %rax
	sarq	$5, %rax
	addq	%rcx, %rax
	js	.L3268
	cmpq	$15, %rax
	jg	.L3269
	salq	$5, %rcx
	leaq	(%r12,%rcx), %rax
.L3270:
	movzbl	(%rax), %ecx
	leal	-8(%rcx), %esi
	cmpb	$1, %sil
	jbe	.L3301
	cmpb	$2, 1(%rax)
	movl	%r15d, %r11d
	je	.L3276
	movq	%rax, %rdi
	call	_ZN2v88internal15TranslatedValue17MaterializeSimpleEv.part.0
.L3300:
	movq	-80(%rbp), %rax
	movq	56(%r14), %r12
	movq	64(%r14), %rbx
	movq	80(%r14), %r13
	movl	(%rax), %r11d
.L3276:
	movslq	%r11d, %rcx
	movl	$1, %esi
	movl	$1, %edi
	jmp	.L3279
	.p2align 4,,10
	.p2align 3
.L3303:
	cmpq	$15, %rax
	jg	.L3281
	movq	%rcx, %rax
	salq	$5, %rax
	leaq	(%r12,%rax), %r8
.L3282:
	movq	-80(%rbp), %rax
	leal	(%rdi,%rcx), %r15d
	subl	$1, %esi
	movl	%r15d, (%rax)
	cmpb	$8, (%r8)
	je	.L3302
	addq	$1, %rcx
	testl	%esi, %esi
	je	.L3278
.L3285:
	movq	56(%r14), %r12
	movq	64(%r14), %rbx
	movq	80(%r14), %r13
.L3279:
	movq	%r12, %rax
	subq	%rbx, %rax
	sarq	$5, %rax
	addq	%rcx, %rax
	jns	.L3303
	movq	%rax, %r8
	notq	%r8
	shrq	$4, %r8
	notq	%r8
.L3283:
	movq	%r8, %r9
	salq	$4, %r9
	subq	%r9, %rax
	salq	$5, %rax
	addq	0(%r13,%r8,8), %rax
	movq	%rax, %r8
	jmp	.L3282
	.p2align 4,,10
	.p2align 3
.L3302:
	addl	28(%r8), %esi
	addq	$1, %rcx
	testl	%esi, %esi
	jg	.L3285
.L3278:
	addl	$1, -68(%rbp)
	movl	-68(%rbp), %eax
	cmpl	%eax, -72(%rbp)
	jne	.L3287
.L3266:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3304
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3281:
	.cfi_restore_state
	movq	%rax, %r8
	sarq	$4, %r8
	jmp	.L3283
	.p2align 4,,10
	.p2align 3
.L3301:
	cmpb	$9, %cl
	jne	.L3273
	.p2align 4,,10
	.p2align 3
.L3274:
	movl	24(%rax), %esi
	movq	-88(%rbp), %rdi
	call	_ZN2v88internal15TranslatedState21GetValueByObjectIndexEi
	movzbl	(%rax), %ecx
	cmpb	$9, %cl
	je	.L3274
	cmpb	$8, %cl
	jne	.L3305
.L3273:
	cmpb	$0, 1(%rax)
	movl	%r15d, %r11d
	jne	.L3276
	movl	24(%rax), %ecx
	movq	-104(%rbp), %rsi
	movq	%rax, -112(%rbp)
	movq	-96(%rbp), %rdi
	movl	%ecx, -60(%rbp)
	call	_ZNSt5dequeIiSaIiEE12emplace_backIJiEEEvDpOT_
	movq	-112(%rbp), %rax
	movb	$1, 1(%rax)
	jmp	.L3300
	.p2align 4,,10
	.p2align 3
.L3269:
	movq	%rax, %rcx
	sarq	$4, %rcx
.L3271:
	movq	%rcx, %rsi
	salq	$4, %rsi
	subq	%rsi, %rax
	salq	$5, %rax
	addq	0(%r13,%rcx,8), %rax
	jmp	.L3270
	.p2align 4,,10
	.p2align 3
.L3268:
	movq	%rax, %rcx
	notq	%rcx
	shrq	$4, %rcx
	notq	%rcx
	jmp	.L3271
	.p2align 4,,10
	.p2align 3
.L3305:
	leaq	.LC64(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3304:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26025:
	.size	_ZN2v88internal15TranslatedState23EnsureChildrenAllocatedEiPNS0_15TranslatedFrameEPiPSt5stackIiSt5dequeIiSaIiEEE, .-_ZN2v88internal15TranslatedState23EnsureChildrenAllocatedEiPNS0_15TranslatedFrameEPiPSt5stackIiSt5dequeIiSaIiEEE
	.section	.text._ZN2v88internal15TranslatedState23EnsureObjectAllocatedAtEPNS0_15TranslatedValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState23EnsureObjectAllocatedAtEPNS0_15TranslatedValueE
	.type	_ZN2v88internal15TranslatedState23EnsureObjectAllocatedAtEPNS0_15TranslatedValueE, @function
_ZN2v88internal15TranslatedState23EnsureObjectAllocatedAtEPNS0_15TranslatedValueE:
.LFB26020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal15TranslatedState21ResolveCapturedObjectEPNS0_15TranslatedValueE
	cmpb	$0, 1(%rax)
	je	.L3320
.L3306:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3321
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3320:
	.cfi_restore_state
	leaq	-128(%rbp), %r12
	pxor	%xmm0, %xmm0
	movq	%rax, %r13
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	call	_ZNSt11_Deque_baseIiSaIiEE17_M_initialize_mapEm
	movl	24(%r13), %eax
	movq	%r12, %rdi
	leaq	-132(%rbp), %rsi
	movl	%eax, -132(%rbp)
	call	_ZNSt5dequeIiSaIiEE12emplace_backIJiEEEvDpOT_
	movb	$1, 1(%r13)
	movq	-80(%rbp), %rdi
	cmpq	%rdi, -112(%rbp)
	je	.L3313
	.p2align 4,,10
	.p2align 3
.L3308:
	cmpq	-72(%rbp), %rdi
	je	.L3322
	movl	-4(%rdi), %esi
	subq	$4, %rdi
	movq	%r12, %rdx
	movq	%rdi, -80(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal15TranslatedState31EnsureCapturedObjectAllocatedAtEiPSt5stackIiSt5dequeIiSaIiEEE
	movq	-80(%rbp), %rdi
	cmpq	%rdi, -112(%rbp)
	jne	.L3308
.L3313:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3306
	movq	-56(%rbp), %rax
	movq	-88(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L3314
	.p2align 4,,10
	.p2align 3
.L3315:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L3315
	movq	-128(%rbp), %rdi
.L3314:
	call	_ZdlPv@PLT
	jmp	.L3306
	.p2align 4,,10
	.p2align 3
.L3322:
	movq	-56(%rbp), %rax
	movq	-8(%rax), %rax
	movl	508(%rax), %r13d
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%rbx, %rdi
	movl	%r13d, %esi
	leaq	-8(%rax), %rdx
	movq	%rdx, -56(%rbp)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -72(%rbp)
	addq	$508, %rax
	movq	%rdx, -64(%rbp)
	movq	%r12, %rdx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal15TranslatedState31EnsureCapturedObjectAllocatedAtEiPSt5stackIiSt5dequeIiSaIiEEE
	movq	-80(%rbp), %rdi
	cmpq	-112(%rbp), %rdi
	jne	.L3308
	jmp	.L3313
.L3321:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26020:
	.size	_ZN2v88internal15TranslatedState23EnsureObjectAllocatedAtEPNS0_15TranslatedValueE, .-_ZN2v88internal15TranslatedState23EnsureObjectAllocatedAtEPNS0_15TranslatedValueE
	.section	.rodata._ZN2v88internal15TranslatedValue8GetValueEv.str1.1,"aMS",@progbits,1
.LC177:
	.string	"unexpected case"
.LC178:
	.string	"internal error: value missing"
	.section	.text._ZN2v88internal15TranslatedValue8GetValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedValue8GetValueEv
	.type	_ZN2v88internal15TranslatedValue8GetValueEv, @function
_ZN2v88internal15TranslatedValue8GetValueEv:
.LFB25982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$2, 1(%rdi)
	je	.L3334
	movzbl	(%rdi), %eax
	cmpb	$7, %al
	ja	.L3326
	testb	%al, %al
	jne	.L3335
	leaq	.LC177(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3326:
	subl	$8, %eax
	cmpb	$1, %al
	ja	.L3336
	movq	8(%rdi), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal15TranslatedState23EnsureObjectAllocatedAtEPNS0_15TranslatedValueE
	movq	8(%r12), %rdi
	addq	$8, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15TranslatedState18InitializeObjectAtEPNS0_15TranslatedValueE
	.p2align 4,,10
	.p2align 3
.L3335:
	.cfi_restore_state
	call	_ZN2v88internal15TranslatedValue17MaterializeSimpleEv.part.0
.L3334:
	movq	16(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3336:
	.cfi_restore_state
	leaq	.LC178(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25982:
	.size	_ZN2v88internal15TranslatedValue8GetValueEv, .-_ZN2v88internal15TranslatedValue8GetValueEv
	.section	.rodata._ZN2v88internal11Deoptimizer22MaterializeHeapObjectsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC179:
	.string	"Materialization [0x%012lx] <- 0x%012lx ;  "
	.section	.rodata._ZN2v88internal11Deoptimizer22MaterializeHeapObjectsEv.str1.1,"aMS",@progbits,1
.LC180:
	.string	"\n"
.LC181:
	.string	"Feedback updated"
.LC182:
	.string	" from deoptimization at "
	.section	.text._ZN2v88internal11Deoptimizer22MaterializeHeapObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer22MaterializeHeapObjectsEv
	.type	_ZN2v88internal11Deoptimizer22MaterializeHeapObjectsEv, @function
_ZN2v88internal11Deoptimizer22MaterializeHeapObjectsEv:
.LFB25861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	128(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	128(%rdi), %rbx
	movq	136(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	120(%rdi), %r15
	cmpq	%r12, %rbx
	je	.L3338
	.p2align 4,,10
	.p2align 3
.L3339:
	movq	%rbx, %rdi
	addq	$120, %rbx
	call	_ZN2v88internal15TranslatedFrame8HandlifyEv
	cmpq	%rbx, %r12
	jne	.L3339
.L3338:
	movq	264(%r13), %rsi
	testq	%rsi, %rsi
	je	.L3340
	movq	152(%r13), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3341
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3342:
	movq	%rax, 256(%r13)
	movq	$0, 264(%r13)
.L3340:
	movq	%r15, 160(%r13)
	movq	%r14, %rdi
	call	_ZN2v88internal15TranslatedState39UpdateFromPreviouslyMaterializedObjectsEv
	movl	_ZN2v88internal24FLAG_deopt_every_n_timesE(%rip), %eax
	testl	%eax, %eax
	jle	.L3344
	movq	0(%r13), %rax
	xorl	%ecx, %ecx
	movl	$21, %edx
	xorl	%esi, %esi
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap17CollectAllGarbageEiNS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
.L3344:
	movq	280(%r13), %rbx
	movq	288(%r13), %r14
	cmpq	%r14, %rbx
	je	.L3345
	leaq	-80(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L3348:
	movq	8(%rbx), %rdi
	call	_ZN2v88internal15TranslatedValue8GetValueEv
	cmpq	$0, 304(%r13)
	movq	%rax, %r12
	je	.L3346
	movq	(%rax), %rdx
	movq	(%rbx), %rsi
	leaq	.LC179(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movq	304(%r13), %rax
	movq	(%rax), %rax
	movq	144(%rax), %rsi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movq	304(%r13), %rax
	leaq	.LC180(%rip), %rsi
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L3346:
	movq	(%rbx), %rax
	movq	(%r12), %rdx
	addq	$48, %rbx
	movq	%rdx, (%rax)
	cmpq	%rbx, %r14
	jne	.L3348
.L3345:
	cmpq	$0, 256(%r13)
	je	.L3350
	cmpl	$-1, 272(%r13)
	je	.L3365
	movq	152(%r13), %rdi
	movl	$47, %esi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	256(%r13), %rdx
	movl	272(%r13), %esi
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movl	%esi, -64(%rbp)
	testq	%rdx, %rdx
	je	.L3352
	movq	(%rdx), %rax
	leaq	-88(%rbp), %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
.L3352:
	leaq	-80(%rbp), %rdi
	movl	$1, %esi
	movl	%eax, -60(%rbp)
	call	_ZN2v88internal13FeedbackNexus18SetSpeculationModeENS0_15SpeculationModeE@PLT
	movq	304(%r13), %rax
	testq	%rax, %rax
	je	.L3350
	movq	(%rax), %rax
	leaq	.LC181(%rip), %rsi
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	304(%r13), %rax
	movq	32(%r13), %rcx
	leaq	16(%r13), %rdi
	leaq	.LC182(%rip), %rdx
	movq	(%rax), %rax
	movq	144(%rax), %rsi
	call	_ZN2v88internal4Code18PrintDeoptLocationEP8_IO_FILEPKcm@PLT
.L3350:
	movq	0(%r13), %rax
	movq	120(%r13), %rsi
	movq	41056(%rax), %rdi
	call	_ZN2v88internal23MaterializedObjectStore6RemoveEm
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3366
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3341:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L3367
.L3343:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L3342
.L3365:
	leaq	.LC72(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3367:
	movq	%rbx, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	jmp	.L3343
.L3366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25861:
	.size	_ZN2v88internal11Deoptimizer22MaterializeHeapObjectsEv, .-_ZN2v88internal11Deoptimizer22MaterializeHeapObjectsEv
	.section	.rodata._ZN2v88internal15TranslatedState26InitializeCapturedObjectAtEiPSt5stackIiSt5dequeIiSaIiEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE.str1.8,"aMS",@progbits,1
	.align 8
.LC183:
	.string	"TranslatedValue::kFinished == slot->materialization_state()"
	.align 8
.LC184:
	.string	"frame->values_[value_index].kind() == TranslatedValue::kTagged"
	.section	.rodata._ZN2v88internal15TranslatedState26InitializeCapturedObjectAtEiPSt5stackIiSt5dequeIiSaIiEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE.str1.1,"aMS",@progbits,1
.LC185:
	.string	"map->IsMap()"
.LC186:
	.string	"map->IsJSObjectMap()"
	.section	.rodata._ZN2v88internal15TranslatedState26InitializeCapturedObjectAtEiPSt5stackIiSt5dequeIiSaIiEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE.str1.8
	.align 8
.LC187:
	.string	"value_index == children_init_index"
	.section	.text._ZN2v88internal15TranslatedState26InitializeCapturedObjectAtEiPSt5stackIiSt5dequeIiSaIiEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState26InitializeCapturedObjectAtEiPSt5stackIiSt5dequeIiSaIiEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	.type	_ZN2v88internal15TranslatedState26InitializeCapturedObjectAtEiPSt5stackIiSt5dequeIiSaIiEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE, @function
_ZN2v88internal15TranslatedState26InitializeCapturedObjectAtEiPSt5stackIiSt5dequeIiSaIiEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE:
.LFB26019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -160(%rbp)
	movq	88(%rdi), %r8
	movq	120(%r15), %rdx
	movq	64(%rdi), %rdi
	movq	%rcx, -176(%rbp)
	subq	%r8, %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	sarq	$3, %rdx
	movq	96(%r15), %rax
	subq	104(%r15), %rax
	subq	$1, %rdx
	sarq	$3, %rax
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	80(%r15), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rax, %rsi
	jnb	.L3419
	movq	%rdi, %rax
	subq	72(%r15), %rax
	sarq	$3, %rax
	addq	%rsi, %rax
	js	.L3370
	cmpq	$63, %rax
	jle	.L3420
	movq	%rax, %rdx
	sarq	$6, %rdx
.L3373:
	movq	%rdx, %rcx
	movq	(%r8,%rdx,8), %rdx
	salq	$6, %rcx
	subq	%rcx, %rax
	leaq	(%rdx,%rax,8), %rdx
.L3372:
	movslq	(%rdx), %rax
	movslq	4(%rdx), %rdx
	leaq	-96(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	%rax, %rcx
	movl	%edx, -100(%rbp)
	salq	$4, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	(%r15), %rcx
	leaq	(%rcx,%rax,8), %r14
	leaq	56(%r14), %rsi
	movq	%rsi, -168(%rbp)
	call	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl
	movl	-100(%rbp), %eax
	movq	-96(%rbp), %rcx
	leal	1(%rax), %r9d
	movl	%r9d, -100(%rbp)
	cmpb	$2, 1(%rcx)
	jne	.L3421
	xorl	%edx, %edx
	cmpb	$8, (%rcx)
	jne	.L3422
	movl	%edx, %r13d
	movl	%r9d, %ebx
	movq	%r15, %rdx
	movq	%rcx, %r15
	.p2align 4,,10
	.p2align 3
.L3375:
	cmpl	%r13d, 28(%r15)
	jle	.L3404
	movq	56(%r14), %r8
	movslq	%ebx, %rcx
	movq	80(%r14), %r12
	movq	%rcx, %rsi
	movq	%r8, %rax
	subq	64(%r14), %rax
	sarq	$5, %rax
	movq	%rax, %r10
	addq	%rax, %rsi
	js	.L3377
	cmpq	$15, %rsi
	jg	.L3378
	movq	%rcx, %rsi
	salq	$5, %rsi
	addq	%r8, %rsi
.L3379:
	movzbl	(%rsi), %eax
	subl	$8, %eax
	cmpb	$1, %al
	jbe	.L3423
.L3381:
	leaq	(%r10,%rcx), %rax
	salq	$5, %rcx
	leal	1(%rbx), %r10d
	leaq	(%r8,%rcx), %rsi
	subl	%eax, %r10d
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L3383:
	testq	%rax, %rax
	js	.L3384
.L3428:
	movq	%rsi, %rcx
	cmpq	$15, %rax
	jle	.L3386
	movq	%rax, %rdi
	sarq	$4, %rdi
.L3387:
	movq	%rdi, %rcx
	movq	%rax, %rbx
	salq	$4, %rcx
	subq	%rcx, %rbx
	movq	%rbx, %rcx
	salq	$5, %rcx
	addq	(%r12,%rdi,8), %rcx
.L3386:
	subl	$1, %r8d
	cmpb	$8, (%rcx)
	leal	(%r10,%rax), %r9d
	je	.L3424
	addq	$1, %rax
	addq	$32, %rsi
	testl	%r8d, %r8d
	jne	.L3383
.L3382:
	addl	$1, %r13d
	cmpb	$8, (%r15)
	je	.L3405
	movq	%r15, %rcx
	movl	%r9d, %r13d
	movq	%rdx, %r15
.L3376:
	movslq	-100(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%rcx, -120(%rbp)
	movq	-152(%rbp), %rdi
	call	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl
	movq	-96(%rbp), %rax
	movq	-120(%rbp), %rcx
	cmpb	$1, (%rax)
	jne	.L3425
	movslq	-100(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%rcx, -120(%rbp)
	movq	-152(%rbp), %rdi
	call	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl
	movq	-96(%rbp), %rdi
	call	_ZN2v88internal15TranslatedValue8GetValueEv
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movq	-120(%rbp), %rcx
	cmpw	$68, 11(%rax)
	jne	.L3426
	addl	$1, -100(%rbp)
	movq	(%r8), %rax
	movzwl	11(%rax), %edx
	cmpw	$158, %dx
	ja	.L3392
	cmpw	$122, %dx
	ja	.L3393
	cmpw	$65, %dx
	je	.L3368
	cmpw	$74, %dx
	jne	.L3395
.L3368:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3427
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3420:
	.cfi_restore_state
	leaq	(%rdi,%rsi,8), %rdx
	jmp	.L3372
	.p2align 4,,10
	.p2align 3
.L3392:
	cmpw	$1024, %dx
	jbe	.L3395
	movq	-176(%rbp), %r9
	leaq	-100(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15TranslatedState20InitializeJSObjectAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
.L3398:
	cmpl	%r13d, -100(%rbp)
	je	.L3368
	leaq	.LC187(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3393:
	subl	$123, %edx
	cmpw	$35, %dx
	ja	.L3395
	leaq	.L3397(%rip), %rsi
	movzwl	%dx, %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal15TranslatedState26InitializeCapturedObjectAtEiPSt5stackIiSt5dequeIiSaIiEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE,"a",@progbits
	.align 4
	.align 4
.L3397:
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3395-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3395-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3395-.L3397
	.long	.L3395-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3396-.L3397
	.long	.L3395-.L3397
	.long	.L3395-.L3397
	.long	.L3395-.L3397
	.long	.L3395-.L3397
	.long	.L3395-.L3397
	.long	.L3395-.L3397
	.long	.L3395-.L3397
	.long	.L3395-.L3397
	.long	.L3395-.L3397
	.long	.L3395-.L3397
	.long	.L3396-.L3397
	.section	.text._ZN2v88internal15TranslatedState26InitializeCapturedObjectAtEiPSt5stackIiSt5dequeIiSaIiEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	.p2align 4,,10
	.p2align 3
.L3424:
	addl	28(%rcx), %r8d
	addq	$1, %rax
	addq	$32, %rsi
	testl	%r8d, %r8d
	jle	.L3382
	testq	%rax, %rax
	jns	.L3428
.L3384:
	movq	%rax, %rdi
	notq	%rdi
	shrq	$4, %rdi
	notq	%rdi
	jmp	.L3387
	.p2align 4,,10
	.p2align 3
.L3405:
	movl	%r9d, %ebx
	jmp	.L3375
	.p2align 4,,10
	.p2align 3
.L3423:
	movq	%rdx, %rdi
	movq	%rcx, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%r10, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal15TranslatedState21ResolveCapturedObjectEPNS0_15TranslatedValueE
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r10
	cmpb	$2, 1(%rax)
	movq	-136(%rbp), %r8
	movq	-144(%rbp), %rcx
	je	.L3381
	movl	24(%rax), %esi
	movq	-160(%rbp), %rdi
	movq	%rdx, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movl	%esi, -96(%rbp)
	movq	-152(%rbp), %rsi
	movq	%rax, -120(%rbp)
	call	_ZNSt5dequeIiSaIiEE12emplace_backIJiEEEvDpOT_
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movb	$2, 1(%rax)
	movq	56(%r14), %r8
	movq	80(%r14), %r12
	movq	%r8, %rax
	subq	64(%r14), %rax
	sarq	$5, %rax
	movq	%rax, %r10
	jmp	.L3381
	.p2align 4,,10
	.p2align 3
.L3378:
	movq	%rsi, %rax
	sarq	$4, %rax
.L3380:
	movq	%rax, %rdi
	salq	$4, %rdi
	subq	%rdi, %rsi
	salq	$5, %rsi
	addq	(%r12,%rax,8), %rsi
	jmp	.L3379
	.p2align 4,,10
	.p2align 3
.L3377:
	movq	%rsi, %rax
	notq	%rax
	shrq	$4, %rax
	notq	%rax
	jmp	.L3380
	.p2align 4,,10
	.p2align 3
.L3370:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L3373
.L3396:
	movq	-176(%rbp), %r9
	leaq	-100(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15TranslatedState34InitializeObjectWithTaggedFieldsAtEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	jmp	.L3398
	.p2align 4,,10
	.p2align 3
.L3419:
	leaq	.LC59(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3421:
	leaq	.LC183(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3422:
	leaq	.LC64(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3425:
	leaq	.LC184(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3426:
	leaq	.LC185(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3395:
	leaq	.LC186(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3404:
	movq	%r15, %rcx
	movl	%ebx, %r13d
	movq	%rdx, %r15
	jmp	.L3376
.L3427:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26019:
	.size	_ZN2v88internal15TranslatedState26InitializeCapturedObjectAtEiPSt5stackIiSt5dequeIiSaIiEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE, .-_ZN2v88internal15TranslatedState26InitializeCapturedObjectAtEiPSt5stackIiSt5dequeIiSaIiEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	.section	.text._ZN2v88internal15TranslatedState18InitializeObjectAtEPNS0_15TranslatedValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState18InitializeObjectAtEPNS0_15TranslatedValueE
	.type	_ZN2v88internal15TranslatedState18InitializeObjectAtEPNS0_15TranslatedValueE, @function
_ZN2v88internal15TranslatedState18InitializeObjectAtEPNS0_15TranslatedValueE:
.LFB26018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal15TranslatedState21ResolveCapturedObjectEPNS0_15TranslatedValueE
	cmpb	$2, 1(%rax)
	movq	%rax, %r12
	jne	.L3443
.L3430:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	16(%r12), %rax
	jne	.L3444
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3443:
	.cfi_restore_state
	leaq	-144(%rbp), %r13
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	$0, -144(%rbp)
	movq	%r13, %rdi
	movaps	%xmm0, -128(%rbp)
	leaq	-148(%rbp), %r14
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIiSaIiEE17_M_initialize_mapEm
	movl	24(%r12), %eax
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	%eax, -148(%rbp)
	call	_ZNSt5dequeIiSaIiEE12emplace_backIJiEEEvDpOT_
	movb	$2, 1(%r12)
	movq	-96(%rbp), %rdi
	cmpq	%rdi, -128(%rbp)
	je	.L3436
	.p2align 4,,10
	.p2align 3
.L3431:
	cmpq	-88(%rbp), %rdi
	je	.L3445
	movl	-4(%rdi), %esi
	subq	$4, %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rdi, -96(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal15TranslatedState26InitializeCapturedObjectAtEiPSt5stackIiSt5dequeIiSaIiEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	-96(%rbp), %rdi
	cmpq	%rdi, -128(%rbp)
	jne	.L3431
.L3436:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3430
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L3437
	.p2align 4,,10
	.p2align 3
.L3438:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L3438
	movq	-144(%rbp), %rdi
.L3437:
	call	_ZdlPv@PLT
	jmp	.L3430
	.p2align 4,,10
	.p2align 3
.L3445:
	movq	-72(%rbp), %rax
	movq	-8(%rax), %rax
	movl	508(%rax), %r15d
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rax
	movq	%rbx, %rdi
	movq	%r14, %rcx
	movl	%r15d, %esi
	leaq	-8(%rax), %rdx
	movq	%rdx, -72(%rbp)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -88(%rbp)
	addq	$508, %rax
	movq	%rdx, -80(%rbp)
	movq	%r13, %rdx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal15TranslatedState26InitializeCapturedObjectAtEiPSt5stackIiSt5dequeIiSaIiEEERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	jne	.L3431
	jmp	.L3436
.L3444:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26018:
	.size	_ZN2v88internal15TranslatedState18InitializeObjectAtEPNS0_15TranslatedValueE, .-_ZN2v88internal15TranslatedState18InitializeObjectAtEPNS0_15TranslatedValueE
	.section	.rodata._ZN2v88internal15TranslatedState27MaterializeFixedDoubleArrayEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE.str1.1,"aMS",@progbits,1
.LC188:
	.string	"length > 0"
	.section	.rodata._ZN2v88internal15TranslatedState27MaterializeFixedDoubleArrayEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC189:
	.string	"TranslatedValue::kCapturedObject != frame->values_[*value_index].kind()"
	.align 8
.LC190:
	.string	"value.is_identical_to(isolate()->factory()->the_hole_value())"
	.section	.text._ZN2v88internal15TranslatedState27MaterializeFixedDoubleArrayEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState27MaterializeFixedDoubleArrayEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE
	.type	_ZN2v88internal15TranslatedState27MaterializeFixedDoubleArrayEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal15TranslatedState27MaterializeFixedDoubleArrayEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE:
.LFB26021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -72(%rbp)
	movq	56(%rsi), %rcx
	movq	%rdi, -64(%rbp)
	movslq	(%rdx), %rdi
	movq	%rcx, %rax
	subq	64(%rsi), %rax
	sarq	$5, %rax
	movq	%rdi, %rdx
	addq	%rdi, %rax
	js	.L3447
	cmpq	$15, %rax
	jle	.L3473
	movq	%rax, %rcx
	sarq	$4, %rcx
.L3450:
	movq	%rcx, %rdi
	movq	80(%r12), %rsi
	salq	$4, %rdi
	subq	%rdi, %rax
	salq	$5, %rax
	addq	(%rsi,%rcx,8), %rax
	movq	%rax, %rdi
	cmpb	$2, 1(%rdi)
	je	.L3474
.L3451:
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movl	0(%r13), %edx
	movq	%rax, %r14
.L3452:
	movq	-64(%rbp), %rax
	addl	$1, %edx
	sarq	$32, %r14
	movl	%edx, 0(%r13)
	movl	%r14d, %esi
	xorl	%edx, %edx
	movq	24(%rax), %rdi
	movl	%r14d, -52(%rbp)
	call	_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	testq	%r14, %r14
	jle	.L3475
	movq	%r13, %rax
	movl	0(%r13), %ecx
	xorl	%r15d, %r15d
	movq	%r12, %r13
	movabsq	$9221120237041090560, %r14
	movq	%rax, %r12
	jmp	.L3468
	.p2align 4,,10
	.p2align 3
.L3460:
	sarq	$32, %rcx
	pxor	%xmm0, %xmm0
	movq	(%rbx), %rdi
	cvtsi2sdl	%ecx, %xmm0
.L3463:
	ucomisd	%xmm0, %xmm0
	movq	%xmm0, %rcx
	leal	16(,%r15,8), %eax
	cltq
	cmovp	%r14, %rcx
	movq	%rcx, -1(%rdi,%rax)
.L3466:
	movl	(%r12), %eax
	addl	$1, %r15d
	leal	1(%rax), %ecx
	movl	%ecx, (%r12)
	cmpl	-52(%rbp), %r15d
	jge	.L3476
.L3468:
	movq	56(%r13), %r8
	movslq	%ecx, %rcx
	movq	%r8, %rax
	subq	64(%r13), %rax
	sarq	$5, %rax
	addq	%rcx, %rax
	js	.L3454
	cmpq	$15, %rax
	jg	.L3455
	movq	%rcx, %rax
	salq	$5, %rax
	leaq	(%r8,%rax), %rdi
	cmpb	$8, (%rdi)
	je	.L3458
.L3459:
	call	_ZN2v88internal15TranslatedValue8GetValueEv
	movq	(%rax), %rcx
	testb	$1, %cl
	je	.L3460
	movq	-1(%rcx), %rdi
	cmpw	$65, 11(%rdi)
	je	.L3477
	movq	-64(%rbp), %rdx
	movq	24(%rdx), %rdi
	leaq	96(%rdi), %r8
	cmpq	%r8, %rax
	je	.L3467
	cmpq	96(%rdi), %rcx
	jne	.L3478
.L3467:
	movq	(%rbx), %rcx
	leal	16(,%r15,8), %eax
	movabsq	$-2251799814209537, %rdx
	cltq
	movq	%rdx, -1(%rax,%rcx)
	jmp	.L3466
	.p2align 4,,10
	.p2align 3
.L3473:
	salq	$5, %rdi
	addq	%rcx, %rdi
	cmpb	$2, 1(%rdi)
	jne	.L3451
.L3474:
	movq	16(%rdi), %rax
	movq	(%rax), %r14
	jmp	.L3452
	.p2align 4,,10
	.p2align 3
.L3455:
	movq	%rax, %r9
	sarq	$4, %r9
.L3457:
	movq	%r9, %rdi
	movq	%rax, %rsi
	movq	80(%r13), %r10
	salq	$4, %rdi
	subq	%rdi, %rsi
	movq	%rsi, %rdi
	salq	$5, %rdi
	addq	(%r10,%r9,8), %rdi
	cmpb	$8, (%rdi)
	je	.L3458
	cmpq	$15, %rax
	ja	.L3459
	movq	%rcx, %rax
	salq	$5, %rax
	leaq	(%r8,%rax), %rdi
	jmp	.L3459
	.p2align 4,,10
	.p2align 3
.L3477:
	movq	(%rbx), %rdi
	movsd	7(%rcx), %xmm0
	jmp	.L3463
	.p2align 4,,10
	.p2align 3
.L3454:
	movq	%rax, %r9
	notq	%r9
	shrq	$4, %r9
	notq	%r9
	jmp	.L3457
	.p2align 4,,10
	.p2align 3
.L3458:
	leaq	.LC189(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3447:
	movq	%rax, %rcx
	notq	%rcx
	shrq	$4, %rcx
	notq	%rcx
	jmp	.L3450
	.p2align 4,,10
	.p2align 3
.L3476:
	movq	-72(%rbp), %rax
	movq	%rbx, 16(%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3478:
	.cfi_restore_state
	leaq	.LC190(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3475:
	leaq	.LC188(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE26021:
	.size	_ZN2v88internal15TranslatedState27MaterializeFixedDoubleArrayEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal15TranslatedState27MaterializeFixedDoubleArrayEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE
	.section	.rodata._ZN2v88internal15TranslatedState21MaterializeHeapNumberEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueE.str1.1,"aMS",@progbits,1
.LC191:
	.string	"value->IsNumber()"
	.section	.text._ZN2v88internal15TranslatedState21MaterializeHeapNumberEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState21MaterializeHeapNumberEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueE
	.type	_ZN2v88internal15TranslatedState21MaterializeHeapNumberEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueE, @function
_ZN2v88internal15TranslatedState21MaterializeHeapNumberEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueE:
.LFB26022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$8, %rsp
	movq	56(%rsi), %rcx
	movslq	(%rdx), %rdx
	movq	%rcx, %rax
	subq	64(%rsi), %rax
	sarq	$5, %rax
	addq	%rdx, %rax
	js	.L3480
	cmpq	$15, %rax
	jle	.L3493
	movq	%rax, %r8
	sarq	$4, %r8
.L3483:
	movq	%r8, %rdi
	movq	%rax, %r9
	movq	80(%rsi), %rsi
	salq	$4, %rdi
	subq	%rdi, %r9
	movq	%r9, %rdi
	salq	$5, %rdi
	addq	(%rsi,%r8,8), %rdi
	cmpb	$8, (%rdi)
	je	.L3484
	cmpq	$15, %rax
	jbe	.L3494
.L3485:
	call	_ZN2v88internal15TranslatedValue8GetValueEv
	movq	(%rax), %rax
	testb	$1, %al
	je	.L3486
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L3495
	movq	24(%r12), %rdi
	movq	7(%rax), %r12
.L3489:
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%r12, 7(%rdx)
	addl	$1, 0(%r13)
	movq	%rax, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3493:
	.cfi_restore_state
	salq	$5, %rdx
	movq	%rdx, %rdi
	addq	%rcx, %rdi
	cmpb	$8, (%rdi)
	jne	.L3485
.L3484:
	leaq	.LC189(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3486:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	movq	24(%r12), %rdi
	cvtsi2sdl	%eax, %xmm0
	movq	%xmm0, %r12
	jmp	.L3489
	.p2align 4,,10
	.p2align 3
.L3480:
	movq	%rax, %r8
	notq	%r8
	shrq	$4, %r8
	notq	%r8
	jmp	.L3483
	.p2align 4,,10
	.p2align 3
.L3495:
	leaq	.LC191(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3494:
	salq	$5, %rdx
	movq	%rdx, %rdi
	addq	%rcx, %rdi
	jmp	.L3485
	.cfi_endproc
.LFE26022:
	.size	_ZN2v88internal15TranslatedState21MaterializeHeapNumberEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueE, .-_ZN2v88internal15TranslatedState21MaterializeHeapNumberEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueE
	.section	.rodata._ZN2v88internal15TranslatedState31EnsureCapturedObjectAllocatedAtEiPSt5stackIiSt5dequeIiSaIiEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC192:
	.string	"TranslatedValue::kAllocated == slot->materialization_state()"
	.align 8
.LC193:
	.string	"instance_size == slot->GetChildrenCount() * kTaggedSize"
	.section	.text._ZN2v88internal15TranslatedState31EnsureCapturedObjectAllocatedAtEiPSt5stackIiSt5dequeIiSaIiEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState31EnsureCapturedObjectAllocatedAtEiPSt5stackIiSt5dequeIiSaIiEEE
	.type	_ZN2v88internal15TranslatedState31EnsureCapturedObjectAllocatedAtEiPSt5stackIiSt5dequeIiSaIiEEE, @function
_ZN2v88internal15TranslatedState31EnsureCapturedObjectAllocatedAtEiPSt5stackIiSt5dequeIiSaIiEEE:
.LFB26024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -120(%rbp)
	movq	88(%rdi), %rdi
	movq	120(%r12), %rdx
	movq	64(%r12), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	subq	%rdi, %rdx
	movq	96(%r12), %rax
	subq	104(%r12), %rax
	sarq	$3, %rdx
	sarq	$3, %rax
	subq	$1, %rdx
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	80(%r12), %rax
	subq	%r8, %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rax, %rsi
	jnb	.L3548
	movq	%r8, %rax
	subq	72(%r12), %rax
	sarq	$3, %rax
	addq	%rsi, %rax
	js	.L3498
	cmpq	$63, %rax
	jle	.L3549
	movq	%rax, %rdx
	sarq	$6, %rdx
.L3501:
	movq	%rdx, %rcx
	movq	(%rdi,%rdx,8), %rdx
	salq	$6, %rcx
	subq	%rcx, %rax
	leaq	(%rdx,%rax,8), %rdx
.L3500:
	movslq	(%rdx), %rax
	leaq	-96(%rbp), %r15
	movslq	4(%rdx), %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	movl	%edx, -100(%rbp)
	salq	$4, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	(%r12), %rcx
	leaq	(%rcx,%rax,8), %r13
	leaq	56(%r13), %rbx
	movq	%rbx, %rsi
	call	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl
	movl	-100(%rbp), %eax
	movq	-96(%rbp), %r14
	leal	1(%rax), %edx
	movl	%edx, -100(%rbp)
	cmpb	$1, 1(%r14)
	jne	.L3550
	cmpb	$8, (%r14)
	jne	.L3551
	movslq	%edx, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl
	movq	-96(%rbp), %rax
	cmpb	$1, (%rax)
	jne	.L3552
	movslq	-100(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl
	movq	-96(%rbp), %rdi
	call	_ZN2v88internal15TranslatedValue8GetValueEv
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L3553
	movl	-100(%rbp), %eax
	leal	1(%rax), %edx
	movl	%edx, -100(%rbp)
	movq	(%r8), %rax
	movzwl	11(%rax), %eax
	cmpw	$158, %ax
	ja	.L3506
	cmpw	$122, %ax
	ja	.L3507
	cmpw	$65, %ax
	je	.L3508
	cmpw	$74, %ax
	jne	.L3510
	leaq	-100(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState27MaterializeFixedDoubleArrayEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE
.L3496:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3554
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3549:
	.cfi_restore_state
	leaq	(%r8,%rsi,8), %rdx
	jmp	.L3500
	.p2align 4,,10
	.p2align 3
.L3507:
	subl	$123, %eax
	cmpw	$35, %ax
	ja	.L3510
	leaq	.L3512(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal15TranslatedState31EnsureCapturedObjectAllocatedAtEiPSt5stackIiSt5dequeIiSaIiEEE,"a",@progbits
	.align 4
	.align 4
.L3512:
	.long	.L3513-.L3512
	.long	.L3510-.L3512
	.long	.L3510-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3510-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3510-.L3512
	.long	.L3510-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3513-.L3512
	.long	.L3510-.L3512
	.long	.L3510-.L3512
	.long	.L3510-.L3512
	.long	.L3510-.L3512
	.long	.L3510-.L3512
	.long	.L3510-.L3512
	.long	.L3510-.L3512
	.long	.L3510-.L3512
	.long	.L3510-.L3512
	.long	.L3510-.L3512
	.long	.L3511-.L3512
	.section	.text._ZN2v88internal15TranslatedState31EnsureCapturedObjectAllocatedAtEiPSt5stackIiSt5dequeIiSaIiEEE
	.p2align 4,,10
	.p2align 3
.L3506:
	cmpw	$1024, %ax
	jbe	.L3510
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal15TranslatedState23EnsureJSObjectAllocatedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE
	movslq	-100(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl
	movq	-96(%rbp), %r15
	addl	$1, -100(%rbp)
	leaq	-100(%rbp), %rcx
	movq	-128(%rbp), %r8
	cmpb	$8, (%r15)
	je	.L3555
.L3527:
	cmpb	$8, (%r14)
	movl	$-2, %esi
	jne	.L3529
	movl	28(%r14), %esi
	subl	$2, %esi
.L3529:
	movq	-120(%rbp), %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState23EnsureChildrenAllocatedEiPNS0_15TranslatedFrameEPiPSt5stackIiSt5dequeIiSaIiEEE
	jmp	.L3496
	.p2align 4,,10
	.p2align 3
.L3498:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L3501
.L3511:
	movq	%r15, %rdi
	movslq	%edx, %rdx
	movq	%rbx, %rsi
	call	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl
	movq	-96(%rbp), %rdi
	cmpb	$2, 1(%rdi)
	je	.L3556
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
.L3523:
	sarq	$32, %rax
	andl	$1023, %eax
	addl	$2, %eax
	cmpb	$8, (%r14)
	je	.L3557
.L3524:
	leaq	.LC193(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3513:
	movq	%r15, %rdi
	movslq	%edx, %rdx
	movq	%rbx, %rsi
	movq	%r8, -128(%rbp)
	call	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedValueERS2_PS2_EplEl
	movq	-96(%rbp), %rdi
	movq	-128(%rbp), %r8
	cmpb	$2, 1(%rdi)
	je	.L3558
	movq	%r8, -128(%rbp)
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	-128(%rbp), %r8
.L3516:
	sarq	$32, %rax
	xorl	%edx, %edx
	cmpb	$8, (%r14)
	leal	2(%rax), %ecx
	jne	.L3517
	movl	28(%r14), %edx
.L3517:
	cmpl	%ecx, %edx
	jne	.L3524
	movq	24(%r12), %rdx
	movq	288(%rdx), %rcx
	movq	-1(%rcx), %rcx
	cmpq	%rcx, (%r8)
	jne	.L3547
	testq	%rax, %rax
	je	.L3559
.L3547:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState18AllocateStorageForEPNS0_15TranslatedValueE
	movq	%rax, 16(%r14)
.L3546:
	cmpb	$8, (%r14)
	movl	$-1, %esi
	jne	.L3525
	movl	28(%r14), %esi
	subl	$1, %esi
.L3525:
	movq	-120(%rbp), %r8
	leaq	-100(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState23EnsureChildrenAllocatedEiPNS0_15TranslatedFrameEPiPSt5stackIiSt5dequeIiSaIiEEE
	jmp	.L3496
	.p2align 4,,10
	.p2align 3
.L3508:
	leaq	-100(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState21MaterializeHeapNumberEPNS0_15TranslatedFrameEPiPNS0_15TranslatedValueE
	jmp	.L3496
	.p2align 4,,10
	.p2align 3
.L3557:
	cmpl	%eax, 28(%r14)
	je	.L3547
	jmp	.L3524
	.p2align 4,,10
	.p2align 3
.L3548:
	leaq	.LC59(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3550:
	leaq	.LC192(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3551:
	leaq	.LC64(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3552:
	leaq	.LC184(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3553:
	leaq	.LC185(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3510:
	leaq	.LC186(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3559:
	addq	$288, %rdx
	movq	%rdx, 16(%r14)
	jmp	.L3546
	.p2align 4,,10
	.p2align 3
.L3555:
	movq	%r15, %rsi
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState34EnsurePropertiesAllocatedAndMarkedEPNS0_15TranslatedValueENS0_6HandleINS0_3MapEEE
	xorl	%esi, %esi
	cmpb	$8, (%r15)
	jne	.L3528
	movl	28(%r15), %esi
.L3528:
	movq	-120(%rbp), %r8
	leaq	-100(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal15TranslatedState23EnsureChildrenAllocatedEiPNS0_15TranslatedFrameEPiPSt5stackIiSt5dequeIiSaIiEEE
	movq	-128(%rbp), %rcx
	jmp	.L3527
	.p2align 4,,10
	.p2align 3
.L3558:
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	jmp	.L3516
	.p2align 4,,10
	.p2align 3
.L3556:
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	jmp	.L3523
.L3554:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26024:
	.size	_ZN2v88internal15TranslatedState31EnsureCapturedObjectAllocatedAtEiPSt5stackIiSt5dequeIiSaIiEEE, .-_ZN2v88internal15TranslatedState31EnsureCapturedObjectAllocatedAtEiPSt5stackIiSt5dequeIiSaIiEEE
	.section	.rodata._ZN2v88internal20DeoptimizedFrameInfoC2EPNS0_15TranslatedStateEN9__gnu_cxx17__normal_iteratorIPNS0_15TranslatedFrameESt6vectorIS6_SaIS6_EEEEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC194:
	.string	"stack_it == frame_it->end()"
	.section	.text._ZN2v88internal20DeoptimizedFrameInfoC2EPNS0_15TranslatedStateEN9__gnu_cxx17__normal_iteratorIPNS0_15TranslatedFrameESt6vectorIS6_SaIS6_EEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20DeoptimizedFrameInfoC2EPNS0_15TranslatedStateEN9__gnu_cxx17__normal_iteratorIPNS0_15TranslatedFrameESt6vectorIS6_SaIS6_EEEEPNS0_7IsolateE
	.type	_ZN2v88internal20DeoptimizedFrameInfoC2EPNS0_15TranslatedStateEN9__gnu_cxx17__normal_iteratorIPNS0_15TranslatedFrameESt6vectorIS6_SaIS6_EEEEPNS0_7IsolateE, @function
_ZN2v88internal20DeoptimizedFrameInfoC2EPNS0_15TranslatedStateEN9__gnu_cxx17__normal_iteratorIPNS0_15TranslatedFrameESt6vectorIS6_SaIS6_EEEEPNS0_7IsolateE:
.LFB25957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movq	16(%rdx), %rax
	movq	56(%rdx), %rdi
	movdqu	56(%rdx), %xmm1
	movdqu	72(%rdx), %xmm2
	movq	(%rax), %rax
	movzwl	41(%rax), %r14d
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal15TranslatedValue8GetValueEv
	addl	$1, -64(%rbp)
	movq	%rax, (%rbx)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	addl	$1, -64(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	movq	16(%r13), %rax
	movl	4(%r13), %esi
	movq	(%rax), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L3608
.L3561:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L3609
.L3563:
	movq	7(%rax), %rax
	movq	7(%rax), %rax
.L3562:
	leaq	-104(%rbp), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rcx
	movzwl	%r14w, %esi
	movl	%eax, 64(%rbx)
	movq	%rdx, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L3610
	jb	.L3611
.L3567:
	movq	-96(%rbp), %r15
	testw	%r14w, %r14w
	je	.L3565
	leal	-1(%r14), %eax
	xorl	%r14d, %r14d
	leaq	8(,%rax,8), %rax
	movq	%rax, -128(%rbp)
	leaq	328(%r12), %rax
	movq	%rax, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L3577:
	cmpb	$2, 1(%r15)
	movq	304(%r12), %rdx
	je	.L3612
	movq	%r15, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	-120(%rbp), %rdx
.L3569:
	cmpq	%rax, %rdx
	jne	.L3570
	cmpb	$7, (%r15)
	je	.L3570
	movq	-136(%rbp), %rax
.L3571:
	movq	16(%rbx), %rdx
	movq	%rax, (%rdx,%r14)
	movl	$1, %eax
	movq	-96(%rbp), %r15
	addl	$1, -64(%rbp)
	movq	-80(%rbp), %rdx
	jmp	.L3572
	.p2align 4,,10
	.p2align 3
.L3606:
	testl	%eax, %eax
	jle	.L3613
.L3572:
	subl	$1, %eax
	cmpb	$8, (%r15)
	jne	.L3574
	addl	28(%r15), %eax
.L3574:
	addq	$32, %r15
	movq	%r15, -96(%rbp)
	cmpq	%r15, %rdx
	jne	.L3606
	movq	-72(%rbp), %rdx
	leaq	8(%rdx), %rsi
	movq	%rsi, -72(%rbp)
	movq	8(%rdx), %r15
	leaq	512(%r15), %rdx
	movq	%r15, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%r15, -96(%rbp)
	testl	%eax, %eax
	jg	.L3572
.L3613:
	addq	$8, %r14
	cmpq	-128(%rbp), %r14
	jne	.L3577
.L3565:
	cmpb	$2, 1(%r15)
	movq	304(%r12), %r14
	je	.L3614
	movq	%r15, %rdi
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
.L3579:
	cmpq	%rax, %r14
	jne	.L3580
	cmpb	$7, (%r15)
	leaq	328(%r12), %rax
	je	.L3580
.L3581:
	movq	%rax, 8(%rbx)
	movq	-144(%rbp), %rdi
	addl	$1, -64(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	movq	48(%rbx), %rdx
	movq	40(%rbx), %rcx
	movslq	24(%r13), %rsi
	movq	%rdx, %rax
	subq	%rcx, %rax
	movq	%rsi, %r14
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L3615
	jb	.L3616
.L3585:
	testl	%r14d, %r14d
	jle	.L3617
.L3604:
	leal	-1(%r14), %eax
	movq	-96(%rbp), %r15
	xorl	%r14d, %r14d
	leaq	8(,%rax,8), %rax
	movq	%rax, -128(%rbp)
	leaq	328(%r12), %rax
	movq	%rax, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L3596:
	cmpb	$2, 1(%r15)
	movq	304(%r12), %rdx
	je	.L3618
	movq	%r15, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZNK2v88internal15TranslatedValue11GetRawValueEv.part.0
	movq	-120(%rbp), %rdx
.L3587:
	cmpq	%rax, %rdx
	jne	.L3588
	cmpb	$7, (%r15)
	je	.L3588
	movq	-136(%rbp), %rax
.L3589:
	movq	40(%rbx), %rdx
	movq	%rax, (%rdx,%r14)
	movl	-64(%rbp), %eax
	movq	-96(%rbp), %r15
	movq	-80(%rbp), %rcx
	leal	1(%rax), %edx
	movl	$1, %eax
	movl	%edx, -64(%rbp)
	jmp	.L3590
	.p2align 4,,10
	.p2align 3
.L3607:
	testl	%eax, %eax
	jle	.L3619
.L3590:
	subl	$1, %eax
	cmpb	$8, (%r15)
	jne	.L3592
	addl	28(%r15), %eax
.L3592:
	addq	$32, %r15
	movq	%r15, -96(%rbp)
	cmpq	%r15, %rcx
	jne	.L3607
	movq	-72(%rbp), %rcx
	leaq	8(%rcx), %rdi
	movq	%rdi, -72(%rbp)
	movq	8(%rcx), %r15
	leaq	512(%r15), %rcx
	movq	%r15, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r15, -96(%rbp)
	testl	%eax, %eax
	jg	.L3590
.L3619:
	addq	$8, %r14
	cmpq	%r14, -128(%rbp)
	jne	.L3596
.L3595:
	movq	-144(%rbp), %rdi
	addl	$1, %edx
	movl	%edx, -64(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E
	movq	88(%r13), %rax
	cmpq	%rax, -96(%rbp)
	jne	.L3620
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3621
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3580:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v88internal15TranslatedValue8GetValueEv
	jmp	.L3581
	.p2align 4,,10
	.p2align 3
.L3588:
	movq	%r15, %rdi
	call	_ZN2v88internal15TranslatedValue8GetValueEv
	jmp	.L3589
	.p2align 4,,10
	.p2align 3
.L3570:
	movq	%r15, %rdi
	call	_ZN2v88internal15TranslatedValue8GetValueEv
	jmp	.L3571
	.p2align 4,,10
	.p2align 3
.L3618:
	movq	16(%r15), %rax
	movq	(%rax), %rax
	jmp	.L3587
	.p2align 4,,10
	.p2align 3
.L3612:
	movq	16(%r15), %rax
	movq	(%rax), %rax
	jmp	.L3569
	.p2align 4,,10
	.p2align 3
.L3616:
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdx
	je	.L3585
	movq	%rax, 48(%rbx)
	testl	%r14d, %r14d
	jg	.L3604
.L3617:
	movl	-64(%rbp), %edx
	jmp	.L3595
	.p2align 4,,10
	.p2align 3
.L3611:
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdx
	je	.L3567
	movq	%rax, 24(%rbx)
	jmp	.L3567
	.p2align 4,,10
	.p2align 3
.L3614:
	movq	16(%r15), %rax
	movq	(%rax), %rax
	jmp	.L3579
	.p2align 4,,10
	.p2align 3
.L3615:
	subq	%rax, %rsi
	leaq	40(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm
	jmp	.L3585
	.p2align 4,,10
	.p2align 3
.L3610:
	subq	%rax, %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm
	jmp	.L3567
	.p2align 4,,10
	.p2align 3
.L3608:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L3561
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L3561
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L3561
	movq	31(%rdx), %rax
	jmp	.L3562
	.p2align 4,,10
	.p2align 3
.L3609:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L3563
	movq	7(%rax), %rax
	jmp	.L3562
	.p2align 4,,10
	.p2align 3
.L3620:
	leaq	.LC194(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3621:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25957:
	.size	_ZN2v88internal20DeoptimizedFrameInfoC2EPNS0_15TranslatedStateEN9__gnu_cxx17__normal_iteratorIPNS0_15TranslatedFrameESt6vectorIS6_SaIS6_EEEEPNS0_7IsolateE, .-_ZN2v88internal20DeoptimizedFrameInfoC2EPNS0_15TranslatedStateEN9__gnu_cxx17__normal_iteratorIPNS0_15TranslatedFrameESt6vectorIS6_SaIS6_EEEEPNS0_7IsolateE
	.globl	_ZN2v88internal20DeoptimizedFrameInfoC1EPNS0_15TranslatedStateEN9__gnu_cxx17__normal_iteratorIPNS0_15TranslatedFrameESt6vectorIS6_SaIS6_EEEEPNS0_7IsolateE
	.set	_ZN2v88internal20DeoptimizedFrameInfoC1EPNS0_15TranslatedStateEN9__gnu_cxx17__normal_iteratorIPNS0_15TranslatedFrameESt6vectorIS6_SaIS6_EEEEPNS0_7IsolateE,_ZN2v88internal20DeoptimizedFrameInfoC2EPNS0_15TranslatedStateEN9__gnu_cxx17__normal_iteratorIPNS0_15TranslatedFrameESt6vectorIS6_SaIS6_EEEEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi.str1.1,"aMS",@progbits,1
.LC195:
	.string	"opcode == Translation::BEGIN"
.LC196:
	.string	"update_feedback_count >= 0"
.LC197:
	.string	"update_feedback_count <= 1"
.LC198:
	.string	"    %3i: "
.LC199:
	.string	"         "
.LC200:
	.string	"  "
	.section	.rodata._ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi.str1.8,"aMS",@progbits,1
	.align 8
.LC201:
	.string	"!iterator->HasNext() || static_cast<Translation::Opcode>( iterator->Next()) == Translation::BEGIN"
	.section	.text._ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi
	.type	_ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi, @function
_ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi:
.LFB26012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$296, %rsp
	movq	%rdx, -280(%rbp)
	movq	16(%rbp), %r12
	movq	%r8, -272(%rbp)
	movq	%r9, -296(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	24(%rbp), %eax
	movq	%rsi, 24(%rdi)
	movl	%eax, 40(%rdi)
	movq	%rcx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, -260(%rbp)
	testl	%eax, %eax
	jne	.L3691
	call	_ZN2v88internal19TranslationIterator4NextEv
	movq	%rbx, %rdi
	movslq	%eax, %rsi
	movl	%eax, -264(%rbp)
	call	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE7reserveEm
	movq	%r15, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv
	call	_ZN2v88internal19TranslationIterator4NextEv
	testl	%eax, %eax
	js	.L3692
	cmpl	$1, %eax
	jg	.L3693
	je	.L3694
.L3626:
	leaq	-256(%rbp), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	$0, -256(%rbp)
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	movq	$0, -248(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	call	_ZNSt11_Deque_baseIiSaIiEE17_M_initialize_mapEm
	movl	-264(%rbp), %edi
	testl	%edi, %edi
	jle	.L3627
	leaq	-176(%rbp), %rax
	movq	%r15, -288(%rbp)
	leaq	.LC200(%rip), %r13
	movq	%rbx, %r15
	movq	%rax, -320(%rbp)
	.p2align 4,,10
	.p2align 3
.L3642:
	movq	-320(%rbp), %rbx
	movq	%r12, %r9
	movq	%r15, %rsi
	movq	-280(%rbp), %r8
	movq	-272(%rbp), %rcx
	movq	-288(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3628
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rbx
	leaq	8(%rax), %r14
	cmpq	%rbx, %r14
	jbe	.L3629
	.p2align 4,,10
	.p2align 3
.L3630:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r14
	ja	.L3630
	movq	-136(%rbp), %rdi
.L3629:
	call	_ZdlPv@PLT
.L3628:
	movq	8(%r15), %rax
	movq	%rax, -304(%rbp)
	movl	-120(%rax), %eax
	cmpl	$1, %eax
	je	.L3631
	ja	.L3695
	movq	-304(%rbp), %rsi
	movq	-112(%rsi), %rax
	movzwl	41(%rax), %ebx
	addl	$1, %ebx
	movzwl	%bx, %ebx
	addl	-96(%rsi), %ebx
	addl	$3, %ebx
	.p2align 4,,10
	.p2align 3
.L3635:
	testl	%ebx, %ebx
	jle	.L3684
.L3637:
	testq	%r12, %r12
	je	.L3641
	movq	-208(%rbp), %rax
	cmpq	%rax, -240(%rbp)
	jne	.L3643
	movq	-304(%rbp), %rax
	movl	-120(%rax), %eax
	cmpl	$1, %eax
	je	.L3644
	ja	.L3696
	movq	-304(%rbp), %rsi
	movq	-112(%rsi), %rax
	movzwl	41(%rax), %edx
	addl	$1, %edx
	movzwl	%dx, %edx
	addl	-96(%rsi), %edx
	addl	$3, %edx
.L3647:
	subl	%ebx, %edx
	leaq	.LC198(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L3648:
	subq	$8, %rsp
	movq	-288(%rbp), %rdx
	movq	-296(%rbp), %r9
	movq	%r15, %rdi
	movq	-280(%rbp), %r8
	movq	-272(%rbp), %rcx
	pushq	%r12
	movl	-260(%rbp), %esi
	call	_ZN2v88internal15TranslatedState25CreateNextTranslatedValueEiPNS0_19TranslationIteratorENS0_10FixedArrayEmPNS0_14RegisterValuesEP8_IO_FILE
	leaq	.LC180(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	popq	%rax
	popq	%rdx
.L3667:
	subl	$1, %ebx
	testl	%r14d, %r14d
	jle	.L3697
	movq	-192(%rbp), %rax
	movq	-208(%rbp), %rdx
	subq	$4, %rax
	cmpq	%rax, %rdx
	je	.L3654
	movl	%ebx, (%rdx)
	movl	%r14d, %ebx
	addq	$4, -208(%rbp)
	testl	%ebx, %ebx
	jg	.L3637
.L3684:
	movq	-208(%rbp), %rax
	cmpq	%rax, -240(%rbp)
	je	.L3698
	testq	%r12, %r12
	je	.L3641
.L3643:
	xorl	%eax, %eax
	leaq	.LC199(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-184(%rbp), %rax
	subq	-216(%rbp), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$7, %rax
	movq	%rax, %rdx
	movq	-208(%rbp), %rax
	subq	-200(%rbp), %rax
	sarq	$2, %rax
	addq	%rdx, %rax
	movq	-224(%rbp), %rdx
	subq	-240(%rbp), %rdx
	sarq	$2, %rdx
	addq	%rdx, %rax
	je	.L3648
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L3650:
	xorl	%eax, %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$1, %r14
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-184(%rbp), %rdx
	subq	-216(%rbp), %rdx
	sarq	$3, %rdx
	movq	-208(%rbp), %rax
	subq	-200(%rbp), %rax
	subq	$1, %rdx
	sarq	$2, %rax
	movq	%rdx, %rcx
	salq	$7, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	-224(%rbp), %rax
	subq	-240(%rbp), %rax
	sarq	$2, %rax
	addq	%rdx, %rax
	cmpq	%rax, %r14
	jb	.L3650
	jmp	.L3648
	.p2align 4,,10
	.p2align 3
.L3696:
	subl	$2, %eax
	cmpl	$3, %eax
	ja	.L3633
	movq	-304(%rbp), %rax
	movl	-96(%rax), %eax
	movl	%eax, -312(%rbp)
	leal	2(%rax), %edx
	jmp	.L3647
	.p2align 4,,10
	.p2align 3
.L3697:
	testl	%ebx, %ebx
	jne	.L3635
	movq	-208(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L3653:
	cmpq	%rdi, -240(%rbp)
	je	.L3684
	cmpq	%rdi, -200(%rbp)
	je	.L3699
	movl	-4(%rdi), %r14d
	subq	$4, %rdi
	movq	%rdi, -208(%rbp)
	testl	%r14d, %r14d
	je	.L3653
	movl	%r14d, %ebx
	jmp	.L3635
	.p2align 4,,10
	.p2align 3
.L3699:
	movq	-184(%rbp), %rax
	movq	-8(%rax), %rax
	movl	508(%rax), %r14d
	call	_ZdlPv@PLT
	movq	-184(%rbp), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, -184(%rbp)
	movq	-8(%rax), %rdi
	leaq	512(%rdi), %rax
	movq	%rdi, -200(%rbp)
	addq	$508, %rdi
	movq	%rax, -192(%rbp)
	movq	%rdi, -208(%rbp)
	testl	%r14d, %r14d
	je	.L3653
	movl	%r14d, %ebx
	jmp	.L3635
	.p2align 4,,10
	.p2align 3
.L3641:
	subq	$8, %rsp
	movl	-260(%rbp), %esi
	movq	-272(%rbp), %rcx
	movq	%r15, %rdi
	movq	-296(%rbp), %r9
	movq	-280(%rbp), %r8
	pushq	$0
	movq	-288(%rbp), %rdx
	call	_ZN2v88internal15TranslatedState25CreateNextTranslatedValueEiPNS0_19TranslationIteratorENS0_10FixedArrayEmPNS0_14RegisterValuesEP8_IO_FILE
	popq	%rcx
	popq	%rsi
	movl	%eax, %r14d
	jmp	.L3667
	.p2align 4,,10
	.p2align 3
.L3644:
	movq	-304(%rbp), %rax
	movl	-96(%rax), %eax
	movl	%eax, -312(%rbp)
	leal	1(%rax), %edx
	jmp	.L3647
	.p2align 4,,10
	.p2align 3
.L3654:
	movq	-184(%rbp), %rsi
	subq	-200(%rbp), %rdx
	sarq	$2, %rdx
	movq	%rsi, %rax
	subq	-216(%rbp), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$7, %rax
	addq	%rdx, %rax
	movq	-224(%rbp), %rdx
	subq	-240(%rbp), %rdx
	sarq	$2, %rdx
	addq	%rax, %rdx
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rdx
	je	.L3700
	movq	-248(%rbp), %rcx
	movq	%rsi, %rax
	subq	-256(%rbp), %rax
	sarq	$3, %rax
	subq	%rax, %rcx
	cmpq	$1, %rcx
	jbe	.L3701
.L3656:
	movl	$512, %edi
	movq	%rsi, -312(%rbp)
	call	_Znwm@PLT
	movq	-312(%rbp), %rsi
	movq	%rax, 8(%rsi)
	movq	-208(%rbp), %rax
	movl	%ebx, (%rax)
	movq	-184(%rbp), %rax
	movl	%r14d, %ebx
	leaq	8(%rax), %rdx
	movq	%rdx, -184(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -200(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%rax, -208(%rbp)
	jmp	.L3635
.L3695:
	subl	$2, %eax
	cmpl	$3, %eax
	ja	.L3633
	movq	-304(%rbp), %rax
	movl	-96(%rax), %eax
	movl	%eax, -312(%rbp)
	leal	2(%rax), %ebx
	jmp	.L3635
	.p2align 4,,10
	.p2align 3
.L3698:
	addl	$1, -260(%rbp)
	movl	-260(%rbp), %eax
	cmpl	%eax, -264(%rbp)
	jne	.L3642
	movq	-288(%rbp), %r15
.L3627:
	movq	(%r15), %rax
	movl	8(%r15), %esi
	cmpl	%esi, 11(%rax)
	jg	.L3660
.L3664:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3622
	movq	-184(%rbp), %rax
	movq	-216(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L3665
.L3666:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L3666
	movq	-256(%rbp), %rdi
.L3665:
	call	_ZdlPv@PLT
.L3622:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3702
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3701:
	.cfi_restore_state
	movq	-328(%rbp), %rdi
	movl	$1, %esi
	xorl	%edx, %edx
	call	_ZNSt5dequeIiSaIiEE17_M_reallocate_mapEmb
	movq	-184(%rbp), %rsi
	jmp	.L3656
.L3631:
	movq	-304(%rbp), %rax
	movl	-96(%rax), %eax
	movl	%eax, -312(%rbp)
	leal	1(%rax), %ebx
	jmp	.L3635
.L3694:
	movq	-272(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15TranslatedState18ReadUpdateFeedbackEPNS0_19TranslationIteratorENS0_10FixedArrayEP8_IO_FILE
	jmp	.L3626
.L3691:
	leaq	.LC195(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3692:
	leaq	.LC196(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3693:
	leaq	.LC197(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3660:
	movq	%r15, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv
	testl	%eax, %eax
	je	.L3664
	leaq	.LC201(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3633:
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3702:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L3700:
	leaq	.LC75(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26012:
	.size	_ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi, .-_ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi
	.section	.rodata._ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv.str1.8,"aMS",@progbits,1
	.align 8
.LC202:
	.string	"[deoptimizing (DEOPT %s): begin "
	.align 8
.LC203:
	.string	" (opt #%d) @%d, FP to SP delta: %d, caller sp: 0x%012lx]\n"
	.align 8
.LC204:
	.string	"            ;;; deoptimize at "
	.align 8
.LC205:
	.string	"catch_handler_frame_index < count"
	.section	.rodata._ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv.str1.1,"aMS",@progbits,1
.LC206:
	.string	"invalid frame"
.LC207:
	.string	"[deoptimizing (%s): end "
	.section	.rodata._ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv.str1.8
	.align 8
.LC208:
	.string	" @%d => node=%d, pc=0x%012lx, caller sp=0x%012lx, took %0.3f ms]\n"
	.section	.text._ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv
	.type	_ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv, @function
_ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv:
.LFB25840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	15(%rax), %r14
	call	_ZN2v88internal15JavaScriptFrame11fp_registerEv@PLT
	movq	56(%rbx), %r13
	movq	8(%rbx), %rdx
	movl	%eax, %eax
	movq	16(%r13,%rax,8), %r12
	movl	$16, %eax
	movq	%r12, 120(%rbx)
	testb	$1, %dl
	je	.L3704
	movq	23(%rdx), %rax
	movzwl	41(%rax), %r13d
	addl	$1, %r13d
	movzwl	%r13w, %r13d
	movl	%r13d, %edi
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	cmpb	$1, %al
	sbbl	$-1, %r13d
	leal	16(,%r13,8), %eax
	movq	56(%rbx), %r13
.L3704:
	addq	%rax, %r12
	movq	%r12, 80(%rbx)
	movl	8(%r13), %r12d
	movl	%r12d, %edi
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	cmpb	$1, %al
	movl	0(%r13), %eax
	sbbl	$-1, %r12d
	sall	$3, %r12d
	subl	$16, %eax
	subl	%r12d, %eax
	cmpq	$0, 304(%rbx)
	leaq	320(%r13,%rax), %rax
	movq	(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rdx
	movq	%rdx, 96(%rbx)
	movq	-8(%rax), %rax
	movq	%rax, 112(%rbx)
	je	.L3749
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, -136(%rbp)
	movzbl	28(%rbx), %eax
	cmpb	$1, %al
	je	.L3750
	cmpb	$2, %al
	je	.L3751
	testb	%al, %al
	jne	.L3742
	leaq	.LC11(%rip), %rdx
.L3708:
	movq	304(%rbx), %rax
	leaq	.LC202(%rip), %rsi
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal11Deoptimizer17PrintFunctionNameEv
	movl	24(%rbx), %ecx
	movq	55(%r14), %rdx
	leaq	.LC203(%rip), %rsi
	movq	304(%rbx), %rax
	movq	80(%rbx), %r9
	movl	40(%rbx), %r8d
	sarq	$32, %rdx
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	cmpb	$1, 28(%rbx)
	jbe	.L3778
.L3707:
	movl	24(%rbx), %eax
	leaq	128(%rbx), %r15
	xorl	%esi, %esi
	leal	(%rax,%rax,2), %edx
	leal	88(,%rdx,8), %eax
	cltq
	movq	-1(%r14,%rax), %rax
	movq	%rax, -128(%rbp)
	leal	96(,%rdx,8), %eax
	movq	15(%r14), %rcx
	cltq
	movq	-1(%r14,%rax), %rax
	movq	%rcx, -96(%rbp)
	sarq	$32, %rax
	movl	%eax, -88(%rbp)
	movq	8(%rbx), %rax
	testb	$1, %al
	jne	.L3779
.L3709:
	movq	304(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L3710
	movq	(%rcx), %rax
	movq	144(%rax), %rcx
.L3710:
	movq	56(%rbx), %rdx
	movq	%rcx, -120(%rbp)
	movl	%esi, -112(%rbp)
	movq	31(%r14), %r12
	movl	8(%rdx), %r14d
	leaq	16(%rdx), %r13
	movq	%rdx, -104(%rbp)
	movl	%r14d, %edi
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	movq	-104(%rbp), %rdx
	movl	-112(%rbp), %esi
	leaq	-96(%rbp), %r10
	cmpb	$1, %al
	movq	-120(%rbp), %rcx
	movq	%r13, %r9
	movq	%r12, %r8
	movl	(%rdx), %eax
	sbbl	$-1, %r14d
	pushq	%rsi
	movq	%r15, %rdi
	pushq	%rcx
	sall	$3, %r14d
	movq	(%rbx), %rsi
	movq	%r10, %rcx
	subl	$16, %eax
	subl	%r14d, %eax
	leaq	320(%rdx,%rax), %rdx
	call	_ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi
	movq	128(%rbx), %rax
	movq	136(%rbx), %r9
	movabsq	$-1229782938247303441, %rdx
	subq	%rax, %r9
	movq	%r9, %r10
	sarq	$3, %r10
	imulq	%rdx, %r10
	cmpb	$0, 44(%rbx)
	popq	%rdx
	popq	%rcx
	movq	%r10, %r13
	movq	%r10, %r12
	je	.L3712
	leaq	-1(%r10), %r12
	leaq	48(%rbx), %r15
	subq	$120, %r9
	leaq	-80(%rbp), %r14
	testq	%r10, %r10
	jne	.L3714
	jmp	.L3715
	.p2align 4,,10
	.p2align 3
.L3780:
	movl	$-1, 52(%rbx)
	subq	$120, %r9
	testq	%r12, %r12
	je	.L3715
.L3787:
	movq	128(%rbx), %rax
	subq	$1, %r12
.L3714:
	addq	%r9, %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L3716
	cmpl	$5, %edx
	jne	.L3780
	movl	$0, 52(%rbx)
.L3723:
	cmpq	%r12, %r13
	jbe	.L3715
	addq	$1, %r12
.L3712:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r12
	ja	.L3725
	leaq	0(,%r12,8), %rdi
	call	_Znam@PLT
	movl	%r12d, %ecx
	movq	%rax, 72(%rbx)
	testq	%r12, %r12
	je	.L3726
.L3746:
	movq	$0, (%rax)
	cmpq	$1, %r12
	jbe	.L3727
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L3728:
	movq	72(%rbx), %rdx
	movq	$0, (%rdx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %r12
	jne	.L3728
.L3727:
	movl	%ecx, 64(%rbx)
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	leaq	.L3744(%rip), %r10
	leaq	.L3743(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L3739:
	movq	128(%rbx), %rsi
	movl	%r14d, %edx
	addq	%r15, %rsi
	cmpb	$0, 44(%rbx)
	movl	(%rsi), %eax
	je	.L3729
	leaq	-1(%r12), %rcx
	cmpq	%rcx, %r14
	je	.L3781
.L3729:
	cmpl	$6, %eax
	ja	.L3732
	movslq	(%r10,%rax,4), %rax
	addq	%r10, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv,"a",@progbits
	.align 4
	.align 4
.L3744:
	.long	.L3758-.L3744
	.long	.L3733-.L3744
	.long	.L3734-.L3744
	.long	.L3735-.L3744
	.long	.L3736-.L3744
	.long	.L3759-.L3744
	.long	.L3738-.L3744
	.section	.text._ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv
.L3759:
	movl	$2, %ecx
.L3737:
	movq	%rbx, %rdi
	call	_ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE
	leaq	.L3744(%rip), %r10
.L3732:
	addq	$1, %r14
	addq	$120, %r15
	cmpq	%r14, %r12
	jne	.L3739
	movq	72(%rbx), %rax
.L3745:
	movq	-8(%rax,%r12,8), %rdx
	movq	(%rbx), %rax
	subq	$-128, %rax
	movq	%rax, 120(%rdx)
	cmpq	$0, 304(%rbx)
	je	.L3703
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-80(%rbp), %rdi
	subq	-136(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movl	64(%rbx), %eax
	leal	-1(%rax), %r12d
	movzbl	28(%rbx), %eax
	cmpb	$1, %al
	je	.L3754
	cmpb	$2, %al
	jne	.L3782
	leaq	.LC10(%rip), %rdx
.L3741:
	movq	304(%rbx), %rax
	leaq	.LC207(%rip), %rsi
	movsd	%xmm0, -104(%rbp)
	movslq	%r12d, %r12
	movq	(%rax), %rax
	movq	144(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal11Deoptimizer17PrintFunctionNameEv
	movq	72(%rbx), %rax
	movq	304(%rbx), %rsi
	movq	-128(%rbp), %rcx
	movl	24(%rbx), %edx
	movq	(%rax,%r12,8), %rax
	movq	(%rsi), %rsi
	movsd	-104(%rbp), %xmm0
	movq	80(%rbx), %r9
	sarq	$32, %rcx
	movq	144(%rsi), %rdi
	movq	280(%rax), %r8
	leaq	.LC208(%rip), %rsi
	movl	$1, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L3703:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3783
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3758:
	.cfi_restore_state
	xorl	%ecx, %ecx
.L3731:
	movq	%rbx, %rdi
	call	_ZN2v88internal11Deoptimizer25DoComputeInterpretedFrameEPNS0_15TranslatedFrameEib
	addl	$1, 68(%rbx)
	leaq	.L3744(%rip), %r10
	jmp	.L3732
.L3736:
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE
	leaq	.L3744(%rip), %r10
	jmp	.L3732
.L3735:
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal11Deoptimizer28DoComputeBuiltinContinuationEPNS0_15TranslatedFrameEiNS0_23BuiltinContinuationModeE
	leaq	.L3744(%rip), %r10
	jmp	.L3732
.L3734:
	movq	%rbx, %rdi
	call	_ZN2v88internal11Deoptimizer27DoComputeConstructStubFrameEPNS0_15TranslatedFrameEi
	leaq	.L3744(%rip), %r10
	jmp	.L3732
.L3733:
	movq	%rbx, %rdi
	call	_ZN2v88internal11Deoptimizer30DoComputeArgumentsAdaptorFrameEPNS0_15TranslatedFrameEi
	leaq	.L3744(%rip), %r10
	jmp	.L3732
	.p2align 4,,10
	.p2align 3
.L3751:
	leaq	.LC10(%rip), %rdx
	jmp	.L3708
	.p2align 4,,10
	.p2align 3
.L3750:
	leaq	.LC9(%rip), %rdx
	jmp	.L3708
	.p2align 4,,10
	.p2align 3
.L3781:
	cmpl	$6, %eax
	ja	.L3732
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv
	.align 4
	.align 4
.L3743:
	.long	.L3756-.L3743
	.long	.L3733-.L3743
	.long	.L3734-.L3743
	.long	.L3735-.L3743
	.long	.L3736-.L3743
	.long	.L3757-.L3743
	.long	.L3738-.L3743
	.section	.text._ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv
.L3757:
	movl	$3, %ecx
	jmp	.L3737
.L3756:
	movl	$1, %ecx
	jmp	.L3731
	.p2align 4,,10
	.p2align 3
.L3716:
	movl	4(%rax), %r11d
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L3784
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L3785
.L3720:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L3786
.L3722:
	movq	7(%rax), %rax
	movq	7(%rax), %rsi
.L3721:
	movq	%r14, %rdi
	movq	%r9, -112(%rbp)
	movl	%r11d, -104(%rbp)
	call	_ZN2v88internal12HandlerTableC1ENS0_13BytecodeArrayE@PLT
	movl	-104(%rbp), %r11d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rdi
	movl	%r11d, %esi
	call	_ZN2v88internal12HandlerTable11LookupRangeEiPiPNS1_15CatchPredictionE@PLT
	movq	-112(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, 52(%rbx)
	jns	.L3723
	subq	$120, %r9
	testq	%r12, %r12
	jne	.L3787
.L3715:
	leaq	.LC205(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3782:
	leaq	.LC11(%rip), %rdx
	testb	%al, %al
	je	.L3741
.L3742:
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3725:
	movq	$-1, %rdi
	call	_Znam@PLT
	movl	%r12d, %ecx
	movq	%rax, 72(%rbx)
	jmp	.L3746
	.p2align 4,,10
	.p2align 3
.L3785:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L3720
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L3720
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L3720
	movq	31(%rdx), %rsi
	jmp	.L3721
	.p2align 4,,10
	.p2align 3
.L3779:
	movq	23(%rax), %rax
	movzwl	41(%rax), %esi
	jmp	.L3709
	.p2align 4,,10
	.p2align 3
.L3749:
	movq	$0, -136(%rbp)
	jmp	.L3707
	.p2align 4,,10
	.p2align 3
.L3786:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L3722
	movq	7(%rax), %rsi
	jmp	.L3721
	.p2align 4,,10
	.p2align 3
.L3778:
	movq	304(%rbx), %rax
	movq	32(%rbx), %rcx
	leaq	16(%rbx), %rdi
	leaq	.LC204(%rip), %rdx
	movq	(%rax), %rax
	movq	144(%rax), %rsi
	call	_ZN2v88internal4Code18PrintDeoptLocationEP8_IO_FILEPKcm@PLT
	jmp	.L3707
.L3754:
	leaq	.LC9(%rip), %rdx
	jmp	.L3741
.L3784:
	leaq	.LC135(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L3738:
	leaq	.LC206(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3726:
	movl	$0, 64(%rbx)
	jmp	.L3745
.L3783:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25840:
	.size	_ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv, .-_ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv
	.section	.text._ZN2v88internal11Deoptimizer19ComputeOutputFramesEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer19ComputeOutputFramesEPS1_
	.type	_ZN2v88internal11Deoptimizer19ComputeOutputFramesEPS1_, @function
_ZN2v88internal11Deoptimizer19ComputeOutputFramesEPS1_:
.LFB25800:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal11Deoptimizer21DoComputeOutputFramesEv
	.cfi_endproc
.LFE25800:
	.size	_ZN2v88internal11Deoptimizer19ComputeOutputFramesEPS1_, .-_ZN2v88internal11Deoptimizer19ComputeOutputFramesEPS1_
	.section	.text._ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi.constprop.0, @function
_ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi.constprop.0:
.LFB34275:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$520, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -504(%rbp)
	movq	%r8, -528(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r9d, 40(%rdi)
	movq	%rsi, 24(%rdi)
	movq	%rcx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv
	testl	%eax, %eax
	jne	.L3888
	call	_ZN2v88internal19TranslationIterator4NextEv
	movq	%r14, %rdi
	movslq	%eax, %rsi
	movq	%rsi, %rbx
	call	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE7reserveEm
	movq	%r15, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv
	call	_ZN2v88internal19TranslationIterator4NextEv
	testl	%eax, %eax
	js	.L3889
	cmpl	$1, %eax
	jg	.L3890
	je	.L3891
.L3793:
	leaq	-416(%rbp), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	$0, -416(%rbp)
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
	movq	$0, -408(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm0, -352(%rbp)
	call	_ZNSt11_Deque_baseIiSaIiEE17_M_initialize_mapEm
	testl	%ebx, %ebx
	jle	.L3794
	leal	-1(%rbx), %eax
	leaq	16+_ZTVN6disasm13NameConverterE(%rip), %rdx
	movq	%r14, %r13
	movq	$0, -512(%rbp)
	movq	%rax, -544(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rdx, %xmm2
	movq	%rax, -440(%rbp)
	leaq	-184(%rbp), %rax
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm2
	movaps	%xmm2, -496(%rbp)
	.p2align 4,,10
	.p2align 3
.L3805:
	movq	-440(%rbp), %rbx
	xorl	%r9d, %r9d
	movq	%r15, %rdx
	movq	%r13, %rsi
	movl	-512(%rbp), %eax
	movq	-504(%rbp), %r8
	movq	-528(%rbp), %rcx
	movq	%rbx, %rdi
	movl	%eax, -516(%rbp)
	call	_ZN2v88internal15TranslatedState25CreateNextTranslatedFrameEPNS0_19TranslationIteratorENS0_10FixedArrayEmP8_IO_FILE
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZNSt6vectorIN2v88internal15TranslatedFrameESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	movq	-296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3795
	movq	-224(%rbp), %rax
	movq	-256(%rbp), %r12
	leaq	8(%rax), %rbx
	cmpq	%r12, %rbx
	jbe	.L3796
	.p2align 4,,10
	.p2align 3
.L3797:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %rbx
	ja	.L3797
	movq	-296(%rbp), %rdi
.L3796:
	call	_ZdlPv@PLT
.L3795:
	movq	8(%r13), %rdx
	movl	-120(%rdx), %eax
	cmpl	$1, %eax
	je	.L3798
	ja	.L3892
	movq	-112(%rdx), %rax
	movzwl	41(%rax), %eax
	addl	$1, %eax
	movzwl	%ax, %eax
	addl	-96(%rdx), %eax
	leal	3(%rax), %r12d
.L3803:
	movq	-512(%rbp), %rsi
	leaq	.L3808(%rip), %r14
	movq	%rsi, %rax
	salq	$4, %rax
	subq	%rsi, %rax
	salq	$3, %rax
	movq	%rax, -480(%rbp)
	.p2align 4,,10
	.p2align 3
.L3802:
	testl	%r12d, %r12d
	jle	.L3878
.L3804:
	movq	-480(%rbp), %rbx
	addq	0(%r13), %rbx
	movq	%r15, %rdi
	movq	$128, -192(%rbp)
	movq	80(%rbx), %rax
	movdqa	-496(%rbp), %xmm1
	movq	112(%rbx), %r10
	movq	96(%rbx), %r11
	movq	%rax, -448(%rbp)
	movq	88(%rbx), %rax
	movaps	%xmm1, -208(%rbp)
	movq	%rax, -456(%rbp)
	movq	72(%rbx), %rax
	movq	%rax, -464(%rbp)
	movq	56(%rbx), %rax
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal19TranslationIterator4NextEv
	subl	$7, %eax
	cmpl	$18, %eax
	ja	.L3806
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi.constprop.0,"a",@progbits
	.align 4
	.align 4
.L3808:
	.long	.L3826-.L3808
	.long	.L3825-.L3808
	.long	.L3824-.L3808
	.long	.L3823-.L3808
	.long	.L3816-.L3808
	.long	.L3816-.L3808
	.long	.L3816-.L3808
	.long	.L3816-.L3808
	.long	.L3816-.L3808
	.long	.L3816-.L3808
	.long	.L3816-.L3808
	.long	.L3815-.L3808
	.long	.L3814-.L3808
	.long	.L3813-.L3808
	.long	.L3812-.L3808
	.long	.L3811-.L3808
	.long	.L3810-.L3808
	.long	.L3809-.L3808
	.long	.L3807-.L3808
	.section	.text._ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi.constprop.0
	.p2align 4,,10
	.p2align 3
.L3816:
	call	_ZN2v88internal19TranslationIterator4NextEv
	xorl	%r11d, %r11d
	movq	%r13, -328(%rbp)
	movw	%r11w, -336(%rbp)
	movq	$0, -320(%rbp)
.L3885:
	movq	-440(%rbp), %rsi
	leaq	40(%rbx), %rdi
	leal	-1(%r12), %ebx
	call	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE9push_backERKS2_
	cmpb	$8, -336(%rbp)
	jne	.L3866
	movl	-308(%rbp), %r12d
	testl	%r12d, %r12d
	jle	.L3866
	movq	-352(%rbp), %rax
	movq	-368(%rbp), %rdx
	subq	$4, %rax
	cmpq	%rax, %rdx
	je	.L3852
	movl	%ebx, (%rdx)
	addq	$4, -368(%rbp)
	testl	%r12d, %r12d
	jg	.L3804
.L3878:
	movq	-400(%rbp), %rax
	cmpq	%rax, -368(%rbp)
	jne	.L3804
	movq	-512(%rbp), %rsi
	leaq	1(%rsi), %rax
	cmpq	-544(%rbp), %rsi
	je	.L3794
	movq	%rax, -512(%rbp)
	jmp	.L3805
	.p2align 4,,10
	.p2align 3
.L3814:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movq	-504(%rbp), %rsi
	movl	$2, %r10d
	cltq
	movl	(%rax,%rsi), %eax
	movw	%r10w, -336(%rbp)
	movq	%r13, -328(%rbp)
	movq	$0, -320(%rbp)
	movl	%eax, -312(%rbp)
	jmp	.L3885
	.p2align 4,,10
	.p2align 3
.L3813:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movq	-504(%rbp), %rcx
	movl	$3, %r9d
	cltq
	movq	(%rax,%rcx), %rax
	movw	%r9w, -336(%rbp)
.L3886:
	movq	%r13, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -312(%rbp)
	jmp	.L3885
	.p2align 4,,10
	.p2align 3
.L3825:
	call	_ZN2v88internal19TranslationIterator4NextEv
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	subl	$1, %r12d
	movq	-504(%rbp), %rdx
	movl	-516(%rbp), %esi
	movl	%eax, %ecx
	call	_ZN2v88internal15TranslatedState39CreateArgumentsElementsTranslatedValuesEimNS0_19CreateArgumentsTypeEP8_IO_FILE
	.p2align 4,,10
	.p2align 3
.L3832:
	testl	%r12d, %r12d
	jne	.L3802
	movq	-368(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L3835:
	cmpq	%rdi, -400(%rbp)
	je	.L3878
	cmpq	%rdi, -360(%rbp)
	je	.L3893
	movl	-4(%rdi), %ebx
	subq	$4, %rdi
	movq	%rdi, -368(%rbp)
	testl	%ebx, %ebx
	je	.L3835
	movl	%ebx, %r12d
	jmp	.L3802
	.p2align 4,,10
	.p2align 3
.L3826:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movq	-440(%rbp), %rdi
	leaq	64(%r13), %rsi
	movslq	%eax, %rdx
	leaq	48(%r13), %rax
	movl	%edx, -448(%rbp)
	movq	%rax, -456(%rbp)
	call	_ZNKSt15_Deque_iteratorIN2v88internal15TranslatedState14ObjectPositionERS3_PS3_EplEl
	movq	112(%r13), %rax
	movq	96(%r13), %rdx
	movq	-336(%rbp), %r9
	movl	-448(%rbp), %ecx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L3827
	movq	(%r9), %rax
	movq	%rax, (%rdx)
	addq	$8, 96(%r13)
.L3828:
	movl	$9, %eax
	movq	%r13, -328(%rbp)
	movw	%ax, -336(%rbp)
	movq	$0, -320(%rbp)
	movl	%ecx, -312(%rbp)
	movl	$-1, -308(%rbp)
	jmp	.L3885
	.p2align 4,,10
	.p2align 3
.L3807:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movq	-528(%rbp), %rsi
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rsi,%rax), %rax
.L3887:
	movl	$1, %edx
	movw	%dx, -336(%rbp)
	jmp	.L3886
	.p2align 4,,10
	.p2align 3
.L3815:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movq	-504(%rbp), %rcx
	cltq
	movq	(%rax,%rcx), %rax
	jmp	.L3887
	.p2align 4,,10
	.p2align 3
.L3810:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movq	-504(%rbp), %rsi
	cltq
	movl	(%rax,%rsi), %eax
	movl	$6, %esi
	movq	%r13, -328(%rbp)
	movw	%si, -336(%rbp)
	movq	$0, -320(%rbp)
	movl	%eax, -312(%rbp)
	jmp	.L3885
	.p2align 4,,10
	.p2align 3
.L3809:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movq	-504(%rbp), %rcx
	cltq
	movq	(%rax,%rcx), %rax
	movl	$7, %ecx
	movw	%cx, -336(%rbp)
	jmp	.L3886
	.p2align 4,,10
	.p2align 3
.L3823:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movq	120(%r13), %rdx
	subq	88(%r13), %rdx
	leaq	48(%r13), %rdi
	sarq	$3, %rdx
	movl	%eax, -520(%rbp)
	subq	-448(%rbp), %r10
	subq	$1, %rdx
	sarq	$3, %r10
	movq	-440(%rbp), %rsi
	movq	%rdx, %rcx
	movq	96(%r13), %rdx
	subq	104(%r13), %rdx
	salq	$6, %rcx
	sarq	$3, %rdx
	addq	%rcx, %rdx
	movq	80(%r13), %rcx
	subq	64(%r13), %rcx
	sarq	$3, %rcx
	leaq	(%rdx,%rcx), %rax
	movl	-516(%rbp), %ecx
	movq	-456(%rbp), %rdx
	movq	%rax, -536(%rbp)
	movl	%ecx, -336(%rbp)
	subq	%r11, %rdx
	leaq	-1(%r10), %rcx
	sarq	$5, %rdx
	salq	$4, %rcx
	leaq	(%rcx,%rdx), %rcx
	movq	-464(%rbp), %rdx
	subq	-472(%rbp), %rdx
	sarq	$5, %rdx
	addq	%rcx, %rdx
	movl	%edx, -332(%rbp)
	call	_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	movl	$8, %eax
	movq	%r13, -328(%rbp)
	movw	%ax, -336(%rbp)
	movl	-536(%rbp), %eax
	movq	$0, -320(%rbp)
	movl	%eax, -312(%rbp)
	movl	-520(%rbp), %eax
	movl	%eax, -308(%rbp)
	jmp	.L3885
	.p2align 4,,10
	.p2align 3
.L3824:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movq	-504(%rbp), %rsi
	movq	%r13, %rdi
	subl	$1, %r12d
	movl	%eax, %edx
	leaq	-420(%rbp), %rcx
	call	_ZN2v88internal15TranslatedState24ComputeArgumentsPositionEmNS0_19CreateArgumentsTypeEPi
	movl	$2, %eax
	leaq	40(%rbx), %rdi
	movq	-440(%rbp), %rsi
	movw	%ax, -336(%rbp)
	movl	-420(%rbp), %eax
	movq	%r13, -328(%rbp)
	movq	$0, -320(%rbp)
	movl	%eax, -312(%rbp)
	call	_ZNSt5dequeIN2v88internal15TranslatedValueESaIS2_EE9push_backERKS2_
	jmp	.L3832
	.p2align 4,,10
	.p2align 3
.L3812:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movq	-504(%rbp), %rsi
	movl	$4, %r8d
	cltq
	movl	(%rax,%rsi), %eax
	movw	%r8w, -336(%rbp)
	movq	%r13, -328(%rbp)
	movq	$0, -320(%rbp)
	movl	%eax, -312(%rbp)
	jmp	.L3885
	.p2align 4,,10
	.p2align 3
.L3811:
	call	_ZN2v88internal19TranslationIterator4NextEv
	movl	%eax, %edi
	call	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi@PLT
	movq	-504(%rbp), %rcx
	movl	$5, %edi
	cltq
	movl	(%rax,%rcx), %eax
	movw	%di, -336(%rbp)
	movq	%r13, -328(%rbp)
	movq	$0, -320(%rbp)
	movl	%eax, -312(%rbp)
	jmp	.L3885
	.p2align 4,,10
	.p2align 3
.L3893:
	movq	-344(%rbp), %rax
	movq	-8(%rax), %rax
	movl	508(%rax), %ebx
	call	_ZdlPv@PLT
	movq	-344(%rbp), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, -344(%rbp)
	movq	-8(%rax), %rdi
	leaq	512(%rdi), %rax
	movq	%rdi, -360(%rbp)
	addq	$508, %rdi
	movq	%rax, -352(%rbp)
	movq	%rdi, -368(%rbp)
	testl	%ebx, %ebx
	je	.L3835
	movl	%ebx, %r12d
	jmp	.L3802
	.p2align 4,,10
	.p2align 3
.L3866:
	movl	%ebx, %r12d
	jmp	.L3832
	.p2align 4,,10
	.p2align 3
.L3852:
	movq	-344(%rbp), %rcx
	subq	-360(%rbp), %rdx
	sarq	$2, %rdx
	movq	%rcx, %rax
	subq	-376(%rbp), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$7, %rax
	addq	%rdx, %rax
	movq	-384(%rbp), %rdx
	subq	-400(%rbp), %rdx
	sarq	$2, %rdx
	addq	%rax, %rdx
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rdx
	je	.L3853
	movq	-408(%rbp), %rsi
	movq	%rcx, %rax
	subq	-416(%rbp), %rax
	sarq	$3, %rax
	subq	%rax, %rsi
	cmpq	$1, %rsi
	jbe	.L3894
.L3854:
	movl	$512, %edi
	movq	%rcx, -448(%rbp)
	call	_Znwm@PLT
	movq	-448(%rbp), %rcx
	movq	%rax, 8(%rcx)
	movq	-368(%rbp), %rax
	movl	%ebx, (%rax)
	movq	-344(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -344(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -360(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rax, -368(%rbp)
	jmp	.L3802
.L3892:
	subl	$2, %eax
	cmpl	$3, %eax
	ja	.L3895
	movl	-96(%rdx), %eax
	leal	2(%rax), %r12d
	jmp	.L3803
	.p2align 4,,10
	.p2align 3
.L3894:
	movq	-552(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIiSaIiEE17_M_reallocate_mapEmb
	movq	-344(%rbp), %rcx
	jmp	.L3854
.L3798:
	movl	-96(%rdx), %eax
	leal	1(%rax), %r12d
	jmp	.L3803
.L3827:
	movq	120(%r13), %rsi
	subq	104(%r13), %rdx
	sarq	$3, %rdx
	movq	%rsi, %rax
	subq	88(%r13), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$6, %rax
	addq	%rax, %rdx
	movq	80(%r13), %rax
	subq	64(%r13), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	je	.L3853
	movq	56(%r13), %rdx
	movq	%rsi, %rax
	subq	48(%r13), %rax
	sarq	$3, %rax
	subq	%rax, %rdx
	cmpq	$1, %rdx
	jbe	.L3896
.L3830:
	movl	$512, %edi
	movq	%rsi, -464(%rbp)
	movq	%r9, -456(%rbp)
	movl	%ecx, -448(%rbp)
	call	_Znwm@PLT
	movq	-464(%rbp), %rsi
	movq	-456(%rbp), %r9
	movl	-448(%rbp), %ecx
	movq	%rax, 8(%rsi)
	movq	(%r9), %rdx
	movq	96(%r13), %rax
	movq	%rdx, (%rax)
	movq	120(%r13), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 120(%r13)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 104(%r13)
	movq	%rdx, 112(%r13)
	movq	%rax, 96(%r13)
	jmp	.L3828
.L3794:
	movq	(%r15), %rax
	movl	8(%r15), %esi
	cmpl	%esi, 11(%rax)
	jg	.L3858
.L3862:
	movq	-416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3789
	movq	-344(%rbp), %rax
	movq	-376(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L3863
.L3864:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L3864
	movq	-416(%rbp), %rdi
.L3863:
	call	_ZdlPv@PLT
.L3789:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3897
	addq	$520, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3896:
	.cfi_restore_state
	movl	$1, %esi
	xorl	%edx, %edx
	leaq	48(%r13), %rdi
	movq	%r9, -456(%rbp)
	movl	%ecx, -448(%rbp)
	call	_ZNSt5dequeIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_reallocate_mapEmb
	movq	120(%r13), %rsi
	movq	-456(%rbp), %r9
	movl	-448(%rbp), %ecx
	jmp	.L3830
.L3891:
	movq	-528(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal15TranslatedState18ReadUpdateFeedbackEPNS0_19TranslationIteratorENS0_10FixedArrayEP8_IO_FILE
	jmp	.L3793
.L3888:
	leaq	.LC195(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3889:
	leaq	.LC196(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3890:
	leaq	.LC197(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3858:
	movq	%r15, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv
	testl	%eax, %eax
	je	.L3862
	leaq	.LC201(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3806:
	leaq	.LC58(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3895:
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3853:
	leaq	.LC75(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3897:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE34275:
	.size	_ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi.constprop.0, .-_ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi.constprop.0
	.section	.text._ZN2v88internal15TranslatedStateC2EPKNS0_15JavaScriptFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TranslatedStateC2EPKNS0_15JavaScriptFrameE
	.type	_ZN2v88internal15TranslatedStateC2EPKNS0_15JavaScriptFrameE, @function
_ZN2v88internal15TranslatedStateC2EPKNS0_15JavaScriptFrameE:
.LFB26010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$48, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -16(%rdi)
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, -48(%rdi)
	movups	%xmm0, -32(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	call	_ZNSt11_Deque_baseIN2v88internal15TranslatedState14ObjectPositionESaIS3_EE17_M_initialize_mapEm
	leaq	-68(%rbp), %rsi
	movq	%rbx, %rdi
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movl	$-1, 144(%r12)
	movl	$-1, -68(%rbp)
	call	_ZNK2v88internal14OptimizedFrame21GetDeoptimizationDataEPi@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	movl	-68(%rbp), %eax
	leal	12(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%r13,%rax), %rax
	movq	15(%r13), %rdx
	sarq	$32, %rax
	movq	%rdx, -64(%rbp)
	movl	%eax, -56(%rbp)
	movq	(%rbx), %rax
	call	*152(%rax)
	leaq	-64(%rbp), %rcx
	movq	%r12, %rdi
	movq	23(%rax), %rax
	movq	31(%r13), %r8
	movq	32(%rbx), %rdx
	movq	16(%rbx), %rsi
	movzwl	41(%rax), %r9d
	call	_ZN2v88internal15TranslatedState4InitEPNS0_7IsolateEmPNS0_19TranslationIteratorENS0_10FixedArrayEPNS0_14RegisterValuesEP8_IO_FILEi.constprop.0
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3901
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3901:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26010:
	.size	_ZN2v88internal15TranslatedStateC2EPKNS0_15JavaScriptFrameE, .-_ZN2v88internal15TranslatedStateC2EPKNS0_15JavaScriptFrameE
	.globl	_ZN2v88internal15TranslatedStateC1EPKNS0_15JavaScriptFrameE
	.set	_ZN2v88internal15TranslatedStateC1EPKNS0_15JavaScriptFrameE,_ZN2v88internal15TranslatedStateC2EPKNS0_15JavaScriptFrameE
	.section	.rodata._ZN2v88internal11Deoptimizer24DebuggerInspectableFrameEPNS0_15JavaScriptFrameEiPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC209:
	.string	"frame->is_optimized()"
	.section	.rodata._ZN2v88internal11Deoptimizer24DebuggerInspectableFrameEPNS0_15JavaScriptFrameEiPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC210:
	.string	"frame_it != translated_values.end()"
	.align 8
.LC211:
	.string	"frame_it->kind() == TranslatedFrame::kInterpretedFunction"
	.section	.text._ZN2v88internal11Deoptimizer24DebuggerInspectableFrameEPNS0_15JavaScriptFrameEiPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer24DebuggerInspectableFrameEPNS0_15JavaScriptFrameEiPNS0_7IsolateE
	.type	_ZN2v88internal11Deoptimizer24DebuggerInspectableFrameEPNS0_15JavaScriptFrameEiPNS0_7IsolateE, @function
_ZN2v88internal11Deoptimizer24DebuggerInspectableFrameEPNS0_15JavaScriptFrameEiPNS0_7IsolateE:
.LFB25768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$4, %eax
	jne	.L3949
	leaq	-208(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15TranslatedStateC1EPKNS0_15JavaScriptFrameE
	movq	32(%r12), %rax
	movq	-208(%rbp), %r14
	movq	-200(%rbp), %r12
	movq	%rax, -216(%rbp)
	cmpq	%r12, %r14
	je	.L3908
	.p2align 4,,10
	.p2align 3
.L3907:
	movq	%r14, %rdi
	addq	$120, %r14
	call	_ZN2v88internal15TranslatedFrame8HandlifyEv
	cmpq	%r14, %r12
	jne	.L3907
.L3908:
	movq	-72(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.L3905
.L3906:
	movq	-216(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal15TranslatedState39UpdateFromPreviouslyMaterializedObjectsEv
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %r12
	cmpq	%r12, %rcx
	je	.L3912
	.p2align 4,,10
	.p2align 3
.L3916:
	movl	(%r12), %eax
	leal	-4(%rax), %edx
	cmpl	$1, %edx
	jbe	.L3929
	testl	%eax, %eax
	jne	.L3913
.L3929:
	testl	%ebx, %ebx
	je	.L3915
	subl	$1, %ebx
.L3913:
	addq	$120, %r12
	cmpq	%r12, %rcx
	jne	.L3916
.L3912:
	leaq	.LC210(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3915:
	cmpq	%r12, %rcx
	je	.L3912
	testl	%eax, %eax
	jne	.L3950
	movl	$72, %edi
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal20DeoptimizedFrameInfoC1EPNS0_15TranslatedStateEN9__gnu_cxx17__normal_iteratorIPNS0_15TranslatedFrameESt6vectorIS6_SaIS6_EEEEPNS0_7IsolateE
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3919
	movq	-88(%rbp), %rax
	movq	-120(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L3920
	.p2align 4,,10
	.p2align 3
.L3921:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L3921
	movq	-160(%rbp), %rdi
.L3920:
	call	_ZdlPv@PLT
.L3919:
	movq	-200(%rbp), %r15
	movq	-208(%rbp), %r12
	cmpq	%r12, %r15
	je	.L3922
	.p2align 4,,10
	.p2align 3
.L3926:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3923
	movq	112(%r12), %rax
	movq	80(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L3924
	.p2align 4,,10
	.p2align 3
.L3925:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L3925
	movq	40(%r12), %rdi
.L3924:
	call	_ZdlPv@PLT
.L3923:
	addq	$120, %r12
	cmpq	%r12, %r15
	jne	.L3926
	movq	-208(%rbp), %r12
.L3922:
	testq	%r12, %r12
	je	.L3902
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3902:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3951
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3905:
	.cfi_restore_state
	movq	-184(%rbp), %r12
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3909
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3910:
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	jmp	.L3906
	.p2align 4,,10
	.p2align 3
.L3909:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3952
.L3911:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3910
	.p2align 4,,10
	.p2align 3
.L3949:
	leaq	.LC209(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3950:
	leaq	.LC211(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3952:
	movq	%r12, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	jmp	.L3911
.L3951:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25768:
	.size	_ZN2v88internal11Deoptimizer24DebuggerInspectableFrameEPNS0_15JavaScriptFrameEiPNS0_7IsolateE, .-_ZN2v88internal11Deoptimizer24DebuggerInspectableFrameEPNS0_15JavaScriptFrameEiPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15DeoptimizerDataC2EPNS0_4HeapE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15DeoptimizerDataC2EPNS0_4HeapE, @function
_GLOBAL__sub_I__ZN2v88internal15DeoptimizerDataC2EPNS0_4HeapE:
.LFB33808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE33808:
	.size	_GLOBAL__sub_I__ZN2v88internal15DeoptimizerDataC2EPNS0_4HeapE, .-_GLOBAL__sub_I__ZN2v88internal15DeoptimizerDataC2EPNS0_4HeapE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15DeoptimizerDataC2EPNS0_4HeapE
	.section	.rodata.CSWTCH.1161,"a"
	.align 32
	.type	CSWTCH.1161, @object
	.size	CSWTCH.1161, 108
CSWTCH.1161:
	.long	3
	.long	5
	.long	3
	.long	3
	.long	3
	.long	3
	.long	2
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	2
	.section	.rodata.CSWTCH.551,"a"
	.align 16
	.type	CSWTCH.551, @object
	.size	CSWTCH.551, 16
CSWTCH.551:
	.long	14
	.long	15
	.long	16
	.long	16
	.weak	_ZTVN2v88internal9AssemblerE
	.section	.data.rel.ro.local._ZTVN2v88internal9AssemblerE,"awG",@progbits,_ZTVN2v88internal9AssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal9AssemblerE, @object
	.size	_ZTVN2v88internal9AssemblerE, 40
_ZTVN2v88internal9AssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal9AssemblerD1Ev
	.quad	_ZN2v88internal9AssemblerD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.weak	_ZTVN2v88internal14MacroAssemblerE
	.section	.data.rel.ro._ZTVN2v88internal14MacroAssemblerE,"awG",@progbits,_ZTVN2v88internal14MacroAssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal14MacroAssemblerE, @object
	.size	_ZTVN2v88internal14MacroAssemblerE, 112
_ZTVN2v88internal14MacroAssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14MacroAssemblerD1Ev
	.quad	_ZN2v88internal14MacroAssemblerD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.quad	_ZN2v88internal14TurboAssembler4JumpERKNS0_17ExternalReferenceE
	.quad	_ZN2v88internal14TurboAssembler18CallBuiltinByIndexENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler14JumpCodeObjectENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_
	.quad	_ZN2v88internal14TurboAssembler22LoadFromConstantsTableENS0_8RegisterEi
	.quad	_ZN2v88internal14TurboAssembler22LoadRootRegisterOffsetENS0_8RegisterEl
	.quad	_ZN2v88internal14TurboAssembler16LoadRootRelativeENS0_8RegisterEi
	.quad	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_117ActivationsFinderE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_117ActivationsFinderE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_117ActivationsFinderE, 40
_ZTVN2v88internal12_GLOBAL__N_117ActivationsFinderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_117ActivationsFinder11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE
	.quad	_ZN2v88internal12_GLOBAL__N_117ActivationsFinderD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_117ActivationsFinderD0Ev
	.section	.bss._ZZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeEE28trace_event_unique_atomic418,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeEE28trace_event_unique_atomic418, @object
	.size	_ZZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeEE28trace_event_unique_atomic418, 8
_ZZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeEE28trace_event_unique_atomic418:
	.zero	8
	.section	.bss._ZZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateEE28trace_event_unique_atomic387,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateEE28trace_event_unique_atomic387, @object
	.size	_ZZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateEE28trace_event_unique_atomic387, 8
_ZZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateEE28trace_event_unique_atomic387:
	.zero	8
	.section	.bss._ZZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateEE28trace_event_unique_atomic366,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateEE28trace_event_unique_atomic366, @object
	.size	_ZZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateEE28trace_event_unique_atomic366, 8
_ZZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateEE28trace_event_unique_atomic366:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.weak	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC212:
	.string	"xmm0"
.LC213:
	.string	"xmm1"
.LC214:
	.string	"xmm2"
.LC215:
	.string	"xmm3"
.LC216:
	.string	"xmm4"
.LC217:
	.string	"xmm5"
.LC218:
	.string	"xmm6"
.LC219:
	.string	"xmm7"
.LC220:
	.string	"xmm8"
.LC221:
	.string	"xmm9"
.LC222:
	.string	"xmm10"
.LC223:
	.string	"xmm11"
.LC224:
	.string	"xmm12"
.LC225:
	.string	"xmm13"
.LC226:
	.string	"xmm14"
.LC227:
	.string	"xmm15"
	.section	.data.rel.ro.local._ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names,"awG",@progbits,_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names,comdat
	.align 32
	.type	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names, @gnu_unique_object
	.size	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names, 128
_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names:
	.quad	.LC212
	.quad	.LC213
	.quad	.LC214
	.quad	.LC215
	.quad	.LC216
	.quad	.LC217
	.quad	.LC218
	.quad	.LC219
	.quad	.LC220
	.quad	.LC221
	.quad	.LC222
	.quad	.LC223
	.quad	.LC224
	.quad	.LC225
	.quad	.LC226
	.quad	.LC227
	.weak	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names
	.section	.rodata.str1.1
.LC228:
	.string	"rcx"
.LC229:
	.string	"rdx"
.LC230:
	.string	"rbx"
.LC231:
	.string	"rsp"
.LC232:
	.string	"rbp"
.LC233:
	.string	"rsi"
.LC234:
	.string	"rdi"
.LC235:
	.string	"r8"
.LC236:
	.string	"r9"
.LC237:
	.string	"r10"
.LC238:
	.string	"r11"
.LC239:
	.string	"r12"
.LC240:
	.string	"r13"
.LC241:
	.string	"r14"
.LC242:
	.string	"r15"
	.section	.data.rel.ro.local._ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names,"awG",@progbits,_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names,comdat
	.align 32
	.type	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names, @gnu_unique_object
	.size	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names, 128
_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names:
	.quad	.LC111
	.quad	.LC228
	.quad	.LC229
	.quad	.LC230
	.quad	.LC231
	.quad	.LC232
	.quad	.LC233
	.quad	.LC234
	.quad	.LC235
	.quad	.LC236
	.quad	.LC237
	.quad	.LC238
	.quad	.LC239
	.quad	.LC240
	.quad	.LC241
	.quad	.LC242
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC19:
	.quad	3203260077
	.quad	3203260077
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
