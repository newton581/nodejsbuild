	.file	"debug-stack-trace-iterator.cc"
	.text
	.section	.text._ZNK2v88internal23DebugStackTraceIterator4DoneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal23DebugStackTraceIterator4DoneEv
	.type	_ZNK2v88internal23DebugStackTraceIterator4DoneEv, @function
_ZNK2v88internal23DebugStackTraceIterator4DoneEv:
.LFB18843:
	.cfi_startproc
	endbr64
	cmpq	$0, 1432(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE18843:
	.size	_ZNK2v88internal23DebugStackTraceIterator4DoneEv, .-_ZNK2v88internal23DebugStackTraceIterator4DoneEv
	.section	.text._ZNK2v88internal23DebugStackTraceIterator20GetFunctionDebugNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal23DebugStackTraceIterator20GetFunctionDebugNameEv
	.type	_ZNK2v88internal23DebugStackTraceIterator20GetFunctionDebugNameEv, @function
_ZNK2v88internal23DebugStackTraceIterator20GetFunctionDebugNameEv:
.LFB18848:
	.cfi_startproc
	endbr64
	movq	1456(%rdi), %rax
	movq	64(%rax), %rax
	ret
	.cfi_endproc
.LFE18848:
	.size	_ZNK2v88internal23DebugStackTraceIterator20GetFunctionDebugNameEv, .-_ZNK2v88internal23DebugStackTraceIterator20GetFunctionDebugNameEv
	.section	.text._ZNK2v88internal23DebugStackTraceIterator9GetScriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal23DebugStackTraceIterator9GetScriptEv
	.type	_ZNK2v88internal23DebugStackTraceIterator9GetScriptEv, @function
_ZNK2v88internal23DebugStackTraceIterator9GetScriptEv:
.LFB18849:
	.cfi_startproc
	endbr64
	movq	1456(%rdi), %rax
	movq	40(%rax), %rax
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L5
.L7:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	-1(%rdx), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L7
	ret
	.cfi_endproc
.LFE18849:
	.size	_ZNK2v88internal23DebugStackTraceIterator9GetScriptEv, .-_ZNK2v88internal23DebugStackTraceIterator9GetScriptEv
	.section	.text._ZNK2v88internal23DebugStackTraceIterator11GetFunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal23DebugStackTraceIterator11GetFunctionEv
	.type	_ZNK2v88internal23DebugStackTraceIterator11GetFunctionEv, @function
_ZNK2v88internal23DebugStackTraceIterator11GetFunctionEv:
.LFB18851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	1456(%rdi), %rdi
	call	_ZN2v88internal14FrameInspector12IsJavaScriptEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L9
	movq	1456(%rbx), %rax
	movq	56(%rax), %rax
.L9:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18851:
	.size	_ZNK2v88internal23DebugStackTraceIterator11GetFunctionEv, .-_ZNK2v88internal23DebugStackTraceIterator11GetFunctionEv
	.section	.text._ZNK2v88internal23DebugStackTraceIterator11GetReceiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal23DebugStackTraceIterator11GetReceiverEv
	.type	_ZNK2v88internal23DebugStackTraceIterator11GetReceiverEv, @function
_ZNK2v88internal23DebugStackTraceIterator11GetReceiverEv:
.LFB18846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	1456(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal14FrameInspector12IsJavaScriptEv@PLT
	testb	%al, %al
	movq	1456(%rbx), %rax
	jne	.L37
.L15:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L30
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L42
.L30:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L43
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	8(%rbx), %rcx
	cmpq	96(%rcx), %rdx
	jne	.L30
.L41:
	xorl	%eax, %eax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L37:
	movq	56(%rax), %rdx
	movq	(%rdx), %rcx
	movq	23(%rcx), %rdx
	movl	47(%rdx), %edx
	andl	$31, %edx
	cmpb	$8, %dl
	jne	.L15
	movq	8(%rbx), %r13
	movq	31(%rcx), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L44
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L20:
	movq	-1(%rsi), %rax
	cmpw	$143, 11(%rax)
	jne	.L41
	movq	1456(%rbx), %rdx
	movq	8(%rbx), %rsi
	leaq	-160(%rbp), %r13
	movl	$2, %ecx
	movq	%r13, %rdi
	leaq	-168(%rbp), %r14
	call	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal13ScopeIterator12GetNonLocalsEv@PLT
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	movq	(%rax), %rax
	leaq	3424(%rsi), %rdx
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal9StringSet3HasEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testb	%al, %al
	jne	.L24
.L26:
	xorl	%eax, %eax
.L25:
	movq	%r13, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal13ScopeIteratorD1Ev@PLT
	movq	-184(%rbp), %rax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L44:
	movq	41088(%r13), %r12
	cmpq	%r12, 41096(%r13)
	je	.L45
.L21:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L20
.L45:
	movq	%r13, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L21
.L24:
	movq	8(%rbx), %rax
	movq	%r14, %rdi
	movq	3424(%rax), %r15
	movq	(%r12), %rax
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	leaq	-170(%rbp), %rcx
	leaq	-171(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	leaq	-169(%rbp), %r8
	call	_ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE@PLT
	testl	%eax, %eax
	js	.L26
	movq	8(%rbx), %r14
	movq	(%r12), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %r12
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L27
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
.L28:
	movq	8(%rbx), %rdx
	cmpq	%r12, 96(%rdx)
	jne	.L25
	jmp	.L26
.L27:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L46
.L29:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%r12, (%rax)
	jmp	.L28
.L43:
	call	__stack_chk_fail@PLT
.L46:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L29
	.cfi_endproc
.LFE18846:
	.size	_ZNK2v88internal23DebugStackTraceIterator11GetReceiverEv, .-_ZNK2v88internal23DebugStackTraceIterator11GetReceiverEv
	.section	.text._ZNK2v88internal23DebugStackTraceIterator16GetScopeIteratorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal23DebugStackTraceIterator16GetScopeIteratorEv
	.type	_ZNK2v88internal23DebugStackTraceIterator16GetScopeIteratorEv, @function
_ZNK2v88internal23DebugStackTraceIterator16GetScopeIteratorEv:
.LFB18854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	1432(%rsi), %rdi
	movq	%rsi, %rbx
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$8, %eax
	je	.L51
	movq	1456(%rbx), %r14
	movl	$104, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r13
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZN2v88internal18DebugScopeIteratorC1EPNS0_7IsolateEPNS0_14FrameInspectorE@PLT
	popq	%rbx
	movq	%r13, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	1432(%rbx), %r14
	movl	$32, %edi
	call	_Znwm@PLT
	movl	1464(%rbx), %ecx
	movq	8(%rbx), %rsi
	movq	%rax, %r13
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZN2v88internal22DebugWasmScopeIteratorC1EPNS0_7IsolateEPNS0_13StandardFrameEi@PLT
	popq	%rbx
	movq	%r13, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18854:
	.size	_ZNK2v88internal23DebugStackTraceIterator16GetScopeIteratorEv, .-_ZNK2v88internal23DebugStackTraceIterator16GetScopeIteratorEv
	.section	.text._ZN2v88internal15InterruptsScopeD2Ev,"axG",@progbits,_ZN2v88internal15InterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15InterruptsScopeD2Ev
	.type	_ZN2v88internal15InterruptsScopeD2Ev, @function
_ZN2v88internal15InterruptsScopeD2Ev:
.LFB14312:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	jne	.L54
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
	.cfi_endproc
.LFE14312:
	.size	_ZN2v88internal15InterruptsScopeD2Ev, .-_ZN2v88internal15InterruptsScopeD2Ev
	.weak	_ZN2v88internal15InterruptsScopeD1Ev
	.set	_ZN2v88internal15InterruptsScopeD1Ev,_ZN2v88internal15InterruptsScopeD2Ev
	.section	.text._ZN2v88internal15InterruptsScopeD0Ev,"axG",@progbits,_ZN2v88internal15InterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15InterruptsScopeD0Ev
	.type	_ZN2v88internal15InterruptsScopeD0Ev, @function
_ZN2v88internal15InterruptsScopeD0Ev:
.LFB14314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	je	.L56
	movq	8(%rdi), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L56:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14314:
	.size	_ZN2v88internal15InterruptsScopeD0Ev, .-_ZN2v88internal15InterruptsScopeD0Ev
	.section	.text._ZN2v88internal22SafeForInterruptsScopeD2Ev,"axG",@progbits,_ZN2v88internal22SafeForInterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal22SafeForInterruptsScopeD2Ev
	.type	_ZN2v88internal22SafeForInterruptsScopeD2Ev, @function
_ZN2v88internal22SafeForInterruptsScopeD2Ev:
.LFB23349:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	jne	.L60
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
	.cfi_endproc
.LFE23349:
	.size	_ZN2v88internal22SafeForInterruptsScopeD2Ev, .-_ZN2v88internal22SafeForInterruptsScopeD2Ev
	.weak	_ZN2v88internal22SafeForInterruptsScopeD1Ev
	.set	_ZN2v88internal22SafeForInterruptsScopeD1Ev,_ZN2v88internal22SafeForInterruptsScopeD2Ev
	.section	.text._ZN2v88internal22SafeForInterruptsScopeD0Ev,"axG",@progbits,_ZN2v88internal22SafeForInterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal22SafeForInterruptsScopeD0Ev
	.type	_ZN2v88internal22SafeForInterruptsScopeD0Ev, @function
_ZN2v88internal22SafeForInterruptsScopeD0Ev:
.LFB23351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	je	.L62
	movq	8(%rdi), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L62:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23351:
	.size	_ZN2v88internal22SafeForInterruptsScopeD0Ev, .-_ZN2v88internal22SafeForInterruptsScopeD0Ev
	.section	.text._ZN2v88internal23DebugStackTraceIterator8EvaluateENS_5LocalINS_6StringEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23DebugStackTraceIterator8EvaluateENS_5LocalINS_6StringEEEb
	.type	_ZN2v88internal23DebugStackTraceIterator8EvaluateENS_5LocalINS_6StringEEEb, @function
_ZN2v88internal23DebugStackTraceIterator8EvaluateENS_5LocalINS_6StringEEEb:
.LFB18856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	movl	$255, %edx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	8(%rdi), %rsi
	leaq	-96(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE@PLT
	movq	1432(%rbx), %rdi
	leaq	16+_ZTVN2v88internal22SafeForInterruptsScopeE(%rip), %rax
	movl	1464(%rbx), %r14d
	movq	%rax, -96(%rbp)
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	8(%rbx), %rdi
	movq	%r12, %rcx
	movzbl	%r13b, %r8d
	movq	%rax, %rsi
	movl	%r14d, %edx
	call	_ZN2v88internal13DebugEvaluate5LocalEPNS0_7IsolateENS0_12StackFrameIdEiNS0_6HandleINS0_6StringEEEb@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L70
.L66:
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, -64(%rbp)
	movq	%rax, -96(%rbp)
	je	.L67
	movq	-88(%rbp), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L67:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	jmp	.L66
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18856:
	.size	_ZN2v88internal23DebugStackTraceIterator8EvaluateENS_5LocalINS_6StringEEEb, .-_ZN2v88internal23DebugStackTraceIterator8EvaluateENS_5LocalINS_6StringEEEb
	.section	.text._ZN2v88internal23DebugStackTraceIterator7RestartEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23DebugStackTraceIterator7RestartEv
	.type	_ZN2v88internal23DebugStackTraceIterator7RestartEv, @function
_ZN2v88internal23DebugStackTraceIterator7RestartEv:
.LFB18855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	1432(%rdi), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$5, %eax
	je	.L73
	cmpl	$8, %eax
	je	.L73
	movq	1432(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8LiveEdit12RestartFrameEPNS0_15JavaScriptFrameE@PLT
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18855:
	.size	_ZN2v88internal23DebugStackTraceIterator7RestartEv, .-_ZN2v88internal23DebugStackTraceIterator7RestartEv
	.section	.text._ZNK2v88internal23DebugStackTraceIterator14GetReturnValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal23DebugStackTraceIterator14GetReturnValueEv
	.type	_ZNK2v88internal23DebugStackTraceIterator14GetReturnValueEv, @function
_ZNK2v88internal23DebugStackTraceIterator14GetReturnValueEv:
.LFB18847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	1456(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L76
	call	_ZN2v88internal14FrameInspector6IsWasmEv@PLT
	testb	%al, %al
	je	.L76
.L79:
	xorl	%eax, %eax
.L77:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	1432(%rbx), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$4, %eax
	je	.L79
	cmpb	$0, 1468(%rbx)
	je	.L79
	movq	8(%rbx), %rax
	movq	1432(%rbx), %rsi
	movq	41472(%rax), %rdi
	call	_ZN2v88internal5Debug15IsBreakAtReturnEPNS0_15JavaScriptFrameE@PLT
	testb	%al, %al
	je	.L79
	movq	8(%rbx), %rax
	movq	41472(%rax), %rdi
	call	_ZN2v88internal5Debug19return_value_handleEv@PLT
	jmp	.L77
	.cfi_endproc
.LFE18847:
	.size	_ZNK2v88internal23DebugStackTraceIterator14GetReturnValueEv, .-_ZNK2v88internal23DebugStackTraceIterator14GetReturnValueEv
	.section	.text._ZNK2v88internal23DebugStackTraceIterator12GetContextIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal23DebugStackTraceIterator12GetContextIdEv
	.type	_ZNK2v88internal23DebugStackTraceIterator12GetContextIdEv, @function
_ZNK2v88internal23DebugStackTraceIterator12GetContextIdEv:
.LFB18845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	1456(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal14FrameInspector10GetContextEv@PLT
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L94
.L96:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	subw	$138, %dx
	cmpw	$9, %dx
	ja	.L96
	movq	39(%rax), %rax
	movq	335(%rax), %rdx
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rdx, %rcx
	sarq	$32, %rcx
	andl	$1, %edx
	cmove	%ecx, %eax
	ret
	.cfi_endproc
.LFE18845:
	.size	_ZNK2v88internal23DebugStackTraceIterator12GetContextIdEv, .-_ZNK2v88internal23DebugStackTraceIterator12GetContextIdEv
	.section	.text._ZN2v88internal23DebugStackTraceIteratorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23DebugStackTraceIteratorD2Ev
	.type	_ZN2v88internal23DebugStackTraceIteratorD2Ev, @function
_ZN2v88internal23DebugStackTraceIteratorD2Ev:
.LFB18840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23DebugStackTraceIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	1456(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L99
	movq	%r12, %rdi
	call	_ZN2v88internal14FrameInspectorD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18840:
	.size	_ZN2v88internal23DebugStackTraceIteratorD2Ev, .-_ZN2v88internal23DebugStackTraceIteratorD2Ev
	.globl	_ZN2v88internal23DebugStackTraceIteratorD1Ev
	.set	_ZN2v88internal23DebugStackTraceIteratorD1Ev,_ZN2v88internal23DebugStackTraceIteratorD2Ev
	.section	.text._ZN2v88internal23DebugStackTraceIteratorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23DebugStackTraceIteratorD0Ev
	.type	_ZN2v88internal23DebugStackTraceIteratorD0Ev, @function
_ZN2v88internal23DebugStackTraceIteratorD0Ev:
.LFB18842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23DebugStackTraceIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	1456(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L103
	movq	%r13, %rdi
	call	_ZN2v88internal14FrameInspectorD1Ev@PLT
	movl	$80, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L103:
	movq	%r12, %rdi
	movl	$1472, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18842:
	.size	_ZN2v88internal23DebugStackTraceIteratorD0Ev, .-_ZN2v88internal23DebugStackTraceIteratorD0Ev
	.section	.text._ZN2v88internal23DebugStackTraceIterator7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23DebugStackTraceIterator7AdvanceEv
	.type	_ZN2v88internal23DebugStackTraceIterator7AdvanceEv, @function
_ZN2v88internal23DebugStackTraceIterator7AdvanceEv:
.LFB18844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movabsq	$7905747460161236407, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L118:
	movl	1464(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 1464(%rbx)
	testl	%edx, %edx
	js	.L112
	leaq	-96(%rbp), %r14
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L133:
	movl	1464(%rbx), %eax
	movb	$0, 1468(%rbx)
	leal	-1(%rax), %edx
	movl	%edx, 1464(%rbx)
	testl	%edx, %edx
	js	.L112
.L111:
	movq	1432(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12FrameSummary3GetEPKNS0_13StandardFrameEi@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal12FrameSummary23is_subject_to_debuggingEv@PLT
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	testb	%r12b, %r12b
	je	.L133
	movl	1464(%rbx), %eax
	testl	%eax, %eax
	jns	.L134
.L112:
	movq	1456(%rbx), %r12
	movb	$0, 1468(%rbx)
	movq	$0, 1456(%rbx)
	testq	%r12, %r12
	je	.L120
	movq	%r12, %rdi
	call	_ZN2v88internal14FrameInspectorD1Ev@PLT
	movl	$80, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L120:
	leaq	16(%rbx), %rdi
	call	_ZN2v88internal23StackTraceFrameIterator7AdvanceEv@PLT
	movq	1432(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L108
	pxor	%xmm0, %xmm0
	movq	$0, -80(%rbp)
	leaq	-96(%rbp), %r14
	movaps	%xmm0, -96(%rbp)
	movq	(%rdi), %rax
	movq	%r14, %rsi
	call	*136(%rax)
	movq	-88(%rbp), %r14
	movq	-96(%rbp), %r12
	movq	%r14, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	imulq	%r13, %rax
	movl	%eax, 1464(%rbx)
	cmpq	%r14, %r12
	je	.L115
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r12, %rdi
	addq	$56, %r12
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%r12, %r14
	jne	.L116
	movq	-96(%rbp), %r14
.L115:
	testq	%r14, %r14
	je	.L118
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L118
.L134:
	movq	1432(%rbx), %r13
	movl	$80, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rcx
	movl	1464(%rbx), %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal14FrameInspectorC1EPNS0_13StandardFrameEiPNS0_7IsolateE@PLT
	movq	1456(%rbx), %r13
	movq	%r12, 1456(%rbx)
	testq	%r13, %r13
	je	.L108
	movq	%r13, %rdi
	call	_ZN2v88internal14FrameInspectorD1Ev@PLT
	movl	$80, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L108:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L135:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18844:
	.size	_ZN2v88internal23DebugStackTraceIterator7AdvanceEv, .-_ZN2v88internal23DebugStackTraceIterator7AdvanceEv
	.section	.text._ZNK2v88internal23DebugStackTraceIterator17GetSourceLocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal23DebugStackTraceIterator17GetSourceLocationEv
	.type	_ZNK2v88internal23DebugStackTraceIterator17GetSourceLocationEv, @function
_ZNK2v88internal23DebugStackTraceIterator17GetSourceLocationEv:
.LFB18850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	1456(%rdi), %rax
	movq	40(%rax), %rdi
	movq	(%rdi), %rdx
	testb	$1, %dl
	jne	.L137
.L139:
	leaq	-20(%rbp), %rdi
	call	_ZN2v85debug8LocationC1Ev@PLT
.L138:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	-20(%rbp), %rax
	movl	-12(%rbp), %edx
	jne	.L142
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L139
	movl	72(%rax), %esi
	call	_ZNK2v85debug6Script17GetSourceLocationEi@PLT
	movq	%rax, -20(%rbp)
	movl	%edx, -12(%rbp)
	jmp	.L138
.L142:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18850:
	.size	_ZNK2v88internal23DebugStackTraceIterator17GetSourceLocationEv, .-_ZNK2v88internal23DebugStackTraceIterator17GetSourceLocationEv
	.section	.text._ZN2v88internal23DebugStackTraceIteratorC2EPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23DebugStackTraceIteratorC2EPNS0_7IsolateEi
	.type	_ZN2v88internal23DebugStackTraceIteratorC2EPNS0_7IsolateEi, @function
_ZN2v88internal23DebugStackTraceIteratorC2EPNS0_7IsolateEi:
.LFB18837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal23DebugStackTraceIteratorE(%rip), %rax
	movq	%rsi, -8(%rdi)
	movq	%rax, -16(%rdi)
	movq	41472(%rsi), %rax
	movl	72(%rax), %edx
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateENS0_12StackFrameIdE@PLT
	movq	1432(%r12), %rdi
	movq	$0, 1456(%r12)
	movb	$1, 1468(%r12)
	testq	%rdi, %rdi
	je	.L143
	pxor	%xmm0, %xmm0
	movq	$0, -32(%rbp)
	leaq	-48(%rbp), %rsi
	movaps	%xmm0, -48(%rbp)
	movq	(%rdi), %rax
	call	*136(%rax)
	movq	-40(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%r12, %rdi
	movabsq	$7905747460161236407, %rdx
	sarq	$3, %rax
	imulq	%rdx, %rax
	movl	%eax, 1464(%r12)
	call	_ZN2v88internal23DebugStackTraceIterator7AdvanceEv
	cmpq	$0, 1432(%r12)
	jne	.L167
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r12, %rdi
	subl	$1, %ebx
	call	_ZN2v88internal23DebugStackTraceIterator7AdvanceEv
	cmpq	$0, 1432(%r12)
	je	.L150
.L167:
	testl	%ebx, %ebx
	jg	.L145
.L150:
	movq	-40(%rbp), %rbx
	movq	-48(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L147
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%r12, %rdi
	addq	$56, %r12
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%r12, %rbx
	jne	.L148
	movq	-48(%rbp), %r12
.L147:
	testq	%r12, %r12
	je	.L143
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L143:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L168
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L168:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18837:
	.size	_ZN2v88internal23DebugStackTraceIteratorC2EPNS0_7IsolateEi, .-_ZN2v88internal23DebugStackTraceIteratorC2EPNS0_7IsolateEi
	.globl	_ZN2v88internal23DebugStackTraceIteratorC1EPNS0_7IsolateEi
	.set	_ZN2v88internal23DebugStackTraceIteratorC1EPNS0_7IsolateEi,_ZN2v88internal23DebugStackTraceIteratorC2EPNS0_7IsolateEi
	.section	.text._ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi
	.type	_ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi, @function
_ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi:
.LFB18816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$1472, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	call	_Znwm@PLT
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN2v88internal23DebugStackTraceIteratorC1EPNS0_7IsolateEi
	movq	%rbx, (%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18816:
	.size	_ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi, .-_ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi, @function
_GLOBAL__sub_I__ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi:
.LFB23437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23437:
	.size	_GLOBAL__sub_I__ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi, .-_GLOBAL__sub_I__ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi
	.weak	_ZTVN2v88internal15InterruptsScopeE
	.section	.data.rel.ro.local._ZTVN2v88internal15InterruptsScopeE,"awG",@progbits,_ZTVN2v88internal15InterruptsScopeE,comdat
	.align 8
	.type	_ZTVN2v88internal15InterruptsScopeE, @object
	.size	_ZTVN2v88internal15InterruptsScopeE, 32
_ZTVN2v88internal15InterruptsScopeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15InterruptsScopeD1Ev
	.quad	_ZN2v88internal15InterruptsScopeD0Ev
	.weak	_ZTVN2v88internal22SafeForInterruptsScopeE
	.section	.data.rel.ro.local._ZTVN2v88internal22SafeForInterruptsScopeE,"awG",@progbits,_ZTVN2v88internal22SafeForInterruptsScopeE,comdat
	.align 8
	.type	_ZTVN2v88internal22SafeForInterruptsScopeE, @object
	.size	_ZTVN2v88internal22SafeForInterruptsScopeE, 32
_ZTVN2v88internal22SafeForInterruptsScopeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal22SafeForInterruptsScopeD1Ev
	.quad	_ZN2v88internal22SafeForInterruptsScopeD0Ev
	.weak	_ZTVN2v88internal23DebugStackTraceIteratorE
	.section	.data.rel.ro.local._ZTVN2v88internal23DebugStackTraceIteratorE,"awG",@progbits,_ZTVN2v88internal23DebugStackTraceIteratorE,comdat
	.align 8
	.type	_ZTVN2v88internal23DebugStackTraceIteratorE, @object
	.size	_ZTVN2v88internal23DebugStackTraceIteratorE, 128
_ZTVN2v88internal23DebugStackTraceIteratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23DebugStackTraceIteratorD1Ev
	.quad	_ZN2v88internal23DebugStackTraceIteratorD0Ev
	.quad	_ZNK2v88internal23DebugStackTraceIterator4DoneEv
	.quad	_ZN2v88internal23DebugStackTraceIterator7AdvanceEv
	.quad	_ZNK2v88internal23DebugStackTraceIterator12GetContextIdEv
	.quad	_ZNK2v88internal23DebugStackTraceIterator11GetReceiverEv
	.quad	_ZNK2v88internal23DebugStackTraceIterator14GetReturnValueEv
	.quad	_ZNK2v88internal23DebugStackTraceIterator20GetFunctionDebugNameEv
	.quad	_ZNK2v88internal23DebugStackTraceIterator9GetScriptEv
	.quad	_ZNK2v88internal23DebugStackTraceIterator17GetSourceLocationEv
	.quad	_ZNK2v88internal23DebugStackTraceIterator11GetFunctionEv
	.quad	_ZNK2v88internal23DebugStackTraceIterator16GetScopeIteratorEv
	.quad	_ZN2v88internal23DebugStackTraceIterator7RestartEv
	.quad	_ZN2v88internal23DebugStackTraceIterator8EvaluateENS_5LocalINS_6StringEEEb
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
