	.file	"frame-constants-x64.cc"
	.text
	.section	.text._ZN2v88internal15JavaScriptFrame11fp_registerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JavaScriptFrame11fp_registerEv
	.type	_ZN2v88internal15JavaScriptFrame11fp_registerEv, @function
_ZN2v88internal15JavaScriptFrame11fp_registerEv:
.LFB19263:
	.cfi_startproc
	endbr64
	movl	$5, %eax
	ret
	.cfi_endproc
.LFE19263:
	.size	_ZN2v88internal15JavaScriptFrame11fp_registerEv, .-_ZN2v88internal15JavaScriptFrame11fp_registerEv
	.section	.text._ZN2v88internal15JavaScriptFrame16context_registerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JavaScriptFrame16context_registerEv
	.type	_ZN2v88internal15JavaScriptFrame16context_registerEv, @function
_ZN2v88internal15JavaScriptFrame16context_registerEv:
.LFB19264:
	.cfi_startproc
	endbr64
	movl	$6, %eax
	ret
	.cfi_endproc
.LFE19264:
	.size	_ZN2v88internal15JavaScriptFrame16context_registerEv, .-_ZN2v88internal15JavaScriptFrame16context_registerEv
	.section	.rodata._ZN2v88internal15JavaScriptFrame30constant_pool_pointer_registerEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal15JavaScriptFrame30constant_pool_pointer_registerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JavaScriptFrame30constant_pool_pointer_registerEv
	.type	_ZN2v88internal15JavaScriptFrame30constant_pool_pointer_registerEv, @function
_ZN2v88internal15JavaScriptFrame30constant_pool_pointer_registerEv:
.LFB19265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19265:
	.size	_ZN2v88internal15JavaScriptFrame30constant_pool_pointer_registerEv, .-_ZN2v88internal15JavaScriptFrame30constant_pool_pointer_registerEv
	.section	.text._ZN2v88internal25InterpreterFrameConstants22RegisterStackSlotCountEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25InterpreterFrameConstants22RegisterStackSlotCountEi
	.type	_ZN2v88internal25InterpreterFrameConstants22RegisterStackSlotCountEi, @function
_ZN2v88internal25InterpreterFrameConstants22RegisterStackSlotCountEi:
.LFB19266:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE19266:
	.size	_ZN2v88internal25InterpreterFrameConstants22RegisterStackSlotCountEi, .-_ZN2v88internal25InterpreterFrameConstants22RegisterStackSlotCountEi
	.section	.text._ZN2v88internal33BuiltinContinuationFrameConstants16PaddingSlotCountEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33BuiltinContinuationFrameConstants16PaddingSlotCountEi
	.type	_ZN2v88internal33BuiltinContinuationFrameConstants16PaddingSlotCountEi, @function
_ZN2v88internal33BuiltinContinuationFrameConstants16PaddingSlotCountEi:
.LFB19267:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19267:
	.size	_ZN2v88internal33BuiltinContinuationFrameConstants16PaddingSlotCountEi, .-_ZN2v88internal33BuiltinContinuationFrameConstants16PaddingSlotCountEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15JavaScriptFrame11fp_registerEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15JavaScriptFrame11fp_registerEv, @function
_GLOBAL__sub_I__ZN2v88internal15JavaScriptFrame11fp_registerEv:
.LFB22998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22998:
	.size	_GLOBAL__sub_I__ZN2v88internal15JavaScriptFrame11fp_registerEv, .-_GLOBAL__sub_I__ZN2v88internal15JavaScriptFrame11fp_registerEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15JavaScriptFrame11fp_registerEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
