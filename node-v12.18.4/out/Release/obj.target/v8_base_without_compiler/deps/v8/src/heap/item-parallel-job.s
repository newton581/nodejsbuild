	.file	"item-parallel-job.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB3875:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE3875:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB3876:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3876:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,"axG",@progbits,_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.type	_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, @function
_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE:
.LFB3885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rdx, -16(%rbp)
	movq	56(%rax), %rax
	movq	$0, (%rsi)
	leaq	-16(%rbp), %rsi
	call	*%rax
	movq	-16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4
	movq	(%rdi), %rax
	call	*8(%rax)
.L4:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3885:
	.size	_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, .-_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.section	.text._ZN2v88internal15ItemParallelJob4Task11RunInternalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ItemParallelJob4Task11RunInternalEv
	.type	_ZN2v88internal15ItemParallelJob4Task11RunInternalEv, @function
_ZN2v88internal15ItemParallelJob4Task11RunInternalEv:
.LFB9183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movl	64(%rdi), %esi
	call	*32(%rax)
	movq	72(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base9Semaphore6SignalEv@PLT
	.cfi_endproc
.LFE9183:
	.size	_ZN2v88internal15ItemParallelJob4Task11RunInternalEv, .-_ZN2v88internal15ItemParallelJob4Task11RunInternalEv
	.section	.text._ZN2v88internal15ItemParallelJob4ItemD0Ev,"axG",@progbits,_ZN2v88internal15ItemParallelJob4ItemD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15ItemParallelJob4ItemD0Ev
	.type	_ZN2v88internal15ItemParallelJob4ItemD0Ev, @function
_ZN2v88internal15ItemParallelJob4ItemD0Ev:
.LFB10792:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10792:
	.size	_ZN2v88internal15ItemParallelJob4ItemD0Ev, .-_ZN2v88internal15ItemParallelJob4ItemD0Ev
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14CancelableTask3RunEv
	.type	_ZThn32_N2v88internal14CancelableTask3RunEv, @function
_ZThn32_N2v88internal14CancelableTask3RunEv:
.LFB10821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L15
	movq	-32(%rbx), %rdx
	leaq	_ZN2v88internal15ItemParallelJob4Task11RunInternalEv(%rip), %rcx
	subq	$32, %rdi
	movq	24(%rdx), %rax
	cmpq	%rcx, %rax
	je	.L19
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L15:
	.cfi_restore_state
	popq	%rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	movl	32(%rbx), %esi
	call	*32(%rdx)
	movq	40(%rbx), %rdi
	popq	%rdx
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base9Semaphore6SignalEv@PLT
	.cfi_endproc
.LFE10821:
	.size	_ZThn32_N2v88internal14CancelableTask3RunEv, .-_ZThn32_N2v88internal14CancelableTask3RunEv
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CancelableTask3RunEv
	.type	_ZN2v88internal14CancelableTask3RunEv, @function
_ZN2v88internal14CancelableTask3RunEv:
.LFB4029:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L20
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal15ItemParallelJob4Task11RunInternalEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L22
	movl	64(%rdi), %esi
	call	*32(%rax)
	movq	72(%r12), %rdi
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base9Semaphore6SignalEv@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rdx
.L20:
	ret
	.cfi_endproc
.LFE4029:
	.size	_ZN2v88internal14CancelableTask3RunEv, .-_ZN2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE
	.type	_ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE, @function
_ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE:
.LFB9179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal15ItemParallelJob4TaskE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 40(%rbx)
	movq	%rax, (%rbx)
	addq	$56, %rax
	movq	%rax, 32(%rbx)
	movl	$1, 64(%rbx)
	movq	$0, 72(%rbx)
	movups	%xmm0, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9179:
	.size	_ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE, .-_ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE
	.globl	_ZN2v88internal15ItemParallelJob4TaskC1EPNS0_7IsolateE
	.set	_ZN2v88internal15ItemParallelJob4TaskC1EPNS0_7IsolateE,_ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal15ItemParallelJob4Task13SetupInternalEPNS_4base9SemaphoreEPSt6vectorIPNS1_4ItemESaIS8_EEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ItemParallelJob4Task13SetupInternalEPNS_4base9SemaphoreEPSt6vectorIPNS1_4ItemESaIS8_EEm
	.type	_ZN2v88internal15ItemParallelJob4Task13SetupInternalEPNS_4base9SemaphoreEPSt6vectorIPNS1_4ItemESaIS8_EEm, @function
_ZN2v88internal15ItemParallelJob4Task13SetupInternalEPNS_4base9SemaphoreEPSt6vectorIPNS1_4ItemESaIS8_EEm:
.LFB9181:
	.cfi_startproc
	endbr64
	movq	%rsi, 72(%rdi)
	movq	%rdx, 40(%rdi)
	movq	8(%rdx), %rax
	subq	(%rdx), %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jnb	.L29
	movq	%rcx, 48(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rax, 56(%rdi)
	ret
	.cfi_endproc
.LFE9181:
	.size	_ZN2v88internal15ItemParallelJob4Task13SetupInternalEPNS_4base9SemaphoreEPSt6vectorIPNS1_4ItemESaIS8_EEm, .-_ZN2v88internal15ItemParallelJob4Task13SetupInternalEPNS_4base9SemaphoreEPSt6vectorIPNS1_4ItemESaIS8_EEm
	.section	.text._ZN2v88internal15ItemParallelJob4Task19WillRunOnForegroundEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ItemParallelJob4Task19WillRunOnForegroundEv
	.type	_ZN2v88internal15ItemParallelJob4Task19WillRunOnForegroundEv, @function
_ZN2v88internal15ItemParallelJob4Task19WillRunOnForegroundEv:
.LFB9182:
	.cfi_startproc
	endbr64
	movl	$0, 64(%rdi)
	ret
	.cfi_endproc
.LFE9182:
	.size	_ZN2v88internal15ItemParallelJob4Task19WillRunOnForegroundEv, .-_ZN2v88internal15ItemParallelJob4Task19WillRunOnForegroundEv
	.section	.text._ZN2v88internal15ItemParallelJobC2EPNS0_21CancelableTaskManagerEPNS_4base9SemaphoreE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ItemParallelJobC2EPNS0_21CancelableTaskManagerEPNS_4base9SemaphoreE
	.type	_ZN2v88internal15ItemParallelJobC2EPNS0_21CancelableTaskManagerEPNS_4base9SemaphoreE, @function
_ZN2v88internal15ItemParallelJobC2EPNS0_21CancelableTaskManagerEPNS_4base9SemaphoreE:
.LFB9203:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	%rdx, %xmm1
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 48(%rdi)
	ret
	.cfi_endproc
.LFE9203:
	.size	_ZN2v88internal15ItemParallelJobC2EPNS0_21CancelableTaskManagerEPNS_4base9SemaphoreE, .-_ZN2v88internal15ItemParallelJobC2EPNS0_21CancelableTaskManagerEPNS_4base9SemaphoreE
	.globl	_ZN2v88internal15ItemParallelJobC1EPNS0_21CancelableTaskManagerEPNS_4base9SemaphoreE
	.set	_ZN2v88internal15ItemParallelJobC1EPNS0_21CancelableTaskManagerEPNS_4base9SemaphoreE,_ZN2v88internal15ItemParallelJobC2EPNS0_21CancelableTaskManagerEPNS_4base9SemaphoreE
	.section	.rodata._ZN2v88internal15ItemParallelJobD2Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"item->IsFinished()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal15ItemParallelJobD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ItemParallelJobD2Ev
	.type	_ZN2v88internal15ItemParallelJobD2Ev, @function
_ZN2v88internal15ItemParallelJobD2Ev:
.LFB9206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	_ZN2v88internal15ItemParallelJob4ItemD0Ev(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	movq	(%rdi), %rdx
	cmpq	%rdx, 8(%rdi)
	jne	.L34
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L38:
	movq	0(%r13), %rdx
	movq	8(%r13), %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L40
.L34:
	movq	(%rdx,%rbx,8), %rdi
	movq	8(%rdi), %rax
	cmpq	$2, %rax
	jne	.L57
	testq	%rdi, %rdi
	je	.L38
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	je	.L58
	call	*%rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	movq	32(%r13), %rbx
	movq	24(%r13), %r12
	cmpq	%r12, %rbx
	je	.L35
	.p2align 4,,10
	.p2align 3
.L36:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L41
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L36
.L42:
	movq	24(%r13), %r12
.L35:
	testq	%r12, %r12
	je	.L44
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L44:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L33
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L36
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9206:
	.size	_ZN2v88internal15ItemParallelJobD2Ev, .-_ZN2v88internal15ItemParallelJobD2Ev
	.globl	_ZN2v88internal15ItemParallelJobD1Ev
	.set	_ZN2v88internal15ItemParallelJobD1Ev,_ZN2v88internal15ItemParallelJobD2Ev
	.section	.rodata._ZN2v88internal15ItemParallelJob3RunEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"disabled-by-default-v8.gc"
.LC3:
	.string	"num_tasks"
.LC4:
	.string	"num_items"
.LC5:
	.string	"ItemParallelJob::Run"
	.section	.text._ZN2v88internal15ItemParallelJob3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ItemParallelJob3RunEv
	.type	_ZN2v88internal15ItemParallelJob3RunEv, @function
_ZN2v88internal15ItemParallelJob3RunEv:
.LFB9208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	subq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	subq	24(%rdi), %rax
	sarq	$3, %rbx
	movq	%rax, -168(%rbp)
	sarq	$3, %rax
	movq	%rax, -144(%rbp)
	movq	_ZZN2v88internal15ItemParallelJob3RunEvE27trace_event_unique_atomic56(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L114
.L61:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L115
.L63:
	movq	$0, -160(%rbp)
	movq	32(%r15), %r13
	subq	24(%r15), %r13
	sarq	$3, %r13
	cmpq	%rbx, %r13
	cmova	%rbx, %r13
	movq	%r13, -152(%rbp)
	testq	%r13, %r13
	je	.L67
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%r13
	movq	%rdx, -160(%rbp)
	movq	%rax, -152(%rbp)
.L67:
	movq	-144(%rbp), %rbx
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rbx
	ja	.L68
	movq	-168(%rbp), %rdi
	call	_Znam@PLT
	movq	%rax, -136(%rbp)
	testq	%rbx, %rbx
	je	.L69
.L83:
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%rbx, 48(%r14)
.L71:
	movq	24(%r14), %rax
	movq	-136(%rbp), %rcx
	movq	%rax, (%rcx,%r13,8)
	testq	%r13, %r13
	jne	.L116
	testq	%r12, %r12
	je	.L87
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%r14, %r12
	call	*8(%rax)
.L77:
	addq	$1, %r13
	cmpq	-160(%rbp), %r13
	adcq	-152(%rbp), %rbx
	cmpq	-144(%rbp), %r13
	je	.L117
.L78:
	movq	24(%r15), %rax
	leaq	(%rax,%r13,8), %rax
	movq	(%rax), %r14
	movq	$0, (%rax)
	movq	56(%r15), %rax
	movq	%r15, 40(%r14)
	movq	%rax, 72(%r14)
	movq	8(%r15), %rax
	subq	(%r15), %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	ja	.L118
	movq	%rax, 56(%r14)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L116:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	32(%r14), %rdx
	leaq	_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%rdx, -128(%rbp)
	movq	64(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L73
	movq	(%rdi), %rax
	leaq	-120(%rbp), %rsi
	movq	$0, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	*56(%rax)
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L75
	movq	(%rdi), %rax
	call	*8(%rax)
.L75:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r14, %r12
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$0, 64(%r12)
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, 16(%r12)
	jne	.L79
	movq	(%r12), %rax
	leaq	_ZN2v88internal15ItemParallelJob4Task11RunInternalEv(%rip), %rcx
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L80
	movq	%r12, %rdi
	movl	64(%r12), %esi
	call	*32(%rax)
	movq	72(%r12), %rdi
	call	_ZN2v84base9Semaphore6SignalEv@PLT
.L79:
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %r13
	movq	%rax, %rbx
	addq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L82:
	movq	48(%r15), %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L81
	movq	56(%r15), %rdi
	call	_ZN2v84base9Semaphore4WaitEv@PLT
.L81:
	addq	$8, %rbx
	cmpq	%r13, %rbx
	jne	.L82
	movq	-136(%rbp), %rdi
	call	_ZdaPv@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	leaq	-128(%rbp), %rsi
	call	*%rax
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L68:
	movq	$-1, %rdi
	call	_Znam@PLT
	movq	%rax, -136(%rbp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L115:
	leaq	.LC4(%rip), %rax
	leaq	.LC3(%rip), %rsi
	movq	%rax, %xmm1
	movq	%rsi, %xmm0
	movl	$771, %eax
	movslq	-144(%rbp), %rsi
	punpcklqdq	%xmm1, %xmm0
	movw	%ax, -58(%rbp)
	movslq	%ebx, %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, %xmm2
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L120
.L64:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L65
	movq	(%rdi), %rax
	call	*8(%rax)
.L65:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L63
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L114:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L121
.L62:
	movq	%r12, _ZZN2v88internal15ItemParallelJob3RunEvE27trace_event_unique_atomic56(%rip)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$0, 64
	ud2
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L79
.L121:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L62
.L120:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$16
	leaq	.LC5(%rip), %rcx
	movl	$73, %esi
	pushq	%rdx
	leaq	-96(%rbp), %rdx
	pushq	%rdx
	leaq	-58(%rbp), %rdx
	pushq	%rdx
	leaq	-112(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$2
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L64
.L119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9208:
	.size	_ZN2v88internal15ItemParallelJob3RunEv, .-_ZN2v88internal15ItemParallelJob3RunEv
	.section	.text._ZN2v88internal15ItemParallelJob4ItemD2Ev,"axG",@progbits,_ZN2v88internal15ItemParallelJob4ItemD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15ItemParallelJob4ItemD2Ev
	.type	_ZN2v88internal15ItemParallelJob4ItemD2Ev, @function
_ZN2v88internal15ItemParallelJob4ItemD2Ev:
.LFB10790:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10790:
	.size	_ZN2v88internal15ItemParallelJob4ItemD2Ev, .-_ZN2v88internal15ItemParallelJob4ItemD2Ev
	.weak	_ZN2v88internal15ItemParallelJob4ItemD1Ev
	.set	_ZN2v88internal15ItemParallelJob4ItemD1Ev,_ZN2v88internal15ItemParallelJob4ItemD2Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE:
.LFB10801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10801:
	.size	_GLOBAL__sub_I__ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15ItemParallelJob4TaskC2EPNS0_7IsolateE
	.weak	_ZTVN2v88internal15ItemParallelJob4TaskE
	.section	.data.rel.ro._ZTVN2v88internal15ItemParallelJob4TaskE,"awG",@progbits,_ZTVN2v88internal15ItemParallelJob4TaskE,comdat
	.align 8
	.type	_ZTVN2v88internal15ItemParallelJob4TaskE, @object
	.size	_ZTVN2v88internal15ItemParallelJob4TaskE, 96
_ZTVN2v88internal15ItemParallelJob4TaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal15ItemParallelJob4Task11RunInternalEv
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.bss._ZZN2v88internal15ItemParallelJob3RunEvE27trace_event_unique_atomic56,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal15ItemParallelJob3RunEvE27trace_event_unique_atomic56, @object
	.size	_ZZN2v88internal15ItemParallelJob3RunEvE27trace_event_unique_atomic56, 8
_ZZN2v88internal15ItemParallelJob3RunEvE27trace_event_unique_atomic56:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
