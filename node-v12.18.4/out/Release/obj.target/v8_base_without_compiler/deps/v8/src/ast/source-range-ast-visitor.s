	.file	"source-range-ast-visitor.cc"
	.text
	.section	.text._ZN2v88internal21SourceRangeAstVisitorC2EmPNS0_10ExpressionEPNS0_14SourceRangeMapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SourceRangeAstVisitorC2EmPNS0_10ExpressionEPNS0_14SourceRangeMapE
	.type	_ZN2v88internal21SourceRangeAstVisitorC2EmPNS0_10ExpressionEPNS0_14SourceRangeMapE, @function
_ZN2v88internal21SourceRangeAstVisitorC2EmPNS0_10ExpressionEPNS0_14SourceRangeMapE:
.LFB10590:
	.cfi_startproc
	endbr64
	leaq	88(%rdi), %rax
	movq	%rdx, 16(%rdi)
	movl	$0, 24(%rdi)
	movq	%rsi, (%rdi)
	movb	$0, 8(%rdi)
	movq	%rcx, 32(%rdi)
	movq	%rax, 40(%rdi)
	movq	$1, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movl	$0x3f800000, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 88(%rdi)
	ret
	.cfi_endproc
.LFE10590:
	.size	_ZN2v88internal21SourceRangeAstVisitorC2EmPNS0_10ExpressionEPNS0_14SourceRangeMapE, .-_ZN2v88internal21SourceRangeAstVisitorC2EmPNS0_10ExpressionEPNS0_14SourceRangeMapE
	.globl	_ZN2v88internal21SourceRangeAstVisitorC1EmPNS0_10ExpressionEPNS0_14SourceRangeMapE
	.set	_ZN2v88internal21SourceRangeAstVisitorC1EmPNS0_10ExpressionEPNS0_14SourceRangeMapE,_ZN2v88internal21SourceRangeAstVisitorC2EmPNS0_10ExpressionEPNS0_14SourceRangeMapE
	.section	.text._ZN2v88internal21SourceRangeAstVisitor32MaybeRemoveLastContinuationRangeEPNS0_8ZoneListIPNS0_9StatementEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SourceRangeAstVisitor32MaybeRemoveLastContinuationRangeEPNS0_8ZoneListIPNS0_9StatementEEE
	.type	_ZN2v88internal21SourceRangeAstVisitor32MaybeRemoveLastContinuationRangeEPNS0_8ZoneListIPNS0_9StatementEEE, @function
_ZN2v88internal21SourceRangeAstVisitor32MaybeRemoveLastContinuationRangeEPNS0_8ZoneListIPNS0_9StatementEEE:
.LFB10596:
	.cfi_startproc
	endbr64
	movl	12(%rsi), %eax
	testl	%eax, %eax
	je	.L30
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$1, %eax
	cltq
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rsi), %rdx
	movq	(%rdx,%rax,8), %rdx
	movq	32(%rdi), %rax
	movzbl	4(%rdx), %ecx
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	andl	$63, %ecx
	cmpb	$9, %cl
	je	.L33
.L6:
	testq	%rax, %rax
	je	.L3
	movq	%rsi, %rcx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L14
.L9:
	cmpq	%rdx, 32(%rax)
	jnb	.L34
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L9
.L14:
	cmpq	%rcx, %rsi
	je	.L3
	cmpq	%rdx, 32(%rcx)
	ja	.L3
	movq	40(%rcx), %r12
.L13:
	testq	%r12, %r12
	je	.L3
	movq	(%r12), %rax
	movl	$2, %esi
	movq	%r12, %rdi
	call	*24(%rax)
	testb	%al, %al
	je	.L3
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	8(%rdx), %rcx
	movzbl	4(%rcx), %edi
	andl	$63, %edi
	cmpb	$52, %dil
	jne	.L6
	testq	%rax, %rax
	je	.L3
	movq	%rsi, %rdx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%rax, %rdx
	movq	16(%rax), %rax
.L12:
	testq	%rax, %rax
	je	.L10
.L8:
	cmpq	%rcx, 32(%rax)
	jnb	.L35
	movq	24(%rax), %rax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L10:
	cmpq	%rdx, %rsi
	je	.L3
	cmpq	%rcx, 32(%rdx)
	jbe	.L36
.L3:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore 6
	.cfi_restore 12
	ret
.L36:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	40(%rdx), %r12
	jmp	.L13
	.cfi_endproc
.LFE10596:
	.size	_ZN2v88internal21SourceRangeAstVisitor32MaybeRemoveLastContinuationRangeEPNS0_8ZoneListIPNS0_9StatementEEE, .-_ZN2v88internal21SourceRangeAstVisitor32MaybeRemoveLastContinuationRangeEPNS0_8ZoneListIPNS0_9StatementEEE
	.section	.text._ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm
	.type	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm, @function
_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm:
.LFB12632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L38
	movq	(%rbx), %r8
.L39:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L48
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L49:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L62
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L63
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L41:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L43
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L45:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L46:
	testq	%rsi, %rsi
	je	.L43
.L44:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L45
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L51
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L44
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L47
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L47:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L48:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L50
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L50:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%rdx, %rdi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L41
.L63:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE12632:
	.size	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm, .-_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm
	.section	.text._ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRiEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRiEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRiEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRiEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRiEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB12358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$16, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movslq	(%rbx), %r10
	movq	8(%r12), %rcx
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movl	%r10d, 8(%rax)
	movq	%r10, %rax
	divq	%rcx
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L65
	movq	(%rax), %rbx
	movq	%r10, %rsi
	movl	8(%rbx), %r8d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L78:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L65
	movslq	8(%rbx), %rax
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L65
.L67:
	cmpl	%r8d, %esi
	jne	.L78
	call	_ZdlPv@PLT
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	%rdi, %rcx
	movq	%r10, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	%r9, %rsi
	call	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12358:
	.size	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRiEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRiEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	.type	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE, @function
_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE:
.LFB10595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L82
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L83
.L82:
	cmpq	%rsi, 32(%rax)
	jnb	.L108
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L82
.L83:
	cmpq	%rdx, %rcx
	je	.L81
	cmpq	%rsi, 32(%rdx)
	jbe	.L109
.L81:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	40(%rdx), %r12
	testq	%r12, %r12
	je	.L81
	movq	(%r12), %rax
	movl	$2, %esi
	movq	%r12, %rdi
	call	*24(%rax)
	testb	%al, %al
	je	.L81
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	$2, %esi
	call	*16(%rax)
	movq	48(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, -32(%rbp)
	movq	%rax, %r8
	cltq
	divq	%rdi
	movq	40(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L87
	movq	(%rax), %rcx
	movl	8(%rcx), %esi
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L87
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L87
.L89:
	cmpl	%esi, %r8d
	jne	.L111
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*32(%rax)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	-32(%rbp), %rsi
	leaq	40(%rbx), %rdi
	call	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRiEEESt4pairINS1_14_Node_iteratorIiLb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	jmp	.L81
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10595:
	.size	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE, .-_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	.section	.rodata._ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	.type	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE, @function
_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE:
.LFB11989:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	je	.L338
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L118(%rip), %rbx
	subq	$8, %rsp
.L114:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L339
	movzbl	4(%r13), %eax
	andl	$63, %eax
	cmpb	$57, %al
	ja	.L112
	movzbl	%al, %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE,"aG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE,comdat
	.align 4
	.align 4
.L118:
	.long	.L121-.L118
	.long	.L173-.L118
	.long	.L172-.L118
	.long	.L171-.L118
	.long	.L170-.L118
	.long	.L168-.L118
	.long	.L168-.L118
	.long	.L167-.L118
	.long	.L166-.L118
	.long	.L160-.L118
	.long	.L112-.L118
	.long	.L164-.L118
	.long	.L163-.L118
	.long	.L121-.L118
	.long	.L121-.L118
	.long	.L160-.L118
	.long	.L159-.L118
	.long	.L158-.L118
	.long	.L157-.L118
	.long	.L121-.L118
	.long	.L155-.L118
	.long	.L121-.L118
	.long	.L153-.L118
	.long	.L152-.L118
	.long	.L151-.L118
	.long	.L150-.L118
	.long	.L149-.L118
	.long	.L148-.L118
	.long	.L147-.L118
	.long	.L146-.L118
	.long	.L145-.L118
	.long	.L144-.L118
	.long	.L143-.L118
	.long	.L142-.L118
	.long	.L141-.L118
	.long	.L140-.L118
	.long	.L139-.L118
	.long	.L121-.L118
	.long	.L137-.L118
	.long	.L121-.L118
	.long	.L135-.L118
	.long	.L121-.L118
	.long	.L121-.L118
	.long	.L132-.L118
	.long	.L131-.L118
	.long	.L130-.L118
	.long	.L129-.L118
	.long	.L128-.L118
	.long	.L127-.L118
	.long	.L126-.L118
	.long	.L125-.L118
	.long	.L121-.L118
	.long	.L123-.L118
	.long	.L122-.L118
	.long	.L121-.L118
	.long	.L120-.L118
	.long	.L119-.L118
	.long	.L117-.L118
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE,comdat
.L144:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	cmpq	$0, 32(%r13)
	je	.L340
	addl	$1, 24(%r12)
	movq	32(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
.L189:
	movl	%eax, 24(%r12)
	movq	40(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	cmpq	$0, 56(%r13)
	je	.L193
	movl	%eax, 24(%r12)
	movq	56(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	cmpb	$0, 8(%r12)
	jne	.L112
.L193:
	cmpq	$0, 64(%r13)
	je	.L192
	addl	$1, 24(%r12)
	movq	64(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	cmpb	$0, 8(%r12)
	jne	.L112
.L192:
	movq	48(%r13), %r13
	movl	12(%r13), %eax
	testl	%eax, %eax
	jle	.L112
	movl	24(%r12), %eax
	movzbl	8(%r12), %ecx
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L201:
	movq	0(%r13), %rdx
	leal	1(%rax), %esi
	movq	(%rdx,%rbx,8), %r14
	movq	(%r14), %rdx
	andq	$-4, %rdx
	movl	4(%rdx), %edx
	movl	%esi, 24(%r12)
	andl	$63, %edx
	cmpb	$41, %dl
	je	.L195
	testb	%cl, %cl
	je	.L341
	movl	%eax, 24(%r12)
.L112:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	%r13, %rsi
.L334:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	movb	$1, 8(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	movq	32(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L112
	movq	40(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L112
	.p2align 4,,10
	.p2align 3
.L325:
	movq	24(%r13), %r13
.L175:
	cmpb	$0, 8(%r12)
	jne	.L112
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	movq	8(%r13), %r13
	jmp	.L175
.L143:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L142:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L141:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	movl	%eax, 24(%r12)
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L140:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L139:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor10VisitBlockEPNS0_5BlockE
	cmpb	$0, 8(%r12)
	jne	.L112
	movq	16(%r13), %rsi
	jmp	.L334
.L125:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	movq	16(%r13), %rax
	movq	(%rax), %rbx
	movslq	12(%rax), %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%r13, %rbx
	je	.L112
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	movq	(%rbx), %r14
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	je	.L204
	movl	%eax, 24(%r12)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L203:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L112
	movq	(%rbx), %r14
	movl	%eax, 24(%r12)
.L204:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L203
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L112
.L137:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21SourceRangeAstVisitor20VisitFunctionLiteralEPNS0_15FunctionLiteralE
.L126:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L135:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L159:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
.L332:
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L112
	jmp	.L325
.L158:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	movq	8(%r13), %rsi
.L333:
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	je	.L325
	jmp	.L112
.L157:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L112
.L327:
	movq	16(%r13), %r13
	jmp	.L175
.L170:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	movq	32(%r13), %rsi
	testq	%rsi, %rsi
	je	.L179
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L112
.L179:
	movq	40(%r13), %rsi
	testq	%rsi, %rsi
	je	.L178
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L112
.L178:
	movq	48(%r13), %rsi
	testq	%rsi, %rsi
	jne	.L333
	jmp	.L325
.L129:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L167:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21SourceRangeAstVisitor10VisitBlockEPNS0_5BlockE
.L120:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L153:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitObjectLiteralEPNS0_13ObjectLiteralE
.L166:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21SourceRangeAstVisitor20VisitSwitchStatementEPNS0_15SwitchStatementE
.L119:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L164:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	jmp	.L327
.L163:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L112
	jmp	.L332
.L173:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	jmp	.L325
.L172:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L112
	movq	32(%r13), %r13
	jmp	.L175
.L171:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	movq	32(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L112
	jmp	.L325
.L127:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L128:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	movl	%eax, 24(%r12)
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L132:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L131:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L130:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L152:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	movl	36(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L112
	movl	24(%r12), %eax
	movzbl	8(%r12), %edx
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L184:
	movq	24(%r13), %rcx
	movq	(%rcx,%rbx,8), %r14
	leal	1(%rax), %ecx
	movl	%ecx, 24(%r12)
	testb	%dl, %dl
	je	.L342
	movl	%eax, 24(%r12)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L150:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L149:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L148:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitNaryOperationEPNS0_13NaryOperationE
.L147:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE9VisitCallEPNS0_4CallE
.L146:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE12VisitCallNewEPNS0_7CallNewE
.L145:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	movl	36(%r13), %edx
	testl	%edx, %edx
	jle	.L112
	movl	24(%r12), %eax
	movzbl	8(%r12), %edx
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L187:
	movq	24(%r13), %rcx
	movq	(%rcx,%rbx,8), %r14
	leal	1(%rax), %ecx
	movl	%ecx, 24(%r12)
	testb	%dl, %dl
	je	.L343
	movl	%eax, 24(%r12)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L155:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
.L123:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L112
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L112
.L117:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L343:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L344
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	movzbl	8(%r12), %edx
	subl	$1, %eax
	movl	%eax, 24(%r12)
	testb	%dl, %dl
	jne	.L112
	addq	$1, %rbx
	cmpl	%ebx, 36(%r13)
	jg	.L187
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L342:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L345
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	movzbl	8(%r12), %edx
	subl	$1, %eax
	movl	%eax, 24(%r12)
	testb	%dl, %dl
	jne	.L112
	addq	$1, %rbx
	cmpl	%ebx, 36(%r13)
	jg	.L184
	jmp	.L112
.L195:
	testb	%cl, %cl
	je	.L205
	movl	%eax, 24(%r12)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L341:
	movq	(%r14), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L346
	movq	%r15, %rsi
	movq	%r12, %rdi
	andq	$-4, %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L112
	movl	%eax, 24(%r12)
.L205:
	movq	8(%r14), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L347
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	movzbl	8(%r12), %ecx
	subl	$1, %eax
	movl	%eax, 24(%r12)
	testb	%cl, %cl
	jne	.L112
	addq	$1, %rbx
	cmpl	%ebx, 12(%r13)
	jg	.L201
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L344:
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L112
.L345:
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L112
.L347:
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L112
.L346:
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L112
.L340:
	movl	24(%r12), %eax
	addl	$1, %eax
	jmp	.L189
	.cfi_endproc
.LFE11989:
	.size	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE, .-_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.type	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE, @function
_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE:
.LFB12350:
	.cfi_startproc
	endbr64
	movzbl	4(%rsi), %eax
	andl	$63, %eax
	cmpb	$57, %al
	ja	.L538
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L351(%rip), %rdx
	movzbl	%al, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"aG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.align 4
	.align 4
.L351:
	.long	.L541-.L351
	.long	.L397-.L351
	.long	.L396-.L351
	.long	.L395-.L351
	.long	.L394-.L351
	.long	.L392-.L351
	.long	.L392-.L351
	.long	.L391-.L351
	.long	.L390-.L351
	.long	.L386-.L351
	.long	.L348-.L351
	.long	.L388-.L351
	.long	.L387-.L351
	.long	.L541-.L351
	.long	.L541-.L351
	.long	.L386-.L351
	.long	.L385-.L351
	.long	.L384-.L351
	.long	.L383-.L351
	.long	.L541-.L351
	.long	.L382-.L351
	.long	.L541-.L351
	.long	.L381-.L351
	.long	.L380-.L351
	.long	.L379-.L351
	.long	.L378-.L351
	.long	.L377-.L351
	.long	.L376-.L351
	.long	.L375-.L351
	.long	.L374-.L351
	.long	.L373-.L351
	.long	.L372-.L351
	.long	.L371-.L351
	.long	.L370-.L351
	.long	.L369-.L351
	.long	.L368-.L351
	.long	.L367-.L351
	.long	.L541-.L351
	.long	.L366-.L351
	.long	.L541-.L351
	.long	.L365-.L351
	.long	.L541-.L351
	.long	.L541-.L351
	.long	.L364-.L351
	.long	.L363-.L351
	.long	.L362-.L351
	.long	.L361-.L351
	.long	.L360-.L351
	.long	.L359-.L351
	.long	.L358-.L351
	.long	.L357-.L351
	.long	.L541-.L351
	.long	.L356-.L351
	.long	.L355-.L351
	.long	.L541-.L351
	.long	.L353-.L351
	.long	.L352-.L351
	.long	.L350-.L351
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.p2align 4,,10
	.p2align 3
.L373:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	movl	36(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L348
	movl	24(%r12), %eax
	movzbl	8(%r12), %edx
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L411:
	movq	24(%r13), %rcx
	movq	(%rcx,%rbx,8), %r14
	leal	1(%rax), %ecx
	movl	%ecx, 24(%r12)
	testb	%dl, %dl
	je	.L556
	movl	%eax, 24(%r12)
.L348:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor10VisitBlockEPNS0_5BlockE
	cmpb	$0, 8(%r12)
	jne	.L348
	movq	16(%r13), %rsi
.L541:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	movq	32(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L348
	movq	40(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L348
	.p2align 4,,10
	.p2align 3
.L544:
	movq	24(%r13), %rsi
.L542:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	movq	8(%r13), %rsi
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L390:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21SourceRangeAstVisitor20VisitSwitchStatementEPNS0_15SwitchStatementE
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore_state
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L352:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L395:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	movq	32(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L348
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L396:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L348
	movq	32(%r13), %rsi
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L397:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L387:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L348
.L551:
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L348
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L388:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
.L545:
	movq	16(%r13), %rsi
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L382:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore_state
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L348
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L384:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	movq	8(%r13), %rsi
.L552:
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	je	.L544
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L357:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	movq	16(%r13), %rax
	movq	(%rax), %r13
	movslq	12(%rax), %rax
	leaq	0(%r13,%rax,8), %rbx
	cmpq	%rbx, %r13
	je	.L348
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	movq	0(%r13), %r14
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	je	.L423
	movl	%eax, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L422:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L348
	movq	0(%r13), %r14
	movl	%eax, 24(%r12)
.L423:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L422
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L394:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	movq	32(%r13), %rsi
	testq	%rsi, %rsi
	je	.L403
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L348
.L403:
	movq	40(%r13), %rsi
	testq	%rsi, %rsi
	je	.L402
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L348
.L402:
	movq	48(%r13), %rsi
	testq	%rsi, %rsi
	jne	.L552
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L374:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE12VisitCallNewEPNS0_7CallNewE
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE9VisitCallEPNS0_4CallE
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitNaryOperationEPNS0_13NaryOperationE
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L359:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L360:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
	movl	%eax, 24(%r12)
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L379:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L380:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	movl	36(%r13), %esi
	testl	%esi, %esi
	jle	.L348
	movl	24(%r12), %eax
	movzbl	8(%r12), %edx
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L408:
	movq	24(%r13), %rcx
	movq	(%rcx,%rbx,8), %r14
	leal	1(%rax), %ecx
	movl	%ecx, 24(%r12)
	testb	%dl, %dl
	je	.L557
	movl	%eax, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L381:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitObjectLiteralEPNS0_13ObjectLiteralE
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	cmpq	$0, 32(%r13)
	je	.L558
	addl	$1, 24(%r12)
	movq	32(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
.L413:
	movl	%eax, 24(%r12)
	movq	40(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %edx
	cmpb	$0, 8(%r12)
	leal	-1(%rdx), %eax
	movl	%eax, 24(%r12)
	jne	.L348
	cmpq	$0, 56(%r13)
	je	.L414
	movl	%edx, 24(%r12)
	movq	56(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %edx
	cmpb	$0, 8(%r12)
	leal	-1(%rdx), %eax
	movl	%eax, 24(%r12)
	jne	.L348
.L414:
	cmpq	$0, 64(%r13)
	je	.L415
	movl	%edx, 24(%r12)
	movq	64(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	jne	.L348
.L415:
	movq	48(%r13), %r13
	movl	12(%r13), %edx
	testl	%edx, %edx
	jle	.L348
	xorl	%ebx, %ebx
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L417:
	movq	%r15, %rsi
	movq	%r12, %rdi
	andq	$-4, %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
	movl	%eax, 24(%r12)
.L424:
	movq	8(%r14), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L559
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	jne	.L348
	addq	$1, %rbx
	cmpl	%ebx, 12(%r13)
	jle	.L348
.L420:
	movq	0(%r13), %rdx
	addl	$1, %eax
	movq	(%rdx,%rbx,8), %r14
	movq	(%r14), %rdx
	andq	$-4, %rdx
	movl	4(%rdx), %edx
	movl	%eax, 24(%r12)
	andl	$63, %edx
	cmpb	$41, %dl
	je	.L424
	movq	(%r14), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L417
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L371:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L385:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L366:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21SourceRangeAstVisitor20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L369:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
	movl	%eax, 24(%r12)
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L370:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L361:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L362:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L363:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L348
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L364:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L365:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L353:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L355:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L356:
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L391:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21SourceRangeAstVisitor10VisitBlockEPNS0_5BlockE
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L348
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L348
.L350:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L557:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L560
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	movzbl	8(%r12), %edx
	subl	$1, %eax
	movl	%eax, 24(%r12)
	testb	%dl, %dl
	jne	.L348
	addq	$1, %rbx
	cmpl	%ebx, 36(%r13)
	jg	.L408
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L556:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L561
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	movzbl	8(%r12), %edx
	subl	$1, %eax
	movl	%eax, 24(%r12)
	testb	%dl, %dl
	jne	.L348
	addq	$1, %rbx
	cmpl	%ebx, 36(%r13)
	jg	.L411
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L560:
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L561:
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L348
.L559:
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L348
.L538:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L558:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	24(%r12), %eax
	addl	$1, %eax
	jmp	.L413
	.cfi_endproc
.LFE12350:
	.size	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE, .-_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitObjectLiteralEPNS0_13ObjectLiteralE,"axG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitObjectLiteralEPNS0_13ObjectLiteralE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitObjectLiteralEPNS0_13ObjectLiteralE
	.type	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitObjectLiteralEPNS0_13ObjectLiteralE, @function
_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitObjectLiteralEPNS0_13ObjectLiteralE:
.LFB12572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L562
	movl	36(%r13), %eax
	testl	%eax, %eax
	jle	.L562
	movl	24(%rbx), %eax
	movzbl	8(%rbx), %ecx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L569:
	movq	24(%r13), %rdx
	movq	(%rdx,%r12,8), %r14
	leal	1(%rax), %edx
	movl	%edx, 24(%rbx)
	testb	%cl, %cl
	je	.L572
.L565:
	movl	%eax, 24(%rbx)
.L562:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	movq	(%r14), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L573
	movq	%r15, %rsi
	movq	%rbx, %rdi
	andq	$-4, %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	jne	.L565
	movq	8(%r14), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L574
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	movzbl	8(%rbx), %ecx
	subl	$1, %eax
	movl	%eax, 24(%rbx)
	testb	%cl, %cl
	jne	.L562
	addq	$1, %r12
	cmpl	%r12d, 36(%r13)
	jg	.L569
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L574:
	.cfi_restore_state
	subl	$1, 24(%rbx)
	movb	$1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	.cfi_restore_state
	movl	24(%rbx), %eax
	movb	$1, 8(%rbx)
	subl	$1, %eax
	jmp	.L565
	.cfi_endproc
.LFE12572:
	.size	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitObjectLiteralEPNS0_13ObjectLiteralE, .-_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitObjectLiteralEPNS0_13ObjectLiteralE
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE,"axG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.type	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE, @function
_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE:
.LFB12570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L575
	movq	8(%r12), %r14
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L575
	movzbl	8(%rbx), %edx
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L581:
	movq	(%r14), %rax
	movq	(%rax,%r13,8), %r15
	movq	(%r15), %r12
	andq	$-4, %r12
	movzbl	4(%r12), %eax
	andl	$63, %eax
	cmpb	$41, %al
	je	.L577
	testb	%dl, %dl
	je	.L591
.L575:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore_state
	testb	%dl, %dl
	jne	.L575
	movq	8(%r15), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L580
	.p2align 4,,10
	.p2align 3
.L592:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	8(%rbx), %edx
	testb	%dl, %dl
	jne	.L575
	addq	$1, %r13
	cmpl	%r13d, 12(%r14)
	jg	.L581
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L580
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L575
	movq	8(%r15), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jnb	.L592
.L580:
	movb	$1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12570:
	.size	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE, .-_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.section	.text._ZN2v88internal21SourceRangeAstVisitor20VisitFunctionLiteralEPNS0_15FunctionLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SourceRangeAstVisitor20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	.type	_ZN2v88internal21SourceRangeAstVisitor20VisitFunctionLiteralEPNS0_15FunctionLiteralE, @function
_ZN2v88internal21SourceRangeAstVisitor20VisitFunctionLiteralEPNS0_15FunctionLiteralE:
.LFB10594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L595
	movl	24(%r12), %eax
	movq	40(%rbx), %rcx
	leal	1(%rax), %edx
	leaq	88(%rcx), %r13
	movl	%edx, 24(%r12)
	movq	96(%rcx), %r14
	movzbl	8(%r12), %ecx
	cmpq	%r14, %r13
	je	.L596
	testb	%cl, %cl
	je	.L602
.L605:
	movl	%eax, 24(%r12)
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L613
	movq	0(%r13), %r13
	addq	$16, %r13
	cmpq	%r13, %r14
	je	.L614
.L602:
	movq	0(%r13), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L598
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
.L595:
	addq	$8, %rsp
	leaq	48(%rbx), %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21SourceRangeAstVisitor32MaybeRemoveLastContinuationRangeEPNS0_8ZoneListIPNS0_9StatementEEE
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	movl	24(%r12), %edx
	leal	-1(%rdx), %eax
	movl	%eax, 24(%r12)
.L603:
	movq	40(%rbx), %rcx
	cmpb	$0, 131(%rcx)
	js	.L595
	movl	%edx, 24(%r12)
	movl	60(%rbx), %edx
	testl	%edx, %edx
	jle	.L605
	movq	48(%rbx), %rax
	xorl	%r13d, %r13d
	movq	(%rax), %r14
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L606:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L613
	addq	$1, %r13
	cmpl	%r13d, 60(%rbx)
	jle	.L613
	movq	48(%rbx), %rax
	movq	(%rax,%r13,8), %r14
.L610:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L606
	movb	$1, 8(%r12)
.L613:
	movl	24(%r12), %eax
	subl	$1, %eax
	jmp	.L605
.L596:
	movl	%eax, 24(%r12)
	testb	%cl, %cl
	jne	.L595
	jmp	.L603
	.cfi_endproc
.LFE10594:
	.size	_ZN2v88internal21SourceRangeAstVisitor20VisitFunctionLiteralEPNS0_15FunctionLiteralE, .-_ZN2v88internal21SourceRangeAstVisitor20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	.section	.rodata._ZN2v88internal21SourceRangeAstVisitor10VisitBlockEPNS0_5BlockE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"enclosingSourceRanges->HasRange(SourceRangeKind::kContinuation)"
	.section	.rodata._ZN2v88internal21SourceRangeAstVisitor10VisitBlockEPNS0_5BlockE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal21SourceRangeAstVisitor10VisitBlockEPNS0_5BlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SourceRangeAstVisitor10VisitBlockEPNS0_5BlockE
	.type	_ZN2v88internal21SourceRangeAstVisitor10VisitBlockEPNS0_5BlockE, @function
_ZN2v88internal21SourceRangeAstVisitor10VisitBlockEPNS0_5BlockE:
.LFB10592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	je	.L616
	cmpq	$0, 24(%r13)
	je	.L625
	movl	24(%r12), %eax
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	movq	24(%r13), %rdx
	movq	96(%rdx), %r14
	leaq	88(%rdx), %rbx
	movzbl	8(%r12), %edx
	cmpq	%r14, %rbx
	je	.L619
	testb	%dl, %dl
	je	.L624
.L623:
	movl	%eax, 24(%r12)
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L621:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L648
	movq	(%rbx), %rbx
	addq	$16, %rbx
	cmpq	%rbx, %r14
	je	.L649
.L624:
	movq	(%rbx), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L621
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
.L616:
	movq	32(%r12), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L628
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L650:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L629
.L628:
	cmpq	%r13, 32(%rax)
	jnb	.L650
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L628
.L629:
	cmpq	%rdx, %rcx
	je	.L615
	cmpq	%r13, 32(%rdx)
	ja	.L615
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L615
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*24(%rax)
	testb	%al, %al
	je	.L651
	addq	$8, %rsp
	leaq	8(%r13), %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21SourceRangeAstVisitor32MaybeRemoveLastContinuationRangeEPNS0_8ZoneListIPNS0_9StatementEEE
	.p2align 4,,10
	.p2align 3
.L615:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L619:
	.cfi_restore_state
	movl	%eax, 24(%r12)
	testb	%dl, %dl
	jne	.L616
	.p2align 4,,10
	.p2align 3
.L625:
	movl	20(%r13), %eax
	testl	%eax, %eax
	jle	.L616
	cmpb	$0, 8(%r12)
	jne	.L616
	xorl	%ebx, %ebx
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L626:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L616
	addq	$1, %rbx
	cmpl	%ebx, 20(%r13)
	jle	.L616
.L627:
	movq	8(%r13), %rax
	movq	(%rax,%rbx,8), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L626
	movb	$1, 8(%r12)
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L649:
	subl	$1, 24(%r12)
	jmp	.L625
.L651:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L648:
	movl	24(%r12), %eax
	subl	$1, %eax
	jmp	.L623
	.cfi_endproc
.LFE10592:
	.size	_ZN2v88internal21SourceRangeAstVisitor10VisitBlockEPNS0_5BlockE, .-_ZN2v88internal21SourceRangeAstVisitor10VisitBlockEPNS0_5BlockE
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE9VisitCallEPNS0_4CallE,"axG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE9VisitCallEPNS0_4CallE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE9VisitCallEPNS0_4CallE
	.type	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE9VisitCallEPNS0_4CallE, @function
_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE9VisitCallEPNS0_4CallE:
.LFB12578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	jne	.L662
.L652:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L662:
	.cfi_restore_state
	movl	24(%rbx), %eax
	cmpb	$0, 8(%rbx)
	leal	1(%rax), %edx
	movl	%edx, 24(%rbx)
	je	.L663
	movl	%eax, 24(%rbx)
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L663:
	movq	8(%r13), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L661
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	jne	.L652
	movl	28(%r13), %edx
	testl	%edx, %edx
	jle	.L652
	xorl	%r12d, %r12d
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L657:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	jne	.L652
	addq	$1, %r12
	cmpl	%r12d, 28(%r13)
	jle	.L652
.L658:
	movq	16(%r13), %rdx
	addl	$1, %eax
	movq	(%rdx,%r12,8), %r14
	movl	%eax, 24(%rbx)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jnb	.L657
.L661:
	subl	$1, 24(%rbx)
	movb	$1, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12578:
	.size	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE9VisitCallEPNS0_4CallE, .-_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE9VisitCallEPNS0_4CallE
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE12VisitCallNewEPNS0_7CallNewE,"axG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE12VisitCallNewEPNS0_7CallNewE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE12VisitCallNewEPNS0_7CallNewE
	.type	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE12VisitCallNewEPNS0_7CallNewE, @function
_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE12VisitCallNewEPNS0_7CallNewE:
.LFB12579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	jne	.L674
.L664:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	movl	24(%rbx), %eax
	cmpb	$0, 8(%rbx)
	leal	1(%rax), %edx
	movl	%edx, 24(%rbx)
	je	.L675
	movl	%eax, 24(%rbx)
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L675:
	movq	8(%r13), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L673
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	jne	.L664
	movl	28(%r13), %edx
	testl	%edx, %edx
	jle	.L664
	xorl	%r12d, %r12d
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L669:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	jne	.L664
	addq	$1, %r12
	cmpl	%r12d, 28(%r13)
	jle	.L664
.L670:
	movq	16(%r13), %rdx
	addl	$1, %eax
	movq	(%rdx,%r12,8), %r14
	movl	%eax, 24(%rbx)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jnb	.L669
.L673:
	subl	$1, 24(%rbx)
	movb	$1, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12579:
	.size	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE12VisitCallNewEPNS0_7CallNewE, .-_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE12VisitCallNewEPNS0_7CallNewE
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitNaryOperationEPNS0_13NaryOperationE,"axG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitNaryOperationEPNS0_13NaryOperationE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitNaryOperationEPNS0_13NaryOperationE
	.type	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitNaryOperationEPNS0_13NaryOperationE, @function
_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitNaryOperationEPNS0_13NaryOperationE:
.LFB12577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	jne	.L685
.L676:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L685:
	.cfi_restore_state
	movl	24(%rbx), %eax
	cmpb	$0, 8(%rbx)
	leal	1(%rax), %edx
	movl	%edx, 24(%rbx)
	je	.L686
	movl	%eax, 24(%rbx)
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L686:
	movq	8(%r12), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L684
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	cmpb	$0, 8(%rbx)
	leal	-1(%rax), %edx
	movl	%edx, 24(%rbx)
	jne	.L676
	movq	24(%r12), %rcx
	cmpq	%rcx, 32(%r12)
	je	.L676
	movl	%eax, 24(%rbx)
	xorl	%r13d, %r13d
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L681:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%rbx), %edx
	cmpb	$0, 8(%rbx)
	leal	-1(%rdx), %eax
	movl	%eax, 24(%rbx)
	jne	.L676
	movq	32(%r12), %rax
	subq	24(%r12), %rax
	addq	$1, %r13
	sarq	$4, %rax
	cmpq	%r13, %rax
	jbe	.L676
	movl	%edx, 24(%rbx)
.L682:
	movq	%r13, %rax
	salq	$4, %rax
	addq	24(%r12), %rax
	movq	(%rax), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jnb	.L681
.L684:
	subl	$1, 24(%rbx)
	movb	$1, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12577:
	.size	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitNaryOperationEPNS0_13NaryOperationE, .-_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE18VisitNaryOperationEPNS0_13NaryOperationE
	.section	.text._ZN2v88internal21SourceRangeAstVisitor20VisitSwitchStatementEPNS0_15SwitchStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SourceRangeAstVisitor20VisitSwitchStatementEPNS0_15SwitchStatementE
	.type	_ZN2v88internal21SourceRangeAstVisitor20VisitSwitchStatementEPNS0_15SwitchStatementE, @function
_ZN2v88internal21SourceRangeAstVisitor20VisitSwitchStatementEPNS0_15SwitchStatementE:
.LFB10593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal21SourceRangeAstVisitor9VisitNodeEPNS0_7AstNodeE
	testb	%al, %al
	jne	.L712
	.p2align 4,,10
	.p2align 3
.L688:
	movslq	36(%r13), %rax
	movq	24(%r13), %r12
.L692:
	leaq	(%r12,%rax,8), %r13
	cmpq	%r12, %r13
	je	.L687
	.p2align 4,,10
	.p2align 3
.L704:
	movq	(%r12), %rax
	movq	%rbx, %rdi
	addq	$8, %r12
	leaq	8(%rax), %rsi
	call	_ZN2v88internal21SourceRangeAstVisitor32MaybeRemoveLastContinuationRangeEPNS0_8ZoneListIPNS0_9StatementEEE
	cmpq	%r12, %r13
	jne	.L704
.L687:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	cmpb	$0, 8(%rbx)
	jne	.L688
	movq	16(%r13), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L710
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L688
	movl	36(%r13), %edx
	movq	24(%r13), %rcx
	movslq	%edx, %rax
	movq	%rcx, %r12
	testl	%edx, %edx
	jle	.L692
	movq	$0, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	(%rcx,%rax,8), %r15
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L713
	.p2align 4,,10
	.p2align 3
.L693:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L714
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L688
	movl	20(%r15), %eax
	testl	%eax, %eax
	jle	.L709
.L694:
	xorl	%r12d, %r12d
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L698:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_21SourceRangeAstVisitorEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L688
	addq	$1, %r12
	cmpl	%r12d, 20(%r15)
	jle	.L709
.L700:
	movq	8(%r15), %rax
	movq	(%rax,%r12,8), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jnb	.L698
.L710:
	movb	$1, 8(%rbx)
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L709:
	movl	36(%r13), %edx
	movq	24(%r13), %rcx
.L695:
	addq	$1, -56(%rbp)
	movq	-56(%rbp), %rdi
	movslq	%edx, %rax
	movq	%rcx, %r12
	cmpl	%edi, %edx
	jle	.L692
	movq	-56(%rbp), %rax
	movq	(%rcx,%rax,8), %r15
	movq	(%r15), %r12
	testq	%r12, %r12
	jne	.L693
.L713:
	movl	20(%r15), %esi
	testl	%esi, %esi
	jg	.L694
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L714:
	movb	$1, 8(%rbx)
	movslq	36(%r13), %rax
	movq	24(%r13), %r12
	jmp	.L692
	.cfi_endproc
.LFE10593:
	.size	_ZN2v88internal21SourceRangeAstVisitor20VisitSwitchStatementEPNS0_15SwitchStatementE, .-_ZN2v88internal21SourceRangeAstVisitor20VisitSwitchStatementEPNS0_15SwitchStatementE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21SourceRangeAstVisitorC2EmPNS0_10ExpressionEPNS0_14SourceRangeMapE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21SourceRangeAstVisitorC2EmPNS0_10ExpressionEPNS0_14SourceRangeMapE, @function
_GLOBAL__sub_I__ZN2v88internal21SourceRangeAstVisitorC2EmPNS0_10ExpressionEPNS0_14SourceRangeMapE:
.LFB13161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13161:
	.size	_GLOBAL__sub_I__ZN2v88internal21SourceRangeAstVisitorC2EmPNS0_10ExpressionEPNS0_14SourceRangeMapE, .-_GLOBAL__sub_I__ZN2v88internal21SourceRangeAstVisitorC2EmPNS0_10ExpressionEPNS0_14SourceRangeMapE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21SourceRangeAstVisitorC2EmPNS0_10ExpressionEPNS0_14SourceRangeMapE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
