	.file	"wasm-serialization.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1554:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1554:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB2342:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2342:
	.size	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB23168:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE23168:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_T0_SC_T1_T2_,"ax",@progbits
	.p2align 4
	.type	_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_T0_SC_T1_T2_, @function
_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_T0_SC_T1_T2_:
.LFB24361:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rax, %r12
	andl	$1, %r13d
	shrq	$63, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	addq	%rax, %r12
	sarq	%r12
	cmpq	%r12, %rsi
	jge	.L6
	movq	%rsi, %r11
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L19:
	subq	$1, %rax
	leaq	(%rdi,%rax,4), %r10
	movl	(%r10), %r9d
	movl	%r9d, (%rdi,%r11,4)
	cmpq	%r12, %rax
	jge	.L8
.L9:
	movq	%rax, %r11
.L10:
	leaq	1(%r11), %r9
	leaq	(%r9,%r9), %rax
	salq	$3, %r9
	leaq	(%rdi,%r9), %r10
	movl	-4(%rdi,%r9), %r14d
	movl	(%r10), %ebx
	movl	%ebx, %r9d
	movq	(%r8,%r9,8), %r9
	cmpq	%r9, (%r8,%r14,8)
	ja	.L19
	movl	%ebx, (%rdi,%r11,4)
	cmpq	%r12, %rax
	jl	.L9
.L8:
	testq	%r13, %r13
	je	.L14
.L11:
	leaq	-1(%rax), %rdx
	movq	%rdx, %r9
	shrq	$63, %r9
	addq	%rdx, %r9
	sarq	%r9
	cmpq	%rsi, %rax
	jle	.L12
	movl	%ecx, %ebx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%edx, (%r10)
	leaq	-1(%r9), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, %rdx
	movq	%r9, %rax
	cmpq	%r9, %rsi
	jge	.L20
	movq	%rdx, %r9
.L13:
	leaq	(%rdi,%r9,4), %r11
	leaq	(%rdi,%rax,4), %r10
	movq	(%r8,%rbx,8), %r14
	movl	(%r11), %edx
	movl	%edx, %eax
	cmpq	%r14, (%r8,%rax,8)
	jb	.L21
.L12:
	movl	%ecx, (%r10)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	leaq	(%rdi,%rsi,4), %r10
	testq	%r13, %r13
	jne	.L12
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	-2(%rdx), %r9
	movq	%r9, %rdx
	shrq	$63, %rdx
	addq	%r9, %rdx
	sarq	%rdx
	cmpq	%rax, %rdx
	jne	.L11
	leaq	2(%rax,%rax), %rax
	movl	-4(%rdi,%rax,4), %edx
	subq	$1, %rax
	movl	%edx, (%r10)
	leaq	(%rdi,%rax,4), %r10
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%r11, %r10
	movl	%ecx, (%r10)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24361:
	.size	_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_T0_SC_T1_T2_, .-_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_T0_SC_T1_T2_
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB24418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE24418:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_SB_T0_T1_,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_SB_T0_T1_, @function
_ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_SB_T0_T1_:
.LFB23740:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$64, %rax
	jle	.L52
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	testq	%rdx, %rdx
	je	.L57
	leaq	4(%rdi), %rdx
	movq	%rdx, -56(%rbp)
.L28:
	sarq	$3, %rax
	subq	$1, %r14
	leaq	(%r12,%rax,4), %r10
	movl	4(%r12), %eax
	movl	(%r10), %ecx
	movq	(%rbx,%rax,8), %r8
	movq	%rax, %r13
	movq	(%rbx,%rcx,8), %rdi
	movq	%rcx, %r11
	movl	-4(%rsi), %ecx
	movq	(%rbx,%rcx,8), %r9
	movq	%rcx, %r15
	movl	(%r12), %ecx
	cmpq	%rdi, %r8
	jnb	.L33
	cmpq	%r9, %rdi
	jb	.L38
	cmpq	%r9, %r8
	jb	.L55
.L56:
	movl	%r13d, (%r12)
	movl	%ecx, 4(%r12)
	movl	-4(%rsi), %ecx
.L35:
	movl	%ecx, %edi
	movq	-56(%rbp), %r13
	movq	(%rbx,%rdi,8), %r9
	movq	(%rbx,%rax,8), %rdi
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L39:
	movl	0(%r13), %r10d
	movq	%r13, %r15
	movq	%r10, %r8
	cmpq	%rdi, (%rbx,%r10,8)
	jb	.L40
	subq	$4, %rax
	cmpq	%rdi, %r9
	jbe	.L41
	.p2align 4,,10
	.p2align 3
.L42:
	movl	-4(%rax), %r9d
	subq	$4, %rax
	movq	%r9, %rcx
	cmpq	%rdi, (%rbx,%r9,8)
	ja	.L42
.L41:
	cmpq	%rax, %r13
	jnb	.L58
	movl	%ecx, 0(%r13)
	movl	-4(%rax), %edi
	movl	%r8d, (%rax)
	movq	%rdi, %rcx
	movq	(%rbx,%rdi,8), %r9
	movl	(%r12), %edi
	movq	(%rbx,%rdi,8), %rdi
.L40:
	addq	$4, %r13
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L33:
	cmpq	%r9, %r8
	jb	.L56
	cmpq	%r9, %rdi
	jnb	.L38
.L55:
	movl	%r15d, (%r12)
	movl	%ecx, -4(%rsi)
	movl	(%r12), %eax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_SB_T0_T1_
	movq	%r13, %rax
	subq	%r12, %rax
	cmpq	$64, %rax
	jle	.L24
	testq	%r14, %r14
	je	.L26
	movq	%r13, %rsi
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L38:
	movl	%r11d, (%r12)
	movl	%ecx, (%r10)
	movl	-4(%rsi), %ecx
	movl	(%r12), %eax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%rsi, %r15
.L26:
	sarq	$2, %rax
	movq	%rbx, %r8
	movq	%r12, %rdi
	leaq	-2(%rax), %rsi
	movq	%rax, %rdx
	movq	%rax, %r13
	sarq	%rsi
	movl	(%r12,%rsi,4), %ecx
	call	_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_T0_SC_T1_T2_
.L29:
	subq	$1, %rsi
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	(%r12,%rsi,4), %ecx
	call	_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_T0_SC_T1_T2_
	testq	%rsi, %rsi
	jne	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	subq	$4, %r15
	movl	(%r12), %eax
	movl	(%r15), %ecx
	xorl	%esi, %esi
	movq	%r15, %r13
	movq	%rbx, %r8
	movq	%r12, %rdi
	subq	%r12, %r13
	movl	%eax, (%r15)
	movq	%r13, %rdx
	sarq	$2, %rdx
	call	_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_T0_SC_T1_T2_
	cmpq	$4, %r13
	jg	.L30
.L24:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE23740:
	.size	_ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_SB_T0_T1_, .-_ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_SB_T0_T1_
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB24583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24583:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB24419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24419:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB24584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE24584:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_16Writer5WriteIjEEvRKT_.part.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"wrote: "
.LC1:
	.string	" sized: "
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_16Writer5WriteIjEEvRKT_.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_16Writer5WriteIjEEvRKT_.part.0, @function
_ZN2v88internal4wasm12_GLOBAL__N_16Writer5WriteIjEEvRKT_.part.0:
.LFB24578:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-304(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -40
	leaq	-384(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	subq	$352, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movw	%ax, -80(%rbp)
	movq	%rbx, -304(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r12, %rdi
	movl	$7, %edx
	movq	%rax, -384(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -304(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%r14), %esi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r14
	testq	%r14, %r14
	je	.L72
	cmpb	$0, 56(%r14)
	je	.L67
	movsbl	67(%r14), %esi
.L68:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -304(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$352, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L68
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L68
.L73:
	call	__stack_chk_fail@PLT
.L72:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE24578:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_16Writer5WriteIjEEvRKT_.part.0, .-_ZN2v88internal4wasm12_GLOBAL__N_16Writer5WriteIjEEvRKT_.part.0
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_112WriteVersionEPNS2_6WriterE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_112WriteVersionEPNS2_6WriterE, @function
_ZN2v88internal4wasm12_GLOBAL__N_112WriteVersionEPNS2_6WriterE:
.LFB19632:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movl	$-1059191884, (%rax)
	addq	$4, 16(%rdi)
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	jne	.L82
.L75:
	movl	_ZN2v88internal7Version6major_E(%rip), %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movl	_ZN2v88internal7Version6minor_E(%rip), %edi
	movq	%rax, %r12
	call	_ZN2v84base10hash_valueEj@PLT
	movl	_ZN2v88internal7Version6build_E(%rip), %edi
	movq	%rax, %r13
	call	_ZN2v84base10hash_valueEj@PLT
	movl	_ZN2v88internal7Version6patch_E(%rip), %edi
	movq	%rax, %r14
	call	_ZN2v84base10hash_valueEj@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	16(%rbx), %rdx
	movl	%eax, -44(%rbp)
	movl	%eax, (%rdx)
	addq	$4, 16(%rbx)
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	jne	.L83
.L76:
	cmpb	$0, _ZN2v88internal11CpuFeatures12initialized_E(%rip)
	jne	.L77
	xorl	%edi, %edi
	movb	$1, _ZN2v88internal11CpuFeatures12initialized_E(%rip)
	call	_ZN2v88internal11CpuFeatures9ProbeImplEb@PLT
.L77:
	movl	_ZN2v88internal11CpuFeatures10supported_E(%rip), %eax
	movq	16(%rbx), %rdx
	movl	%eax, -44(%rbp)
	movl	%eax, (%rdx)
	addq	$4, 16(%rbx)
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	jne	.L84
.L78:
	call	_ZN2v88internal8FlagList4HashEv@PLT
	movq	16(%rbx), %rdx
	movl	%eax, -44(%rbp)
	movl	%eax, (%rdx)
	addq	$4, 16(%rbx)
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	jne	.L85
.L74:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	leaq	-44(%rbp), %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_16Writer5WriteIjEEvRKT_.part.0
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L82:
	leaq	_ZN2v88internal14SerializedData12kMagicNumberE(%rip), %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_16Writer5WriteIjEEvRKT_.part.0
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	-44(%rbp), %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_16Writer5WriteIjEEvRKT_.part.0
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	-44(%rbp), %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_16Writer5WriteIjEEvRKT_.part.0
	jmp	.L78
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19632:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_112WriteVersionEPNS2_6WriterE, .-_ZN2v88internal4wasm12_GLOBAL__N_112WriteVersionEPNS2_6WriterE
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC2Ev.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC2Ev.constprop.0, @function
_ZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC2Ev.constprop.0:
.LFB24586:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	4656+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_ZN2v88internal17ExternalReference17abort_with_reasonEv@PLT
	movq	%rax, _ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference30address_of_double_abs_constantEv@PLT
	movq	%rax, 8+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference30address_of_double_neg_constantEv@PLT
	movq	%rax, 16+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference29address_of_float_abs_constantEv@PLT
	movq	%rax, 24+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference29address_of_float_neg_constantEv@PLT
	movq	%rax, 32+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference18address_of_min_intEv@PLT
	movq	%rax, 40+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference42address_of_mock_arraybuffer_allocator_flagEv@PLT
	movq	%rax, 48+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference19address_of_one_halfEv@PLT
	movq	%rax, 56+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference29address_of_runtime_stats_flagEv@PLT
	movq	%rax, 64+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference23address_of_the_hole_nanEv@PLT
	movq	%rax, 72+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference22address_of_uint32_biasEv@PLT
	movq	%rax, 80+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference27bytecode_size_table_addressEv@PLT
	movq	%rax, 88+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference17check_object_typeEv@PLT
	movq	%rax, 96+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference20compute_integer_hashEv@PLT
	movq	%rax, 104+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference30compute_output_frames_functionEv@PLT
	movq	%rax, 112+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference48copy_fast_number_jsarray_elements_to_typed_arrayEv@PLT
	movq	%rax, 120+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference31copy_typed_array_elements_sliceEv@PLT
	movq	%rax, 128+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference40copy_typed_array_elements_to_typed_arrayEv@PLT
	movq	%rax, 136+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference12cpu_featuresEv@PLT
	movq	%rax, 144+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference30delete_handle_scope_extensionsEv@PLT
	movq	%rax, 152+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference36ephemeron_key_write_barrier_functionEv@PLT
	movq	%rax, 160+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference25f64_acos_wrapper_functionEv@PLT
	movq	%rax, 168+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference25f64_asin_wrapper_functionEv@PLT
	movq	%rax, 176+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference24f64_mod_wrapper_functionEv@PLT
	movq	%rax, 184+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference23get_date_field_functionEv@PLT
	movq	%rax, 192+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference22get_or_create_hash_rawEv@PLT
	movq	%rax, 200+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference21ieee754_acos_functionEv@PLT
	movq	%rax, 208+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference22ieee754_acosh_functionEv@PLT
	movq	%rax, 216+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference21ieee754_asin_functionEv@PLT
	movq	%rax, 224+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference22ieee754_asinh_functionEv@PLT
	movq	%rax, 232+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference21ieee754_atan_functionEv@PLT
	movq	%rax, 240+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference22ieee754_atan2_functionEv@PLT
	movq	%rax, 248+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference22ieee754_atanh_functionEv@PLT
	movq	%rax, 256+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference21ieee754_cbrt_functionEv@PLT
	movq	%rax, 264+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference20ieee754_cos_functionEv@PLT
	movq	%rax, 272+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference21ieee754_cosh_functionEv@PLT
	movq	%rax, 280+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference20ieee754_exp_functionEv@PLT
	movq	%rax, 288+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference22ieee754_expm1_functionEv@PLT
	movq	%rax, 296+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference20ieee754_log_functionEv@PLT
	movq	%rax, 304+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference22ieee754_log10_functionEv@PLT
	movq	%rax, 312+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference22ieee754_log1p_functionEv@PLT
	movq	%rax, 320+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference21ieee754_log2_functionEv@PLT
	movq	%rax, 328+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference20ieee754_pow_functionEv@PLT
	movq	%rax, 336+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference20ieee754_sin_functionEv@PLT
	movq	%rax, 344+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference21ieee754_sinh_functionEv@PLT
	movq	%rax, 352+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference20ieee754_tan_functionEv@PLT
	movq	%rax, 360+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference21ieee754_tanh_functionEv@PLT
	movq	%rax, 368+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference41incremental_marking_record_write_functionEv@PLT
	movq	%rax, 376+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference36invalidate_prototype_chains_functionEv@PLT
	movq	%rax, 384+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference31invoke_accessor_getter_callbackEv@PLT
	movq	%rax, 392+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference24invoke_function_callbackEv@PLT
	movq	%rax, 400+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference46jsarray_array_join_concat_to_sequential_stringEv@PLT
	movq	%rax, 408+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference31jsreceiver_create_identity_hashEv@PLT
	movq	%rax, 416+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference20libc_memchr_functionEv@PLT
	movq	%rax, 424+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference20libc_memcpy_functionEv@PLT
	movq	%rax, 432+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference21libc_memmove_functionEv@PLT
	movq	%rax, 440+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference20libc_memset_functionEv@PLT
	movq	%rax, 448+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference25mod_two_doubles_operationEv@PLT
	movq	%rax, 456+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference54mutable_big_int_absolute_add_and_canonicalize_functionEv@PLT
	movq	%rax, 464+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference41mutable_big_int_absolute_compare_functionEv@PLT
	movq	%rax, 472+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference54mutable_big_int_absolute_sub_and_canonicalize_functionEv@PLT
	movq	%rax, 480+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference24new_deoptimizer_functionEv@PLT
	movq	%rax, 488+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference26orderedhashmap_gethash_rawEv@PLT
	movq	%rax, 496+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference15printf_functionEv@PLT
	movq	%rax, 504+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference18refill_math_randomEv@PLT
	movq	%rax, 512+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference25search_string_raw_one_oneEv@PLT
	movq	%rax, 520+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference25search_string_raw_one_twoEv@PLT
	movq	%rax, 528+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference25search_string_raw_two_oneEv@PLT
	movq	%rax, 536+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference25search_string_raw_two_twoEv@PLT
	movq	%rax, 544+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference34smi_lexicographic_compare_functionEv@PLT
	movq	%rax, 552+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference30store_buffer_overflow_functionEv@PLT
	movq	%rax, 560+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference31try_internalize_string_functionEv@PLT
	movq	%rax, 568+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference35wasm_call_trap_callback_for_testingEv@PLT
	movq	%rax, 576+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference13wasm_f32_ceilEv@PLT
	movq	%rax, 584+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference14wasm_f32_floorEv@PLT
	movq	%rax, 592+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference20wasm_f32_nearest_intEv@PLT
	movq	%rax, 600+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference14wasm_f32_truncEv@PLT
	movq	%rax, 608+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference13wasm_f64_ceilEv@PLT
	movq	%rax, 616+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference14wasm_f64_floorEv@PLT
	movq	%rax, 624+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference20wasm_f64_nearest_intEv@PLT
	movq	%rax, 632+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference14wasm_f64_truncEv@PLT
	movq	%rax, 640+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference21wasm_float32_to_int64Ev@PLT
	movq	%rax, 648+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference22wasm_float32_to_uint64Ev@PLT
	movq	%rax, 656+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference16wasm_float64_powEv@PLT
	movq	%rax, 664+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference21wasm_float64_to_int64Ev@PLT
	movq	%rax, 672+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference22wasm_float64_to_uint64Ev@PLT
	movq	%rax, 680+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference14wasm_int64_divEv@PLT
	movq	%rax, 688+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference14wasm_int64_modEv@PLT
	movq	%rax, 696+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference21wasm_int64_to_float32Ev@PLT
	movq	%rax, 704+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference21wasm_int64_to_float64Ev@PLT
	movq	%rax, 712+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference15wasm_uint64_divEv@PLT
	movq	%rax, 720+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference15wasm_uint64_modEv@PLT
	movq	%rax, 728+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference22wasm_uint64_to_float32Ev@PLT
	movq	%rax, 736+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference22wasm_uint64_to_float64Ev@PLT
	movq	%rax, 744+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference15wasm_word32_ctzEv@PLT
	movq	%rax, 752+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference18wasm_word32_popcntEv@PLT
	movq	%rax, 760+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference15wasm_word32_rolEv@PLT
	movq	%rax, 768+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference15wasm_word32_rorEv@PLT
	movq	%rax, 776+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference15wasm_word64_ctzEv@PLT
	movq	%rax, 784+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference18wasm_word64_popcntEv@PLT
	movq	%rax, 792+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference16wasm_memory_copyEv@PLT
	movq	%rax, 800+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference16wasm_memory_fillEv@PLT
	movq	%rax, 808+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference31call_enqueue_microtask_functionEv@PLT
	movq	%rax, 816+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference27call_enter_context_functionEv@PLT
	movq	%rax, 824+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference25atomic_pair_load_functionEv@PLT
	movq	%rax, 832+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference26atomic_pair_store_functionEv@PLT
	movq	%rax, 840+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference24atomic_pair_add_functionEv@PLT
	movq	%rax, 848+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference24atomic_pair_sub_functionEv@PLT
	movq	%rax, 856+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference24atomic_pair_and_functionEv@PLT
	movq	%rax, 864+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference23atomic_pair_or_functionEv@PLT
	movq	%rax, 872+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference24atomic_pair_xor_functionEv@PLT
	movq	%rax, 880+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference29atomic_pair_exchange_functionEv@PLT
	movq	%rax, 888+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference37atomic_pair_compare_exchange_functionEv@PLT
	movq	%rax, 896+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference30intl_convert_one_byte_to_lowerEv@PLT
	movq	%rax, 904+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference26intl_to_latin1_lower_tableEv@PLT
	xorl	%edi, %edi
	movq	%rax, 912+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$1, %edi
	movq	%rax, 920+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$2, %edi
	movq	%rax, 928+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$3, %edi
	movq	%rax, 936+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$4, %edi
	movq	%rax, 944+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$5, %edi
	movq	%rax, 952+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$6, %edi
	movq	%rax, 960+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$7, %edi
	movq	%rax, 968+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$8, %edi
	movq	%rax, 976+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$9, %edi
	movq	%rax, 984+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$10, %edi
	movq	%rax, 992+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$11, %edi
	movq	%rax, 1000+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$12, %edi
	movq	%rax, 1008+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$13, %edi
	movq	%rax, 1016+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$14, %edi
	movq	%rax, 1024+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$15, %edi
	movq	%rax, 1032+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$16, %edi
	movq	%rax, 1040+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$17, %edi
	movq	%rax, 1048+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$18, %edi
	movq	%rax, 1056+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$19, %edi
	movq	%rax, 1064+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$20, %edi
	movq	%rax, 1072+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$21, %edi
	movq	%rax, 1080+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$22, %edi
	movq	%rax, 1088+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$23, %edi
	movq	%rax, 1096+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$24, %edi
	movq	%rax, 1104+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$25, %edi
	movq	%rax, 1112+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$26, %edi
	movq	%rax, 1120+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$27, %edi
	movq	%rax, 1128+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$28, %edi
	movq	%rax, 1136+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$29, %edi
	movq	%rax, 1144+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$30, %edi
	movq	%rax, 1152+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$31, %edi
	movq	%rax, 1160+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$32, %edi
	movq	%rax, 1168+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$33, %edi
	movq	%rax, 1176+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$34, %edi
	movq	%rax, 1184+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$35, %edi
	movq	%rax, 1192+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$36, %edi
	movq	%rax, 1200+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$37, %edi
	movq	%rax, 1208+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$38, %edi
	movq	%rax, 1216+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$39, %edi
	movq	%rax, 1224+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$40, %edi
	movq	%rax, 1232+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$41, %edi
	movq	%rax, 1240+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$42, %edi
	movq	%rax, 1248+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$43, %edi
	movq	%rax, 1256+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$44, %edi
	movq	%rax, 1264+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$45, %edi
	movq	%rax, 1272+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$46, %edi
	movq	%rax, 1280+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$47, %edi
	movq	%rax, 1288+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$48, %edi
	movq	%rax, 1296+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$49, %edi
	movq	%rax, 1304+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$50, %edi
	movq	%rax, 1312+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$51, %edi
	movq	%rax, 1320+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$52, %edi
	movq	%rax, 1328+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$53, %edi
	movq	%rax, 1336+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$54, %edi
	movq	%rax, 1344+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$55, %edi
	movq	%rax, 1352+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$56, %edi
	movq	%rax, 1360+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$57, %edi
	movq	%rax, 1368+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$58, %edi
	movq	%rax, 1376+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$59, %edi
	movq	%rax, 1384+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$60, %edi
	movq	%rax, 1392+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$61, %edi
	movq	%rax, 1400+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$62, %edi
	movq	%rax, 1408+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$63, %edi
	movq	%rax, 1416+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$64, %edi
	movq	%rax, 1424+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$65, %edi
	movq	%rax, 1432+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$66, %edi
	movq	%rax, 1440+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$67, %edi
	movq	%rax, 1448+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$68, %edi
	movq	%rax, 1456+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$69, %edi
	movq	%rax, 1464+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$70, %edi
	movq	%rax, 1472+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$71, %edi
	movq	%rax, 1480+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$72, %edi
	movq	%rax, 1488+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$73, %edi
	movq	%rax, 1496+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$74, %edi
	movq	%rax, 1504+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$75, %edi
	movq	%rax, 1512+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$76, %edi
	movq	%rax, 1520+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$77, %edi
	movq	%rax, 1528+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$78, %edi
	movq	%rax, 1536+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$79, %edi
	movq	%rax, 1544+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$80, %edi
	movq	%rax, 1552+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$81, %edi
	movq	%rax, 1560+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$82, %edi
	movq	%rax, 1568+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$83, %edi
	movq	%rax, 1576+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$84, %edi
	movq	%rax, 1584+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$85, %edi
	movq	%rax, 1592+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$86, %edi
	movq	%rax, 1600+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$87, %edi
	movq	%rax, 1608+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$88, %edi
	movq	%rax, 1616+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$89, %edi
	movq	%rax, 1624+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$90, %edi
	movq	%rax, 1632+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$91, %edi
	movq	%rax, 1640+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$92, %edi
	movq	%rax, 1648+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$93, %edi
	movq	%rax, 1656+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$94, %edi
	movq	%rax, 1664+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$95, %edi
	movq	%rax, 1672+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$96, %edi
	movq	%rax, 1680+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$97, %edi
	movq	%rax, 1688+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$98, %edi
	movq	%rax, 1696+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$99, %edi
	movq	%rax, 1704+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$100, %edi
	movq	%rax, 1712+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$101, %edi
	movq	%rax, 1720+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$102, %edi
	movq	%rax, 1728+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$103, %edi
	movq	%rax, 1736+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$104, %edi
	movq	%rax, 1744+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$105, %edi
	movq	%rax, 1752+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$106, %edi
	movq	%rax, 1760+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$107, %edi
	movq	%rax, 1768+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$108, %edi
	movq	%rax, 1776+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$109, %edi
	movq	%rax, 1784+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$110, %edi
	movq	%rax, 1792+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$111, %edi
	movq	%rax, 1800+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$112, %edi
	movq	%rax, 1808+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$113, %edi
	movq	%rax, 1816+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$114, %edi
	movq	%rax, 1824+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$115, %edi
	movq	%rax, 1832+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$116, %edi
	movq	%rax, 1840+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$117, %edi
	movq	%rax, 1848+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$118, %edi
	movq	%rax, 1856+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$119, %edi
	movq	%rax, 1864+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$120, %edi
	movq	%rax, 1872+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$121, %edi
	movq	%rax, 1880+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$122, %edi
	movq	%rax, 1888+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$123, %edi
	movq	%rax, 1896+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$124, %edi
	movq	%rax, 1904+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$125, %edi
	movq	%rax, 1912+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$126, %edi
	movq	%rax, 1920+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$127, %edi
	movq	%rax, 1928+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$128, %edi
	movq	%rax, 1936+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$129, %edi
	movq	%rax, 1944+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$130, %edi
	movq	%rax, 1952+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$131, %edi
	movq	%rax, 1960+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$132, %edi
	movq	%rax, 1968+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$133, %edi
	movq	%rax, 1976+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$134, %edi
	movq	%rax, 1984+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$135, %edi
	movq	%rax, 1992+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$136, %edi
	movq	%rax, 2000+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$137, %edi
	movq	%rax, 2008+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$138, %edi
	movq	%rax, 2016+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$139, %edi
	movq	%rax, 2024+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$140, %edi
	movq	%rax, 2032+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$141, %edi
	movq	%rax, 2040+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$142, %edi
	movq	%rax, 2048+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$143, %edi
	movq	%rax, 2056+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$144, %edi
	movq	%rax, 2064+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$145, %edi
	movq	%rax, 2072+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$146, %edi
	movq	%rax, 2080+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$147, %edi
	movq	%rax, 2088+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$148, %edi
	movq	%rax, 2096+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$149, %edi
	movq	%rax, 2104+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$150, %edi
	movq	%rax, 2112+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$151, %edi
	movq	%rax, 2120+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$152, %edi
	movq	%rax, 2128+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$153, %edi
	movq	%rax, 2136+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$154, %edi
	movq	%rax, 2144+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$155, %edi
	movq	%rax, 2152+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$156, %edi
	movq	%rax, 2160+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$157, %edi
	movq	%rax, 2168+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$158, %edi
	movq	%rax, 2176+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$159, %edi
	movq	%rax, 2184+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$160, %edi
	movq	%rax, 2192+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$161, %edi
	movq	%rax, 2200+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$162, %edi
	movq	%rax, 2208+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$163, %edi
	movq	%rax, 2216+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$164, %edi
	movq	%rax, 2224+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$165, %edi
	movq	%rax, 2232+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$166, %edi
	movq	%rax, 2240+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$167, %edi
	movq	%rax, 2248+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$168, %edi
	movq	%rax, 2256+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$169, %edi
	movq	%rax, 2264+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$170, %edi
	movq	%rax, 2272+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$171, %edi
	movq	%rax, 2280+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$172, %edi
	movq	%rax, 2288+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$173, %edi
	movq	%rax, 2296+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$174, %edi
	movq	%rax, 2304+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$175, %edi
	movq	%rax, 2312+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$176, %edi
	movq	%rax, 2320+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$177, %edi
	movq	%rax, 2328+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$178, %edi
	movq	%rax, 2336+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$179, %edi
	movq	%rax, 2344+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$180, %edi
	movq	%rax, 2352+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$181, %edi
	movq	%rax, 2360+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$182, %edi
	movq	%rax, 2368+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$183, %edi
	movq	%rax, 2376+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$184, %edi
	movq	%rax, 2384+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$185, %edi
	movq	%rax, 2392+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$186, %edi
	movq	%rax, 2400+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$187, %edi
	movq	%rax, 2408+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$188, %edi
	movq	%rax, 2416+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$189, %edi
	movq	%rax, 2424+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$190, %edi
	movq	%rax, 2432+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$191, %edi
	movq	%rax, 2440+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$192, %edi
	movq	%rax, 2448+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$193, %edi
	movq	%rax, 2456+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$194, %edi
	movq	%rax, 2464+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$195, %edi
	movq	%rax, 2472+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$196, %edi
	movq	%rax, 2480+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$197, %edi
	movq	%rax, 2488+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$198, %edi
	movq	%rax, 2496+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$199, %edi
	movq	%rax, 2504+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$200, %edi
	movq	%rax, 2512+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$201, %edi
	movq	%rax, 2520+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$202, %edi
	movq	%rax, 2528+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$203, %edi
	movq	%rax, 2536+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$204, %edi
	movq	%rax, 2544+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$205, %edi
	movq	%rax, 2552+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$206, %edi
	movq	%rax, 2560+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$207, %edi
	movq	%rax, 2568+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$208, %edi
	movq	%rax, 2576+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$209, %edi
	movq	%rax, 2584+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$210, %edi
	movq	%rax, 2592+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$211, %edi
	movq	%rax, 2600+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$212, %edi
	movq	%rax, 2608+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$213, %edi
	movq	%rax, 2616+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$214, %edi
	movq	%rax, 2624+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$215, %edi
	movq	%rax, 2632+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$216, %edi
	movq	%rax, 2640+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$217, %edi
	movq	%rax, 2648+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$218, %edi
	movq	%rax, 2656+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$219, %edi
	movq	%rax, 2664+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$220, %edi
	movq	%rax, 2672+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$221, %edi
	movq	%rax, 2680+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$222, %edi
	movq	%rax, 2688+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$223, %edi
	movq	%rax, 2696+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$224, %edi
	movq	%rax, 2704+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$225, %edi
	movq	%rax, 2712+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$226, %edi
	movq	%rax, 2720+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$227, %edi
	movq	%rax, 2728+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$228, %edi
	movq	%rax, 2736+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$229, %edi
	movq	%rax, 2744+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$230, %edi
	movq	%rax, 2752+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$231, %edi
	movq	%rax, 2760+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$232, %edi
	movq	%rax, 2768+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$233, %edi
	movq	%rax, 2776+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$234, %edi
	movq	%rax, 2784+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$235, %edi
	movq	%rax, 2792+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$236, %edi
	movq	%rax, 2800+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$237, %edi
	movq	%rax, 2808+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$238, %edi
	movq	%rax, 2816+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$239, %edi
	movq	%rax, 2824+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$240, %edi
	movq	%rax, 2832+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$241, %edi
	movq	%rax, 2840+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$242, %edi
	movq	%rax, 2848+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$243, %edi
	movq	%rax, 2856+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$244, %edi
	movq	%rax, 2864+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$245, %edi
	movq	%rax, 2872+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$246, %edi
	movq	%rax, 2880+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$247, %edi
	movq	%rax, 2888+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$248, %edi
	movq	%rax, 2896+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$249, %edi
	movq	%rax, 2904+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$250, %edi
	movq	%rax, 2912+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$251, %edi
	movq	%rax, 2920+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$252, %edi
	movq	%rax, 2928+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$253, %edi
	movq	%rax, 2936+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$254, %edi
	movq	%rax, 2944+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$255, %edi
	movq	%rax, 2952+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$256, %edi
	movq	%rax, 2960+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$257, %edi
	movq	%rax, 2968+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$258, %edi
	movq	%rax, 2976+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$259, %edi
	movq	%rax, 2984+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$260, %edi
	movq	%rax, 2992+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$261, %edi
	movq	%rax, 3000+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$262, %edi
	movq	%rax, 3008+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$263, %edi
	movq	%rax, 3016+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$264, %edi
	movq	%rax, 3024+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$265, %edi
	movq	%rax, 3032+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$266, %edi
	movq	%rax, 3040+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$267, %edi
	movq	%rax, 3048+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$268, %edi
	movq	%rax, 3056+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$269, %edi
	movq	%rax, 3064+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$270, %edi
	movq	%rax, 3072+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$271, %edi
	movq	%rax, 3080+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$272, %edi
	movq	%rax, 3088+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$273, %edi
	movq	%rax, 3096+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$274, %edi
	movq	%rax, 3104+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$275, %edi
	movq	%rax, 3112+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$276, %edi
	movq	%rax, 3120+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$277, %edi
	movq	%rax, 3128+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$278, %edi
	movq	%rax, 3136+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$279, %edi
	movq	%rax, 3144+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$280, %edi
	movq	%rax, 3152+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$281, %edi
	movq	%rax, 3160+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$282, %edi
	movq	%rax, 3168+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$283, %edi
	movq	%rax, 3176+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$284, %edi
	movq	%rax, 3184+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$285, %edi
	movq	%rax, 3192+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$286, %edi
	movq	%rax, 3200+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$287, %edi
	movq	%rax, 3208+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$288, %edi
	movq	%rax, 3216+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$289, %edi
	movq	%rax, 3224+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$290, %edi
	movq	%rax, 3232+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$291, %edi
	movq	%rax, 3240+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$292, %edi
	movq	%rax, 3248+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$293, %edi
	movq	%rax, 3256+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$294, %edi
	movq	%rax, 3264+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$295, %edi
	movq	%rax, 3272+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$296, %edi
	movq	%rax, 3280+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$297, %edi
	movq	%rax, 3288+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$298, %edi
	movq	%rax, 3296+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$299, %edi
	movq	%rax, 3304+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$300, %edi
	movq	%rax, 3312+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$301, %edi
	movq	%rax, 3320+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$302, %edi
	movq	%rax, 3328+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$303, %edi
	movq	%rax, 3336+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$304, %edi
	movq	%rax, 3344+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$305, %edi
	movq	%rax, 3352+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$306, %edi
	movq	%rax, 3360+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$307, %edi
	movq	%rax, 3368+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$308, %edi
	movq	%rax, 3376+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$309, %edi
	movq	%rax, 3384+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$310, %edi
	movq	%rax, 3392+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$311, %edi
	movq	%rax, 3400+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$312, %edi
	movq	%rax, 3408+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$313, %edi
	movq	%rax, 3416+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$314, %edi
	movq	%rax, 3424+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$315, %edi
	movq	%rax, 3432+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$316, %edi
	movq	%rax, 3440+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$317, %edi
	movq	%rax, 3448+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$318, %edi
	movq	%rax, 3456+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$319, %edi
	movq	%rax, 3464+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$320, %edi
	movq	%rax, 3472+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$321, %edi
	movq	%rax, 3480+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$322, %edi
	movq	%rax, 3488+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$323, %edi
	movq	%rax, 3496+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$324, %edi
	movq	%rax, 3504+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$325, %edi
	movq	%rax, 3512+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$326, %edi
	movq	%rax, 3520+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$327, %edi
	movq	%rax, 3528+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$328, %edi
	movq	%rax, 3536+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$329, %edi
	movq	%rax, 3544+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$330, %edi
	movq	%rax, 3552+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$331, %edi
	movq	%rax, 3560+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$332, %edi
	movq	%rax, 3568+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$333, %edi
	movq	%rax, 3576+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$334, %edi
	movq	%rax, 3584+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$335, %edi
	movq	%rax, 3592+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$336, %edi
	movq	%rax, 3600+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$337, %edi
	movq	%rax, 3608+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$338, %edi
	movq	%rax, 3616+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$339, %edi
	movq	%rax, 3624+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$340, %edi
	movq	%rax, 3632+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$341, %edi
	movq	%rax, 3640+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$342, %edi
	movq	%rax, 3648+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$343, %edi
	movq	%rax, 3656+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$344, %edi
	movq	%rax, 3664+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$345, %edi
	movq	%rax, 3672+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$346, %edi
	movq	%rax, 3680+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$347, %edi
	movq	%rax, 3688+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$348, %edi
	movq	%rax, 3696+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$349, %edi
	movq	%rax, 3704+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$350, %edi
	movq	%rax, 3712+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$351, %edi
	movq	%rax, 3720+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$352, %edi
	movq	%rax, 3728+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$353, %edi
	movq	%rax, 3736+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$354, %edi
	movq	%rax, 3744+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$355, %edi
	movq	%rax, 3752+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$356, %edi
	movq	%rax, 3760+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$357, %edi
	movq	%rax, 3768+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$358, %edi
	movq	%rax, 3776+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$359, %edi
	movq	%rax, 3784+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$360, %edi
	movq	%rax, 3792+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$361, %edi
	movq	%rax, 3800+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$362, %edi
	movq	%rax, 3808+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$363, %edi
	movq	%rax, 3816+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$364, %edi
	movq	%rax, 3824+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$365, %edi
	movq	%rax, 3832+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$366, %edi
	movq	%rax, 3840+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$367, %edi
	movq	%rax, 3848+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$368, %edi
	movq	%rax, 3856+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$369, %edi
	movq	%rax, 3864+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$370, %edi
	movq	%rax, 3872+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$371, %edi
	movq	%rax, 3880+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$372, %edi
	movq	%rax, 3888+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$373, %edi
	movq	%rax, 3896+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$374, %edi
	movq	%rax, 3904+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$375, %edi
	movq	%rax, 3912+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$376, %edi
	movq	%rax, 3920+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$377, %edi
	movq	%rax, 3928+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$378, %edi
	movq	%rax, 3936+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$379, %edi
	movq	%rax, 3944+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$380, %edi
	movq	%rax, 3952+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$381, %edi
	movq	%rax, 3960+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$382, %edi
	movq	%rax, 3968+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$383, %edi
	movq	%rax, 3976+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$384, %edi
	movq	%rax, 3984+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$385, %edi
	movq	%rax, 3992+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$386, %edi
	movq	%rax, 4000+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$387, %edi
	movq	%rax, 4008+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$388, %edi
	movq	%rax, 4016+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$389, %edi
	movq	%rax, 4024+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$390, %edi
	movq	%rax, 4032+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$391, %edi
	movq	%rax, 4040+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$392, %edi
	movq	%rax, 4048+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$393, %edi
	movq	%rax, 4056+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$394, %edi
	movq	%rax, 4064+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$395, %edi
	movq	%rax, 4072+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$396, %edi
	movq	%rax, 4080+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$397, %edi
	movq	%rax, 4088+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$398, %edi
	movq	%rax, 4096+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$399, %edi
	movq	%rax, 4104+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$400, %edi
	movq	%rax, 4112+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$401, %edi
	movq	%rax, 4120+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$402, %edi
	movq	%rax, 4128+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$403, %edi
	movq	%rax, 4136+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$404, %edi
	movq	%rax, 4144+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$405, %edi
	movq	%rax, 4152+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$406, %edi
	movq	%rax, 4160+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$407, %edi
	movq	%rax, 4168+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$408, %edi
	movq	%rax, 4176+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$409, %edi
	movq	%rax, 4184+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$410, %edi
	movq	%rax, 4192+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$411, %edi
	movq	%rax, 4200+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$412, %edi
	movq	%rax, 4208+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$413, %edi
	movq	%rax, 4216+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$414, %edi
	movq	%rax, 4224+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$415, %edi
	movq	%rax, 4232+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$416, %edi
	movq	%rax, 4240+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$417, %edi
	movq	%rax, 4248+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$418, %edi
	movq	%rax, 4256+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$419, %edi
	movq	%rax, 4264+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$420, %edi
	movq	%rax, 4272+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$421, %edi
	movq	%rax, 4280+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$422, %edi
	movq	%rax, 4288+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$423, %edi
	movq	%rax, 4296+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$424, %edi
	movq	%rax, 4304+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$425, %edi
	movq	%rax, 4312+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$426, %edi
	movq	%rax, 4320+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$427, %edi
	movq	%rax, 4328+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$428, %edi
	movq	%rax, 4336+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$429, %edi
	movq	%rax, 4344+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$430, %edi
	movq	%rax, 4352+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$431, %edi
	movq	%rax, 4360+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$432, %edi
	movq	%rax, 4368+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$433, %edi
	movq	%rax, 4376+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$434, %edi
	movq	%rax, 4384+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$435, %edi
	movq	%rax, 4392+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$436, %edi
	movq	%rax, 4400+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$437, %edi
	movq	%rax, 4408+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$438, %edi
	movq	%rax, 4416+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$439, %edi
	movq	%rax, 4424+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$440, %edi
	movq	%rax, 4432+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$441, %edi
	movq	%rax, 4440+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$442, %edi
	movq	%rax, 4448+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$443, %edi
	movq	%rax, 4456+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$444, %edi
	movq	%rax, 4464+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$445, %edi
	movq	%rax, 4472+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$446, %edi
	movq	%rax, 4480+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$447, %edi
	movq	%rax, 4488+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$448, %edi
	movq	%rax, 4496+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$449, %edi
	movq	%rax, 4504+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$450, %edi
	movq	%rax, 4512+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$451, %edi
	movq	%rax, 4520+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$452, %edi
	movq	%rax, 4528+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$453, %edi
	movq	%rax, 4536+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$454, %edi
	movq	%rax, 4544+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$455, %edi
	movq	%rax, 4552+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$456, %edi
	movq	%rax, 4560+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$457, %edi
	movq	%rax, 4568+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$458, %edi
	movq	%rax, 4576+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$459, %edi
	movq	%rax, 4584+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$460, %edi
	movq	%rax, 4592+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$461, %edi
	movq	%rax, 4600+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$462, %edi
	movq	%rax, 4608+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$463, %edi
	movq	%rax, 4616+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$464, %edi
	movq	%rax, 4624+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$465, %edi
	movq	%rax, 4632+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	$466, %edi
	movq	%rax, 4640+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movdqa	.LC2(%rip), %xmm0
	movdqa	.LC3(%rip), %xmm2
	leaq	2320(%r13), %rdx
	movq	%rax, 4648+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L88:
	movdqa	%xmm0, %xmm1
	addq	$16, %rax
	paddd	%xmm2, %xmm0
	movaps	%xmm1, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L88
	leaq	_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %rcx
	leaq	4660+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %r12
	movl	$18, %edx
	movabsq	$2495375999556, %rax
	leaq	6984(%rcx), %rsi
	leaq	4656(%rcx), %rdi
	movq	%rax, 6976+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
	call	_ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_comp_iterIZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC4EvEUljjE_EEEvT_SB_T0_T1_
	leaq	-4660(%r12), %rbx
	leaq	4720(%rbx), %r14
.L93:
	movl	(%r12), %esi
	movl	4656+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %eax
	movq	(%rbx,%rsi,8), %rcx
	movq	%rsi, %r15
	cmpq	(%rbx,%rax,8), %rcx
	jnb	.L89
	movl	$4, %eax
	movq	%r12, %rdx
	leaq	4656+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %rsi
	subq	%r13, %rdx
	leaq	0(%r13,%rax), %rdi
	call	memmove@PLT
	movl	%r15d, 4656+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip)
.L90:
	addq	$4, %r12
	cmpq	%r14, %r12
	jne	.L93
	leaq	6984+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L96:
	movl	-4(%r12), %ecx
	movl	(%r12), %esi
	leaq	-4(%r12), %rax
	movq	%rcx, %rdx
	movq	%rsi, %r8
	movq	(%rbx,%rcx,8), %rcx
	cmpq	%rcx, (%rbx,%rsi,8)
	jnb	.L98
	.p2align 4,,10
	.p2align 3
.L95:
	movl	%edx, 4(%rax)
	movq	%rax, %rdi
	movl	-4(%rax), %ecx
	subq	$4, %rax
	movq	%rcx, %rdx
	movq	(%rbx,%rcx,8), %rcx
	cmpq	%rcx, (%rbx,%rsi,8)
	jb	.L95
	addq	$4, %r12
	movl	%r8d, (%rdi)
	cmpq	%r9, %r12
	jne	.L96
.L105:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	movl	-4(%r12), %edi
	leaq	-4(%r12), %rax
	movq	%rdi, %rdx
	cmpq	(%rbx,%rdi,8), %rcx
	jnb	.L97
	.p2align 4,,10
	.p2align 3
.L92:
	movl	%edx, 4(%rax)
	movq	%rax, %rdi
	movl	-4(%rax), %ecx
	subq	$4, %rax
	movq	%rcx, %rdx
	movq	(%rbx,%rcx,8), %rcx
	cmpq	%rcx, (%rbx,%rsi,8)
	jb	.L92
.L91:
	movl	%r15d, (%rdi)
	jmp	.L90
.L98:
	movq	%r12, %rdi
	addq	$4, %r12
	movl	%r8d, (%rdi)
	cmpq	%r9, %r12
	jne	.L96
	jmp	.L105
.L97:
	movq	%r12, %rdi
	jmp	.L91
	.cfi_endproc
.LFE24586:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC2Ev.constprop.0, .-_ZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC2Ev.constprop.0
	.section	.text._ZN2v88internal4wasm22NativeModuleSerializerC2EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm22NativeModuleSerializerC2EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE
	.type	_ZN2v88internal4wasm22NativeModuleSerializerC2EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE, @function
_ZN2v88internal4wasm22NativeModuleSerializerC2EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE:
.LFB19677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	40(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movq	%rsi, (%rdi)
	movq	%rdx, 8(%rdi)
	movq	%rcx, 16(%rdi)
	movb	$0, 24(%rdi)
	movl	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	%rax, -56(%rbp)
	movq	%rax, 56(%rdi)
	movq	%rax, 64(%rdi)
	movq	$0, 72(%rdi)
	.p2align 4,,10
	.p2align 3
.L118:
	movslq	%ebx, %rax
	movl	$48, %edi
	movq	248(%rsi,%rax,8), %r15
	call	_Znwm@PLT
	movq	48(%r14), %r12
	movq	%r15, 32(%rax)
	movq	%rax, %r13
	movl	%ebx, 40(%rax)
	testq	%r12, %r12
	jne	.L108
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L129:
	movq	16(%r12), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L109
.L130:
	movq	%rax, %r12
.L108:
	movq	32(%r12), %rsi
	cmpq	%rsi, %r15
	jb	.L129
	movq	24(%r12), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L130
.L109:
	testb	%cl, %cl
	jne	.L131
	cmpq	%rsi, %r15
	ja	.L119
.L114:
	movq	%r13, %rdi
	addl	$1, %ebx
	call	_ZdlPv@PLT
	cmpl	$31, %ebx
	je	.L106
.L133:
	movq	(%r14), %rsi
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L131:
	cmpq	%r12, 56(%r14)
	je	.L119
.L120:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	%r15, 32(%rax)
	jnb	.L114
	testq	%r12, %r12
	je	.L114
.L119:
	movl	$1, %edi
	cmpq	%r12, -56(%rbp)
	jne	.L132
.L115:
	movq	-56(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	addl	$1, %ebx
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 72(%r14)
	cmpl	$31, %ebx
	jne	.L133
.L106:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	xorl	%edi, %edi
	cmpq	32(%r12), %r15
	setb	%dil
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L128:
	movq	-56(%rbp), %rax
	cmpq	56(%r14), %rax
	je	.L122
	movq	%rax, %r12
	jmp	.L120
.L122:
	movq	-56(%rbp), %r12
	movl	$1, %edi
	jmp	.L115
	.cfi_endproc
.LFE19677:
	.size	_ZN2v88internal4wasm22NativeModuleSerializerC2EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE, .-_ZN2v88internal4wasm22NativeModuleSerializerC2EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE
	.globl	_ZN2v88internal4wasm22NativeModuleSerializerC1EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE
	.set	_ZN2v88internal4wasm22NativeModuleSerializerC1EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE,_ZN2v88internal4wasm22NativeModuleSerializerC2EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE
	.section	.text._ZNK2v88internal4wasm22NativeModuleSerializer11MeasureCodeEPKNS1_8WasmCodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm22NativeModuleSerializer11MeasureCodeEPKNS1_8WasmCodeE
	.type	_ZNK2v88internal4wasm22NativeModuleSerializer11MeasureCodeEPKNS1_8WasmCodeE, @function
_ZNK2v88internal4wasm22NativeModuleSerializer11MeasureCodeEPKNS1_8WasmCodeE:
.LFB19679:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L136
	movq	40(%rsi), %rdx
	movq	8(%rsi), %rax
	addq	24(%rsi), %rax
	leaq	93(%rax,%rdx), %rax
	movq	128(%rsi), %rdx
	leaq	(%rax,%rdx,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	movl	$8, %eax
	ret
	.cfi_endproc
.LFE19679:
	.size	_ZNK2v88internal4wasm22NativeModuleSerializer11MeasureCodeEPKNS1_8WasmCodeE, .-_ZNK2v88internal4wasm22NativeModuleSerializer11MeasureCodeEPKNS1_8WasmCodeE
	.section	.text._ZNK2v88internal4wasm22NativeModuleSerializer7MeasureEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm22NativeModuleSerializer7MeasureEv
	.type	_ZNK2v88internal4wasm22NativeModuleSerializer7MeasureEv, @function
_ZNK2v88internal4wasm22NativeModuleSerializer7MeasureEv:
.LFB19680:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	movl	$8, %r8d
	leaq	(%rdx,%rax,8), %rdi
	cmpq	%rdi, %rdx
	je	.L137
	.p2align 4,,10
	.p2align 3
.L142:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L139
	movq	40(%rax), %rsi
	movq	24(%rax), %rcx
	addq	$8, %rdx
	addq	8(%rax), %rcx
	movq	128(%rax), %rax
	leaq	93(%rcx,%rsi), %rcx
	leaq	(%rcx,%rax,8), %rax
	addq	%rax, %r8
	cmpq	%rdx, %rdi
	jne	.L142
.L137:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	addq	$8, %rdx
	addq	$8, %r8
	cmpq	%rdx, %rdi
	jne	.L142
	jmp	.L137
	.cfi_endproc
.LFE19680:
	.size	_ZNK2v88internal4wasm22NativeModuleSerializer7MeasureEv, .-_ZNK2v88internal4wasm22NativeModuleSerializer7MeasureEv
	.section	.text._ZN2v88internal4wasm14WasmSerializerC2EPNS1_12NativeModuleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm14WasmSerializerC2EPNS1_12NativeModuleE
	.type	_ZN2v88internal4wasm14WasmSerializerC2EPNS1_12NativeModuleE, @function
_ZN2v88internal4wasm14WasmSerializerC2EPNS1_12NativeModuleE:
.LFB19694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -8(%rdi)
	call	_ZNK2v88internal4wasm12NativeModule17SnapshotCodeTableEv@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L147
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L147:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19694:
	.size	_ZN2v88internal4wasm14WasmSerializerC2EPNS1_12NativeModuleE, .-_ZN2v88internal4wasm14WasmSerializerC2EPNS1_12NativeModuleE
	.globl	_ZN2v88internal4wasm14WasmSerializerC1EPNS1_12NativeModuleE
	.set	_ZN2v88internal4wasm14WasmSerializerC1EPNS1_12NativeModuleE,_ZN2v88internal4wasm14WasmSerializerC2EPNS1_12NativeModuleE
	.section	.text._ZN2v88internal4wasm24NativeModuleDeserializerC2EPNS1_12NativeModuleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm24NativeModuleDeserializerC2EPNS1_12NativeModuleE
	.type	_ZN2v88internal4wasm24NativeModuleDeserializerC2EPNS1_12NativeModuleE, @function
_ZN2v88internal4wasm24NativeModuleDeserializerC2EPNS1_12NativeModuleE:
.LFB19702:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movb	$0, 8(%rdi)
	ret
	.cfi_endproc
.LFE19702:
	.size	_ZN2v88internal4wasm24NativeModuleDeserializerC2EPNS1_12NativeModuleE, .-_ZN2v88internal4wasm24NativeModuleDeserializerC2EPNS1_12NativeModuleE
	.globl	_ZN2v88internal4wasm24NativeModuleDeserializerC1EPNS1_12NativeModuleE
	.set	_ZN2v88internal4wasm24NativeModuleDeserializerC1EPNS1_12NativeModuleE,_ZN2v88internal4wasm24NativeModuleDeserializerC2EPNS1_12NativeModuleE
	.section	.text._ZN2v88internal4wasm18IsSupportedVersionENS0_6VectorIKhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm18IsSupportedVersionENS0_6VectorIKhEE
	.type	_ZN2v88internal4wasm18IsSupportedVersionENS0_6VectorIKhEE, @function
_ZN2v88internal4wasm18IsSupportedVersionENS0_6VectorIKhEE:
.LFB19714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$15, %rsi
	jbe	.L149
	leaq	-48(%rbp), %rax
	leaq	-32(%rbp), %rdx
	movq	%rdi, %rbx
	movq	%rax, %xmm0
	movq	%rdx, %xmm1
	leaq	-80(%rbp), %rdi
	movq	%rax, -64(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_112WriteVersionEPNS2_6WriterE
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	xorq	-48(%rbp), %rax
	xorq	-40(%rbp), %rdx
	orq	%rax, %rdx
	sete	%al
.L149:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L156
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L156:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19714:
	.size	_ZN2v88internal4wasm18IsSupportedVersionENS0_6VectorIKhEE, .-_ZN2v88internal4wasm18IsSupportedVersionENS0_6VectorIKhEE
	.section	.rodata._ZN2v88internal4wasm23DeserializeNativeModuleEPNS0_7IsolateENS0_6VectorIKhEES6_.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(decode_result.value()) != nullptr"
	.section	.rodata._ZN2v88internal4wasm23DeserializeNativeModuleEPNS0_7IsolateENS0_6VectorIKhEES6_.str1.1,"aMS",@progbits,1
.LC5:
	.string	"Check failed: %s."
.LC6:
	.string	"read: "
.LC7:
	.string	"read vector of "
.LC8:
	.string	" elements"
.LC9:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4wasm23DeserializeNativeModuleEPNS0_7IsolateENS0_6VectorIKhEES6_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm23DeserializeNativeModuleEPNS0_7IsolateENS0_6VectorIKhEES6_
	.type	_ZN2v88internal4wasm23DeserializeNativeModuleEPNS0_7IsolateENS0_6VectorIKhEES6_, @function
_ZN2v88internal4wasm23DeserializeNativeModuleEPNS0_7IsolateENS0_6VectorIKhEES6_:
.LFB19715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$3176, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -7072(%rbp)
	movq	%rdi, %r14
	movq	%rcx, %r12
	movq	%r8, %rbx
	movq	%rdx, -7064(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %r13
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L158
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L159:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm20IsWasmCodegenAllowedEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE@PLT
	testb	%al, %al
	jne	.L161
.L164:
	xorl	%eax, %eax
.L162:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L432
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	cmpq	$15, -7064(%rbp)
	jbe	.L164
	leaq	-80(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movq	%rdx, %xmm1
	movq	%rax, -6880(%rbp)
	movq	%rax, %xmm0
	leaq	-6896(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %rdi
	movq	%rax, -7136(%rbp)
	movaps	%xmm0, -6896(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_112WriteVersionEPNS2_6WriterE
	movq	-7072(%rbp), %rcx
	movq	(%rcx), %rax
	movq	8(%rcx), %rdx
	xorq	-80(%rbp), %rax
	xorq	-72(%rbp), %rdx
	orq	%rax, %rdx
	jne	.L164
	movq	%r14, %rdi
	movq	%r12, -7024(%rbp)
	leaq	-7037(%rbp), %r13
	movq	%rbx, -7016(%rbp)
	call	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%rax, -7037(%rbp)
	movzbl	%dh, %eax
	movq	-7016(%rbp), %rcx
	leaq	-432(%rbp), %rdi
	movb	%al, -7028(%rbp)
	movq	%rdx, %rax
	shrq	$16, %rax
	movb	%dl, -7029(%rbp)
	movb	%al, -7027(%rbp)
	movq	%rdx, %rax
	shrq	$32, %rdx
	shrq	$24, %rax
	movb	%dl, -7025(%rbp)
	movq	-7024(%rbp), %rdx
	movb	%al, -7026(%rbp)
	movq	45752(%r14), %rax
	addq	%rdx, %rcx
	movq	%rdi, -7256(%rbp)
	addq	$408, %rax
	pushq	%rax
	pushq	40960(%r14)
	call	_ZN2v88internal4wasm16DecodeWasmModuleERKNS1_12WasmFeaturesEPKhS6_bNS1_12ModuleOriginEPNS0_8CountersEPNS0_19AccountingAllocatorE@PLT
	movl	-416(%rbp), %eax
	pxor	%xmm0, %xmm0
	popq	%r11
	leaq	-392(%rbp), %rcx
	movdqa	-432(%rbp), %xmm3
	movq	-432(%rbp), %rdx
	movaps	%xmm0, -432(%rbp)
	movl	%eax, -6816(%rbp)
	leaq	-6792(%rbp), %rax
	popq	%r15
	movq	%rax, -7264(%rbp)
	movq	%rax, -6808(%rbp)
	movq	-408(%rbp), %rax
	movaps	%xmm3, -6832(%rbp)
	cmpq	%rcx, %rax
	je	.L433
	movq	%rax, -6808(%rbp)
	movq	-392(%rbp), %rax
	movq	%rax, -6792(%rbp)
.L168:
	movq	-400(%rbp), %rax
	movq	%rax, -6800(%rbp)
	testq	%rax, %rax
	je	.L169
	xorl	%eax, %eax
.L170:
	movq	-6808(%rbp), %rdi
	cmpq	-7264(%rbp), %rdi
	je	.L287
	movq	%rax, -7064(%rbp)
	call	_ZdlPv@PLT
	movq	-7064(%rbp), %rax
.L287:
	movq	-6824(%rbp), %r12
	testq	%r12, %r12
	je	.L162
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L290
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r12)
.L291:
	cmpl	$1, %edx
	jne	.L162
	movq	(%r12), %rdx
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rcx
	movq	16(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L434
.L293:
	testq	%r15, %r15
	je	.L294
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r12)
.L295:
	cmpl	$1, %edx
	jne	.L162
	movq	(%r12), %rdx
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rsi
	movq	%rax, -7064(%rbp)
	movq	%r12, %rdi
	movq	24(%rdx), %rcx
	cmpq	%rsi, %rcx
	jne	.L296
	call	*8(%rdx)
	movq	-7064(%rbp), %rax
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L158:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L435
.L160:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r13, (%rsi)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L435:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L169:
	testq	%rdx, %rdx
	je	.L436
	leaq	-7024(%rbp), %rsi
	addq	$408, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm16CreateWasmScriptEPNS0_7IsolateERKNS1_15ModuleWireBytesERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movdqa	-6832(%rbp), %xmm1
	movq	45752(%r14), %rsi
	movq	%rax, -7272(%rbp)
	movq	-6824(%rbp), %rax
	movaps	%xmm1, -6896(%rbp)
	testq	%rax, %rax
	je	.L172
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L173
	lock addl	$1, 8(%rax)
.L172:
	movq	-7136(%rbp), %r8
	movq	%r13, %rcx
	leaq	-7008(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrIKNS1_10WasmModuleEE@PLT
	movq	-6888(%rbp), %r13
	testq	%r13, %r13
	je	.L175
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L176
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L177:
	cmpl	$1, %eax
	je	.L437
.L175:
	movq	-7008(%rbp), %r13
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	jne	.L438
.L183:
	movq	-7136(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rcx, -6896(%rbp)
	movq	%rbx, -6888(%rbp)
	call	_ZN2v88internal4wasm12NativeModule12SetWireBytesENS0_11OwnedVectorIKhEE@PLT
	movq	-6896(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L184
	call	_ZdaPv@PLT
.L184:
	movq	-7008(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm12NativeModule15SetRuntimeStubsEPNS0_7IsolateE@PLT
	movq	-7008(%rbp), %rax
	leaq	-7048(%rbp), %rdx
	movq	%r14, %rdi
	movq	$0, -7048(%rbp)
	movq	208(%rax), %rsi
	call	_ZN2v88internal4wasm23CompileJsToWasmWrappersEPNS0_7IsolateEPKNS1_10WasmModuleEPNS0_6HandleINS0_10FixedArrayEEE@PLT
	movdqa	-7008(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	-7048(%rbp), %rcx
	movq	-7272(%rbp), %rdx
	movq	-7136(%rbp), %rsi
	movq	%r14, %rdi
	movaps	%xmm0, -7008(%rbp)
	movaps	%xmm4, -6896(%rbp)
	call	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEE@PLT
	movq	-6888(%rbp), %r12
	movq	%rax, -7280(%rbp)
	testq	%r12, %r12
	je	.L186
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L187
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L188:
	cmpl	$1, %eax
	je	.L439
.L186:
	movq	-7280(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r13
	leaq	-6960(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -7288(%rbp)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	-7072(%rbp), %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movl	16(%rax), %eax
	movl	%eax, -7104(%rbp)
	jne	.L440
	movq	-7072(%rbp), %rax
	movl	20(%rax), %ebx
	addq	$24, %rax
	movq	%rax, -7088(%rbp)
.L198:
	movq	208(%r13), %rax
	movl	60(%rax), %ecx
	movl	68(%rax), %edx
	addl	%ecx, %edx
	movl	%ecx, -7080(%rbp)
	movl	%edx, -7244(%rbp)
	cmpl	%ebx, %ecx
	jne	.L277
	movl	-7104(%rbp), %ebx
	cmpl	%ebx, %edx
	je	.L202
.L277:
	xorl	%eax, %eax
.L204:
	movq	-7288(%rbp), %rdi
	movq	%rax, -7064(%rbp)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	-7000(%rbp), %r12
	movq	-7064(%rbp), %rax
	testq	%r12, %r12
	je	.L170
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L280
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r12)
.L281:
	cmpl	$1, %edx
	jne	.L170
	movq	(%r12), %rdx
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rcx
	movq	16(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L441
.L283:
	testq	%r15, %r15
	je	.L284
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r12)
.L285:
	cmpl	$1, %edx
	jne	.L170
	movq	(%r12), %rdx
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rsi
	movq	%rax, -7064(%rbp)
	movq	%r12, %rdi
	movq	24(%rdx), %rcx
	cmpq	%rsi, %rcx
	jne	.L286
	call	*8(%rdx)
	movq	-7064(%rbp), %rax
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L290:
	movl	8(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	jmp	.L291
.L202:
	cmpl	%edx, -7080(%rbp)
	jnb	.L205
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%r14, -7304(%rbp)
	movq	-7088(%rbp), %r14
	movq	%rdx, %xmm3
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm3
	movaps	%xmm3, -7104(%rbp)
	.p2align 4,,10
	.p2align 3
.L276:
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	(%r14), %r12
	jne	.L442
	testq	%r12, %r12
	je	.L312
	movq	8(%r14), %rax
	movq	%rax, -7240(%rbp)
.L211:
	movq	16(%r14), %rax
	movq	%rax, -7192(%rbp)
	leaq	24(%r14), %rax
.L311:
	movq	(%rax), %rbx
	addq	$8, %rax
	movq	%rbx, -7184(%rbp)
.L310:
	movq	(%rax), %rcx
	addq	$8, %rax
	movq	%rcx, -7224(%rbp)
.L309:
	movq	(%rax), %rbx
	addq	$8, %rax
	movq	%rbx, -7232(%rbp)
.L308:
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, -7196(%rbp)
.L307:
	movl	(%rax), %ebx
	addq	$4, %rax
	movl	%ebx, -7200(%rbp)
.L306:
	movq	(%rax), %rcx
	addq	$8, %rax
	movq	%rcx, -7168(%rbp)
.L305:
	movq	(%rax), %rbx
	addq	$8, %rax
	movq	%rbx, -7144(%rbp)
.L304:
	movq	(%rax), %rcx
	addq	$8, %rax
	movq	%rcx, -7152(%rbp)
.L303:
	movq	(%rax), %rbx
	addq	$8, %rax
	movq	%rbx, -7160(%rbp)
.L302:
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, -7208(%rbp)
.L301:
	movzbl	(%rax), %ebx
	addq	$1, %rax
	movq	%rax, -7176(%rbp)
	movb	%bl, -7216(%rbp)
.L247:
	movq	-7176(%rbp), %r14
	xorl	%r12d, %r12d
	addq	-7168(%rbp), %r14
	cmpq	$0, -7144(%rbp)
	jne	.L443
.L299:
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	jne	.L444
.L250:
	xorl	%r15d, %r15d
	cmpq	$0, -7152(%rbp)
	jne	.L445
.L297:
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	jne	.L446
.L253:
	movq	-7160(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L334
	movabsq	$1152921504606846975, %rax
	leaq	0(,%rbx,8), %rdi
	cmpq	%rax, %rbx
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movslq	%ebx, %r8
	salq	$3, %r8
	movq	%rax, %rcx
	je	.L256
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%r8, -7088(%rbp)
	call	memcpy@PLT
	movq	-7088(%rbp), %r8
	movq	%rax, %rcx
	addq	%r8, %r14
.L256:
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	jne	.L447
.L258:
	movq	-7152(%rbp), %rax
	movq	%rcx, -6896(%rbp)
	movq	%r13, %rdi
	movl	-7200(%rbp), %r9d
	movl	-7196(%rbp), %r8d
	movq	%r15, -6992(%rbp)
	movq	%rax, -6984(%rbp)
	movq	-7144(%rbp), %rax
	movq	-7176(%rbp), %rdx
	movq	-7168(%rbp), %rcx
	movq	%r12, -6976(%rbp)
	movq	%rax, -6968(%rbp)
	movq	-7160(%rbp), %rax
	movl	-7080(%rbp), %esi
	movq	%rax, -6888(%rbp)
	movzbl	-7216(%rbp), %eax
	pushq	%rax
	movl	-7208(%rbp), %eax
	pushq	%rax
	leaq	-6992(%rbp), %rax
	pushq	%rax
	leaq	-6976(%rbp), %rax
	pushq	%rax
	pushq	-7136(%rbp)
	pushq	-7232(%rbp)
	pushq	-7224(%rbp)
	pushq	-7240(%rbp)
	pushq	-7184(%rbp)
	pushq	-7192(%rbp)
	call	_ZN2v88internal4wasm12NativeModule19AddDeserializedCodeEjNS0_6VectorIKhEEjjmmmmmNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IS4_EESA_NS1_8WasmCode4KindENS1_13ExecutionTierE@PLT
	movq	-6896(%rbp), %rdi
	addq	$80, %rsp
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L261
	call	_ZdaPv@PLT
.L261:
	movq	-6976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L262
	call	_ZdaPv@PLT
.L262:
	movq	-6992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L263
	call	_ZdaPv@PLT
.L263:
	movq	%rbx, %rdi
	call	_ZNK2v88internal4wasm8WasmCode13constant_poolEv@PLT
	subq	$8, %rsp
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	24(%rbx), %r8
	movq	16(%rbx), %rcx
	pushq	$944
	movq	%rax, %r9
	movq	-7136(%rbp), %rdi
	call	_ZN2v88internal13RelocIteratorC1ENS0_6VectorIhEENS2_IKhEEmi@PLT
	cmpb	$0, -6840(%rbp)
	popq	%rax
	movq	-7136(%rbp), %r12
	popq	%rdx
	je	.L264
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L267:
	movq	-6880(%rbp), %rax
	movq	%r13, %rdi
	movl	(%rax), %esi
	call	_ZNK2v88internal4wasm12NativeModule24GetCallTargetForFunctionEj@PLT
	leaq	-6880(%rbp), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal9RelocInfo21set_wasm_call_addressEmNS0_15ICacheFlushModeE@PLT
.L271:
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -6840(%rbp)
	jne	.L275
.L264:
	movzbl	-6872(%rbp), %eax
	cmpb	$7, %al
	je	.L265
	jg	.L266
	cmpb	$4, %al
	je	.L267
	cmpb	$5, %al
	jne	.L269
	movq	-6880(%rbp), %rax
	leaq	-6880(%rbp), %rdi
	movl	$1, %edx
	movslq	(%rax), %rax
	movq	248(%r13,%rax,8), %rsi
	call	_ZN2v88internal9RelocInfo26set_wasm_stub_call_addressEmNS0_15ICacheFlushModeE@PLT
	jmp	.L271
.L436:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L266:
	subl	$8, %eax
	cmpb	$1, %al
	ja	.L269
	movq	-6880(%rbp), %rax
	movq	(%rbx), %rdx
	addq	%rdx, (%rax)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L265:
	movq	-6880(%rbp), %rax
	movl	(%rax), %r15d
	movzbl	_ZGVZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %eax
	testb	%al, %al
	je	.L448
.L273:
	leaq	_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	movq	-6880(%rbp), %rax
	movq	%rdx, (%rax)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L448:
	leaq	_ZGVZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L273
	call	_ZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC2Ev.constprop.0
	leaq	_ZGVZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L275:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal4wasm8WasmCode10MaybePrintEPKc@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal4wasm8WasmCode8ValidateEv@PLT
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
.L210:
	addl	$1, -7080(%rbp)
	movl	-7080(%rbp), %eax
	cmpl	-7244(%rbp), %eax
	jb	.L276
	movq	%r14, -7088(%rbp)
	movq	-7304(%rbp), %r14
.L205:
	movq	-7072(%rbp), %rdx
	addq	-7064(%rbp), %rdx
	cmpq	%rdx, -7088(%rbp)
	jne	.L277
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm12NativeModule12LogWasmCodesEPNS0_7IsolateE@PLT
	movq	41472(%r14), %rdi
	movq	-7272(%rbp), %rsi
	call	_ZN2v88internal5Debug14OnAfterCompileENS0_6HandleINS0_6ScriptEEE@PLT
	movq	-7280(%rbp), %rax
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L312:
	movl	-7080(%rbp), %esi
	movq	%r13, %rdi
	addq	$8, %r14
	call	_ZN2v88internal4wasm12NativeModule11UseLazyStubEj@PLT
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L442:
	leaq	-5984(%rbp), %rax
	leaq	-6064(%rbp), %r8
	movq	%rax, %rdi
	movq	%rax, -7144(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	movq	%r8, -7088(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%r8d, %r8d
	pxor	%xmm0, %xmm0
	movq	stdout(%rip), %rdx
	movw	%r8w, -5760(%rbp)
	movq	-7088(%rbp), %r8
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -5752(%rbp)
	movq	%r8, %rdi
	movups	%xmm0, -5736(%rbp)
	movq	%rbx, -5984(%rbp)
	movq	$0, -5768(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7088(%rbp), %r8
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$6, %edx
	leaq	40(%rax), %r15
	leaq	.LC6(%rip), %rsi
	movq	%rax, -7128(%rbp)
	movq	%r8, %rdi
	movq	%rax, -6064(%rbp)
	movq	%r15, -5984(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7088(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7088(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7088(%rbp), %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L207
	movsbl	67(%rdi), %esi
.L208:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-7104(%rbp), %xmm2
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-6000(%rbp), %rdi
	movq	%rax, -7120(%rbp)
	movq	%rax, -5984(%rbp)
	movaps	%xmm2, -6064(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7144(%rbp), %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%rbx, -5984(%rbp)
	movq	%rax, -7088(%rbp)
	movq	%rax, -6064(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	testq	%r12, %r12
	je	.L312
	movq	8(%r14), %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7240(%rbp)
	je	.L211
	leaq	-1408(%rbp), %rcx
	leaq	-1488(%rbp), %r12
	movq	%rcx, %rdi
	movq	%rcx, -7144(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	xorl	%edi, %edi
	pxor	%xmm0, %xmm0
	movw	%di, -1184(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r12, %rdi
	movups	%xmm0, -1176(%rbp)
	movups	%xmm0, -1160(%rbp)
	movq	%rbx, -1408(%rbp)
	movq	$0, -1192(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7128(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%r15, -1408(%rbp)
	movq	%rcx, -1488(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7152(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7152(%rbp), %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L212
	movsbl	67(%rdi), %esi
.L213:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7120(%rbp), %rax
	movdqa	-7104(%rbp), %xmm4
	leaq	-1424(%rbp), %rdi
	movq	%rax, -1408(%rbp)
	movaps	%xmm4, -1488(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rax
	movq	-7144(%rbp), %rdi
	movq	%rbx, -1408(%rbp)
	movq	%rax, -1488(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	16(%r14), %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7192(%rbp)
	je	.L214
	leaq	-1760(%rbp), %rcx
	leaq	-1840(%rbp), %r12
	movq	%rcx, %rdi
	movq	%rcx, -7144(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	movw	%si, -1536(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -1528(%rbp)
	movups	%xmm0, -1512(%rbp)
	movq	%rbx, -1760(%rbp)
	movq	$0, -1544(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7128(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%r15, -1760(%rbp)
	movq	%rcx, -1840(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7152(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7152(%rbp), %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L215
	movsbl	67(%rdi), %esi
.L216:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7120(%rbp), %rax
	movdqa	-7104(%rbp), %xmm5
	leaq	-1776(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movaps	%xmm5, -1840(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rax
	movq	-7144(%rbp), %rdi
	movq	%rbx, -1760(%rbp)
	movq	%rax, -1840(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	24(%r14), %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7184(%rbp)
	je	.L217
	leaq	-2112(%rbp), %rcx
	leaq	-2192(%rbp), %r12
	movq	%rcx, %rdi
	movq	%rcx, -7144(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -1880(%rbp)
	movups	%xmm0, -1864(%rbp)
	movw	%cx, -1888(%rbp)
	movq	%rbx, -2112(%rbp)
	movq	$0, -1896(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7128(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%r15, -2112(%rbp)
	movq	%rcx, -2192(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7152(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7152(%rbp), %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L218
	movsbl	67(%rdi), %esi
.L219:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7120(%rbp), %rax
	movdqa	-7104(%rbp), %xmm6
	leaq	-2128(%rbp), %rdi
	movq	%rax, -2112(%rbp)
	movaps	%xmm6, -2192(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rax
	movq	-7144(%rbp), %rdi
	movq	%rbx, -2112(%rbp)
	movq	%rax, -2192(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	32(%r14), %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7224(%rbp)
	je	.L220
	leaq	-2464(%rbp), %rcx
	leaq	-2544(%rbp), %r12
	movq	%rcx, %rdi
	movq	%rcx, -7144(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movw	%dx, -2240(%rbp)
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -2232(%rbp)
	movups	%xmm0, -2216(%rbp)
	movq	%rbx, -2464(%rbp)
	movq	$0, -2248(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7128(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%r15, -2464(%rbp)
	movq	%rcx, -2544(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7152(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7152(%rbp), %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L221
	movsbl	67(%rdi), %esi
.L222:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7120(%rbp), %rax
	movdqa	-7104(%rbp), %xmm7
	leaq	-2480(%rbp), %rdi
	movq	%rax, -2464(%rbp)
	movaps	%xmm7, -2544(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rax
	movq	-7144(%rbp), %rdi
	movq	%rbx, -2464(%rbp)
	movq	%rax, -2544(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	40(%r14), %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7232(%rbp)
	je	.L223
	leaq	-2816(%rbp), %rcx
	leaq	-2896(%rbp), %r12
	movq	%rcx, %rdi
	movq	%rcx, -7144(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -2584(%rbp)
	movups	%xmm0, -2568(%rbp)
	movw	%ax, -2592(%rbp)
	movq	%rbx, -2816(%rbp)
	movq	$0, -2600(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7128(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%r15, -2816(%rbp)
	movq	%rcx, -2896(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7152(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7152(%rbp), %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L224
	movsbl	67(%rdi), %esi
.L225:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7120(%rbp), %rax
	movdqa	-7104(%rbp), %xmm4
	leaq	-2832(%rbp), %rdi
	movq	%rax, -2816(%rbp)
	movaps	%xmm4, -2896(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rax
	movq	-7144(%rbp), %rdi
	movq	%rbx, -2816(%rbp)
	movq	%rax, -2896(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	48(%r14), %eax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movl	%eax, -7196(%rbp)
	je	.L226
	leaq	-3168(%rbp), %rcx
	leaq	-3248(%rbp), %r12
	movq	%rcx, %rdi
	movq	%rcx, -7144(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -2936(%rbp)
	movups	%xmm0, -2920(%rbp)
	movw	%ax, -2944(%rbp)
	movq	%rbx, -3168(%rbp)
	movq	$0, -2952(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7128(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%r15, -3168(%rbp)
	movq	%rcx, -3248(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-7196(%rbp), %esi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7152(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7152(%rbp), %rdi
	movl	$4, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L227
	movsbl	67(%rdi), %esi
.L228:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7120(%rbp), %rax
	movdqa	-7104(%rbp), %xmm5
	leaq	-3184(%rbp), %rdi
	movq	%rax, -3168(%rbp)
	movaps	%xmm5, -3248(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rax
	movq	-7144(%rbp), %rdi
	movq	%rbx, -3168(%rbp)
	movq	%rax, -3248(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	52(%r14), %eax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movl	%eax, -7200(%rbp)
	je	.L229
	leaq	-3520(%rbp), %rcx
	leaq	-3600(%rbp), %r12
	movq	%rcx, %rdi
	movq	%rcx, -7144(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -3288(%rbp)
	movups	%xmm0, -3272(%rbp)
	movw	%ax, -3296(%rbp)
	movq	%rbx, -3520(%rbp)
	movq	$0, -3304(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7128(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%r15, -3520(%rbp)
	movq	%rcx, -3600(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-7200(%rbp), %esi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7152(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7152(%rbp), %rdi
	movl	$4, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L230
	movsbl	67(%rdi), %esi
.L231:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7120(%rbp), %rax
	movdqa	-7104(%rbp), %xmm6
	leaq	-3536(%rbp), %rdi
	movq	%rax, -3520(%rbp)
	movaps	%xmm6, -3600(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rax
	movq	-7144(%rbp), %rdi
	movq	%rbx, -3520(%rbp)
	movq	%rax, -3600(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	56(%r14), %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7168(%rbp)
	je	.L232
	leaq	-3872(%rbp), %rcx
	leaq	-3952(%rbp), %r12
	movq	%rcx, %rdi
	movq	%rcx, -7144(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -3640(%rbp)
	movups	%xmm0, -3624(%rbp)
	movw	%ax, -3648(%rbp)
	movq	%rbx, -3872(%rbp)
	movq	$0, -3656(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7128(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%r15, -3872(%rbp)
	movq	%rcx, -3952(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7168(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7152(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7152(%rbp), %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L233
	movsbl	67(%rdi), %esi
.L234:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7120(%rbp), %rax
	movdqa	-7104(%rbp), %xmm7
	leaq	-3888(%rbp), %rdi
	movq	%rax, -3872(%rbp)
	movaps	%xmm7, -3952(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rax
	movq	-7144(%rbp), %rdi
	movq	%rbx, -3872(%rbp)
	movq	%rax, -3952(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	64(%r14), %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7144(%rbp)
	je	.L235
	leaq	-4224(%rbp), %rcx
	leaq	-4304(%rbp), %r12
	movq	%rcx, %rdi
	movq	%rcx, -7152(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -3992(%rbp)
	movups	%xmm0, -3976(%rbp)
	movw	%ax, -4000(%rbp)
	movq	%rbx, -4224(%rbp)
	movq	$0, -4008(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7128(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%r15, -4224(%rbp)
	movq	%rcx, -4304(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7160(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7160(%rbp), %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L236
	movsbl	67(%rdi), %esi
.L237:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7120(%rbp), %rax
	movdqa	-7104(%rbp), %xmm3
	leaq	-4240(%rbp), %rdi
	movq	%rax, -4224(%rbp)
	movaps	%xmm3, -4304(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rax
	movq	-7152(%rbp), %rdi
	movq	%rbx, -4224(%rbp)
	movq	%rax, -4304(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	72(%r14), %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7152(%rbp)
	je	.L238
	leaq	-4576(%rbp), %rcx
	leaq	-4656(%rbp), %r12
	movq	%rcx, %rdi
	movq	%rcx, -7160(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r11d, %r11d
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r11w, -4352(%rbp)
	movups	%xmm0, -4344(%rbp)
	movups	%xmm0, -4328(%rbp)
	movq	%rbx, -4576(%rbp)
	movq	$0, -4360(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7128(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%r15, -4576(%rbp)
	movq	%rcx, -4656(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7152(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7176(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7176(%rbp), %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L239
	movsbl	67(%rdi), %esi
.L240:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7120(%rbp), %rax
	movdqa	-7104(%rbp), %xmm2
	leaq	-4592(%rbp), %rdi
	movq	%rax, -4576(%rbp)
	movaps	%xmm2, -4656(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rax
	movq	-7160(%rbp), %rdi
	movq	%rbx, -4576(%rbp)
	movq	%rax, -4656(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	80(%r14), %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7160(%rbp)
	je	.L241
	leaq	-4928(%rbp), %rcx
	leaq	-5008(%rbp), %r12
	movq	%rcx, %rdi
	movq	%rcx, -7176(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r10d, %r10d
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r10w, -4704(%rbp)
	movups	%xmm0, -4696(%rbp)
	movups	%xmm0, -4680(%rbp)
	movq	%rbx, -4928(%rbp)
	movq	$0, -4712(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7128(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%r15, -4928(%rbp)
	movq	%rcx, -5008(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7208(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7208(%rbp), %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L242
	movsbl	67(%rdi), %esi
.L243:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7120(%rbp), %rax
	movdqa	-7104(%rbp), %xmm4
	leaq	-4944(%rbp), %rdi
	movq	%rax, -4928(%rbp)
	movaps	%xmm4, -5008(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rax
	movq	-7176(%rbp), %rdi
	movq	%rbx, -4928(%rbp)
	movq	%rax, -5008(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	88(%r14), %eax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movl	%eax, -7208(%rbp)
	je	.L244
	leaq	-5280(%rbp), %rcx
	leaq	-5360(%rbp), %r12
	movq	%rcx, %rdi
	movq	%rcx, -7176(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r9w, -5056(%rbp)
	movups	%xmm0, -5048(%rbp)
	movups	%xmm0, -5032(%rbp)
	movq	%rbx, -5280(%rbp)
	movq	$0, -5064(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7128(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%r15, -5280(%rbp)
	movq	%rcx, -5360(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-7208(%rbp), %esi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7216(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7216(%rbp), %rdi
	movl	$4, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L245
	movsbl	67(%rdi), %esi
.L246:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7120(%rbp), %rax
	movdqa	-7104(%rbp), %xmm5
	leaq	-5296(%rbp), %rdi
	movq	%rax, -5280(%rbp)
	movaps	%xmm5, -5360(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rax
	movq	-7176(%rbp), %rdi
	movq	%rbx, -5280(%rbp)
	movq	%rax, -5360(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movzbl	92(%r14), %eax
	leaq	93(%r14), %rcx
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rcx, -7176(%rbp)
	movb	%al, -7216(%rbp)
	je	.L247
	leaq	-5632(%rbp), %r12
	leaq	-5712(%rbp), %r14
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r8w, -5408(%rbp)
	movups	%xmm0, -5400(%rbp)
	movups	%xmm0, -5384(%rbp)
	movq	%rbx, -5632(%rbp)
	movq	$0, -5416(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7128(%rbp), %rcx
	movq	%r14, %rdi
	movl	$6, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r15, -5632(%rbp)
	movq	%rcx, -5712(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movsbq	-7216(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %r15
	testq	%r15, %r15
	je	.L199
	cmpb	$0, 56(%r15)
	je	.L248
	movsbl	67(%r15), %esi
.L249:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7120(%rbp), %rax
	movdqa	-7104(%rbp), %xmm6
	leaq	-5648(%rbp), %rdi
	movq	%rax, -5632(%rbp)
	movaps	%xmm6, -5712(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rax
	movq	%r12, %rdi
	movq	%rbx, -5632(%rbp)
	movq	%rax, -5712(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L334:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L447:
	leaq	-352(%rbp), %rax
	movq	%r8, -7120(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	movq	%rax, %rdi
	movq	%rax, -7088(%rbp)
	movq	%rcx, -7128(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	movq	-7256(%rbp), %rdi
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%cx, -128(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	movq	%rbx, -352(%rbp)
	movq	$0, -136(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7256(%rbp), %rdi
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$15, %edx
	movq	%rax, -432(%rbp)
	leaq	.LC7(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -352(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7120(%rbp), %r8
	movq	-7256(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$9, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7120(%rbp), %r8
	movq	(%r8), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	movq	-7128(%rbp), %rcx
	je	.L259
	movsbl	67(%rdi), %esi
.L260:
	movq	%r8, %rdi
	movq	%rcx, -7120(%rbp)
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-7104(%rbp), %xmm2
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-368(%rbp), %rdi
	movq	%rax, -352(%rbp)
	movaps	%xmm2, -432(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%rbx, -352(%rbp)
	movq	%rax, -432(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7120(%rbp), %rcx
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L446:
	leaq	-704(%rbp), %rax
	leaq	-784(%rbp), %r8
	movq	%rax, %rdi
	movq	%rax, -7088(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	movq	%r8, -7120(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	-7120(%rbp), %r8
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	stdout(%rip), %rdx
	movw	%si, -480(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r8, %rdi
	movups	%xmm0, -472(%rbp)
	movups	%xmm0, -456(%rbp)
	movq	%rbx, -704(%rbp)
	movq	$0, -488(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7120(%rbp), %r8
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$15, %edx
	movq	%rax, -784(%rbp)
	leaq	.LC7(%rip), %rsi
	addq	$40, %rax
	movq	%r8, %rdi
	movq	%rax, -704(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7120(%rbp), %r8
	movq	-7152(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$9, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7120(%rbp), %r8
	movq	(%r8), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L254
	movsbl	67(%rdi), %esi
.L255:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-7104(%rbp), %xmm3
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-720(%rbp), %rdi
	movq	%rax, -704(%rbp)
	movaps	%xmm3, -784(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%rbx, -704(%rbp)
	movq	%rax, -784(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L444:
	leaq	-1056(%rbp), %rax
	leaq	-1136(%rbp), %r15
	movq	%rax, %rdi
	movq	%rax, -7088(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	xorl	%edi, %edi
	pxor	%xmm0, %xmm0
	movw	%di, -832(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r15, %rdi
	movups	%xmm0, -824(%rbp)
	movups	%xmm0, -808(%rbp)
	movq	%rbx, -1056(%rbp)
	movq	$0, -840(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$15, %edx
	movq	%rax, -1136(%rbp)
	leaq	.LC7(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -1056(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7144(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$9, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L251
	movsbl	67(%rdi), %esi
.L252:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-7104(%rbp), %xmm7
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1072(%rbp), %rdi
	movq	%rax, -1056(%rbp)
	movaps	%xmm7, -1136(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7088(%rbp), %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%rbx, -1056(%rbp)
	movq	%rax, -1136(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L445:
	movq	-7152(%rbp), %rbx
	movq	%rbx, %rdi
	call	_Znam@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdx
	addq	%rbx, %r14
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memcpy@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L443:
	movq	-7144(%rbp), %rbx
	movq	%rbx, %rdi
	call	_Znam@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdx
	addq	%rbx, %r14
	movq	%rax, %rdi
	movq	%rax, %r12
	call	memcpy@PLT
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%r8, -7120(%rbp)
	movq	%rdi, -7088(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7088(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7120(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L208
	movq	%r8, -7088(%rbp)
	call	*%rax
	movq	-7088(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L433:
	movdqu	-392(%rbp), %xmm6
	movups	%xmm6, -6792(%rbp)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%r8, -7216(%rbp)
	movq	%rdi, -7208(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7208(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7216(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L243
	movq	%r8, -7208(%rbp)
	call	*%rax
	movq	-7208(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L239:
	movq	%r8, -7208(%rbp)
	movq	%rdi, -7176(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7176(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7208(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L240
	movq	%r8, -7176(%rbp)
	call	*%rax
	movq	-7176(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%r8, -7176(%rbp)
	movq	%rdi, -7160(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7160(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7176(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L237
	movq	%r8, -7160(%rbp)
	call	*%rax
	movq	-7160(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%r8, -7160(%rbp)
	movq	%rdi, -7152(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7152(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7160(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L234
	movq	%r8, -7152(%rbp)
	call	*%rax
	movq	-7152(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L230:
	movq	%r8, -7160(%rbp)
	movq	%rdi, -7152(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7152(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7160(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L231
	movq	%r8, -7152(%rbp)
	call	*%rax
	movq	-7152(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L227:
	movq	%r8, -7160(%rbp)
	movq	%rdi, -7152(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7152(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7160(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L228
	movq	%r8, -7152(%rbp)
	call	*%rax
	movq	-7152(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L248:
	movq	%r15, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L249
	movq	%r15, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%r8, -7296(%rbp)
	movq	%rdi, -7216(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7216(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7296(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L246
	movq	%r8, -7216(%rbp)
	call	*%rax
	movq	-7216(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%r8, -7160(%rbp)
	movq	%rdi, -7152(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7152(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7160(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L213
	movq	%r8, -7152(%rbp)
	call	*%rax
	movq	-7152(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%r8, -7128(%rbp)
	movq	%rdi, -7120(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7120(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7128(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L255
	movq	%r8, -7120(%rbp)
	call	*%rax
	movq	-7120(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%r8, -7296(%rbp)
	movq	%rcx, -7128(%rbp)
	movq	%rdi, -7120(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7120(%rbp), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7128(%rbp), %rcx
	movq	-7296(%rbp), %r8
	movl	$10, %esi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L260
	movq	%r8, -7128(%rbp)
	movq	%rcx, -7120(%rbp)
	call	*%rax
	movq	-7128(%rbp), %r8
	movq	-7120(%rbp), %rcx
	movsbl	%al, %esi
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%rdi, -7120(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7120(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L252
	call	*%rax
	movsbl	%al, %esi
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L224:
	movq	%r8, -7160(%rbp)
	movq	%rdi, -7152(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7152(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7160(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L225
	movq	%r8, -7152(%rbp)
	call	*%rax
	movq	-7152(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%r8, -7160(%rbp)
	movq	%rdi, -7152(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7152(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7160(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L222
	movq	%r8, -7152(%rbp)
	call	*%rax
	movq	-7152(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%r8, -7160(%rbp)
	movq	%rdi, -7152(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7152(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7160(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L219
	movq	%r8, -7152(%rbp)
	call	*%rax
	movq	-7152(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L215:
	movq	%r8, -7160(%rbp)
	movq	%rdi, -7152(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7152(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7160(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L216
	movq	%r8, -7152(%rbp)
	call	*%rax
	movq	-7152(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L438:
	movq	%rbx, %rdi
	call	_Znam@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L440:
	leaq	-6336(%rbp), %r12
	leaq	-6416(%rbp), %r15
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r10d, %r10d
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r10w, -6112(%rbp)
	movups	%xmm0, -6104(%rbp)
	movups	%xmm0, -6088(%rbp)
	movq	%rax, -7080(%rbp)
	movq	%rax, -6336(%rbp)
	movq	$0, -6120(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$6, %edx
	movq	%rax, -7160(%rbp)
	leaq	.LC6(%rip), %rsi
	movq	%rax, -6416(%rbp)
	addq	$40, %rax
	movq	%rax, -7128(%rbp)
	movq	%rax, -6336(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-7104(%rbp), %esi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$4, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rbx
	testq	%rbx, %rbx
	je	.L199
	cmpb	$0, 56(%rbx)
	je	.L196
	movsbl	67(%rbx), %esi
.L197:
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rdx, %xmm5
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm5
	movaps	%xmm5, -7120(%rbp)
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-7120(%rbp), %xmm5
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-6352(%rbp), %rdi
	movq	%rax, -7144(%rbp)
	movq	%rax, -6336(%rbp)
	movaps	%xmm5, -6416(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -7152(%rbp)
	movq	%rax, -6416(%rbp)
	movq	-7080(%rbp), %rax
	movq	%rax, -6336(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7072(%rbp), %rcx
	movl	20(%rcx), %ebx
	addq	$24, %rcx
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rcx, -7088(%rbp)
	je	.L198
	leaq	-6688(%rbp), %r12
	leaq	-6768(%rbp), %r15
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	movq	-7080(%rbp), %rax
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r9w, -6464(%rbp)
	movups	%xmm0, -6456(%rbp)
	movups	%xmm0, -6440(%rbp)
	movq	%rax, -6688(%rbp)
	movq	$0, -6472(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-7160(%rbp), %rax
	movq	%r15, %rdi
	movl	$6, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rax, -6768(%rbp)
	movq	-7128(%rbp), %rax
	movq	%rax, -6688(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	%ebx, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$4, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rdi
	testq	%rdi, %rdi
	je	.L199
	cmpb	$0, 56(%rdi)
	je	.L200
	movsbl	67(%rdi), %esi
.L201:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-7144(%rbp), %rax
	movdqa	-7120(%rbp), %xmm7
	leaq	-6704(%rbp), %rdi
	movq	%rax, -6688(%rbp)
	movaps	%xmm7, -6768(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7152(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, -6768(%rbp)
	movq	-7080(%rbp), %rax
	movq	%rax, -6688(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L173:
	addl	$1, 8(%rax)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L187:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L176:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L280:
	movl	8(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	jmp	.L281
.L437:
	movq	0(%r13), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L449
.L179:
	testq	%r15, %r15
	je	.L180
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L181:
	cmpl	$1, %eax
	jne	.L175
	movq	0(%r13), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L182
	call	*8(%rax)
	jmp	.L175
.L439:
	movq	(%r12), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L450
.L190:
	testq	%r15, %r15
	je	.L191
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L192:
	cmpl	$1, %eax
	jne	.L186
	movq	(%r12), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L193
	call	*8(%rax)
	jmp	.L186
.L294:
	movl	12(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r12)
	jmp	.L295
.L200:
	movq	%rdi, -7128(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7128(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L201
	call	*%rax
	movsbl	%al, %esi
	jmp	.L201
.L196:
	movq	%rbx, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%rbx), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L197
	movq	%rbx, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L197
.L191:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L192
.L180:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L181
.L284:
	movl	12(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r12)
	jmp	.L285
.L434:
	movq	%rax, -7064(%rbp)
	movq	%r12, %rdi
	call	*%rdx
	movq	-7064(%rbp), %rax
	jmp	.L293
.L450:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L190
.L449:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L179
.L441:
	movq	%rax, -7064(%rbp)
	movq	%r12, %rdi
	call	*%rdx
	movq	-7064(%rbp), %rax
	jmp	.L283
.L269:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L296:
	call	*%rcx
	movq	-7064(%rbp), %rax
	jmp	.L162
.L182:
	call	*%rdx
	jmp	.L175
.L286:
	call	*%rcx
	movq	-7064(%rbp), %rax
	jmp	.L170
.L193:
	call	*%rdx
	jmp	.L186
.L199:
	call	_ZSt16__throw_bad_castv@PLT
.L432:
	call	__stack_chk_fail@PLT
.L214:
	leaq	24(%r14), %rax
	jmp	.L311
.L232:
	leaq	64(%r14), %rax
	jmp	.L305
.L229:
	leaq	56(%r14), %rax
	jmp	.L306
.L238:
	leaq	80(%r14), %rax
	jmp	.L303
.L235:
	leaq	72(%r14), %rax
	jmp	.L304
.L244:
	leaq	92(%r14), %rax
	jmp	.L301
.L241:
	leaq	88(%r14), %rax
	jmp	.L302
.L220:
	leaq	40(%r14), %rax
	jmp	.L309
.L217:
	leaq	32(%r14), %rax
	jmp	.L310
.L226:
	leaq	52(%r14), %rax
	jmp	.L307
.L223:
	leaq	48(%r14), %rax
	jmp	.L308
	.cfi_endproc
.LFE19715:
	.size	_ZN2v88internal4wasm23DeserializeNativeModuleEPNS0_7IsolateENS0_6VectorIKhEES6_, .-_ZN2v88internal4wasm23DeserializeNativeModuleEPNS0_7IsolateENS0_6VectorIKhEES6_
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB23030:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L459
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L453:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L453
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE23030:
	.size	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.text._ZNK2v88internal4wasm14WasmSerializer29GetSerializedNativeModuleSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm14WasmSerializer29GetSerializedNativeModuleSizeEv
	.type	_ZNK2v88internal4wasm14WasmSerializer29GetSerializedNativeModuleSizeEv, @function
_ZNK2v88internal4wasm14WasmSerializer29GetSerializedNativeModuleSizeEv:
.LFB19696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-128(%rbp), %r8
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	subq	%rdx, %rcx
	sarq	$3, %rcx
	call	_ZN2v88internal4wasm22NativeModuleSerializerC1EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rax
	leaq	(%rdx,%rax,8), %rdi
	cmpq	%rdi, %rdx
	je	.L471
	movl	$8, %eax
	.p2align 4,,10
	.p2align 3
.L467:
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L464
	movq	40(%rcx), %r8
	movq	24(%rcx), %rsi
	addq	$8, %rdx
	addq	8(%rcx), %rsi
	movq	128(%rcx), %rcx
	leaq	93(%rsi,%r8), %rsi
	leaq	(%rsi,%rcx,8), %rcx
	addq	%rcx, %rax
	cmpq	%rdx, %rdi
	jne	.L467
.L465:
	leaq	16(%rax), %r12
.L463:
	movq	-80(%rbp), %rbx
	leaq	-96(%rbp), %r13
	testq	%rbx, %rbx
	je	.L462
.L468:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L468
.L462:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L478
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	addq	$8, %rdx
	addq	$8, %rax
	cmpq	%rdx, %rdi
	jne	.L467
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L471:
	movl	$24, %r12d
	jmp	.L463
.L478:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19696:
	.size	_ZNK2v88internal4wasm14WasmSerializer29GetSerializedNativeModuleSizeEv, .-_ZNK2v88internal4wasm14WasmSerializer29GetSerializedNativeModuleSizeEv
	.section	.rodata._ZNK2v88internal4wasm14WasmSerializer21SerializeNativeModuleENS0_6VectorIhEE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"wrote vector of "
	.section	.text._ZNK2v88internal4wasm14WasmSerializer21SerializeNativeModuleENS0_6VectorIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm14WasmSerializer21SerializeNativeModuleENS0_6VectorIhEE
	.type	_ZNK2v88internal4wasm14WasmSerializer21SerializeNativeModuleENS0_6VectorIhEE, @function
_ZNK2v88internal4wasm14WasmSerializer21SerializeNativeModuleENS0_6VectorIhEE:
.LFB19700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$3240, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rbx
	movq	8(%rdi), %rdx
	leaq	-7168(%rbp), %r8
	movq	%rsi, %r12
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	subq	%rdx, %rcx
	sarq	$3, %rcx
	call	_ZN2v88internal4wasm22NativeModuleSerializerC1EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE
	movq	-7160(%rbp), %rax
	movq	-7152(%rbp), %rdx
	leaq	(%rax,%rdx,8), %rdi
	cmpq	%rdi, %rax
	je	.L579
	movl	$8, %edx
	.p2align 4,,10
	.p2align 3
.L484:
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.L481
	movq	40(%rcx), %r8
	movq	24(%rcx), %rsi
	addq	$8, %rax
	addq	8(%rcx), %rsi
	movq	128(%rcx), %rcx
	leaq	93(%rsi,%r8), %rsi
	leaq	(%rsi,%rcx,8), %rcx
	addq	%rcx, %rdx
	cmpq	%rax, %rdi
	jne	.L484
.L482:
	addq	$16, %rdx
.L480:
	xorl	%r13d, %r13d
	cmpq	%rdx, %rbx
	jnb	.L683
.L485:
	movq	-7120(%rbp), %rbx
	leaq	-7136(%rbp), %r12
	testq	%rbx, %rbx
	je	.L479
.L576:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L576
.L479:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L684
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	addq	$8, %rax
	addq	$8, %rdx
	cmpq	%rax, %rdi
	jne	.L484
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L683:
	addq	%r12, %rbx
	leaq	-7328(%rbp), %rdi
	movq	%r12, -7328(%rbp)
	movq	%r12, -7312(%rbp)
	movq	%rbx, -7320(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_112WriteVersionEPNS2_6WriterE
	movq	-7168(%rbp), %rax
	movb	$1, -7144(%rbp)
	movq	208(%rax), %rax
	movl	60(%rax), %r12d
	addl	68(%rax), %r12d
	movq	-7312(%rbp), %rax
	movl	%r12d, (%rax)
	movq	-7312(%rbp), %rax
	addq	$4, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L685
.L486:
	movq	-7168(%rbp), %rdx
	movq	208(%rdx), %rdx
	movl	60(%rdx), %r12d
	movl	%r12d, (%rax)
	addq	$4, -7312(%rbp)
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	jne	.L686
.L490:
	movq	-7160(%rbp), %r14
	movq	-7152(%rbp), %rax
	leaq	(%r14,%rax,8), %rax
	movq	%rax, -7360(%rbp)
	cmpq	%rax, %r14
	je	.L574
	.p2align 4,,10
	.p2align 3
.L575:
	movq	(%r14), %r12
	movq	-7312(%rbp), %rdx
	testq	%r12, %r12
	je	.L687
	movq	40(%r12), %rcx
	movq	24(%r12), %rax
	addq	8(%r12), %rax
	leaq	93(%rax,%rcx), %rax
	movq	128(%r12), %rcx
	leaq	(%rax,%rcx,8), %r8
	movq	%r8, (%rdx)
	movq	-7312(%rbp), %rax
	addq	$8, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L688
.L500:
	movq	64(%r12), %r8
	movq	%r8, (%rax)
	movq	-7312(%rbp), %rax
	addq	$8, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L689
.L503:
	movq	80(%r12), %r8
	movq	%r8, (%rax)
	movq	-7312(%rbp), %rax
	addq	$8, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L690
.L506:
	movq	88(%r12), %r8
	movq	%r8, (%rax)
	movq	-7312(%rbp), %rax
	addq	$8, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L691
.L509:
	movq	96(%r12), %r8
	movq	%r8, (%rax)
	movq	-7312(%rbp), %rax
	addq	$8, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L692
.L512:
	movq	104(%r12), %r8
	movq	%r8, (%rax)
	movq	-7312(%rbp), %rax
	addq	$8, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L693
.L515:
	movl	72(%r12), %ecx
	movl	%ecx, (%rax)
	movq	-7312(%rbp), %rax
	addq	$4, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L694
.L518:
	movl	76(%r12), %ecx
	movl	%ecx, (%rax)
	movq	-7312(%rbp), %rax
	addq	$4, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L695
.L521:
	movq	8(%r12), %r8
	movq	%r8, (%rax)
	movq	-7312(%rbp), %rax
	addq	$8, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L696
.L524:
	movq	24(%r12), %r8
	movq	%r8, (%rax)
	movq	-7312(%rbp), %rax
	addq	$8, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L697
.L527:
	movq	40(%r12), %r8
	movq	%r8, (%rax)
	movq	-7312(%rbp), %rax
	addq	$8, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L698
.L530:
	movq	128(%r12), %r8
	movq	%r8, (%rax)
	movq	-7312(%rbp), %rax
	addq	$8, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L699
.L533:
	movl	60(%r12), %ecx
	movl	%ecx, (%rax)
	movq	-7312(%rbp), %rax
	addq	$4, %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	movq	%rax, -7312(%rbp)
	jne	.L700
.L536:
	movzbl	136(%r12), %ecx
	movb	%cl, (%rax)
	movq	-7312(%rbp), %rax
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	leaq	1(%rax), %r13
	movq	%r13, -7312(%rbp)
	jne	.L701
.L539:
	movq	8(%r12), %rax
	movq	24(%r12), %r8
	leaq	0(%r13,%rax), %rdi
	movq	%rax, -7336(%rbp)
	movq	%rdi, -7312(%rbp)
	testq	%r8, %r8
	jne	.L702
.L542:
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	jne	.L703
.L543:
	movq	40(%r12), %r8
	testq	%r8, %r8
	jne	.L704
.L546:
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	jne	.L705
.L547:
	movslq	128(%r12), %r8
	salq	$3, %r8
	movslq	%r8d, %r8
	testq	%r8, %r8
	jne	.L706
.L550:
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	jne	.L707
.L551:
	movq	(%r12), %rsi
	movq	-7336(%rbp), %rdx
	movq	%r13, %rdi
	leaq	-7296(%rbp), %r15
	leaq	-7232(%rbp), %rbx
	call	memcpy@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal4wasm8WasmCode13constant_poolEv@PLT
	subq	$8, %rsp
	movq	(%r12), %rsi
	movq	%r15, %rdi
	movq	24(%r12), %r8
	movq	16(%r12), %rcx
	movq	%rax, %r9
	movq	8(%r12), %rdx
	pushq	$944
	call	_ZN2v88internal13RelocIteratorC1ENS0_6VectorIhEENS2_IKhEEmi@PLT
	movq	64(%r12), %r9
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	8(%r12), %rdx
	movq	24(%r12), %r8
	movq	16(%r12), %rcx
	addq	%r13, %r9
	movl	$944, (%rsp)
	call	_ZN2v88internal13RelocIteratorC1ENS0_6VectorIhEENS2_IKhEEmi@PLT
	cmpb	$0, -7176(%rbp)
	popq	%rax
	popq	%rdx
	je	.L554
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L557:
	leaq	-7280(%rbp), %rdi
	call	_ZNK2v88internal9RelocInfo17wasm_call_addressEv@PLT
	movq	-7168(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZNK2v88internal4wasm12NativeModule33GetFunctionIndexFromJumpTableSlotEm@PLT
	movq	-7216(%rbp), %rdx
	movl	%eax, (%rdx)
.L561:
	movq	%rbx, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -7176(%rbp)
	jne	.L499
.L554:
	movzbl	-7272(%rbp), %eax
	cmpb	$7, %al
	je	.L555
	jg	.L556
	cmpb	$4, %al
	je	.L557
	cmpb	$5, %al
	jne	.L559
	leaq	-7280(%rbp), %rdi
	call	_ZNK2v88internal9RelocInfo22wasm_stub_call_addressEv@PLT
	movq	-7120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L601
	leaq	-7128(%rbp), %rsi
	movq	%rsi, %rcx
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L708:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L564
.L563:
	cmpq	32(%rdx), %rax
	jbe	.L708
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L563
.L564:
	cmpq	%rsi, %rcx
	je	.L562
	cmpq	32(%rcx), %rax
	cmovb	%rsi, %rcx
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L601:
	leaq	-7128(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L562:
	movl	40(%rcx), %edx
	movq	-7216(%rbp), %rax
	movl	%edx, (%rax)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L555:
	movq	-7280(%rbp), %rax
	movq	(%rax), %r13
	movzbl	_ZGVZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %eax
	testb	%al, %al
	je	.L709
.L568:
	movl	$582, %eax
	leaq	4656+_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L571:
	testq	%rax, %rax
	jle	.L570
.L710:
	movq	%rax, %rcx
	leaq	_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %r10
	sarq	%rcx
	leaq	(%rdx,%rcx,4), %rsi
	movl	(%rsi), %edi
	cmpq	(%r10,%rdi,8), %r13
	jbe	.L604
	subq	%rcx, %rax
	leaq	4(%rsi), %rdx
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L710
.L570:
	movl	(%rdx), %edx
	movq	-7216(%rbp), %rax
	movl	%edx, (%rax)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L709:
	leaq	_ZGVZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L568
	call	_ZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceListC2Ev.constprop.0
	leaq	_ZGVZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L556:
	subl	$8, %eax
	cmpb	$1, %al
	ja	.L559
	movq	-7280(%rbp), %rax
	movq	-7216(%rbp), %rdx
	movq	(%rax), %rax
	subq	(%r12), %rax
	movq	%rax, (%rdx)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L687:
	movq	$0, (%rdx)
	addq	$8, -7312(%rbp)
	cmpb	$0, _ZN2v88internal29FLAG_trace_wasm_serializationE(%rip)
	je	.L499
	leaq	-6304(%rbp), %r12
	leaq	-6384(%rbp), %r13
	movq	%r12, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r9w, -6080(%rbp)
	movups	%xmm0, -6072(%rbp)
	movups	%xmm0, -6056(%rbp)
	movq	%rbx, -6304(%rbp)
	movq	$0, -6088(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r13, %rdi
	movl	$7, %edx
	movq	%rax, -6384(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -6304(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r15
	testq	%r15, %r15
	je	.L491
	cmpb	$0, 56(%r15)
	je	.L497
	movsbl	67(%r15), %esi
.L498:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -6304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-6320(%rbp), %rdi
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -6384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r12, %rdi
	movq	%rbx, -6304(%rbp)
	movq	%rax, -6384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L499:
	addq	$8, %r14
	cmpq	%r14, -7360(%rbp)
	jne	.L575
.L574:
	movl	$1, %r13d
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L604:
	movq	%rcx, %rax
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L707:
	leaq	-5952(%rbp), %rax
	movq	%r8, -7352(%rbp)
	leaq	-6032(%rbp), %r15
	movq	%rax, %rdi
	movq	%rax, -7344(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%cx, -5728(%rbp)
	movups	%xmm0, -5720(%rbp)
	movups	%xmm0, -5704(%rbp)
	movq	%rbx, -5952(%rbp)
	movq	$0, -5736(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$16, %edx
	movq	%r15, %rdi
	movq	%rax, -6032(%rbp)
	leaq	.LC10(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -5952(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7352(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$9, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7352(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7352(%rbp), %r8
	movq	(%r8), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L491
	cmpb	$0, 56(%rdi)
	je	.L552
	movsbl	67(%rdi), %esi
.L553:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -5952(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-5968(%rbp), %rdi
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -6032(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7344(%rbp), %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%rbx, -5952(%rbp)
	movq	%rax, -6032(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L706:
	movq	120(%r12), %rsi
	movq	-7312(%rbp), %rdi
	movq	%r8, %rdx
	movq	%r8, -7344(%rbp)
	call	memcpy@PLT
	movq	-7344(%rbp), %r8
	addq	%r8, -7312(%rbp)
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L705:
	leaq	-5600(%rbp), %rax
	movq	%r8, -7352(%rbp)
	leaq	-5680(%rbp), %r15
	movq	%rax, %rdi
	movq	%rax, -7344(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	movw	%si, -5376(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -5368(%rbp)
	movups	%xmm0, -5352(%rbp)
	movq	%rbx, -5600(%rbp)
	movq	$0, -5384(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$16, %edx
	movq	%r15, %rdi
	movq	%rax, -5680(%rbp)
	leaq	.LC10(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -5600(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7352(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$9, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7352(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7352(%rbp), %r8
	movq	(%r8), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L491
	cmpb	$0, 56(%rdi)
	je	.L548
	movsbl	67(%rdi), %esi
.L549:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -5600(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-5616(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -5680(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7344(%rbp), %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%rbx, -5600(%rbp)
	movq	%rax, -5680(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L704:
	movq	32(%r12), %rsi
	movq	-7312(%rbp), %rdi
	movq	%r8, %rdx
	movq	%r8, -7344(%rbp)
	call	memcpy@PLT
	movq	-7344(%rbp), %r8
	addq	%r8, -7312(%rbp)
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L703:
	leaq	-5248(%rbp), %rax
	movq	%r8, -7352(%rbp)
	leaq	-5328(%rbp), %r15
	movq	%rax, %rdi
	movq	%rax, -7344(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	xorl	%edi, %edi
	pxor	%xmm0, %xmm0
	movw	%di, -5024(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r15, %rdi
	movups	%xmm0, -5016(%rbp)
	movups	%xmm0, -5000(%rbp)
	movq	%rbx, -5248(%rbp)
	movq	$0, -5032(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$16, %edx
	movq	%r15, %rdi
	movq	%rax, -5328(%rbp)
	leaq	.LC10(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -5248(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7352(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$9, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -7352(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7352(%rbp), %r8
	movq	(%r8), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L491
	cmpb	$0, 56(%rdi)
	je	.L544
	movsbl	67(%rdi), %esi
.L545:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -5248(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-5264(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -5328(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-7344(%rbp), %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%rbx, -5248(%rbp)
	movq	%rax, -5328(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L702:
	movq	16(%r12), %rsi
	movq	%r8, %rdx
	movq	%r8, -7344(%rbp)
	call	memcpy@PLT
	movq	-7344(%rbp), %r8
	addq	%r8, -7312(%rbp)
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L701:
	leaq	-4896(%rbp), %r13
	movb	%cl, -7336(%rbp)
	leaq	-4976(%rbp), %r15
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r8w, -4672(%rbp)
	movups	%xmm0, -4664(%rbp)
	movups	%xmm0, -4648(%rbp)
	movq	%rbx, -4896(%rbp)
	movq	$0, -4680(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -4976(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -4896(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movsbq	-7336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$1, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rdi
	testq	%rdi, %rdi
	je	.L491
	cmpb	$0, 56(%rdi)
	je	.L540
	movsbl	67(%rdi), %esi
.L541:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -4896(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-4912(%rbp), %rdi
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -4976(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -4896(%rbp)
	movq	%rax, -4976(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %r13
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L700:
	leaq	-4544(%rbp), %r13
	movl	%ecx, -7336(%rbp)
	leaq	-4624(%rbp), %r15
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r9w, -4320(%rbp)
	movups	%xmm0, -4312(%rbp)
	movups	%xmm0, -4296(%rbp)
	movq	%rbx, -4544(%rbp)
	movq	$0, -4328(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -4624(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -4544(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-7336(%rbp), %esi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$4, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rdi
	testq	%rdi, %rdi
	je	.L491
	cmpb	$0, 56(%rdi)
	je	.L537
	movsbl	67(%rdi), %esi
.L538:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -4544(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-4560(%rbp), %rdi
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -4624(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -4544(%rbp)
	movq	%rax, -4624(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L699:
	leaq	-320(%rbp), %r13
	movq	%r8, -7336(%rbp)
	leaq	-400(%rbp), %r15
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r10d, %r10d
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r10w, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rbx, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -400(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7336(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rdi
	testq	%rdi, %rdi
	je	.L491
	cmpb	$0, 56(%rdi)
	je	.L534
	movsbl	67(%rdi), %esi
.L535:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -320(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L691:
	leaq	-3136(%rbp), %r13
	movq	%r8, -7336(%rbp)
	leaq	-3216(%rbp), %r15
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%cx, -2912(%rbp)
	movups	%xmm0, -2904(%rbp)
	movups	%xmm0, -2888(%rbp)
	movq	%rbx, -3136(%rbp)
	movq	$0, -2920(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -3216(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -3136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7336(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %r15
	testq	%r15, %r15
	je	.L491
	cmpb	$0, 56(%r15)
	je	.L510
	movsbl	67(%r15), %esi
.L511:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -3136(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-3152(%rbp), %rdi
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -3216(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -3136(%rbp)
	movq	%rax, -3216(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L689:
	leaq	-3840(%rbp), %r13
	movq	%r8, -7336(%rbp)
	leaq	-3920(%rbp), %r15
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	xorl	%edi, %edi
	pxor	%xmm0, %xmm0
	movw	%di, -3616(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r15, %rdi
	movups	%xmm0, -3608(%rbp)
	movups	%xmm0, -3592(%rbp)
	movq	%rbx, -3840(%rbp)
	movq	$0, -3624(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -3920(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -3840(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7336(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %r15
	testq	%r15, %r15
	je	.L491
	cmpb	$0, 56(%r15)
	je	.L504
	movsbl	67(%r15), %esi
.L505:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -3840(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-3856(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -3920(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -3840(%rbp)
	movq	%rax, -3920(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L695:
	leaq	-1728(%rbp), %r13
	movl	%ecx, -7336(%rbp)
	leaq	-1808(%rbp), %r15
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -1496(%rbp)
	movups	%xmm0, -1480(%rbp)
	movw	%ax, -1504(%rbp)
	movq	%rbx, -1728(%rbp)
	movq	$0, -1512(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -1808(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -1728(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-7336(%rbp), %esi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$4, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %r15
	testq	%r15, %r15
	je	.L491
	cmpb	$0, 56(%r15)
	je	.L522
	movsbl	67(%r15), %esi
.L523:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -1728(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-1744(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -1808(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -1728(%rbp)
	movq	%rax, -1808(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L690:
	leaq	-3488(%rbp), %r13
	movq	%r8, -7336(%rbp)
	leaq	-3568(%rbp), %r15
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	movw	%si, -3264(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -3256(%rbp)
	movups	%xmm0, -3240(%rbp)
	movq	%rbx, -3488(%rbp)
	movq	$0, -3272(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -3568(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -3488(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7336(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %r15
	testq	%r15, %r15
	je	.L491
	cmpb	$0, 56(%r15)
	je	.L507
	movsbl	67(%r15), %esi
.L508:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -3488(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-3504(%rbp), %rdi
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -3568(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -3488(%rbp)
	movq	%rax, -3568(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L697:
	leaq	-1024(%rbp), %r13
	movq	%r8, -7336(%rbp)
	leaq	-1104(%rbp), %r15
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -792(%rbp)
	movups	%xmm0, -776(%rbp)
	movw	%ax, -800(%rbp)
	movq	%rbx, -1024(%rbp)
	movq	$0, -808(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -1104(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -1024(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7336(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %r15
	testq	%r15, %r15
	je	.L491
	cmpb	$0, 56(%r15)
	je	.L528
	movsbl	67(%r15), %esi
.L529:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -1024(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-1040(%rbp), %rdi
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -1104(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -1024(%rbp)
	movq	%rax, -1104(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L688:
	leaq	-4192(%rbp), %r13
	leaq	-4272(%rbp), %r15
	movq	%r8, -7336(%rbp)
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -3960(%rbp)
	movups	%xmm0, -3944(%rbp)
	movw	%r8w, -3968(%rbp)
	movq	%rbx, -4192(%rbp)
	movq	$0, -3976(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -4272(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -4192(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7336(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %r15
	testq	%r15, %r15
	je	.L491
	cmpb	$0, 56(%r15)
	je	.L501
	movsbl	67(%r15), %esi
.L502:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -4192(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-4208(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -4272(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -4192(%rbp)
	movq	%rax, -4272(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L693:
	leaq	-2432(%rbp), %r13
	movq	%r8, -7336(%rbp)
	leaq	-2512(%rbp), %r15
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -2200(%rbp)
	movups	%xmm0, -2184(%rbp)
	movw	%ax, -2208(%rbp)
	movq	%rbx, -2432(%rbp)
	movq	$0, -2216(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -2512(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -2432(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7336(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %r15
	testq	%r15, %r15
	je	.L491
	cmpb	$0, 56(%r15)
	je	.L516
	movsbl	67(%r15), %esi
.L517:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -2432(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-2448(%rbp), %rdi
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -2512(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -2432(%rbp)
	movq	%rax, -2512(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L698:
	leaq	-672(%rbp), %r13
	movq	%r8, -7336(%rbp)
	leaq	-752(%rbp), %r15
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r11d, %r11d
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r11w, -448(%rbp)
	movups	%xmm0, -440(%rbp)
	movups	%xmm0, -424(%rbp)
	movq	%rbx, -672(%rbp)
	movq	$0, -456(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -752(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -672(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7336(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rdi
	testq	%rdi, %rdi
	je	.L491
	cmpb	$0, 56(%rdi)
	je	.L531
	movsbl	67(%rdi), %esi
.L532:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -672(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-688(%rbp), %rdi
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -752(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -672(%rbp)
	movq	%rax, -752(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L694:
	leaq	-2080(%rbp), %r13
	movl	%ecx, -7336(%rbp)
	leaq	-2160(%rbp), %r15
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -1848(%rbp)
	movups	%xmm0, -1832(%rbp)
	movw	%ax, -1856(%rbp)
	movq	%rbx, -2080(%rbp)
	movq	$0, -1864(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -2160(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -2080(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-7336(%rbp), %esi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$4, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %r15
	testq	%r15, %r15
	je	.L491
	cmpb	$0, 56(%r15)
	je	.L519
	movsbl	67(%r15), %esi
.L520:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -2080(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-2096(%rbp), %rdi
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -2160(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -2080(%rbp)
	movq	%rax, -2160(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L696:
	leaq	-1376(%rbp), %r13
	movq	%r8, -7336(%rbp)
	leaq	-1456(%rbp), %r15
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -1144(%rbp)
	movups	%xmm0, -1128(%rbp)
	movw	%ax, -1152(%rbp)
	movq	%rbx, -1376(%rbp)
	movq	$0, -1160(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -1456(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -1376(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7336(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %r15
	testq	%r15, %r15
	je	.L491
	cmpb	$0, 56(%r15)
	je	.L525
	movsbl	67(%r15), %esi
.L526:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -1376(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-1392(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -1456(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -1376(%rbp)
	movq	%rax, -1456(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L692:
	leaq	-2784(%rbp), %r13
	movq	%r8, -7336(%rbp)
	leaq	-2864(%rbp), %r15
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movw	%dx, -2560(%rbp)
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -2552(%rbp)
	movups	%xmm0, -2536(%rbp)
	movq	%rbx, -2784(%rbp)
	movq	$0, -2568(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%rax, -2864(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -2784(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-7336(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$8, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %r15
	testq	%r15, %r15
	je	.L491
	cmpb	$0, 56(%r15)
	je	.L513
	movsbl	67(%r15), %esi
.L514:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -2784(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-2800(%rbp), %rdi
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -2864(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -2784(%rbp)
	movq	%rax, -2864(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L504:
	movq	%r15, %rdi
	movq	%r8, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7336(%rbp), %r8
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L505
	movq	%r15, %rdi
	call	*%rax
	movq	-7336(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L501:
	movq	%r15, %rdi
	movq	%r8, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7336(%rbp), %r8
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L502
	movq	%r15, %rdi
	call	*%rax
	movq	-7336(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L507:
	movq	%r15, %rdi
	movq	%r8, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7336(%rbp), %r8
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L508
	movq	%r15, %rdi
	call	*%rax
	movq	-7336(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L513:
	movq	%r15, %rdi
	movq	%r8, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7336(%rbp), %r8
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L514
	movq	%r15, %rdi
	call	*%rax
	movq	-7336(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L510:
	movq	%r15, %rdi
	movq	%r8, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7336(%rbp), %r8
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L511
	movq	%r15, %rdi
	call	*%rax
	movq	-7336(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L516:
	movq	%r15, %rdi
	movq	%r8, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7336(%rbp), %r8
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L517
	movq	%r15, %rdi
	call	*%rax
	movq	-7336(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L519:
	movq	%r15, %rdi
	movq	%r8, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7336(%rbp), %r8
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L520
	movq	%r15, %rdi
	call	*%rax
	movq	-7336(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L522:
	movq	%r15, %rdi
	movq	%r8, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7336(%rbp), %r8
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L523
	movq	%r15, %rdi
	call	*%rax
	movq	-7336(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L528:
	movq	%r15, %rdi
	movq	%r8, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7336(%rbp), %r8
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L529
	movq	%r15, %rdi
	call	*%rax
	movq	-7336(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L525:
	movq	%r15, %rdi
	movq	%r8, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7336(%rbp), %r8
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L526
	movq	%r15, %rdi
	call	*%rax
	movq	-7336(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L531:
	movq	%rdi, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7336(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L532
	call	*%rax
	movsbl	%al, %esi
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L537:
	movq	%rdi, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7336(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L538
	call	*%rax
	movsbl	%al, %esi
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L534:
	movq	%rdi, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7336(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L535
	call	*%rax
	movsbl	%al, %esi
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L540:
	movq	%rdi, -7336(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7336(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L541
	call	*%rax
	movsbl	%al, %esi
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L544:
	movq	%r8, -7368(%rbp)
	movq	%rdi, -7352(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7352(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7368(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L545
	movq	%r8, -7352(%rbp)
	call	*%rax
	movq	-7352(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L548:
	movq	%r8, -7368(%rbp)
	movq	%rdi, -7352(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7352(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7368(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L549
	movq	%r8, -7352(%rbp)
	call	*%rax
	movq	-7352(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%r8, -7368(%rbp)
	movq	%rdi, -7352(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-7352(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-7368(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L553
	movq	%r8, -7352(%rbp)
	call	*%rax
	movq	-7352(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L497:
	movq	%r15, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L498
	movq	%r15, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L686:
	leaq	-6656(%rbp), %r13
	leaq	-6736(%rbp), %r14
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r10d, %r10d
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r10w, -6432(%rbp)
	movups	%xmm0, -6424(%rbp)
	movups	%xmm0, -6408(%rbp)
	movq	%rbx, -6656(%rbp)
	movq	$0, -6440(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r14, %rdi
	movl	$7, %edx
	movq	%rax, -6736(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -6656(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r14
	testq	%r14, %r14
	je	.L491
	cmpb	$0, 56(%r14)
	je	.L492
	movsbl	67(%r14), %esi
.L493:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -6656(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-6672(%rbp), %rdi
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -6736(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -6656(%rbp)
	movq	%rax, -6736(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L685:
	leaq	-7008(%rbp), %r13
	leaq	-7088(%rbp), %r14
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r11d, %r11d
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r11w, -6784(%rbp)
	movups	%xmm0, -6776(%rbp)
	movups	%xmm0, -6760(%rbp)
	movq	%rbx, -7008(%rbp)
	movq	$0, -6792(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r14, %rdi
	movl	$7, %edx
	movq	%rax, -7088(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -7008(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r14
	testq	%r14, %r14
	je	.L491
	cmpb	$0, 56(%r14)
	je	.L488
	movsbl	67(%r14), %esi
.L489:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -7008(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-7024(%rbp), %rdi
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -7088(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -7008(%rbp)
	movq	%rax, -7088(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-7312(%rbp), %rax
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L579:
	movl	$24, %edx
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L492:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L493
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L489
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L559:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L491:
	call	_ZSt16__throw_bad_castv@PLT
.L684:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19700:
	.size	_ZNK2v88internal4wasm14WasmSerializer21SerializeNativeModuleENS0_6VectorIhEE, .-_ZNK2v88internal4wasm14WasmSerializer21SerializeNativeModuleENS0_6VectorIhEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm22NativeModuleSerializerC2EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm22NativeModuleSerializerC2EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE, @function
_GLOBAL__sub_I__ZN2v88internal4wasm22NativeModuleSerializerC2EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE:
.LFB24457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24457:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm22NativeModuleSerializerC2EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE, .-_GLOBAL__sub_I__ZN2v88internal4wasm22NativeModuleSerializerC2EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm22NativeModuleSerializerC2EPKNS1_12NativeModuleENS0_6VectorIKPNS1_8WasmCodeEEE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZGVZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list, @object
	.size	_ZGVZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list, 8
_ZGVZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list:
	.zero	8
	.section	.bss._ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list,"aw",@nobits
	.align 32
	.type	_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list, @object
	.size	_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list, 6984
_ZZN2v88internal4wasm12_GLOBAL__N_121ExternalReferenceList3GetEvE4list:
	.zero	6984
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	0
	.long	1
	.long	2
	.long	3
	.align 16
.LC3:
	.long	4
	.long	4
	.long	4
	.long	4
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
