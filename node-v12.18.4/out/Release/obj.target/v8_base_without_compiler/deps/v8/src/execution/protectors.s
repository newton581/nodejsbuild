	.file	"protectors.cc"
	.text
	.section	.rodata._ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"regexp_species_protector"
	.section	.text._ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE
	.type	_ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE, @function
_ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE:
.LFB17951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	(%rsi), %rax
	movq	1239(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L5
.L11:
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L6:
	addq	$16, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L10
.L4:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L11
.L5:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L12
.L7:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	$0, (%rcx)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L7
	.cfi_endproc
.LFE17951:
	.size	_ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE, .-_ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE
	.section	.rodata._ZN2v88internal10Protectors33InvalidateArraySpeciesLookupChainEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"array_species_protector"
	.section	.text._ZN2v88internal10Protectors33InvalidateArraySpeciesLookupChainEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Protectors33InvalidateArraySpeciesLookupChainEPNS0_7IsolateE
	.type	_ZN2v88internal10Protectors33InvalidateArraySpeciesLookupChainEPNS0_7IsolateE, @function
_ZN2v88internal10Protectors33InvalidateArraySpeciesLookupChainEPNS0_7IsolateE:
.LFB17952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L14
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L15:
	addq	$8, %rsp
	leaq	4520(%r12), %rdx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PropertyCell24SetValueWithInvalidationEPNS0_7IsolateEPKcNS0_6HandleIS1_EENS6_INS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L18
.L16:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	$0, (%rcx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L16
	.cfi_endproc
.LFE17952:
	.size	_ZN2v88internal10Protectors33InvalidateArraySpeciesLookupChainEPNS0_7IsolateE, .-_ZN2v88internal10Protectors33InvalidateArraySpeciesLookupChainEPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE, @function
_GLOBAL__sub_I__ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE:
.LFB21640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21640:
	.size	_GLOBAL__sub_I__ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE, .-_GLOBAL__sub_I__ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
