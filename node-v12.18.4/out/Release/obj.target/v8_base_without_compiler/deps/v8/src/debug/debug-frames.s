	.file	"debug-frames.cc"
	.text
	.section	.text._ZN2v88internal23RedirectActiveFunctionsD2Ev,"axG",@progbits,_ZN2v88internal23RedirectActiveFunctionsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23RedirectActiveFunctionsD2Ev
	.type	_ZN2v88internal23RedirectActiveFunctionsD2Ev, @function
_ZN2v88internal23RedirectActiveFunctionsD2Ev:
.LFB22615:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22615:
	.size	_ZN2v88internal23RedirectActiveFunctionsD2Ev, .-_ZN2v88internal23RedirectActiveFunctionsD2Ev
	.weak	_ZN2v88internal23RedirectActiveFunctionsD1Ev
	.set	_ZN2v88internal23RedirectActiveFunctionsD1Ev,_ZN2v88internal23RedirectActiveFunctionsD2Ev
	.section	.text._ZN2v88internal23RedirectActiveFunctionsD0Ev,"axG",@progbits,_ZN2v88internal23RedirectActiveFunctionsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23RedirectActiveFunctionsD0Ev
	.type	_ZN2v88internal23RedirectActiveFunctionsD0Ev, @function
_ZN2v88internal23RedirectActiveFunctionsD0Ev:
.LFB22617:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22617:
	.size	_ZN2v88internal23RedirectActiveFunctionsD0Ev, .-_ZN2v88internal23RedirectActiveFunctionsD0Ev
	.section	.text._ZN2v88internal23RedirectActiveFunctions11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RedirectActiveFunctions11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE
	.type	_ZN2v88internal23RedirectActiveFunctions11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE, @function
_ZN2v88internal23RedirectActiveFunctions11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE:
.LFB18585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-1488(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$1456, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	cmpq	$0, -72(%rbp)
	je	.L4
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L4
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*152(%rax)
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	(%r12), %rax
	call	*8(%rax)
	cmpl	$12, %eax
	jne	.L23
	movq	23(%rbx), %rdx
	movq	8(%r14), %rax
	cmpq	%rdx, %rax
	jne	.L23
	cmpl	$1, 16(%r14)
	movq	31(%rax), %rdx
	jne	.L8
	movq	39(%rdx), %rsi
.L9:
	movq	%r12, %rdi
	call	_ZN2v88internal16InterpretedFrame18PatchBytecodeArrayENS0_13BytecodeArrayE@PLT
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L4:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	addq	$1456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	testb	$1, %dl
	jne	.L25
.L10:
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L26
.L14:
	movq	8(%r14), %rax
	movq	7(%rax), %rax
	movq	7(%rax), %rsi
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L25:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L10
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L10
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L10
	movq	31(%rdx), %rsi
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L26:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L14
	movq	8(%r14), %rax
	movq	7(%rax), %rsi
	jmp	.L9
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18585:
	.size	_ZN2v88internal23RedirectActiveFunctions11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE, .-_ZN2v88internal23RedirectActiveFunctions11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE
	.section	.text._ZN2v88internal14FrameInspectorC2EPNS0_13StandardFrameEiPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FrameInspectorC2EPNS0_13StandardFrameEiPNS0_7IsolateE
	.type	_ZN2v88internal14FrameInspectorC2EPNS0_13StandardFrameEiPNS0_7IsolateE, @function
_ZN2v88internal14FrameInspectorC2EPNS0_13StandardFrameEiPNS0_7IsolateE:
.LFB18562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$4294967295, %eax
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	movq	%rcx, 32(%rdi)
	movq	%rax, 72(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movups	%xmm0, 16(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal12FrameSummary3GetEPKNS0_13StandardFrameEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal12FrameSummary30EnsureSourcePositionsAvailableEv@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal12FrameSummary14is_constructorEv@PLT
	movq	%r12, %rdi
	movb	%al, 79(%rbx)
	call	_ZNK2v88internal12FrameSummary14SourcePositionEv@PLT
	movq	%r12, %rdi
	movl	%eax, 72(%rbx)
	call	_ZNK2v88internal12FrameSummary12FunctionNameEv@PLT
	movq	%r12, %rdi
	movq	%rax, 64(%rbx)
	call	_ZNK2v88internal12FrameSummary6scriptEv@PLT
	movq	%r12, %rdi
	movq	%rax, 40(%rbx)
	call	_ZNK2v88internal12FrameSummary8receiverEv@PLT
	movq	%rax, 48(%rbx)
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	jne	.L28
	movq	-88(%rbp), %rax
	movq	%rax, 56(%rbx)
.L28:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	cmpl	$20, %eax
	jbe	.L29
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
	movq	%rdi, %r9
.L30:
	movb	%r13b, 78(%rbx)
	movq	(%rdi), %rax
	movq	%r9, %rdi
	movq	%r8, -136(%rbp)
	call	*8(%rax)
	movq	(%rbx), %rdi
	cmpl	$4, %eax
	sete	76(%rbx)
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-136(%rbp), %r8
	cmpl	$12, %eax
	sete	77(%rbx)
	cmpb	$0, 76(%rbx)
	jne	.L54
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$8, %eax
	je	.L55
.L33:
	movq	%r12, %rdi
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movl	$1150992, %r13d
	movl	%eax, %ecx
	movq	(%rbx), %rdi
	shrq	%cl, %r13
	notq	%r13
	movq	%rdi, %r9
	andl	$1, %r13d
	jne	.L41
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	(%rbx), %r8
	xorl	%edi, %edi
	movq	%r8, %r9
	testq	%r8, %r8
	je	.L30
	movq	32(%r8), %rax
	movq	%r8, %rdi
	movq	(%rax), %rax
	cmpq	$38, -8(%rax)
	sete	%r13b
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal11Deoptimizer24DebuggerInspectableFrameEPNS0_15JavaScriptFrameEiPNS0_7IsolateE@PLT
	movq	16(%rbx), %r13
	movq	%rax, 16(%rbx)
	testq	%r13, %r13
	je	.L33
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	_ZdlPv@PLT
.L34:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	%r13, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L55:
	movq	(%rbx), %rdi
	leaq	-128(%rbp), %r13
	call	_ZNK2v88internal25WasmInterpreterEntryFrame10debug_infoEv@PLT
	leaq	-120(%rbp), %rsi
	movl	%r14d, %ecx
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	movq	(%rbx), %rax
	movq	32(%rax), %rdx
	call	_ZN2v88internal13WasmDebugInfo19GetInterpretedFrameEmi@PLT
	movq	-128(%rbp), %rax
	movq	24(%rbx), %rsi
	movq	$0, -128(%rbp)
	movq	%rax, 24(%rbx)
	testq	%rsi, %rsi
	je	.L33
	leaq	24(%rbx), %rdi
	call	_ZN2v88internal4wasm23InterpretedFrameDeleterclEPNS1_16InterpretedFrameE@PLT
	movq	-128(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L33
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm23InterpretedFrameDeleterclEPNS1_16InterpretedFrameE@PLT
	jmp	.L33
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18562:
	.size	_ZN2v88internal14FrameInspectorC2EPNS0_13StandardFrameEiPNS0_7IsolateE, .-_ZN2v88internal14FrameInspectorC2EPNS0_13StandardFrameEiPNS0_7IsolateE
	.globl	_ZN2v88internal14FrameInspectorC1EPNS0_13StandardFrameEiPNS0_7IsolateE
	.set	_ZN2v88internal14FrameInspectorC1EPNS0_13StandardFrameEiPNS0_7IsolateE,_ZN2v88internal14FrameInspectorC2EPNS0_13StandardFrameEiPNS0_7IsolateE
	.section	.text._ZN2v88internal14FrameInspectorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FrameInspectorD2Ev
	.type	_ZN2v88internal14FrameInspectorD2Ev, @function
_ZN2v88internal14FrameInspectorD2Ev:
.LFB18565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %rsi
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L58
	leaq	24(%rdi), %rdi
	call	_ZN2v88internal4wasm23InterpretedFrameDeleterclEPNS1_16InterpretedFrameE@PLT
.L58:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L57
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18565:
	.size	_ZN2v88internal14FrameInspectorD2Ev, .-_ZN2v88internal14FrameInspectorD2Ev
	.globl	_ZN2v88internal14FrameInspectorD1Ev
	.set	_ZN2v88internal14FrameInspectorD1Ev,_ZN2v88internal14FrameInspectorD2Ev
	.section	.text._ZN2v88internal14FrameInspector16javascript_frameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FrameInspector16javascript_frameEv
	.type	_ZN2v88internal14FrameInspector16javascript_frameEv, @function
_ZN2v88internal14FrameInspector16javascript_frameEv:
.LFB18567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18567:
	.size	_ZN2v88internal14FrameInspector16javascript_frameEv, .-_ZN2v88internal14FrameInspector16javascript_frameEv
	.section	.text._ZN2v88internal14FrameInspector18GetParametersCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FrameInspector18GetParametersCountEv
	.type	_ZN2v88internal14FrameInspector18GetParametersCountEv, @function
_ZN2v88internal14FrameInspector18GetParametersCountEv:
.LFB18568:
	.cfi_startproc
	endbr64
	cmpb	$0, 76(%rdi)
	je	.L75
	movq	16(%rdi), %rdx
	movq	24(%rdx), %rax
	subq	16(%rdx), %rax
	sarq	$3, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	movq	24(%rdi), %r8
	testq	%r8, %r8
	je	.L77
	movq	%r8, %rdi
	jmp	_ZNK2v88internal4wasm16InterpretedFrame17GetParameterCountEv@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*120(%rax)
	.cfi_endproc
.LFE18568:
	.size	_ZN2v88internal14FrameInspector18GetParametersCountEv, .-_ZN2v88internal14FrameInspector18GetParametersCountEv
	.section	.text._ZN2v88internal14FrameInspector12GetParameterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FrameInspector12GetParameterEi
	.type	_ZN2v88internal14FrameInspector12GetParameterEi, @function
_ZN2v88internal14FrameInspector12GetParameterEi:
.LFB18569:
	.cfi_startproc
	endbr64
	cmpb	$0, 76(%rdi)
	je	.L80
	movq	16(%rdi), %rax
	movslq	%esi, %rsi
	movq	16(%rax), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	32(%rdi), %rbx
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	call	*112(%rax)
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L82
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L88
.L84:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L84
	.cfi_endproc
.LFE18569:
	.size	_ZN2v88internal14FrameInspector12GetParameterEi, .-_ZN2v88internal14FrameInspector12GetParameterEi
	.section	.text._ZN2v88internal14FrameInspector13GetExpressionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FrameInspector13GetExpressionEi
	.type	_ZN2v88internal14FrameInspector13GetExpressionEi, @function
_ZN2v88internal14FrameInspector13GetExpressionEi:
.LFB18570:
	.cfi_startproc
	endbr64
	cmpb	$0, 76(%rdi)
	je	.L90
	movq	16(%rdi), %rax
	movslq	%esi, %rsi
	movq	40(%rax), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	32(%rdi), %rbx
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	call	*144(%rax)
	movq	41112(%rbx), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L92
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L98
.L94:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L94
	.cfi_endproc
.LFE18570:
	.size	_ZN2v88internal14FrameInspector13GetExpressionEi, .-_ZN2v88internal14FrameInspector13GetExpressionEi
	.section	.text._ZN2v88internal14FrameInspector10GetContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FrameInspector10GetContextEv
	.type	_ZN2v88internal14FrameInspector10GetContextEv, @function
_ZN2v88internal14FrameInspector10GetContextEv:
.LFB18571:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L100
	movq	8(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	32(%rdi), %rbx
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L102
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L108
.L104:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L104
	.cfi_endproc
.LFE18571:
	.size	_ZN2v88internal14FrameInspector10GetContextEv, .-_ZN2v88internal14FrameInspector10GetContextEv
	.section	.text._ZN2v88internal14FrameInspector6IsWasmEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FrameInspector6IsWasmEv
	.type	_ZN2v88internal14FrameInspector6IsWasmEv, @function
_ZN2v88internal14FrameInspector6IsWasmEv:
.LFB18572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*8(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$5, %eax
	sete	%dl
	cmpl	$8, %eax
	sete	%al
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE18572:
	.size	_ZN2v88internal14FrameInspector6IsWasmEv, .-_ZN2v88internal14FrameInspector6IsWasmEv
	.section	.text._ZN2v88internal14FrameInspector12IsJavaScriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FrameInspector12IsJavaScriptEv
	.type	_ZN2v88internal14FrameInspector12IsJavaScriptEv, @function
_ZN2v88internal14FrameInspector12IsJavaScriptEv:
.LFB18573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*8(%rax)
	movl	$1, %r8d
	movl	%eax, %edx
	andl	$-9, %edx
	cmpl	$4, %edx
	je	.L111
	leal	-15(%rax), %edx
	cmpl	$1, %edx
	setbe	%r8b
	cmpl	$20, %eax
	sete	%al
	orl	%eax, %r8d
.L111:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18573:
	.size	_ZN2v88internal14FrameInspector12IsJavaScriptEv, .-_ZN2v88internal14FrameInspector12IsJavaScriptEv
	.section	.text._ZN2v88internal14FrameInspector33ParameterIsShadowedByContextLocalENS0_6HandleINS0_9ScopeInfoEEENS2_INS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FrameInspector33ParameterIsShadowedByContextLocalENS0_6HandleINS0_9ScopeInfoEEENS2_INS0_6StringEEE
	.type	_ZN2v88internal14FrameInspector33ParameterIsShadowedByContextLocalENS0_6HandleINS0_9ScopeInfoEEENS2_INS0_6StringEEE, @function
_ZN2v88internal14FrameInspector33ParameterIsShadowedByContextLocalENS0_6HandleINS0_9ScopeInfoEEENS2_INS0_6StringEEE:
.LFB18574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rdx), %rsi
	movq	(%r8), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-10(%rbp), %rcx
	leaq	-11(%rbp), %rdx
	leaq	-9(%rbp), %r8
	call	_ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE@PLT
	cmpl	$-1, %eax
	setne	%al
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L120
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L120:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18574:
	.size	_ZN2v88internal14FrameInspector33ParameterIsShadowedByContextLocalENS0_6HandleINS0_9ScopeInfoEEENS2_INS0_6StringEEE, .-_ZN2v88internal14FrameInspector33ParameterIsShadowedByContextLocalENS0_6HandleINS0_9ScopeInfoEEENS2_INS0_6StringEEE
	.section	.text._ZN2v88internal23RedirectActiveFunctionsC2ENS0_18SharedFunctionInfoENS1_4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RedirectActiveFunctionsC2ENS0_18SharedFunctionInfoENS1_4ModeE
	.type	_ZN2v88internal23RedirectActiveFunctionsC2ENS0_18SharedFunctionInfoENS1_4ModeE, @function
_ZN2v88internal23RedirectActiveFunctionsC2ENS0_18SharedFunctionInfoENS1_4ModeE:
.LFB18583:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal23RedirectActiveFunctionsE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movl	%edx, 16(%rdi)
	ret
	.cfi_endproc
.LFE18583:
	.size	_ZN2v88internal23RedirectActiveFunctionsC2ENS0_18SharedFunctionInfoENS1_4ModeE, .-_ZN2v88internal23RedirectActiveFunctionsC2ENS0_18SharedFunctionInfoENS1_4ModeE
	.globl	_ZN2v88internal23RedirectActiveFunctionsC1ENS0_18SharedFunctionInfoENS1_4ModeE
	.set	_ZN2v88internal23RedirectActiveFunctionsC1ENS0_18SharedFunctionInfoENS1_4ModeE,_ZN2v88internal23RedirectActiveFunctionsC2ENS0_18SharedFunctionInfoENS1_4ModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal14FrameInspectorC2EPNS0_13StandardFrameEiPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal14FrameInspectorC2EPNS0_13StandardFrameEiPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal14FrameInspectorC2EPNS0_13StandardFrameEiPNS0_7IsolateE:
.LFB22707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22707:
	.size	_GLOBAL__sub_I__ZN2v88internal14FrameInspectorC2EPNS0_13StandardFrameEiPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal14FrameInspectorC2EPNS0_13StandardFrameEiPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal14FrameInspectorC2EPNS0_13StandardFrameEiPNS0_7IsolateE
	.weak	_ZTVN2v88internal23RedirectActiveFunctionsE
	.section	.data.rel.ro.local._ZTVN2v88internal23RedirectActiveFunctionsE,"awG",@progbits,_ZTVN2v88internal23RedirectActiveFunctionsE,comdat
	.align 8
	.type	_ZTVN2v88internal23RedirectActiveFunctionsE, @object
	.size	_ZTVN2v88internal23RedirectActiveFunctionsE, 40
_ZTVN2v88internal23RedirectActiveFunctionsE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23RedirectActiveFunctions11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE
	.quad	_ZN2v88internal23RedirectActiveFunctionsD1Ev
	.quad	_ZN2v88internal23RedirectActiveFunctionsD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
