	.file	"feedback-vector.cc"
	.text
	.section	.text._ZN2v88internalL22IsPropertyNameFeedbackENS0_11MaybeObjectE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22IsPropertyNameFeedbackENS0_11MaybeObjectE, @function
_ZN2v88internalL22IsPropertyNameFeedbackENS0_11MaybeObjectE:
.LFB19512:
	.cfi_startproc
	movq	%rdi, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L2
.L4:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	-1(%rdi), %rdx
	movl	$1, %eax
	cmpw	$63, 11(%rdx)
	ja	.L7
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	-1(%rdi), %rax
	cmpw	$64, 11(%rax)
	jne	.L4
	movq	%rdi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	%rdi, -33752(%rax)
	je	.L4
	cmpq	%rdi, -33824(%rax)
	je	.L4
	cmpq	%rdi, -33856(%rax)
	setne	%al
	ret
	.cfi_endproc
.LFE19512:
	.size	_ZN2v88internalL22IsPropertyNameFeedbackENS0_11MaybeObjectE, .-_ZN2v88internalL22IsPropertyNameFeedbackENS0_11MaybeObjectE
	.section	.rodata._ZNK2v88internal18FeedbackVectorSpec18HasTypeProfileSlotEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZNK2v88internal18FeedbackVectorSpec18HasTypeProfileSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18FeedbackVectorSpec18HasTypeProfileSlotEv
	.type	_ZNK2v88internal18FeedbackVectorSpec18HasTypeProfileSlotEv, @function
_ZNK2v88internal18FeedbackVectorSpec18HasTypeProfileSlotEv:
.LFB19511:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	subq	%rdx, %rax
	testl	%eax, %eax
	jle	.L11
	testq	%rax, %rax
	je	.L15
	cmpb	$18, (%rdx)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%eax, %eax
	ret
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE19511:
	.size	_ZNK2v88internal18FeedbackVectorSpec18HasTypeProfileSlotEv, .-_ZNK2v88internal18FeedbackVectorSpec18HasTypeProfileSlotEv
	.section	.rodata._ZN2v88internallsERSoNS0_16FeedbackSlotKindE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"ForIn"
.LC2:
	.string	"InstanceOf"
.LC3:
	.string	"CloneObject"
.LC4:
	.string	"Call"
.LC5:
	.string	"Invalid"
.LC6:
	.string	"LoadGlobalInsideTypeof"
.LC7:
	.string	"LoadGlobalNotInsideTypeof"
.LC8:
	.string	"LoadKeyed"
.LC9:
	.string	"HasKeyed"
.LC10:
	.string	"StoreNamedSloppy"
.LC11:
	.string	"StoreNamedStrict"
.LC12:
	.string	"StoreOwnNamed"
.LC13:
	.string	"StoreGlobalSloppy"
.LC14:
	.string	"StoreGlobalStrict"
.LC15:
	.string	"StoreKeyedSloppy"
.LC16:
	.string	"StoreKeyedStrict"
.LC17:
	.string	"StoreInArrayLiteral"
.LC18:
	.string	"BinaryOp"
.LC19:
	.string	"CompareOp"
.LC20:
	.string	"StoreDataPropertyInLiteral"
.LC21:
	.string	"Literal"
.LC22:
	.string	"TypeProfile"
.LC23:
	.string	"LoadProperty"
.LC24:
	.string	"unreachable code"
	.section	.text._ZN2v88internallsERSoNS0_16FeedbackSlotKindE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_16FeedbackSlotKindE
	.type	_ZN2v88internallsERSoNS0_16FeedbackSlotKindE, @function
_ZN2v88internallsERSoNS0_16FeedbackSlotKindE:
.LFB19513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpl	$22, %esi
	ja	.L17
	leaq	.L19(%rip), %rdx
	movl	%esi, %esi
	movq	%rdi, %r12
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internallsERSoNS0_16FeedbackSlotKindE,"a",@progbits
	.align 4
	.align 4
.L19:
	.long	.L41-.L19
	.long	.L40-.L19
	.long	.L39-.L19
	.long	.L38-.L19
	.long	.L37-.L19
	.long	.L36-.L19
	.long	.L35-.L19
	.long	.L42-.L19
	.long	.L33-.L19
	.long	.L32-.L19
	.long	.L31-.L19
	.long	.L30-.L19
	.long	.L29-.L19
	.long	.L28-.L19
	.long	.L27-.L19
	.long	.L26-.L19
	.long	.L25-.L19
	.long	.L24-.L19
	.long	.L23-.L19
	.long	.L22-.L19
	.long	.L21-.L19
	.long	.L20-.L19
	.long	.L18-.L19
	.section	.text._ZN2v88internallsERSoNS0_16FeedbackSlotKindE
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$10, %edx
	leaq	.LC2(%rip), %rsi
.L34:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	$5, %edx
	leaq	.LC1(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$7, %edx
	leaq	.LC21(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$11, %edx
	leaq	.LC22(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$26, %edx
	leaq	.LC20(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$9, %edx
	leaq	.LC19(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$8, %edx
	leaq	.LC18(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$19, %edx
	leaq	.LC17(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$16, %edx
	leaq	.LC16(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$13, %edx
	leaq	.LC12(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$16, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$17, %edx
	leaq	.LC14(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$8, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$9, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$22, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$25, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$12, %edx
	leaq	.LC23(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$4, %edx
	leaq	.LC4(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$16, %edx
	leaq	.LC15(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$16, %edx
	leaq	.LC10(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$17, %edx
	leaq	.LC13(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$7, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$11, %edx
	leaq	.LC3(%rip), %rsi
	jmp	.L34
.L17:
	leaq	.LC24(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19513:
	.size	_ZN2v88internallsERSoNS0_16FeedbackSlotKindE, .-_ZN2v88internallsERSoNS0_16FeedbackSlotKindE
	.section	.text._ZNK2v88internal16FeedbackMetadata7GetKindENS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16FeedbackMetadata7GetKindENS0_12FeedbackSlotE
	.type	_ZNK2v88internal16FeedbackMetadata7GetKindENS0_12FeedbackSlotE, @function
_ZNK2v88internal16FeedbackMetadata7GetKindENS0_12FeedbackSlotE:
.LFB19514:
	.cfi_startproc
	endbr64
	movslq	%esi, %rax
	movl	%esi, %edx
	movq	(%rdi), %rdi
	imulq	$715827883, %rax, %rax
	sarl	$31, %edx
	shrq	$32, %rax
	subl	%edx, %eax
	leal	16(,%rax,4), %edx
	leal	(%rax,%rax,2), %eax
	addl	%eax, %eax
	movslq	%edx, %rdx
	subl	%eax, %esi
	movl	-1(%rdx,%rdi), %eax
	leal	(%rsi,%rsi,4), %ecx
	shrl	%cl, %eax
	andl	$31, %eax
	ret
	.cfi_endproc
.LFE19514:
	.size	_ZNK2v88internal16FeedbackMetadata7GetKindENS0_12FeedbackSlotE, .-_ZNK2v88internal16FeedbackMetadata7GetKindENS0_12FeedbackSlotE
	.section	.text._ZN2v88internal16FeedbackMetadata7SetKindENS0_12FeedbackSlotENS0_16FeedbackSlotKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FeedbackMetadata7SetKindENS0_12FeedbackSlotENS0_16FeedbackSlotKindE
	.type	_ZN2v88internal16FeedbackMetadata7SetKindENS0_12FeedbackSlotENS0_16FeedbackSlotKindE, @function
_ZN2v88internal16FeedbackMetadata7SetKindENS0_12FeedbackSlotENS0_16FeedbackSlotKindE:
.LFB19515:
	.cfi_startproc
	endbr64
	movslq	%esi, %rcx
	movl	%esi, %eax
	movq	%rdi, %r8
	imulq	$715827883, %rcx, %rcx
	sarl	$31, %eax
	shrq	$32, %rcx
	subl	%eax, %ecx
	leal	(%rcx,%rcx,2), %eax
	leal	16(,%rcx,4), %edi
	addl	%eax, %eax
	movslq	%edi, %rdi
	addq	(%r8), %rdi
	subl	%eax, %esi
	movl	$31, %eax
	leal	(%rsi,%rsi,4), %ecx
	sall	%cl, %eax
	sall	%cl, %edx
	notl	%eax
	andl	-1(%rdi), %eax
	orl	%edx, %eax
	movl	%eax, -1(%rdi)
	ret
	.cfi_endproc
.LFE19515:
	.size	_ZN2v88internal16FeedbackMetadata7SetKindENS0_12FeedbackSlotENS0_16FeedbackSlotKindE, .-_ZN2v88internal16FeedbackMetadata7SetKindENS0_12FeedbackSlotENS0_16FeedbackSlotKindE
	.section	.text._ZN2v88internal16FeedbackMetadata3NewEPNS0_7IsolateEPKNS0_18FeedbackVectorSpecE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FeedbackMetadata3NewEPNS0_7IsolateEPKNS0_18FeedbackVectorSpecE
	.type	_ZN2v88internal16FeedbackMetadata3NewEPNS0_7IsolateEPKNS0_18FeedbackVectorSpecE, @function
_ZN2v88internal16FeedbackMetadata3NewEPNS0_7IsolateEPKNS0_18FeedbackVectorSpecE:
.LFB19516:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L59
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	32(%rsi), %edx
	movq	%rsi, %rbx
	movq	16(%rsi), %r12
	subq	8(%rsi), %r12
	movl	%edx, %eax
	orl	%r12d, %eax
	leaq	1040(%rdi), %rax
	jne	.L62
.L50:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movl	$1, %ecx
	movl	%r12d, %esi
	call	_ZN2v88internal7Factory19NewFeedbackMetadataEiiNS0_14AllocationTypeE@PLT
	testl	%r12d, %r12d
	jle	.L50
	leal	-1(%r12), %r9d
	xorl	%esi, %esi
	movl	$2863311531, %r8d
	movl	$31, %edi
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%rdx, %rsi
.L53:
	movq	8(%rbx), %r10
	movq	16(%rbx), %rdx
	subq	%r10, %rdx
	cmpq	%rsi, %rdx
	jbe	.L63
	movl	%esi, %edx
	movq	(%rax), %r11
	movzbl	(%r10,%rsi), %r10d
	imulq	%r8, %rdx
	shrq	$34, %rdx
	leal	16(,%rdx,4), %ecx
	leal	(%rdx,%rdx,2), %edx
	movslq	%ecx, %rcx
	addl	%edx, %edx
	leaq	-1(%rcx,%r11), %r11
	movl	%esi, %ecx
	subl	%edx, %ecx
	movl	%edi, %edx
	leal	(%rcx,%rcx,4), %ecx
	sall	%cl, %edx
	sall	%cl, %r10d
	notl	%edx
	andl	(%r11), %edx
	orl	%r10d, %edx
	movl	%edx, (%r11)
	leaq	1(%rsi), %rdx
	cmpq	%rsi, %r9
	jne	.L54
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	leaq	1040(%rdi), %rax
	ret
.L63:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE19516:
	.size	_ZN2v88internal16FeedbackMetadata3NewEPNS0_7IsolateEPKNS0_18FeedbackVectorSpecE, .-_ZN2v88internal16FeedbackMetadata3NewEPNS0_7IsolateEPKNS0_18FeedbackVectorSpecE
	.section	.text._ZNK2v88internal16FeedbackMetadata15SpecDiffersFromEPKNS0_18FeedbackVectorSpecE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16FeedbackMetadata15SpecDiffersFromEPKNS0_18FeedbackVectorSpecE
	.type	_ZNK2v88internal16FeedbackMetadata15SpecDiffersFromEPKNS0_18FeedbackVectorSpecE, @function
_ZNK2v88internal16FeedbackMetadata15SpecDiffersFromEPKNS0_18FeedbackVectorSpecE:
.LFB19517:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	movq	8(%rsi), %r10
	movq	16(%rsi), %rdx
	movl	7(%r8), %r9d
	subq	%r10, %rdx
	cmpl	%edx, %r9d
	jne	.L72
	testl	%r9d, %r9d
	jle	.L73
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	$1, %r8
	xorl	%esi, %esi
	movl	$2863311531, %r11d
	leaq	.L68(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	.p2align 4,,10
	.p2align 3
.L71:
	movl	%esi, %eax
	movl	%esi, %ecx
	imulq	%r11, %rax
	shrq	$34, %rax
	leal	16(,%rax,4), %ebx
	leal	(%rax,%rax,2), %eax
	addl	%eax, %eax
	movslq	%ebx, %rbx
	subl	%eax, %ecx
	movl	(%rbx,%r8), %eax
	leal	(%rcx,%rcx,4), %ecx
	shrl	%cl, %eax
	andl	$31, %eax
	cmpl	$23, %eax
	ja	.L74
	movl	%eax, %ecx
	movslq	(%rdi,%rcx,4), %rcx
	addq	%rdi, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZNK2v88internal16FeedbackMetadata15SpecDiffersFromEPKNS0_18FeedbackVectorSpecE,"a",@progbits
	.align 4
	.align 4
.L68:
	.long	.L67-.L68
	.long	.L69-.L68
	.long	.L69-.L68
	.long	.L69-.L68
	.long	.L69-.L68
	.long	.L69-.L68
	.long	.L69-.L68
	.long	.L69-.L68
	.long	.L69-.L68
	.long	.L69-.L68
	.long	.L69-.L68
	.long	.L69-.L68
	.long	.L69-.L68
	.long	.L69-.L68
	.long	.L69-.L68
	.long	.L74-.L68
	.long	.L74-.L68
	.long	.L69-.L68
	.long	.L74-.L68
	.long	.L74-.L68
	.long	.L74-.L68
	.long	.L74-.L68
	.long	.L69-.L68
	.long	.L67-.L68
	.section	.text._ZNK2v88internal16FeedbackMetadata15SpecDiffersFromEPKNS0_18FeedbackVectorSpecE
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$2, %ebx
.L66:
	movslq	%esi, %r12
	cmpq	%rdx, %r12
	jnb	.L81
	movzbl	(%r10,%r12), %ecx
	cmpl	%eax, %ecx
	jne	.L75
	addl	%ebx, %esi
	cmpl	%esi, %r9d
	jg	.L71
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movl	$1, %ebx
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L75:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	leaq	.LC24(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L73:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
.L81:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	%r12, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE19517:
	.size	_ZNK2v88internal16FeedbackMetadata15SpecDiffersFromEPKNS0_18FeedbackVectorSpecE, .-_ZNK2v88internal16FeedbackMetadata15SpecDiffersFromEPKNS0_18FeedbackVectorSpecE
	.section	.text._ZN2v88internal16FeedbackMetadata11Kind2StringENS0_16FeedbackSlotKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FeedbackMetadata11Kind2StringENS0_16FeedbackSlotKindE
	.type	_ZN2v88internal16FeedbackMetadata11Kind2StringENS0_16FeedbackSlotKindE, @function
_ZN2v88internal16FeedbackMetadata11Kind2StringENS0_16FeedbackSlotKindE:
.LFB19518:
	.cfi_startproc
	endbr64
	cmpl	$22, %edi
	ja	.L83
	leaq	.L85(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal16FeedbackMetadata11Kind2StringENS0_16FeedbackSlotKindE,"a",@progbits
	.align 4
	.align 4
.L85:
	.long	.L107-.L85
	.long	.L106-.L85
	.long	.L105-.L85
	.long	.L104-.L85
	.long	.L103-.L85
	.long	.L108-.L85
	.long	.L101-.L85
	.long	.L100-.L85
	.long	.L99-.L85
	.long	.L98-.L85
	.long	.L97-.L85
	.long	.L96-.L85
	.long	.L95-.L85
	.long	.L94-.L85
	.long	.L93-.L85
	.long	.L92-.L85
	.long	.L91-.L85
	.long	.L90-.L85
	.long	.L89-.L85
	.long	.L88-.L85
	.long	.L87-.L85
	.long	.L86-.L85
	.long	.L84-.L85
	.section	.text._ZN2v88internal16FeedbackMetadata11Kind2StringENS0_16FeedbackSlotKindE
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	.LC1(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	leaq	.LC21(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	.LC22(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	.LC20(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	.LC19(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	leaq	.LC18(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	.LC17(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	.LC16(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	.LC12(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	.LC14(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	leaq	.LC9(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	leaq	.LC8(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	leaq	.LC6(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	.LC7(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	.LC23(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	leaq	.LC4(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	leaq	.LC15(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	.LC13(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	leaq	.LC5(%rip), %rax
	ret
.L83:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC24(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19518:
	.size	_ZN2v88internal16FeedbackMetadata11Kind2StringENS0_16FeedbackSlotKindE, .-_ZN2v88internal16FeedbackMetadata11Kind2StringENS0_16FeedbackSlotKindE
	.section	.text._ZNK2v88internal16FeedbackMetadata18HasTypeProfileSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16FeedbackMetadata18HasTypeProfileSlotEv
	.type	_ZNK2v88internal16FeedbackMetadata18HasTypeProfileSlotEv, @function
_ZNK2v88internal16FeedbackMetadata18HasTypeProfileSlotEv:
.LFB19519:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movl	7(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L111
	movl	15(%rdx), %eax
	andl	$31, %eax
	cmpl	$18, %eax
	sete	%al
.L111:
	ret
	.cfi_endproc
.LFE19519:
	.size	_ZNK2v88internal16FeedbackMetadata18HasTypeProfileSlotEv, .-_ZNK2v88internal16FeedbackMetadata18HasTypeProfileSlotEv
	.section	.text._ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE
	.type	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE, @function
_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE:
.LFB19520:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	%esi, %edx
	sarl	$31, %edx
	movq	7(%rax), %rcx
	movslq	%esi, %rax
	imulq	$715827883, %rax, %rax
	movq	23(%rcx), %rdi
	shrq	$32, %rax
	subl	%edx, %eax
	leal	16(,%rax,4), %edx
	leal	(%rax,%rax,2), %eax
	addl	%eax, %eax
	movslq	%edx, %rdx
	subl	%eax, %esi
	movl	-1(%rdx,%rdi), %eax
	leal	(%rsi,%rsi,4), %ecx
	shrl	%cl, %eax
	andl	$31, %eax
	ret
	.cfi_endproc
.LFE19520:
	.size	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE, .-_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE
	.section	.text._ZNK2v88internal14FeedbackVector18GetTypeProfileSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14FeedbackVector18GetTypeProfileSlotEv
	.type	_ZNK2v88internal14FeedbackVector18GetTypeProfileSlotEv, @function
_ZNK2v88internal14FeedbackVector18GetTypeProfileSlotEv:
.LFB19521:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19521:
	.size	_ZNK2v88internal14FeedbackVector18GetTypeProfileSlotEv, .-_ZNK2v88internal14FeedbackVector18GetTypeProfileSlotEv
	.section	.text._ZN2v88internal24ClosureFeedbackCellArray3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ClosureFeedbackCellArray3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal24ClosureFeedbackCellArray3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal24ClosureFeedbackCellArray3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB19522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movl	11(%rax), %r12d
	movl	%r12d, %esi
	call	_ZN2v88internal7Factory27NewClosureFeedbackCellArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	testl	%r12d, %r12d
	jle	.L126
	leal	-1(%r12), %eax
	leaq	88(%rbx), %r15
	movl	$16, %r14d
	leaq	24(,%rax,8), %rax
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory17NewNoClosuresCellENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	0(%r13), %rdi
	movq	(%rax), %r12
	leaq	-1(%rdi,%r14), %rsi
	movq	%r12, (%rsi)
	testb	$1, %r12b
	je	.L123
	movq	%r12, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -64(%rbp)
	testl	$262144, %eax
	je	.L119
	movq	%r12, %rdx
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	8(%r8), %rax
.L119:
	testb	$24, %al
	je	.L123
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L123
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L123:
	addq	$8, %r14
	cmpq	-56(%rbp), %r14
	jne	.L122
.L126:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19522:
	.size	_ZN2v88internal24ClosureFeedbackCellArray3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal24ClosureFeedbackCellArray3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal14FeedbackVector29AddToVectorsForProfilingToolsEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FeedbackVector29AddToVectorsForProfilingToolsEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal14FeedbackVector29AddToVectorsForProfilingToolsEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal14FeedbackVector29AddToVectorsForProfilingToolsEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB19524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	7(%rax), %rbx
	movq	31(%rbx), %rax
	testb	$1, %al
	jne	.L143
.L133:
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L144
.L132:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L146
.L137:
	leaq	4720(%r13), %rsi
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal9ArrayList3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate35SetFeedbackVectorsForProfilingToolsENS0_6ObjectE@PLT
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L143:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L147
.L134:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	je	.L132
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L147:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L134
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L146:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L137
	jmp	.L132
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19524:
	.size	_ZN2v88internal14FeedbackVector29AddToVectorsForProfilingToolsEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal14FeedbackVector29AddToVectorsForProfilingToolsEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text.unlikely._ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE,"ax",@progbits
	.align 2
.LCOLDB25:
	.section	.text._ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE,"ax",@progbits
.LHOTB25:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE
	.type	_ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE, @function
_ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE:
.LFB19523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movl	7(%rax), %ebx
	call	_ZN2v88internal7Factory17NewFeedbackVectorENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_24ClosureFeedbackCellArrayEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.L149
	xorl	%esi, %esi
	movl	$2863311531, %r8d
	leaq	.L165(%rip), %r9
	leaq	.L152(%rip), %rdi
	leaq	.L156(%rip), %r10
	.p2align 4,,10
	.p2align 3
.L162:
	movl	%esi, %eax
	movq	(%r14), %rcx
	imulq	%r8, %rax
	movq	23(%rcx), %r11
	movl	%esi, %ecx
	shrq	$34, %rax
	leal	16(,%rax,4), %edx
	leal	(%rax,%rax,2), %eax
	addl	%eax, %eax
	movslq	%edx, %rdx
	subl	%eax, %ecx
	movl	-1(%rdx,%r11), %eax
	leal	(%rcx,%rcx,4), %ecx
	shrl	%cl, %eax
	andl	$31, %eax
	cmpl	$23, %eax
	ja	.L150
	movl	%eax, %r11d
	movslq	(%rdi,%r11,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE,"a",@progbits
	.align 4
	.align 4
.L152:
	.long	.L151-.L152
	.long	.L153-.L152
	.long	.L153-.L152
	.long	.L153-.L152
	.long	.L153-.L152
	.long	.L153-.L152
	.long	.L153-.L152
	.long	.L153-.L152
	.long	.L153-.L152
	.long	.L153-.L152
	.long	.L153-.L152
	.long	.L153-.L152
	.long	.L153-.L152
	.long	.L153-.L152
	.long	.L153-.L152
	.long	.L150-.L152
	.long	.L150-.L152
	.long	.L153-.L152
	.long	.L150-.L152
	.long	.L150-.L152
	.long	.L150-.L152
	.long	.L150-.L152
	.long	.L153-.L152
	.long	.L151-.L152
	.section	.text._ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE
	.p2align 4,,10
	.p2align 3
.L153:
	movq	3840(%r13), %rcx
	cmpl	$22, %eax
	ja	.L154
	movslq	(%r10,%r11,4), %rax
	addq	%r10, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE
	.align 4
	.align 4
.L156:
	.long	.L154-.L156
	.long	.L154-.L156
	.long	.L168-.L156
	.long	.L168-.L156
	.long	.L159-.L156
	.long	.L168-.L156
	.long	.L154-.L156
	.long	.L154-.L156
	.long	.L168-.L156
	.long	.L168-.L156
	.long	.L154-.L156
	.long	.L168-.L156
	.long	.L168-.L156
	.long	.L168-.L156
	.long	.L168-.L156
	.long	.L166-.L156
	.long	.L166-.L156
	.long	.L168-.L156
	.long	.L168-.L156
	.long	.L166-.L156
	.long	.L166-.L156
	.long	.L168-.L156
	.long	.L168-.L156
	.section	.text._ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$2, %eax
.L155:
	movq	(%r12), %r11
	leal	48(,%rsi,8), %edx
	movslq	%edx, %rdx
	movq	%rcx, -1(%rdx,%r11)
.L160:
	cmpl	$1, %eax
	je	.L161
.L178:
	movq	(%r12), %rdx
	leal	56(,%rsi,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	movl	$2, %eax
.L161:
	addl	%eax, %esi
	cmpl	%ebx, %esi
	jl	.L162
.L149:
	movl	41832(%r13), %eax
	testl	%eax, %eax
	jne	.L163
	cmpl	$1, 41836(%r13)
	je	.L163
.L174:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	subl	$15, %eax
	movq	3840(%r13), %rcx
	cmpl	$8, %eax
	ja	.L169
	movslq	(%r9,%rax,4), %rax
	addq	%r9, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE
	.align 4
	.align 4
.L165:
	.long	.L170-.L165
	.long	.L170-.L165
	.long	.L171-.L165
	.long	.L171-.L165
	.long	.L170-.L165
	.long	.L170-.L165
	.long	.L171-.L165
	.long	.L171-.L165
	.long	.L151-.L165
	.section	.text._ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE
	.p2align 4,,10
	.p2align 3
.L171:
	movl	$1, %eax
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L170:
	movl	$1, %eax
.L158:
	movq	(%r12), %r11
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r15
	leal	48(,%rsi,8), %edx
	movslq	%edx, %rdx
	movq	%r15, -1(%rdx,%r11)
	jmp	.L160
.L166:
	movl	$2, %eax
	jmp	.L158
.L154:
	movq	(%r12), %rdx
	leal	48(,%rsi,8), %eax
	cltq
	movq	$3, -1(%rax,%rdx)
	jmp	.L178
.L159:
	movq	(%r12), %rdx
	leal	48(,%rsi,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	xorl	%ecx, %ecx
	jmp	.L178
.L163:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14FeedbackVector29AddToVectorsForProfilingToolsEPNS0_7IsolateENS0_6HandleIS1_EE
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L151:
	leaq	.LC24(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE
	.cfi_startproc
	.type	_ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE.cold, @function
_ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE.cold:
.LFSB19523:
.L169:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1, %eax
	jmp	.L161
	.cfi_endproc
.LFE19523:
	.section	.text._ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE
	.size	_ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE, .-_ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE
	.section	.text.unlikely._ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE
	.size	_ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE.cold, .-_ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE.cold
.LCOLDE25:
	.section	.text._ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE
.LHOTE25:
	.section	.text._ZN2v88internal14FeedbackVector16SetOptimizedCodeENS0_6HandleIS1_EENS2_INS0_4CodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FeedbackVector16SetOptimizedCodeENS0_6HandleIS1_EENS2_INS0_4CodeEEE
	.type	_ZN2v88internal14FeedbackVector16SetOptimizedCodeENS0_6HandleIS1_EENS2_INS0_4CodeEEE, @function
_ZN2v88internal14FeedbackVector16SetOptimizedCodeENS0_6HandleIS1_EENS2_INS0_4CodeEEE:
.LFB19525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rdx
	movq	(%rdi), %r12
	movq	%rdx, %rax
	orq	$2, %rax
	movq	%rax, 15(%r12)
	testb	$1, %al
	je	.L180
	cmpl	$3, %eax
	je	.L180
	movq	%rdx, %rbx
	movq	%rdx, %r14
	leaq	15(%r12), %r13
	andq	$-262144, %rbx
	andq	$-3, %r14
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L192
.L182:
	testb	$24, %al
	je	.L180
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L193
.L180:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L193:
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE19525:
	.size	_ZN2v88internal14FeedbackVector16SetOptimizedCodeENS0_6HandleIS1_EENS2_INS0_4CodeEEE, .-_ZN2v88internal14FeedbackVector16SetOptimizedCodeENS0_6HandleIS1_EENS2_INS0_4CodeEEE
	.section	.text._ZN2v88internal14FeedbackVector18ClearOptimizedCodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FeedbackVector18ClearOptimizedCodeEv
	.type	_ZN2v88internal14FeedbackVector18ClearOptimizedCodeEv, @function
_ZN2v88internal14FeedbackVector18ClearOptimizedCodeEv:
.LFB19526:
	.cfi_startproc
	endbr64
	movabsq	$4294967296, %rdx
	movq	(%rdi), %rax
	movq	%rdx, 15(%rax)
	ret
	.cfi_endproc
.LFE19526:
	.size	_ZN2v88internal14FeedbackVector18ClearOptimizedCodeEv, .-_ZN2v88internal14FeedbackVector18ClearOptimizedCodeEv
	.section	.text._ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv
	.type	_ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv, @function
_ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv:
.LFB24118:
	.cfi_startproc
	endbr64
	movabsq	$4294967296, %rdx
	movq	(%rdi), %rax
	movq	%rdx, 15(%rax)
	ret
	.cfi_endproc
.LFE24118:
	.size	_ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv, .-_ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv
	.section	.text._ZN2v88internal14FeedbackVector21SetOptimizationMarkerENS0_18OptimizationMarkerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FeedbackVector21SetOptimizationMarkerENS0_18OptimizationMarkerE
	.type	_ZN2v88internal14FeedbackVector21SetOptimizationMarkerENS0_18OptimizationMarkerE, @function
_ZN2v88internal14FeedbackVector21SetOptimizationMarkerENS0_18OptimizationMarkerE:
.LFB19528:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	salq	$32, %rsi
	movq	%rsi, 15(%rax)
	ret
	.cfi_endproc
.LFE19528:
	.size	_ZN2v88internal14FeedbackVector21SetOptimizationMarkerENS0_18OptimizationMarkerE, .-_ZN2v88internal14FeedbackVector21SetOptimizationMarkerENS0_18OptimizationMarkerE
	.section	.rodata._ZN2v88internal14FeedbackVector41EvictOptimizedCodeMarkedForDeoptimizationENS0_18SharedFunctionInfoEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"[evicting optimizing code marked for deoptimization (%s) for "
	.section	.rodata._ZN2v88internal14FeedbackVector41EvictOptimizedCodeMarkedForDeoptimizationENS0_18SharedFunctionInfoEPKc.str1.1,"aMS",@progbits,1
.LC27:
	.string	"]\n"
	.section	.text._ZN2v88internal14FeedbackVector41EvictOptimizedCodeMarkedForDeoptimizationENS0_18SharedFunctionInfoEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FeedbackVector41EvictOptimizedCodeMarkedForDeoptimizationENS0_18SharedFunctionInfoEPKc
	.type	_ZN2v88internal14FeedbackVector41EvictOptimizedCodeMarkedForDeoptimizationENS0_18SharedFunctionInfoEPKc, @function
_ZN2v88internal14FeedbackVector41EvictOptimizedCodeMarkedForDeoptimizationENS0_18SharedFunctionInfoEPKc:
.LFB19529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rcx
	movq	%rsi, -24(%rbp)
	movq	15(%rcx), %rax
	testb	$1, %al
	je	.L197
	cmpl	$3, %eax
	je	.L207
	andq	$-3, %rax
	movq	%rax, %rbx
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L197
	cmpb	$0, _ZN2v88internal16FLAG_trace_deoptE(%rip)
	movq	%rdi, %r12
	jne	.L208
.L201:
	movq	31(%rbx), %rax
	movl	15(%rax), %eax
	testb	$4, %al
	je	.L209
.L202:
	movabsq	$4294967296, %rdx
	movq	(%r12), %rax
	movq	%rdx, 15(%rax)
.L197:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movq	31(%rbx), %rax
	movl	15(%rax), %eax
	movq	31(%rbx), %rdx
	orl	$4, %eax
	movl	%eax, 15(%rdx)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%rdx, %rsi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	-24(%rbp), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC27(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L207:
	movabsq	$4294967296, %rax
	movq	%rax, 15(%rcx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19529:
	.size	_ZN2v88internal14FeedbackVector41EvictOptimizedCodeMarkedForDeoptimizationENS0_18SharedFunctionInfoEPKc, .-_ZN2v88internal14FeedbackVector41EvictOptimizedCodeMarkedForDeoptimizationENS0_18SharedFunctionInfoEPKc
	.section	.text._ZN2v88internal14FeedbackVector19AssertNoLegacyTypesENS0_11MaybeObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FeedbackVector19AssertNoLegacyTypesENS0_11MaybeObjectE
	.type	_ZN2v88internal14FeedbackVector19AssertNoLegacyTypesENS0_11MaybeObjectE, @function
_ZN2v88internal14FeedbackVector19AssertNoLegacyTypesENS0_11MaybeObjectE:
.LFB19531:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE19531:
	.size	_ZN2v88internal14FeedbackVector19AssertNoLegacyTypesENS0_11MaybeObjectE, .-_ZN2v88internal14FeedbackVector19AssertNoLegacyTypesENS0_11MaybeObjectE
	.section	.text._ZN2v88internal13FeedbackNexus17EnsureArrayOfSizeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus17EnsureArrayOfSizeEi
	.type	_ZN2v88internal13FeedbackNexus17EnsureArrayOfSizeEi, @function
_ZN2v88internal13FeedbackNexus17EnsureArrayOfSizeEi:
.LFB19532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L236
	movq	(%rax), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
.L213:
	movl	16(%rbx), %eax
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %r12
	movq	%r12, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L214
.L217:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17NewWeakFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdx
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rdx, %rdx
	je	.L237
	movq	(%rdx), %r13
.L219:
	movl	16(%rbx), %edx
	leal	48(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r13,%rdx), %r14
	movq	%rax, (%r14)
	testb	$1, %al
	je	.L227
	cmpl	$3, %eax
	je	.L227
	movq	%rax, %r15
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r15
	testl	$262144, %eax
	jne	.L238
.L221:
	testb	$24, %al
	je	.L227
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L239
	.p2align 4,,10
	.p2align 3
.L227:
	movq	%r12, %rax
.L223:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movq	8(%rdi), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L237:
	movq	8(%rbx), %r13
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L214:
	movq	-1(%r12), %rax
	movzwl	11(%rax), %eax
	subw	$148, %ax
	cmpw	$1, %ax
	ja	.L217
	cmpl	%esi, 11(%r12)
	jne	.L217
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L224
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L238:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L239:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L224:
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L240
.L226:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%r12, (%rax)
	jmp	.L223
.L240:
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rdi
	jmp	.L226
	.cfi_endproc
.LFE19532:
	.size	_ZN2v88internal13FeedbackNexus17EnsureArrayOfSizeEi, .-_ZN2v88internal13FeedbackNexus17EnsureArrayOfSizeEi
	.section	.text._ZN2v88internal13FeedbackNexus22EnsureExtraArrayOfSizeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus22EnsureExtraArrayOfSizeEi
	.type	_ZN2v88internal13FeedbackNexus22EnsureExtraArrayOfSizeEi, @function
_ZN2v88internal13FeedbackNexus22EnsureExtraArrayOfSizeEi:
.LFB19533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L266
	movq	(%rax), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
.L243:
	movl	16(%rbx), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %r12
	movq	%r12, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L244
.L247:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17NewWeakFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdx
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rdx, %rdx
	je	.L267
	movq	(%rdx), %r13
.L249:
	movl	16(%rbx), %edx
	leal	56(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r13,%rdx), %r14
	movq	%rax, (%r14)
	testb	$1, %al
	je	.L257
	cmpl	$3, %eax
	je	.L257
	movq	%rax, %r15
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r15
	testl	$262144, %eax
	jne	.L268
.L251:
	testb	$24, %al
	je	.L257
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L269
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%r12, %rax
.L253:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	movq	8(%rdi), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L267:
	movq	8(%rbx), %r13
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L244:
	movq	-1(%r12), %rax
	movzwl	11(%rax), %eax
	subw	$148, %ax
	cmpw	$1, %ax
	ja	.L247
	cmpl	%esi, 11(%r12)
	jne	.L247
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L254
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L254:
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L270
.L256:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%r12, (%rax)
	jmp	.L253
.L270:
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rdi
	jmp	.L256
	.cfi_endproc
.LFE19533:
	.size	_ZN2v88internal13FeedbackNexus22EnsureExtraArrayOfSizeEi, .-_ZN2v88internal13FeedbackNexus22EnsureExtraArrayOfSizeEi
	.section	.text._ZN2v88internal13FeedbackNexus22ConfigureUninitializedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus22ConfigureUninitializedEv
	.type	_ZN2v88internal13FeedbackNexus22ConfigureUninitializedEv, @function
_ZN2v88internal13FeedbackNexus22ConfigureUninitializedEv:
.LFB19534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdx
	movl	20(%rdi), %eax
	testq	%rdx, %rdx
	je	.L319
	movq	(%rdx), %r13
	movq	%r13, %r14
	andq	$-262144, %r14
	movq	24(%r14), %rsi
	leaq	-37592(%rsi), %r12
	cmpl	$22, %eax
	ja	.L273
	leaq	.L279(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal13FeedbackNexus22ConfigureUninitializedEv,"a",@progbits
	.align 4
	.align 4
.L279:
	.long	.L273-.L279
	.long	.L278-.L279
	.long	.L277-.L279
	.long	.L277-.L279
	.long	.L274-.L279
	.long	.L277-.L279
	.long	.L278-.L279
	.long	.L278-.L279
	.long	.L277-.L279
	.long	.L277-.L279
	.long	.L278-.L279
	.long	.L277-.L279
	.long	.L277-.L279
	.long	.L277-.L279
	.long	.L277-.L279
	.long	.L273-.L279
	.long	.L273-.L279
	.long	.L277-.L279
	.long	.L273-.L279
	.long	.L273-.L279
	.long	.L273-.L279
	.long	.L276-.L279
	.long	.L274-.L279
	.section	.text._ZN2v88internal13FeedbackNexus22ConfigureUninitializedEv
	.p2align 4,,10
	.p2align 3
.L277:
	movl	16(%rbx), %edx
	movq	3840(%r12), %rax
	leal	48(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r13,%rdx), %rsi
	movq	%rax, (%rsi)
	testb	$1, %al
	je	.L296
	cmpl	$3, %eax
	je	.L296
	movq	%rax, %rdx
	andq	$-262144, %rax
	movq	%rax, %r15
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	jne	.L320
.L292:
	testb	$24, %al
	je	.L296
	testb	$24, 8(%r14)
	je	.L321
	.p2align 4,,10
	.p2align 3
.L296:
	movq	(%rbx), %rax
	movq	3840(%r12), %rcx
	testq	%rax, %rax
	je	.L318
.L294:
	movq	(%rax), %rdx
.L295:
	movl	16(%rbx), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rdx,%rax)
.L271:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	movl	16(%rbx), %eax
	leal	48(,%rax,8), %eax
	cltq
	movq	$3, -1(%r13,%rax)
	movq	(%rbx), %rax
	movq	3840(%r12), %rcx
	testq	%rax, %rax
	jne	.L294
.L318:
	movq	8(%rbx), %rdx
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L274:
	movl	16(%rbx), %edx
	movq	3840(%r12), %rax
	leal	48(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r13,%rdx), %r15
	movq	%rax, (%r15)
	testb	$1, %al
	je	.L297
	cmpl	$3, %eax
	je	.L297
	movq	%rax, %rdx
	andq	$-262144, %rax
	movq	%rax, %r12
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	jne	.L322
.L284:
	testb	$24, %al
	je	.L297
	testb	$24, 8(%r14)
	je	.L323
	.p2align 4,,10
	.p2align 3
.L297:
	movq	(%rbx), %rax
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rcx
	testq	%rax, %rax
	jne	.L294
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L276:
	movl	16(%rbx), %edx
	movq	3840(%r12), %rax
	leal	48(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r13,%rdx), %r12
	movq	%rax, (%r12)
	testb	$1, %al
	je	.L271
	cmpl	$3, %eax
	je	.L271
	movq	%rax, %r15
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r15
	testl	$262144, %eax
	jne	.L324
.L289:
	testb	$24, %al
	je	.L271
	testb	$24, 8(%r14)
	jne	.L271
	addq	$24, %rsp
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	movq	8(%rdi), %r13
	movq	%r13, %r14
	andq	$-262144, %r14
	movq	24(%r14), %rcx
	leaq	-37592(%rcx), %r12
	cmpl	$22, %eax
	ja	.L273
	leaq	.L275(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal13FeedbackNexus22ConfigureUninitializedEv
	.align 4
	.align 4
.L275:
	.long	.L273-.L275
	.long	.L278-.L275
	.long	.L277-.L275
	.long	.L277-.L275
	.long	.L274-.L275
	.long	.L277-.L275
	.long	.L278-.L275
	.long	.L278-.L275
	.long	.L277-.L275
	.long	.L277-.L275
	.long	.L278-.L275
	.long	.L277-.L275
	.long	.L277-.L275
	.long	.L277-.L275
	.long	.L277-.L275
	.long	.L273-.L275
	.long	.L273-.L275
	.long	.L277-.L275
	.long	.L273-.L275
	.long	.L273-.L275
	.long	.L273-.L275
	.long	.L276-.L275
	.long	.L274-.L275
	.section	.text._ZN2v88internal13FeedbackNexus22ConfigureUninitializedEv
.L273:
	leaq	.LC24(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L320:
	movq	%r13, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-56(%rbp), %rdx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L321:
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L297
	.cfi_endproc
.LFE19534:
	.size	_ZN2v88internal13FeedbackNexus22ConfigureUninitializedEv, .-_ZN2v88internal13FeedbackNexus22ConfigureUninitializedEv
	.section	.text._ZN2v88internal13FeedbackNexus23ConfigurePremonomorphicENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus23ConfigurePremonomorphicENS0_6HandleINS0_3MapEEE
	.type	_ZN2v88internal13FeedbackNexus23ConfigurePremonomorphicENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal13FeedbackNexus23ConfigurePremonomorphicENS0_6HandleINS0_3MapEEE:
.LFB19536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L351
	movq	(%rax), %r14
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rax
	movq	-33824(%rax), %rax
.L327:
	movl	16(%rbx), %edx
	leal	48(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r14,%rdx), %r15
	movq	%rax, (%r15)
	testb	$1, %al
	je	.L337
	cmpl	$3, %eax
	je	.L337
	movq	%rax, %rdx
	andq	$-262144, %rax
	movq	%rax, %r12
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	jne	.L352
.L329:
	testb	$24, %al
	je	.L337
	testb	$24, 8(%rcx)
	je	.L353
	.p2align 4,,10
	.p2align 3
.L337:
	movq	0(%r13), %rcx
	movq	(%rbx), %rax
	movq	%rcx, %rdx
	orq	$2, %rdx
	testq	%rax, %rax
	je	.L354
	movq	(%rax), %r12
.L332:
	movl	16(%rbx), %eax
	leal	56(,%rax,8), %eax
	cltq
	leaq	-1(%r12,%rax), %r13
	movq	%rdx, 0(%r13)
	testb	$1, %dl
	je	.L325
	cmpl	$3, %edx
	je	.L325
	movq	%rcx, %rbx
	movq	%rcx, %r14
	andq	$-262144, %rbx
	andq	$-3, %r14
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L355
.L334:
	testb	$24, %al
	je	.L325
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L356
.L325:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	movq	8(%rbx), %r12
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L351:
	movq	8(%rdi), %r14
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rax
	movq	-33824(%rax), %rax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L355:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L356:
	addq	$24, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L337
	.cfi_endproc
.LFE19536:
	.size	_ZN2v88internal13FeedbackNexus23ConfigurePremonomorphicENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal13FeedbackNexus23ConfigurePremonomorphicENS0_6HandleINS0_3MapEEE
	.section	.text._ZN2v88internal13FeedbackNexus20ConfigureMegamorphicEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicEv
	.type	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicEv, @function
_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicEv:
.LFB19537:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L366
	movq	(%rdx), %rcx
	movq	%rcx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-33856(%rax), %rsi
.L359:
	movl	16(%rdi), %r8d
	leal	48(,%r8,8), %eax
	cltq
	movq	-1(%rcx,%rax), %rcx
	xorl	%eax, %eax
	cmpq	%rsi, %rcx
	jne	.L367
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	testq	%rdx, %rdx
	je	.L368
	movq	(%rdx), %rdx
.L362:
	leal	48(,%r8,8), %eax
	cltq
	movq	%rsi, -1(%rdx,%rax)
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L369
	movq	(%rax), %rdx
.L364:
	movl	16(%rdi), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	$3, -1(%rdx,%rax)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	movq	8(%rdi), %rcx
	movq	%rcx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-33856(%rax), %rsi
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L368:
	movq	8(%rdi), %rdx
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L369:
	movq	8(%rdi), %rdx
	jmp	.L364
	.cfi_endproc
.LFE19537:
	.size	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicEv, .-_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicEv
	.section	.text._ZN2v88internal13FeedbackNexus20ConfigureMegamorphicENS0_11IcCheckTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicENS0_11IcCheckTypeE
	.type	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicENS0_11IcCheckTypeE, @function
_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicENS0_11IcCheckTypeE:
.LFB19538:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L385
	movq	(%rax), %r8
	movq	%r8, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-33856(%rdx), %r9
.L372:
	movl	16(%rdi), %edx
	salq	$32, %rsi
	leal	48(,%rdx,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%r8,%rcx), %rcx
	cmpq	%r9, %rcx
	jne	.L373
	testq	%rax, %rax
	je	.L386
	movq	(%rax), %r8
.L379:
	leal	56(,%rdx,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%r8,%rcx), %rcx
	xorl	%r8d, %r8d
	cmpq	%rsi, %rcx
	je	.L370
	testq	%rax, %rax
	je	.L387
.L381:
	movq	(%rax), %rcx
.L382:
	leal	56(,%rdx,8), %eax
	movl	$1, %r8d
	cltq
	movq	%rsi, -1(%rcx,%rax)
.L370:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	testq	%rax, %rax
	je	.L388
	movq	(%rax), %rcx
.L377:
	leal	48(,%rdx,8), %eax
	cltq
	movq	%r9, -1(%rcx,%rax)
	movq	(%rdi), %rax
	movl	16(%rdi), %edx
	testq	%rax, %rax
	jne	.L381
.L387:
	movq	8(%rdi), %rcx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L385:
	movq	8(%rdi), %r8
	movq	%r8, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-33856(%rdx), %r9
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L386:
	movq	8(%rdi), %r8
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L388:
	movq	8(%rdi), %rcx
	jmp	.L377
	.cfi_endproc
.LFE19538:
	.size	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicENS0_11IcCheckTypeE, .-_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicENS0_11IcCheckTypeE
	.section	.text._ZN2v88internal13FeedbackNexus25ConfigurePropertyCellModeENS0_6HandleINS0_12PropertyCellEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus25ConfigurePropertyCellModeENS0_6HandleINS0_12PropertyCellEEE
	.type	_ZN2v88internal13FeedbackNexus25ConfigurePropertyCellModeENS0_6HandleINS0_12PropertyCellEEE, @function
_ZN2v88internal13FeedbackNexus25ConfigurePropertyCellModeENS0_6HandleINS0_12PropertyCellEEE:
.LFB19550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rcx
	movq	(%rdi), %rax
	movq	%rcx, %rdx
	orq	$2, %rdx
	testq	%rax, %rax
	je	.L405
	movq	(%rax), %r14
	movq	%r14, %r8
	andq	$-262144, %r8
	movq	24(%r8), %rax
	leaq	-37592(%rax), %r12
.L391:
	movl	16(%rbx), %eax
	leal	48(,%rax,8), %eax
	cltq
	leaq	-1(%r14,%rax), %r15
	movq	%rdx, (%r15)
	testb	$1, %dl
	je	.L397
	cmpl	$3, %edx
	je	.L397
	movq	%rcx, %rdx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	andq	$-3, %rdx
	movq	%rcx, %r13
	testl	$262144, %eax
	jne	.L406
.L393:
	testb	$24, %al
	je	.L397
	testb	$24, 8(%r8)
	je	.L407
	.p2align 4,,10
	.p2align 3
.L397:
	movq	(%rbx), %rax
	movq	3840(%r12), %rcx
	testq	%rax, %rax
	je	.L408
	movq	(%rax), %rdx
.L396:
	movl	16(%rbx), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rdx,%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r14, %r8
	andq	$-262144, %r8
	movq	24(%r8), %rax
	leaq	-37592(%rax), %r12
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L408:
	movq	8(%rbx), %rdx
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rax
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rdx
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L397
	.cfi_endproc
.LFE19550:
	.size	_ZN2v88internal13FeedbackNexus25ConfigurePropertyCellModeENS0_6HandleINS0_12PropertyCellEEE, .-_ZN2v88internal13FeedbackNexus25ConfigurePropertyCellModeENS0_6HandleINS0_12PropertyCellEEE
	.section	.text._ZN2v88internal13FeedbackNexus23ConfigureLexicalVarModeEiib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus23ConfigureLexicalVarModeEiib
	.type	_ZN2v88internal13FeedbackNexus23ConfigureLexicalVarModeEiib, @function
_ZN2v88internal13FeedbackNexus23ConfigureLexicalVarModeEiib:
.LFB19551:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	$-4096, %esi
	jne	.L409
	testl	$-262144, %edx
	je	.L417
.L409:
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	sall	$30, %ecx
	sall	$12, %edx
	movq	(%rdi), %rax
	orl	%edx, %ecx
	orl	%esi, %ecx
	salq	$32, %rcx
	testq	%rax, %rax
	je	.L418
	movq	(%rax), %rdx
.L412:
	movl	16(%rdi), %eax
	leal	48(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rdx,%rax)
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L419
	movq	(%rax), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-33752(%rax), %rcx
.L414:
	movl	16(%rdi), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rdx,%rax)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	movq	8(%rdi), %rdx
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L419:
	movq	8(%rdi), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-33752(%rax), %rcx
	jmp	.L414
	.cfi_endproc
.LFE19551:
	.size	_ZN2v88internal13FeedbackNexus23ConfigureLexicalVarModeEiib, .-_ZN2v88internal13FeedbackNexus23ConfigureLexicalVarModeEiib
	.section	.rodata._ZN2v88internal13FeedbackNexus20ConfigureHandlerModeERKNS0_17MaybeObjectHandleE.str1.1,"aMS",@progbits,1
.LC28:
	.string	"(location_) != nullptr"
.LC29:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal13FeedbackNexus20ConfigureHandlerModeERKNS0_17MaybeObjectHandleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus20ConfigureHandlerModeERKNS0_17MaybeObjectHandleE
	.type	_ZN2v88internal13FeedbackNexus20ConfigureHandlerModeERKNS0_17MaybeObjectHandleE, @function
_ZN2v88internal13FeedbackNexus20ConfigureHandlerModeERKNS0_17MaybeObjectHandleE:
.LFB19558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L443
	movq	(%rax), %rdx
.L422:
	movl	16(%rdi), %eax
	leal	48(,%rax,8), %eax
	cltq
	movq	$3, -1(%rdx,%rax)
	movl	(%rsi), %eax
	testl	%eax, %eax
	movq	8(%rsi), %rax
	jne	.L423
	testq	%rax, %rax
	je	.L426
	movq	(%rax), %rax
	movq	(%rdi), %rdx
	orq	$2, %rax
	testq	%rdx, %rdx
	jne	.L427
.L444:
	movq	8(%rdi), %r12
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L423:
	testq	%rax, %rax
	je	.L426
	movq	(%rdi), %rdx
	movq	(%rax), %rax
	testq	%rdx, %rdx
	je	.L444
.L427:
	movq	(%rdx), %r12
.L428:
	movl	16(%rdi), %edx
	leal	56(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r12,%rdx), %r13
	movq	%rax, 0(%r13)
	testb	$1, %al
	je	.L420
	cmpl	$3, %eax
	je	.L420
	movq	%rax, %r14
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r14
	testl	$262144, %eax
	jne	.L445
	testb	$24, %al
	je	.L420
.L446:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L420
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	movq	8(%rdi), %rdx
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L446
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L426:
	leaq	.LC28(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19558:
	.size	_ZN2v88internal13FeedbackNexus20ConfigureHandlerModeERKNS0_17MaybeObjectHandleE, .-_ZN2v88internal13FeedbackNexus20ConfigureHandlerModeERKNS0_17MaybeObjectHandleE
	.section	.rodata._ZN2v88internal13FeedbackNexus12GetCallCountEv.str1.1,"aMS",@progbits,1
.LC30:
	.string	"call_count.IsSmi()"
	.section	.text._ZN2v88internal13FeedbackNexus12GetCallCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus12GetCallCountEv
	.type	_ZN2v88internal13FeedbackNexus12GetCallCountEv, @function
_ZN2v88internal13FeedbackNexus12GetCallCountEv:
.LFB19561:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L454
	movq	(%rax), %rdx
.L449:
	movl	16(%rdi), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	testb	$1, %al
	jne	.L455
	shrq	$33, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	movq	8(%rdi), %rdx
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L455:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC30(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19561:
	.size	_ZN2v88internal13FeedbackNexus12GetCallCountEv, .-_ZN2v88internal13FeedbackNexus12GetCallCountEv
	.section	.text._ZN2v88internal13FeedbackNexus18SetSpeculationModeENS0_15SpeculationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus18SetSpeculationModeENS0_15SpeculationModeE
	.type	_ZN2v88internal13FeedbackNexus18SetSpeculationModeENS0_15SpeculationModeE, @function
_ZN2v88internal13FeedbackNexus18SetSpeculationModeENS0_15SpeculationModeE:
.LFB19562:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L465
	movq	(%rdx), %rax
.L458:
	movl	16(%rdi), %r8d
	leal	56(,%r8,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%rax,%rcx), %rax
	testb	$1, %al
	jne	.L466
	sarq	$32, %rax
	andl	$-2, %eax
	orl	%esi, %eax
	salq	$32, %rax
	testq	%rdx, %rdx
	je	.L467
	movq	(%rdx), %rcx
.L461:
	leal	56(,%r8,8), %edx
	movslq	%edx, %rdx
	movq	%rax, -1(%rcx,%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L465:
	movq	8(%rdi), %rax
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L467:
	movq	8(%rdi), %rcx
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L466:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC30(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19562:
	.size	_ZN2v88internal13FeedbackNexus18SetSpeculationModeENS0_15SpeculationModeE, .-_ZN2v88internal13FeedbackNexus18SetSpeculationModeENS0_15SpeculationModeE
	.section	.text._ZN2v88internal13FeedbackNexus18GetSpeculationModeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus18GetSpeculationModeEv
	.type	_ZN2v88internal13FeedbackNexus18GetSpeculationModeEv, @function
_ZN2v88internal13FeedbackNexus18GetSpeculationModeEv:
.LFB19564:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L475
	movq	(%rax), %rdx
.L470:
	movl	16(%rdi), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	testb	$1, %al
	jne	.L476
	sarq	$32, %rax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	movq	8(%rdi), %rdx
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L476:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC30(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19564:
	.size	_ZN2v88internal13FeedbackNexus18GetSpeculationModeEv, .-_ZN2v88internal13FeedbackNexus18GetSpeculationModeEv
	.section	.text._ZN2v88internal13FeedbackNexus20ComputeCallFrequencyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus20ComputeCallFrequencyEv
	.type	_ZN2v88internal13FeedbackNexus20ComputeCallFrequencyEv, @function
_ZN2v88internal13FeedbackNexus20ComputeCallFrequencyEv:
.LFB19565:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L487
	movq	(%rax), %rdx
	pxor	%xmm1, %xmm1
	movl	35(%rdx), %ecx
	cvtsi2sdl	%ecx, %xmm1
.L479:
	movl	16(%rdi), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	testb	$1, %al
	jne	.L488
	pxor	%xmm0, %xmm0
	testl	%ecx, %ecx
	je	.L477
	shrq	$33, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
.L477:
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	movq	8(%rdi), %rdx
	pxor	%xmm1, %xmm1
	movl	35(%rdx), %ecx
	cvtsi2sdl	%ecx, %xmm1
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L488:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC30(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19565:
	.size	_ZN2v88internal13FeedbackNexus20ComputeCallFrequencyEv, .-_ZN2v88internal13FeedbackNexus20ComputeCallFrequencyEv
	.section	.text._ZN2v88internal13FeedbackNexus20ConfigureMonomorphicENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus20ConfigureMonomorphicENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE
	.type	_ZN2v88internal13FeedbackNexus20ConfigureMonomorphicENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE, @function
_ZN2v88internal13FeedbackNexus20ConfigureMonomorphicENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE:
.LFB19566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$17, 20(%rdi)
	je	.L603
	movq	%rcx, %r13
	testq	%rsi, %rsi
	je	.L604
	movl	$2, %esi
	call	_ZN2v88internal13FeedbackNexus22EnsureExtraArrayOfSizeEi
	movq	(%r12), %rdx
	movq	%rax, %r15
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L605
	movq	(%rax), %r12
.L519:
	movl	16(%rbx), %eax
	leal	48(,%rax,8), %esi
	movslq	%esi, %rsi
	leaq	-1(%r12,%rsi), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L536
	cmpl	$3, %edx
	je	.L536
	movq	%rdx, %rbx
	movq	%rdx, %r8
	andq	$-262144, %rbx
	andq	$-3, %r8
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L521
	movq	%r8, %rdx
	movq	%r12, %rdi
	movq	%r8, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rsi
.L521:
	testb	$24, %al
	je	.L536
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L536
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L536:
	movq	(%r14), %rdx
	movq	(%r15), %r12
	movq	%rdx, %rax
	leaq	15(%r12), %r14
	orq	$2, %rax
	movq	%rax, 15(%r12)
	testb	$1, %al
	je	.L535
	cmpl	$3, %eax
	je	.L535
	movq	%rdx, %rbx
	movq	%rdx, %r8
	andq	$-262144, %rbx
	andq	$-3, %r8
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L524
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %r8
.L524:
	testb	$24, %al
	je	.L535
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L535
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L535:
	movl	0(%r13), %edx
	movq	(%r15), %r12
	movq	8(%r13), %rax
	testl	%edx, %edx
	jne	.L526
	testq	%rax, %rax
	je	.L511
	movq	(%rax), %rax
	orq	$2, %rax
.L527:
	movq	%rax, 23(%r12)
	leaq	23(%r12), %r13
	testb	$1, %al
	je	.L489
.L598:
	cmpl	$3, %eax
	je	.L489
	movq	%rax, %r14
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r14
	testl	$262144, %eax
	jne	.L606
.L529:
	testb	$24, %al
	je	.L489
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L607
.L489:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L511
	movq	(%rax), %rax
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L604:
	movq	(%rdx), %rcx
	movq	(%rdi), %rax
	movq	%rcx, %rdx
	orq	$2, %rdx
	testq	%rax, %rax
	je	.L608
	movq	(%rax), %r14
.L504:
	movl	16(%rbx), %eax
	leal	48(,%rax,8), %eax
	cltq
	leaq	-1(%r14,%rax), %r15
	movq	%rdx, (%r15)
	testb	$1, %dl
	je	.L534
	cmpl	$3, %edx
	je	.L534
	movq	%rcx, %rdx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	andq	$-3, %rdx
	movq	%rcx, %r12
	testl	$262144, %eax
	jne	.L609
.L506:
	testb	$24, %al
	je	.L534
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L610
	.p2align 4,,10
	.p2align 3
.L534:
	movl	0(%r13), %ecx
	movq	8(%r13), %rax
	testl	%ecx, %ecx
	jne	.L508
	testq	%rax, %rax
	je	.L511
	movq	(%rax), %rax
	orq	$2, %rax
.L510:
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L601
.L512:
	movq	(%rdx), %r12
.L513:
	movl	16(%rbx), %edx
	leal	56(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r12,%rdx), %r13
	movq	%rax, 0(%r13)
	testb	$1, %al
	jne	.L598
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L603:
	.cfi_restore_state
	movq	(%rdx), %rcx
	movq	(%rdi), %rax
	movq	%rcx, %rdx
	orq	$2, %rdx
	testq	%rax, %rax
	je	.L611
	movq	(%rax), %r14
.L492:
	movl	16(%rbx), %eax
	leal	48(,%rax,8), %eax
	cltq
	leaq	-1(%r14,%rax), %r15
	movq	%rdx, (%r15)
	testb	$1, %dl
	je	.L532
	cmpl	$3, %edx
	je	.L532
	movq	%rcx, %rdx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	andq	$-3, %rdx
	movq	%rcx, %r13
	testl	$262144, %eax
	jne	.L612
.L494:
	testb	$24, %al
	je	.L532
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L532
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L532:
	movq	(%rbx), %rdx
	movq	(%r12), %rax
	testq	%rdx, %rdx
	jne	.L512
.L601:
	movq	8(%rbx), %r12
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L605:
	movq	8(%rbx), %r12
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L606:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L607:
	addq	$24, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L608:
	.cfi_restore_state
	movq	8(%rdi), %r14
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L611:
	movq	8(%rdi), %r14
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L508:
	testq	%rax, %rax
	je	.L511
	movq	(%rax), %rax
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L511:
	leaq	.LC28(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L609:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-56(%rbp), %rdx
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L612:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rax
	movq	-56(%rbp), %rdx
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L610:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L534
	.cfi_endproc
.LFE19566:
	.size	_ZN2v88internal13FeedbackNexus20ConfigureMonomorphicENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE, .-_ZN2v88internal13FeedbackNexus20ConfigureMonomorphicENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE
	.section	.text._ZN2v88internal13FeedbackNexus20ConfigurePolymorphicENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus20ConfigurePolymorphicENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE
	.type	_ZN2v88internal13FeedbackNexus20ConfigurePolymorphicENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE, @function
_ZN2v88internal13FeedbackNexus20ConfigurePolymorphicENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE:
.LFB19567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	8(%rdx), %rbx
	subq	(%rdx), %rbx
	sarq	$3, %rbx
	leal	(%rbx,%rbx), %esi
	testq	%r13, %r13
	je	.L662
	call	_ZN2v88internal13FeedbackNexus22EnsureExtraArrayOfSizeEi
	movq	(%r12), %rcx
	movq	0(%r13), %rdx
	testq	%rcx, %rcx
	je	.L663
	movq	(%rcx), %r13
.L619:
	movl	16(%r12), %ecx
	leal	48(,%rcx,8), %ecx
	movslq	%ecx, %rcx
	leaq	-1(%r13,%rcx), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L617
	cmpl	$3, %edx
	je	.L617
	movq	%rdx, %r8
	andq	$-262144, %rdx
	movq	%rdx, %r12
	movq	8(%rdx), %rdx
	andq	$-3, %r8
	testl	$262144, %edx
	jne	.L664
	andl	$24, %edx
	je	.L617
.L668:
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L617
	movq	%r8, %rdx
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L617:
	testl	%ebx, %ebx
	jle	.L613
	subl	$1, %ebx
	movl	$16, %r13d
	xorl	%r12d, %r12d
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L666:
	testq	%rdx, %rdx
	je	.L631
	movq	(%rdx), %rdx
	orq	$2, %rdx
.L630:
	leaq	7(%rdi,%r13), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L637
	cmpl	$3, %edx
	je	.L637
	movq	%rdx, %rcx
	movq	%rdx, %r8
	andq	$-262144, %rcx
	andq	$-3, %r8
	movq	8(%rcx), %rdx
	movq	%rcx, -56(%rbp)
	testl	$262144, %edx
	je	.L633
	movq	%r8, %rdx
	movq	%rax, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rsi
	movq	8(%rcx), %rdx
	movq	-64(%rbp), %rdi
.L633:
	andl	$24, %edx
	je	.L637
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L637
	movq	%r8, %rdx
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L637:
	leaq	1(%r12), %rdx
	addq	$16, %r13
	cmpq	%rbx, %r12
	je	.L613
	movq	%rdx, %r12
.L636:
	movq	(%r14), %rdx
	movq	(%rax), %rdi
	movq	(%rdx,%r12,8), %rdx
	leaq	-1(%rdi,%r13), %rsi
	movq	(%rdx), %rcx
	movq	%rcx, %rdx
	orq	$2, %rdx
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L638
	cmpl	$3, %edx
	je	.L638
	movq	%rcx, %r8
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -56(%rbp)
	andq	$-3, %r8
	testl	$262144, %edx
	je	.L625
	movq	%r8, %rdx
	movq	%rax, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rsi
	movq	8(%rcx), %rdx
	movq	-64(%rbp), %rdi
.L625:
	andl	$24, %edx
	je	.L638
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L638
	movq	%r8, %rdx
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L638:
	movq	(%r15), %rsi
	movq	8(%r15), %rdx
	movq	(%rax), %rdi
	subq	%rsi, %rdx
	sarq	$4, %rdx
	cmpq	%r12, %rdx
	jbe	.L665
	movq	%r12, %rdx
	salq	$4, %rdx
	addq	%rdx, %rsi
	movl	(%rsi), %ecx
	movq	8(%rsi), %rdx
	testl	%ecx, %ecx
	je	.L666
	testq	%rdx, %rdx
	je	.L631
	movq	(%rdx), %rdx
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L663:
	movq	8(%r12), %r13
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L631:
	leaq	.LC28(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L613:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L662:
	.cfi_restore_state
	call	_ZN2v88internal13FeedbackNexus17EnsureArrayOfSizeEi
	movq	(%r12), %rdx
	testq	%rdx, %rdx
	je	.L667
	movq	(%rdx), %rcx
	movq	%rcx, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-33752(%rdx), %rsi
.L616:
	movl	16(%r12), %edx
	leal	56(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	%rsi, -1(%rcx,%rdx)
	jmp	.L617
.L664:
	movq	%r8, %rdx
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rdx
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rsi
	andl	$24, %edx
	jne	.L668
	jmp	.L617
.L667:
	movq	8(%r12), %rcx
	movq	%rcx, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-33752(%rdx), %rsi
	jmp	.L616
.L665:
	movq	%r12, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE19567:
	.size	_ZN2v88internal13FeedbackNexus20ConfigurePolymorphicENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE, .-_ZN2v88internal13FeedbackNexus20ConfigurePolymorphicENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE
	.section	.text._ZNK2v88internal13FeedbackNexus17FindHandlerForMapENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus17FindHandlerForMapENS0_6HandleINS0_3MapEEE
	.type	_ZNK2v88internal13FeedbackNexus17FindHandlerForMapENS0_6HandleINS0_3MapEEE, @function
_ZNK2v88internal13FeedbackNexus17FindHandlerForMapENS0_6HandleINS0_3MapEEE:
.LFB19570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L730
	movq	(%rax), %rcx
.L671:
	movl	16(%r8), %edx
	leal	48(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rcx,%rdx), %rcx
	testq	%rax, %rax
	je	.L731
	movq	(%rax), %rax
.L673:
	andq	$-262144, %rax
	movq	%rcx, %rdi
	movq	24(%rax), %rbx
	call	_ZN2v88internalL22IsPropertyNameFeedbackENS0_11MaybeObjectE
	movq	%rcx, %rdx
	subq	$37592, %rbx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L732
	testb	%al, %al
	jne	.L682
	cmpq	$3, %rdx
	je	.L733
.L698:
	movl	$1, %eax
	xorl	%edx, %edx
.L715:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L732:
	.cfi_restore_state
	movq	-1(%rcx), %rdx
	movzwl	11(%rdx), %edx
	subw	$148, %dx
	cmpw	$1, %dx
	jbe	.L734
	testb	%al, %al
	je	.L698
	.p2align 4,,10
	.p2align 3
.L682:
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L735
	movq	(%rax), %rdx
.L684:
	movl	16(%r8), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rcx
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L730:
	movq	8(%rdi), %rcx
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L731:
	movq	8(%r8), %rax
	jmp	.L673
.L734:
	testb	%al, %al
	jne	.L682
.L683:
	movl	11(%rcx), %eax
	leaq	7(%rcx), %r8
	testl	%eax, %eax
	jle	.L698
	leaq	-1(%rcx), %r10
	leaq	15(%rcx), %rdx
	xorl	%eax, %eax
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L686:
	addq	$16, %rdx
	cmpl	%eax, 4(%r8)
	jle	.L698
.L685:
	movq	(%rdx), %rcx
	movl	%eax, %r9d
	addl	$2, %eax
	movq	%rcx, %rdi
	andl	$3, %edi
	cmpq	$3, %rdi
	jne	.L686
	cmpl	$3, %ecx
	je	.L686
	andq	$-3, %rcx
	cmpq	%rcx, (%rsi)
	jne	.L686
	leal	24(,%r9,8), %ecx
	addq	%r10, %rcx
	movq	(%rcx), %rdi
	cmpl	$3, %edi
	je	.L686
	movq	(%rcx), %r12
.L729:
	movq	%r12, %rax
	movq	41112(%rbx), %rdi
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L736
.L705:
	testq	%rdi, %rdi
	je	.L710
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L711:
	movl	$1, %eax
.L709:
	andl	$1, %eax
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L733:
	cmpl	$3, %ecx
	je	.L698
	movq	%rcx, %rdx
	andq	$-3, %rdx
	cmpq	%rdx, (%rsi)
	jne	.L698
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L737
	movq	(%rax), %rsi
.L700:
	movl	16(%r8), %ecx
	leal	56(,%rcx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rsi,%rdx), %rdx
	cmpl	$3, %edx
	je	.L698
	testq	%rax, %rax
	je	.L738
	movq	(%rax), %rdx
.L703:
	leal	56(,%rcx,8), %eax
	cltq
	movq	-1(%rdx,%rax), %r12
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L735:
	movq	8(%r8), %rdx
	jmp	.L684
.L710:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L739
.L712:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rdx)
	jmp	.L711
.L736:
	cmpl	$3, %r12d
	je	.L705
	andq	$-3, %r12
	testq	%rdi, %rdi
	je	.L706
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
	xorl	%eax, %eax
	jmp	.L709
.L737:
	movq	8(%r8), %rsi
	jmp	.L700
.L706:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L740
.L708:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	xorl	%eax, %eax
	movq	%r12, (%rdx)
	jmp	.L709
.L738:
	movq	8(%r8), %rdx
	jmp	.L703
.L739:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L712
.L740:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L708
	.cfi_endproc
.LFE19570:
	.size	_ZNK2v88internal13FeedbackNexus17FindHandlerForMapENS0_6HandleINS0_3MapEEE, .-_ZNK2v88internal13FeedbackNexus17FindHandlerForMapENS0_6HandleINS0_3MapEEE
	.section	.text._ZNK2v88internal13FeedbackNexus7GetNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus7GetNameEv
	.type	_ZNK2v88internal13FeedbackNexus7GetNameEv, @function
_ZNK2v88internal13FeedbackNexus7GetNameEv:
.LFB19571:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %edx
	cmpl	$13, %edx
	ja	.L742
	movl	$8968, %eax
	btq	%rdx, %rax
	jnc	.L757
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L761
	movq	(%rax), %rcx
.L745:
	movl	16(%rdi), %eax
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rcx,%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L762
.L742:
	cmpl	$17, %edx
	je	.L763
.L757:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	movq	8(%rdi), %rcx
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L762:
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L758
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L742
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	%rax, -33752(%rcx)
	je	.L742
	cmpq	%rax, -33824(%rcx)
	je	.L742
	cmpq	%rax, -33856(%rcx)
	je	.L742
.L758:
	ret
	.p2align 4,,10
	.p2align 3
.L763:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rax, %rax
	je	.L764
	movq	(%rax), %rdx
.L751:
	movl	16(%rdi), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rcx
	movq	%rcx, %rdi
	call	_ZN2v88internalL22IsPropertyNameFeedbackENS0_11MaybeObjectE
	testb	%al, %al
	je	.L743
	movq	%rcx, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L743:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L764:
	.cfi_restore_state
	movq	8(%rdi), %rdx
	jmp	.L751
	.cfi_endproc
.LFE19571:
	.size	_ZNK2v88internal13FeedbackNexus7GetNameEv, .-_ZNK2v88internal13FeedbackNexus7GetNameEv
	.section	.text._ZNK2v88internal13FeedbackNexus10GetKeyTypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus10GetKeyTypeEv
	.type	_ZNK2v88internal13FeedbackNexus10GetKeyTypeEv, @function
_ZNK2v88internal13FeedbackNexus10GetKeyTypeEv:
.LFB19585:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L778
	movq	(%rax), %rsi
.L767:
	movl	16(%rdi), %ecx
	leal	48(,%rcx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rsi,%rdx), %rdx
	testq	%rax, %rax
	je	.L779
	movq	(%rax), %rax
.L777:
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-33856(%rsi), %rdx
	je	.L780
	cmpl	$17, 20(%rdi)
	je	.L781
.L772:
	movq	%rdx, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L773
.L775:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L778:
	movq	8(%rdi), %rsi
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L779:
	movq	8(%rdi), %rax
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L780:
	leal	56(,%rcx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rax,%rdx), %rax
	shrq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L773:
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	jbe	.L774
	movq	-1(%rdx), %rax
	cmpw	$64, 11(%rax)
	jne	.L775
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-33752(%rax), %rdx
	je	.L775
	cmpq	-33824(%rax), %rdx
	je	.L775
	cmpq	-33856(%rax), %rdx
	je	.L775
.L774:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L781:
	leal	56(,%rcx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rax,%rdx), %rdx
	jmp	.L772
	.cfi_endproc
.LFE19585:
	.size	_ZNK2v88internal13FeedbackNexus10GetKeyTypeEv, .-_ZNK2v88internal13FeedbackNexus10GetKeyTypeEv
	.section	.text._ZNK2v88internal13FeedbackNexus26GetBinaryOperationFeedbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus26GetBinaryOperationFeedbackEv
	.type	_ZNK2v88internal13FeedbackNexus26GetBinaryOperationFeedbackEv, @function
_ZNK2v88internal13FeedbackNexus26GetBinaryOperationFeedbackEv:
.LFB19586:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L787
	movq	(%rax), %rdx
.L784:
	movl	16(%rdi), %eax
	movl	$8, %r8d
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	cmpl	$32, %eax
	ja	.L782
	movl	%eax, %eax
	leaq	CSWTCH.434(%rip), %rdx
	movzbl	(%rdx,%rax), %r8d
.L782:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	movq	8(%rdi), %rdx
	jmp	.L784
	.cfi_endproc
.LFE19586:
	.size	_ZNK2v88internal13FeedbackNexus26GetBinaryOperationFeedbackEv, .-_ZNK2v88internal13FeedbackNexus26GetBinaryOperationFeedbackEv
	.section	.text._ZNK2v88internal13FeedbackNexus27GetCompareOperationFeedbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus27GetCompareOperationFeedbackEv
	.type	_ZNK2v88internal13FeedbackNexus27GetCompareOperationFeedbackEv, @function
_ZNK2v88internal13FeedbackNexus27GetCompareOperationFeedbackEv:
.LFB19587:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L806
	movq	(%rax), %rdx
.L790:
	movl	16(%rdi), %eax
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	cmpq	$32, %rax
	jg	.L791
	movl	$10, %r8d
	testl	%eax, %eax
	js	.L788
	cmpl	$32, %eax
	ja	.L793
	leaq	.L795(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal13FeedbackNexus27GetCompareOperationFeedbackEv,"a",@progbits
	.align 4
	.align 4
.L795:
	.long	.L802-.L795
	.long	.L800-.L795
	.long	.L793-.L795
	.long	.L799-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L798-.L795
	.long	.L797-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L796-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L793-.L795
	.long	.L794-.L795
	.section	.text._ZNK2v88internal13FeedbackNexus27GetCompareOperationFeedbackEv
.L800:
	movl	$1, %r8d
.L788:
	movl	%r8d, %eax
	ret
.L802:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
.L793:
	movl	$10, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L791:
	movl	$8, %r8d
	cmpl	$128, %eax
	je	.L788
	cmpl	$384, %eax
	jne	.L807
	movl	$9, %r8d
	movl	%r8d, %eax
	ret
.L807:
	cmpl	$64, %eax
	setne	%r8b
	leal	7(%r8,%r8,2), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L806:
	movq	8(%rdi), %rdx
	jmp	.L790
.L794:
	movl	$6, %r8d
	jmp	.L788
.L799:
	movl	$2, %r8d
	jmp	.L788
.L798:
	movl	$3, %r8d
	jmp	.L788
.L797:
	movl	$4, %r8d
	jmp	.L788
.L796:
	movl	$5, %r8d
	jmp	.L788
	.cfi_endproc
.LFE19587:
	.size	_ZNK2v88internal13FeedbackNexus27GetCompareOperationFeedbackEv, .-_ZNK2v88internal13FeedbackNexus27GetCompareOperationFeedbackEv
	.section	.rodata._ZNK2v88internal13FeedbackNexus8ic_stateEv.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"feedback == MaybeObject::FromObject( *FeedbackVector::UninitializedSentinel(isolate))"
	.section	.text.unlikely._ZNK2v88internal13FeedbackNexus8ic_stateEv,"ax",@progbits
	.align 2
.LCOLDB33:
	.section	.text._ZNK2v88internal13FeedbackNexus8ic_stateEv,"ax",@progbits
.LHOTB33:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus8ic_stateEv
	.type	_ZNK2v88internal13FeedbackNexus8ic_stateEv, @function
_ZNK2v88internal13FeedbackNexus8ic_stateEv:
.LFB19549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r8, %r8
	je	.L890
	movq	(%r8), %rcx
	movq	%rcx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	subq	$37592, %rdx
.L810:
	movl	16(%rdi), %r9d
	leal	48(,%r9,8), %eax
	cltq
	movq	-1(%rcx,%rax), %rsi
	cmpl	$23, 20(%rdi)
	ja	.L846
	movl	20(%rdi), %eax
	leaq	.L813(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal13FeedbackNexus8ic_stateEv,"a",@progbits
	.align 4
	.align 4
.L813:
	.long	.L812-.L813
	.long	.L823-.L813
	.long	.L822-.L813
	.long	.L822-.L813
	.long	.L824-.L813
	.long	.L822-.L813
	.long	.L823-.L813
	.long	.L823-.L813
	.long	.L822-.L813
	.long	.L822-.L813
	.long	.L823-.L813
	.long	.L822-.L813
	.long	.L822-.L813
	.long	.L822-.L813
	.long	.L822-.L813
	.long	.L821-.L813
	.long	.L820-.L813
	.long	.L819-.L813
	.long	.L818-.L813
	.long	.L817-.L813
	.long	.L816-.L813
	.long	.L815-.L813
	.long	.L814-.L813
	.long	.L812-.L813
	.section	.text._ZNK2v88internal13FeedbackNexus8ic_stateEv
	.p2align 4,,10
	.p2align 3
.L890:
	movq	8(%rdi), %rcx
	movq	%rcx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	subq	$37592, %rdx
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L822:
	cmpq	%rsi, 3840(%rdx)
	je	.L830
	cmpq	%rsi, 3736(%rdx)
	je	.L853
	cmpq	%rsi, 3768(%rdx)
	je	.L850
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L825
	cmpq	$1, %rax
	jne	.L812
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subw	$148, %ax
	cmpw	$1, %ax
	jbe	.L888
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	ja	.L812
	testq	%r8, %r8
	je	.L891
	movq	(%r8), %rdx
.L834:
	leal	56(,%r9,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	cmpl	$2, 11(%rax)
	jle	.L825
	.p2align 4,,10
	.p2align 3
.L888:
	movl	$5, %eax
.L808:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L823:
	.cfi_restore_state
	testb	$1, %sil
	je	.L825
	movl	$2, %eax
	cmpq	%rsi, 3768(%rdx)
	je	.L808
	testq	%r8, %r8
	je	.L892
	movq	(%r8), %rcx
.L829:
	leal	56(,%r9,8), %eax
	cltq
	movq	-1(%rcx,%rax), %rcx
	cmpl	$3, %esi
	jne	.L825
	movl	$1, %eax
	cmpq	%rcx, 3840(%rdx)
	jne	.L825
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L815:
	.cfi_restore_state
	cmpq	%rsi, 3840(%rdx)
	je	.L830
	movl	$6, %eax
	cmpq	%rsi, 3736(%rdx)
	je	.L808
.L825:
	movl	$3, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L814:
	.cfi_restore_state
	cmpq	%rsi, 3840(%rdx)
	je	.L830
	cmpq	%rsi, 3736(%rdx)
	je	.L853
	andl	$3, %esi
	cmpq	$3, %rsi
	jne	.L888
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L819:
	cmpq	%rsi, 3840(%rdx)
	je	.L830
	andl	$3, %esi
	cmpq	$3, %rsi
	je	.L825
.L853:
	movl	$6, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L818:
	.cfi_restore_state
	cmpq	%rsi, 3840(%rdx)
	jne	.L825
.L830:
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L817:
	.cfi_restore_state
	andl	$1, %esi
	jne	.L825
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L816:
	testq	%r8, %r8
	je	.L893
	movq	(%r8), %rdx
.L845:
	leal	48(,%r9,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	cmpl	$3, %eax
	ja	.L851
	movl	%eax, %eax
	leaq	CSWTCH.437(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L830
	cmpb	$3, %al
	jne	.L825
.L851:
	movl	$7, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L824:
	.cfi_restore_state
	cmpq	%rsi, 3736(%rdx)
	je	.L851
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L825
	cmpq	$1, %rax
	je	.L836
.L838:
	cmpq	%rsi, 3840(%rdx)
	je	.L830
	leaq	.LC32(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L821:
	testq	%r8, %r8
	je	.L894
	movq	(%r8), %rdx
.L840:
	leal	48(,%r9,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	cmpl	$32, %eax
	ja	.L851
	movl	%eax, %eax
	leaq	CSWTCH.434(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L830
	cmpb	$8, %al
	jne	.L825
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L820:
	call	_ZNK2v88internal13FeedbackNexus27GetCompareOperationFeedbackEv
	testb	%al, %al
	je	.L830
	cmpb	$10, %al
	jne	.L825
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L812:
	leaq	.LC24(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L893:
	movq	8(%rdi), %rdx
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L894:
	movq	8(%rdi), %rdx
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L836:
	movq	-1(%rsi), %rax
	cmpw	$121, 11(%rax)
	jne	.L838
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L892:
	movq	8(%rdi), %rcx
	jmp	.L829
.L850:
	movl	$2, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L891:
	.cfi_restore_state
	movq	8(%rdi), %rdx
	jmp	.L834
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal13FeedbackNexus8ic_stateEv
	.cfi_startproc
	.type	_ZNK2v88internal13FeedbackNexus8ic_stateEv.cold, @function
_ZNK2v88internal13FeedbackNexus8ic_stateEv.cold:
.LFSB19549:
.L846:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	$1, %eax
	jmp	.L808
	.cfi_endproc
.LFE19549:
	.section	.text._ZNK2v88internal13FeedbackNexus8ic_stateEv
	.size	_ZNK2v88internal13FeedbackNexus8ic_stateEv, .-_ZNK2v88internal13FeedbackNexus8ic_stateEv
	.section	.text.unlikely._ZNK2v88internal13FeedbackNexus8ic_stateEv
	.size	_ZNK2v88internal13FeedbackNexus8ic_stateEv.cold, .-_ZNK2v88internal13FeedbackNexus8ic_stateEv.cold
.LCOLDE33:
	.section	.text._ZNK2v88internal13FeedbackNexus8ic_stateEv
.LHOTE33:
	.section	.text._ZN2v88internal13FeedbackNexus5ClearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus5ClearEv
	.type	_ZN2v88internal13FeedbackNexus5ClearEv, @function
_ZN2v88internal13FeedbackNexus5ClearEv:
.LFB19535:
	.cfi_startproc
	endbr64
	cmpl	$23, 20(%rdi)
	ja	.L919
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L898(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movl	20(%rdi), %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal13FeedbackNexus5ClearEv,"a",@progbits
	.align 4
	.align 4
.L898:
	.long	.L897-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L908-.L898
	.long	.L908-.L898
	.long	.L899-.L898
	.long	.L908-.L898
	.long	.L900-.L898
	.long	.L908-.L898
	.long	.L899-.L898
	.long	.L899-.L898
	.long	.L897-.L898
	.section	.text._ZN2v88internal13FeedbackNexus5ClearEv
	.p2align 4,,10
	.p2align 3
.L899:
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv
	movl	%eax, %edx
	movzbl	_ZN2v88internal11FLAG_use_icE(%rip), %eax
	testb	%al, %al
	je	.L895
	subl	$1, %edx
	movb	%al, -33(%rbp)
	cmpl	$1, %edx
	jbe	.L908
	movq	%r12, %rdi
	call	_ZN2v88internal13FeedbackNexus22ConfigureUninitializedEv
	movzbl	-33(%rbp), %eax
.L895:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L900:
	.cfi_restore_state
	movq	(%rdi), %rdx
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	testq	%rdx, %rdx
	je	.L922
	movq	(%rdx), %r13
.L902:
	movl	16(%r12), %edx
	leal	48(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r13,%rdx), %r14
	movq	%rax, (%r14)
	testb	$1, %al
	je	.L906
	cmpl	$3, %eax
	je	.L906
	movq	%rax, %r15
	andq	$-262144, %rax
	movq	%rax, %r12
	movq	8(%rax), %rax
	andq	$-3, %r15
	testl	$262144, %eax
	je	.L904
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
.L904:
	testb	$24, %al
	je	.L906
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L923
.L906:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L908:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L922:
	.cfi_restore_state
	movq	8(%rdi), %r13
	jmp	.L902
.L897:
	leaq	.LC24(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L923:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L919:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19535:
	.size	_ZN2v88internal13FeedbackNexus5ClearEv, .-_ZN2v88internal13FeedbackNexus5ClearEv
	.section	.text._ZN2v88internal14FeedbackVector10ClearSlotsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FeedbackVector10ClearSlotsEPNS0_7IsolateE
	.type	_ZN2v88internal14FeedbackVector10ClearSlotsEPNS0_7IsolateE, @function
_ZN2v88internal14FeedbackVector10ClearSlotsEPNS0_7IsolateE:
.LFB19530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	7(%rax), %rax
	movq	23(%rax), %r13
	movq	-1(%r13), %rax
	cmpw	$75, 11(%rax)
	jne	.L927
	movq	3840(%rsi), %rax
	leaq	7(%r13), %rbx
	movq	%rax, -88(%rbp)
	movl	7(%r13), %eax
	testl	%eax, %eax
	jle	.L927
	movq	%rdi, %r14
	subq	$1, %r13
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	leaq	.L930(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L934:
	movslq	%esi, %rax
	movl	%esi, %ecx
	movl	%esi, %edx
	imulq	$715827883, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	subl	%ecx, %eax
	leal	16(,%rax,4), %edi
	leal	(%rax,%rax,2), %eax
	addl	%eax, %eax
	movslq	%edi, %rdi
	subl	%eax, %edx
	movl	(%rdi,%r13), %eax
	leal	(%rdx,%rdx,4), %ecx
	shrl	%cl, %eax
	andl	$31, %eax
	cmpl	$23, %eax
	ja	.L936
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal14FeedbackVector10ClearSlotsEPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L930:
	.long	.L929-.L930
	.long	.L931-.L930
	.long	.L931-.L930
	.long	.L931-.L930
	.long	.L931-.L930
	.long	.L931-.L930
	.long	.L931-.L930
	.long	.L931-.L930
	.long	.L931-.L930
	.long	.L931-.L930
	.long	.L931-.L930
	.long	.L931-.L930
	.long	.L931-.L930
	.long	.L931-.L930
	.long	.L931-.L930
	.long	.L936-.L930
	.long	.L936-.L930
	.long	.L931-.L930
	.long	.L936-.L930
	.long	.L936-.L930
	.long	.L936-.L930
	.long	.L936-.L930
	.long	.L931-.L930
	.long	.L929-.L930
	.section	.text._ZN2v88internal14FeedbackVector10ClearSlotsEPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L931:
	movl	$2, %r8d
.L928:
	movq	(%r14), %r9
	leal	48(,%rsi,8), %eax
	addl	%esi, %r8d
	cltq
	movq	-1(%rax,%r9), %rax
	cmpq	%rax, -88(%rbp)
	je	.L932
	movq	$0, -80(%rbp)
	xorl	%eax, %eax
	movq	%r9, -72(%rbp)
	movl	%esi, -64(%rbp)
	testq	%r9, %r9
	je	.L933
	movq	7(%r9), %rax
	movq	23(%rax), %rax
	movl	-1(%rdi,%rax), %eax
	shrl	%cl, %eax
	andl	$31, %eax
.L933:
	leaq	-80(%rbp), %rdi
	movl	%r8d, -92(%rbp)
	movl	%eax, -60(%rbp)
	call	_ZN2v88internal13FeedbackNexus5ClearEv
	movl	-92(%rbp), %r8d
	orl	%eax, %r15d
.L932:
	cmpl	%r8d, (%rbx)
	jle	.L924
	movl	%r8d, %esi
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L936:
	movl	$1, %r8d
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L927:
	xorl	%r15d, %r15d
.L924:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L941
	addq	$56, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L929:
	.cfi_restore_state
	leaq	.LC24(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L941:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19530:
	.size	_ZN2v88internal14FeedbackVector10ClearSlotsEPNS0_7IsolateE, .-_ZN2v88internal14FeedbackVector10ClearSlotsEPNS0_7IsolateE
	.section	.text.unlikely._ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_,"ax",@progbits
	.align 2
.LCOLDB34:
	.section	.text._ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_,"ax",@progbits
.LHOTB34:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_
	.type	_ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_, @function
_ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_:
.LFB19559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %rax
	movq	%rdx, -64(%rbp)
	testq	%rax, %rax
	je	.L1132
	movq	(%rax), %rcx
	movq	%rcx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	subq	$37592, %rdx
.L944:
	movl	16(%r12), %eax
	xorl	%esi, %esi
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rcx,%rax), %r15
	movq	%r15, %r13
	notq	%r13
	andl	$1, %r13d
	jne	.L945
	movq	%r15, %rax
	andq	$-3, %rax
	cmpl	$3, %r15d
	cmovne	%rax, %rsi
.L945:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L946
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r14
.L947:
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv
	cmpl	$3, %eax
	je	.L949
	cmpl	$5, %eax
	movq	-56(%rbp), %rdx
	je	.L950
	cmpl	$1, %eax
	je	.L1133
	leaq	.LC24(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L946:
	movq	41088(%rdx), %r14
	cmpq	%r14, 41096(%rdx)
	je	.L1134
.L948:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	(%rbx), %rcx
	movq	(%r12), %rax
	movq	%rcx, %rdx
	orq	$2, %rdx
	testq	%rax, %rax
	je	.L1135
	movq	(%rax), %r13
.L953:
	movl	16(%r12), %eax
	leal	48(,%rax,8), %eax
	cltq
	leaq	-1(%r13,%rax), %r15
	movq	%rdx, (%r15)
	testb	$1, %dl
	je	.L1023
	cmpl	$3, %edx
	je	.L1023
	movq	%rcx, %r14
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	andq	$-3, %r14
	movq	%rcx, %rbx
	testl	$262144, %eax
	je	.L986
.L1131:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L986:
	testb	$24, %al
	je	.L1023
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1023
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1023:
	movq	-64(%rbp), %rax
	movq	(%r12), %rdx
	movq	(%rax), %rax
	testq	%rdx, %rdx
	je	.L1136
	movq	(%rdx), %r13
.L989:
	movl	16(%r12), %edx
	leal	56(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r13,%rdx), %r12
	movq	%rax, (%r12)
	testb	$1, %al
	je	.L942
	cmpl	$3, %eax
	je	.L942
	movq	%rax, %r14
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r14
	testl	$262144, %eax
	jne	.L1137
.L991:
	testb	$24, %al
	je	.L942
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L942
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
.L1114:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L996:
	.cfi_restore_state
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1138
.L999:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	cmpq	%rax, %rbx
	jne	.L1000
.L1113:
	movq	(%r14), %rdi
.L997:
	movslq	11(%rdi), %rax
	movl	%eax, %esi
	cmpl	%eax, %r15d
	jl	.L995
.L993:
	movl	-80(%rbp), %r13d
	addl	%r13d, %r13d
	cmpl	%r13d, %r15d
	jne	.L1004
	movq	(%r12), %rax
	movq	3736(%rdx), %rcx
	testq	%rax, %rax
	je	.L1139
	movq	(%rax), %rdx
.L1006:
	movl	16(%r12), %eax
	leal	48(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rdx,%rax)
.L1120:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L1140
	movq	(%rax), %rdx
.L1008:
	movl	16(%r12), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	$3, -1(%rdx,%rax)
.L942:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L950:
	.cfi_restore_state
	movl	_ZN2v88internal30FLAG_max_polymorphic_map_countE(%rip), %eax
	movq	(%r14), %rdi
	xorl	%r15d, %r15d
	movl	$16, %r13d
	movl	%eax, -80(%rbp)
	movslq	11(%rdi), %rax
	movl	%eax, %esi
	cmpl	%eax, %r15d
	jge	.L993
.L1141:
	movq	-1(%rdi,%r13), %rsi
	leal	2(%r15), %r9d
	cmpl	$3, %esi
	je	.L997
	movq	41112(%rdx), %rdi
	andq	$-3, %rsi
	testq	%rdi, %rdi
	je	.L996
	movq	%rdx, -72(%rbp)
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-56(%rbp), %r9d
	movq	-72(%rbp), %rdx
	cmpq	%rax, %rbx
	je	.L1113
	testq	%rax, %rax
	je	.L1001
	movq	(%rax), %rsi
.L1000:
	testq	%rbx, %rbx
	je	.L1002
	cmpq	%rsi, (%rbx)
	je	.L1113
.L1002:
	movl	15(%rsi), %eax
	leaq	16(%r13), %rsi
	testl	$16777216, %eax
	jne	.L1113
	movq	(%r14), %rdi
	movl	%r9d, %r15d
	movq	%rsi, %r13
	movslq	11(%rdi), %rax
	movl	%eax, %esi
	cmpl	%eax, %r15d
	jl	.L1141
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1004:
	addl	$2, %esi
	movq	%r12, %rdi
	movl	$15, %r13d
	call	_ZN2v88internal13FeedbackNexus17EnsureArrayOfSizeEi
	movq	(%r14), %rdx
	xorl	%r8d, %r8d
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L1013
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	(%rax), %rdi
	movq	0(%r13,%rdx), %rdx
	leaq	(%rdi,%r13), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L1028
	cmpl	$3, %edx
	je	.L1028
	movq	%rdx, %r9
	andq	$-262144, %rdx
	movq	%rdx, %r12
	movq	8(%rdx), %rdx
	andq	$-3, %r9
	testl	$262144, %edx
	je	.L1011
	movq	%r9, %rdx
	movl	%r8d, -92(%rbp)
	movq	%rax, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rdx
	movl	-92(%rbp), %r8d
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %rsi
	movq	-56(%rbp), %rdi
.L1011:
	andl	$24, %edx
	je	.L1028
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L1028
	movq	%r9, %rdx
	movl	%r8d, -72(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-72(%rbp), %r8d
	movq	-56(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L1028:
	movq	(%r14), %rdx
	addl	$1, %r8d
	addq	$8, %r13
	cmpl	%r8d, 11(%rdx)
	jg	.L1009
.L1013:
	leal	16(,%r15,8), %r8d
	movq	(%rax), %rdi
	movq	%rax, %r14
	movslq	%r8d, %r13
.L995:
	movq	(%rbx), %rdx
	leaq	(%rdi,%r13), %r8
	leaq	-1(%r8), %r12
	movq	%rdx, %rax
	orq	$2, %rax
	movq	%rax, -1(%r8)
	testb	$1, %al
	je	.L1030
	cmpl	$3, %eax
	je	.L1030
	movq	%rdx, %rbx
	movq	%rdx, %r13
	andq	$-262144, %rbx
	andq	$-3, %r13
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1142
.L1015:
	testb	$24, %al
	je	.L1030
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1143
	.p2align 4,,10
	.p2align 3
.L1030:
	movq	-64(%rbp), %rax
	movq	(%r14), %r12
	leal	24(,%r15,8), %edx
	movslq	%edx, %rdx
	movq	(%rax), %rax
	leaq	-1(%r12,%rdx), %r13
	movq	%rax, 0(%r13)
	testb	$1, %al
	je	.L942
	cmpl	$3, %eax
	je	.L942
	movq	%rax, %r14
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r14
	testl	$262144, %eax
	jne	.L1144
.L1018:
	testb	$24, %al
	je	.L942
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L942
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L949:
	cmpq	%rbx, %r14
	je	.L963
	cmpl	$3, %r15d
	je	.L963
	movq	(%r14), %rax
	testq	%r14, %r14
	je	.L964
	testq	%rbx, %rbx
	je	.L964
	cmpq	%rax, (%rbx)
	je	.L965
.L964:
	movl	15(%rax), %eax
	testl	$16777216, %eax
	je	.L966
.L963:
	movq	(%rbx), %rax
.L965:
	movq	(%r12), %rdx
	movq	%rax, %rcx
	orq	$2, %rcx
	testq	%rdx, %rdx
	je	.L1145
	movq	(%rdx), %r13
.L984:
	movl	16(%r12), %edx
	leal	48(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r13,%rdx), %r15
	movq	%rcx, (%r15)
	testb	$1, %cl
	je	.L1023
	cmpl	$3, %ecx
	je	.L1023
	movq	%rax, %r14
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r14
	testl	$262144, %eax
	je	.L986
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1132:
	movq	8(%rdi), %rcx
	movq	%rcx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	subq	$37592, %rdx
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1145:
	movq	8(%r12), %r13
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1136:
	movq	8(%r12), %r13
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L1138:
	movq	%rdx, %rdi
	movq	%rsi, -88(%rbp)
	movl	%r9d, -72(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movl	-72(%rbp), %r9d
	movq	-56(%rbp), %rdx
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1135:
	movq	8(%r12), %r13
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L1134:
	movq	%rdx, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	-56(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L966:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal13FeedbackNexus17EnsureArrayOfSizeEi
	movq	(%rax), %rdi
	movq	%rax, %r14
	movq	%r15, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	%r13b, %r13b
	jne	.L1027
	movq	%r15, %r13
	andq	$-262144, %r15
	movq	8(%r15), %rdx
	andq	$-3, %r13
	testl	$262144, %edx
	je	.L968
	movq	%r13, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rdx
	movq	-72(%rbp), %rsi
	movq	-56(%rbp), %rdi
.L968:
	andl	$24, %edx
	je	.L1027
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L1027
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1027:
	movq	(%r12), %rdx
	movq	(%r14), %r15
	testq	%rdx, %rdx
	je	.L1146
	movq	(%rdx), %rcx
.L971:
	movl	16(%r12), %edx
	leaq	23(%r15), %rsi
	leal	56(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rcx,%rdx), %rdx
	movq	%rdx, 23(%r15)
	testb	$1, %dl
	je	.L1026
	cmpl	$3, %edx
	je	.L1026
	movq	%rdx, %r8
	andq	$-262144, %rdx
	movq	%rdx, %r13
	movq	8(%rdx), %rdx
	andq	$-3, %r8
	testl	$262144, %edx
	je	.L973
	movq	%r8, %rdx
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rdx
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %rsi
.L973:
	andl	$24, %edx
	je	.L1026
	movq	%r15, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L1026
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1026:
	movq	(%rbx), %rcx
	movq	(%r14), %r13
	movq	%rcx, %rdx
	leaq	31(%r13), %r15
	orq	$2, %rdx
	movq	%rdx, 31(%r13)
	testb	$1, %dl
	je	.L1025
	cmpl	$3, %edx
	je	.L1025
	movq	%rcx, %r8
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	andq	$-3, %r8
	movq	%rcx, %rbx
	testl	$262144, %edx
	je	.L976
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rdx
	movq	-56(%rbp), %r8
.L976:
	andl	$24, %edx
	je	.L1025
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L1025
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1025:
	movq	-64(%rbp), %rax
	movq	(%r14), %r13
	movq	(%rax), %rax
	leaq	39(%r13), %r14
	movq	%rax, 39(%r13)
	testb	$1, %al
	je	.L1120
	cmpl	$3, %eax
	je	.L1120
	movq	%rax, %r15
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r15
	testl	$262144, %eax
	je	.L979
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L979:
	testb	$24, %al
	je	.L1120
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1120
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rdi
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1143:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1140:
	movq	8(%r12), %rdx
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1139:
	movq	8(%r12), %rdx
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	8(%r12), %rcx
	jmp	.L971
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_
	.cfi_startproc
	.type	_ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_.cold, @function
_ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_.cold:
.LFSB19559:
.L1001:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE19559:
	.section	.text._ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_
	.size	_ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_, .-_ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_
	.section	.text.unlikely._ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_
	.size	_ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_.cold, .-_ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_.cold
.LCOLDE34:
	.section	.text._ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_
.LHOTE34:
	.section	.text._ZNK2v88internal13FeedbackNexus16GetForInFeedbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus16GetForInFeedbackEv
	.type	_ZNK2v88internal13FeedbackNexus16GetForInFeedbackEv, @function
_ZNK2v88internal13FeedbackNexus16GetForInFeedbackEv:
.LFB19588:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L1152
	movq	(%rax), %rdx
.L1149:
	movl	16(%rdi), %eax
	movl	$3, %r8d
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	cmpl	$3, %eax
	ja	.L1147
	movl	%eax, %eax
	leaq	CSWTCH.437(%rip), %rdx
	movzbl	(%rdx,%rax), %r8d
.L1147:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	8(%rdi), %rdx
	jmp	.L1149
	.cfi_endproc
.LFE19588:
	.size	_ZNK2v88internal13FeedbackNexus16GetForInFeedbackEv, .-_ZNK2v88internal13FeedbackNexus16GetForInFeedbackEv
	.section	.text._ZNK2v88internal13FeedbackNexus22GetConstructorFeedbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus22GetConstructorFeedbackEv
	.type	_ZNK2v88internal13FeedbackNexus22GetConstructorFeedbackEv, @function
_ZNK2v88internal13FeedbackNexus22GetConstructorFeedbackEv:
.LFB19589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L1162
	movq	(%rax), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	subq	$37592, %rbx
.L1155:
	movl	16(%rdi), %eax
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rsi
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L1163
.L1156:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1163:
	.cfi_restore_state
	cmpl	$3, %esi
	je	.L1156
	movq	41112(%rbx), %rdi
	andq	$-3, %rsi
	testq	%rdi, %rdi
	je	.L1157
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1162:
	.cfi_restore_state
	movq	8(%rdi), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	subq	$37592, %rbx
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1164
.L1159:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1164:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1159
	.cfi_endproc
.LFE19589:
	.size	_ZNK2v88internal13FeedbackNexus22GetConstructorFeedbackEv, .-_ZNK2v88internal13FeedbackNexus22GetConstructorFeedbackEv
	.section	.text._ZN2v88internal13FeedbackNexus7CollectENS0_6HandleINS0_6StringEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus7CollectENS0_6HandleINS0_6StringEEEi
	.type	_ZN2v88internal13FeedbackNexus7CollectENS0_6HandleINS0_6StringEEEi, @function
_ZN2v88internal13FeedbackNexus7CollectENS0_6HandleINS0_6StringEEEi:
.LFB19594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L1212
	movq	(%rax), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %r14
	subq	$37592, %r14
.L1167:
	movl	16(%rbx), %eax
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %r15
	cmpq	%r15, 3840(%r14)
	je	.L1213
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1170
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
	movq	%rax, %r13
.L1169:
	movq	1176(%r14), %rax
	movl	%r12d, %edi
	movq	15(%rax), %rsi
	call	_Z11halfsiphashjm@PLT
	movslq	35(%r15), %rsi
	movq	88(%r14), %r11
	leaq	-1(%r15), %rcx
	movq	96(%r14), %r10
	movl	$1, %edi
	subl	$1, %esi
	andl	%esi, %eax
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1214:
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	cvttsd2siq	%xmm0, %rdx
	cmpl	%edx, %r12d
	je	.L1176
.L1173:
	addl	%edi, %eax
	addl	$1, %edi
	andl	%esi, %eax
.L1177:
	leal	(%rax,%rax), %r9d
	leal	40(,%r9,8), %edx
	movslq	%edx, %rdx
	movq	(%rdx,%rcx), %rdx
	cmpq	%rdx, %r11
	je	.L1196
	cmpq	%rdx, %r10
	je	.L1173
	testb	$1, %dl
	je	.L1214
	movsd	7(%rdx), %xmm0
	cvttsd2siq	%xmm0, %rdx
	cmpl	%edx, %r12d
	jne	.L1173
.L1176:
	cmpl	$-1, %eax
	je	.L1196
	movq	0(%r13), %rdx
	leal	48(,%r9,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1180
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L1181:
	movl	11(%rsi), %edx
	testl	%edx, %edx
	jne	.L1215
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	-72(%rbp), %rdx
	movq	%r15, %rsi
.L1211:
	movq	%r14, %rdi
	call	_ZN2v88internal9ArrayList3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal22SimpleNumberDictionary3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE@PLT
	movq	%rax, %r13
.L1179:
	movq	(%rbx), %rdx
	movq	0(%r13), %rax
	testq	%rdx, %rdx
	je	.L1216
	movq	(%rdx), %r12
.L1191:
	movl	16(%rbx), %edx
	leal	48(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r12,%rdx), %r13
	movq	%rax, 0(%r13)
	testb	$1, %al
	je	.L1165
	cmpl	$3, %eax
	je	.L1165
	movq	%rax, %r14
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r14
	testl	$262144, %eax
	jne	.L1217
.L1193:
	testb	$24, %al
	je	.L1165
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1218
	.p2align 4,,10
	.p2align 3
.L1165:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1219
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1170:
	.cfi_restore_state
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L1220
.L1171:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, 0(%r13)
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1196:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9ArrayList3NewEPNS0_7IsolateEi@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1215:
	leaq	-64(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L1189:
	movq	15(%rsi), %rax
	sarq	$32, %rax
	cmpl	%edx, %eax
	jle	.L1184
	movq	(%r15), %rax
	movq	23(%rax,%rdx,8), %rax
	movq	-72(%rbp), %rcx
	movq	(%rcx), %rsi
	movq	%rax, -64(%rbp)
	cmpq	%rsi, %rax
	je	.L1179
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L1187
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L1188
.L1187:
	movq	-88(%rbp), %rdi
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	movq	-80(%rbp), %rdx
	testb	%al, %al
	jne	.L1179
.L1188:
	movq	(%r15), %rsi
	addq	$1, %rdx
	movl	11(%rsi), %eax
	testl	%eax, %eax
	je	.L1184
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L1221
.L1182:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	jmp	.L1181
	.p2align 4,,10
	.p2align 3
.L1212:
	movq	8(%rdi), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %r14
	subq	$37592, %r14
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1216:
	movq	8(%rbx), %r12
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1213:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9HashTableINS0_22SimpleNumberDictionaryENS0_27SimpleNumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	(%rax), %r15
	movq	%rax, %r13
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1217:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1218:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1220:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L1171
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1182
.L1219:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19594:
	.size	_ZN2v88internal13FeedbackNexus7CollectENS0_6HandleINS0_6StringEEEi, .-_ZN2v88internal13FeedbackNexus7CollectENS0_6HandleINS0_6StringEEEi
	.section	.rodata._ZNK2v88internal13FeedbackNexus26GetTypesForSourcePositionsEj.str1.1,"aMS",@progbits,1
.LC35:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNK2v88internal13FeedbackNexus26GetTypesForSourcePositionsEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus26GetTypesForSourcePositionsEj
	.type	_ZNK2v88internal13FeedbackNexus26GetTypesForSourcePositionsEj, @function
_ZNK2v88internal13FeedbackNexus26GetTypesForSourcePositionsEj:
.LFB19611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L1282
	movq	(%rax), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	subq	$37592, %rbx
.L1224:
	movl	16(%rsi), %eax
	pxor	%xmm0, %xmm0
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %r15
	movups	%xmm0, (%r12)
	movq	$0, 16(%r12)
	cmpq	%r15, 3840(%rbx)
	je	.L1222
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1226
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
	movq	%rax, %r14
.L1227:
	movq	1176(%rbx), %rax
	movl	%r13d, %edi
	movq	15(%rax), %rsi
	call	_Z11halfsiphashjm@PLT
	movslq	35(%r15), %r8
	movq	88(%rbx), %r10
	leaq	-1(%r15), %rdi
	movq	96(%rbx), %r9
	movl	$1, %esi
	subl	$1, %r8d
	andl	%r8d, %eax
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1283:
	sarq	$32, %rcx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	cvttsd2siq	%xmm0, %rcx
	cmpl	%ecx, %r13d
	je	.L1233
.L1230:
	addl	%esi, %eax
	addl	$1, %esi
	andl	%r8d, %eax
.L1234:
	leal	(%rax,%rax), %edx
	leal	40(,%rdx,8), %ecx
	movslq	%ecx, %rcx
	movq	(%rcx,%rdi), %rcx
	cmpq	%rcx, %r10
	je	.L1222
	cmpq	%rcx, %r9
	je	.L1230
	testb	$1, %cl
	je	.L1283
	movsd	7(%rcx), %xmm0
	cvttsd2siq	%xmm0, %rcx
	cmpl	%ecx, %r13d
	jne	.L1230
.L1233:
	cmpl	$-1, %eax
	je	.L1222
	leal	48(,%rdx,8), %eax
	movq	(%r14), %rdx
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1235
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L1236:
	movl	11(%rsi), %edx
	testl	%edx, %edx
	je	.L1222
	movq	%r14, %rcx
	xorl	%r15d, %r15d
	movq	%rbx, %r14
	.p2align 4,,10
	.p2align 3
.L1253:
	movq	15(%rsi), %rax
	sarq	$32, %rax
	cmpl	%r15d, %eax
	jle	.L1222
	movq	(%rcx), %rax
	movq	23(%rax,%r15,8), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1239
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rcx
	movq	8(%r12), %rdx
	movq	%rax, %r13
	cmpq	16(%r12), %rdx
	je	.L1242
.L1287:
	movq	%r13, (%rdx)
	addq	$8, 8(%r12)
.L1243:
	movq	(%rcx), %rsi
	addq	$1, %r15
	movl	11(%rsi), %eax
	testl	%eax, %eax
	jne	.L1253
.L1222:
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1226:
	.cfi_restore_state
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L1284
.L1228:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%r15, (%r14)
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1282:
	movq	8(%rsi), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	subq	$37592, %rbx
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1235:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L1285
.L1237:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1239:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L1286
.L1241:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	movq	8(%r12), %rdx
	cmpq	16(%r12), %rdx
	jne	.L1287
.L1242:
	movq	(%r12), %r8
	movq	%rdx, %r9
	movabsq	$1152921504606846975, %rbx
	subq	%r8, %r9
	movq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	je	.L1288
	testq	%rax, %rax
	je	.L1255
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1289
.L1245:
	movq	%rsi, %rdi
	movq	%rcx, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	movq	%rax, %rbx
	movq	-88(%rbp), %rcx
	addq	%rax, %rsi
	leaq	8(%rax), %rax
.L1246:
	movq	%r13, (%rbx,%r9)
	cmpq	%r8, %rdx
	je	.L1247
	leaq	-8(%rdx), %rdi
	leaq	15(%rbx), %rax
	subq	%r8, %rdi
	subq	%r8, %rax
	movq	%rdi, %r9
	shrq	$3, %r9
	cmpq	$30, %rax
	jbe	.L1258
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r9
	je	.L1258
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L1249:
	movdqu	(%r8,%rdx), %xmm1
	movups	%xmm1, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rax
	jne	.L1249
	movq	%r9, %r10
	andq	$-2, %r10
	leaq	0(,%r10,8), %rdx
	leaq	(%r8,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%r9, %r10
	je	.L1251
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1251:
	leaq	16(%rbx,%rdi), %rax
.L1247:
	testq	%r8, %r8
	je	.L1252
	movq	%r8, %rdi
	movq	%rcx, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
.L1252:
	movq	%rbx, %xmm0
	movq	%rax, %xmm2
	movq	%rsi, 16(%r12)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1286:
	movq	%r14, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1289:
	testq	%rdi, %rdi
	jne	.L1290
	movl	$8, %eax
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1255:
	movl	$8, %esi
	jmp	.L1245
	.p2align 4,,10
	.p2align 3
.L1258:
	movq	%rbx, %r9
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	(%rax), %r10
	addq	$8, %rax
	addq	$8, %r9
	movq	%r10, -8(%r9)
	cmpq	%rax, %rdx
	jne	.L1248
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L1228
.L1285:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1237
.L1288:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1290:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	%rax, %rsi
	cmovbe	%rdi, %rsi
	salq	$3, %rsi
	jmp	.L1245
	.cfi_endproc
.LFE19611:
	.size	_ZNK2v88internal13FeedbackNexus26GetTypesForSourcePositionsEj, .-_ZNK2v88internal13FeedbackNexus26GetTypesForSourcePositionsEj
	.section	.text._ZNK2v88internal13FeedbackNexus14GetTypeProfileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus14GetTypeProfileEv
	.type	_ZNK2v88internal13FeedbackNexus14GetTypeProfileEv, @function
_ZNK2v88internal13FeedbackNexus14GetTypeProfileEv:
.LFB19628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L1317
	movq	(%rax), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	subq	$37592, %r12
.L1293:
	movl	16(%rdi), %eax
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rsi
	cmpq	%rsi, 3840(%r12)
	je	.L1318
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1299
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L1300:
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1302
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1303:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$39, %ebx
	movl	$3, %r15d
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	movq	0(%r13), %rax
	cmpl	$3, 11(%rax)
	jg	.L1305
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1307:
	addq	$16, %rbx
	cmpl	%r15d, 11(%rax)
	jle	.L1306
.L1305:
	movq	(%rbx,%rax), %rcx
	addl	$2, %r15d
	movq	%rcx, -56(%rbp)
	andl	$1, %ecx
	jne	.L1307
	leal	9(%rbx), %edx
	movslq	%edx, %rdx
	movq	-1(%rdx,%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1308
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1309:
	movq	%r12, %rdi
	addq	$16, %rbx
	call	_ZN2v88internal9ArrayList8ElementsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	(%rax), %rax
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	-56(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	sarq	$32, %rsi
	call	_ZN2v88internal8JSObject14AddDataElementENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	0(%r13), %rax
	cmpl	%r15d, 11(%rax)
	jg	.L1305
.L1306:
	movq	(%r14), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1299:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L1319
.L1301:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1308:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1320
.L1310:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1302:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1321
.L1304:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	8(%rdi), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	subq	$37592, %r12
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1318:
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1295
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1296:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1295:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1322
.L1297:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1320:
	movq	%r12, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1321:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1297
	.cfi_endproc
.LFE19628:
	.size	_ZNK2v88internal13FeedbackNexus14GetTypeProfileEv, .-_ZNK2v88internal13FeedbackNexus14GetTypeProfileEv
	.section	.text._ZN2v88internal13FeedbackNexus16ResetTypeProfileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13FeedbackNexus16ResetTypeProfileEv
	.type	_ZN2v88internal13FeedbackNexus16ResetTypeProfileEv, @function
_ZN2v88internal13FeedbackNexus16ResetTypeProfileEv:
.LFB19629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L1337
	movq	(%rax), %r12
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	24(%r14), %rax
	movq	-33752(%rax), %rax
.L1325:
	movl	16(%rdi), %edx
	leal	48(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r12,%rdx), %r13
	movq	%rax, 0(%r13)
	testb	$1, %al
	je	.L1323
	cmpl	$3, %eax
	je	.L1323
	movq	%rax, %r15
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r15
	testl	$262144, %eax
	jne	.L1338
.L1327:
	testb	$24, %al
	je	.L1323
	testb	$24, 8(%r14)
	je	.L1339
.L1323:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1337:
	.cfi_restore_state
	movq	8(%rdi), %r12
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	24(%r14), %rax
	movq	-33752(%rax), %rax
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1338:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L1339:
	addq	$8, %rsp
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE19629:
	.size	_ZN2v88internal13FeedbackNexus16ResetTypeProfileEv, .-_ZN2v88internal13FeedbackNexus16ResetTypeProfileEv
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_:
.LFB22722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L1341
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1341:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1367
	testq	%rax, %rax
	je	.L1352
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1368
.L1344:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L1345:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L1346
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L1355
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L1355
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L1348:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L1348
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L1350
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1350:
	leaq	16(%r13,%rsi), %rax
.L1346:
	testq	%r14, %r14
	je	.L1351
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L1351:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1368:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L1369
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1352:
	movl	$8, %r15d
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1355:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L1347:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L1347
	jmp	.L1350
.L1367:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1369:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L1344
	.cfi_endproc
.LFE22722:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZNK2v88internal13FeedbackNexus11ExtractMapsEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus11ExtractMapsEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EE
	.type	_ZNK2v88internal13FeedbackNexus11ExtractMapsEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EE, @function
_ZNK2v88internal13FeedbackNexus11ExtractMapsEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EE:
.LFB19568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L1407
	movq	(%rax), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	subq	$37592, %r12
.L1372:
	movl	16(%rcx), %eax
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internalL22IsPropertyNameFeedbackENS0_11MaybeObjectE
	movq	%rbx, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L1408
	testb	%al, %al
	jne	.L1381
	cmpq	$3, %rdx
	je	.L1379
.L1395:
	xorl	%r15d, %r15d
.L1370:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1409
	addq	$56, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1379:
	.cfi_restore_state
	cmpl	$3, %ebx
	je	.L1395
	movq	41112(%r12), %rdi
	andq	$-3, %rbx
	testq	%rdi, %rdi
	je	.L1390
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1398:
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	movl	$1, %r15d
	call	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L1410
	movq	(%rax), %rdx
.L1383:
	movl	16(%rcx), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rbx
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1407:
	movq	8(%rdi), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	subq	$37592, %r12
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1408:
	movq	-1(%rbx), %rdx
	movzwl	11(%rdx), %edx
	subw	$148, %dx
	cmpw	$1, %dx
	jbe	.L1411
	testb	%al, %al
	jne	.L1381
	movq	%rbx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	%rbx, -33824(%rax)
	jne	.L1395
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L1412
	movq	(%rax), %rdx
.L1394:
	movl	16(%rcx), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	jne	.L1395
	cmpl	$3, %eax
	je	.L1395
	movq	41112(%r12), %rdi
	andq	$-3, %rax
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1413
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1411:
	testb	%al, %al
	jne	.L1381
.L1382:
	movl	11(%rbx), %eax
	leaq	7(%rbx), %rdx
	testl	%eax, %eax
	jle	.L1395
	leaq	-64(%rbp), %rax
	addq	$15, %rbx
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movq	%rax, -80(%rbp)
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1386:
	addq	$16, %rbx
	cmpl	%r14d, 4(%rdx)
	jle	.L1370
.L1385:
	movq	(%rbx), %rsi
	addl	$2, %r14d
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	jne	.L1386
	cmpl	$3, %esi
	je	.L1386
	movq	41112(%r12), %rdi
	andq	$-3, %rsi
	testq	%rdi, %rdi
	je	.L1387
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %rdx
.L1388:
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rdx, -72(%rbp)
	addl	$1, %r15d
	movq	%rax, -64(%rbp)
	call	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	movq	-72(%rbp), %rdx
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1387:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1414
.L1389:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1390:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1415
.L1392:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rbx, (%rax)
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	%r12, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rsi
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1410:
	movq	8(%rcx), %rdx
	jmp	.L1383
.L1415:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1392
.L1412:
	movq	8(%rcx), %rdx
	jmp	.L1394
.L1413:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1416
.L1399:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r14, (%rax)
	jmp	.L1398
.L1409:
	call	__stack_chk_fail@PLT
.L1416:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1399
	.cfi_endproc
.LFE19568:
	.size	_ZNK2v88internal13FeedbackNexus11ExtractMapsEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EE, .-_ZNK2v88internal13FeedbackNexus11ExtractMapsEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EE
	.section	.text._ZNK2v88internal13FeedbackNexus11GetFirstMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus11GetFirstMapEv
	.type	_ZNK2v88internal13FeedbackNexus11GetFirstMapEv, @function
_ZNK2v88internal13FeedbackNexus11GetFirstMapEv:
.LFB19539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	leaq	-48(%rbp), %rsi
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZNK2v88internal13FeedbackNexus11ExtractMapsEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EE
	movq	-48(%rbp), %rdi
	cmpq	%rdi, -40(%rbp)
	je	.L1418
	movq	(%rdi), %rax
	movq	(%rax), %r12
.L1419:
	call	_ZdlPv@PLT
.L1420:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1426
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1418:
	.cfi_restore_state
	xorl	%r12d, %r12d
	testq	%rdi, %rdi
	je	.L1420
	jmp	.L1419
.L1426:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19539:
	.size	_ZNK2v88internal13FeedbackNexus11GetFirstMapEv, .-_ZNK2v88internal13FeedbackNexus11GetFirstMapEv
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB22787:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L1441
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1437
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1442
.L1429:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1436:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1443
	testq	%r13, %r13
	jg	.L1432
	testq	%r9, %r9
	jne	.L1435
.L1433:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1443:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1432
.L1435:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1442:
	testq	%rsi, %rsi
	jne	.L1430
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1436
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1433
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1437:
	movl	$4, %r14d
	jmp	.L1429
.L1441:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1430:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L1429
	.cfi_endproc
.LFE22787:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZNK2v88internal13FeedbackNexus18GetSourcePositionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus18GetSourcePositionsEv
	.type	_ZNK2v88internal13FeedbackNexus18GetSourcePositionsEv, @function
_ZNK2v88internal13FeedbackNexus18GetSourcePositionsEv:
.LFB19595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L1462
	movq	(%rax), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	subq	$37592, %rbx
.L1446:
	movl	16(%rsi), %eax
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rsi
	cmpq	%rsi, 3840(%rbx)
	je	.L1444
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1448
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L1449:
	cmpl	$3, 11(%rsi)
	movl	$39, %r12d
	movl	$3, %ebx
	leaq	-60(%rbp), %r15
	jg	.L1451
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	(%r14), %rsi
	addq	$16, %r12
	cmpl	%ebx, 11(%rsi)
	jle	.L1444
.L1451:
	movq	(%rsi,%r12), %rdx
	addl	$2, %ebx
	testb	$1, %dl
	jne	.L1452
	sarq	$32, %rdx
	movq	8(%r13), %rsi
	movl	%edx, -60(%rbp)
	cmpq	16(%r13), %rsi
	je	.L1453
	movl	%edx, (%rsi)
	addq	$16, %r12
	addq	$4, 8(%r13)
	movq	(%r14), %rsi
	cmpl	%ebx, 11(%rsi)
	jg	.L1451
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1463
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1448:
	.cfi_restore_state
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L1464
.L1450:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1453:
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1462:
	movq	8(%rsi), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	subq	$37592, %rbx
	jmp	.L1446
.L1464:
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1450
.L1463:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19595:
	.size	_ZNK2v88internal13FeedbackNexus18GetSourcePositionsEv, .-_ZNK2v88internal13FeedbackNexus18GetSourcePositionsEv
	.section	.text._ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	.type	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_, @function
_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_:
.LFB22917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r12
	movq	%r13, %rax
	subq	%r12, %rax
	cmpq	$2147483647, %rax
	je	.L1503
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r14
	movq	%rsi, %rbx
	subq	%r12, %rdx
	testq	%rax, %rax
	je	.L1483
	leaq	(%rax,%rax), %rdi
	movl	$2147483648, %esi
	movl	$2147483647, %ecx
	cmpq	%rdi, %rax
	jbe	.L1504
.L1467:
	movq	(%r14), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L1505
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1470:
	leaq	(%rax,%rcx), %rsi
	leaq	1(%rax), %rcx
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1504:
	testq	%rdi, %rdi
	jne	.L1506
	movl	$1, %ecx
	xorl	%esi, %esi
	xorl	%eax, %eax
.L1468:
	movzbl	(%r15), %edi
	movb	%dil, (%rax,%rdx)
	cmpq	%r12, %rbx
	je	.L1471
	leaq	-1(%rbx), %rdx
	subq	%r12, %rdx
	cmpq	$14, %rdx
	jbe	.L1472
	leaq	15(%rax), %rdx
	subq	%r12, %rdx
	cmpq	$30, %rdx
	jbe	.L1472
	movq	%rbx, %r8
	xorl	%edx, %edx
	subq	%r12, %r8
	movq	%r8, %rcx
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L1473:
	movdqu	(%r12,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1473
	movq	%r8, %rdi
	andq	$-16, %rdi
	leaq	(%r12,%rdi), %rdx
	leaq	(%rax,%rdi), %rcx
	cmpq	%rdi, %r8
	je	.L1476
	movzbl	(%rdx), %edi
	movb	%dil, (%rcx)
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	1(%rdx), %edi
	movb	%dil, 1(%rcx)
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	2(%rdx), %edi
	movb	%dil, 2(%rcx)
	leaq	3(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	3(%rdx), %edi
	movb	%dil, 3(%rcx)
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	4(%rdx), %edi
	movb	%dil, 4(%rcx)
	leaq	5(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	5(%rdx), %edi
	movb	%dil, 5(%rcx)
	leaq	6(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	6(%rdx), %edi
	movb	%dil, 6(%rcx)
	leaq	7(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	7(%rdx), %edi
	movb	%dil, 7(%rcx)
	leaq	8(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	8(%rdx), %edi
	movb	%dil, 8(%rcx)
	leaq	9(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	9(%rdx), %edi
	movb	%dil, 9(%rcx)
	leaq	10(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	10(%rdx), %edi
	movb	%dil, 10(%rcx)
	leaq	11(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	11(%rdx), %edi
	movb	%dil, 11(%rcx)
	leaq	12(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	12(%rdx), %edi
	movb	%dil, 12(%rcx)
	leaq	13(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	13(%rdx), %edi
	movb	%dil, 13(%rcx)
	leaq	14(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L1476
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L1476:
	movq	%rbx, %rdx
	subq	%r12, %rdx
	leaq	1(%rax,%rdx), %rcx
.L1471:
	cmpq	%r13, %rbx
	je	.L1477
	leaq	15(%rbx), %rdx
	subq	%rcx, %rdx
	cmpq	$30, %rdx
	jbe	.L1478
	movq	%rbx, %rdx
	notq	%rdx
	addq	%r13, %rdx
	cmpq	$14, %rdx
	jbe	.L1478
	movq	%r13, %r9
	xorl	%edx, %edx
	subq	%rbx, %r9
	movq	%r9, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L1479:
	movdqu	(%rbx,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L1479
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rcx,%r8), %rdi
	leaq	(%rbx,%r8), %rdx
	cmpq	%r8, %r9
	je	.L1482
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %r13
	je	.L1482
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L1482:
	subq	%rbx, %r13
	addq	%r13, %rcx
.L1477:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rsi, 24(%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1483:
	.cfi_restore_state
	movl	$8, %esi
	movl	$1, %ecx
	jmp	.L1467
.L1505:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1478:
	movq	%r13, %r8
	xorl	%edx, %edx
	subq	%rbx, %r8
	.p2align 4,,10
	.p2align 3
.L1481:
	movzbl	(%rbx,%rdx), %edi
	movb	%dil, (%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L1481
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1472:
	movq	%rbx, %rdi
	xorl	%edx, %edx
	subq	%r12, %rdi
	.p2align 4,,10
	.p2align 3
.L1475:
	movzbl	(%r12,%rdx), %ecx
	movb	%cl, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L1475
	jmp	.L1476
.L1506:
	cmpq	%rsi, %rdi
	cmovb	%rdi, %rcx
	leaq	7(%rcx), %rsi
	andq	$-8, %rsi
	jmp	.L1467
.L1503:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22917:
	.size	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_, .-_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	.section	.text._ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE
	.type	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE, @function
_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE:
.LFB19509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r8, %rbx
	subq	8(%rdi), %rbx
	cmpl	$23, %esi
	ja	.L1518
	leaq	.L1510(%rip), %rcx
	movl	%esi, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE,"a",@progbits
	.align 4
	.align 4
.L1510:
	.long	.L1509-.L1510
	.long	.L1511-.L1510
	.long	.L1511-.L1510
	.long	.L1511-.L1510
	.long	.L1511-.L1510
	.long	.L1511-.L1510
	.long	.L1511-.L1510
	.long	.L1511-.L1510
	.long	.L1511-.L1510
	.long	.L1511-.L1510
	.long	.L1511-.L1510
	.long	.L1511-.L1510
	.long	.L1511-.L1510
	.long	.L1511-.L1510
	.long	.L1511-.L1510
	.long	.L1518-.L1510
	.long	.L1518-.L1510
	.long	.L1511-.L1510
	.long	.L1518-.L1510
	.long	.L1518-.L1510
	.long	.L1518-.L1510
	.long	.L1518-.L1510
	.long	.L1511-.L1510
	.long	.L1509-.L1510
	.section	.text._ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE
	.p2align 4,,10
	.p2align 3
.L1511:
	movb	%sil, -25(%rbp)
	movl	$2, %r12d
	cmpq	24(%rdi), %r8
	je	.L1512
.L1524:
	movb	%sil, (%r8)
	addq	$1, 16(%rdi)
.L1513:
	cmpl	$1, %r12d
	je	.L1514
	movb	$0, -25(%rbp)
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L1515
	movb	$0, (%rsi)
	addq	$1, 16(%rdi)
.L1514:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1523
	addq	$32, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1515:
	.cfi_restore_state
	leaq	-25(%rbp), %rdx
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L1514
	.p2align 4,,10
	.p2align 3
.L1518:
	movb	%sil, -25(%rbp)
	movl	$1, %r12d
	cmpq	24(%rdi), %r8
	jne	.L1524
	.p2align 4,,10
	.p2align 3
.L1512:
	leaq	-25(%rbp), %rdx
	movq	%r8, %rsi
	movq	%rdi, -40(%rbp)
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	-40(%rbp), %rdi
	jmp	.L1513
.L1509:
	leaq	.LC24(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1523:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19509:
	.size	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE, .-_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE
	.section	.rodata._ZN2v88internal18FeedbackVectorSpec18AddTypeProfileSlotEv.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"FeedbackVectorSpec::kTypeProfileSlotIndex == FeedbackVector::GetIndex(slot)"
	.section	.text._ZN2v88internal18FeedbackVectorSpec18AddTypeProfileSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FeedbackVectorSpec18AddTypeProfileSlotEv
	.type	_ZN2v88internal18FeedbackVectorSpec18AddTypeProfileSlotEv, @function
_ZN2v88internal18FeedbackVectorSpec18AddTypeProfileSlotEv:
.LFB19510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$18, -25(%rbp)
	movq	%rsi, %rbx
	subq	8(%rdi), %rbx
	cmpq	24(%rdi), %rsi
	je	.L1526
	movb	$18, (%rsi)
	addq	$1, 16(%rdi)
	testl	%ebx, %ebx
	jne	.L1531
.L1528:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1532
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1526:
	.cfi_restore_state
	leaq	-25(%rbp), %rdx
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	testl	%ebx, %ebx
	je	.L1528
	.p2align 4,,10
	.p2align 3
.L1531:
	leaq	.LC36(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1532:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19510:
	.size	_ZN2v88internal18FeedbackVectorSpec18AddTypeProfileSlotEv, .-_ZN2v88internal18FeedbackVectorSpec18AddTypeProfileSlotEv
	.section	.text._ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB23346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$576460752303423487, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L1552
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L1543
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L1553
.L1535:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	16(%r14), %r8
.L1542:
	movq	8(%rdx), %rsi
	movl	(%rdx), %edx
	addq	%r14, %rcx
	movl	%edx, (%rcx)
	movq	%rsi, 8(%rcx)
	cmpq	%r15, %rbx
	je	.L1537
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L1538:
	movl	(%rdx), %edi
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movl	%edi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1538
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	16(%r14,%rdx), %r8
.L1537:
	cmpq	%r12, %rbx
	je	.L1539
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L1540:
	movq	8(%rdx), %rsi
	movl	(%rdx), %edi
	addq	$16, %rdx
	addq	$16, %rcx
	movl	%edi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L1540
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L1539:
	testq	%r15, %r15
	je	.L1541
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L1541:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1553:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L1536
	movl	$16, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L1542
	.p2align 4,,10
	.p2align 3
.L1543:
	movl	$16, %esi
	jmp	.L1535
.L1536:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$4, %rax
	movq	%rax, %rsi
	jmp	.L1535
.L1552:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23346:
	.size	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZNK2v88internal13FeedbackNexus22ExtractMapsAndHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus22ExtractMapsAndHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EE
	.type	_ZNK2v88internal13FeedbackNexus22ExtractMapsAndHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EE, @function
_ZNK2v88internal13FeedbackNexus22ExtractMapsAndHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EE:
.LFB19569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L1604
	movq	(%rax), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	subq	$37592, %rbx
.L1556:
	movl	16(%r8), %eax
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rsi
	movq	%rsi, %rdi
	call	_ZN2v88internalL22IsPropertyNameFeedbackENS0_11MaybeObjectE
	movq	%rsi, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L1605
	testb	%al, %al
	jne	.L1565
	cmpq	$3, %rdx
	je	.L1563
.L1568:
	xorl	%r14d, %r14d
.L1554:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1606
	addq	$88, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1563:
	.cfi_restore_state
	cmpl	$3, %esi
	je	.L1568
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L1607
	movq	(%rax), %rdx
.L1585:
	movl	16(%r8), %eax
	xorl	%r14d, %r14d
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %r15
	cmpl	$3, %r15d
	je	.L1554
	movq	41112(%rbx), %rdi
	andq	$-3, %rsi
	testq	%rdi, %rdi
	je	.L1587
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1588:
	movq	-88(%rbp), %rdi
	leaq	-80(%rbp), %r14
	movq	%rax, -80(%rbp)
	movq	%r14, %rsi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	movq	%r15, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L1608
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1595
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1596:
	movl	$1, %edx
.L1594:
	movl	%edx, -80(%rbp)
	movq	8(%r12), %rsi
	movq	%rax, -72(%rbp)
	cmpq	16(%r12), %rsi
	je	.L1598
	movl	%edx, (%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 8(%r12)
.L1599:
	movl	$1, %r14d
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	-1(%rsi), %rdx
	movzwl	11(%rdx), %edx
	subw	$148, %dx
	cmpw	$1, %dx
	jbe	.L1609
	testb	%al, %al
	je	.L1568
	.p2align 4,,10
	.p2align 3
.L1565:
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L1610
	movq	(%rax), %rdx
.L1567:
	movl	16(%r8), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rsi
	jmp	.L1566
	.p2align 4,,10
	.p2align 3
.L1604:
	movq	8(%rdi), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	subq	$37592, %rbx
	jmp	.L1556
	.p2align 4,,10
	.p2align 3
.L1609:
	testb	%al, %al
	jne	.L1565
.L1566:
	movl	11(%rsi), %eax
	leaq	7(%rsi), %r9
	testl	%eax, %eax
	jle	.L1568
	leaq	-1(%rsi), %rax
	leaq	15(%rsi), %r13
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1570:
	addq	$16, %r13
	cmpl	%r15d, 4(%r9)
	jle	.L1554
.L1569:
	movq	0(%r13), %rsi
	movl	%r15d, %edx
	addl	$2, %r15d
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	jne	.L1570
	cmpl	$3, %esi
	je	.L1570
	movq	-96(%rbp), %rcx
	leal	24(,%rdx,8), %eax
	movq	(%rax,%rcx), %r11
	cmpl	$3, %r11d
	je	.L1570
	movq	41112(%rbx), %rdi
	andq	$-3, %rsi
	testq	%rdi, %rdi
	je	.L1571
	movq	%r9, -112(%rbp)
	movq	%r11, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %r11
	movq	-112(%rbp), %r9
.L1572:
	movq	-120(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	%r9, -112(%rbp)
	movq	%r11, -104(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	movq	-104(%rbp), %r11
	movq	-112(%rbp), %r9
	movq	%r11, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L1611
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1579
	movq	%r11, %rsi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %r9
.L1580:
	movl	$1, %edx
.L1578:
	movl	%edx, -80(%rbp)
	movq	8(%r12), %rsi
	movq	%rax, -72(%rbp)
	cmpq	16(%r12), %rsi
	je	.L1582
	movl	%edx, (%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 8(%r12)
.L1583:
	addl	$1, %r14d
	jmp	.L1570
	.p2align 4,,10
	.p2align 3
.L1571:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1612
.L1573:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1579:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1613
.L1581:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r11, (%rax)
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1611:
	movq	41112(%rbx), %rdi
	andq	$-3, %r11
	testq	%rdi, %rdi
	je	.L1575
	movq	%r11, %rsi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %r9
	xorl	%edx, %edx
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1582:
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r9, -104(%rbp)
	call	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	-104(%rbp), %r9
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1612:
	movq	%rbx, %rdi
	movq	%r9, -128(%rbp)
	movq	%r11, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %r9
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %rsi
	jmp	.L1573
.L1587:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1614
.L1589:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L1575:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1615
.L1577:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	xorl	%edx, %edx
	movq	%r11, (%rax)
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1607:
	movq	8(%r8), %rdx
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1610:
	movq	8(%r8), %rdx
	jmp	.L1567
.L1613:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r11, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r11
	jmp	.L1581
.L1595:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1616
.L1597:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r15, (%rax)
	jmp	.L1596
.L1608:
	movq	41112(%rbx), %rdi
	andq	$-3, %r15
	testq	%rdi, %rdi
	je	.L1591
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	xorl	%edx, %edx
	jmp	.L1594
.L1598:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L1599
.L1615:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r11, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r11
	jmp	.L1577
.L1591:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1617
.L1593:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	xorl	%edx, %edx
	movq	%r15, (%rax)
	jmp	.L1594
.L1614:
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	jmp	.L1589
.L1616:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1597
.L1617:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1593
.L1606:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19569:
	.size	_ZNK2v88internal13FeedbackNexus22ExtractMapsAndHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EE, .-_ZNK2v88internal13FeedbackNexus22ExtractMapsAndHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EE
	.section	.text._ZNK2v88internal13FeedbackNexus22GetKeyedAccessLoadModeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus22GetKeyedAccessLoadModeEv
	.type	_ZNK2v88internal13FeedbackNexus22GetKeyedAccessLoadModeEv, @function
_ZNK2v88internal13FeedbackNexus22GetKeyedAccessLoadModeEv:
.LFB19572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	$0, -48(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZNK2v88internal13FeedbackNexus10GetKeyTypeEv
	cmpl	$1, %eax
	je	.L1619
	leaq	-64(%rbp), %rdx
	leaq	-96(%rbp), %rsi
	call	_ZNK2v88internal13FeedbackNexus22ExtractMapsAndHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EE
	movq	-64(%rbp), %rbx
	movq	-56(%rbp), %r13
	cmpq	%r13, %rbx
	jne	.L1626
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1643:
	testq	%rax, %rax
	je	.L1624
	movq	(%rax), %rdi
	orq	$2, %rdi
	call	_ZN2v88internal11LoadHandler22GetKeyedAccessLoadModeENS0_11MaybeObjectE@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L1642
.L1625:
	addq	$16, %rbx
	cmpq	%rbx, %r13
	je	.L1642
.L1626:
	movl	(%rbx), %edx
	movq	8(%rbx), %rax
	testl	%edx, %edx
	je	.L1643
	testq	%rax, %rax
	je	.L1624
	movq	(%rax), %rdi
	call	_ZN2v88internal11LoadHandler22GetKeyedAccessLoadModeENS0_11MaybeObjectE@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L1625
.L1642:
	movq	-64(%rbp), %r13
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1619:
	movq	-64(%rbp), %r13
	xorl	%r12d, %r12d
.L1620:
	testq	%r13, %r13
	je	.L1627
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1627:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1618
	call	_ZdlPv@PLT
.L1618:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1644
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1624:
	.cfi_restore_state
	leaq	.LC28(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1630:
	xorl	%r12d, %r12d
	jmp	.L1620
.L1644:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19572:
	.size	_ZNK2v88internal13FeedbackNexus22GetKeyedAccessLoadModeEv, .-_ZNK2v88internal13FeedbackNexus22GetKeyedAccessLoadModeEv
	.section	.text._ZNK2v88internal13FeedbackNexus23GetKeyedAccessStoreModeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13FeedbackNexus23GetKeyedAccessStoreModeEv
	.type	_ZNK2v88internal13FeedbackNexus23GetKeyedAccessStoreModeEv, @function
_ZNK2v88internal13FeedbackNexus23GetKeyedAccessStoreModeEv:
.LFB19584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	$0, -48(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZNK2v88internal13FeedbackNexus10GetKeyTypeEv
	cmpl	$1, %eax
	je	.L1646
	leaq	-64(%rbp), %rdx
	leaq	-96(%rbp), %rsi
	call	_ZNK2v88internal13FeedbackNexus22ExtractMapsAndHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EE
	movq	-64(%rbp), %r12
	movq	-56(%rbp), %r13
	cmpq	%r12, %r13
	je	.L1664
.L1660:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L1676
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L1677
.L1650:
	addq	$16, %r12
	cmpq	%r12, %r13
	jne	.L1660
	.p2align 4,,10
	.p2align 3
.L1646:
	movq	-64(%rbp), %r12
	xorl	%r13d, %r13d
.L1647:
	testq	%r12, %r12
	je	.L1661
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1661:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1645
	call	_ZdlPv@PLT
.L1645:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1678
	addq	$80, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1677:
	.cfi_restore_state
	movq	-1(%rsi), %rdx
	cmpw	$164, 11(%rdx)
	je	.L1679
.L1659:
	movl	59(%rsi), %eax
	cmpl	$-1, %eax
	je	.L1650
	subl	$132, %eax
	cmpl	$19, %eax
	ja	.L1650
	leaq	CSWTCH.236(%rip), %rdx
	movq	-64(%rbp), %r12
	movl	(%rdx,%rax,4), %r13d
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1676:
	leaq	.LC28(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1679:
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L1680
	movq	(%rdx), %rdx
.L1655:
	andq	$-262144, %rdx
	movq	(%rax), %rax
	movq	24(%rdx), %r14
	movq	7(%rax), %rsi
	movq	3520(%r14), %rdi
	subq	$37592, %r14
	testq	%rdi, %rdi
	je	.L1656
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1681
.L1658:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1680:
	movq	8(%rbx), %rdx
	jmp	.L1655
.L1664:
	xorl	%r13d, %r13d
	jmp	.L1647
.L1681:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	jmp	.L1658
.L1678:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19584:
	.size	_ZNK2v88internal13FeedbackNexus23GetKeyedAccessStoreModeEv, .-_ZNK2v88internal13FeedbackNexus23GetKeyedAccessStoreModeEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE, @function
_GLOBAL__sub_I__ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE:
.LFB23929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23929:
	.size	_GLOBAL__sub_I__ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE, .-_GLOBAL__sub_I__ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE
	.section	.rodata.CSWTCH.437,"a"
	.type	CSWTCH.437, @object
	.size	CSWTCH.437, 4
CSWTCH.437:
	.byte	0
	.byte	1
	.byte	3
	.byte	2
	.section	.rodata.CSWTCH.434,"a"
	.align 32
	.type	CSWTCH.434, @object
	.size	CSWTCH.434, 33
CSWTCH.434:
	.byte	0
	.byte	1
	.byte	8
	.byte	2
	.byte	8
	.byte	8
	.byte	8
	.byte	4
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	5
	.byte	6
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	7
	.section	.rodata.CSWTCH.236,"a"
	.align 32
	.type	CSWTCH.236, @object
	.size	CSWTCH.236, 80
CSWTCH.236:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	0
	.long	0
	.long	1
	.long	2
	.long	3
	.long	1
	.long	2
	.long	3
	.long	0
	.long	1
	.long	2
	.long	3
	.long	0
	.long	1
	.long	2
	.long	3
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
