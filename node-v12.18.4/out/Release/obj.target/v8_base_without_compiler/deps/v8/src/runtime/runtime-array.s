	.file	"runtime-array.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4754:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4754:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4755:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4755:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4757:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4757:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB9344:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE9344:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC1:
	.string	"V8.Runtime_Runtime_IsArray"
	.section	.text._ZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateE.isra.0:
.LFB25113:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L47
.L16:
	movq	_ZZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic190(%rip), %rbx
	testq	%rbx, %rbx
	je	.L48
.L18:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L49
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L24
.L26:
	movq	120(%r12), %r12
.L25:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L50
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L52
.L19:
	movq	%rbx, _ZZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic190(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L49:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L53
.L21:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	0(%r13), %rax
	movq	%r14, -120(%rbp)
	testb	$1, %al
	je	.L26
.L24:
	movq	-1(%rax), %rax
	cmpw	$1061, 11(%rax)
	jne	.L26
	movq	112(%r12), %r12
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L47:
	movq	40960(%rsi), %rax
	movl	$209, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L53:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L19
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25113:
	.size	_ZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL37Stats_Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"V8.Runtime_Runtime_ArraySpeciesConstructor"
	.section	.text._ZN2v88internalL37Stats_Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateE.isra.0:
.LFB25122:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L87
.L55:
	movq	_ZZN2v88internalL37Stats_Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic197(%rip), %rbx
	testq	%rbx, %rbx
	je	.L88
.L57:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L89
.L59:
	addl	$1, 41104(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal6Object23ArraySpeciesConstructorEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L90
	movq	(%rax), %r13
.L64:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L67
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L67:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L91
.L54:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L93
.L60:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	movq	(%rdi), %rax
	call	*8(%rax)
.L61:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	movq	(%rdi), %rax
	call	*8(%rax)
.L62:
	leaq	.LC2(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L88:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L94
.L58:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic197(%rip)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L90:
	movq	312(%r12), %r13
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L87:
	movq	40960(%rsi), %rax
	movl	$207, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L93:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L60
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25122:
	.size	_ZN2v88internalL37Stats_Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL22Stats_Runtime_NewArrayEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"V8.Runtime_Runtime_NewArray"
.LC4:
	.string	"args[0].IsJSFunction()"
.LC5:
	.string	"Check failed: %s."
.LC6:
	.string	"args[argc + 1].IsJSReceiver()"
.LC7:
	.string	"args[argc + 2].IsHeapObject()"
	.section	.text._ZN2v88internalL22Stats_Runtime_NewArrayEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22Stats_Runtime_NewArrayEiPmPNS0_7IsolateE, @function
_ZN2v88internalL22Stats_Runtime_NewArrayEiPmPNS0_7IsolateE:
.LFB20088:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L183
.L96:
	movq	_ZZN2v88internalL22Stats_Runtime_NewArrayEiPmPNS0_7IsolateEE27trace_event_unique_atomic44(%rip), %rbx
	testq	%rbx, %rbx
	je	.L184
.L98:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L185
.L100:
	movq	41088(%r15), %rax
	leal	-3(%r14), %edx
	addl	$1, 41104(%r15)
	leaq	-8(%r12), %rcx
	movslq	%edx, %rdx
	movq	%rcx, -168(%rbp)
	movq	41096(%r15), %r13
	movq	%rax, -184(%rbp)
	movq	(%r12), %rax
	movq	%rdx, -176(%rbp)
	testb	$1, %al
	jne	.L186
.L104:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L184:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L187
.L99:
	movq	%rbx, _ZZN2v88internalL22Stats_Runtime_NewArrayEiPmPNS0_7IsolateEE27trace_event_unique_atomic44(%rip)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L186:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L104
	leal	-16(,%r14,8), %eax
	movq	%r12, %r11
	movslq	%eax, %rsi
	subq	%rsi, %r11
	movq	(%r11), %rsi
	movq	%rsi, %rbx
	notq	%rbx
	andl	$1, %ebx
	movb	%bl, -186(%rbp)
	je	.L188
.L105:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L185:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L189
.L101:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L102
	movq	(%rdi), %rax
	call	*8(%rax)
.L102:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L103
	movq	(%rdi), %rax
	call	*8(%rax)
.L103:
	leaq	.LC3(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L188:
	movq	-1(%rsi), %rsi
	cmpw	$1023, 11(%rsi)
	jbe	.L105
	addl	$8, %eax
	movq	%r12, %rbx
	cltq
	subq	%rax, %rbx
	movq	(%rbx), %rsi
	movq	%rsi, %rax
	notq	%rax
	andl	$1, %eax
	jne	.L190
	movq	-1(%rsi), %rsi
	movb	$1, -185(%rbp)
	cmpw	$121, 11(%rsi)
	cmovne	%rax, %rbx
	xorl	%r8d, %r8d
	cmpl	$1, %edx
	je	.L191
.L110:
	movq	%r12, %rsi
	movq	%r11, %rdx
	movq	%r15, %rdi
	movb	%r8b, -187(%rbp)
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movzbl	-187(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L182
	testq	%rbx, %rbx
	je	.L114
	movq	(%rbx), %rax
	movslq	11(%rax), %r12
	movzbl	-185(%rbp), %eax
	movb	%al, -186(%rbp)
	andl	$31, %r12d
.L115:
	testb	%r8b, %r8b
	je	.L181
	testb	$1, %r12b
	je	.L118
	cmpb	$5, %r12b
	jbe	.L181
.L119:
	cmpb	$4, %r12b
	je	.L141
	cmpb	$2, %r12b
	je	.L142
	cmpb	$6, %r12b
	je	.L143
	movzbl	%r12b, %edx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L181:
	movzbl	%r12b, %edx
.L117:
	movq	%r15, %rdi
	call	_ZN2v88internal3Map14AsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	cmpb	$2, %r12b
	movl	$0, %ecx
	movq	%r15, %rdi
	cmovb	%rbx, %rcx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r14
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory17NewJSArrayStorageENS0_6HandleINS0_7JSArrayEEEiiNS0_26ArrayStorageAllocationModeE@PLT
	movq	(%r14), %rax
	leaq	-176(%rbp), %rsi
	movq	%r14, %rdi
	movq	-1(%rax), %rax
	movzbl	14(%rax), %r12d
	call	_ZN2v88internal32ArrayConstructInitializeElementsENS0_6HandleINS0_7JSArrayEEEPNS0_9ArgumentsE@PLT
	testq	%rax, %rax
	je	.L182
	movq	(%r14), %rax
	shrl	$3, %r12d
	movq	-1(%rax), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	testq	%rbx, %rbx
	je	.L124
	cmpb	%dl, %r12b
	je	.L125
.L127:
	movq	(%rbx), %rdx
	movslq	11(%rdx), %rax
	orl	$32, %eax
	salq	$32, %rax
	movq	%rax, 7(%rdx)
	movq	(%r14), %rax
.L126:
	movq	%rax, %r12
.L113:
	movq	-184(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r13
	je	.L133
	movq	%r13, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L133:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L192
.L95:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L193
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movq	312(%r15), %r12
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L191:
	movq	(%rcx), %rax
	movq	%rax, %r14
	notq	%r14
	movl	%r14d, %ecx
	andl	$1, %ecx
	movb	%cl, -185(%rbp)
	jne	.L194
.L111:
	movq	%r12, %rsi
	movq	%r11, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L182
	movb	$1, -185(%rbp)
	xorl	%r8d, %r8d
.L114:
	movq	(%rsi), %rax
	movzbl	14(%rax), %r12d
	shrl	$3, %r12d
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L118:
	testb	%r12b, %r12b
	jne	.L119
	movl	$1, %edx
	movl	$1, %r12d
.L120:
	testq	%rbx, %rbx
	je	.L117
	movq	(%rbx), %rcx
	movzbl	%r12b, %edi
	movslq	11(%rcx), %rax
	andl	$-32, %eax
	orl	%edi, %eax
	salq	$32, %rax
	movq	%rax, 7(%rcx)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L125:
	cmpb	$0, -186(%rbp)
	je	.L127
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L124:
	cmpb	%dl, %r12b
	je	.L128
.L130:
	movq	4496(%r15), %rcx
	movabsq	$4294967296, %rdx
	cmpq	%rdx, 7(%rcx)
	jne	.L126
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate35InvalidateArrayConstructorProtectorEv@PLT
	movq	(%r14), %rax
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L183:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$210, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L190:
	leaq	.LC7(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L192:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L189:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L194:
	sarq	$32, %rax
	cmpq	$33554432, %rax
	ja	.L111
	movzbl	-186(%rbp), %r8d
	testq	%rax, %rax
	je	.L110
	cmpl	$16375, %eax
	movl	%ecx, %r8d
	setle	-185(%rbp)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L128:
	cmpb	$0, -185(%rbp)
	je	.L130
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L141:
	movl	$5, %edx
	movl	$5, %r12d
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$3, %edx
	movl	$3, %r12d
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L143:
	movl	$7, %edx
	movl	$7, %r12d
	jmp	.L120
.L193:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20088:
	.size	_ZN2v88internalL22Stats_Runtime_NewArrayEiPmPNS0_7IsolateE, .-_ZN2v88internalL22Stats_Runtime_NewArrayEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL26Stats_Runtime_ArrayIsArrayEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"V8.Runtime_Runtime_ArrayIsArray"
	.section	.text._ZN2v88internalL26Stats_Runtime_ArrayIsArrayEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Stats_Runtime_ArrayIsArrayEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL26Stats_Runtime_ArrayIsArrayEiPmPNS0_7IsolateE.isra.0:
.LFB25136:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L237
.L196:
	movq	_ZZN2v88internalL26Stats_Runtime_ArrayIsArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic181(%rip), %rbx
	testq	%rbx, %rbx
	je	.L238
.L198:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L239
.L200:
	movl	41104(%r12), %edx
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	leal	1(%rdx), %eax
	movl	%eax, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	je	.L217
	movq	-1(%rax), %rcx
	cmpw	$1061, 11(%rcx)
	je	.L205
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	je	.L206
.L217:
	movq	%rbx, %rcx
.L209:
	movq	120(%r12), %r13
.L208:
	movq	%r14, 41088(%r12)
	movl	%edx, 41104(%r12)
	cmpq	%rcx, %rbx
	je	.L212
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L212:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L240
.L195:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L241
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal7JSProxy7IsArrayENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	jne	.L207
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rcx
	leal	-1(%rax), %edx
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L239:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L242
.L201:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L202
	movq	(%rdi), %rax
	call	*8(%rax)
.L202:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L203
	movq	(%rdi), %rax
	call	*8(%rax)
.L203:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L238:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L243
.L199:
	movq	%rbx, _ZZN2v88internalL26Stats_Runtime_ArrayIsArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic181(%rip)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%rbx, %rcx
.L213:
	movq	112(%r12), %r13
	jmp	.L208
.L207:
	movl	41104(%r12), %ecx
	shrw	$8, %ax
	leal	-1(%rcx), %edx
	movq	41096(%r12), %rcx
	jne	.L213
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L240:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L237:
	movq	40960(%rsi), %rax
	movl	$206, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L243:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L242:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L201
.L241:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25136:
	.size	_ZN2v88internalL26Stats_Runtime_ArrayIsArrayEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL26Stats_Runtime_ArrayIsArrayEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"V8.Runtime_Runtime_TransitionElementsKind"
	.section	.rodata._ZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC10:
	.string	"args[0].IsJSObject()"
.LC11:
	.string	"args[1].IsMap()"
	.section	.text._ZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateE.isra.0:
.LFB25140:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L279
.L245:
	movq	_ZZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateEE27trace_event_unique_atomic24(%rip), %r13
	testq	%r13, %r13
	je	.L280
.L247:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L281
.L249:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L282
.L253:
	leaq	.LC10(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L280:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L283
.L248:
	movq	%r13, _ZZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateEE27trace_event_unique_atomic24(%rip)
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L282:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L253
	movq	-8(%rbx), %rax
	leaq	-8(%rbx), %rdx
	testb	$1, %al
	jne	.L284
.L254:
	leaq	.LC11(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L281:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L285
.L250:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L251
	movq	(%rdi), %rax
	call	*8(%rax)
.L251:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L252
	movq	(%rdi), %rax
	call	*8(%rax)
.L252:
	leaq	.LC9(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L284:
	movq	-1(%rax), %rcx
	cmpw	$68, 11(%rcx)
	jne	.L254
	movzbl	14(%rax), %eax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rcx
	movq	%rbx, %rsi
	shrl	$3, %eax
	movq	(%rcx,%rax,8), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	movq	(%rbx), %r15
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L257
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L257:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L286
.L244:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L287
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	movq	40960(%rsi), %rax
	movl	$212, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L286:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L285:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L283:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L248
.L287:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25140:
	.size	_ZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"V8.Runtime_Runtime_TransitionElementsKindWithKind"
	.section	.rodata._ZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC13:
	.string	"args[1].IsSmi()"
	.section	.text._ZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE.isra.0:
.LFB25141:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L322
.L289:
	movq	_ZZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateEE27trace_event_unique_atomic34(%rip), %r13
	testq	%r13, %r13
	je	.L323
.L291:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L324
.L293:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L325
.L297:
	leaq	.LC10(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L323:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L326
.L292:
	movq	%r13, _ZZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateEE27trace_event_unique_atomic34(%rip)
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L325:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L297
	movq	-8(%rbx), %rsi
	testb	$1, %sil
	jne	.L327
	sarq	$32, %rsi
	movq	%rbx, %rdi
	movzbl	%sil, %esi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	(%rbx), %r15
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L300
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L300:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L328
.L288:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L329
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L330
.L294:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L295
	movq	(%rdi), %rax
	call	*8(%rax)
.L295:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L296
	movq	(%rdi), %rax
	call	*8(%rax)
.L296:
	leaq	.LC12(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L322:
	movq	40960(%rsi), %rax
	movl	$213, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L327:
	leaq	.LC13(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L328:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L330:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L326:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L292
.L329:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25141:
	.size	_ZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"V8.Runtime_Runtime_GrowArrayElements"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC15:
	.string	"args[1].IsNumber()"
	.section	.text._ZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateE.isra.0:
.LFB25142:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L389
.L332:
	movq	_ZZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic160(%rip), %rbx
	testq	%rbx, %rbx
	je	.L390
.L334:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L391
.L336:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L392
.L340:
	leaq	.LC10(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L390:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L393
.L335:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic160(%rip)
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L392:
	movq	-1(%rsi), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L340
	movq	-8(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	movq	%rcx, %rdx
	testb	$1, %al
	je	.L345
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L394
	movq	7(%rax), %rdi
	movsd	.LC17(%rip), %xmm2
	movq	%rdi, %xmm1
	andpd	.LC16(%rip), %xmm1
	movq	%rdi, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L346
	movsd	.LC18(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L395
.L346:
	movabsq	$9218868437227405312, %rax
	testq	%rax, %rdi
	je	.L365
	movq	%rdi, %rax
	shrq	$52, %rax
	andl	$2047, %eax
	movl	%eax, %ecx
	subl	$1075, %ecx
	js	.L396
	cmpl	$31, %ecx
	jg	.L365
	movabsq	$4503599627370495, %rdx
	movq	%rdx, %rax
	addq	$1, %rdx
	andq	%rdi, %rax
	addq	%rax, %rdx
	salq	%cl, %rdx
	movl	%edx, %edx
.L352:
	sarq	$63, %rdi
	orl	$1, %edi
	imull	%edi, %edx
.L345:
	testl	%edx, %edx
	jns	.L350
.L354:
	xorl	%r15d, %r15d
.L356:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L359
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L359:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L397
.L331:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L398
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	xorl	%edx, %edx
.L350:
	movq	15(%rsi), %r15
	cmpl	%edx, 11(%r15)
	ja	.L356
	movq	-1(%rsi), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rcx
	movq	%r13, %rsi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rcx,%rax,8), %rdi
	movq	(%rdi), %rax
	call	*120(%rax)
	testb	%al, %al
	je	.L354
	movq	0(%r13), %rax
	movq	15(%rax), %r15
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L391:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L399
.L337:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L338
	movq	(%rdi), %rax
	call	*8(%rax)
.L338:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L339
	movq	(%rdi), %rax
	call	*8(%rax)
.L339:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L395:
	comisd	.LC19(%rip), %xmm0
	jb	.L346
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L346
	je	.L345
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L396:
	cmpl	$-52, %ecx
	jl	.L365
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rcx
	andq	%rdi, %rdx
	addq	%rcx, %rdx
	movl	$1075, %ecx
	subl	%eax, %ecx
	shrq	%cl, %rdx
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L389:
	movq	40960(%rsi), %rax
	movl	$208, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L394:
	leaq	.LC15(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L397:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L399:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L393:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L335
.L398:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25142:
	.size	_ZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"V8.Runtime_Runtime_ArrayIncludes_Slow"
	.section	.text._ZN2v88internalL32Stats_Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateE.isra.0:
.LFB25143:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L527
.L401:
	movq	_ZZN2v88internalL32Stats_Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateEE28trace_event_unique_atomic206(%rip), %rbx
	testq	%rbx, %rbx
	je	.L528
.L403:
	movq	$0, -240(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L529
.L405:
	movq	41088(%r15), %rax
	movq	41112(%r15), %rdi
	addl	$1, 41104(%r15)
	movq	0(%r13), %rsi
	movq	%rax, -280(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -264(%rbp)
	testq	%rdi, %rdi
	je	.L409
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
	testb	$1, %sil
	jne	.L412
.L415:
	movq	%r14, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L525
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	je	.L530
.L417:
	leaq	2768(%r15), %r12
	testb	$1, %al
	jne	.L424
.L426:
	movl	$-1, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rbx
.L425:
	movq	2768(%r15), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L427
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L427:
	movl	%eax, -160(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -148(%rbp)
	movq	2768(%r15), %rax
	movq	%r15, -136(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L531
.L428:
	movq	%r12, -128(%rbp)
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	movq	$0, -120(%rbp)
	movq	%r14, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -156(%rbp)
	jne	.L429
	movq	-136(%rbp), %rax
	movq	88(%rax), %rsi
	addq	$88, %rax
	testb	$1, %sil
	jne	.L431
.L540:
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r15), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L432
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L433:
	testq	%rax, %rax
	je	.L525
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L436
.L542:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L437:
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -272(%rbp)
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L409:
	movq	%rax, %rcx
	movq	-280(%rbp), %rax
	movq	%rax, %r14
	cmpq	%rcx, %rax
	je	.L532
.L411:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	testb	$1, %sil
	je	.L415
.L412:
	movq	-1(%rsi), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L415
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	jne	.L417
	.p2align 4,,10
	.p2align 3
.L530:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L418
	sarq	$32, %rax
	movq	%rax, -272(%rbp)
	js	.L524
.L423:
	cmpq	$0, -272(%rbp)
	je	.L524
	movq	-16(%r13), %rbx
	leaq	-16(%r13), %rsi
	cmpq	88(%r15), %rbx
	jne	.L439
.L450:
	xorl	%ebx, %ebx
.L440:
	movq	(%r14), %rcx
	leaq	-8(%r13), %rax
	movq	%rax, -304(%rbp)
	movq	-1(%rcx), %rax
	cmpw	$1040, 11(%rax)
	jbe	.L454
	movl	$4294967295, %eax
	cmpq	%rax, -272(%rbp)
	jle	.L533
.L454:
	movq	41096(%r15), %r13
	movl	41104(%r15), %eax
	cmpq	%rbx, -272(%rbp)
	jle	.L486
	leaq	-241(%rbp), %rcx
	movq	%r14, -296(%rbp)
	leaq	-160(%rbp), %r12
	movq	%rcx, -288(%rbp)
.L460:
	addl	$1, %eax
	movl	$4294967295, %ecx
	movq	41088(%r15), %r14
	movl	%eax, 41104(%r15)
	movl	$2147483648, %eax
	addq	%rbx, %rax
	cmpq	%rcx, %rax
	ja	.L462
	movq	41112(%r15), %rdi
	movq	%rbx, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L463
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L466:
	movq	-288(%rbp), %r8
	movq	-296(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$3, %r9d
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L534
	movq	-304(%rbp), %rcx
	movq	%r12, %rdi
	movq	(%rcx), %rdx
	movq	%rdx, -160(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal6Object13SameValueZeroES1_@PLT
	testb	%al, %al
	jne	.L535
	movl	41104(%r15), %eax
	movq	41096(%r15), %rdx
	movq	%r14, 41088(%r15)
	subl	$1, %eax
	movl	%eax, 41104(%r15)
	cmpq	%rdx, %r13
	je	.L470
	movq	%r13, 41096(%r15)
	movq	%r15, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	41104(%r15), %eax
	movq	41096(%r15), %rdx
	cmpq	%rbx, -272(%rbp)
	je	.L459
.L471:
	movq	%rdx, %r13
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L441:
	movq	%r15, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	jne	.L444
	.p2align 4,,10
	.p2align 3
.L525:
	movq	312(%r15), %r12
	movl	41104(%r15), %eax
	movq	41096(%r15), %rdx
.L416:
	movq	-280(%rbp), %rcx
	subl	$1, %eax
	movl	%eax, 41104(%r15)
	movq	%rcx, 41088(%r15)
	cmpq	%rdx, -264(%rbp)
	je	.L474
	movq	-264(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, 41096(%r15)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L474:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L536
.L400:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L537
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L538
.L404:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateEE28trace_event_unique_atomic206(%rip)
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L529:
	pxor	%xmm0, %xmm0
	xorl	%r12d, %r12d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L539
.L406:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L407
	movq	(%rdi), %rax
	call	*8(%rax)
.L407:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L408
	movq	(%rdi), %rax
	call	*8(%rax)
.L408:
	leaq	.LC20(%rip), %rax
	movq	%rbx, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r12, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L429:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L525
	movq	(%rax), %rsi
	testb	$1, %sil
	je	.L540
.L431:
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L424:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L426
	movq	%r14, %rbx
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L439:
	movq	%rbx, %rax
	notq	%rax
	testb	$1, %bl
	jne	.L441
	testb	$1, %al
	je	.L443
.L442:
	sarq	$32, %rbx
	jns	.L440
	addq	-272(%rbp), %rbx
	movl	$0, %eax
	cmovs	%rax, %rbx
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L432:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L541
.L434:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	movq	(%rax), %rax
	testb	$1, %al
	je	.L542
.L436:
	movsd	7(%rax), %xmm0
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L418:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L543
	.p2align 4,,10
	.p2align 3
.L524:
	movq	120(%r15), %r12
	movl	41104(%r15), %eax
	movq	41096(%r15), %rdx
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L470:
	addq	$1, %rbx
	cmpq	%rbx, -272(%rbp)
	jne	.L471
.L459:
	movq	120(%r15), %r12
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L462:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	cvtsi2sdq	%rbx, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	%rax, %rcx
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L463:
	movq	%r14, %rcx
	cmpq	%r13, %r14
	je	.L544
.L465:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L534:
	movq	312(%r15), %r12
	movq	%r14, %rax
.L468:
	movq	%rax, 41088(%r15)
	movl	41104(%r15), %eax
	movq	41096(%r15), %rdx
	subl	$1, %eax
	movl	%eax, 41104(%r15)
	cmpq	%rdx, %r13
	je	.L416
	movq	%r13, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L523:
	movl	41104(%r15), %eax
	movq	41096(%r15), %rdx
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L531:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r12
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L527:
	movq	40960(%rsi), %rax
	movl	$204, %edx
	leaq	-200(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L536:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L532:
	movq	%r15, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L539:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC20(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r12
	addq	$64, %rsp
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L538:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L544:
	movq	%r15, %rdi
	movq	%rsi, -312(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L444:
	movq	(%rax), %rbx
	movq	%rbx, %rax
	notq	%rax
	movl	%eax, %edx
	andl	$1, %edx
	testb	$1, %al
	jne	.L442
	testb	%dl, %dl
	jne	.L443
	movsd	7(%rbx), %xmm0
.L447:
	pxor	%xmm1, %xmm1
	cvtsi2sdq	-272(%rbp), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L524
	movsd	.LC17(%rip), %xmm3
	movapd	%xmm0, %xmm2
	andpd	.LC16(%rip), %xmm2
	ucomisd	%xmm2, %xmm3
	jb	.L450
	pxor	%xmm2, %xmm2
	comisd	%xmm0, %xmm2
	ja	.L545
.L517:
	cvttsd2siq	%xmm0, %rbx
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L533:
	movq	-1(%rcx), %rax
	movq	104(%r15), %rsi
	movq	288(%r15), %rdi
	movq	1016(%r15), %r8
	movq	23(%rax), %rax
	cmpq	%rsi, %rax
	je	.L455
	.p2align 4,,10
	.p2align 3
.L458:
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	jbe	.L454
	movq	15(%rax), %rax
	cmpq	%rax, %rdi
	je	.L490
	cmpq	%rax, %r8
	jne	.L454
.L490:
	movq	23(%rdx), %rax
	cmpq	%rax, %rsi
	jne	.L458
.L455:
	movq	-1(%rcx), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movl	%ebx, %r8d
	movq	%r15, %rsi
	movl	-272(%rbp), %r9d
	movq	-304(%rbp), %rcx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	%r14, %rdx
	movq	(%rdi), %rax
	call	*200(%rax)
	testb	%al, %al
	je	.L546
	movzbl	%ah, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r12
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L535:
	movq	112(%r15), %r12
	movq	%r14, %rax
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L543:
	movsd	.LC21(%rip), %xmm0
	addsd	7(%rax), %xmm0
	movq	%xmm0, %rdx
	movq	%xmm0, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L524
	movl	%eax, %eax
	movq	%rax, -272(%rbp)
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L541:
	movq	%r15, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	jmp	.L434
.L486:
	movq	%r13, %rdx
	jmp	.L459
.L443:
	sarq	$32, %rbx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	jmp	.L447
.L546:
	movq	312(%r15), %r12
	jmp	.L523
.L545:
	addsd	%xmm1, %xmm0
	xorl	%ebx, %ebx
	comisd	%xmm0, %xmm2
	jbe	.L517
	jmp	.L440
.L537:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25143:
	.size	_ZN2v88internalL32Stats_Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"V8.Runtime_Runtime_ArrayIndexOf"
	.section	.rodata._ZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC24:
	.string	"Array.prototype.indexOf"
	.section	.text._ZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateE.isra.0:
.LFB25144:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L678
.L548:
	movq	_ZZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic311(%rip), %r12
	testq	%r12, %r12
	je	.L679
.L550:
	movq	$0, -240(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L680
.L552:
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	%rax, -272(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -264(%rbp)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L556
.L559:
	leaq	.LC24(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L584
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	je	.L681
.L561:
	leaq	2768(%r15), %r14
	testb	$1, %al
	jne	.L568
.L570:
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r13
.L569:
	movq	2768(%r15), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L571
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L571:
	movl	%eax, -160(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -148(%rbp)
	movq	2768(%r15), %rax
	movq	%r15, -136(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L682
.L572:
	movq	%r14, -128(%rbp)
	leaq	-160(%rbp), %r14
	movq	%r14, %rdi
	movq	$0, -120(%rbp)
	movq	%r12, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -156(%rbp)
	jne	.L573
	movq	-136(%rbp), %rax
	addq	$88, %rax
.L574:
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L575
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r15), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L576
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L577:
	testq	%rax, %rax
	je	.L584
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L580
.L694:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L581:
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -280(%rbp)
	cmpq	$0, -280(%rbp)
	je	.L564
.L690:
	movq	-16(%rbx), %rax
	leaq	-16(%rbx), %rsi
	testb	$1, %al
	jne	.L582
	cmpq	$16, %rbx
	je	.L584
.L583:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L586:
	pxor	%xmm1, %xmm1
	cvtsi2sdq	-280(%rbp), %xmm1
	comisd	%xmm1, %xmm0
	ja	.L564
	comisd	.LC25(%rip), %xmm0
	jb	.L670
	cvttsd2siq	%xmm0, %r13
	testq	%r13, %r13
	js	.L589
.L591:
	movq	(%r12), %rcx
	leaq	-8(%rbx), %rax
	movq	%rax, -312(%rbp)
	movq	-1(%rcx), %rax
	cmpw	$1040, 11(%rax)
	jbe	.L592
	movl	$4294967295, %eax
	cmpq	%rax, -280(%rbp)
	jle	.L683
.L592:
	movq	41096(%r15), %rbx
	movl	41104(%r15), %eax
	movq	%rbx, -288(%rbp)
	cmpq	%r13, -280(%rbp)
	jle	.L630
	leaq	-249(%rbp), %rbx
	leaq	-248(%rbp), %rcx
	movq	%r12, -304(%rbp)
	movq	%rbx, -296(%rbp)
	leaq	-160(%rbp), %r14
	movq	%rcx, -320(%rbp)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L687:
	movq	41112(%r15), %rdi
	movq	%r13, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L605
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L608:
	movq	-296(%rbp), %r8
	movq	%r14, %rdi
	movq	%rbx, %rcx
	movq	%r15, %rsi
	movq	-304(%rbp), %rdx
	movl	$3, %r9d
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	je	.L677
	shrw	$8, %ax
	je	.L684
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L677
	movq	-312(%rbp), %rcx
	movq	-320(%rbp), %rdi
	movq	(%rcx), %rdx
	movq	%rdx, -248(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal6Object12StrictEqualsES1_@PLT
	testb	%al, %al
	jne	.L685
	movl	41104(%r15), %eax
	movq	%r12, 41088(%r15)
	movq	-288(%rbp), %rbx
	subl	$1, %eax
	movl	%eax, 41104(%r15)
	cmpq	41096(%r15), %rbx
	je	.L613
	movq	%rbx, 41096(%r15)
.L675:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	41096(%r15), %rbx
	movl	41104(%r15), %eax
	movq	%rbx, -288(%rbp)
.L613:
	addq	$1, %r13
	cmpq	%r13, -280(%rbp)
	je	.L686
.L597:
	addl	$1, %eax
	movl	$4294967295, %ecx
	movq	41088(%r15), %r12
	movl	%eax, 41104(%r15)
	movl	$2147483648, %eax
	addq	%r13, %rax
	cmpq	%rcx, %rax
	jbe	.L687
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	cvtsi2sdq	%r13, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L573:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L574
.L584:
	movq	312(%r15), %r12
	movl	41104(%r15), %eax
	movq	41096(%r15), %rdx
.L560:
	movq	-272(%rbp), %rcx
	subl	$1, %eax
	movl	%eax, 41104(%r15)
	movq	%rcx, 41088(%r15)
	cmpq	%rdx, -264(%rbp)
	je	.L619
	movq	-264(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, 41096(%r15)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L619:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L688
.L547:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L689
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L556:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L559
	movq	%rbx, %r12
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	jne	.L561
	.p2align 4,,10
	.p2align 3
.L681:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L562
	sarq	$32, %rax
	movq	%rax, -280(%rbp)
	js	.L564
.L567:
	cmpq	$0, -280(%rbp)
	jne	.L690
	.p2align 4,,10
	.p2align 3
.L564:
	movl	41104(%r15), %eax
	movq	41096(%r15), %rdx
	movabsq	$-4294967296, %r12
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L680:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L691
.L553:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L554
	movq	(%rdi), %rax
	call	*8(%rax)
.L554:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L555
	movq	(%rdi), %rax
	call	*8(%rax)
.L555:
	leaq	.LC23(%rip), %rax
	movq	%r12, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r13, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L679:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L692
.L551:
	movq	%r12, _ZZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic311(%rip)
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L568:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L570
	movq	%r12, %r13
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L576:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L693
.L578:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	movq	(%rax), %rax
	testb	$1, %al
	je	.L694
.L580:
	movsd	7(%rax), %xmm0
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L562:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L564
	movsd	.LC21(%rip), %xmm0
	addsd	7(%rax), %xmm0
	movq	%xmm0, %rdx
	movq	%xmm0, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L564
	movl	%eax, %eax
	movq	%rax, -280(%rbp)
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L575:
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L684:
	movl	41104(%r15), %eax
	movq	%r12, 41088(%r15)
	movq	-288(%rbp), %rcx
	subl	$1, %eax
	movl	%eax, 41104(%r15)
	cmpq	41096(%r15), %rcx
	je	.L613
	movq	%rcx, 41096(%r15)
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L605:
	movq	%r12, %rbx
	cmpq	-288(%rbp), %r12
	je	.L695
.L607:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L682:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r14
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L678:
	movq	40960(%rsi), %rax
	movl	$205, %edx
	leaq	-200(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L688:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L582:
	movq	%r15, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L584
	movq	(%rax), %rax
	testb	$1, %al
	je	.L583
	movsd	7(%rax), %xmm0
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L692:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L691:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC23(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L677:
	movq	%r12, %rax
	movq	312(%r15), %r12
.L610:
	movq	%rax, 41088(%r15)
	movl	41104(%r15), %eax
	movq	41096(%r15), %rdx
	subl	$1, %eax
	movl	%eax, 41104(%r15)
	cmpq	%rdx, -288(%rbp)
	je	.L560
	movq	-288(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, 41096(%r15)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L676:
	movl	41104(%r15), %eax
	movq	41096(%r15), %rdx
	jmp	.L560
.L670:
	movabsq	$-9223372036854775808, %r13
.L589:
	addq	-280(%rbp), %r13
	movl	$0, %eax
	cmovs	%rax, %r13
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L683:
	movq	-1(%rcx), %rax
	movq	104(%r15), %rsi
	movq	288(%r15), %r8
	movq	1016(%r15), %rdi
	movq	23(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L596
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L696:
	movq	15(%rax), %rax
	cmpq	%rax, %rdi
	je	.L637
	cmpq	%rax, %r8
	jne	.L592
.L637:
	movq	23(%rdx), %rax
	cmpq	%rax, %rsi
	je	.L593
.L596:
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	ja	.L696
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L695:
	movq	%r15, %rdi
	movq	%rsi, -328(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-328(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L693:
	movq	%r15, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L686:
	movq	-288(%rbp), %rdx
	movabsq	$-4294967296, %r12
	jmp	.L560
.L685:
	movq	%r12, %rax
	movq	(%rbx), %r12
	jmp	.L610
.L593:
	movq	-1(%rcx), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movl	%r13d, %r8d
	movq	%r15, %rsi
	movl	-280(%rbp), %r9d
	movq	-312(%rbp), %rcx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*208(%rax)
	movq	%rdx, %r12
	testb	%al, %al
	je	.L697
	movl	$2147483648, %eax
	movl	$4294967295, %edx
	addq	%r12, %rax
	cmpq	%rdx, %rax
	ja	.L599
	movq	41112(%r15), %rdi
	salq	$32, %r12
	testq	%rdi, %rdi
	je	.L600
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	jmp	.L676
.L630:
	movabsq	$-4294967296, %r12
	movq	%rbx, %rdx
	jmp	.L560
.L697:
	movq	312(%r15), %r12
	jmp	.L676
.L599:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	cvtsi2sdq	%r12, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r12
	jmp	.L676
.L600:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L698
.L602:
	movq	%r12, (%rax)
	jmp	.L676
.L689:
	call	__stack_chk_fail@PLT
.L698:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L602
	.cfi_endproc
.LFE25144:
	.size	_ZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"V8.Runtime_Runtime_NormalizeElements"
	.align 8
.LC27:
	.string	"!array->HasTypedArrayElements()"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC28:
	.string	"!array->IsJSGlobalProxy()"
	.section	.text._ZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateE.isra.0:
.LFB25145:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L734
.L700:
	movq	_ZZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic148(%rip), %rbx
	testq	%rbx, %rbx
	je	.L735
.L702:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L736
.L704:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L737
.L708:
	leaq	.LC10(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L735:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L738
.L703:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic148(%rip)
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L737:
	movq	-1(%rdx), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L708
	movq	-1(%rdx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$17, %eax
	cmpb	$10, %al
	jbe	.L739
	movq	-1(%rdx), %rax
	cmpw	$1026, 11(%rax)
	je	.L740
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject17NormalizeElementsENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %r13
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L712
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L712:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L741
.L699:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L742
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L736:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L743
.L705:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L706
	movq	(%rdi), %rax
	call	*8(%rax)
.L706:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L707
	movq	(%rdi), %rax
	call	*8(%rax)
.L707:
	leaq	.LC26(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L734:
	movq	40960(%rsi), %rax
	movl	$211, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L739:
	leaq	.LC27(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L740:
	leaq	.LC28(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L741:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L738:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L743:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC26(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L705
.L742:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25145:
	.size	_ZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal30Runtime_TransitionElementsKindEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_TransitionElementsKindEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_TransitionElementsKindEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_TransitionElementsKindEiPmPNS0_7IsolateE:
.LFB20083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L753
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L754
.L746:
	leaq	.LC10(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L754:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L746
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L755
.L747:
	leaq	.LC11(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L755:
	movq	-1(%rax), %rcx
	cmpw	$68, 11(%rcx)
	jne	.L747
	movzbl	14(%rax), %eax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rcx
	shrl	$3, %eax
	movq	(%rcx,%rax,8), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	movq	0(%r13), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L744
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L744:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L753:
	.cfi_restore_state
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rdx, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20083:
	.size	_ZN2v88internal30Runtime_TransitionElementsKindEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_TransitionElementsKindEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE:
.LFB20086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L764
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L765
.L758:
	leaq	.LC10(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L765:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L758
	movq	-8(%rsi), %rsi
	testb	$1, %sil
	jne	.L766
	sarq	$32, %rsi
	movq	%r13, %rdi
	movzbl	%sil, %esi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	0(%r13), %r13
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L756
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L756:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L764:
	.cfi_restore_state
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rdx, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L766:
	.cfi_restore_state
	leaq	.LC13(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20086:
	.size	_ZN2v88internal38Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE, .-_ZN2v88internal38Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal16Runtime_NewArrayEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16Runtime_NewArrayEiPmPNS0_7IsolateE
	.type	_ZN2v88internal16Runtime_NewArrayEiPmPNS0_7IsolateE, @function
_ZN2v88internal16Runtime_NewArrayEiPmPNS0_7IsolateE:
.LFB20089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L831
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	leaq	-8(%rsi), %rcx
	movq	41096(%rdx), %r13
	leal	-3(%rdi), %edx
	movq	%rcx, -72(%rbp)
	movq	%rax, -88(%rbp)
	movq	(%rsi), %rax
	movslq	%edx, %rdx
	movq	%rdx, -80(%rbp)
	testb	$1, %al
	jne	.L832
.L770:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L832:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L770
	leal	-16(,%rdi,8), %eax
	movq	%rsi, %r8
	movslq	%eax, %rdi
	subq	%rdi, %r8
	movq	(%r8), %rdi
	movq	%rdi, %rbx
	notq	%rbx
	andl	$1, %ebx
	movb	%bl, -90(%rbp)
	je	.L833
.L771:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L833:
	movq	-1(%rdi), %rdi
	cmpw	$1023, 11(%rdi)
	jbe	.L771
	addl	$8, %eax
	movq	%rsi, %rbx
	cltq
	subq	%rax, %rbx
	movq	(%rbx), %rdi
	movq	%rdi, %rax
	notq	%rax
	andl	$1, %eax
	jne	.L834
	movq	-1(%rdi), %rdi
	movb	$1, -89(%rbp)
	cmpw	$121, 11(%rdi)
	cmovne	%rax, %rbx
	xorl	%r12d, %r12d
	cmpl	$1, %edx
	je	.L835
.L776:
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L830
	testq	%rbx, %rbx
	je	.L780
	movq	(%rbx), %rax
	movslq	11(%rax), %rcx
	movzbl	-89(%rbp), %eax
	movb	%al, -90(%rbp)
	movl	%ecx, %r14d
	andl	$31, %r14d
.L781:
	testb	%r12b, %r12b
	je	.L829
	testb	$1, %r14b
	je	.L784
	cmpb	$5, %r14b
	jbe	.L829
.L785:
	cmpb	$4, %r14b
	je	.L804
	cmpb	$2, %r14b
	je	.L805
	cmpb	$6, %r14b
	je	.L806
	movzbl	%r14b, %edx
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L829:
	movzbl	%r14b, %edx
.L783:
	movq	%r15, %rdi
	call	_ZN2v88internal3Map14AsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	cmpb	$2, %r14b
	movl	$0, %ecx
	movq	%r15, %rdi
	cmovb	%rbx, %rcx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r14
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory17NewJSArrayStorageENS0_6HandleINS0_7JSArrayEEEiiNS0_26ArrayStorageAllocationModeE@PLT
	movq	(%r14), %rax
	leaq	-80(%rbp), %rsi
	movq	%r14, %rdi
	movq	-1(%rax), %rax
	movzbl	14(%rax), %r12d
	call	_ZN2v88internal32ArrayConstructInitializeElementsENS0_6HandleINS0_7JSArrayEEEPNS0_9ArgumentsE@PLT
	testq	%rax, %rax
	je	.L830
	movl	%r12d, %eax
	movq	(%r14), %r12
	shrl	$3, %eax
	movq	-1(%r12), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	testq	%rbx, %rbx
	je	.L790
	cmpb	%dl, %al
	je	.L791
.L793:
	movq	(%rbx), %rdx
	movslq	11(%rdx), %rax
	orl	$32, %eax
	salq	$32, %rax
	movq	%rax, 7(%rdx)
	movq	(%r14), %r12
.L779:
	movq	-88(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r13
	je	.L767
	movq	%r13, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L767:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L836
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L830:
	.cfi_restore_state
	movq	312(%r15), %r12
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L835:
	movq	(%rcx), %rax
	movq	%rax, %r14
	notq	%r14
	movl	%r14d, %ecx
	andl	$1, %ecx
	movb	%cl, -89(%rbp)
	jne	.L837
.L777:
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L830
	movb	$1, -89(%rbp)
	xorl	%r12d, %r12d
.L780:
	movq	(%rsi), %rax
	movzbl	14(%rax), %ecx
	shrl	$3, %ecx
	movl	%ecx, %r14d
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L784:
	testb	%r14b, %r14b
	jne	.L785
	movl	$1, %edx
	movl	$1, %r14d
.L786:
	testq	%rbx, %rbx
	je	.L783
	movq	(%rbx), %rdi
	movzbl	%r14b, %r8d
	movslq	11(%rdi), %rax
	andl	$-32, %eax
	orl	%r8d, %eax
	salq	$32, %rax
	movq	%rax, 7(%rdi)
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L791:
	cmpb	$0, -90(%rbp)
	je	.L793
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L790:
	cmpb	%dl, %al
	je	.L794
.L796:
	movq	4496(%r15), %rdx
	movabsq	$4294967296, %rax
	cmpq	%rax, 7(%rdx)
	jne	.L779
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate35InvalidateArrayConstructorProtectorEv@PLT
	movq	(%r14), %r12
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L831:
	call	_ZN2v88internalL22Stats_Runtime_NewArrayEiPmPNS0_7IsolateE
	movq	%rax, %r12
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L834:
	leaq	.LC7(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L837:
	sarq	$32, %rax
	cmpq	$33554432, %rax
	ja	.L777
	movzbl	-90(%rbp), %r12d
	testq	%rax, %rax
	je	.L776
	cmpl	$16375, %eax
	movl	%ecx, %r12d
	setle	-89(%rbp)
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L794:
	cmpb	$0, -89(%rbp)
	je	.L796
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L804:
	movl	$5, %edx
	movl	$5, %r14d
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L805:
	movl	$3, %edx
	movl	$3, %r14d
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L806:
	movl	$7, %edx
	movl	$7, %r14d
	jmp	.L786
.L836:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20089:
	.size	_ZN2v88internal16Runtime_NewArrayEiPmPNS0_7IsolateE, .-_ZN2v88internal16Runtime_NewArrayEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_NormalizeElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_NormalizeElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_NormalizeElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_NormalizeElementsEiPmPNS0_7IsolateE:
.LFB20092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L847
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L848
.L840:
	leaq	.LC10(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L848:
	movq	-1(%rdx), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L840
	movq	-1(%rdx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$17, %eax
	cmpb	$10, %al
	jbe	.L849
	movq	-1(%rdx), %rax
	cmpw	$1026, 11(%rax)
	je	.L850
	movq	%rsi, %rdi
	call	_ZN2v88internal8JSObject17NormalizeElementsENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %r13
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L838
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L838:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L847:
	.cfi_restore_state
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rdx, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L849:
	.cfi_restore_state
	leaq	.LC27(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L850:
	leaq	.LC28(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20092:
	.size	_ZN2v88internal25Runtime_NormalizeElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_NormalizeElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_GrowArrayElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_GrowArrayElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_GrowArrayElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_GrowArrayElementsEiPmPNS0_7IsolateE:
.LFB20095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r8d
	testl	%r8d, %r8d
	jne	.L883
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rsi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %sil
	jne	.L884
.L853:
	leaq	.LC10(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L884:
	movq	-1(%rsi), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L853
	movq	-8(%r13), %rdx
	movq	%rdx, %rax
	shrq	$32, %rax
	testb	$1, %dl
	je	.L858
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L885
	movq	7(%rdx), %rdx
	movsd	.LC17(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC16(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L886
.L859:
	movabsq	$9218868437227405312, %rax
	testq	%rax, %rdx
	je	.L863
	movq	%rdx, %rdi
	shrq	$52, %rdi
	andl	$2047, %edi
	movl	%edi, %ecx
	subl	$1075, %ecx
	js	.L887
	cmpl	$31, %ecx
	jg	.L863
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rdi
	andq	%rdx, %rax
	addq	%rdi, %rax
	salq	%cl, %rax
	movl	%eax, %eax
.L865:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %eax
.L858:
	testl	%eax, %eax
	jns	.L888
.L867:
	xorl	%r15d, %r15d
.L869:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L851
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L851:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L888:
	.cfi_restore_state
	movl	%eax, %r8d
.L863:
	movq	15(%rsi), %r15
	cmpl	%r8d, 11(%r15)
	ja	.L869
	movq	-1(%rsi), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	%r13, %rsi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movl	%r8d, %edx
	movq	(%rdi), %rax
	call	*120(%rax)
	testb	%al, %al
	je	.L867
	movq	0(%r13), %rax
	movq	15(%rax), %r15
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L886:
	movsd	.LC18(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L859
	comisd	.LC19(%rip), %xmm0
	jb	.L859
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L859
	je	.L858
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L887:
	cmpl	$-52, %ecx
	jl	.L863
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rax
	addq	%rcx, %rax
	movl	$1075, %ecx
	subl	%edi, %ecx
	shrq	%cl, %rax
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L883:
	addq	$8, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L885:
	.cfi_restore_state
	leaq	.LC15(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20095:
	.size	_ZN2v88internal25Runtime_GrowArrayElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_GrowArrayElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Runtime_ArrayIsArrayEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Runtime_ArrayIsArrayEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Runtime_ArrayIsArrayEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Runtime_ArrayIsArrayEiPmPNS0_7IsolateE:
.LFB20098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L905
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	41104(%rdx), %edx
	leal	1(%rdx), %eax
	movl	%eax, 41104(%r12)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L906
.L900:
	movq	%rbx, %rcx
.L896:
	movq	120(%r12), %r13
.L895:
	movq	%r14, 41088(%r12)
	movl	%edx, 41104(%r12)
	cmpq	%rcx, %rbx
	je	.L889
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L889:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L906:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1061, 11(%rcx)
	je	.L892
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jne	.L900
	call	_ZN2v88internal7JSProxy7IsArrayENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	jne	.L894
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rcx
	leal	-1(%rax), %edx
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L892:
	movq	%rbx, %rcx
.L899:
	movq	112(%r12), %r13
	jmp	.L895
.L894:
	movl	41104(%r12), %ecx
	shrw	$8, %ax
	leal	-1(%rcx), %edx
	movq	41096(%r12), %rcx
	jne	.L899
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L905:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL26Stats_Runtime_ArrayIsArrayEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20098:
	.size	_ZN2v88internal20Runtime_ArrayIsArrayEiPmPNS0_7IsolateE, .-_ZN2v88internal20Runtime_ArrayIsArrayEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal15Runtime_IsArrayEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15Runtime_IsArrayEiPmPNS0_7IsolateE
	.type	_ZN2v88internal15Runtime_IsArrayEiPmPNS0_7IsolateE, @function
_ZN2v88internal15Runtime_IsArrayEiPmPNS0_7IsolateE:
.LFB20101:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L912
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L909
.L911:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	movq	-1(%rax), %rax
	cmpw	$1061, 11(%rax)
	jne	.L911
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L912:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20101:
	.size	_ZN2v88internal15Runtime_IsArrayEiPmPNS0_7IsolateE, .-_ZN2v88internal15Runtime_IsArrayEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateE:
.LFB20104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L920
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	call	_ZN2v88internal6Object23ArraySpeciesConstructorEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L921
	movq	(%rax), %r14
.L916:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L913
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L913:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L921:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L920:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Stats_Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20104:
	.size	_ZN2v88internal31Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateE:
.LFB20107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %ebx
	testl	%ebx, %ebx
	jne	.L1024
	movq	41088(%rdx), %rax
	movq	41112(%rdx), %rdi
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rsi
	movq	%rax, -184(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -168(%rbp)
	testq	%rdi, %rdi
	je	.L925
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
	testb	$1, %sil
	jne	.L928
.L931:
	movq	%r14, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1023
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	je	.L1025
.L933:
	leaq	2768(%r15), %r12
	testb	$1, %al
	jne	.L940
.L942:
	movl	$-1, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rcx
.L941:
	movq	2768(%r15), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	je	.L1026
	movl	$3, %ebx
.L943:
	movabsq	$824633720832, %rax
	movl	%ebx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	2768(%r15), %rax
	movq	%r15, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1027
.L944:
	movq	%r12, -112(%rbp)
	leaq	-144(%rbp), %r12
	movq	%r12, %rdi
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L945
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L946:
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L947
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r15), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L948
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L949:
	testq	%rax, %rax
	je	.L1023
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L952
.L1034:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L953:
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -176(%rbp)
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L925:
	movq	%rax, %rdi
	movq	-184(%rbp), %rax
	movq	%rax, %r14
	cmpq	%rdi, %rax
	je	.L1028
.L927:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	testb	$1, %sil
	je	.L931
.L928:
	movq	-1(%rsi), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L931
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	jne	.L933
	.p2align 4,,10
	.p2align 3
.L1025:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L934
	sarq	$32, %rax
	movq	%rax, -176(%rbp)
	js	.L1022
.L939:
	cmpq	$0, -176(%rbp)
	je	.L1022
	movq	-16(%r13), %rbx
	leaq	-16(%r13), %rsi
	cmpq	88(%r15), %rbx
	jne	.L955
.L966:
	xorl	%ebx, %ebx
.L956:
	movq	(%r14), %rcx
	leaq	-8(%r13), %rax
	movq	%rax, -208(%rbp)
	movq	-1(%rcx), %rax
	cmpw	$1040, 11(%rax)
	jbe	.L970
	movl	$4294967295, %eax
	cmpq	%rax, -176(%rbp)
	jle	.L1029
.L970:
	movq	41096(%r15), %r13
	movl	41104(%r15), %edx
	cmpq	%rbx, -176(%rbp)
	jle	.L999
	leaq	-145(%rbp), %rax
	movq	%r14, -200(%rbp)
	leaq	-144(%rbp), %r12
	movq	%rax, -192(%rbp)
.L976:
	movl	$2147483648, %eax
	addl	$1, %edx
	movl	$4294967295, %ecx
	movq	41088(%r15), %r14
	addq	%rbx, %rax
	movl	%edx, 41104(%r15)
	cmpq	%rcx, %rax
	ja	.L978
	movq	41112(%r15), %rdi
	movq	%rbx, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L979
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L982:
	movq	-192(%rbp), %r8
	movq	-200(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$3, %r9d
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L1030
	movq	-208(%rbp), %rcx
	movq	%r12, %rdi
	movq	(%rcx), %rdx
	movq	%rdx, -144(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal6Object13SameValueZeroES1_@PLT
	testb	%al, %al
	jne	.L1031
	movl	41104(%r15), %eax
	movq	41096(%r15), %rcx
	movq	%r14, 41088(%r15)
	leal	-1(%rax), %edx
	movl	%edx, 41104(%r15)
	cmpq	%rcx, %r13
	je	.L986
	movq	%r13, 41096(%r15)
	movq	%r15, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	41104(%r15), %edx
	movq	41096(%r15), %rcx
	cmpq	%rbx, -176(%rbp)
	je	.L975
.L987:
	movq	%rcx, %r13
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L945:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L946
.L1023:
	movq	312(%r15), %r12
	movl	41104(%r15), %edx
	movq	41096(%r15), %rcx
.L932:
	movq	-184(%rbp), %rax
	subl	$1, %edx
	movl	%edx, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	%rcx, -168(%rbp)
	je	.L922
	movq	-168(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, 41096(%r15)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L922:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1032
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L955:
	.cfi_restore_state
	movq	%rbx, %rax
	notq	%rax
	testb	$1, %bl
	jne	.L957
	testb	$1, %al
	je	.L959
.L958:
	sarq	$32, %rbx
	jns	.L956
	addq	-176(%rbp), %rbx
	movl	$0, %eax
	cmovs	%rax, %rbx
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L948:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L1033
.L950:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	movq	(%rax), %rax
	testb	$1, %al
	je	.L1034
.L952:
	movsd	7(%rax), %xmm0
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L934:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1035
	.p2align 4,,10
	.p2align 3
.L1022:
	movq	120(%r15), %r12
	movl	41104(%r15), %edx
	movq	41096(%r15), %rcx
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L986:
	addq	$1, %rbx
	cmpq	%rbx, -176(%rbp)
	jne	.L987
.L975:
	movq	120(%r15), %r12
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L978:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	cvtsi2sdq	%rbx, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	%rax, %rcx
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L979:
	movq	%r14, %rcx
	cmpq	%r13, %r14
	je	.L1036
.L981:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1030:
	movq	312(%r15), %r12
	movq	%r14, %rax
.L984:
	movq	%rax, 41088(%r15)
	movl	41104(%r15), %eax
	movq	41096(%r15), %rcx
	leal	-1(%rax), %edx
	movl	%edx, 41104(%r15)
	cmpq	%rcx, %r13
	je	.L932
	movq	%r13, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1021:
	movl	41104(%r15), %edx
	movq	41096(%r15), %rcx
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L1026:
	testb	$1, 11(%rax)
	movl	$3, %eax
	cmove	%eax, %ebx
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L940:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L942
	movq	%r14, %rcx
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L947:
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L1024:
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internalL32Stats_Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r12
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L1027:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rcx, -176(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-176(%rbp), %rcx
	movq	%rax, %r12
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L1028:
	movq	%rdx, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L1036:
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L957:
	movq	%r15, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L1023
	movq	(%rax), %rbx
	movq	%rbx, %rax
	notq	%rax
	movl	%eax, %edx
	andl	$1, %edx
	testb	$1, %al
	jne	.L958
	testb	%dl, %dl
	jne	.L959
	movsd	7(%rbx), %xmm0
.L963:
	pxor	%xmm1, %xmm1
	cvtsi2sdq	-176(%rbp), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L1022
	movsd	.LC17(%rip), %xmm3
	movapd	%xmm0, %xmm2
	andpd	.LC16(%rip), %xmm2
	ucomisd	%xmm2, %xmm3
	jb	.L966
	pxor	%xmm2, %xmm2
	comisd	%xmm0, %xmm2
	ja	.L1037
.L1015:
	cvttsd2siq	%xmm0, %rbx
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L1029:
	movq	-1(%rcx), %rax
	movq	104(%r15), %rsi
	movq	288(%r15), %r8
	movq	1016(%r15), %rdi
	movq	23(%rax), %rax
	cmpq	%rsi, %rax
	je	.L971
	.p2align 4,,10
	.p2align 3
.L974:
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	jbe	.L970
	movq	15(%rax), %rax
	cmpq	%rax, %rdi
	je	.L1003
	cmpq	%rax, %r8
	jne	.L970
.L1003:
	movq	23(%rdx), %rax
	cmpq	%rax, %rsi
	jne	.L974
.L971:
	movq	-1(%rcx), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movl	%ebx, %r8d
	movq	%r15, %rsi
	movl	-176(%rbp), %r9d
	movq	-208(%rbp), %rcx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	%r14, %rdx
	movq	(%rdi), %rax
	call	*200(%rax)
	testb	%al, %al
	je	.L1038
	movzbl	%ah, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r12
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	112(%r15), %r12
	movq	%r14, %rax
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1035:
	movsd	.LC21(%rip), %xmm0
	addsd	7(%rax), %xmm0
	movq	%xmm0, %rdx
	movq	%xmm0, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1022
	movl	%eax, %eax
	movq	%rax, -176(%rbp)
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L1033:
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	jmp	.L950
.L999:
	movq	%r13, %rcx
	jmp	.L975
.L959:
	sarq	$32, %rbx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	jmp	.L963
.L1038:
	movq	312(%r15), %r12
	jmp	.L1021
.L1037:
	addsd	%xmm1, %xmm0
	xorl	%ebx, %ebx
	comisd	%xmm0, %xmm2
	jbe	.L1015
	jmp	.L956
.L1032:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20107:
	.size	_ZN2v88internal26Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Runtime_ArrayIndexOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Runtime_ArrayIndexOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Runtime_ArrayIndexOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Runtime_ArrayIndexOfEiPmPNS0_7IsolateE:
.LFB20110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %ebx
	testl	%ebx, %ebx
	jne	.L1148
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -176(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -168(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1042
.L1045:
	leaq	.LC24(%rip), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1070
.L1044:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	je	.L1149
	leaq	2768(%r15), %r14
	testb	$1, %al
	jne	.L1054
.L1056:
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rdx
.L1055:
	movq	2768(%r15), %rax
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	je	.L1150
	movl	$3, %ebx
.L1057:
	movabsq	$824633720832, %rax
	movl	%ebx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	2768(%r15), %rax
	movq	%r15, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1151
.L1058:
	movq	%r14, -112(%rbp)
	leaq	-144(%rbp), %r14
	movq	%r14, %rdi
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L1059
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L1060:
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L1061
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r15), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L1062
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1063:
	testq	%rax, %rax
	je	.L1070
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L1066
.L1159:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L1067:
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -184(%rbp)
	cmpq	$0, -184(%rbp)
	je	.L1050
.L1156:
	movq	-16(%r12), %rax
	leaq	-16(%r12), %rsi
	testb	$1, %al
	jne	.L1068
	cmpq	$16, %r12
	je	.L1070
.L1069:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L1072:
	pxor	%xmm1, %xmm1
	cvtsi2sdq	-184(%rbp), %xmm1
	comisd	%xmm1, %xmm0
	ja	.L1050
	comisd	.LC25(%rip), %xmm0
	jb	.L1138
	cvttsd2siq	%xmm0, %rbx
	testq	%rbx, %rbx
	js	.L1075
.L1077:
	movq	0(%r13), %rcx
	leaq	-8(%r12), %rax
	movq	%rax, -216(%rbp)
	movq	-1(%rcx), %rax
	cmpw	$1040, 11(%rax)
	jbe	.L1078
	movl	$4294967295, %eax
	cmpq	%rax, -184(%rbp)
	jg	.L1078
	movq	-1(%rcx), %rax
	movq	104(%r15), %rsi
	movq	288(%r15), %r8
	movq	1016(%r15), %rdi
	movq	23(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1082
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	15(%rax), %rax
	cmpq	%rax, %rdi
	je	.L1120
	cmpq	%rax, %r8
	jne	.L1078
.L1120:
	movq	23(%rdx), %rax
	cmpq	%rax, %rsi
	je	.L1079
.L1082:
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	ja	.L1152
	.p2align 4,,10
	.p2align 3
.L1078:
	movq	41096(%r15), %rax
	movl	41104(%r15), %edx
	movq	%rax, -192(%rbp)
	cmpq	%rbx, -184(%rbp)
	jle	.L1145
	leaq	-153(%rbp), %rax
	movq	%r13, -208(%rbp)
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	leaq	-152(%rbp), %rax
	movq	%rax, -224(%rbp)
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	41112(%r15), %rdi
	movq	%rbx, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L1091
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L1094:
	movq	-200(%rbp), %r8
	movq	%r14, %rdi
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	-208(%rbp), %rdx
	movl	$3, %r9d
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	je	.L1146
	shrw	$8, %ax
	je	.L1101
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L1146
	movq	-216(%rbp), %rcx
	movq	-224(%rbp), %rdi
	movq	(%rcx), %rdx
	movq	%rdx, -152(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal6Object12StrictEqualsES1_@PLT
	testb	%al, %al
	jne	.L1153
.L1101:
	movl	41104(%r15), %eax
	movq	%r13, 41088(%r15)
	leal	-1(%rax), %edx
	movq	-192(%rbp), %rax
	movl	%edx, 41104(%r15)
	cmpq	41096(%r15), %rax
	je	.L1099
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	41096(%r15), %rax
	movl	41104(%r15), %edx
	movq	%rax, -192(%rbp)
.L1099:
	addq	$1, %rbx
	cmpq	%rbx, -184(%rbp)
	je	.L1154
.L1083:
	movl	$2147483648, %eax
	addl	$1, %edx
	movl	$4294967295, %ecx
	movq	41088(%r15), %r13
	addq	%rbx, %rax
	movl	%edx, 41104(%r15)
	cmpq	%rcx, %rax
	jbe	.L1155
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	cvtsi2sdq	%rbx, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1149:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1048
	sarq	$32, %rax
	movq	%rax, -184(%rbp)
	js	.L1050
.L1053:
	cmpq	$0, -184(%rbp)
	jne	.L1156
	.p2align 4,,10
	.p2align 3
.L1050:
	movl	41104(%r15), %edx
	movq	41096(%r15), %rax
	movabsq	$-4294967296, %r12
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1059:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L1060
.L1070:
	movq	312(%r15), %r12
	movl	41104(%r15), %edx
	movq	41096(%r15), %rax
.L1046:
	movq	-176(%rbp), %rcx
	subl	$1, %edx
	movl	%edx, 41104(%r15)
	movq	%rcx, 41088(%r15)
	cmpq	%rax, -168(%rbp)
	je	.L1039
	movq	-168(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, 41096(%r15)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1039:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1157
	addq	$200, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1042:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1045
	movq	%rsi, %r13
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L1158
.L1064:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	movq	(%rax), %rax
	testb	$1, %al
	je	.L1159
.L1066:
	movsd	7(%rax), %xmm0
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1050
	movsd	.LC21(%rip), %xmm0
	addsd	7(%rax), %xmm0
	movq	%xmm0, %rdx
	movq	%xmm0, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1050
	movl	%eax, %eax
	movq	%rax, -184(%rbp)
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1150:
	testb	$1, 11(%rax)
	movl	$3, %eax
	cmove	%eax, %ebx
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1054:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1056
	movq	%r13, %rdx
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1091:
	movq	%r13, %r12
	cmpq	-192(%rbp), %r13
	je	.L1160
.L1093:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1148:
	movq	%r12, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r12
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1151:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-184(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	%r15, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L1070
	movq	(%rax), %rax
	testb	$1, %al
	je	.L1069
	movsd	7(%rax), %xmm0
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	312(%r15), %r12
	movq	%r13, %rax
.L1096:
	movq	%rax, 41088(%r15)
	movl	41104(%r15), %eax
	leal	-1(%rax), %edx
	movq	41096(%r15), %rax
	movl	%edx, 41104(%r15)
	cmpq	%rax, -192(%rbp)
	je	.L1046
	movq	-192(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, 41096(%r15)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1144:
	movl	41104(%r15), %edx
	movq	41096(%r15), %rax
	jmp	.L1046
.L1138:
	movabsq	$-9223372036854775808, %rbx
.L1075:
	addq	-184(%rbp), %rbx
	movl	$0, %eax
	cmovs	%rax, %rbx
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	%r15, %rdi
	movq	%rsi, -232(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1158:
	movq	%r15, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	jmp	.L1064
.L1154:
	movq	-192(%rbp), %rax
.L1145:
	movabsq	$-4294967296, %r12
	jmp	.L1046
.L1153:
	movq	(%r12), %r12
	movq	%r13, %rax
	jmp	.L1096
.L1079:
	movq	-1(%rcx), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movl	%ebx, %r8d
	movq	%r15, %rsi
	movl	-184(%rbp), %r9d
	movq	-216(%rbp), %rcx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	%r13, %rdx
	movq	(%rdi), %rax
	call	*208(%rax)
	testb	%al, %al
	je	.L1161
	movl	$2147483648, %eax
	movl	$4294967295, %ecx
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	ja	.L1085
	movq	41112(%r15), %rdi
	salq	$32, %rdx
	movq	%rdx, %r12
	testq	%rdi, %rdi
	je	.L1086
	movq	%rdx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1089:
	movq	(%rax), %r12
	jmp	.L1144
.L1161:
	movq	312(%r15), %r12
	jmp	.L1144
.L1085:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	cvtsi2sdq	%rdx, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	jmp	.L1089
.L1086:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L1162
.L1088:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%r12, (%rax)
	jmp	.L1089
.L1157:
	call	__stack_chk_fail@PLT
.L1162:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1088
	.cfi_endproc
.LFE20110:
	.size	_ZN2v88internal20Runtime_ArrayIndexOfEiPmPNS0_7IsolateE, .-_ZN2v88internal20Runtime_ArrayIndexOfEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal30Runtime_TransitionElementsKindEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal30Runtime_TransitionElementsKindEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal30Runtime_TransitionElementsKindEiPmPNS0_7IsolateE:
.LFB25099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25099:
	.size	_GLOBAL__sub_I__ZN2v88internal30Runtime_TransitionElementsKindEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal30Runtime_TransitionElementsKindEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal30Runtime_TransitionElementsKindEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic311,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic311, @object
	.size	_ZZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic311, 8
_ZZN2v88internalL26Stats_Runtime_ArrayIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic311:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateEE28trace_event_unique_atomic206,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateEE28trace_event_unique_atomic206, @object
	.size	_ZZN2v88internalL32Stats_Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateEE28trace_event_unique_atomic206, 8
_ZZN2v88internalL32Stats_Runtime_ArrayIncludes_SlowEiPmPNS0_7IsolateEE28trace_event_unique_atomic206:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic197,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic197, @object
	.size	_ZZN2v88internalL37Stats_Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic197, 8
_ZZN2v88internalL37Stats_Runtime_ArraySpeciesConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic197:
	.zero	8
	.section	.bss._ZZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic190,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic190, @object
	.size	_ZZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic190, 8
_ZZN2v88internalL21Stats_Runtime_IsArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic190:
	.zero	8
	.section	.bss._ZZN2v88internalL26Stats_Runtime_ArrayIsArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic181,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL26Stats_Runtime_ArrayIsArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic181, @object
	.size	_ZZN2v88internalL26Stats_Runtime_ArrayIsArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic181, 8
_ZZN2v88internalL26Stats_Runtime_ArrayIsArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic181:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic160,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic160, @object
	.size	_ZZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic160, 8
_ZZN2v88internalL31Stats_Runtime_GrowArrayElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic160:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic148,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic148, @object
	.size	_ZZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic148, 8
_ZZN2v88internalL31Stats_Runtime_NormalizeElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic148:
	.zero	8
	.section	.bss._ZZN2v88internalL22Stats_Runtime_NewArrayEiPmPNS0_7IsolateEE27trace_event_unique_atomic44,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL22Stats_Runtime_NewArrayEiPmPNS0_7IsolateEE27trace_event_unique_atomic44, @object
	.size	_ZZN2v88internalL22Stats_Runtime_NewArrayEiPmPNS0_7IsolateEE27trace_event_unique_atomic44, 8
_ZZN2v88internalL22Stats_Runtime_NewArrayEiPmPNS0_7IsolateEE27trace_event_unique_atomic44:
	.zero	8
	.section	.bss._ZZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateEE27trace_event_unique_atomic34,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateEE27trace_event_unique_atomic34, @object
	.size	_ZZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateEE27trace_event_unique_atomic34, 8
_ZZN2v88internalL44Stats_Runtime_TransitionElementsKindWithKindEiPmPNS0_7IsolateEE27trace_event_unique_atomic34:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateEE27trace_event_unique_atomic24,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateEE27trace_event_unique_atomic24, @object
	.size	_ZZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateEE27trace_event_unique_atomic24, 8
_ZZN2v88internalL36Stats_Runtime_TransitionElementsKindEiPmPNS0_7IsolateEE27trace_event_unique_atomic24:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC16:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC17:
	.long	4294967295
	.long	2146435071
	.align 8
.LC18:
	.long	4290772992
	.long	1105199103
	.align 8
.LC19:
	.long	0
	.long	-1042284544
	.align 8
.LC21:
	.long	0
	.long	1127219200
	.align 8
.LC25:
	.long	0
	.long	-1008730112
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
