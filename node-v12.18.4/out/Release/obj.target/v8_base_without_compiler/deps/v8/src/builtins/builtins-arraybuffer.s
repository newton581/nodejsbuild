	.file	"builtins-arraybuffer.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal12_GLOBAL__N_115ConstructBufferEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115ConstructBufferEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEb, @function
_ZN2v88internal12_GLOBAL__N_115ConstructBufferEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEb:
.LFB20040:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	xorl	%edx, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE@PLT
	testq	%rax, %rax
	je	.L22
	movq	(%r15), %rdx
	movq	%rax, %r13
	testb	$1, %dl
	jne	.L8
	sarq	$32, %rdx
	js	.L9
.L10:
	movq	(%r14), %rax
	movzbl	%bl, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	31(%rax), %rax
	movq	39(%rax), %rax
	movq	79(%rax), %rax
	cmpq	%rax, (%r14)
	setne	%r8b
	movzbl	%r8b, %r8d
	call	_ZN2v88internal13JSArrayBuffer19SetupAllocatingDataENS0_6HandleIS1_EEPNS0_7IsolateEmbNS0_10SharedFlagE@PLT
	testb	%al, %al
	je	.L23
	movq	0(%r13), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movsd	7(%rdx), %xmm0
	comisd	.LC0(%rip), %xmm0
	jb	.L9
	movsd	.LC1(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L24
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13JSArrayBuffer12SetupAsEmptyENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$186, %esi
.L21:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L13
	cvttsd2siq	%xmm0, %rdx
.L14:
	movabsq	$9007199254740991, %rax
	cmpq	%rax, %rdx
	ja	.L9
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L13:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rdx
	btcq	$63, %rdx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L23:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$187, %esi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L22:
	movq	312(%r12), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20040:
	.size	_ZN2v88internal12_GLOBAL__N_115ConstructBufferEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEb, .-_ZN2v88internal12_GLOBAL__N_115ConstructBufferEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEb
	.section	.text._ZN2v88internalL51Builtin_Impl_ArrayBufferConstructor_DoNotInitializeENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL51Builtin_Impl_ArrayBufferConstructor_DoNotInitializeENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL51Builtin_Impl_ArrayBufferConstructor_DoNotInitializeENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20046:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	movq	39(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L26
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L27:
	movq	79(%rsi), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L29
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L30:
	subq	$8, %rbx
	leaq	88(%r12), %rcx
	cmpl	$5, %r15d
	movq	%rsi, %rdx
	cmovg	%rbx, %rcx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	call	_ZN2v88internal12_GLOBAL__N_115ConstructBufferEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEb
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r15
	cmpq	41096(%r12), %r14
	je	.L36
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L36:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%r13, %rax
	cmpq	41096(%rdx), %r13
	je	.L38
.L28:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L29:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L39
.L31:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%rdx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L31
	.cfi_endproc
.LFE20046:
	.size	_ZN2v88internalL51Builtin_Impl_ArrayBufferConstructor_DoNotInitializeENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL51Builtin_Impl_ArrayBufferConstructor_DoNotInitializeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL35Builtin_Impl_ArrayBufferConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Builtin_Impl_ArrayBufferConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL35Builtin_Impl_ArrayBufferConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20043:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-16(,%rdi,8), %eax
	addl	$1, 41104(%rdx)
	movslq	%eax, %rdx
	addl	$8, %eax
	cltq
	subq	%rdx, %r14
	subq	%rax, %r15
	movq	(%r15), %rax
	cmpq	%rax, 88(%r12)
	je	.L69
	subq	$8, %rsi
	leaq	88(%r12), %rcx
	cmpl	$5, %edi
	cmovg	%rsi, %rcx
	movq	(%rcx), %rax
	testb	$1, %al
	jne	.L70
.L55:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm0
	comisd	%xmm0, %xmm1
	ja	.L71
.L65:
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_115ConstructBufferEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEb
	movq	%rax, %r14
.L51:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L62
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L62:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$186, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L68:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%rcx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L56
	movq	312(%r12), %r14
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L56:
	movq	(%rax), %rax
	testb	$1, %al
	je	.L55
	movsd	7(%rax), %xmm0
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L65
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L69:
	movq	(%r14), %rax
	movq	23(%rax), %r14
	movq	15(%r14), %rax
	testb	$1, %al
	jne	.L73
.L42:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L43:
	testb	%al, %al
	je	.L47
	movq	15(%r14), %r15
	testb	$1, %r15b
	jne	.L74
.L45:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L48
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L49:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L73:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L42
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L48:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L75
.L50:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rdx)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L47:
	andq	$-262144, %r14
	movq	24(%r14), %rax
	movq	-37464(%rax), %r15
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L74:
	movq	-1(%r15), %rax
	cmpw	$136, 11(%rax)
	jne	.L45
	leaq	-64(%rbp), %rdi
	movq	%r15, -64(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L47
	movq	-72(%rbp), %rdi
	movq	%r15, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %r15
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L50
.L72:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20043:
	.size	_ZN2v88internalL35Builtin_Impl_ArrayBufferConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL35Builtin_Impl_ArrayBufferConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_ArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"get ArrayBuffer.prototype.byteLength"
	.section	.rodata._ZN2v88internalL46Builtin_Impl_ArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"(location_) != nullptr"
.LC5:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL46Builtin_Impl_ArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_ArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_ArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20049:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L77
.L80:
	leaq	.LC3(%rip), %rax
	movq	$36, -72(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
.L105:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L78
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L83:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L92
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L92:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$48, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1059, 11(%rdx)
	jne	.L80
	movl	39(%rax), %edx
	andl	$8, %edx
	je	.L107
	leaq	.LC3(%rip), %rax
	movq	$36, -56(%rbp)
	leaq	-64(%rbp), %rsi
	movq	%rax, -64(%rbp)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L107:
	movq	23(%rax), %r13
	cmpq	$2147483647, %r13
	ja	.L84
	movq	41112(%r12), %rdi
	salq	$32, %r13
	testq	%rdi, %rdi
	je	.L85
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L84:
	testq	%r13, %r13
	js	.L89
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r13, %xmm0
.L90:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%r14, %rax
	cmpq	41096(%r12), %r14
	je	.L108
.L87:
	movq	%r13, (%rax)
	jmp	.L83
.L89:
	movq	%r13, %rax
	andl	$1, %r13d
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%r13, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L90
.L108:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L87
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20049:
	.size	_ZN2v88internalL46Builtin_Impl_ArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_ArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL52Builtin_Impl_SharedArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"get SharedArrayBuffer.prototype.byteLength"
	.section	.text._ZN2v88internalL52Builtin_Impl_SharedArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL52Builtin_Impl_SharedArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL52Builtin_Impl_SharedArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20052:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L110
.L113:
	leaq	.LC6(%rip), %rax
	movq	$42, -72(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
.L138:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L111
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L116:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L125
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L125:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L139
	addq	$48, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1059, 11(%rdx)
	jne	.L113
	movl	39(%rax), %edx
	andl	$8, %edx
	jne	.L140
	leaq	.LC6(%rip), %rax
	movq	$42, -56(%rbp)
	leaq	-64(%rbp), %rsi
	movq	%rax, -64(%rbp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L140:
	movq	23(%rax), %r13
	cmpq	$2147483647, %r13
	ja	.L117
	movq	41112(%r12), %rdi
	salq	$32, %r13
	testq	%rdi, %rdi
	je	.L118
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L117:
	testq	%r13, %r13
	js	.L122
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r13, %xmm0
.L123:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r14, %rax
	cmpq	41096(%r12), %r14
	je	.L141
.L120:
	movq	%r13, (%rax)
	jmp	.L116
.L122:
	movq	%r13, %rax
	andl	$1, %r13d
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%r13, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L123
.L141:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L120
.L139:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20052:
	.size	_ZN2v88internalL52Builtin_Impl_SharedArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL52Builtin_Impl_SharedArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7Factory25NewStringFromAsciiCheckedEPKcNS0_14AllocationTypeE,"axG",@progbits,_ZN2v88internal7Factory25NewStringFromAsciiCheckedEPKcNS0_14AllocationTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory25NewStringFromAsciiCheckedEPKcNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory25NewStringFromAsciiCheckedEPKcNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory25NewStringFromAsciiCheckedEPKcNS0_14AllocationTypeE:
.LFB8099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	-64(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L146
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L147
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8099:
	.size	_ZN2v88internal7Factory25NewStringFromAsciiCheckedEPKcNS0_14AllocationTypeE, .-_ZN2v88internal7Factory25NewStringFromAsciiCheckedEPKcNS0_14AllocationTypeE
	.section	.rodata._ZN2v88internalL11SliceHelperENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcb.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"TryNumberToSize(*first_obj, &first_size)"
	.align 8
.LC8:
	.string	"TryNumberToSize(*new_len_obj, &new_len_size)"
	.section	.rodata._ZN2v88internalL11SliceHelperENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcb.str1.1,"aMS",@progbits,1
.LC9:
	.string	"NewArray"
	.section	.text._ZN2v88internalL11SliceHelperENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL11SliceHelperENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcb, @function
_ZN2v88internalL11SliceHelperENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcb:
.LFB20056:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	leaq	88(%rdx), %rax
	cmpl	$6, %edi
	leaq	-16(%rsi), %rdx
	cmovg	%rdx, %rax
	movq	%rax, %r9
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L151
.L154:
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, -144(%rbp)
	leaq	-144(%rbp), %rsi
	movq	%rax, -136(%rbp)
.L299:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L152
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
.L292:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L293:
	movq	%rax, %r13
.L157:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L216
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L216:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1059, 11(%rdx)
	jne	.L154
	movl	39(%rax), %edx
	shrl	$3, %edx
	andl	$1, %edx
	cmpb	%dl, %r8b
	je	.L272
	movq	%rcx, %rdi
	call	strlen@PLT
	movq	%r15, -128(%rbp)
	leaq	-128(%rbp), %rsi
	movq	%rax, -120(%rbp)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L152:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L272:
	testb	%r8b, %r8b
	je	.L304
.L158:
	movq	23(%rax), %rax
	leaq	-8(%r13), %rsi
	testq	%rax, %rax
	js	.L160
	pxor	%xmm5, %xmm5
	cvtsi2sdq	%rax, %xmm5
	movsd	%xmm5, -152(%rbp)
.L161:
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L305
.L163:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L167:
	pxor	%xmm4, %xmm4
	comisd	%xmm0, %xmm4
	ja	.L306
	minsd	-152(%rbp), %xmm0
.L171:
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%r8d, -172(%rbp)
	movq	%r9, -168(%rbp)
	movsd	%xmm0, -160(%rbp)
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	-168(%rbp), %r9
	movsd	-160(%rbp), %xmm0
	movq	%rax, -184(%rbp)
	movl	-172(%rbp), %r8d
	movq	(%r9), %rax
	cmpq	%rax, 88(%r12)
	je	.L172
.L232:
	testb	$1, %al
	jne	.L307
.L174:
	movq	(%r9), %rax
	testb	$1, %al
	jne	.L175
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L176:
	pxor	%xmm6, %xmm6
	comisd	%xmm1, %xmm6
	jbe	.L275
	addsd	-152(%rbp), %xmm1
	movapd	%xmm6, %xmm7
	maxsd	%xmm1, %xmm7
	movsd	%xmm7, -152(%rbp)
.L172:
	movsd	-152(%rbp), %xmm2
	pxor	%xmm3, %xmm3
	subsd	%xmm0, %xmm2
	maxsd	%xmm2, %xmm3
	movsd	%xmm3, -152(%rbp)
.L179:
	movsd	-152(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%r8d, -160(%rbp)
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movl	-160(%rbp), %r8d
	movq	%rax, -168(%rbp)
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	testb	%r8b, %r8b
	je	.L180
	movq	1183(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L185
.L290:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-160(%rbp), %r8d
	movq	%rax, %rdx
.L184:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%r8d, -160(%rbp)
	call	_ZN2v88internal6Object18SpeciesConstructorEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_10JSFunctionEEE@PLT
	movl	-160(%rbp), %r8d
	testq	%rax, %rax
	je	.L295
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$8, %edi
	movl	%r8d, -172(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-160(%rbp), %r11
	movl	-172(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L189
.L217:
	movq	$0, (%r10)
	movq	%r10, %rcx
	movq	%r11, %rsi
	movq	%r12, %rdi
	movq	-168(%rbp), %rax
	movl	$1, %edx
	movl	%r8d, -172(%rbp)
	movq	%r10, -160(%rbp)
	movq	%rax, (%r10)
	call	_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEiPS6_@PLT
	movq	-160(%rbp), %r10
	movl	-172(%rbp), %r8d
	testq	%rax, %rax
	je	.L308
	movq	%r10, %rdi
	movl	%r8d, -172(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZdaPv@PLT
	movq	-160(%rbp), %rcx
	movq	(%rcx), %rax
	movq	-1(%rax), %rdx
	movl	-172(%rbp), %r8d
	cmpw	$1059, 11(%rdx)
	je	.L192
	movq	%r15, %rdi
	movq	%rcx, -152(%rbp)
	call	strlen@PLT
	movq	%r15, -96(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%rax, -88(%rbp)
.L302:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L152
	movq	-152(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L304:
	movl	39(%rax), %edx
	andl	$4, %edx
	je	.L158
	movq	%rcx, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	leaq	-112(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r15, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L152
.L294:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$50, %esi
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, -152(%rbp)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L180:
	movq	79(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L185
	movl	%r8d, -160(%rbp)
	jmp	.L290
.L307:
	movq	%r9, %rsi
	movq	%r12, %rdi
	movl	%r8d, -168(%rbp)
	movsd	%xmm0, -160(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movsd	-160(%rbp), %xmm0
	movl	-168(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r9
	jne	.L174
	.p2align 4,,10
	.p2align 3
.L295:
	movq	312(%r12), %r13
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L185:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L309
.L187:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L306:
	addsd	-152(%rbp), %xmm0
	comisd	%xmm0, %xmm4
	jbe	.L171
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movl	%r8d, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	-160(%rbp), %r9
	movl	-168(%rbp), %r8d
	pxor	%xmm0, %xmm0
	movq	%rax, -184(%rbp)
	movq	(%r9), %rax
	cmpq	88(%r12), %rax
	je	.L179
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L305:
	movq	%r12, %rdi
	movl	%r8d, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L295
	movq	(%rax), %rax
	movq	-160(%rbp), %r9
	movl	-168(%rbp), %r8d
	testb	$1, %al
	je	.L163
	movsd	7(%rax), %xmm0
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L275:
	minsd	-152(%rbp), %xmm1
	movsd	%xmm1, -152(%rbp)
	jmp	.L172
.L192:
	movl	39(%rax), %edx
	shrl	$3, %edx
	andl	$1, %edx
	cmpb	%dl, %r8b
	je	.L193
	movq	%r15, %rdi
	movq	%rcx, -152(%rbp)
	call	strlen@PLT
	movq	%r15, -80(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%rax, -72(%rbp)
	jmp	.L302
.L175:
	movsd	7(%rax), %xmm1
	jmp	.L176
.L308:
	movq	%r10, %rdi
	movq	312(%r12), %r13
	call	_ZdaPv@PLT
	jmp	.L157
.L193:
	testb	%r8b, %r8b
	je	.L310
	movq	0(%r13), %rdi
	movq	31(%rdi), %rsi
	cmpq	%rsi, 31(%rax)
	je	.L311
	movq	23(%rax), %rsi
	testq	%rsi, %rsi
	js	.L221
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L222:
	movsd	-152(%rbp), %xmm5
	movl	$160, %esi
	comisd	%xmm0, %xmm5
	jbe	.L223
.L219:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L292
.L309:
	movq	%r12, %rdi
	movl	%r8d, -172(%rbp)
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-172(%rbp), %r8d
	movq	-160(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L187
.L223:
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L312
	movq	%rdx, %rsi
	sarq	$32, %rsi
	js	.L198
.L199:
	movq	-168(%rbp), %rdx
	movq	(%rdx), %rdx
	testb	$1, %dl
	jne	.L204
	sarq	$32, %rdx
	js	.L205
.L206:
	testq	%rdx, %rdx
	je	.L293
	movq	31(%rax), %r8
	addq	31(%rdi), %rsi
	cmpq	$7, %rdx
	ja	.L213
	movzbl	(%rsi), %eax
	movb	%al, (%r8)
	cmpq	$1, %rdx
	je	.L291
	movzbl	1(%rsi), %eax
	movb	%al, 1(%r8)
	cmpq	$2, %rdx
	je	.L291
	movzbl	2(%rsi), %eax
	movb	%al, 2(%r8)
	cmpq	$3, %rdx
	je	.L291
	movzbl	3(%rsi), %eax
	movb	%al, 3(%r8)
	cmpq	$4, %rdx
	je	.L291
	movzbl	4(%rsi), %eax
	movb	%al, 4(%r8)
	subq	$5, %rdx
	je	.L291
	movzbl	5(%rsi), %eax
	subq	$1, %rdx
	movb	%al, 5(%r8)
	je	.L291
	movzbl	6(%rsi), %eax
	movb	%al, 6(%r8)
	movq	(%rcx), %rax
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L310:
	movl	39(%rax), %edx
	andl	$4, %edx
	jne	.L233
	movq	0(%r13), %rsi
	leaq	-80(%rbp), %rdi
	movq	%rcx, -160(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6Object9SameValueES1_@PLT
	movq	-160(%rbp), %rcx
	testb	%al, %al
	jne	.L313
	movq	(%rcx), %rax
	movq	23(%rax), %rsi
	testq	%rsi, %rsi
	js	.L225
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L226:
	movsd	-152(%rbp), %xmm7
	comisd	%xmm0, %xmm7
	ja	.L314
	movq	0(%r13), %rdi
	movl	39(%rdi), %edx
	andb	$4, %dl
	je	.L223
.L233:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory25NewStringFromAsciiCheckedEPKcNS0_14AllocationTypeE
	movq	%rax, %rdx
	jmp	.L294
.L314:
	movl	$13, %esi
	jmp	.L219
.L225:
	movq	%rsi, %rdx
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rsi, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L226
.L312:
	movsd	7(%rdx), %xmm0
	pxor	%xmm5, %xmm5
	comisd	%xmm5, %xmm0
	jb	.L198
	movsd	.LC1(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L315
.L198:
	leaq	.LC7(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L221:
	movq	%rsi, %rdx
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rsi, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L222
.L204:
	movsd	7(%rdx), %xmm0
	pxor	%xmm7, %xmm7
	comisd	%xmm7, %xmm0
	jb	.L205
	movsd	.LC1(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L316
.L205:
	leaq	.LC8(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L313:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$14, %esi
	jmp	.L292
.L303:
	call	__stack_chk_fail@PLT
.L315:
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L202
	cvttsd2siq	%xmm0, %rsi
	jmp	.L199
.L311:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$161, %esi
	jmp	.L292
.L316:
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L209
	cvttsd2siq	%xmm0, %rdx
	jmp	.L206
.L213:
	movq	%r8, %rdi
	movq	%rcx, -152(%rbp)
	call	memcpy@PLT
	movq	-152(%rbp), %rcx
.L291:
	movq	(%rcx), %rax
	jmp	.L293
.L202:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rsi
	btcq	$63, %rsi
	jmp	.L199
.L209:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rdx
	btcq	$63, %rdx
	jmp	.L206
.L189:
	movl	%r8d, -172(%rbp)
	movq	%r11, -160(%rbp)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$8, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-160(%rbp), %r11
	movl	-172(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L217
	leaq	.LC9(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE20056:
	.size	_ZN2v88internalL11SliceHelperENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcb, .-_ZN2v88internalL11SliceHelperENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcb
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L323
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L326
.L317:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L317
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"V8.Builtin_ArrayBufferIsView"
	.section	.text._ZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateE:
.LFB20053:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L360
.L328:
	movq	_ZZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateEE28trace_event_unique_atomic119(%rip), %rbx
	testq	%rbx, %rbx
	je	.L361
.L330:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L362
.L332:
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L336
.L339:
	movq	120(%r12), %r12
.L337:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L363
.L327:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L364
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L365
.L331:
	movq	%rbx, _ZZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateEE28trace_event_unique_atomic119(%rip)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L336:
	movq	-1(%rax), %rdx
	cmpw	$1087, 11(%rdx)
	jne	.L366
.L338:
	movq	112(%r12), %r12
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L362:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L367
.L333:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L334
	movq	(%rdi), %rax
	call	*8(%rax)
.L334:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L335
	movq	(%rdi), %rax
	call	*8(%rax)
.L335:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L366:
	movq	-1(%rax), %rax
	cmpw	$1086, 11(%rax)
	jne	.L339
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L363:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L360:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$685, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L367:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L365:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L331
.L364:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20053:
	.size	_ZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL52Builtin_Impl_Stats_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"V8.Builtin_ArrayBufferPrototypeGetByteLength"
	.section	.text._ZN2v88internalL52Builtin_Impl_Stats_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL52Builtin_Impl_Stats_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE, @function
_ZN2v88internalL52Builtin_Impl_Stats_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE:
.LFB20047:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L397
.L369:
	movq	_ZZN2v88internalL52Builtin_Impl_Stats_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateEE27trace_event_unique_atomic98(%rip), %rbx
	testq	%rbx, %rbx
	je	.L398
.L371:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L399
.L373:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL46Builtin_Impl_ArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L400
.L377:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L401
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L402
.L372:
	movq	%rbx, _ZZN2v88internalL52Builtin_Impl_Stats_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateEE27trace_event_unique_atomic98(%rip)
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L399:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L403
.L374:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L375
	movq	(%rdi), %rax
	call	*8(%rax)
.L375:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L376
	movq	(%rdi), %rax
	call	*8(%rax)
.L376:
	leaq	.LC12(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L400:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L397:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$684, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L403:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L402:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L372
.L401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20047:
	.size	_ZN2v88internalL52Builtin_Impl_Stats_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE, .-_ZN2v88internalL52Builtin_Impl_Stats_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL58Builtin_Impl_Stats_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8.Builtin_SharedArrayBufferPrototypeGetByteLength"
	.section	.text._ZN2v88internalL58Builtin_Impl_Stats_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL58Builtin_Impl_Stats_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE, @function
_ZN2v88internalL58Builtin_Impl_Stats_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE:
.LFB20050:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L433
.L405:
	movq	_ZZN2v88internalL58Builtin_Impl_Stats_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic109(%rip), %rbx
	testq	%rbx, %rbx
	je	.L434
.L407:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L435
.L409:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL52Builtin_Impl_SharedArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L436
.L413:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L438
.L408:
	movq	%rbx, _ZZN2v88internalL58Builtin_Impl_Stats_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic109(%rip)
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L435:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L439
.L410:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L411
	movq	(%rdi), %rax
	call	*8(%rax)
.L411:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L412
	movq	(%rdi), %rax
	call	*8(%rax)
.L412:
	leaq	.LC13(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L436:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L433:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$830, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L439:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L438:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L408
.L437:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20050:
	.size	_ZN2v88internalL58Builtin_Impl_Stats_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE, .-_ZN2v88internalL58Builtin_Impl_Stats_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL50Builtin_Impl_Stats_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"V8.Builtin_SharedArrayBufferPrototypeSlice"
	.align 8
.LC15:
	.string	"SharedArrayBuffer.prototype.slice"
	.section	.text._ZN2v88internalL50Builtin_Impl_Stats_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL50Builtin_Impl_Stats_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE, @function
_ZN2v88internalL50Builtin_Impl_Stats_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE:
.LFB20058:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L469
.L441:
	movq	_ZZN2v88internalL50Builtin_Impl_Stats_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateEE28trace_event_unique_atomic289(%rip), %rbx
	testq	%rbx, %rbx
	je	.L470
.L443:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L471
.L445:
	movq	%r12, %rdx
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	%r13, %rsi
	leaq	.LC15(%rip), %rcx
	call	_ZN2v88internalL11SliceHelperENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcb
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L472
.L440:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L473
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L474
.L444:
	movq	%rbx, _ZZN2v88internalL50Builtin_Impl_Stats_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateEE28trace_event_unique_atomic289(%rip)
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L471:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L475
.L446:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L447
	movq	(%rdi), %rax
	call	*8(%rax)
.L447:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L448
	movq	(%rdi), %rax
	call	*8(%rax)
.L448:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L472:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L469:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$831, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L475:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L474:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L444
.L473:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20058:
	.size	_ZN2v88internalL50Builtin_Impl_Stats_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE, .-_ZN2v88internalL50Builtin_Impl_Stats_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"V8.Builtin_ArrayBufferPrototypeSlice"
	.section	.rodata._ZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"ArrayBuffer.prototype.slice"
	.section	.text._ZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE:
.LFB20061:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L505
.L477:
	movq	_ZZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateEE28trace_event_unique_atomic296(%rip), %rbx
	testq	%rbx, %rbx
	je	.L506
.L479:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L507
.L481:
	movq	%r12, %rdx
	xorl	%r8d, %r8d
	leaq	.LC17(%rip), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL11SliceHelperENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcb
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L508
.L476:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L509
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L506:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L510
.L480:
	movq	%rbx, _ZZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateEE28trace_event_unique_atomic296(%rip)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L507:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L511
.L482:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L483
	movq	(%rdi), %rax
	call	*8(%rax)
.L483:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L484
	movq	(%rdi), %rax
	call	*8(%rax)
.L484:
	leaq	.LC16(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L508:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L505:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$686, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L511:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC16(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L510:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L480
.L509:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20061:
	.size	_ZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL57Builtin_Impl_Stats_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"V8.Builtin_ArrayBufferConstructor_DoNotInitialize"
	.section	.text._ZN2v88internalL57Builtin_Impl_Stats_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL57Builtin_Impl_Stats_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL57Builtin_Impl_Stats_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE:
.LFB20044:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L541
.L513:
	movq	_ZZN2v88internalL57Builtin_Impl_Stats_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateEE27trace_event_unique_atomic89(%rip), %rbx
	testq	%rbx, %rbx
	je	.L542
.L515:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L543
.L517:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL51Builtin_Impl_ArrayBufferConstructor_DoNotInitializeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L544
.L521:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L545
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L542:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L546
.L516:
	movq	%rbx, _ZZN2v88internalL57Builtin_Impl_Stats_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateEE27trace_event_unique_atomic89(%rip)
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L543:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L547
.L518:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L519
	movq	(%rdi), %rax
	call	*8(%rax)
.L519:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L520
	movq	(%rdi), %rax
	call	*8(%rax)
.L520:
	leaq	.LC18(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L544:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L541:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$683, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L547:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC18(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L546:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L516
.L545:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20044:
	.size	_ZN2v88internalL57Builtin_Impl_Stats_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE, .-_ZN2v88internalL57Builtin_Impl_Stats_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_Stats_ArrayBufferConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"V8.Builtin_ArrayBufferConstructor"
	.section	.text._ZN2v88internalL41Builtin_Impl_Stats_ArrayBufferConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_Stats_ArrayBufferConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_Stats_ArrayBufferConstructorEiPmPNS0_7IsolateE:
.LFB20041:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L577
.L549:
	movq	_ZZN2v88internalL41Builtin_Impl_Stats_ArrayBufferConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic61(%rip), %rbx
	testq	%rbx, %rbx
	je	.L578
.L551:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L579
.L553:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL35Builtin_Impl_ArrayBufferConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L580
.L557:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L581
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L578:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L582
.L552:
	movq	%rbx, _ZZN2v88internalL41Builtin_Impl_Stats_ArrayBufferConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic61(%rip)
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L579:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L583
.L554:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L555
	movq	(%rdi), %rax
	call	*8(%rax)
.L555:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L556
	movq	(%rdi), %rax
	call	*8(%rax)
.L556:
	leaq	.LC19(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L580:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L577:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$682, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L583:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC19(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L582:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L552
.L581:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20041:
	.size	_ZN2v88internalL41Builtin_Impl_Stats_ArrayBufferConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_Stats_ArrayBufferConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Builtin_ArrayBufferConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Builtin_ArrayBufferConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Builtin_ArrayBufferConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Builtin_ArrayBufferConstructorEiPmPNS0_7IsolateE:
.LFB20042:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L588
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL35Builtin_Impl_ArrayBufferConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L588:
	.cfi_restore 6
	jmp	_ZN2v88internalL41Builtin_Impl_Stats_ArrayBufferConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20042:
	.size	_ZN2v88internal30Builtin_ArrayBufferConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal30Builtin_ArrayBufferConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal46Builtin_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal46Builtin_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal46Builtin_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE, @function
_ZN2v88internal46Builtin_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE:
.LFB20045:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L593
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL51Builtin_Impl_ArrayBufferConstructor_DoNotInitializeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	.cfi_restore 6
	jmp	_ZN2v88internalL57Builtin_Impl_Stats_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20045:
	.size	_ZN2v88internal46Builtin_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE, .-_ZN2v88internal46Builtin_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Builtin_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Builtin_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Builtin_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Builtin_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE:
.LFB20048:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L598
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL46Builtin_Impl_ArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L598:
	.cfi_restore 6
	jmp	_ZN2v88internalL52Builtin_Impl_Stats_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20048:
	.size	_ZN2v88internal41Builtin_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE, .-_ZN2v88internal41Builtin_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal47Builtin_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal47Builtin_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE
	.type	_ZN2v88internal47Builtin_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE, @function
_ZN2v88internal47Builtin_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE:
.LFB20051:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L603
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL52Builtin_Impl_SharedArrayBufferPrototypeGetByteLengthENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L603:
	.cfi_restore 6
	jmp	_ZN2v88internalL58Builtin_Impl_Stats_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20051:
	.size	_ZN2v88internal47Builtin_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE, .-_ZN2v88internal47Builtin_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Builtin_ArrayBufferIsViewEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Builtin_ArrayBufferIsViewEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Builtin_ArrayBufferIsViewEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Builtin_ArrayBufferIsViewEiPmPNS0_7IsolateE:
.LFB20054:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L610
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L606
.L609:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	movq	-1(%rax), %rcx
	cmpw	$1087, 11(%rcx)
	jne	.L611
.L608:
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	movq	-1(%rax), %rax
	cmpw	$1086, 11(%rax)
	jne	.L609
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L610:
	jmp	_ZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20054:
	.size	_ZN2v88internal25Builtin_ArrayBufferIsViewEiPmPNS0_7IsolateE, .-_ZN2v88internal25Builtin_ArrayBufferIsViewEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal39Builtin_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Builtin_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE
	.type	_ZN2v88internal39Builtin_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE, @function
_ZN2v88internal39Builtin_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE:
.LFB20059:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L616
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movl	$1, %r8d
	leaq	.LC15(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL11SliceHelperENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcb
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L616:
	.cfi_restore 6
	jmp	_ZN2v88internalL50Builtin_Impl_Stats_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20059:
	.size	_ZN2v88internal39Builtin_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE, .-_ZN2v88internal39Builtin_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Builtin_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Builtin_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Builtin_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Builtin_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE:
.LFB20062:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L621
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	xorl	%r8d, %r8d
	leaq	.LC17(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL11SliceHelperENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcb
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore 6
	jmp	_ZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20062:
	.size	_ZN2v88internal33Builtin_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE, .-_ZN2v88internal33Builtin_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal30Builtin_ArrayBufferConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal30Builtin_ArrayBufferConstructorEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal30Builtin_ArrayBufferConstructorEiPmPNS0_7IsolateE:
.LFB25034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25034:
	.size	_GLOBAL__sub_I__ZN2v88internal30Builtin_ArrayBufferConstructorEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal30Builtin_ArrayBufferConstructorEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal30Builtin_ArrayBufferConstructorEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateEE28trace_event_unique_atomic296,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateEE28trace_event_unique_atomic296, @object
	.size	_ZZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateEE28trace_event_unique_atomic296, 8
_ZZN2v88internalL44Builtin_Impl_Stats_ArrayBufferPrototypeSliceEiPmPNS0_7IsolateEE28trace_event_unique_atomic296:
	.zero	8
	.section	.bss._ZZN2v88internalL50Builtin_Impl_Stats_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateEE28trace_event_unique_atomic289,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL50Builtin_Impl_Stats_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateEE28trace_event_unique_atomic289, @object
	.size	_ZZN2v88internalL50Builtin_Impl_Stats_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateEE28trace_event_unique_atomic289, 8
_ZZN2v88internalL50Builtin_Impl_Stats_SharedArrayBufferPrototypeSliceEiPmPNS0_7IsolateEE28trace_event_unique_atomic289:
	.zero	8
	.section	.bss._ZZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateEE28trace_event_unique_atomic119,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateEE28trace_event_unique_atomic119, @object
	.size	_ZZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateEE28trace_event_unique_atomic119, 8
_ZZN2v88internalL36Builtin_Impl_Stats_ArrayBufferIsViewEiPmPNS0_7IsolateEE28trace_event_unique_atomic119:
	.zero	8
	.section	.bss._ZZN2v88internalL58Builtin_Impl_Stats_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic109,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL58Builtin_Impl_Stats_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic109, @object
	.size	_ZZN2v88internalL58Builtin_Impl_Stats_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic109, 8
_ZZN2v88internalL58Builtin_Impl_Stats_SharedArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic109:
	.zero	8
	.section	.bss._ZZN2v88internalL52Builtin_Impl_Stats_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateEE27trace_event_unique_atomic98,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL52Builtin_Impl_Stats_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateEE27trace_event_unique_atomic98, @object
	.size	_ZZN2v88internalL52Builtin_Impl_Stats_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateEE27trace_event_unique_atomic98, 8
_ZZN2v88internalL52Builtin_Impl_Stats_ArrayBufferPrototypeGetByteLengthEiPmPNS0_7IsolateEE27trace_event_unique_atomic98:
	.zero	8
	.section	.bss._ZZN2v88internalL57Builtin_Impl_Stats_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateEE27trace_event_unique_atomic89,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL57Builtin_Impl_Stats_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateEE27trace_event_unique_atomic89, @object
	.size	_ZZN2v88internalL57Builtin_Impl_Stats_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateEE27trace_event_unique_atomic89, 8
_ZZN2v88internalL57Builtin_Impl_Stats_ArrayBufferConstructor_DoNotInitializeEiPmPNS0_7IsolateEE27trace_event_unique_atomic89:
	.zero	8
	.section	.bss._ZZN2v88internalL41Builtin_Impl_Stats_ArrayBufferConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic61,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Builtin_Impl_Stats_ArrayBufferConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic61, @object
	.size	_ZZN2v88internalL41Builtin_Impl_Stats_ArrayBufferConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic61, 8
_ZZN2v88internalL41Builtin_Impl_Stats_ArrayBufferConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic61:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC1:
	.long	0
	.long	1139802112
	.align 8
.LC2:
	.long	0
	.long	1138753536
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
